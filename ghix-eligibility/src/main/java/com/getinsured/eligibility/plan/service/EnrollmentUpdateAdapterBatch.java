package com.getinsured.eligibility.plan.service;

import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto.ENROLLEE_ACTION;
import com.getinsured.hix.dto.enrollment.EnrollmentCoreDto;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.dto.enrollment.LocationCoreDto;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.enums.MaritalStatusEnum;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Component("enrollmentUpdateAdapterBatch")
public class EnrollmentUpdateAdapterBatch {

	private static final String NO_NEW_DENTAL_PLAN_INFORMATION_FOUND = "No new dental plan information found!";
	private static final String NO_NEW_HEALTH_PLAN_INFORMATION_FOUND = "No new health plan information found!";
	private static final String AI = "AI";
	private static final String MARRIED = "Married";
	private static final String TRUE = "true";
	private static final String YES = "Yes";
	private static final String SINGLE = "Single";
	private static final String N = "N";
	private static final String T = "T";
	private static final String U = "U";
	private static final String F = "F";
	private static final String M = "M";
	private static final String MALE = "Male";
	private static final String SPA = "spa";
	private static final String SPANISH = "Spanish";
	private static final String ENG = "eng";
	private static final String ENGLISH = "English";
	private static final String MEMBER_ID = "memberId";
	private static final String RELATIONSHIP_CODE = "relationshipCode";
	private static final String EXCEPTION_OCCURED_WHILE_GETTING_BENCHMARK_PREMIUM = "Exception occured while getting benchmark premium";
	private static final String INVALID_PLAN_AVAILABILITY_ADAPTER_RESPONSE_FOUND = "Invalid planAvailabilityAdapterResponse found";
	private static final String SUPPLIED_APPLICATION_IS_NOT_ENROLLED = "Supplied application is not enrolled ";
	private static final String AUTO_ENROLLMENT_UPDATE_BATCH = "AUTO_ENROLLMENT_UPDATE_BATCH";
	private static final String EXCEPTION = "Exception - ";
	private static final String NEW_LINE = "\n";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String EMPTY_RESPONSE_FROM_AUTOMATE_ENROLLMENT_API = "Empty response from Automate Special enrollment API";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";
	private static final String RESPONSE = "Response - ";
	private static final String REQUEST = "Request - ";
	private final GsonBuilder gsonBuilder = new GsonBuilder();

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private IntegrationLogService integrationLogService;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private BenchmarkPlanService benchmarkPlanService;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;

	public String execute(Long enrolledSsapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {

		StringBuilder finalPayload = new StringBuilder();
		gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT);
		Gson gson = gsonBuilder.create();
	
		String status = GhixConstants.RESPONSE_FAILURE;

		EnrollmentUpdateRequest request;
		try {
			validateRequest(planAvailabilityAdapterResponse);
			
			Map<String, ApplicantPlanDto> healthPlanDisplayMap = planAvailabilityAdapterResponse.getHealthPlanDisplayMap();
			Map<String, ApplicantPlanDto> dentalPlanDisplayMap = planAvailabilityAdapterResponse.getDentalPlanDisplayMap();
			
			request = formRequest(enrolledSsapApplicationId, healthPlanDisplayMap, dentalPlanDisplayMap, healthEnrollees, dentalEnrollees, planAvailabilityAdapterResponse);

			String requestJson = gson.toJson(request);

			finalPayload.append(REQUEST).append(requestJson);
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.AUTOMATE_SPECIAL_ENROLLMENT_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON,
			        String.class, requestJson);

			if (response == null || response.getBody() == null) {
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_AUTOMATE_ENROLLMENT_API);
			}
			
			String responseJson = response.getBody();
			EnrollmentResponse enrollmentResponse = gson.fromJson(responseJson, EnrollmentResponse.class);

			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);

			if (enrollmentResponse != null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus())) {
				status = GhixConstants.RESPONSE_SUCCESS;
			} else {
				/* Donot throw exception as we have persisted events */			
			}


		} catch (Exception e) {
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(enrolledSsapApplicationId, finalPayload.toString(), status);
		}

		return status;

	}

	private void validateRequest(PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {
		boolean isValid = true;
		if (planAvailabilityAdapterResponse != null){
			if (!planAvailabilityAdapterResponse.isDentalEnrolled() && !planAvailabilityAdapterResponse.isHealthEnrolled()){
				isValid = false;
			} 
			if (planAvailabilityAdapterResponse.getHealthPlanDisplayMap() == null && planAvailabilityAdapterResponse.getDentalPlanDisplayMap() == null){
				isValid = false;
			}
		} else {
			isValid = false;
		}
		
		if (!isValid){
			throw new GIRuntimeException(INVALID_PLAN_AVAILABILITY_ADAPTER_RESPONSE_FOUND);
		}
	}

	private EnrollmentUpdateRequest formRequest(Long enrolledSsapApplicationId, Map<String, ApplicantPlanDto> healthPlanDisplayMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap,
			List<String> healthEnrollees, List<String> dentalEnrollees,
			PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {

		SsapApplication enrolledApplication = validate(enrolledSsapApplicationId);

		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(enrolledApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		SsapApplicant primaryApplicantDB = getPrimaryApplicantDB(enrolledApplication);
		HouseholdMember primaryApplicantJson = getPrimaryApplicantJson(members, primaryApplicantDB);

		if (primaryApplicantDB == null || primaryApplicantJson == null) {
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}
		
		Household household = cmrHouseholdRepository.findOne(enrolledApplication.getCmrHouseoldId().intValue());

		// populate applicant event map to draw enrollment OTHER sections ONLY
		Map<String, SsapApplicant> changeEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();
		
		for (SsapApplicant applicant : enrolledApplication.getSsapApplicants()) {
			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						changeEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				changeEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}


		EnrollmentUpdateRequest request = new EnrollmentUpdateRequest();
		List<EnrollmentCoreDto> enrollmentDto = new ArrayList<EnrollmentCoreDto>();

		// populate health only if enrolledApp was enrolled in health
		if (planAvailabilityAdapterResponse.isHealthEnrolled() && 
				planAvailabilityAdapterResponse.isProcessHealth() &&
					planAvailabilityAdapterResponse.isHealthTerminationFuture()){
			// HLT
			EnrollmentCoreDto healthEnrollment = populateHealthEnrollment( changeEligibleApplicantDBRowMap,  
			        changeEligibleApplicantJsonRowMap, healthPlanDisplayMap, primaryApplicantJson, planAvailabilityAdapterResponse, healthEnrollees, enrolledApplication, household);
	
			healthEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getHealthEnrollmentId())));
			healthEnrollment.setSsapApplicationid(enrolledSsapApplicationId);
			healthEnrollment.setCsrLevel(enrolledApplication.getCsrLevel());
			healthEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());
			
			enrollmentDto.add(healthEnrollment);
		}
		// DLT
		if (planAvailabilityAdapterResponse.isDentalEnrolled() && 
				planAvailabilityAdapterResponse.isProcessDental() &&
					planAvailabilityAdapterResponse.isDentalTerminationFuture()){
			if (dentalPlanDisplayMap.size() > 0) {
				EnrollmentCoreDto dentalEnrollment = populateDentalEnrollment(changeEligibleApplicantDBRowMap, changeEligibleApplicantJsonRowMap, dentalPlanDisplayMap,
				        primaryApplicantJson, planAvailabilityAdapterResponse, enrolledApplication, dentalEnrollees, household);
	
				dentalEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getDentalEnrollmentId())));
				dentalEnrollment.setSsapApplicationid(enrolledSsapApplicationId);
				dentalEnrollment.setCsrLevel(enrolledApplication.getCsrLevel());
				dentalEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());
			
				enrollmentDto.add(dentalEnrollment);
			}
		}
		// add enrollments to request
		request.setEnrollmentDto(enrollmentDto);

		return request;
	}

	private EnrollmentCoreDto populateDentalEnrollment(Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
	        Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap, HouseholdMember primaryApplicantJson, 
	        PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, SsapApplication currentApplication, List<String> dentalEnrollees, Household household) {
		EnrollmentCoreDto dentalEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> dentalEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);

		ApplicantPlanDto memberPlan = null;

		if (planAvailabilityAdapterResponse.isDentalPlanAvailable()){
			// other eligible members
			for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
				if (dentalEnrollees.contains(otherEligibleApplicantGuid)){
					EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
					SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
					HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
					memberPlan = dentalPlanDisplayMap.get(otherEligibleApplicantGuid);
		
					populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE, household);
		
					enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, String.valueOf(memberDB.getPersonId()), currentApplication,primaryApplicantJson));
		
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
					
					DateTime coverageStartDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate()).plusDays(ReferralConstants.ONE);
					
					enrolleeCoreDto.setEffectiveStartDate(TSDate.getNoOffsetTSDate(coverageStartDate.getMillis()));
					dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
				}
			}
			
			if (memberPlan == null) {
				// this should never happen!
				throw new GIRuntimeException(NO_NEW_DENTAL_PLAN_INFORMATION_FOUND);
			}
			
			dentalEnrollment.setAptcAmt(ReferralConstants.N.equalsIgnoreCase(currentApplication.getFinancialAssistanceFlag()) ? null : memberPlan.getPlanLevelMonthlySubsidy());
			dentalEnrollment.setPlanId(memberPlan.getPlanId().intValue());
			dentalEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
			dentalEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());
		} else {
			// remove members
			for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
				if (dentalEnrollees.contains(otherEligibleApplicantGuid)){
					EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
					SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
					HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
					memberPlan = dentalPlanDisplayMap.get(otherEligibleApplicantGuid);
		
					populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.TERM, household);
		
					enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, String.valueOf(memberDB.getPersonId()), currentApplication,primaryApplicantJson));
		
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
					enrolleeCoreDto.setEffectiveEndDate(planAvailabilityAdapterResponse.getCoverageStartDate());
					dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
				}
			}
		}

		//dentalEnrollment.setAptcAmt(ReferralUtil.convertToFloat(currentApplication.getMaximumAPTC()));
		dentalEnrollment.setEnrolleeCoreDtoList(dentalEnrolleeCoreDtoList);

		return dentalEnrollment;
	}

	private EnrollmentCoreDto populateHealthEnrollment(Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
	        Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap,
	        Map<String, ApplicantPlanDto> healthPlanDisplayMap, HouseholdMember primaryApplicantJson, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, 
	        List<String> healthEnrollees, SsapApplication currentApplication, Household household) {

		EnrollmentCoreDto healthEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> healthEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		ApplicantPlanDto memberPlan = null;
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);

		
		if (planAvailabilityAdapterResponse.isHealthPlanAvailable()){
			// other eligible members
			for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
				if (healthEnrollees.contains(otherEligibleApplicantGuid)){
					EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
					SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
					HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
					memberPlan = healthPlanDisplayMap.get(otherEligibleApplicantGuid);
	
					populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE, household);
		
					enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, String.valueOf(memberDB.getPersonId()), currentApplication,primaryApplicantJson));
		
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
					
					DateTime coverageStartDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate()).plusDays(ReferralConstants.ONE);
					
					enrolleeCoreDto.setEffectiveStartDate(TSDate.getNoOffsetTSDate(coverageStartDate.getMillis()));
		
					healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
				}
			}
			

			if (memberPlan == null) {
				// this should never happen!
				throw new GIRuntimeException(NO_NEW_HEALTH_PLAN_INFORMATION_FOUND);
			}

			healthEnrollment.setAptcAmt(ReferralConstants.N.equalsIgnoreCase(currentApplication.getFinancialAssistanceFlag()) ? null : memberPlan.getPlanLevelMonthlySubsidy());
			healthEnrollment.setPlanId(memberPlan.getPlanId().intValue());
			healthEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
			healthEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());

		} else {
			// remove members
			for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
				if (healthEnrollees.contains(otherEligibleApplicantGuid)){
					EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
					SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
					HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
					memberPlan = healthPlanDisplayMap.get(otherEligibleApplicantGuid);
	
					populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.TERM, household);
					
					enrolleeCoreDto.setEffectiveEndDate(planAvailabilityAdapterResponse.getCoverageStartDate());
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
		
					healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
				}
			}
			
		}
		
		//healthEnrollment.setAptcAmt(ReferralUtil.convertToFloat(currentApplication.getMaximumAPTC()));
		healthEnrollment.setEnrolleeCoreDtoList(healthEnrolleeCoreDtoList);
		
		return healthEnrollment;
	}

	private String populateSubscriberFlag(SsapApplicant memberDB, Map<String, ApplicantPlanDto> applicantPlanMap) {
		String subscriberFlag = N;
		if (applicantPlanMap.containsKey(memberDB.getApplicantGuid())) {
			ApplicantPlanDto applicantPlanDto = applicantPlanMap.get(memberDB.getApplicantGuid());
			subscriberFlag = applicantPlanDto.getSubscriberFlag().name();
		}
		
		return subscriberFlag;
	}

	private void populateEnrolleeCoreDto(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB, HouseholdMember memberJson, 
			ApplicantPlanDto memberPlan, HouseholdMember primaryApplicantJson, 
	        SsapApplication currentApplication, Map<String, ApplicantPlanDto> planMap, SsapApplicant primaryApplicant, ENROLLEE_ACTION action, Household household) {

		enrolleeCoreDto.setExchgIndivIdentifier(memberDB.getApplicantGuid());
		enrolleeCoreDto.setTaxIdNumber(memberDB.getSsn());
		
		enrolleeCoreDto.setLastName(memberDB.getLastName());
		enrolleeCoreDto.setFirstName(memberDB.getFirstName());
		enrolleeCoreDto.setMiddleName(memberDB.getMiddleName());
		enrolleeCoreDto.setSuffix(memberDB.getNameSuffix());
		enrolleeCoreDto.setBirthDate(memberDB.getBirthDate());

		enrolleeCoreDto.setMaintainenceReasonCode(AI);

		String primaryPhone = null, secondaryPhone = null;
		if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) && !StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
			secondaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		}

		if (StringUtils.isEmpty(primaryPhone)) {
			primaryPhone = household.getPhoneNumber();
		}
		enrolleeCoreDto.setPrimaryPhoneNo(primaryPhone);
		enrolleeCoreDto.setSecondaryPhoneNo(secondaryPhone);

		if (EnrolleeCoreDto.ENROLLEE_ACTION.TERM != action) {

			enrolleeCoreDto.setRatingArea(String.valueOf(memberPlan.getRegion()));
			enrolleeCoreDto.setTotalIndvResponsibilityAmt(memberPlan.getPremium());

			enrolleeCoreDto.setRaceMap(populateRaceDetails(memberJson));

			String relationshipToHCPLkpStr = populateRelationshipToHCP(memberJson, "" + memberDB.getPersonId(), primaryApplicantJson);
			enrolleeCoreDto.setRelationToHCPLkpStr(relationshipToHCPLkpStr);

			populateLocationDetails(enrolleeCoreDto, memberJson, primaryApplicantJson);
			String languageWrittenLkpStr = languageWritten(memberJson);
			

			String languageSpokenLkpStr = languageSpoken(memberJson);
			enrolleeCoreDto.setLanguageSpokenLkpStr(languageSpokenLkpStr);
			enrolleeCoreDto.setLanguageWrittenLkpStr(languageWrittenLkpStr);
		}
		populateTobaccoUsageDetails(enrolleeCoreDto, memberDB);

		//boolean citizenshipIndicator = memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null ? memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator().booleanValue() : false;
		
		// populateCitizenshipDetails(enrolleeCoreDto);

		populateMaritalStatus(enrolleeCoreDto, memberDB);

		populateGenderDetails(enrolleeCoreDto, memberDB);
		
		
		enrolleeCoreDto.setSubscriberNewFlag(populateSubscriberFlag(memberDB, planMap));
		
		
	}

	private void populateMaritalStatus(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String maritalStatusLkpStr = MaritalStatusEnum.value(memberDB.getMarried());
		if (YES.equalsIgnoreCase(memberDB.getMarried()) || TRUE.equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = MARRIED;
		}else if("No".equalsIgnoreCase(memberDB.getMarried()) || "false".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = SINGLE;
		}
		enrolleeCoreDto.setMaritalStatusLkpStr(maritalStatusLkpStr);
	}

	private String populateRelationshipToHCP(HouseholdMember memberJson, String personId, HouseholdMember primaryApplicantJson) {
		String relatioshipToHCPStr = null;

		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId()) && String.valueOf(PRIMARY_PERSON_ID).equalsIgnoreCase(bloodRelationship.getRelatedPersonId())) {
				relatioshipToHCPStr = bloodRelationship.getRelation();
			}
		}
		return relatioshipToHCPStr;
	}

	private String languageSpoken(HouseholdMember memberJson) {
		String languageSpoken = null;
		String languageSpokenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageSpoken = memberJson.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
		}

		if(StringUtils.isNotBlank(languageSpoken)) {
			languageSpokenCode = LanguageEnum.getCode(languageSpoken);								
		}
		return languageSpokenCode;
	}

	private String languageWritten(HouseholdMember memberJson) {
		String languageWritten = null;
		String languageWrittenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageWritten = memberJson.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
		}

		if(StringUtils.isNotBlank(languageWritten)) {
			languageWrittenCode = LanguageEnum.getCode(languageWritten);								
		}
		return languageWrittenCode;
	}

	private void populateGenderDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String genderLkpStr = MALE.equalsIgnoreCase(memberDB.getGender()) ? M : F;
		enrolleeCoreDto.setGenderLkpStr(genderLkpStr);
	}

	private void populateLocationDetails(EnrolleeCoreDto enrolleeCoreDto, HouseholdMember memberJson, HouseholdMember primaryApplicantJson) {

		Address homeAddress = memberJson.getHouseholdContact().getHomeAddress();

		Address mailingAddress = memberJson.getHouseholdContact().getMailingAddress();

		enrolleeCoreDto.setHomeAddressDto(populateHomeAddress(homeAddress, memberJson, primaryApplicantJson));
		enrolleeCoreDto.setMailingAddressDto(populateAddress(mailingAddress));
	}
	
	private LocationCoreDto populateHomeAddress(Address address, HouseholdMember memberJson, HouseholdMember primaryApplicantJson) {
		LocationCoreDto locationCoreDto = null;
		if (address != null) {
			locationCoreDto = new LocationCoreDto();
			locationCoreDto.setAddress1(address.getStreetAddress1());
			locationCoreDto.setAddress2(address.getStreetAddress2());
			locationCoreDto.setCity(address.getCity());
			locationCoreDto.setState(address.getState());
			locationCoreDto.setZip(address.getPostalCode());
			locationCoreDto.setCountycode(getCounty(memberJson, primaryApplicantJson));

		}
		return locationCoreDto;
	}
	
	private String getCounty(HouseholdMember applicantJsonRow, HouseholdMember primaryApplicantJson) {
		String countyCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}

	private void populateTobaccoUsageDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String tobaccoUsage = U;
		if (memberDB.getTobaccouser() != null) {
			tobaccoUsage = StringUtils.equalsIgnoreCase(ReferralConstants.Y, memberDB.getTobaccouser()) ? T : N;
		}

		enrolleeCoreDto.setTobaccoUsageLkpStr(tobaccoUsage);
	}


	private LocationCoreDto populateAddress(Address address) {
		LocationCoreDto locationCoreDto = null;
		if (address != null) {
			locationCoreDto = new LocationCoreDto();
			locationCoreDto.setAddress1(address.getStreetAddress1());
			locationCoreDto.setAddress2(address.getStreetAddress2());
			locationCoreDto.setCity(address.getCity());
			locationCoreDto.setState(address.getState());
			locationCoreDto.setZip(address.getPostalCode());
			locationCoreDto.setCountycode(address.getCountyCode());

		}
		return locationCoreDto;
	}

	private Map<String, String> populateRaceDetails(HouseholdMember memberJson) {

		EthnicityAndRace ethnicityAndRace = memberJson.getEthnicityAndRace();
		Map<String, String> raceMap = null;
		if (ethnicityAndRace != null && ReferralUtil.listSize(ethnicityAndRace.getRace()) != 0) {

			raceMap = new HashMap<String, String>();
			for (Race race : ethnicityAndRace.getRace()) {
				raceMap.put(race.getCode(), race.getLabel() != null ? race.getLabel() : race.getOtherLabel());
			}

		}

		return raceMap;
	}

	private List<Map<String, String>> populateRelationship(HouseholdMember memberJson, String personId, SsapApplication currentApplication, HouseholdMember primaryApplicantJson) {
		List<Map<String, String>> relatioshipMapList = new ArrayList<Map<String, String>>();
		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		Map<String, String> applicantGuidMapwithPersonId = populateApplicantGuidWithPersonId(currentApplication.getSsapApplicants());

		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId())) {
				Map<String, String> relationshipMap = new HashMap<String, String>();
				relationshipMap.put(RELATIONSHIP_CODE, bloodRelationship.getRelation());
				relationshipMap.put(MEMBER_ID, applicantGuidMapwithPersonId.get(bloodRelationship.getRelatedPersonId()));
				relatioshipMapList.add(relationshipMap);
			}
		}

		return relatioshipMapList;
	}

	private Map<String, String> populateApplicantGuidWithPersonId(List<SsapApplicant> ssapApplicants) {
		Map<String, String> applicantGuidMapwithPersonId = new HashMap<String, String>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidMapwithPersonId.put(String.valueOf(ssapApplicant.getPersonId()), ssapApplicant.getApplicantGuid());
		}
		return applicantGuidMapwithPersonId;

	}

	private SsapApplication validate(Long ssapApplicationId) {
		
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}
		SsapApplication enrolledApp = ssapApplications.get(0);
		
		if (!ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equals(enrolledApp.getApplicationStatus())){
			throw new GIRuntimeException(SUPPLIED_APPLICATION_IS_NOT_ENROLLED + ssapApplicationId);
		}
		return enrolledApp;
	}

	private HouseholdMember getPrimaryApplicantJson(List<HouseholdMember> members, SsapApplicant primaryApplicantDB) {
		HouseholdMember primaryApplicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) primaryApplicantDB.getPersonId()) == 0) {
				primaryApplicantJson = householdMember;
				break;
			}
		}
		return primaryApplicantJson;
	}

	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.getPersonId() == 1) {
				primaryApplicantDB = applicant;
				break;
			}
		}
		return primaryApplicantDB;
	}

	

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, AUTO_ENROLLMENT_UPDATE_BATCH, status, ssapApplicationId, null);
	}
}
