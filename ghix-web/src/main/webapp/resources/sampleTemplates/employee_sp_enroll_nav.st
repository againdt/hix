
	$if(navDataList)$
		    <div class="row-fluid">
				<div id="progress-bar">
                  <div id="bar">
                  	<ul>
                  	$navDataList: {navData|
					        
							<li id="one"><span id="step-one" class="steps $navData.STEP_CLASS$" alt="$navData.ALT_TEXT$" aria-label="$navData.ALT_TEXT$"><span class="step-number" style="$navData.STEP_NUMBER_STYLE$" id="no-one">$i$</span></span><span id="s$i$" class="step-details">$navData.ALT_TEXT$</span></li>
					    }$
                  	</ul>
                  </div>
              </div>   
			</div>
			
			
			<div class="row-fluid">
            <div class="gutter10">
             
              <div class="span9" id="complete-oep">
                <p id="enroll-s1">$currentNavData.STEP_DESC$</p>
              </div>  
            	<div class="span3" id="make-changes">
                <button class="btn btn-primary makeChanges" alt="$currentNavData.ALT_TEXT$"  onClick="gotoNext('$navStep$')" >$currentNavData.ALT_TEXT$</button> 
              </div>
            
            </div>
            <div class="pull-left" style="font-size: 16px; color: red">* Please complete all four steps above for your special enrollment to be effective. </div>
          </div>
			
	$else$
		<div class="row-fluid">	
			<div id="complete-oep" class="span9">
				<p id="enroll-s5">
					If you experience any changes in circumstances such as marriage, divorce, birth of child, adoption, death of family member, or change in eligibility for 
	        		other coverage, you should report the changes here.
				</p>
			</div>
			<div id="report-Changes" class="span3">
				<a href="#" id="qualified-event-box" class="btn btn-primary makeChanges" data-toggle="modal">Report Changes</a>
			</div>				
		</div>	
	$endif$	
