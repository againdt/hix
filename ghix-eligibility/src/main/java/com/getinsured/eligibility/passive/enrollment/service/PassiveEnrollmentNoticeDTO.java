package com.getinsured.eligibility.passive.enrollment.service;

import java.util.Date;
import java.util.Set;

public class PassiveEnrollmentNoticeDTO {

	private String planName;
	private Float netPremiumAmt;
	private Date benefitEffectiveDate;
	private Set<String> enrolleeNames;
	private String insurerName;
	private Long ssapApplicationId;
	private String caseNumber;
	private String issuerName;
	private String activationUrl;

	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public Float getNetPremiumAmt() {
		return netPremiumAmt;
	}
	public void setNetPremiumAmt(Float netPremiumAmt) {
		this.netPremiumAmt = netPremiumAmt;
	}
	public Date getBenefitEffectiveDate() {
		return benefitEffectiveDate;
	}
	public void setBenefitEffectiveDate(Date benefitEffectiveDate) {
		this.benefitEffectiveDate = benefitEffectiveDate;
	}
	public Set<String> getEnrolleeNames() {
		return enrolleeNames;
	}
	public void setEnrolleeNames(Set<String> enrolleeNames) {
		this.enrolleeNames = enrolleeNames;
	}
	public String getInsurerName() {
		return insurerName;
	}
	public void setInsurerName(String insurerName) {
		this.insurerName = insurerName;
	}
	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getIssuerName() {
		return issuerName;
	}
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	public String getActivationUrl() {
		return activationUrl;
	}
	public void setActivationUrl(String activationUrl) {
		this.activationUrl = activationUrl;
	}
	@Override
	public String toString() {
		return "PassiveEnrollmentNoticeDTO [planName=" + planName
				+ ", netPremiumAmt=" + netPremiumAmt
				+ ", benefitEffectiveDate=" + benefitEffectiveDate
				+ ", enrolleeNames=" + enrolleeNames + ", insurerName="
				+ insurerName + ", ssapApplicationId=" + ssapApplicationId
				+ ", caseNumber=" + caseNumber + ", issuerName=" + issuerName
				+ ", activationUrl=" + activationUrl + "]";
	}

}
