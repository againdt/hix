<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>

<div id="container" style="width:100%; height:400px;"></div>
<div id="btnSubmit" align="center">
	<input type="button" name="downloadBtn" id="downloadBtn" value="<spring:message code="btn.ticketstat.download"/>" class="btn btn-primary btn-small" />
</div>

<script type="text/javascript" >

$('#downloadBtn').click(function(){
	var ticketData = ${pieDataJson};
	var countBy = '${countBy}';
	ticketData=JSON.stringify(ticketData);
	countBy = countBy.charAt(0).toUpperCase() + countBy.slice(1);
	var validateUrl='${pageContext.request.contextPath}'+'/ticketmgmt/ticket/downloadTicketReport?type=Ticket '+countBy+'&reportName=TicketStatsBy'+countBy+'&ticketData='+encodeURI(ticketData);
	window.location.assign(
			validateUrl
   );

});
$(function () {
	var dataString = ${pieDataString};
	var countBy = '${countBy}';
	var titleText = '';
	if(countBy == 'type') {
		titleText = "<spring:message code='text.title.type.ticketstat' />";
	} else {
		titleText = "<spring:message code='text.title.priority.ticketstat' />";
	}
	
	// Radialize the colors
	Highcharts.getOptions().colors = Highcharts.map(Highcharts.getOptions().colors, function(color) {
	    return {
	        radialGradient: { cx: 0.5, cy: 0.3, r: 0.7 },
	        stops: [
	            [0, color],
	            [1, Highcharts.Color(color).brighten(-0.3).get('rgb')] // darken
	        ]
	    };
	});
	
	// Build the chart
    $('#container').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: titleText
        },
        credits: {
            enabled: false
        },
        tooltip: {
        	pointFormat: '{series.name}: <b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    color: '#000000',
                    connectorColor: '#000000',
                    formatter: function() {
                        return '<b>'+ this.point.name +'</b>: '+ this.y;
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: "<spring:message code='text.series.ticketstat' />",
            data: dataString
           
        }]
    });
});



</script>
	