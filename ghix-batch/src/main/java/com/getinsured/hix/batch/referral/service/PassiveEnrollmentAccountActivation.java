package com.getinsured.hix.batch.referral.service;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.jpa.AccountActivationServiceImpl;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Service
@Qualifier("passiveEnrollmentAccountActivation")
public class PassiveEnrollmentAccountActivation extends AccountActivationServiceImpl {


	@Override
	public AccountActivation initiateActivationForCreatedRecord(CreatedObject createdObject, CreatorObject creatorObject, Integer expirationDays) throws GIException {

		//1. perform basic incoming data validation
		if (createdObject == null){
			throw new IllegalArgumentException("createdObject cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getFullName())){
			throw new IllegalArgumentException("createdObject.FullName cannot be blank");
		}

		if (createdObject.getObjectId() == null){
			throw new IllegalArgumentException("createdObject.ObjectId cannot be blank");
		}

		if (StringUtils.isEmpty(createdObject.getRoleName())){
			throw new IllegalArgumentException("createdObject.RoleName cannot be blank");
		}
		validateCreatorObjectData(creatorObject);

		//2. form ActivationJson object
		ActivationJson jsonObject = createActivationJson(createdObject, creatorObject);

		//4. convert it to JSON string
		String jsonString = gson.toJson(jsonObject);

		//5. generate secure random token
		String randomToken = generateRandomToken();

		//6. create new AccountActivation object & populate details & persist record
		AccountActivation accountActivation = formAccountActivationEntity(jsonString, randomToken, createdObject, creatorObject,expirationDays);//HIX-30523
		accountActivation = iAccountActivationRepository.save(accountActivation);

		return accountActivation;
	}


	@Override
	public int sendActivationReminders(AccountActivation accountActivation){

		throw new GIRuntimeException("Method not implemented!");

	}

	

}

