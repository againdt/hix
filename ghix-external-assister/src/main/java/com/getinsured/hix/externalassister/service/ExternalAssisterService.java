package com.getinsured.hix.externalassister.service;

import java.util.List;

import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;

public interface ExternalAssisterService {
	
	public DesignateAssisterResponse designator(DesignateAssisterRequest designateAssisterRequest);
	
	public DesignateAssisterResponse getAssisterDetailForhouseholdId(int householdId);
	
	public List<DesignateAssisterResponse> householdsForBroker(DesignateAssisterRequest designateAssisterRequest);

	public DesignateAssisterResponse getAssisterDetails(String externalAssisterId);

	String getExternalUserIdsForBroker(String externalAssisterId);

}
