package com.getinsured.iex.hub.vlp37.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;

public class PassportNumberValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PassportNumberValidator.class);
	
	/** 
	 * Validations for passport number
 	 * Should be between 6 and 12 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String passportNumber = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			passportNumber = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(passportNumber.length() < VLPServiceConstants.PASSPORT_NUMBER_MIN_LEN
				|| passportNumber.length() > VLPServiceConstants.PASSPORT_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.PASSPORT_NUMBER_LEN_ERROR_MESSAGE);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
