package com.getinsured.iex.hub.nmec.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.IndividualResponseSetType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.IndividualResponseType;

public class IndividualResponseSetTypeWrapper implements JSONAware {

	private List<IndividualResponseTypeWrapper> individualResponseTypeWrappers;
	
	public IndividualResponseSetTypeWrapper(IndividualResponseSetType individualResponseSetType)
	{
		if(individualResponseSetType != null)
		{
			List<IndividualResponseType> listIndividualResponseType = individualResponseSetType.getIndividualResponse();
			if(listIndividualResponseType != null){
    			int responseCount = listIndividualResponseType.size();
    			
    			if(responseCount > 0)
    			{
    				individualResponseTypeWrappers = new ArrayList<IndividualResponseTypeWrapper>();
    			}
    			
    			for(int i = 0; i < responseCount; i++)
    			{
    				IndividualResponseTypeWrapper individualResponseTypeWrapper = new IndividualResponseTypeWrapper(listIndividualResponseType.get(i));
    				individualResponseTypeWrappers.add(individualResponseTypeWrapper);
    			}
			}
		}
	}
	
	@Override
    @SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		JSONArray individualResponseType = null;
		
		if(this.individualResponseTypeWrappers != null){
			individualResponseType = new JSONArray();
			for(IndividualResponseTypeWrapper data : this.individualResponseTypeWrappers){
				individualResponseType.add(data);
			}
		}
		obj.put("IndividualResponseType", individualResponseType);
		return obj.toJSONString();
	}
}
