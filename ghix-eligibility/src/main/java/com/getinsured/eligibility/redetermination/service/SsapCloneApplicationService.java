package com.getinsured.eligibility.redetermination.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface SsapCloneApplicationService {

	Boolean isValidLCEApplication(SsapApplication ssapApplication);
	
	SsapApplication getEnrolledSsapApplicationByMaxIdAndCmrHouseoldId(BigDecimal cmrHouseoldId);
	
	SsapApplication saveSsapSepApplication(SsapApplication ssapApplication, boolean updateJson) throws Exception;

	SsapApplication cloneSsapApplicationFromParent(String ssapJson, SsapApplication currentApplication, SsapApplication parentApplication, SepRequestParamDTO sepRequestParamDTO, String applicationType, String applicationSource, String applicationStatus, Long renewalYear);
	
	void closeSsapApplication(SsapApplication ssapApplication);

	List<Long> getEnrolledSsapApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize);

	List<Long> getSsapApplicationsByCoverageYearAndApplicationStatusList(long coverageYear, List<String> applicationStatusList, Long batchSize);
	
	List<Long> getOTRApplicationsByCoverageYear(long coverageYear, List<String> applicationStatusList, Long batchSize);
	
	Long findOpenApplicationByCoverageYearAndCmrHouseoldId(BigDecimal cmrHouseoldId, long coverageYear);

	List<SsapApplication> getRenewedSsapApplicationsByCoverageYear(Long renewalYear,Long batchSize);

	List<RenewalApplication> getAutoRenewalApplicationsByCoverageYear(List<RenewalStatus> statusList, Long coverageYear,long batchSize);

	List<Long> getAutoRenewalApplIdListByCoverageYear(List<RenewalStatus> statusList, Long coverageYear, long batchSize);

	List<RenewalApplication> getAutoRenewalApplListByIds(List<Long> renewalApplIdList);

	void updateCSRLevel(SsapApplication ssapApplication) throws Exception;

	void logData(Set<Future<Map<String, String>>> tasks,String batchName);

	void logData(List<Map<String, String>> jsonResultSuccess, List<Map<String, String>> jsonResultFailure, Map<String, Long> summary, String batchName);

	long findPreivousYearEnrolledApplication(BigDecimal cmrHouseholdId) throws GIException;
	
	Set<String> dependentDentalAgeCheck(SsapApplication ssapApplication,List<SsapApplicant> ssapApplicants,Long previousYearEnrolledApplication,Long renewalYear) throws GIException;

	Map<String,Object> dependentDentalAgeCheck(SsapApplication ssapApplication,Long renewalYear) throws GIException;
	
	void logProcessData(Set<Future<Map<String,String>>> tasks,String batchName);
	
	Float getRemainingAPTCForDental(SsapApplication ssapApplication) throws GIException;
	
	Date getCoverageStartDate(SsapApplication ssapApplication) throws GIException;

	float getEhbAmount(String caseNumber, Date coverageStartDate);

	void createApplicationEvents(SsapApplication ssapApplication, List<SsapApplicant> applicantList, String reasonCode, Long enrollmentId, String planId, String planType);
	
	List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYear(Long renewalYear,Long batchSize);
	
	SsapApplication findPreivousYearApplicationAndApplicants(Long previousYearApplicationId) throws GIException;
	
	List<SsapApplication> getAutoRenewalApplicationsForNextCoverageYearWithErrors(Long renewalYear,Long batchSize);
	
	List<Long> getSsapApplicationIdsForRenewal(Long renewalYear,Long batchSize);
	
	List<Long> getSsapApplicationIdsForErrorProcessing(Long renewalYear, int serverCount, int serverName);
	
	List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList);
	
	List<Long> getSsapAppIdsForRenewalWithSgStatus(Long renewalYear, Long batchSize);

	List<Long> getAutoRenewalApplIdListByServerName(List<RenewalStatus> statusList, Long coverageYear, int serverCount,
			int serverName, long batchSize);

	List<Long> getSsapApplIDsByCoverageYearAndApplStatusAndEligStatus(long coverageYear,
			List<String> applicationStatusList, Long batchSize);
	
	List<Long> getSsapApplicationIdsByServerName(Long renewalYear, int serverCount, int serverName, Long batchSize);

	List<Long> getEnrolledSsapApplicationsIdByCoverageYear(long coverageYear, List<String> applicationStatusList,
			Long batchSize, String renewalStatus);

}
