/*
 * Custom script for My Profile Page
 */

$(function(){
	
	//My Profile page model
	PrescreenProfileModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			claimerName: "",
			zipCode: "",
			isMarried : false,
			numberOfDependents : "",
			taxHouseholdIncome : 0.0,
			isIncomePageLatest : false,
			stateCode : "",
			county:""	
      	}
		
	});
	
	
	profileModel = new PrescreenProfileModel();
	
	prescrenUpdateView = new UpdateView({
			el : $ ('#profileForm'),
			model : profileModel,
			bindings : null
		}
	);
	
});