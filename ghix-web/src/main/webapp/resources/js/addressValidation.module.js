(function() {
	"use strict";
	var app = angular.module("addressValidationModule", []);
	
	app.value('addressValidationUrl', '/hix/address/validate');
	app.directive('addressValidationDirective', addressValidationDirective);
	
	addressValidationDirective.$inject = ['$http', 'addressValidationUrl'];
	
	function addressValidationDirective($http, addressValidationUrl){
		
		var directive = {
			restrict: 'AE',
			transclude: true,
			templateUrl: '/hix/resources/html/addressValidationModal.temp.html',
			controller: AddressValidationCtrl,
			controllerAs: 'vm',
			bindToController: true,
			scope:{
				addressValidationDirective: '=',
				addressValidationSameAddressDto: '=',
				addressValidationCallback: '&'
			},
			link: function(scope, element, attrs){
				element.on('click', '.address-validation-click', addressValidationCall);
							
				function addressValidationCall(){
					var address = {
						city: scope.addressValidationDirective.city,
						state: scope.addressValidationDirective.state,
					};
					
					if(scope.addressValidationSameAddressDto === false){
						address.addressLine1 = scope.addressValidationDirective.streetAddress1;
						address.addressLine2 = scope.addressValidationDirective.streetAddress2;
						address.zipcode = scope.addressValidationDirective.postalCode;
					}else{
						address.addressLine1 = scope.addressValidationDirective.addressLine1;
						address.addressLine2 = scope.addressValidationDirective.addressLine2;					
						address.zipcode = scope.addressValidationDirective.zipcode;
					}
					
					var promise = $http.post(addressValidationUrl, address);
					
					promise.success(function(response) {
						scope.addressValidatioResponse = response;
					
						//exact Match
						if(scope.addressValidatioResponse.exactMatch){
							scope.addressValidationCallback();
							
						//invalid
						}else if(scope.addressValidatioResponse.suggestions === null){
							scope.vm.addressSelected = angular.toJson(scope.addressValidationDirective);
							element.find('.addressNotFoundModal').modal();
						
						//valid but not exact Match	
						}else{
							scope.vm.addressSelected = scope.addressValidatioResponse.suggestions[0];
							
							getAddressValidationErrorMessage(scope.addressValidatioResponse.suggestions);
							
							//show addressMatchModal
							element.find('.addressMatchModal').modal();
							
						};
					});
					
					promise.error(function(response) {
						alert('Failed to check address, please try again later!');
				});
				
				 }
			}
		};
		
		return directive;
	}
	
	
	function AddressValidationCtrl($scope){
		var vm = this;
		vm.updateAddress = updateAddress;
		
		function updateAddress(){
			var addressSelectedObject = angular.fromJson(vm.addressSelected);
			
			if($scope.addressValidationSameAddressDto === false){
				
				if(addressSelectedObject.addressLine1 !== undefined){
					//select suggested address
					$scope.addressValidationDirective.streetAddress1 = addressSelectedObject.addressLine1;
					$scope.addressValidationDirective.streetAddress2 = addressSelectedObject.addressLine2;				
					$scope.addressValidationDirective.postalCode = addressSelectedObject.zipcode;
				}else{
					//select entered address
					$scope.addressValidationDirective.streetAddress1 = addressSelectedObject.streetAddress1;
					$scope.addressValidationDirective.streetAddress2 = addressSelectedObject.streetAddress2;				
					$scope.addressValidationDirective.postalCode = addressSelectedObject.postalCode;
				}
				
			}else{
			$scope.addressValidationDirective.addressLine1 = addressSelectedObject.addressLine1;
			$scope.addressValidationDirective.addressLine2 = addressSelectedObject.addressLine2;
				$scope.addressValidationDirective.zipcode = addressSelectedObject.zipcode;
			}
			
			$scope.addressValidationDirective.city = addressSelectedObject.city;
			$scope.addressValidationDirective.state = addressSelectedObject.state;
			
			$(".address-modal").modal('hide');
			$scope.addressValidationCallback();
		}
	}
	

	
	function getAddressValidationErrorMessage(suggestions){
		
		angular.forEach(suggestions, function(suggestion){
			
			suggestion.addressValidationErrorMessages = [];
			
			angular.forEach(suggestion, function(value, key){
				var errorMessage = '';
				if(value === true){
					switch(key) {
						case 'redundantSecondaryNumber':
							errorMessage = 'The secondary information (apartment, suite, etc.) does not match that on the record. Please check accuracy of the information.';
							break;
						case 'zipcodeCorrected':
							errorMessage = 'The address submitted has a different 5-digit ZIP. The correct ZIP Code is shown in the ZIP Code field.';
							break;
						case 'cityStateSpellingFixed':
							errorMessage = 'The spelling of the city name and/or state abbreviation has been updated with standard spelling or abbreviation.';
							break;
						case 'zipInvalid':
							errorMessage = 'The ZIP Code in the submitted address could not be found. Please check the accuracy of the submitted address.';
							break;
						case 'addressNotFound':
							errorMessage = 'The address, exactly as submitted, could not be found. Please verify the entire address is correct.';
							break;
						case 'missingSecondaryNumber':
							errorMessage = 'Our information indicates that this address is a multi-unit building. Please include apartment or suite number.';
							break;
						case 'insufficientAddressData':
							errorMessage = 'More than one record was found to satisfy the address as submitted. Please check the accuracy and completeness of the submitted address.';
							break;
						case 'fixedStreetSpelling':
							errorMessage = 'The spelling of the street name was changed in order to achieve a match.';
							break;	
						case 'fixedAbreviations':
							errorMessage = 'The mailing address you entered has been standardized. For example, if you entered STREET, our system would standardize that to ST.';
							break;
						case 'invalidSecondaryAddress':
							errorMessage = 'The secondary (apartment, suite, etc.)  information, is not valid. Please check accuracy of address.';
							break;
						case 'cityStateZipComboInvalid':
							errorMessage = 'The address, submitted, could not be found in the city, state, or ZIP Code provided.';
							break;
						case 'invalidDeliveryAddress':
							errorMessage = 'The address submitted is identified as a "small town default." The USPS does not deliver to this address. Please provide an address serviced by the post office.';
							break;	
						case 'otherReason':
							errorMessage = 'The submitted address did not contain complete or correct data to determine a single record. Please check the accuracy and completeness of the submitted address.';
							break;
						default:
							errorMessage = 'The submitted address did not contain complete or correct data to determine a single record. Please check the accuracy and completeness of the submitted address.';
					}
					
					suggestion.addressValidationErrorMessages.push(errorMessage);
				}
			});	
			
		});
		
	}

	
})();