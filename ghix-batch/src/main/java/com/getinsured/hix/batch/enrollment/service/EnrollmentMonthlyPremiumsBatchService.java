/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

/**
 * @author negi_s
 *
 */
public interface EnrollmentMonthlyPremiumsBatchService {
	
	/**
	 * Get unique enrollment Ids for given year
	 * @param year
	 * @return List<Integer>
	 */
	List<Integer> getEnrollmentIdsForYear(Integer year);
	
	 void populateMonthlyEnrolmentPremium(Integer enrollmentId, String fillOnlySLCSP, boolean updateLastSlice) throws Exception;

	List<Integer> getDeltaEnrollmentIdsForYear(Integer applicableYear);
	
	List<Integer> getEmptySLCSPEnrollmentIdsForYear(Integer year);
}
