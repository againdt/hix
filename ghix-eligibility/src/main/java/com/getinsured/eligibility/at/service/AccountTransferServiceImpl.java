package com.getinsured.eligibility.at.service;

import static com.getinsured.eligibility.at.ref.service.LceProcessHandlerService.ERROR_LCE_NOTIFICATION;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.mes.service.AtSpanProcessingServiceImpl;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.service.LceEditApplicationService;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.AtAdditionalParamsDTO;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.at.server.endpoint.schemavalidator.ATRequestValidator;
import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicantRepository;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.eligibility.ssap.model.EligibilityResponse;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilitySpansType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TimeshiftContext;
import com.getinsured.timeshift.util.TSDate;

@Service("accountTransferService")
@DependsOn("dynamicPropertiesUtil")
@Transactional
public class AccountTransferServiceImpl implements AccountTransferService {
	
	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private static final String FAILURE = "Failure";
	private static final String SUCCESS = "Success";
	private static final String AT_VERSION = "2.4";
	private static final String ONE_OR_MORE_RULES_FAILED_VALIDATION = "One or more rules failed validation";
	private static final String ERRORS_FOUND_IN_THE_ACCOUNT_TRANSFER_REQUEST_PAYLOAD_TYPE_OBJECT = "errors found in the AccountTransferRequestPayloadType object...";
	private static final String UNKNOWN_EXCEPTION = "Invalid Data Exception occurred during validating AT Request";
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
	
	private static final Logger LOGGER = Logger.getLogger(AccountTransferServiceImpl.class);
	private static int numberOfenrollmentDays = 60;
	private static final int IN_PROGRESS_WAIT_INTERVAL = 15;

	@Autowired private ATRequestValidator aTRequestValidator;

	@Autowired private GenericAtStrategyImpl genericAtStrategyImpl;
	@Autowired private AtSpanProcessingServiceImpl atSpanProcessingServiceImpl;
	/*@Autowired private IDProcessing iDProcessing;
	@Autowired private NMProcessing nMProcessing;
	@Autowired private CAProcessing cAProcessing;
	@Autowired private WAProcessing wAProcessing;
	@Autowired private CTProcessing cTProcessing;
	@Autowired private MNProcessing mNProcessing;*/
	@Autowired private IntegrationLogService integrationLogService;
	private AccountTransferStrategy accountTransferStrategy;
	@Autowired
	ATSpanProcessingRepository atSpanProcessingRepository;
	@Autowired 	private ReferralOEService referralOEService;
	
	@Autowired
	private ExceptionUtil exceptionUtil;

	@Autowired private GIWSPayloadService giwsPayloadService;
	@Autowired private OutboundATApplicantRepository outboundATApplicantRepository;
	
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;
	
	@Autowired
	private IUserRepository userRepository;
	
	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired LceEditApplicationService lceEditApplicationService;
	
	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	private static final String PROCESS_ELIGIBILITY = "PROCESS_ELIGIBILITY";
	
	@PostConstruct
	public void createStateContext() {
		accountTransferStrategy = genericAtStrategyImpl;
		
		/*String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

		//stateCode="NM";
		if ("NM".equals(stateCode)) {
			accountTransferStrategy = nMProcessing;
		}else if ("ID".equals(stateCode)) {
			accountTransferStrategy = iDProcessing;
		}else if ("CA".equals(stateCode)) {
			accountTransferStrategy = cAProcessing;
		}else if ("WA".equals(stateCode)) {
			accountTransferStrategy = wAProcessing;
		}else if ("CT".equals(stateCode)) {
			accountTransferStrategy = cTProcessing;
		}else if ("MN".equals(stateCode)) {
			accountTransferStrategy = mNProcessing;
		}else{
			LOGGER.error("Unknown state code found in global configuration.");
			throw new IllegalArgumentException("Unknown state code found in global configuration.");
		}*/
	}

	@Override
	public AccountTransferResponseDTO prepareATResponse(AccountTransferRequestPayloadType request, String inputXml){

		AccountTransferResponseDTO result = new AccountTransferResponseDTO();
		AccountTransferResponsePayloadType accountTransferResponsePayloadType;
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		
		if("NV".equalsIgnoreCase(stateCode)){
			if(request.getInsuranceApplication().getApplicationExtension() == null || request.getInsuranceApplication().getApplicationExtension().getCoverageYear() ==  null){
				GYear gyear = AccountTransferUtil.basicFactory.createGYear();
				XMLGregorianCalendar coverageYear = AccountTransferUtil.getSystemDateYear();
				coverageYear.setYear(2020);
				gyear.setValue(coverageYear);
				
				ApplicationExtensionType applicationExtensionType = AccountTransferUtil.insuranceApplicationObjFactory.createApplicationExtensionType();
				applicationExtensionType.setCoverageYear(gyear);
				request.getInsuranceApplication().setApplicationExtension(applicationExtensionType);	
			}
		}
		
		
		List<String> errorList = aTRequestValidator.validate(request, inputXml);
		if(GhixPlatformConstants.ALLOW_MASKED_IDENTIFICATION_ID && errorList != null) {
			Iterator<String> cursor = errorList.iterator();
			while(cursor.hasNext()) {
				String str = cursor.next();
				if(str.indexOf("nc:IdentificationID".intern()) != -1){
					cursor.remove();
				}
			}
		}
		if (errorList != null && errorList.size() > 0) {
			accountTransferResponsePayloadType = formFailureResponse(errorList,EligibilityConstants.SCHEMATRON_VALIDATION_ERROR_CODE); 
		} else {
			String validateAtElements = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_VALIDATE_AT_ELEMENTS);
			if("N".equalsIgnoreCase(validateAtElements)) {
				accountTransferResponsePayloadType = formSuccessResponse(errorList, null);
			}else {
				try {
					errorList = accountTransferStrategy.validate(request);
				} catch (Exception e) {
					LOGGER.error(UNKNOWN_EXCEPTION + ExceptionUtils.getFullStackTrace(e));
					errorList = new ArrayList<String>();
					errorList.add(UNKNOWN_EXCEPTION);
					exceptionUtil.persistGiMonitorId(request, e);
				}
	
				final String WARNING_CONST = "Warning";
				List<String> warningList = errorList.stream().filter(str -> str.startsWith(WARNING_CONST)).collect(Collectors.toList());
				errorList.removeIf(str -> str.startsWith(WARNING_CONST));
	
				if (!CollectionUtils.isEmpty(errorList)) {
					accountTransferResponsePayloadType = formFailureResponse(errorList,EligibilityConstants.BUSINESS_VALIDATION_ERROR_CODE); 
				}
				else {
	
					String warningMessage = null;
	
					if (!CollectionUtils.isEmpty(warningList)) {
						warningMessage = warningList.get(0);
					}
					accountTransferResponsePayloadType = formSuccessResponse(errorList, warningMessage);
				}
			}
		}

		result.setAccountTransferResponsePayloadType(accountTransferResponsePayloadType);

		return result;
	}

	private AccountTransferResponsePayloadType formSuccessResponse(List<String> errorList,String warningError) {
		if(StringUtils.isNotBlank(warningError)){
			return formResponse(EligibilityConstants.SUCCESS_WITH_WARNING_CODE, errorList,warningError);	// SUCCESS WITH WARNING
		}
		else{
			return formResponse(EligibilityConstants.HS000000, errorList,warningError);	// SUCCESS	
		}
		
	}

	/*
	 * Added a responseCode for returning different codes for schema and business validations
	 */
	@Override
	public AccountTransferResponsePayloadType formFailureResponse(List<String> errorList, String responseCode) {
		LOGGER.info(ERRORS_FOUND_IN_THE_ACCOUNT_TRANSFER_REQUEST_PAYLOAD_TYPE_OBJECT + errorList.size());
		if (LOGGER.isDebugEnabled()){
			for (String string : errorList) {
				LOGGER.info(string);
			}
		}
		return formResponse(responseCode, errorList,null);	// FAILURE
		//return formResponse(EligibilityConstants.HE001111, errorList,null);	// FAILURE
	}

	private AccountTransferResponsePayloadType formResponse(String responseValue, List<String> errorList, String warningError) {

		AccountTransferResponsePayloadType response = new AccountTransferResponsePayloadType();
		response.setAtVersionText(AT_VERSION);

		ResponseMetadataType value = new ResponseMetadataType();
		TextType tt = new TextType();
		tt.setValue(responseValue);
		value.setResponseCode(tt);
		TextType responseDescription = new TextType();

		if (StringUtils.isNotBlank(warningError)) {
			responseDescription.setValue(SUCCESS+" "+warningError);
		}
		else {
			responseDescription.setValue(SUCCESS);
		}
		
		if (errorList != null && errorList.size() > 0) {
			TextType responseErrorDescrip = new TextType();
			responseErrorDescrip.setValue(ONE_OR_MORE_RULES_FAILED_VALIDATION);
			value.setResponseDescriptionText(responseErrorDescrip);


			List<TextType> tdsResponseDescriptionTextList = new ArrayList<>();

			for (String error : errorList) {
				TextType errorDescrip = new TextType();
				errorDescrip.setValue(error);
				tdsResponseDescriptionTextList.add(errorDescrip);
			}

			value.getTDSResponseDescriptionText().addAll(tdsResponseDescriptionTextList);

			responseDescription.setValue(FAILURE);
		}

		value.setResponseDescriptionText(responseDescription);
		response.setResponseMetadata(value);
		return response;
	}

	@Override
	public AccountTransferRequestPayloadType prepareForReTrigger(Integer giwsPayloadId) {

		try {
			GIWSPayload giwsPayload = giwsPayloadService.findById(giwsPayloadId);

			if (giwsPayload == null || !"ERP-TRANSFERACCOUNT".equals(giwsPayload.getEndpointFunction())){
				throw new GIRuntimeException("Supplied giwsPayloadId doesnot exist for endpoint function ERP-TRANSFERACCOUNT - " + giwsPayloadId );
			}
			AccountTransferRequestPayloadType accountTransferRequestPayloadType = null;
			
			if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
			{
				String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
				accountTransferRequestPayloadType = ReferralUtil.unmarshal(requestBody);
			}
			else
			{
				accountTransferRequestPayloadType = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
			}

			return accountTransferRequestPayloadType;

		} catch (Exception e) {
			throw new GIRuntimeException("Error re-triggering account transfer for giwsPayloadId - " +
												giwsPayloadId + " , reason - " + ExceptionUtils.getFullStackTrace(e));
		}

	}

	@Override
	@Async
	public void process(AccountTransferRequestPayloadType request, Integer giwsPayloadId) {
		AtAdditionalParamsDTO atAdditionalParams = new AtAdditionalParamsDTO();
		atAdditionalParams.setGiWsPayloadId(giwsPayloadId);
		accountTransferStrategy.process(request,atAdditionalParams);
	}

	@Override
	@Async
	public void process(AccountTransferRequestPayloadType request, AccountTransferResponseDTO result) {
		final String linkATUsingAppId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_LINK_AT_USING_APPLICATION_ID);
		extractHouseholdCaseId(request,result);
		
		Integer giwsPayloadId = result.getGiWsPayloadId();
		TimeshiftContext.getCurrent().setUserName(result.getUser());
		TimeshiftContext.getCurrent().setTimeOffset(result.getTSoffset());
		AccountTransferResponsePayloadType response = result.getAccountTransferResponsePayloadType();
		String responseCode = response.getResponseMetadata().getResponseCode().getValue();
		/*if("TRUE".equalsIgnoreCase(linkATUsingAppId)){
			this.updateOutboundAtApplicant(request, giwsPayloadId);
		}*/
		
		if (StringUtils.equalsIgnoreCase(responseCode, EligibilityConstants.HS000000) || StringUtils.equalsIgnoreCase(responseCode, EligibilityConstants.SUCCESS_WITH_WARNING_CODE)){
			AtAdditionalParamsDTO atAdditionalParams = new AtAdditionalParamsDTO();
			atAdditionalParams.setGiWsPayloadId(giwsPayloadId);
			atAdditionalParams.setAtSpanInfoId(result.getAtSpanInfoId());
			accountTransferStrategy.process(request, atAdditionalParams);
		}
		TimeshiftContext.clearCurrent();
	}
	
	private String extractHouseholdCaseId(AccountTransferRequestPayloadType accountTransferRequest, AccountTransferResponseDTO result)
	{
		final String useExternalId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_HOUSEHOLD_CASE_ID_ENABLE);
		final String linkATUsingAppId =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_LINK_AT_USING_APPLICATION_ID);
		
		String hhCaseId = null;
		if((null != useExternalId && "TRUE".equalsIgnoreCase(useExternalId))){
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = accountTransferRequest.getInsuranceApplication().getApplicationIdentification();
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if( null != linkATUsingAppId && "TRUE".equalsIgnoreCase(linkATUsingAppId) && (tIdentificationType.getIdentificationCategoryText() == null) ){
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
				if(ReferralConstants.HOUSE_HOLD_CASE_ID.equalsIgnoreCase( tIdentificationType.getIdentificationCategoryText( ).getValue())) {
					hhCaseId = tIdentificationType.getIdentificationID().getValue();
					break;
				}
			}
			
			if(hhCaseId != null && result.getGiwsPayload() != null){
				result.getGiwsPayload().setHouseHoldCaseId(hhCaseId);
			}
		}
		return hhCaseId;
	}
	

	@Override
	public void logProcessingTime(long giwsPayloadId,long time) {
		SsapIntegrationLog sil = integrationLogService.findByGiWsPayloadId(giwsPayloadId);
		if(sil !=null) {
			sil.setResponseTimeMillis(time);
			integrationLogService.save(sil);
		}
	}

	@Override
	public boolean createSpan(AccountTransferRequestPayloadType request,AccountTransferResponseDTO result){
		
		boolean isProcessingSpanAllowed = true;
		String externalApplicantIdOfPrimaryContact = null;
		int currentEligibilitySpan=0;
		int totalEligibilitySpan = 0;
		
		
			// Find Primary Contact's external member id
			PersonType primaryContact = (PersonType) request.getInsuranceApplication().getSSFPrimaryContact().getRoleOfPersonReference().getRef();
			List<PersonType> personList = request.getPerson();
			
			for (PersonType personType : personList) {
				if(personType.getId().equalsIgnoreCase(primaryContact.getId())){
					if (personType.getPersonAugmentation() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification() != null && personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID() != null) {
						externalApplicantIdOfPrimaryContact = personType.getPersonAugmentation().getPersonMedicaidIdentification().getIdentificationID().getValue();
						break;
					}
				}
			}
			
			// Fetch Spans
			EligibilitySpansType eligibilitySpansType = request.getInsuranceApplication().getApplicationExtension().getEligibilitySpans();
			if(eligibilitySpansType != null && eligibilitySpansType.getCurrentEligibilitySpan() != null && eligibilitySpansType.getTotalEligibilitySpan() != null){
				totalEligibilitySpan = ReferralUtil.convertToInt(eligibilitySpansType.getTotalEligibilitySpan().getValue());
				currentEligibilitySpan = ReferralUtil.convertToInt(eligibilitySpansType.getCurrentEligibilitySpan().getValue());
			}
			
			if(externalApplicantIdOfPrimaryContact != null && totalEligibilitySpan>0 && currentEligibilitySpan>0){
				ATSpanInfoRequestDTO atSpanInfoRequestDTO = new ATSpanInfoRequestDTO();
				atSpanInfoRequestDTO.setCurrentSpan(currentEligibilitySpan);
				atSpanInfoRequestDTO.setTotalSpans(totalEligibilitySpan);
				atSpanInfoRequestDTO.setGiWsPayloadId(result.getGiWsPayloadId());
				atSpanInfoRequestDTO.setExternalApplicantId(externalApplicantIdOfPrimaryContact);
				atSpanInfoRequestDTO.setProcessStatus(AccountTransferProcessStatus.ATRECEIVED);
				atSpanInfoRequestDTO.setCoverageYear(request.getInsuranceApplication().getApplicationExtension().getCoverageYear().getValue().getYear());
				long atSpanInfoId = atSpanProcessingServiceImpl.createAtSpanInfo(atSpanInfoRequestDTO);
				result.setAtSpanInfoId(atSpanInfoId);
				
				DateTime toDate = TSDateTime.getInstance();
				DateTime fromDate = toDate.minusMinutes(IN_PROGRESS_WAIT_INTERVAL);
				
				List<AccountTransferSpanInfo> atSpanInfoList = atSpanProcessingRepository.findPastSpansByStatusAndInterval(externalApplicantIdOfPrimaryContact, atSpanInfoRequestDTO.getCoverageYear(), AccountTransferProcessStatus.INPROGRESS,fromDate.toDate(),toDate.toDate());
				if(atSpanInfoList != null && atSpanInfoList.size()>0){
					isProcessingSpanAllowed = false;
				}
			}
			
			return isProcessingSpanAllowed;
	}

	/**
	 * This method is used to process the response AT and save the data as per accepted or rejected members
	 * The maps has keys as person id i.e external_applicant_id in ssap_applicant table
	 * and the values we need to update and save in the database
	 */
	@Override
	public void processResponseAT(String clonedAppId, Map<String, Boolean> seekingCoverageAndQHPMap,
			Map<String, String> applicantEventsMap) {
		// 1. set the seeking coverage indicator for ssap applicant 
		// 2. set seek_qhp 
		// 3. Add event 
		LOGGER.info("clonedAppId: "+clonedAppId+" ,seekingCoverageAndQHPMap: "+seekingCoverageAndQHPMap+ " ,applicantEventsMap: "+applicantEventsMap);
		try {			
			// get the application cloned from the case number passed in the AT response 
			List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(Long.valueOf(clonedAppId));
			if (ReferralUtil.listSize(ssapApplications) == 0) {
				throw new GIRuntimeException("Ssap Application not found for that id "+clonedAppId);
			}
			
			SsapApplication ssapApplication = ssapApplications.get(0);
			boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;
			List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
			// update the seeking coverage
			for(SsapApplicant applicant : ssapApplicants) {
				boolean seekingCoverageValue = seekingCoverageAndQHPMap.get(applicant.getApplicantGuid()+"P"+applicant.getPersonId());
				// add the updated applicant
				//modifyCurrentApplicants(ssapApplication.getSsapApplicants(), applicant.getExternalApplicantId(), seekingCoverageValue);
				applicant.setApplyingForCoverage(ReferralUtil.converToYN(seekingCoverageValue));
			}
			
			// update seek qhp in the application data
			final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
			// set the edit application flag
			ssappln.setIsEditApplication(true);
			if(null != ssappln.getTaxHousehold() && !ssappln.getTaxHousehold().isEmpty()) {
				for (HouseholdMember houseHoldMember : ssappln.getTaxHousehold().get(0).getHouseholdMember()) {
					if (seekingCoverageAndQHPMap.containsKey(String.valueOf(houseHoldMember.getApplicantGuid()+"P"+houseHoldMember.getPersonId()))) {
						houseHoldMember.setSeeksQhp((boolean) seekingCoverageAndQHPMap.get(String.valueOf(houseHoldMember.getApplicantGuid()+"P"+houseHoldMember.getPersonId())));
						// applying for coverage
						houseHoldMember.setApplyingForCoverageIndicator((boolean) seekingCoverageAndQHPMap.get(String.valueOf(houseHoldMember.getApplicantGuid()+"P"+houseHoldMember.getPersonId())));
						// seeking coverage / qhp is true for rejected / denied member so if true set the medicaidDeniedIndicator as true in json
						if((boolean) seekingCoverageAndQHPMap.get(String.valueOf(houseHoldMember.getApplicantGuid()+"P"+houseHoldMember.getPersonId()))) {
							houseHoldMember.setMedicaidChipDenial(true);
						}						
					}
				}
			}
			
			final String ssapJson = ssapJsonBuilder.transformToJson(ssappln);
			updateSSAPJSON(ssapApplication, ssapJson);
			updateSsapApplicants(ssapApplication.getSsapApplicants());
			
			// add applicant and application events as per the accepted and rejected status
			// this map will contain applicant guid and the event which needs to be inserted for that applicant
			Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();
			Map<String, SepEvents> sepEventsRemove = fetchRemoveEvents();
			Map<String, SepEvents> sepEventsOther = fetchOtherEvents();
			SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();

			for(SsapApplicant applicant : ssapApplicants) {
				if(applicantEventsMap.containsKey(applicant.getApplicantGuid()+"P"+applicant.getPersonId())) {
					// get the event other
					SepEvents sepEvent = sepEventsOther.get(applicantEventsMap.get(applicant.getApplicantGuid()+"P"+applicant.getPersonId()));
					if(null == sepEvent) {
						// it could be gainofMedicaid
						sepEvent = sepEventsRemove.get(applicantEventsMap.get(applicant.getApplicantGuid()+"P"+applicant.getPersonId()));
					}
					if(sepEvent != null) {
						LOGGER.info("sepEvent: "+sepEvent.getName()+" sep event id: "+sepEvent.getId());	
					}					
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSepEvents(sepEvent);
					ssapApplicantEvent.setSsapApplicant(applicant);

					ssapApplicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
					ssapApplicantEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(new Timestamp(new TSDate().getTime()),isNextYearsOE));
					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());
					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}
			
			Integer userId = null;
			List<Integer> userIds = userRepository.userIdsFromEmail(ReferralConstants.EXADMIN_USERNAME);
			if (ReferralUtil.listSize(userIds) > 0) {
				userId = userIds.get(0);
			}
			boolean keepOnly = true;
			ssapApplicationEventService.createApplication_Applicant_SepEvents
					(Long.valueOf(clonedAppId), finalApplicantEvents, applicationSepEndDates.last(), userId, keepOnly);
			
			// RUN_VERIFICATION_ELIGIBILITY using the API eligibilityEngineIntegrationService.invokeEligibilityEngine
			// batch code referred to call RedetermineEligibilityServiceImpl invokeEligibilityEngine
			invokeEligibilityEngine(Long.valueOf(clonedAppId));
			
			//RUN_AT_AUTOMATION using ReferralSEPController.editAndProcessApplication
			//lceEditApplicationService.editApplicationProcess(Long.valueOf(clonedAppId));
			
			// trigger notices for Response AT
			triggerResponseATChangeEmail(ssapApplication);
			
		} catch (Exception e) {
			LOGGER.error("Error while processResponseAT and account Transfer Service: ", e);
		}		
	}
	
	/**
	 * Trigger response AT email 
	 * @param currentApplication
	 */
	private void triggerResponseATChangeEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger Response AT Email Notice");
		try {
			referralLceNotificationService.generateResponseATChangeNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Response AT Notice ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
	
	/**
	 * invoke the eligibility engine API
	 * @param ssapApplicationId
	 * @return
	 */
	public EligibilityResponse invokeEligibilityEngine(long ssapApplicationId) {
		String endpointUrl = GhixEndPoints.GHIXHIX_SERVICE_URL + "api/newssap/eligibility/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(Long.toString(ssapApplicationId));
		String response = null;
		String status = RenewalConstants.FAILURE;
		EligibilityResponse eligibilityResponse = null;
		try {
			ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(endpointUrl,
					"exadmin@ghix.com", HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);
			if (responseEntity != null) {
				response = responseEntity.getBody();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EligibilityResponse.class);
				eligibilityResponse = reader.readValue(response);
				if("200".equalsIgnoreCase(eligibilityResponse.getStatus())){
					status = RenewalConstants.SUCCESS;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while invoking eligibility engine for application - " + ssapApplicationId, ex);
			throw new GIRuntimeException("Exception occured while invoking eligibility engine for application - " + ssapApplicationId, ex);
		} finally {
			logServicePayload(ssapApplicationId, String.valueOf(ssapApplicationId), endpointUrl,RenewalConstants.REDETERMINE_ELIGIBILITY_BATCH, response, status, PROCESS_ELIGIBILITY);
		}
		return eligibilityResponse;
	}
	
	@Override
	public GIWSPayload logServicePayload(Long ssapApplicationId, String request, String endpointUrl,String endpointFunction, String response, String status, String operationName) {
		try {
			GIWSPayload giwsPayload = new GIWSPayload();
	        giwsPayload.setSsapApplicationId(ssapApplicationId);
	        giwsPayload.setCreatedTimestamp(new TSDate());
	        giwsPayload.setEndpointFunction(endpointFunction);
	        giwsPayload.setEndpointOperationName(operationName);
	        giwsPayload.setEndpointUrl(endpointUrl);
	        giwsPayload.setRequestPayload(request);
	        giwsPayload.setResponsePayload(response);
	        giwsPayload.setStatus(status);
	        giwsPayloadService.save(giwsPayload);
	        return giwsPayload;
		} catch(Exception e){
			LOGGER.error("Exception occurred while inserting records in payload table", e);
			throw new GIRuntimeException(e);
		} 
	}
	
	private void updateSSAPJSON(SsapApplication currentApplication, String ssapJson) {
		currentApplication.setApplicationData(ssapJson);
		ssapApplicationRepository.save(currentApplication);
    }
	
	private void updateSsapApplicants(List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			persistSeekingCoverage(ssapApplicant);
		}
	}
	
	private void persistSeekingCoverage(SsapApplicant applicant) {
		SsapApplicant ssapapplicant = ssapApplicantRepository.findSsapApplicantById(applicant.getId());
		ssapapplicant.setApplyingForCoverage(applicant.getApplyingForCoverage());
		ssapApplicantRepository.save(ssapapplicant);
	}
	
	private Map<String, SepEvents> fetchOtherEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
	}

	private Map<String, SepEvents> fetchRemoveEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.REMOVE.name());
	}
	
	private int calculateEnrollmentGracePeriod() {
		int enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
		return enrollmentGracePeriod;
	}
	
	private Timestamp calculateEndDate(Date currDate, boolean isNextYearsOE) {
		if (isNextYearsOE)
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				LOGGER.error("Error while calculating the SsapApplicantEvent Enrollment End Date",e);
				return null;
			}
		}
		else {
			DateTime endDate = new DateTime(currDate.getTime());
			endDate = endDate.plusDays(calculateEnrollmentGracePeriod() + numberOfenrollmentDays);
			return new Timestamp(endDate.getMillis());
		}
	}	
}
