package com.getinsured.hix.account.util;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.util.GhixPlatformConstants;

@Component
public class UserAccountAuditLog {

	@GiAudit(transactionName = "Account has been locked due to several unsuccessful login attempts", eventType = EventTypeEnum.AUTHENTICATION, eventName = EventNameEnum.DEACTIVATE_ACCOUNTS)
	public void accntLockWrongPswrd(HttpServletRequest request, boolean isAccLocked, boolean isAccInactiveDormant,	boolean isAccInactive) {

		GiAuditParameterUtil.add(new GiAuditParameter("isAccLocked", isAccLocked), new GiAuditParameter("isAccInactiveDormant", isAccInactiveDormant),
				new GiAuditParameter("isAccInactive", isAccInactive), new GiAuditParameter("authfailed", "true"),
				new GiAuditParameter("AUTH_FAILED_PASSWORD_MAX_AGE", request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE")),
				new GiAuditParameter("AUTH_FAILED_PASSWORD_USERNAME", request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) != null?
							request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) : "" + ""));

	}

	@GiAudit(transactionName = "Account has been locked due to several unsuccessful attempts to change password", eventType = EventTypeEnum.PASSWORD_CHANGE, eventName = EventNameEnum.DEACTIVATE_ACCOUNTS)
	public void accntLockOnChangePswrd(HttpServletRequest request) {
		
		GiAuditParameterUtil.add(new GiAuditParameter("authfailed", "true"),
				new GiAuditParameter("AUTH_FAILED_PASSWORD_MAX_AGE", request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE")),
				new GiAuditParameter("AUTH_FAILED_PASSWORD_USERNAME", request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) != null? 
								request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) : "" + ""));
	
	}

}
