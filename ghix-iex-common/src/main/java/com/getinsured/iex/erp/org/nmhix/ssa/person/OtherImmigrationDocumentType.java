package com.getinsured.iex.erp.org.nmhix.ssa.person;

/**
 * @author chopra_s
 * 
 */
public class OtherImmigrationDocumentType {
	private boolean WithholdingOfRemovalIndicator;
	private boolean StayOfRemovalIndicator;
	private boolean ORREligibilityLetterIndicator;
	private boolean ORRCertificationIndicator;
	private boolean CubanHaitianEntrantIndicator;
	private boolean AmericanSamoanIndicator;
	
	public boolean getWithholdingOfRemovalIndicator() {
		return WithholdingOfRemovalIndicator;
	}
	public void setWithholdingOfRemovalIndicator(
			boolean withholdingOfRemovalIndicator) {
		WithholdingOfRemovalIndicator = withholdingOfRemovalIndicator;
	}
	public boolean getStayOfRemovalIndicator() {
		return StayOfRemovalIndicator;
	}
	public void setStayOfRemovalIndicator(boolean stayOfRemovalIndicator) {
		StayOfRemovalIndicator = stayOfRemovalIndicator;
	}
	public boolean getORREligibilityLetterIndicator() {
		return ORREligibilityLetterIndicator;
	}
	public void setORREligibilityLetterIndicator(
			boolean oRREligibilityLetterIndicator) {
		ORREligibilityLetterIndicator = oRREligibilityLetterIndicator;
	}
	public boolean getORRCertificationIndicator() {
		return ORRCertificationIndicator;
	}
	public void setORRCertificationIndicator(boolean oRRCertificationIndicator) {
		ORRCertificationIndicator = oRRCertificationIndicator;
	}
	public boolean getCubanHaitianEntrantIndicator() {
		return CubanHaitianEntrantIndicator;
	}
	public void setCubanHaitianEntrantIndicator(boolean cubanHaitianEntrantIndicator) {
		CubanHaitianEntrantIndicator = cubanHaitianEntrantIndicator;
	}
	public boolean getAmericanSamoanIndicator() {
		return AmericanSamoanIndicator;
	}
	public void setAmericanSamoanIndicator(boolean americanSamoanIndicator) {
		AmericanSamoanIndicator = americanSamoanIndicator;
	}
	
	
}
