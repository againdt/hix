import { ActionTypes as types } from '../constants/constants';

function dashboard(state=[], action) {
    switch (action.type) {
        case (types.RECEIVED_DASHBOARD_INFO_SUCCESS):
            console.log("healthPlans=" + JSON.stringify(action.data));
            return action.data;

        default:
            return state;
    }
}

export default dashboard;