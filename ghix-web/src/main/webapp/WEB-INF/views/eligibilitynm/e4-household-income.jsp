
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>Build your household</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">

<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">

<div id="topnav"> </div>
<form class="form-horizontal">
<div class="row-fluid" >
    <div class="row-fluid" id="titlebar">
      <div class="span3">
         <a class="btn btn-link" href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Your Household Income</h1>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid" id="main">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li class="active"><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li><a href="e5-additional-questions">  Additional Questions </a></li>
          <li><a href="e6-review-declare-file"> Review &amp; Sign </a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      <img src="../resources/eligibilitynm/img/build.png" alt="header image" class="img-polaroid">
        <div class="margin-right"> <!-- gutter10  -->
           <div class="span8">
            <h4>Coming up in this section</h4>
            <p>To accurately determine the amount of financial assistance that your household will receive, the Exchange will need you to provide some specific 
            information about your household income.<!--  We will walk you through four sections: Job Income, Self Employment Income, 
            Other Income and Adjustments.  After you provide this income information for each member of your household, the Exchange will verify your household 
            income against trusted data sources. You will then be ready to start comparing your health insurance options! --></p>
<!--             <p>If you are  Native American, please do not include judgment funds, tribal trust income, trust allotment, or certain fishing income as part of your income.  In addition, please do not include health care benefits paid by the Indian Health Service or your tribe as part of income.</p>
 -->          </div>
           <div class="span4">
            <h5>You may need</h5>
            <hr>
            <p> <i class="icon-ok"></i> Most recent tax filing</p>
            <p> <i class="icon-ok"></i> Pay stubs</p>
            <p> <i class="icon-time"></i> Estimated time needed to complete this section: 10 minutes.</p>
          </div>
           
          <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#income1">Next</a> 
	                  <a class="btn edit secondary">Next</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
           

            <div id="income1" class="section">
            <div class="opacity">
            	<div class="header">
              <h4>Your Household Income</h4>
              </div>
                <h5 class="clear">Review our records of Stan Jacobs and edit if necessary:</h5>
                  <div class="control-group">
                    <label class=" control-label">Income Type</label>
                    <div class="controls">
                        <input type="text" class="input-medium" value="Job">
                      </div>
                  </div>
                  <div class="control-group">
                    <label class=" control-label">Employee Name</label>
                    <div class="controls">
                        <input type="text" class="input-medium" value="Walmart">
                      </div>
                  </div>
                  <div class="control-group">
                    <label class=" control-label">Street Address</label>
                    <div class="controls">
                        <input type="text" class="input-medium" value="8300 W. Overland Road">
                      </div>
                  </div>
                    <div class="control-group">
                     <label class=" control-label">City</label>
                       <div class="controls">
                          <input type="text" class="input-medium" value="Boise">
                        </div>
                    </div>
                  
                  <div class="control-group">
                    <label class=" control-label">State</label>
                  <div class="controls">
                    <select name="inputState" class="span4" id="inputState">
                      <option value="">Select a State</option>
                      <option value="AL">Alabama</option>
                      <option value="AK">Alaska</option>
                      <option value="AZ">Arizona</option>
                      <option value="AR">Arkansas</option>
                      <option value="CA">California</option>
                      <option value="CO">Colorado</option>
                      <option value="CT">Connecticut</option>
                      <option value="DE">Delaware</option>
                      <option value="DC">District Of Columbia</option>
                      <option value="FL">Florida</option>
                      <option value="GA">Georgia</option>
                      <option value="HI">Hawaii</option>
                      <option value="ID" selected>Idaho</option>
                      <option value="IL">Illinois</option>
                      <option value="IN">Indiana</option>
                      <option value="IA">Iowa</option>
                      <option value="KS">Kansas</option>
                      <option value="KY">Kentucky</option>
                      <option value="LA">Louisiana</option>
                      <option value="ME">Maine</option>
                      <option value="MD">Maryland</option>
                      <option value="MA">Massachusetts</option>
                      <option value="MI">Michigan</option>
                      <option value="MN">Minnesota</option>
                      <option value="MS">Mississippi</option>
                      <option value="MO">Missouri</option>
                      <option value="MT">Montana</option>
                      <option value="NE">Nebraska</option>
                      <option value="NV">Nevada</option>
                      <option value="NH">New Hampshire</option>
                      <option value="NJ">New Jersey</option>
                      <option value="NM">New Mexico</option>
                      <option value="NY">New York</option>
                      <option value="NC">North Carolina</option>
                      <option value="ND">North Dakota</option>
                      <option value="OH">Ohio</option>
                      <option value="OK">Oklahoma</option>
                      <option value="OR">Oregon</option>
                      <option value="PA">Pennsylvania</option>
                      <option value="RI">Rhode Island</option>
                      <option value="SC">South Carolina</option>
                      <option value="SD">South Dakota</option>
                      <option value="TN">Tennessee</option>
                      <option value="TX">Texas</option>
                      <option value="UT">Utah</option>
                      <option value="VT">Vermont</option>
                      <option value="VA">Virginia</option>
                      <option value="WA">Washington</option>
                      <option value="WV">West Virginia</option>
                      <option value="WI">Wisconsin</option>
                      <option value="WY">Wyoming</option>
                    </select>
                  </div>
            </div>
                  
               <div class="control-group">
                     <label class=" control-label">Phone Number</label>
                          <div class="controls">
                          <input type="text" name="phone1" id="phone1" value="208" maxlength="3" class="input-mini">
                          <input type="text" name="phone2" id="phone2" value="321" maxlength="3" class="input-mini">
                          <input type="text" name="phone3" id="phone3" value="9077" maxlength="4" class="input-mini">
                        </div>
                    </div>    
                      
                <div class="control-group">
                     <label class=" control-label">Employer ID Number</label>
                          <div class="controls">
                          <input type="text" value="51" maxlength="2" class="input-mini">
                          <input type="text" value="1784356" maxlength="7" class="input-mini">
                        </div>
                    </div>    
                            
                <div class="control-group">
                     <label class=" control-label">Income Amount</label>
                          <div class="controls">
                            $&nbsp;<input type="text" value="1,115"class="input-mini">&nbsp;
                             <select class="span4">
			                    <option>&nbsp;</option>
			                    <option>Hourly</option>
			                    <option>Daily</option>
			                    <option>Weekly</option>
			                    <option selected>Every 2 weeks</option>
			                    <option>Twice a month</option>
			                    <option>Monthly</option>
			                    <option>Yearly</option>
			                    <option>One time only</option>
			                  </select>
                        </div>
                    </div>         
                   <div class="control-group">
                     <label class=" control-label">Frequency</label>
                          <div class="controls">
			                  <select>
			                    <option>&nbsp;</option>
			                    <option>Hourly</option>
			                    <option>Daily</option>
			                    <option>Weekly</option>
			                    <option selected>Every 2 weeks</option>
			                    <option>Twice a month</option>
			                    <option>Monthly</option>
			                    <option>Yearly</option>
			                    <option>One time only</option>
			                  </select>
			                </div>
                    </div>      
                     <div class="controls">
                     <label class="checkbox">
                            <input type="checkbox" value="" class="input-mini">
                            The income information above is correct
                  </label>
                    </div> 
             
                  
              
              <h5 class="clear">Does Stan Jacobs have another income source?</h5>
                  <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox" value="Yes">
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" value="No" checked>
                      No </label>
                  </div>
              <h5 class="clear">Does Janet Jacobs have any of the following income?</h5>
                  <ul>
                  	<li>Job </li>
					<li>Self-employment </li>
					<li>Social Security Benefits </li>
					<li>Unemployment </li>
					<li>Retirement </li>
					<li>Pension </li>
					<li>Capital Gains </li>
					<li>Investment Income </li>
					<li>Rental or Royalty Income </li>
					<li>Farming or Fishing Income </li>
					<li>Alimony Received</li>
                  </ul>
                 <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox" value="Yes" checked>
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" value="No" >
                      No </label>
                  </div>
                      
               <!-- <label class="checkbox ">
                        <input type="checkbox" checked>
                      Job </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Self-employment </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Social Security Benefits </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Unemployment </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Retirement </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Pension </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Capital Gains </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Investment Income </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Rental or Royalty Income </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Farming or Fishing Income </label>
                      <label class="checkbox ">
                        <input type="checkbox">
                      Alimony Received</label> -->
			<div class="control-group">
				<label class="control-label">What type of income would you like to add?</label>
				<div class="controls">
					<select>
						<option selected>Job </option>
						<option>Self-employment </option>
						<option>Social Security Benefits </option>
						<option>Unemployment </option>
						<option>Retirement </option>
						<option>Pension </option>
						<option>Capital Gains </option>
						<option>Investment Income </option>
						<option>Rental or Royalty Income </option>
						<option>Farming or Fishing Income </option>
						<option>Alimony Received</option>
					</select>
				</div>
			</div>
			
			
            <div class="control-group">
              <label class="control-label">Name of Employer</label>
                <div class="controls">
                  <input type="text" class="input-medium" >
                </div>
            </div>
                  
            <h5>How much does Janet Jacobs get paid (before taxes are taken out)? Tell us about the regular pay from all jobs that Janet Jacobs gets as well as any one-time amounts this month, like a bonus or a severance payment.</h5>

            <div class="control-group">
              <label class="control-label">Amount</label>
                <div class="controls">
                  $&nbsp;<input type="text" class="input-medium" >
                </div>
            </div>
            <div class="control-group">
              <label class="control-label">How often does <strong>Janet Jacobs</strong> get paid this amount?</label>
                <div class="controls">
                  <select>
                    <option>&nbsp;</option>
                    <option>Hourly</option>
                    <option>Daily</option>
                    <option>Weekly</option>
                    <option>Every 2 weeks</option>
                    <option>Twice a month</option>
                    <option>Monthly</option>
                    <option>Yearly</option>
                    <option>One time only</option>
                  </select>
                </div>
            </div>
            <div class="control-group">
              <a class="clear btn span4" href="#">Add Another Job</a>
            </div>
          
                 <h5 class="clear">Does Joe Jacobs have any of the following income?</h5>
                  <ul>
                  	<li>Job </li>
					<li>Self-employment </li>
					<li>Social Security Benefits </li>
					<li>Unemployment </li>
					<li>Retirement </li>
					<li>Pension </li>
					<li>Capital Gains </li>
					<li>Investment Income </li>
					<li>Rental or Royalty Income </li>
					<li>Farming or Fishing Income </li>
					<li>Alimony Received</li>
                  </ul>
                 <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox" value="Yes">
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" value="No" checked="">
                      No </label>
                  </div>
                      
                <h5>Does Stan Jacobs pay alimony, student loan interest, or other deductions that get reported on the front page of a federal income tax return from1040? This could make the cost of coverage a little lower.</h5>
                <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox"> Yes
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox"checked> No
                    </label>
                </div>
                
                <h5>Does Janet Jacobs pay alimony, student loan interest, or other deductions that get reported on the front page of a federal income tax return from1040? This could make the cost of coverage a little lower.</h5>
                <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox"> Yes
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox"checked> No
                    </label>
                </div>
                
                
                <h5>Does Joe Jacobs pay alimony, student loan interest, or other deductions that get reported on the front page of a federal income tax return from1040? This could make the cost of coverage a little lower.</h5>
                <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox"> Yes
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox"checked> No
                    </label>
                </div>
                
        <h5>Based on what you told us, if Stan Jacobs' income is steady each month, then it's about $30,000 per year. Is this how much you think Stan Jacobs will get in 2014?</h5>
                <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox"checked> Yes
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox"> No
                    </label>
                </div>
                
                <h5>Based on what you told us, if Janet Jacobs' income is steady each month, then it's about $23,000 per year. Is this how much you think Janet Jacobs will get in 2014?</h5>
                <div class="control-group">
                    <label class="checkbox inline">
                      <input type="checkbox"checked> Yes
                    </label>
                    <label class="checkbox inline">
                      <input type="checkbox"> No
                    </label>
                </div>
           
            <div id="totalresults">
              <h4>Your Household Income Summary for 2014</h4>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>&nbsp;</th>
                    <th>This Month</th>
                    <th>This Year</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Job Income</td>
                    <td>+$4,416.00</td>
                    <td>$53,000.00</td>
                    <!--<td><button class="btn btn-small">Edit</button></td>-->
                    <td><p class="btn btn-small" style="width: 25px">Edit</p></td>
                  </tr>
                  <tr>
                    <td>Self-Employment Income</td>
                    <td>+$0.00</td>
                    <td>$0.00</td>
                    <!--<td><button class="btn btn-small">Edit</button></td>-->
                    <td><p class="btn btn-small" style="width: 25px">Edit</p></td>
                  </tr>

                  <tr>
                    <td>Other Income</td>
                    <td>+$0.00</td>
                    <td>$0.00</td>
                    <!--<td><button class="btn btn-small">Edit</button></td>-->
                    <td><p class="btn btn-small" style="width: 25px">Edit</p></td>
                <tr>
                <tr>
                    <td>Adjustments</td>
                    <td>-$0.00</td>
                    <td>-$0.00</td>
                    <!--<td><button class="btn btn-small">Edit</button></td>-->
                    <td><p class="btn btn-small" style="width: 25px">Edit</p></td>
                <tr>
                  <td class="txt-right">&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>
                <tr>
                  <td class="txt-right"><strong>Total  Household Income</strong></td>
                  <td><strong>$4,416.00</strong></td>
                  <td><strong>$53,000.00</strong></td>
                  <td>&nbsp;</td>
                </tr>
              </table>
            </div>
             </div><!-- opacity -->
	             <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn btn-primary" href="e5-additional-questions">Save &amp; Continue</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household2 .section -->   
                

    
     
             
           </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
    </div>
    <!--row-fluid-->
</div>
<!--main-->
</form>

<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script>
    $(document).ready(function () {
        $(".flip").click(function () {
            $(".mailingAddress").fadeToggle();
        });
        $('input, textarea').placeholder();
    });
    $(".next").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        var hash = $(this.hash);
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(hash).closest('.section').show();
        $(currentsection).next().addClass('activediv');
        $(hash).prev('div').removeClass('activediv');
        $('html,body').delay(300).animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 500)
        $(hash).delay(700).prev('div').find('.edit').show();
        $(hash).delay(700).prev('div').find('.next').hide();
    });
    $(".edit").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 1.0
        });
        $(currentsection).find('.cancel, .save').show();
        $('.activediv').animate({
            opacity: 0.45
        });
        $(currentsection).find('.edit').hide();
    });
    $(".save").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $(currentsection).nextAll('.activediv').offset().top - 100
        }, 500)
    });
    $(".cancel").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $('div').nextAll('.activediv').offset().top - 100
        }, 500)
    });
</script>
<script>
$(document).ready(function(){
$('.qualify, .income-result').hide();
$('#qualify-coverage').modal();
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('#dob').datepicker();

$('input, textarea').placeholder();

});
</script>

