package com.getinsured.hix.consumer.model;

/**
 * 
 * @author bhatt_s
 * Used as View Model for MyEligibility
 */
public class MemberView {
	private String name;
	private String program;
	private String eligibilityStatus;
	private String eligibilityPeriodStart;
	private String eligibilityPeriodEnd;
	
	public MemberView(){}
	
	public MemberView(String name, String program, String eligibilityStatus,
			String eligibilityPeriodStart, String eligibilityPeriodEnd) {
		super();
		this.name = name;
		this.program = program;
		this.eligibilityStatus = eligibilityStatus;
		this.eligibilityPeriodStart = eligibilityPeriodStart;
		this.eligibilityPeriodEnd = eligibilityPeriodEnd;
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProgram() {
		return program;
	}
	public void setProgram(String program) {
		this.program = program;
	}
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}
	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}
	public String getEligibilityPeriodStart() {
		return eligibilityPeriodStart;
	}
	public void setEligibilityPeriodStart(String eligibilityPeriodStart) {
		this.eligibilityPeriodStart = eligibilityPeriodStart;
	}
	public String getEligibilityPeriodEnd() {
		return eligibilityPeriodEnd;
	}
	public void setEligibilityPeriodEnd(String eligibilityPeriodEnd) {
		this.eligibilityPeriodEnd = eligibilityPeriodEnd;
	}
	
	
	
}
