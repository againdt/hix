<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<%@ page import="java.util.HashMap" %>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%
 String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>	

<title>Getinsured Health Exchange: Agent Status</title>

<script src="<gi:cdnurl value="/resources/js/vimo.assister.js" />" type="text/javascript"></script>
<script src="<gi:cdnurl value="/resources/js/bootstrap-typeahead.js" />" type="text/javascript"></script>

<link href="<gi:cdnurl value="/resources/css/assister.css" />" media="screen" rel="stylesheet" type="text/css" />
<link href="<gi:cdnurl value="/resources/css/accessibility.css" />" media="screen" rel="stylesheet" type="text/css" />

<link href="<gi:cdnurl value="/resources/css/broker.css" />" media="screen" rel="stylesheet" type="text/css" />


<script src="<gi:cdnurl value="/resources/js/jquery.autoSuggest.minified.js" />" type="text/javascript"></script>

<meta name="description" content="">
<meta name="author" content="">

<link href="<gi:cdnurl value="/resources/css/assister.css" />" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/phoneUtils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/utility/dateUtils.js" />"></script>

<script type="text/javascript">



	$(document).ready(function() {
		$("#brkCertInfo").removeClass("link");
		$("#brkStatus").addClass("active");

		var brkId = ${broker.id};
		if (brkId != 0) {
			$('#certificationInfo').attr('class', 'active');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};


	});


</script>

<div class="gutter10">

	<div class="row-fluid">
		 	<h1> <c:if test="${not empty broker.user.firstName}"> ${broker.user.firstName}&nbsp;${broker.user.lastName} </c:if>
		 		<c:if test="${  empty broker.user.firstName}"> ${broker.firstName}&nbsp;${broker.lastName} </c:if>
		 	</h1><!-- <spring:message  code="label.newAgentRegistration"/>  -->
	</div>
	<div class="row-fluid">
		<!-- #sidebar -->
	       <c:choose>
	       		<c:when test="${ (agencyCertificationStatus == 'PENDING' or agencyCertificationStatus == 'CERTIFIED' )  and isAgencyManager}">
	       			<jsp:include page="brokerLeftNavigationMenu.jsp" />
	       		</c:when>
				<c:when test="${isAgencyManager  && requestScope.newBrokerRegistration == null && requestScope.brokerId == null }">
					<jsp:include page="agencyLeftNavigationMenu.jsp" />
				</c:when>
				<c:when test="${ isAdminUser }">
						<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
						  <div id="sidebar" class="span3">
							<div class="header"></div>
							<ul class="nav nav-list">
				                 <li ><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				                 <li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				                 <li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				 				<c:if test="${CA_STATE_CODE}">
									<li class="active" id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
								</c:if>
				                 <li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>
				                 <c:if test="${CA_STATE_CODE}">
									<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
								</c:if>
				                 <c:if test="${!CA_STATE_CODE}">
									 <li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>
									 <%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
								 </c:if>

				               </ul>
				               <br>
				            <c:if test="${!CA_STATE_CODE}">
								<div class="header">
									<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
								</div>
								<ul class="nav nav-list">
								<c:if test="${checkDilog == null}">
									<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
								</c:if>
								<c:if test="${checkDilog != null}">
									<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
								</c:if>
					            </ul>
				            </c:if>
						</div><!-- end of span3 -->
				</c:when>
				<c:otherwise>
	       			<jsp:include page="brokerLeftNavigationMenu.jsp" />
				</c:otherwise>
			</c:choose>
	    <!-- #sidebar ENDS -->
		<!-- end of span3 -->

		<div class="span9" id="rightpanel">
				<div class="header">
					<h4 class="pull-left"><spring:message  code="label.brkActivityStatus"/></h4>
					<c:if test="${isAgencyManager and agencyCertificationStatus == 'CERTIFIED' and broker.certificationStatus == 'Certified'}">
						<jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
						<c:set target="${moduleParamsMap}" property="brokerId" value="${broker.id}" />

						<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
						<a id="cancelLink" title="<spring:message code='label.brkCancel'/>" class="btn btn-small pull-right hide"  type="button" onclick="viewActivate();"  href="#"><spring:message code="label.brkCancel"/></a>
						<a id="editLink" title="<spring:message code='label.brkEdit'/>" class="btn btn-small pull-right"  type="button" onclick="editActivate();"  href="#"><spring:message code="label.brkEdit"/></a>
					</c:if>
				</div>

				<div class="gutter10">
				<form  action="<c:url value="/admin/broker/activitystatus" />" id="frmbrokerActivityStatus"   name="frmbrokerActivityStatus" method="POST"    >
					<table class="table table-border-none table-condensed" role="presentation">
						<tbody>


							<tr id="viewStatusRow">
								<td class="txt-right" width="50%"><spring:message code="label.brkActivityStatus"/></td>
								<c:if test="${broker.status == null}">
									<td><strong><spring:message code="label.brk.active"/></strong></td>
								</c:if>
								<c:if test="${broker.status != null}">
									<td><strong>${broker.status}</strong></td>
								</c:if>
							</tr>

									<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
									 <input type="hidden" name="encBrokerId" value="${encBrokerId}" />
									 <df:csrfToken/>
									 <tr class="hide" id = "editStatusRow" >
										<td class="span4 txt-right"><spring:message code="label.brkActivityStatus"/>
											<img src="/hix/resources/img/requiredAsterisk.png?v=NO_VERSION" alt="Required!" />
										</td>
										<td>
											<select  id="activityStatus" name="activityStatus" class="input-large">
												<option value=""><spring:message  code="label.brkSelect"/></option>
												<option value="Active" <c:if test="${broker.status  == 'Active'}">selected </c:if> > <spring:message code="label.brkactive"/>  </option>
												<option value="InActive" <c:if test="${broker.status  == 'InActive'}">selected </c:if> >  <spring:message code="label.brk.in-Active"/> </option>
											</select>
											<div id="activityStatus_error"></div>
										</td>
									</tr>
									<tr class="hide" id="editCommentRow">
										<td class="span4 txt-right"><spring:message  code="label.brkComment"/></td>
										<td><textarea id="comment" name= "comment" class="input-xlarge" maxlength="4000" rows="3" ></textarea></td>
									</tr>
						</tbody>
					</table>
					</form>

					<div class="form-actions hide" id="submitStatusForm">
								<input type="submit" class="btn btn-primary pull-right "  id="submitBtn" value="<spring:message code="label.brkSubmit"/>"  />
					</div>

				</div>
				<!-- end of .gutter10 -->
				<div class="header" id="historyDiv">
					<h4 class="pull-left"><spring:message code="label.brk.status.history"/></h4>
				</div>
				<div class="gutter10-tb">
					<table class="table table-condensed table-border-none table-striped" id="brokerStatusHistory">
						<thead>
							<tr>
								<th> <spring:message code="label.brkdate"/>  </th>
								<th> <spring:message code="label.bkr.previousStatus"/>  </th>
								<th> <spring:message code="label.brkNewStatus"/>  </th>
								<th> <spring:message code="label.bkr.viewComment"/>  </th>

							</tr>
						</thead>
						<tbody>
							<c:forEach var="statusRecord" items="${brokerActivityStatusHistory}">
									<tr class="odd">
										<td> <fmt:formatDate pattern = "MMM dd, yyyy" value = "${statusRecord.updated}" /></p> </td>
										<td>${statusRecord.previousActivityStatus} </td>
										<td>${statusRecord.newActivityStatus} </td>
										<td>${statusRecord.activityComments} </td>
									</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>

		</div>

	</div>
	<div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.bkr.viewComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		 <label id="commentlbl" ></label>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.button.close"/></a>
	</div>
</div>

<script>
	$(document).ready(function(){


		var validator = $("#frmbrokerActivityStatus").validate({
			onkeyup: false,
			onclick: false,
			rules : {
					"activityStatus" : { activityStatusValidator  : true}
			},
			messages : {
				"activityStatus" : {activityStatusValidator : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em><spring:message code='label.bkr.validateSatus' javaScriptEscape='true'/></span>"}
			},
			errorClass: "error",
			errorPlacement: function(error, element) {
				var elementId = element.attr('id');
				error.appendTo( $("#" + elementId + "_error"));
				$("#" + elementId + "_error").attr('class','error help-inline');
			}
		});



		 jQuery.validator.addMethod("activityStatusValidator", function(value, element, param) {
			 try {
					var activityStatus = $("#activityStatus").val();

					if(null == activityStatus || "undefined" === activityStatus || "Select".match(activityStatus)) {
						return false;
					}
					else {
						return true;
					}
				}catch(err) {
					return false;
				}
		});


		 <c:if test="${isAgencyManager and agencyCertificationStatus == 'CERTIFIED'}">

			 $("#submitBtn").click(function(){
				if( $("#frmbrokerActivityStatus").validate().form()){
					$("#frmbrokerActivityStatus").submit();
				}

		        return false;
		      });

		 	window.editActivate = function(){
		 		$('#historyDiv').hide();
			  $('#editLink').hide();

			  $('#cancelLink').show();
			  $('#editStatusRow').show();
			  $('#editCommentRow').show();
			  $('#submitStatusForm').show();

			};
			window.viewActivate = function (){

			 $('#editStatusRow').hide();
			 $('#editCommentRow').hide();
			 $('#submitStatusForm').hide();
			 $('#cancelLink').hide();

			 $('#viewStatusRow').show();
			 $('#editLink').show();
			 $('#historyDiv').show(); 
			};
		</c:if>

	});


	function getComment(comments)
	{
		$('#commentlbl').html("<p> Loading Comment...</p>");
		comments =comments.replace(/#newLine#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
		comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
		$('#commentlbl').html("<p> "+ comments + "</p>");

	}


</script>
