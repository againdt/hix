package com.getinsured.hix.entity.admin.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EntityDocuments;

/**
 * Encapsulates service layer method calls for Entity Admin for the Assisters
 */
public interface AssisterAdminService {

	/**
	 * Finds the Status History for the given Assister
	 * 
	 * @param userId
	 *            the given Assister User id
	 * @return list of Assister's status history
	 */
	List<Map<String, Object>> loadAssisterStatusHistoryForAdmin(Integer userId);

	/**
	 * Saves the given Assister Document
	 * 
	 * @param brokerDocuments
	 *            object to save
	 * @return the saved Assister Document
	 */
	EntityDocuments saveAssisterDocuments(EntityDocuments assisterDocuments);

	/**
	 * Finds the Assister Document
	 * 
	 * @param id
	 *            Assister Id
	 * @return the Assister Document
	 */
	EntityDocuments getAssisterDocumentsByRecordID(Integer id);

	/**
	 * Finds the list of Assisters
	 * 
	 * @param assister
	 * 
	 * @return list of Assisters
	 */
	List<Assister> findAssister(Assister assister);

	/**
	 * Finds the list of Assisters
	 * 
	 * @param assisterRequestDTO
	 *            contains the Assister search criteria parameters
	 * @return list of Assisters
	 */
	List<Assister> searchAssister(AssisterRequestDTO assisterRequestDTO);

	/**
	 * Finds the list of Assisters
	 * 
	 * @param assisterRequestDTO
	 *            contains the Assister search criteria parameters
	 * @return list of Assisters
	 */
	List<Assister> searchAssisterForEntity(AssisterRequestDTO assisterRequestDTO);
	
	/**
	 * Finds the Assister by Record Id
	 * 
	 * @param recordId
	 *            record id of the Assister
	 * @return the matching Assister
	 */
	Assister getAssisterByRecordID(int recordId);
	
	/**
	 * Get total of {@link Assister}
	 * 
	 * @param assisterRequestDTO
	 *            {@link AssisterRequestDTO}
	 * @return count of records
	 */
	int totalNoOfAssistersBySearch(AssisterRequestDTO assisterRequestDTO);

	/**
	 * Finds total of {@link Assister}
	 * 
	 * @return count of all records
	 */
	int findTotalNoOfAssisters();

	/**
	 * Finds total of {@link Assister}
	 * 
	 * @return count of all records
	 */
	int findTotalNoOfAssistersForEntity(int enrollmentEntityId);
	/**
	 * Finds {@link Assister} by activity status
	 * 
	 * @param activityStatus
	 *            Assister activity status
	 * @return count of records
	 */
	int findAssistersByStatus(String activityStatus);
}
