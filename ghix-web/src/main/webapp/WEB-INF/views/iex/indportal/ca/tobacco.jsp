<script type="text/ng-template" id="additionalinfo">

<div class="header row-fluid ng-scope">
  <h4><spring:message code="indportal.tobacco.additionalInfoNeeded"/></h4>
</div>

<div class="well ng-scope" ng-init="showApplicantDetails()">
  <div class="well ng-scope">
		<div class="alert alert-error" ng-if="coverageYearOver">
			We received an error while attempting to process your changes. In order to accurately update your account, please call Your Health Idaho at 1-855-YH-IDAHO (1-855-944-3246) at your earliest convenience. 
		</div>
		
		<div class="well-step">
	    <div class="margin20-lr">
			<spring:message code="indportal.tobacco.needDetails"/>
	      <p class="alert alert-info"><strong><spring:message code="indportal.tobacco.tobaccoUse"/>:</strong> <spring:message code="indportal.tobacco.tobaccoUseContent"/></p>
	      <p class="alert alert-info"><strong><spring:message code="indportal.tobacco.hardshipExempt"/>:</strong> <spring:message code="indportal.tobacco.hardshipExemptContent"/></p>
			   
          <div><input type="checkbox" name="sc3" ng-model="hardshipExemption" class="ng-pristine ng-valid" id="exemptionCheckbox">
					<label for="exemptionCheckbox"><strong><spring:message code="indportal.tobacco.qualifyHardshipQ"/></label>
        </div>
      </div>
	
			<div class="row-fluid">
			  <div id="submitTobacco">
			    <form name="exemptForm" class="form-horizontal ng-pristine ng-valid">
			
			      <div class="gutter20">
					<div class="header row-fluid ng-scope" ng-if="additionalDetails.length>0">
 					 <h4><strong><spring:message code="indportal.tobacco.eligibleMembers"/></h4>
					</div>
			        <table class="table head" ng-if="additionalDetails.length>0">
			          <tbody>
            			<tr>
			              <th class="exemptCell" scope="col"><a href="javascript:void(0)" data-toggle="tooltip" rel="tooltip" data-placement="bottom" data-original-title="<strong>Household Member(s)</strong><br> The people that you put on your tax form: you, your spouse, any children or relatives you financially support." data-html="true" tabindex="-1">Household</a> Member(s)</th>
			              <th class="exemptCell txt-center" scope="col" ><a href="javascript:void(0)" data-toggle="tooltip" rel="tooltip" data-placement="bottom" data-original-title="<strong><spring:message code="indportal.tobacco.seekCoverage"/></strong><br>These are the family members who are seeking health insurance." data-html="true" tabindex="-1"><spring:message code="indportal.tobacco.seekCoverage"/>?</a></th>
			              <th class="exemptCell txt-center" scope="col"><a href="javascript:void(0)" data-toggle="tooltip" rel="tooltip" data-placement="bottom" data-original-title="<strong><spring:message code="indportal.tobacco.tobaccoUse"/></strong><br>Please check the Tobacco Use box if this family member used tobacco regularly(4 or more times per week) over the last 6 months." data-html="true" tabindex="-1"><spring:message code="indportal.tobacco.tobaccoUse"/>?</a></th>
			              <th class="exemptCell txt-center" scope="col" for="exemptionNumber" ng-if="hardshipExemption" width="80px"><spring:message code="indportal.tobacco.hardshipExemptNum"/></th>
			            </tr>
						
						<tr ng-repeat="applicant in additionalDetails"  ng-if="additionalDetails.length>0">
			              <th scope="row" class="exemptCell">{{applicant.name}}</th>
			              <td class="exemptCell">
			                <div class="no-checkedselector">
			                   <div class="toggle-container center">
			                     <input type="checkbox" name="sc1" ng-model="applicant.applyingForCoverage" value="true" class="ng-pristine ng-valid" ng-disabled=true>
			                   </div>
			                </div>
			              </td>
			              <td class="exemptCell">
			                <div>
			                  <div class="no-checkedselector">
			                    <div class="toggle-container center">
			                      <input type="checkbox" name="sc2" ng-model="applicant.tobaccoUser" value="true" class="ng-pristine ng-valid" ng-disabled="!applicant.enableTobacco">
			                    </div>
			                  </div>
			                </div>
			              </td>
			               <td ng-if="hardshipExemption" class="exemptCell">
			                <div >
			                  <div class="no-checkedselector">
			                    <div class="toggle-container center">
			                      <input type="text" name="sc3" class="exempt span12" ng-model="applicant.exemptionNumber" class="ng-pristine ng-valid" maxlength="10" size="10" id="exemptionNumber">
			                    </div>
			                  </div>
			                </div>
			              </td>
			            </tr>
					 </tbody>
			        </table>
					<br><br><br>
			        <div class="pull-left">
						<a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" ng-click="goHome()"><spring:message code="indportal.portalhome.godashboard" javaScriptEscape="true"/></a>
					</div>
					<div class="pull-right" ng-if="additionalDetails.length>0 && !coverageYearOver">
						 <a href="javascript:void(0)" class="btn btn-primary"  ng-click=enrollAfterTobacoo() ng-disabled="!exemptForm.$valid"><spring:message code="indportal.tobacco.saveContinue"/></a>
					</div>
			      </div>
			
			    </form>
				</div>
			</div>
		</div>

		<div spinning-loader="loader"></div>

		<!-- Modal for failing submission of additional info-->
		<div ng-show= "modalForm.techIssue">
			<div modal-show="modalForm.techIssue" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedSubmissionModal" aria-hidden="true">
		     <div class="modal-dialog">
		     	<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h3 id="failedSubmissionModal"><spring:message code="indportal.portalhome.technicalissue"/></h3>
						</div>
						<div class="modal-body">
							<p><spring:message code="indportal.tobacco.plsComeBack"/></p>
						</div>
						<div class="modal-footer">
	          	<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
	         	</div>
		       </div><!-- /.modal-content-->
		     </div> <!-- /.modal-dialog-->
	    </div>
		</div>
		<!-- Modal for failing submission of additional info-->

<!-- Modal for Filling in Exemption Number-->
<div ng-show= "hardship.fillExemptNum">
      <div modal-show="hardship.fillExemptNum" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ExemptNumberModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="ExemptNumberModal"><spring:message code="indportal.tobacco.exemptNo"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="indportal.tobacco.fillInExemptNo"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for Seeking Coverage-->



  	<!-- Modal-->
    <div ng-show="showDialog" modal-show="showDialog" class="modal fade ng-scope ng-hide bigger-modal" tabindex="-1" role="dialog" aria-labelledby="financialHelpModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true"><span aria-hidden="true">x</span><span class="aria-hidden">close</span></span><span class="aria-hidden">close</span></button>
            <h3 id="financialHelpModal"><spring:message code="indportal.portalhome.financialhelp" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
          <p><spring:message code="indportal.tobacco.stepsFinancialApp"/></p>
          </div>
          <div class="modal-footer">
      <button class="btn pull-left" data-dismiss="modal">Cancel</button>
            <button class="btn btn-primary" data-dismiss="modal" ng-click="financialHelpPath()"><spring:message code="indportal.tobacco.goToFinApp"/></button>
            <button class="btn btn-primary" data-dismiss="modal" ng-click="showApplication(modelattrs.caseNumber)"><spring:message code="indportal.portalhome.nothanks"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
   	 </div>
   </div> 
</div>



</script>