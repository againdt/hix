<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util.tld" prefix="util"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}<span></h1>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    <div class="span9" id="rightpanel">      
      <div class="header">
      	<h4 class="pull-left">My Dependents</h4>
        
        <c:if test="${dependentCount > 1 }">
	        <c:choose>
			    <c:when test="${onlyViewAllowed == 'Y'}">
			    	<a href="#" id="mydependents-edit" class="btn btn-small pull-right" alt="Edit" aria-describedby="description-edit">Edit</a>
			    </c:when>
			    <c:otherwise>
        			<a href="<c:url value='/shop/employee/spplanselection/editmydependents/${employeeApplication.id}'/>" id="mydependents-edit" class="btn btn-small pull-right" alt="Edit" aria-describedby="description-edit">Edit</a>
			    </c:otherwise>
			</c:choose>
        </c:if>
        
      </div>   	
        <div class="gutter10">
          <div class="row-fluid">
            <p>Following is the information provided by your employer and updated by you.</p>
            <c:if test="${dependentCount <= 1 }">
            	No Dependents Found
            </c:if>
			<div id="mydependent-info" class="row-fluid">
				<c:if test="${spouseInfo!=null}">
					<h4 class="bottom-border">Spouse/Life Partner</h4>
					<dl is="spouse" class="dl-horizontal offset1">
						<dt>Name</dt>
						<dd >${spouseInfo.firstName} ${spouseInfo.lastName}
							<c:if test="${spouseInfo.suffix != null && spouseInfo.suffix != ''}">
								${spouseInfo.suffix}
							</c:if>
						</dd>
						<dt>Relationship</dt>
						<dd>
							<c:choose>
							    <c:when test="${spouseInfo.type == null || spouseInfo.type == ''}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							       <c:set var="relationsp" value="${fn:replace(spouseInfo.type,'_', ' ')}"/>
							       <strong>${fn:toUpperCase(fn:substring(relationsp, 0, 1))}${fn:toLowerCase(fn:substring(relationsp, 1, -1))}</strong>
							    </c:otherwise>
						    </c:choose>
						</dd>
						<dt>Home Address</dt>
						<dd ><address>
	  						 ${spouseInfo.location.address1} <br>
	  						 ${spouseInfo.location.address2} ${(empty spouseInfo.location.address2) ? '' : '<br>'}
			                 ${spouseInfo.location.city}, 
			                 ${spouseInfo.location.state} &nbsp;
			                 ${spouseInfo.location.zip}<br>
			                 ${fn:toUpperCase(fn:substring(spouseInfo.location.county, 0, 1))}${fn:toLowerCase(fn:substring(spouseInfo.location.county, 1, -1))}
						</address></dd>
						<dt>Gender</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${spouseInfo.gender != null && spouseInfo.gender != ''}">
				               	  	${fn:toUpperCase(fn:substring(spouseInfo.gender, 0, 1))}${fn:toLowerCase(fn:substring(spouseInfo.gender, 1, -1))}
								</c:when>
								<c:otherwise>
			               			N/A
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Date of Birth</dt>
						<dd >
							<fmt:formatDate pattern="MM/dd/yyyy" value="${spouseInfo.dob}" var="dob" /> ${dob}
						</dd>
						<dt>Tax ID Number</dt>
						<dd >
						<c:choose>
		               	  	<c:when test="${spouseInfo.ssn != null && spouseInfo.ssn != '' && spouseInfo.ssn != '--'}">
			               	  	<c:set var="ssnParts" value="${fn:split(spouseInfo.ssn,'-')}" />
									***-**-${ssnParts[2]}
							</c:when>
							<c:otherwise>
		               			N/A
		               		</c:otherwise>
						</c:choose>
						</dd>
						<dt>Email Address</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${spouseInfo.email == null}">
									N/A
								</c:when>
								<c:otherwise>
			               			<strong>${spouseInfo.email}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Phone Number</dt>
						<dd>
							<c:choose>
			               	  	<c:when test="${spouseInfo.contactNumber == null}">
									N/A
								</c:when>
								<c:otherwise>
			               			<strong>${spouseInfo.contactNumber}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Tobacco User</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${spouseInfo.smoker == null}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(spouseInfo.smoker, 0, 1))}${fn:toLowerCase(fn:substring(spouseInfo.smoker, 1, -1))}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt><spring:message  code="label.tobaccoCessationPrg"/></dt>
						<dd>
						<c:choose>
						    <c:when test="${spouseInfo.tobaccoCessPrg == null || spouseInfo.tobaccoCessPrg == ''}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(spouseInfo.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(spouseInfo.tobaccoCessPrg, 1, -1))}</strong>
						    </c:otherwise>
					    </c:choose>
						</dd>
						<dt>Member of Federally Recognized Tribe</dt>
						<dd >
							<c:choose>
							    <c:when test="${spouseInfo.nativeAmr == null}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							       <strong>${fn:toUpperCase(fn:substring(spouseInfo.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(spouseInfo.nativeAmr, 1, -1))}</strong>
							    </c:otherwise>
							</c:choose>
						</dd>
					</dl>	
				</c:if>	
				
				
				
				<c:forEach  items="${childInfo}" var="empDtls" varStatus="childCount">
					<h4 class="bottom-border">Child/Ward ${childCount.count}</h4>
					<dl class="dl-horizontal offset1">
						<dt>Name</dt>
						<dd id="name_${childCount.index}">${empDtls.firstName} ${empDtls.lastName}
						<c:if test="${empDtls.suffix != null && empDtls.suffix != ''}">
							${empDtls.suffix}
						</c:if>
						</dd>
						<dt>Relationship</dt>
						<dd>
						<c:choose>
						    <c:when test="${empDtls.type == null || empDtls.type == ''}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <c:set var="relationch" value="${fn:replace(empDtls.type,'_', ' ')}"/>
						       <strong>${fn:toUpperCase(fn:substring(relationch, 0, 1))}${fn:toLowerCase(fn:substring(relationch, 1, -1))}</strong>
						    </c:otherwise>
					    </c:choose>
						</dd>
						<dt>Home Address</dt>
						<dd id="address_${childCount.index}"><address>
	  						 ${empDtls.location.address1} <br>
	  						 ${empDtls.location.address2} ${(empty empDtls.location.address2) ? '' : '<br>'}
			                 ${empDtls.location.city}, 
			                 ${empDtls.location.state} &nbsp;
			                 ${empDtls.location.zip}<br>
			                 ${fn:toUpperCase(fn:substring(empDtls.location.county, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.location.county, 1, -1))}
						</address></dd>
						<dt>Gender</dt>
						<dd>
							<c:choose>
			               	  	<c:when test="${empDtls.gender != null && empDtls.gender != ''}">
				               	  	${fn:toUpperCase(fn:substring(empDtls.gender, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.gender, 1, -1))}
								</c:when>
								<c:otherwise>
			               			N/A
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Date of Birth</dt>
						<dd ><fmt:formatDate pattern="MM/dd/yyyy" value="${empDtls.dob}" var="dob" /> ${dob}</dd>
						<dt>Tax ID Number</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${empDtls.ssn != null && empDtls.ssn != '' && empDtls.ssn != '--'}">
				               		<c:set var="ssnParts" value="${fn:split(empDtls.ssn,'-')}" />
										***-**-${ssnParts[2]}
								</c:when>
								<c:otherwise>
			               			N/A
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Email Address</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${empDtls.email == null}">
									N/A
								</c:when>
								<c:otherwise>
			               			<strong>${empDtls.email}</strong>
			               		</c:otherwise>
							</c:choose>
							
						</dd>
						<dt>Phone Number</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${empDtls.contactNumber == null}">
									N/A
								</c:when>
								<c:otherwise>
			               			<strong>${empDtls.contactNumber}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt>Tobacco User</dt>
						<dd >
							<c:choose>
			               	  	<c:when test="${empDtls.smoker == null}">
									N/A
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(empDtls.smoker, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.smoker, 1, -1))}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt><spring:message  code="label.tobaccoCessationPrg"/></dt>
						<dd>
						<c:choose>
						    <c:when test="${spouseInfo.tobaccoCessPrg == null || spouseInfo.tobaccoCessPrg == ''}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDtls.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.tobaccoCessPrg, 1, -1))}</strong>
						    </c:otherwise>
					    </c:choose>
						</dd>
						<dt>Member of Federally Recognized Tribe</dt>
						<dd >
							<c:choose>
							    <c:when test="${empDtls.nativeAmr == null}">
							       N/A
							    </c:when>
							    <c:otherwise>
							       <strong>${fn:toUpperCase(fn:substring(empDtls.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(empDtls.nativeAmr, 1, -1))}</strong>
							    </c:otherwise>
							</c:choose>
						</dd>
						<c:if test="${wardDocDetails != 'none'}">
							<c:forEach var="wardDocDetail" items="${wardDocDetails}">
								<c:if test="${ empDtls.id == wardDocDetail.key}">
								<dt><spring:message  code="label.uploadsupportingdocument"/></dt>
									<c:forEach var="wordDoc" items="${wardDocDetail.value}">
										<dd id="wardDoc_${childCount.index}" ><strong>${wordDoc.value} </strong> <a href="javascript:downloadFile('${wordDoc.key}')">download</a></dd>
									</c:forEach>
								</c:if>
							</c:forEach>
						</c:if>
					</dl>	
				</c:forEach>
			</div>
          </div>
      </div>
  </div><!--/rightpanel--> 
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->
<c:if test="${isEnrollmentEnded }">
<jsp:include page="../terminateemployercoverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
	<jsp:param name="nativeAmr" value="${nativeAmr}" />
</jsp:include>  
</c:if>
<c:if test="${!isEnrollmentEnded }">
<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>
</c:if> 

<script>
$(".inactive").click(function(e) {
	e.preventDefault();
});
function downloadFile(fileId){
	var  contextPath =  "<%=request.getContextPath()%>";

	var url = contextPath+"/shop/employee/spplanselection/downloadDocument?udId="+fileId;
	var subUrl =  '<c:url value="'+url+'"/>';
	window.open(subUrl,"Supported Document");
}
</script>