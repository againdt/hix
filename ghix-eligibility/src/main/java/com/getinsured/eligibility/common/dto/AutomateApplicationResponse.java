package com.getinsured.eligibility.common.dto;

import java.math.BigInteger;

public class AutomateApplicationResponse implements Cloneable{
	
	private Long clonedApplicationId;
	private boolean isApplicationCloned;
	private boolean ranEligibility;
	private boolean ranAutomation;
	private String status;
	private String message;
	
	public Long getClonedApplicationId() {
		return clonedApplicationId;
	}

	public void setClonedApplicationId(Long clonedApplicationId) {
		this.clonedApplicationId = clonedApplicationId;
	}

	public boolean isApplicationCloned() {
		return isApplicationCloned;
	}

	public void setApplicationCloned(boolean isApplicationCloned) {
		this.isApplicationCloned = isApplicationCloned;
	}

	public boolean isRanEligibility() {
		return ranEligibility;
	}

	public void setRanEligibility(boolean ranEligibility) {
		this.ranEligibility = ranEligibility;
	}

	public boolean isRanAutomation() {
		return ranAutomation;
	}

	public void setRanAutomation(boolean ranAutomation) {
		this.ranAutomation = ranAutomation;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	@Override
	public AutomateApplicationResponse clone() {
	    try {
	      return (AutomateApplicationResponse)super.clone();
	    }
	    catch(CloneNotSupportedException e) {
	      throw new AssertionError(e);
	    }
	}
	
}
