package com.getinsured.ahbx.platform;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.ahbx.platform.ecm.webclient.CreateContentAction;
import com.getinsured.ahbx.platform.ecm.webclient.GetContentAction;
import com.getinsured.ahbx.platform.webclient.AddressWebClient;
import com.getinsured.ahbx.platform.webclient.NoticesServiceWebClient;
import com.getinsured.hix.model.PlatformRequest;


/**
 * PlatformWsController - common controller to invoke all AHBX Shared Services (Platform) SOAP WS
 * @author Ekram Ali Kazi
 *
 */
@Controller
@RequestMapping(value = "/platform")
public class PlatformWsController {

	@Autowired private AddressWebClient addressWebClient;
	@Autowired private NoticesServiceWebClient noticesServiceWebClient;
	@Autowired private GetContentAction getContentAction;
	@Autowired private CreateContentAction createContentAction;


	@RequestMapping(value = "/createnotice/adobelivecycle", method = RequestMethod.POST)
	@ResponseBody public String createAdobeLiveCycleNotice(@RequestBody PlatformRequest platformRequest) {

		return noticesServiceWebClient.sendRequest(platformRequest);
	}

	@RequestMapping(value = "/validateaddress/oedq", method = RequestMethod.POST)
	@ResponseBody public String validateIncomingAddress(@RequestBody PlatformRequest platformRequest) {

		return addressWebClient.sendRequest(platformRequest);
	}

	@RequestMapping(value = "/ecm/createcontent", method = RequestMethod.POST)
	@ResponseBody public String createContent(@RequestBody PlatformRequest platformRequest) {

		return createContentAction.createContent(platformRequest);
	}

	@RequestMapping(value = "/ecm/getfilecontent", method = RequestMethod.POST)
	@ResponseBody public String getFileContent(@RequestBody PlatformRequest platformRequest) {

		return getContentAction.getFileContent(platformRequest);

	}

}
