package com.getinsured.eligibility.plan.service;

import java.util.Date;
import java.util.Map;

public class PlanAvailabilityAdapterResponse {

	private Map<String, ApplicantPlanDto> healthPlanDisplayMap;
	private Map<String, ApplicantPlanDto> dentalPlanDisplayMap;
	private Date coverageStartDate;
	private long dentalEnrollmentId;
	private long healthEnrollmentId;
	private String status;

	private boolean healthEnrolled;
	private boolean healthPlanAvailable;
	private boolean dentalEnrolled;
	private boolean dentalPlanAvailable;
	private boolean processHealth;
	private boolean processDental;
	
	private boolean healthTerminationFuture;
	private boolean dentalTerminationFuture;

	public boolean isHealthEnrolled() {
		return healthEnrolled;
	}
	public void setHealthEnrolled(boolean healthEnrolled) {
		this.healthEnrolled = healthEnrolled;
	}
	public boolean isHealthPlanAvailable() {
		return healthPlanAvailable;
	}
	public void setHealthPlanAvailable(boolean healthPlanAvailable) {
		this.healthPlanAvailable = healthPlanAvailable;
	}
	public boolean isDentalEnrolled() {
		return dentalEnrolled;
	}
	public void setDentalEnrolled(boolean dentalEnrolled) {
		this.dentalEnrolled = dentalEnrolled;
	}
	public boolean isDentalPlanAvailable() {
		return dentalPlanAvailable;
	}
	public void setDentalPlanAvailable(boolean dentalPlanAvailable) {
		this.dentalPlanAvailable = dentalPlanAvailable;
	}
	public Map<String, ApplicantPlanDto> getHealthPlanDisplayMap() {
		return healthPlanDisplayMap;
	}
	public void setHealthPlanDisplayMap(
			Map<String, ApplicantPlanDto> healthPlanDisplayMap) {
		this.healthPlanDisplayMap = healthPlanDisplayMap;
	}
	public Map<String, ApplicantPlanDto> getDentalPlanDisplayMap() {
		return dentalPlanDisplayMap;
	}
	public void setDentalPlanDisplayMap(
			Map<String, ApplicantPlanDto> dentalPlanDisplayMap) {
		this.dentalPlanDisplayMap = dentalPlanDisplayMap;
	}
	public Date getCoverageStartDate() {
		return coverageStartDate;
	}
	public void setCoverageStartDate(Date coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}
	public long getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}
	public void setDentalEnrollmentId(long dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}
	public long getHealthEnrollmentId() {
		return healthEnrollmentId;
	}
	public void setHealthEnrollmentId(long healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isProcessHealth() {
		return processHealth;
	}
	public void setProcessHealth(boolean processHealth) {
		this.processHealth = processHealth;
	}
	public boolean isProcessDental() {
		return processDental;
	}
	public void setProcessDental(boolean processDental) {
		this.processDental = processDental;
	}
	public boolean isHealthTerminationFuture() {
		return healthTerminationFuture;
	}
	public void setHealthTerminationFuture(boolean healthTerminationFuture) {
		this.healthTerminationFuture = healthTerminationFuture;
	}
	public boolean isDentalTerminationFuture() {
		return dentalTerminationFuture;
	}
	public void setDentalTerminationFuture(boolean dentalTerminationFuture) {
		this.dentalTerminationFuture = dentalTerminationFuture;
	}


}
