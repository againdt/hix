package com.getinsured.hix.cap.util;

public class AddConsumerAjaxResponse extends AjaxResponseBase {

	public AddConsumerAjaxResponse(boolean status, String message) 
	{
		super(status, message);
	}
	@Override
	public String toString()
	{
		StringBuffer sb = new StringBuffer();
		sb.append(String.format("[HouseholdId:%s]", householdId));
		sb.append(String.format("[FirstName:%s]", firstName));
		sb.append(String.format("[LastName:%s]", lastName));
		
		return String.format("{%s %s }", super.toString(),sb.toString());
	}
	public String getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(String householdId) {
		this.householdId = householdId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	private String householdId;
	private String firstName;
	private String lastName;
}
