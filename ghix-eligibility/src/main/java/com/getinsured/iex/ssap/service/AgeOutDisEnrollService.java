/**
 * 
 */
package com.getinsured.iex.ssap.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * HIX-112981
 * Age out members using IND71G 
 * @author negi_s
 *
 */
public interface AgeOutDisEnrollService {
	
	/**
	 * Disenroll members passed in the disenroll map using IND71G
	 * @param ssapApplicationId
	 * @param enrolledSsapApplicationId
	 * @param enrollmentIdMap
	 * @param disenrollMap
	 * @throws GIException
	 */
	public void handleAgeOutDisenroll(long ssapApplicationId, long enrolledSsapApplicationId,
			Map<String, List<EnrollmentShopDTO>> enrollmentIdMap,
			Map<String, Set<String>> disenrollMap) throws GIException;
}
