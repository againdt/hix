<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
#sidebar h4, #sidebar .header, .gray h4, .header{
	max-height: 10%;
}
h4 small.headerSmall{
	font-size:12px;
	color: #666;
}

.grayBgTable{
	background-color:#F8F8F8;
}

.page-breadcrumb{
	display:inline;
}

h4.rule, .span9 .rule{
	border-bottom:1px solid #999;
	padding-bottom:14px;
}

.top30{
	margin-top:30px;
}
#issuer1 .header{
	height: 55px;
}
</style>
<script>
$(document).ready(function(){
	document.title="Help for YHI";
});

</script>
</head>
<body>
<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a><spring:message code="label.FAQ.agent.FAQ" htmlEscape="false"/></h1>
		 <small class="gutter10"><a href="<c:url value='/faqhome'/>" class="pull-right"> FAQ Home</a></small> 
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    	
		<div class="header">
			<h4 class="margin0">&nbsp;</h4>
		</div>
		<df:csrfToken/>
		<ul class="nav nav-list faq-nav" id="nav">
								<li><a href="#issuer1"><spring:message code="label.FAQ.agent.loginFirstTime" htmlEscape="false"/></a></li>
								<li><a href="#issuer2"><spring:message code="label.FAQ.agent.issuerRep" htmlEscape="false"/></a></li>
								<li><a href="#issuer3"><spring:message code="label.FAQ.agent.ChangingMarketProfiles" htmlEscape="false"/></a></li>
								<li><a href="#issuer4"><spring:message code="label.FAQ.agent.myCertificationInfo" htmlEscape="false"/></a></li>
								<li><a href="#issuer5"><spring:message code="label.FAQ.agent.issuerHistoryScreen" htmlEscape="false"/></a></li>
								<li><a href="#issuer6"><spring:message code="label.FAQ.agent.financialAcctInfo" htmlEscape="false"/></a></li>
								<li><a href="#issuer7"><spring:message code="label.FAQ.agent.submittingPlans" htmlEscape="false"/></a></li>
								<li><a href="#issuer8"><spring:message code="label.FAQ.agent.healthPlanScreens" htmlEscape="false"/></a></li>
								<li><a href="#issuer9"><spring:message code="label.FAQ.agent.theCertStatusInfo" htmlEscape="false"/></a></li>
								<li><a href="#issuer10"><spring:message code="label.FAQ.agent.howtoVerifyPlan" htmlEscape="false"/></a></li>
		</ul>
	</div>
    	
       			<div class="span9 " id="rightpanel">
       			
       				<div id="issuer1">
       				<div class="header">
       					<h4><spring:message code="label.FAQ.agent.howToLoginQ" htmlEscape="false"/></h4>
       				</div>
       				<!--------------------------- question 1 ------------------------>
       				<div class="gutter10">

					<p><spring:message code="label.FAQ.agent.howToLoginA" htmlEscape="false"/></p>
					</div>
						
						</div>						
						<!--------------------------- question 2 ------------------------>
						<div id="issuer2">
						<div class="header">
							<h4><spring:message code="label.FAQ.agent.issuerRep" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>
							<div class="lightgrey">
								<h4 class=""><spring:message code="label.FAQ.agent.createAdditionalRepsQ" htmlEscape="false"/></h4>
							</div>
							<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.createAdditionalRepsA" htmlEscape="false"/></p>
												
							<p><spring:message code="label.FAQ.agent.createAccount" htmlEscape="false"/></p>
							
							<ol>
								<li><spring:message code="label.FAQ.agent.createAccount1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount6" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.createAccount7" htmlEscape="false"/></li>
							</ol>
							</div>
						</div>
						<!--------------------------- question 3 ------------------------>
						
						<div class="lightgrey">
							<h4 class=""><spring:message code="label.FAQ.agent.deactivate" htmlEscape="false"/></h4>
						</div>	
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.deactivateA" htmlEscape="false"/></p>
						</div>
						
								
						<!--------------------------- question 4 ------------------------>		
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.issuerProfile" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">
							<ol>
								<li><spring:message code="label.FAQ.agent.issuerProfile1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.issuerProfile2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.issuerProfile3" htmlEscape="false"/></li>
							</ol>
						</div>
						<!--------------------------- question 5 ------------------------>
						<div class="header">
							<h4 id="issuer3"><spring:message code="label.FAQ.agent.ChangingMarketProfiles" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>			
						</div>					
						<div class="lightgrey">
							<h4 class=""><spring:message code="label.FAQ.agent.companyProfile" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<ol>
								<li><spring:message code="label.FAQ.agent.companyProfile1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile6" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.companyProfile7" htmlEscape="false"/></li>
							</ol>
						</div>										
									
						
						<!--------------------------- question 6 ------------------------>
						
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.indMarketProfile" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.indMarketProfile1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile6" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.indMarketProfile7" htmlEscape="false"/></li>
							</ol>
						</div>
						
						
						
						
						<!--------------------------- question 7 ------------------------>
						<div class="header">
							<h4 id="issuer4"><spring:message code="label.FAQ.agent.myCertificationInfo" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.currentCertStatus" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.currentCertStatusA" htmlEscape="false"/></p>
							<p><spring:message code="label.FAQ.agent.currentCertStatusB" htmlEscape="false"/></p>
							
							<ul>
								<li><spring:message code="label.FAQ.agent.currentCertStatus1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.currentCertStatus2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.currentCertStatus3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.currentCertStatus4" htmlEscape="false"/></li>
							</ul>
						</div>
						
						<!--------------------------- question 8 ------------------------>
						
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.certificationStatus" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.certificationStatusA" htmlEscape="false"/></p>
							
							<ul>
								<li><spring:message code="label.FAQ.agent.certificationStatus1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certificationStatus2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certificationStatus3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certificationStatus4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certificationStatus5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certificationStatus6" htmlEscape="false"/></li>
							</ul>
						</div>
						<!--------------------------- question 9 ------------------------>
						<div class="header" id="issuer5">
							<h4><spring:message code="label.FAQ.agent.issuerHistory" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>

						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.issuerHistoryA" htmlEscape="false"/></p>
						</div>
						<!--------------------------- question 10 ------------------------>
						<div class="header">
							<h4 id="issuer6"><spring:message code="label.FAQ.agent.financialAcctInfo" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.viewBankAcct" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.viewBankAcctA" htmlEscape="false"/></p>
							
							<ol>
								<li><spring:message code="label.FAQ.agent.viewBankAcct1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewBankAcct2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewBankAcct3" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 11 ------------------------>
						
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.addBankAccount" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.addBankAccountA" htmlEscape="false"/></p>
							
							<ol>
								<li><spring:message code="label.FAQ.agent.addBankAccount1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.addBankAccount2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.addBankAccount3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.addBankAccount4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.addBankAccount5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.addBankAccount6" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 12 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.deactivateBankAcct" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.deactivateBankAcctA" htmlEscape="false"/></p>
							
							
							<ol>
								<li><spring:message code="label.FAQ.agent.deactivateBankAcct1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.deactivateBankAcct2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.deactivateBankAcct3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.deactivateBankAcct4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.deactivateBankAcct5" htmlEscape="false"/></li>
							</ol>
						</div>
						
						
						
						
						<!--------------------------- question 13 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.reactivateBankAcct" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<ol>
								<li><spring:message code="label.FAQ.agent.reactivateBankAcct1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.reactivateBankAcct2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.reactivateBankAcct3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.reactivateBankAcct4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.reactivateBankAcct5" htmlEscape="false"/></li>
							</ol>
						</div>
									
						<!--------------------------- question 14 ------------------------>	
						<div class="header" id="issuer7">							
						<h4><spring:message code="label.FAQ.agent.submitPlans" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>	

						
						<div class="gutter10">
							<p><spring:message code="label.FAQ.agent.submitPlansA" htmlEscape="false"/></p>
						</div>
						<!--------------------------- question 15 ------------------------>		
						<div class="header" id="issuer8">						
							<h4><spring:message code="label.FAQ.agent.healthPlanScreens" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>	
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.healthPlanScreens" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">
							<ol>
								<li><spring:message code="label.FAQ.agent.viewHealthPlans1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewHealthPlans2" htmlEscape="false"/></li>
								
							</ol>
						</div>
						
						<!--------------------------- question 16 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.searchHealthPlans" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.searchHealthPlans1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.searchHealthPlans2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.searchHealthPlans3" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 17 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.viewDetailedInfo.HealthPlans" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.viewDetailedInfo.HealthPlans1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewDetailedInfo.HealthPlans2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewDetailedInfo.HealthPlans3" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 18 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.viewPlanBenefits" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.viewPlanBenefits1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanBenefits2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanBenefits3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanBenefits4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanBenefits5" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 19 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.viewPlanRates" htmlEscape="false"/></h4>
						</div>
						
						<div class="gutter10">
							<ol>
								<li><spring:message code="label.FAQ.agent.viewPlanRates1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanRates2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanRates3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanRates4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanRates5" htmlEscape="false"/></li>
							</ol>
						</div>
						
						<!--------------------------- question 20 ------------------------>									
							
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.viewPlanStatus" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.viewPlanStatus1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanStatus2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanStatus3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.viewPlanStatus4" htmlEscape="false"/></li>
							</ol>
						</div>
						<!--------------------------- question 21 ------------------------>			
						<div class="header" id="issuer9">						
							<h4><spring:message code="label.FAQ.agent.theCertStatusInfo" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>
						<div class="lightgrey">
							<h4><spring:message code="label.FAQ.agent.certStatusMeaning" htmlEscape="false"/></h4>
						</div>
						<div class="gutter10">	
							<ul>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.certStatusMeaning6" htmlEscape="false"/></li>
							</ul>
								
						</div>
						<!--------------------------- question 22 ------------------------>				
						<div class="header" id="issuer10">				
							<h4><spring:message code="label.FAQ.agent.verifyPlan" htmlEscape="false"/><small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
						</div>	

						
						<div class="gutter10">	
							<ol>
								<li><spring:message code="label.FAQ.agent.verifyPlan1" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan2" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan3" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan4" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan5" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan6" htmlEscape="false"/></li>
								<li><spring:message code="label.FAQ.agent.verifyPlan7" htmlEscape="false"/></li>
							</ol>
						</div>
									
																					
						 <p class="pull-right"><a id="role-top" href="#">Back to Top</a></p>
						
						
				</div>
		</div>	
</div>	
<script>
$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});
</script>	
</body>
</html>