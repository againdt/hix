<script type="text/ng-template" id="numberofdependents.jsp">
    <div class="gutter10" ng-controller="numOfDepController">
        <h3>
            <spring:message code="label.lce.entereventdetails"/>
        </h3>
        <form class="form-horizontal" name="primaryForm">
            <div class="control-group">
                <label class="control-label margin5-t" for="">
                    <spring:message code="label.lce.selectcriteria"/>:
                </label>
                <div class="controls">
                    <label class="radio-inline">
                        <input type="radio" name="primaryRadio" class="add" required ng-click="reInitialiseDependentPartial(); numOfDependents.table='false';currentEvent.dateOfChange='';eventInfo.eventSubCategory='';resetData('addADependent');" ng-model="numOfDependents.addDepdendents" value="true" ng-value="true">
                        {{dependentEventArray[0].label}}
                    </label>
                    <div ng-if="numOfDependents.addDepdendents" class="margin30-l">
                        <div>
                            <label class="radio-inline">
                            <input type="radio" ng-click="reInitialiseDependentPartial(); numOfDependents.table='false';currentEvent.dateOfChange=''" ng-model="eventInfo.eventSubCategory" name="addDependentsType" value="{{dependentEventArray[5].name}}" required>
                            <spring:message code="label.lce.birth"/>
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                            <input type="radio" ng-click="reInitialiseDependentPartial(); numOfDependents.table='false';currentEvent.dateOfChange=''" ng-model="eventInfo.eventSubCategory" name="addDependentsType" value="{{dependentEventArray[3].name}}" required>
                            <spring:message code="label.lce.adpotionFoster"/> 
                            </label>
                        </div>
						<div>
                            <label class="radio-inline">
                            <input type="radio" ng-click="reInitialiseDependentPartial(); numOfDependents.table='false';currentEvent.dateOfChange=''" ng-model="eventInfo.eventSubCategory" name="addDependentsType" value="{{dependentEventArray[6].name}}" required>
                            <spring:message code="label.lce.gain.custody"/> 
                            </label>
                        </div>
                        <div>
                            <label class="radio-inline">
                            <input type="radio" ng-click="reInitialiseDependentPartial(); numOfDependents.table='false';currentEvent.dateOfChange=''" ng-model="eventInfo.eventSubCategory" name="addDependentsType" value="{{dependentEventArray[0].name}}" required>
                            <spring:message code="label.lce.addOtherDependents"/>
                            </label>
                        </div>
                    </div>
                </div>
                <div class="controls" ng-if="householdArray.length > 1">
                    <label class="radio-inline">
                        <input type="radio" name="primaryRadio" class="remove" required ng-model="eventInfo.eventSubCategory" value="{{dependentEventArray[1].name}}" ng-click="numOfDependents.table='false';currentEvent.dateOfChange='';numOfDependents.addDepdendents=false;resetData(dependentEventArray[1].name);" >
                        {{dependentEventArray[1].label}}
                    </label>
                </div>
                <div class="controls" ng-if="householdArray.length > 1">
                    <label class="radio-inline">
                        <input type="radio" name="primaryRadio" class="remove" required ng-model="eventInfo.eventSubCategory" value="{{dependentEventArray[2].name}}" ng-click="numOfDependents.table='false';currentEvent.dateOfChange='';numOfDependents.addDepdendents=false;resetData(dependentEventArray[2].name);" >
                        {{dependentEventArray[2].label}}
                    </label>
                </div>
                <!-- Error -->
                <div class="controls margin5-t">
                    <span class="validation-error-text-container" ng-if="submitted && primaryForm.primaryRadio.$error.required">Please select valid value.</span>
                </div>
            </div>
        </form>
        <!-- ADD A DEPENDENT: BIRTH -->
        <div class="control-group" ng-if="numOfDependents.addDepdendents">
            <form class="form-horizontal" name="addDependentForm">
                <div class="">
                    <div class="control-group">
                        <label class="control-label" for="birthDate">
                            <spring:message code="label.lce.eventdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
							  	<a  class="ttclass" rel="tooltip" href="javascript:void(null);" data-original-title='<spring:message code="label.lce.addadependent.date.message"/>'>
                      				<i class="icon-question-sign"></i>
                    			</a>
                        </label>
                        <div class="controls">
                            <div id="dependentDate" name="dependentDate" class="input-append" ng-required="numOfDependents.addDepdendents" datetimez ng-model="currentEvent.dateOfChange" check-after="0" >
                                <input data-format="MM/dd/yyyy" type="text" id="birthDate" name="birthDate" class="span12" ng-model="currentEvent.dateOfChange" required placeholder="MM/DD/YYYY"></input>
                                <span class="add-on">
                                <i class="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <!-- Controls -->
                        <!-- Error -->
                        <div class="controls margin5-t">
                            <span class="validation-error-text-container" ng-if="submitted && addDependentForm.dependentDate.$error.required">Please enter event date.</span>
							<span class="validation-error-text-container" ng-if="submitted && addDependentForm.dependentDate.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
                        </div>
                    </div>
					<div class="control-group" ng-if="eventInfo.eventSubCategory == dependentEventArray[0].name">
                        <label class="control-label" for="birthDate">
                            <spring:message code="label.lce.reason.add.tax.dependent"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <div   class="input-append"  >
                                <select name="reasonForAdding"  id="reasonForAdding" ng-options="reasonObject.code as reasonObject.label for reasonObject in addTaxDepeReasons.reasonArray" ng-model="addTaxDepReason.reason" required >
									 <option value="">Select</option>
								</select>
                            </div>
                        </div>
                        <!-- Error -->
                        <div class="controls margin5-t">
                            <span class="validation-error-text-container" ng-if="submitted && addDependentForm.reasonForAdding.$error.required">Please select value.</span>
                        </div>
					</div>
                </div>
                <div>
                    <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
                </div>
                <table role="presentation" id="primaryTable" class="table table-striped" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
                    <tbody>
                        <tr>
                            <td>
                                <spring:message code="label.lce.firstname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.lastname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.relationship"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.seekingcoverage"/>
                            </td>
                        </tr>
                        <tr ng-repeat=" householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                            <td>{{householdMember.name.firstName}}</td>
                            <td>{{householdMember.name.lastName}}</td>
                            <td>
                                {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                            </td>
                            <td>
                                <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                                <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- dependent table table-striped -->
                <div class="center gutter10" style="background: #F7F7F7;">
                    <span class="margin20-r">
                        <spring:message code="label.lce.specifythenumberofdependentsyouwantoadd"/>
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                    </span>
					<label class="aria-hidden" for="numDependents"><spring:message code="label.lce.enterNumDependents"/></label>
                    <input type="text" class="span1 margin20-l" id="numDependents" name="numDependents" ng-model="numDependents" required numbers-only max-number numbers-only-max="99"></input>
                    <div class="controls margin5-t">
 						<span class="validation-error-text-container" ng-if="submitted && (addDependentForm.numDependents.$error.maxNumber || addDependentForm.numDependents.$error.required)"><spring:message code="label.lce.plsEnterNoDependents"/></span>
                    </div>
                </div>
                <dl class="dl-horizontal pull-right">
                    <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
                        <spring:message code="label.lce.back"/>
                    </a>
                    <a ng-if="addDependentForm.$valid && primaryForm.$valid " class="btn btn-primary" ng-click="addDep(numDependents); next()">
                        <spring:message code="label.lce.next"/>
                    </a>
                    <a ng-if="!(addDependentForm.$valid && primaryForm.$valid)" ng-click="$parent.submitted=true" class="btn btn-secondary">
                        <spring:message code="label.lce.next"/>
                    </a>
                </dl>
            </form>
        </div>
        <!-- REMOVE A DEPENDENT: REMOVE -->
        <div class="control-group" ng-if="eventInfo.eventSubCategory== dependentEventArray[1].name">
            <form class="form-horizontal" name="removeDependentForm" novalidate>
                <div class="">
                    <div class="control-group">
                        <label class="control-label" for="removeDate">
                            <spring:message code="label.lce.eventdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <div id="dateRemove" name="dateRemove"  class="input-append" datetimez ng-model="currentEvent.dateOfChange" required check-after="0" >
                                <input data-format="MM/dd/yyyy" type="text" id="removeDate" name="removeDate" class="span12" ng-model="currentEvent.dateOfChange"  required placeholder="MM/DD/YYYY"></input>
                                <span class="add-on">
                                <i class="icon-calendar"></i>
                                </span>
                            </div>
                             <!-- Error -->
                			<div >
                    			<span class="validation-error-text-container" ng-if="submitted && removeDependentForm.dateRemove.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
								<span class="validation-error-text-container" ng-if="submitted && removeDependentForm.dateRemove.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
                			</div>
                        </div>
                    </div>
                </div>
               


                <div>
                    <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
                </div>
                <table role="presentation" id="removeTable" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
                    <tbody>
                        <tr>
                            <td>
                                <spring:message code="label.lce.firstname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.lastname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.relationship"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.seekingcoverage"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.edit"/>
                            </td>
                        </tr>
                        <tr ng-repeat="householdMember  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                            <td>{{householdMember.name.firstName}}</td>
                            <td>{{householdMember.name.lastName}}</td>
                            <td>
                                {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                            </td>
                            <td>
                                <span ng-if="householdMember.applyingForCoverageIndicator">Yes</span>
                                <span ng-if="!householdMember.applyingForCoverageIndicator">No</span>
                            </td>
                            <td>
                               
                                <button class="btn-primary btn-small" ng-click="toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));" ng-if="removeDependentForm.dateRemove.$valid">
                                    <spring:message code="label.lce.remove"/>
                                </button>
                                <button class="btn btn-secondary btn-small" ng-if="!removeDependentForm.dateRemove.$valid" ng-click="$parent.$parent.$parent.submitted=true" >
                                    <spring:message code="label.lce.remove"/>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <dl class="dl-horizontal pull-right">
                    <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
                        <spring:message code="label.lce.back"/>
                    </a>
                    <a ng-if="removeDependentForm.$valid" class="btn btn-primary" ng-click="removeDependents();">
                        <spring:message code="label.lce.next"/>
                    </a>
                    <a ng-if="!removeDependentForm.$valid" ng-click="$parent.submitted=true" class="btn btn-secondary">
                        <spring:message code="label.lce.next"/>
                    </a>
                </dl>
            </form>
        </div>
        <!-- ADD A DEPENDENT: DEATH -->
        <div class="control-group" ng-if="eventInfo.eventSubCategory== dependentEventArray[2].name">
            <form class="form-horizontal" name="deathDependentForm" novalidate>
                <div class="">
                    <div class="control-group">
                        <label class="control-label" for="deathDate">
                            <spring:message code="label.lce.eventdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!">
                        </label>
                        <div class="controls">
                            <div id="dateDeath" name="dateDeath" class="input-append" datetimez ng-model="currentEvent.dateOfChange" ng-required="eventInfo.eventSubCategory== dependentEventArray[2].name" check-after="0">
                                <input data-format="MM/dd/yyyy" type="text" id="deathDate" name="deathDate" class="span12" ng-model="currentEvent.dateOfChange" required placeholder="MM/DD/YYYY"></input>
                                <span class="add-on">
                                <i class="icon-calendar"></i>
                                </span>
                            </div>
                        </div>
                        <!-- Error -->
                        <div class="controls margin5-t">
                            <span class="validation-error-text-container" ng-if="submitted && deathDependentForm.dateDeath.$error.required">Please enter event date.</span>
							<span class="validation-error-text-container" ng-if="submitted && deathDependentForm.dateDeath.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
                        </div>
                    </div>
                </div>

                <div>
                    <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
                </div>
                <table role="presentation" id="deathTable" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
                    <tbody>
                        <tr>
                            <td>
                                <spring:message code="label.lce.firstname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.lastname"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.relationship"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.seekingcoverage"/>
                            </td>
                            <td>
                                <spring:message code="label.lce.edit"/>
                            </td>
                        </tr>
                        <tr ng-repeat="householdMember  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                            <td>{{householdMember.name.firstName}}</td>
                            <td>{{householdMember.name.lastName}}</td>
                            <td>
                                {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                            </td>
                            <td>
                                <span ng-if="householdMember.applyingForCoverageIndicator">Yes</span>
                                <span ng-if="!householdMember.applyingForCoverageIndicator">No</span>
                            </td>
                            <td>
                               
                                <button class="btn btn-primary btn-small" ng-click="toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));" ng-if="deathDependentForm.dateDeath.$valid">
                                    <spring:message code="label.lce.remove"/>
                                </button>
                                <button class="btn btn-secondary btn-small" ng-if="!deathDependentForm.dateDeath.$valid" ng-click="$parent.$parent.$parent.submitted=true" >
                                    <spring:message code="label.lce.remove"/>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <dl class="dl-horizontal pull-right">
                    <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
                        <spring:message code="label.lce.back"/>
                    </a>
                    <a ng-if="deathDependentForm.$valid" class="btn btn-primary" ng-click="removeDependents()">
                        <spring:message code="label.lce.next"/>
                    </a>
                    <a ng-if="!deathDependentForm.$valid"  ng-click="$parent.submitted=true" class="btn btn-secondary">
                        <spring:message code="label.lce.next"/>
                    </a>
                </dl>
            </form>
        </div>
        <div class="margin50-t" ng-if="numOfDependents.table=='true'">
            <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
        </div>
        <table role="presentation" id="" class="table table-striped" ng-if="numOfDependents.table=='true'" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
            <tbody>
                <tr>
                    <td>
                        <spring:message code="label.lce.firstname"/>
                    </td>
                    <td>
                        <spring:message code="label.lce.lastname"/>
                    </td>
                    <td>
                        <spring:message code="label.lce.relationship"/>
                    </td>
                    <td>
                        <spring:message code="label.lce.seekingcoverage"/>
                    </td>
                </tr>
                <tr ng-repeat=" householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                    <td>{{householdMember.name.firstName}}</td>
                    <td>{{householdMember.name.lastName}}</td>
                    <td>
                        {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                    </td>
                    <td>
                        <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                        <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                    </td>
                </tr>
            </tbody>
        </table>

        <!-- MODAL THAT POPS UP WHEN USER WANT TO EDIT  -->
        <modal-small show='modalShown' ng-model="modalHouseholdMember">
            <form class="form-horizontal" name="numOfDepForm" novalidate>
                <div class="gutter20">
                    <h4><spring:message code="lce.areyoursure"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}} </strong> <spring:message code="lce.fromhousehold"/>?
                    </h4>
					<div ng-if="!(eventInfo.eventSubCategory === 'DEATH')">
                        <div id="ssnNumber">
                            <label for="hidDummyAriaField1">
                            <spring:message code="label.lce.plsSelectFollowing"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
							<input type="hidden" id="hidDummyAriaField1" />
                            </label>
                            <div class="margin10-l">
                                <label class="radio-inline" for="numofDepAge">
                                <input type="radio" id="numofDepAge" name="numofdeps" required   value="DEPENDENT_CHILD_AGES_OUT"  ng-model="eventInfoArrayTemp[originalIndex].removeDepEventName" > <spring:message code="label.lce.dependentAging"/>
                                </label>
                                <label class="radio-inline" for="numofDepOther">
                                <input type="radio" id="numofDepOther" name="numofdeps" required value="REMOVE_DEPENDENT"         ng-model="eventInfoArrayTemp[originalIndex].removeDepEventName" > <spring:message code="label.lce.other"/>
                                </label>
                            </div>
                            <!-- controls -->
                            <!-- Error -->
                            <div class="margin30-l margin10-t">
                                <span class="validation-error-text-container" ng-if="submitted && numOfDepForm.numofdeps.$error.required">This value is required.</span>
                            </div>
                        </div>
                        <div class="pull-right">
                            <button class="btn btn-secondary" ng-click="closeModal()">
                                <spring:message code="lce.cancelbutton"/>
                            </button>
    						<button class="btn btn-secondary" ng-if="!numOfDepForm.$valid" ng-click="$parent.submitted=true">
                                <spring:message code="label.lce.remove"/>
                            </button>
                            <button class="btn btn-primary" ng-if="numOfDepForm.$valid" ng-click="addChangedApplicants(modalHouseholdMember.personId,currentEvent.dateOfChange);modalHouseholdMember.applyingForCoverageIndicator= false;updateSeekCoverageFlag(modalHouseholdMember); modalHouseholdMember.active=false; closeModal();updateGlobalIncarcerationFlag();">
                                <spring:message code="label.lce.remove"/>
                            </button>
                        </div>
					</div>
                    <div ng-if="eventInfo.eventSubCategory === 'DEATH'">
                        <div class="pull-right">
                            <button class="btn btn-secondary" ng-click="closeModal()">
                            <spring:message code="lce.cancelbutton"/>
                            </button>
                            <button class="btn btn-primary"  ng-click="eventInfoArrayTemp[originalIndex].removeDepEventName='DEATH';addChangedApplicants(modalHouseholdMember.personId,currentEvent.dateOfChange);modalHouseholdMember.socialSecurityCard.deathIndicator=true;modalHouseholdMember.applyingForCoverageIndicator= false;updateSeekCoverageFlag(modalHouseholdMember);modalHouseholdMember.active=false; closeModal();updateGlobalIncarcerationFlag();">
                                <spring:message code="label.lce.remove"/>
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </modal-small>
		<modal-Tiny show='showWarningOnChange' ng-model="modalHouseholdMember">
           <div class="gutter20">
                   <div>
						<spring:message code="label.lce.changeEvent"/> <strong>{{previousEventName}}</strong> <spring:message code="label.lce.to"/>  <strong>{{currentEventName}}</strong>. <spring:message code="label.lce.yourChangesFor"/> <strong>{{previousEventName}}</strong> <spring:message code="label.lce.revertToOriginal"/>
                   </div>
				  <div class="pull-right">
                            <button class="btn btn-secondary" ng-click="hideDataChangeModel();">
                                <spring:message code="label.lce.ok"/>
                            </button>
    			  </div>
			</div>	
   		</modal-Tiny>
    </div>
</script>