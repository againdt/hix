//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SocialSecurityBenefit complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SocialSecurityBenefit">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="benefitAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="benefitFrequency" type="{http://www.cohbe.org/SSA/Person}BenefitFrequency-Enum" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SocialSecurityBenefit", propOrder = {
    "benefitAmount",
    "benefitFrequency"
})

@Entity
@Table(name="SSBenefit")
public class SocialSecurityBenefit {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

    protected Double benefitAmount;
    protected BenefitFrequencyEnum benefitFrequency;

    /**
     * Gets the value of the benefitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getBenefitAmount() {
        return benefitAmount;
    }

    /**
     * Sets the value of the benefitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setBenefitAmount(Double value) {
        this.benefitAmount = value;
    }

    /**
     * Gets the value of the benefitFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link BenefitFrequencyEnum }
     *     
     */
    public BenefitFrequencyEnum getBenefitFrequency() {
        return benefitFrequency;
    }

    /**
     * Sets the value of the benefitFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link BenefitFrequencyEnum }
     *     
     */
    public void setBenefitFrequency(BenefitFrequencyEnum value) {
        this.benefitFrequency = value;
    }

}
