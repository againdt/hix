package com.getinsured.hix.batch.externalnotices.reader;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.getinsured.hix.model.ExternalNotice;

public class ExternalNoticeMapper implements RowMapper<ExternalNotice>{

	@Override
	public ExternalNotice mapRow(ResultSet rs, int rowNum) throws SQLException {
		ExternalNotice externalNotices = new ExternalNotice(rs.getInt("ID"), rs.getInt("SSAP_ID"),
				rs.getInt("NOTICE_ID"), rs.getString("EXTERNAL_HOUSEHOLD_CASE_ID"), rs.getString("STATUS"),
				rs.getString("REQUEST_PAYLOAD"),
				rs.getString("RESPONSE_PAYLOAD"),
				rs.getDate("CREATION_TIMESTAMP"),
				rs.getDate("LAST_UPDATE_TIMESTAMP"),
				rs.getInt("RETRY_COUNT"));		
				
		return externalNotices;
	}

}
