package com.getinsured.ssap.service

import com.getinsured.iex.ssap.SingleStreamlinedApplication
import com.getinsured.ssap.model.ComparisonResult

/**
 * Comparison service definition.
 *
 * @author Yevgen Golubenko
 * @since 8/10/19
 */
interface SsapComparisonService {
  /**
   * Compares two given SSAP's and returns [ComparisonResult] that will indicate
   * which part of given SSAP's are different.
   *
   * It is important to pass current SSAP application as first argument and
   * old (parent ssap, that current is cloned from) as second.
   *
   * @param newSsap [SingleStreamlinedApplication] first SSAP (current SSAP).
   * @param oldSsap [SingleStreamlinedApplication] second SSAP (old SSAP, one thats newSsap is cloned from)
   * @return List of differences defined by [ComparisonResult].
   */
  fun compare(newSsap: SingleStreamlinedApplication, oldSsap: SingleStreamlinedApplication): List<ComparisonResult>
}
