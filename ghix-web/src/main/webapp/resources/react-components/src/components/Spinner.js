import React from 'react';
import classNames from "classnames";

const Spinner = props => {
    return (
        <div
            className={classNames(
                `ds-u-display--flex ds-u-justify-content--center ds-u-align-items--center spinner-whole-page`
            )}
        >
      <span
          className={classNames(
              "ds-c-spinner",
              "ds-c-spinner--filled",
              `ds-c-spinner--${props.size}`,
              `${props.customStyle}`
          )}
          aria-valuetext={props.ariaText}
          role={props.role}
      />
        </div>
    );
};

export default Spinner;


