package com.getinsured.eligibility.lce.endpoint;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;

import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.lce.task.LifeChangeEventTask;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentRequest;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentService;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ssap.validation.service.QleValidationService;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.SsapEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.dto.SepEventsResource;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.SsapApplicationEventResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapSEPIntegrationDTO;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.thoughtworks.xstream.XStream;


@Controller
@RequestMapping("/iex/lce/")
public class LifeChangeEventEndPoint {
	
	
	private static final int NO_OF_DAYS_TO_DENY_SEP = 70;

	private static final String NUMBER_OF_DAYS = "Numberofdays";

	private static final String ES = "ES";

	private static final String LOCAL_ES = "es";

	private static final String DATE_FORMAT = "MMMM dd, YYYY";

	private static final String SPANISH_DATE = "spanishDate";

	private static final String ENGLISH_DATE = "englishDate";

	private static final String DO_MOVE_ENROLLMENT = "doMoveEnrollment";

	private static final String IS_DISENROLL_CASE = "isDisenrollCase";
	
	private static final String IS_NO_CHANGE__CASE = "isNoChangeCase";

	private static final String APPLICATION_STATUS_DE = "DE";

	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventEndPoint.class);

	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";	
	private static final String STATE_NAME = "exgStateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String EXCHANGE_NAME = "exgName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_ADDRESS_TWO = "exchangeAddress2";
	private static final String EXCHANGE_CITY_NAME = "exgCityName";
	private static final String EXCHANGE_ZIP = "zip";
	private static final String PRIMARY_APPLICANT = "primaryContact";
	private static final String APPLICANTS = "applicants";
	private static final String EXCHANGE_ADDRESS_EMAIL = "exchangeAddressEmail";
	private static final String USER_NAME = "userName";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "SpecialEnrollmentEndDate";
	private static final String DATE_FORMAT_FOR_NOTICE ="MMM dd, yyyy";
	private static final String EMPTY_STRING="";
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private ResourceCreator resourceCreator;
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	@Qualifier("moveEnrollmentService")
	private MoveEnrollmentService moveEnrollmentService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private ILocationRepository iLocationRepository;
	@Autowired
	private LifeChangeEventTask lifeChangeEventTask;
	@Autowired
	private QleValidationService qleValidationService;
	@Autowired 
	private IUserRepository iUserRepository;
	@Autowired
	private ReferralLceNotificationService referralLceNotificationService;
	
	private ThreadLocal<Set<String>> events;
	private ThreadLocal<Set<Integer>> demoEvents;
	
	@RequestMapping(value = "/processor", method = RequestMethod.POST)
	@ResponseBody
	public String process(@RequestBody SsapSEPIntegrationDTO ssapSEPIntegrationDTO) throws Exception {
		LOGGER.info("Life Change Event EndPoint : Starts");

		long applicationEventId;
		String status;
		AccountUser user;
		Long applicationId = null;
		boolean doMoveEnrollment = false;
		boolean isDisenrollCase = false;
		boolean isSepDenial = false;
		boolean isNoChangeCase = false;
		
		try {
			// if sep denial case return success.
			if(ssapSEPIntegrationDTO != null && ssapSEPIntegrationDTO.getIsSepDenialCase()!=null && ssapSEPIntegrationDTO.getIsSepDenialCase()){
				return  GhixConstants.RESPONSE_SUCCESS;
			}
			applicationId = ssapSEPIntegrationDTO.getApplicationId();
			init(applicationId);
			
			if(isEventsNotNullOrEmpty()){
				status = GhixConstants.RESPONSE_SUCCESS;
			}
			SsapApplicationResource ssapApplicationResource = resourceCreator.getSsapApplicationById(applicationId);
			String eligibiliyStatus = ssapApplicationResource.getEligibilityStatus();
			LOGGER.debug("Application status "+eligibiliyStatus);
			user = getUser(ssapApplicationResource.getCmrHouseoldId().toString());
			Household household = getHouseholdById(ssapApplicationResource.getCmrHouseoldId().toString());
			List<SsapApplicant> ssapApplicants = getAllApplicantsByApplicationId(applicationId);
			//get application event for notifications
			SsapApplicationEventResource ssapApplicationEventResource = resourceCreator.findSsapApplicationEventBySsapApplicationId(applicationId);
			applicationEventId = LifeChangeEventUtil.getIdFromLinks(ssapApplicationEventResource.get_links(), LifeChangeEventConstant.SELF);
			List<SsapApplicantEvent> ssapApplicantEvents = ssapApplicantEventRepository.findByApplicationEventId(applicationEventId);
			LOGGER.info("Retrive enrolled applicants");
				
			Set<Long> enrolledPersons = null;
			String lcePerfMode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_PERF_MODE);
			boolean lcePerfModeActive = (lcePerfMode != null && lcePerfMode.equalsIgnoreCase("Y"));
			if(lcePerfModeActive) {
				doMoveEnrollment = ssapSEPIntegrationDTO.isDoMoveEnrollment();
				isDisenrollCase = ssapSEPIntegrationDTO.getIsDisenrollCase();
				isSepDenial = ssapSEPIntegrationDTO.getIsSepDenialCase();
				isNoChangeCase = ssapSEPIntegrationDTO.getIsNoChangeCase();
			}
			else {
				enrolledPersons = invokeEnrollmentApi(ssapApplicationResource.getParentSsapApplicationId());
				//check if move enrollment case
				Map<String,Boolean> result = isMoveEnrollmentCase(ssapSEPIntegrationDTO, ssapApplicants, enrolledPersons,ssapApplicantEvents);
				doMoveEnrollment = result.get(DO_MOVE_ENROLLMENT);  
				isDisenrollCase = result.get(IS_DISENROLL_CASE);
				isNoChangeCase = result.get(IS_NO_CHANGE__CASE);
			}
			
			boolean validationResult = qleValidationService.triggerQleValidation(ssapApplicationResource.getCaseNumber(),getAdminUserId());
			//set no QLE validation required - set allow enrollment as Y else stop flow. 
			if(!validationResult){
				referralLceNotificationService.generateLceDocumentRequiredNotice(ssapApplicationResource.getCaseNumber());
				return GhixConstants.RESPONSE_SUCCESS;
			}
			
			//if application don't have seeking coverage "Y" member proceed for disenroll
			if(isDisenrollCase) {
				  updateApplicationStatusAndEligiblityStatus(applicationId, ApplicationStatus.ELIGIBILITY_RECEIVED, EligibilityStatus.DE);
				  //send notification
				  sendNotification(null, ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_DISENROLL_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_DISENROLL_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_DISENROLL_ECM_RELATIVE_PATH, null,ssapApplicants, household);
				  return GhixConstants.RESPONSE_SUCCESS;
			 }
			
			
			
			if(doMoveEnrollment || isChangeInDemoOnly()) {
				  LOGGER.info("Move enrollment started");
				  moveEnrollment(applicationId, ssapApplicationResource.getParentSsapApplicationId(), user);
				  LOGGER.info("Move enrollment Ended");
			}	
			
			
			
			
			//Sep Denials logic starts here // added if here common logic works with LCE perf mode Y and N
			if(!doMoveEnrollment && !isDisenrollCase){
				if(!lcePerfModeActive){
					isSepDenial = processSepDenial(enrolledPersons,ssapApplicantEvents,ssapSEPIntegrationDTO);
				}
				
				//close the application if DenialCase
				if(isSepDenial){
					updateApplicationStatus(applicationId, ApplicationStatus.CLOSED);
				}
			}
			
			
			
			if(!isNoChangeCase){
				if(eligibiliyStatus.equals(APPLICATION_STATUS_DE)){
					sendNotification(null, ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_DISENROLL_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_DISENROLL_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_DISENROLL_ECM_RELATIVE_PATH, null,ssapApplicants, household);
					return GhixConstants.RESPONSE_SUCCESS;
				} if(isSepDenial){
					//send SEP Denial Notification
					sendNotification(null, ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_SEP_DENIAL_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_SEP_DENIAL_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_SEP_DENIAL_ECM_RELATIVE_PATH, null,ssapApplicants, household);
				} else if(isChangeInDemoOnly() || isAddressChangeDemoEvent(ssapSEPIntegrationDTO)){
					//String response = moveEnrollment(applicationId, oldApplicationId);				
					//HIX-85076 - Do not send this notice
					/*if(GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response)){
						//sending notification to primary applicant
						List<SsapApplicantResource> applicants = resourceCreator.findApplicantsByApplicationEventAndSepEventIds(applicationEventId, demoEvents.get());
						sendNotification(applicants,ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_DEMO_CHANGE_NOTIFICATION_FILE_NAME, 
								LifeChangeEventConstant.LCE_DEMO_CHANGE_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_DEMO_CHANGE_ECM_RELATIVE_PATH, null,ssapApplicants, household);
					}*/
				}  else if(isActionRequiredDemographic(ssapSEPIntegrationDTO)){
					ssapApplicationEventResource.setKeepOnly(LifeChangeEventConstant.YES);
					resourceCreator.updateSsapApplicationEventById(applicationEventId, ssapApplicationEventResource);
					
					//sending notification to primary applicant
					List<SsapApplicantResource> applicants = resourceCreator.findApplicantsByApplicationEventAndSepEventIds(applicationEventId, demoEvents.get());
					sendNotification(applicants,ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_DEMOGRAPHICS_ACTION_REQUIRED_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_DOB_CHANGE_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_DEMOGRAPHICS_ACTION_REQUIRED_NOTIFICATION_ECM_RELATIVE_PATH, ssapApplicationEventResource.getEnrollmentEndDate(),ssapApplicants, household);
				} /*else if(isGainOrLossInNativeAmericanStatus()){ //commented as HIX-63368 - No notice in case of Tribe change 
					//send notification for loss in status
					sendNotification(null, ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_AMERICAN_STATUS_CHANGE_NOTIFICATION_FILE_NAME, 
							LifeChangeEventConstant.LCE_DOB_CHANGE_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_AMERICAN_STATUS_CHANGE_ECM_RELATIVE_PATH, ssapApplicationEventResource.getEnrollmentEndDate(),ssapApplicants, household);
				}*/ else {
					// TODO : the below notice will be triggered in case of MN for now we can change if needed
					// Replace EE027 by EE020 in case of MN
					String stateCode = DynamicPropertiesUtil
							.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					if (stateCode.equalsIgnoreCase("MN")) {
						// trigger SEP LCE04 notice (EE020)
						referralLceNotificationService.generateSEPEventNotice(ssapApplicationResource.getCaseNumber());
					} else {
						//send SEP notification. Assuming Enrollment Dates in Application Event table is already populated by Integration API.					
						sendNotification(null, ssapApplicationResource.getCmrHouseoldId().intValue(), user, LifeChangeEventConstant.LCE_SEP_NOTIFICATION_FILE_NAME, 
								LifeChangeEventConstant.LCE_SEP_NOTIFICATION_TEMPLATE_NAME, LifeChangeEventConstant.LCE_SEP_ECM_RELATIVE_PATH, ssapApplicationEventResource.getEnrollmentEndDate(),ssapApplicants, household);	
					}					
				}
			}
						
			status = GhixConstants.RESPONSE_SUCCESS;
		} catch(Exception exception){
			LOGGER.error("Error occurred while processing enrollment for Demo changes : ", exception);
			status = GhixConstants.RESPONSE_FAILURE;
		}
		
		reset();
		
		LOGGER.info("Life Change Event EndPoint : Ends");
		
		return status;
	}

	private boolean isEventsNotNullOrEmpty() {
		return events.get()!=null && events.get().size()>0;
	}

	private Map<String,Boolean> isMoveEnrollmentCase(SsapSEPIntegrationDTO ssapSEPIntegrationDTO, List<SsapApplicant> ssapApplicants, Set<Long> enrolledPersons,List<SsapApplicantEvent> ssapApplicantEvents) {
		boolean doMoveEnrollment = true;
		boolean isDisenrollCase = true;
		Map<String,Boolean> result= new HashMap<String,Boolean>();
		List<Boolean> moveApplicantFlags = new ArrayList<Boolean>();
		List<Boolean> noChangeCaseFlags = new ArrayList<Boolean>();
		boolean isNoChangeCase = true;
		boolean noChangeFlag = false;
		
		//prepare Map of Applicant Id and Applicants
		Map<Long,List<SsapApplicantEvent>> applicantEvents = prepareMapofAppliantEvents(ssapApplicantEvents);
		Iterator<SsapApplicant> it = ssapApplicants.iterator();
		  while (it.hasNext()) {
		        SsapApplicant ssapApplicant = it.next();
		       
		        if(ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("Y")) {
		        	isDisenrollCase = false;
		        }
		        
		        //check if events is Demographic or Address change Address change
		        if(isDemographicEvent(ssapSEPIntegrationDTO,ssapApplicant,applicantEvents)){
		        	moveApplicantFlags.add(true);	
		        } else if(ssapApplicant.getStatus().equals(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString())) {
		        	moveApplicantFlags.add(true);	
		        	noChangeFlag = true;
		        } else if(!onEnrolledApplication(enrolledPersons, ssapApplicant) && ssapApplicant.getApplyingForCoverage().equalsIgnoreCase("N") && !ssapApplicant.getStatus().equals("LCE_NO_CHANGE")) {
		        	moveApplicantFlags.add(true);	
		        	noChangeFlag = true;
		        } else {
		        	moveApplicantFlags.add(false);	
		        }
		        
		        noChangeCaseFlags.add(noChangeFlag);
		  }
		  
		  for (Boolean moveApplicantFlag : moveApplicantFlags) {
			if(!moveApplicantFlag) {
				doMoveEnrollment = false;
				break;
			}
		  }
		  
		  for (Boolean noChangeCaseFlag : noChangeCaseFlags) {
				if(!noChangeCaseFlag) {
					isNoChangeCase = false;
					break;
				}
		  }
		  
		  if(isAddressChangeDemoEvent(ssapSEPIntegrationDTO)) {
			  doMoveEnrollment = true;
		  }
		  
		  result.put(DO_MOVE_ENROLLMENT, doMoveEnrollment);
		  result.put(IS_DISENROLL_CASE, isDisenrollCase);
		  result.put(IS_NO_CHANGE__CASE, isNoChangeCase);
		return result;
	}

	private boolean isDemographicEvent(SsapSEPIntegrationDTO ssapSEPIntegrationDTO, SsapApplicant ssapApplicant,
			Map<Long, List<SsapApplicantEvent>> applicantEvents) {
		boolean isDemographicEvent = true;
		List<SsapApplicantEvent> eventsForApplicant = applicantEvents.get(ssapApplicant.getId());
		if(eventsForApplicant == null || eventsForApplicant.isEmpty()){
			return false;
		}
		Boolean isAddressChangeDemoEvent = ssapSEPIntegrationDTO.getIsAddressChangeDemoEvent();
		if(isAddressChangeDemoEvent == null) {
			isAddressChangeDemoEvent = false;
		}
		for(SsapApplicantEvent ssapApplicantEvent : eventsForApplicant){
			if(!LifeChangeEventUtil.getDemographicEvents().contains(ssapApplicantEvent.getSepEvents().getName()) && !(ssapApplicantEvent.getSepEvents().getName().equals(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE) && isAddressChangeDemoEvent )){
				isDemographicEvent = false;
			}
		}
		
		return isDemographicEvent;
	}

	private Map<Long,List<SsapApplicantEvent>> prepareMapofAppliantEvents(
			List<SsapApplicantEvent> ssapApplicantEvents) {
		Map<Long,List<SsapApplicantEvent>> applicantsEvents = new HashMap<Long, List<SsapApplicantEvent>>();
		for(SsapApplicantEvent ssapApplicantEvent : ssapApplicantEvents){
			 List<SsapApplicantEvent> eventForApplicant =  applicantsEvents.get(ssapApplicantEvent.getSsapApplicant().getId());
			if(eventForApplicant== null){
				eventForApplicant =  new ArrayList<SsapApplicantEvent>();
				applicantsEvents.put(ssapApplicantEvent.getSsapApplicant().getId(), eventForApplicant);
			}
			eventForApplicant.add(ssapApplicantEvent);
		}
		return applicantsEvents;
		
	}

	private Boolean isAddressChangeDemoEvent(
			SsapSEPIntegrationDTO ssapSEPIntegrationDTO) {
		Boolean isAddressChangeDemoEvent;
		boolean isAddressChangeWithDemographicsOnly = true;
		boolean processAddressChangeDemoOnly = false;
		
		isAddressChangeDemoEvent = ssapSEPIntegrationDTO.getIsAddressChangeDemoEvent();
		if(isAddressChangeDemoEvent == null) {
			  return false;
		}
		
		for(String event:events.get()){
			if(!event.equals(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE) && !LifeChangeEventUtil.getDemographicEvents().contains(event)){
				isAddressChangeWithDemographicsOnly = false;
				break;
			}
		}
		
		if(isAddressChangeWithDemographicsOnly &&  isAddressChangeDemoEvent){
			processAddressChangeDemoOnly = true;
		}
		
		
		return processAddressChangeDemoOnly;
	}

	private List<SsapApplicant> getAllApplicantsByApplicationId(
			Long applicationId) {
		SsapApplication ssapApplication = new SsapApplication();
		ssapApplication.setId(applicationId);
		List<SsapApplicant> ssapApplicants = ssapApplicantRepository.findBySsapApplication(ssapApplication);
		return ssapApplicants;
	}
	
	private boolean isChangeInDemoOnly(){
		for(String event:events.get()){
			if(!LifeChangeEventUtil.getDemographicEvents().contains(event)){
				return false;
			}
		}
		return true;
	}
	
	private boolean isActionRequiredDemographic(SsapSEPIntegrationDTO ssapSEPIntegrationDTO){
		Set<String> actionRequiredDemoEvents = LifeChangeEventUtil.getActionRequiredDemographicEvents();
		Set<String> demoEvents = LifeChangeEventUtil.getDemographicEvents();
		
		boolean isActionRequiredDemographic = false;
		for(String event:events.get()){
			if(  !actionRequiredDemoEvents.contains(event)  && !demoEvents.contains(event)  ){
				return false;
			}else {
				if(actionRequiredDemoEvents.contains(event)){
					isActionRequiredDemographic = true;
				}
			}
		}
		return isActionRequiredDemographic;
	}
	
	private void init(Long applicationId) throws Exception{
		events = new ThreadLocal<Set<String>>();
		demoEvents = new ThreadLocal<Set<Integer>>();
		
		events.set(new HashSet<String>());
		demoEvents.set(new HashSet<Integer>());
		
		List<SepEventsResource> listOfEvents = resourceCreator.findEventsByApplicationId(applicationId);
		
		for(SepEventsResource resource:listOfEvents){
			if(!LifeChangeEventConstant.OTHER.equals(resource.getName())){
				events.get().add(resource.getName());
				demoEvents.get().add(Integer.valueOf(LifeChangeEventUtil.getIdFromLinks(resource.get_links(), LifeChangeEventConstant.SELF).toString()));
			}
		}
	}
	
	private void reset(){
		events = null;
		demoEvents = null;
	}
	
	public String moveEnrollment(long newApplicationId, long oldApplicationId, AccountUser user){
		LOGGER.info("Move Enrollment : Starts");
		
		MoveEnrollmentRequest moveEnrollmentRequest;
		
		try {
			moveEnrollmentRequest = new MoveEnrollmentRequest();
			moveEnrollmentRequest.setEnrolledSsapApplicationId(oldApplicationId);
			moveEnrollmentRequest.setNewSsapApplicationId(newApplicationId);
			moveEnrollmentRequest.setUserName(LifeChangeEventConstant.EXADMIN_USERNAME); 
			String status = moveEnrollmentService.process(moveEnrollmentRequest);
			if (!status.equals(GhixConstants.RESPONSE_SUCCESS)) {
				throw new GIRuntimeException("Error occured while moving enrollment :" + newApplicationId);
			}
			//updated new application status to EN
			updateSsapAppStatusOnMemberStatus(newApplicationId, user);
			//HIX-116154 update new application dental status
			updateApplicationDentalstatus(oldApplicationId, newApplicationId);
			//update old application to CL
			updateApplicationStatus(oldApplicationId,ApplicationStatus.CLOSED);
			
		} catch(Exception exception){
			LOGGER.error("Moving Enrollment Failed ", exception);
			throw exception;
		}
		
		LOGGER.info("Move Enrollment : Ends");
		
		return GhixConstants.RESPONSE_SUCCESS;   
	}
	
	private void updateSsapAppStatusOnMemberStatus(long ssapApplicationId, AccountUser user) {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		ApplicationStatus applicationStatus = ApplicationStatus.ENROLLED_OR_ACTIVE;
		EnrolleeResponse enrolleeResponse;
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		List<String> exchgIndivIdList = new ArrayList<String>();
		
		try {
			EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
			enrolleeRequest.setSsapApplicationId(ssapApplicationId);
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, user.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest));
			enrolleeResponse = (EnrolleeResponse) xstream.fromXML(response.getBody());
			
			if (null != enrolleeResponse && null!= enrolleeResponse.getStatus() && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
					List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
					for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
						exchgIndivIdList.add(enrolleeMembers.getExchgIndivIdentifier());
					}
				}
			} 

			if(null == exchgIndivIdList || exchgIndivIdList.isEmpty()) {
				LOGGER.error("Error: Member Id List can not be null/empty for application ID " + ssapApplicationId );
				throw new Exception("Error: Member Id List null/empty while updating application status. ");
			}

			List <SsapApplicant> applicationMembrList =ssapApplicantRepository.findEligibleMembersBySsapApplicationId(ssapApplicationId)  ;
			for(SsapApplicant applicationMember: applicationMembrList) {
				if(!exchgIndivIdList.contains(applicationMember.getApplicantGuid())) {
					applicationStatus = ApplicationStatus.PARTIALLY_ENROLLED;
					break;
				}
			 }
			 
			 updateApplicationStatus(ssapApplicationId, applicationStatus);
		} catch(Exception exception){
			LOGGER.error("Application Status Update Failed ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getMessage(exception), Severity.HIGH, GIRuntimeException.Component.LCE.toString(), null);
		}
	}
	
	private void updateApplicationDentalstatus(long oldApplicationId,long newApplicationId) {
		if(oldApplicationId != newApplicationId) {
			SsapApplication enrolledApp = ssapApplicationRepository.findAndLoadApplicantsByAppId(oldApplicationId);
			SsapApplication newApp = ssapApplicationRepository.findAndLoadApplicantsByAppId(newApplicationId);
			if(!StringUtils.isEmpty(enrolledApp.getApplicationDentalStatus())) {
				newApp.setApplicationDentalStatus(enrolledApp.getApplicationDentalStatus());
				ssapApplicationRepository.saveAndFlush(newApp);
			}
		}
	}
	
	private void updateApplicationStatus(long applicationId,ApplicationStatus applicationStatus){
		SsapApplication ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(applicationId);
		ssapApplication.setApplicationStatus(applicationStatus.getApplicationStatusCode());
		ssapApplication.setAllowEnrollment(LifeChangeEventConstant.YES);
		ssapApplicationRepository.saveAndFlush(ssapApplication);
	}
	
	private AccountUser getUser(String householdId) throws Exception{
		LOGGER.info("Fetching User Info by HouseHold Id : Starts"+householdId);

		XStream xstream = null;
		AccountUser user = null;
		ConsumerResponse consumerResponse;
		
		try {
			ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, null, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, householdId);
			if (responseEntity != null) {
				if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
					throw new Exception();
				} else {
					xstream = GhixUtils.getXStreamStaxObject();
					consumerResponse = (ConsumerResponse) xstream.fromXML(responseEntity.getBody());
					user = consumerResponse.getHousehold().getUser();
				}
			}
		} catch(Exception exception){
			LOGGER.error("Rest call to find household failed", exception);
			throw exception;
		}
		
		LOGGER.info("Fetching User Info by HouseHold Id : Ends");
		
		return user;
	}
	
	
	private Household getHouseholdById(String householdId) throws Exception{
		LOGGER.info("Fetching User Info by HouseHold Id : Starts"+householdId);
		XStream xstream = null;
		ResponseEntity<String> responseEntity = null;
		ConsumerResponse consumerResponse = null;
		Household household = null;
		try {
			responseEntity = ghixRestTemplate.exchange(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, null, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, householdId);
			if (responseEntity != null) {
				if (!responseEntity.getStatusCode().equals(HttpStatus.OK)) {
					throw new Exception();
				} else {
					xstream = GhixUtils.getXStreamStaxObject();
					consumerResponse = (ConsumerResponse) xstream.fromXML(responseEntity.getBody());
					household = consumerResponse.getHousehold();
				}
			}
		} catch(Exception exception){
			LOGGER.error("Rest call to find household failed", exception);
			throw exception;
		}
		
		LOGGER.info("Fetching User Info by HouseHold Id : Ends");
		
		return household;
	}
	
	public SsapApplicationResource updateSsapApplication(long applicationId, SsapApplicationResource ssapApplicationResource) throws Exception {
		try {
			if(ssapApplicationResource != null){
				return resourceCreator.updateSsapApplicationById(applicationId, ssapApplicationResource);
			} else{
				throw new Exception("SSAP Application does not exist.");
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating ssap json : ", exception);
			throw exception;
		}
	}
	
	
	
	private void sendNotification(List<SsapApplicantResource> applicants, Integer individualId, AccountUser user, String fileName, String templateName, String relativePath, Date sepEndDate,List<SsapApplicant> ssapApplicants, Household household) throws Exception {
		LOGGER.info("Sending Notification : Starts");
		
		String lcePerfMode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_PERF_MODE);
		if(lcePerfMode != null && lcePerfMode.equalsIgnoreCase("Y")) {
			lifeChangeEventTask.sendNotification(applicants, user, fileName, templateName, relativePath, sepEndDate,ssapApplicants, household);
			return;
		}
		
		Map<String, Object> individualTemplateData = new HashMap<String, Object>();
		List<String> sendToEmailList = new LinkedList<String>();;
		DateFormat dateFormat = null;
		DateFormat dateFormatForSpanish = null;
		try {
			individualTemplateData.put(LifeChangeEventConstant.NOTIFICATION_SEND_DATE, new TSDate());
			individualTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
			individualTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			individualTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			individualTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			individualTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);			
			individualTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			individualTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			
			individualTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			individualTemplateData.put(EXCHANGE_ADDRESS_TWO , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_2));
			individualTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			individualTemplateData.put(EXCHANGE_ZIP , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			//set data for SEP Denial Notification
			individualTemplateData.put(NUMBER_OF_DAYS,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SEP_NUMBER_OF_DAYS_TO_SUBMIT_AN_APPEAL));
			
			//initialize the date formatter
			dateFormat = new SimpleDateFormat(DATE_FORMAT);
			dateFormatForSpanish = new SimpleDateFormat(DATE_FORMAT,new Locale(LOCAL_ES, ES));
			//set English and Spanish date into template data
			Date todaysDate = new TSDate(); 
			individualTemplateData.put(ENGLISH_DATE ,dateFormat.format(todaysDate));			
			individualTemplateData.put(SPANISH_DATE ,dateFormatForSpanish.format(todaysDate));			
			
			//get name of primary applicant
			String name = household.getFirstName() + " " + household.getLastName();//LifeChangeEventUtil.getNameOfPrimaryApplicantFromSsapApplicant(ssapApplicants);
			individualTemplateData.put(PRIMARY_APPLICANT , name);
			individualTemplateData.put(USER_NAME , user.getUserName());
			individualTemplateData.put(EXCHANGE_ADDRESS_EMAIL , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			individualTemplateData.put(APPLICANTS , applicants);
			if(sepEndDate!=null){
				DateFormat df = new SimpleDateFormat(DATE_FORMAT_FOR_NOTICE);
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, df.format(sepEndDate));
			}else{
				individualTemplateData.put(SPECIAL_ENROLLMENT_END_DATE, EMPTY_STRING);
			}
			
			individualTemplateData.put("ssapApplicationId", ssapApplicants.get(0).getSsapApplication().getId());
			
			sendToEmailList.add(household.getEmail());
			sendToEmailList.add(user.getEmail());
			
			fileName = fileName + TimeShifterUtil.currentTimeMillis() + LifeChangeEventConstant.PDF;
			String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, individualTemplateData, relativePath, fileName,
					LifeChangeEventConstant.INDIVIDUAL, individualId, sendToEmailList, exchangeName, name ,getLocation(ssapApplicants),GhixNoticeCommunicationMethod.Mail);
		} catch(Exception exception){
			LOGGER.error("Error sending notification for " + templateName);
		}
		
		LOGGER.info("Sending Notification : Ends");
	}
	
	public Set<Long> invokeEnrollmentApi(long applicationId) throws RestClientException, Exception {
		LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
		lifeChangeEventDTO.setOldApplicationId(applicationId);
		lifeChangeEventDTO.setUserName("exadmin@ghix.com");
		String responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
		lifeChangeEventDTO =   JacksonUtils.getJacksonObjectReaderForJavaType(LifeChangeEventDTO.class).readValue(responseDTO);
		Set<Long> enrolledPersons = new HashSet<Long>();
		if(lifeChangeEventDTO!=null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(lifeChangeEventDTO.getStatus())){
			enrolledPersons = lifeChangeEventDTO.getPersonIds();
		}
		return enrolledPersons;
	}
	
	private boolean onEnrolledApplication(Set<Long> enrolledPersons, SsapApplicant ssapApplicant) {
		if(enrolledPersons!= null && enrolledPersons.contains(ssapApplicant.getPersonId())){
			return true;
		}
		return false;
	}
	
	private void updateApplicationStatusAndEligiblityStatus(Long applicationId, ApplicationStatus applicationStatus, EligibilityStatus eligibilityStatus) {
		SsapApplication ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(applicationId);
		ssapApplication.setApplicationStatus(applicationStatus.getApplicationStatusCode());
		ssapApplication.setEligibilityStatus(eligibilityStatus);
		ssapApplication.setAllowEnrollment(LifeChangeEventConstant.NO);
		ssapApplicationRepository.saveAndFlush(ssapApplication);
	}
	
	private Location getLocation(List<SsapApplicant> applicants) {
		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null){
					return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null){
					return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		return null;
	}
	

	public boolean processSepDenial(Set<Long> enrolledPersons,List<SsapApplicantEvent> ssapApplicantEvents,SsapSEPIntegrationDTO ssapSEPIntegrationDTO) {
		String eventDaysInPast = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_SEP_DENIAL_DAYS);
		boolean isSepDenial = false;
		int noOfDaysInPastToDenySEP = NO_OF_DAYS_TO_DENY_SEP;
		if(eventDaysInPast != null && NumberUtils.isNumber(eventDaysInPast)){
			noOfDaysInPastToDenySEP = Integer.parseInt(eventDaysInPast);
		}
		for(SsapApplicantEvent ssapApplicantEvent : ssapApplicantEvents ){
			//ignore the dates for Demo Events 
			if(LifeChangeEventUtil.getDemographicEvents().contains(ssapApplicantEvent.getSepEvents().getName()) || (ssapApplicantEvent.getSepEvents().getName().equals(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE) && ssapSEPIntegrationDTO.getIsAddressChangeDemoEvent())){
				continue;
			}
			//get SsapApplicant for this event
			SsapApplicant applicantForTheEvents = ssapApplicantEvent.getSsapApplicant();
			//NO CHANGE are not involved in SEP Denial 
			if(ssapApplicantEvent.getEventDate() != null && (!applicantForTheEvents.getStatus().equals(LifeChangeEventConstant.Status.LCE_NO_CHANGE.toString())) && (enrolledPersons.contains(applicantForTheEvents.getPersonId()) ||((!enrolledPersons.contains(applicantForTheEvents.getPersonId())) && applicantForTheEvents.getApplyingForCoverage().equals("Y") ) )){
				Calendar cal = TSCalendar.getInstance();
				cal.setTime(ssapApplicantEvent.getEventDate());
				LifeChangeEventUtil.resetTimeInCalendar(cal);
				cal.add(Calendar.DATE, noOfDaysInPastToDenySEP);
				Calendar todaysDate = TSCalendar.getInstance();
				LifeChangeEventUtil.resetTimeInCalendar(todaysDate);
				if(todaysDate.getTimeInMillis() > cal.getTimeInMillis() ){
					isSepDenial = true;
					break;
				}
			}
		}
		
		return isSepDenial;
	}
	
	private Integer getAdminUserId(){
		return iUserRepository.findByUserName("exadmin@ghix.com").getId();
	}
}
