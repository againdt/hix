import React from 'react';

// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Utils

import normalizeNameOfPerson from '../../../../utils/normalizeNameOfPerson';

// Constants

import { THC_TYPES } from '../../../../constants/masksConstants';
import INPUT_TYPES from '../../../../constants/inputTypes';

const FilersBlock = props => {
    const {
        analyzeFamily,
        coverageYear,
        householdMembers,
        noneOfAbove,
        onFieldChange,
        ...restProps
    } = props;

    const getPotentialTaxFilersList = () => {

        return [
            ...householdMembers.map((hhm, idx) => {

                const {
                    name,
                    personId
                } = hhm;

                const {
                    taxRelationship
                } = analyzeFamily.getIndicatorsValues(personId);

                const isTaxFiler = taxRelationship === THC_TYPES.FILER || taxRelationship === THC_TYPES.FILER_DEPENDENT;

                return (
                    <UniversalInput
                        key={`${name.firstName}_${personId}`}
                        label={{
                            text: normalizeNameOfPerson(name)
                        }}
                        inputData={{
                            isInputRequired: false,
                            type: INPUT_TYPES.CHECKBOX,
                            name: 'isTaxFiler',
                            classes: [''],
                            checked: isTaxFiler,
                            value: isTaxFiler,
                            currentData: {},
                            disabled: false,
                            fields: [],
                        }}
                        inputActions={{
                            onFieldChange
                        }}
                        indexInArray={idx}
                        {...restProps}
                    />
                );
            }),
            <UniversalInput
                key={'None of Above'}
                label={{
                    text: 'None of Above'
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfAbove',
                    classes: [''],
                    checked: noneOfAbove,
                    value: noneOfAbove,
                    currentData: {},
                    fields: [],
                }}
                inputActions={{
                    onFieldChange
                }}
                {...restProps}
            />
        ];
    };

    return (
        <div className="subsection">
            <p className='required-true'>
                Who plans to file a federal income tax return for &nbsp;
                <strong>
                    { coverageYear } &nbsp;
                </strong>
                ?
            </p>
            {
                getPotentialTaxFilersList()
            }
            <p>
                You don’t have to file taxes to apply for coverage, but you will need to file next year
                if you want to get a premium tax credit to help pay for coverage now.
            </p>
        </div>
    );
};

export default FilersBlock;
