<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/utility/browser-utils.js" />"></script> 

<div id="update_comment">
	<input type="hidden" name="commentId" id="commentId" value="" />
	<input type="hidden" name="existingHtml" id="existingHtml" value="" />
</div>

	<a class="btn btn-primary showcomments"><spring:message code="label.brkaddcomments"/></a> 
	<c:url value="/platform/web/comment/controller/confirmdeletecommentPopup?commentId=" var="theurlforDeleteComment">  
	</c:url>

<div id="commentbox" style="display: none">
	<form class="form-vertical">
		<div class="profile">
			<!-- <div class="control-group">
				<label class="control-label">Your Name</label>
				<div class="controls">
					<input type="text" class="input-xlarge" name="commenter_name" id="commenter_name" maxlength="25"/>
					<div id="commenter_name_error"></div>
				</div>
			</div>	 -->	
			<div id="loading" class="txt-center" style="display:none">
				<span class="green" class="gutter10" style="display:block;"><spring:message code="label.agent.employers.SavingYourComment"/></span>
				<img  src="<%=request.getContextPath() %>/resources/img/loader_greendots.gif" width="15%" alt="green loader dots"/>
			</div>		
			<br>
			<div class="control-group">
				<label class="control-label aria-hidden" for="comment_textarea">Enter Comment Textarea</label>	
				<div class="controls">
					<textarea class="span" name="comment_text" id="comment_textarea" rows="4" cols="40" style="resize: none;" 
							maxlength="4000" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>
					<div id="comment_text_error"></div>
					<span class="pull-right hide_comment_btn">
					<input type="button" name="cancel" id="cancel" value="<spring:message code="label.brkCancel"/>" class="btn cancelComment btn-small" />&nbsp;&nbsp;
					<input type="button" name="submit_comment" id="submit_comment" value="<spring:message code="label.agent.employers.PostComment"/>" class="btn btn-primary btn-small" />
					</span>
					<span id="chars_left" class="pull-right"><spring:message code="label.agent.employers.CharactersLeft"/>&#58; <strong>4000&nbsp;</strong></span>
				</div>
			</div>
		</div>
		
	</form>	
</div>

<script type="text/javascript">

var theurlforDeleteComment = '<c:out value="${theurlforDeleteComment}"/>';

	$(document).ready(function() {
		
		$('.showcomments').click(function(){
			if ($('#commentbox').is(':hidden')) {
				$('#commentbox').show('slow', 'linear');
			}			
		});
		
		$(function() {
		    $(document).on('click', '.cancelComment', function(e) {
				$('#commentbox').hide('slow', 'linear');
				
				$('#loading').hide('slow', 'linear');
				document.getElementById("comment_textarea").value = '';
				$('#chars_left').html('Characters left <strong>4000</strong>' );
				
		    });
		});

		$(function() {
		    $(document).on('click', '.cancelEditComment', function(e) {
		    	var idToUpdate = this.id;
				if(idToUpdate != undefined  ){   
					if (idToUpdate.toLowerCase().indexOf("updatecomment") >= 0)
						{
						idToUpdate = idToUpdate.substring(13);
						}
				}
		    
				$('#loading').hide('slow', 'linear');
				document.getElementById("comment_textarea").value = '';
				$('#chars_left').html('Characters left <b>4000</b>' );
				$('#comment_text'+idToUpdate).html($('#comment_text_'+idToUpdate).html());
				
				if(isClientAppMSInternetExplorer()) {
					window.location.reload(true);
				}
		   });
		});

		$('#submit_comment').click(function(){
			
			$('#submit_comment').prop('disabled',true);
			
			//Request Parameters
			var target_name = $('#target_name').val();
			var target_id =  $('#target_id').val();
			//var commented_by =  document.getElementById("commenter_name").value;
			var comment_text =  document.getElementById("comment_textarea").value;
			
			//Validations
			var valMessage="";
			/* if($.trim(commented_by) == ''){
				valMessage = "Please enter your name";
				document.getElementById("commenter_name").value = '';
			} */
			if($.trim(comment_text) == ''){
				valMessage += '<spring:message code="label.bkr.PleaseEnterComment"/>';
				document.getElementById("comment_textarea").value = '';
				$('#submit_comment').prop('disabled',false);
			}
			if(target_name == ''){
				valMessage += "\nTarget-name missing";
			}
			if(target_id == ''){
				valMessage += "\nTarget-id missing";
			}
			if(valMessage != ''){
				//alert(valMessage);
				$('<div id="alert" class="modal"><div class="modal-body txt-center">' + valMessage + ' <br/><br/><button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button><br/></div></div>').modal();
				return false;
			}
			
			if ($('#loading').is(':hidden')) {
				$('#loading').show('slow', 'linear');
			}
			
			var  strPath =  "<%=  request.getContextPath()  %>";
			//var  pathURL=strPath+"/platform/web/comment/controller/savecomment?target_name="+target_name+"&target_id="+target_id+"&comment_text="+comment_text;
			var  pathURL=strPath+"/platform/web/comment/controller/savecomment";
			
			//$.get(pathURL, function(data) {
			//POST request for ajax call
			$.post(pathURL, {target_id : $('#target_id').val(), target_name : $('#target_name').val(), comment_text : document.getElementById("comment_textarea").value,csrftoken:'<df:csrfToken plainToken="true"/>'  }, 
					function(data) {	
				
				if(data == 'success'){
					//alert('Thank you for your comments !');
				}
				else{		
					alert('Sorry your comment could not be added');
				}
				hideCommentBoxForAddComment();
				window.location.reload(true);
				
			}).fail(function(jqXHR, textStatus, errorThrown) {
				if(jqXHR.responseText.indexOf('Missing CSRF token') != -1)	    				
					alert('Session is invalid');	    
				else
					alert('Error occured while saving comments');
				hideCommentBoxForAddComment();
			});	; 
			
		});
		
	});
	
	$(function() {
	var		valMessage;
	    $(document).on('click', '.updateComment', function(e) {
		var idToUpdate = this.id;
		if(idToUpdate != undefined){   
			if (idToUpdate.toLowerCase().indexOf("updatecomment") >= 0)
				{
				idToUpdate = idToUpdate.substring(13);
				}
		}
		
		var  strPath =  "<%=  request.getContextPath()  %>";
		//var  pathURL=strPath+"/platform/web/comment/controller/savecomment?target_name="+target_name+"&target_id="+target_id+"&comment_text="+comment_text;
		var  pathURL=strPath+"/platform/web/comment/controller/editcomment";
		
		//$.get(pathURL, function(data) {
		//POST request for ajax call
		
		if($.trim(document.getElementById('comment_text_'+idToUpdate).value) == ''){
			valMessage = "Please enter comment";
			document.getElementById('comment_text_'+idToUpdate).value = '';
			$('<div id="alert" class="modal"><div class="modal-body modal-txt-center">    <br/> ' + valMessage + ' <br/><br/><button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button><br/></div></div>').modal();
		}
		
		else{
			$.post(pathURL, {commentId : idToUpdate, comment_text : document.getElementById('comment_text_'+idToUpdate).value,csrftoken:'<df:csrfToken plainToken="true"/>'}, 
					function(data) {
				
				if(data == 'success'){
					//alert('Thanks for your comment!');
					
					$('<div id="alert1" class="modal modal-comment updateModalBox"><div class="modal-body modal-txt-center">' + '<br/><spring:message code="label.agent.employers.ThnxForComment"/>' + ' <br/><br/><button onclick="testAfterUpdateComment();" class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button><br/></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
				else{		
					alert('Sorry your comment could not be added');
				}
				hideCommentBox();
			}).fail(function(jqXHR, textStatus, errorThrown) {
				if(jqXHR.responseText.indexOf('Missing CSRF token') != -1)	    				
					alert('Session is invalid');	    
				else
					alert('Error occured while saving comments');
				$('#commentbox').hide('slow', 'linear');
				$('#loading').hide('slow', 'linear');
			});;			
			
		}

		
		});
	});


	function hideCommentBoxForAddComment(){
		$('#commentbox').hide('slow', 'linear');
		$('#loading').hide('slow', 'linear');
		document.getElementById("comment_textarea").value = '';
		$('#chars_left').html('Characters left <strong>4000</strong>' );
	}

    
    function hideCommentBox(){
    	$('#commentbox').hide('slow', 'linear');
		$('#loading').hide('slow', 'linear');
		document.getElementById("comment_textarea").value = '';
		$('#chars_left').html('Characters left <strong>4000</strong>' );
    }
	    function testAfterUpdateComment(){
	    	window.location.reload(true);
	    }
	//Update remaining characters for text area
	function updateCharCount(){		
		var tempStr= document.getElementById("comment_textarea").value;
		var currentLen = (tempStr.replace(/\n/g, "\r\n")).length;
		var maxLen = 4000; 
		var charLeft = maxLen - currentLen;
		if(charLeft<=0){
			charLeft=0;
			$('textarea[maxlength]').live('keyup blur', function() {
		        // Store the maxlength and value of the field.
		        var maxlength = $(this).attr('maxlength');
		        var val = $(this).val();

		        // Trim the field if it has content over the maxlength.
		        if (val.length > maxlength) {
		            $(this).val(val.slice(0, maxlength));
		        }
		    });

		}
		$('#chars_left').html('Characters left <strong>' + charLeft + '</strong>' );
	}
	
	function editComment(id) { //removed exisitingComment parameter
		var updateCommentTextArea = document.getElementById('comment_text_'+id);
		
		if(updateCommentTextArea == undefined ){
			$('#commentId').val(id);
			existingComment=$('#cmt'+id).val(); //access the hidden field for exisiting comment text
			if(existingComment != undefined){
				var existingHtml = $('#comment_text'+id).html();
				$('#existingHtml').val(existingHtml);
				$('#comment_text'+id).html('<textarea class="span" name="comment_text" id="comment_text_'+id+'" maxlength="4000" rows="4" cols="40" style="resize: none;">'+existingComment+'</textarea> <br/> <input type="button" name="cancel'+id+'" id="updateComment'+id+'" value="<spring:message code="label.brkCancel"/>" class="btn btn-small Cancel cancelEditComment" /> <input type="button" name="updateComment'+id+'" id="updateComment'+id+'" value="<spring:message code="label.agent.employers.UpdateComment"/>" class="btn btn-primary btn-small btn-info updateComment margin5-l" /> ');
				/* var anchorEle=document.getElementById('a'+id);
				anchorEle.removeAttribute('onClick'); */
				//$('a'+id).removeAttr('onClick');
			}
		}
		
	}
	
	function disableLink(e) {
	    // cancels the event
	    e.preventDefault();

	    return false;
	}
	
	function delComment(id) {		
		$('#commentId').val(id);
	}
	
	$(function() {
	    $(document).on('click', '.deleteComment', function(e) {
	    	var commId = $('#commentId').val();		
			var href=theurlforDeleteComment + commId;
			if (href.indexOf('#') != 0) {
		           $('<div id="confirmdeletepopup" class="modal"><div class="modal-body"><iframe id="confirmdeletepopup" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:110px;"></iframe></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
		});
	});
	
	function closeIFrame() {
		$("#confirmdeletepopup").remove();
		$(".modal-backdrop,.modal").remove();
		window.location.reload(true);
	}    
</script>
