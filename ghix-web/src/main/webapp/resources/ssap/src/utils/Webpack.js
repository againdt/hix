import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { changeData, switchConsoleMode } from '../actions/actionCreators';

import CONFIG from '../.env';

const Webpack = props => {

    const {
        console: {
            isFormValidationShown,
            isInvalidInputsShown,
            isValidatorsGeneratorResultsShown
        },
        maxAchievedPage
    } = props;

    const defaultButtonStyle = {
        padding: '3px',
        backgroundColor: '#00a6d2',
        outline: 'none',
        fontSize: '1.2rem'
    };

    const handleClick = ({target: {name}}) => {
        props.actions.switchConsoleMode({[name]: !props.console[name]});
    };

    const handleClick_user_progress = ({target: {name}}) => {
        const newData = {
            ...props.data,
            singleStreamlinedApplication: {
                ...props.data.singleStreamlinedApplication,
                maxAchievedPage: props.data.singleStreamlinedApplication.maxAchievedPage + (name === 'increase' ? 1 : -1)
            }
        };
        props.actions.changeData(newData);
    };

    return (
        <div
            className='usa-grid'
            style={{padding: 0,  margin: '0px 20px'}}
        >
            <span
                className='usa-width-one-sixth'
                style={{
                    fontWeight: 'bold',
                    fontSize: '1.0rem',
                    padding: '7px 0px'
                }}
            >
                Webpack created bundle on:
            </span >
            <span
                className='usa-width-five-sixths'
                style={{
                    fontWeight: 'bold',
                    fontSize: '1.0rem',
                    padding: '7px 0px'
                }}
            >
                <span>
                    {
                        CONFIG.ENV.DEVELOPMENT ?
                            'It is current time bundle (UI matches actual local codebase)' :
                            __SNAPSHOT__
                    }
                </span> <br/>
                <span>
                    <button
                        className='member-btn'
                        name='isValidatorsGeneratorResultsShown'
                        onClick={handleClick}
                        style={{
                            ...defaultButtonStyle,
                            backgroundColor: isValidatorsGeneratorResultsShown ? 'rgb(134, 196, 140)' : '#00a6d2'
                        }}
                    >
                        {`Generator is ${isValidatorsGeneratorResultsShown ? 'On' : 'Off'}`}
                    </button> &nbsp; &nbsp;

                    <button
                        className='member-btn'
                        name='isInvalidInputsShown'
                        onClick={handleClick}
                        style={{
                            ...defaultButtonStyle,
                            backgroundColor: isInvalidInputsShown ? 'rgb(134, 196, 140)' : '#00a6d2'
                        }}
                    >
                        {`Invalid inputs are ${isInvalidInputsShown ? 'On' : 'Off'}`}
                    </button> &nbsp; &nbsp;

                    <button
                        className='member-btn'
                        name='isFormValidationShown'
                        onClick={handleClick}
                        style={{
                            ...defaultButtonStyle,
                            backgroundColor: isFormValidationShown ? 'rgb(134, 196, 140)' : '#00a6d2'
                        }}
                    >
                        {`FormValidations are ${isFormValidationShown ? 'On' : 'Off'}`}
                    </button>

                    <span>
                        {`Max achieved Page is: ${maxAchievedPage}`}
                        <button
                            className='member-btn'
                            onClick={handleClick_user_progress}
                            name='increase'
                            style={defaultButtonStyle}
                        >
                            &nbsp; + &nbsp;
                        </button>
                        <button
                            className='member-btn'
                            onClick={handleClick_user_progress}
                            name='decrease'
                            style={defaultButtonStyle}
                        >
                            &nbsp; - &nbsp;
                        </button>
                    </span>

                </span>
            </span>
        </div>
    );
};

const mapStateToProps = state => ({
    console: state.console,

    data: state.data,

    maxAchievedPage: state
        .data
        .singleStreamlinedApplication
        .maxAchievedPage
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        switchConsoleMode
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Webpack);