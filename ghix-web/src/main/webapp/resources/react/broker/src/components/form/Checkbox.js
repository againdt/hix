import React, { Fragment } from 'react';
import classNames from 'classnames';
import InputFeedback from './InputFeedback';

// Checkbox input
const Checkbox = ({ input, meta, id, label, className, ...props }) => (
  <Fragment>
    <div className="ds-l-row">
      <div className="ds-l-col">
        <input
          name={input.name}
          id={id}
          type="checkbox"
          value={input.value}
          checked={input.value}
          onChange={input.onChange}
          onBlur={input.onBlur}
          className={classNames(className)}
          disabled={props.disabled}
        />
        <label htmlFor={id}>{label}</label>
      </div>
    </div>
    <div className="ds-l-row">
      <div className="ds-l-xl-col--12 ds-l-lg-col--12 ds-l-md-col--12 ds-l-sm-col--12 ds-l-xs-col--12">
        {meta.touched && meta.error && (
          <InputFeedback
            error={
              <div className="error help-inline">
                <span className="error">
                  <em className="excl">!</em>
                  {/*<i className="fas fa-exclamation-circle" />*/}
                  {meta.error}
                </span>
              </div>
            }
            errorClasses="ds-c-field__hint nv-label-error"
          />
        )}
      </div>
    </div>
  </Fragment>
);

export default Checkbox;
