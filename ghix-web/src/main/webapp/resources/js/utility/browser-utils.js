/**
 * Browser Utility script.
 * @version 1.0
 * @since Oct 13, 2013
 */

/**
 * Function to trim beginning and trailing spaces
 * 
 * @param rawStr The raw String
 * @returns {String} The processes String. 
 */
function trim(rawStr) {
        return rawStr.replace(/^\s+|\s+$/g,"");
}

/**
 * Function to check whether the client application is 'Microsoft Internet Explorer'.
 * 
 * @returns Boolean <code>true</code> client application is 'Microsoft Internet Explorer', 
 * 					<code>false</code> client application is not 'Microsoft Internet Explorer'.
 */
function isClientAppMSInternetExplorer() {
	var clientAppName = new String(navigator.appName);
	
	if(clientAppName.search("Microsoft") != -1 && clientAppName.search("Internet") != -1  && clientAppName.search("Explorer") != -1 ) {
		return true;
	}
	
	return false;
}