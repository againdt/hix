package com.getinsured.hix.agency.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.agency.AgencyAssistantInfoDto;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdRequestDTO;
import com.getinsured.hix.dto.planmgmt.IssuerByHIOSIdResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;

@Component
public final class AgencyUtils {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgencyUtils.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired 
	private Gson platformGson;
	@Autowired
	private BrokerMgmtUtils brokerMgmtUtils;
	@Autowired
	private DesignateService designateService;
	@Autowired 
	private CommentTargetService commentTargetService;
	
	@Autowired
	GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	public static String ECMTYPE;
	
	@Value("#{configProp['ecm.type']}")
	public void setECMTYPE(String eCMTYPE) {
		ECMTYPE = eCMTYPE;
	}
	
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	
	private static final String DOCUMENT_URL = "download/document?documentId=";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static final String SWITCH_TO_MODULE_ID = "switchToModuleId";
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final String TARGET_NAME = "AGENCY";
		
	public Long retrieveAgencyMgrForBrokerId(AccountUser user){
		
		Broker brokerObj = null;
		Long agencyId = null;
		
		if (userService.hasUserRole(user, RoleService.AGENCY_MANAGER)) {
			
			Validate.notNull(user);
			
			if (!StringUtils.isBlank(user.getRecordId())) {
				brokerObj = retrieveBrokerDetailForUser(user);
			}
			else{
				brokerObj = brokerService.findBrokerByUserId(user.getId());
			}
			
			if(brokerObj != null && user!= null){
				agencyId = brokerObj.getAgencyId();
				if(user.getActiveModuleId() == 0){
					user.setActiveModuleId(brokerObj.getId());
				}
			}
		}else if (userService.hasUserRole(user, RoleService.APPROVED_ADMIN_STAFF_L2)) {
			
			Validate.notNull(user);
			AgencyAssistantInfoDto agencyAssistantInfoDto = null;
			if (!StringUtils.isBlank(user.getRecordId())) {
				agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/information/"+user.getRecordId(), AgencyAssistantInfoDto.class);
				if(agencyAssistantInfoDto!=null){
					if(agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
						agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
					}
				}
			}
			else{
				agencyAssistantInfoDto = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_BROKER_URL+"/staff/getstaffbyuser/"+user.getId(), AgencyAssistantInfoDto.class);
				if(agencyAssistantInfoDto.getAgencyId()!=null && !agencyAssistantInfoDto.getAgencyId().isEmpty()){
					agencyId = Long.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyAssistantInfoDto.getAgencyId()));
				}
			}
		}else {
			throw new GIRuntimeException(null,"Invalid User", Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return agencyId;
	}
	
	/**
	 * Method to retrieve Broker details for broker identified by given User identifier.
	 * 
	 * @param user The current AccountUser instance.
	 * @return brokerObj The Broker instance.
	 */
	public Broker retrieveBrokerDetailForUser(AccountUser user) {
		Broker brokerObj;
		try {
			brokerObj = brokerService.getBrokerDetail(Integer.valueOf(user.getRecordId()));
		} catch (NumberFormatException nfe) {
			brokerObj = new Broker();
		}
		return brokerObj;
	}
	
	public void setModuleUserForAgency(){
		try{
			
			AccountUser user = userService.getLoggedInUser();
			
			if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) && user.getActiveModuleId() == 0){
				Broker broker =  brokerService.findBrokerByUserId(user.getId());
				if(broker != null){
					user.setActiveModuleId(broker.getId());
				}
			}
		}catch(Exception ex){
			
		}
		
	}
	
	public String getAgencyId(HttpServletRequest request) throws InvalidUserException{
		AccountUser user = userService.getLoggedInUser();
		String agencyId = null;
		
		if(ModuleUserService.AGENCY_MODULE.equalsIgnoreCase(user.getActiveModuleName()) 
				|| ModuleUserService.APPROVED_ADMIN_STAFF_L1.equalsIgnoreCase(user.getActiveModuleName()) 
				|| ModuleUserService.APPROVED_ADMIN_STAFF_L2.equalsIgnoreCase(user.getActiveModuleName())){
			if(request.getSession().getAttribute("landingAgencyId")!=null){
				agencyId = (String) request.getSession().getAttribute("landingAgencyId");
				if(agencyId!=null && !agencyId.isEmpty()){
					agencyId = ghixJasyptEncrytorUtil.decryptStringByJasypt(agencyId);
				}
			}
		}
		
		if(agencyId==null || StringUtils.isEmpty(agencyId)){
			Broker broker = brokerService.getBrokerDetail(user.getActiveModuleId());
			agencyId = String.valueOf(broker.getAgencyId());
		}
		
		return agencyId;
	}
	
	public List<String> getAgencyStatusList() {
		List<String> statusList = new ArrayList<String>();
		statusList.add("Incomplete");
		statusList.add("Pending");
		statusList.add("Suspended");
		statusList.add("Certified");
		statusList.add("Terminated");
		return statusList;
	}
	
	public void compareAgencyId(AccountUser user, Broker broker) {
		if(userService.hasUserRole(user, RoleService.AGENCY_MANAGER) && broker!=null && broker.getAgencyId()!=null) {
			long agencyId = retrieveAgencyMgrForBrokerId(user);
			if(agencyId!=broker.getAgencyId()){
				throw new AccessDeniedException(BrokerConstants.ACCESS_IS_DENIED);
			}
		}
	}
	
	public Float getFloatValue(String input) {
		Float val = null;
		if (input != null && !input.equalsIgnoreCase("NA")) {
			try {
				val = Float.valueOf(input);
			} catch (Exception ex) {
				LOGGER.error("", ex);
			}
		}
		return val;
	}
	
	public String getIssuerLogoUrl(String issuerLogo) {
		return (issuerLogo != null && !issuerLogo.isEmpty()) ? appUrl+ DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerLogo) : "";
	}
	public String getIssuerLogo(String hiosPlanId, AccountUser user) throws GIException {
		IssuerByHIOSIdResponseDTO issuerByHIOSIdResponseDTO = null;
		String issuerLogo = null;
		
		try {
			if (hiosPlanId != null ) {
				IssuerByHIOSIdRequestDTO request = new IssuerByHIOSIdRequestDTO();
				String hiosId = StringUtils.substring(hiosPlanId, 0, 5);
				request.setHiosId(hiosId);
				
				ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_INFO_BY_HIOS_ID, user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, request);
				if(response != null){
					issuerByHIOSIdResponseDTO = platformGson.fromJson(response.getBody(), IssuerByHIOSIdResponseDTO.class);
					issuerLogo = issuerByHIOSIdResponseDTO.getCompanyLogo();
				}
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while retrieving Plan Details : ", exception);
		}
		
		return issuerLogo;		
	}
	
	public boolean isUserSwitch( HttpServletRequest request){
		boolean returnValue=false;

		returnValue=(request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW) != null) && (request.getSession().getAttribute(SWITCH_TO_MODULE_ID) != null)
				&& (((String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW)).equalsIgnoreCase("Y"));

		return returnValue;
	}
	
	public void declineBrokerActiveIndividual(String brokerId, String individualId, AccountUser user) throws GIRuntimeException{
		Broker broker = null;
		DesignateBroker designateBroker;
		
		try {
			broker = brokerService.getBrokerDetail(Integer.valueOf(brokerId));
			
			LOGGER.info("Broker ID : "+brokerId+" Individual ID : "+individualId);

			if(BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)){
				designateBroker =  designateService.findBrokerByIndividualId(Integer.parseInt(individualId));
			} else {
				designateBroker =  designateService.findBrokerByExternalIndividualId(Integer.parseInt(individualId));
			}
			
			LOGGER.info("Designate Broker : "+designateBroker);
			
			if (designateBroker != null) {
				designateBroker.setStatus(Status.InActive);
				designateBroker.setUpdated(new TSDate());
				designateBroker.setUpdatedBy(user.getId());
				
				//Enrollment update is not required for CA
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					LOGGER.info("Invoking Update Enrollment");
					brokerMgmtUtils.updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getExternalIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG );
				} else {
					brokerMgmtUtils.updateEnrollmentDetails(broker, BrokerConstants.ENROLLMENT_TYPE_INDIVIDUAL, user.getUsername(),
							designateBroker.getIndividualId().longValue(), BrokerConstants.AGENT_ROLE, BrokerConstants.REMOVEACTIONFLAG );
				}

				designateService.saveDesignateBroker(designateBroker);
			} else {
				throw new GIRuntimeException("No Designation found for selected Individual");
			}
		} catch(Exception e){
			LOGGER.error("Exception occured while handling Agency decline designation request for Individual:", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	public Comment saveComments(String targetId, String commentText) {
		List<Comment> comments = null;
		AccountUser accountUser = null;
		String commenterName = "";
		CommentTarget commentTarget = null;
		
		try {
			commentTarget = commentTargetService.findByTargetIdAndTargetType(Long.valueOf(targetId), TargetName.valueOf(TARGET_NAME));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				commentTarget.setTargetId(Long.valueOf(targetId));
				commentTarget.setTargetName(TargetName.valueOf(TARGET_NAME));
			}
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			} else{
				comments = commentTarget.getComments();
			}

			accountUser = userService.getLoggedInUser();
				
			String firstName = accountUser.getFirstName();
			String lastName  = accountUser.getLastName();
			commenterName += (StringUtils.isBlank(firstName)) ? "" :  firstName+" ";
			commenterName += (StringUtils.isBlank(lastName)) ? "" : lastName;
			
			//Checking if user's name is blank
			if(StringUtils.isBlank(commenterName)){
				commenterName = "Anonymous User";
			}
			
			Comment comment = new Comment();
			comment.setComentedBy(accountUser.getId());
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTarget = commentTargetService.saveCommentTarget(commentTarget);
			List<Comment> list = commentTarget.getComments();
			Collections.sort(list);
			
			return list.get(0);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while saving comments.", ex);
			throw new GIRuntimeException("Exception occured while saving comments.", ex);
		}
	}
}
