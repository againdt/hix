package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for doc first Name
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 *
 */
public class DocFirstNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocFirstNameValidator.class);
	
	/** 
	 * Validations for doc firstName
	 * String between 1-25 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String docFirstName = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			docFirstName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(docFirstName.length() < VLPServiceConstants.DOC_FIRST_NAME_MIN_LEN
				|| docFirstName.length() > VLPServiceConstants.DOC_FIRST_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.DOC_FIRST_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
