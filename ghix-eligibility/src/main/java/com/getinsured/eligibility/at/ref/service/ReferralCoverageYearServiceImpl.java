package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralCoverageYearService")
@Scope("singleton")
public class ReferralCoverageYearServiceImpl implements ReferralCoverageYearService {

	private static final Logger LOGGER = Logger.getLogger(ReferralCoverageYearServiceImpl.class);

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Override
	@Transactional
	public int executeCoverageYear(int referralActivationId, int coverageYear) {
		int result = ReferralConstants.ERROR;
		LOGGER.debug("executeCoverageYear starts " + referralActivationId);
		final ReferralActivation referralActivation = referralActivationRepository.findOne(new Long(referralActivationId));

		if (referralActivation == null) {
			LOGGER.warn(ReferralConstants.NO_REFERRALACTIVATION_FOUND);
			return result;
		}

		final boolean blnFlag = updateCoverageYear(referralActivation, coverageYear);

		if (blnFlag) {
			updateWorkFlow(referralActivation);
			result = ReferralConstants.SUCCESS;
		}
		LOGGER.debug("executeCoverageYear ends with status " + result);
		return result;
	}

	private void updateWorkFlow(ReferralActivation referralActivation) {
		referralActivation.setWorkflowStatus(ReferralActivationEnum.RIDP_PENDING);
		referralActivation.setLastUpdatedOn(new TSDate());
		referralActivationRepository.save(referralActivation);
	}

	private boolean updateCoverageYear(ReferralActivation referralActivation, int coverageYear) {
		final SsapApplication ssapApplication = ssapApplicationRepository.findOne(new Long(referralActivation.getSsapApplicationId()));

		if (ssapApplication == null) {
			LOGGER.warn(ReferralConstants.NO_SSAP_FOUND + referralActivation.getSsapApplicationId());
			return false;
		}

		ssapApplication.setCoverageYear(coverageYear);
		ssapApplicationRepository.save(ssapApplication);
		return true;
	}

}
