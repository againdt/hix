class StateMachine {
    isRunning = false;

    constructor(stateTree, startRootState, onFinishCb = (res) => {}) {
        this.stateTree = this.initializeTree(stateTree);
        this.startRootState = startRootState;
        this.onFinishCb = onFinishCb;
    }

    run (model, config) {
        this.isRunning = true;
        let currentState = this.stateTree.states[this.startRootState];
        let historyTree = [this.startRootState];
        while(this.isRunning) {
            let foundTransition = false;
            for (let transitionId of currentState.transitions) {
                const transition = this.stateTree.transitions[transitionId];
                const condition = this.stateTree.conditions[transition.conditionId];
                if (condition(this, model, config)) {
                    const nextState = this.stateTree.states[transition.stateId];
                    nextState.onEnter(this, model);
                    currentState = nextState;
                    historyTree.push(transition.stateId);
                    foundTransition = true;
                    break;
                }
            }
            this.isRunning = foundTransition;

            if (!this.isRunning) {
                // console.log('Exiting', 'last state: ', currentState  );
            }
        }
        // console.log('!!historyTree ', historyTree);
    }

    stop(result) {
        this.isRunning = false;
        this.onFinishCb(result);
    }

    initializeTree(tree) {
        const resultStateTree = {};
        resultStateTree.states = Object.keys(tree.states).reduce((memo, stateName) => {
            const s = new State();
            memo[stateName] = Object.assign(s, tree.states[stateName]);
            return memo;
        }, {});
        resultStateTree.transitions = tree.transitions;
        resultStateTree.conditions = tree.conditions;
        return resultStateTree;
    }
}

class State {
    transitions = [];
    onEnter(sm, model) {}
}

export default StateMachine;