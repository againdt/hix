package com.getinsured.eligibility.plan.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.client.Ind19ClientUtil;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.ind19.util.service.EnrollmentsExtractionService;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto.ENROLLEE_ACTION;
import com.getinsured.hix.dto.enrollment.EnrollmentCoreDto;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.dto.enrollment.LocationCoreDto;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.enums.MaritalStatusEnum;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ObjectFactory;

// TODO: this is in progress. we are building complex and huge client.
@Component("enrollmentUpdateAdapter")
public class EnrollmentUpdateAdapter {

	private static final String AUTO_ENROLLMENT_UPDATE = "AUTO_ENROLLMENT_UPDATE";
	private static final String EXCEPTION = "Exception - ";
	private static final String NEW_LINE = "\n";

	private static final String PRIMARY_PERSON_ID = "1";
	private static final String PRIMARY_INFORMATION_NOT_FOUND = "Primary information not found!";
	private static final String NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION = "No Application Events found for application!";
	private static final String EMPTY_RESPONSE_FROM_AUTOMATE_ENROLLMENT_API = "Empty response from Automate Special enrollment API";
	private static final String UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID = "Unable to find application for ssapApplicationId - ";
	private static final String RESPONSE = "Response - ";
	private static final String REQUEST = "Request - ";
	private Logger logger = LoggerFactory.getLogger(EnrollmentUpdateAdapter.class);
	private final GsonBuilder gsonBuilder = new GsonBuilder();

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
	private EnrollmentsExtractionService enrollmentsExtractionService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private IntegrationLogService integrationLogService;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private BenchmarkPlanService benchmarkPlanService;

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired
	private CoverageStartDateService coverageStartDateService;
	
	@Autowired
	private Ind19ClientUtil ind19ClientUtil;
	
	private	final ObjectFactory objectFactory = new ObjectFactory();


	public String execute(Long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {

		Map<String, ApplicantPlanDto> healthPlanDisplayMap = planAvailabilityAdapterResponse.getHealthPlanDisplayMap();
		Map<String, ApplicantPlanDto> dentalPlanDisplayMap = planAvailabilityAdapterResponse.getDentalPlanDisplayMap();
		StringBuilder finalPayload = new StringBuilder();
		gsonBuilder.setDateFormat(ReferralConstants.JSON_DATE_FORMAT);
		Gson gson = gsonBuilder.create();
	
		String status = GhixConstants.RESPONSE_FAILURE;
		EnrollmentUpdateRequest request;
		try {
			request = formRequest(ssapApplicationId, healthPlanDisplayMap, dentalPlanDisplayMap, healthEnrollees, dentalEnrollees, planAvailabilityAdapterResponse);

			String requestJson = gson.toJson(request);

			finalPayload.append(REQUEST).append(requestJson);
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.AUTOMATE_SPECIAL_ENROLLMENT_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON,
			        String.class, requestJson);

			if (response == null || response.getBody() == null) {
				throw new GIRuntimeException(EMPTY_RESPONSE_FROM_AUTOMATE_ENROLLMENT_API);
			}
			
			String responseJson = response.getBody();
			EnrollmentResponse enrollmentResponse = gson.fromJson(responseJson, EnrollmentResponse.class);

			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);

			if (enrollmentResponse != null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus())) {
				status = GhixConstants.RESPONSE_SUCCESS;
			} else {
				/* Donot throw exception as we have persisted events */			
			}


		} catch (Exception e) {
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplicationId, finalPayload.toString(), status);
		}

		return status;

	}

	private EnrollmentUpdateRequest formRequest(Long ssapApplicationId, Map<String, ApplicantPlanDto> healthPlanDisplayMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap,
			List<String> healthEnrollees, List<String> dentalEnrollees,
			PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {

		SsapApplication currentApplication = validate(ssapApplicationId);

		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		SsapApplicant primaryApplicantDB = getPrimaryApplicantDB(currentApplication);
		HouseholdMember primaryApplicantJson = getPrimaryApplicantJson(members, primaryApplicantDB);

		if (primaryApplicantDB == null || primaryApplicantJson == null) {
			throw new GIRuntimeException(PRIMARY_INFORMATION_NOT_FOUND);
		}

		Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		if (ssapApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}

		// populate applicant event map to draw enrollment ADD, TERM, OTHER sections
		Map<String, SsapApplicantEvent> ssapApplicantEventMap = new HashMap<String, SsapApplicantEvent>();
		List<SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent ssapApplicantEvent : applicantEvents) { 
			ssapApplicantEventMap.put(ssapApplicantEvent.getSsapApplicant().getApplicantGuid(), ssapApplicantEvent);
		}

		Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.ADD.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (!ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.REMOVE.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyIneigibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyIneligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> changeEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.OTHER.name().equals(applicantEvent.getSepEvents().getChangeType())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						changeEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				changeEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}


		EnrollmentUpdateRequest request = new EnrollmentUpdateRequest();
		List<EnrollmentCoreDto> enrollmentDto = new ArrayList<EnrollmentCoreDto>();

		// HLT
		EnrollmentCoreDto healthEnrollment = populateHealthEnrollment(newlyEligibleApplicantDBRowMap, newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, newlyEligibleApplicantJsonRowMap, newlyIneigibleApplicantJsonRowMap,
		        changeEligibleApplicantJsonRowMap, healthPlanDisplayMap, primaryApplicantJson, ssapApplicantEventMap, planAvailabilityAdapterResponse, household, healthEnrollees, currentApplication);

		healthEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getHealthEnrollmentId())));
		healthEnrollment.setSsapApplicationid(ssapApplicationId);
		healthEnrollment.setCsrLevel(currentApplication.getCsrLevel());
		healthEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());
		enrollmentDto.add(healthEnrollment);

		// DLT

		if (dentalPlanDisplayMap.size() > 0) {
			EnrollmentCoreDto dentalEnrollment = populateDentalEnrollment(newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, newlyIneigibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap, dentalPlanDisplayMap,
			        primaryApplicantJson, ssapApplicantEventMap, planAvailabilityAdapterResponse, household, currentApplication, dentalEnrollees);

			dentalEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getDentalEnrollmentId())));
			dentalEnrollment.setSsapApplicationid(ssapApplicationId);
			dentalEnrollment.setCsrLevel(currentApplication.getCsrLevel());
			dentalEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());
			enrollmentDto.add(dentalEnrollment);
		}
		// add enrollments to request
		request.setEnrollmentDto(enrollmentDto);

		return request;
	}

	private EnrollmentCoreDto populateDentalEnrollment(Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap, Map<String, SsapApplicant> otherEligibleApplicantDBRowMap, Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap,
	        Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap, HouseholdMember primaryApplicantJson, Map<String, SsapApplicantEvent> applicantEventMap,
	        PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, Household household, SsapApplication currentApplication, List<String> dentalEnrollees) {
		EnrollmentCoreDto dentalEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> dentalEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);

		ApplicantPlanDto memberPlan = null;
		// DO NOT automate add members to DENTAL

		// remove members
		for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
			if (dentalEnrollees.contains(newIneligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
	
				SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
				HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
	
				memberPlan = dentalPlanDisplayMap.get(newIneligibleApplicantGuid);
	
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				DateTime terminationDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate());
				if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {
					terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
				}
				enrolleeCoreDto.setEffectiveEndDate(TSDate.getNoOffsetTSDate(terminationDate.getMillis()));
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		// other eligible members
		for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
			if (dentalEnrollees.contains(otherEligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
				SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
				HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
				memberPlan = dentalPlanDisplayMap.get(otherEligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(otherEligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication,primaryApplicantJson));
	
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
				dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		if (memberPlan == null) {
			// this should never happen!
			throw new GIRuntimeException("No new dental plan information found!");
		}

		dentalEnrollment.setEnrolleeCoreDtoList(dentalEnrolleeCoreDtoList);
		dentalEnrollment.setAptcAmt(memberPlan.getPlanLevelMonthlySubsidy());
		dentalEnrollment.setPlanId(memberPlan.getPlanId().intValue());
		dentalEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
		dentalEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());

		return dentalEnrollment;
	}

	private EnrollmentCoreDto populateHealthEnrollment(Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap, Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap, Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
	        Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap, Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap, Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap,
	        Map<String, ApplicantPlanDto> healthPlanDisplayMap, HouseholdMember primaryApplicantJson, Map<String, SsapApplicantEvent> applicantEventMap, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, Household household,
	        List<String> healthEnrollees, SsapApplication currentApplication) {

		EnrollmentCoreDto healthEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> healthEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		ApplicantPlanDto memberPlan = null;
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);
		
		// add members
		for (String newApplicantGuid : newlyEligibleApplicantDBRowMap.keySet()) {
			EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();

			SsapApplicant memberDB = newlyEligibleApplicantDBRowMap.get(newApplicantGuid);
			HouseholdMember memberJson = newlyEligibleApplicantJsonRowMap.get(newApplicantGuid);
			memberPlan = healthPlanDisplayMap.get(newApplicantGuid);
			SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newApplicantGuid);

			populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.ADD);

			enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication ,primaryApplicantJson));

			enrolleeCoreDto.setEffectiveStartDate(planAvailabilityAdapterResponse.getCoverageStartDate());
			enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.ADD);

			healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
		}

		// remove members
		for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(newIneligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
	
				SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
				HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
				memberPlan = healthPlanDisplayMap.get(newIneligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant ,EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
				DateTime terminationDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate());
				if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {
					terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
				}
				enrolleeCoreDto.setEffectiveEndDate(TSDate.getNoOffsetTSDate(terminationDate.getMillis()));
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		// other eligible members
		for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(otherEligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
				SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
				HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
				memberPlan = healthPlanDisplayMap.get(otherEligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(otherEligibleApplicantGuid);

				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication,primaryApplicantJson));
	
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		if (memberPlan == null) {
			// this should never happen!
			throw new GIRuntimeException("No new health plan information found!");
		}

		healthEnrollment.setEnrolleeCoreDtoList(healthEnrolleeCoreDtoList);
		healthEnrollment.setAptcAmt(memberPlan.getPlanLevelMonthlySubsidy());
		healthEnrollment.setPlanId(memberPlan.getPlanId().intValue());
		healthEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
		healthEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());

		return healthEnrollment;
	}

	private String populateSubscriberFlag(SsapApplicant memberDB, Map<String, ApplicantPlanDto> applicantPlanMap) {
		String subscriberFlag = "N";
		if (applicantPlanMap.containsKey(memberDB.getApplicantGuid())) {
			ApplicantPlanDto applicantPlanDto = applicantPlanMap.get(memberDB.getApplicantGuid());
			subscriberFlag = applicantPlanDto.getSubscriberFlag().name();
		}
		
		return subscriberFlag;
	}

	private void populateEnrolleeCoreDto(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB, HouseholdMember memberJson, ApplicantPlanDto memberPlan, HouseholdMember primaryApplicantJson, SsapApplicantEvent ssapApplicantEvent,
	        Household household, SsapApplication currentApplication, Map<String, ApplicantPlanDto> planMap, SsapApplicant primaryApplicant, ENROLLEE_ACTION action) {

		enrolleeCoreDto.setExchgIndivIdentifier(memberDB.getApplicantGuid());
		enrolleeCoreDto.setTaxIdNumber(memberDB.getSsn());
		
		enrolleeCoreDto.setLastName(memberDB.getLastName());
		enrolleeCoreDto.setFirstName(memberDB.getFirstName());
		enrolleeCoreDto.setMiddleName(memberDB.getMiddleName());
		enrolleeCoreDto.setSuffix(memberDB.getNameSuffix());
		enrolleeCoreDto.setBirthDate(memberDB.getBirthDate());

		enrolleeCoreDto.setMaintainenceReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());

		String primaryPhone = null, secondaryPhone = null;
		if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) && !StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
			secondaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		}

		if (StringUtils.isEmpty(primaryPhone)) {
			primaryPhone = household.getPhoneNumber();
		}
		enrolleeCoreDto.setPrimaryPhoneNo(primaryPhone);
		enrolleeCoreDto.setSecondaryPhoneNo(secondaryPhone);

		if (EnrolleeCoreDto.ENROLLEE_ACTION.TERM != action) {

			enrolleeCoreDto.setRatingArea(String.valueOf(memberPlan.getRegion()));
			enrolleeCoreDto.setTotalIndvResponsibilityAmt(memberPlan.getPremium());

			enrolleeCoreDto.setRaceMap(populateRaceDetails(memberJson));

			String relationshipToHCPLkpStr = populateRelationshipToHCP(memberJson, "" + memberDB.getPersonId(), primaryApplicantJson);
			enrolleeCoreDto.setRelationToHCPLkpStr(relationshipToHCPLkpStr);

			populateLocationDetails(enrolleeCoreDto, memberJson, primaryApplicantJson);
			String languageWrittenLkpStr = languageWritten(memberJson);
			

			String languageSpokenLkpStr = languageSpoken(memberJson);
			enrolleeCoreDto.setLanguageSpokenLkpStr(languageSpokenLkpStr);
			enrolleeCoreDto.setLanguageWrittenLkpStr(languageWrittenLkpStr);
		}
		populateTobaccoUsageDetails(enrolleeCoreDto, memberDB);

		//boolean citizenshipIndicator = memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null ? memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator().booleanValue() : false;
		
		// populateCitizenshipDetails(enrolleeCoreDto);

		populateMaritalStatus(enrolleeCoreDto, memberDB);

		populateGenderDetails(enrolleeCoreDto, memberDB);
		
		
		enrolleeCoreDto.setSubscriberNewFlag(populateSubscriberFlag(memberDB, planMap));
		
		
	}

	private void populateMaritalStatus(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String maritalStatusLkpStr = MaritalStatusEnum.value(memberDB.getMarried());
		if ("Yes".equalsIgnoreCase(memberDB.getMarried()) || "true".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "Married";
		}else if("No".equalsIgnoreCase(memberDB.getMarried()) || "false".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "Single";
		}
		enrolleeCoreDto.setMaritalStatusLkpStr(maritalStatusLkpStr);
	}

	private String populateRelationshipToHCP(HouseholdMember memberJson, String personId, HouseholdMember primaryApplicantJson) {
		String relatioshipToHCPStr = null;

		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId()) && String.valueOf(PRIMARY_PERSON_ID).equalsIgnoreCase(bloodRelationship.getRelatedPersonId())) {
				relatioshipToHCPStr = bloodRelationship.getRelation();
			}
		}
		return relatioshipToHCPStr;
	}

	private String languageSpoken(HouseholdMember memberJson) {
		String languageSpoken = null;
		String languageSpokenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageSpoken = memberJson.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
		}
		if(StringUtils.isNotBlank(languageSpoken)) {
			languageSpokenCode = LanguageEnum.getCode(languageSpoken);								
		}
		return languageSpokenCode;
	}

	private String languageWritten(HouseholdMember memberJson) {
		String languageWritten = null;
		String languageWrittenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageWritten = memberJson.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
		}
		if(StringUtils.isNotBlank(languageWritten)) {
			languageWrittenCode = LanguageEnum.getCode(languageWritten);								
		}
		return languageWrittenCode;
	}

	private void populateGenderDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String genderLkpStr = "Male".equalsIgnoreCase(memberDB.getGender()) ? "M" : "F";
		enrolleeCoreDto.setGenderLkpStr(genderLkpStr);
	}

	private void populateLocationDetails(EnrolleeCoreDto enrolleeCoreDto, HouseholdMember memberJson, HouseholdMember primaryApplicantJson) {

		Address homeAddress = memberJson.getHouseholdContact().getHomeAddress();

		Address mailingAddress = memberJson.getHouseholdContact().getMailingAddress();

		enrolleeCoreDto.setHomeAddressDto(populateHomeAddress(homeAddress, memberJson, primaryApplicantJson));
		enrolleeCoreDto.setMailingAddressDto(populateAddress(mailingAddress));
	}
	
	private LocationCoreDto populateHomeAddress(Address address, HouseholdMember memberJson, HouseholdMember primaryApplicantJson) {
		LocationCoreDto locationCoreDto = null;
		if (address != null) {
			locationCoreDto = new LocationCoreDto();
			locationCoreDto.setAddress1(address.getStreetAddress1());
			locationCoreDto.setAddress2(address.getStreetAddress2());
			locationCoreDto.setCity(address.getCity());
			locationCoreDto.setState(address.getState());
			locationCoreDto.setZip(address.getPostalCode());
			locationCoreDto.setCountycode(getCounty(memberJson, primaryApplicantJson));

		}
		return locationCoreDto;
	}
	
	private String getCounty(HouseholdMember applicantJsonRow, HouseholdMember primaryApplicantJson) {
		String countyCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}

	private void populateTobaccoUsageDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String tobaccoUsage = "U";
		if (memberDB.getTobaccouser() != null) {
			tobaccoUsage = StringUtils.equalsIgnoreCase(ReferralConstants.Y, memberDB.getTobaccouser()) ? "T" : "N";
		}

		enrolleeCoreDto.setTobaccoUsageLkpStr(tobaccoUsage);
	}


	private LocationCoreDto populateAddress(Address address) {
		LocationCoreDto locationCoreDto = null;
		if (address != null) {
			locationCoreDto = new LocationCoreDto();
			locationCoreDto.setAddress1(address.getStreetAddress1());
			locationCoreDto.setAddress2(address.getStreetAddress2());
			locationCoreDto.setCity(address.getCity());
			locationCoreDto.setState(address.getState());
			locationCoreDto.setZip(address.getPostalCode());
			locationCoreDto.setCountycode(address.getCountyCode());

		}
		return locationCoreDto;
	}

	private Map<String, String> populateRaceDetails(HouseholdMember memberJson) {

		EthnicityAndRace ethnicityAndRace = memberJson.getEthnicityAndRace();
		Map<String, String> raceMap = null;
		if (ethnicityAndRace != null && ReferralUtil.listSize(ethnicityAndRace.getRace()) != 0) {

			raceMap = new HashMap<String, String>();
			for (Race race : ethnicityAndRace.getRace()) {
				raceMap.put(race.getCode(), race.getLabel() != null ? race.getLabel() : race.getOtherLabel());
			}

		}

		return raceMap;
	}

	private List<Map<String, String>> populateRelationship(HouseholdMember memberJson, String personId, SsapApplication currentApplication, HouseholdMember primaryApplicantJson) {
		List<Map<String, String>> relatioshipMapList = new ArrayList<Map<String, String>>();
		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		Map<String, String> applicantGuidMapwithPersonId = populateApplicantGuidWithPersonId(currentApplication.getSsapApplicants());

		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId())) {
				Map<String, String> relationshipMap = new HashMap<String, String>();
				relationshipMap.put("relationshipCode", bloodRelationship.getRelation());
				relationshipMap.put("memberId", applicantGuidMapwithPersonId.get(bloodRelationship.getRelatedPersonId()));
				relatioshipMapList.add(relationshipMap);
			}
		}

		return relatioshipMapList;
	}

	private Map<String, String> populateApplicantGuidWithPersonId(List<SsapApplicant> ssapApplicants) {
		Map<String, String> applicantGuidMapwithPersonId = new HashMap<String, String>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidMapwithPersonId.put(String.valueOf(ssapApplicant.getPersonId()), ssapApplicant.getApplicantGuid());
		}
		return applicantGuidMapwithPersonId;

	}

	private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);

		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);

		if (currentApplicationEvent == null) {
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}

	private HouseholdMember getPrimaryApplicantJson(List<HouseholdMember> members, SsapApplicant primaryApplicantDB) {
		HouseholdMember primaryApplicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) primaryApplicantDB.getPersonId()) == 0) {
				primaryApplicantJson = householdMember;
				break;
			}
		}
		return primaryApplicantJson;
	}

	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.getPersonId() == 1) {
				primaryApplicantDB = applicant;
				break;
			}
		}
		return primaryApplicantDB;
	}

	

	private void logData(Long ssapApplicationId, String message, String status) {
		integrationLogService.save(message, AUTO_ENROLLMENT_UPDATE, status, ssapApplicationId, null);
	}
	
	//Coverage Start date HIX-105146
	public Date getCoverageStartDate(SsapApplication currentApplication,SsapApplicationEvent ssapApplicationEvent) {
			
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
			Date coverageStartDate = null;
					
			try {
				SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
				if(enrolledApplication == null){
					// If EN/PN application is not found check if only dental enrolled application is found.
					enrolledApplication = ssapApplicationRepository.findOnlyDentalEnrolledSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear(),currentApplication.getId());
				}
				if(enrolledApplication != null) {
					enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveMedicalAndDentalEnrollments(enrolledApplication.getId());
				}
			    /*if(enrollmentsByTypeMap==null || enrollmentsByTypeMap.isEmpty()){
			    	throw new GIRuntimeException("Existing enrollments could not be retrieved for the enrolled application with casenumber "+enrolledApplication.getCaseNumber());
			    }*/
			    coverageStartDate = coverageStartDateService.computeCoverageStartDate(BigDecimal.valueOf(currentApplication.getId()),enrollmentsByTypeMap,false,false);
			    IndividualInformationRequest.Household.DisenrollMembers disenrollMembers = 
						objectFactory.createIndividualInformationRequestHouseholdDisenrollMembers();
				if(ssapApplicationEvent != null){
					Date newCoverageStartDate = ind19ClientUtil.getDisenrolledMembers(disenrollMembers, ssapApplicationEvent.filteredPreferredSsapApplicantEvents(), enrollmentsByTypeMap,coverageStartDate);
					if(newCoverageStartDate!=null){
						coverageStartDate=newCoverageStartDate;
					}
				}
				LocalDate coverageDate = new LocalDate(coverageStartDate.getTime());
				if(coverageDate.getYear() > currentApplication.getCoverageYear()){
					throw new GIException("Enrollment start date spills to next year. Start Date: " + coverageDate +", Coverage Year: " + currentApplication.getCoverageYear());
				}
			}
			catch(GIException e) {
				logger.error("Error finding the coverage start date", e);
				throw new GIRuntimeException("Coverstartdate could not be calculated for the SEP application with casenumber "+currentApplication.getCaseNumber());
			}
			
			return coverageStartDate;
	}
}
