

import { THC_TYPES } from '../constants/masksConstants';

class ConditionResolver {
    constructor(config, caller){

        this.undefValues = new Set();

        this.config = Object.assign(
            {},
            ...Object.entries(config).map(entry => {
                const [ key, value ] = entry;
                if(value === undefined) {
                    this.undefValues.add({
                        name: key,
                        criteria: value
                    });
                    return {};
                } else {
                    return {[key]: value};
                }
            }));

        this.caller = caller;

        /**
         *  Different conditions
         */

        this.isAbleToClaimDependent = {
            type: 'AND',
            condition_1: this.config.claimingDependents,
            condition_2: this.config.taxFiler,
            condition_3: this.config.taxRelationship === THC_TYPES.FILER,
            condition_4: this.config.taxDependent === false
        };

        this.isNoSpouse = {
            type: 'AND',
            condition_1: this.config.married === false,
            condition_2: this.config.spouseIndex === null
        };

        this.isSpouseProvided = {
            type: 'AND',
            condition_1: this.config.married,
            condition_2: this.config.spouseIndex !== null
        };

        this.isSpouseProvidedButRejected = {
            type: 'AND',
            condition_1: this.config.married === false,
            condition_2: this.config.spouseIndex !== null
        };

        this.isSpouseProvidedAndConfirmedByUser = {
            type: 'AND',
            condition_1: this.isSpouseProvided,
            condition_2: this.config.spouseMatch
        };

        this.isSpouseConfirmed = {
            type: 'OR',
            condition_1: {
                type: 'AND',
                condition_1_1: this.isSpouseProvidedAndConfirmedByUser,
                condition_1_2: this.config.isApplicationFiler
            },
            condition_2: {
                type:'AND',
                condition_2_1: this.isSpouseProvidedAndConfirmedByUser,
                condition_2_2: this.config.applyingForCoverageIndicator,
                condition_2_3: this.config.isOlderThanMarriageAge
            }
        };




        /**
         *  Questions:
         *
         *  1. Is [Person name] married?
         *
         *  2. Is [Spouse name] spouse of [Person name]?
         *
         *  3. Does [Person name] live with [his/her] spouse?
         *
         *  4. Does [Person name] plan to file federal income tax return for [coverage year]?
         *
         *  5. Does [Person name] plan to file joint federal income tax return
         *     with [his/her] spouse for [coverage year]?
         *
         *  6. Will [Person name] and [Spouse name <if married amd filing jointly>] claim
         *     any dependent on [his/her/their joint] federal income tax return for [coverage year]?
         *
         *  7. Who are [Person name] and [Spouse name <if married amd filing jointly>] dependents?
         *
         *  8. Will [Person name] be claimed as a dependent on someone's else tax return for [coverage year]?
         *
         */


        this.isQuestion_1_shown = {
            type: 'OR',
            condition_0: this.config.questionOneHasBeenResolved,
            condition_1: {
                type: 'AND',
                condition_1: {
                    type: 'OR',
                    condition_1_1: this.config.isApplicationFiler,
                    condition_1_2: {
                        type: 'AND',
                        condition_1_2_1: this.config.applyingForCoverageIndicator,
                        condition_1_2_2: {
                            type: 'OR',
                            condition_1_2_2_1: this.config.isMarriedDefined,
                            condition_1_2_2_2: this.config.married === null
                        }
                    }
                },
                condition_3: this.config.isOlderThanMarriageAge
            }
        };

        this.isQuestion_2_shown = {
            type: 'AND',
            condition_1: this.isQuestion_1_shown,
            condition_2: this.config.married === true,
            condition_3: this.config.spouseIndex !== null
        };

        this.isQuestion_3_shown = {
            type: 'AND',
            condition_1: this.isQuestion_2_shown,
            condition_2: this.config.spouseMatch === true
        };

        this.isQuestion_4_shown = {
            type: 'OR',
            condition_0: this.config.questionTwoHasBeenResolved,
            condition_1: {
                type: 'AND',
                condition_1: {
                    type: 'OR',
                    condition_1_1: this.config.isApplicationFiler,
                    condition_1_2: this.config.taxDependent !== true
                },
                condition_2: {
                    type: 'OR',
                    condition_2_1: {
                        type: 'AND',
                        condition_2_1_1: this.isNoSpouse,
                        condition_2_1_2: this.config.married === false
                    },
                    condition_2_2: this.config.livesWithSpouse !== null
                },
                condition_3: {
                    type: 'OR',
                    condition_3_1: this.config.isTaxFilerDefined,
                    condition_3_2: this.config.taxFiler === null
                }
            }
        };

        this.isQuestion_5_shown = {
            type: 'AND',
            condition_1: this.isQuestion_4_shown,
            condition_2: this.config.taxFiler === true,
            condition_3: this.config.married === true
        };

        this.isQuestion_6_shown = {
            type: 'AND',
            condition_1: {
                type: 'AND',
                condition_1_1: this.config.taxFiler,
                condition_1_2: this.isQuestion_4_shown,
            },
            condition_2: {
                type: 'OR',
                condition_2_1: this.isNoSpouse,
                condition_2_2: this.config.taxFilingStatus !== 'UNSPECIFIED'
            },
        };

        this.isQuestion_7_shown = {
            type: 'AND',
            condition_1: this.isQuestion_6_shown,
            condition_2: this.config.claimingDependents
        };

        this.isQuestion_8_shown = {
            type: 'AND',
            condition_1: this.config.isApplicationFiler === false,
            condition_2: this.config.isOlderThanChildAge === false,
            condition_3: this.config.taxFiler !== true,
            condition_4: this.config.taxDependent !== true
        };

        this.resolve = this.resolve.bind(this);
    }

    resolve(condition, name) {
        let output = [];
        Object.entries(condition).forEach(([ conditionName, conditionValue ]) => {
            if(conditionValue === undefined) {
                this.undefValues.add({
                    name,
                    criteria: conditionName
                });
            }
            if(conditionName !== 'type') {
                if(conditionValue instanceof Object) {
                    output.push(this.resolve(conditionValue, name));
                } else {
                    output.push(conditionValue);
                }
            }
        });
        switch(condition.type) {
        case 'AND':
            return output.every(v => v);
        case 'OR':
            return output.some(v => v);
        }
    }

    getResolvedConditions(neededValues = []) {

        const valuesAreAbleToBeResolved = [
            'isAbleToClaimDependent',
            'isNoSpouse',
            'isSpouseConfirmed',
            'isSpouseProvided',
            'isSpouseProvidedAndConfirmedByUser',
            'isSpouseProvidedButRejected',

            'isQuestion_1_shown',
            'isQuestion_2_shown',
            'isQuestion_3_shown',
            'isQuestion_4_shown',
            'isQuestion_5_shown',
            'isQuestion_6_shown',
            'isQuestion_7_shown',
            'isQuestion_8_shown'
        ];

        const output = Object.assign(
            {},
            ...valuesAreAbleToBeResolved.map(value => ({
                [value]: this.resolve(this[value], value)
            }))
        );

        const areConditionsCannotBeResolved = neededValues.some(neededValue =>
            !![...this.undefValues]
                .find(v => v.name === neededValue)
        );

        this.undefValues.size > 0 && areConditionsCannotBeResolved && console.log(`This caller ${this.caller} cannot resolve: `, [...this.undefValues]);
        return output;
    }

    getConfig() {
        return this.config;
    }

    getUndefConfigValues() {
        return this.undefValues;
    }
}

export default ConditionResolver;