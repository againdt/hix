import React, { Fragment } from 'react';

/**
 * This Component creates Header (with main and additional text) for every page
 */
const CommonHeader = props => {

    const {
        headerText,
        additionalElement,
        additionalElementClassName,
        additionalElementStyle,
        additionalText
    } = props;

    return (
        <Fragment>
            <h1>
                {headerText}
                {
                    additionalText &&
                    <span className='header-additional-text'>
                        {additionalText}
                    </span>
                }

            </h1>
            {
                additionalElement && (
                    <span
                        className={additionalElementClassName}
                        style={additionalElementStyle}
                    >
                        {additionalElement}
                    </span>
                )
            }
        </Fragment>
    );
};

export default CommonHeader;