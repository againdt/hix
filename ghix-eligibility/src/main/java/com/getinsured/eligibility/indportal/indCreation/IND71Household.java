package com.getinsured.eligibility.indportal.indCreation;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ObjectFactory;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.ssap.model.SsapApplication;

@Service("IND71Household")
public class IND71Household implements IHousehold{
	private final ObjectFactory factory = new ObjectFactory();
	private static final Logger LOGGER = Logger.getLogger(IND71Household.class);
	
	@Override
	public Object createResponsiblePerson(HouseholdData householdData) {
		ResponsiblePerson responsiblePerson = factory.createAutoRenewalRequestHouseholdResponsiblePerson();
		
		return responsiblePerson;
	}

	@Override
	public Object createHouseHoldContact(HouseholdData householdData) {
		HouseHoldContact houseHoldContact = factory.createAutoRenewalRequestHouseholdHouseHoldContact();;
		
		return houseHoldContact;
	}

	@Override
	public Object createIndividual(HouseholdData householdData, SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object createMembers(Object membersObject, SsapApplication ssapApplication,
			Ind19ClientRequest ind19ClientRequest, HouseholdData householdData, String userName, List<String> memberGUIDs) throws GIException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object createDisenrollMembers(Ind19ClientRequest ind19ClientRequest, HouseholdData householdData){
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void setHouseholdData(SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest,
			HouseholdData householdData) throws GIException {
		// TODO Auto-generated method stub
		
	}	

}
