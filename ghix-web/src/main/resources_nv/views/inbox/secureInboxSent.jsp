<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table id="inbox" class="table table-condensed table-border-none">
	<thead>
		<tr class="graydrkbg">
			<th class="graydrkbg header">
				<div class="control-group">
					<div class="controls">
						<label class="checkbox"><input type="checkbox" class="checkall" style="padding-left:18px;"></label>
					</div>
				</div>
			</th>
			<th class="header"><i class="icon-envelope icon-white"></i></th>
			<th class="sortable"><dl:sort title="To" sortBy="toUserNameList" sortOrder="${sortOrder}"></dl:sort></th>
			<th class="sortable"><dl:sort title="Subject" sortBy="msgSub" sortOrder="${sortOrder}"></dl:sort></th>
			<th class="sortable span2"><dl:sort title="Date" sortBy="dateCreated" sortOrder="${sortOrder}"></dl:sort></th>
			<th class="header"><i class="icon-attachment icon-white"></i></th>
			<th  class="span1"></th>
		</tr>
	</thead>
	<tbody class="msg">
		<c:choose>
			<%-- <c:when test="${inboxMsgList != null && !inboxMsgList.isEmpty()}"> --%>
			<c:when test="${inboxMsgList != null}">
				<c:forEach var="inboxMsg" items="${inboxMsgList}">
					<tr class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
						<td>
							<div class="control-group">
								<div class="controls">
									<label class="checkbox"><input type="checkbox" class="checkbox" value="${inboxMsg.id}"></label>
								</div>
							</div>
						</td>
						<td>
							<c:choose>
								<c:when test="${inboxMsg.status == 'S'}"><i class='icon-envelope-read' title="Read"></i></c:when>
								<c:when test="${inboxMsg.status == 'P'}"><i class='icon-reply' title="This message is a Reply"></i></c:when>
								<c:when test="${inboxMsg.status == 'F'}"><i class="icon-reply" title="This message is a Forward" style="background-position: -48px -528px;"></i></c:when>
							</c:choose>
							</td>
						<td class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
							<a href="javascript: doReadSubmit(${inboxMsg.id});"><i class="icon-person"></i>${inboxMsg.toUserNameList}</a>
						</td>
						<td><a href="javascript: doReadSubmit(${inboxMsg.id});">${inboxMsg.msgSub}</a></td>
						<td>
							<jsp:useBean id="now" class="java.util.Date"/>
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="MMM dd, yyyy" var="dateCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="MMM dd, yyyy" var="dateNow" />
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="yyyy" var="yearCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="yyyy" var="yearNow" />
							<c:choose>
								<c:when test="${dateCreated eq dateNow}">
									<fmt:formatDate type="TIME" value="${inboxMsg.dateCreated}" pattern="hh:mm a"/>
								</c:when>
								<c:when test="${yearCreated eq yearNow}">
									<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="EEE MMM dd"/>
								</c:when>
								<c:otherwise>
									${dateCreated}
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<i class="attachment-icon">
								<i class="${inboxMsg.messageDocs == null || empty inboxMsg.messageDocs ? '': 'icon-attachment'}"></i>
							</i>
						</td>
						<td>
							<div class="dropdown">
								<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
								<ul class="pull-right dropdown-menu " role="menu" aria-labelledby="dLabel">
									<li><a href="javascript: singleAction('Archive', '${inboxMsg.id}');"><i class="icon-archive"></i> Archive</a></li>
								</ul>
							</div>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr class="read">
					<td colspan="7"><spring:message code='label.norecords' /></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
<div>
	<dl:paginate resultSize="${totalRecords + 0}" pageSize="${pageSize + 0}" firstAndLastView="true" hideTotal="true" />
</div>