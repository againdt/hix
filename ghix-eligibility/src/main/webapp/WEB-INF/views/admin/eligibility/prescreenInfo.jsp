<%@page import="com.getinsured.eligibility.prescreen.util.PrescreenInfoRec"%>
<%@page import="com.getinsured.eligibility.prescreen.util.PrescreenInfo"%>
<%@page import="com.getinsured.hix.platform.startup.*"%>
<%@include file="datasource.jsp"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<link rel="stylesheet" href="<c:url value='/resources/css/styles.css'/>"/>

<%! private PrescreenInfo prescreenInfo = null; %>
<%
	if(request.getSession().getAttribute("prescreenRecords") == null){
		prescreenInfo = (PrescreenInfo) ctx.getBean("prescreenBean");
		List<PrescreenInfoRec> records = prescreenInfo.getPrescreenInfo();
		request.getSession().setAttribute("prescreenRecords", records );
	}
%>
<h3>Prescreen Information</h3>
<display:table name="${sessionScope.prescreenRecords}"  requestURI="prescreenInfo" id="table" style="white-space: pre-wrap;width: 100%;" export="true" pagesize="1000" defaultsort="1"> 
	<display:column property="group" title="Group" sortable="true" />
	<display:column property="attribute" title="Attribute" sortable="true" />
	<display:column property="value" title="Value" sortable="true" />
</display:table>

