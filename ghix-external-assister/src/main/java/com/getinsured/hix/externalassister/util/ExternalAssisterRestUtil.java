package com.getinsured.hix.externalassister.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;

import static com.getinsured.hix.externalassister.util.ExternalBrokerUtils.isNotNullAndEmpty;
import com.google.gson.Gson;


@Component
public class ExternalAssisterRestUtil {

	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalAssisterRestUtil.class);
	private GhixRestTemplate ghixRestTemplate;
	private Gson platformGson;

	@Autowired
	public ExternalAssisterRestUtil(GhixRestTemplate ghixRestTemplate, Gson platformGson) {
		this.ghixRestTemplate = ghixRestTemplate;
		this.platformGson = platformGson;
	}

	/**
	 * Jira - HIX-111554
	 * update enrollment details
	 * @author Anoop
	 * @since 02-04-2019
	 * @param cmrHouseholdId
	 * @return enrollmentResponse
	 */
	public EnrollmentResponse updateEnrollmentDetails(EnrollmentRequest enrollmentRequest) throws GIException {
		String enrollmentResponse = null;
		if (isNotNullAndEmpty(enrollmentRequest)) {
			try {
				enrollmentResponse = ghixRestTemplate.postForObject(
						EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS, enrollmentRequest, String.class);
			} catch (Exception e) {
				LOGGER.error("Exception occured in Rest call for UPDATE_ENROLLMENT_BROKER_DETAILS: ", e);
				throw new GIException(e);
			}
			return platformGson.fromJson(enrollmentResponse, EnrollmentResponse.class);
		}
		return null;
	}
}
