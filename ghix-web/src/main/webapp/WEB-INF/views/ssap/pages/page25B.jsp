<%@ include file="../include.jsp" %>
<div id="appscr75B" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages"><spring:message code="ssap.page25AandB.additionalQuestions" text="Additional Questions"/></h4>
		</div>
		
		<div class="container-fluid">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page25AandB.somethingAboutYourFamily" text="We need to know a few more things about you and your family to make sure we are matching you accurately with the best available programs "/> 
				</p>
				<label>
					<em>
						<spring:message code="ssap.page25AandB.mandatoryFields" text="All fields on this Additional Questions section are required unless otherwise indicated."/>  
					</em>
				</label>
				<hr>
			</div>
			
			<div class="control-group margin30-t">
				<label class="span9">
					<img  alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
					<spring:message code="ssap.page25AandB.estimateTime" text=" Estimated time for this section:"/>
					<strong><spring:message code="ssap.page25A.aMinuteAndFiveMinutes" text="1 Minutes - 5 Minutes"/></strong>
				</label>
				<img src="<c:url value="/resources/images/icons/2button.png" />" class="call-us-img pull-right">
			</div>
			
		</div>
	</div>
</div>