package com.getinsured.eligibility.ind19.util.service;

import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.platform.util.exception.GIException;

public interface EnrollmentsExtractionService {

	Map<String, EnrollmentShopDTO> extractActiveMedicalAndDentalEnrollments(Long ssapApplicationId) throws GIException;

	Map<String, EnrollmentShopDTO> extractActiveEnrollmentsForApplicants(Long ssapApplicationId,
			List<String> applicationGUIDs) throws GIException;
}
