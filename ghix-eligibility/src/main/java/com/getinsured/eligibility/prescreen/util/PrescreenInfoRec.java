/**
 * 
 */
package com.getinsured.eligibility.prescreen.util;

/**
 * Record structure for Prescreen Information records
 * @author Nikhil Talreja
 * @since 30 July, 2013
 */
public class PrescreenInfoRec {
	
	private String group;
	private String attribute;
	private String value;
	
	public PrescreenInfoRec(String group,String attribute,String value){
		this.group = group;
		this.attribute = attribute;
		this.value = value;
	}
	
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public String getAttribute() {
		return attribute;
	}
	

	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	@Override
	public String toString() {
		return "[group=" + group + ", attribute=" + attribute
				+ ", value=" + value + "]";
	}
	
}
