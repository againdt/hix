package com.getinsured.hix.indportal;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TSLocalTime;

import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.getinsured.eligibility.startdates.dto.EligibilityDatesOverrideDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.indportal.dto.ApplicantEventsDTO;
import com.getinsured.hix.indportal.dto.PlanSummaryDetails;
import com.getinsured.hix.indportal.dto.SEPOverrideDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

/**
 * 
 * @author Sunil Desu
 *
 */
public class IndividualPortalCommons {

	@Autowired
	public GhixRestTemplate ghixRestTemplate;

	@Autowired
	public GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@PersistenceUnit
	public EntityManagerFactory emf;

	@Autowired
	public UserService userService;

	@Value("#{configProp['appUrl']}")
	private String appUrl;

	@Autowired 
	public GIMonitorService giMonitorService;

	@Autowired 
	private CommentTargetService commentTargetService;
	
	@Autowired 
	private ContentManagementService ecmService;
	
	@Autowired
	private AppEventService appEventService;
	
	@Autowired
	private LookupService lookupService;
	
	private static final String CASE_NUMBER = "caseNumber";
	
	private static final String FINANCIAL_APPLN = "(Financial Application)";
	private static final String NON_FINANCIAL_APPL = "(Non-Financial Application)";

	private static final Logger LOGGER = LoggerFactory.getLogger(IndividualPortalCommons.class);

	@SuppressWarnings("unchecked")
	public Object getIdFromCaseNumber(String caseNumber) {
		EntityManager em = emf.createEntityManager();
		Object id = null;
		try{
			Query query = em.createNativeQuery("select id from ssap_applications where case_number=:caseNumber");
			query.setParameter(IndividualPortalConstants.CASE_NUMBER, caseNumber);
			@SuppressWarnings("unchecked")
			List<Object[]> resultSet = query.getResultList();
			id = resultSet.get(0);
		}catch(Exception ex){
			LOGGER.debug("Exception while retrieving id from case number ", ex);
		}finally{
			em.close();
		}
		return id;
	}
	
	public String persistToGiMonitor(Throwable throwable){
		AccountUser user =null;
		String giMonitorId=null;
		try{
			user = userService.getLoggedInUser();
			GIMonitor giMonitor = new GIMonitor();
			//API is setting some other value if errorcode is null
			giMonitor.setComponent("INDPORTAL");
			giMonitor.setEventTime(new TSDate());
			giMonitor.setException(throwable.getClass().getName());
			giMonitor.setExceptionStackTrace(ExceptionUtils.getStackTrace(throwable));
			giMonitor.setUser(user);
			
			giMonitorId = giMonitorService.saveOrUpdateGIMonitor(giMonitor).getId().toString();
			return  giMonitorId;
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
			return giMonitorId;
		}
	}

	/**
	 * Story HIX-53164 (Coverage date logic) Dashboard - Report Changes, Proceed
	 * to Plan Selection and compute coverage start date for valid event when
	 * change referral is received
	 * 
	 * Sub-task HIX-55448 Compute coverage start date for OE type - include CSR override date logic
	 * 
	 * Checks if the applicant is an adult on coverage start date
	 * 
	 * @author Nikhil Talreja
	 * 
	 * @param coverageStartDate - Coverage Start Date
	 * @param dateOfBirth - DOB of applicant
	 * @return true/fasle
	 */
	public boolean checkIfAdult(Timestamp coverageStartDate, Date dateOfBirth){
		
		LOGGER.debug("Checking if applicant is adult");
		if(coverageStartDate == null || dateOfBirth == null){
			return true;
		}
		LocalDate now = new LocalDate(coverageStartDate.getTime());
		LocalDate birthdate = new LocalDate(dateOfBirth.getTime());
		
		Years years = Years.yearsBetween(birthdate, now);
		int age =  years.getYears();
		
		if(age > IndividualPortalConstants.ADULT_AGE){
			return true;
		}
		
		return false;
	}

	
	/**
	 * Computes effective date for coverage start
	 * 
	 * Following logic is implemented as part of HIX-24255
	 * 
	 * 1. If current date is on or before 15th of a month, effective date is 1st of next month
	 * 2. If current date is after 15th of a month, effective date is 1st of next to next month
	 * 
	 * Exmaples:
	 * 1. Current Date - 12th July 2014 , Effective Date - 1st August 2014
	 * 2. Current Date - 19th July 2014, Effective Date - 1st September 2014
	 * 3. Current Date - 28th November 2013, Effective Date - 1st January 2014
	 * 4. Current Date - 25th December 2013, Effective Date - 1st February 2014
	 * 
	 * @author - Sunil Desu, Nikhil Talreja
	 */
	protected Calendar computeEffectiveDate() {
		
		LOGGER.debug("Computing effective date");
		/*
		 * HIX-55453 Modify computeEffectiveDate base on 15 day rule
		 * @author - Nikhil Talreja
		 */
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);

		// Current Date is on or before 15 of month
		if (currentDate.get(Calendar.DAY_OF_MONTH) <= IndividualPortalConstants.FIFTEEN) {
			effectiveDate.set(Calendar.MONTH,
					currentDate.get(Calendar.MONTH) + 1);
		}
		// Current Date is after 15 of month
		else {
			effectiveDate.set(Calendar.MONTH,
					currentDate.get(Calendar.MONTH) + 2);
		}

		effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
		effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
		effectiveDate.set(Calendar.MINUTE, 0);
		effectiveDate.set(Calendar.SECOND, 0);
		effectiveDate.set(Calendar.MILLISECOND, 0);

		return effectiveDate;
	}
	
	/**
	 * Converts timestamp to string for a particular format
	 * 
	 * Does a null safe operation
	 * 
	 * @param format - Format of response string
	 * @param date - Timestamp Object to be formatted
	 * @return String - Formatted String or null
	 * 
	 * @author Nikhil Talreja
	 * 
	 */
	public String dateToString(String format, Timestamp date){
		
		if(date == null || StringUtils.isBlank(format)){
			return null;
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		return formatter.format(date);
	}
	
	public String dateToString(String format, Date date){
		
		if(date == null || StringUtils.isBlank(format)){
			return null;
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		return formatter.format(date);
	}

	public Long daysRemainingToStartEnrl(int coverageYear){
		Long days = null;
		Date currentDate = new TSDate();
		Date oeStart = null;
		int prevYear=0;
		int currYear=0;
		int renYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			String renewalYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if(coverageYear == prevYear){
				oeStart = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE));
			}
			if(coverageYear == currYear){
				oeStart = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));
			}
			if(StringUtils.isNumeric(renewalYear)){
				renYear = Integer.parseInt(renewalYear);
			}
			if(coverageYear == renYear){
				return 0L;
			}
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(oeStart);
			//cal.add(Calendar.DATE, 1);
			oeStart = cal.getTime();
			Long milliseconds = oeStart.getTime()-currentDate.getTime();
			days = TimeUnit.MILLISECONDS.toDays(milliseconds);
		} catch (ParseException e) {
			LOGGER.error("Exception occured while fetching no of days to start enrollment ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured while fetching no of days to start enrollment :");
		}
		return days;
	}
	
	public Long daysRemainingToEndEnrl(int coverageYear){
		Long days = null;
		Date currentDate = new TSDate();
		Date oeEnd = null;
		int prevYear=0;
		int currYear=0;
		int renYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			String renewalYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if(coverageYear == prevYear){
				oeEnd = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE));
			}
			if(coverageYear == currYear){
				oeEnd = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE));
			}
			if(StringUtils.isNumeric(renewalYear)){
				renYear = Integer.parseInt(renewalYear);
			}
			if(coverageYear == renYear){
				return 0L;
			}
			Calendar cal = TSCalendar.getInstance();
			cal.setTime(oeEnd);
			//cal.add(Calendar.DATE, 1);
			oeEnd = cal.getTime();
				Long milliseconds = oeEnd.getTime()-currentDate.getTime();
				days = TimeUnit.MILLISECONDS.toDays(milliseconds);
		} catch (ParseException e) {
			LOGGER.error("Exception occured while fetching no of days left to end enrollment : ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured while fetching no of days left to end enrollment :");
		}
		return days;
	}

	public String getAppUrl() {
		return appUrl;
	}

	public Float getFloatValue(String input) {
		Float val = null;
		if (StringUtils.isNotBlank(input)) {
			try {
				val = Float.valueOf(input);
			} catch (Exception ex) {
				LOGGER.error("", ex);
				persistToGiMonitor(ex);
			}
		}
		return val;
	}

	public String getIssuerLogoUrl(String issuerLogo) {
		return (issuerLogo != null && !issuerLogo.isEmpty()) ? getAppUrl()+ IndividualPortalConstants.DOCUMENT_URL + ghixJasyptEncrytorUtil.encryptStringByJasypt(issuerLogo) : "";
	}

	public boolean isEnrollWindowActive(){
		boolean status = false;
		Date currentDate = new TSDate();
		Date oeEnd;
		try {
			oeEnd = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_END_DATE));
			if((currentDate.getTime() - oeEnd.getTime()) <= 0){
				status= true;	
			}
		} catch (ParseException e) {
			LOGGER.error("Exception occured while fetching no of days to start enrollment ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException("Exception occured while fetching no of days to start enrollment :");
		}
		return status;
	}
	
	public boolean isOpenEnrollmentApproaching(int coverageYear) {
		Date currentDate = new TSDate();
		boolean status = false;
		Date oeStart = null;
		int prevYear=0;
		int currYear=0;
		int renYear=0;
		try {
			String previousYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
			String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
			String renewalYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
			if(StringUtils.isNumeric(previousYear)){
				prevYear = Integer.parseInt(previousYear);
			}
			if(StringUtils.isNumeric(currentYear)){
				currYear = Integer.parseInt(currentYear);
			}
			if(coverageYear == prevYear){
				oeStart = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE));
			}
			if(coverageYear == currYear){
				oeStart = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT).parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE));
			}
			if(StringUtils.isNumeric(renewalYear)){
				renYear = Integer.parseInt(renewalYear);
			}
			if(coverageYear == renYear){
				return false;
			}
			Long day90Time = Long.valueOf((IndividualPortalConstants.NINETY*IndividualPortalConstants.NO_OF_HOURS_IN_DAY*IndividualPortalConstants.SIXTY*IndividualPortalConstants.THOUSAND))*IndividualPortalConstants.SIXTY;
			Long differncetime= oeStart.getTime()-currentDate.getTime();
			if(differncetime>0 && differncetime<=day90Time){
				status = true;
			}
		} catch (ParseException e) {
			LOGGER.error("Exception occured while checking the current with open enrollment start date : ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(IndividualPortalConstants.ENROLLMENT_EXCEPTION_MESSAGE);
		}

		return status;
	}
	
	/**
	 * HIX-62535 Disenroll CR
	 * 
	 * Utitlity method to store a comment in COMMENTS table
	 * 
	 * @param target_id
	 * @param target_name
	 * @param comment_text
	 * @return success/failure
	 */
	public String saveComments(String targetId, String targetName, String commentText){
		
		try{
			LOGGER.debug("Target id : " + targetId);
			LOGGER.debug("Target name : " + targetName);
			LOGGER.debug("Comment text : " + commentText);
			commentText = StringUtils.trim(commentText);
			
			CommentTarget commentTarget = commentTargetService.findByTargetIdAndTargetType(new Long(targetId), TargetName.valueOf(targetName));
			if(commentTarget == null){
				commentTarget =  new CommentTarget();
				commentTarget.setTargetId(new Long(targetId));
				commentTarget.setTargetName(TargetName.valueOf(targetName));
			}
			
			List<Comment> comments = null;
			
			if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
				comments = new ArrayList<Comment>();
			}
			else{
				comments = commentTarget.getComments();
			}
			
			AccountUser accountUser = null;
			accountUser = userService.getLoggedInUser();
				
			String commenterName = "";

			String firstName = accountUser.getFirstName();
			String lastName  = accountUser.getLastName();
			commenterName += (StringUtils.isBlank(firstName)) ? "" :  firstName+" ";
			commenterName += (StringUtils.isBlank(lastName)) ? "" : lastName;
			
			//Checking if user's name is blank
			if(StringUtils.isBlank(commenterName)){
				commenterName = "Anonymous User";
			}
			
			LOGGER.debug("Commented by : " + commenterName);
			
			Comment comment = new Comment();
			//comment.setComentedBy(request.getParameter("commented_by").trim());
			
			//Setting logged in user's id
			LOGGER.debug("Commenter's user Id : " + accountUser.getId());
			comment.setComentedBy(accountUser.getId());
			
			comment.setCommenterName(commenterName);
			
			//Fetching first 4000 char for comment text;
			int beginIndex = 0;
			int endIndex = commentText.length() > IndividualPortalConstants.MAX_COMMENT_LENGTH ? IndividualPortalConstants.MAX_COMMENT_LENGTH :  commentText.length();
			commentText = commentText.substring(beginIndex, endIndex);
			comment.setComment(commentText);
			
			comment.setCommentTarget(commentTarget);
			
			comments.add(comment);
			commentTarget.setComments(comments);
			commentTargetService.saveCommentTarget(commentTarget);
			
			return IndividualPortalConstants.SUCCESS;
		}
		catch(Exception e){
			LOGGER.error("Unable to save Comment",e);
			return IndividualPortalConstants.FAILURE;
		}
	}

	/**
	 * Converts String date to Timestamp for a particular format
	 * 
	 * Does a null safe operation
	 * 
	 * @param format - Format of response string
	 * @param date - String to be converted
	 * @return Timestamp - java.sql.Timestamp or null
	 * 
	 * @author Nikhil Talreja
	 * 
	 */
	public Timestamp stringToDate(String format, String date, boolean isEndDate) throws ParseException{
		
		if(StringUtils.isBlank(date) || StringUtils.isBlank(format)){
			return null;
		}
		
		// Convert String to SQL Timestamp
		DateFormat dateFormatter = new SimpleDateFormat(format);
		dateFormatter.setLenient(false);
		
		Date eventDate = dateFormatter.parse(date);
	
		if(isEndDate){
			Calendar now = TSCalendar.getInstance();
			now.setTime(eventDate);
			now.set(Calendar.HOUR_OF_DAY, IndividualPortalConstants.ELEVEN_PM);
			now.set(Calendar.MINUTE, IndividualPortalConstants.FIFTY_NINE);
			now.set(Calendar.SECOND,IndividualPortalConstants.FIFTY_NINE);
			return new Timestamp(now.getTimeInMillis());
		}
		else{
			return new Timestamp(eventDate.getTime());
		}
		
	}
	
	public String uploadFileToECM(CommonsMultipartFile fileToUpload, int activeModuleId, String docFor) throws GIException{
        String ecmDocumentId = null;
		String fileName = null;
		String errorMessage = null;
		String relativePath =  "individualComplaintFlowDocuments/"+activeModuleId;

		if(fileToUpload != null) {
			fileName = fileToUpload.getOriginalFilename();
	        try {
	        	fileName = new SimpleDateFormat("yyyyMMddHHmmss").format(TSCalendar.getInstance().getTime()) + "." + fileToUpload.getOriginalFilename();
	        	ecmDocumentId = ecmService.createContent(relativePath,  fileName, fileToUpload.getBytes()
	        			,ECMConstants.Eligibility.DOC_CATEGORY , docFor, null);
	        } catch (ContentManagementServiceException contentManagementServiceException) {
				errorMessage = "Unable to upload file to ECM";
				LOGGER.error(errorMessage, contentManagementServiceException);
				persistToGiMonitor(contentManagementServiceException);
				throw new GIException(errorMessage, contentManagementServiceException);
	        }
		}
		return ecmDocumentId;
	}

	public PlanSummaryDetails chooseHealthOrDentalEnrollment(Map<String, PlanSummaryDetails> enrollmentMap) throws ParseException{
		PlanSummaryDetails healthPlan = null;
		PlanSummaryDetails dentalPlan = null;
		if(enrollmentMap != null && !enrollmentMap.isEmpty()){
			healthPlan = enrollmentMap.get(IndividualPortalConstants.HEALTH_PLAN) ;
			dentalPlan = enrollmentMap.get(IndividualPortalConstants.DENTAL_PLAN);
		}
		
		
		PlanSummaryDetails planToCompare = null;
		
		if(null!=healthPlan && isEnrollmentActive(healthPlan)){
			planToCompare = healthPlan;
		}else if(null!=dentalPlan && isEnrollmentActive(dentalPlan)){
			planToCompare=dentalPlan;
		}else if(null!=healthPlan){
			planToCompare = healthPlan;
		}else if(null!=dentalPlan){
			planToCompare=dentalPlan;
		}
		
		return planToCompare;
		
	}
	
	public String checkForHealthAndDentalTermination(Map<String, PlanSummaryDetails> enrollmentMap) throws ParseException{
		PlanSummaryDetails healthPlan = enrollmentMap.get(IndividualPortalConstants.HEALTH_PLAN) ;
		PlanSummaryDetails dentalPlan = enrollmentMap.get(IndividualPortalConstants.DENTAL_PLAN);
		
		String isPlanEffective = "true";
		
		boolean isHealthActive = true;
		boolean isDentalActive = true;
		
		if(null!=healthPlan && ("Terminated".equalsIgnoreCase(healthPlan.getEnrollmentStatus()) || "Cancelled".equalsIgnoreCase(healthPlan.getEnrollmentStatus())) && isBeforeTodaysDate(healthPlan.getCoverageEndDate())){
			isHealthActive = false;
		}
		if(null!=dentalPlan && ("Terminated".equalsIgnoreCase(dentalPlan.getEnrollmentStatus()) || "Cancelled".equalsIgnoreCase(dentalPlan.getEnrollmentStatus())) && isBeforeTodaysDate(dentalPlan.getCoverageEndDate())){
			isDentalActive = false;
		}

		if(!(isHealthActive && isDentalActive)){
			isPlanEffective = "false";
		}
		return isPlanEffective;
		
	}
	
	public boolean isEnrollmentActive(PlanSummaryDetails enrollment) throws ParseException{
	    
	    if(enrollment == null){
	        return false;
	    }
	    
		boolean isEnrollmentActive = false;
		if(IndividualPortalConstants.ACTIVE_ENROLLMENT_STATUS.contains(enrollment.getEnrollmentStatus()) || ("Terminated".equalsIgnoreCase(enrollment.getEnrollmentStatus()) && isAfterTodaysDate(enrollment.getCoverageEndDate()))){
			isEnrollmentActive = true;
		}
		return isEnrollmentActive;
	}
	
	public boolean isAfterTodaysDate(String date) throws ParseException{
		boolean isAfterTodaysDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
		LocalDate dateToCompare = new DateTime(mmddyyyy.parseObject(date)).toLocalDate();
		if(dateToCompare.isAfter(todaysDate)){
			isAfterTodaysDate = true;
		}
		
		return isAfterTodaysDate;
	}
	

	public boolean isBeforeTodaysDate(String date) throws ParseException{
		boolean isBeforeTodaysDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
		LocalDate dateToCompare = new DateTime(mmddyyyy.parseObject(date)).toLocalDate();
		if(dateToCompare.isBefore(todaysDate)){
			isBeforeTodaysDate = true;
		}
		return isBeforeTodaysDate;
	}
	

	public void logIndividualAppEvent(String eventName, String eventType, Map<String,String> params, HttpServletRequest request){
       
		try {
			
			Map<String, String> mapEventParam = new HashMap<String, String>();
			AccountUser user = this.userService.getLoggedInUser();
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			if (eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_APPLICATION)) {
				
				String caseNumber = request.getParameter(CASE_NUMBER);
				mapEventParam.put(IndividualPortalConstants.APPLICATION_ID, caseNumber);
				
			}else if(eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES)) {
				
				String overrideComment = (String) request.getSession().getAttribute(IndividualPortalConstants.OVERRIDE_COMMENT);
				request.getSession().removeAttribute(IndividualPortalConstants.OVERRIDE_COMMENT);
				String cmrHouseholdId = String.valueOf(user.getActiveModuleId());
				eventInfoDto.setComments(overrideComment);
				mapEventParam.put(IndividualPortalConstants.CMR_HOUSEHOLD_ID, cmrHouseholdId);
				
			} else if(eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_PREFERENCES)) {
				String changeDate = dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, new TSDate());
				mapEventParam.put(IndividualPortalConstants.PREFERENCES_CHANGE_DATE, changeDate);
				
				//HIX-101055
				if(params.containsKey(IndividualPortalConstants.PREV_PREF_COMMUNICATION))
					mapEventParam.put(IndividualPortalConstants.PREFERENCES_PREV_NOTIFICATION, params.get(IndividualPortalConstants.PREV_PREF_COMMUNICATION));
				if(params.containsKey(IndividualPortalConstants.TEXT_MESSAGING_PREF))
					mapEventParam.put(IndividualPortalConstants.PREF_TEXT_MESSAGING, params.get(IndividualPortalConstants.TEXT_MESSAGING_PREF));
				if(params.containsKey(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV))
					mapEventParam.put(IndividualPortalConstants.PREF_PREV_TEXT_MESSAGING, params.get(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV));
				if(params.containsKey(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION))
				mapEventParam.put(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION, params.get(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION));
				
				if(params.containsKey(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE))
					mapEventParam.put(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE, params.get(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE));
				
				if(params.containsKey(IndividualPortalConstants.UPDATED_BY))
					mapEventParam.put(IndividualPortalConstants.UPDATED_BY, params.get(IndividualPortalConstants.UPDATED_BY));
			}
			
			//event type and category
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event",e);
			persistToGiMonitor(e);
		}
	}
	
	public LocalDate getLatestCoverageEndDate(Map<String, EnrollmentShopDTO> enrollments){
		
		LocalDate olderCoverageEndDate = null;
		
		LocalDate healthCoverageEndDate = null;
		LocalDate dentalCoverageEndDate = null;
		if(null == enrollments){
			return null;
		}
		if(null!=enrollments.get("health")){
			healthCoverageEndDate = new LocalDate(enrollments.get("health").getCoverageEndDate().getTime());
			olderCoverageEndDate = healthCoverageEndDate;
		}
		if(null!=enrollments.get("dental")){
			dentalCoverageEndDate = new LocalDate(enrollments.get("dental").getCoverageEndDate().getTime());
			olderCoverageEndDate = dentalCoverageEndDate;
		}
		
		if(healthCoverageEndDate!=null && dentalCoverageEndDate!=null && healthCoverageEndDate.isAfter(dentalCoverageEndDate)){
			olderCoverageEndDate = healthCoverageEndDate;
		}else if(healthCoverageEndDate!=null && dentalCoverageEndDate!=null && dentalCoverageEndDate.isAfter(healthCoverageEndDate)){
			olderCoverageEndDate = dentalCoverageEndDate;
		}
		return olderCoverageEndDate;
		
	}
	
	public boolean isBeforeOrAfterOTRDates(String startDate, String endDate) throws ParseException{
		boolean isBeforeOrAfterOTRDates = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
		LocalDate todaysDate = TSLocalTime.getLocalDateInstance();
		
		LocalDate startDateToCompare = new DateTime(mmddyyyy.parseObject(startDate)).toLocalDate();
		LocalDate endDateToCompare = new DateTime(mmddyyyy.parseObject(endDate)).toLocalDate();
		
		if(todaysDate.isBefore(startDateToCompare) || todaysDate.isAfter(endDateToCompare)){
			isBeforeOrAfterOTRDates = true;
		}		
		return isBeforeOrAfterOTRDates;
	}
	
	/**
	 * Logs Override Save details updates in CAP History
	 * @param eventName
	 * @param eventType
	 * @param sepOverrideDTO
	 */
	public void logAppEventForOverrideSEPDetails(String eventName, String eventType, SEPOverrideDTO sepOverrideDTO){
	       
		try {
			
			Map<String, String> mapEventParam = new HashMap<String, String>();
			
			//event type and category
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			
			
			String applicationType = sepOverrideDTO.getApplicationType();
			if(sepOverrideDTO.getIsFinancial()) {
				applicationType += StringUtils.SPACE + FINANCIAL_APPLN;
			} else {
				applicationType += StringUtils.SPACE + NON_FINANCIAL_APPL;
			}
			
			if(sepOverrideDTO.getApplicantEvents() != null && !sepOverrideDTO.getApplicantEvents().isEmpty()) {
				for(ApplicantEventsDTO applicantEventsDTO : sepOverrideDTO.getApplicantEvents()) {
					EventInfoDto eventInfoDto = new EventInfoDto();
					eventInfoDto.setEventLookupValue(lookupValue);
					
					String applicantName = applicantEventsDTO.getFirstName();
					applicantName += StringUtils.isBlank(applicantEventsDTO.getMiddleName()) ? StringUtils.EMPTY : (StringUtils.SPACE + applicantEventsDTO.getMiddleName());
					applicantName += StringUtils.SPACE + applicantEventsDTO.getLastName();
					
					mapEventParam.put(IndividualPortalConstants.SSAP_CASE_NUMBER, sepOverrideDTO.getCaseNumber());
					mapEventParam.put(IndividualPortalConstants.SSAP_APPLICATION_TYPE, applicationType);
					mapEventParam.put(IndividualPortalConstants.SSAP_APPLICANT_NAME, applicantName);
					mapEventParam.put(IndividualPortalConstants.EVENT_TYPE, applicantEventsDTO.getEventNameLabel());
					mapEventParam.put(IndividualPortalConstants.EVENT_DATE, applicantEventsDTO.getEventDate());
					mapEventParam.put(IndividualPortalConstants.SEP_START_DATE, sepOverrideDTO.getSepStartDate());
					mapEventParam.put(IndividualPortalConstants.SEP_END_DATE, sepOverrideDTO.getSepEndDate());
					
					//mapEventParam.put(IndividualPortalConstants.OVERRIDE_COMMENT, sepOverrideDTO.getOverrideCommentText());
					eventInfoDto.setComments(sepOverrideDTO.getOverrideCommentText());
					appEventService.record(eventInfoDto, mapEventParam);
				}	
			} else {
				EventInfoDto eventInfoDto = new EventInfoDto();
				eventInfoDto.setEventLookupValue(lookupValue);
				mapEventParam.put(IndividualPortalConstants.SEP_START_DATE, sepOverrideDTO.getSepStartDate());
				mapEventParam.put(IndividualPortalConstants.SEP_END_DATE, sepOverrideDTO.getSepEndDate());
				//mapEventParam.put(IndividualPortalConstants.OVERRIDE_COMMENT, sepOverrideDTO.getOverrideCommentText());
				eventInfoDto.setComments(sepOverrideDTO.getOverrideCommentText());
				
				appEventService.record(eventInfoDto, mapEventParam);
			}
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event",e); 
			persistToGiMonitor(e);
		}
	}
	
	/**
	 * Logs ESD updates in CAP History
	 * @param eventName
	 * @param eventType
	 * @param datesOverrideDTO
	 * @param params 
	 */
	public void logAppEventForValidateESDDetails(String eventName, String eventType, String caseNumber, 
			EligibilityDatesOverrideDTO datesOverrideDTO, Map<String, Object> params, String comments){
	       
		try {
			
			Map<String, String> mapEventParam = new HashMap<String, String>();
			
			//event type and category
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			
			if(datesOverrideDTO != null) {
				EventInfoDto eventInfoDto = new EventInfoDto();
				eventInfoDto.setEventLookupValue(lookupValue);
				if(StringUtils.isNotBlank(comments)) {
					eventInfoDto.setComments(comments);
				}
				mapEventParam.put(IndividualPortalConstants.ESD_APPLN_CASE_NUMBER, caseNumber);
				mapEventParam.put(IndividualPortalConstants.ESD_ELIGIBILITY_START_DATE, 
						dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, datesOverrideDTO.getFinancialEffectiveDate()));
				mapEventParam.put(IndividualPortalConstants.ESD_OVERRIDE_STATUS, datesOverrideDTO.getStatus().toString());
				mapEventParam.put(IndividualPortalConstants.ESD_ENROLLMENT_START_DATE, 
						dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, datesOverrideDTO.getEnrollmentStartDate()));
				mapEventParam.put(IndividualPortalConstants.ESD_ENROLLMENT_END_DATE, 
						dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, datesOverrideDTO.getEnrollmentEndDate()));
				
				if(params != null && !params.isEmpty()) {
					if(params.containsKey(IndividualPortalConstants.ESD_SEP_START_DATE))
						mapEventParam.put(IndividualPortalConstants.SEP_START_DATE, 
								dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, (Timestamp)params.get(IndividualPortalConstants.ESD_SEP_START_DATE)));
					if(params.containsKey(IndividualPortalConstants.ESD_SEP_END_DATE))
						mapEventParam.put(IndividualPortalConstants.SEP_END_DATE, 
								dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT,(Timestamp)params.get(IndividualPortalConstants.ESD_SEP_END_DATE)));
				}
				appEventService.record(eventInfoDto, mapEventParam);
			}
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event",e); 
			persistToGiMonitor(e);
		}
	}

}
