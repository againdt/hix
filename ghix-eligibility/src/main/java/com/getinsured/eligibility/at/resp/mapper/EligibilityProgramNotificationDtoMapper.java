package com.getinsured.eligibility.at.resp.mapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.getinsured.eligibility.notification.dto.EligibilityProgramDTO;
import com.getinsured.eligibility.notification.dto.SsapApplicantEligibilityDTO;
import com.getinsured.eligibility.notification.dto.SsapApplicationEligibilityDTO;

public final class EligibilityProgramNotificationDtoMapper {

	private EligibilityProgramNotificationDtoMapper() {}


	public static SsapApplicationEligibilityDTO map(com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO ssapApplicationEligibilityDTO) {

		SsapApplicationEligibilityDTO output = new SsapApplicationEligibilityDTO();

		output.setApplicationStatus(ssapApplicationEligibilityDTO.getApplicationStatus());
		output.setPrimaryKey(ssapApplicationEligibilityDTO.getPrimaryKey());
		output.setCaseNumber(ssapApplicationEligibilityDTO.getCaseNumber());
		output.setSource(ssapApplicationEligibilityDTO.getSource());
		output.setCoverageYear(ssapApplicationEligibilityDTO.getCoverageYear());
		output.setApplicationStatus(ssapApplicationEligibilityDTO.getApplicationStatus());
		output.setEligibilityStatus(ssapApplicationEligibilityDTO.getEligibilityStatus());
		output.setExchangeEligibilityStatus(ssapApplicationEligibilityDTO.getExchangeEligibilityStatus());
		output.setCsrLevel(ssapApplicationEligibilityDTO.getCsrLevel());
		output.setMaxAPTCAmount(ssapApplicationEligibilityDTO.getMaxAPTCAmount());
		output.setElectedAPTCAmount(ssapApplicationEligibilityDTO.getElectedAPTCAmount());
		output.setEnableEnrollment(ssapApplicationEligibilityDTO.getEnableEnrollment());
		output.setCmrHouseholdId(ssapApplicationEligibilityDTO.getCmrHouseholdId());
		output.setEligibilityReceivedDate(ssapApplicationEligibilityDTO.getEligibilityReceivedDate());
		output.setEligibilityResponseType(ssapApplicationEligibilityDTO.getEligibilityResponseType());
		output.setNativeAmericanHousehold(ssapApplicationEligibilityDTO.getNativeAmericanHousehold());

		output.setPrimaryFirstName(ssapApplicationEligibilityDTO.getPrimaryFirstName());
		output.setPrimaryMiddleName(ssapApplicationEligibilityDTO.getPrimaryMiddleName());
		output.setPrimaryLastName(ssapApplicationEligibilityDTO.getPrimaryLastName());

		// TODO: Unsure if mapping method needed at all, change to only use the DTO and not UI DTO
		output.setOutboundATSent(ssapApplicationEligibilityDTO.isOutboundATSent());
		output.setBrokerName(ssapApplicationEligibilityDTO.getBrokerName());
		output.setApplicationEligibilityStatus(ssapApplicationEligibilityDTO.getApplicationEligibilityStatus());
		output.setIncomeDMIExists(ssapApplicationEligibilityDTO.isIncomeDMIExists());
		output.setNonIncomeDMIExists(ssapApplicationEligibilityDTO.isNonIncomeDMIExists());
		output.setContainsMedicaidCHIPAssessed(ssapApplicationEligibilityDTO.isContainsMedicaidCHIPAssessed());
		output.setContainsHealthLinkEligible(ssapApplicationEligibilityDTO.isContainsHealthLinkEligible());
		output.setContainsHealthLinkInEligible(ssapApplicationEligibilityDTO.isContainsHealthLinkInEligible());
		output.setContainsNotSeekingCoverage(ssapApplicationEligibilityDTO.isContainsNotSeekingCoverage());
		output.setHouseholdCaseId(ssapApplicationEligibilityDTO.getHouseholdCaseId());
		output.setSepEligible(ssapApplicationEligibilityDTO.isSepEligible());
		output.setApplicationType(ssapApplicationEligibilityDTO.getApplicationType());
		output.setInsideOEWindow(ssapApplicationEligibilityDTO.isInsideOEWindow());

		List<SsapApplicantEligibilityDTO> ssapApplicantEligibilityNotificationDTOList = new ArrayList<>();

		List<com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO> ssapApplicantEligibilityDTOList = ssapApplicationEligibilityDTO.getSsapApplicantList();
		for (com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO : ssapApplicantEligibilityDTOList) {
			SsapApplicantEligibilityDTO data = new SsapApplicantEligibilityDTO();
			data.setFirstName(ssapApplicantEligibilityDTO.getFirstName());
			data.setMiddleName(ssapApplicantEligibilityDTO.getMiddleName());
			data.setLastName(ssapApplicantEligibilityDTO.getLastName());

			data.setVerificationMap(ssapApplicantEligibilityDTO.getVerificationMap());

			Set<EligibilityProgramDTO> eligibilityProgramDTO = new HashSet<>();

			data.setConditional(ssapApplicantEligibilityDTO.isConditional());
			data.setApplyingForCoverage(ssapApplicantEligibilityDTO.isApplyingForCoverage());
			data.setMedicaidAssessed(ssapApplicantEligibilityDTO.isMedicaidAssessed());
			data.setHealthLinkEligible(ssapApplicantEligibilityDTO.isHealthLinkEligible());

			for (com.getinsured.eligibility.ui.dto.EligibilityProgramDTO epDTO : ssapApplicantEligibilityDTO.getEligibilities()) {
				eligibilityProgramDTO.add(EligibilityProgramNotificationDtoMapper.map(epDTO));
			}
			data.setEligibilityProgramDTO(eligibilityProgramDTO);
			ssapApplicantEligibilityNotificationDTOList.add(data);
		}

		output.setSsapApplicantEligibilityDTO(ssapApplicantEligibilityNotificationDTOList);
		output.setSsapApplicantEligibleEligibilityDTO(ssapApplicantEligibilityNotificationDTOList);


		List<SsapApplicantEligibilityDTO> ssapApplicantInEligibleEligibilityNotificationDTOList = new ArrayList<>();

		List<com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO> ssapApplicantIneligibleEligibilityDTOList = ssapApplicationEligibilityDTO.getSsapApplicantIneligibleEligibilityDTO();
		for (com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO ssapApplicantEligibilityDTO : ssapApplicantIneligibleEligibilityDTOList) {
			SsapApplicantEligibilityDTO data = new SsapApplicantEligibilityDTO();
			data.setFirstName(ssapApplicantEligibilityDTO.getFirstName());
			data.setMiddleName(ssapApplicantEligibilityDTO.getMiddleName());
			data.setLastName(ssapApplicantEligibilityDTO.getLastName());

			data.setVerificationMap(ssapApplicantEligibilityDTO.getVerificationMap());

			Set<EligibilityProgramDTO> eligibilityProgramDTO = new HashSet<>();

			for (com.getinsured.eligibility.ui.dto.EligibilityProgramDTO epDTO : ssapApplicantEligibilityDTO.getEligibilities()) {
				eligibilityProgramDTO.add(EligibilityProgramNotificationDtoMapper.map(epDTO));
			}
			data.setEligibilityProgramDTO(eligibilityProgramDTO);
			ssapApplicantInEligibleEligibilityNotificationDTOList.add(data);
		}
		output.setSsapApplicantIneligibleEligibilityDTO(ssapApplicantInEligibleEligibilityNotificationDTOList);


		return output;
	}

	public static EligibilityProgramDTO map(com.getinsured.eligibility.ui.dto.EligibilityProgramDTO epDTO){

		EligibilityProgramDTO eligibilityProgramDto = new EligibilityProgramDTO();
		eligibilityProgramDto.setId(epDTO.getId());
		eligibilityProgramDto.setEligibilityType(epDTO.getEligibilityType());
		eligibilityProgramDto.setEligibilityIndicator(epDTO.getEligibilityIndicator());
		eligibilityProgramDto.setEligibilityStartDate(epDTO.getEligibilityStartDate());
		eligibilityProgramDto.setEligibilityEndDate(epDTO.getEligibilityEndDate());
		eligibilityProgramDto.setEligibilityDeterminationDate(epDTO.getEligibilityDeterminationDate());
		eligibilityProgramDto.setIneligibleReason(epDTO.getIneligibleReason());
		eligibilityProgramDto.setEstablishingCategoryCode(epDTO.getEstablishingCategoryCode());
		eligibilityProgramDto.setEstablishingCountyName(epDTO.getEstablishingCountyName());
		eligibilityProgramDto.setEstablishingStateCode(epDTO.getEstablishingStateCode());

		eligibilityProgramDto.setEligibilityStatus(epDTO.getEligibilityStatus());
		eligibilityProgramDto.setMaxAPTCAmount(epDTO.getMaxAPTCAmount());
		eligibilityProgramDto.setCsrLevel(epDTO.getCsrLevel());

		return eligibilityProgramDto;
	}

}
