package com.getinsured.ssap.service;

import java.util.List;
import java.util.Map;

import com.getinsured.ssap.model.financial.TaxFilingRequirement;

/**
 * Describes tax filing requirement service.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-10
 */
public interface TaxFilingRequirementService {
  /**
   * Returns tax filing requirements by given year.
   * @param year year for which to give tax filing requirements.
   * @return list of {@link TaxFilingRequirement} objects.
   */
  List<TaxFilingRequirement> getTaxFilingRequirementsByYear(int year);

  /**
   * Returns tax filing requirements for all configured years.
   * @return Map of List, with following structure: "2018" => list of {@link TaxFilingRequirement}, "2019" => ...
   */
  Map<Integer, List<TaxFilingRequirement>> getTaxFilingRequirements();
}
