package com.getinsured.ssap.util;

import java.util.Arrays;
import java.util.List;

/**
 * Eligibility program constant types.
 *
 * @author Yevgen Golubenko
 * @since 5/8/19
 */
public class EligibilityConstants
{
  public static final String CHIP_ELIGIBILITY_TYPE = "CHIPEligibilityType";
  public static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
  public static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
  public static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
  public static final String STATE_SUBSIDY_ELIGIBILITY_TYPE = "StateSubsidyEligibilityType";
  public static final String MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "MedicaidNonMAGIEligibilityType";
  public static final String MEDICAID_MAGI_ELIGIBILITY_TYPE = "MedicaidMAGIEligibilityType";
  public static final String REFUGEE_MEDICAL_ASSISTANCE_ELIGIBILITY_TYPE = "RefugeeMedicalAssistanceEligibilityType";
  public static final String EMERGENCY_MEDICAID_ELIGIBILITY_TYPE = "EmergencyMedicaidEligibilityType";
  public static final String MEDICAID_ELIGIBILITY_TYPE = "MedicaidEligibilityType";
  public static final String ASSESSED_MEDICAID_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidMAGIEligibilityType";
  public static final String ASSESSED_MEDICAID_NON_MAGI_ELIGIBILITY_TYPE = "AssessedMedicaidNonMAGIEligibilityType";
  public static final String ASSESSED_CHIP_ELIGIBILITY_TYPE = "AssessedCHIPEligibilityType";

  // Verification Statuses
  public static final String PENDING = "PENDING";
  public static final String MORE_INFO_NEEDED = "MORE_INFO_NEEDED";
  public static final String NOT_REQUIRED = "NOT_REQUIRED";
  public static final String NOT_VERIFIED = "NOT_VERIFIED";
  public static final String NOT_VERIFIED_VARIANT = "NOT VERIFIED";
  public static final String VERIFIED = "VERIFIED";
  public static final List<String> NOT_VERIFIED_STATUS_LIST = Arrays.asList(PENDING, MORE_INFO_NEEDED, NOT_VERIFIED, NOT_VERIFIED_VARIANT);
}
