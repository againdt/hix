//Select fix ie8

$(document).ready(function(){
	var el; 
	$("select").each(function() { 
		el = $(this); 
		el.data("origWidth", el.outerWidth()); // IE 8 can haz padding 
	}).mouseenter(function(){ 
		$(this).css({"width": "auto",'position':'relative'}); 
	}).bind("blur change", function(){ 
		el = $(this); 
		el.css("width", el.data("origWidth")); 
	}); 

});