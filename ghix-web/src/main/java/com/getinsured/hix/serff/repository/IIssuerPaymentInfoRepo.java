package com.getinsured.hix.serff.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.IssuerPaymentInformation;

/**
 * Interface is used provide JpaRepository of table ISSUER_PAYMENT_INFO.
 * @author Vani Sharma
 */
public interface IIssuerPaymentInfoRepo extends JpaRepository<IssuerPaymentInformation , Integer>{
	
	IssuerPaymentInformation findByIssuerId(Integer issuerId);

}
