import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent implements OnInit {

    constructor(private router: Router) { }

    ngOnInit() {

    }

    learnMore() {
    	const currentUrl =  window.location.href;
    	let redirectUrl;
    	if(currentUrl.indexOf('lang=es') !== -1) {
    		redirectUrl = 'https://faq.wahealthplanfinder.org/es/faq/smartplanfinder_es.htm';
    	}else {
    		redirectUrl = 'https://faq.wahealthplanfinder.org/faq/Smart_planfinder.htm';
    	}

    	window.open(redirectUrl, '_blank');
    }

}
