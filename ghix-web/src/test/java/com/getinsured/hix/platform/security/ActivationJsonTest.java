package com.getinsured.hix.platform.security;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.google.gson.Gson;


public class ActivationJsonTest /*extends BaseTest*/ {

	ActivationJson jsonObject;

	@Before
	public void setUp(){

		jsonObject = new ActivationJson();

		CreatedObject createdObject = new CreatedObject();
		createdObject.setObjectId(1);
		createdObject.setEmailId("a.a@a.com");
		createdObject.setRoleName(GhixRole.EMPLOYEE.toString());
		createdObject.setPhoneNumbers(Arrays.asList("9930096717"));
		createdObject.setFullName("A B");

		CreatorObject creatorObject = new CreatorObject();
		creatorObject.setObjectId(1);
		creatorObject.setFullName("Ekram Ali Kazi");
		creatorObject.setRoleName(GhixRole.EMPLOYER.toString());

		jsonObject.setCreatorObject(creatorObject);
		jsonObject.setCreatedObject(createdObject);

	}

	@Test
	public void test() {
		Gson gson = new Gson();

		String jsonString = gson.toJson(jsonObject);
		assertNotNull(jsonString);

		ActivationJson newObject = gson.fromJson(jsonString, ActivationJson.class);
		assertNotNull(newObject);
		assertNotNull(newObject.getRepositoryName());
		assertEquals("employeeRepository", newObject.getRepositoryName());

	}

}
