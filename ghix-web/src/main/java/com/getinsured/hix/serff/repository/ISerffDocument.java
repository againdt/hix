package com.getinsured.hix.serff.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.serff.service.SerffDocumentPlanPOJO;

/**
 * Interface is used provide JpaRepository of table SERFF_DOCUMENT.
 * @author Bhavin Parmar
 */
@Repository
public interface ISerffDocument extends JpaRepository<SerffDocument, Long> {

	@Query("SELECT new com.getinsured.hix.serff.service.SerffDocumentPlanPOJO(sd.docName, spm.serffTrackNum, sd.ecmDocId, sd.createdOn) FROM SerffDocument sd, SerffPlanMgmt spm where spm.operation=:operation and sd.serffReqId = "
	 		+ "spm.serffReqId and spm.issuerId =:issuerId and spm.requestStatus ='S' order by sd.createdOn desc ")
	List<SerffDocumentPlanPOJO> getAllDocumentsforThisIssuer(@Param("issuerId") String issuerId, @Param("operation") String operation);
}
