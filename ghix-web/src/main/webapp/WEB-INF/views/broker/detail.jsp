<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%
String designatedBrokerName = (String)request.getAttribute("designatedBrokerName"); 
String isSameBroker = (String) request.getAttribute("isSameBroker");
String isAnonymousUser = (String) request.getAttribute("isAnonymousUser");
String isAssisterDesignated = (String) request.getAttribute("isAssisterDesignated");
String isBrokerDesignated = (String) request.getAttribute("isBrokerDesignated");
String isCompanySetForEmployer = (String) request.getAttribute("isCompanySetForEmployer");
%> 

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->	


<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/accessibility.css" />" media="screen"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/broker.css" />" media="screen"/>
<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:if test='${stateCode == "CA"}'>
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<style type="text/css">
.collapse-content {display: none;}
.collapse {font-weight: bold;}
#button-section,
#designateButton {float:left; margin-right: 10px;}
#display-info td{padding: 0 20px 0 0 !important;}
#profile-info h1,
/* #profile-info p {padding-left: 20px;} */
#profile-info h1 {margin-top: 0; word-break: break-all;}
/* #profile-info p {margin-bottom: 20px;} */
.dl-horizontal {border-top: 1px solid #e2dfd7; padding-top: 10px;}
.dl-horizontal dt {text-align: left; font-weight: normal;}
#designated-msg2 p{margin-top: 0; margin-bottom: 0; }
.idhide {
  display:none;
}
</style>
<script type="text/javascript">
$(function() {

$(".collapse").click(function () {
	
	  $(".collapse-content").toggle();
	  $(this).text($(this).text() == '[-] What should I know before I designate an Agent?' ? '[+] What should I know before I designate an Agent?' : '[-] What should I know before I designate an Agent?');
	});
});
</script>


<div class="gutter10" id="broker_details">
	<form class="form-horizontal" id="frmdetail" name="frmdetail" action="<c:url value="../broker/designate" />"  method="POST">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	  <df:csrfToken/>
		<input type="hidden" name="id" id="id" value="">

							<% if("false".equals(isSameBroker))  { %>	
										<h4><spring:message code="label.brk.Selection"/></h4>
										 <p><spring:message code="label.brk.SelectionComment"/></p>		
									<% } %>													
																				
			                    <div id="btn btn-primary pull-right">
			                    <%
									 if (isAnonymousUser == null || isAnonymousUser.equalsIgnoreCase("N"))
									 {
										 boolean assisterFlag = (isAssisterDesignated!=null)&&(isAssisterDesignated.equalsIgnoreCase("Y"))?true:false;
										 boolean brokerFlag = (isBrokerDesignated!=null) && (isBrokerDesignated.equalsIgnoreCase("Y"))?true:false;
										 String designatedAssisterName = (String) request.getAttribute("designatedAssisterName");										 
										 String brokerOrAssisterName = (assisterFlag)?designatedAssisterName:(brokerFlag?designatedBrokerName:"");
										 boolean isCompanySetForEmployerFlag = (isCompanySetForEmployer != null && isCompanySetForEmployer.equalsIgnoreCase("Y")) ? true : false;
										 if(isSameBroker.equals("true")){%>
											 <span id="designated-msg">
											 	<h4>${broker.user.firstName}&nbsp;${broker.user.lastName} <spring:message  code="label.brk.Designatedbrk"/></h4>
											 </span>
										 <%}
										 else if (assisterFlag || brokerFlag)
							             {%>
											 <a class="btn btn-primary pull-right" onClick="alreadyDesignated('<%=brokerOrAssisterName%>')"><spring:message  code="label.brkContinue"/></a>
							             <%}
										 else if (isCompanySetForEmployerFlag)
							             {%>
											 <%-- <a class="btn btn-primary pull-right" onClick="companyNotSetForEmployer()"><spring:message  code="label.brkContinue"/></a> --%>
							             <%}
							             else
							             {%>
							            	 <a class="btn btn-primary pull-right" href="../broker/esignature/${encBrokerId}"><spring:message  code="label.brkContinue"/></a>
							             <%}
									 }
									%>
								</div>	 <!-- button-section end -->

					<!-- Since refresh/reload is not possible, this is displayed on successful de-designation--> 
			<div class="span10 alert alert-info" style="width:80%" id="designateButton">
				<h4 class="span7">${broker.user.firstName}&nbsp;${broker.user.lastName} <spring:message  code="label.brk.DeDesignatedbrk"/></h4>										
			</div>
			<div class="gutter10-b bottomBorder">			                    										    
				    <div class="row-fluid">
						<span class="pull-right">
							<a class="btn" href="../broker/searchlist?usesession=yes&pageNumber=${brkpage}" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Search by name', 1]);"><spring:message code="label.brkBack"/></a>
							<a class="btn btn-primary" href="../broker/search" style="margin-right: 80px;"><spring:message code="label.brk.SearchAgain"/></a>
						</span>
					</div>
			</div>
			<div class="row-fluid">
				<div class="gutter10" id="broker-info">
				<c:choose>
					<c:when test="${!empty broker}">
						<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
						
						<div  id="profile-img" class="col-xs-12 col-sm-2 col-md-2 col-lg-2">
							<img src="${photoUrl}" alt="Broker profile photo thumbnail" class="profilephoto thumbnail" />
						</div>
						
						<div id="profile-info" class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
							<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>

							<p>${broker.location.address1}, 
							<c:if test="${broker.location.address2!=null && broker.location.address2!=\"\"}">
								${broker.location.address2}, 
							</c:if>
								${broker.location.city}, ${broker.location.state}  ${broker.location.zip}<br />
								<i class="icon-phone" aria-label="phone number"></i>${broker.contactNumber}<br/>
								${broker.yourPublicEmail}
							</p>
							
							<dl class="dl-horizontal">
							    <dt><spring:message  code="label.brkProductExpertises"/></dt>
							    <dd class="DDagentdetails"><strong>${broker.productExpertise}</strong></dd>
							    <dt><spring:message  code="label.brkLanguageSpoken"/></dt>
							    <dd class="DDagentdetails"><strong>${broker.languagesSpoken}</strong></dd>
							    <dt><spring:message  code="label.brkStateLicenseNumber"/></dt>
							    <dd class="DDagentdetails"><strong>${broker.licenseNumber}</strong></dd>
							    <dt><spring:message  code="label.brkClientServed"/></dt>
							    <dd class="DDagentdetails"><strong>${broker.clientsServed}</strong></dd>  
							</dl>
						</div>
						
						<div id="profile-map" class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
								<iframe width="300" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;z=13&amp;output=embed&amp;iwloc=near"></iframe>
						</div>
						
					<% if(!"true".equals(isSameBroker)) {%>
						<div class="row-fluid">								
								<div id="info-designate" class="span12">
								<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
								<c:if test='${stateCode != "ID"}'>
									<div class="alert alert-info margin30-t">
										<h4><a href="javascript:void(0)" class="collapse link" aria-describedby="aria-content"><spring:message  code="label.brk.DesignatebrkQuery"/></a></h4>
										<div class="collapse-content" id="aria-content">
											<p class="margin20-t"> <spring:message  code="label.brk.DesignatebrkInfo"/></p>
										</div>							
									</div>
								</c:if>
								</div>
						</div>  <!-- row-fluid end -->
						<% } %>
					</c:when>
					<c:otherwise>
						<b> <spring:message  code="label.brknomatching"/></b>
					</c:otherwise>
				</c:choose>
				</div> <!-- gutter10-tb end -->
			</div>
</form>
</div>
<!-- Hides de-designate button. So it is not shown on load -->
<script type="text/javascript">$('#designateButton').css('display','none');</script> 
<script type="text/javascript">
/**
 * Function to  encode name field data.
 *
 * @version 1.0
 * @param currURL The unencoded URL.
 * @return currURL The URL with encoded name field data.
 */
function encodeNameData(currURL) {
	if(null != currURL && "undefined" != currURL && "" !== currURL) {
	    var fNameTerm = "?firstName=";
	    var lNameTerm = "&lastName=";
	    var brokerIdTerm = "&brokerId=";
	    
		var origURL = currURL.substring(0,currURL.indexOf("?"));
	    
		var queryString = currURL.substring(currURL.indexOf("?"), currURL.length);	
	
	    var fName = queryString.substring(queryString.indexOf(fNameTerm)+fNameTerm.length, queryString.indexOf(lNameTerm));
	    fName = encodeURIComponent(fName);
	
	    var lName = queryString.substring(queryString.indexOf(lNameTerm)+lNameTerm.length, queryString.indexOf(brokerIdTerm));
	    lName = encodeURIComponent(lName);
	
		currURL = origURL + fNameTerm + fName + lNameTerm + lName + queryString.substring(queryString.indexOf(brokerIdTerm), queryString.length);
	}
	
	return currURL;
}

/* $(function(){
	if($('#designateButton').css('display') == 'none' ){
		$("#info-designate").css('display','none');
		$("#designated-msg2").css('display','block');
	}
	else {
		$("#info-designate").css('display','block');
		$("#designated-msg2").css('display','none');	 
 	}
}); */
function alreadyDesignated(designatedBroker) {
	$('<div id="alreadyDesignated" class="modal" style="top:30%"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center"><iframe id="success" src="#" style="overflow-x:hidden;width:30%;border:0;margin:0;padding:0;height:20px;" class="hide"></iframe> <spring:message code="label.agent.details.designatedto"/> ' + designatedBroker + '. <spring:message code="label.agent.details.designationmessage"/>'+'<br/> <br/> <button class="btn" data-dismiss="modal" aria-hidden="true">Ok</button></div></div>').modal({backdrop:false});
}

$(function(){ 
	$("#dedesignateLightboxLink").click(function(e){
        e.preventDefault();
        var href = $(this).attr('href');
        
        href = encodeNameData(href);
        
        if (href.indexOf('#') != 0) {
           $('<div id="modal" class="modal" style="top:80%"><div class="modal-header" style="border-bottom:0;"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe id="dedesignatePopup" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:140px;"></iframe></div></div>').modal({backdrop:false});
		}
	});
});

function closeLightboxOnCancel(){
	$("#modal").remove();	
}

function closeLightbox(){
	$("#modal").remove();
	//Will provide the reload effect
	$("#designateButton").show();
	$("#designated-msg").hide();
	$("#designated-msg2").hide();
	$("#button-section").hide();
}

</script>
