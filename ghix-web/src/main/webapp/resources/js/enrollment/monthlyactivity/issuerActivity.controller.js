(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .controller('issuerActivityCtrl', ['$scope', '$http', '$state', '$timeout', 'workbenchService', 'NotifyingService', 'searchReconciliation', 'dataStorageService', function ($scope, $http, $state, $timeout, workbenchService, NotifyingService, searchReconciliation, dataStorageService) {
            $scope.issuerFiles = [];
            var paginationOptions = {
                pageNumber: 1,
                pageSize: 5,
                sort: null
            };
            $scope.gridOptionsForIssuer = {
                paginationPageSizes: [$scope.pageSize, $scope.pageSize * 2, $scope.pageSize * 3],
                paginationPageSize: paginationOptions.pageSize,
                minRowsToShow: paginationOptions.pageSize * 2,
                enableSorting: true,
                enableColumnMenus: false,
                rowHeight: 34,
                columnDefs: [
                    { field: 'fileName', displayName: 'Filename'.toUpperCase(), width: 150, cellTemplate: "" +
                    		"<div>" +
                    		"<div class='ui-grid-cell-contents' ng-if=\"row.entity.status !=='Duplicate' \"><a href=\"\" ng-click=\"grid.appScope.searchReconcillation(row.entity)\"> {{ row.entity.fileName}} </a></div>" +
                    		"<div class='ui-grid-cell-contents' ng-if=\"row.entity.status ==='Duplicate' \"> {{row.entity.fileName}}</div>"+
                    		"</div>"+
                    		"" }, //totalEnrlDiscrepancyCount}, //issuerName
                    { field: 'dateReceived', displayName: 'Received Date'.toUpperCase() , cellFilter: 'date:\'MM-dd-yyyy\'' , width: 80},
                    { field: 'status', displayName: 'Status'.toUpperCase() ,  width: 85}, //totalNumberOfFiles
                    { field: 'totalEnrlCntInFile', displayName: 'In File'.toUpperCase(),  width: 50 }, //totalEnrlCntInFile
                    { field: 'totalEnrlInHix', displayName: 'In HIX'.toUpperCase(),  width: 70 }, //totalEnrlInHix
                    { field: 'totalSuccess', displayName: 'Success'.toUpperCase(),  width: 70 }, //totalEnrlInHix
                    { field: 'totalEnrlDiscrepancyCount', displayName: 'Discrepancies'.toUpperCase(),  width: 110,  type: 'number' }, //totalEnrlDiscrepancyCount
                    { field: 'totalFailure', displayName: 'Errors'.toUpperCase() ,  width: 70,  type: 'number' }, //enrollmentFailedCount
                    // { field: 'totalEnrlMissingInFile', displayName: 'Missing (File)'.toUpperCase(),  width: 70}, //totalEnrlDiscrepancyCount }, // Kuldeep to add new field
                    // { field: 'totalEnrlMissingInHix', displayName: 'Missing (HIX)'.toUpperCase() ,  width: 70}, // Kuldeep to add new field
                    { field: 'totalDiscrepancyCount', displayName: 'Total Discrepancies'.toUpperCase(), width: 110 ,  type: 'number'}, //totalDiscrepancyCount
                    { field: 'discrepancyReportName', displayName: 'Report Name'.toUpperCase() , width: 150}, // Kuldeep to add new field
                    { field: 'discrepancyReportCreationTimestamp', displayName: 'Report Date'.toUpperCase() , cellFilter: 'date:\'MM-dd-yyyy\'' , width: 80} 
                ]
            };

            $scope.searchReconcillation = function (searchReconObj) {
                searchReconObj.hiosIssuerId = $("#isserSelect :selected").val();
                searchReconObj.reconYear = $("#yearSelect :selected").val();
                searchReconObj.reconMonth = $("#monthSelect :selected").val();
                searchReconObj.lastUpdateTime = moment();
                $state.go('search');
                dataStorageService.setItem("searchData", null);
                dataStorageService.setItem("ReconObj", searchReconObj);
                searchReconciliation.setIssuer(searchReconObj);
            };

            NotifyingService.subscribe($scope, function fileLevelActivity() {
                $scope.issuerFiles =  workbenchService.getFileLevelSummary();
                $scope.gridOptionsForIssuer.data = [];
                if($scope.issuerFiles) {

                    $timeout(function() {
                        angular.forEach($scope.issuerFiles, function (row) {
                            $scope.gridOptionsForIssuer.data.push(row);
                        });
                    }, 0);
                }
            });
        }]);
})();