package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsRequestType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper Class for GetCaseDetailsRequestType
 * 
 * @author Vishaka Tharani
 * @since  17-Feb'2014
 * 
 */
public class GetCaseDetailsRequestWrapper implements JSONAware {
	
	private GetCaseDetailsRequestType getCaseDetailsRequestType;
	
	private String caseNbr;
	private static final String CASE_NUMBER = "caseNumber";
	
	public GetCaseDetailsRequestWrapper(){
		ObjectFactory factory = new ObjectFactory();
		this.getCaseDetailsRequestType = factory.createGetCaseDetailsRequestType();
	}
	
	public void setCaseNbr(String caseNbr) throws HubServiceException{
		
		if(caseNbr == null){
			throw new HubServiceException("No case number provided");
		}
		
		this.caseNbr = caseNbr;
		this.getCaseDetailsRequestType.setCaseNumber(caseNbr);
	}
	
	public String getCaseNbr() {
		return caseNbr;
	}
	
	public GetCaseDetailsRequestType getCaseDetailsRequest(){
		return this.getCaseDetailsRequestType;
	}

	/**
	 * Converts the JSON Object to CloseCaseRequestWrapper object
	 * 
	 * @param obj - JSON representation for CloseCaseRequestWrapper
	 * @return CloseCaseRequestWrapper Object
	 */
	public static GetCaseDetailsRequestWrapper fromJSONObject(JSONObject obj) throws HubServiceException {
		GetCaseDetailsRequestWrapper wrapper = new GetCaseDetailsRequestWrapper();
		wrapper.setCaseNbr((String)obj.get(CASE_NUMBER));
		return wrapper;
	}
	
	/**
	 * Converts the JSON String to CloseCaseRequestWrapper object
	 * 
	 * @param jsonString - String representation for CloseCaseRequestWrapper
	 * @return CloseCaseRequestWrapper Object
	 */
	public static GetCaseDetailsRequestWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create CloseCaseRequestWrapper from null or empty input");
		}

		Object tmpObj = null;
		
		GetCaseDetailsRequestWrapper wrapper = new GetCaseDetailsRequestWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		tmpObj = obj.get(CASE_NUMBER);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create CloseCaseRequestWrapper, No case number found");
		}
		wrapper.caseNbr = (String)tmpObj;
		wrapper.getCaseDetailsRequestType.setCaseNumber(wrapper.caseNbr);
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNbr);
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNbr);
		return obj;
	}
}
