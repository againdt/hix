(function(){
	
	angular.module('verificationResultDashboardApp')
	.factory("fileUpload", fileUpload);
	
	fileUpload.$inject = ['$http'];
	
	function fileUpload($http){
		return {
			uploadFileAndFields: function(fileName, file, fields, uploadUrl){
				var fd = new FormData();
				fd.append(fileName, file);
			    
			    if(fields != undefined){
			    	for(var i = 0; i < fields.length; i++){
			            fd.append(fields[i].name, fields[i].data);
			        };
			    }
			    
			   return  $http.post(uploadUrl, fd, {
			        transformRequest: angular.identity,
			        headers: {'Content-Type': undefined}
			    });
			}
		};
	}

	
})();



