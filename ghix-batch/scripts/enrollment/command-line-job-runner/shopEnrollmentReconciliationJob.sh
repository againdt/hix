#!/bin/bash
# run this script as root
cd /home/jboss/default/standalone/deployments/ghix-batch/WEB-INF/classes
java -cp ".:../lib/*" org.springframework.batch.core.launch.support.CommandLineJobRunner file:./META-INF/spring/batch/jobs/enrollment/commandlinejobrunner/shopEnrollmentReconciliationJobCL.xml shopEnrollmentReconciliationJob
