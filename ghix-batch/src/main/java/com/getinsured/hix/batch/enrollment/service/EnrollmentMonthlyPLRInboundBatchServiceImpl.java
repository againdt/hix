/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.repository.IEnrollmentPLRInRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrollmentPLRIn;
import com.getinsured.hix.platform.util.DateUtil;

/**
 * @author negi_s
 *
 */
@Service("enrollmentMonthlyPLRInboundBatchService")
@Transactional
public class EnrollmentMonthlyPLRInboundBatchServiceImpl implements EnrollmentMonthlyPLRInboundBatchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLRInboundBatchServiceImpl.class);
	
	@Autowired private IEnrollmentPLRInRepository enrollmentPLRInRepository;

	@Override
	public void processPlrInFile(Long jobId) {
		LOGGER.info("Processing PLR inbound files ::" + jobId);
		String wipFolderPath = moveAllInFileToWipFolder();
		if(null != wipFolderPath){
			processPlrInboundResponse(wipFolderPath, jobId);
		}
	}

	/**
	 * Processes and stores inbound data received
	 * @param wipFolderPath Location of inbound files
	 * @param jobId Batch job ID
	 */
	private void processPlrInboundResponse(String wipFolderPath, Long jobId) {
		File[] plrSdboFiles = EnrollmentUtils.getFilesInAFolderByName(new File(wipFolderPath).getAbsolutePath(),
				CmsSftpServiceImpl.PLR_INBOUND_FUNC_CODE.SDBO.toString());
		for(File sdboFile : plrSdboFiles){
			try{
				LOGGER.info("Processing PLR inbound files ::" + sdboFile.getName());
				List<EnrollmentPLRIn> enrollmentPLRInList = processCSVFile(sdboFile);
				saveEnrollmentPLRInbound(enrollmentPLRInList);
			}catch(Exception e){
				LOGGER.error("Error processing SDBO file :: " + sdboFile.getName(), e.getMessage());
			}
			moveToArchive(sdboFile, jobId);
		}
	}
	
	/**
	 * Persists data to the inbound table
	 * @param enrollmentPLRInList Data list to be saved
	 */
	private void saveEnrollmentPLRInbound(List<EnrollmentPLRIn> enrollmentPLRInList) {
		if(null != enrollmentPLRInList && !enrollmentPLRInList.isEmpty()){
			//Save response to table
			for(EnrollmentPLRIn obj : enrollmentPLRInList){
				try{
					enrollmentPLRInRepository.saveAndFlush(obj);
				}catch(Exception e){
					LOGGER.error("Error saving response record", e);
				}
			}
		}
	}

	/**
	 * Processes the CSV data received
	 * @param sdboFile Inbound file
	 * @return List<EnrollmentPLRIn> Processed data list
	 * @throws IOException
	 */
	private List<EnrollmentPLRIn> processCSVFile(File sdboFile) throws IOException {
		List<EnrollmentPLRIn> enrollmentPLRInList = new ArrayList<>();
		if (null != sdboFile && sdboFile.isFile()) {
			List<String> csvRecord = Files.readAllLines(sdboFile.toPath(), Charset.defaultCharset());
			if (csvRecord.size() > 1) {
				int counter = 0;
				for (String delimitedString : csvRecord) {
					if (counter++ == 0) {
						continue;
					}
					enrollmentPLRInList.add(populateInboundRecord(delimitedString));
				}
//				System.out.println(enrollmentPLRInList);
			} else {
				LOGGER.debug("No Inbound Data found in file: " + sdboFile.getName());
			}
		}
		return enrollmentPLRInList;
	}

	/**
	 * Populates the inbound record with CSV record
	 * @param delimitedString CSV record
	 * @return EnrollmentPLRIn Inbound record
	 */
	private EnrollmentPLRIn populateInboundRecord(String delimitedString) {
		String[] record = delimitedString.split("\\|");
		EnrollmentPLRIn enrollmentPLRIn = new EnrollmentPLRIn();
		if (null != record && record.length == 7) {
			enrollmentPLRIn.setDomainName(record[EnrollmentConstants.ZERO]);
			enrollmentPLRIn.setSubDomainName(record[EnrollmentConstants.ONE]);
			enrollmentPLRIn.setPlrXmlFileName(record[EnrollmentConstants.TWO]);
			enrollmentPLRIn.setPlrPackageName(record[EnrollmentConstants.THREE]);
			enrollmentPLRIn.setErrorCodeIdentifier(record[EnrollmentConstants.FOUR]);
			if (null != record[EnrollmentConstants.FIVE]) {
				String commentDtl = record[EnrollmentConstants.FIVE].trim();
				int firstIndexSpace = commentDtl.indexOf(" ");
				if (NumberUtils.isDigits(commentDtl.substring(0, firstIndexSpace))) {
					enrollmentPLRIn.setPolicyNumber(Integer.valueOf(commentDtl.substring(0, firstIndexSpace)));
					enrollmentPLRIn.setFileValidationCommentDtl(commentDtl.substring(firstIndexSpace + 1, commentDtl.length()));
				} else {
					enrollmentPLRIn.setFileValidationCommentDtl(commentDtl);
				}
			}
			enrollmentPLRIn.setEtlLoadDate(DateUtil.StringToDate(record[EnrollmentConstants.SIX], "yyyy-MM-dd"));
		}else if(null != record ){
			LOGGER.debug("Inbound record not as per specifications. Expected columns :: 7, Actual columns received ::" +  record.length);
		}else{
			LOGGER.debug("Inbound record is null or empty");
		}
		return enrollmentPLRIn;
	}

	/**
	 * Moves inbound files to WIP path for processing
	 * @return String WIP path
	 */
	private String moveAllInFileToWipFolder() {
		StringBuilder plrInboundPathBuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.ReportType.PLR.toString(), EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = plrInboundPathBuilder.toString();
		File dir = new File(inboundDir);
		// Move all files from inboundCMSXMLPath to WIP folder
		String wipFolderPath = inboundDir + File.separator + EnrollmentConstants.WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPath);
		boolean wipPathValid = EnrollmentUtils.createDirectory(wipFolderPath);
		if (wipPathValid) {
			// Move files from inboundDir to WIP folder
			if (dir.isDirectory()) {
				File[] files = dir.listFiles();
				for (File file : files) {
					if (file.isFile() && file.getName().endsWith(EnrollmentConstants.FILE_TYPE_OUT)
							&& file.getName().contains(CmsSftpServiceImpl.PLR_INBOUND_FUNC_CODE.SDBO.toString())) {
						File wipFile = new File(wipFolderPath + File.separator + file.getName());
						file.renameTo(wipFile);
					}
				}
			}
			if (wipFolder.list() != null && wipFolder.list().length > 0) {
				return wipFolderPath;
			} else {
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Moves the inbound file received to the archive location
	 * @param fileToArchive Inbound file to be archived
	 * @param jobId Batch job ID
	 */
	private void moveToArchive(File fileToArchive, Long jobId) {
		String plrMonthlyResponseArchivePath = EnrollmentUtils
				.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.PLR.toString(),
						EnrollmentConstants.TRANSFER_DIRECTION_IN)
				.append(File.separator).append(EnrollmentConstants.ARCHIVE_FOLDER).append(File.separator).append(jobId)
				.toString();
		try{
			EnrollmentUtils.createDirectory(plrMonthlyResponseArchivePath);
			if (fileToArchive!=null && new File(plrMonthlyResponseArchivePath).exists()) {
				//move all files from source to target folder
				fileToArchive.renameTo(new File(plrMonthlyResponseArchivePath+File.separator+fileToArchive.getName()));
			}
		}catch(Exception e){
			LOGGER.error("Error in moveFiles()"+e.toString(),e);
		}
	}
}
