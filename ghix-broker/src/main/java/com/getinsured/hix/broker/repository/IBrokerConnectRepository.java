package com.getinsured.hix.broker.repository;

import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerConnect;
import org.springframework.data.jpa.repository.JpaRepository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IBrokerConnectRepository extends JpaRepository<BrokerConnect, Integer> {

    @Query("FROM BrokerConnect WHERE brokerId = :broker")
    BrokerConnect findBrokerConnectById(@Param("broker") Broker broker);
}
