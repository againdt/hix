package com.getinsured.eligibility.reset.password.controller;

import java.util.List;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.getinsured.eligibility.reset.password.dto.Question;
import com.getinsured.eligibility.reset.password.dto.Questions;
import com.getinsured.eligibility.reset.password.util.ConsumerUtil;
import com.getinsured.hix.model.AccountUser;

@Controller
@RequestMapping(ResetPasswordController.API_BOT)
@Validated
public class ResetPasswordController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ResetPasswordController.class);

	private static final String VERIFY_ACCOUNT = "/verify";
	private static final String GENERATE_AUTHENTICATION_QS = "/{accountId}/questions";
	private static final String VALIDATE_AUTHENTICATION_QS = "/{accountId}/questions/validate";
	private static final String GENERATE_PASSWORD_LINK = "/{accountId}/resetlink";
	private static final String LOCK_ACCOUNT = "/{accountId}/lock";
	static final String API_BOT = "/resetpassword/account";

	
	@Autowired
	ConsumerUtil consumerUtil;

	@RequestMapping(value = VERIFY_ACCOUNT, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Integer> verifyAccount(
			@RequestParam(value = "email", required = false) @Email String email,
			@RequestParam(value = "phone", required = false) @Pattern(regexp = "(^$|[0-9]{10})", message = "should be 10 digit number.") String phone)
			{
		
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("inside verify Account");
		}
		AccountUser verifiedAccount = null;
		AccountUser user = consumerUtil.findAccount(phone, email);
		if(user != null){
			verifiedAccount = consumerUtil.accountWithApplication(user);
			if(verifiedAccount != null){
				if(verifiedAccount.getConfirmed() == 1 && verifiedAccount.getStatus().equalsIgnoreCase("Active") && !consumerUtil.verifyIfAccountLocked(verifiedAccount)){
					return new ResponseEntity<Integer>(verifiedAccount.getId(), HttpStatus.OK);
				}
				return new ResponseEntity<Integer>(verifiedAccount.getId(), HttpStatus.FOUND);
			}else{
				return new ResponseEntity<Integer>(HttpStatus.UNPROCESSABLE_ENTITY);
			}
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@RequestMapping(value = GENERATE_AUTHENTICATION_QS, method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Questions> generateAuthenticationQuestions(@PathVariable @NotNull @Min(1) final Integer accountId) {
		Questions questions = new Questions();
		List<Question> questionList = null;
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("inside generate authentication qs");
		}
		
		questionList = consumerUtil.getResetPasswordQuestions(accountId);
		if(questionList != null){
			questions.setQuestionList(questionList);
			return new ResponseEntity<>(questions, HttpStatus.OK);
		}
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@RequestMapping(value = VALIDATE_AUTHENTICATION_QS, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Boolean> validateAuthenticationQuestions(@PathVariable @NotNull @Min(1) final Integer accountId,
			@RequestBody @NotNull @Validated final Questions questions) {

		Boolean responseJson = false;
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("inside consumer authentication");
		}

		responseJson = consumerUtil.validateResetPasswordQuestions(accountId, questions);
		if(responseJson){
			return new ResponseEntity<>(responseJson, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseJson, HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@RequestMapping(value = GENERATE_PASSWORD_LINK, method = RequestMethod.POST)
	public HttpEntity<String> generatePasswordResetLink(@PathVariable @NotNull @Min(1) final Integer accountId, @RequestBody @NotNull String channelType) {
		String responseJson = null;
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("inside generate password link");
		}
		responseJson = consumerUtil.generateResetLink(accountId, channelType);
		if(!responseJson.isEmpty()){
			return new ResponseEntity<>(responseJson, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseJson, HttpStatus.INTERNAL_SERVER_ERROR);

	}
	
	@RequestMapping(value = LOCK_ACCOUNT, method = RequestMethod.POST)
	public HttpEntity<Boolean> lockConsumerAccount(@PathVariable @NotNull @Min(1) final Integer accountId, @RequestBody @NotNull String channelType) {
		boolean responseJson = false;
		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("inside lock consumer account");
		}
		responseJson = consumerUtil.lockAccount(accountId, channelType);
		if(responseJson){
			return new ResponseEntity<>(responseJson, HttpStatus.OK);
		}
		return new ResponseEntity<>(responseJson, HttpStatus.INTERNAL_SERVER_ERROR);

	}
}
