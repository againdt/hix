package com.getinsured.eligibility.at.ref.transform;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.indportal.customGrouping.CustomGroupingController.EligibilityType;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralUtil;
/**
 * @author chopra_s
 * 
 */
@Component("compareApplicationTransformer")
@Scope("singleton")
public class CompareApplicationTransformer {
	@Autowired private IEligibilityProgram eligibilityProgramRepository;

	public CompareApplicationDTO transformApplication(SsapApplication ssapApplication, boolean demographic) {
		final CompareApplicationDTO compareApplicationDTO = new CompareApplicationDTO();
		ReferralUtil.copyProperties(ssapApplication, compareApplicationDTO);
		transformApplicants(compareApplicationDTO, ssapApplication);
		transformDataFromJson(compareApplicationDTO, demographic);
		return compareApplicationDTO;
	}

	private void transformDataFromJson(CompareApplicationDTO compareApplicationDTO, boolean demographic) {
		CompareJsonTransformer.transformDataFromJson(compareApplicationDTO, demographic);
	}

	private void transformApplicants(CompareApplicationDTO compareApplicationDTO, SsapApplication ssapApplication) {
		CompareApplicantDTO compareApplicantDTO;
		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			compareApplicantDTO = new CompareApplicantDTO();
			ReferralUtil.copyProperties(ssapApplicant, compareApplicantDTO);
			compareApplicationDTO.addApplicant(compareApplicantDTO);
			this.setEligibilityDetails(compareApplicantDTO, ssapApplicant);
		}
		
	}

	private void setEligibilityDetails(CompareApplicantDTO compareApplicantDTO, SsapApplicant ssapApplicant) {
		List<EligibilityProgram> programs = eligibilityProgramRepository.getApplicantEligibilities(ssapApplicant.getId());
		compareApplicantDTO.setAPTCEligible(this.checkEligibility(programs, EligibilityType.APTC_ELIGIBILITY_TYPE));
		compareApplicantDTO.setCSREligible(this.checkEligibility(programs, EligibilityType.CSR_ELIGIBILITY_TYPE));
		compareApplicantDTO.setExchangeEligible(this.checkEligibility(programs, EligibilityType.EXCHANGE_ELIGIBILITY_TYPE));
	}
	
	private boolean checkEligibility(List<EligibilityProgram> programs,EligibilityType eligibilityType) 
	{
		if(programs != null && programs.size() > 0) {
			for(EligibilityProgram eligibilityProgram : programs){
				if (eligibilityType.toString().equals(eligibilityProgram.getEligibilityType()) && "TRUE".equals(eligibilityProgram.getEligibilityIndicator())){	
					return true;
				}
			}
		}
		return false;
	}
}
