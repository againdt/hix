package com.getinsured.iex.hub.vlp.wrapper;


import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfBenefitsListArrayType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfEmpAuthDataType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ArrayOfSponsorshipDataType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsResponseSetType;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Wrapper Class for GetCaseDetailsResponseWrapper
 * 
 * @author Vishaka Tharani
 * @since 17-Feb-2014
 */

public class GetCaseDetailsResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GetCaseDetailsResponseSetTypeWrapper.class);
	
	private static final String DATE_FORMAT = "yyyy-MM-dd";
	
    private String initoGrantDate;
    private String initoGrantDateReasonCd;
    private String addiiAlienNumber;
    private String addiiReceiptNumber;
    private String addiiPocFirstLastName;
    private String addiiPocPhoneNumber;
    private String addiiPocPhoneNbrExt;
    private String addioGrantDate;
    private String addioParoleeExpiredDate;
    private String thirdiAlienNumber;
    private String thirdiI94Number;
    private String thirdiReceiptNumber;
    private String thirdiPassportNumber;
    private String thirdiCountryOfIssuance;
    private String thirdiCountryOfIssuanceDesc;
    private String thirdiAliasName;
    private String thirdiComments;
    private String thirdiPocFirstLastName;
    private String thirdiPocPhoneNumber;
    private String thirdiPocPhoneNbrExt;
    private String thirdiPocAddress1;
    private String thirdiPocAddress2;
    private String thirdiPocCity;
    private String thirdiPocStateCode;
    private String thirdiPocZipCode;
    private String thirdoParoleeExpiredDate;
    private String thirdoGrantDate;
    private Boolean photoIncludedIndicator;
    private byte[] photoBinaryAttachment;
    private byte[] upLoadedDocBinaryAttachment;

	private String caseNumber;
    private String currentState;
    private String initiAlienNumber;
    private String initiI94Number;
    private String initiSevisId;
    private String initiPassportNumber;
    private String initiCountryOfIssuance;
    private String initiCountryOfIssuanceDesc;
    private String initiVisaNumber;
    private String initiReceiptNumber;
    private String initiNaturalizationNumber;
    private String initiCitizenshipNumber;
    private String initiQueryUserId;
    private String initiQueryDate;
    private String initiDocLastName;
    private String initiDocFirstName;
    private String initiDocMiddleName;
    private String initiDocBirthDate;
    private String initiDocumentId;
    private String initiDocType;
    private String initiDocOtherDesc;
    private String initiDocExpDate;
    private String initoLastName;
    private String initoFirstName;
    private String initoMiddleName;
    private String initoCoa;
    private String initoEadsExpireDate;
    private String initoBirthCountryCode;
    private String initoBirthCountryDescr;
    private String initoBirthDate;
    private String initoEntryDate;
    private String initoAdmittedToDate;
    private String initoAdmittedToText;
    private String initoEligStatementCode;
    private String initoEligStatementText;
    private String initoIAVTypeCode;
    private String initoIAVTypeTxt;
    private String addiiI94Number;
    private String addiiSpecialComments;
    private String addiiQueryUserId;
    private String addiiQueryDate;
    private Boolean addiiRequestSponsorDataIndicator;
    private Boolean addiiRequestEmpAuthDataIndicator;
    private String addiiPassportNumber;
    private String addiiCountryOfIssuance;
    private String addiiCountryOfIssuanceDesc;
    private String addioDhsRespDate;
    private String addioG845VersionId;
    private String addioG845MajorCd;
    private String addioMajorStatement;
    private String addioG845MinorCd;
    private String addioMinorStatement;
    private String addioExpiresDate;
    private String addioCoaCode;
    private String addioRevocationDate;
    private String addioAdmittedToDate;
    private String addioAdmittedToText;
    private String addioPendStatusDesc;
    private Boolean addioSponsorIndicator;
    private String addioServiceReceiptDate;
    private String addioLprStatusDate;
    private Boolean addioNoEmpAuthDataIndicator;
    private String addioDhsComments;
    private String thirdiCreatorUserId;
    private String thirdiCreatedDate;
    private String thirdoDhsRespDate;
    private String thirdoG845VersionId;
    private String thirdoG845MajorCd;
    private String thirdoMajorStatement;
    private String thirdoG845MinorCd;
    private String thirdoMinorStatement;
    private String thirdoExpiresDate;
    private String thirdoCoaCode;
    private String thirdoRevocationDate;
    private String thirdoAdmittedToDate;
    private String thirdoAdmittedToText;
    private String thirdoPendStatusDesc;
    private Boolean thirdoSponsorIndicator;
    private String thirdoServiceReceiptDate;
    private String thirdoLprStatusDate;
  
    private Boolean thirdoNoEmpAuthDataIndicator;
 
    private String thirdoDhsComments;
    private String closoBy;
    private String closoDate;
    private String webServSftwrVer;
    
    private ArrayOfSponsorshipDataTypeGCDWrapper addioSponsorshipData;
    private ArrayOfEmpAuthDataTypeWrapper addioEmpAuthData;
    private ArrayOfSponsorshipDataTypeGCDWrapper thirdoSponsorshipData;
    private ArrayOfEmpAuthDataTypeWrapper thirdoEmpAuthData;
    private ArrayOfBenefitsListArrayTypeWrapper initiBenefitsArray; 
    
    
	public GetCaseDetailsResponseSetTypeWrapper(GetCaseDetailsResponseSetType resultType){
		
		if (resultType == null){
    		return;
    	}
		try {
		caseNumber = resultType.getCaseNumber();                                      
	    currentState = resultType.getCurrentState();                                 
	    initiAlienNumber = resultType.getInitiAlienNumber();                             
	    initiI94Number = resultType.getInitiI94Number();                               
	    initiSevisId = resultType.getInitiSevisId();                                 
	    initiPassportNumber = resultType.getInitiPassportNumber();                          
	    initiCountryOfIssuance = resultType.getInitiCountryOfIssuance();                       
	    initiCountryOfIssuanceDesc = resultType.getInitiCountryOfIssuanceDesc();                   
	    initiVisaNumber = resultType.getInitiVisaNumber();                              
	    initiReceiptNumber = resultType.getInitiReceiptNumber();                           
	    initiNaturalizationNumber = resultType.getInitiNaturalizationNumber();                    
	    initiCitizenshipNumber = resultType.getInitiCitizenshipNumber();                       
	    initiQueryUserId = resultType.getInitiQueryUserId();                             
	    XMLGregorianCalendar calendar = resultType.getInitiQueryDate();  
	    
	    if(calendar != null)
	    {
	    	initiQueryDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }
	    
	    initiDocLastName = resultType.getInitiDocLastName();                             
	    initiDocFirstName = resultType.getInitiDocFirstName();                            
	    initiDocMiddleName = resultType.getInitiDocMiddleName();           
	    
	    initiDocumentId = resultType.getInitiDocumentId();                              
	    initiDocType = resultType.getInitiDocType();                                 
	    initiDocOtherDesc = resultType.getInitiDocOtherDesc();     
	    
	    calendar = resultType.getInitiDocBirthDate();  

	    if(calendar != null)
	    {
	    	initiDocBirthDate =  PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }

	    calendar = resultType.getInitiDocBirthDate();  

	    if(calendar != null)
	    {
	    	initiDocBirthDate =  PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }

	    calendar = resultType.getInitiDocExpDate();  

	    if(calendar != null)
	    {
	    	initiDocExpDate =  PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }

	    calendar = resultType.getInitoEadsExpireDate();

	    if(calendar != null)
	    {
	    	initoEadsExpireDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);  
	    }

	    calendar = resultType.getInitoBirthDate();

	    if(calendar != null)
	    {
	    	initoBirthDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getInitoEntryDate();  

	    if(calendar != null)
	    {
	    	initoEntryDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getInitoAdmittedToDate(); 

	    if(calendar != null)
	    {
	    	initoAdmittedToDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT); 
	    }

	    calendar = resultType.getAddiiQueryDate();

	    if(calendar != null)
	    {
	    	addiiQueryDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getAddioDhsRespDate();

	    if(calendar != null)
	    {
	    	addioDhsRespDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getAddioRevocationDate();

	    if(calendar != null)
	    {
	    	addioRevocationDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getAddioAdmittedToDate();

	    if(calendar != null)
	    {
	    	addioAdmittedToDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getAddioExpiresDate();

	    if(calendar != null)
	    {
	    	addioExpiresDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);     
	    }

	    calendar = resultType.getAddioServiceReceiptDate();

	    if(calendar != null)
	    {
	    	addioServiceReceiptDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getAddioLprStatusDate();

	    if(calendar != null)
	    {
	    	addioLprStatusDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getThirdoExpiresDate();

	    if(calendar != null)
	    {
	    	thirdoExpiresDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getThirdiCreatedDate();

	    if(calendar != null)
	    {
	    	thirdiCreatedDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getThirdoDhsRespDate();

	    if(calendar != null)
	    {
	    	thirdoDhsRespDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getThirdoRevocationDate();

	    if(calendar != null)
	    {
	    	thirdoRevocationDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);   
	    }

	    calendar = resultType.getThirdoAdmittedToDate();

	    if(calendar != null)
	    {
	    	thirdoAdmittedToDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);  
	    }

	    calendar = resultType.getThirdoServiceReceiptDate();

	    if(calendar != null)
	    {
	    	thirdoServiceReceiptDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);  
	    }

	    calendar = resultType.getThirdoLprStatusDate();

	    if(calendar != null)
	    {
	    	thirdoLprStatusDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);  
	    }
	    
	    initoLastName = resultType.getInitoLastName();                                
	    initoFirstName = resultType.getInitoFirstName();                               
	    initoMiddleName = resultType.getInitoMiddleName();                              
	    initoCoa = resultType.getInitoCoa();     
	    
        initoBirthCountryCode = resultType.getInitoBirthCountryCode();                        
	    initoBirthCountryDescr = resultType.getInitoBirthCountryDescr();   
	                              
	    initoAdmittedToText = resultType.getInitoAdmittedToText();                          
	    initoEligStatementCode = resultType.getInitoEligStatementCode();                       
	    initoEligStatementText = resultType.getInitoEligStatementText();                       
	    initoIAVTypeCode = resultType.getInitoIAVTypeCode();                             
	    initoIAVTypeTxt = resultType.getInitoIAVTypeTxt();                              
	    addiiI94Number = resultType.getAddiiI94Number();                               
	    addiiSpecialComments = resultType.getAddiiSpecialComments();                         
	    addiiQueryUserId = resultType.getAddiiQueryUserId();      
	    
	    addiiRequestSponsorDataIndicator = resultType.isAddiiRequestSponsorDataIndicator();                   
	    addiiRequestEmpAuthDataIndicator = resultType.isAddiiRequestEmpAuthDataIndicator();   
	    
	    addiiPassportNumber = resultType.getAddiiPassportNumber();                          
	    addiiCountryOfIssuance = resultType.getAddiiCountryOfIssuance();                       
	    addiiCountryOfIssuanceDesc = resultType.getAddiiCountryOfIssuanceDesc();   
	    
	    addioG845VersionId = resultType.getAddioG845VersionId();                           
	    addioG845MajorCd = resultType.getAddioG845MajorCd();                             
	    addioMajorStatement = resultType.getAddioMajorStatement();                          
	    addioG845MinorCd = resultType.getAddioG845MinorCd();                             
	    addioMinorStatement = resultType.getAddioMinorStatement();       
	    
	    addioCoaCode = resultType.getAddioCoaCode();          
	    
	    addioAdmittedToText = resultType.getAddioAdmittedToText();                          
	    addioPendStatusDesc = resultType.getAddioPendStatusDesc();                          
	    addioSponsorIndicator = resultType.isAddioSponsorIndicator();   
	    
	    addioNoEmpAuthDataIndicator = resultType.isAddioNoEmpAuthDataIndicator();                        
	    addioDhsComments = resultType.getAddioDhsComments();                             
	    thirdiCreatorUserId = resultType.getThirdiCreatorUserId();  
	    
	    thirdoG845VersionId = resultType.getThirdoG845VersionId();                          
	    thirdoG845MajorCd = resultType.getThirdoG845MajorCd();                            
	    thirdoMajorStatement = resultType.getThirdoMajorStatement();                         
	    thirdoG845MinorCd = resultType.getThirdoG845MinorCd();                            
	    thirdoMinorStatement = resultType.getThirdoMinorStatement();   
	    
	    thirdoCoaCode = resultType.getThirdoCoaCode();    
	    
	    thirdoAdmittedToText = resultType.getThirdoAdmittedToText();                         
	    thirdoPendStatusDesc = resultType.getThirdoPendStatusDesc();                         
	    thirdoSponsorIndicator = resultType.isThirdoSponsorIndicator();      
	    
	    thirdoNoEmpAuthDataIndicator = resultType.isThirdoNoEmpAuthDataIndicator();                       
	                                                
	    thirdoDhsComments = resultType.getThirdoDhsComments();                            
	    closoBy = resultType.getClosoBy();                                      
	    closoDate = resultType.getClosoDate();                                    
	    webServSftwrVer = resultType.getWebServSftwrVer();   
	    
	    
	    initoGrantDateReasonCd = resultType.getInitoGrantDateReasonCd();
	    addiiAlienNumber = resultType.getAddiiAlienNumber();
	    addiiReceiptNumber = resultType.getAddiiReceiptNumber();
	    addiiPocFirstLastName = resultType.getAddiiPocFirstLastName();
	    addiiPocPhoneNumber = resultType.getAddiiPocPhoneNumber();
	    addiiPocPhoneNbrExt = resultType.getAddiiPocPhoneNbrExt();
	   
	    
	    thirdiAlienNumber = resultType.getThirdiAlienNumber();
	    thirdiI94Number = resultType.getThirdiI94Number();
	    thirdiReceiptNumber = resultType.getThirdiReceiptNumber();
	    thirdiPassportNumber = resultType.getThirdiPassportNumber();
	    thirdiCountryOfIssuance = resultType.getThirdiCountryOfIssuance();
	    thirdiCountryOfIssuanceDesc = resultType.getThirdiCountryOfIssuanceDesc();
	    thirdiAliasName = resultType.getThirdiAliasName();
	    thirdiComments = resultType.getThirdiComments();
	    thirdiPocFirstLastName = resultType.getThirdiPocFirstLastName();
	    thirdiPocPhoneNumber = resultType.getThirdiPocPhoneNumber();
	    thirdiPocPhoneNbrExt = resultType.getThirdiPocPhoneNbrExt();
	    thirdiPocAddress1 = resultType.getThirdiPocAddress1();
	    thirdiPocAddress2 = resultType.getThirdiPocAddress2();
	    thirdiPocCity = resultType.getThirdiPocCity();
	    thirdiPocStateCode = resultType.getThirdiPocStateCode();
	    thirdiPocZipCode = resultType.getThirdiPocZipCode();
	    
	    calendar = resultType.getInitiDocBirthDate();  

	    calendar = resultType.getInitoGrantDate();
	    if(calendar != null)
	    {
	    	initoGrantDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT); 
	    }
	    calendar = resultType.getAddioGrantDate();
	    if(calendar != null)
	    {
	    	addioGrantDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }
	    calendar = resultType.getAddioParoleeExpiredDate();
	    if(calendar != null)
	    {
	    	addioParoleeExpiredDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT);
	    }
	    calendar = resultType.getThirdoParoleeExpiredDate();
	    if(calendar != null)
	    {
	    	thirdoParoleeExpiredDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT); 
	    }
	    calendar = resultType.getThirdoGrantDate();
	    if(calendar != null)
	    {
	    	thirdoGrantDate = PlatformServiceUtil.xmlDateToString(calendar, DATE_FORMAT); 
	    }

	    
	    photoIncludedIndicator = resultType.isPhotoIncludedIndicator();
	    photoBinaryAttachment = resultType.getPhotoBinaryAttachment();
	    upLoadedDocBinaryAttachment = resultType.getUpLoadedDocBinaryAttachment();
	    
	    ArrayOfSponsorshipDataType addioSponsorshipDataType = resultType.getAddioSponsorshipData();
	    
	    if( addioSponsorshipDataType != null)
	    {
	    	addioSponsorshipData = new ArrayOfSponsorshipDataTypeGCDWrapper(addioSponsorshipDataType);
	    }
	    
	    ArrayOfSponsorshipDataType thirdoSponsorshipDataType = resultType.getThirdoSponsorshipData();
        if(thirdoSponsorshipDataType != null)
        {
        	thirdoSponsorshipData = new ArrayOfSponsorshipDataTypeGCDWrapper(thirdoSponsorshipDataType);
        }
        
        ArrayOfEmpAuthDataType addioEmpAuthDataType = resultType.getAddioEmpAuthData();
	    
        if(addioEmpAuthDataType != null)
        {
        	addioEmpAuthData = new ArrayOfEmpAuthDataTypeWrapper(addioEmpAuthDataType);
        }
        
        ArrayOfEmpAuthDataType thirdoEmpAuthDataType = resultType.getThirdoEmpAuthData();
        
        if(thirdoEmpAuthDataType != null)
        {
        	thirdoEmpAuthData = new ArrayOfEmpAuthDataTypeWrapper(thirdoEmpAuthDataType);
        }
        
        ArrayOfBenefitsListArrayType initiBenefitsArrayType =  resultType.getInitiBenefitsArray();
        if(initiBenefitsArrayType != null)
        {
        	initiBenefitsArray = new ArrayOfBenefitsListArrayTypeWrapper(initiBenefitsArrayType);
        }
	} 
	catch (Exception e) {
		LOGGER.error(e.getMessage(),e);
	}
	}
    
	public String getCaseNumber(){
		return caseNumber;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
	     
	    obj.put("caseNumber", this.caseNumber);
	    obj.put("currentState", this.currentState);
	    obj.put("initiAlienNumber", this.initiAlienNumber);
	    obj.put("initiI94Number", this.initiI94Number);
	    obj.put("initiSevisId", this.initiSevisId);
	    obj.put("initiPassportNumber", this.initiPassportNumber);
	    obj.put("initiCountryOfIssuance", this.initiCountryOfIssuance);
	    obj.put("initiCountryOfIssuanceDesc", this.initiCountryOfIssuanceDesc);
	    obj.put("initiVisaNumber", this.initiVisaNumber);
	    obj.put("initiReceiptNumber", this.initiReceiptNumber);
	    obj.put("initiNaturalizationNumber", this.initiNaturalizationNumber);
	    obj.put("initiCitizenshipNumber", this.initiCitizenshipNumber);
	    obj.put("initiBenefitsArray", this.initiBenefitsArray);
	    obj.put("initiQueryUserId", this.initiQueryUserId);
	    obj.put("initiQueryDate", this.initiQueryDate);
	    obj.put("initiDocLastName", this.initiDocLastName);
	    obj.put("initiDocFirstName", this.initiDocFirstName);
	    obj.put("initiDocMiddleName", this.initiDocMiddleName);
	    obj.put("initiDocBirthDate", this.initiDocBirthDate);
	    obj.put("initiDocumentId", this.initiDocumentId);
	    obj.put("initiDocType", this.initiDocType);
	    obj.put("initiDocOtherDesc", this.initiDocOtherDesc);
	    obj.put("initiDocExpDate", this.initiDocExpDate);
	    obj.put("initoLastName", this.initoLastName);
	    obj.put("initoFirstName", this.initoFirstName);
	    obj.put("initoMiddleName", this.initoMiddleName);
	    obj.put("initoCoa", this.initoCoa);
	    obj.put("initoEadsExpireDate", this.initoEadsExpireDate);
	    obj.put("initoBirthCountryCode", this.initoBirthCountryCode);
	    obj.put("initoBirthCountryDescr", this.initoBirthCountryDescr);
	    obj.put("initoBirthDate", this.initoBirthDate);
	    obj.put("initoEntryDate", this.initoEntryDate);
	    obj.put("initoAdmittedToDate", this.initoAdmittedToDate);
	    obj.put("initoAdmittedToText", this.initoAdmittedToText);
	    obj.put("initoEligStatementCode", this.initoEligStatementCode);
	    obj.put("initoEligStatementText", this.initoEligStatementText);
	    obj.put("initoIAVTypeCode", this.initoIAVTypeCode);
	    obj.put("initoIAVTypeTxt", this.initoIAVTypeTxt);
	    obj.put("initoGrantDate", this.initoGrantDate);
	    obj.put("initoGrantDateReasonCd", this.initoGrantDateReasonCd);
	    obj.put("addiiI94Number", this.addiiI94Number);
	    obj.put("addiiSpecialComments", this.addiiSpecialComments);
	    obj.put("addiiQueryUserId", this.addiiQueryUserId);
	    obj.put("addiiQueryDate", this.addiiQueryDate);
	    obj.put("addiiRequestSponsorDataIndicator", this.addiiRequestSponsorDataIndicator);
	    obj.put("addiiRequestEmpAuthDataIndicator", this.addiiRequestEmpAuthDataIndicator);
	    obj.put("addiiPassportNumber", this.addiiPassportNumber);
	    obj.put("addiiCountryOfIssuance", this.addiiCountryOfIssuance);
	    obj.put("addiiCountryOfIssuanceDesc", this.addiiCountryOfIssuanceDesc);
	    obj.put("addiiAlienNumber", this.addiiAlienNumber);
	    obj.put("addiiReceiptNumber", this.addiiReceiptNumber);
	    obj.put("addiiPocFirstLastName", this.addiiPocFirstLastName);
	    obj.put("addiiPocPhoneNumber", this.addiiPocPhoneNumber);
	    obj.put("addiiPocPhoneNbrExt", this.addiiPocPhoneNbrExt);
	    obj.put("addioDhsRespDate", this.addioDhsRespDate);
	    obj.put("addioG845VersionId", this.addioG845VersionId);
	    obj.put("addioG845MajorCd", this.addioG845MajorCd);
	    obj.put("addioMajorStatement", this.addioMajorStatement);
	    obj.put("addioG845MinorCd", this.addioG845MinorCd);
	    obj.put("addioMinorStatement", this.addioMinorStatement);
	    obj.put("addioExpiresDate", this.addioExpiresDate);
	    obj.put("addioCoaCode", this.addioCoaCode);
	    obj.put("addioRevocationDate", this.addioRevocationDate);
	    obj.put("addioAdmittedToDate", this.addioAdmittedToDate);
	    obj.put("addioAdmittedToText", this.addioAdmittedToText);
	    obj.put("addioPendStatusDesc", this.addioPendStatusDesc);
	    obj.put("addioSponsorIndicator", this.addioSponsorIndicator);
	    obj.put("addioServiceReceiptDate", this.addioServiceReceiptDate);
	    obj.put("addioLprStatusDate", this.addioLprStatusDate);
	    obj.put("addioSponsorshipData", this.addioSponsorshipData);
	    obj.put("addioNoEmpAuthDataIndicator", this.addioNoEmpAuthDataIndicator);
	    obj.put("addioEmpAuthData", this.addioEmpAuthData);
	    obj.put("addioDhsComments", this.addioDhsComments);
	    obj.put("addioGrantDate", this.addioGrantDate);
	    obj.put("addioParoleeExpiredDate", this.addioParoleeExpiredDate);
	    obj.put("thirdiCreatorUserId", this.thirdiCreatorUserId);
	    obj.put("thirdiCreatedDate", this.thirdiCreatedDate);
	    obj.put("thirdiAlienNumber", this.thirdiAlienNumber);
	    obj.put("thirdiI94Number", this.thirdiI94Number);
	    obj.put("thirdiReceiptNumber", this.thirdiReceiptNumber);
	    obj.put("thirdiPassportNumber", this.thirdiPassportNumber);
	    obj.put("thirdiCountryOfIssuance", this.thirdiCountryOfIssuance);
	    obj.put("thirdiCountryOfIssuanceDesc", this.thirdiCountryOfIssuanceDesc);
	    obj.put("thirdiAliasName", this.thirdiAliasName);
	    obj.put("thirdiComments", this.thirdiComments);
	    obj.put("thirdiPocFirstLastName", this.thirdiPocFirstLastName);
	    obj.put("thirdiPocPhoneNumber", this.thirdiPocPhoneNumber);
	    obj.put("thirdiPocPhoneNbrExt", this.thirdiPocPhoneNbrExt);
	    obj.put("thirdiPocAddress1", this.thirdiPocAddress1);
	    obj.put("thirdiPocAddress2", this.thirdiPocAddress2);
	    obj.put("thirdiPocCity", this.thirdiPocCity);
	    obj.put("thirdiPocStateCode", this.thirdiPocStateCode);
	    obj.put("thirdiPocZipCode", this.thirdiPocZipCode);
	    obj.put("thirdoDhsRespDate", this.thirdoDhsRespDate);
	    obj.put("thirdoG845VersionId", this.thirdoG845VersionId);
	    obj.put("thirdoG845MajorCd", this.thirdoG845MajorCd);
	    obj.put("thirdoMajorStatement", this.thirdoMajorStatement);
	    obj.put("thirdoG845MinorCd", this.thirdoG845MinorCd);
	    obj.put("thirdoMinorStatement", this.thirdoMinorStatement);
	    obj.put("thirdoExpiresDate", this.thirdoExpiresDate);
	    obj.put("thirdoCoaCode", this.thirdoCoaCode);
	    obj.put("thirdoRevocationDate", this.thirdoRevocationDate);
	    obj.put("thirdoAdmittedToDate", this.thirdoAdmittedToDate);
	    obj.put("thirdoAdmittedToText", this.thirdoAdmittedToText);
	    obj.put("thirdoPendStatusDesc", this.thirdoPendStatusDesc);
	    obj.put("thirdoSponsorIndicator", this.thirdoSponsorIndicator);
	    obj.put("thirdoServiceReceiptDate", this.thirdoServiceReceiptDate);
	    obj.put("thirdoLprStatusDate", this.thirdoLprStatusDate);
	    obj.put("thirdoSponsorshipData", this.thirdoSponsorshipData);
	    obj.put("thirdoNoEmpAuthDataIndicator", this.thirdoNoEmpAuthDataIndicator);
	    obj.put("thirdoEmpAuthData", this.thirdoEmpAuthData);
	    obj.put("thirdoDhsComments", this.thirdoDhsComments);
	    obj.put("thirdoParoleeExpiredDate", this.thirdoParoleeExpiredDate);
	    obj.put("thirdoGrantDate", this.thirdoGrantDate);
	    obj.put("photoIncludedIndicator", this.photoIncludedIndicator);
	    obj.put("photoBinaryAttachment", this.photoBinaryAttachment);
	    obj.put("upLoadedDocBinaryAttachment", this.upLoadedDocBinaryAttachment);
	    obj.put("closoBy", this.closoBy);
	    obj.put("closoDate", this.closoDate);
	    obj.put("webServSftwrVer", this.webServSftwrVer);
		return obj.toJSONString();
	}
}
