package com.getinsured.hix.indportal.dto.dm.application;

public class TaxHousehold {
	private AnnualIncome annualIncome;
	private MaxAPTC maxAPTC;
	private TaxHouseholdComposition taxHouseHoldComposition;


	 // Getter Methods 

	 public AnnualIncome getAnnualIncome() {
	  return annualIncome;
	 }

	 public MaxAPTC getMaxAPTC() {
	  return maxAPTC;
	 }
	 // Setter Methods 

	 public void setAnnualIncome(AnnualIncome annualIncome) {
	  this.annualIncome = annualIncome;
	 }

	 public void setMaxAPTC(MaxAPTC maxAPTC) {
	  this.maxAPTC = maxAPTC;
	 }

	public TaxHouseholdComposition getTaxHouseHoldComposition() {
		return taxHouseHoldComposition;
	}

	public void setTaxHouseHoldComposition(TaxHouseholdComposition taxHouseHoldComposition) {
		this.taxHouseHoldComposition = taxHouseHoldComposition;
	}

	}
