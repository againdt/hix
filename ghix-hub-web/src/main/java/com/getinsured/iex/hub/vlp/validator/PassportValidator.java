package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.wrapper.PassportCountryWrapper;

/**
 * Validator for passport details
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class PassportValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CountryOfIssuanceValidator.class);
	
	/** 
	 * Validations for passport details
	 * Passport country is required if passport number is input
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		PassportCountryWrapper passport = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		
		LOGGER.debug("Validating passport details");
		try{
			passport = PassportCountryWrapper.fromJsonString(value.toString());
				
			new PassportNumberValidator().validate("passportNumber", passport.getPassportNumber());
			new CountryOfIssuanceValidator().validate("countryOfIssuance", passport.getCountryOfIssuance());
				
			if(passport.getPassportNumber() != null &&
				(passport.getCountryOfIssuance() == null || passport.getCountryOfIssuance().trim().length() ==0)){
				throw new InputDataValidationException(VLPServiceConstants.PASSPORT_COUNTRY_MISSING_ERROR_MESSAGE);
			}
		}
		catch(ClassCastException e){
			throw new InputDataValidationException(VLPServiceConstants.INVALID_OBJECT_TYPE_FOR_PASSPORT_ERROR_MESSAGE + PassportCountryWrapper.class.getName(),e);
		}
		catch (HubServiceException e) {
			throw new InputDataValidationException(e.getMessage(),e);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
	
}
