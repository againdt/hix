<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li>Tickets</li>
        </ul><!--page-breadcrumb ends-->
            <h1>Tickets <small>101 total tickets.....</small></h1>
    </div>	
    
    <div class="row-fluid">
		<div id="sidebar" class="span3 ">
			<div class="gray">
            	<form class="form-vertical" id="frmsearchannouncement" name="frmsearchannouncement" action="manage" method="POST">
            	<df:csrfToken/>
                	<h4 class="margin0"><spring:message code='label.refineresult' /></h4>
                    <div class="gutter10 graybg">
                    
                    <div class="control-group">
                            <label for="status" class="control-label">Job Number</label>
                            <div class="controls">
                                <select  name="status" id="status" class="input-medium">
                                    <option value="">SELECT</option>
                                    <option ${searchCriteria.status == 'PENDING' ? 'selected="selected"' : ''} value="PENDING">PENDING</option>
                                    <option ${searchCriteria.status == 'APPROVED' ? 'selected="selected"' : ''} value="APPROVED">APPROVED</option>
                                    <option ${searchCriteria.status == 'DELETED' ? 'selected="selected"' : ''} value="DELETED">DELETED</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        <div class="control-group">
                            <label for="status" class="control-label">ATTRIBUTE 2</label>
                            <div class="controls">
                                <select  name="status" id="status" class="input-medium">
                                    <option value="">SELECT</option>
                                    <option ${searchCriteria.status == 'PENDING' ? 'selected="selected"' : ''} value="PENDING">PENDING</option>
                                    <option ${searchCriteria.status == 'APPROVED' ? 'selected="selected"' : ''} value="APPROVED">APPROVED</option>
                                    <option ${searchCriteria.status == 'DELETED' ? 'selected="selected"' : ''} value="DELETED">DELETED</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        <div class="control-group">
                            <label for="status" class="control-label">ATTRIBUTE 3</label>
                            <div class="controls">
                                <select  name="status" id="status" class="input-medium">
                                    <option value="">SELECT</option>
                                    <option ${searchCriteria.status == 'PENDING' ? 'selected="selected"' : ''} value="PENDING">PENDING</option>
                                    <option ${searchCriteria.status == 'APPROVED' ? 'selected="selected"' : ''} value="APPROVED">APPROVED</option>
                                    <option ${searchCriteria.status == 'DELETED' ? 'selected="selected"' : ''} value="DELETED">DELETED</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="effectiveStartDate" class="control-label">ATTRIBUTE 4</label>
                            <div class="controls">
                                <input type="text" title="MM/dd/yyyy" value="${searchCriteria.effectiveStartDate}" class="datepick input-medium"   name="effectiveStartDate" id="effectiveStartDate" />
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="effectiveStartDate" class="control-label">ATTRIBUTE 5</label>
                            <div class="controls">
                                <p><input type="checkbox" title="1" value="1" /> Value 1</p>
                                <p><input type="checkbox" title="1" value="1" /> Value 2</p>
                                <p><input type="checkbox" title="1" value="1" /> Value 3</p>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                     
                        
                        <div class="center">
                           <input type="submit" name="submitButton" id="submitButton" value="<spring:message  code="label.submit"/>" class="btn btn-primary" title="<spring:message  code="label.submit"/>"/>
                        </div>
                    </div>
                </form>
                <a name="addNewIssuer" id="addNewIssuer" class="btn btn-primary span11" href="#">New Ticket</a>
            </div>
		</div><!-- span3 ends-->
        
        <div class="span9 " id="rightpanel">
        	<form class="form-horizontal" id="frmManageQHP" name="frmManageQHP" method="POST">
        	  <df:csrfToken/>
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(announcementlist) > 0}">
                                <table class="table">
                                    <thead>
                                        <tr class="graydrkbg">
                                             <th scope="col"><input type="checkbox"> </th>
                                             <th scope="col">Job Number</th>
                                             <th scope="col">Job Date-Time</th>
                                             <th scope="col">Head 3</th>
                                             <th scope="col">Success</th>
                                             <th scope="col">Failure Reason</th>   
                                             <th scope="col">View Raw Data</th>
                                             <th scope="col">&nbsp;</th>
                                        </tr>
                                    </thead>
                                   <tbody>
	                                   <tr>
	                                   		<td><input type="checkbox"> </td>
											<td>12345</td>
											<td align="center">col2</td>
											<td align="center">col3</td>
											<td align="center"> col4</td>
											<td align="center">col5</td>
											<td align="center"></td>
											<td>
											  	<div class="dropdown">
				                                    <a alt="dropdown toggle" href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-cog"></i><i class="caret"></i></a>
				                                    <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu">
				                                        <li><a href="/hix/admin/issuer/details/361"><i class="icon-pencil"></i>Edit</a></li>
				                                        <li><a href="/hix/admin/issuer/certification/status/edit/361"><i class="icon-refresh"></i>Update Status</a></li>
				                                    </ul>
				                                </div>
											</td>
										</tr>
										<tr>
	                                   		<td><input type="checkbox"> </td>
											<td>12345</td>
											<td align="center">col2</td>
											<td align="center">col3</td>
											<td align="center"> col4</td>
											<td align="center">col5</td>
											<td align="center"></td>
											<td>
											  	<div class="dropdown">
				                                    <a alt="dropdown toggle" href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-cog"></i><i class="caret"></i></a>
				                                    <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu">
				                                        <li><a href="/hix/admin/issuer/details/361"><i class="icon-pencil"></i>Edit</a></li>
				                                        <li><a href="/hix/admin/issuer/certification/status/edit/361"><i class="icon-refresh"></i>Update Status</a></li>
				                                    </ul>
				                                </div>
											</td>
										</tr>
										<tr>
	                                   		<td><input type="checkbox"> </td>
											<td>12345</td>
											<td align="center">col2</td>
											<td align="center">col3</td>
											<td align="center"> col4</td>
											<td align="center">col5</td>
											<td align="center"></td>
											<td>
											  	<div class="dropdown">
				                                    <a alt="dropdown toggle" href="#" data-toggle="dropdown" class="dropdown-toggle"><i class="icon-cog"></i><i class="caret"></i></a>
				                                    <ul aria-labelledby="dLabel" role="menu" class="dropdown-menu">
				                                        <li><a href="/hix/admin/issuer/details/361"><i class="icon-pencil"></i>Edit</a></li>
				                                        <li><a href="/hix/admin/issuer/certification/status/edit/361"><i class="icon-refresh"></i>Update Status</a></li>
				                                    </ul>
				                                </div>
											</td>
										</tr>
                                   </tbody>
                                </table>
                                <div class="center">
                                    <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
            </form>
		</div><!--span9 ends-->
	</div>	

    <div class="notes" style="display: none">
        <div class="row">
            <div class="span">

                <p>The prototype showcases three scenarios (A, B and C) dependent on
                    a particular Employer's eligibility to use the SHOP Exchange.</p>
            </div>
        </div>
    </div>

</div> <!-- row-fluid -->


<script type="text/javascript">
$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});
</script>