package com.getinsured.iex.dto;

import java.util.List;
import java.util.Map;

public class QleEventDto {
	String applicantEventId;
	String eventLabel;
	String eventName;
	String eventDate;
	String eventCategoryLabel;
	String eventCategory;
	String changeType;
	int type;
	
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	String eventId;
	
	List<Map<String,String>> uploadedDocumentList;
	
	String validationStatus;
	
	List<String>  validDocumentList;
	
	/*boolean isDocumentUploaded;
	String documentName;
	String documentList;*/
	//Array uploadedDoc List..3 fields
	public String getEventLabel() {
		return eventLabel;
	}
	public void setEventLabel(String eventLabel) {
		this.eventLabel = eventLabel;
	}
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	/*public boolean isDocumentUploaded() {
		return isDocumentUploaded;
	}
	public void setDocumentUploaded(boolean isDocumentUploaded) {
		this.isDocumentUploaded = isDocumentUploaded;
	}
	public String getDocumentName() {
		return documentName;
	}
	public void setDocumentName(String documentName) {
		this.documentName = documentName;
	}
	public String getDocumentList() {
		return documentList;
	}
	public void setDocumentList(String documentList) {
		this.documentList = documentList;
	}*/
	public String getApplicantEventId() {
		return applicantEventId;
	}
	public void setApplicantEventId(String applicantEventId) {
		this.applicantEventId = applicantEventId;
	}
	public String getEventId() {
		return eventId;
	}
	public void setEventId(String eventId) {
		this.eventId = eventId;
	}
	public List<Map<String, String>> getUploadedDocumentList() {
		return uploadedDocumentList;
	}
	public void setUploadedDocumentList(List<Map<String, String>> uploadedDocumentList) {
		this.uploadedDocumentList = uploadedDocumentList;
	}
	public String getValidationStatus() {
		return validationStatus;
	}
	public void setValidationStatus(String validationStatus) {
		this.validationStatus = validationStatus;
	}
	public List<String> getValidDocumentList() {
		return validDocumentList;
	}
	public void setValidDocumentList(List<String> validDocumentList) {
		this.validDocumentList = validDocumentList;
	}
	public String getEventCategoryLabel() {
		return eventCategoryLabel;
	}
	public void setEventCategoryLabel(String eventCategoryLabel) {
		this.eventCategoryLabel = eventCategoryLabel;
	}
	public String getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(String eventCategoery) {
		this.eventCategory = eventCategoery;
	}
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
	
	

}
