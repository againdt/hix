package com.getinsured.hix.broker.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.getinsured.hix.broker.repository.IBrokerConnectHoursRepository;
import com.getinsured.hix.broker.repository.IBrokerConnectRepository;
import com.getinsured.hix.dto.broker.BrokerConnectSearchDTO;
import com.getinsured.hix.model.BrokerCallPrioritization;
import com.getinsured.hix.model.BrokerConnect;
import com.getinsured.hix.model.BrokerConnectHours;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.broker.repository.IBrokerRepository;
import com.getinsured.hix.broker.repository.IDesignateBrokerRepository;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.broker.BrokerContactDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.entity.repository.IDesignateAssisterRepository;
import com.getinsured.hix.entity.service.AssisterService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.timeshift.TimeShifterUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

/**
 * Implements {@link BrokerService} to perform broker related operations like
 * find,save,update etc.
 */
@Service("brokerService")
@Transactional
public class BrokerServiceImpl implements BrokerService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);

    @Autowired
    private IBrokerRepository brokerRepository;

    @Autowired
    private IBrokerConnectRepository brokerConnectRepository;

    @Autowired
    private IBrokerConnectHoursRepository brokerConnectHoursRepository;

    @Autowired
    private IDesignateBrokerRepository brokerDesignateRepository;

    @Autowired
    AssisterService assisterService;

    @Autowired
    private IDesignateAssisterRepository designateAssisterRepository;

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private ZipCodeService zipCodeService;

    private static final int ZERO = 0;
    private static final double STATE_WIDTH_FACTOR = 4;
    private static final double MILES_FACTOR = 69;
    private static final String CERTIFICATION_STATUS = "certificationStatus";
    private static final String PERCENTAGE = "%";
    private static final int COL_CONNECT_PHONE_NUMBER = 4;
    private static final double ZERO_DISTANCE = 0.0;
    private static final String LANGUAGE_SPOKEN_PARAM = "languageSpoken_";
    private static final String DISTANCE = "distance";
    private static final String ZIP = "zip";
    private static final String EXCEPTION_OCCURRED_WHILE_SEARCHING_BROKERS = "Exception occurred while searching brokers ";
    private static final String LATTITUDE_CONST = "lattitude";
    private static final String LONGITUDE_CONST = "longitude";
    private static final int DISTANCE_FOR_OPTION_ANY = 4000;
    private static final String BROKER_CERTIFICATION_STATUS_CERTIFIED = "Certified";
    private static final int START_DISTANCE = 3;
    private static final int DISTANCE_STEP = 5;
    private static final int MAX_DISTANCE = 25;
    private static final int LIST_ACCEPTABLE_SIZE = 3;
    private static final String POSTGRESQL = "POSTGRESQL";

    /**
     * @see BrokerService#findById(int)
     */
    @Override
    @Transactional(readOnly = true)
    public Broker findById(int brokerId) {
        LOGGER.info("findById : START");
        LOGGER.info("findById : END");
        return brokerRepository.findById(brokerId);
    }

    /**
     * @see BrokerService#update(Broker)
     */
    @Override
    @Transactional
    public Broker update(Broker broker) {
        long startTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("update : START-> {}", startTime);
        Broker existingBroker = brokerRepository.findById(broker.getId());
        if (existingBroker != null) {
            brokerRepository.save(existingBroker);
        }
        long endTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("update : END->{}", endTime);
        LOGGER.info("update : total duration->{}", (endTime - startTime));
        return existingBroker;
    }

    /**
     * @see BrokerService#generateBrokerRecordId(Broker)
     */
    @Override
    @Transactional
    public Integer generateBrokerRecordId(Broker broker) {
        long startTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("generateBrokerRecordId: START-> {}", startTime);
        Broker newBroker = null;
        if (broker != null) {
            newBroker = brokerRepository.save(broker);
            LOGGER.info("generateBrokerRecordId : END");
            return newBroker.getId();
        }
        long endTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("generateBrokerRecordId : END-> {}", endTime);
        LOGGER.info("generateBrokerRecordId: total duration-> {}", (endTime - startTime));
        return null;
    }

    /**
     * @see BrokerService#findByLicenseNo(int)
     */
    @Override
    @Transactional(readOnly = true)
    public Broker findByLicenseNo(String licenseNo) {
        LOGGER.info("findByLicenseNo : START");
        LOGGER.info("findByLicenseNo : END");
        return brokerRepository.findByLicenseNo(licenseNo);
    }

    /**
     * @see BrokerService#getBrokerDetail(int)
     */
    @Override
    @Transactional(readOnly = true)
    public Broker getBrokerDetail(int brokerId) {
        LOGGER.info("getBrokerDetail : START");
        LOGGER.info("getBrokerDetail : END");
        return brokerRepository.findById(brokerId);
    }

    @Override
    public Broker getBrokerDetailByUserId(int userId) {
        return brokerRepository.findByUserId(userId);
    }

    @Override
    public List<BrokerContactDTO> findBrokerListByStatus() {
        List<Broker> brokers = brokerRepository.getBRokerList();
        List<BrokerContactDTO> brokerContactDTOs = new ArrayList<BrokerContactDTO>();
        BrokerContactDTO brokerContactDTO;
        for (Broker broker : brokers) {
            brokerContactDTO = new BrokerContactDTO();
            brokerContactDTO.setId(broker.getId());
            brokerContactDTO.setFirstName(broker.getUser().getFirstName());
            brokerContactDTO.setLastName(broker.getUser().getLastName());
            brokerContactDTO.setEmail(broker.getUser().getEmail());

            brokerContactDTOs.add(brokerContactDTO);
        }

        return brokerContactDTOs;
    }

    @Override
    @Transactional
    public void saveBroker(Broker broker) {
        long startTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("saveBroker : START->{}", startTime);
        if (broker != null) {
            brokerRepository.save(broker);
        }
        long endTime = TimeShifterUtil.currentTimeMillis();
        LOGGER.info("saveBroker : END->{}", endTime);
        LOGGER.info("saveBroker : total duration->{}", (endTime - startTime));
    }


    @Override
    public List<Broker> findByAgencyManagers(long agencyId) {
        return brokerRepository.findByAgencyManagerInfo(agencyId);
    }

    public Broker findByAgencyManagerInfo(long agencyId) {
        List<Broker> tBr = brokerRepository.findByAgencyManagerInfo(agencyId);
        if (null != tBr && !tBr.isEmpty()) {
            return tBr.get(0);
        }
        return null;
    }

    @Override
    public EntityResponseDTO getBrokerAssisterDetailsByHouseholdId(Integer householdId) {
        EntityResponseDTO response = new EntityResponseDTO();
        DesignateBroker designateBroker = null;
        DesignateAssister designateAssister = null;

        try {
            if (BrokerUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)) {
                designateBroker = brokerDesignateRepository.findDesigBrokerByExternalIndividualId(householdId);
                if (designateBroker == null || DesignateBroker.Status.InActive.equals(designateBroker.getStatus())) {
                    designateAssister = designateAssisterRepository.findDesigAssisterByIndividualId(householdId);
                }
            } else if (BrokerUtils.checkStateCode(BrokerConstants.ID_STATE_CODE) || BrokerUtils.checkStateCode(BrokerConstants.NV_STATE_CODE)) {
                designateBroker = brokerDesignateRepository.findDesigBrokerByIndividualId(householdId);
                if (designateBroker == null || DesignateBroker.Status.InActive.equals(designateBroker.getStatus())) {
                    designateAssister = designateAssisterRepository.findDesigAssisterByIndividualId(householdId);
                }
            } else {
                designateBroker = brokerDesignateRepository.findBrokerByEmployerId(householdId);
            }

            if (designateAssister != null && DesignateAssister.Status.Active.equals(designateAssister.getStatus())) {
                Assister assister = assisterService.findById(designateAssister.getAssisterId());
                response.setBrokerId(assister.getId());
                response.setRole(BrokerConstants.ASSISTER_ROLE);
                response.setBrokerName(assister.getFirstName() + " " + assister.getLastName());

                response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                response.setResponseDescription("Success");
            } else if (designateBroker != null && DesignateBroker.Status.Active.equals(designateBroker.getStatus())) {
                Broker broker = findById(designateBroker.getBrokerId());

                AccountUser brokerUser = broker.getUser();
                if (null != brokerUser) {
                    response.setBrokerName(brokerUser.getFirstName() + " " + brokerUser.getLastName());
                }

                response.setBrokerId(broker.getId());
                response.setStateLicenseNumber(broker.getLicenseNumber());
                response.setStateEINNumber(broker.getFederalEIN());
				if (broker.getNpn() != null) {
					response.setAgentNPN(broker.getNpn());
				}
                response.setRole(BrokerConstants.AGENT_ROLE);
                response.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
                response.setResponseDescription("Success");
            } else {
                response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
                response.setResponseDescription("No Active designation found");
            }
        } catch (Exception exception) {
            LOGGER.error("Error while designating an agent", exception);
            response.setResponseCode(BrokerConstants.RESPONSECODEFAILURE);
            response.setResponseDescription(exception.getMessage());
        }

        return response;
    }

    @Override
    public BrokerConnect getBrokerConnectDetails(Broker broker) {
        BrokerConnect brokerConnect = null;
        try {
            if (broker != null) {
                brokerConnect = brokerConnectRepository.findBrokerConnectById(broker);
                if (brokerConnect != null) {
                    LOGGER.debug("getBrokerConnectDetails::brokerConnect = {}", brokerConnect);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getBrokerConnectDetails::error getting broker connect details", ex);
        }
        return brokerConnect;
    }

    @Override
    public BrokerConnect addBrokerConnectDetails(BrokerConnect brokerConnect) {
        BrokerConnect response = null;
        try {
            if (brokerConnect != null && !StringUtils.isEmpty(brokerConnect.getPhoneNumber()) && brokerConnect.getStatus() != null) {
                response = brokerConnectRepository.save(brokerConnect);

                if (response != null) {
                    LOGGER.debug("addBrokerConnectDetails::broker connect successfully added");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("addBrokerConnectDetails::error saving broker connect details for id: " + brokerConnect.getBrokerId(), ex);
        }

        return response;
    }

    @Override
    public BrokerConnect updateBrokerConnectDetails(BrokerConnect brokerConnect) {
        BrokerConnect response = null;
        try {
            if (brokerConnect != null) {
                BrokerConnect brokerConnectUpdated = brokerConnectRepository.findOne(brokerConnect.getId());
                brokerConnectUpdated.setAvailable(brokerConnect.isAvailable());
                brokerConnectUpdated.setPhoneNumber(brokerConnect.getPhoneNumber());
                brokerConnectUpdated.setStatus(brokerConnect.getStatus());
                response = brokerConnectRepository.save(brokerConnectUpdated);

                if (response != null) {
                    LOGGER.debug("updateBrokerConnectDetails::broker connect update success = {}", brokerConnect);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("updateBrokerConnectDetails::error getting broker connect details from id: ", ex);
        }
        return response;
    }

    @Override
    public List<BrokerConnectHours> getBrokerConnectHours(BrokerConnect brokerConnect) {
        List<BrokerConnectHours> brokerConnectHours = null;
        try {
            if (brokerConnect != null) {
                brokerConnectHours = brokerConnectHoursRepository.findBrokerConnectHoursById(brokerConnect);
                if (brokerConnectHours != null) {
                    LOGGER.debug("getBrokerConnectHours::connect hours size = {}", brokerConnectHours.size());
                }
            }
        } catch (Exception ex) {
            LOGGER.error("getBrokerConnectHours::error getting broker connect details from id: " + brokerConnect.getId(), ex);
        }
        return brokerConnectHours;
    }

    @Override
    public List<BrokerConnectHours> addBrokerConnectHours(List<BrokerConnectHours> brokerConnectHours) {
        List<BrokerConnectHours> response = null;
        try {
            if (brokerConnectHours != null) {
                response = brokerConnectHoursRepository.save(brokerConnectHours);
                if (response != null) {
                    LOGGER.debug("addBrokerConnectHours::broker connect hours updated");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("addBrokerConnectHours::error getting broker connect details", ex);
        }
        return response;
    }

    @Override
    public List<BrokerConnectHours> updateBrokerConnectHours(List<BrokerConnectHours> brokerConnectHours) {
        List<BrokerConnectHours> response = null;
        try {
            if (brokerConnectHours != null) {
                response = brokerConnectHoursRepository.save(brokerConnectHours);
                if (response != null) {
                    LOGGER.debug("updateBrokerConnectHours::broker connect hours updated");
                }
            }
        } catch (Exception ex) {
            LOGGER.error("updateBrokerConnectHours::error getting broker connect details", ex);
        }
        return response;
    }

    @Override
    public BrokerConnect updateBrokerAvailability(Integer brokerConnectId, boolean availability) {
        BrokerConnect response = null;
        try {
            if (brokerConnectId != null) {
                BrokerConnect brokerConnectUpdated = brokerConnectRepository.findOne(brokerConnectId);
                brokerConnectUpdated.setAvailable(availability);
                LOGGER.debug("updateBrokerAvailability::saving updated broker connect");
                response = brokerConnectRepository.save(brokerConnectUpdated);

                if (response != null) {
                    LOGGER.debug("updateBrokerAvailability::broker connect update success = {}", response);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("updateBrokerAvailability::error getting broker connect details from id: ", ex);
        }
        return response;
    }

    @Override
    public List<String> searchAvailableBrokers(BrokerCallPrioritization brokerCallPrioritization) {
        LOGGER.debug("searchAvailableBrokers::START");
        List<String> availableBrokerPhones = new ArrayList<>();
        try {
            if (brokerCallPrioritization != null && StringUtils.isNotBlank(brokerCallPrioritization.getLanguage()) &&
                    StringUtils.isNotBlank(brokerCallPrioritization.getZipCode())) {
                LOGGER.debug("searchAvailableBrokers::request valid");
                availableBrokerPhones = searchBrokersCallPrioritization(brokerCallPrioritization);
            }
        } catch (Exception ex) {
            LOGGER.error("getAvailableBrokers::error getting available brokers", ex);
        }

        return availableBrokerPhones;
    }

    private List<String> searchBrokersCallPrioritization(BrokerCallPrioritization brokerCallPrioritization) {
        LOGGER.debug("searchBrokersCallPrioritization::START");
        List<String> availableBrokerPhones = new ArrayList<>();
        ZipCode zipCode = null;
        int distance = START_DISTANCE;

        try {
            if (StringUtils.isNotBlank(brokerCallPrioritization.getZipCode())) {
                if (brokerCallPrioritization.getZipCode().length() != 5) {
                    throw new GIRuntimeException("Invalid Zip Code");
                }

                zipCode = zipCodeService.findByZipCode(brokerCallPrioritization.getZipCode());
                if (zipCode == null || (zipCode.getLat() == ZERO && zipCode.getLon() == ZERO)) {
                    LOGGER.error("searchBrokersCallPrioritization::Invalid zip code : {}", brokerCallPrioritization.getZipCode());
                    return availableBrokerPhones;
                }
            }

            BrokerConnectSearchDTO brokerConnectSearchDTO = new BrokerConnectSearchDTO();
            brokerConnectSearchDTO.setZipCode(zipCode);
            brokerConnectSearchDTO.setCertificationStatus(BROKER_CERTIFICATION_STATUS_CERTIFIED);
            brokerConnectSearchDTO.setDistance(distance);
            brokerConnectSearchDTO.setLanguagesSpoken(brokerCallPrioritization.getLanguage());

            List<Object[]> brokerObjectArrayList;
            int totalCount = 0;
            List totalCountList = searchForBrokers(0, 1, brokerConnectSearchDTO, true);
            if (totalCountList != null && !totalCountList.isEmpty()) {
                totalCount = Integer.parseInt(totalCountList.get(0) + "");
            }

            while (totalCount < LIST_ACCEPTABLE_SIZE && (distance + DISTANCE_STEP) <= MAX_DISTANCE) {
                distance += DISTANCE_STEP;
                brokerConnectSearchDTO.setDistance(distance);
                totalCountList = searchForBrokers(0, 1, brokerConnectSearchDTO, true);
                if (totalCountList != null && !totalCountList.isEmpty()) {
                    totalCount = Integer.parseInt(totalCountList.get(0) + "");
                }
            }

            if (totalCount <= LIST_ACCEPTABLE_SIZE) {
                if (totalCount != 0) {
                    brokerObjectArrayList = searchForBrokers(0, LIST_ACCEPTABLE_SIZE, brokerConnectSearchDTO, false);
                    getBrokerAvailabiliyList(brokerObjectArrayList, availableBrokerPhones);
                    Collections.shuffle(availableBrokerPhones);
                }

                if (totalCount == LIST_ACCEPTABLE_SIZE) {
                    LOGGER.debug("searchBrokersCallPrioritization::total count was correct size {}, availableBrokerPhones = {}", LIST_ACCEPTABLE_SIZE, availableBrokerPhones.toString());
                    return availableBrokerPhones;
                }

                // Query count for all of the USA
                brokerConnectSearchDTO.setDistance(DISTANCE_FOR_OPTION_ANY);
                brokerConnectSearchDTO.setZipCode(new ZipCode());
                totalCountList = searchForBrokers(0, 1, brokerConnectSearchDTO, true);
                if (totalCountList != null && !totalCountList.isEmpty()) {
                    totalCount = Integer.parseInt(totalCountList.get(0) + "");
                }
            }

            // Get random offset between 0 and (totalCount - LIST_ACCEPTABLE_SIZE) + 1
            // We need LIST_ACCEPTABLE_SIZE - totalCount so we can have an offset of LIST_ACCEPTABLE_SIZE and potentially get the last three records
            // EDGE CASE: If total USA count is less than LIST_ACCEPTABLE_SIZE then we need offset to be zero
            Integer offset;
            if (totalCount > LIST_ACCEPTABLE_SIZE) {
                Random r = new Random();
                offset = r.nextInt((totalCount - LIST_ACCEPTABLE_SIZE) + 1);
            } else {
                offset = 0;
            }

            LOGGER.debug("searchBrokersCallPrioritization::random offset = {}, Page Size = {}", offset, LIST_ACCEPTABLE_SIZE);
            brokerObjectArrayList = searchForBrokers(offset, LIST_ACCEPTABLE_SIZE, brokerConnectSearchDTO, false);

            List<String> brokerOffsetList = new ArrayList<>();
            getBrokerAvailabiliyList(brokerObjectArrayList, brokerOffsetList);

            brokerOffsetList.removeAll(availableBrokerPhones);
            Collections.shuffle(brokerOffsetList);

            // If list from 0 to LIST_ACCEPTABLE_SIZE plus USA list without 0 to LIST_ACCEPTABLE_SIZE is less than LIST_ACCEPTABLE_SIZE
            // then return everything else return with subset of USA list
            if(brokerOffsetList.size() + availableBrokerPhones.size() <= LIST_ACCEPTABLE_SIZE) {
                availableBrokerPhones.addAll(brokerOffsetList);
            } else {
                availableBrokerPhones.addAll(brokerOffsetList.subList(0, LIST_ACCEPTABLE_SIZE - availableBrokerPhones.size()));
            }

            LOGGER.debug("searchBrokersCallPrioritization::availableBrokerPhones size = {}", availableBrokerPhones);
        } catch (Exception ex) {
            LOGGER.error(EXCEPTION_OCCURRED_WHILE_SEARCHING_BROKERS, ex);
        }

        LOGGER.debug("searchBrokersCallPrioritization::availableBrokerPhones = {}", availableBrokerPhones.toString());
        return availableBrokerPhones;
    }

    private void getBrokerAvailabiliyList(List<Object[]> brokerObjectArrayList, List<String> availableBrokerPhones) {
        LOGGER.debug("getBrokerAvailabiliyList:: START");
        for (Object[] brokerObjArr : brokerObjectArrayList) {
            String phoneNumber = brokerObjArr[COL_CONNECT_PHONE_NUMBER] != null ? brokerObjArr[COL_CONNECT_PHONE_NUMBER].toString() : "";
            if (StringUtils.isNotBlank(phoneNumber)) {
                LOGGER.debug("getBrokerAvailabiliyList::available::phoneNumber = {}", phoneNumber);
                availableBrokerPhones.add(phoneNumber);
            }
        }
        LOGGER.debug("getBrokerAvailabiliyList::size = {}", availableBrokerPhones.size());
    }

    private List<Object[]> searchForBrokers(Integer startRecord, Integer pageSize, BrokerConnectSearchDTO brokerConnectSearchDTO, boolean isTotalCount) {
        List<Object[]> brokerObjectArrayList = new ArrayList<>();
        EntityManager em = null;
        Query query = null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("searchForBrokers::START zip = {}", brokerConnectSearchDTO.getZipCode().getZip());
        }

        try {
            em = emf.createEntityManager();
            query = createBrokerSearchQuery(brokerConnectSearchDTO, isTotalCount, em);
            if (query != null) {
                query.setFirstResult(startRecord);
                query.setMaxResults(pageSize);
                brokerObjectArrayList = (List<Object[]>) query.getResultList();
            }
        } catch (GIRuntimeException giexception) {
            LOGGER.error(EXCEPTION_OCCURRED_WHILE_SEARCHING_BROKERS, giexception);
            throw giexception;
        } finally {
            if (em != null && em.isOpen()) {
                em.clear();
                em.close();
            }
            LOGGER.info("searchForBrokers: END");
        }

        return brokerObjectArrayList;
    }

    /**
     * Method to create Broker search query.
     *
     * @param brokerConnectSearchDTO The Broker Connect Search DTO.
     * @param isTotalCountQuery      The total count query indicator.
     * @param em                     The entity manager for query.
     * @return query The updated Query instance.
     */
    private Query createBrokerSearchQuery(BrokerConnectSearchDTO brokerConnectSearchDTO, boolean isTotalCountQuery, EntityManager em) {
        String queryStr;
        Query query = null;

        try {
            queryStr = getBrokerSearchQuery(brokerConnectSearchDTO, isTotalCountQuery);
            query = em.createNativeQuery(queryStr);
            setParametersInBrokerSearchQuery(brokerConnectSearchDTO, query);
        } catch (Exception ex) {
            LOGGER.warn("Exception occurred.", ex);
        }

        return query;
    }

    /**
     * Method to get broker search query
     *
     * @param brokerConnectSearchDTO The Broker Connect Search DTO.
     * @param isTotalCountQuery      The total count query flag.
     * @return The stringified parameterized query string.
     */
    private String getBrokerSearchQuery(BrokerConnectSearchDTO brokerConnectSearchDTO, boolean isTotalCountQuery) {

        LOGGER.debug("getBrokerSearchQuery: START");

        StringBuilder rawQuery = new StringBuilder();

        if (brokerConnectSearchDTO != null) {
            double longitude = ZERO_DISTANCE;
            double latitude = ZERO_DISTANCE;

            if (brokerConnectSearchDTO.getZipCode() != null) {
                longitude = brokerConnectSearchDTO.getZipCode().getLon();
                latitude = brokerConnectSearchDTO.getZipCode().getLat();
            }

            if (isTotalCountQuery) {
                createTotalCountRawQuery(brokerConnectSearchDTO.getZipCode(), rawQuery, longitude, latitude);
            } else {
                createRefineSearchRawQuery(brokerConnectSearchDTO.getZipCode(), rawQuery, longitude, latitude);
            }

            rawQuery.append(" FROM brokers b INNER JOIN users u ON  u.id = b.userid INNER JOIN locations l ON l.id = b.location_id");
            rawQuery.append(" INNER JOIN broker_connect bc ON b.id = bc.broker_id");
            rawQuery.append(" WHERE ");
            rawQuery.append(" clients_served IS NOT NULL ");

            appendRawQueryWithCertificationStatus(brokerConnectSearchDTO.getCertificationStatus(), rawQuery);

            rawQuery.append(" AND (b.status IS NULL OR b.status='Active') ");

            appendRawQueryForLanguagesSpoken(brokerConnectSearchDTO.getLanguagesSpoken(), rawQuery);

            appendRawQueryWithLatLongRange(brokerConnectSearchDTO.getZipCode(), brokerConnectSearchDTO.getDistance(), rawQuery, latitude, longitude);

            rawQuery.append(" AND bc.is_available = 1 ");

            if (!isTotalCountQuery || (brokerConnectSearchDTO.getZipCode() != null && longitude != 0 && latitude != 0)) {
                rawQuery.append(" ) SUB1 ");
            }

            appendRawQueryWithDistance(brokerConnectSearchDTO.getZipCode(), rawQuery, brokerConnectSearchDTO.getDistance());

            createOrderyByCriteriaForRawQuery(brokerConnectSearchDTO.getZipCode(), brokerConnectSearchDTO.getDistance(), isTotalCountQuery, rawQuery);
        }

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("getBrokerSearchQuery::END rawQuery = {}", rawQuery.toString());
        }
        return rawQuery.toString();
    }

    private void appendRawQueryWithLatLongRange(ZipCode zipCode, double distance, StringBuilder rawQuery, double latitude, double longitude) {
        if (longitude != 0 && latitude != 0) {
            double dis = (distance * STATE_WIDTH_FACTOR) / MILES_FACTOR;
            rawQuery.append(" AND (l.lattitude between (" + latitude + " - " + dis + ") and (" + latitude + " + " + dis + ") ");
            rawQuery.append(" AND l.longitude between ( " + longitude + " - " + dis + ") and (" + longitude + " + " + dis + ") or l.zip = '" + zipCode.getZip() + "' ) ");
        }

    }

    /**
     * Method to append distance  condition in where clause of raw query.
     *
     * @param zipCode  The current zip code.
     * @param rawQuery The raw query's StringBuilder instance.
     * @param distance The current distance.
     */
    private void appendRawQueryWithDistance(ZipCode zipCode, StringBuilder rawQuery, int distance) {
        LOGGER.debug("appendRawQueryWithDistance: START");

        if (distance > 0 && StringUtils.isNotBlank(zipCode.getZip()) && distance != DISTANCE_FOR_OPTION_ANY) {
            rawQuery.append(" where ((distance > 0 and distance <= :" + DISTANCE + " ) or (zip = :" + ZIP + " ))");
        }

        LOGGER.debug("appendRawQueryWithDistance: END");
    }

    /**
     * The method to append Certification status where clause in raw query.
     *
     * @param certificationStatus Broker Certification Status.
     * @param rawQuery            The raw query's StringBuilder instance.
     */
    private void appendRawQueryWithCertificationStatus(String certificationStatus, StringBuilder rawQuery) {

        LOGGER.debug("appendRawQueryWithCertificationStatus: START");

        if (StringUtils.isNotBlank(certificationStatus)) {
            rawQuery.append(" and b.certification_status = :" + CERTIFICATION_STATUS + " ");
        }

        LOGGER.debug("appendRawQueryWithCertificationStatus: END");
    }

    /**
     * Method to append languages spoken where clauses in raw query.
     *
     * @param languagesSpoken Languages Spoken.
     * @param rawQuery        The raw query's StringBuilder instance.
     */
    private void appendRawQueryForLanguagesSpoken(String languagesSpoken, StringBuilder rawQuery) {

        LOGGER.debug("appendRawQueryForLanguagesSpoken: START");

        String[] languages;
        if (StringUtils.isNotBlank(languagesSpoken)) {
            languages = languagesSpoken.split(",");

            if (languages != null && languages.length > 0) {
                rawQuery.append(" and ( ");

                for (int i = 0; i < languages.length; i++) {
                    if (i > 0) {
                        rawQuery.append(" or ");
                    }

                    rawQuery.append(" languages_spoken like :" + LANGUAGE_SPOKEN_PARAM + i);
                }
                rawQuery.append(" ) ");
            }
        }

        LOGGER.debug("appendRawQueryForLanguagesSpoken: END");
    }

    /**
     * Method to create Order By criteria for raw query.
     *
     * @param zipCode           The current zip value.
     * @param distance          The distance value.
     * @param isTotalCountQuery The total count query indicator.
     * @param rawQuery          The raw query.
     */
    private void createOrderyByCriteriaForRawQuery(ZipCode zipCode, int distance, boolean isTotalCountQuery, StringBuilder rawQuery) {
        LOGGER.debug("createOrderyByCriteriaForRawQuery: START");

        if (!isTotalCountQuery) {
            rawQuery.append(" ORDER BY ");
            if ((distance > 0 && StringUtils.isNotBlank(zipCode.getZip())) || (distance == DISTANCE_FOR_OPTION_ANY)) {
                rawQuery.append(" distance, id ");
            }
        }
        LOGGER.debug("createOrderyByCriteriaForRawQuery: END");
    }

    /**
     * Method to create refine search query.
     *
     * @param zipCode   The current ZipCode instance.
     * @param rawQuery  The raw query.
     * @param longitude The longitude value.
     * @param lattitude The latitude value.
     */
    private void createRefineSearchRawQuery(ZipCode zipCode, StringBuilder rawQuery, double longitude, double lattitude) {
        LOGGER.debug("createRefineSearchRawQuery: START");

        rawQuery.append("select * from ( ");
        rawQuery.append("select ");
        rawQuery.append("b.id, ");

        if (zipCode != null && longitude != 0 && lattitude != 0) {
            if (POSTGRESQL.equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
                rawQuery.append("TRUNC( CAST( 3959 * acos( TRUNC( CAST( cos( TRUNC(   CAST( (:" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC ) ,4) )  * ");
            } else {
                rawQuery.append("TRUNC(   3959 * acos( TRUNC(   cos( TRUNC(    :" + LATTITUDE_CONST + " / 180.0 * 3.14   ,4) )  * ");
            }

            rawQuery.append("cos( TRUNC(    l.lattitude / 180.0 * 3.14   ,4  )   ) * ");
            rawQuery.append("cos( (TRUNC(    l.longitude / 180.0 * 3.14   , 4 )  ) - ");

            if (POSTGRESQL.equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
                rawQuery.append("TRUNC( CAST(   (:" + LONGITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  ,4  ) ) + ");
                rawQuery.append("sin( TRUNC(  CAST(  ( :" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  , 4) ) ");
                rawQuery.append("* sin( TRUNC(   l.lattitude / 180.0 * 3.14   ,4) )  AS NUMERIC ) , 15) )  AS NUMERIC ) ,4) as distance , ");
            } else {
                rawQuery.append("TRUNC(    (:" + LONGITUDE_CONST + " / 180.0 * 3.14 )  ,4  ) ) + ");
                rawQuery.append("sin( TRUNC(    :" + LATTITUDE_CONST + " / 180.0 * 3.14   , 4) ) ");
                rawQuery.append("* sin( TRUNC(   l.lattitude / 180.0 * 3.14   ,4) )    , 15) )    ,4) as distance , ");
            }
        } else {
            rawQuery.append(" -1 as distance , ");
        }

        rawQuery.append("l.zip, ");
        rawQuery.append("b.userid, ");
        rawQuery.append("bc.phone_number, ");
        rawQuery.append("l.lattitude, ");
        rawQuery.append("l.longitude");

        LOGGER.debug("createRefineSearchRawQuery: END");
    }

    /**
     * Method to create total count raw query.
     *
     * @param zipCode   The current ZipCode instance.
     * @param rawQuery  The raw query.
     * @param longitude The longitude value.
     * @param lattitude The latitude value.
     */
    private void createTotalCountRawQuery(ZipCode zipCode,
                                          StringBuilder rawQuery, double longitude, double lattitude) {
        LOGGER.debug("createTotalCountRawQuery: START");

        rawQuery.append("select count(*) ");

        if (zipCode != null && longitude != 0 && lattitude != 0) {
            rawQuery.append(" from ( ");
            rawQuery.append(" select ");

            if (POSTGRESQL.equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
                rawQuery.append(" TRUNC( CAST( 3959 * acos( TRUNC(  CAST( cos( TRUNC( CAST(   ( :" + LATTITUDE_CONST + " / 180.0 * 3.14) AS NUMERIC )  ,4) )  * ");
            } else {
                rawQuery.append(" TRUNC(   3959 * acos( TRUNC(   cos( TRUNC( CAST (   ( :" + LATTITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC) ,4) )  * ");
            }

            rawQuery.append("cos( TRUNC(   l.lattitude / 180.0 * 3.14  ,4 )  ) * ");
            rawQuery.append("cos( (TRUNC(   l.longitude / 180.0 * 3.14  ,4 )  ) - ");

            if (POSTGRESQL.equalsIgnoreCase(GhixConstants.DATABASE_TYPE)) {
                rawQuery.append("TRUNC( CAST (   (:" + LONGITUDE_CONST + " / 180.0 * 3.14 ) AS NUMERIC)  ,4 ) ) + ");
                rawQuery.append("sin( TRUNC(  CAST ( ( :" + LATTITUDE_CONST + " / 180.0 * 3.14) AS NUMERIC )  ,4) ) ");
                rawQuery.append("* sin( TRUNC(    l.lattitude / 180.0 * 3.14   ,4) ) AS NUMERIC ) , 15) ) AS NUMERIC ),4)  ");
            } else {
                rawQuery.append("TRUNC(     (:" + LONGITUDE_CONST + " / 180.0 * 3.14 )   ,4 ) ) + ");
                rawQuery.append("sin( TRUNC(    ( :" + LATTITUDE_CONST + " / 180.0 * 3.14)    ,4) ) ");
                rawQuery.append("* sin( TRUNC(    l.lattitude / 180.0 * 3.14   ,4) )   , 15) )  ,4)  ");
            }

            rawQuery.append(" as distance, l.zip ");
        }

        LOGGER.debug("createTotalCountRawQuery: END");
    }

    /**
     * Method to set parameters in broker search query
     *
     * @param brokerConnectSearchDTO The Broker Connect Search DTO.
     * @param query                  The Query instance.
     * @return query The Query instance with set parameters.
     */
    private Query setParametersInBrokerSearchQuery(BrokerConnectSearchDTO brokerConnectSearchDTO, Query query) {
        LOGGER.debug("setParametersInBrokerSearchQuery: START");

        if (brokerConnectSearchDTO != null) {
            int distance = brokerConnectSearchDTO.getDistance();

            setParametersInBrokerSearchQueryForLocation(query, brokerConnectSearchDTO.getZipCode());

            setParametersInBrokerSearchQueryForLanguagesSpoken(brokerConnectSearchDTO, query);

            if (StringUtils.isNotBlank(brokerConnectSearchDTO.getCertificationStatus())) {
                query.setParameter(CERTIFICATION_STATUS, brokerConnectSearchDTO.getCertificationStatus());
            }

            if (distance > 0 && StringUtils.isNotBlank(brokerConnectSearchDTO.getZipCode().getZip()) && distance != DISTANCE_FOR_OPTION_ANY) {
                query.setParameter(DISTANCE, distance);
                query.setParameter(ZIP, brokerConnectSearchDTO.getZipCode().getZip());
            }
        }

        LOGGER.debug("setParametersInBrokerSearchQuery: END");

        return query;
    }

    /**
     * Method to set parameters in Broker search query for Languages spoken.
     *
     * @param brokerConnectSearchDTO The Broker Connect Search DTO.
     * @param query                  The current Query instance.
     */
    private void setParametersInBrokerSearchQueryForLanguagesSpoken(
            BrokerConnectSearchDTO brokerConnectSearchDTO, Query query) {
        String[] languages;
        if (StringUtils.isNotBlank(brokerConnectSearchDTO.getLanguagesSpoken())) {
            languages = brokerConnectSearchDTO.getLanguagesSpoken().split(",");

            for (int i = 0; i < languages.length; i++) {
                query.setParameter(LANGUAGE_SPOKEN_PARAM + i, PERCENTAGE + languages[i] + PERCENTAGE);
            }
        }
    }

    /**
     * Method to set parameters in Broker search query for location data (zip, lattitude, longitude).
     *
     * @param query   The current Query instance.
     * @param zipCode The current ZipCode instance.
     */
    private void setParametersInBrokerSearchQueryForLocation(Query query, ZipCode zipCode) {
        double longitude;
        double latitude;

        if (zipCode != null) {
            longitude = zipCode.getLon();
            latitude = zipCode.getLat();

            if (longitude != 0 && latitude != 0) {
                query.setParameter(LATTITUDE_CONST, latitude);
                query.setParameter(LONGITUDE_CONST, longitude);
            }
        }
    }
}
