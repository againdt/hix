package com.getinsured.iex.hub.nmec.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ApplicantType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.IndividualResponseType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OtherCoverageType;


public class IndividualResponseTypeWrapper implements JSONAware{
	
	private boolean partialResponseIndicator;
	private List<OtherCoverageTypeWrapper> otherCoverages;
	
	
	private ApplicantTypeWrapper applicantWrapper = null;
	
	public IndividualResponseTypeWrapper(IndividualResponseType individualResponseType){
		
		if(individualResponseType != null)
		{
			ApplicantType applicantType = individualResponseType.getApplicant();
			if(applicantType != null)
			{
				applicantWrapper = new ApplicantTypeWrapper(applicantType);
			}
			
			partialResponseIndicator = individualResponseType.isPartialResponseIndicator();
			
			List<OtherCoverageType> listOtherCoverageType = individualResponseType.getOtherCoverage();
			
			int othercoverageCount = listOtherCoverageType.size();
			
			if(othercoverageCount > 0)
			{
				otherCoverages = new ArrayList<OtherCoverageTypeWrapper>();
			}
			
			for(int i = 0; i < othercoverageCount; i++)
			{
				OtherCoverageTypeWrapper otherCoverageTypeWrapper = new OtherCoverageTypeWrapper(listOtherCoverageType.get(i));
				otherCoverages.add(otherCoverageTypeWrapper);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ApplicantType",applicantWrapper);
		obj.put("PartialResponseIndicator",partialResponseIndicator);
		
		JSONArray otherCoverage = new JSONArray();
		
		if(this.otherCoverages != null){
			for(OtherCoverageTypeWrapper data : this.otherCoverages){
				otherCoverage.add(data);
			}
		}
		obj.put("OtherCoverages", otherCoverage);
		
		return obj.toJSONString();
	}
	
}
