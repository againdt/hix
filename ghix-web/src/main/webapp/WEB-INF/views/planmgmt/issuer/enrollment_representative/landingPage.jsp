<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/preserveImgAspectRatio.js" />"></script>

<%@ page isELIgnored="false"%>
	<div class="row-fluid">
		<div class="gutter10">
			<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
               
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span10"><spring:message  code="pgheader.issuerInfo"/></h4>
                </div>
				<div class="gutter10">
					<div class="profile">
						<table class="table table-border-none">
							<tbody>
								<tr>
									<td class="txt-right span4"><spring:message  code="label.name"/></td>
									<td><strong>${issuerObj.name}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.NAICCompanyCode"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.NAICCompanyCode'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.naicCompanyCode}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.NAICGroupCode"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.NAICGroupCode'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.naicGroupCode}</strong>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.FederalEmployerId"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.FederalEmployerId'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.federalEin}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message  code="label.HIOSIssuerId"/>
										<a class="ttclass" rel="tooltip" href="#" data-original-title="<spring:message code='title.HIOSIssuerId'/>"><i class="icon-question-sign"></i></a>
									</td>
									<td><strong>${issuerObj.hiosIssuerId}</strong></td>
								</tr>
							</tbody>
						</table><br />
                        <div class="header">
                            <h4 class="span10"><spring:message  code="pgheader.issuerAddress"/></h4>
                        </div>
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine1"/></td>
                                <td><strong>${issuerObj.addressLine1}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><spring:message  code="label.addressLine2"/></td>
                                <td><strong>${issuerObj.addressLine2}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.city"/></td>
                                <td><strong>${issuerObj.city}</strong>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.state"/></td>
                                <td><strong>${issuerObj.state}</strong></td>
                            </tr>
                            <tr>
                                <td class="txt-right"><spring:message  code="label.zipCode"/></td>
                                <td><strong>${issuerObj.zip}</strong></td>
                            </tr>
						</table>
					</div>			
			</div>
		</div><!--  end of span9 -->
	</div><!-- end row-fluid -->
	
<script type="text/javascript">
$('.ttclass').tooltip();
</script>
