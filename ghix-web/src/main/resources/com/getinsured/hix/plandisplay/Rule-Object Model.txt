
Key Inputs:
  int numberOfPrimaryCareVisits
  int numberOfSpecialityVisits
  boolean isPregnant
  boolean willHaveMajorMedicalProcedure
  boolean hasMajorChronicCondition

Plan Attributes:
  int premiumValue
  int annualIndividualDeductibleValue
  int annualIndividaulOut-of-PocketMaxValue
  int annualFamilyDeductibleValue
  int annualFamilyOut-of-PocketMaxValue

  int estimateUnitCharge

  int inpatientHospitalValue
  int inpatientHospitalAssumption
  int inpatientHospitalUtilization
  int inpatientHospitalNLValue
  int inpatientHospitalCost
  int inpatientHospitalCumCost
  String inpatientHospitalNLAttribute
  String inpatientHospitalAttribute

  int outpatientHospitalValue
  int outpatientHospitalAssumption
  int outpatientHospitalUtilization
  int outpatientHospitalNLValue
  int outpatientHospitalCost
  int outpatientHospitalCumCost
  String outpatientHospitalNLAttribute
  String outpatientHospitalAttribute

  int emergencyRoomValue
  int emergencyRoomAssumption
  int emergencyRoomUtilization
  int emergencyRoomNLValue
  int emergencyRoomCost
  int emergencyRoomCumCost
  String emergencyRoomNLAttribute
  String emergencyRoomAttribute

  int preventiveCareValue
  int preventiveCareAssumption
  int preventiveCareUtilization
  int preventiveCareNLValue
  int preventiveCareCost
  int preventiveCareCumCost
  String preventiveCareNLAttribute
  String preventiveCareAttribute

  int primaryCareVisitsValue
  int primaryCareVisitsAssumption
  int primaryCareVisitsUtilization
  int primaryCareVisitsNLValue
  int primaryCareVisitsCost
  int primaryCareVisitsCumCost
  String primaryCareVisitsNLAttribute
  String primaryCareVisitsAttribute

  int specialtyCareVisitsValue
  int specialtyCareVisitsAssumption
  int specialtyCareVisitsUtilization
  int specialtyCareVisitsNLValue
  int specialtyCareVisitsCost
  int specialtyCareVisitsCumCost
  String specialtyCareVisitsNLAttribute
  String specialtyCareVisitsAttribute

  int imaging-AdvancedX-rayValue
  int imaging-AdvancedX-rayAssumption
  int imaging-AdvancedX-rayUtilization
  int imaging-AdvancedX-rayNLValue
  int imaging-AdvancedX-rayCost
  int imaging-AdvancedX-rayCumCost
  String imaging-AdvancedX-rayNLAttribute
  String imaging-AdvancedX-rayAttribute

  int labTestsValue
  int labTestsAssumption
  int labTestsUtilization
  int labTestsNLValue
  int labTestsCost
  int labTestsCumCost
  String labTestsNLAttribute
  String labTestsAttribute

  int pt-ot-stValue
  int pt-ot-stAssumption
  int pt-ot-stUtilization
  int pt-ot-stNLValue
  int pt-ot-stCost
  int pt-ot-stCumCost
  String pt-ot-stNLAttribute
  String pt-ot-stAttribute

  int wellnessServicesValue
  int wellnessServicesAssumption
  int wellnessServicesUtilization
  int wellnessServicesNLValue
  int wellnessServicesCost
  int wellnessServicesCumCost
  String wellnessServicesNLAttribute
  String wellnessServicesAttribute

  int acupunctureValue
  int acupunctureAssumption
  int acupunctureUtilization
  int acupunctureNLValue
  int acupunctureCost
  int acupunctureCumCost
  String acupunctureNLAttribute
  String acupunctureAttribute

  int chiropracticCareValue
  int chiropracticCareAssumption
  int chiropracticCareUtilization
  int chiropracticCareNLValue
  int chiropracticCareCost
  int chiropracticCareCumCost
  String chiropracticCareNLAttribute
  String chiropracticCareAttribute

  int pediatricDentalValue
  int pediatricDentalAssumption
  int pediatricDentalUtilization
  int pediatricDentalNLValue
  int pediatricDentalCost
  int pediatricDentalCumCost
  String pediatricDentalNLAttribute
  String pediatricDentalAttribute

  int dentalCheck-upValue
  int dentalCheck-upAssumption
  int dentalCheck-upUtilization
  int dentalCheck-upNLValue
  int dentalCheck-upCost
  int dentalCheck-upCumCost
  String dentalCheck-upNLAttribute
  String dentalCheck-upAttribute

  int pediatricVisionValue
  int pediatricVisionAssumption
  int pediatricVisionUtilization
  int pediatricVisionNLValue
  int pediatricVisionCost
  int pediatricVisionCumCost
  String pediatricVisionNLAttribute
  String pediatricVisionAttribute

  int eyeExamValue
  int eyeExamAssumption
  int eyeExamUtilization
  int eyeExamNLValue
  int eyeExamCost
  int eyeExamCumCost
  String eyeExamNLAttribute
  String eyeExamAttribute

  int glassesValue
  int glassesAssumption
  int glassesUtilization
  int glassesNLValue
  int glassesCost
  int glassesCumCost
  String glassesNLAttribute
  String glassesAttribute

  int mentalHealth-SubstanceAbuseInpatientValue
  int mentalHealth-SubstanceAbuseInpatientAssumption
  int mentalHealth-SubstanceAbuseInpatientUtilization
  int mentalHealth-SubstanceAbuseInpatientNLValue
  int mentalHealth-SubstanceAbuseInpatientCost
  int mentalHealth-SubstanceAbuseInpatientCumCost
  String mentalHealth-SubstanceAbuseInpatientNLAttribute
  String mentalHealth-SubstanceAbuseInpatientAttribute

  int mentalHealth-SubstanceAbuseOutpatientValue
  int mentalHealth-SubstanceAbuseOutpatientAssumption
  int mentalHealth-SubstanceAbuseOutpatientUtilization
  int mentalHealth-SubstanceAbuseOutpatientNLValue
  int mentalHealth-SubstanceAbuseOutpatientCost
  int mentalHealth-SubstanceAbuseOutpatientCumCost
  String mentalHealth-SubstanceAbuseOutpatientNLAttribute
  String mentalHealth-SubstanceAbuseOutpatientAttribute

  int maternityPrenatalAndPostnatalVisitsValue
  int maternityPrenatalAndPostnatalVisitsAssumption
  int maternityPrenatalAndPostnatalVisitsUtilization
  int maternityPrenatalAndPostnatalVisitsNLValue
  int maternityPrenatalAndPostnatalVisitsCost
  int maternityPrenatalAndPostnatalVisitsCumCost
  String maternityPrenatalAndPostnatalVisitsNLAttribute
  String maternityPrenatalAndPostnatalVisitsAttribute

  int prenatalDiagnosisOfGeneticDisordersValue
  int prenatalDiagnosisOfGeneticDisordersAssumption
  int prenatalDiagnosisOfGeneticDisordersUtilization
  int prenatalDiagnosisOfGeneticDisordersNLValue
  int prenatalDiagnosisOfGeneticDisordersCost
  int prenatalDiagnosisOfGeneticDisordersCumCost
  String prenatalDiagnosisOfGeneticDisordersNLAttribute
  String prenatalDiagnosisOfGeneticDisordersAttribute

  int naturalChildbirthClassesValue
  int naturalChildbirthClassesAssumption
  int naturalChildbirthClassesUtilization
  int naturalChildbirthClassesNLValue
  int naturalChildbirthClassesCost
  int naturalChildbirthClassesCumCost
  String naturalChildbirthClassesNLAttribute
  String naturalChildbirthClassesAttribute

  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyValue
  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyAssumption
  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyUtilization
  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyNLValue
  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyCost
  int hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyCumCost
  String hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyNLAttribute
  String hospitalServicesForNormalDeliveryC-sectionAndComplicationsOfPregnancyAttribute

  int newbornCareValue
  int newbornCareAssumption
  int newbornCareUtilization
  int newbornCareNLValue
  int newbornCareCost
  int newbornCareCumCost
  String newbornCareNLAttribute
  String newbornCareAttribute

  int blankFieldValue
  int blankFieldAssumption
  int blankFieldUtilization
  int blankFieldNLValue
  int blankFieldCost
  int blankFieldCumCost
  String blankFieldNLAttribute
  String blankFieldAttribute

  int prescriptionBrandDeductableValue
  int prescriptionBrandDeductableAssumption
  int prescriptionBrandDeductableUtilization
  int prescriptionBrandDeductableNLValue
  int prescriptionBrandDeductableCost
  int prescriptionBrandDeductableCumCost
  String prescriptionBrandDeductableNLAttribute
  String prescriptionBrandDeductableAttribute

  int genericValue
  int genericAssumption
  int genericUtilization
  int genericNLValue
  int genericCost
  int genericCumCost
  String genericNLAttribute
  String genericAttribute

  int brand-PreferredValue
  int brand-PreferredAssumption
  int brand-PreferredUtilization
  int brand-PreferredNLValue
  int brand-PreferredCost
  int brand-PreferredCumCost
  String brand-PreferredNLAttribute
  String brand-PreferredAttribute

  int brand-non-PreferredValue
  int brand-non-PreferredAssumption
  int brand-non-PreferredUtilization
  int brand-non-PreferredNLValue
  int brand-non-PreferredCost
  int brand-non-PreferredCumCost
  String brand-non-PreferredNLAttribute
  String brand-non-PreferredAttribute

  int rawOut-of-PocketCost
  int out-of-PocketEstimate
  boolean linearCumulativeCost



  private int numberOfPrimaryCareVisits;
  private int numberOfSpecialityVisits;
  private String isPregnant;
  private String willHaveMajorMedicalProcedure;
  private String hasMajorChronicCondition;


  private int premiumValue;
  private int annualIndividualDeductibleValue;
  private int annualIndividaulOut_of_PocketMaxValue;
  private int annualFamilyDeductibleValue;
  private int annualFamilyOut_of_PocketMaxValue;

  private int estimateUnitCharge;

  private int inpatientHospitalValue;
  private int inpatientHospitalAssumption;
  private int inpatientHospitalUtilization;
  private int inpatientHospitalNLValue;
  private int inpatientHospitalCost;
  private int inpatientHospitalCumCost;
  private String inpatientHospitalNLAttribute;
  private String inpatientHospitalAttribute;

  private int outpatientHospitalValue;
  private int outpatientHospitalAssumption;
  private int outpatientHospitalUtilization;
  private int outpatientHospitalNLValue;
  private int outpatientHospitalCost;
  private int outpatientHospitalCumCost;
  private String outpatientHospitalNLAttribute;
  private String outpatientHospitalAttribute;

  private int emergencyRoomValue;
  private int emergencyRoomAssumption;
  private int emergencyRoomUtilization;
  private int emergencyRoomNLValue;
  private int emergencyRoomCost;
  private int emergencyRoomCumCost;
  private String emergencyRoomNLAttribute;
  private String emergencyRoomAttribute;

  private int preventiveCareValue;
  private int preventiveCareAssumption;
  private int preventiveCareUtilization;
  private int preventiveCareNLValue;
  private int preventiveCareCost;
  private int preventiveCareCumCost;
  private String preventiveCareNLAttribute;
  private String preventiveCareAttribute;

  private int primaryCareVisitsValue;
  private int primaryCareVisitsAssumption;
  private int primaryCareVisitsUtilization;
  private int primaryCareVisitsNLValue;
  private int primaryCareVisitsCost;
  private int primaryCareVisitsCumCost;
  private String primaryCareVisitsNLAttribute;
  private String primaryCareVisitsAttribute;

  private int specialtyCareVisitsValue;
  private int specialtyCareVisitsAssumption;
  private int specialtyCareVisitsUtilization;
  private int specialtyCareVisitsNLValue;
  private int specialtyCareVisitsCost;
  private int specialtyCareVisitsCumCost;
  private String specialtyCareVisitsNLAttribute;
  private String specialtyCareVisitsAttribute;

  private int imaging_AdvancedX_rayValue;
  private int imaging_AdvancedX_rayAssumption;
  private int imaging_AdvancedX_rayUtilization;
  private int imaging_AdvancedX_rayNLValue;
  private int imaging_AdvancedX_rayCost;
  private int imaging_AdvancedX_rayCumCost;
  private String imaging_AdvancedX_rayNLAttribute;
  private String imaging_AdvancedX_rayAttribute;

  private int labTestsValue;
  private int labTestsAssumption;
  private int labTestsUtilization;
  private int labTestsNLValue;
  private int labTestsCost;
  private int labTestsCumCost;
  private String labTestsNLAttribute;
  private String labTestsAttribute;

  private int pt_ot_stValue;
  private int pt_ot_stAssumption;
  private int pt_ot_stUtilization;
  private int pt_ot_stNLValue;
  private int pt_ot_stCost;
  private int pt_ot_stCumCost;
  private String pt_ot_stNLAttribute;
  private String pt_ot_stAttribute;

  private int wellnessServicesValue;
  private int wellnessServicesAssumption;
  private int wellnessServicesUtilization;
  private int wellnessServicesNLValue;
  private int wellnessServicesCost;
  private int wellnessServicesCumCost;
  private String wellnessServicesNLAttribute;
  private String wellnessServicesAttribute;

  private int acupunctureValue;
  private int acupunctureAssumption;
  private int acupunctureUtilization;
  private int acupunctureNLValue;
  private int acupunctureCost;
  private int acupunctureCumCost;
  private String acupunctureNLAttribute;
  private String acupunctureAttribute;

  private int chiropracticCareValue;
  private int chiropracticCareAssumption;
  private int chiropracticCareUtilization;
  private int chiropracticCareNLValue;
  private int chiropracticCareCost;
  private int chiropracticCareCumCost;
  private String chiropracticCareNLAttribute;
  private String chiropracticCareAttribute;

  private int pediatricDentalValue;
  private int pediatricDentalAssumption;
  private int pediatricDentalUtilization;
  private int pediatricDentalNLValue;
  private int pediatricDentalCost;
  private int pediatricDentalCumCost;
  private String pediatricDentalNLAttribute;
  private String pediatricDentalAttribute;

  private int dentalCheck_upValue;
  private int dentalCheck_upAssumption;
  private int dentalCheck_upUtilization;
  private int dentalCheck_upNLValue;
  private int dentalCheck_upCost;
  private int dentalCheck_upCumCost;
  private String dentalCheck_upNLAttribute;
  private String dentalCheck_upAttribute;

  private int pediatricVisionValue;
  private int pediatricVisionAssumption;
  private int pediatricVisionUtilization;
  private int pediatricVisionNLValue;
  private int pediatricVisionCost;
  private int pediatricVisionCumCost;
  private String pediatricVisionNLAttribute;
  private String pediatricVisionAttribute;

  private int eyeExamValue;
  private int eyeExamAssumption;
  private int eyeExamUtilization;
  private int eyeExamNLValue;
  private int eyeExamCost;
  private int eyeExamCumCost;
  private String eyeExamNLAttribute;
  private String eyeExamAttribute;

  private int glassesValue;
  private int glassesAssumption;
  private int glassesUtilization;
  private int glassesNLValue;
  private int glassesCost;
  private int glassesCumCost;
  private String glassesNLAttribute;
  private String glassesAttribute;

  private int mentalHealth_SubstanceAbuseInpatientValue;
  private int mentalHealth_SubstanceAbuseInpatientAssumption;
  private int mentalHealth_SubstanceAbuseInpatientUtilization;
  private int mentalHealth_SubstanceAbuseInpatientNLValue;
  private int mentalHealth_SubstanceAbuseInpatientCost;
  private int mentalHealth_SubstanceAbuseInpatientCumCost;
  private String mentalHealth_SubstanceAbuseInpatientNLAttribute;
  private String mentalHealth_SubstanceAbuseInpatientAttribute;

  private int mentalHealth_SubstanceAbuseOutpatientValue;
  private int mentalHealth_SubstanceAbuseOutpatientAssumption;
  private int mentalHealth_SubstanceAbuseOutpatientUtilization;
  private int mentalHealth_SubstanceAbuseOutpatientNLValue;
  private int mentalHealth_SubstanceAbuseOutpatientCost;
  private int mentalHealth_SubstanceAbuseOutpatientCumCost;
  private String mentalHealth_SubstanceAbuseOutpatientNLAttribute;
  private String mentalHealth_SubstanceAbuseOutpatientAttribute;

  private int maternityPrenatalAndPostnatalVisitsValue;
  private int maternityPrenatalAndPostnatalVisitsAssumption;
  private int maternityPrenatalAndPostnatalVisitsUtilization;
  private int maternityPrenatalAndPostnatalVisitsNLValue;
  private int maternityPrenatalAndPostnatalVisitsCost;
  private int maternityPrenatalAndPostnatalVisitsCumCost;
  private String maternityPrenatalAndPostnatalVisitsNLAttribute;
  private String maternityPrenatalAndPostnatalVisitsAttribute;

  private int prenatalDiagnosisOfGeneticDisordersValue;
  private int prenatalDiagnosisOfGeneticDisordersAssumption;
  private int prenatalDiagnosisOfGeneticDisordersUtilization;
  private int prenatalDiagnosisOfGeneticDisordersNLValue;
  private int prenatalDiagnosisOfGeneticDisordersCost;
  private int prenatalDiagnosisOfGeneticDisordersCumCost;
  private String prenatalDiagnosisOfGeneticDisordersNLAttribute;
  private String prenatalDiagnosisOfGeneticDisordersAttribute;

  private int naturalChildbirthClassesValue;
  private int naturalChildbirthClassesAssumption;
  private int naturalChildbirthClassesUtilization;
  private int naturalChildbirthClassesNLValue;
  private int naturalChildbirthClassesCost;
  private int naturalChildbirthClassesCumCost;
  private String naturalChildbirthClassesNLAttribute;
  private String naturalChildbirthClassesAttribute;

  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyValue;
  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyAssumption;
  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyUtilization;
  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyNLValue;
  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyCost;
  private int hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyCumCost;
  private String hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyNLAttribute;
  private String hospitalServicesForNormalDeliveryC_sectionAndComplicationsOfPregnancyAttribute;

  private int newbornCareValue;
  private int newbornCareAssumption;
  private int newbornCareUtilization;
  private int newbornCareNLValue;
  private int newbornCareCost;
  private int newbornCareCumCost;
  private String newbornCareNLAttribute;
  private String newbornCareAttribute;

  private int blankFieldValue;
  private int blankFieldAssumption;
  private int blankFieldUtilization;
  private int blankFieldNLValue;
  private int blankFieldCost;
  private int blankFieldCumCost;
  private String blankFieldNLAttribute;
  private String blankFieldAttribute;

  private int prescriptionBrandDeductableValue;
  private int prescriptionBrandDeductableAssumption;
  private int prescriptionBrandDeductableUtilization;
  private int prescriptionBrandDeductableNLValue;
  private int prescriptionBrandDeductableCost;
  private int prescriptionBrandDeductableCumCost;
  private String prescriptionBrandDeductableNLAttribute;
  private String prescriptionBrandDeductableAttribute;

  private int genericValue;
  private int genericAssumption;
  private int genericUtilization;
  private int genericNLValue;
  private int genericCost;
  private int genericCumCost;
  private String genericNLAttribute;
  private String genericAttribute;

  private int brand_PreferredValue;
  private int brand_PreferredAssumption;
  private int brand_PreferredUtilization;
  private int brand_PreferredNLValue;
  private int brand_PreferredCost;
  private int brand_PreferredCumCost;
  private String brand_PreferredNLAttribute;
  private String brand_PreferredAttribute;

  private int brand_non_PreferredValue;
  private int brand_non_PreferredAssumption;
  private int brand_non_PreferredUtilization;
  private int brand_non_PreferredNLValue;
  private int brand_non_PreferredCost;
  private int brand_non_PreferredCumCost;
  private String brand_non_PreferredNLAttribute;
  private String brand_non_PreferredAttribute;

  private int rawOut_of_PocketCost;
  private int out_of_PocketEstimate;
  private String linearCumulativeCost;


  public String toString() {

		Gson gson = new Gson();
		return gson.toJson(this);

	}



##### DT Source package = package com.getinsured.eligibility.service.rules;
//generated from Decision Table
import com.getinsured.eligibility.model.RulePlanOutput;
	no-loop true
// rule values at D15, header at D10
rule "Usage_15"
	salience 3000
	when
		$rulePlanOutput : RulePlanOutput(this==$rulePlanOutput, outpatientHospitalUtilization==2, isPregnant=="tt")
	then
		$rulePlanOutput.setInpatientHospitalUtilization(4); update($rulePlanOutput); System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at D16, header at D10
rule "Usage_16"
	salience 2990
	when
		$rulePlanOutput : RulePlanOutput(this==$rulePlanOutput, outpatientHospitalUtilization==1, isPregnant=="ww")
	then
		$rulePlanOutput.setInpatientHospitalUtilization(5); update($rulePlanOutput); System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at D17, header at D10
rule "Usage_17"
	salience 2980
	when
		$rulePlanOutput : RulePlanOutput(this==$rulePlanOutput, outpatientHospitalUtilization==3, isPregnant=="ee")
	then
		$rulePlanOutput.setInpatientHospitalUtilization(6); update($rulePlanOutput); System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at D18, header at D10
rule "Usage_18"
	salience 2970
	when
		$rulePlanOutput : RulePlanOutput(this==$rulePlanOutput, outpatientHospitalUtilization==2)
	then
		$rulePlanOutput.setInpatientHospitalUtilization(7); update($rulePlanOutput); System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at D19, header at D10
rule "Usage_19"
	salience 2960
	when
		$rulePlanOutput : RulePlanOutput(isPregnant=="tt")
	then
		$rulePlanOutput.setInpatientHospitalUtilization(8); update($rulePlanOutput); System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at D20, header at D10
rule "Usage_20"
	salience 2950
	when
	then
end









##### Before Firing Rules....
Rule Name: Usage_16
##### After Firing Rules....
InpatientHospitalUtilization => 1
##### DT Source package = package com.getinsured.eligibility.service.rules;
//generated from Decision Table
import com.getinsured.eligibility.model.RulePlanOutput;
	no-loop true
	auto-focus true
// rule values at B16, header at B11
rule "CostCal_16"
	agenda-group "cost"
	when
		$rulePlanOutput : RulePlanOutput(this == $rulePlanOutput, inpatientHospitalAttribute == "copay")
	then
		$rulePlanOutput.setInpatientHospitalCost(101); update($rulePlanOutput); //drools.setFocus("cost");
System.out.println("Rule Name: " + drools.getRule().getName());
end

// rule values at B17, header at B11
rule "CostCal_17"
	agenda-group "cost1"
	when
		$rulePlanOutput : RulePlanOutput(outpatientHospitalAttribute == "copay")
	then
		$rulePlanOutput.setInpatientHospitalCost(102); update($rulePlanOutput); //drools.setFocus("cost");
System.out.println("Rule Name: " + drools.getRule().getName());
end