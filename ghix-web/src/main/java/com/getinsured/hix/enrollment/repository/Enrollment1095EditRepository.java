
package com.getinsured.hix.enrollment.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;


public interface Enrollment1095EditRepository extends JpaRepository<Enrollment1095, Long>{
	
	
	@Query("From EnrollmentMember1095 m where m.enrollment1095.id =:id AND m.memberType =:memberType")
	EnrollmentMember1095 findSpouseDetailsByEnrollment1095Id(@Param("id") Integer id , @Param("memberType") String memberType);
	
	@Query("FROM Enrollment1095 as en WHERE en.id = :id ")
	Enrollment1095 findEnrollment1095ById(@Param("id") Integer id);
	
	@Query("SELECT m.firstName, m.lastName, m.ssn ,m.memberType from EnrollmentMember1095 m where m.enrollment1095.id =:id")
	List<EnrollmentMember1095> findEnrollmentMember1095ById(@Param("id") Integer id);
	
	@Modifying
	@Transactional(readOnly=false)
	@Query("Update Enrollment1095 en set en.resendPdfFlag =:status where en.id =:id")
	public int updateResendFlag(@Param("id") Integer id, @Param("status") String resendPdfFlag);
	
	@Query("From EnrollmentMember1095 as m where m.enrollment1095.id =:enrollment1095Id")
	public List<EnrollmentMember1095> findEnrollmentMember1095ListByEnrollment1095Id(@Param("enrollment1095Id") Integer enrollment1095Id);
	
}

//LOWER(en.firstName)  LIKE '%' || LOWER(:firstName) || '%'