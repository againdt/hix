package com.getinsured.hix.broker.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerDocuments;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.model.BrokerPhoto;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyDetailsListDTO;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.SearchAgencyDto;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.broker.BrokerExportResponseDTO;

public interface BrokerService {

	/**
	 * Saves {@link Broker} object along with {@link Location}
	 * 
	 * @param broker
	 *            {@link Broker}
	 * @param triggerIND
	 *            Flag to check if AHBX interface is to be called
	 * @return
	 */
	Broker saveBrokerWithLocation(Broker broker);

	/**
	 * Retrieves brokers based on search criteria
	 * 
	 * @param broker
	 *            {@link Broker}
	 * @param startRecord
	 *            Start of record on page
	 * @param pageSize
	 *            Page size for pagination
	 * @return {@link Map} of {@link Broker}
	 */
	Map<String, Object> findBroker(Broker broker, Integer startRecord, Integer pageSize);

	/**
	 * Retrieves brokers based on search criteria
	 * 
	 * @param broker
	 *            {@link Broker}
	 * @param startRecord
	 *            Start of record on page
	 * @param pageSize
	 *            Page size for pagination
	 * @return {@link Map} of {@link Broker}
	 */
	Map<String, Object> searchBroker(String anonymousFlag,Broker broker, Integer startRecord,
			Integer pageSize);

	/**
	 * Retrieves broker by broker id
	 * 
	 * @param id
	 *            broker id
	 * @return {@link Broker}
	 */
	Broker getBrokerDetail(int id);

	/**
	 * Retrieves broker by license number
	 * 
	 * @param licenseNumber
	 *            license number of the broker
	 * @return {@link Broker}
	 */
	Broker findBrokerByLicenseNumber(String licenseNumber);

	/**
	 * Retrieves broker based on his user id
	 * 
	 * @param userid
	 *            user id
	 * @return {@link Broker}
	 */
	Broker findBrokerByUserId(int userid);

	/**
	 * Saves broker certification status
	 * 
	 * @param broker
	 *            {@link Broker}
	 * @param paymentMethods
	 *            {@link PaymentMethods}
	 */
	void saveBrokerCertification(Broker broker, PaymentMethods paymentMethods);

	/**
	 * Retrieves all the brokers
	 * 
	 * @return {@link List} of {@link Broker}
	 */
	List<Broker> findAllBrokers();

	/**
	 * Loads Brokers status history from broker audit table
	 * 
	 * @param brokerId
	 *            broker id
	 * @return {@link List} of {@link Map} of {@link Broker}
	 */
	List<Map<String, Object>> loadBrokerStatusHistory(Integer brokerId);

	/**
	 * Handles get Payment Method Details For Broker
	 * 
	 * @param moduleId
	 *            Module Id i.e. Broker Id
	 * @param moduleName
	 *            Module Name i.e. BROKER
	 * @return paymentMethods {@link PaymentMethods}
	 */
	PaymentMethods getPaymentMethodDetailsForBroker(int moduleId, PaymentMethods.ModuleName moduleName);

	/**
	 * Retrieves AHBX broker delegation response.
	 * 
	 * @param delegationRequest
	 *            {@link DelegationRequest}
	 * @return {@link DelegationResponse}
	 * @throws GIException
	 */
	DelegationResponse getAhbxBrokerDelegateResp(DelegationRequest delegationRequest) throws GIException;

	/**
	 * Updates delegation information
	 * 
	 * @param brokerId
	 *            Broker id
	 * @param delegationCode
	 *            delegation code of Broker sent by AHBX
	 * @param responseCode
	 *            AHBX response code
	 * @param responseDescription
	 *            AHBX response description
	 * @return {@link Broker}
	 */
	Broker updateDelegateInfo(int brokerId, String delegationCode, int responseCode, String responseDescription);

	/**
	 * save document information
	 * 
	 * @param createdBy
	 *            Id of logged in Admin
	 * @param documentId
	 *            ECM encrypted Id
	 * @param documenttype
	 *            Status for which doc is to be uploaded
	 * @param mimeType
	 *            Mime type of doc
	 * @param originalFile
	 *            Doc original name
	 * @return int Seq id of record
	 */
	int saveDocument(String createdBy, String documentId, String documenttype, String mimeType, String originalFile);

	/**
	 * get list of Documents
	 * 
	 * @param documentId
	 *            record id in broker_doc table
	 * @return BrokerDocuments record retrieved from table
	 */
	BrokerDocuments getListofDocuments(Integer documentId);
	
	String generateEmployerActivationLink(Employer employer, Broker brokerObj) throws GIException;
	
	/**
	     * Retrieves broker photo by broker id
	     *
	     * @param brokerId
	     *            {@link brokerId}
	    
	     * @return BrokerPhoto
	     */
	    BrokerPhoto getBrokerPhotoById(int brokerId);
	    
	    /**
	     * Retrieves broker photo by user id
	     *
	     * @param userId
	     *            {@link userId}
	    
	     * @return BrokerPhoto
	     */
	    BrokerPhoto  getBrokerPhotoByUserId(int userId);
	    
	    /**
	     * save broker photo into Brokers table
	     *
	     * @param brokerId
	     *            {@link brokerId}
	    
	     * @return BrokerPhoto
	     */
	    int saveBrokerPhoto(int brokerId, byte[] fileContent);
	    
	    /**
		 * Uploads the broker photo on ECM
		 * 
		 * @param brokerId
		 *            {@link brokerId}
		 *  @param fileInput
		
		 * @return imageDocumentId
		 */
		int uploadPhoto(int brokerId,MultipartFile fileInput) throws GIException;
		
		/**
		 * Uploads the broker photo on ECM and sets the photo Blob object to null
		 * 
		 * @param brokerPhoto
		 *            {@link brokerPhoto}
		 * 
		 * @return BrokerPhoto
		 */
		BrokerPhoto uploadPhoto(BrokerPhoto brokerPhoto) throws GIException;
		
		/**
		 * Finds Brokers with given Filtering and Ordering Criteria
		 * 
		 * @param broker
		 * @param startRecord
		 * @param pageSize
		 * @param sortBy
		 * @param sortOrder
		 * @return Map of Brokers and Record Count
		 */
		Map<String, Object> findBroker(Broker broker, Integer startRecord,
				Integer pageSize, String sortBy, SortOrder sortOrder);
		
		/**
		 * Get export agent list 
		 * 
		 * @param broker
		 * @return BrokerExportResponseDTO
		 */
		BrokerExportResponseDTO getExportAgentList();
		
		/**
		 * Get agent list for agency 
		 * 
		 * @param agencyId
		 * @return brokersList
		 */
		AgencyBrokerListDTO getBrokersForAgency(SearchBrokerDTO searchBrokerDTO);
		
		/**
		 * Saves userId of Agency manager
		 * 
		 * @param broker
		 * @return broker
		 */
		Broker saveAgencyManagerUser(Broker broker);
		
		AgencyDetailsListDTO findAgency(SearchAgencyDto searchAgencyDto);

		List<Map<String, Object>> loadBrokerActivityStatusHistory(Integer brokerId);

		Comment saveComments(long targetId, String commentText);

		AgencyInformationDto findAgencyByLoggedInUser(AccountUser user);
		
		List<Broker> findBrokersByLicenseNumber(String licenseNumber);

		List<Broker> findCompleteBrokersByLicenseNumber(String licenseNumber);
		
		/**
		 * 
		 * @param agencyId
		 * @param agencyRole
		 * @return
		 */
		int getAgencyManagerCount( Long agencyId,String agencyRole);

}