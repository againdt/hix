package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;

import org.apache.commons.lang3.StringUtils;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class NetworkIdProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;
	private static final String HYPHEN = "-";
	private static final int TWENTIETH_CENTURY = 20;

	@Override
	public Object process(Object objToBeValidated) throws ProviderValidationException {

		String networkIds = objToBeValidated.toString();
		networkIds = networkIds.replaceAll("\"", "");
		String[] networks = networkIds.split("\\|");
		ArrayList<String> networkList = new ArrayList<String>();

		for (String networkId : networks) {

			int len = networkId.length();
			if (len != 16) {
				throw new ProviderValidationException("Invalid input provided for network id ["+ networkId +"] from " + networkIds);
			}
			String[] networkData = new String[2];
			networkData[0] = networkId.substring(0,5);
			networkData[1] = networkId.substring(5);

			String[] yearData = networkData[1].split(HYPHEN);

			if (StringUtils.isNumeric(yearData[1])) {

				if (0 == Integer.valueOf(yearData[1].substring(2))) {
					networkList.add(networkData[0] + HYPHEN + yearData[0] + HYPHEN + TWENTIETH_CENTURY + yearData[1].substring(0, 2));
				}
				else {
					networkList.add(networkData[0] + HYPHEN + yearData[0] + HYPHEN + TWENTIETH_CENTURY + yearData[1].substring(0, 2));
					networkList.add(networkData[0] + HYPHEN + yearData[0] + HYPHEN + TWENTIETH_CENTURY + yearData[1].substring(2));
				}
			}
		}

		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null) ? null : (String) tmp;

		if (outputField == null) {
			//Not available in metadata file, create a default one 
			outputField = "network_id";
			this.context.addContextInfo("output_field", outputField);
			
		}

		if (networkList.size() > 0) {
			context.addContextInfo(outputField, networkList);
		}
		return networkIds;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
