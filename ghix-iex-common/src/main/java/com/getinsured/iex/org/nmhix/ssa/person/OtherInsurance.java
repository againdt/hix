//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OtherInsurance complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OtherInsurance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="medicareEligibleIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tricareEligibleIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="peaceCorpsIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="otherStateOrFederalProgramIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="otherStateOrFederalProgramType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="otherStateOrFederalProgramName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="noneOfTheAboveIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OtherInsurance", propOrder = {
    "medicareEligibleIndicator",
    "tricareEligibleIndicator",
    "peaceCorpsIndicator",
    "otherStateOrFederalProgramIndicator",
    "otherStateOrFederalProgramType",
    "otherStateOrFederalProgramName",
    "noneOfTheAboveIndicator"
})

@Entity
public class OtherInsurance {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
    protected Boolean medicareEligibleIndicator;
    protected Boolean tricareEligibleIndicator;
    protected Boolean peaceCorpsIndicator;
    
    @Column(name="OSOFPIndicator")
    protected Boolean otherStateOrFederalProgramIndicator;
    
    @Column(name="OSOFPType")
    protected String otherStateOrFederalProgramType;
    
    @Column(name="OSOFPName")
    protected String otherStateOrFederalProgramName;
    
    @Column(name="NOTAIndicator")
    protected Boolean noneOfTheAboveIndicator;

    /**
     * Gets the value of the medicareEligibleIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isMedicareEligibleIndicator() {
        return medicareEligibleIndicator;
    }

    /**
     * Sets the value of the medicareEligibleIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setMedicareEligibleIndicator(Boolean value) {
        this.medicareEligibleIndicator = value;
    }

    /**
     * Gets the value of the tricareEligibleIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTricareEligibleIndicator() {
        return tricareEligibleIndicator;
    }

    /**
     * Sets the value of the tricareEligibleIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTricareEligibleIndicator(Boolean value) {
        this.tricareEligibleIndicator = value;
    }

    /**
     * Gets the value of the peaceCorpsIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isPeaceCorpsIndicator() {
        return peaceCorpsIndicator;
    }

    /**
     * Sets the value of the peaceCorpsIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPeaceCorpsIndicator(Boolean value) {
        this.peaceCorpsIndicator = value;
    }

    /**
     * Gets the value of the otherStateOrFederalProgramIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isOtherStateOrFederalProgramIndicator() {
        return otherStateOrFederalProgramIndicator;
    }

    /**
     * Sets the value of the otherStateOrFederalProgramIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setOtherStateOrFederalProgramIndicator(Boolean value) {
        this.otherStateOrFederalProgramIndicator = value;
    }

    /**
     * Gets the value of the otherStateOrFederalProgramType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherStateOrFederalProgramType() {
        return otherStateOrFederalProgramType;
    }

    /**
     * Sets the value of the otherStateOrFederalProgramType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherStateOrFederalProgramType(String value) {
        this.otherStateOrFederalProgramType = value;
    }

    /**
     * Gets the value of the otherStateOrFederalProgramName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOtherStateOrFederalProgramName() {
        return otherStateOrFederalProgramName;
    }

    /**
     * Sets the value of the otherStateOrFederalProgramName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOtherStateOrFederalProgramName(String value) {
        this.otherStateOrFederalProgramName = value;
    }

    /**
     * Gets the value of the noneOfTheAboveIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isNoneOfTheAboveIndicator() {
        return noneOfTheAboveIndicator;
    }

    /**
     * Sets the value of the noneOfTheAboveIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setNoneOfTheAboveIndicator(Boolean value) {
        this.noneOfTheAboveIndicator = value;
    }

}
