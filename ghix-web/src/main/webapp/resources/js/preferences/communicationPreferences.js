$('.ttclass').tooltip();

	/*     $('#submitAdditionalInfo').on('click', function(){
	 $('div.alert, form.box-loose, h1').hide();
	 $('div.processingRequest').show(); 
	 var formJSON = $("form").serializeArray();
	 console.log(formJSON)
	 }); */

	var test = $('#phone1').val();
	/*     var val = $("#pNum").val();  */

	(function() {
		angular.module("formApp", [ "formCtrlM", 'ui.mask','spring-security-csrf-token-interceptor']);
	})();

	(function() {

		var formCtrl = function($scope, $http) {
			var controller = this;
			this.data = {};
			this.phone1Invalid = false;
			this.phone2Invalid = false;
			this.phone3Invalid = false;

			this.addressValidation = false;
			this.validIdahoAddress = true;
			//this.addressCheckIndicator = false;
			
			this.data.prefSpokenLang = "Please Select";
			this.data.prefWrittenLang = "Please Select";
			this.data.phone1;

			this.checkLen = function(len, validator) {
				if (len !== event.target.value.length) {
					this[validator] = true;
				} else {
					this[validator] = false;
				}
			};

			this.addressCheck = function(){
				
				if(this.data.addressLine1 === undefined || this.data.city === undefined || this.data.zipcode === '' ){
					
					return;
				}
				
				//this.addressCheckIndicator = true;
				
				var enteredAddress = this.data.addressLine1 + ',' + (this.data.addressLine2 === undefined? '': this.data.addressLine2) + ',' + this.data.city + ',' + this.data.state + ',' + this.data.zipcode;
				var ids = "home_addressLine1~home_addressLine2~home_primary_city~home_primary_state~home_primary_zip~homelat~homelon~rdihome~countyhome";
				$http({
					method : 'GET',
					url : "/hix/platform/validateaddressnew?enteredAddress=" + enteredAddress+"&ids=" + ids
				}).success(function(response) {
					//this.addressValidation = true;
					if(response.toUpperCase() === 'SUCCESS'){
						controller.addressValidation = true;
						controller.validIdahoAddress = false;
						$('#addressValidationModal .modal-body').load( '/hix/ssap/addressmatch', function(){
							$('#addressValidationModal').modal({
								keyboard: false
							});
						});
					}else if(response.substr(0,7).toUpperCase() === 'FAILURE'){
						controller.addressValidation = false;
						
						$('#noAddressModal .modal-body').html('<p>The home address you entered is not in the postal database. Please check it for accuracy.</p>');
						$('#noAddressModal').modal();
						controller.validIdahoAddress = false;
					}else if(response.substr(0,11).toUpperCase() === 'MATCH_FOUND'){
						controller.addressValidation = true;
						controller.validIdahoAddress = true;
					}

				}).error(function(data) {
					
				});  

			};
			
			this.updateAddress = function(){
				var selectedAddressArr = $('#addressValidationModal input:radio:checked').val().split(",");
				this.data.addressLine1 + ',' + (this.data.addressLine2 === undefined? '': this.data.addressLine2) + ',' + this.data.city + ',' + this.data.state + ',' + this.data.zipcode;
				if(this.data.addressLine1 != selectedAddressArr[0]) controller.validIdahoAddress = true;				
				this.data.addressLine1 = selectedAddressArr[0];
				if((this.data.addressLine2 != undefined) && (this.data.addressLine2 != selectedAddressArr[1])) controller.validIdahoAddress = true;				
				this.data.addressLine2 = selectedAddressArr[1] === 'null'? '' : selectedAddressArr[1];
				if(this.data.city != selectedAddressArr[2]) controller.validIdahoAddress = true;
				this.data.city = selectedAddressArr[2]; 
				//this.data.state = selectedAddressArr[3];
				if(this.data.zipcode != selectedAddressArr[4]) controller.validIdahoAddress = true;
				this.data.zipcode = selectedAddressArr[4];
				if(this.data.state != selectedAddressArr[3]) controller.validIdahoAddress = false;
			};
			

			this.submit = function() {

				this.data.phoneNumber = "" + this.data.phone1
						+ this.data.phone2 + this.data.phone3;
				var dataToSend = {};
				angular.forEach(this.data, function(val, key) {
					dataToSend[key] = val;
				});

				if (this.data.prefSpokenLang === "Spanish") {
					dataToSend.prefSpokenLang = "SP-SPA";
				} else if (this.data.prefSpokenLang === "English") {
					dataToSend.prefSpokenLang = "SP-ENG";
				} else {
					delete dataToSend.prefSpokenLang
				}

				if (this.data.prefWrittenLang === "Spanish") {
					dataToSend.prefWrittenLang = "SP-SPA";
				} else if (this.data.prefWrittenLang === "English") {
					dataToSend.prefWrittenLang = "SP-ENG";
				} else {
					delete dataToSend.prefWrittenLang
				}
				;

				delete dataToSend.phone1;
				delete dataToSend.phone2;
				delete dataToSend.phone3;
				var config = {
					headers: { 'Content-Type' : 'application/json; charset=UTF-8'}
				};	
				
				/*if(controller.validIdahoAddress==false){
					$('#noAddressModal .modal-body').html('<p>The home address you entered is not in the postal database. Please check it for accuracy.</p>');
					$('#noAddressModal').modal();					
					return false;
				}*/
				$http({
					method : 'POST',
					url : "/hix/saveContactPref",
					data : JSON.stringify(dataToSend),
					config:	config
				}).success(function(data) {
					if (data === 'success') {
						location.href = "../indportal";
					} else if (data == 'failure') {
						//console.log('failure', data)
						alert('Unable to save Communication Preferences');
					}

				}).error(function(data) {
					//console.log("err", data);
					alert('Unable to save Communication Preferences');
				});
			};

		};

		angular.module("formCtrlM", []).controller('formCtrl', formCtrl)
	})();
	
	(function() {
		var numOnly = function() {
			return {
				restrict : "A",
				require : "ngModel",
				link : function(scope, ele, attrs, ngM) {
					ele.bind("keydown", function(event) {
						if (event.which === 96 || event.which === 48) {
							if (event.target.value === "0"){
								event.preventDefault();
							}
						};
						
						//for copy and paste;
						if(event.ctrlKey || event.metaKey) {
							if (event.which === 86 || event.which === 67){
								return;
							}
						}
						
						if (event.which < 8 || event.which > 57) {
							if (event.which >= 96 && event.which <=105){
								return;
							}
							event.preventDefault();
						} else {
							//prevent special characters; 
							if (event.shiftKey){
							event.preventDefault();
						}
						}
					})
			  }
			}
	    };

		angular.module('formApp')
			.directive("numOnly", numOnly)
	})();


	(function(){
		var dateValidation = function(){
			return {
				restrict : 'AE',
				require : 'ngModel',
				link : function(scope, element, attrs, ngModel) {		
					
					var dateType = attrs.dateValidation;
					
					function isDateValid(value){
						//remove placeholder
						var date = value.replace(/[A-Z]/g, '');
						
						//remove extra-last-character
						if(date.length === 11){
							date = date.substring(0, date.length - 1);
						}
						
						
						if(date.length === 10){
							if(!moment(date,'MM/DD/YYYY').isValid()){
								return false;
							}
							
							if(dateType === 'dob'){
								if(ageCheck('between',date, 0, 104)){
									return true;
								}else{
									return false;
								};
							}else if(dateType === 'future'){
								if(ageCheck('greater',date, 0)){
									return true;
								}else{
									return false;
								};
							}else if(dateType === 'under21'){
								if(ageCheck('between',date, 21, 104)){
									return true;
								}else{
									return false;
								};
							}else if(dateType === 'qe'){
								var inputDate = date.split('/');
								var start = new Date(inputDate[2], inputDate[0]-1, inputDate[1]);

								var today = new Date();
								var end = new Date(today.getFullYear(), today.getMonth(), today.getDate());
								
								var diff = (start.getTime() - end.getTime()) / (24*60*60*1000);
								if(diff <= 0 && diff >= -60){
									return true;
								}else{
									return false;
								};
							}
						}else{
							return true;
						}
						
					}

					function ageCheck(comparator, birth, age, maxAge){
						var birthDate = new Date(birth);
						var today = new Date();
						
						 var years = (today.getFullYear() - birthDate.getFullYear());

					    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
					        years--;
					    }
					    
					    if(comparator === 'older'){
					    	if(years >= parseInt(age)){
						    	return true;
						    }else{
						    	return false;
						    }
						}else if(comparator === 'between'){
							if(years >= parseInt(age) && years <= parseInt(maxAge)){
						    	return true;
						    }else{
						    	return false;
						    }
						}else{
							if(years < parseInt(age)){
						    	return true;
						    }else{
						    	return false;
						    }
						}
						
					};

					ngModel.$parsers.unshift(function(value){
						var dateValidationResult = isDateValid(value);
						
						ngModel.$setValidity(dateType, dateValidationResult);
						
						return value;	
					});
					
					 ngModel.$formatters.unshift(function(value) {
						 if(value === undefined || value === null){
							 return;
						 }
						 var dateValidationResult = isDateValid(value);
							
						 ngModel.$setValidity(dateType, dateValidationResult);
						 
						 return value;
		             });
					
					
				}
			};

		};
		angular.module('formApp').directive("dateValidation", dateValidation);
	})();
