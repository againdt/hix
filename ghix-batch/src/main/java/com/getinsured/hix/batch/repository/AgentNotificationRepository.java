package com.getinsured.hix.batch.repository;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.Notice;


public interface AgentNotificationRepository extends JpaRepository<Notice, Integer> {

}
