package com.getinsured.hix.entity.admin.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.entity.admin.service.EntityIndTriggerService;
import com.getinsured.hix.entity.assister.repository.IAssisterRepository;
import com.getinsured.hix.entity.enrollmententity.repository.ISiteRepository;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentIsDefault;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.thoughtworks.xstream.XStream;

@Service
public class EntityIndTriggerServiceImpl implements EntityIndTriggerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(EntityIndTriggerServiceImpl.class);

	private static final String ONE = "1";
	private static final String YES = "Yes";
	private static final String NO = "No";
	
	@Autowired
	private RestTemplate restTemplate;
	 
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;

	@Autowired
	private IAssisterRepository assisterRepository;
	
	@Autowired
	private ISiteRepository siteRepository;

	
	@GiFeature("ahbx.aee.ind35.enabled")
	@Override
	public String triggerInd35(EnrollmentEntity entity) {
		 
		Site primarySite = null;
		
		try {
			// Get Payment method information
			LOGGER.info("Fetching Payment Method for EE");
			PaymentMethods paymentMethod =   findPaymentMethodDetailsForEntity(entity.getId(), entity.getEntityType());
			
			// Get Assister information
			LOGGER.info("Fetching Assisters of EE");
			List<Assister> assisterAssociatedWithEEList= new ArrayList<Assister>();
			List<Assister> listOfAssisters  ;

			if (EntityConstants.STATUS_DEREGISTERED.equalsIgnoreCase(entity.getRegistrationStatus())) {
				listOfAssisters = assisterRepository.findByEntity(entity);
				
				if(listOfAssisters != null){
					for(Assister assister:listOfAssisters){
						assister.setStatus(EntityConstants.STATUS_INACTIVE);
						assister.setCertificationStatus(EntityConstants.ENTITY_DEREGISTERED);
						assisterAssociatedWithEEList.add(assisterRepository.save(assister));
					}
				}
			} else {
				 assisterAssociatedWithEEList = assisterRepository.findByEntity(entity);
			}
			
			ArrayList<Assister> assisterList = new ArrayList<Assister>(assisterAssociatedWithEEList);  
			
			// Get Primary Site information
			LOGGER.info("Fetching Primary Site of EE");
			List<Site> siteList = siteRepository.findByEnrollmentEntityAndSiteType( entity.getId(), Site.site_type.PRIMARY.toString());

			// Retrieving primary site from the list
			if (siteList != null && !siteList.isEmpty()) {
				LOGGER.debug("Retrieving Primary Site from the list");
				primarySite = siteList.get(0);
			}

			LOGGER.info("Send Enrollment Entity Information call starts");
			String wsResponse =  sendEnrollmentEntityInfo(entity, paymentMethod, primarySite, assisterList);
			/*LOGGER.info("Send Enrollment Entity Information call ends. Received the following response : "
					+ wsResponse);*/
			
			LOGGER.info("Send Enrollment Entity Information call ends. ");
			return wsResponse;
		} catch (Exception ex) {
			LOGGER.error("Error while sending Entities information to AHBX : ", ex);
		}
	
		return "";

	}
	
	@Transactional(readOnly = true)
	private PaymentMethods findPaymentMethodDetailsForEntity(int moduleId, String entityModule) {

		LOGGER.info("findPaymentMethodDetailsForEntity: START");

		LOGGER.info("Get PaymentMethodDetails For Entity");
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
		paymentMethodRequestDTO.setModuleID(moduleId);
		paymentMethodRequestDTO.setModuleName(PaymentMethods.ModuleName.ASSISTERENROLLMENTENTITY);
		paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.Active);
		paymentMethodRequestDTO.setIsDefaultPaymentMethod(PaymentIsDefault.Y);
		LOGGER.info("Retrieving Payment Detail REST Call Starts.");
		String paymentMethodsStr = ghixRestServiceInvoker.invokeRestService(paymentMethodRequestDTO, FinanceServiceEndPoints.SEARCH_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
		LOGGER.info("Retrieving Payment Detail REST Call Ends.");

		PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsStr);
		PaymentMethods paymentMethods = null;
		Map<String, Object> paymentMethodMap = paymentMethodResponse.getPaymentMethodMap();
		if (paymentMethodMap != null && paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY) != null) {
			List<PaymentMethods> paymentMethodList = (List<PaymentMethods>) paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
			paymentMethods = paymentMethodList != null ? paymentMethodList.get(0) : null;
		}

		LOGGER.info("findPaymentMethodDetailsForEntity: END");

		return paymentMethods;
	}


	private String sendEnrollmentEntityInfo(EnrollmentEntity enrollmentEntity, PaymentMethods eePaymentMethod, Site primarySite,ArrayList<Assister> assisterList) {
		List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();

		String wsResponse = null;
		entityRequestDTOList = populateEnrollmentEntityDto(enrollmentEntity, eePaymentMethod, primarySite, assisterList);

		/*
		 * Currently logging response received from AHBX. TODO Need to log
		 * response in db
		 */
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Enrollment Entity : ").append(enrollmentEntity.getId()).append(" : ");
		strResponse.append(enrollmentEntity.getUser().getFirstName()).append(" ");
		strResponse.append(enrollmentEntity.getUser().getLastName());

		wsResponse = callAHBXAndGetResponse(entityRequestDTOList, wsResponse, strResponse);

		return wsResponse;
	}


	private String callAHBXAndGetResponse(List<EntityRequestDTO> entityRequestDTOList, String wsResponse, StringBuilder strResponse) {

		LOGGER.info("IND35: Sending Enrollment Entity Details to ghix-ahbx: Request objects are: "+entityRequestDTOList);
		StringBuilder logger = new StringBuilder();
		logger.append("IND35: Sending Enrollment Entity Details to ghix-ahbx: Request objects are: ");
		
		//Appending Request objects. For logging purpose only
		for(EntityRequestDTO entityRequestDTO: entityRequestDTOList){
			logger.append(entityRequestDTO.toString());
			logger.append("\n");
		}
		
		/*LOGGER.info(logger.toString());*/
		
		try {
			String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);
			wsResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL, strEntityRequestDTOList, String.class);

			strResponse.append("\nResponse is: ").append(wsResponse);

			List<EntityResponseDTO> entityResponseList = (List<EntityResponseDTO>) GhixUtils.xStreamHibernateXmlMashaling().fromXML(wsResponse);

			StringBuilder sb = new StringBuilder();
			sb.append("IND35: Successfully sent Enrollment Entity Details over Rest call to Ghix-Ahbx. Received following response : \n");
			int i = 0;
			for (EntityResponseDTO entityResponse : entityResponseList) {
				sb.append("Response Object : ").append(i++);
				sb.append(" : ").append(entityResponse.toString());
			}
			/*LOGGER.info(sb.toString());*/

		} catch (Exception exception) {

			/**
			 * TODO TBD how to handle network breakdown
			 */
			LOGGER.error("IND35: Send Enrollment Entity Details Web Service failed: Exception is :"
					, exception);
		}
		return wsResponse;
	
	}


	private List<EntityRequestDTO> populateEnrollmentEntityDto(EnrollmentEntity enrollmentEntity,PaymentMethods eePaymentMethod, Site primarySite, ArrayList<Assister> assisterList) {


		LOGGER.info("Populate Enrollment Entity object in request DTO");
		List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();
		EntityRequestDTO entityRequestDTO = new EntityRequestDTO();

		entityRequestDTO.setRecordId(enrollmentEntity.getId());
		entityRequestDTO.setRecordType("Entity");
		entityRequestDTO.setFirstName(enrollmentEntity.getUser() != null ? enrollmentEntity.getUser().getFirstName(): "");
		entityRequestDTO.setLastName(enrollmentEntity.getUser() != null ? enrollmentEntity.getUser().getLastName() : "");

		entityRequestDTO.setBusinessLegalName(enrollmentEntity.getBusinessLegalName());
		entityRequestDTO.setFunctionalIdentifier(enrollmentEntity.getEntityNumber());
		entityRequestDTO.setFederalEIN(Long.toString(enrollmentEntity.getFederalTaxID() ));
		entityRequestDTO.setStateEIN(enrollmentEntity.getStateTaxID());
		entityRequestDTO.setEntityIdNumber(Long.valueOf(enrollmentEntity.getId()));
		// adding new fields to accomodate changes as of 07/15
		entityRequestDTO.setOrgType(enrollmentEntity.getOrgType());
		entityRequestDTO.setEntityType(enrollmentEntity.getEntityType());
		if (enrollmentEntity.getReceivedPayments() != null & ONE.equals(enrollmentEntity.getReceivedPayments())) {
			entityRequestDTO.setReceivePayments(YES);
		} else {
			entityRequestDTO.setReceivePayments(NO);
		}
		// Assuming Entity Mailing Location to be Primary Site Location
		if (primarySite != null) {
			entityRequestDTO.setAddressLine1(primarySite.getMailingLocation() != null ? primarySite
					.getMailingLocation().getAddress1() : null);
			entityRequestDTO.setAddressLine2(primarySite.getMailingLocation() != null ? primarySite
					.getMailingLocation().getAddress2() : null);
			entityRequestDTO.setCity(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation()
					.getCity() : null);
			entityRequestDTO.setState(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation()
					.getState() : null);
			entityRequestDTO.setZipCode(primarySite.getMailingLocation() != null ? String.valueOf(primarySite
					.getMailingLocation().getZip()) : null);
		} else {
			entityRequestDTO.setAddressLine1("");
			entityRequestDTO.setAddressLine2("");
			entityRequestDTO.setCity("");
			entityRequestDTO.setState("");
			entityRequestDTO.setZipCode("");
		}

		String contactNumber = "";
		if (enrollmentEntity.getPrimaryPhoneNumber() != null && enrollmentEntity.getPrimaryPhoneNumber().contains("-")) {
			contactNumber = enrollmentEntity.getPrimaryPhoneNumber().replaceAll("-", "");
		} else {
			contactNumber = enrollmentEntity.getPrimaryPhoneNumber();
		}
		entityRequestDTO.setPhoneNumber(contactNumber);
		entityRequestDTO.setEmail(enrollmentEntity.getPrimaryEmailAddress() != null ? enrollmentEntity.getPrimaryEmailAddress() : "");

		entityRequestDTO.setRegistrationStatusCd(enrollmentEntity.getRegistrationStatus());
		// Setting Registration Start Date for an Entity
		if (enrollmentEntity.getRegistrationDate() != null) {
			entityRequestDTO.setRegiStartDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
					enrollmentEntity.getRegistrationDate()));
		}
		// Setting Registration End Date for an Entity,if null then set it by
		// default to 1 year from
		// Registration End Date
		if (enrollmentEntity.getRegistrationRenewalDate() != null) {
			entityRequestDTO.setRegiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
					enrollmentEntity.getRegistrationRenewalDate()));
		}// In case Registration End Date is Null for a
			// Registered/Deregistered/Active Entity
		else {
			if (enrollmentEntity.getRegistrationStatus() != null
					&& (EntityConstants.STATUS_REGISTERED.equalsIgnoreCase(enrollmentEntity.getRegistrationStatus()
							.toString())
							|| EntityConstants.STATUS_ACTIVE.equalsIgnoreCase(enrollmentEntity.getRegistrationStatus()
									.toString()) || EntityConstants.STATUS_DEREGISTERED
								.equalsIgnoreCase(enrollmentEntity.getRegistrationStatus().toString()))) {
				Calendar date = TSCalendar.getInstance();
				if (enrollmentEntity.getRegistrationDate() != null) {
					date.setTime(enrollmentEntity.getRegistrationDate());
					date.add(Calendar.YEAR, 1);
					Date newdate = date.getTime();
					entityRequestDTO.setRegiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, newdate));
				}
			}
		}
		if (enrollmentEntity.getStatusChangeDate() != null) {
			entityRequestDTO.setStatusDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
					enrollmentEntity.getStatusChangeDate()));
		}
		// If broker is certified then, setting certification end date as
		// certification date + 1 year, else set current date as certification
		// end date
		if (enrollmentEntity.getRegistrationStatus() != null
				&& EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(enrollmentEntity.getRegistrationStatus()
						.toString())) {
			Calendar date = TSCalendar.getInstance();
			if (enrollmentEntity.getRegistrationDate() != null) {
				date.setTime(enrollmentEntity.getRegistrationDate());
				date.add(Calendar.YEAR, 1);
				Date newdate = date.getTime();
				entityRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, newdate));
			}
		} else {
			entityRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, new TSDate()));
		}

		// If payment type is EFT, setting flag to true. Else, false
		if (eePaymentMethod != null && eePaymentMethod.getPaymentType().name().equals("EFT")) {
			LOGGER.debug("Payment Method is EFT. Setting Direct Deposit Flag to True");
			entityRequestDTO.setDirectDepositFlag("True");
		} else {
			LOGGER.debug("Payment Method is not EFT. Setting Direct Deposit Flag to False");
			entityRequestDTO.setDirectDepositFlag("False");
		}

		if (eePaymentMethod != null && eePaymentMethod.getFinancialInfo() != null
				&& eePaymentMethod.getFinancialInfo().getBankInfo() != null) {

			BankInfo bankInfo = eePaymentMethod.getFinancialInfo().getBankInfo();

			entityRequestDTO.setBankRoutingNumber(bankInfo.getRoutingNumber());

			/*LOGGER.debug("Bank Account Holder Name:" + bankInfo.getNameOnAccount());*/

			entityRequestDTO.setAcctHolderName(bankInfo.getNameOnAccount());
			entityRequestDTO.setBankAcctNumber(bankInfo.getAccountNumber());

			// Checking if account type is 'C' or 'S' to set hard-coded values
			// in DTO
			String accountType = null;
			if (bankInfo.getAccountType() != null) {
				if ("C".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Checking";
				} else if ("S".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Savings";
				}
			}
			entityRequestDTO.setBankAcctType(accountType);
		}
		entityRequestDTO.setRecordIndicator("Update");
		entityRequestDTOList.add(entityRequestDTO);

		// Populate each assister associated with entity in request DTO and add
		// into request list
		populateAssisterList(entityRequestDTOList, assisterList);

		return entityRequestDTOList;
	
	}


	private void populateAssisterList(List<EntityRequestDTO> entityRequestDTOList, ArrayList<Assister> assisterList) {

		LOGGER.info("Populate Assisters associated with Entity");

		if (assisterList != null && !assisterList.isEmpty()) {
			for (Assister assister : assisterList) {
				LOGGER.debug("Create Assister request DTO");
				EntityRequestDTO entityAssisterRequestDTO = new EntityRequestDTO();

				entityAssisterRequestDTO.setRecordId(assister.getId());
				entityAssisterRequestDTO.setRecordType("Assister");
				entityAssisterRequestDTO.setBusinessLegalName(assister.getEntity().getBusinessLegalName());

				entityAssisterRequestDTO.setFirstName(assister.getFirstName());
				entityAssisterRequestDTO.setLastName(assister.getLastName());
				entityAssisterRequestDTO.setEmail(assister.getEmailAddress());
				entityAssisterRequestDTO.setEntityIdNumber(assister.getEntity() != null ? Long.valueOf(assister
						.getEntity().getId()) : null);

				entityAssisterRequestDTO.setFunctionalIdentifier(assister.getAssisterNumber());

				// Assuming Entity Mailing Location to be Primary Site Location
				if (assister.getMailingLocation() != null) {
					entityAssisterRequestDTO.setAddressLine1(assister.getMailingLocation().getAddress1());
					entityAssisterRequestDTO.setAddressLine2(assister.getMailingLocation().getAddress2());
					entityAssisterRequestDTO.setCity(assister.getMailingLocation().getCity());
					entityAssisterRequestDTO.setState(assister.getMailingLocation().getState());
					entityAssisterRequestDTO.setZipCode(String.valueOf(assister.getMailingLocation().getZip()));
				} else {
					entityAssisterRequestDTO.setAddressLine1("");
					entityAssisterRequestDTO.setAddressLine2("");
					entityAssisterRequestDTO.setCity("");
					entityAssisterRequestDTO.setState("");
					entityAssisterRequestDTO.setZipCode("");
				}

				String contactNumber = "";
				if (assister.getPrimaryPhoneNumber() != null && assister.getPrimaryPhoneNumber().contains("-")) {
					contactNumber = assister.getPrimaryPhoneNumber().replaceAll("-", "");
				} else {
					contactNumber = assister.getPrimaryPhoneNumber();
				}
				entityAssisterRequestDTO.setPhoneNumber(contactNumber);

				entityAssisterRequestDTO.setCertiStatusCode(assister.getCertificationStatus());
				

				if (assister.getCertificationStatus() != null
						&& EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(assister.getCertificationStatus()
								.toString())) {
					entityAssisterRequestDTO.setCertiStartDate(EntityUtils.formatDate(
							EntityConstants.DATE_FORMAT_PLAIN, assister.getCertificationDate()));
					entityAssisterRequestDTO.setCertificationNumber(assister.getCertificationNumber());
				} else {
					entityAssisterRequestDTO.setCertiStartDate(null);
				}

				if (assister.getStatusChangeDate() != null) {
					entityAssisterRequestDTO.setStatusDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							assister.getStatusChangeDate()));
				} else {
					entityAssisterRequestDTO.setStatusDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							new TSDate()));
				}

				if (assister.getCertificationStatus() != null && EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(assister.getCertificationStatus().toString())) {
						Calendar date = TSCalendar.getInstance();
						date.setTime(assister.getCertificationDate());
						date.add(Calendar.YEAR, 1);
						Date newdate = date.getTime();
					entityAssisterRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							newdate));
					entityAssisterRequestDTO.setCertiRenewalDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, assister.getReCertificationDate()));
				} else {
					entityAssisterRequestDTO.setCertiEndDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN,
							new TSDate()));
					entityAssisterRequestDTO.setCertiRenewalDate(EntityUtils.formatDate(EntityConstants.DATE_FORMAT_PLAIN, new TSDate()));
				}
				
				// TODO - Hard-coded it to "False" until available in Payment
				// Information
				entityAssisterRequestDTO.setDirectDepositFlag("False");

				if (assister.getCertificationStatus() != null
						&& EntityConstants.STATUS_PENDING.equalsIgnoreCase(assister.getCertificationStatus())) {
					entityAssisterRequestDTO.setRecordIndicator("New");
				} else {
					entityAssisterRequestDTO.setRecordIndicator("Update");
				}

				LOGGER.debug("Add assister into request DTO");
				entityRequestDTOList.add(entityAssisterRequestDTO);
			}
		}
	
		
	}

}
