<script type="text/ng-template" id="modaltemplatetiny-saving.jsp">
  <div class='ng-modal' ng-show='show'>
    <div class='ng-modal-overlay'></div>
    <div class='ng-modal-dialog' ng-style='dialogStyle' style="height:200px">
      <div class='modal-header'>
        <h3><spring:message code="label.lce.saveApp"/></h3>
      </div>
      <div class='modal-body' ng-transclude></div>
    </div>
  </div>
</script>