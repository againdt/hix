<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<!--<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>-->
<%@ page isELIgnored="false"%>

<div class="gutter10">
	<div class="row-fluid">
				<h1>View Invoices <a href="#" rel="tooltip" data-placement="top"
				data-original-title="You can view all your active invoices here. 
				Click on link or the gear button to view invoice details showing your aggregated 
				premiums, and reissue invoice." 
				class="info"><i class="icon-question-sign customMargin"></i></a></h1>
	</div>


<div class="row-fluid">
	<div class="span3" id="sidebar">
			<div class="header">
				<h4 class="margin0">Refine Results</h4>
				</div>
				<div class="graybg">
					<form class="form-vertical gutter10" id="frminvoiceinfolist" name="frminvoiceinfolist" action="activeemployerinvoices" method="POST">
					<df:csrfToken/>
						<div class="control-group">
							<label>Start Date</label>
							<div class="input-append date date-picker" id="date">
								<input class="input-medium" type="text" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" pattern="mm-dd-yyyy" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<!-- <div class="controls">
								<input type="text" class="input-medium" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" onChange="setdate();" />
							</div> -->
						</div>
						
						<div class="control-group">
							<label>End Date</label>
							<div class="input-append date date-picker" id="date">
								<input class="input-medium" type="text" name="enddate" id="myenddate" value="${searchCriteria.enddate}" pattern="mm-dd-yyyy" onChange="setdate();"/> 
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
							<!--<div class="controls">
								<input type="text" title="MM/dd/yyyy" class="datepick input-medium" name="enddate" id="myenddate" value="${searchCriteria.enddate}"  onChange="setdate(); " />
							</div>-->
						</div>
                       	<div id="myenddate_error"></div>
                       	
                       	<div class="txt-center">
							<input type="submit" name="submitBtn" id="submitBtn" Value="Go" title="Go" class="btn input-medium">
						</div>
					</form>

				</div>
	</div>


	<div class="span9" id="rightpanel">
    <!-- <h4 class="graydrkbg gutter10"></h4> -->

		<form id="frminvoicelist"
			name="frminvoicelist" action="employerinvoice" method="POST">
			<df:csrfToken/>
			<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="id" id="id" />
			<input type="hidden" value="" name="startdate" id="startdate" />
		    <input type="hidden" value="" name="enddate" id="enddate" />
		    <input type="hidden" value="" name="employerId" id="employerId" />
		    <input type="hidden" value="" name="reissueinvoiceid" id="reissueinvoiceid" />
			<c:choose>
				<c:when test="${resultSize >= 1}">
					<c:choose>
						<c:when test="${fn:length(invoicelist) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="graydrkbg ">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
											<th class="sortable" scope="col"><dl:sort
													title="Invoice" sortBy="invoiceNumber"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort
													title="Employer Name" sortBy="employer.name"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Premium Amount" sortBy="premiumThisPeriod"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Due on"
													sortBy="paymentDueDate"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Status" sortBy="paidStatus"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Due Amount" sortBy="totalAmountDue"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th></th>
										</tr>
									</thead>
									
									<c:forEach items="${invoicelist}" var="invoice" varStatus="vs">
									<tr>
											<!-- <td><input type="checkbox"> </td> -->
											<td style ="white-space:nowrap"><a href="#" onClick="showdetail('${invoice.ecmDocId}');" id="edit_${vs.index}">${invoice.invoiceNumber}</a></td>
											<td>${invoice.employername}</td>
											<td><fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${invoice.premiumThisPeriod}" type="currency"/></td>
											
											<td><fmt:formatDate
													value="${invoice.paymentDueDate}"
													pattern="MM-dd-yyyy" /></td>
											<%-- <td>${invoice.paidStatus}</td> --%>
											<td> 						
											<c:choose>
											<c:when test="${invoice.paidStatus =='PARTIALLY_PAID'}">PARTIALLY PAID
											</c:when>
											<c:otherwise>${invoice.paidStatus}</c:otherwise></c:choose></td>
											<%-- <td>$ ${invoice.totalAmountDue}</td> --%>
											<td>
											<fmt:setLocale value="en_US" scope="session"/>
											<fmt:formatNumber value="${invoice.totalAmountDue - invoice.totalPaymentReceived}" type="currency"/></td>
											<td><div class="controls">
													<div class="dropdown">
														<a href="/page.html" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"> <i
															class="icon-cog"></i> <b class="caret"></b>
														</a>
														<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right">
															<li><a href="#" onClick="showdetail('${invoice.ecmDocId}');" id="edit_${vs.index}">View Detail</a></li>
														<%-- 	<c:if test="${'DUE' == invoice.paidStatus && 'Y' == invoice.isActive && 'N' == invoice.issuerInvoiceGenerated}"> --%>
															<c:if test="${'DUE' == invoice.paidStatus && 'Y' == invoice.isActive}">
																<c:set var = "tempInvoiceID"><encryptor:enc value = "${invoice.id}"/></c:set>
																<c:set var = "tempInvEmployerID"><encryptor:enc value = "${invoice.employerid}"/></c:set>
															  <li><a href="#" onClick="reissueinvoice('${tempInvoiceID}', '${tempInvEmployerID}');" id="edit_${vs.index}">Reissue Invoice</a></li>
															</c:if>
														</ul>
													</div>
												</div></td>
										</tr>				
									</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
					</c:choose>
				</c:when>
				<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
		</form>
	</div>
</div>
</div>


<div class="row-fluid">
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">

				<p>The prototype showcases three scenarios (A, B and C)
					dependant on a particular Employer's eligibility to use the SHOP
					Exchange.</p>
			</div>
		</div>
	</div>

</div>
<!-- row-fluid -->


<script type="text/javascript">
$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});

$('#myenddate, #mystartdate').keypress(function(e){
  	if (window.event) { var charCode = window.event.keyCode; }
    else if (e) { var charCode = e.which; }
    else { return true; }
    if (charCode > 31 && (charCode > 48 || charCode < 57)) { return false; }
    return true;
  
});
		/* $(function() {
			$('.datepick').each(function() {
				var ctx = "${pageContext.request.contextPath}";
				var imgpath = ctx+'/resources/images/calendar.gif';
				$(this).datepicker({
					showOn : "button",
					buttonImage : imgpath,
					buttonImageOnly : true
				});
			});
		}); */


/* $.validator.addMethod("endDate", function(value, element) {
	var startdatevalue = $('#mystartdate').val();
	 if (value == null || value.length <1)
		return true;
	return Date.parse(startdatevalue) < Date.parse(value); 
}); */

jQuery.validator.addMethod("endDate", function(value, element,param) {
	var startdatevalue = $('#mystartdate').val(),
		regExp = (/(\d{1,2})\-(\d{1,2})\-(\d{2,4})/);
	
	//console.log('startdatevalue',startdatevalue)
	if (value == null || value.length <1)
		return true;
	//console.log(parseInt(value.replace(regExp, "$3$2$1")) > parseInt(startdatevalue.replace(regExp, "$3$2$1")))
	return (parseInt(value.replace(regExp, "$3$1$2")) > parseInt(startdatevalue.replace(regExp, "$3$1$2")));
});
	
function setdate(){
	$("#startdate").val($("#mystartdate").val());
	$("#enddate").val($("#myenddate").val());
}

function setFilterDate()
{
	alert("onload");
	$("#startdate").val($("#searchCriteria.startdate").val());
	$("#enddate").val($("#searchCriteria.enddate").val());
}

function showdetail(docid) {
	if(docid==null || $.trim(docid).length==0 ||docid==""||docid==" "){
		alert("Invoice report is not yet generated");
	}else{
		var  contextPath =  "<%=request.getContextPath()%>";
	    var documentUrl = contextPath + "/admin/account/viewinvoicedtl?documentId="+docid;

	    window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
	}
}
	
function reissueinvoice(invoiceId, employerId) {
	$("#reissueinvoiceid").val(invoiceId);
	$("#employerId").val(employerId);
	
	$("#frminvoicelist").attr("action", "reissueinvoice");
	$("#frminvoicelist").submit();
}
	
var validator = $("#frminvoiceinfolist").validate({ 
		ignore: "",
		rules : {
			enddate : { endDate : true}
		}, 
		messages : {
			enddate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"}
		},
		highlight: function (element, errorClass) {
			$('#myenddate').removeAttr('error');
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class', 'error');
		} ,
		
});

$('.info').tooltip()
</script>
