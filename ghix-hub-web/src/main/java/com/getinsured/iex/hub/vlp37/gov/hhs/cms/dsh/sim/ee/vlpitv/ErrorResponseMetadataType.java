//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.02.06 at 01:57:25 PM IST 
//


package com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpitv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ErrorResponseMetadataType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ErrorResponseMetadataType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlpitv.ee.sim.dsh.cms.hhs.gov}ErrorResponseCode"/>
 *         &lt;element ref="{http://vlpitv.ee.sim.dsh.cms.hhs.gov}ErrorResponseDescriptionText"/>
 *         &lt;element ref="{http://vlpitv.ee.sim.dsh.cms.hhs.gov}ErrorTDSResponseDescriptionText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorResponseMetadataType", propOrder = {
    "errorResponseCode",
    "errorResponseDescriptionText",
    "errorTDSResponseDescriptionText"
})
public class ErrorResponseMetadataType {

    @XmlElement(name = "ErrorResponseCode", required = true)
    protected String errorResponseCode;
    @XmlElement(name = "ErrorResponseDescriptionText", required = true)
    protected String errorResponseDescriptionText;
    @XmlElement(name = "ErrorTDSResponseDescriptionText")
    protected String errorTDSResponseDescriptionText;

    /**
     * Gets the value of the errorResponseCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorResponseCode() {
        return errorResponseCode;
    }

    /**
     * Sets the value of the errorResponseCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorResponseCode(String value) {
        this.errorResponseCode = value;
    }

    /**
     * Gets the value of the errorResponseDescriptionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorResponseDescriptionText() {
        return errorResponseDescriptionText;
    }

    /**
     * Sets the value of the errorResponseDescriptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorResponseDescriptionText(String value) {
        this.errorResponseDescriptionText = value;
    }

    /**
     * Gets the value of the errorTDSResponseDescriptionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorTDSResponseDescriptionText() {
        return errorTDSResponseDescriptionText;
    }

    /**
     * Sets the value of the errorTDSResponseDescriptionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorTDSResponseDescriptionText(String value) {
        this.errorTDSResponseDescriptionText = value;
    }

}
