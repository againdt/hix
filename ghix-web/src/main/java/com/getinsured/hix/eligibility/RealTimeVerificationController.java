package com.getinsured.hix.eligibility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.eligibility.EventVerificationDetailsDTO;
import com.getinsured.hix.dto.eligibility.EventVerificationDetailsListDTO;
import com.getinsured.hix.dto.eligibility.IssuerDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Controller
@RequestMapping("/realtime")
public class RealTimeVerificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RealTimeVerificationController.class);

	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private UserService userService;
	@Autowired
	private LookupService lookupService;

	private static final String FAILURE = "failure";
	private static final String HMS_LOOKUP_TYPE = "HMS_ISSUERS";

	@RequestMapping(value = "/event/verification/details", method = RequestMethod.POST,produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> realTimeVerification(@RequestBody EventVerificationDetailsDTO request) {
    	String response = null;
           final Map<String, String> responseObject = new HashMap<>(1);

    	try {
    		if (request == null || request.getAction() == null || StringUtils.isEmpty(request.getId())) {
    			responseObject.put("validationStatus", FAILURE);
    			return ResponseEntity.ok(responseObject);
    		}
               else
               {
                   final AccountUser user = userService.getLoggedInUser();
                   request.setUserId(user.getId());
                   response = ghixRestTemplate.postForObject(GhixEndPoints.ELIGIBILITY_URL+"/realtime/event/verification/details",request,String.class);
    	    }
    	} catch (Exception exception) {
    		throw new GIRuntimeException("Exception occurred while saving event verification details - ",exception);
    	}

    	responseObject.put("validationStatus", response);
    	return ResponseEntity.ok(responseObject);
    }

	
	@ResponseBody
	@RequestMapping(value={"/event/verification/details/{encCaseNumber}"},method=RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	public EventVerificationDetailsListDTO getEventVerificationDetails(@PathVariable("encCaseNumber") String  encCaseNumber){
		EventVerificationDetailsListDTO eventVerificationDetailsListDTO = null;
		try {
			eventVerificationDetailsListDTO = ghixRestTemplate.getForObject(GhixEndPoints.ELIGIBILITY_URL+"/realtime/event/verification/details/"+encCaseNumber, EventVerificationDetailsListDTO.class);
			AccountUser user = userService.getLoggedInUser();
			if(eventVerificationDetailsListDTO!=null && checkAdminUserRole(user)){
				eventVerificationDetailsListDTO.setShowOverride(true); // ***STOP*** using Strings where it should be boolean!!!
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while fetching event verification details - ",ex);
			throw new GIRuntimeException("Exception occurred while fetching event verification details - ",ex);
		}
		
		return eventVerificationDetailsListDTO;
	}
	
	@RequestMapping(value = "/event/verification/loadIssuers", method = RequestMethod.GET,produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<IssuerDTO> loadIssuers(Model model,HttpServletRequest request) throws GIRuntimeException {
		IssuerDTO issuerDto = null;
		List<IssuerDTO> listOfIssuers = new ArrayList<>();
		try {
			
			List<LookupValue> values = lookupService.getLookupValueList(HMS_LOOKUP_TYPE);
			
			for(LookupValue value:values){
				issuerDto = new IssuerDTO();
				issuerDto.setIssuerCode(value.getLookupValueCode());
				issuerDto.setIssuerName(value.getLookupValueLabel());
				
				listOfIssuers.add(issuerDto);
			}
		} catch(Exception exception){
			LOGGER.error("Exception occurred while loading issuers : ", exception);
			throw new GIRuntimeException("Exception occurred while loading issuers - ",exception);
		}
		return listOfIssuers;

	}
	
	private boolean checkAdminUserRole(AccountUser user) {
		Set<String> adminRoles = new HashSet<>();
		adminRoles.add("L0_READONLY");
		adminRoles.add("SUPERVISOR");
		adminRoles.add("L1_CSR");
		adminRoles.add("L2_CSR");
		adminRoles.add("CSR");
		
		if(user!=null){
			Set<UserRole> userRoles = user.getUserRole();
			for (UserRole userRole : userRoles) {
				if(adminRoles.contains(userRole.getRole().getName())){
					return true;
				}
			}
		}
		return false;
	}
	
}
