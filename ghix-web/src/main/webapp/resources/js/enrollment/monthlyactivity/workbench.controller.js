(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .controller('workbenchContrl', function($scope,
                                                $timeout,
                                                $state,
                                                $log,
                                                workbenchService,
                                                NotifyingService,
                                                issuerNotifyingService,
                                                dataStorageService) {
            $scope.activity = {};
            $scope.selectedIssuer = null;
            $scope.selectedMonth = null;
            $scope.selected_recon_month = '';
            $scope.selected_recon_year = '';
            $scope.countUp = 25;

            $scope.initalize = function () {

                workbenchService.setData($scope.monthlyReconData);

                $scope.issuers = [];
                $scope.issuers.push({"issuerId": "000000", "issuerName": "All", "hiosIssuerId": "-1"});
                for (var i = 0; i < $scope.carrierDettails.enrlReconMonthlyActivityDTO.issuerDetailsList.length; i++) {
                    $scope.issuers.push($scope.carrierDettails.enrlReconMonthlyActivityDTO.issuerDetailsList[i]);
                }

                // $scope.selectedIssuer = $scope.issuers[0];

                $scope.months = [];
                for (var j = 1; j <= 12; j++) {
                    if($scope.carrierDettails.enrlReconMonthlyActivityDTO.monthDetailsMap[j]) {
                        $scope.months.push({
                            "monthNum": j,
                            "monthName": $scope.carrierDettails.enrlReconMonthlyActivityDTO.monthDetailsMap[j]
                        });
                    }
                }

                $scope.years = [];
                for (var k = 0; k < $scope.carrierDettails.enrlReconMonthlyActivityDTO.yearList.length; k++) {
                    $scope.years.push($scope.carrierDettails.enrlReconMonthlyActivityDTO.yearList[k]);
                }

                // $scope.years = $scope.activity.enrlReconMonthlyActivityDTO.yearList;
                if($scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList)
                    $scope.highLevelSummary = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList[0];

                var topDiscrepancyList =  $scope.monthlyReconData.enrlReconMonthlyActivityDTO.topDiscrepancyList;
                if(topDiscrepancyList && topDiscrepancyList.length > 5){
                    $scope.topLevelDiscrepancyList = topDiscrepancyList.slice(0,5);
                }
                else {
                    $scope.topLevelDiscrepancyList = topDiscrepancyList;
                }

                $scope.initializeEnrollemntDetails();
                $scope.initialize12MonthTrend();
            };

            $scope.initializeEnrollemntDetails = function () {
                $scope.enrollmentDetails = [];

                if($scope.highLevelSummary){



                    $scope.enrollmentDetails.push({
                        "percentage" : $scope.highLevelSummary.totalSuccessPercent ? Math.round($scope.highLevelSummary.totalSuccessPercent) : 0,
                        "field": "Policies Fully Matched",
                        "value": $scope.highLevelSummary.totalSuccess,
                        "total": $scope.highLevelSummary.totalEnrlInHix,
                        "color": d3.schemeSet2[0]});

                    $scope.enrollmentDetails.push({
                        "percentage" : $scope.highLevelSummary.totalMissingInFilePercent ? Math.round($scope.highLevelSummary.totalMissingInFilePercent) : 0,
                        "field": "Policies missing in File",
                        "value": $scope.highLevelSummary.totalMissingInFile,
                        "total": $scope.highLevelSummary.totalEnrlInHix,
                        "color": d3.schemeSet2[1]});

//                    $scope.enrollmentDetails.push({
//                        "percentage" : $scope.highLevelSummary.totalMissingInHixPercent ? Math.round($scope.highLevelSummary.totalMissingInHixPercent) : 0,
//                        "field": "Policies missing in HIX",
//                        "value": $scope.highLevelSummary.totalMissingInHix,
//                        "total": $scope.highLevelSummary.totalEnrlInHix,
//                        "color": d3.schemeSet2[2]});

                    $scope.enrollmentDetails.push({
                        "percentage" : $scope.highLevelSummary.totalFailurePercent ? Math.round($scope.highLevelSummary.totalFailurePercent) : 0,
                        "field": "Policies with Error",
                        "value": $scope.highLevelSummary.totalFailure,
                        "total": $scope.highLevelSummary.totalEnrlInHix,
                        "color": d3.schemeSet2[2]});

                    $scope.enrollmentDetails.push({
                        "percentage" : $scope.highLevelSummary.totalEnrlDiscrepancyPercent ? Math.round($scope.highLevelSummary.totalEnrlDiscrepancyPercent) : 0,
                        "field": "Policies with Discrepancies",
                        "value": $scope.highLevelSummary.totalEnrlDiscrepancyCount,
                        "total": $scope.highLevelSummary.totalEnrlInHix,
                        "color": d3.schemeSet2[3]});
                }
            };

            $scope.isPieChartVisible = function (){
                return $scope.highLevelSummary;
            };

            $scope.isTopDiscrepancyChartVisible = function (){
                return $scope.topLevelDiscrepancyList && $scope.topLevelDiscrepancyList.length > 0;
            };

            $scope.isRollingTrendsChartVisible = function () {
                return $scope.rollingTrends && $scope.rollingTrends.length > 0;
            };

            $scope.initialize12MonthTrend = function () {
                $scope.lastTwelveMonthSummaryList = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.lastTwelveMonthSummaryList;
                $scope.rollingTrends = [];
                if($scope.lastTwelveMonthSummaryList) {
                    for (var i = 0; i < $scope.lastTwelveMonthSummaryList.length; i++) {
                        var date = $scope.lastTwelveMonthSummaryList[i].month + "/" + $scope.lastTwelveMonthSummaryList[i].year;
                        $scope.rollingTrends.push(
                            {
                                "totalSuccessPercent": $scope.lastTwelveMonthSummaryList[i].totalSuccessPercent? $scope.lastTwelveMonthSummaryList[i].totalSuccessPercent : 0,
                                "totalSuccess": $scope.lastTwelveMonthSummaryList[i].totalSuccess? $scope.lastTwelveMonthSummaryList[i].totalSuccess : 0,
                                "totalMissingInFilePercent": $scope.lastTwelveMonthSummaryList[i].totalMissingInFilePercent ? $scope.lastTwelveMonthSummaryList[i].totalMissingInFilePercent : 0,
                                "totalMissingInFile": $scope.lastTwelveMonthSummaryList[i].totalMissingInFile ? $scope.lastTwelveMonthSummaryList[i].totalMissingInFile : 0,
                                "totalMissingInHixPercent": $scope.lastTwelveMonthSummaryList[i].totalMissingInHixPercent ? $scope.lastTwelveMonthSummaryList[i].totalMissingInHixPercent : 0,
                                "totalMissingInHix": $scope.lastTwelveMonthSummaryList[i].totalMissingInHix ? $scope.lastTwelveMonthSummaryList[i].totalMissingInHix : 0,
                                "totalFailurePercent": $scope.lastTwelveMonthSummaryList[i].totalFailurePercent ? $scope.lastTwelveMonthSummaryList[i].totalFailurePercent : 0,
                                "totalFailure": $scope.lastTwelveMonthSummaryList[i].totalFailure ? $scope.lastTwelveMonthSummaryList[i].totalFailure : 0,
                                "totalEnrlDiscrepancyCountPercent": $scope.lastTwelveMonthSummaryList[i].totalEnrlDiscrepancyPercent ? $scope.lastTwelveMonthSummaryList[i].totalEnrlDiscrepancyPercent : 0,
                                "totalEnrlDiscrepancyCount": $scope.lastTwelveMonthSummaryList[i].totalEnrlDiscrepancyCount ? $scope.lastTwelveMonthSummaryList[i].totalEnrlDiscrepancyCount : 0,
                                "totalEnrlCntInFilePercent": $scope.lastTwelveMonthSummaryList[i].totalEnrlCntInFilePercent ? $scope.lastTwelveMonthSummaryList[i].totalEnrlCntInFilePercent : 0,
                                "totalEnrlCntInFile": $scope.lastTwelveMonthSummaryList[i].totalEnrlCntInFile ? $scope.lastTwelveMonthSummaryList[i].totalEnrlCntInFile : 0,
                                "date": date
                            });
                    }

                    // $scope.fillRollingTrends();
                }
            };

            issuerNotifyingService.subscribe($scope, function issuerChanged(event, issuerData) {

                var scope = angular.element($("#isserSelect")).scope();
                // scope.$apply(function(){
                //     scope.selectValue = issuerData.issuerName;
                // });

                $("#isserSelect").val(issuerData.hiosIssuerId).change();
                $scope.updateMonthlyReconData(true);
            });

            $scope.fillRollingTrends = function () {
                var previousMonth = $scope.lastTwelveMonthSummaryList[$scope.lastTwelveMonthSummaryList.length - 1].month;
                var previousYear =  $scope.lastTwelveMonthSummaryList[$scope.lastTwelveMonthSummaryList.length - 1].year;

                for (var k = 0; k < 12 - $scope.lastTwelveMonthSummaryList.length; k++) {
                    if(previousMonth === 12){
                        previousMonth = 1;
                        previousYear++;
                    }
                    else {
                        previousMonth ++;
                    }

                    var date = previousMonth + "/" + previousYear;
                    $scope.rollingTrends.push(
                        {
                            "totalSuccessPercent": 0,
                            "totalSuccess": 0,
                            "totalMissingInFilePercent": 0,
                            "totalMissingInFile": 0,
                            "totalMissingInHixPercent": 0,
                            "totalMissingInHix": 0,
                            "totalFailurePercent": 0,
                            "totalFailure": 0,
                            "totalEnrlDiscrepancyCountPercent": 0,
                            "totalEnrlDiscrepancyCount": 0,
                            "totalEnrlCntInFilePercent": 0,
                            "totalEnrlCntInFile": 0,
                            "date": date
                        });
                }
            };

            $scope.isAllIssuers = function () {
                var issuerId = $("#isserSelect :selected").val();
                return issuerId === "-1";
            };

            $scope.updateMonthlyReconData = function (refresh) {
                var issuerId = $("#isserSelect :selected").val();
                var year = $("#yearSelect :selected").val();
                var month = $("#monthSelect :selected").val();

                var requestJSON = {
                    "monthNum" : month,
                    "year": year
                };

                if(issuerId !== "-1"){
                    requestJSON.hiosIssuerId =  issuerId;
                }


                workbenchService.getIssuerActivity(requestJSON).then(
                    function(monthlyActivity) {
                        // $timeout(function() {
                        $scope.monthlyReconData = monthlyActivity;
                        if($scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList) {
                            $scope.highLevelSummary = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList[0];

                            $scope.monthlyReconData.selectedMonth =  $scope.months[$scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList[0].month - 1];
                            if($scope.monthlyReconData.selectedMonth)
                                $scope.selectedMonth = $scope.monthlyReconData.selectedMonth.monthName;

                            $scope.monthlyReconData.selectedYear = $scope.selectedYear = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.highLevelSummaryList[0].year;
                        }
                        else {
                            for(var m = 0 ; m < $scope.issuers.length; m++){
                                if($scope.issuers[m].hiosIssuerId === issuerId){
                                    $scope.monthlyReconData.selectedIssuer = $scope.issuers[m];
                                    break;
                                }
                            }
                            $scope.monthlyReconData.selectedMonth =  $scope.months[month - 1];
                            $scope.monthlyReconData.selectedYear = year;
                        }

                        if(issuerId !== "-1" && $scope.monthlyReconData.enrlReconMonthlyActivityDTO.issuerLevelSummaryList) {
                            $scope.monthlyReconData.selectedIssuer = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.issuerLevelSummaryList[0];
                            if ($scope.monthlyReconData.selectedIssuer)
                                $scope.selectedIssuer = $scope.monthlyReconData.selectedIssuer.issuerName;
                        }

                        // $scope.initialize12MonthTrend();
                        // $scope.topLevelDiscrepancyList = $scope.monthlyReconData.enrlReconMonthlyActivityDTO.topDiscrepancyList;

                        NotifyingService.notify();

                        dataStorageService.setItem("monthlyReconData", $scope.monthlyReconData);

                        if(refresh && refresh === true){
                            $state.reload();
                        }
                        // $state.reload();

                        // }, 0);
                    },
                    /* error function */
                    function(result) {
                        //console.log("Failed to get get Issuer Activity");
                    });

            };


            (function () {
                $scope.populateCarrierNMonthJson = $('#populateCarrierNMonthJson').val();
                $scope.monthlyReconDataJson = $('#monthlyReconDataJson').val();
                $scope.carrierDettails = JSON.parse($scope.populateCarrierNMonthJson );

                $scope.monthlyReconData = dataStorageService.getItem("monthlyReconData");

                if($scope.monthlyReconData) {
                    $scope.initalize();
                    $timeout(function() {

                        if ($scope.monthlyReconData.selectedIssuer) {
                            $scope.selectedIssuer = $scope.monthlyReconData.selectedIssuer.issuerName;
                            $("#isserSelect").val($scope.monthlyReconData.selectedIssuer.hiosIssuerId).change();
                        }

                        if ($scope.monthlyReconData.selectedMonth) {
                            $scope.selectedMonth = $scope.monthlyReconData.selectedMonth.monthName;
                            $("#monthSelect").val($scope.monthlyReconData.selectedMonth.monthNum).change();
                        }

                        if ($scope.monthlyReconData.selectedYear) {
                            $scope.selectedYear = $scope.monthlyReconData.selectedYear;
                            $("#yearSelect").val($scope.monthlyReconData.selectedYear).change();
                        }

                        $scope.updateMonthlyReconData();
                    }, 0);
                }
                else {
                    $scope.monthlyReconData = JSON.parse($scope.monthlyReconDataJson );
                    $scope.initalize();

                    $timeout(function() {

                        $scope.selectedMonth = moment().format("MM")
                        $("#monthSelect").val(Number.parseInt($scope.selectedMonth)).change();
                        $scope.selectedYear = moment().format("YYYY");
                        $("#yearSelect").val($scope.selectedYear).change();
                        $("#isserSelect").val($scope.monthlyReconData.selectedIssuer).change();
                        $scope.updateMonthlyReconData();
                    }, 0);
                }


            })();
        });
})();
