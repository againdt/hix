package com.getinsured.hix.batch.enrollment.service;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.service.EnrollmentCreationService;
import com.getinsured.hix.enrollment.service.EnrollmentService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

@Service("resendInd20Service")
@Transactional
public class ResendInd20ServiceImpl implements ResendInd20Service {

	private static final Logger LOGGER = Logger.getLogger(ResendInd20ServiceImpl.class);

	@Autowired 
	private RestTemplate restTemplate;

	@Autowired 
	private EnrollmentService enrollmentService;
	
	@Autowired EnrollmentCreationService enrollmentCreationService;

	/*@Autowired
	private EnrolleeService enrolleeService;*/
	
	@Autowired
	private IEnrollmentRepository iEnrollmentRepository;
	
	@Autowired
	private JobService jobService;

	@Value("#{configProp['enrollment.resendIND20FilePath']}")
	private String resendIND20File;

	public String getResendIND20File() {
		return resendIND20File;
	}

	public void setResendIND20File(String resendIND20File) {
		this.resendIND20File = resendIND20File;
	}

	public EnrollmentService getEnrollmentService() {
		return enrollmentService;
	}

	public void setEnrollmentService(EnrollmentService enrollmentService) {
		this.enrollmentService = enrollmentService;
	}

	public RestTemplate getRestTemplate() {
		return restTemplate;
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}



	@Override
	public void sendIndvPSDetails(long jobExecutionId) throws Exception {
		List<String> enrollmentIdString;
		try {
			enrollmentIdString = readCSVFileToGetEnrollments();
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			LOGGER.error("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20 Failed" + getResendIND20File());
			return;
		}

		LOGGER.debug("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20 :: Enrollment IDs in file :: "+enrollmentIdString);
		
		if(enrollmentIdString != null){
			for(String enrollmentIdStr : enrollmentIdString){
				
				Integer enrollmentId;
				String batchJobStatus=null;
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}
				
				try{
					enrollmentId = Integer.parseInt(enrollmentIdStr);
					Enrollment enrollment = iEnrollmentRepository.getByEnrollmentId(enrollmentId);
					LOGGER.debug("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20  Processing Enrollment Id :: "+enrollmentId);
					// Don't send aborted enrollments
					if(enrollment != null && enrollment.getEnrollmentStatusLkp() != null && !enrollment.getEnrollmentStatusLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_ABORTED)){
						List<Enrollment> enrollmentList= new ArrayList<Enrollment>();
						enrollmentList.add(enrollment);
						//Map<String,Object> orderMap = getIndvPSDetails(enrollment);
						Character enroll_type = 'I';
						Map<String,Object> orderMap = enrollmentCreationService.getIndvPSDetails(null, null,enrollmentList, null, enroll_type, null, null);
						final String postResp = getRestTemplate().postForObject(AHBXEndPoints.ENROLLMENT_IND20_CALL_URL, orderMap,String.class);
						XStream xStream = GhixUtils.getXStreamStaxObject();
						EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xStream.fromXML(postResp);
						if(enrollmentResponse != null && enrollmentResponse.getStatus() != null){
							
							LOGGER.debug("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20 Successful for Enrollment Id :: "+enrollmentId);
							if(enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)){
								enrollmentResponse.setErrMsg(EnrollmentConstants.MSG_ENROLLMENT_PROCESS_SUCCESS);
								enrollmentResponse.setModuleStatusCode(EnrollmentConstants.MODULE_STATUS_CODE_E000);
							}
							else{
								LOGGER.error("Problem in IND20 call :  " + enrollmentResponse.getErrMsg());
							}
						}

					}
					else{
						LOGGER.debug("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20  Enrollment ID :- " + enrollmentIdStr +" :: either does not exist or is in aborted state");
					}
				}
				catch(NumberFormatException numberFormatException){
					LOGGER.error("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20  Failed for Enrollment Id :: " + enrollmentIdStr+" :: Invalid Enrollment ID");
				}
				catch (Exception e) {
					LOGGER.error("ResendInd20ServiceImpl :: sendIndvPSDetails()  RESEND IND20  Failed for Enrollment Id :: " + enrollmentIdStr + " :: "+e.getMessage());
				}
			}
		}
	}

	private List<String> readCSVFileToGetEnrollments() throws GIException{
		List<String>  enrollmentIdList = new ArrayList<String>();
		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";

		try {
			br = new BufferedReader(new FileReader(getResendIND20File()));
			while ((line = br.readLine()) != null) {
				List<String> tempStringList = null;

				tempStringList = (Arrays.asList(line.split(cvsSplitBy)));
				enrollmentIdList.addAll(tempStringList);
			}

		} catch (Exception e) {
			throw new GIException("ResendInd20ServiceImpl :: readCSVFileToGetEnrollments() :: RESEND IND20 Failed"+e.getMessage(),e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage());
				}
			}
		}
		return enrollmentIdList;
	}


}
