/**
 * 
 */
package com.getinsured.eligibility.phix.service;

import java.util.List;

import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;

/**
 * This is the service interface for PHIX
 * @author Nikhil Talreja
 * @since 28 August, 2013
 *
 */
public interface PhixService {
	
	EligLead getEligLeadRecord(long id);
	
	EligLead saveEligLeadRecord(EligLead record);
	
	StateChipMedicaid getStateChipMedicaidRecordByState(String stateCode);
	
	List<EligLead> fetchNewRecordsForOutBoundCall(Long batchStage, String api);

}
