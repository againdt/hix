package com.getinsured.hix.iex.ssap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.iex.ssap.model.AptcHistory;

public interface AptcHistoryRepository  extends JpaRepository<AptcHistory, Long> {
	
	
}
