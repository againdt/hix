package com.getinsured.hix.indportal.dto;

public class OverrideComment {

	private String caseNumber;
	private String overrideComment;
	private String commenterName;
	private String commentedDate;
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getOverrideComment() {
		return overrideComment;
	}

	public void setOverrideComment(String overrideComment) {
		this.overrideComment = overrideComment;
	}

	public String getCommenterName() {
		return commenterName;
	}

	public void setCommenterName(String commenterName) {
		this.commenterName = commenterName;
	}

	public String getCommentedDate() {
		return commentedDate;
	}

	public void setCommentedDate(String commentedDate) {
		this.commentedDate = commentedDate;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OverrideComment [caseNumber=");
		builder.append(caseNumber);
		builder.append(", overrideComment=");
		builder.append(overrideComment);
		builder.append(", commenterName=");
		builder.append(commenterName);
		builder.append(", commentedDate=");
		builder.append(commentedDate);
		builder.append("]");
		return builder.toString();
	}
}
