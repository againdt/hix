package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;



@Component("enrollment1095XmlIRSOutProcessor")
public class Enrollment1095XmlIRSOutProcessor implements ItemProcessor< Integer, Integer> {

	private int applicableYear;
	//	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	//	private SkippedRecords skippedRecords;

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlIRSOutProcessor.class);


	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){

		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			applicableYear=ec.getInt("year",0);
		}

	}
	@Override
	public Integer process(Integer enrollment1095Id) throws Exception {
		LOGGER.info("Process :: " + enrollment1095Id);
		return enrollment1095Id;
	}
}