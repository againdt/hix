<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
 <c:set var="encPlanId" ><encryptor:enc value="${planId}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<h1><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/> ${plan.name} </h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.planAbout'/></h4>
       		</div>  
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/issuer/qhp/plandetails/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/viewbenefits/${encPlanId}"/>"><spring:message  code='label.planBenefits'/></a></li>
                    <li class="active"><a href="#"><spring:message  code='label.planRates'/></a></li>
                    <li><a href="<c:url value="/issuer/qhp/qhphistory/${encPlanId}"/>"><spring:message  code='label.planStatusHistory'/></a></li>
			   </ul>

				<!-- <h4>Actions</h4>
				<ul class="nav nav-tabs nav-stacked">
					<li><a href="#"><i class="icon icon-user"></i> Start New Ticket</a></li>
				</ul> -->
        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			<display:table name="rates" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
			           <display:column property="lastUpdateTimestamp" titleKey="label.date"  format="{0,date,MMM dd, yyyy}" sortable="false" headerClass="graydrkbg" />
			           <display:column property="rateEffectiveDate" titleKey="label.effectiveStartDate" format="{0,date,MMM dd, yyyy}"  sortable="false" headerClass="graydrkbg"/>
			           <display:column property="rateEffectiveEndDate" titleKey="label.effectiveEndDate" format="{0,date,MMM dd, yyyy}" sortable="false" headerClass="graydrkbg"/>
			           <%-- <display:column property="userName" media="none" titleKey="label.userName" sortable="false" headerClass="graydrkbg"/>
			        	<c:if test="${exchangeType != 'PHIX'}">
			           		<display:column property="commentId" media="none" title="Comment" sortable="false" headerClass="graydrkbg"/>
			       	 	</c:if> --%>
			       	 	<display:column property="downloadLink" title="Download" sortable="false" headerClass="graydrkbg"/>
			           <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
			           <display:setProperty name="paging.banner.last" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<a class="active"><strong>{0}</strong></a>'/>
			           <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.one_item_found" value=''/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks">
			           <div class="pagination center">
						<ul>
							<li>{0}</li>
							<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
						</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.last" value='<span class="pagelinks">
						<div class="pagination center">
							<ul>
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
							</ul>
						</div>
						</span>'/>
					<display:setProperty name="paging.banner.full" value='
						<div class="pagination center">
							<ul
								<li><a href="{2}"><spring:message code="label.prevPage"/></a></li>
								<li>{0}</li>
								<li><a href="{3}"><spring:message code="label.nextPage"/></a></li>
							</ul>
						</div>
						'/>
			</display:table>
		</div><!-- end of .span9 -->
	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})
</script>
