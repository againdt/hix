package com.getinsured.eligibility.at.resp.si.handler;

import java.util.Map;

import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;


/**
 * MemberMatchHandler to compare stored ssapApplicants with the received state's response.
 *
 * @author Deval
 *
 */
@Component("nvHeaderEnricher")
public class NVHeaderEnricher {
	
	public ERPResponse processERP(Message<ERPResponse> message){

		String messageString = (String) message.getHeaders().get("AT_PROCESS");
		@SuppressWarnings("unchecked")
		Map<String, Object> resultMap = (Map<String, Object>) EligibilityUtils.unmarshal(messageString);
		ERPResponse erpResponse = (ERPResponse) resultMap.get(EligibilityConstants.ERP_RESPONSE);

		return erpResponse;
	}
}
