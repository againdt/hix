import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FormattedMessage } from 'react-intl';
import _ from 'lodash';

// Components

import Form from './Form';
import HomeAddressInputs from './pageIncludes/common/address/HomeAddressInputs';
import MailingAddressInputs from './pageIncludes/common/address/MailingAddressInputs';
import NamesInputs from './pageIncludes/common/names/NamesInputs';
import PageHeader from '../headers/PageHeader';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData, fetchCounties, fetchCounty, validateAddress } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import validators from '../../utils/validators';
import { normalizeBooleanToString } from '../../utils/normalizeBooleanToString';


// Constants

import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import ENDPOINTS from '../../constants/endPointsList';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';

// CustomHooks

import { useFetchEffect } from '../../hooks/customHooks';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Primary Contact Information' Page
 *
 */
const Page1_02  = props => {

    const {
        counties,
        currentData,
        data,
        headerLinks: {
            stateCode
        }
    } = props;

    const {
        pageName
    } = props;

    const {
        dateOfBirth,
        householdContact: {
            homeAddress,
            homeAddress: {
                postalCode,
                countyCode,
                streetAddress1,
                streetAddress2,
                state,
                city,
            },
            phone: {
                phoneNumber,
            },
            otherPhone: {
                phoneNumber: _phoneNumber,
                phoneExtension
            },
            contactPreferences: {
                preferredSpokenLanguage,
                preferredWrittenLanguage,
                preferredContactMethod
            },
            mailingAddressSameAsHomeAddressIndicator,
            mailingAddress: {
                postalCode: _postalCode,
                countyCode: _countyCode,
                streetAddress1: _streetAddress1,
                streetAddress2: _streetAddress2,
                state: _state,
                city: _city,
            },
            contactPreferences: {
                emailAddress
            }
        },
        name: {
            firstName,
            middleName,
            lastName,
            suffix
        }
    } = currentData;

    // Local State for Functional Component

    const [ localState, setLocalState ] = useState({
        addressBeforeChanges: {
            postalCode: null,
            streetAddress1: null,
            state: null,
            city: null
        }
    });

    const [ isBlur, toggleBlur ] = useState(false);

    useEffect(() => {
        document.body.scrollTop = document.documentElement.scrollTop = 0;
    }, []);

    // Fetching needed counties for state / states:
    // a) on ComponentDidMount
    // b) on ComponentDidUpdate (if changes are related to
    // <householdContact.homeAddress.state> or <householdContact.mailingAddress.state>)

    const statesForCounties = [
        currentData.householdContact.homeAddress.state,
        currentData.householdContact.mailingAddress.state
    ];

    const { selectedCall } = useFetchEffect(
        statesForCounties,
        [
            ENDPOINTS.COUNTIES_FOR_STATE_ENDPOINT,
            ENDPOINTS.COUNTIES_FOR_STATES_ENDPOINT
        ],
        counties,
        props.actions.fetchCounties
    );

    const isPostalCodeValid = validators
        .find(validator => validator.validatorType === VALIDATION_TYPES.ZIP_CODE)
        .verifier({ value: postalCode });

    const cleanCountyCode = () => {
        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[0]
            .householdContact
            .homeAddress
            .countyCode
            = null;
        props.actions.changeData(newState);
    };

    useEffect(() => {
        if(countyCode !== null && !isPostalCodeValid) {
            cleanCountyCode();
        }
    }, [postalCode]);

    useEffect(() => {
        if(isPostalCodeValid) {
            if(CONFIG.ENV.PRODUCTION) {
                if(state && postalCode.slice(0, 5)) {
                    props.actions.fetchCounty(
                        `${window.location.origin}${ENDPOINTS.COUNTY_ENDPOINT}${state}/${postalCode}`,
                        pageName,
                        cleanCountyCode
                    );
                }
            }
        }
    }, [state, postalCode]);

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            selectedCall && selectedCall !== null && selectedCall();
        }
    }, [selectedCall]);

    useEffect(() => {
        if(mailingAddressSameAsHomeAddressIndicator) {
            const newState = { ...data };

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[0]
                .householdContact
                .mailingAddress = newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[0]
                    .householdContact
                    .homeAddress;

            props.actions.changeData(newState);
        }
    }, Object.values(homeAddress));


    // Here we on focus inputs related to Address (streetAddress1, city, postalCode, state) make a snapshot
    // to know if after User's updates in inputs to invoke async address validation or not

    const snapshotAddress = () => {

        toggleBlur(false);

        const {
            postalCode,
            streetAddress1,
            state,
            city,
        } = currentData.householdContact.homeAddress;

        setLocalState({
            addressBeforeChanges: {
                postalCode,
                streetAddress1,
                state,
                city,
            }
        });
    };

    // Updating some props in JSON with the simultaneous cleaning up county and county code

    const onFieldChange = (...args) => {

        const newState = cloneObject(data);

        const [ newData, , fields, [ name, value, , text ] ] = args;

        const isHomeAddressChanged = (fields => _.difference(fields, ['householdContact', 'homeAddress']).length === 0)(fields);

        if(isHomeAddressChanged) {
            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .otherAddresses[0] = newData
                    .householdContact
                    .homeAddress;
        }

        if(name === 'countyCode') {
            newData.householdContact[fields[1]].county = text;
            newData.householdContact[fields[1]].primaryAddressCountyFipsCode = value;
        }

        const currentState_01 = newData.householdContact.homeAddress.state;

        if (currentState_01 !== currentData.householdContact.homeAddress.state) {
            newData.householdContact.homeAddress.county = null;
            newData.householdContact.homeAddress.countyCode = null;
        }

        const currentState_02 = newData.householdContact.mailingAddress.state;

        if (currentState_02 !== currentData.householdContact.mailingAddress.state) {
            newData.householdContact.mailingAddress.county = null;
            newData.householdContact.mailingAddress.countyCode = null;
        }


        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = newData;
        props.actions.changeData(newState);
    };

    // Hide/show mailing address and:
    // a) if checked to overwrite <mailingAddress> object to be equal to <homeAddress> object
    // b) if unchecked to clean up <mailingAddress> object

    const onCheckTheSameAddress = newData => {

        const newState = { ...data };

        const sameAddress = newData
            .householdContact
            .mailingAddressSameAsHomeAddressIndicator ?

            {
                ...newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[0]
                    .householdContact
                    .homeAddress
            } :
            {
                ...cloneObject(EMPTY_OBJECTS.HOUSEHOLD_CONTACT.mailingAddress)
            };

        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = newData;
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[0].householdContact.mailingAddress = sameAddress;
        props.actions.changeData(newState);
    };

    // Async validation of home address
    // If exact match: nothing on UI
    // If we have discrepancy - show modal where User decides what to do: to agree with suggestions or not


    const {
        homeAddress: currentHomeAddress,
    } = currentData.householdContact;

    const { addressBeforeChanges}  = localState;

    const notEmptyAndNotNull = (() => {
        let result = true;
        [ streetAddress1, city, state, postalCode ].forEach(value => {
            if (value === '' || value === null) {
                result = false;
            }
        });
        return result;
    })();

    const isAddressesTheSame = (() => {
        let result = true;
        Object.keys(addressBeforeChanges).forEach(key => {
            if (addressBeforeChanges[key] !== currentHomeAddress[key]) {
                result = false;
            }
        });
        return result;
    })();

    useEffect(() => {
        if(!isAddressesTheSame
            && notEmptyAndNotNull && isBlur) {

            if(CONFIG.ENV.PRODUCTION) {
                props.actions.validateAddress(
                    onFieldChange,
                    `${window.location.origin}${ENDPOINTS.VALIDATE_ADDRESS_ENDPOINT}${window.csrf_token}`,
                    {
                        addressLine1: streetAddress1,
                        addressLine2: streetAddress2,
                        city: city,
                        state: state,
                        zipcode: postalCode
                    }
                );
            }
        }
    }, [ normalizeBooleanToString(!isAddressesTheSame, notEmptyAndNotNull, isBlur) ]);

    const validateAddress = () => {
        toggleBlur(true);
    };

    const handlePhoneChange = (...args) => {
        const [ newData , , [ , phoneType ] ] = args;
        const newState = { ...data };

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[0]
            .householdContact[phoneType]
            .phoneNumber = newData.householdContact[phoneType].phoneNumber;

        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <div id="saPrimaryContact" className="">
                <Form>
                    <div className="subsection">
                        <PageHeader
                            text="Primary Contact Name"
                            headerClassName={'usa-heading-alt'}
                        />
                        <NamesInputs
                            {...{
                                firstName,
                                middleName,
                                lastName,
                                suffix,
                                currentData,
                                fields: ['name'],
                                isRequired: true,
                                inputActions: {
                                    onFieldChange
                                }
                            }}
                        />

                        <UniversalInput
                            label={{
                                labelClass: ['gi-dropdown-label'],
                                text: 'Date of Birth'
                            }}
                            inputData={{
                                inputClass:['gi-dropdown'],
                                isInputRequired: true,
                                type: INPUT_TYPES.DATE_PICKER,
                                name: 'dateOfBirth',
                                value: dateOfBirth,
                                placeholder: 'MM/DD/YYYY',
                                validationType: VALIDATION_TYPES.DATE_NOT_GREATER_THAN_CURRENT,
                                currentData: currentData,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            indexInArray={0}
                        />
                        <UniversalInput
                            label={{
                                text: 'Email Address',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'emailAddress',
                                helperClasses: [],
                                value: emailAddress,
                                placeholder: 'Enter your email',
                                validationType: VALIDATION_TYPES.EMAIL,
                                currentData: currentData,
                                fields: ['householdContact', 'contactPreferences']
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                    </div>

                    <div className="subsection">
                        <PageHeader
                            text="Primary Contact Home Address"
                            headerClassName={'usa-heading-alt'}
                        />
                        <HomeAddressInputs
                            {...{
                                postalCode,
                                countyCode,
                                streetAddress1,
                                streetAddress2,
                                state,
                                city,
                                currentData: cloneObject(currentData),
                                counties,
                                pageName: pageName,
                                inputActions: {
                                    onFieldChange,
                                    onFieldBlur: validateAddress,
                                    onFieldFocus: snapshotAddress
                                },
                                stateCode
                            }}
                        />
                    </div>
                    <div className="subsection">
                        <PageHeader
                            text="Primary Contact Mailing Address"
                            headerClassName={'usa-heading-alt'}
                        />
                        <fieldset className="usa-fieldset-inputs usa-sans ">
                            <ul className="usa-unstyled-list">
                                <li>
                                    <UniversalInput
                                        label={{
                                            text: 'Check if same as Primary Contact Home Address'
                                        }}
                                        inputData={{
                                            isInputRequired: false,
                                            type: INPUT_TYPES.CHECKBOX,
                                            name: 'mailingAddressSameAsHomeAddressIndicator',
                                            classes:['layout--02'],
                                            checked: mailingAddressSameAsHomeAddressIndicator,
                                            value: mailingAddressSameAsHomeAddressIndicator,
                                            currentData: currentData,
                                            fields: ['householdContact']
                                        }}
                                        inputActions={{
                                            onFieldChange: onCheckTheSameAddress
                                        }}
                                    />
                                </li>
                            </ul>
                        </fieldset>

                        {/********************************************************************
                         Begin - If User is #mailingAddressSameAsHomeAddressIndicator (false)
                         **********************************************************************/}

                        {
                            !mailingAddressSameAsHomeAddressIndicator &&

                            <MailingAddressInputs
                                {...{
                                    _postalCode,
                                    _countyCode,
                                    _streetAddress1,
                                    _streetAddress2,
                                    _state,
                                    _city,
                                    currentData,
                                    counties,
                                    inputActions: {
                                        onFieldChange
                                    }
                                }}
                            />
                        }

                        {/********************************************************************
                         End - If User is #mailingAddressSameAsHomeAddressIndicator (false)
                         **********************************************************************/}

                    </div>
                    <div className="subsection">
                        <PageHeader
                            text="Primary Contact Phone"
                            headerClassName={'h4-not-highlighted'}
                        />
                        <UniversalInput
                            label={{
                                text: 'Mobile Phone Number',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.PHONE_NUMBER_INPUT,
                                name: 'phoneNumber',
                                helperClasses: [],
                                value: phoneNumber,
                                placeholder: '(xxx) xxx-xxxx',
                                validationType: VALIDATION_TYPES.PHONE_NUMBER,
                                currentData: currentData,
                                fields: ['householdContact', 'phone']
                            }}
                            inputActions={{
                                onFieldChange: handlePhoneChange,
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Home Phone Number',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.PHONE_NUMBER_INPUT,
                                name: '_phoneNumber',
                                helperClasses: [],
                                value: _phoneNumber,
                                placeholder: '(xxx) xxx-xxxx',
                                validationType: VALIDATION_TYPES.PHONE_NUMBER,
                                currentData: currentData,
                                fields: ['householdContact', 'otherPhone']
                            }}
                            inputActions={{
                                onFieldChange: handlePhoneChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Phone Extension',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'phoneExtension',
                                helperClasses: [],
                                value: phoneExtension,
                                placeholder: 'Ext.',
                                validationType: VALIDATION_TYPES.PHONE_EXTENSION,
                                currentData: currentData,
                                fields: ['householdContact', 'otherPhone']
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                    </div>
                    <div className="subsection">
                        <PageHeader
                            text="Primary Contact Preferences"
                            headerClassName={'usa-heading-alt'}
                        />
                        <UniversalInput
                            label={{
                                text: 'Preferred Spoken Language'
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.DROPDOWN,
                                name: 'preferredSpokenLanguage',
                                value: preferredSpokenLanguage,
                                options: DROPDOWN_OPTIONS.LANGUAGE,
                                currentData: currentData,
                                fields: ['householdContact', 'contactPreferences']
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Preferred Written Language'
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.DROPDOWN,
                                name: 'preferredWrittenLanguage',
                                value: preferredWrittenLanguage,
                                options: DROPDOWN_OPTIONS.LANGUAGE,
                                currentData: currentData,
                                fields: ['householdContact', 'contactPreferences']
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        <UniversalInput
                            legend={{
                                legendText: 'Preferred Method of Communication'
                            }}
                            label={{
                                text: DROPDOWN_OPTIONS.COMMUNICATION_METHOD,
                                labelClass: ['usa-width-one-half', 'margin-t-20'],
                                ulClass: ['usa-width-one-whole layout--01'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'preferredContactMethod',
                                inputClass: ['usa-width-one-half'],
                                value: preferredContactMethod,
                                options: DROPDOWN_OPTIONS.COMMUNICATION_METHOD,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: ['householdContact', 'contactPreferences']
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        <div className="usa-width-one-whole">
                            <div className="usa-alert usa-alert-info alert--info">
                                <div className="usa-alert-body" >
                                    <p className="usa-alert-text">
                                        <FormattedMessage
                                            id="ssap.primaryContactInfo.msg.one"
                                            defaultMessage="You will receive notifications based on your preferred method of communication, and each notification will be preceded by an email alert if applicable."
                                        />
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Form>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    counties: state.counties,

    currentData: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0],

    data: state.data,
    headerLinks: state.headerLinks
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        fetchCounty,
        fetchCounties,
        validateAddress
    }, dispatch)
});

Page1_02.defaultProps = {
    name: 'Page1_02'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_02);
