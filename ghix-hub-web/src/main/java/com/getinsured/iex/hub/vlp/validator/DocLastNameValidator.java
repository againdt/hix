package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for doc last Name
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 *
 */
public class DocLastNameValidator extends Validator{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocLastNameValidator.class);
	
	/** 
	 * Validations for doc last name
	 * String between 1-40 characters
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String docLastName = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			docLastName = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(docLastName.length() < VLPServiceConstants.DOC_LAST_NAME_MIN_LEN
				|| docLastName.length() > VLPServiceConstants.DOC_LAST_NAME_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.DOC_LAST_NAME_LEN_ERROR_MESSAGE);
		}

	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
