package com.getinsured.consumer.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.service.ConsumerDocumentService;

/*
 * Handles the consumer documents stored in CMR_DOCUMENTS table 
 */
@Controller
@RequestMapping("/consumerDocument")
public class ConsumerDocumentController {
	
	private static final Logger LOGGER = Logger.getLogger(ConsumerDocumentController.class);
	
	@Autowired
	private ConsumerDocumentService consumerDocumentService;

	@RequestMapping(value = "/update", 
			        method = RequestMethod.POST, 
			        produces = MediaType.APPLICATION_JSON_VALUE, 
			        consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ResponseEntity<String> update(@RequestBody String jsonInput) {
		String jsonResponse = null;
		LOGGER.info("Inside update");
		try {
			jsonResponse = consumerDocumentService.updateDocument(jsonInput);
		} catch (ConsumerServiceException consumerServiceException) {
			LOGGER.error("Error in updating the consumer document");
			jsonResponse = consumerDocumentService.handleException(consumerServiceException);
		} catch (Exception exception) {
			LOGGER.error("Unknown error in updating the consumer document", exception);
			jsonResponse = consumerDocumentService.handleException(exception);
		}
		LOGGER.debug("Done update");
		return new ResponseEntity<String>(jsonResponse,HttpStatus.OK);

	}

}
