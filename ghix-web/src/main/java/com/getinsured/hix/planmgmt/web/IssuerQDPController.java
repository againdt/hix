package com.getinsured.hix.planmgmt.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Esignature;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.EnrollmentAvail;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.Plan.PlanMarket;
import com.getinsured.hix.model.Plan.PlanStatus;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.EsignatureService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.getinsured.hix.util.TicketMgmtUtils;


@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerQDPController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerController.class);
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private EsignatureService esignatureService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private RoleService roleService;
	@Autowired private TicketMgmtUtils ticketMgmtUtils;
	
	@Value("#{configProp.uploadPath}") private String uploadPath;
	
	private static final String ISSUER_REP_ROLE = "ISSUER_REPRESENTATIVE";
	private static final String TICKET_CATEGORY = "Issues";
	private static final String TICKET_TYPE = "Issuer Problem";
	
	// Issuer Portal : Manage QDP plans
	@RequestMapping(value = "/issuer/manageqdp", method = { RequestMethod.GET,RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String manageQDP(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan plan, Model model, HttpServletRequest manageQDPrequest) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			Integer startRecord = 0;
			int currentPage = 1;
	
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			String displayPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
			model.addAttribute(PlanMgmtConstants.DISPLAYPLANLEVEL, displayPlanLevel);
			model.addAttribute(PlanMgmtConstants.STATUS_LIST, plan.getPlanStatuses());
			model.addAttribute("marketList", plan.getMarketList());
			model.addAttribute("levelList", plan.getLevelList());
			model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
			List<Map<String, Integer>> planYearsList = planMgmtService.getPlanYears(PlanMgmtConstants.DENTAL, issuerObj.getId());
			// Search attributes
			manageQDPrequest.setAttribute(PlanMgmtConstants.ISSUERID, issuerObj.getId());
	
			if (manageQDPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER) != null) {
				manageQDPrequest.setAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQDPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));
			}
			for (PlanLevel dir : PlanLevel.values()) {
				if (manageQDPrequest.getParameter(dir.toString()) != null) {
					manageQDPrequest.setAttribute(manageQDPrequest.getParameter(dir.toString()), manageQDPrequest.getParameter(dir.toString()));
				}
			}
			if (manageQDPrequest.getParameter(PlanMgmtConstants.MARKET) != null
					&& !manageQDPrequest.getParameter(PlanMgmtConstants.MARKET).equals(PlanMgmtConstants.EMPTY_STRING)) {
				manageQDPrequest.setAttribute(PlanMgmtConstants.MARKET, manageQDPrequest.getParameter(PlanMgmtConstants.MARKET));
			}
			if (!StringUtils.isEmpty(manageQDPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY))) {
				manageQDPrequest.setAttribute(PlanMgmtConstants.ENROLLMENTAVAILABILITY, manageQDPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY));
			}
			if (manageQDPrequest.getParameter(PlanMgmtConstants.STATUS) != null
					&& !manageQDPrequest.getParameter(PlanMgmtConstants.STATUS).equals(PlanMgmtConstants.EMPTY_STRING)) {
				LOGGER.debug("Apply status");
				manageQDPrequest.setAttribute(PlanMgmtConstants.STATUS, manageQDPrequest.getParameter(PlanMgmtConstants.STATUS));
			}
			if (manageQDPrequest.getParameter(PlanMgmtConstants.ENROLLMENT) != null
					&& !manageQDPrequest.getParameter(PlanMgmtConstants.ENROLLMENT).equals(PlanMgmtConstants.EMPTY_STRING)) {
				manageQDPrequest.setAttribute(PlanMgmtConstants.ENROLLMENT, manageQDPrequest.getParameter(PlanMgmtConstants.ENROLLMENT));
			}
			
			if(manageQDPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null && !manageQDPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).equals("")){
				Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
				selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(manageQDPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
				model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
				LOGGER.debug("Fetching Plans for Year : " + SecurityUtil.sanitizeForLogging(selectedYearMap.toString()));
				manageQDPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (manageQDPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
			}
			else {
				if(planYearsList.size() > PlanMgmtConstants.ZERO){
					model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, planYearsList.get(0));
					LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(planYearsList.get(0).toString()));
					manageQDPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (planYearsList.get(0).get(PlanMgmtConstants.PLAN_YEARS_KEY)));
				}else{
					return PlanMgmtConstants.ISSUER_MANAGEQDP;
				}
			}
			// Sorting attributes
			if (manageQDPrequest.getParameter(PlanMgmtConstants.SORT_BY) != null) {
				manageQDPrequest.setAttribute(PlanMgmtConstants.SORT_BY, manageQDPrequest.getParameter(PlanMgmtConstants.SORT_BY));
			}
			String sortOrder = (manageQDPrequest.getParameter(PlanMgmtConstants.SORT_ORDER) == null) ? PlanMgmtConstants.SORT_ORDER_ASC : ((manageQDPrequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
			sortOrder = (sortOrder.equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.SORT_ORDER_ASC : sortOrder;
			
			String changeOrder = (manageQDPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER) == null) ? PlanMgmtConstants.FALSE_STRING : ((manageQDPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
			sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.ASC : PlanMgmtConstants.SORT_ORDER_DESC) : sortOrder;
			
			manageQDPrequest.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
			manageQDPrequest.setAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
			manageQDPrequest.setAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			String param = manageQDPrequest.getParameter(PlanMgmtConstants.PAGE_NUMBER);
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1) * GhixConstants.PAGE_SIZE;
				currentPage = Integer.parseInt(param);
			}
			manageQDPrequest.setAttribute("startRecord", startRecord);
	
			// retrieving the plans
			List<Plan> planList = planMgmtService.getPlanList(manageQDPrequest, PlanMgmtService.DENTAL);
	
			// template/view variables
			model.addAttribute("plans", planList);
			model.addAttribute(PlanMgmtConstants.STATUS, manageQDPrequest.getParameter(PlanMgmtConstants.STATUS));
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQDPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));
			model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
			model.addAttribute("issuerCertStatusList", issuerObj.getIssuerStatuses());
			model.addAttribute(PlanMgmtConstants.MARKET, manageQDPrequest.getParameter(PlanMgmtConstants.MARKET));
			model.addAttribute("pendingStatus", PlanMgmtService.ISSUER_VERIFICATION_STATUS_PENDING);
			model.addAttribute("certifyStatus", PlanMgmtService.STATUS_CERTIFIED);
			model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
			model.addAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
			model.addAttribute(PlanMgmtConstants.SORT_BY, manageQDPrequest.getParameter(PlanMgmtConstants.SORT_BY));
			model.addAttribute("displayDtFmt", GhixConstants.DISPLAY_DATE_FORMAT);
			model.addAttribute("selectedPlanLevel", manageQDPrequest.getParameter("planLevel"));
			model.addAttribute(PlanMgmtConstants.PLAN_YEARS_LIST, planYearsList);
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
			
			return PlanMgmtConstants.ISSUER_MANAGEQDP;
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGEQDP_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	@RequestMapping(value = "/issuer/qdp/plandetails/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_PLAN_DETAILS)
	public String qdpPlanDetails(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				model.addAttribute("acturialValueCertificate", plan.getPlanDental().getActurialValueCertificate());
				model.addAttribute("market", plan.getMarketList().get(Enum.valueOf(PlanMarket.class, plan.getMarket())));
				model.addAttribute(PlanMgmtConstants.STATUS, plan.getPlanStatuses().get(Enum.valueOf(PlanStatus.class, plan.getStatus())));
				model.addAttribute(PlanMgmtConstants.LAST_UPDATED, plan.getLastUpdateTimestamp());
				String enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, plan.getEnrollmentAvail()));
	
				Calendar cal = TSCalendar.getInstance();
				Date today = cal.getTime();
				// if enrollment status is available/dependents only but available
				// from future date then availability consider as not available
				if (!plan.getEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())
						&& (plan.getEnrollmentAvailEffDate().compareTo(today) >= 0)) {
					enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, EnrollmentAvail.NOTAVAILABLE.toString()));
				}
				model.addAttribute("enrollAvailability", enrollmentAvial);
				
				// HIX-31861 Code changes
				model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
				model.addAttribute(PlanMgmtConstants.FUTURE_DATE, plan.getFutureErlAvailEffDate());
				model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));

				Date sysDate = new TSDate();
				if (plan.getFutureEnrollmentAvail() == null) {
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
				} else if (plan.getFutureErlAvailEffDate() != null && plan.getFutureErlAvailEffDate().compareTo(sysDate) > 0) {
					if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.AVAILABLE.toString())) {
						model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.AVAILABLE);
					} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())) {
						model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.NOTAVAILABLE);
					} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.DEPENDENTSONLY.toString())) {
						model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.DEPENDENTSONLY);
					}
				}
 	 	 	 	// HIX-31861 end changes
				
				if(plan.getServiceAreaId() != null){
					String serfServiceAreaId = issuerService.getSerfServiceAreaIdById(plan.getServiceAreaId());
					model.addAttribute(PlanMgmtConstants.SERVICE_AREA_ID, serfServiceAreaId);
				}
				
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				
			} else {
				// if plan id is not given in the request, then redirect to manage
				// qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			String displayPlanLevel = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QDP_SUPPORTPLANLEVEL);
			model.addAttribute(PlanMgmtConstants.DISPLAYPLANLEVEL, displayPlanLevel);
			return "issuer/qdp/plandetails";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QDPPLANDETAILS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	// Issuer Portal : Download QHP plan Benefits/Rates
	@RequestMapping(value = "/issuer/qdp/viewbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String qdpBenefitsRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				// fetching plan details	
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				List<Map<String, Object>> planBenefitRateHistory = planMgmtService.loadPlanBenefitsHistory(Integer.parseInt(planId), PlanMgmtConstants.ISSUER_ROLE);

				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute(PlanMgmtConstants.PLAN_CLASS, plan);
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.STATUS_HISTORY_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute("data", planBenefitRateHistory);
				model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));

				if (planBenefitRateHistory != null
						&& !planBenefitRateHistory.isEmpty()) {
					Map<String, Object> lastUpdatedMap = planBenefitRateHistory.get(0);
					model.addAttribute(PlanMgmtConstants.LAST_UPDATED, lastUpdatedMap.get(PlanMgmtConstants.UPDATED));
				}

				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);

				// Code changes for HIX-29035
				Calendar cal = TSCalendar.getInstance();
				Date today = cal.getTime();
				SimpleDateFormat ft = new SimpleDateFormat ("MMddyyyy");
				if(today.compareTo(plan.getStartDate()) < 0){
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(plan.getStartDate()));
				}
				else if(today.compareTo(plan.getEndDate()) > 0){
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(plan.getEndDate()));
				} else {
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(today));
				}
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				// End changes
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			return "issuer/qdp/viewbenefits";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QDPPBENEFITRATES_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	// Issuer Portal : Verify QDP plan
	@RequestMapping(value = "/issuer/qdp/verifyplan/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qdpVerifyPlan(Model model, @PathVariable("encPlanId") String encPlanId,  HttpServletRequest request, RedirectAttributes redirectAttributes) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		
		String planId = null;
		try {
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_FIRST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_FIRST_NAME);
			}			
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_LAST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_LAST_NAME);
			}
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				if (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null) {
					redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
				}
				Date sysDate = new TSDate();
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute("moduleName", ModuleUserService.ISSUER_MODULE);
				model.addAttribute("currDate", DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getFirstName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_FIRST_NAME, userService.getLoggedInUser().getFirstName());
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getLastName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_LAST_NAME, userService.getLoggedInUser().getLastName());
			}
			return "issuer/qdp/verifyplan";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QDPPVERIFYPLAN_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Verify QDP plan : Submit form
	@RequestMapping(value = "/issuer/qdp/verifyplan", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qdpIssuerVerifyPlan(@ModelAttribute("Esignature") Esignature esignatureObj, BindingResult result, Model model, HttpServletRequest request,RedirectAttributes redirectAttributes) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try {
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			} else {
				//server side validation
				Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
				 Set<ConstraintViolation<Esignature>> violations = validator.validate(esignatureObj, Esignature.VerifyPlan.class);
				 if(violations != null && !violations.isEmpty()){
					 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
					 }
				// Save the eSignature
				try {
					esignatureObj.setUser(userService.getLoggedInUser());
				} catch (InvalidUserException e) {
					LOGGER.error("Invalid User : " + e.getMessage());
					return PlanMgmtConstants.ISSUER_MANAGEQDP;
				}
				Date sysDate = new TSDate();
				esignatureObj.setModuleName(ModuleUserService.PLAN_MODULE);
				esignatureObj.setEsignDate(sysDate);
				esignatureObj.setStatements(request.getParameter("statements"));
				try {
					// Saving the esignature
					esignatureService.saveEsignature(esignatureObj);
				} catch (Exception e) {
					LOGGER.error("Error while saving the esignature. Exception in : "
							+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
					if(esignatureObj.getRefId() != null){
						String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(esignatureObj.getRefId()));
						return "redirect:/issuer/qdp/verifyplan/" + encryptedPlanId;
					}else{
						return "redirect:/issuer/manageqdp";
					}
					
				}
				
				String isBulkAction = request.getParameter("isBulkAction");
				String planIds = request.getParameter("planIds");
				
				if (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null) {
					redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
				}
				
				if(isBulkAction != null && isBulkAction.equals("YES") && StringUtils.isNotEmpty(planIds)){
				
					String[] planIdsArry = planIds.split(",");
					
					for(int i=0; i<planIdsArry.length; i++){
						String planId = planIdsArry[i];
						Plan planToVerify = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId.trim()), issuerService.getIssuerId());
						planToVerify.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED);
						try {
							planMgmtService.savePlan(planToVerify);
			
						} catch (Exception e) {
							LOGGER.error("Error while updating plan. Exception in : "
									+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
							return "redirect:/issuer/manageqdp";
						}
					}
					
				}else{					
					Plan planToVerify = planMgmtService.findPlanByIdAndIssuerId(esignatureObj.getRefId(), issuerService.getIssuerId());
					planToVerify.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED);
					try {
						
						planMgmtService.savePlan(planToVerify);
		
					} catch (Exception e) {
						LOGGER.error("Error while updating plan. Exception in : "
								+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
						String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(esignatureObj.getRefId()));
						return "redirect:/issuer/qdp/verifyplan/" + encryptedPlanId;
					}
				}
					
				return "redirect:/issuer/manageqdp";
			}
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.ISSUERVERIFYPLAN_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	// Issuer Portal : Download QHP plan history
	@RequestMapping(value = "/issuer/qdp/qdphistory/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String qdpHistory(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				// fetching plan details
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				// fetching the plan status history
				List<Map<String, Object>> planStatusHistory = planMgmtService.loadPlanStatusHistory(plan.getId());				
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.STATUS_HISTORY_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.PLAN_STATUS_HISTORY, planStatusHistory);

				if (planStatusHistory != null && !planStatusHistory.isEmpty()) {
					Map<String, Object> lastUpdatedMap = planStatusHistory.get(0);
					model.addAttribute(PlanMgmtConstants.LAST_UPDATED, lastUpdatedMap.get(PlanMgmtConstants.LAST_UPDATE_TIMESTAMP));
					model.addAttribute("currentStatus", lastUpdatedMap.get(PlanMgmtConstants.STATUS));
					model.addAttribute("enrollmentStatus", lastUpdatedMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL));
				}
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			return "issuer/qdp/viewhistory";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QDPHISTORY_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	

	@RequestMapping(value="/planmgmt/issuer/reports/sadpratebenefitreport", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String sadpRateBenefitReport(Model model, HttpServletRequest request){
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try{
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			if(null != issuerObj){
				LOGGER.info("Admin :: View Issuer History");
				List<Map<String, String>> rateBenefitReportData = issuerService.loadRateBenefitReportList(request, issuerObj.getId(), PlanMgmtConstants.SADP);
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute("ratebenefits", rateBenefitReportData);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
			}
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME, ex, null, Severity.HIGH);
		}
		return "planmgmt/issuer/reports/sadpratebenefitreport";
	}
	
	
	@RequestMapping(value = "/issuer/qdp/verifyplansBulk", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qdpVerifyPlansFromBulk(Model model, @RequestParam(value = "planIdList", required = false) String strPlanIdList,HttpServletRequest request, RedirectAttributes redirectAttributes) {
		try {
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_FIRST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_FIRST_NAME);
			}			
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_LAST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_LAST_NAME);
			}
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			
			if(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR)!= null){
				redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
			}
			if (strPlanIdList != null && !strPlanIdList.isEmpty()) {
				List<Integer> planIdList = new ArrayList<Integer>();
				
				String planIDArray[] = strPlanIdList.split(PlanMgmtConstants.COMMA_STRING);
				for(int i =0; i< planIDArray.length; i++){
					if(planIDArray[i] != null){
						planIdList.add(Integer.parseInt(planIDArray[i]));
					}
				}
				
				List<Plan> plans = planMgmtService.getPlans(planIdList);
				StringBuilder planNumbers = new StringBuilder();
				StringBuilder planIds = new StringBuilder();
				for(int i=0; i < plans.size(); i++){
					Plan plan = plans.get(i);
					if(plan.getIssuerVerificationStatus().equalsIgnoreCase(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.IS_VERIFIED, "YES");
						return "redirect:/" + PlanMgmtConstants.ISSUER_MANAGEQDP;
					}else if(!plan.getStatus().equalsIgnoreCase(PlanMgmtService.STATUS_CERTIFIED)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.IS_CERTIFIED, "NO");
						return "redirect:/" + PlanMgmtConstants.ISSUER_MANAGEQDP;
					}else{
						planNumbers.append(plan.getIssuerPlanNumber());
						planIds.append(plan.getId());
						if(i != plans.size()-1){
							planNumbers.append(", ");
							planIds.append(", ");
						}
					}
				}
				
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				Date sysDate = new TSDate();
				model.addAttribute(PlanMgmtConstants.PLAN_COUNT, plans.size());
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.PLAN_NUMBERS, planNumbers);
				model.addAttribute(PlanMgmtConstants.PLAN_ID_LIST, planIds);
				model.addAttribute(PlanMgmtConstants.IS_BULK_ACTION, "YES");
				model.addAttribute(PlanMgmtConstants.MODULENAME, ModuleUserService.ISSUER_MODULE);
				model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
				}else {
				// if plan id is not given in the request, then redirect to manage qhp screen
					return "redirect:/" + PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getFirstName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_FIRST_NAME, userService.getLoggedInUser().getFirstName());
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getLastName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_LAST_NAME, userService.getLoggedInUser().getLastName());
			}
			return "issuer/qdp/verifyplan";
		} catch (Exception e) {
			LOGGER.error("Exception in qdpVerifyPlan ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QDPPVERIFYPLAN_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}
	
	
	@RequestMapping(value = "/issuer/qdp/createTicket/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String createQDPTicket(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);	
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				String email = userService.getLoggedInUser().getEmail();
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				if(null != plan) {
					model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				}				
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj) {
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				}
				IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByEmail(email);
				if(null != issuerRepObj) {
					model.addAttribute(PlanMgmtConstants.ISSUER_REP_OBJ, issuerRepObj);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occurred in IssuerController.createQDPTicket():: ",e);
		}
		return "planmgmt/issuer/qdp/createTicket";		
	}
	
	
	
	@RequestMapping(value = "/planmgmt/issuer/qdp/save", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String saveQDPCreateTicket(Model model, HttpServletRequest request) throws InvalidUserException, GIException {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();		
		Integer userId = null;
		String planId = null;
		String encPlanId = null;
		String encIssuerId = null;
		String issuerId = null;
		try {	
			AccountUser user =userService.getLoggedInUser();
			String email = userService.getLoggedInUser().getEmail();
			userId = user.getId();
			String priority = request.getParameter("priority");
			String description = request.getParameter("description");
			encPlanId = request.getParameter("planId");
			String planName = request.getParameter("planName");
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);			
			tkmRequest.setCategory(TICKET_CATEGORY);
			tkmRequest.setLastUpdatedBy(userId);			
			tkmRequest.setType(TICKET_TYPE);
			tkmRequest.setUserRoleId(roleService.findRoleByName(ISSUER_REP_ROLE).getId());
			tkmRequest.setDescription(description);
			tkmRequest.setModuleName(ISSUER_REP_ROLE);
			tkmRequest.setPriority(priority);
			tkmRequest.setSubject(planId + " " + planName);	
			tkmRequest.setRequester(userId);
			tkmRequest.setComment(planId + " " + planName);
			tkmRequest.setCreatedBy(userId);
			encIssuerId = request.getParameter("issuerId");
			issuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(issuerId));
			if(null != issuerObj) {
				IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByEmail(email);
				if(null != issuerRepObj) {
					tkmRequest.setModuleId(issuerRepObj.getId());					
				}
			}
			TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
			LOGGER.debug("Ticket ID is "+ (tkmResponse!=null ?  SecurityUtil.sanitizeForLogging(tkmResponse.getNumber()) : "null"));
		}catch(InvalidUserException iui) {
			LOGGER.error("Invalid User IssuerController.saveQDPCreateTicket():: ",iui);
		}catch(Exception e) {
			LOGGER.error("Exception Occurred in IssuerController.saveQDPCreateTicket():: ", e);
		}		
		
		return "redirect:/issuer/qdp/plandetails/"+ghixJasyptEncrytorUtil.encryptStringByJasypt(planId);
	}
	
	
	public TicketMgmtUtils getTicketMgmtUtils() {
		return ticketMgmtUtils;
	}
}
