<%@ page import="com.getinsured.hix.platform.security.service.ModuleUserService"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page isELIgnored="false"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

<link rel="stylesheet" type="text/css" href="
<gi:cdnurl value="/resources/broker/css/style.css" />
" />
<script type="text/javascript" src="
<c:url value="/resources/broker/js/jquery.waypoints.min.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/broker/js/sticky.min.js" />
"></script>
<script type="text/javascript" >
	var encFromModule = '${encFromModule}';
	var individualsIdAtClient = new Array();
	var totalCountAtClient = 0;
	var stateCode = ${CA_STATE_CODE};
	var recordTypeVar = '${recordType}';
</script>
<script type="text/javascript" src="
<c:url value="/resources/broker/js/app.js" />
"></script>
<script src="/hix/resources/js/parsley.2.0.js"></script>
<%
	Integer pageSizeInt = (Integer) request.getAttribute("pageSize");
	String reqURI = (String) request.getAttribute("reqURI");
	Integer totalResultCount = (Integer) request
			.getAttribute("resultSize");
	if (totalResultCount == null) {
		totalResultCount = 0;
	}
	Integer currentPage = (Integer) request.getAttribute("currentPage");
	int noOfPage = 0;

	if (totalResultCount > 0) {
		if ((totalResultCount % pageSizeInt) > 0) {
			noOfPage = (totalResultCount / pageSizeInt) + 1;
		} else {
			noOfPage = (totalResultCount / pageSizeInt);
		}
	}
%>

<!--start page-breadcrumb -->
<div class="gutter10-lr"  ng-app="agentEntityApp">
<div class="row-fluid" style="margin-bottom:10px" ng-controller="angentEntityCtrl">

	<div class="span12">
		<div class="l-page-breadcrumb">
			<div class="row-fluid">
			<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}" />
				<ul class="page-breadcrumb">
                        <li>
                            <a href="javascript:history.back()">
                                &lt;
                                <spring:message code="label.back" />
                            </a>
                        </li>
                        <li>
                            <a href="<c:url value="/broker/individuals"/>">
                            <spring:message
                                code="label.individuals" />
                            </a>
                        </li>
					<li>${desigStatus}</li>
				</ul>
			</div>
			<!--page-breadcrumb ends-->
		</div>


		<c:if test="${message != null}">
			<div class="errorblock alert alert-info">
				<p>${message}</p>
			</div>
		</c:if>
            <h1>
                <a name="skip"></a>
                 <c:choose>
			   		<c:when test="${CA_STATE_CODE}">
			   			<spring:message code="label.broker.active.consumers"/>
			   		</c:when>
					<c:otherwise>
						<spring:message code="label.individuals"/>
					</c:otherwise>
			 	</c:choose>
               <small>${resultSize}	${tableTitle}</small>
            </h1>
            <c:choose>
			   		<c:when test="${CA_STATE_CODE}">
            <p class="alert alert-info"><spring:message code="label.agent.activeindividual.disclaimer"/></p>
			   		</c:when>
			</c:choose>
	  	<div class="searchPanelWrapper " ng-controller="searchFilterCtrl" id="panel">
			<div>
                    <c:set var="encFromModule" >
                        <encryptor:enc value="<%=ModuleUserService.INDIVIDUAL_MODULE%>" isurl="true"/>
                    </c:set>
	         <div class="panel-heading clearfix" ng-click="searchFilterSlider($event)">
				<h4 class="noPadding noMargin pull-left"><i class="icon-search"></i> <spring:message code="label.search"/> </h4>
				<!-- <span class="appliedFilters pull-left" ng-show="true" ng-repeat="(key ,filter) in params.filters">
					<span class="badge filterContainer" ng-if="filter.length > 0">{{filter}} <span class="icon-remove-sign removeFilter" filterName="{{key}}" ng-click="removeFilter($event)"></span></span>
				</span> -->
				<button href="javascript:;" class="btn btn-default btn-primary pull-right search" aria-label="searchbar expanded" title="searchbar collapsed" ng-hide="isSliderOpen" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Search Bar Open', 1]);">+</button>
				<button href="javascript:;" class="btn btn-default btn-primary pull-right search" aria-label="searchbar collapsed" title="searchbar expdanded" ng-show="isSliderOpen" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Search Bar Close', 1]);">-</button>
			  </div>
			  <div class="panel-body searchPanel" aria-label="searchbar collapsed">
					<form id="searchFilterForm" class="form-horizontal form-wrapper-two">
						<div class="row-fluid" style="margin-bottom:10px">
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="firstName" class="bs3-control-label-left"><spring:message code="label.agent.individual.firstName"/></label>
								<input type="text" class="input-full-width" placeholder="" id="firstName"  name="firstName" ng-model="filters.firstName" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field First Name', 1]);"/>
							</div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="lastName" class="bs3-control-label-left"><spring:message code="label.agent.individual.lastName"/></label>
								<input type="text" class="input-full-width " placeholder="" id="lastName" name="lastName" ng-model="filters.lastName" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Last Name', 1]);" />
							</div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="applicationType" class="bs3-control-label-left"><spring:message code="label.agent.individual.applicationType"/></label>
								<select type="text" class="input-full-width " placeholder="" id="applicationType" name="applicationType" ng-model="filters.applicationType" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Application Type', 1]);">
								<option value="">Select Application Type</option>
								<c:forEach var="applicationType" items="${applicationTypeList}">
									 <option value="${applicationType}">${applicationType}</option>
								 </c:forEach>
								</select>
							</div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="issuer" class="bs3-control-label-left"><spring:message code="label.agent.issuer"/></label>
							<select type="text" class="input-full-width " placeholder="" id="issuer" name="issuer" id="issuer" ng-model="filters.issuerval"	onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Issuer', 1]);">
								<c:if test="${CA_STATE_CODE}">
									<option value="">                  Select Issuer     </option>
									<option value="All Health Issuers">All Health Issuers</option>
									<option value="All Dental Issuers">All Dental Issuers</option>
								</c:if>
								<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
								  <optgroup label="Both QHP & QDP">
								    <option ng-repeat="bothdata in bothQHPAndQDPIssuerNames" value="{{bothdata}}">
								   		{{bothdata}}
								  	</option>
								  </optgroup>
							  </c:if>
							  <optgroup label="QHP only">
							    <option ng-repeat="qhpdata in QHPIssuerNames track by $index" value="{{qhpdata}}">
							   		{{qhpdata}}
							  	</option>
							  </optgroup>
							  <optgroup label="QDP only">
							    <option ng-repeat="qdpdata in QDPIssuerNames" value="{{qdpdata}}">
							   		{{qdpdata}}
							  	</option>
							  </optgroup>
							  </select>
							</div>
						  </div>
						</div>
						<div class="row-fluid">
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="currentStatus" class="bs3-control-label-left"><spring:message code="label.agent.individual.currentStatus"/></label>
							  <select type="text" class="input-full-width" placeholder="" id="currentStatus" name="currentStatus"	 ng-model="filters.currentStatus" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Current Status', 1]);">
								  <option value="">Select Current Status</option>
								  <c:forEach var="currentStatus" items="${currentStatusList}">
									 <option value="${currentStatus}">${currentStatus}</option>
								  </c:forEach>
							  </select>
							  </div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							  <div class="form-group">
									<label for="nextStep" class="bs3-control-label-left"><spring:message code="label.agent.individual.nextSteps"/></label>
								<select class="input-full-width" name="nextStep" id="nextStep"  ng-model="filters.nextStep" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Next Steps', 1]);">
									<option value="">None</option>
									<c:forEach var="nextSteps" items="${nextStepList}">
									 	<option value="${nextSteps}">${nextSteps}</option>
								    </c:forEach>
								</select>
							  </div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								 <c:choose>
								   		<c:when test="${CA_STATE_CODE}">
								   			<label for="enrollmentStatus" class="control-label"><spring:message code="label.agent.individual.enrollmentStatus"/> </label>
											<select class="form-control" id="enrollmentStatus" name="enrollmentStatus" ng-model="filters.enrollmentStatus" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Enrollment Status', 1]);">
												<option value="">Select Enrollment Status</option>
												<c:forEach var="enrollmentStatus" items="${enrollmentStatuslist}">
													<option  value="${enrollmentStatus}">${enrollmentStatus}</option>
												</c:forEach>
											</select>
								   		</c:when>
										<c:otherwise>
											<label for="dueDate" class="control-label"><spring:message code="label.agent.individual.dueDate"/> </label>
											<select class="form-control" id="dueDate" name="dueDate" ng-model="filters.dueDate">
												<option value="">Select Due Date</option>
												<option value="This Week">This Week</option>
												<option value="Next Week">Next Week</option>
											</select>
										</c:otherwise>
								 </c:choose>
							</div>
						  </div>
						  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-3">
							<div class="form-group">
								<label for="coverageYear" class="bs3-control-label-left"><spring:message code="label.agent.individual.applicationYear"/> </label>
								<select class="input-full-width" id="coverageYear" name="coverageYear"  ng-model="filters.coverageYear" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'onFocus', 'Search Field Application Year', 1]);">
									<option value="">Select Year</option>
									<c:forEach var="coverageYear" items="${coverageYearList}">
										 <option value="${coverageYear}">${coverageYear}</option>
								    </c:forEach>
								</select>
							</div>
						  </div>
						</div>
						<div class="row-fluid">
						  	<div class="span12"> <button type="submit" class="btn btn-default btn-primary pull-right" ng-click="applyFilter()" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Search Go', 1]);"><spring:message code="label.search.button.go"/></button></div>
						</div>
					</form>
			  </div>
	       </div>
		</div>
		<div class="dataSortContainer" ng-controller="searchSortingCtrl">
                <div>
                	<c:choose>
				   		<c:when test="${ID_STATE_CODE || NV_STATE_CODE}">
				   			<a id="exportCSVLink"  href="#" class="pull-right offset1" ng-click="exportExcelConfirmation($event)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Export BOB', 1]);" style=" font-size: 13px;"><spring:message code="label.agent.individual.exportAsExcel"/></a>
				   		</c:when>
						<c:otherwise>
							<a id="exportCSVLink" name="PREVENT_DIRECT_HIT" ng-click="exportExcelConfirmation($event)" href="<c:url value='${bobDownloadUrl}'/>" class="pull-right offset1" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Export BOB', 1]);" style=" font-size: 13px;"><spring:message code="label.agent.individual.exportAsExcel"/></a>
						</c:otherwise>
					 </c:choose>
                </div>
                <div class="row-fluid">
	                	<form id="sortDataForm">
								<div class="span6 pull-right">
								   	<div class="control-group pull-right noMargin">
								   		<select name="sortData" id="sortDataDD" class="form-control noMargin" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'SortBy', 1]);">
									   		<c:choose>
										   		<c:when test="${ID_STATE_CODE || NV_STATE_CODE}">
													<option value="DUE_DATE_ASC" selected>Due Date (first due)</option>
									   				<option value="DUE_DATE_DESC">Due Date (last due)</option>
										   			<option value="FIRST_NAME_ASC">First Name A-Z </option>
										   		</c:when>
												<c:otherwise>
										   			<option value="FIRST_NAME_ASC" selected>First Name A-Z </option>
												</c:otherwise>
											 </c:choose>
											 <option value="FIRST_NAME_DESC">First Name Z-A </i></option>
								   			 <option value="LAST_NAME_ASC">Last Name A-Z </option>
								   			 <option value="LAST_NAME_DESC">Last Name Z-A </option>
										</select>
									</div>
									<label for="sortDataDD" class="control-label pull-right" style="margin-top:5px"><spring:message code="label.agent.sortBy"/>: </label>
							   </div>
						</form>
				</div>
			<div ng-include="exportExcelConfirmationContainer"></div>
		</div>
		<h3 class="noResultMsg"><spring:message code="label.agent.noRecordsFound"/></h3>
		<div class="gridDataContainer"  ng-show="isDataLoaded">
			<div class="gridHeader">
				<div class="row-fluid">
				  <div class="span1 text-center"> # </div>
				  <div class="span4">
				  	<spring:message code="label.agent.household"/>
				  </div>

				  <div class="span4">
				  	<c:choose>
				   		<c:when test="${CA_STATE_CODE}">
				   			<spring:message code="label.broker.case"/>
				   		</c:when>
						<c:otherwise>
							<spring:message code="label.agent.householdStatus"/>
						</c:otherwise>
			 		</c:choose>
			 	  </div>
				  <div class="span3"> <spring:message code="label.agent.householdCoverage"/>	  </div>
				</div>
                </div>
                <!--  gridHeader -->
			<div class="gridBody">
				<div class="gridRow" ng-repeat="data in contacts | filter: searchFilter" style="position:relative">
					<div class="row-fluid rowHead">
					  <div class="span1 text-center"> {{$index+1}}  </div>
					  <div class="span4"> <span class="labelValue">
                                <a ng-click="switchRole(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Name', 1]);">{{data.firstName | uppercase}}  {{data.lastName | uppercase}} </a></span>
                      </div>
                      <c:if test="${CA_STATE_CODE}">
                      	<div class="span4">
					  		<div ng-if="data.caseNumber"><spring:message code="label.broker.caseid"/>: <span class="labelValue">{{data.caseNumber}}</span></div>
					  	</div>
                      </c:if>

					  <div class="span3">
					  	<div class="row-fluid">
					  		<c:if test="${CA_STATE_CODE}">
					  			<div class="span6">
						  			<div ng-if="data.isEnrolled=='true'">
								 		<img ng-src="{{data.issuerLogo}}" alt="" width="100px">
								 	</div>
					  			</div>
					  		</c:if>

					  		<div class="span6">
					  			<div style="width:320px;"><span class="labelValue">{{data.issuerName}}</span></div>
					  			<div style="width:320px;"><span class="labelValue">{{data.planName}}</span></div>
						  		<div><span class="labelValue">{{data.planType}}</span></div>
						  		<div ng-if="data.premium"><span class="labelValue">{{data.premium | currency}}/month</span></div>

					  		</div>
					  	</div>

					  </div>
					</div>
					<div class="row-fluid" style="margin-bottom:10px">
					  <div class="col-sm-1 col-md-1 col-lg-1"> &nbsp; </div>
					  <div class="col-sm-4 col-md-4 col-lg-4">
					  	<table>
					  		<tr>
					  			<td><spring:message code="label.agent.householdPhone"/>:</td>
					  			<td><span class="labelValue">{{data.phoneNumber}}</span> </td>
					  		</tr>
					  		<tr class="email">
					  			<td><spring:message code="label.agent.householdEmail"/>:</td>
					  			<td><span title="{{data.emailAddress}}" class="labelValue">{{data.emailAddress}} </span></td>
					  		</tr>
					  		<tr  ng-if="data.zipCode">
					  			<td><spring:message code="label.IndividualAddress"/>:</td>
					  			<td><span class="labelValue"> {{data.address1}} {{data.address2}}<br />
					  			{{data.city}} {{data.state}} {{data.zipCode}}
                                            </span>
                                        </td>
					  		</tr>

					  	</table>
					  </div>
					  <div class="col-sm-4 col-md-4 col-lg-4">
					  	<table>
					  		<tr ng-if="data.applicationType">
					  			<td><spring:message code="label.agent.individual.applicationType"/>: </td>
					  			<td><span class="labelValue">{{data.applicationType}}</span> </td>
					  		</tr>
					  		<tr>
					  			<td><spring:message code="label.agent.individual.applicationYear"/>: </td>
					  			<c:choose>
					  				<c:when test="${CA_STATE_CODE}">
					  					<td><span class="labelValue">{{data.applicationYear}} </span></td>
					  				</c:when>
					  				<c:otherwise>
					  					<td><span class="labelValue">{{data.coverageYear}} </span></td>
					  				</c:otherwise>
					  			</c:choose>
					  		</tr>
					  		<tr>
					  			<td><spring:message code="label.agent.individual.currentStatus"/>: </td>
					  			<td><span class="labelValue">{{data.currentStatus}} </span></td>
					  		</tr>
					  		<tr ng-if="data.nextStep!='N/A'">
					  			<td><spring:message code="label.agent.individual.nextSteps"/>: </td>
					  			<td><span class="labelValue"><a ng-click="switchRole(data)">{{data.nextStep}} </a></span> </td>
					  		</tr>
					  		<tr ng-if="data.dueDate && data.dueDate!='N/A'">
					  			<td><spring:message code="label.agent.individual.dueDate"/>: </td>
					  			<td><span class="labelValue">{{data.dueDate}}</span> </td>
					  		</tr>
					  	</table>
					  </div>
					  <div class="col-sm-3 col-md-3 col-lg-3">
					  	<table  ng-if="data.isEnrolled=='true'">
					  		<tr>
					  			<td><spring:message code="label.agent.individual.officeVisit"/>: </td>
					  			<td><span class="labelValue">{{data.officeVisit}}</span> </td>
					  		</tr>
					  		<tr>
					  			<td><spring:message code="label.agent.individual.genericDrugs"/>: </td>
					  			<td><span class="labelValue">{{data.genericDrugs}} </span></td>
					  		</tr>
					  		<tr>
					  			<td><spring:message code="label.agent.individual.deductible"/>: </td>
					  			<td><span class="labelValue">{{data.deductible}}</span> </td>
					  		</tr>
					  	</table>

					  </div>
					</div>
					<div class="row-fluid">
					  	<div class="actionListContainer">
							<div aria-label="Additional Case Details" role="group" class="btn-group dataActionLinks">
								<c:choose>
                                     <c:when test="${CA_STATE_CODE}">
                                     		<button class="btn" type="button"  ng-click="switchRole(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Account', 1]);" ><i class="icon-user"></i>  <spring:message code="label.agent.individual.account"/></button>
                                            <button class="btn" type="button" ng-click="showHouseHoldInfo(data)"  onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Household', 1]);" ><i class="icon-home"></i>  <spring:message code="label.agent.householdLink"/></button>
                                            <button class="btn" type="button" ng-click="showEligiblityInfo(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Eligibility', 1]);"><i class="squareroot" aria-hidden="true">&#x221A;</i>  <spring:message code="label.agent.individual.eligibility"/></button>
                                            <button class="btn" type="button"  ng-click="markInactivePopup(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Mark as Inactive', 1]);"><i class="icon-remove"></i> <spring:message code="label.agent.individual.markAsInactive"/> </button>
                                     </c:when>
                                     <c:otherwise>
                                            <button class="btn" type="button"  ng-click="switchRole(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Account', 1]);" ><i class="icon-user"></i>  <spring:message code="label.agent.individual.account"/></button>
                                            <button class="btn" type="button" ng-click="showHouseHoldInfo(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Household', 1]);" ><i class="icon-home"></i>  <spring:message code="label.agent.householdLink"/></button>
                                            <button class="btn" type="button" ng-click="showEligiblityInfo(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Eligibility', 1]);"><i class="squareroot" aria-hidden="true">&#x221A;</i><spring:message code="label.agent.individual.eligibility"/></button>
                                            <button class="btn" type="button" ng-click="commentPopup(data)"><i class="icon-comment"></i>  <spring:message code="label.agent.individual.comments"/></button>
                                            <button class="btn" type="button" ng-click="resendActivationEmail(data)"><i class="icon-envelope"></i>  <spring:message code="label.agent.individual.resendActivationEmail"/></button>
                                            <button class="btn" type="button"  ng-click="markInactivePopup(data)" onClick="triggerGoogleTrackingEvent(['_trackEvent', 'Agent BOB Clicks', 'Click', 'Individual Mark as Inactive', 1]);"><i class="icon-remove"></i> <spring:message code="label.agent.individual.markAsInactive"/> </button>
                                            <button ng-if="data.isUserExists" class="btn btn-default btn-primary" type="button"  ng-click="createTicketPopup(data)"  ><i class="icon-pencil"></i> <spring:message code="label.agent.individual.contact"/> <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME)%> </button>
                                     </c:otherwise>
                              </c:choose>
							</div>
						</div>
					</div>

				</div>
                </div>
                <!-- gridBody -->
				<div class="lazyLoader">

			          <div class="gpsoff ">
			          <div class="small loaderMsg"><spring:message code="label.agent.pleaseWait"/> . . .</div>
				  		<div class="ball"></div>
				  		<div class="ball1"></div>
			  		</div>
				</div>
			</div>
            <!-- end of grid data container controller -->
		<div class="loader">

	          <div class="gpsoff ">
	          <div class="small loaderMsg"><spring:message code="label.agent.pleaseWait"/> . . .</div>
		  		<div class="ball"></div>
		  		<div class="ball1"></div>
	  		</div>
		</div>


		<div ng-include="modalContainer"></div>
		<div ng-include="contactExchangeContainer"></div>
		<div ng-include="ticketSuccessContainer"></div>
		<div ng-include="accountAlreadyCreated"></div>
		<div ng-include="resendActivationEmailToInd"></div>
		<div ng-include="resendActEmailSuccess"></div>
		<div ng-include="markAsInActiveContainer"></div>

		<div>
				<c:choose>
			   		<c:when test="${CA_STATE_CODE}">
			   			 <form action="<c:url value='${switchAccUrl}&_pageLabel=individualHomePage&ahbxUserType=1&recordId=${encryptedRecordId}&newAppInd=N&lang='/><spring:message  code="label.language.${lang}"/>"  id="switchForm" name="switchForm" method="get" >
						 </form>
			   		</c:when>
					<c:otherwise>
						 <form action="<c:url value='/broker/individual/dashboard'/>" id="switchForm" name="switchForm" method="post" >
							<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
							 <input id="csrftoken" name="csrftoken" type="hidden" value="${sessionScope.csrftoken}"/>
							<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="{{modalContactData.encryptedIndividualId}}" />
							<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="{{modalContactData.firstName + ' ' + modalContactData.lastName}}" />
							<input type="hidden" name="checkIndividualView" id="checkIndividualView" value="N" />
						 </form>
					</c:otherwise>
			 	</c:choose>
		</div>

		 <script id="switchModalConfirmation" type="text/ng-template" >
			<div id="viewindModal" class="modal hide fade" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
	<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-dismiss="modal" >&times;</button>
				<h3 id="myModalLabel">
					<spring:message code="label.indViewIndividualAccount"/>
				</h3>
	</div>
	 <div class="modal-body">
    	<p>
					<spring:message code="label.indClickingIndividualView"/>
					{{modalContactData.firstName}}  {{modalContactData.lastName}}.
					<spring:message code="label.IndThroughThisPortal"/>
				</p>
				<p>
					<spring:message code="label.indProceedtoIndividualview"/>
				</p>
	  </div>

				<div class="modal-footer clearfix">
				<form action="<c:url value='/broker/individual/dashboard'/>" id="frmPopup" name="frmPopup" method="post">
				<div class="span6">
				<input id="csrftoken" name="csrftoken" type="hidden" value="${sessionScope.csrftoken}"/>
					<label for="showPopupInFuture" class="aria-hidden">Show pop up in future checkbox</label> <!-- Accessibility Use -->
					<input class="pull-left pull-left-small-devices"  type="checkbox" id="checkIndividualView" name="checkIndividualView"  value="N"  />
						<div class="pull-left pull-left-small-devices">
						&nbsp;
						<spring:message code="label.employee.dontshow"/>
					</div>
				</div>
				<div class="span6 pull-left pull-left-small-devices">
					<button class="btn btn" data-dismiss="modal" type="button"  aria-hidden="true">
						<spring:message code="label.brkCancel"/>
					</button>
					<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
					<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="{{modalContactData.encryptedIndividualId}}" />
					<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="{{modalContactData.firstName + ' ' + modalContactData.lastName}}" />

					<c:choose>
						<c:when test="${CA_STATE_CODE}">
							<button class="btn btn-primary" type="button" ng-click="redirectToCASwitch(modalContactData)" >
								<spring:message code="label.indIndividualView"/>
							</button >
						</c:when>
						<c:otherwise>
							<button class="btn btn-primary" type="submit" >
								<spring:message code="label.indIndividualView"/>
							</button >
						</c:otherwise>
					</c:choose>

				</div>

				</form>
				</div>

      </div>
	</script>

	 <script id="markAsInActiveModalConfirmation" type="text/ng-template" >
			<div id="viewIndmarkAsInActiveModal" class="modal hide fade" tabindex="-1"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" >
				<div class="modal-header">
					<button type="button" class="close" aria-hidden="true" data-dismiss="modal" >&times;</button>
					<h3 id="myModalLabel">
						<spring:message code="label.broker.pleaseConfirm"/>
					</h3>
				</div>
	 			<div class="modal-body">
				<p>
				<c:choose>
			   		<c:when test="${CA_STATE_CODE}">
			   			 	<spring:message code="label.broker.markAsInActivetext1"/>
            				{{modalContactData.firstName}}  {{modalContactData.lastName}}
							<spring:message code="label.broker.markAsInActivetext2"/>

							<br /><br />
            				<spring:message code="label.broker.markAsInActivetext3"/>
			   		</c:when>
					<c:otherwise>
							<spring:message code="label.broker.markAsInActivetext1"/>
            				{{modalContactData.firstName}}  {{modalContactData.lastName}}
            				<spring:message code="label.broker.markAsInActivetext2"/>
            				{{modalContactData.firstName}}  {{modalContactData.lastName}}
           					 <spring:message code="label.broker.markAsInActivetext3"/>

 							<br />
            				<spring:message code="label.broker.clickConfirm"/>
			 				<b><spring:message code="label.broker.clickConfirmOne"/></b>
 			 				<spring:message code="label.broker.clickConfirmTwo"/>
            				{{modalContactData.firstName}}  {{modalContactData.lastName}}.
					</c:otherwise>
			 	</c:choose>
				</p>
	 		 	</div>

				<div class="modal-footer clearfix">
					<button class="btn btn" data-dismiss="modal" type="button"  aria-hidden="true">
						<spring:message code="label.brkCancel"/>
					</button>
					<button class="btn btn-primary" type="submit" ng-click="markInactive($event,modalContactData)" >
							<spring:message code="label.brkConfirm"/>
					</button>
				</div>

      		</div>
	</script>


	<!-- resend Activation Email info popup -->
            <jsp:include page="resendActivationEmail.jsp">
                <jsp:param value="" name=""/>
            </jsp:include>
	<!-- Household info popup -->
            <jsp:include page="householdInfo.jsp">
                <jsp:param value="" name=""/>
            </jsp:include>
	<!-- resend Activation Email info popup -->
            <jsp:include page="createTicketForIndByAgent.jsp">
                <jsp:param value="" name=""/>
            </jsp:include>
            <jsp:include page="eligibilityInfo.jsp">
                <jsp:param value="" name=""/>
            </jsp:include>
            <jsp:include page="exportConfirmation.jsp"/>
</div>
</div>
</div>
