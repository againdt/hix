////=========================================================================
////                  // Configurations: START //
////=========================================================================
//// Configuring $translateProvider
//newMexico.config(['$translateProvider', function ($translateProvider) {
//
//    // Simply register translation table as object hash
//    $translateProvider.translations('english', {
//        'LANGUAGE': 'Spanish',
//        'HEADLINE': 'Introducing ngTranslate',
//        'SUB_HEADLINE': 'Translations for your Angular Apps!',
//        'TEXT': 'This works with every translation id combination.',
//        'ONE': 'The woman eats an apple.',
//        'TWO': 'The horse drinks milk.',
//        'THREE': 'Three',
//        'SELECT_CRITERIA': 'Select Criteria',
//        'EVENT_DATE': 'Event Date',
//        'ADD_DETAILS_OF_YOUR': 'Add details of your',
//        'SPOUSE': 'spouse',
//        'DEPENDENT': 'dependent',
//        'HOUSEHOLD_INFORMATION': 'Household Information',
//        'F_NAME': 'First Name',
//        'M_NAME': 'Middle Name',
//        'L_NAME': 'Last Name',
//        'SUFFIX': 'Suffix',
//        'DOB': 'Date of Birth',
//        'RELATIONSHIP': 'Relationship To (Primary Contact)',
//        'SEEKING_COVERAGE': 'Are You Seeking Coverage?',
//        'SEX': 'Sex?',
//        
//    });
//
//    $translateProvider.translations('spanish', {
//        'LANGUAGE': 'English',
//        'HEADLINE': 'Introduciendo ngTranslate',
//        'SUB_HEADLINE': 'Traducciones de sus Aplicaciones angulares!',
//        'TEXT': 'Esto funciona con todas las combinaciones de id traducción.',
//        'ONE': 'La mujer come una manzana.',
//        'TWO': 'El caballo bebe leche.',
//        'THREE': 'Tres',
//        'SELECT_CRITERIA': 'Seleccione los criterios',
//        'EVENT_DATE': 'Fecha del evento',
//        'ADD_DETAILS_OF_YOUR': 'Añadir detalles de su',
//        'SPOUSE': 'cónyuge',
//        'DEPENDENT': 'depende',
//        'HOUSEHOLD_INFORMATION': 'Información del Hogar',
//        'F_NAME': 'Nombre',
//        'M_NAME': 'Segundo Nombre',
//        'L_NAME': 'Apellido',
//        'SUFFIX': 'Sufijo',
//        'DOB': 'Fecha de Nacimiento',
//        'RELATIONSHIP': 'Relación con el (contacto principal)',
//        'SEEKING_COVERAGE': '¿Está usted buscando cobertura?',
//        'SEX': '¿Sexo?',
//        
//        
//    });
//
//    $translateProvider.uses('english');
//}]);
////=========================================================================
////                  // Configurations: END //
////=========================================================================