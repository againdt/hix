package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.PlanCsrMultiplier;

public interface ICSRMultiplierRepository extends JpaRepository<PlanCsrMultiplier, Integer>{
	
}
