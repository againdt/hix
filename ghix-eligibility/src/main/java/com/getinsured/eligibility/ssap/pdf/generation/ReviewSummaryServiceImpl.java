package com.getinsured.eligibility.ssap.pdf.generation;

import java.io.*;
import java.math.BigDecimal;
import java.util.*;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.resp.service.EligibilityService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;
import com.jayway.jsonpath.JsonPath;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

@Service("reviewSummaryService")
@DependsOn("dynamicPropertiesUtil")
@Transactional(readOnly = true)

public class ReviewSummaryServiceImpl implements ReviewSummaryService {

  @Autowired
  private EligibilityService eligibilityService;
  @Autowired
  private NoticeService noticeService;
  @Autowired
  private CmrHouseholdRepository cmrHouseholdRepository;
  @Autowired
  private ContentManagementService ecmService;
  @Autowired
  private SsapApplicationRepository ssapApplicationRepository;
  @Autowired
  private ResourceCreator resourceCreator;

  public static final String EMAIL_HEADER_LOCATION = "notificationTemplate/emailTemplateHeader.html";
  public static final String EMAIL_FOOTER_LOCATION = "notificationTemplate/emailTemplateFooter.html";
  public static final String PRIVACY_CONTENT_LOCATION = "notificationTemplate/privacyContent.html";
  public static final String APPEALS_CONTENT_LOCATION = "notificationTemplate/eligibilityAppealsStaticContent.html";
  public static final String HEARING_RIGHTS_LOCATION = "notificationTemplate/HearingRights.html";
  public static final String PRIVACY_CONTENT = "privacyContent";
  public static final String ELIGIBILITY_APPEALS = "eligibilityAppeals";
  public static final String HEARING_RIGHTS = "hearingRights";
  private static final Map<String, String> relations = new HashMap<String, String>() {{
    put("01", "Spouse");
    put("03", "Parent (father or mother)");
    put("04", "Grandparent (grandfather or grandmother)");
    put("05", "Grandchild (grandson or granddaughter)");
    put("06", "Uncle or aunt");
    put("07", "Nephew or niece");
    put("08", "First cousin");
    put("09", "Adopted child (son or daughter)");
    put("10", "Foster child (foster son or foster daughter)");
    put("11", "Son-in-law or daughter-in-law");
    put("12", "Brother-in-law or sister-in-law");
    put("13", "Mother-in-law or father-inlaw");
    put("14", "Sibling (brother or sister)");
    put("15", "Ward");
    put("16", "Stepparent (stepfather or stepmother)");
    put("17", "Stepchild (stepson or stepdaughter)");
    put("18", "Self");
    put("19", "Child (son or daughter)");
    put("23", "Sponsored dependent");
    put("24", "Dependent of a minor dependent");
    put("25", "Former spouse");
    put("26", "Guardian");
    put("31", "Court-appointed guardian");
    put("38", "Collateral dependent");
    put("53", "Domestic partner");
    put("60", "Annuitant");
    put("D2", "Trustee");
    put("G8", "Unspecified relationship");
    put("G9", "Unspecified relative");
    put("03-53", "Parent's domestic partner");
    put("53-19", "Child of domestic partner");


  }};

  @Autowired
  private ApplicationContext appContext;
  @Autowired
  private PreferencesService preferencesService;
  private static final Logger LOGGER = LoggerFactory.getLogger(ReviewSummaryServiceImpl.class);

  @Override
  public ReviewSummaryDTO getApplicationData(String caseNumber) {
    List<SsapApplication> ssapApplications = this.getSsapApplicationRepository().findByCaseNumber(caseNumber);
    List<HouseholdMembersDTO> householdMembersList;
    ReviewSummaryDTO reviewSummaryDTO = new ReviewSummaryDTO();
    List<JSONObject> relationshipList = null;
    SsapApplication ssapReviewSummary = ssapApplications.get(0);
    BigDecimal cmrHousholeId = ssapReviewSummary.getCmrHouseoldId();
    Long Id = ssapReviewSummary.getId();
    String ssapJSON = ssapReviewSummary.getApplicationData();
    String applicationId = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.applicationGuid");
    List<JSONObject> householdMemberList = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.taxHousehold[0].householdMember[*]");
	reviewSummaryDTO.setApplicationId(applicationId);
	for (JSONObject primaryMember : householdMemberList) {
      long personId = (Integer) primaryMember.get("personId");
      if (personId == 1) {
        String householdContact = null;
        String mailingContact = null;
        String fullName = null;
        JSONObject personNameObject = (JSONObject) primaryMember.get("name");
        String firstName = (String) personNameObject.get("firstName");
        String middleName = (String) personNameObject.get("middleName");
        String lastName = (String) personNameObject.get("lastName");
        String suffix = (String) personNameObject.get("suffix");

        if (middleName != null && middleName.trim().length() > 0) {

          fullName = firstName + " " + middleName + " " + lastName;
        } else {
          fullName = firstName + " " + lastName;
        }

        if (suffix != null && suffix.trim().length() > 0) {
          fullName = fullName + "" + suffix;
        }

        String birthdate = (String) primaryMember.get("dateOfBirth");
        JSONObject jsonHouseHoldContact = (JSONObject) primaryMember.get("householdContact");
        JSONObject homeAddress = (JSONObject) jsonHouseHoldContact.get("homeAddress");
        String street1 = (String) homeAddress.get("streetAddress1");
        String street2 = (String) homeAddress.get("streetAddress2");
        String city = (String) homeAddress.get("city");
        String state = (String) homeAddress.get("state");
        String postalCode = (String) homeAddress.get("postalCode");
        String county = (String) homeAddress.get("county");
        if (street1 != null && street1.trim().length() > 0)
          householdContact = street1;
        if (street2 != null && street2.trim().length() > 0)
          householdContact += ", " + street2;
        if (city != null && city.trim().length() > 0)
          householdContact += ", " + city;
        if (state != null && state.trim().length() > 0)
          householdContact += ", " + state;
        if (postalCode != null && postalCode.trim().length() > 0)
          householdContact += ", " + postalCode;
        if (county != null && county.trim().length() > 0)
          householdContact += ", " + county;

        if (jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator") != null && (boolean) jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator")) {
          mailingContact = householdContact;
        } else {
          JSONObject mailingAddress = (JSONObject) jsonHouseHoldContact.get("mailingAddress");
          String mailingstreet1 = (String) mailingAddress.get("streetAddress1");
          String mailingstreet2 = (String) mailingAddress.get("streetAddress2");
          String mailingcity = (String) mailingAddress.get("city");
          String mailingstate = (String) mailingAddress.get("state");
          String mailingpostalCode = (String) mailingAddress.get("postalCode");
          String mailingCounty = (String) mailingAddress.get("county");
          if (mailingstreet1 != null && mailingstreet1.trim().length() > 0)
            mailingContact = mailingstreet1;
          if (mailingstreet2 != null && mailingstreet2.trim().length() > 0)
            mailingContact += ", " + mailingstreet2;
          if (mailingcity != null && mailingcity.trim().length() > 0)
            mailingContact += ", " + mailingcity;
          if (mailingstate != null && mailingstate.trim().length() > 0)
            mailingContact += ", " + mailingstate;
          if (mailingpostalCode != null && mailingpostalCode.trim().length() > 0)
            mailingContact += ", " + mailingpostalCode;
          if (mailingCounty != null && mailingCounty.trim().length() > 0)
            mailingContact += ", " + mailingCounty;
        }
        JSONObject contactPreferences = (JSONObject) jsonHouseHoldContact.get("contactPreferences");
        String preferredContactMethod = (String) contactPreferences.get("preferredContactMethod");
        String email = (String) contactPreferences.get("emailAddress");
        String preferredSpokenLanguage = (String) contactPreferences.get("preferredSpokenLanguage");
        String preferredWrittenLanguage = (String) contactPreferences.get("preferredWrittenLanguage");
        String phoneNumber = (String) ((JSONObject) jsonHouseHoldContact.get("phone")).get("phoneNumber");
        String otherPhoneNumber = (String) ((JSONObject) jsonHouseHoldContact.get("otherPhone")).get("phoneNumber");


        reviewSummaryDTO.setHomeAddress(householdContact);
        reviewSummaryDTO.setFullName(fullName);
        reviewSummaryDTO.setMailingAddress(mailingContact);
        reviewSummaryDTO.setBirthDate(birthdate.substring(0, 12));

        if ((phoneNumber != null && phoneNumber.trim().length() > 0) && (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0)) {
          reviewSummaryDTO.setPhone(phoneNumber + " / " + otherPhoneNumber);

        } else if ((phoneNumber != null && phoneNumber.trim().length() > 0) && (otherPhoneNumber == null || otherPhoneNumber.trim().length() == 0)) {
          reviewSummaryDTO.setPhone(phoneNumber);
        } else if ((phoneNumber == null || phoneNumber.trim().length() == 0) && (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0)) {
          reviewSummaryDTO.setPhone(otherPhoneNumber);
        } else {
          reviewSummaryDTO.setPhone(" ");
        }

        if (phoneNumber != null && phoneNumber.trim().length() > 0) {
          reviewSummaryDTO.setPhoneNumber(phoneNumber + " (Cell)");
        } else {
          reviewSummaryDTO.setPhoneNumber("");
        }
        if (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0) {
          reviewSummaryDTO.setOtherPhone(otherPhoneNumber + " (Home)");
        } else {
          reviewSummaryDTO.setOtherPhone("");
        }
        if (preferredSpokenLanguage != null) {
          reviewSummaryDTO.setPreferredSpokenLanguage(preferredSpokenLanguage);
        } else {
          reviewSummaryDTO.setPreferredSpokenLanguage("");
        }

        if (preferredWrittenLanguage != null) {
          reviewSummaryDTO.setPreferredWrittenLanguage(preferredWrittenLanguage);
        } else {
          reviewSummaryDTO.setPreferredWrittenLanguage("");

        }
        reviewSummaryDTO.setPreferredContactMethod(preferredContactMethod);

        if (email != null) {
          reviewSummaryDTO.setEmail(email);
        } else {
          reviewSummaryDTO.setEmail("");
        }

        reviewSummaryDTO.setCmrHouseholdId(cmrHousholeId);
        reviewSummaryDTO.setPrimaryKey(Id);

        relationshipList = (List<JSONObject>) primaryMember.get("bloodRelationship");


      }


    }

    try {
      householdMembersList = getHouseholdMemberDetails(ssapJSON, relationshipList);
      reviewSummaryDTO.setHouseholdMembersDTO(householdMembersList);
      } catch (Exception e) {
      LOGGER.error("Error Creating HouseholdMember and ReviewSummary DTO" + e);
    }


    return reviewSummaryDTO;

  }

  @Override
  public ReviewSummaryDTO getAppData(String ssapJSON) {
    List<HouseholdMembersDTO> householdMembersList;
    ReviewSummaryDTO reviewSummaryDTO = new ReviewSummaryDTO();
    List<JSONObject> relationshipList = null;
    String applicationId = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.applicationGuid");
    String ssapApplicationId = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.ssapApplicationId");
    List<JSONObject> householdMemberList = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.taxHousehold[0].householdMember[*]");
    for (JSONObject primaryMember : householdMemberList) {
      long personId = (Integer) primaryMember.get("personId");
      if (personId == 1) {
        String householdContact = null;
        String mailingContact = null;
        String fullName = null;
        JSONObject personNameObject = (JSONObject) primaryMember.get("name");
        String firstName = (String) personNameObject.get("firstName");
        String middleName = (String) personNameObject.get("middleName");
        String lastName = (String) personNameObject.get("lastName");
        String suffix = (String) personNameObject.get("suffix");

        if (middleName != null && middleName.trim().length() > 0) {

          fullName = firstName + " " + middleName + " " + lastName;
        } else {
          fullName = firstName + " " + lastName;
        }

        if (suffix != null && suffix.trim().length() > 0) {
          fullName = fullName + "" + suffix;
        }

        String birthdate = (String) primaryMember.get("dateOfBirth");
        JSONObject jsonHouseHoldContact = (JSONObject) primaryMember.get("householdContact");
        JSONObject homeAddress = (JSONObject) jsonHouseHoldContact.get("homeAddress");
        String street1 = (String) homeAddress.get("streetAddress1");
        String street2 = (String) homeAddress.get("streetAddress2");
        String city = (String) homeAddress.get("city");
        String state = (String) homeAddress.get("state");
        String postalCode = (String) homeAddress.get("postalCode");
        if (street1 != null && street1.trim().length() > 0)
          householdContact = street1;
        if (street2 != null && street2.trim().length() > 0)
          householdContact += ", " + street2;
        if (city != null && city.trim().length() > 0)
          householdContact += ", " + city;
        if (state != null && state.trim().length() > 0)
          householdContact += ", " + state;
        if (postalCode != null && postalCode.trim().length() > 0)
          householdContact += ", " + postalCode;

        if (jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator") != null && (boolean) jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator")) {
          mailingContact = householdContact;
        } else {
          JSONObject mailingAddress = (JSONObject) jsonHouseHoldContact.get("mailingAddress");
          String mailingstreet1 = (String) mailingAddress.get("streetAddress1");
          String mailingstreet2 = (String) mailingAddress.get("streetAddress2");
          String mailingcity = (String) mailingAddress.get("city");
          String mailingstate = (String) mailingAddress.get("state");
          String mailingpostalCode = (String) mailingAddress.get("postalCode");
          if (mailingstreet1 != null && mailingstreet1.trim().length() > 0)
            mailingContact = mailingstreet1;
          if (mailingstreet2 != null && mailingstreet2.trim().length() > 0)
            mailingContact += ", " + mailingstreet2;
          if (mailingcity != null && mailingcity.trim().length() > 0)
            mailingContact += ", " + mailingcity;
          if (mailingstate != null && mailingstate.trim().length() > 0)
            mailingContact += ", " + mailingstate;
          if (mailingpostalCode != null && mailingpostalCode.trim().length() > 0)
            mailingContact += ", " + mailingpostalCode;
        }
        JSONObject contactPreferences = (JSONObject) jsonHouseHoldContact.get("contactPreferences");
        String preferredContactMethod = (String) contactPreferences.get("preferredContactMethod");
        String email = (String) contactPreferences.get("emailAddress");
        String preferredSpokenLanguage = (String) contactPreferences.get("preferredSpokenLanguage");
        String preferredWrittenLanguage = (String) contactPreferences.get("preferredWrittenLanguage");
        String phoneNumber = (String) ((JSONObject) jsonHouseHoldContact.get("phone")).get("phoneNumber");
        String otherPhoneNumber = (String) ((JSONObject) jsonHouseHoldContact.get("otherPhone")).get("phoneNumber");


        reviewSummaryDTO.setHomeAddress(householdContact);
        reviewSummaryDTO.setFullName(fullName);
        reviewSummaryDTO.setMailingAddress(mailingContact);
        reviewSummaryDTO.setBirthDate(birthdate.substring(0, 12));

        if ((phoneNumber != null && phoneNumber.trim().length() > 0) && (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0)) {
          reviewSummaryDTO.setPhone(phoneNumber + " / " + otherPhoneNumber);

        } else if ((phoneNumber != null && phoneNumber.trim().length() > 0) && (otherPhoneNumber == null || otherPhoneNumber.trim().length() == 0)) {
          reviewSummaryDTO.setPhone(phoneNumber);
        } else if ((phoneNumber == null || phoneNumber.trim().length() == 0) && (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0)) {
          reviewSummaryDTO.setPhone(otherPhoneNumber);
        } else {
          reviewSummaryDTO.setPhone(" ");
        }

        if (phoneNumber != null && phoneNumber.trim().length() > 0) {
          reviewSummaryDTO.setPhoneNumber(phoneNumber + " (Cell)");
        } else {
          reviewSummaryDTO.setPhoneNumber("");
        }
        if (otherPhoneNumber != null && otherPhoneNumber.trim().length() > 0) {
          reviewSummaryDTO.setOtherPhone(otherPhoneNumber + " (Home)");
        } else {
          reviewSummaryDTO.setOtherPhone("");
        }
        if (preferredSpokenLanguage != null) {
          reviewSummaryDTO.setPreferredSpokenLanguage(preferredSpokenLanguage);
        } else {
          reviewSummaryDTO.setPreferredSpokenLanguage("");
        }

        if (preferredWrittenLanguage != null) {
          reviewSummaryDTO.setPreferredWrittenLanguage(preferredWrittenLanguage);
        } else {
          reviewSummaryDTO.setPreferredWrittenLanguage("");

        }
        reviewSummaryDTO.setPreferredContactMethod(preferredContactMethod);

        if (email != null) {
          reviewSummaryDTO.setEmail(email);
        } else {
          reviewSummaryDTO.setEmail("");
        }


        reviewSummaryDTO.setCmrHouseholdId(getHouseholdId(ssapApplicationId));
        reviewSummaryDTO.setPrimaryKey(0);

        relationshipList = (List<JSONObject>) primaryMember.get("bloodRelationship");


      }


    }

    try {
      householdMembersList = getHouseholdMemberDetails(ssapJSON, relationshipList);
      reviewSummaryDTO.setHouseholdMembersDTO(householdMembersList);
      reviewSummaryDTO.setApplicationId(applicationId);
    } catch (Exception e) {
      LOGGER.error("Error Creating HouseholdMember and ReviewSummary DTO" + e);
    }


    return reviewSummaryDTO;

  }

  private BigDecimal getHouseholdId(String applicationid) {
    if (applicationid != null) {
      SsapApplicationResource ssapApplicationResource = resourceCreator.getSsapApplicationById(Long.valueOf(applicationid));

      return ssapApplicationResource.getCmrHouseoldId();
    } else {
      return null;
    }
  }

  @Override
  public String generateReviewSummaryNotification(String caseNumber) throws Exception {
    ReviewSummaryDTO reviewSummaryDTO = getApplicationData(caseNumber);

    Household household = fetchConsumer(reviewSummaryDTO);
    int moduleId = household.getId();
    String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
    String fullName = household.getFirstName() + " " + household.getLastName();
    List<String> emailIds = new ArrayList<String>();
    PreferencesDTO preferencesDTO = preferencesService.getPreferences(household.getId(), false);
    Location location = getLocationFromDTO(preferencesDTO.getLocationDto());
    emailIds.add(preferencesDTO.getEmailAddress());

    String noticeTemplateName = "ReviewSummary";
    String relativePath = "cmr/" + moduleId + "/ssap/" + reviewSummaryDTO.getPrimaryKey() + "/notifications/";
    String ecmFileName = "ReviewSummary" + moduleId + (new TSDate().getTime()) + ".pdf";


    Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
        getReplaceableObjectData(reviewSummaryDTO),
        relativePath, ecmFileName,
        moduleName, moduleId, emailIds,
        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
        fullName, location, GhixNoticeCommunicationMethod.Email);

    return notice.getEcmId();
  }


  private Location getLocationFromDTO(LocationDTO locationDto) {
    if (locationDto == null) {
      return null;
    }
    Location l = new Location();
    l.setAddress1(locationDto.getAddressLine1());
    l.setAddress2(locationDto.getAddressLine2());
    l.setCity(locationDto.getCity());
    l.setState(locationDto.getState());
    l.setZip(locationDto.getZipcode());
    l.setCounty(locationDto.getCountyName());
    l.setCountycode(locationDto.getCountyCode());
    return l;
  }


  @Override
  public String generateSummaryPDF(String ssapJason) throws Exception {
    ReviewSummaryDTO reviewSummaryDTO = getAppData(ssapJason);

    Household household = fetchConsumer(reviewSummaryDTO);
    int moduleId = household.getId();
    String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
    String fullName = household.getFirstName() + " " + household.getLastName();
    List<String> emailIds = new ArrayList<String>();
    PreferencesDTO preferencesDTO = preferencesService.getPreferences(household.getId(), false);
    Location location = getLocationFromDTO(preferencesDTO.getLocationDto());
    emailIds.add(preferencesDTO.getEmailAddress());

    String noticeTemplateName = "ReviewSummary";
    String relativePath = "cmr/" + moduleId + "/ssap/" + reviewSummaryDTO.getPrimaryKey() + "/notifications/";
    String ecmFileName = "ReviewSummary" + moduleId + (new TSDate().getTime()) + ".pdf";

    Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
        getReplaceableObjectData(reviewSummaryDTO),
        relativePath, ecmFileName,
        moduleName, moduleId, emailIds,
        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
        fullName, location, GhixNoticeCommunicationMethod.Email);

    return notice.getEcmId();
  }


  private Household fetchConsumer(ReviewSummaryDTO reviewSummaryDTO) {

    int cmrId = fetchModuleId(reviewSummaryDTO);
    return cmrHouseholdRepository.findOne(cmrId);
  }

  private int fetchModuleId(ReviewSummaryDTO reviewSummaryDTO) {

    int cmrId = reviewSummaryDTO.getCmrHouseholdId() != null ? reviewSummaryDTO.getCmrHouseholdId().intValue() : 0;

    if (cmrId == 0) {
      throw new GIRuntimeException("Cannot generate notification! CMR Household ID not found for case number - " + reviewSummaryDTO.getCaseNumber());
    }
    return cmrId;
  }


  private Map<String, Object> getReplaceableObjectData(ReviewSummaryDTO reviewSummaryDTO) throws NoticeServiceException {
    Map<String, Object> tokens = new HashMap<String, Object>();
    tokens.put("reviewSummaryDTO", reviewSummaryDTO);
    tokens.put("primaryApplicantName", reviewSummaryDTO.getFullName());
    tokens.put("homeAddress", reviewSummaryDTO.getHomeAddress());
    tokens.put("MailingAddress", reviewSummaryDTO.getMailingAddress());
    tokens.put("DateOfBirth", reviewSummaryDTO.getBirthDate());
    tokens.put("Email", reviewSummaryDTO.getEmail());
    tokens.put("preferredSpokenLanguage", reviewSummaryDTO.getPreferredSpokenLanguage());
    tokens.put("preferredWrittenLanguage", reviewSummaryDTO.getPreferredWrittenLanguage());
    tokens.put("preferredContactMethod", reviewSummaryDTO.getPreferredContactMethod());
    tokens.put("applicationId", reviewSummaryDTO.getApplicationId());
    tokens.put("phoneNumber", reviewSummaryDTO.getPhoneNumber());
    tokens.put("otherPhoneNumber", reviewSummaryDTO.getOtherPhone());
    tokens.put("Phone", reviewSummaryDTO.getPhone());
    tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
    tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
    tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
    tokens.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
    tokens.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
    tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
    tokens.put("exchangeFax", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
    tokens.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
    tokens.put("currentYear", String.valueOf(TSCalendar.getInstance().get(Calendar.YEAR)));
    tokens.put("ssapApplicationId", reviewSummaryDTO.getApplicationId());

    return tokens;
  }

  private String getName(ReviewSummaryDTO reviewSummaryDTO) {
    return reviewSummaryDTO.getFirstName() + " " + reviewSummaryDTO.getLastName();
  }

  private String getTemplateHeaderOrFooter(String templateLocation) throws NoticeServiceException {

    InputStream inputStreamUrl = null;

    try {
      if ("Y".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.USEECMTEMPLATE))) {
        inputStreamUrl = getTemplateFromECM(templateLocation);
      } else {
        Resource resource = appContext.getResource("classpath:" + templateLocation);
        inputStreamUrl = resource.getInputStream();
      }
      return populateHeaderFooterTemplate(IOUtils.toString(inputStreamUrl, "UTF-8"));
    } catch (FileNotFoundException fnfe) {
      LOGGER.error("Error in reading template from:", templateLocation);
      throw new NoticeServiceException(fnfe);
    } catch (IOException ioe) {
      LOGGER.error("Error in reading template from:", templateLocation);
      throw new NoticeServiceException(ioe);
    } catch (Exception e) {
      LOGGER.error("Error while populating the template:", e);
      throw new NoticeServiceException(e);
    } finally {
      IOUtils.closeQuietly(inputStreamUrl);
    }

  }

  private InputStream getTemplateFromECM(String location) throws ContentManagementServiceException {
    String ecmTemplateFolderPath = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ECMTEMPLATEFOLDERPATH);
    return new ByteArrayInputStream(ecmService.getContentDataByPath(ecmTemplateFolderPath + location));
  }


  private String populateHeaderFooterTemplate(String templateContent) throws NoticeServiceException {
    Map<String, Object> replaceableObj = new HashMap<String, Object>();
    replaceableObj.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
    replaceableObj.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
    replaceableObj.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
    replaceableObj.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
    replaceableObj.put(TemplateTokens.CITY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
    replaceableObj.put(TemplateTokens.PIN_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
    replaceableObj.put("exchangeFax", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
    replaceableObj.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
    replaceableObj.put(TemplateTokens.HOST, GhixEndPoints.GHIXWEB_SERVICE_URL);

    StringTemplateLoader stringLoader = new StringTemplateLoader();
    Configuration templateConfig = new Configuration();
    StringWriter sw = new StringWriter();

    try {
      stringLoader.putTemplate("noticeTemplate", templateContent);
      templateConfig.setTemplateLoader(stringLoader);
      Template tmpl = templateConfig.getTemplate("noticeTemplate");
      tmpl.process(replaceableObj, sw);

    } catch (Exception e) {
      LOGGER.error("Exception found while populating Header Template", e);
      throw new NoticeServiceException(e);
    } finally {
      IOUtils.closeQuietly(sw);
    }
    return sw.toString();
  }

  private List<HouseholdMembersDTO> getHouseholdMemberDetails(String ssapJSON, List<JSONObject> relationshipList) {

    List<JSONObject> householdMemberList = JsonPath.read(ssapJSON, "$.singleStreamlinedApplication.taxHousehold[0].householdMember[*]");
    List<HouseholdMembersDTO> householdMemberDTO = new ArrayList<HouseholdMembersDTO>();
    JSONObject application = (JSONObject) JsonPath.read(ssapJSON, "singleStreamlinedApplication");
    JSONArray taxDependants = (JSONArray) application.get("taxFilerDependants");


    for (JSONObject householdMember : householdMemberList) {

      if (householdMember.get("active") != null && !(boolean) householdMember.get("active")) {
        break;
      }

      HouseholdMembersDTO member = new HouseholdMembersDTO();
      String nameOnSsn;
      String Name = null;
      String homecontactAddress = null;
      int i = 0;
      JSONObject personNameObject = (JSONObject) householdMember.get("name");
      JSONObject jsonHouseHoldContact = (JSONObject) householdMember.get("householdContact");
      JSONObject homeAddress = (JSONObject) jsonHouseHoldContact.get("homeAddress");
      JSONObject jsonotherAddress = (JSONObject) householdMember.get("otherAddress");
      JSONObject otherAddress = (JSONObject) jsonotherAddress.get("address");
      JSONObject socialSecurityCard = (JSONObject) householdMember.get("socialSecurityCard");
      JSONObject immigrationStatus = (JSONObject) householdMember.get("citizenshipImmigrationStatus");
      JSONObject healthCoverage = (JSONObject) householdMember.get("healthCoverage");
      JSONObject specialCircumstances = (JSONObject) householdMember.get("specialCircumstances");
      JSONObject medicaidInsurance = (JSONObject) healthCoverage.get("medicaidInsurance");
      long personId = (Integer) householdMember.get("personId");
      JSONObject detailedIncome = (JSONObject) householdMember.get("detailedIncome");


      String streetAddress1 = null;
      String streetAddress2 = null;
      String city = null;
      String state = null;
      String zip = null;
      String county = null;

      if (householdMember.get("livesAtOtherAddressIndicator") != null && (boolean) householdMember.get("livesAtOtherAddressIndicator")) {
        streetAddress1 = (String) otherAddress.get("streetAddress1");
        streetAddress2 = (String) otherAddress.get("streetAddress2");
        city = (String) otherAddress.get("city");
        state = (String) otherAddress.get("state");
        zip = ((String) otherAddress.get("postalCode")).toString();
        county = ((String) otherAddress.get("county"));
      } else {

        streetAddress1 = (String) homeAddress.get("streetAddress1");
        streetAddress2 = (String) homeAddress.get("streetAddress2");
        city = (String) homeAddress.get("city");
        state = (String) homeAddress.get("state");
        zip = ((String) homeAddress.get("postalCode"));
        county = ((String) homeAddress.get("county"));
      }
      String firstName = (String) personNameObject.get("firstName");
      String middleName = (String) personNameObject.get("middleName");
      String lastName = (String) personNameObject.get("lastName");
      String suffix = (String) personNameObject.get("suffix");
      String ssn = (String) (((JSONObject) householdMember.get("socialSecurityCard")).get("socialSecurityNumber"));

      if (middleName != null && middleName.trim().length() > 0) {
        Name = firstName + " " + middleName + " " + lastName;
      } else {
        Name = firstName + " " + lastName;
      }
      if (suffix != null) {
        Name = Name + " " + suffix;
      }


      if (streetAddress1 != null && streetAddress1.trim().length() > 0) {
        homecontactAddress = streetAddress1;
      }

      if (streetAddress2 != null && streetAddress2.trim().length() > 0) {
        homecontactAddress = homecontactAddress + ", " + streetAddress2;
      }

      if (city != null && city.trim().length() > 0) {
        homecontactAddress = homecontactAddress + ", " + city;
      }

      if (state != null && state.trim().length() > 0) {
        homecontactAddress = homecontactAddress + ", " + state;

      }

      if (zip != null && zip.trim().length() > 0) {
        homecontactAddress = homecontactAddress + ", " + zip;
      }

      if (county != null && county.trim().length() > 0) {
        homecontactAddress = homecontactAddress + ", " + county;
      }

	  if (socialSecurityCard.get("nameSameOnSSNCardIndicator") != null 
	  	&& Boolean.parseBoolean(String.valueOf( socialSecurityCard.get("nameSameOnSSNCardIndicator")))) {

        nameOnSsn = Name;
      } else {
        nameOnSsn = (String) (((JSONObject) householdMember.get("socialSecurityCard")).get("firstNameOnSSNCard")) + " " +
            (String) (((JSONObject) householdMember.get("socialSecurityCard")).get("middleNameOnSSNCard")) + " " +
            (String) (((JSONObject) householdMember.get("socialSecurityCard")).get("lastNameOnSSNCard"));
        String suffixOnSSN = (String) (((JSONObject) householdMember.get("socialSecurityCard")).get("suffixOnSSNCard"));

        if (suffixOnSSN != null) {
          nameOnSsn = nameOnSsn + " " + suffixOnSSN;

        }
      }


      member.setFullName(Name);
      member.setGender((String) householdMember.get("gender") != null ? (String) householdMember.get("gender") : "");

      if (householdMember.get("planToFileFTRIndicator") != null && (boolean) householdMember.get("planToFileFTRIndicator")) {
        member.setPlanToFileFTRIndicator((String) householdMember.get("planToFileFTRIndicator"));

        if (householdMember.get("marriedIndicator") != null && (boolean) householdMember.get("marriedIndicator")) {
          String spouseId = (String) (((JSONObject) householdMember.get("taxFiler")).get("spouseHouseholdMemberId"));

          if (spouseId != "" && spouseId != null) {
            JSONObject householdMemberSpouseObj = householdMemberList.get(Integer.valueOf(spouseId) - 1);

            String spouseInformation = (String) (((JSONObject) householdMemberSpouseObj.get("name")).get("firstName")) + " " +
                (String) (((JSONObject) householdMemberSpouseObj.get("name")).get("middleName")) + " " +
                (String) (((JSONObject) householdMemberSpouseObj.get("name")).get("lastName"));


            member.setSpouseInformation("Yes, Jointly with" + spouseInformation);
            member.setTaxdependentApplyingfor(String.valueOf(taxDependants.size()));
          }
        } else {
          member.setSpouseInformation("No");
          member.setTaxdependentApplyingfor("None");

        }

      } else {
        member.setSpouseInformation("No");
        member.setTaxdependentApplyingfor("None");
        member.setPlanToFileFTRIndicator("");
      }

      //if(taxDependants.size()!=null && )

      if (householdMember.get("applyingForCoverageIndicator") != null && (boolean) householdMember.get("applyingForCoverageIndicator")) {

        member.setApplyingForCoverageIndicator("Yes");
      } else {
        member.setApplyingForCoverageIndicator("No");

      }
      if (homecontactAddress != null && homecontactAddress.trim().length() > 0) {
        member.setHomeAddress(homecontactAddress);
      } else {
        member.setHomeAddress("");

      }
      if (jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator") != null && (boolean) jsonHouseHoldContact.get("mailingAddressSameAsHomeAddressIndicator")) {


        member.setMailingAddress("Same As home address");
      } else {

        JSONObject mailingAddress = (JSONObject) jsonHouseHoldContact.get("mailingAddress");
        String mailingcontactAddress = null;
        String mailingStreetAddress1 = (String) mailingAddress.get("streetAddress1");
        String mailingStreetAddress2 = (String) mailingAddress.get("streetAddress2");
        String mailingCity = (String) mailingAddress.get("city");
        String mailingState = (String) mailingAddress.get("state");
        String mailingZip = ((String) mailingAddress.get("postalCode")).toString();
        String mailingCounty = ((String) mailingAddress.get("county"));

        if (mailingStreetAddress1 != null && mailingStreetAddress1.trim().length() > 0) {
          mailingcontactAddress = mailingStreetAddress1;
        }

        if (mailingStreetAddress2 != null && mailingStreetAddress2.trim().length() > 0) {
          mailingcontactAddress = mailingcontactAddress + ", " + mailingStreetAddress2;
        }

        if (mailingCity != null && mailingCity.trim().length() > 0) {
          mailingcontactAddress = mailingcontactAddress + ", " + mailingCity;
        }

        if (mailingState != null && mailingState.trim().length() > 0) {
          mailingcontactAddress = mailingcontactAddress + ", " + mailingState;

        }

        if (mailingZip != null && mailingZip.trim().length() > 0) {
          mailingcontactAddress = mailingcontactAddress + ", " + mailingZip;
        }

        if (mailingCounty != null && mailingCounty.trim().length() > 0) {
          mailingcontactAddress = mailingcontactAddress + ", " + mailingCounty;
        }


        if (mailingcontactAddress != null && mailingcontactAddress.trim().length() > 0) {
          member.setMailingAddress(mailingcontactAddress);
        } else {
          member.setMailingAddress("");
        }
      }
      if (ssn != null && ssn.trim().length() > 0) {
        member.setSsn("***-**-" + ssn.substring(5));
      } else {
        member.setSsn("");
      }

      if (immigrationStatus.get("citizenshipStatusIndicator") != null && (boolean) immigrationStatus.get("citizenshipStatusIndicator")) {
        member.setCitizenshipStatusIndicator("Yes");
      } else {
        member.setCitizenshipStatusIndicator("No");

      }

      String dateOfBirth = householdMember.get("dateOfBirth").toString().substring(0, 12);
      member.setNameOnSsnCard(nameOnSsn);
      member.setDateOfBirth(dateOfBirth);


      if (specialCircumstances.get("americanIndianAlaskaNativeIndicator") != null && (boolean) specialCircumstances.get("americanIndianAlaskaNativeIndicator")) {

        member.setAmericanIndianAlaskaNativeIndicator("Yes");
      } else {
        member.setAmericanIndianAlaskaNativeIndicator("No");

      }
      if (specialCircumstances.get("pregnantIndicator") != null && (boolean) specialCircumstances.get("pregnantIndicator")) {


        member.setPregnantIndicator("Yes");
      } else {
        member.setPregnantIndicator("No");


      }

      if (medicaidInsurance.get("requestHelpPayingMedicalBillsLast3MonthsIndicator") != null && (boolean) medicaidInsurance.get("requestHelpPayingMedicalBillsLast3MonthsIndicator")) {

        member.setRequestHelpPayingMedicalBillsLast3MonthsIndicator("Yes");
      } else {
        member.setRequestHelpPayingMedicalBillsLast3MonthsIndicator("No");

      }

      if (householdMember.get("disabilityIndicator") != null && (boolean) householdMember.get("disabilityIndicator")) {

        member.setDisabilityIndicator("Yes");
      } else {
        member.setDisabilityIndicator("No");
      }

      if (relationshipList != null) {
        String relationshipWithPrimary = getRelationshipWithPrimary(personId, relationshipList);
        if (relationshipWithPrimary != null) {
          member.setRelationshipWithPrimary(relationshipWithPrimary);
        } else {
          member.setRelationshipWithPrimary("None");

        }
      }

      householdMemberDTO.add(member);
      //getFinancialDetailsSummary(detailedIncome);
      i++;


    }


    return householdMemberDTO;
  }


  private String getRelationshipWithPrimary(long personId, List<JSONObject> relationshipList) {

    for (JSONObject relation : relationshipList) {

      String IndividualPersonId = ((String) relation.get("individualPersonId"));

      if (IndividualPersonId != null && IndividualPersonId.equals(Long.toString(personId)) && ((String) relation.get("relatedPersonId")).equals("1")) {

        return getRelashipText((String) relation.get("relation"));
      }
    }
    // TODO Auto-generated method stub
    return null;
  }

  private String getRelashipText(String relationshipCode) {

    if (relationshipCode != null) {


      String text = relations.get(relationshipCode);

      return text;

    }

    // TODO Auto-generated method stub
    return null;
  }

  public SsapApplicationRepository getSsapApplicationRepository() {
    return ssapApplicationRepository;
  }

  public void setSsapApplicationRepository(SsapApplicationRepository ssapApplicationRepository) {
    this.ssapApplicationRepository = ssapApplicationRepository;
  }

}
