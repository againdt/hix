<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="announcement" uri="/WEB-INF/tld/announcement-view.tld" %>
 <script type="text/javascript" src="<c:url value="/resources/js/bootstrap-twipsy.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/verticaltabs.pack.js" />"></script>
<%
Broker mybroker = (Broker)request.getAttribute("broker");
Date currentdate = new Date();
//if certification date is null, then renew button need not be shown, thus, initializing diffDays with 40 any number which is greater than 30
long diffDays = 40;
long isProfileExists = 0;
if(mybroker != null)
{
 isProfileExists = mybroker.getId();
}
Calendar calUpdated = Calendar.getInstance();
if(mybroker.getReCertificationDate()!=null){
calUpdated.set(mybroker.getReCertificationDate().getYear()+1900, mybroker.getReCertificationDate().getMonth(),  mybroker.getReCertificationDate().getDate());
long currentDateMilliSec = currentdate.getTime();
long updateDateMilliSec = calUpdated.getTimeInMillis();
diffDays = (updateDateMilliSec - currentDateMilliSec) / (24 * 60 * 60 * 1000);
// System.out.println("test differ days "+diffDays);
}
%>

<script>
$(document).ready(function() {
	var updatedProfile = '${updatedProfile}';
	<% request.getSession().setAttribute("updatedProfile", "false");  %>
	if ('true' == updatedProfile) {
	var href = "/hix/broker/profileupdated";
	$(
		'<div id="success" class="modal"><div class="modal-header" id="header" style="border-bottom:0;"><h4 class="pull-left margin0"><spring:message code="label.brkregcomplete"/></h4><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body"><iframe id="success" src="'+ href
				+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:240px;"></iframe></div></div>').modal();
	}

	$("li#brkCertStatus").addClass("active");
	$("a[title='My Information']").parent().addClass("active");
});

function closeSuccess() {
	$("#success").remove();
	$('.modal-backdrop').remove();
	//window.location.replace = "/hix/broker/viewprofile";
}

$(function () {
	$("a[rel=twipsy]").twipsy({
		live: true
	});
});

$(function () {
	$("a[rel=popover]")
		.popover({
			offset: 10
		})
		.click(function(e) {
			e.preventDefault()  // prevent the default action, e.g., following a link
		});
});

$(document).ready(function(){

	$("#myTable").tablesorter();
	$("#showGraphs").verticaltabs({speed: 500,slideShow: false,activeIndex: 2});

	$('#feedback-badge-right').click(function() {
		$dialog.dialog('open');
		return false;
	});
});

function highlightThis(val){
	var currUrl = window.location.href;
	var newUrl="";

	/* Check if Query String parameter is set or not
	* If yes
	*/
	if(currUrl.indexOf("?", 0) > 0) {
		if(currUrl.indexOf("?lang=", 0) > 0) { /* Check if locale is already set without querystring param */
			newUrl = "?lang="+val;
		} else if(currUrl.indexOf("&lang=", 0) > 0) { /* Check if locale is already set with querystring param  */
			newUrl = currUrl.substring(0, currUrl.length-2)+val;
		} else {
			newUrl = currUrl + "&lang="+val;
		}
	} else { /* If No */
		newUrl = currUrl + "?lang="+val;
	}
	window.location = newUrl;
}
	</script>


<div class="gutter10">
	<div class="l-page-breadcrumb"><!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb"  style="margin-top:5px">
			<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message
							code="label.account" /></a></li>
	        	<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
	        </ul>
		</div>
	</div><!--page-breadcrumb ends-->
	<div class="row-fluid">
		<h1 id="skip">${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>
	<div class="row-fluid broker-panel">
		<!-- #sidebar -->
	       <jsp:include page="brokerLeftNavigationMenu.jsp" />
	    <!-- #sidebar ENDS -->

		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">

			<div class="header">
				<h4><spring:message code="label.certificationstatus"/></h4>
			</div>

			<form class="form-horizontal gutter10" id="frmcertificationstatus" name="frmcertificationstatus" action="certificationstatus"  method="POST">

					<table class="table table-border-none table-condensed table-res" role="presentation">
						<tbody>
						    <tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.brkNumber"/></td>
							    <td class="span4"><strong>${broker.brokerNumber}</strong></td>
						    </tr>
							<tr>
								<td class="span4 txt-right"><spring:message  code="label.brkCertificationApplSubmissionDate"/></td>
								<td><strong><fmt:formatDate value="${broker.applicationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkCertificationStatus"/></td>
								<td><strong>${broker.certificationStatus}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkCertificationNumber"/></td>
								<td><strong>${broker.certificationNumber}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkCertificationDate"/></td>
								<td><strong><fmt:formatDate value= "${broker.certificationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
					<%--	<tr>
								<td class="txt-right">Decertified On</td>
								<td><strong><fmt:formatDate value="${broker.deCertificationDate}" pattern="mm-dd-yyyy"/></strong></td>
							</tr> --%>
							<tr>
								<td class="txt-right"><spring:message  code="label.brkRenewalDate"/></td>
								<td><strong><fmt:formatDate value= "${broker.reCertificationDate}" pattern="MM-dd-yyyy"/></strong></td>
							</tr>
							<c:if test="${CA_STATE_CODE}">
                            <tr>
                                <td class="txt-right"><spring:message code="label.brkDelegationCode"/></td>
                                <td><strong>${broker.delegationCode}</strong></td>
                            </tr>
							</c:if>
						</tbody>
					</table>


<%-- Commented as of HIX-15540	<% if(diffDays<=30) { %> --%>
<!-- 				<div class="form-actions"> -->
<!-- 					<input type="submit" name="submit" id="submit" value="Get Renewed" class="btn btn-primary" />  -->
<!-- 				</div>end of form-actions -->
<%-- 	<%} %> --%>
			</form>
		</div><!-- end of span9 -->
		</div>
	</div><!--  end of row-fluid -->
