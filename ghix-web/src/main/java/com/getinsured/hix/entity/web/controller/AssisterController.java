package com.getinsured.hix.entity.web.controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.commons.CommonsMultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.broker.utils.BrokerMgmtUtils;
import com.getinsured.hix.dto.enrollment.EnrollmentCountDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.entity.service.EntityService;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.consumer.ConsumerResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.config.AEEConfiguration.AEEConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.thoughtworks.xstream.XStream;

/**
 * Handles requests for the assister pages.
 */
@Controller
public class AssisterController {
	
	private static final String EXCHANGE_PHONE = "exchangePhone";

	private static final String NOTIFICATION_DATE = "notificationDate";

	private static final String EXCHANGE_NAME = "exchangeName";



	private static final String CECLN = "CECLN";

	private static final String CECFN = "CECFN";

	private static final String INDIVIDUAL_LN = "IndividualLN";

	private static final String INDIVIDUAL_MN = "IndividualMN";

	private static final String INDIVIDUAL_FN = "IndividualFN";

	private static final String PDF = ".pdf";
	private static final String ECMRELATIVEPATH = "Enrollment Counselor De-designation Notice (by Individual)";
	


	private static final String REGISTRATION_STATUS = "registrationStatus";

	private static final String HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER = "hasPermission(#model, 'ASSISTER_EDIT_ASSISTER')";

	private static final String HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER = "hasPermission(#model, 'ASSISTER_VIEW_ASSISTER')";
	private static final int THIRTY=30;
	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterController.class);

	private static final String FILE_INPUT = "fileInput";
	private static final String ASSISTER_ID = "assisterId";
	private static final String NAVIGATION_MENU_MAP = "navigationmenumap";
	private static final String ASSISTER = "assister";
	private static final String SHOW_POSTAL_MAIL="showPostalMailOption";
	private static final String LIST_OF_ASSISTERS = "assisterList";
	private static final String ASSISTER_LANGUAGES = "assisterLanguages";
	private static final String LIST_OF_STATES = "statelist";
	private static final String LIST_OF_EDUCATIONS = "educationlist";
	private static final String IS_SHOW = "isShow";
	private static final String IS_CERTIFICATION_CERTIFIED = "isCertified";
	private static final String LIST_OF_PRIMARY_SITES = "primarySitelist";
	private static final String LIST_OF_SECONDARY_SITES = "secondarySitelist";
	private static final String PAGE_TITLE = "page_title";
	private static final String ENROLLMENT_ENTITY = "enrollmentEntity";
	private static final String OTHER_SPOKEN_LANGUAGE = "otherSpokenLanguage";
	private static final String OTHER_WRITTEN_LANGUAGE = "otherWrittenLanguage";
	private static final String LANGUAGE_NAMES = "languageNames";
	private static final String ASSISTER_REGISTRATION = "Assister Registration";
	private static final int ZERO = 0;
	private static final int THREE = 3;
	private static final int SIX = 6;
	private static final int TEN = 10;
	private static final String ENROLLMENTENTITY = "enrollmentEntity";
	private static final String LANGUAGE_WRITTEN_NAMES = "languageWrittenNames";
	private static final String ENTITY_LANGUAGE_SPOKEN = "ENTITY_LANGUAGE_SPOKEN";
	private static final String ENTITY_LANGUAGE_WRITTEN = "ENTITY_LANGUAGE_WRITTEN";
	private static final String LIST_OF_LANGUAGES_SPOKEN = "languageSpokenList";
	private static final String LIST_OF_LANGUAGES_WRITTEN = "languageWrittenList";
	private static final String NEXTREGISTRATIONSTEP = "nextRegistrationStep";
	private static final String RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS = "Retrieving Assister Detail REST Call Starts : ";
	private static final String LANGUAGES_LIST = "languagesList";
	private static final String SWITCH_ACC_URL = "switchAccUrl";
	private static final String ASSISTER_LOGIN = "assisterLogin";
	private static final String LOGGED_USER = "loggedUser";

	private static final String EDUCATION_UP_TO_8TH_GRADE = "Up to 8th Grade";
	private static final String EDUCATION_SOME_HIGH_SCHOOL = "Some High School";
	private static final String EDUCATION_HIGH_SCHOOL_GRADUATE= "High School Graduate";
	private static final String EDUCATION_SOME_COLLEGE = "Some College";
	private static final String EDUCATION_COLLEGE_GRADUATE = "College Graduate";
	private static final String EDUCATION_TWO_YEAR_ASSOCIATE_DEGREE = "Two Year Associate Degree";
	private static final String INAPPLICABLE = "Inapplicable/Not Ascertained";

	private static final String PRIMARY_PHONE1 = "primaryPhone1";
	private static final String PRIMARY_PHONE2 = "primaryPhone2";
	private static final String PRIMARY_PHONE3 = "primaryPhone3";

	private static final String SECONDARY_PHONE3 = "secondaryPhone3";
	private static final String SECONDARY_PHONE1 = "secondaryPhone1";
	private static final String SECONDARY_PHONE2 = "secondaryPhone2";
	
	private static final String FIRSTNAME = "firstName";
	private static final String LASTNAME = "lastName";
	
	private static final String TRUE = "true";
	private static final String FALSE = "false";
	private static final String DESIGNATED_ASSISTER_PROFILE = "designatedAssisterProfile";
	private static final String DESIGNATE_ASSISTER = "designateAssister";
	private static final String PAGE_AFTER_SWITCH_BACK = "pageAfterSwitchBack";
	private static final String IS_USER_SWITCH_TO_OTHER_VIEW = "isUserSwitchToOtherView";
	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;
	private static final String ENCRYPTED_ASSISTER_ID = "encryptedAssisterId";
	
	@Autowired private Gson platformGson;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private EERestCallInvoker restClassCommunicator;
	@Autowired
	private UserService userService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private EntityService entityService;
	
	@Autowired 
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired
	private NoticeService noticeService;
	
	@Autowired
	private SmartValidator validator;
	
	@Autowired
	private SCIMUserManager scimUserManager;
	
	/**
	 * Loads the information needed in Assister Registration page and redirects
	 * to Assister registration form
	 * 
	 * @param enrollmentEntityId
	 * @param assisterId
	 * @param model
	 * @param fileInput
	 * @param request
	 * @return URL string
	 * @throws InvalidUserException 
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assister/registration", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ASSISTER_MANAGE_ASSISTER')")
	public String registerAssister(
			@RequestParam(value = "enrollmentEntityId", required = false) String enrollmentEntityId,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId, Model model,
			HttpServletRequest request) throws GIException {

		LOGGER.info("registerAssister: START");

		LOGGER.info(ASSISTER_REGISTRATION);
		model.addAttribute(PAGE_TITLE, ASSISTER_REGISTRATION);
		assisterId=assisterId != null && !"".equals(assisterId) ?ghixJasyptEncrytorUtil.decryptStringByJasypt(assisterId):null;
		enrollmentEntityId=enrollmentEntityId != null && !"".equals(enrollmentEntityId)?ghixJasyptEncrytorUtil.decryptStringByJasypt(enrollmentEntityId):null;
		AccountUser user = null;
		EnrollmentEntity enrollmentEntity = null;
		List<Assister> listOfAssisters = null;
		String showPostalMailOption=FALSE;
		List<Site> listOfPrimarySites = null, listOfSecondarySites = null, listOfSites = new ArrayList<Site>();
		
		try {
			user = userService.getLoggedInUser();
			request.getSession().setAttribute("showCECLeftnavigation", FALSE);
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			if (!isEntityAdmin()) {
				enrollmentEntity = getEnrollmentEntityByUserId(user.getId());
			}
			else {
				// retrieve enrollmententity
				enrollmentEntity = (EnrollmentEntity) request.getSession().getAttribute(ENROLLMENTENTITY);
			}
			if (enrollmentEntity != null) {
				// fetching list of Assisters for the given Enrollment Entity
				listOfAssisters = getListOfAssistersByEnrollmentEntity(enrollmentEntity);
				String languageDetails =  loadLanguages();
				model.addAttribute(LANGUAGES_LIST, languageDetails);
				model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntity);
				model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);
                
				if(enrollmentEntity.getRegistrationStatus()!=null){
					model.addAttribute(REGISTRATION_STATUS, enrollmentEntity.getRegistrationStatus());
				}
				
				// Edit page block
				if (assisterId != null) {

					// Fetching Assister's Primary Site
					listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
							Site.site_type.PRIMARY.toString());

					// Fetching Assister's Secondary/Sub-Sites
					listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
							Site.site_type.SUBSITE.toString());

					if (listOfPrimarySites != null) {
						listOfSites.addAll(listOfPrimarySites);
					}
					if (listOfSecondarySites != null) {
						listOfSites.addAll(listOfSecondarySites);
					}

					model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
					model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);

					// Fetching List of Languages
					List<LookupValue> listOfLanguagesSpoken = lookupService.getLookupValueList(ENTITY_LANGUAGE_SPOKEN);
					Collections.sort(listOfLanguagesSpoken);
					Collections.reverse(listOfLanguagesSpoken);
					model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

					// Fetching List of Languages
					List<LookupValue> listOfLanguagesWritten = lookupService
							.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
					Collections.sort(listOfLanguagesWritten);
					Collections.reverse(listOfLanguagesWritten);
					model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);

					model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

					model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());

					// This flag is required to show the Add/Edit page
					model.addAttribute(IS_SHOW, TRUE);

					// Adding the list of languages to model
					AssisterResponseDTO response = setAssisterAndLanguages(model, Integer.valueOf(assisterId));
					Assister assister = response.getAssister();
					EntityUtils.checkEntityAssisterAssociation(assisterId, user, response);
					// This flag is required to show certification number if
					// entered by
					// EE while registering the Assister
					if (assister.getCertificationNumber() != null && !"".equals(assister.getCertificationNumber())) {
						model.addAttribute(IS_CERTIFICATION_CERTIFIED, TRUE);
					}

					// Setting other spoken and written languages
					AssisterLanguages assisterLanguages = response.getAssisterLanguages();
					setOtherSpokenLanguage(assisterLanguages, listOfLanguagesSpoken, model);
					setOtherWrittenLanguage(assisterLanguages, listOfLanguagesWritten, model);

					// Set language names for other language text box checking
					setLanguageNames(listOfLanguagesSpoken, listOfLanguagesWritten, model);
				}
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister registration info : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister registration info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("registerAssister: END");

		return "entity/assister/registration";
	}

	
	/**
	 * Loads the information needed in Assister Registration page and redirects
	 * to Assister registration form
	 * 
	 * @param enrollmentEntityId
	 * @param assisterId
	 * @param model
	 * @param fileInput
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/entity/assister/viewassisterinformationbyuserid", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String viewAssisterInformationPage(Model model) {

		LOGGER.info("viewAssisterInformationPage: START");

		LOGGER.info("View Assister Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Information");
		AccountUser user = null;
		String showPostalMailOption=FALSE;
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		
		try {
			user = userService.getLoggedInUser();
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			assisterRequestDTO.setUserId(user.getId());
			assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
			assisterResponseDTO = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
			Assister assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister info page : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		// assisterNavigationMap(request, assister);

		LOGGER.info("viewAssisterInformationPage: END");

		return "entity/assisteradmin/viewassisterinformation";
	}

	@RequestMapping(value = "/entity/assister/addassister", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String addAssister(Model model) {

		LOGGER.info("addAssister: START");

		LOGGER.info("ADD : Assister Registration Starts");

		AccountUser user = null;
		EnrollmentEntity enrollmentEntity = null;
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		List<Site> listOfPrimarySites = null;
		List<Site> listOfSecondarySites = null;
		List<Site> listOfSites = new ArrayList<Site>();
		String showPostalMailOption=FALSE;

		try {
			user = userService.getLoggedInUser();
			if (user != null && user.getId() != 0) {
				// fetching Enrollment Entity by user id
				enrollmentEntity = getEnrollmentEntityByUserId(user.getId());
				if (enrollmentEntity != null) {
					// fetching list of Assisters for the given Enrollment Entity
					listOfAssisters = getListOfAssistersByEnrollmentEntity(enrollmentEntity);

					// Fetching Enrollment Entity Primary Site
					listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
							Site.site_type.PRIMARY.toString());

					// Fetching Enrollment Entity Secondary/Sub-Sites
					listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
							Site.site_type.SUBSITE.toString());

					listOfSites.addAll(listOfPrimarySites);
					listOfSites.addAll(listOfSecondarySites);
				}
			}
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntity);

			model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
			model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);

			// Fetching list of Assisters for the Enrollment Entity
			model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);

			// Fetching List of Languages
			List<LookupValue> listOfLanguagesSpoken = sortLanguage(ENTITY_LANGUAGE_SPOKEN);
			model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

			List<LookupValue> listOfLanguagesWritten = sortLanguage(ENTITY_LANGUAGE_WRITTEN);
			model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);

			// Set language names for other language text box checking
			setLanguageNames(listOfLanguagesSpoken, listOfLanguagesWritten, model);

			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

			model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());

			// This flag is required to show the Add/Edit page
			model.addAttribute(IS_SHOW, TRUE);
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while adding assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while adding assister in addAssister method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("ADD : Assister Registration Ends");

		LOGGER.info("addAssister: END");

		return "entity/assister/registration";
	}

	/**
	 * 
	 * @param entityLanguage 
	 * @return
	 */
	private List<LookupValue> sortLanguage(String entityLanguage) {
		// Fetching List of Languages
		LookupValue langWritten=null;
		List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(entityLanguage);
		for(LookupValue lookupValue:listOfLanguagesWritten){
			if("English".equalsIgnoreCase(lookupValue.getLookupValueLabel())){
				langWritten=lookupValue;
				listOfLanguagesWritten.remove(lookupValue);
				break;
			}
		}
		Collections.sort(listOfLanguagesWritten);
		Collections.reverse(listOfLanguagesWritten);
		if(langWritten!=null){
			listOfLanguagesWritten.add(0, langWritten);
		}
		return listOfLanguagesWritten;
	}

	/**
	 * Saves new Assister registration information
	 * 
	 * @param model
	 * @param assister
	 * @param enrollmentEntity
	 * @param assisterBindingResult
	 * @param languageBindingResult
	 * @param primarySite
	 * @param secondarySite
	 * @param assisterLanguages
	 * @param assisterId
	 * @param assisterLanguageId
	 * @param fileInput
	 * @param request
	 * @return URL string
	 * @throws InvalidUserException 
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assister/registration", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String saveAssisterRegistrationInfo(Model model, @ModelAttribute(ASSISTER) Assister assister,BindingResult assisterBindingResult,
			@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity,
			@ModelAttribute(ASSISTER_LANGUAGES)@Validated AssisterLanguages assisterLanguages,BindingResult languageBindingResult,
			@RequestParam(value = ASSISTER_ID, required = false) String assisterId,
			@RequestParam(value = "assisterLanguageId", required = false) String assisterLanguageId,
			@RequestParam(value = "primarySite", required = false) String primarySite,
			@RequestParam(value = "secondarySite", required = false) String secondarySite,
			@RequestParam(value = FILE_INPUT, required = false) CommonsMultipartFile fileInput,
			HttpServletRequest request) throws GIException, InvalidUserException {

		LOGGER.info("saveAssisterRegistrationInfo: START");

		LOGGER.info("Assister Registration Submit");
		model.addAttribute(PAGE_TITLE, ASSISTER_REGISTRATION);
		String isAssisterCertified= request.getParameter("isAssisterCertified");
		EnrollmentEntity enrollmentEntityObj= enrollmentEntity;
		/** Check for XSS Attack - Starts */
		stripXss(assister, assisterLanguages);
		/** Check for XSS Attack - Ends */

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AccountUser user = null;

		try {
			user = userService.getLoggedInUser();
			if (user != null) {
				// fetching Enrollment Entity by user id
				enrollmentEntityObj = getEnrollmentEntityByUserId(user.getId());
			}

			// Setting Assister Id to update Registration Information (Required in
			// case of only Edit Operation)
			if (EntityUtils.isNullOrBlank(assisterId)) {
				assister.setId(Integer.valueOf(assisterId));
			}

			// Setting Assister Languages Id to update Languages (Required in case
			// of only Edit Operation)
			if (EntityUtils.isNullOrBlank(assisterLanguageId)) {
				assisterLanguages.setId(Integer.valueOf(assisterLanguageId));
			}

			// Fetching selected Primary Site object and associating with Assister
			if (EntityUtils.isNullOrBlank(primarySite)) {
				assister.setPrimaryAssisterSite(getSiteById(Integer.valueOf(primarySite)));
			}

			// Fetching selected Secondary/Sub Site object and associating with
			// Assister
			if (EntityUtils.isNullOrBlank(secondarySite)) {
				assister.setSecondaryAssisterSite(getSiteById(Integer.valueOf(secondarySite)));
			}

			//validate after setting primary site and secondary site
			validateAddNewAssister(assisterBindingResult,languageBindingResult,assister,isAssisterCertified);
			
			// Associating EE to Assister
			assister.setEntity(enrollmentEntityObj);
			assisterRequestDTO.setAssister(assister);
			assisterRequestDTO.setAssisterLanguages(assisterLanguages);

			// Saving Assister details
			AssisterResponseDTO assisterResponseDTO = saveAssisterDetails(assisterRequestDTO);
			EntityUtils.checkEntityAssisterAssociation(assisterId, user, assisterResponseDTO);

			// saving assister photo
			if (assisterResponseDTO != null && assisterResponseDTO.getAssister() != null)
			{
				entityService.saveAssisterPhoto(assisterResponseDTO.getAssister().getId(), fileInput);
			}

			// Enabling Left Navigation Menu for Assister in case of successful
			// Assister Registration
			Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(
					NAVIGATION_MENU_MAP);
			if (navigationMenuMap != null) {
				navigationMenuMap.put("ASSISTER", true);
				request.getSession().setAttribute(NAVIGATION_MENU_MAP, navigationMenuMap);
				// Set the registration step next to Assister so that it is enabled
				// in left navigation.
				request.getSession().setAttribute(NEXTREGISTRATIONSTEP,
						EntityUtils.getNextRegistrationStep(EnrollmentEntity.registrationSteps.ASSISTER.toString()));
			}
			model.addAttribute("CA_STATE_CODE", EntityUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving Assister information : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving Assister Registration Info : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveAssisterRegistrationInfo: END");

		return "redirect:/entity/assister/registration";
	}

	private void validateAddNewAssister(BindingResult assisterBindingResult,BindingResult languageBindingResult, Assister assister,String isAssisterCertified) {
		
		if(!EntityUtils.isEmpty(isAssisterCertified) && "Yes".equalsIgnoreCase(isAssisterCertified)){
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterCerticationNumber.class,Assister.AssisterInfoGroup.class);
		}else{
			validator.validate(assister,assisterBindingResult,Assister.AddAssisterByEntity.class,Assister.AssisterInfoGroup.class);
		}
		if(assisterBindingResult.hasErrors() || languageBindingResult.hasErrors()){
			 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
		}
	}

	/**
	 * Loads the information needed in Assister Registration page and redirects
	 * to Assister registration form used when registered EE adds a new assister
	 * 
	 * @param model
	 * @param request
	 * @return URL string
	 */
	@RequestMapping(value = "/entity/assister/addnewassister", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String addNewAssister(Model model) {

		LOGGER.info("addNewAssister: START");

		LOGGER.info(ASSISTER_REGISTRATION);
		String showPostalMailOption=FALSE;
		model.addAttribute(PAGE_TITLE, ASSISTER_REGISTRATION);

		AccountUser user = null;
		EnrollmentEntity enrollmentEntity = null;
		List<Assister> listOfAssisters = new ArrayList<Assister>();
		List<Site> listOfPrimarySites = null;
		List<Site> listOfSecondarySites = null;
		List<Site> listOfSites = new ArrayList<Site>();

		try {
			user = userService.getLoggedInUser();
			if (user != null && user.getId() != 0) {
				// fetching Enrollment Entity by user id
				enrollmentEntity = getEnrollmentEntityByUserId(user.getId());
			}
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntity);
			model.addAttribute(LIST_OF_ASSISTERS, listOfAssisters);

			// Fetching Assister's Primary Site
			listOfPrimarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
					Site.site_type.PRIMARY.toString());

			// Fetching Assister's Secondary/Sub-Sites
			listOfSecondarySites = getListOfEnrollmentEntitySitesBySiteType(enrollmentEntity,
					Site.site_type.SUBSITE.toString());

			if(null != listOfPrimarySites) {
				listOfSites.addAll(listOfPrimarySites);				
			}

			if(null != listOfSecondarySites) {
				listOfSites.addAll(listOfSecondarySites);
			}


			model.addAttribute(LIST_OF_PRIMARY_SITES, listOfSites);
			model.addAttribute(LIST_OF_SECONDARY_SITES, listOfSites);

			// Fetching List of Languages
			List<LookupValue> listOfLanguagesSpoken = lookupService.getLookupValueList(ENTITY_LANGUAGE_SPOKEN);
			Collections.sort(listOfLanguagesSpoken);
			Collections.reverse(listOfLanguagesSpoken);
			model.addAttribute(LIST_OF_LANGUAGES_SPOKEN, listOfLanguagesSpoken);

			// Fetching List of Languages
			List<LookupValue> listOfLanguagesWritten = lookupService.getLookupValueList(ENTITY_LANGUAGE_WRITTEN);
			Collections.sort(listOfLanguagesWritten);
			Collections.reverse(listOfLanguagesWritten);
			model.addAttribute(LIST_OF_LANGUAGES_WRITTEN, listOfLanguagesWritten);
			model.addAttribute(LIST_OF_STATES, new StateHelper().getAllStates());

			model.addAttribute(LIST_OF_EDUCATIONS, getEducationList());
			String languageDetails =  loadLanguages();
			model.addAttribute(LANGUAGES_LIST, languageDetails);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while adding assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while adding assister in addNewAssister method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("addNewAssister: END");

		return "entity/assister/addnewassister";
	}

	/**
	 * Saves the assister registration information
	 * 
	 * @param model
	 * @param assister
	 * @param enrollmentEntity
	 * @param assisterLanguages
	 * @param fileInput
	 * @param primarySite
	 * @param secondarySite
	 * @return URL string
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/assister/addnewassister", method = RequestMethod.POST)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_EDIT_ASSISTER)
	public String saveNewAssister(Model model, @ModelAttribute(ASSISTER) Assister assister,
			@ModelAttribute(ENROLLMENT_ENTITY) EnrollmentEntity enrollmentEntity,
			@ModelAttribute(ASSISTER_LANGUAGES) AssisterLanguages assisterLanguages,
			@RequestParam(value = "primarySite", required = false) String primarySite,
			@RequestParam(value = "secondarySite", required = false) String secondarySite,
			@RequestParam(value = FILE_INPUT, required = false) CommonsMultipartFile fileInput) throws InvalidUserException {

		LOGGER.info("saveNewAssister: START");

		LOGGER.info("Add New Assister Submit");
		model.addAttribute(PAGE_TITLE, "Add New Assister");
		EnrollmentEntity enrollmentEntityObj = enrollmentEntity;
		/** Check for XSS Attack - Starts */
		stripXss(assister, assisterLanguages);

		/** Check for XSS Attack - Ends */

		AccountUser user = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();

		try {
			user = userService.getLoggedInUser();
			if (null != user) {
				// fetching Enrollment Entity by user id
				enrollmentEntityObj = getEnrollmentEntityByUserId(user.getId());

				// Fetching selected Primary Site object and associating with
				// Assister
				if (EntityUtils.isNullOrBlank(primarySite)) {
					assister.setPrimaryAssisterSite(getSiteById(Integer.valueOf(primarySite)));
				}

				// Fetching selected Secondary/Sub Site object and associating
				// with
				// Assister
				if (EntityUtils.isNullOrBlank(secondarySite)) {
					assister.setSecondaryAssisterSite(getSiteById(Integer.valueOf(secondarySite)));
				}

				if (EntityUtils.isEmpty(assister.getStatus())) {
					assister.setStatus("Active");
					assister.setCertificationStatus(EntityConstants.STATUS_PENDING);
					assister.setStatusChangeDate(new TSDate());
				}

				// Associating EE to Assister
				assister.setEntity(enrollmentEntityObj);
				assisterRequestDTO.setAssister(assister);
				assisterRequestDTO.setAssisterLanguages(assisterLanguages);

				// Saving Assister details
				AssisterResponseDTO assisterResponseDTO = saveAssisterDetails(assisterRequestDTO);

				// saving assister photo
				if (assisterResponseDTO != null && assisterResponseDTO.getAssister() != null)
				{
					entityService.saveAssisterPhoto(assisterResponseDTO.getAssister().getId(), fileInput);
				}
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while saving Assister information in saveNewAssister method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while saving Assister information in saveNewAssister method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("saveNewAssister: END");

		return "redirect:/entity/entityadmin/displayassisters";
	}

	/**
	 * @param assister
	 * @param assisterLanguages
	 */
	private void stripXss(Assister assister, AssisterLanguages assisterLanguages) {
		assister.setComments(XssHelper.stripXSS(assister.getComments()));
		assister.setBusinessLegalName(XssHelper.stripXSS(assister.getBusinessLegalName()));
		assister.setEmailAddress(XssHelper.stripXSS(assister.getEmailAddress()));
		assister.setFirstName(XssHelper.stripXSS(assister.getFirstName()));
		assister.setLastName(XssHelper.stripXSS(assister.getLastName()));
		assister.setPrimaryPhoneNumber(XssHelper.stripXSS(assister.getPrimaryPhoneNumber()));
		assister.setSecondaryPhoneNumber(XssHelper.stripXSS(assister.getSecondaryPhoneNumber()));

		if (assister.getCertificationNumber() != null) {
			String certNo = XssHelper.stripXSS(String.valueOf(assister.getCertificationNumber()));
			assister.setCertificationNumber(!StringUtils.isEmpty(certNo) ? Long.parseLong(certNo) : null);
		}

		if (assister.getMailingLocation() != null) {
			assister.getMailingLocation().setAddress1(XssHelper.stripXSS(assister.getMailingLocation().getAddress1()));
			assister.getMailingLocation().setAddress2(XssHelper.stripXSS(assister.getMailingLocation().getAddress2()));
			assister.getMailingLocation().setCity(XssHelper.stripXSS(assister.getMailingLocation().getCity()));

			String zip = XssHelper.stripXSS(String.valueOf(assister.getMailingLocation().getZip()));
			assister.getMailingLocation().setZip(!StringUtils.isEmpty(zip) ? zip : null);
		}

		if (assisterLanguages != null) {
			// Adding check for Assister languages
			assisterLanguages.setSpokenLanguages(XssHelper.stripXSS(assisterLanguages.getSpokenLanguages()));
			assisterLanguages.setWrittenLanguages(XssHelper.stripXSS(assisterLanguages.getWrittenLanguages()));
		}
	}

	/**
	 * Handles the Assister view page
	 * 
	 * @param model
	 * 
	 * @param encryptedassisterId
	 * @return URL for navigation to View Assister Information page
	 * @throws GIException 
	 */
	@RequestMapping(value = "/entity/assister/viewassisterinformation", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String viewAssisterInformation(Model model,
			@RequestParam(value = ASSISTER_ID, required = false) String encryptedassisterId) throws GIException {

		LOGGER.info("viewAssisterInformation: START");
		String assisterId=null;
		if(encryptedassisterId!=null)
		{
			assisterId=ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedassisterId);
		}

		LOGGER.info("View Assister Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Information");
		AccountUser user = null;
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		EnrollmentEntity enrollmentEntity = null;
		String showPostalMailOption=FALSE;
		
		try {
			user = userService.getLoggedInUser();
			showPostalMailOption=EntityUtils.isAllaowMailNotices();
			enrollmentEntity = getEnrollmentEntityByUserId(user.getId());

			if (EntityUtils.isNullOrBlank(assisterId)) {
				assisterRequestDTO.setId(Integer.valueOf(assisterId));

				LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
				assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
				assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			} else {
				assisterRequestDTO.setUserId(user.getId());
				LOGGER.debug("Retrieving Assister Detail By User Id REST Call Starts :");
				assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
				assisterResponseDTO = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
		   }
			
			
			LOGGER.debug("Retrieving Assister Detail By User Id REST Call Ends :");
			
			Validate.notNull(assisterResponseDTO);
			
			if((enrollmentEntity != null) && (assisterResponseDTO != null) && 
				((assisterResponseDTO.getAssister() == null) || enrollmentEntity.getId() != assisterResponseDTO.getAssister().getEntity().getId())){
				LOGGER.error("Error while processing Assister data. Invalid Assister ID.");
				throw new GIException("Access not allowed");
			}
			
			AssisterLanguages assisterLanguages = assisterResponseDTO.getAssisterLanguages();
			String languageSpoken = assisterLanguages.getSpokenLanguages();
			languageSpoken = splitLanguages(languageSpoken);
			assisterLanguages.setSpokenLanguages(languageSpoken==null?"":languageSpoken);

			String languageWritten = assisterLanguages.getWrittenLanguages();
			languageWritten = splitLanguages(languageWritten);
			assisterLanguages.setWrittenLanguages(languageWritten==null?"":languageWritten);
			model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
			model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
			model.addAttribute(ENROLLMENT_ENTITY, enrollmentEntity);
			model.addAttribute("IsAssister", true);
			model.addAttribute(SHOW_POSTAL_MAIL,showPostalMailOption);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing assister in viewAssisterInformation method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing assister in viewAssisterInformation method : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("viewAssisterInformation: END");

		return "entity/assister/viewassisterinformation";
	}

	/**
	 * @param language
	 * @return
	 */
	private String splitLanguages(String language) {
		if(language !=null){
			List<String> languageWrittenList = EntityUtils.splitStringValues(language, ",");
			StringBuilder strBuilderWritten = new StringBuilder();
			for (String languageWrittenStr : languageWrittenList) {
				strBuilderWritten.append(languageWrittenStr);
				strBuilderWritten.append(", ");
			}
			return strBuilderWritten.substring(0, strBuilderWritten.length() - 2);
		}
		return language;
	}

	/**
	 * Handles the Assister Profile View page
	 * 
	 * @param model
	 * @param request
	 * @return URL for navigation to View Assister Information page
	 */
	@RequestMapping(value = "/entity/assister/viewassisterprofilebyuserid", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String viewAssisterProfile(Model model) {

		LOGGER.info("viewAssisterProfile: START");

		LOGGER.info("View Assister Information");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Information");
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		Assister assister = null;
		AccountUser user = null;
		
		try {
			user = userService.getLoggedInUser();
			assisterRequestDTO.setUserId(user.getId());
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			LOGGER.info("Assister User ID : "+user.getId());
			assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
			assisterResponseDTO = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
			LOGGER.debug("Retrieving Assister Detail REST Call Ends.");
			AssisterLanguages assisterLanguages = assisterResponseDTO.getAssisterLanguages();
			String languageSpoken = assisterLanguages.getSpokenLanguages();
			languageSpoken = splitLanguages(languageSpoken);
			assisterLanguages.setSpokenLanguages(languageSpoken==null?"":languageSpoken);

			String languageWritten = assisterLanguages.getWrittenLanguages();
				languageWritten = splitLanguages(languageWritten);
			assisterLanguages.setWrittenLanguages(languageWritten==null?"":languageWritten);
			assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());
			setContactNumber(model, assister);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
			// assisterNavigationMap(request, assister);
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing assister profile in viewAssisterProfile method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing assister profile in viewAssisterProfile method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("viewAssisterProfile: END");

		return "entity/assisteradmin/viewassisterprofile";
	}

	@RequestMapping(value = "/entity/assister/certificationstatusbyuserid", method = RequestMethod.GET)
	@PreAuthorize(HAS_PERMISSION_MODEL_ASSISTER_VIEW_ASSISTER)
	public String getCertificationStatus(Model model, HttpServletRequest request) {

		LOGGER.info("getCertificationStatus: START");

		LOGGER.info("View Assister Certification Status");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Certification Status");
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		String assisterDetailsResponse = null;
		Assister assister = null;
		AccountUser user = null;
		
		try {
			user = userService.getLoggedInUser();
			assisterRequestDTO.setUserId(user.getId());
			LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
			assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
			assisterResponseDTO = (AssisterResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(assisterDetailsResponse);
			LOGGER.debug("Retrieving Assister Detail REST Call Ends.");
			assister = assisterResponseDTO.getAssister();
			model.addAttribute(ASSISTER, assister);
			assisterNavigationMap(request, assister);
			model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(EntityConstants.CA_STATE_CODE));
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while retrieving assister certification status in getCertificationStatus method: ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while retrieving assister certification status : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getCertificationStatus: END");

		return "entity/assister/certificationstatus";
	}

	private void assisterNavigationMap(HttpServletRequest request, Assister assister) {

		LOGGER.info("assisterNavigationMap: START");

		Map<String, Boolean> map = null;
		if (assister != null) {
			map = getLeftNavigationMenuMap(assister);
		} else {
			map = new HashMap<String, Boolean>();
		}

		Map<String, Boolean> navigationMenuMap = (Map<String, Boolean>) request.getSession().getAttribute(
				NAVIGATION_MENU_MAP);

		if (navigationMenuMap != null) {
			map.putAll(navigationMenuMap);
		}
		request.getSession().setAttribute(EntityConstants.NAVIGATION_MENU_MAP, map);

		LOGGER.info("assisterNavigationMap: END");
	}

	private Map<String, Boolean> getLeftNavigationMenuMap(Assister assister) {

		LOGGER.info("getLeftNavigationMenuMap: START");

		AssisterResponseDTO assisterResponseDTO = null;

		Map<String, Boolean> map = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setAssister(assister);
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			String requestXML = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Populate Left Navigation Map REST Call Starts :");
			String response = restClassCommunicator.assisterPopulateNavigationMenuMap(requestXML);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(response);
			map = assisterResponseDTO.getNavigationMenuMap();
			LOGGER.debug("Populate Left Navigation Map REST Call Ends :");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Populating Left Navigation Map : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getLeftNavigationMenuMap: END");

		return map;
	}

	private AssisterResponseDTO setAssisterAndLanguages(Model model, Integer assisterID) throws JsonProcessingException, IOException {

		LOGGER.info("setAssisterAndLanguages: START");

		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		assisterRequestDTO.setId(assisterID);

		LOGGER.debug(RETRIEVING_ASSISTER_DETAIL_REST_CALL_STARTS);
		String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
		AssisterResponseDTO assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
		LOGGER.debug("Retrieving Assister Detail REST Call Ends :");

		model.addAttribute(ASSISTER, assisterResponseDTO.getAssister());
		model.addAttribute(ASSISTER_LANGUAGES, assisterResponseDTO.getAssisterLanguages());

		setContactNumber(model, assisterResponseDTO.getAssister());

		LOGGER.info("setAssisterAndLanguages: END");

		return assisterResponseDTO;
	}

	private void setOtherSpokenLanguage(AssisterLanguages assisterLanguages, List<LookupValue> listOfLanguagesSpoken,
			Model model) {

		LOGGER.info("setOtherSpokenLanguage: START");

		String[] spokenLanguages = assisterLanguages.getSpokenLanguages().split(",");
		List<String> listOfNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		StringBuilder strSpoken = new StringBuilder();
		for (String language : spokenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strSpoken.length() == 0) {
					strSpoken.append(language);
				} else {
					strSpoken.append(",");
					strSpoken.append(language);
				}
			}
		}
		if (strSpoken.length() != 0) {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, strSpoken);
		} else {
			model.addAttribute(OTHER_SPOKEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherSpokenLanguage: END");
	}

	private void setOtherWrittenLanguage(AssisterLanguages assisterLanguages, List<LookupValue> listOfLanguagesWritten,
			Model model) {

		LOGGER.info("setOtherWrittenLanguage: START");

		String[] writtenLanguages = assisterLanguages.getWrittenLanguages().split(",");
		List<String> listOfNames = new ArrayList<String>();
		StringBuilder strWritten = new StringBuilder();
		for (LookupValue language : listOfLanguagesWritten) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (String language : writtenLanguages) {
			if (!"".equals(language) && !listOfNames.contains(language)) {
				if (strWritten.length() == 0) {
					strWritten.append(language);
				} else {
					strWritten.append(",");
					strWritten.append(language);
				}
			}
		}
		if (strWritten.length() != 0) {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, strWritten);
		} else {
			model.addAttribute(OTHER_WRITTEN_LANGUAGE, null);
		}

		LOGGER.info("setOtherWrittenLanguage: END");
	}

	private void setLanguageNames(List<LookupValue> listOfLanguagesSpoken, List<LookupValue> listOfLanguagesWritten,
			Model model) {

		LOGGER.info("setLanguageNames: START");

		List<String> listOfNames = new ArrayList<String>();
		List<String> listWrittenNames = new ArrayList<String>();
		for (LookupValue language : listOfLanguagesSpoken) {
			listOfNames.add(language.getLookupValueLabel());
		}
		for (LookupValue language : listOfLanguagesWritten) {
			listWrittenNames.add(language.getLookupValueLabel());
		}
		model.addAttribute(LANGUAGE_NAMES, listOfNames);
		model.addAttribute(LANGUAGE_WRITTEN_NAMES, listWrittenNames);

		LOGGER.info("setLanguageNames: END");
	}

	private List<Site> getListOfEnrollmentEntitySitesBySiteType(EnrollmentEntity enrollmentEntity, String siteType) {

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntity);
		enrollmentEntityRequestDTO.setSiteType(siteType);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Starts : ");
			String listOfSitesResponse = restClassCommunicator.getListOfEnrollmentEntitySitesBySiteType(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Enrollment Entity List of Sites REST Call Ends :");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Enrollment Entity List of Sites : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getListOfEnrollmentEntitySitesBySiteType: END");

		return enrollmentEntityResponseDTO.getSiteList();
	}

	private Site getSiteById(int siteId) {

		LOGGER.info("getSiteById: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();

		Site site = new Site();
		site.setId(siteId);

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setSite(site);

		try {
			String requestJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityRequestDTO.class).writeValueAsString(enrollmentEntityRequestDTO);

			LOGGER.debug("Retrieving Site By ID REST Call Starts : ");
			String listOfSitesResponse = restClassCommunicator.getSiteById(requestJSON);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(listOfSitesResponse);
			LOGGER.debug("Retrieving Site By ID REST Call Ends :");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Site By ID : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSiteById: END");

		return enrollmentEntityResponseDTO.getSite();
	}

	private AssisterResponseDTO saveAssisterDetails(AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("saveAssisterDetails: START");

		AssisterResponseDTO assisterResponseDTO = null;
		AccountUser user =  null;
		Assister assister = null;
		String request = null;
		
		try {
			//get logged in user
			user= userService.getLoggedInUser();
			//get assister object
			assister = assisterRequestDTO.getAssister();
			//check for update or new record operation
			if(assister.getId() == 0){
				// new record
				assister.setCreatedBy(user.getId());
			}else{
				//update
				assister.setUpdatedBy(user.getId());
			}
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			request = writer.writeValueAsString(assisterRequestDTO);
			LOGGER.debug("Save Assister Detail REST Call Starts : ");
			String assisterDetailsResponse = restClassCommunicator.saveAssisterDetails(request);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug("Save Assister Detail REST Call Ends :");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Saving Assister Detail : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("saveAssisterDetails: END");

		return assisterResponseDTO;
	}

	private EnrollmentEntity getEnrollmentEntityByUserId(Integer enrollmentEntityUserId) {

		LOGGER.info("getEnrollmentEntityByUserId: START");

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;

		try {
			enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
			enrollmentEntityRequestDTO.setModuleId(enrollmentEntityUserId);

			LOGGER.debug("Retrieving Enrollment Entity By User Id REST Call Starts : ");
			String enrollmentEntityResponse = restClassCommunicator
					.retrieveEnrollmentEntityObjectByUserId(enrollmentEntityRequestDTO);
			enrollmentEntityResponseDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(enrollmentEntityResponse);
			LOGGER.debug("Retrieving Enrollment Entity By User Id REST Call Ends : ");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Retrieving Enrollment Entity By User Id : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getEnrollmentEntityByUserId: END");

		return enrollmentEntityResponseDTO.getEnrollmentEntity();
	}

	private List<Assister> getListOfAssistersByEnrollmentEntity(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("getListOfAssistersByEnrollmentEntity: START");

		AssisterRequestDTO assisterRequestDTO = null;
		AssisterResponseDTO assisterResponseDTO = null;
		String requestXML = null;
		
		try {
			assisterRequestDTO = new AssisterRequestDTO();
			assisterRequestDTO.setEnrollmentEntity(enrollmentEntity);
			assisterRequestDTO.setAdmin(true);
			
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterRequestDTO.class);
			requestXML = writer.writeValueAsString(assisterRequestDTO);

			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Starts :");
			String listOfAssistersResponse = restClassCommunicator.getListOfAssistersByEnrollmentEntity(requestXML);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(listOfAssistersResponse);
			LOGGER.debug("Retrieving List Of Assisters By Enrollment Entity REST Call Ends : ");
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while retrieving List Of Assisters By Enrollment Entity : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getListOfAssistersByEnrollmentEntity: END");

		return assisterResponseDTO.getListOfAssisters();
	}

	private List<String> getEducationList() {

		LOGGER.info("getEducationList: START");

		List<String> educationList = new ArrayList<String>();
		educationList.add(EDUCATION_UP_TO_8TH_GRADE);
		educationList.add(EDUCATION_SOME_HIGH_SCHOOL);
		educationList.add(EDUCATION_HIGH_SCHOOL_GRADUATE);
		educationList.add(EDUCATION_SOME_COLLEGE);
		educationList.add(EDUCATION_COLLEGE_GRADUATE);
		educationList.add(EDUCATION_TWO_YEAR_ASSOCIATE_DEGREE);
		educationList.add(INAPPLICABLE);

		LOGGER.info("getEducationList: END");

		return educationList;
	}

	private void setContactNumber(Model model, Assister assister) {

		LOGGER.info("setContactNumber: START");

		if (assister.getPrimaryPhoneNumber() != null && !"".equals(assister.getPrimaryPhoneNumber())) {
			model.addAttribute(PRIMARY_PHONE1, assister.getPrimaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(PRIMARY_PHONE2, assister.getPrimaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(PRIMARY_PHONE3, assister.getPrimaryPhoneNumber().substring(SIX, TEN));
		}
		if (assister.getSecondaryPhoneNumber() != null && !"".equals(assister.getSecondaryPhoneNumber())) {
			model.addAttribute(SECONDARY_PHONE1, assister.getSecondaryPhoneNumber().substring(ZERO, THREE));
			model.addAttribute(SECONDARY_PHONE2, assister.getSecondaryPhoneNumber().substring(THREE, SIX));
			model.addAttribute(SECONDARY_PHONE3, assister.getSecondaryPhoneNumber().substring(SIX, TEN));
		}

		LOGGER.info("setContactNumber: END");
	}

	/**
	 * Verifies if logged in user is with role ENTITY_ADMIN
	 * 
	 * @return boolean true/false
	 * @throws InvalidUserException
	 *             exception
	 */
	private boolean isEntityAdmin() throws InvalidUserException {

		return userService.hasUserRole(userService.getLoggedInUser(), RoleService.ENTITY_ADMIN_ROLE);
	}

	/**
	 * Handles navigation to Assister dashboard
	 * 
	 * @param model
	 * 
	 * @param request
	 * @return URL for navigation to Assister dashboard page
	 * @throws InvalidUserException 
	 */
	@RequestMapping(value = "/entity/assister/dashboard", method = RequestMethod.GET)
	/*@GiAudit(transactionName = "ManageAssister", eventType = EventTypeEnum.ASSISTER, eventName = EventNameEnum.PII_READ)*/
	@PreAuthorize("hasPermission(#model, 'ASSISTER_MANAGE_ASSISTER')")
	public String viewAssisterDashboard(Model model, HttpServletRequest request) throws InvalidUserException {
		LOGGER.info("AssisterController : viewAssisterDashboard : START");
		AssisterRequestDTO assisterRequestDTO = null;
		String json = "[{\"planLevel\":\"NoPlan\",\"enrollmentCount\":0}]";
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String enableConsumerSignup = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ENABLE_CONSUMER_SIGNUP);
		
		String pageAfterSwitchBack = (String) request.getSession().getAttribute(PAGE_AFTER_SWITCH_BACK);
		if(pageAfterSwitchBack != null && !pageAfterSwitchBack.equalsIgnoreCase("")){
			request.getSession().removeAttribute(PAGE_AFTER_SWITCH_BACK);
			return "redirect:"+ pageAfterSwitchBack;
		}
		request.getSession().removeAttribute(PAGE_AFTER_SWITCH_BACK);
		model.addAttribute("enableConsumerSignup", enableConsumerSignup);
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange Assister : Assister Dashboard");

		request.getSession().setAttribute(LOGGED_USER, ASSISTER_LOGIN);
		if(EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE)) {
			request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE1, EntityUtils.checkStateCode(EntityConstants.NV_STATE_CODE));
		}else {
			request.getSession().setAttribute(EntityConstants.GLOBAL_STATE_CODE, EntityUtils.checkStateCode(EntityConstants.ID_STATE_CODE));
		}
		request.getSession().removeAttribute("designatedAssisterProfile");
		
		Assister existingAssister = null;
		AssisterLanguages assisterLanguages = null;
		
		try {
			// Get logged in user and retrieve record id i.e. Assister id to
			// fetch existing assister
			AccountUser user = userService.getLoggedInUser();
			if(userService.hasUserRole(user, RoleService.ASSISTER_ROLE)){
				LOGGER.info("logged role: "+ RoleService.ASSISTER_ROLE);
				user.setActiveModuleName(RoleService.ASSISTER_ROLE);
				request.getSession().setAttribute("userActiveRoleName",RoleService.ASSISTER_ROLE.toLowerCase());
				request.getSession().setAttribute("switchToModuleName", RoleService.ASSISTER_ROLE.toLowerCase());
			}
			assisterRequestDTO = new AssisterRequestDTO();
			if (user != null) {
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					if (user.getRecordId() != null) {
						assisterRequestDTO.setId(Integer.parseInt(user.getRecordId()));
						String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
						assisterResponseDTO =JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
						existingAssister = assisterResponseDTO.getAssister();
						assisterLanguages = assisterResponseDTO.getAssisterLanguages();
						request.getSession().setAttribute(DESIGNATED_ASSISTER_PROFILE, existingAssister);
					}
				} else {
					assisterRequestDTO.setUserId(user.getId());
					String assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
					assisterResponseDTO = (AssisterResponseDTO) xstream.fromXML(assisterDetailsResponse);
					existingAssister = assisterResponseDTO.getAssister();
					assisterLanguages = assisterResponseDTO.getAssisterLanguages();
					request.getSession().setAttribute(DESIGNATED_ASSISTER_PROFILE, existingAssister);
					
					//If certification status of assister is not certified, hide individual menu items and show Certificationstatus page.
					if (existingAssister != null && existingAssister.getCertificationStatus() != null 
							&& !existingAssister.getCertificationStatus().equalsIgnoreCase(EntityConstants.STATUS_CERTIFIED)) {
											
						return "redirect:/entity/assister/certificationstatusbyuserid";
						
					}	
				}

				Validate.notNull(existingAssister);
				
				// Associating user with existing assister because IND65 is not
				// executed for Assister
				if (null == existingAssister.getUser()) {
					user = userService.findById(user.getId());
					existingAssister.setUser(user);
					assisterRequestDTO.setAssister(existingAssister);
					assisterRequestDTO.setAssisterLanguages(assisterLanguages);
					saveAssisterDetails(assisterRequestDTO);
				} /*else {*/
					EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
					// set role,assisterId,start date and end date in
					// enrollmentrequest object to fetch required records from
					// db
					//set assisterId in session
					request.getSession().setAttribute("assisterId", existingAssister.getId());
					enrollmentRequest.setAssisterBrokerId(existingAssister.getId());
					enrollmentRequest.setRole(EnrollmentRequest.Role.ASSISTER);
					enrollmentRequest.setStartDate(DateUtil.addToDate(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT,
							-THIRTY));
					//sets the end date to one day ahead of current date so that current date records are also fetched.
					enrollmentRequest.setEndDate(DateUtil.addToDate(new TSDate(), GhixConstants.REQUIRED_DATE_FORMAT,
							1));
					String requestXML = GhixUtils.xStreamHibernateXmlMashaling().toXML(enrollmentRequest);
					// call enrollment api with paramters:assisterId,Assister
					// role,start date and end date
					String restResponse = ghixRestTemplate.postForObject(EnrollmentEndPoints.FIND_ENROLLMENT_PLANLEVEL_COUNT_BY_BROKER, requestXML, String.class);

					EnrollmentResponse response = (EnrollmentResponse) xstream.fromXML(restResponse);

					if (response != null && response.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {

						List<EnrollmentCountDTO> enrollmentCountDTOList = response.getEnrollmentCountDTO();

						json = new GsonBuilder().create().toJson(enrollmentCountDTOList);
					}
					request.getSession().setAttribute("json", json);
				//}
				
				model.addAttribute(ASSISTER_ID, existingAssister.getId());
				model.addAttribute("CA_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE));
				model.addAttribute("ID_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.ID_STATE_CODE));
				model.addAttribute("NV_STATE_CODE", BrokerMgmtUtils.checkStateCode(BrokerConstants.NV_STATE_CODE));
				
				if(BrokerMgmtUtils.checkStateCode(BrokerConstants.CA_STATE_CODE)){
					String ahbxSwitchAccountUrl = GhixConstants.SWITCH_URL_IND66;
					model.addAttribute(SWITCH_ACC_URL, ahbxSwitchAccountUrl);
					model.addAttribute(ENCRYPTED_ASSISTER_ID, ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(existingAssister.getId())));
				}
				//GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter("ASSISTER_ID", existingAssister.getId()));
				
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while showing dashboard: ", giexception);
			//GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while navigating to viewAssisterDashboard"));
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while showing dashboard in viewAssisterDashboard method: ", exception);
			//GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while navigating to viewAssisterDashboard"));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("AssisterController : viewAssisterDashboard : END");
		return "entity/assister/dashboard";
	}
	
	private String loadLanguages(){
		JsonArray languageJsonArr = null;
        LOGGER.info("getLanguageList : START");
        String jsonLangData = "";
       
	    List languageSpokenList = null;
	    
		try { 
		    languageSpokenList = lookupService.populateLanguageNames("");
		    languageJsonArr = new JsonArray();
		    for(int i=0;i<languageSpokenList.size();i++)
		    {
		    	String lan = EntityUtils.initializeAndUnproxy(languageSpokenList.get(i));
		    	if(lan!=null){
		    		languageJsonArr.add(new JsonPrimitive(lan));
		    	}
		    }
		    
		    jsonLangData = platformGson.toJson(languageJsonArr);
		} catch(Exception exception)
		{
			LOGGER.error("Exception occurred while loading Languages : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
        LOGGER.info("getLanguageList : END");
        return jsonLangData;
	}
	
	@RequestMapping(value = "/entity/assister/viewAssisterProfile/{encryptedAssisterId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'VIEW_CEC_PROFILE')")
	public String viewAssisterProfileById(Model model, @PathVariable(value = "encryptedAssisterId") String encryptedAssisterId) {
		LOGGER.info("View Assister Profile");
		model.addAttribute("page_title", "Getinsured Health Exchange:Individual Home Page");
		
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		
		try {
			String assisterId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encryptedAssisterId);
			assisterRequestDTO.setId(Integer.valueOf(assisterId));
			
			LOGGER.debug("REST Call starts to get assister details ");
			String assisterDetailsResponse = restClassCommunicator.getAssisterDetail(assisterRequestDTO);
			assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDetailsResponse);
			LOGGER.debug("REST Call ends");
			
			if(assisterResponseDTO!=null){
				// added to show Assister details from employer menu navigation
				model.addAttribute("assister", assisterResponseDTO.getAssister());
				//model.addAttribute("locationObj", brokerObj.getLocation());
			}
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while viewing assister profile : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while viewing assister profile in viewAssisterProfileById method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return "entity/assister/viewAssisterProfile";
	}
	
	/**
	 * @param firstName
	 *            First Name of Assister
	 * @param lastName
	 *            Last Name of Assister
	 * @param assisterId
	 *            Id of Assister
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation to appropriate result page
	 */
	//@GiAudit(transactionName = "Assister DeDeisgnate User Popup", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/dedesignateAssisterPopup", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'BROKER_DE_DESIGNATE_EMPLOYER')")
	public String dedesignateAssisterPopup(@RequestParam(FIRSTNAME) String firstName, @RequestParam(LASTNAME) String lastName,
			@RequestParam("assisterId") String assisterId, Model model) {

		LOGGER.info("dedesignateAssisterPopup : START");

		model.addAttribute("assisterName", firstName + ' ' + lastName);
		model.addAttribute("assisterId", assisterId);

		LOGGER.info("dedesignateAssisterPopup : END");

		return "entity/assister/dedesignateAssisterPopup";
	}
	
	//@GiAudit(transactionName = "Assister DeDesignate User", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/entity/assister/dedesignateAssister/{id}", method = { RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'BROKER_DE_DESIGNATE_EMPLOYER')")
	@ResponseBody
	public String dedesignateAssister(@PathVariable("id") String id, @ModelAttribute("assister") Assister assister,
			BindingResult result, Model model, HttpServletRequest request) throws GIException {

		LOGGER.info("dedesignateAssister : START");
		AccountUser user = null;
		Household houseHold = null;
		DesignateAssister designateAssister = null;
		AssisterRequestDTO assisterRequestDTO = new AssisterRequestDTO();
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;

		try {
			user = userService.getLoggedInUser();
			String isUserSwitchToOtherView = (String) request.getSession().getAttribute(IS_USER_SWITCH_TO_OTHER_VIEW);
			
			if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.equalsIgnoreCase(BrokerConstants.YES)){
				
				assisterRequestDTO.setIndividualId(user.getActiveModuleId());
				houseHold = getHouseHoldById(user.getActiveModuleId());				
			}
			else
			{										
					houseHold = getHouseHoldByUserId(user.getId());
					if(houseHold != null){
						assisterRequestDTO.setIndividualId(houseHold.getId());						
					}				
			}
									
			assisterDesignateResponseDTO = retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			if(assisterDesignateResponseDTO!=null) {
				designateAssister = assisterDesignateResponseDTO.getDesignateAssister();
				assister = assisterDesignateResponseDTO.getAssister();
				
				if (designateAssister != null) {
					AssisterRequestDTO assisterRequestDTOObj2 = new AssisterRequestDTO();
					assisterRequestDTOObj2.setDesignateAssister(designateAssister);
					designateAssister.setStatus(Status.InActive);
					//designateService.saveDedesignateBroker(designateBroker, brokerObj.getUser());
					designateAssister = saveDesignateAssisterInformation(assisterRequestDTOObj2);
					if(DynamicPropertiesUtil.getPropertyValue(AEEConfigurationEnum.ENROLLMENT_COUNSELOR_INDIVIDUAL_DEDESIGNATION_NOTICE).equalsIgnoreCase("Y"))
					{
						sendNotificationMailToEnrollmentCounselorOfIndividual(assister,houseHold);
					}
					request.getSession().setAttribute(DESIGNATED_ASSISTER_PROFILE, assister);
					request.getSession().setAttribute(DESIGNATE_ASSISTER, designateAssister);
												
				}
			}				
		} catch(GIRuntimeException giexception){
			LOGGER.error("GIRuntimeException occured while de-designating assister : ", giexception);
			throw giexception;
		} catch(Exception exception){
			LOGGER.error("Exception occured while de-designating assister in dedesignateAssister method: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("dedesignateAssister : END");
		return "success";
		
	}
	private void sendNotificationMailToEnrollmentCounselorOfIndividual(Assister currAssister, Household household)
	{
		int counselorId=currAssister.getId();
		List<String> emailList=new LinkedList<String>();
		Map<String, Object> templateMap=new HashMap<String, Object>();
		try{
		emailList.add(currAssister.getEmailAddress());
		String noticeTemplateName = "Enrollment Counselor De-designation Notice (by Individual)";
		String fileName = "EnrollmentCounselorDedesignationNoticebyIndividual" + TimeShifterUtil.currentTimeMillis() + PDF;
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("MMMMMMMMM dd, yyyy");
		templateMap.put(NOTIFICATION_DATE, simpleDateFormat.format(new TSDate()));
		templateMap.put(CECFN, currAssister.getFirstName());
	    templateMap.put(CECLN, currAssister.getLastName());
		templateMap.put(INDIVIDUAL_FN, household.getFirstName());
		String individualName = household.getFirstName();
		if(household.getMiddleName()!=null && !household.getMiddleName().isEmpty()){
			templateMap.put(INDIVIDUAL_MN, household.getMiddleName());
			individualName = individualName + " " + household.getMiddleName();
		}else{
			templateMap.put(INDIVIDUAL_MN, "");
		}
		individualName = individualName + " " + household.getLastName();
		templateMap.put(INDIVIDUAL_LN, household.getLastName());
		templateMap.put(EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
        templateMap.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
        templateMap.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
        String assisterFullName = currAssister.getUser() != null ? currAssister.getUser().getFullName() : getFullName(currAssister)  ;
		noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, templateMap, 
				ECMRELATIVEPATH, fileName, "assister", counselorId, emailList, individualName, assisterFullName);
	}
	 catch(Exception exception) {
			LOGGER.error("Exception occurred while sending notification :: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}
	
	private String getFullName(Assister assister) {
		StringBuffer fullName = new StringBuffer();
		if (assister.getFirstName() != null
				&& assister.getFirstName().trim().length() > 0) {
			fullName.append(assister.getFirstName().trim());
		}

		if ( assister.getLastName()  != null
				&& assister.getLastName().trim().length() > 0) {
			fullName.append(" ").append(assister.getLastName().trim());
		}
		return fullName.toString();
	}
	
	private Household getHouseHoldByUserId(int userId){
		Household household = null;
		XStream xstream = null;
		LOGGER.debug("Rest call to get householeId starts");
		//get the household using rest call
		try{
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_USER_ID, String.valueOf(userId), String.class);
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				household = (Household) xstream.fromXML(response);
			}
		}catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("rest call to get householeId ends");
		return household;
	}

	/**
	 * Method to retrieve Assister designation detail by individual identifier.
	 * 
	 * @param assisterRequestDTO The AssisterRequestDTO instance.
	 * @return assisterDesignateResponseDTO The AssisterDesignateResponseDTO instance.
	 * @throws Exception 
	 */
	private AssisterDesignateResponseDTO retrieveAssisterDesignationDetailByIndividualId(
			AssisterRequestDTO assisterRequestDTO) throws GIException {
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = null;
		LOGGER.info("REST call start to the GHIX-ENITY module to retreive Assister Designation details.");
		
		try {
			assisterDesignateResponseDTO = restClassCommunicator.retrieveAssisterDesignationDetailByIndividualId(assisterRequestDTO);
			
			if(assisterDesignateResponseDTO != null && assisterDesignateResponseDTO.getResponseCode()==EntityConstants.RESPONSE_CODE_FAILURE){				
					throw new Exception();
			}	
			
			LOGGER.debug("REST call Ends to the GHIX-ENITY module.");
		} catch(Exception ex){
			LOGGER.error("Exception in retrieving designatedAssister details, REST response code is: "+
					((assisterDesignateResponseDTO!=null)?assisterDesignateResponseDTO.getResponseCode():"assisterDesignateResponseDTO is null"));
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return assisterDesignateResponseDTO;
	}
	
	private DesignateAssister saveDesignateAssisterInformation(AssisterRequestDTO assisterRequestDTO) throws JsonProcessingException, IOException{
		AssisterResponseDTO assisterResponseDTO =null;
		LOGGER.info("REST call start to the GHIX-ENITY module to save Assister Designation details.");
		String assisterDesigResponse = restClassCommunicator.saveAssisterDesignation(assisterRequestDTO);
		assisterResponseDTO = JacksonUtils.getJacksonObjectReader(AssisterResponseDTO.class).readValue(assisterDesigResponse);
		LOGGER.info("REST call ends to the GHIX-ENITY module to save Assister Designation details.");
		return assisterResponseDTO.getDesignateAssister();
	}
	
	/**
	 * Method to get Household for given identifier.
	 *
	 * @param householdId The Household identifier.
	 * @return household The Household instance.
	 */
	private Household getHouseHoldById(int householdId){
		Household household = null;
		XStream xstream = null;
		LOGGER.debug("Rest call to getHouseHoldById starts");

		try{
			String response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_ID, String.valueOf(householdId), String.class);
			xstream = GhixUtils.getXStreamStaxObject();
			if(response != null){
				ConsumerResponse consumerResponse = (ConsumerResponse) xstream.fromXML(response);

				if(null != consumerResponse) {
					household = consumerResponse.getHousehold();
				}
			}
		}
		catch(Exception ex){
			LOGGER.error("Rest call to find household failed", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			LOGGER.debug("rest call to getHouseHoldById ends");
		}

		return household;
	}
	
	@RequestMapping(value = "/assister/checkEmail")
	@ResponseBody
	public Boolean checkEmail(@RequestParam String newEmail, @RequestParam String oldEmail) {
		LOGGER.info("checkEmail : START");
		
		if (newEmail != null && newEmail.equalsIgnoreCase(oldEmail)) {
			return false;
		}
		
		if(checkEmailForUsers(newEmail)){
			return true;
		}
		
		if(checkAssisterForEmail(newEmail)){
			return true;
		}
		
		LOGGER.info("checkEmail : END");
		return false;
	}
	
	private Boolean checkAssisterForEmail(String email) {
		String userExist = restClassCommunicator.AssisterForEmail(email.toLowerCase());
		return Boolean.valueOf(userExist);
	}
	
	private Boolean checkEmailForUsers(String email) {
		AccountUser emailObj = null;
		try {
			if (isScimEnabled) {
				emailObj = scimUserManager.findByEmail(email.toLowerCase());
			} else {
				emailObj = userService.findByUserName(email.toLowerCase());
			}
		} catch (Exception e) {
			emailObj = null;
		}

		if (emailObj != null) {
			LOGGER.info("checkEmail : END");
			return true;
		}
		return false;
	}
}
