package com.getinsured.iex.hub.nmec.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.MECCoverageType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OrganizationCodeType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.OtherCoverageType;
import com.getinsured.iex.hub.nmec.hhs.cms.dsh.sim.ee.vnem.ResponseMetadataType;

public class OtherCoverageTypeWrapper implements JSONAware {

	private String organizationCode;
	private ResponseMetadataWrapper responseMetadataWrapper;
	private MECCoverageTypeWrapper mecCoverageTypeWrapper;
	
	public OtherCoverageTypeWrapper(OtherCoverageType otherCoverageType)
	{
		if(otherCoverageType != null)
		{
			OrganizationCodeType organizationCodeType = otherCoverageType.getOrganizationCode();
			
			if(organizationCodeType != null)
			{
				organizationCode = organizationCodeType.getValue().value();
			}
			MECCoverageType mecCoverageType = otherCoverageType.getMECCoverage();
			
			if(mecCoverageType != null)
			{
				mecCoverageTypeWrapper = new MECCoverageTypeWrapper(mecCoverageType);
			}
			
			ResponseMetadataType metadata =  otherCoverageType.getResponseMetadata();
			
			if(metadata != null)
			{
				responseMetadataWrapper = new ResponseMetadataWrapper(metadata);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("OrganizationCode", this.organizationCode);
		obj.put("MECCoverageType", this.mecCoverageTypeWrapper);
		obj.put("ResponseMetadata", this.responseMetadataWrapper);
		return obj.toJSONString();
	}
}
