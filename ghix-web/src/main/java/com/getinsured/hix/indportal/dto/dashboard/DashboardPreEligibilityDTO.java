package com.getinsured.hix.indportal.dto.dashboard;

import java.util.List;

public class DashboardPreEligibilityDTO {

	private String aptc;
	private String csrLevel;
	private boolean aptcEligible;
	private boolean csrEligible;
	private List<DashboardMemberDTO> members;
	private List<DashboardPlanDTO> plans;

	public String getAptc() {
		return aptc;
	}

	public void setAptc(String aptc) {
		this.aptc = aptc;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public List<DashboardMemberDTO> getMembers() {
		return members;
	}

	public void setMembers(List<DashboardMemberDTO> members) {
		this.members = members;
	}

	public List<DashboardPlanDTO> getPlans() {
		return plans;
	}

	public void setPlans(List<DashboardPlanDTO> plans) {
		this.plans = plans;
	}

	public boolean getAptcEligible() {
		return aptcEligible;
	}

	public void setAptcEligible(boolean aptcEligible) {
		this.aptcEligible = aptcEligible;
	}

	public boolean getCsrEligible() {
		return csrEligible;
	}

	public void setCsrEligible(boolean csrEligible) {
		this.csrEligible = csrEligible;
	}

}
