package com.getinsured.hix.provider.search;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.provider.service.ManageFacilitiesDTO;
import com.getinsured.hix.provider.service.ManageProvidersDTO;
import com.getinsured.hix.provider.service.zipcode.ZipCode;

@Service(value = "solrProxy")
public class SolrProxyImpl implements SolrProxy {

	@Autowired
	@Qualifier("solrProviderServer")
	private SolrServer solrServer;

	@Autowired
	@Qualifier("providerSolrServer")
	private SolrServer providerSolrServer;

	public static enum  PROVIDER_FIELD {
		group_key_prac_key, provider_type_indicator, first_name, middle_name, last_name, facility_name, specialty_name, network_id;
	}

	private static final Logger LOGGER = LoggerFactory.getLogger(SolrProxyImpl.class);
	private static final String STATE_CODE_FOR_CA = "CA";
	private static final boolean HAS_PROVIDER_TYPE = true;

	// Caller should cache the size of the provider list returned by this call
	// and increment it by one for next call
	// for "start" parameter
	public String findProviders(String searchKey, ZipCode userZip,
			int pageSize, int start, String lanugage, String gender,
			float distance, String providerType, String searchType, String providerSpecialty, String networkId) {
		ProviderQuery pQuery = new ProviderQuery();
		
		if (StringUtils.isNotEmpty(searchType)){
			if((searchType.equalsIgnoreCase(ProviderQuery.DENTIST)) || (searchType.equalsIgnoreCase(ProviderQuery.DOCTOR))) {
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}else if(searchType.equalsIgnoreCase(ProviderQuery.HOSPITAL)){
				pQuery.setProviderClass(ProviderQuery.FACILITY);
			}else{
				//Default
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}
		}else{
			pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
		}
		
		
		
		/*if (StringUtils.isNotEmpty(providerType)){
			if(providerType.equalsIgnoreCase("individual")) {
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}else if(providerType.equalsIgnoreCase("facility")){
				pQuery.setProviderClass(ProviderQuery.FACILITY);
			}else{
				//Default
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}
		}else{
			pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
		}*/
		
		if (StringUtils.isNotEmpty(gender) && !gender.equals("all")) {
			pQuery.setGenderFilter(gender);
			
		}
		pQuery.setSearchType(searchType);

		if (StringUtils.isNotEmpty(lanugage) && !lanugage.equals("all")) {
			pQuery.setLanguageFilter(lanugage);
		}
		
		if (StringUtils.isNotEmpty(providerSpecialty) && !providerSpecialty.equals("all")) {
			pQuery.setSpecialtyFilter(providerSpecialty);
		}
		if (StringUtils.isNotEmpty(networkId)) {
			pQuery.setNetworkId(networkId);
		}
		
		pQuery.setUserZipCode(userZip);

		if (distance == 0.0f) {
			/*
			 * If distance not provided search provider within 25 miles of user
			 * location
			 */
			distance = 25f;
		}
		// Convert miles into KM, SOLR understands KM only
		pQuery.setProxymity((float) (distance * 1.60));
		pQuery.setResultFormat("json");
		QueryResponse response = null;
		ProviderSearchResult result = null;
		try {
			response = solrServer.query(pQuery.getQuery(searchKey, pageSize,
					start));
			result = new ProviderSearchResult(response);
			result.setCurrentStart(start);
			result.setPageSize(pageSize);
		} catch (SolrServerException e) {
			LOGGER.error("Failed to execute search", e);
			result = new ProviderSearchResult("Failed to execute search for "
					+ searchKey + "::" + e.getMessage());
		}
		String tmp = result.toJSONString();
		LOGGER.debug("Response:" + tmp);
		return tmp;
	}

	@Override
	public String findProvidersByCity(String searchKey, String userCity,
			int pageSize, int start, String lanugage, String gender,
			String providerType, String searchType, String providerSpecialty) {
		ProviderQuery pQuery = new ProviderQuery();
		if (StringUtils.isNotEmpty(providerType)){
			if(providerType.equalsIgnoreCase("individual")) {
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}else if(providerType.equalsIgnoreCase("facility")){
				pQuery.setProviderClass(ProviderQuery.FACILITY);
			}else{
				//Default
				pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
			}
		}else{
			pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
		}
		if (StringUtils.isNotEmpty(gender) && !gender.equals("all")) {
			pQuery.setGenderFilter(gender);
			
		}
		
		if (StringUtils.isNotEmpty(lanugage) && !lanugage.equals("all")) {
			pQuery.setLanguageFilter(lanugage);
		}
		if (StringUtils.isNotEmpty(providerSpecialty) && !providerSpecialty.equals("all")) {
			pQuery.setSpecialtyFilter(providerSpecialty);
		}
		pQuery.setProviderCity(userCity);
		pQuery.setResultFormat("json");
		QueryResponse response = null;
		ProviderSearchResult result = null;
		try {
			response = solrServer.query(pQuery.getQuery(searchKey, pageSize,
					start));
			result = new ProviderSearchResult(response);
			result.setCurrentStart(start);
			result.setPageSize(pageSize);
		} catch (SolrServerException e) {
			LOGGER.error("Failed to execute search", e);
			result = new ProviderSearchResult("Failed to execute search for "
					+ searchKey + "::" + e.getMessage());
		}
		String tmp = result.toJSONString();
		LOGGER.debug("Response:" + tmp);
		return tmp;
	}

	public String findProvidersDetails(String groupKey, ZipCode userZip) {

		ProviderDetailsQuery pQuery = new ProviderDetailsQuery();
		/*
		 * Commenting Provider type filter to search both individual and
		 * facility data pQuery.setProviderClass(ProviderQuery.INDIVIDUAL);
		 */
		pQuery.setProxymity(10);
		pQuery.setResultFormat("json");
		pQuery.setUserZipCode(userZip);
		QueryResponse response = null;
		ProviderDetailsResult result = null;
		try {
			response = solrServer.query(pQuery.getQuery(groupKey));
			result = new ProviderDetailsResult(response);
		} catch (SolrServerException e) {
			result = new ProviderDetailsResult("Failed to execute search for "
					+ groupKey + "::" + e.getMessage());
		}
		String tmp = result.toJSONString();
		LOGGER.info(tmp);
		return tmp;

	}

	/**
	 * Method is used to get SOLR Query for Provider Data.
	 */
	private SolrQuery getSolrQueryForProviderData(boolean hasProviderType, String nameFilter, String facilityFilter,
			String specialityFilter, String networkIdFilter, int page, int size) {

		final String OR_STR = " OR ";

		List<String> filterQueries = new ArrayList<String>();
		String providerType = null;
		String name = null;
		String facility = null;

		if (hasProviderType) {
			providerType = getFieldFilter(PROVIDER_FIELD.provider_type_indicator.name(), "P") + OR_STR + getFieldFilter(PROVIDER_FIELD.provider_type_indicator.name(), "D");

			if (StringUtils.isNotBlank(nameFilter)) {
				name = getLikeFieldFilter(PROVIDER_FIELD.first_name.name(), nameFilter) + OR_STR
						+ getLikeFieldFilter(PROVIDER_FIELD.middle_name.name(), nameFilter) + OR_STR
						+ getLikeFieldFilter(PROVIDER_FIELD.last_name.name(), nameFilter);
				filterQueries.add(name);
			}
		}
		else {
			providerType = getFieldFilter(PROVIDER_FIELD.provider_type_indicator.name(), "H");

			if (StringUtils.isNotBlank(facilityFilter)) {
				facility = getLikeFieldFilter(PROVIDER_FIELD.facility_name.name(), facilityFilter);
				filterQueries.add(facility);
			}
		}
		filterQueries.add(providerType);

		String speciality = StringUtils.isNotBlank(specialityFilter) ? getLikeFieldFilter(PROVIDER_FIELD.specialty_name.name(), specialityFilter) : null;
		String networkId = StringUtils.isNotBlank(networkIdFilter) ? getLikeFieldFilter(PROVIDER_FIELD.network_id.name(), networkIdFilter) : null;

		if (null != speciality) {
			filterQueries.add(speciality);
		}

		if (null != networkId) {
			filterQueries.add(networkId);
		}
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setQuery("*:*");
		solrQuery.setFilterQueries(convertStringListToArray(filterQueries));

		if (hasProviderType) {
			solrQuery.setFields(PROVIDER_FIELD.group_key_prac_key.name(), PROVIDER_FIELD.first_name.name(),
					PROVIDER_FIELD.middle_name.name(), PROVIDER_FIELD.last_name.name(),
					PROVIDER_FIELD.specialty_name.name(), PROVIDER_FIELD.network_id.name());
		}
		else {
			solrQuery.setFields(PROVIDER_FIELD.group_key_prac_key.name(), PROVIDER_FIELD.facility_name.name(),
					PROVIDER_FIELD.specialty_name.name(), PROVIDER_FIELD.network_id.name());
		}
		solrQuery.setStart(page);
		solrQuery.setRows(size);
		return solrQuery;
	}

	private String getFieldFilter(String field, String value) {
		return field + ":" + value;
	}

	private String getLikeFieldFilter(String field, String value) {
		return field + ":*" + value + "*";
	}

	/**
	 * Method is used to convert String List to Array.
	 */
	private String[] convertStringListToArray(List<String> listOfString) {

		String[] strResult = new String[listOfString.size()];
		listOfString.toArray(strResult);
		return strResult;
	}

	/**
	 * Method is used to get Provider Data Count from SOLR.
	 */
	@Override
	public long getProviderDataCount(boolean hasProviderType, String nameFilter, String facilityFilter, String specialityFilter, String networkIdFilter) {

		LOGGER.debug("getProviderDataCount() End");
		long providerDataCount = 0;

		try {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			SolrQuery query = getSolrQueryForProviderData(hasProviderType, nameFilter, facilityFilter, specialityFilter, networkIdFilter, 0, 0);
			QueryResponse response = null;

			if (STATE_CODE_FOR_CA.equalsIgnoreCase(stateCode)) {
				response = solrServer.query(query);
			}
			else {
				response = providerSolrServer.query(query);
			}
			providerDataCount = response.getResults().getNumFound();
		}
		catch (SolrServerException ex) {
			LOGGER.error("Failed to get Provider Data Count from SOLR : ", ex);
		}
		catch (Exception ex) {
			LOGGER.error("Failed to get Provider Data Count : : ", ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("getProviderDataCount() End with Provider Data Count: " + providerDataCount);
			}
		}
		return providerDataCount;
	}

	/**
	 * Method is used to get Provider Data List from SOLR.
	 */
	@Override
	public List<ManageProvidersDTO> getProviderDataList(String nameFilter, String specialityFilter, String networkIdFilter, int page, int size) {

		LOGGER.debug("getProviderDataList() Start");
		List<ManageProvidersDTO> providerDTOList = null;

		try {

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			SolrQuery query = getSolrQueryForProviderData(HAS_PROVIDER_TYPE, nameFilter, null, specialityFilter, networkIdFilter, page, size);
			QueryResponse response = null;

			if (STATE_CODE_FOR_CA.equalsIgnoreCase(stateCode)) {
				response = solrServer.query(query);
			}
			else {
				response = providerSolrServer.query(query);
			}
			SolrDocumentList results = response.getResults();

			if (0 < results.size()) {

				final String SPACE = " ";
				StringBuffer sbName = null;
				SolrDocument solrDocument = null;
				ManageProvidersDTO providerDTO = null;
				Object firstName = null, middleName = null, lastName = null;
				providerDTOList = new ArrayList<ManageProvidersDTO>();

		        for (int i = 0; i < results.size(); i++) {

		        	solrDocument = results.get(i);
		        	sbName = new StringBuffer();
		        	firstName = solrDocument.get(PROVIDER_FIELD.first_name.name());
		        	middleName = solrDocument.get(PROVIDER_FIELD.middle_name.name());
		        	lastName = solrDocument.get(PROVIDER_FIELD.last_name.name());

		        	if (null != firstName) {
		        		sbName.append(firstName.toString().trim());
		        	}

		        	if (null != middleName && 0 < middleName.toString().trim().length()) {
		        		sbName.append(SPACE).append(middleName.toString().trim());
		        	}

		        	if (null != lastName) {
		        		sbName.append(SPACE).append(lastName.toString().trim());
		        	}
		        	providerDTO = new ManageProvidersDTO();
		        	providerDTO.setProviderField(PROVIDER_FIELD.group_key_prac_key.name(), solrDocument.get(PROVIDER_FIELD.group_key_prac_key.name()));
		        	providerDTO.setProviderField("name", sbName.toString().trim());
		        	providerDTO.setProviderField(PROVIDER_FIELD.specialty_name.name(), solrDocument.get(PROVIDER_FIELD.specialty_name.name()));
		        	providerDTO.setProviderField(PROVIDER_FIELD.network_id.name(), solrDocument.get(PROVIDER_FIELD.network_id.name()));
//			        providerDTO.setProviderField("unknownNetworkIds", solrDocument.get("unknownNetworkIds"));
		        	providerDTOList.add(providerDTO);
		        }

		        if (LOGGER.isDebugEnabled()) {
		        	LOGGER.debug("Provider Data List size: " + providerDTOList.size());
		        }
			}
		}
		catch (SolrServerException ex) {
			LOGGER.error("Failed to get Provider Data List from SOLR : ", ex);
		}
		catch (Exception ex) {
			LOGGER.error("Failed to execute search: ", ex);
		}
		finally {
			LOGGER.debug("getProviderDataList() End");
		}
		return providerDTOList;
	}

	/**
	 * Method is used to get Facility Data List from SOLR.
	 */
	@Override
	public List<ManageFacilitiesDTO> getFacilityDataList(String facilityFilter, String specialityFilter, String networkIdFilter, int page, int size) {

		LOGGER.debug("getFacilityDataList() Start");
		List<ManageFacilitiesDTO> facilityDTOList = null;

		try {

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			SolrQuery query = getSolrQueryForProviderData(!HAS_PROVIDER_TYPE, null, facilityFilter, specialityFilter, networkIdFilter, page, size);
			QueryResponse response = null;

			if (STATE_CODE_FOR_CA.equalsIgnoreCase(stateCode)) {
				response = solrServer.query(query);
			}
			else {
				response = providerSolrServer.query(query);
			}
			SolrDocumentList results = response.getResults();

			if (0 < results.size()) {

				SolrDocument solrDocument = null;
				ManageFacilitiesDTO facilityDTO = null;
				facilityDTOList = new ArrayList<ManageFacilitiesDTO>();

		        for (int i = 0; i < results.size(); i++) {

		        	solrDocument = results.get(i);
		        	facilityDTO = new ManageFacilitiesDTO();
		        	facilityDTO.setFacilityField(PROVIDER_FIELD.group_key_prac_key.name(), solrDocument.get(PROVIDER_FIELD.group_key_prac_key.name()));
		        	facilityDTO.setFacilityField(PROVIDER_FIELD.facility_name.name(), solrDocument.get(PROVIDER_FIELD.facility_name.name()));
		        	facilityDTO.setFacilityField(PROVIDER_FIELD.specialty_name.name(), solrDocument.get(PROVIDER_FIELD.specialty_name.name()));
		        	facilityDTO.setFacilityField(PROVIDER_FIELD.network_id.name(), solrDocument.get(PROVIDER_FIELD.network_id.name()));
//			        facilityDTO.setFacilityField("unknownNetworkIds", solrDocument.get("unknownNetworkIds"));
		        	facilityDTOList.add(facilityDTO);
		        }

		        if (LOGGER.isDebugEnabled()) {
		        	LOGGER.debug("Facility Data List size: " + facilityDTOList.size());
		        }
			}
		}
		catch (SolrServerException ex) {
			LOGGER.error("Failed to get Facility Data List from SOLR : ", ex);
		}
		catch (Exception ex) {
			LOGGER.error("Failed to execute search: ", ex);
		}
		finally {
			LOGGER.debug("getFacilityDataList() End");
		}
		return facilityDTOList;
	}

	public SolrServer getSolrServer() {
		return solrServer;
	}

	public void setSolrServer(SolrServer solrServer) {
		this.solrServer = solrServer;
	}

	public SolrServer getProviderSolrServer() {
		return providerSolrServer;
	}

	public void setProviderSolrServer(SolrServer providerSolrServer) {
		this.providerSolrServer = providerSolrServer;
	}
}
