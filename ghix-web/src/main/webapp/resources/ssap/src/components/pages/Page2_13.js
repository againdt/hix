import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import moment from 'moment';
import _ from 'lodash';

// Components

import PageHeader from '../headers/PageHeader';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import BLOOD_RELATIONSHIP_TYPES, { BLOOD_RELATIONSHIP_CODES, BLOOD_RELATIONSHIP_MIRROR_CODES } from '../../constants/bloodRelationshipCodesList';
import CONFIG from '../../.env';
import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';


/**
 *
 * @param {Object} props
 * @returns {React.Component} - Page 'Blood Relationship'
 *
 */
const Page2_13  = props => {


    const {
        form: {
            Form: {
                isEligiblyForSave
            }
        },
        householdMembers,
        householdMembers: {
            length : qtyOfHouseholdMembers
        },
        relations: {
            dropdownOptions,
            loadedRelations
        }
    } = props;

    const { bloodRelationship } = props.householdMembers[0];

    const spouseCode = (relation => relation ? relation.code : null)(loadedRelations.find(lr => lr.type === BLOOD_RELATIONSHIP_TYPES.SPOUSE));

    useEffect(() => {
        const {
            householdMembers: {
                length: qtyOfHouseholdMembers
            },
            headOfHousehold: {
                bloodRelationship: existingBloodRelations,
                bloodRelationship: {
                    length: qtyOfBloodRelations
                }
            }
        } = props;

        let updatedBloodRelations = [];
        let qtyOfDescribedHouseholdMembers = Math.sqrt(qtyOfBloodRelations);

        /**
         * Here we estimate if all household members are described in <bloodRelationship> Array (in the head of household) or not
         * If not we generate needed qty of describing relations objects and inject in this Array
         */

        if (qtyOfHouseholdMembers !== qtyOfDescribedHouseholdMembers) {
            updatedBloodRelations.push(...generateBloodRelations(existingBloodRelations));
            updateBloodRelationsAfterGenerating(updatedBloodRelations);
        }
    }, []);

    /**
     *
     * @param {Array} updatedBloodRelations - Array of objects after generation, each of them describes relation:
     * a) between two household members;
     * b) household member to himself
     *
     * If this function is invoked it updates <bloodRelationship> Array (in the head of household)
     */
    const updateBloodRelationsAfterGenerating = updatedBloodRelations => {
        let newState = {...props.data};

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[0]
            .bloodRelationship = updatedBloodRelations;

        props.actions.changeData(newState);
    };

    /**
     *
     * @param {Array} existingBloodRelations - Array of objects in existing JSON, each of them describes relation:
     * a) between two household members;
     * b) household member to himself
     * @returns {Array} - Array of objects, each of them describes relation
     *
     * Because of logic that we need to describe relation of each household member to himself and to each
     * another household member we need Qty of household members powered by 2 objects
     *
     *  For example:
     *  1 household member - 1 object (to himself);
     *  2 household members - 4 objects (1st to 2nd, 1st to himself, 2nd to 1st, 2nd to himself)
     *  and so on...
     */
    const generateBloodRelations = existingBloodRelations => {
        let relations = [...existingBloodRelations];

        // here we iterate through all of household members
        props.householdMembers.forEach((member, index) => {

            // Here we check is this household member already described
            const describedRelation =
                existingBloodRelations
                    .find(m => m.individualPersonId === String(index + 1) && m.relatedPersonId === String(index + 1));

            // If household member isn't described we add here relation of this household member to himself
            if (!describedRelation) {
                relations.push({
                    'individualPersonId': String(index + 1),
                    'relatedPersonId': String(index + 1),
                    'relation': '18'
                });
            }

            // Here we iterate again to add relations of this household member to other household members
            props.householdMembers.forEach((m , i) => {

                const describedRelation =
                    existingBloodRelations
                        .find(m => m.individualPersonId === String(index + 1) && m.relatedPersonId === String(i + 1));

                const getRelation = (individualPersonId, relatedPersonId) => {
                    const reflectedPerson = existingBloodRelations
                        .find(br => br.individualPersonId === individualPersonId && br.relatedPersonId === relatedPersonId);

                    if(!reflectedPerson) {
                        return null;
                    } else {
                        return (mirrorType => loadedRelations.find(rel => rel.type === mirrorType).code)(loadedRelations.find(rel => rel.code === reflectedPerson.relation).mirrorType);
                    }
                };

                if (!describedRelation) {
                    if (i !== index) {
                        relations.push({
                            'individualPersonId': String(index + 1),
                            'relatedPersonId': String(i + 1),
                            'relation': getRelation(String(i + 1), String(index + 1))
                        });
                    }
                }
            });

        });

        relations.map(relation => ({
            ...relation,
            name: `relation_${relation.individualPersonId}_to_${relation.relatedPersonId}`,
        }));

        return relations;
    };

    /**
     *
     * @param {Array} householdMembers - Array of all household members
     * @returns {Array} - Array of Lis with names of household members
     */
    const getHouseholdMembersList = householdMembers => {

        return householdMembers.map((member, i) =>
            <li key={`${member.name.firstName}_${i}`}>
                {normalizeNameOfPerson(member.name)}
            </li>
        );
    };

    /**
     *
     * @param {Array} args -    newData: updated relation (result of merging updated dropdown value and initial relation object)
     *                          value: actual relation code
     *
     * Replaces relation in <bloodRelationship> Array with updated relation
     */
    const onFieldChange = (...args) => {

        const [ newData, , , [ , value ] ] = args;

        /**
         *
         *  Here we rewrite to <relation> property the value from unique property in <bloodRelation> object
         */
        const updatedRelationPropData = {
            ...newData,
            relation: newData[`relation_${newData.individualPersonId}_to_${newData.relatedPersonId}`],
        };

        /**
         *
         * @param {string, null} relation - code of relation, that should be 'mirrored'
         * @returns {string, null} 'mirrored' relation - code of relation that matches initial value
         */
        const provideMirroredRelation = relation => {

            if(CONFIG.ENV.DEVELOPMENT) {
                return BLOOD_RELATIONSHIP_MIRROR_CODES[relation];
            }

            const mirrorType = loadedRelations.find(lr => lr.code === relation).mirrorType;

            return relation === null ?
                null :
                loadedRelations.find(lr => lr.type === mirrorType).code;
        };

        const newDataReversed = {
            ...updatedRelationPropData,
            individualPersonId: updatedRelationPropData.relatedPersonId,
            relatedPersonId: updatedRelationPropData.individualPersonId,
            relation: provideMirroredRelation(updatedRelationPropData.relation)
        };

        const newState = {...props.data };

        // Cleaning up <spouse> selection if the same has been just chosen and related to previous selections

        if(value === spouseCode) {

            const result = [];

            const indexes = [];

            const bloodRelationships = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[0]
                .bloodRelationship;

            bloodRelationships
                .forEach((br, i) => {
                    if(br.individualPersonId === newData.individualPersonId && br.relation === spouseCode) {
                        indexes.push(i);
                    }
                    if(br.relatedPersonId === newData.individualPersonId && br.relation === spouseCode) {
                        indexes.push(i);
                    }
                    if(br.relatedPersonId === newData.relatedPersonId && br.relation === spouseCode) {
                        indexes.push(i);
                    }
                    if(br.individualPersonId === newData.relatedPersonId && br.relation === spouseCode) {
                        indexes.push(i);
                    }
                });

            bloodRelationships
                .map((br, i) => {

                    if(indexes.some(idx => idx === i)) {
                        result.push({
                            ...br,
                            relation: null
                        });
                    } else {
                        result.push(br);
                    }

                });

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[0]
                .bloodRelationship = result;
        }

        const updatedBloodRelationship = newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[0]
            .bloodRelationship
            .map(bloodRelation =>
                (bloodRelation.individualPersonId === updatedRelationPropData.relatedPersonId && // Here we found 'mirrored' relation
                 bloodRelation.relatedPersonId === updatedRelationPropData.individualPersonId) ?
                    newDataReversed :
                    (bloodRelation.individualPersonId === updatedRelationPropData.individualPersonId && // Here we found the same relation
                    bloodRelation.relatedPersonId === updatedRelationPropData.relatedPersonId) ?
                        updatedRelationPropData :
                        bloodRelation
            )
            .map(bloodRelation => _.omit(bloodRelation, // Here we clean up <bloodRelation> from unneeded properties
                [
                    `relation_${bloodRelation.individualPersonId}_to_${bloodRelation.relatedPersonId}`,
                    `relation_${bloodRelation.relatedPersonId}_to_${bloodRelation.individualPersonId}`,
                ]));

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[0]
            .bloodRelationship = updatedBloodRelationship;

        newState
            .singleStreamlinedApplication
            .taxHousehold[0] = {
                ...newState.singleStreamlinedApplication.taxHousehold[0],
                primaryTaxFilerPersonId: 0,
                householdMember: newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember.map(hhm => {
                        return {
                            ...hhm,
                            householdId: 0,
                            taxHouseholdComposition: {
                                ...EMPTY_OBJECTS.HOUSEHOLD_COMPOSITION
                            }
                        };
                    })
            };

        props.actions.changeData(newState);
    };

    Page2_13.checkDependants = () => {

        if(!isEligiblyForSave) {
            return null;
        }

        const exceptionCodes = [];

        if(CONFIG.ENV.PRODUCTION) {
            loadedRelations.forEach(lr => {
                if(lr.type === BLOOD_RELATIONSHIP_TYPES.WARD_OF_COURT_APPOINTED_GUARDIAN ||
                    lr.type === BLOOD_RELATIONSHIP_TYPES.SPOUSE) {
                    exceptionCodes.push(lr.code);
                }
            });
        } else {
            exceptionCodes.push(BLOOD_RELATIONSHIP_CODES.SPOUSE, BLOOD_RELATIONSHIP_CODES.COURT_APPOINTED_GUARDIAN);
        }


        const listOfMembers = [];

        householdMembers.forEach((member, i) => {
            const {
                dateOfBirth
            } = member;

            const isShouldBeSelected = bloodRelationship.find(br => {
                const { individualPersonId, relatedPersonId, relation } = br;
                return (
                    individualPersonId === String(i + 1) &&
                    relatedPersonId === '1' && !exceptionCodes.some(exCode => exCode === relation)
                );
            });

            if(moment().diff(dateOfBirth, 'years') > 25 && i !== 0 && isShouldBeSelected) {
                listOfMembers.push(member);
            }
        });
        return listOfMembers;
    };

    return (
        <Fragment>
            <div id="fhRelationship" className="fade-in">
                <div className="subsection">
                    <PageHeader text="Household Relationship Details"/>
                    <div className="usa-width-one-whole">
                        <div className="usa-alert usa-alert-info alert--info">
                            <div className="usa-alert-body" >
                                <p className="usa-alert-text">
                                    {
                                        `Here are the ${qtyOfHouseholdMembers} members in your household. Please answer the
                                            following questions to help us establish everyone's relationship to each other.`
                                    }
                                </p>
                                <ol>
                                    {
                                        getHouseholdMembersList(householdMembers)
                                    }
                                </ol>
                            </div>
                        </div>
                    </div>
                    <Form>
                        {
                            householdMembers.map((member, i) => {

                                if(i !== qtyOfHouseholdMembers - 1) {
                                    return (
                                        <Fragment key={`${member.name.firstName}_${i}`}>

                                            <div className="usa-width-one-whole margin-tb-20">
                                                <p>
                                                    {i + 1}. How is &nbsp;
                                                    <strong>
                                                        {normalizeNameOfPerson(member.name)} &nbsp;
                                                    </strong>
                                                    related to the other household members?
                                                </p>
                                            </div>

                                            {
                                                bloodRelationship.map(bloodRelation => {

                                                    const {
                                                        individualPersonId,
                                                        relatedPersonId,
                                                        relation
                                                    } = bloodRelation;

                                                    if(+individualPersonId === i + 1 && +relatedPersonId > i + 1) {

                                                        return (
                                                            <UniversalInput
                                                                key={`${individualPersonId}_${relatedPersonId}`}
                                                                label={{
                                                                    text: `${normalizeNameOfPerson(member.name)}
                                                                        is ${normalizeNameOfPerson(householdMembers[+relatedPersonId - 1].name)}'s`,
                                                                    isLabelRequired: false
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: true,
                                                                    type: INPUT_TYPES.DROPDOWN,
                                                                    name: `relation_${individualPersonId}_to_${relatedPersonId}`,
                                                                    value: relation,
                                                                    options: dropdownOptions,
                                                                    validationType: VALIDATION_TYPES.CHOSEN,
                                                                    currentData: bloodRelation,
                                                                    fields: []

                                                                }}
                                                                inputActions={{
                                                                    onFieldChange
                                                                }}
                                                            />
                                                        );
                                                    }
                                                })
                                            }
                                        </Fragment>
                                    );
                                }
                            })
                        }
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    relations: state.relations
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_13.defaultProps = {
    name: 'Page2_13'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_13);
