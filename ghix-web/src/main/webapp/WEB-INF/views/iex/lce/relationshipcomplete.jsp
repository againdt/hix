<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<script type="text/ng-template" id="relationshipcomplete.jsp">
  <div class="gutter10">

    <spring:message code="label.lce.relationshipComplete"/>
  </div>
    <!-- BACK & NEXT BUTTON -->
     <dl class="dl-horizontal pull-right">
      	<a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/> {{nextButtonLifeEvent}}</a>
    </dl>

</script>