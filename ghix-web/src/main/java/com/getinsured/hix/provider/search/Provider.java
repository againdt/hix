package com.getinsured.hix.provider.search;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.solr.common.SolrDocument;
import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class Provider implements JSONAware {
	
	private String groupKey;
	private String name;
	private String providerType;
	private List<ProviderAddress> providerAddress;
	private String specialty;
	private float distanceFromUserLocation;
	private String languages;
	private String networkId;
	private String networkTierId;
	private String acceptingNewPatients;
	private String medicalGroupAffiliation;
	private String hospitalAffiliation;
	private String accessibilityCodes;
	private String locationId;
	private String sanctionStatus;
	private String providerTypeIndicator;
	private String pracPhone;
	private String id;

	
	@SuppressWarnings("unchecked")
	public Provider(SolrDocument doc){
		String tmp;
		this.groupKey = (String) doc.get("group_key");
		String first_name = (String) doc.get("first_name")+" ";
		String middle_name = (String) doc.get("middle_name");
		String suffix = (String) doc.get("suffix");
		String professional_title = (String) doc.get("professional_title");
		String providerType = (String)doc.get("provider_class");
		if(StringUtils.isNotBlank(providerType)&&providerType.equals("individual")){
			if(middle_name == null){
				middle_name="";
			}else{
				middle_name += " ";
			}
			String last_name = (String)	doc.get("last_name");
			if(last_name == null){
				last_name = "";
			}

			this.name = first_name+middle_name+last_name.trim();
			//		if(suffix != null){
			//			this.name += ","+suffix;
			//		}
			if(professional_title != null){
				this.name += ","+professional_title;
			}
		}else{
			this.name = (String)doc.get("facility_name") ;
		}
		Object obj = null;
		obj = doc.get("specialty_code");
		this.specialty = "";
		ArrayList<String> tmpList = null;
		if(obj != null){
			tmpList = (ArrayList<String>)obj;
			for(String x: tmpList){
				if(x != null){
					this.specialty += x+", ";
				}
			}
		}
		if(this.specialty != null && this.specialty.endsWith(", ")){
			this.specialty = this.specialty.trim().substring(0, this.specialty.length()-2);
		}
		this.languages = "";
		if(tmpList != null){
			tmpList.clear();
		}
		// Not a mandatory field
		obj = doc.get("languages_spoken");
		if(obj != null){
			tmpList = (ArrayList<String>)obj;
			for(String x: tmpList){
				if(x != null){
						this.languages += x+", ";
				}
			}
		}
		
		if(this.languages.endsWith(", ")){
			this.languages = this.languages.trim().substring(0, this.languages.length()-2);
		}
		obj = doc.getFieldValue("_dist_");
		if(obj != null){
			this.distanceFromUserLocation = ((Double)obj).floatValue();
		}
		this.providerAddress = new ArrayList<ProviderAddress>();
		ProviderAddress address = new ProviderAddress((String)doc.get("address"));
		address.setProximity(this.distanceFromUserLocation);
		this.providerAddress.add(address);
		
		this.providerType = (String) doc.get("provider_class");
		
		this.networkId=doc.get("network_id")!=null?doc.get("network_id").toString():"";
		this.networkTierId = doc.get("network_tier_id")!=null?doc.get("network_tier_id").toString():"";
		
		this.acceptingNewPatients= doc.get("accepting_new_patients")!=null?doc.get("accepting_new_patients").toString():"";
		
		this.medicalGroupAffiliation = doc.get("medical_group_affiliation")!=null?doc.get("medical_group_affiliation").toString():"";
		
		this.hospitalAffiliation = doc.get("hospital_affiliation")!=null?doc.get("hospital_affiliation").toString():"";
		
		this.accessibilityCodes = doc.get("accessibility_codes")!=null?doc.get("accessibility_codes").toString():"";
		
		this.acceptingNewPatients= doc.get("accepting_new_patients")!=null?doc.get("accepting_new_patients").toString():"";
		
		this.locationId =  doc.get("location_id")!=null?doc.get("location_id").toString():"";
		
		this.sanctionStatus =  doc.get("sanction_status")!=null?doc.get("sanction_status").toString():"";
		
		this.specialty = "";
		if(tmpList != null){
			tmpList.clear();
		}
		obj = doc.get("specialty_name");
		if(obj != null){
			this.specialty = obj.toString();
			tmpList = (ArrayList<String>)obj;
			if(tmpList.size() > 2){
				tmpList = new ArrayList<String>(tmpList.subList(0, 2));
				this.specialty = tmpList.toString();
			}
		}
		this.providerTypeIndicator =  doc.get("provider_type_indicator")!=null?doc.get("provider_type_indicator").toString():"";
		this.pracPhone =  doc.get("prac_phone")!=null?doc.get("prac_phone").toString():"";
		this.id =  doc.get("provider_index")!=null?doc.get("provider_index").toString():"";
		
	}
	
	public boolean mergeProviderDocument(SolrDocument anotherProviderRecord)throws IllegalArgumentException{
		String docGroupKey = (String) anotherProviderRecord.get("group_key");
		if(!this.groupKey.equals(docGroupKey)){
			throw new IllegalArgumentException("Can't merge the two records with different group keys");
		}
		String addStr = (String) anotherProviderRecord.get("address");
		Object distObj = anotherProviderRecord.getFieldValue("_dist_");
		float proximity = 0.0f;
		if(distObj != null) {
			proximity = ((Double)distObj).floatValue();
		}
		ProviderAddress tmpAddress = null;
		if(addStr != null){
			tmpAddress = new ProviderAddress(addStr);
			tmpAddress.setProximity(proximity);
			this.providerAddress.add(tmpAddress);
			return true;
		}
		return false;
	}
	
	public String getGroupKey() {
		return groupKey;
	}
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getProviderType() {
		return providerType;
	}
	public void setProviderType(String providerType) {
		this.providerType = providerType;
	}
	public List<ProviderAddress> getProviderAddress() {
		return providerAddress;
	}
	public void setProviderAddress(List<ProviderAddress> providerAddress) {
		this.providerAddress = providerAddress;
	}
	public String getSpecialty() {
		return specialty;
	}
	public void setSpecialty(String specialty) {
		this.specialty = specialty;
	}

	public float getDistanceFromUserLocation() {
		return distanceFromUserLocation;
	}

	public void setDistanceFromUserLocation(float distanceFromUserLocation) {
		this.distanceFromUserLocation = distanceFromUserLocation;
	}

	public String getLanguages() {
		return languages;
	}

	public void setLanguages(String languages) {
		this.languages = languages;
	}
	
	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}

	public String getNetworkTierId() {
		return networkTierId;
	}

	public void setNetworkTierId(String networkTierId) {
		this.networkTierId = networkTierId;
	}

	public String getAcceptingNewPatients() {
		return acceptingNewPatients;
	}

	public void setAcceptingNewPatients(String acceptingNewPatients) {
		this.acceptingNewPatients = acceptingNewPatients;
	}

	public String getMedicalGroupAffiliation() {
		return medicalGroupAffiliation;
	}

	public void setMedicalGroupAffiliation(String medicalGroupAffiliation) {
		this.medicalGroupAffiliation = medicalGroupAffiliation;
	}

	public String getHospitalAffiliation() {
		return hospitalAffiliation;
	}

	public void setHospitalAffiliation(String hospitalAffiliation) {
		this.hospitalAffiliation = hospitalAffiliation;
	}

	public String getAccessibilityCodes() {
		return accessibilityCodes;
	}

	public void setAccessibilityCodes(String accessibilityCodes) {
		this.accessibilityCodes = accessibilityCodes;
	}

	public String getLocationId() {
		return locationId;
	}

	public void setLocationId(String locationId) {
		this.locationId = locationId;
	}

	public String getSanctionStatus() {
		return sanctionStatus;
	}

	public void setSanctionStatus(String sanctionStatus) {
		this.sanctionStatus = sanctionStatus;
	}

	public String getProviderTypeIndicator() {
		return providerTypeIndicator;
	}

	public void setProviderTypeIndicator(String providerTypeIndicator) {
		this.providerTypeIndicator = providerTypeIndicator;
	}
	
	public com.getinsured.hix.dto.plandisplay.Provider getNewProviderObj(){
		com.getinsured.hix.dto.plandisplay.Provider providerObj = new com.getinsured.hix.dto.plandisplay.Provider();
		providerObj.setGroupKey(this.groupKey);
		providerObj.setName(this.name);
		providerObj.setLanguages(this.languages);
		providerObj.setSpecialty(this.specialty);
		providerObj.setNetworkTierId(this.networkId);
		List<com.getinsured.hix.dto.plandisplay.ProviderAddress> addresses = new ArrayList<com.getinsured.hix.dto.plandisplay.ProviderAddress>();
		Collections.sort(this.providerAddress);
		for(ProviderAddress add: this.providerAddress){
			com.getinsured.hix.dto.plandisplay.ProviderAddress providerAddressObj = new com.getinsured.hix.dto.plandisplay.ProviderAddress(add.getAddline1());
			providerAddressObj.setAddLine2(add.getAddLine2());
			providerAddressObj.setCity(add.getCity());
			providerAddressObj.setState(add.getState());
			providerAddressObj.setZip(add.getZip());
			providerAddressObj.setLat(add.getLat());
			providerAddressObj.setLon(add.getLon());
			addresses.add(providerAddressObj);
		}
		providerObj.setProviderAddress(addresses);
		providerObj.setProviderType(this.providerType);
		List<String> practicePhones = new ArrayList<String>();
		practicePhones.add(this.pracPhone);
		providerObj.setPracticePhones(practicePhones);
		providerObj.setStrenuus_id(this.id);
		
		if(providerObj.getPracticePhones() != null && providerObj.getPracticePhones().size() > 0){
			providerObj.setPhones(providerObj.getPracticePhones());
		}
		
		return providerObj;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		com.getinsured.hix.dto.plandisplay.Provider providerObj = new com.getinsured.hix.dto.plandisplay.Provider();
		providerObj.setGroupKey(this.groupKey);
		providerObj.setName(this.name);
		providerObj.setLanguages(this.languages);
		providerObj.setSpecialty(this.specialty);
		providerObj.setNetworkTierId(this.networkId);
		List<com.getinsured.hix.dto.plandisplay.ProviderAddress> addresses = new ArrayList<com.getinsured.hix.dto.plandisplay.ProviderAddress>();
		Collections.sort(this.providerAddress);
		for(ProviderAddress add: this.providerAddress){
			com.getinsured.hix.dto.plandisplay.ProviderAddress providerAddressObj = new com.getinsured.hix.dto.plandisplay.ProviderAddress(add.getAddline1());
			providerAddressObj.setAddLine2(add.getAddLine2());
			providerAddressObj.setCity(add.getCity());
			providerAddressObj.setState(add.getState());
			providerAddressObj.setZip(add.getZip());
			providerAddressObj.setLat(add.getLat());
			providerAddressObj.setLon(add.getLon());
			addresses.add(providerAddressObj);
		}
		providerObj.setProviderAddress(addresses);
		providerObj.setProviderType(this.providerType);
		List<String> practicePhones = new ArrayList<String>();
		practicePhones.add(this.pracPhone);
		providerObj.setPracticePhones(practicePhones);
		providerObj.setStrenuus_id(this.id);
		
		return providerObj.toJSONString();
	}
	
}
