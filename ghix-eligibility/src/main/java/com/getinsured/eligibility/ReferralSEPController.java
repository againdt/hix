package com.getinsured.eligibility;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.at.ref.service.LceEditApplicationService;
import com.getinsured.eligibility.at.ref.service.ReferralLceNotificationService;
import com.getinsured.eligibility.at.ref.service.ReferralSEPService;
import com.getinsured.eligibility.at.service.GenericAtStrategyImpl;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;

/**
 * 
 * @author raguram_p
 * 
 */

@Controller
@RequestMapping("/referral")
public class ReferralSEPController {

	@Autowired
	private ReferralSEPService referralSEPService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired LceEditApplicationService lceEditApplicationService;

	@Autowired private GenericAtStrategyImpl genericAtStrategyImpl;

	/**
	 * to prepopulate all the applicants using casenumber.
	 */
	@RequestMapping(value = "/sep/applicant/events", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<LceActivityDTO> fetchLceEventsforReferralApplicants(@RequestBody String caseNumber) {

		LceActivityDTO lceActivityDTO = referralSEPService.populateApplicantsforSEPEvents(caseNumber);
		return new ResponseEntity<LceActivityDTO>(lceActivityDTO, HttpStatus.OK);
	}

	/**
	 * to update all the sep Events for the applicant and application
	 */
	@RequestMapping(value = "/sep/update/events", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Map<String, String>> updateSEPEvents(@RequestBody LceActivityDTO lceActivityDTO) {
		Map<String, String> result = referralSEPService.persistSEPAndQEPEventforApplicantandApplication(lceActivityDTO);
		return new ResponseEntity<Map<String, String>>(result, HttpStatus.OK);
	}

	/**
	 * to fetch all the sep Events
	 */
	@RequestMapping(value = "/sep/events", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Map<String, List<SepEvents>>> fetchSEPAndQEPEvents() {
		Map<String, List<SepEvents>> result = referralSEPService.fetchSepandQEPEvents();
		return new ResponseEntity<Map<String, List<SepEvents>>>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/sep/notifications/{caseNumber}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> generateLCEnotifications(@PathVariable("caseNumber") String caseNumber) {
		String result;
		try {
			String ecmId = referralLceNotificationService.generateSEPEventNotice(caseNumber);
			result = "LCE Notification generated for - " + caseNumber + " document id - " + ecmId;
		} catch (Exception e) {
			result = "Error occured while generating LCE notification for caseNumber - " + caseNumber;

		}

		return new ResponseEntity<String>(result, HttpStatus.OK);
	}

	@RequestMapping(value = "/appChangeAutomation/{applicationId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> editAndProcessApplication(@PathVariable("applicationId") Long applicationId) {
		String result;
		try {
			//genericAtStrategyImpl.processEditApplication(caseNumber);
			lceEditApplicationService.editApplicationProcess(applicationId);
			result = "LCE editAndProcessApplication completed successfully for - " + applicationId;
		} catch (Exception e) {
			result = "Error occured during editAndProcessApplication process - " + applicationId;

		}
        return new ResponseEntity<String>(result, HttpStatus.OK);
	}

}
