import React from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import _ from 'lodash';

// ActionCreators

import { changeData } from '../../actions/actionCreators';


// Utils

import normalizeNameOfPerson from '../normalizeNameOfPerson';
import normalizeStringToBooleanValue from '../normalizeStringToBooleanValue';
import { getHhmByPersonId } from '../getHhmByPersonId';
import { convertValuesForReactRender } from '../convertValuesForReactRender';

const Taxes = props => {

    let {
        data,
        data: {
            singleStreamlinedApplication: {
                taxHousehold
            }
        },
        householdMembers
    } = props;

    const {
        primaryTaxFilerPersonId
    } = taxHousehold[0];

    const primaryTaxFiler = primaryTaxFilerPersonId ?
        normalizeNameOfPerson(getHhmByPersonId(primaryTaxFilerPersonId, householdMembers).name) :
        'no Primary Tax Filer';


    const handleIndicatorClick = (hhmIndex, type) => {

        const newState = {...data};

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[hhmIndex]
            .taxHouseholdComposition[type] =
        (v => type === 'claimerIds' ? v.split().map(v => Number(v)) : normalizeStringToBooleanValue(v))(prompt('Enter new value'));

        props.actions.changeData(newState);
    };

    const handlePTFClick = () => {

        const newState = {...data};

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .primaryTaxFilerPersonId =
            (v => normalizeStringToBooleanValue(v, true))(prompt('Enter new value'));

        props.actions.changeData(newState);
    };

    return (
        <div className='hyper-navigation-tooltip-anchor'>
            T
            <ul className='hyper-navigation-tooltip'>
                <div className='list-caption'>
                    Taxes current status
                </div>
                <div onClick={handlePTFClick}>
                    {`Primary Tax Filer : ${primaryTaxFiler}`}
                </div>
                {
                    householdMembers.map((hhm, idx) => {
                        const {
                            householdId,
                            name,
                            taxHouseholdComposition
                        } = hhm;

                        const omitted = [
                            'married',
                            'spouseMatch',
                            'livesWithSpouse',
                            'taxFiler',
                            'taxDependent',
                            'claimingDependents',
                            'claimedOutsideHousehold'
                        ];

                        const cleanedTaxHouseholdComposition = _.omit(taxHouseholdComposition, omitted);

                        return (
                            <li
                                key={`${name}_${idx}`}
                                style={{
                                    position: 'relative'
                                }}
                            >
                                <div className='hhm-masks'>
                                    {normalizeNameOfPerson(name)} &nbsp; &nbsp; &nbsp; &nbsp;
                                    {`Household ID: ${householdId}`}
                                </div>
                                {
                                    Object.entries(cleanedTaxHouseholdComposition).map(([ indicator, value ]) => {

                                        return (
                                            <ul key={indicator}>
                                                <li
                                                    style={{
                                                        position: 'relative'
                                                    }}
                                                >
                                                    <span className='hyper-navigation-tooltip-number'>
                                                        { indicator }
                                                    </span>
                                                    <span
                                                        className='hyper-navigation-tooltip-masks'
                                                        style={{ position: 'absolute', left: '200px' }}
                                                        onClick={() => handleIndicatorClick(idx, indicator)}
                                                    >
                                                        { convertValuesForReactRender(value) }
                                                    </span>
                                                </li>
                                            </ul>
                                        );
                                    })
                                }
                            </li>
                        );
                    })

                }
            </ul>
        </div>
    );
};

const mapStateToProps = state => ({
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Taxes);