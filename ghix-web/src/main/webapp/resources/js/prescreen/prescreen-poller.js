/*
 * This function polls an input field for changes
 */

(function($, window) {
    "use strict";
    var tag = "changePolling",
        ChangePolling = function(element, options) {
            this.$element = $(element);
            this.options = $.extend(true, {}, $.fn.changePolling.defaults, options);
 
            this.options.previousValue = this.$element.val();
 
            this.stop();
 
            if (this.options.autoStart === true) {
                this.start();
            }
        };
 
    ChangePolling.prototype = {
 
        constructor: ChangePolling
 
        ,
        start: function() {
            this.options.intervalId = window.setInterval($.proxy(this.checkForChange, this), this.options.interval);
        }
 
        ,
        stop: function() {
            window.clearInterval(this.options.intervalId);
 
            this.options.intervalId = null;
        }
 
        ,
        checkForChange: function() {
            var currentValue = this.$element.val();
            
            //If value is changed, call APTC income
            if (this.options.previousValue && 
            		formatIncome(this.options.previousValue) != formatIncome(currentValue)) {
            	calculateAPTC();
            }
 
            this.options.previousValue = currentValue;
        }
    };
 
    $.fn.extend({
        changePolling: function(option) {
            return this.each(function() {
                var $this = $(this),
                    data = $this.data(tag),
                    options = typeof option === "object" && option;
 
                if (!data) {
                    $this.data(tag, (data = new ChangePolling(this, options)));
                }
 
                if (typeof option === "string") {
                    data[option]();
                }
            });
        }
    });
 
    $.fn.changePolling.defaults = {
        interval: 1000,
        autoStart: true
    };
 
    $.fn.changePolling.Constructor = ChangePolling;
 
}(jQuery, window));