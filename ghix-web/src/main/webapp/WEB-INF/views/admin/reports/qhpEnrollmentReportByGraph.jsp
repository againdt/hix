<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />

<!--<a href="<c:url value="/admin/reports/qhpenrollmentreport/list"/>">goto Listpage</a>-->

<div class="gutter10">
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
                    <li><a href="<c:url value="/admin/reports/qhpenrollmentreport/graph"/>">&lt;</a>Back</li>
                    <li><a href="<c:url value="/admin/reports/qhpenrollmentreport/graph"/>"><spring:message code='label.report'/></a></li>
                    <li><spring:message code='label.qhpReport'/></li>
                </ul><!--page-breadcrumb ends-->
		<h1><spring:message code='label.qhpEnrollmentReport'/></h1>
    </div><!--  end of row-fluid -->
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
    		<div class="header">
				<h4 class="margin0"><spring:message code='label.refineResult'/></h4>
			</div>
	             <form class="form-vertical gutter10 lightgray" id="frmbrokerreg" name="frmbrokerreg" action="<c:url value="/admin/reports/qhpenrollmentreport/graph" />" method="POST">
						<df:csrfToken/>
						<div class="control-group">
							<label for="issuername" class="control-label"><spring:message code='label.issuerName' /></label>
							<div class="controls">
								<select id="issuerId" name="issuerId" class="span12">
									<option value=""><spring:message code='label.any' /></option>
									<c:forEach items="${issuersList}" var="issuersName">
										<option <c:if test="${issuersName.id== issuerId}">selected</c:if> value="${issuersName.id}">${issuersName.name}</option>
									</c:forEach>
								</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
                            
						<div class="control-group">
							<label for="planLevel" class="control-label"><spring:message  code="label.planLevel"/></label>
							<div class="controls">							
								<c:set var="platinum" value="false"></c:set>
								<c:set var="gold" value="false"></c:set>
								<c:set var="silver" value="false"></c:set>
								<c:set var="bronze" value="false"></c:set>
								<c:set var="expandedBronze" value="false"></c:set>
								<c:set var="catastrophic" value="false"></c:set>
								
								<c:forEach items="${planLevel}" var="planlevelName">
									<c:choose>
									    <c:when test="${'platinum' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="platinum" value="true"></c:set>
									    </c:when>
									    <c:when test="${'gold' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="gold" value="true"></c:set>
									    </c:when>
									    <c:when test="${'silver' == fn:toLowerCase(planlevelName)}">
									     <c:set var="silver" value="true"></c:set>
									    </c:when>
									    <c:when test="${'bronze' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="bronze" value="true"></c:set>
									    </c:when>
									     <c:when test="${'expandedbronze' == fn:toLowerCase(planlevelName)}">
									    	<c:set var="expandedBronze" value="true"></c:set>
									    </c:when>
									    <c:when test="${'catastrophic' == fn:toLowerCase(planlevelName)}">
									     	<c:set var="catastrophic" value="true"></c:set>
									    </c:when>
									    <c:otherwise>
									    </c:otherwise>
									</c:choose>	
								</c:forEach>
								
							    <label class="label-checkbox clearfix"><input id="platinumCheck" name="planLevel" type="checkbox" value="platinum" <c:if test="${platinum == 'true'}">checked</c:if> /><span><spring:message code='label.platinum'/></span></label>
							    <label class="label-checkbox clearfix"><input id="goldCheck" name="planLevel" type="checkbox" value="gold" <c:if test="${gold == 'true'}">checked </c:if> /><span><spring:message code='label.gold'/></span></label>
								<label class="label-checkbox clearfix"><input id="silverCheck" name="planLevel" type="checkbox" value="silver" <c:if test="${silver == 'true'}">checked</c:if> /><span><spring:message code='label.silver'/></span></label>
								<label class="label-checkbox clearfix"><input id="bronzeCheck" name="planLevel" type="checkbox" value="bronze" <c:if test="${bronze == 'true'}">checked</c:if> /><span><spring:message code='label.bronze'/></span></label>
								<label class="label-checkbox clearfix"><input id="expandedBronzeCheck" name="planLevel" type="checkbox" value="expandedbronze" <c:if test="${expandedBronze == 'true'}">checked</c:if> /><span><spring:message code='label.expandedBronze'/></span></label>
								<label class="label-checkbox clearfix"><input id="catastrophicCheck" name="planLevel" type="checkbox" value="catastrophic" <c:if test="${catastrophic == 'true'}">checked</c:if> /><span><spring:message code='label.catastrophic'/></span></label>															
								
								<%--<label class="label-checkbox clearfix"><input id="platinumCheck" name="platinumCheck" type="checkbox" value="platinum" checked="<c:if test="${platinum==platinumCheck and platinumCheck!='uncheck'}">checked</c:if>"/><span>Platinum</span></label> --%>
								<%--<label class="label-checkbox clearfix"><input id="goldCheck" name="goldCheck" type="checkbox" value="gold" checked="<c:if test="${gold==goldCheck}">checked</c:if>"/><span>Gold</span></label>--%>
								<%--<label class="label-checkbox clearfix"><input id="silverCheck" name="silverCheck" type="checkbox" value="silver" checked="<c:if test="${silver==silverCheck}">checked</c:if>"/><span>Silver</span></label> --%>
								<%--<label class="label-checkbox clearfix"><input id="bronzeCheck" name="bronzeCheck" type="checkbox" value="bronze" checked="<c:if test="${bronze==bronzeCheck}">checked</c:if>"/><span>Bronze</span></label>--%>
								<%--<label class="label-checkbox clearfix"><input id="catastrophicCheck" name="catastrophicCheck" type="checkbox" value="Catastrophic" checked="<c:if test="${Catastrophic==catastrophicCheck}">checked</c:if>"/><span>Catastrophic</span></label>--%>
							</div> <!-- end of controls-->
						</div>

						<div class="control-group">
							<label for="market" class="control-label"><spring:message  code="label.market"/></label>
							<div class="controls">
								<select id="marketID" name="marketID" class="span12">
									<option value=""><spring:message code='label.any' /></option>
									<option <c:if test="${'Shop'==marketID}">selected</c:if> value="Shop"><spring:message code='label.shop'/></option>
									<option <c:if test="${'Individual'==marketID}">selected</c:if> value="Individual"><spring:message code='label.individual'/></option>
								</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
                           
						<div class="control-group">
							<label for="status" class="control-label"><spring:message code='label.Period' /></label>
							<div class="controls">
								<select id="periodID" name="periodID" class="span12">
									<option <c:if test="${'Monthly'==periodID}">selected</c:if> value="Monthly"><spring:message code='label.monthly'/></option>
									<option <c:if test="${'Quarterly'==periodID}">selected</c:if> value="Quarterly"><spring:message code='label.quarterly'/></option>
									<option <c:if test="${'Yearly'==periodID}">selected</c:if> value="Yearly"><spring:message code='label.yearly'/></option>
							</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
						
						<div class="control-group">
							<label for="verified" class="control-label"><spring:message code='label.RatingRegion' /></label>
							<div class="controls">
								<select id="rName" name="rName" class="span12">
									<option value=""><spring:message code='label.any' /></option>
									<c:forEach var="regionList" items="${regionList}">
										<option <c:if test="${regionList==rName}">selected</c:if> value="${regionList}">${regionList}</option>
									</c:forEach>
								</select>
							</div> <!-- end of controls-->
						</div> <!-- end of control-group -->
                            
						<div class="txt-center">
 						<input type="submit" class="btn" value="<spring:message  code='label.go'/>" title="<spring:message  code='label.go'/>">
 						</div>
				 </form>
		</div>
    	
        <div id="rightpanel" class="span9">
            <div class="header">
                <h4 class="pull-left"><spring:message code='label.graphViewofReport'/></h4>
                <a id="listViewBtn" class="btn btn-primary btn-small pull-right" href="<c:url value="/admin/reports/qhpenrollmentreport/list" />"><spring:message code='label.listView'/></a>
            </div>
			<c:choose>
			    <c:when test="${not empty planID || platinum eq true || gold eq true || silver eq true || bronze eq true || catastrophic eq true}">
			        <div class="gutter10">
			            <p><spring:message code='label.trendsfortotal'/></p>
		                <div id="QHPGraph" style="height: 300px;"></div>
		            </div>
			    </c:when>
			    <c:otherwise>
			    </c:otherwise>
			</c:choose>
        </div>

          <%--
          <div class="gutter10">
           <p><spring:message code='label.trendsfortotal'/></p>
              <div id="QHPGraph" style="height: 300px;"></div>
          </div>
          --%>
        
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->

<%--<script src="http://code.highcharts.com/highcharts.js"></script>--%>
<script type="text/javascript" src="<c:url value="/resources/js/highcharts.js" />"></script>
<script type="text/javascript">
var chart1;
$(function () {
	 chart1 = new Highcharts.Chart({
            chart: {
            	renderTo: 'QHPGraph',
                type: 'column'
            },
			legend: {
				layout: 'vertical',
				backgroundColor: '#FFFFFF',
				align: 'left',
				verticalAlign: 'top',
				floating: true,
				x: 90,
				y: 45
			},
            title: {
                text: 'Active Enrollment'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: ["${monthlyPeriod1}","${monthlyPeriod2}","${monthlyPeriod3}","${monthlyPeriod4}","${monthlyPeriod5}","${monthlyPeriod6}","${monthlyPeriod7}","${monthlyPeriod8}","${monthlyPeriod9}","${monthlyPeriod10}","${monthlyPeriod11}","${monthlyPeriod12}"]
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Number of Active Enrollments'
                },
				allowDecimals: false,
				tickInterval: 1
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b> {point.y}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Bronze',
                data: [Number("${timeValueBronze1}"), Number("${timeValueBronze2}"), Number("${timeValueBronze3}"), Number("${timeValueBronze4}"), Number("${timeValueBronze5}"), Number("${timeValueBronze6}"), Number("${timeValueBronze7}"), Number("${timeValueBronze8}"), Number("${timeValueBronze9}"), Number("${timeValueBronze10}"), Number("${timeValueBronze11}"), Number("${timeValueBronze12}")]
    
            },
            {
                name: 'ExpandedBronze',
                data: [Number("${timeValueExpandedBronze1}"), Number("${timeValueExpandedBronze2}"), Number("${timeValueExpandedBronze3}"), Number("${timeValueExpandedBronze4}"), Number("${timeValueExpandedBronze5}"), Number("${timeValueExpandedBronze6}"), Number("${timeValueExpandedBronze7}"), Number("${timeValueExpandedBronze8}"), Number("${timeValueExpandedBronze9}"), Number("${timeValueExpandedBronze10}"), Number("${timeValueExpandedBronze11}"), Number("${timeValueExpandedBronze12}")]
    
            },
            {
                name: 'Silver',
                data: [Number("${timeValueSilver1}"), Number("${timeValueSilver2}"), Number("${timeValueSilver3}"), Number("${timeValueSilver4}"), Number("${timeValueSilver5}"), Number("${timeValueSilver6}"), Number("${timeValueSilver7}"), Number("${timeValueSilver8}"), Number("${timeValueSilver9}"), Number("${timeValueSilver10}"), Number("${timeValueSilver11}"), Number("${timeValueSilver12}")]
    
            }, {
                name: 'Gold',
                data: [Number("${timeValueGold1}"), Number("${timeValueGold2}"), Number("${timeValueGold3}"), Number("${timeValueGold4}"), Number("${timeValueGold5}"), Number("${timeValueGold6}"), Number("${timeValueGold7}"), Number("${timeValueGold8}"), Number("${timeValueGold9}"), Number("${timeValueGold10}"), Number("${timeValueGold11}"), Number("${timeValueGold12}")]
    
            }, {
                name: 'Platinum',
                data: [Number("${timeValuePlatinum1}"), Number("${timeValuePlatinum2}"), Number("${timeValuePlatinum3}"), Number("${timeValuePlatinum4}"), Number("${timeValuePlatinum5}"), Number("${timeValuePlatinum6}"), Number("${timeValuePlatinum7}"), Number("${timeValuePlatinum8}"), Number("${timeValuePlatinum9}"), Number("${timeValuePlatinum10}"), Number("${timeValuePlatinum11}"), Number("${timeValuePlatinum12}")]
            },{
                name: 'Catastrophic',
                data: [Number("${timeValueCatastrophic1}"), Number("${timeValueCatastrophic2}"), Number("${timeValueCatastrophic3}"), Number("${timeValueCatastrophic4}"), Number("${timeValueCatastrophic5}"), Number("${timeValueCatastrophic6}"), Number("${timeValueCatastrophic7}"), Number("${timeValueCatastrophic8}"), Number("${timeValueCatastrophic9}"), Number("${timeValueCatastrophic10}"), Number("${timeValueCatastrophic11}"), Number("${timeValueCatastrophic12}")]
            }],
			credits : {
				enabled : false,
			}
        });
    });
</script>