import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import {changeData} from '../../actions/actionCreators';
import currencyTools from '../../utils/currencyTools';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import {FREQUENCY_LIST, INCOME_SOURCES_LIST} from '../../constants/dropdownOptions';

// Components

// ActionCreators

// Utils

// Constants


const Page3_33_E1 = props => {

    const {
        data
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        incomes
    } = currentHhm;

    const onFieldChange = (newData, index) => {
        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .incomes[index] = newData;
        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <div
                id="iiTribalIncome"
                className=""
            >
                <div className="subsection">
                    <h4>
                        Is any of this income from these sources?
                    </h4>
                    <ul>
                        <li>
                            Per capita payments from the tribe that come from natural resources, usage rights, leases or royalties.
                        </li>
                        <li>
                            Payments from natural resources, farming, ranching, fishing, leases, or royalties from land designated as Indian land by the Development of Interior (including reservations and former reservations).
                        </li>
                        <li>
                            Money from selling things that have cultural significance.
                        </li>
                    </ul>
                    <Form>
                        <div className="table-res-wrap">
                            <table className="table-res">
                                <thead>
                                    <tr>
                                        <th>Income Source</th>
                                        <th>Amount</th>
                                        <th>How often?</th>
                                        <th>Tribal Income</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        incomes.map((source, idx) => {

                                            const {
                                                amount,
                                                tribalAmount
                                            } = source;

                                            return (
                                                <tr
                                                    key={`${source.type}_${idx}`}
                                                >
                                                    <td>
                                                        {  INCOME_SOURCES_LIST[source.type] }
                                                    </td>
                                                    <td>
                                                        { currencyTools.getMoneyFormat(source.amount, true) }
                                                    </td>
                                                    <td>
                                                        { FREQUENCY_LIST[source.frequency] }
                                                    </td>
                                                    <td>
                                                        <UniversalInput
                                                            label={{
                                                                text: 'Amount',
                                                                labelClasses: ['hidden']
                                                            }}
                                                            inputData={{
                                                                isInputRequired: false,
                                                                type: INPUT_TYPES.MONEY_INPUT,
                                                                name: 'tribalAmount',
                                                                value: tribalAmount,
                                                                valuesToMatch: amount >= 0 ? [0, amount] : [],
                                                                helperClasses: ['margin-0', 'padding-0'],
                                                                isOnlyPositiveAvailable: true,
                                                                placeholder: 'Amount',
                                                                validationType: VALIDATION_TYPES.MONEY_NOT_EXCEED,
                                                                currentData: { ...source },
                                                                fields: []
                                                            }}
                                                            errorClasses={['margin-l-20-ve margin-b-10']}

                                                            inputActions={{
                                                                onFieldChange
                                                            }}
                                                            indexInArray={idx}
                                                        />
                                                    </td>
                                                </tr>
                                            );
                                        })
                                    }
                                </tbody>
                            </table>
                        </div>
                    </Form>
                </div>
            </div>
        </Fragment>
    );
};

Page3_33_E1.defaultProps = {
    name: 'Page3_33_E1'
};

const mapStateToProps = state => ({
    data: state.data
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(Page3_33_E1);
