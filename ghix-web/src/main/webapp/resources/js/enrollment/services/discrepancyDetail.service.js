(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .factory('discrepancyDetailService', discrepancyDetailService);

    discrepancyDetailService.$inject = ['$http', '$q'];

    function discrepancyDetailService($http, $q) {
        var searchFilters = {};
        var openDiscrepancies = [];
        var cooments = [];
        var visualSummaryData = {};

        return {
            doDiscrepancyAction: doDiscrepancyAction,
            fetchOpenDiscrepancies: fetchOpenDiscrepancies,
            getOpenDiscrepancies: getOpenDiscrepancies,
            fetchCommentsForDiscrepancies: fetchCommentsForDiscrepancies,
            getCommentsForDiscrepancies: getCommentsForDiscrepancies,
            fetchVisualSummary: fetchVisualSummary,
            getVisualSummaryData: getVisualSummaryData
        };

        function doDiscrepancyAction(discrepancy){

            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/addeditcommentorsnoozediscrepancy';

            var promise = $http.post(url, discrepancy);

            promise.success(function(response){
                //console.log("success");
                if(response.errCode === 200){
                    searchFilters = response;
                    deferred.resolve(response);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;

        }

        function fetchOpenDiscrepancies(requestJSON){

            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/getenrlrecondiscrepancydetail';

            var promise = $http.post(url, requestJSON);

            promise.success(function(response){
                //console.log("Get OpenDiscrepancies Success");
                if(response.errCode === 200){
                    openDiscrepancies = response;
                    deferred.resolve(response);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;
        }

        function getOpenDiscrepancies() {
            return openDiscrepancies;
        }

        function fetchCommentsForDiscrepancies(requestJSON){

            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/getenrlreconcommentdetail';

            var promise = $http.post(url, requestJSON);

            promise.success(function(response){
                //console.log("Get CommentsForDiscrepancies Success");
                if(response.errCode === 200){
                    cooments = response;
                    deferred.resolve(response);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;
        }

        function getCommentsForDiscrepancies() {
            return cooments;
        }

        function fetchVisualSummary(requestJSON) {
            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/getvisualsummarydetail';

            var promise = $http.post(url, requestJSON);

            promise.success(function(response){
                //console.log("Get Visual Summary Success");
                if(response.errCode === 200){
                    visualSummaryData  = response;

                    // {
                    //     "discrepancyEnrollmentDTO": {
                    //         "enrollmentId": 2314,
                    //         "benefitEffectiveStartDate": {
                    //             "hixValue": "Jan 1, 2016 12:00:00 AM",
                    //             "issuerValue": "Jan 1, 2016 12:00:00 AM",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "benefitEffectiveEndDate": {
                    //             "hixValue": "Dec 31, 2016 11:59:59 PM",
                    //             "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "homeAddress": {
                    //             "hixValue": "1910 University dr Boise ID 83725",
                    //             "issuerValue": "1910 University dr Boise ID 83725",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "mailingAddress": {
                    //             "hixValue": "123 North st Boise ID 83702",
                    //             "issuerValue": "123 North st Boise ID 83702",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "agentName": {
                    //             "hixValue": "Bobby Maltas",
                    //             "hasDiscrepancy": true
                    //         },
                    //         "brokerFEDTaxPayerId": {
                    //             "hixValue": 4452170,
                    //             "issuerValue": 4452170,
                    //             "hasDiscrepancy": false
                    //         },
                    //         "phoneNumber": {
                    //             "hixValue": "555-555-5555",
                    //             "issuerValue": "555-555-5555",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "memberCount": {
                    //             "hixValue": 4,
                    //             "issuerValue": 4,
                    //             "hasDiscrepancy": false
                    //         },
                    //         "cmsPlanId": {
                    //             "hixValue": "61589ID226000101",
                    //             "issuerValue": "61589ID226000101",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "issuerName": {
                    //             "hixValue": "Blue Cross Blue Shield of Idaho",
                    //             "issuerValue": "Blue Cross Blue Shield of Idaho",
                    //             "hasDiscrepancy": false
                    //         },
                    //         "discrepancyMemberDTOList": [{
                    //             "relationshipWithSubscriber": {
                    //                 "hixValue": "Self",
                    //                 "issuerValue": "Self",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "exchgIndivIdentifier": {
                    //                 "hixValue": "1111111111",
                    //                 "issuerValue": "1111111111",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberFullName": {
                    //                 "hixValue": "John Carpenter",
                    //                 "issuerValue": "John Carpenter",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "gender": {
                    //                 "hixValue": "Male",
                    //                 "issuerValue": "Male",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "SSN": {
                    //                 "hixValue": "111-22-3333",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberEffectiveStartDate": {
                    //                 "hixValue": "Jan 1, 2016 12:00:00 AM",
                    //                 "issuerValue": "Jan 1, 2016 12:00:00 AM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberEffectiveEndDate": {
                    //                 "hixValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberDateOfBirth": {
                    //                 "hixValue": "May 01, 1970 11:59:59 PM",
                    //                 "issuerValue": "May 01, 1970 11:59:59 PM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "tobaccoUsage": {
                    //                 "hixValue": "Y",
                    //                 "issuerValue": "N",
                    //                 "hasDiscrepancy": true
                    //             }
                    //         }, {
                    //             "relationshipWithSubscriber": {
                    //                 "hixValue": "Spouse",
                    //                 "issuerValue": "Spouse",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "exchgIndivIdentifier": {
                    //                 "hixValue": "2222222222",
                    //                 "issuerValue": "2222222222",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberFullName": {
                    //                 "hixValue": "Rosie Carpenter",
                    //                 "issuerValue": "Rosie Carpenter",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "gender": {
                    //                 "hixValue": "Female",
                    //                 "issuerValue": "female",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "SSN": {
                    //                 "hixValue": "222-33-4444",
                    //                 "issuerValue": "222-33-4444",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberEffectiveStartDate": {
                    //                 "hixValue": "Jan 1, 2016 12:00:00 AM",
                    //                 "issuerValue": "Jan 1, 2016 12:00:00 AM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberEffectiveEndDate": {
                    //                 "hixValue": "Nov 30, 2016 11:59:59 PM",
                    //                 "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberDateOfBirth": {
                    //                 "hixValue": "Feb 10, 1970 11:59:59 PM",
                    //                 "issuerValue": "Feb 10, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "tobaccoUsage": {
                    //                 "hixValue": "N",
                    //                 "issuerValue": "N",
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "relationshipWithSubscriber": {
                    //                 "hixValue": "Child",
                    //                 "issuerValue": "Child",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "exchgIndivIdentifier": {
                    //                 "hixValue": "3333333333",
                    //                 "issuerValue": "3333333333",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberFullName": {
                    //                 "hixValue": "Junior Carpenter",
                    //                 "issuerValue": "Junior Carpenter",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "gender": {
                    //                 "hixValue": "Male",
                    //                 "issuerValue": "Male",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "SSN": {
                    //                 "hixValue": "333-44-5555",
                    //                 "issuerValue": "333-44-5555",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberEffectiveStartDate": {
                    //                 "hixValue": "Mar 1, 2016 12:00:00 AM",
                    //                 "issuerValue": "Feb 1, 2016 12:00:00 AM",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberEffectiveEndDate": {
                    //                 "hixValue": "Nov 30, 2016 11:59:59 PM",
                    //                 "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberDateOfBirth": {
                    //                 "hixValue": "Apr 20, 2010 11:59:59 PM",
                    //                 "issuerValue": "Apr 20, 2010 11:59:59 PM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "tobaccoUsage": {
                    //                 "hixValue": "N",
                    //                 "issuerValue": "N",
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "relationshipWithSubscriber": {
                    //                 "hixValue": "Child",
                    //                 "issuerValue": "Child",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "exchgIndivIdentifier": {
                    //                 "hixValue": "1111111111",
                    //                 "issuerValue": "1111111111",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberFullName": {
                    //                 "hixValue": "Anna Carpenter",
                    //                 "issuerValue": "Anna Carpenter",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "gender": {
                    //                 "hixValue": "Female",
                    //                 "issuerValue": "Female",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "SSN": {
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "memberEffectiveStartDate": {
                    //                 "hixValue": "Mar 1, 2016 12:00:00 AM",
                    //                 "issuerValue": "Feb 1, 2016 12:00:00 AM",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberEffectiveEndDate": {
                    //                 "hixValue": "Nov 30, 2016 11:59:59 PM",
                    //                 "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "memberDateOfBirth": {
                    //                 "hixValue": "Dec 31, 1970 11:59:59 PM",
                    //                 "issuerValue": "Dec 31, 2016 11:59:59 PM",
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "tobaccoUsage": {
                    //                 "hixValue": "N",
                    //                 "issuerValue": "N",
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }],
                    //         "discrepancyPremiumDTOList": [{
                    //             "monthNumber": 1,
                    //             "monthName": "January",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 350,
                    //                 "issuerValue": 350,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 100.5,
                    //                 "issuerValue": 100.5,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 249.5,
                    //                 "issuerValue": 249.5,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 2,
                    //             "monthName": "February",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 100.5,
                    //                 "issuerValue": 100.5,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 3,
                    //             "monthName": "March",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 4,
                    //             "monthName": "April",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 5,
                    //             "monthName": "May",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 6,
                    //             "monthName": "June",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 7,
                    //             "monthName": "July",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 8,
                    //             "monthName": "August",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 9,
                    //             "monthName": "September",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 10,
                    //             "monthName": "October",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 11,
                    //             "monthName": "November",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }, {
                    //             "monthNumber": 12,
                    //             "monthName": "December",
                    //             "year": 2016,
                    //             "grossPremiumAmount": {
                    //                 "hixValue": 1000,
                    //                 "issuerValue": 1000,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "aptcAmount": {
                    //                 "hixValue": 200.5,
                    //                 "issuerValue": 200,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "netPremiumAmount": {
                    //                 "hixValue": 799.5,
                    //                 "issuerValue": 800,
                    //                 "hasDiscrepancy": true
                    //             },
                    //             "csrAmount": {
                    //                 "hixValue": 100,
                    //                 "issuerValue": 100,
                    //                 "hasDiscrepancy": false
                    //             },
                    //             "isFinancial": {
                    //                 "hixValue": true,
                    //                 "issuerValue": true,
                    //                 "hasDiscrepancy": false
                    //             }
                    //         }]
                    //     },
                    //     "status": "SUCCESS",
                    //     "errCode": 200,
                    //     "execDuration": 8,
                    //     "startTime": 1490841140191
                    // };
                    deferred.resolve(visualSummaryData);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;
        }

        function getVisualSummaryData() {
            return visualSummaryData;
        }
    }
})();
