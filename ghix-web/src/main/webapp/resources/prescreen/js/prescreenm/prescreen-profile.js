/*
 * Custom script for My Profile Page
 */

$(function(){
	
	//My Profile page model
	PrescreenProfileModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			claimerName: "",
			zipCode: "",
			isMarried : false,
			numberOfDependents : 0,
			taxHouseholdIncome : 0.0,
			isIncomePageLatest : false,
			stateCode : "CA"
      	}
		
	});
	
	
	profileModel = new PrescreenProfileModel();
	
	prescrenUpdateView = new UpdateView({
			el : $ ('#profileForm'),
			model : profileModel,
			bindings : null
		}
	);
	
});