package com.getinsured.hix.batch.platform.task;

import java.sql.Timestamp;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.platform.security.service.UserService;

public class UserProfileMigrationTask implements Tasklet {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserProfileMigrationTask.class);
	//private static final String DATASOURCE ="java:/jdbc/ghixBatchDS";
	private String taskParameter;
	private UserService userService;
	
	/*
	 * HIX-32106 : Define Process to update existing passwords
	 * @author :Venkata Tadepalli ; since 03/30/2014
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 * 
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		//Connection con = null;
		LOGGER.info("Batch job UserProfileMigration started on :-"+new Timestamp(System.currentTimeMillis()));
		
		LOGGER.info("Migrating Users' username starts with ::  " + taskParameter);
		try {
			List<String> failedUsers = userService.migrateUsers(taskParameter);
			LOGGER.info("Unable to migrate Users::"+ failedUsers.size()+" ; details follows");
			for(String currRec:failedUsers){
				LOGGER.info(currRec);
			}
			
	
		} catch (Exception e) {
			LOGGER.error("Exception occured while executing  batch job", e);
		}
	      
		LOGGER.info("Batch job UserProfileMigration completed on :-" +new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}

	public String getTaskParameter() {
		return taskParameter;
	}

	public void setTaskParameter(String taskParameter) {
		this.taskParameter = taskParameter;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
	
	/*
	public RepeatStatus execute(StepContribution contribution,ChunkContext chunkContext) throws Exception {
		Connection con = null;
		LOGGER.info("Batch job UserProfileMigration started on :-"+new Timestamp(System.currentTimeMillis()));
		
		LOGGER.info("taskParameter value :  " + taskParameter);
		try {
			int noOfUsers=userBatchRepository.getUserListToMigrate(taskParameter).size();
			LOGGER.info("No Of Users::", noOfUsers);
			
			  Context initialContext = new InitialContext();
			  Context envContext  = (Context)initialContext.lookup("java:/comp/env");
			//  DataSource ds = (DataSource)envContext.lookup("jdbc/MyDatasource");
			  
		      DataSource datasource = (DataSource)envContext.lookup("jdbc/ghixBatchDS");
		      if (datasource != null) {
		    	  con = datasource.getConnection();
		      }
		      Statement createStmt = con.createStatement();
		      String strQuery="select count(*) from users where (uuid is null or length(uuid)<1) and username like "+taskParameter;
		      ResultSet rsCount = createStmt.executeQuery(strQuery);
		      //ResultSet rs = createStmt.executeQuery("SELECT 'System Time : ' ||SYSTIMESTAMP || ' IP Adress : '|| SYS_CONTEXT('USERENV','IP_ADDRESS') || ' Host Name : '|| SYS_CONTEXT('USERENV','SERVER_HOST') result FROM DUAL");
		      long noOfRecs= 0;
		      
		      while(rsCount.next()){
			      noOfRecs=rsCount.getLong(1);
			      
			      System.out.println("User Migration Query no. of records ("+noOfRecs+") for the following queiry ::\r\n\t"+strQuery);
		    	  
		      }
		      rsCount.close();
		      strQuery="select id, uuid, username from users where uuid is null or length(uuid)<1 and username like "+taskParameter;
		      ResultSet rsData = createStmt.executeQuery(strQuery);
		      
		      while (rsData.next()) {
		    	  String logText=rsData.getString(1)+"\t"+rsData.getString(2)+"\t"+rsData.getString(3);
		    	  System.out.println(logText);
		    	  LOGGER.info(logText);
			  }
		      rsData.close();
		} catch (Exception e) {
			LOGGER.error("Exception occured while executing  batch job", e);
		}finally{
			
			con.close();
		}
	      
		LOGGER.info("Batch job UserProfileMigration completed on :-" +new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}*/
}
