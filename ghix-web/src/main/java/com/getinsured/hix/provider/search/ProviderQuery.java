package com.getinsured.hix.provider.search;

import org.apache.commons.lang3.StringUtils;
import org.apache.solr.client.solrj.SolrQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import com.getinsured.hix.provider.service.zipcode.ZipCode;

public class ProviderQuery {
	public static final int INDIVIDUAL = 0;
	public static final int FACILITY = 1;
	public static final String MEDICAL = "medical";
	public static final String DENTAL = "dental";
	public static final String DOCTOR = "doctor";
	public static final String DENTIST = "dentist";
	public static final String HOSPITAL = "hospital";
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(ProviderQuery.class);
	protected ZipCode userZipCode = null;
	protected SolrQuery query = null;
	protected float proxymity;
	@Value("#{configProp['provider.search.rfields']}")
	protected String resultSetFields = "group_key,first_name,last_name,middle_name,address,network_id,network_tier_id,languages_spoken,specialty_name,provider_class,facility_name,city,provider_type_indicator,accepting_new_patients,prac_phone,provider_index"; // default
	protected String resultFormat = "json"; // For now only JSON supported by
											// provider search module, could be
											// XML as well in future
	protected String providerClassStr;
	protected String latLonStr;
	private String languageFilter;
	private String genderFilter;
	private boolean distanceSort = true;
	protected String city;
	protected String searchType;
	private String specialtyFilter;
	private String networkId;

	public ProviderQuery() {
	}

	public ZipCode getUserZipCode() {
		return userZipCode;
	}

	public void setUserZipCode(ZipCode userLocation) {
		if(userLocation != null){
			this.latLonStr = userLocation.getLat() + "," + userLocation.getLon();
		}else{
			LOGGER.warn("NULL user's location provided, is it expected?");
		}
		this.userZipCode = userLocation;
	}

	public float getProxymity() {
		return proxymity;
	}

	public void setProxymity(float proxymity) {
		this.proxymity = proxymity;
	}


	/**
	 * Object returned by this call is re-usable for pagination queries, caller
	 * should cache the object and re-use it for start count
	 *
	 * @param searchTerm
	 *            : Provider by user
	 * @param rows
	 *            : caller to decide how many rows should be returned, can be
	 *            made configurable
	 * @param start
	 *            : row start number for pagination
	 * @return Solr Query object
	 * @throws IllegalStateException
	 */
	public SolrQuery getQuery(String searchTerm, int rows, int start)
			throws IllegalStateException {
		
		this.query = new SolrQuery();
		String sq = "";
		if(searchTerm != null && searchTerm.length() > 0){
			sq+= "text:"+searchTerm;
		}
		if(this.city != null){
			if(city.indexOf(" ") != -1){
				city = "\""+city+"\"";
			}
			if(sq.length() > 0){
				sq += " AND "+"city:"+this.city;
			}else{
				sq += "city:"+this.city;
			}
		}
		
		if(StringUtils.isNotBlank(this.searchType)){
			if(this.searchType.equals(DOCTOR)){
				sq += " provider_type_indicator:P"; // If medical skip DENTAL, select MEDICAL and VISION
			}else if(this.searchType.equals(DENTIST)){
				sq += " provider_type_indicator:D";	// If medical select only DENTAL			
			}else if(this.searchType.equals(HOSPITAL)){
				sq += " provider_type_indicator:H";			
			}
		}
		if(StringUtils.isNotBlank(this.networkId)){
			sq += " AND network_id:"+this.networkId;
		}
		sq = sq.trim();
		this.query.set("q", sq);
		this.query.set("q.op", "AND");
		
		
		if (this.providerClassStr != null) {
			this.query.addFilterQuery("provider_class:"+this.providerClassStr);
		}
		
		this.query.setFacet(true);
		this.query.setFacetMinCount(1);
		this.query.setFacetLimit(25);
		String [] facetFields = {"languages_spoken","taxonomy_code","specialty_code", "specialty_name"};
		this.query.set("facet.field", facetFields);
		if (this.userZipCode != null && this.proxymity > 0) {
			this.query.set("sfield", "prac_location");
			this.query.set("pt", this.latLonStr);
			this.query.set("d", Float.toString(this.proxymity));
			this.query.addFilterQuery("{!geofilt}");
			this.resultSetFields += ",_dist_:geodist()";
		} else {
			LOGGER.warn("failed to get user geo location for zip code:"
					+ this.userZipCode
					+ " performing query without geo distance");
		}
		this.query.set("fl", resultSetFields);
		this.query.set("wt", this.resultFormat);
		this.query.setRows(rows);
		this.query.setStart(start);
		if(this.genderFilter != null){
			if(this.genderFilter.equalsIgnoreCase("M") || this.genderFilter.equalsIgnoreCase("F")){
				this.query.addFilterQuery("gender:"+this.genderFilter);
			}
		}
		if(this.languageFilter != null){
			this.query.addFilterQuery("languages_spoken:"+this.languageFilter);
		}
		if(this.specialtyFilter != null){
			this.query.addFilterQuery("specialty_code:"+this.specialtyFilter);
		}
		if(this.isDistanceSort()){
			this.query.set("sort", "geodist() asc");
		}
		LOGGER.debug("Provider List Query:" + query);
		return query;
	}

	public String getResultSetFields() {
		return resultSetFields;
	}

	public String getResultFormat() {
		return resultFormat;
	}

	public void setResultFormat(String resultFormat) {
		this.resultFormat = resultFormat;
	}

	public String getProviderClass() {
		return providerClassStr;
	}

	public void setProviderClass(int providerClass)
			throws IllegalArgumentException {
		switch (providerClass) {
		case INDIVIDUAL:
			this.providerClassStr = "individual";
			break;
		case FACILITY:
			this.providerClassStr = "facility";
			break;
		default:
			throw new IllegalArgumentException(
					"Unknown provider class value provided");
		}
	}

	public String getLanguageFilter() {
		return languageFilter;
	}

	public void setLanguageFilter(String languageFilter) {
		this.languageFilter = languageFilter;
	}

	public String getGenderFilter() {
		return genderFilter;
	}

	public void setGenderFilter(String genderFilter) {
		this.genderFilter = genderFilter;
	}

	public boolean isDistanceSort() {
		return distanceSort;
	}

	public void setDistanceSort(boolean distanceSort) {
		this.distanceSort = distanceSort;
	}

	public void setProviderCity(String userCity) {
		this.city = userCity;
	}

	public String getSearchType() {
		return searchType;
	}

	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	public String getSpecialtyFilter() {
		return specialtyFilter;
	}

	public void setSpecialtyFilter(String specialtyFilter) {
		if(specialtyFilter.indexOf(" ") != -1){
			this.specialtyFilter = "\""+specialtyFilter+"\"";
		}else{
			this.specialtyFilter = specialtyFilter;
		}
	}

	public String getNetworkId() {
		return networkId;
	}

	public void setNetworkId(String networkId) {
		this.networkId = networkId;
	}
	
	
}
