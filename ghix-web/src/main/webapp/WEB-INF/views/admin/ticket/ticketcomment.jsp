<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl" %>
<%@ page isELIgnored="false" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<!--[if IE]>
<style type="text/css">
form#commentForm + table.table {
visibility: hidden;
}
</style>
<![endif]-->

<script type="text/javascript">
    $(document).ready(function () {
        $("#showcomments").addClass("active navmenu");
    });
    $(function () {

        // As per HIX-14171 - to hide edit & delete button from comment list taglib
        // Commented hide function for HIX-86889
        //$(".pull-right.hide_comment_btn").hide();

        $(".newcommentiframe").click(function (e) {
            e.preventDefault();
            var href = $(this).attr('href');
            if (href.indexOf('#') != 0) {
                $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop: false});
            }
        });

        var status = '${TICKET_STATUS}';
        if (status == 'Completed' || status == 'Canceled' || status == 'Resolved') {
            $(".showcomments").hide();
        }
    });

    function closeCommentBox() {
        $("#newcommentiframe").remove();
        window.location.reload(true);
    }

    function closeIFrame() {
        $("#newcommentiframe").remove();
        window.location.reload(true);
    }
</script>

<!-- Cancel ticket JSP to add confirm dialog and cancel ticket functionality -->

<!-- Cancel ticket JSP ends -->

<div class="gutter10">
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message code="label.back"/></a></li>
            <li><a href="<c:url value="/ticketmgmt/ticket/ticketlist/" />"><spring:message code="lbl.viewTickets"/></a>
            </li>
            <li><spring:message code="lbl.comments"/></li>
        </ul><!--page-breadcrumb ends-->
        <div class="row-fluid">
            <h1>${ticketdtl.subject}</h1>
            <div class="row-fluid">
                <jsp:include page="ticketQuickActionBar.jsp">
                    <jsp:param value="${ticketdtl.id}" name="ticketId"/>
                    <jsp:param value="${ticketdtl.tkmWorkflows.type}" name="ticketType"/>
                    <jsp:param value="${ticketdtl.tkmWorkflows.category}" name="ticketCategory"/>
                </jsp:include>
            </div>
        </div>
    </div>
    <div class="row-fluid">
        <div class="span3" id="sidebar">
            <c:set var="tkmTicketsObj" value="${ticketdtl}" scope="request"/>
            <jsp:include page="ticketsummary.jsp">
                <jsp:param value="${ticketdtl.id}" name="ticketId"/>
                <jsp:param value="../ticketlist?pageNumber=${gotoPageNumber}" name="backButtonUrl"/>
            </jsp:include>
        </div>

        <div class="span9" id="rightpanel">

            <div class="header">
                <h4><spring:message code="lbl.comments"/></h4>
            </div>
            <div class="row-fluid gutter10">

                <form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
                    <table class="table">
                        <!-- <thead>
                            <tr class="header">
                                <th scope="col"><h4>Comments</h4></th>
                                <th scope="col" class="txt-right"><a class="btn btn-primary showcomments">Add Comments</a></th>
                            </tr>
                        </thead>-->
                        <tbody>
                        <tr>
                            <td colspan="2" class="">
                                <comment:view targetId="${ticketdtl.id}" targetName="TICKET">
                                </comment:view>

                                <jsp:include page="../../platform/comment/addcomments.jsp">
                                    <jsp:param value="" name=""/>
                                </jsp:include>
                                <input type="hidden" value="TICKET" id="target_name" name="target_name"/>
                                <input type="hidden" value="${ticketdtl.id}" id="target_id" name="target_id"/>

                            </td>
                        </tr>
                        </tbody>
                    </table>

                </form>
            </div>
        </div>
    </div><!--  end of row-fluid -->
</div>
<!-- end of gutter10 -->
