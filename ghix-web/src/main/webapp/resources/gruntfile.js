module.exports = function(grunt) {

  grunt.registerTask('watch', [ 'watch' ]);

  grunt.initConfig({
    notify_hooks: {
        options: {
          enabled: true,
          max_jshint_notifications: 5, // maximum number of notifications from jshint output
          title: "STATE" // defaults to the name in package.json, or will use project directory's name
        }
      },
    imagemin: {
        dynamic: {
            files: [{
                expand: true,
                cwd: 'img/',
                src: ['**/*.{png,jpg,gif}'],
                dest:'img/'
            },{
                expand: true,
                cwd: 'images/',
                src: ['**/*.{png,jpg,gif}'],
                dest:'images/'
            }],
        }
    },
    less: {
      style: {
        files: {
          "css/bootstrap_NM.css": "less/bootstrap_NM.less",
          "css/bootstrap_MS.css": "less/bootstrap_MS.less",
          "css/bootstrap_ID.css": "less/bootstrap_ID.less",
          "css/bootstrap_CA.css": "less/bootstrap_CA.less",
          "css/bootstrap_CT.css": "less/bootstrap_CT.less",
          "css/bootstrap_MN.css": "less/bootstrap_MN.less",
          "css/bootstrap_NV.css": "less/bootstrap_NV.less",
          "css/agency_portal.css": "less/agency_portal.less",
          "css/broker-search.css": "less/broker-search-responsive.less",
          "css/broker-search-id.css": "less/broker-search-id.less"
        }
      }
    },
    cssmin: {
      combine: {
        files: {
          'css/bootstrap_NM.css': ['css/bootstrap_NM.css']
        }
      }
    },
    watch: {
      css: {
        files: ['less/*.less'],
        tasks: ['less:style'],
        options: {
          livereload: true,
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-imagemin');
  grunt.loadNpmTasks('grunt-notify');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.task.run('notify_hooks');
};
