<script type="text/ng-template" id="income-invest.jsp">
  <div class="gutter10 jobIncome" ng-controller="incomeInvest" >
    <h3><spring:message code="label.lce.investmentincome"/></h3>
    <form class="form-horizontal" id="investmentIncome" name="investmentIncome" novalidate>
    <spring:message code="label.lce.howmuchdoes"/> {{householdMember.name.firstName}} {{householdMember.name.lastName}} <spring:message code="label.lce.getfrominvesting"/>
      <div class="control-group">
        <label class="control-label" for="typeOfRequest"></label>
        <div class="controls margin10-t">
          <div class="input-prepend">
            <span class="add-on">$</span>
            <input type="text" class="span4 margin10-r" value="700"  name="investmentIncomeAmount"  ng-model="householdMember.detailedIncome.investmentIncome.incomeAmount" required numbers-Only ng-trim />
            <select class="span5"  name="investmentIncomeAmountFrequency" ng-model="householdMember.detailedIncome.investmentIncome.incomeFrequency" required>
              <option><spring:message code="label.lce.weekly"/>Weekly</option>
			        <option><spring:message code="label.lce.biweekly"/>Bi-Weekly</option>
			        <option><spring:message code="label.lce.monthly"/>Monthly</option>
			        <option><spring:message code="label.lce.bimonthly"/>Bi-Monthly</option>
			        <option><spring:message code="label.lce.yearly"/>Yearly</option>
			        <option><spring:message code="label.lce.onceonly"/>Once Only</option>
            </select>
          </div>
        </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && investmentIncome.investmentIncomeAmount.$error.required"><spring:message code="label.lce.incomeinvesting.error1"/></span>
        </div>
        <div class="controls margin5-t">
          <span class="validation-error-text-container" ng-show="submitted && investmentIncome.investmentIncomeAmountFrequency.$error.required"><spring:message code="label.lce.incomeinvesting.error2"/></span>
        </div>
      </div>
    </form>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
      <a ng-show="investmentIncome.$valid"class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
      <a ng-show="!investmentIncome.$valid"class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>