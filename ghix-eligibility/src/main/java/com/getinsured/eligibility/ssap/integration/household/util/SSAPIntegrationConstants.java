package com.getinsured.eligibility.ssap.integration.household.util;

public class SSAPIntegrationConstants {

    // Verification Status constants
    public static final String MEC_VERIFICATION_STATUS = "mecVerificationStatus";
    public static final String CITIZENSHIP_IMMIGRATION_STATUS = "citizenshipImmigrationStatus";
    public static final String DEATH_STATUS = "deathStatus";
    public static final String INCARCERATION_STATUS = "incarcerationStatus";
    public static final String SSN_VERIFICATION_STATUS = "ssnVerificationStatus";
    public static final String VLP_VERIFICATION_STATUS = "vlpVerificationStatus";
    public static final String INCOME_VERIFICATION_STATUS = "incomeVerificationStatus";

    // SSAP JSON Field name constants
    public static final String SINGLE_STREAMLINED_APPLICATION = "singleStreamlinedApplication";
    public static final String TAX_HOUSEHOLD = "taxHousehold";
    public static final String HOUSEHOLD_MEMBER = "householdMember";
    public static final String CITIZENSHIP_AS_ATTESTED_INDICATOR = "citizenshipAsAttestedIndicator";
    public static final String CLIENT_IP = "clientIp";
    public static final String PERSON_ID = "personId";
    public static final String SSAP_APPLICATION_ID = "ssapApplicationId";
    public static final String APPLYING_FOR_COVERAGE_INDICATOR = "applyingForCoverageIndicator";
    public static final String APPLYING_FOR_FINANCIAL_ASSISTANCE_INDICATOR = "applyingForFinancialAssistanceIndicator";
    public static final String GENDER = "gender";
    public static final String SOCIAL_SECURITY_CARD = "socialSecurityCard";
    public static final String NAME_SAME_ON_SSN_CARD_INDICATOR = "nameSameOnSSNCardIndicator";
    public static final String APPLICATION_EVENT_ID = "applicationEventId";
    public static final String SSAP_JSON = "ssapJson";
    public static final String TAX_FILER_CATEGORY_CODE = "taxFilerCategoryCode";
    public static final String ELIGIBLE_IMMIGRATION_DOCUMENT_SELECTED = "eligibleImmigrationDocumentSelected";
    public static final String PRIMARY_TAX_FILER = "PRIMARY";
    public static final String ACCOUNT_TRANSFER_SERVICE = "accountTransferService";
    public static final String VLP_SERVICE = "vlpService";
    
    // SSAP Integration flow header key names
    public static final String CLIENT_IP_HEADER = "CLIENT_IP";
    public static final String SSAP_APPLICANT_DATA_HEADER = "SSAP_APPLICANT_DATA";
    public static final String SSAP_APPLICATION_ID_HEADER = "SSAP_APPLICATION_ID";
    public static final String SSAP_APPLICATION_EVENT_ID_HEADER = "APPLICATION_EVENT_ID";
    public static final String SSAP_JSON_HEADER = "SSAP_JSON";
    public static final String FILTERED_SSAP_JSON_HEADER = "FILTERED_SSAP_JSON";
    public static final String SSAP_MEMBER = "SSAP_MEMBER";
    public static final String NMEC_RESPONSE_HEADER = "NMEC_RESPONSE_JSON";
    public static final String CORRELATION_ID_HEADER = "CORRELATION_ID";
    public static final String MESSAGE_HISTORY = "history";
    

    // VLP Document Types
    public static final String I327 = "I327";
    public static final String I551 = "I551";
    public static final String I571 = "I571";
    public static final String I766 = "I766";
    public static final String I797 = "I797";
    public static final String CertOfCit = "CertOfCit";
    public static final String NatrOfCert = "NatrOfCert";
    public static final String NaturalizationCertificate = "NaturalizationCertificate";
    public static final String CitizenshipCertificate = "CitizenshipCertificate";
    public static final String MacReadI551 = "MacReadI551";
    public static final String TempI551 = "TempI551";
    public static final String I94 = "I94";
    public static final String I94Document = "I94Document";
    public static final String I94UnexpForeignPassport = "I94UnexpForeignPassport";
    public static final String UnexpForeignPassport = "UnexpForeignPassport";
    public static final String I20 = "I20";
    public static final String DS2019 = "DS2019";
    public static final String OtherCase1 = "OtherCase1";
    public static final String OtherCase2 = "OtherCase2";

    // Account Transfer verification states / constants
    public static final String VERIFICATION_PENDING = "VERIFICATION_PENDING";
    public static final String VERIFICATION_COMPLETE = "VERIFICATION_COMPLETE";
    public static final String FORCE_ACCOUNT_TRANSFER = "FORCE_ACCOUNT_TRANSFER";
    public static final String ACCOUNT_TRANSFER_RETRY_COUNT = "ACCOUNT_TRANSFER_RETRY_COUNT";
    
    public static final String VERIFIED = "VERIFIED";
    public static final String NOT_VERIFIED = "NOT_VERIFIED";
    
    public static final String FLOW_ACTION = "flowAction";
    public static final String STATUS_FAILURE = "FAILURE";
    public static final String STATUS_SUCCESS = "SUCCESS";
    public static final String EXCEPTION_CAUSE = "ExceptionCause";
    public static final String EXCEPTION_MESSAGE = "ExceptionMessage";
    public static final String EXCEPTION_TYPE = "Exception Type";
    public static final String SOCIAL_SECURITY_NUMBER = "socialSecurityNumber";
    public static final String NO_SSN = "NO_SSN";
    public static final Object APPLICATION_EVENT_TYPE = "applicationEventType";
}
