(function() {
    'use strict';

    angular.module('popoverDirective', [])
        .directive('giPopover', giPopover);

    function giPopover(){
        var ddo = {
            restrict: 'A',
            link: function(scope, element, attrs) {
                if (attrs.popoverHtml && attrs.popoverHtml.match(/[a-zA-Z]+/g) ) {
                    $(element).popover({
                        trigger: 'manual',
                        html: true,
                        content: attrs.popoverHtml,
                        placement: function (context, source) {
                            if (attrs.popoverPlacement !== undefined) {
                                return attrs.popoverPlacement;
                            }

                            var position = $(source).position();

                            if (position.left > 515) {
                                return "left";
                            }

                            if (position.left < 515) {
                                return "right";
                            }

                            if (position.top < 110){
                                return "bottom";
                            }

                            return "top";
                        },
                        "is-open" : true
                    }).on('mouseenter', function () {
                        var _this = this;
                        $(this).popover('show');
                        $('.popover').on('mouseleave', function () {
                            $(_this).popover('hide');
                        });
                    }).on('mouseleave', function () {
                        var _this = this;
                        setTimeout(function () {
                            if (!$('.popover:hover').length) {
                                $(_this).popover('hide');
                            }
                        }, 500);
                    });
                } else {
                    $(element).popover({
                        "is-open" : false
                    });
                }
            }
        };
        return ddo;
    }
})();
