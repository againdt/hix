package com.getinsured.hix.indportal.dto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ActiveEnrollmentHistoryComparator implements
		Comparator<EnrollmentHistoryEntry> {
	private static final Logger LOGGER = LoggerFactory.getLogger(ActiveEnrollmentHistoryComparator.class);

	@Override
	public int compare(EnrollmentHistoryEntry enrollHistoryOne,
			EnrollmentHistoryEntry enrollHistoryTwo) {
		if (StringUtils.isNotBlank(enrollHistoryOne.getCoverageEndDate())
				&& StringUtils
						.isNotBlank(enrollHistoryTwo.getCoverageEndDate())) {
			SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
			LocalDate enrollmentOneEndDate;
			LocalDate enrollmentTwoEndDate;
			try {
				enrollmentOneEndDate = new DateTime(mmddyyyy.parseObject(enrollHistoryOne.getCoverageEndDate())).toLocalDate();
				enrollmentTwoEndDate = new DateTime(mmddyyyy.parseObject(enrollHistoryTwo.getCoverageEndDate())).toLocalDate();
				if (enrollmentOneEndDate.isAfter(enrollmentTwoEndDate)) {
					return 1;
				}
			} catch (ParseException e) {
				LOGGER.error("Dates cannot be processed and defaulting to default sorting");
			}
		}

		return 0;
	}
}
