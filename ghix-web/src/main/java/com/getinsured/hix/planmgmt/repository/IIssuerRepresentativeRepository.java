package com.getinsured.hix.planmgmt.repository;



import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;

@Repository("iIssuerRepresentativeRepository")
public interface IIssuerRepresentativeRepository extends JpaRepository<IssuerRepresentative, Integer> {
	
	IssuerRepresentative findByUserRecordAndIssuer(AccountUser user, Issuer issuer);

	IssuerRepresentative findByUserRecord(AccountUser user);
	
	@Query("SELECT ir FROM IssuerRepresentative ir WHERE LOWER(ir.email) like LOWER(:email)")
	IssuerRepresentative findByEmail(@Param("email") String email);

	List<IssuerRepresentative> findRepListByIssuer(Issuer issuer);
	
	IssuerRepresentative findByIssuerAndPrimaryContact(Issuer issuer, PrimaryContact primaryContact);
	
	List<IssuerRepresentative> findByIssuerId(Integer issuerId);
	
	Page<IssuerRepresentative> findByIssuerId(Integer issuerId, Pageable pageable);//By Venkata Tadepalli; HIX-16301
	
	IssuerRepresentative findById(Integer repId);
	
	@Query("SELECT ir FROM IssuerRepresentative ir WHERE (ir.issuer.id=:issuerId)  AND ((LOWER(ir.firstName) like LOWER(:repFirstName)||'%') OR (LOWER(ir.lastName) like LOWER(:repLastName)||'%')) AND ((ir.title IS NOT NULL AND LOWER(ir.title) like LOWER(:repTitle)||'%'))")
	Page<IssuerRepresentative> findIssuerRepresentative(@Param("repFirstName") String repFirstName, @Param("repLastName") String repLastName, @Param("repTitle") String repTitle, @Param("issuerId") Integer issuerId, Pageable pageable);
	
	@Query("SELECT ir FROM IssuerRepresentative ir WHERE (ir.issuer.id=:issuerId)  AND ((LOWER(ir.firstName) like LOWER(:repFirstName)||'%') OR (LOWER(ir.lastName) like LOWER(:repLastName)||'%'))")
	Page<IssuerRepresentative> findIssuerRepresentative(@Param("repFirstName") String repFirstName, @Param("repLastName") String repLastName, @Param("issuerId") Integer issuerId, Pageable pageable);
	
//	@Query("SELECT ir FROM IssuerRepresentative ir WHERE (ir.issuer.id=:issuerId) AND ((LOWER(ir.firstName) like LOWER(:repFirstName)||'%') OR (LOWER(ir.lastName) like LOWER(:repLastName)||'%')) AND ((ir.title IS NOT NULL AND LOWER(ir.title) like LOWER(:repTitle)||'%'))")
//	Page<IssuerRepresentative> findIssuerRepresentative(@Param("repFirstName") String repFirstName, @Param("repLastName") String repLastName, @Param("repTitle") String repTitle,@Param("issuerId") Integer issuerId, Pageable pageable);
//	
//	@Query("SELECT ir FROM IssuerRepresentative ir WHERE (ir.issuer.id=:issuerId) AND ((LOWER(ir.firstName) like LOWER(:repFirstName)||'%') OR (LOWER(ir.lastName) like LOWER(:repLastName)||'%'))")
//	Page<IssuerRepresentative> findIssuerRepresentative(@Param("repFirstName") String repFirstName, @Param("repLastName") String repLastName,@Param("issuerId") Integer issuerId, Pageable pageable);

	@Query("SELECT ir.userRecord.id FROM IssuerRepresentative ir WHERE ir.issuer.id=:issuerId AND ir.primaryContact='YES'")
	Integer getUserIdFromIssuer(@Param("issuerId") Integer issuerId);
 	
	@Modifying
    @Transactional
    @Query("UPDATE IssuerRepresentative as ir SET ir.primaryContact ='NO' WHERE ir.issuer.id=:issuerId AND ir.primaryContact ='YES' ")
    void updatePrimaryContactToNo(@Param("issuerId") int issuerId);
	
	@Modifying
    @Transactional
    @Query("UPDATE IssuerRepresentative as ir SET ir.primaryContact ='NO' WHERE ir.issuer.id=:issuerId AND ir.primaryContact ='YES' AND ir.id != :issuerRepId")
    void updatePrimaryContactToNo(@Param("issuerId") int issuerId, @Param("issuerRepId") int issuerRepId);
}

