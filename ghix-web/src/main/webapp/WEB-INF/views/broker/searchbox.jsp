<%@ page import="com.getinsured.hix.model.AccountUser"%>
<%@ page import="com.getinsured.hix.model.Location"%>
<%@ page import="com.getinsured.hix.model.Broker"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<link href="../resources/css/assister.css" media="screen"
	rel="stylesheet" type="text/css" />
		<script src="../resources/js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="../resources/js/bootstrap-typeahead.js" type="text/javascript"></script>
	<link href="../resources/css/assister.css" media="screen" rel="stylesheet" type="text/css" />
	<link href="../resources/css/accessibility.css" media="screen" rel="stylesheet" type="text/css" />

	<script type="text/javascript"
		src='<c:url value="/resources/js/jquery.validate.min.js" />'></script>
	<!-- start of secondary navbar - TO DO: Move to topnav.jsp -->
	<!-- <ul class="nav nav-tabs">
		<li class="active"><a href="#">Home</a></li>
		<li><a href="#">Find Insurance</a></li>
		<li><a href="#">Learn More</a></li>   
		<li><a href="#">Get Assistance</a></li>   
	</ul> -->
	<!-- end of secondary navbar -->

<div class="gutter10" role="search">
	<div class="row-fluid">
		<h1>
			<a name="skip" class="skip"><spring:message  code="label.brksearchinfo"/></a>
		</h1>
		<p><spring:message code="label.brkfillforminfo"/></p>
	</div>
	<div class="row-fluid">
		<form class="form-horizontal gray" id="frmbrokersearch"
			name="frmbrokersearch" action="searchlist" method="POST">
					<!-- page header -->
					<div class="span6 margin0" id="name_search">
						<h4><spring:message code="label.searchbyname"/></h4>
						<div class="control-group">
							<label for="user-firstName" class="control-label"><spring:message code="label.brkFirstName"/></label>
							<div class="controls">
								<input type="text" name="user.firstName" id="user.firstName"
									value="${sessionScope.brokersearchparam.user.firstName}"
									class="" size="30" />
							</div>
							<!-- end of controls -->
						</div>
						<!-- end of control-group -->
						<div class="control-group">
							<label for="user-lastName" class="control-label"><spring:message code="label.brkLastNames"/></label>
							<div class="controls">
								<input type="text" name="user.lastName" id="user.lastName"
									value="${sessionScope.brokersearchparam.user.lastName}"
									class="" size="30" />
							</div>
							<!-- end of controls -->
						</div>
						<!-- end of control-group -->
						<div class="control-group">
							<div class="controls">
								<input type="submit" name="submit" id="submit"
									value="<spring:message  code='label.search'/>"
									class="btn btn-primary" 
									title="<spring:message  code='label.search'/>"/>
							</div>
						</div>
					<!-- end of control-group -->
						</div>
						
						
						<div class="span6 vr margin0" id="location_search">
						<h4><spring:message code="label.searchbylocation"/></h4>
						<div class="control-group">
							<label for="ZipCode" class="control-label"><spring:message code="label.brkZipCode"/></label>
							<div class="controls">
								<input type="text" id="location.zip"
									value="${sessionScope.brokersearchparam.location.zip!=0?sessionScope.brokersearchparam.location.zip:''}"
									name="location.zip" class="input-small" maxlength="5" />
							</div>
							<div id="zip_error"></div>
							<!-- end of controls -->
						</div>
						<!-- end of control-group -->

						<div class="control-group">
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistance"/></label>
							<div class="controls">
								<select id="Distancer" name="distance" class="input-mini">
									<option value="5"
										<c:if test="${sessionScope.brokermaxdis == '5'}"> selected="selected"</c:if>>5</option>
									<option value="10"
										<c:if test="${sessionScope.brokermaxdis == '10'}"> selected="selected"</c:if>>10</option>
									<option value="15"
										<c:if test="${sessionScope.brokermaxdis == '15'}"> selected="selected"</c:if>>15</option>
									<option value="20"
										<c:if test="${sessionScope.brokermaxdis == '20'}"> selected="selected"</c:if>>20</option>
									<option value="30"
										<c:if test="${sessionScope.brokermaxdis == '30'}"> selected="selected"</c:if>>30</option>
									<option value="more"
										<c:if test="${sessionScope.brokermaxdis == 'more'}"> selected="selected"</c:if>>50+</option>
								</select> (miles)
							</div>
							<!-- end of controls -->
						</div>
						<!-- end of control-group -->

						<div class="control-group">
							<label for="languagesSpokentest" class="control-label"><spring:message code="label.brkLanguagesSpoken" /></label>
							<div class="controls">
								<select name="languagesSpoken" id="languagesSpokentest">
									<option value="">Any</option>
									<option value="English"
										<c:if test="${sessionScope.brokersearchparam.languagesSpoken == 'English'}"> SELECTED </c:if>>English</option>
									<option value="Spanish"
										<c:if test="${sessionScope.brokersearchparam.languagesSpoken == 'Spanish'}"> SELECTED </c:if>>Spanish</option>
									<option value="German"
										<c:if test="${sessionScope.brokersearchparam.languagesSpoken == 'German'}"> SELECTED </c:if>>German</option>
									<option value="Swedish"
										<c:if test="${sessionScope.brokersearchparam.languagesSpoken == 'Swedish'}"> SELECTED </c:if>>Swedish</option>
									<option value="Navajo"
										<c:if test="${sessionScope.brokersearchparam.languagesSpoken == 'Navajo'}"> SELECTED </c:if>>Navajo</option>
								</select>
							</div>
							<!-- end of control-group -->
						</div>
						<!-- end of control-group -->

						<div class="control-group">
							<div class="controls">
								<input type="submit" name="submit" id="submit"
									value="<spring:message  code='label.search'/>"
									class="btn btn-primary" 
									title="<spring:message  code='label.search'/>"/>
							</div>
							<!-- end of control-group -->
						</div>
						<!-- end of control-group -->
						</div>
		</form>
	</div> <!-- end of row-fluid -->
</div> <!-- end of .gutter10 -->

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->		

<script type="text/javascript">
var validator = $("#frmbrokersearch").validate({ 
	rules : {
		zip : { required : false, 
	    	    number : true,
	        	minlength : 5,
	        	maxlength : 5
      	}
	}, 
	messages : {
		zip : { number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber.aee' javaScriptEscape='true'/></span>",
	  			minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>",              
	  			maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	}
});

function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
          String.prototype.trim = function() {
           	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
          }
	}
}

jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) )){ 
		return false; 
	}
	return true;
});
</script>

