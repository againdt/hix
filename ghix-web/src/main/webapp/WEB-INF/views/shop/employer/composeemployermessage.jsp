<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>
	
<div class="gutter10">
		<h1>${employer.name} <small>&nbsp;Contact Name: ${employer.contactFirstName} ${employer.contactLastName}</small></h1>
	<div class="row-fluid">
		<div class="row-fluid">
			<div class="span3">
				<div class="gray">
					<h4 class="margin0">About this Employer</h4>
						<ul class="nav nav-list">
	<%-- 			                  <li class="active"><a href="<c:url value="/shop/employer/employercasesummary/${employer.id}"/>">Summary</a></li> --%>
	<%-- 			                  <li><a href="<c:url value="/shop/employer/employercasecontactinfo/${employer.id}"/>">Contact Info</a></li> --%>
							<li><a href="/hix/shop/employer/employercase/${employer.id}">Summary</a></li>
		                 	<li><a href="/hix/shop/employer/employercontactinfo/${employer.id}">Contact Info</a></li>
		                  	<li><a href="#">Messages</a></li>
		                <!-- <li><a href="#">Notifications</a></li> -->
		                  	<li><a href="/hix/shop/employer/employercomments/${employer.id}">Comments</a></li>
		                </ul>
				</div>
				<br>
				<div class="gray">
					<h4 class="margin0"><i class="icon-cog icon-white"></i> Actions</h4>
						<ul class="nav nav-list">
		                  <li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> View Employer Account</a></li>
		                  <li><a href="#"><i class="icon-edit"></i> Compose Message</a></li>
		                  <li><a href="#"><i class="icon-comment"></i> New Comment</a></li>
		                </ul>
				</div>
		</div>
	
	
		<div id="rightpanel" class="span9">
			<font size="3" color="red">This page is under construction</font> 
		</div>
	</div>
</div>

</div>
		 
		<!-- Modal -->
		<div id="viewempModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3 id="myModalLabel">View Employer Account</h3>
			</div>
			<div class="modal-body">
			<p>Clicking &#34;Employer View&#34; will take you to the Employer&#39;s portal for ${employer.name}.
				Through this portal you will be able to take actions on behalf of this employer, such as view and edit employee list, fill out employer eligibility, etc.
			</p>
			<p>
				Proceed to Employer view?
			</p>
			</div>
			<div class="modal-footer">
				<label class="checkbox pull-left">
					<input type="checkbox" value="">
					Don&#39;t show this message again.
				</label>
			<button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
				 <a class="btn btn-primary""  type="button"  href="<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>">Employer View</a>
			
			</div>
		</div>	
	
	
<!--  Latest UI -->
	
	
	