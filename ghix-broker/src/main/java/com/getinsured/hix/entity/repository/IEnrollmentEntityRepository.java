package com.getinsured.hix.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.EnrollmentEntity;

/**
 * Spring-JPA repository that encapsulates methods that interact with the
 * Enrollment Entity table
 */
public interface IEnrollmentEntityRepository extends JpaRepository<EnrollmentEntity, Integer> {
	/**
	 * Finds the Enrollment Entity by given email address
	 * 
	 * @param emailAddress
	 *            the given email address
	 * @return the existing Enrollment Entity if found
	 */
	@Query("FROM EnrollmentEntity where primaryEmailAddress = :emailAddress")
	EnrollmentEntity findByEmailAddress(@Param("emailAddress") String emailAddress);
	
	/**
	 * Finds the Enrollment Entity by given state EIN
	 * 
	 * @param stateTaxID
	 *            the given state EIN address
	 * @return the existing Enrollment Entity if found
	 */
	@Query("FROM EnrollmentEntity where stateTaxID = :stateEIN")
	EnrollmentEntity findByStateTaxID(@Param("stateEIN") String stateTaxID);
}