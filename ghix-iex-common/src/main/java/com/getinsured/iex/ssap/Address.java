package com.getinsured.iex.ssap;

import javax.validation.constraints.Pattern;

import org.apache.commons.lang.StringUtils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Address {
	@Expose
	@Pattern(regexp="(^[0-9]{5}$)|^$")
	private String postalCode;
	@Expose
	//@Pattern(regexp="^[0-9a-zA-Z .'#,\\u005C-]+")
	private String county;
	@Expose
	@Pattern(regexp="|^[0-9]{5}$")
	private String primaryAddressCountyFipsCode;
	@Expose
	private String streetAddress1;
	@Expose
	private String streetAddress2;
	@Expose
	@Pattern(regexp="|^[A-Z]{2}$")
	private String state;
	@Expose
	private String city;

	@Expose
	private String countyCode;

	@Expose
	private int addressId;

	public String getCountyCode() {
		return countyCode;
	}

	public void setCountyCode(String countyCode) {
		this.countyCode = countyCode;
	}

	public String getCounty() {
		return county;
	}

	public void setCounty(String county) {
		this.county = county;
	}

	public String getPrimaryAddressCountyFipsCode() {
		return primaryAddressCountyFipsCode;
	}

	public void setPrimaryAddressCountyFipsCode(String primaryAddressCountyFipsCode) {
		this.primaryAddressCountyFipsCode = primaryAddressCountyFipsCode;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getStreetAddress1() {
		return streetAddress1;
	}

	public void setStreetAddress1(String streetAddress1) {
		this.streetAddress1 = StringUtils.trim(streetAddress1);
	}

	public String getStreetAddress2() {
		return streetAddress2;
	}

	public void setStreetAddress2(String streetAddress2) {
		this.streetAddress2 = StringUtils.trim(streetAddress2);
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = StringUtils.trim(city);
	}

	/**
	 * Returns address id for this address, assignable on the UI.
	 * <p>
	 *   This is used to build personId => addressId matrix.
	 * </p>
	 * @return address id for this address.
	 */
	public int getAddressId() {
		return addressId;
	}

	/**
	 * Sets address id for this address, used on the UI to distinguish
	 * between various addresses.
	 * @param addressId address id to set.
	 */
	public void setAddressId(int addressId) {
		this.addressId = addressId;
	}
}
