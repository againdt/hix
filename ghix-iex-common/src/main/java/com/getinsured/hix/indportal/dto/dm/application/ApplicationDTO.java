package com.getinsured.hix.indportal.dto.dm.application;

public class ApplicationDTO {
	 private String resultType;
	 private Links links;
	 private Result result;


	 // Getter Methods 

	 public String getResultType() {
	  return resultType;
	 }

	 public Links getLinks() {
	  return links;
	 }

	 public Result getResult() {
	  return result;
	 }

	 // Setter Methods 

	 public void setResultType(String resultType) {
	  this.resultType = resultType;
	 }

	 public void setLinks(Links linksObject) {
	  this.links = linksObject;
	 }

	 public void setResult(Result result) {
	  this.result = result;
	 }
	}
