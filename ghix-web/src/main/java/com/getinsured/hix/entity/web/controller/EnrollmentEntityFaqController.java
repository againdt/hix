package com.getinsured.hix.entity.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * 
 * FAQ Controller class to intercept any FAQ specific requests
 * for Enrollment Entity
 * 
 */
@Controller
public class EnrollmentEntityFaqController {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityFaqController.class);

	/**
	 * Intercepts any FAQ specific requests for enrollment entity
	 */
	@RequestMapping(value = "/entity/enrollmententity/faq", method = {RequestMethod.GET,RequestMethod.POST})
	public String interceptRequest(Model model, HttpServletRequest request) {
		
		

		LOGGER.info("interceptRequest: START");
		
		LOGGER.info("InterceptRequest method has been invoked");
		

		LOGGER.info("interceptRequest: END");

		return "entity/enrollmententity/faq";
	}
}
