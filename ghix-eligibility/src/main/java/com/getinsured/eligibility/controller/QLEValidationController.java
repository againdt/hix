package com.getinsured.eligibility.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.exception.InvalidIncomingValueException;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.hix.dto.ssap.SsapApplicantEvent;



@Controller
@RequestMapping("/ssap")
public class QLEValidationController {
	
	private static final String INVALID_CASE_NUMBER_OR_LAST_UPDATED_USER_ID = "Invalid case_number or lastUpdatedUserId";
	
	@Autowired private QLEValidationService qleValidationService;
	
	@RequestMapping(value = "/application/{case_number}/validate", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Optional<ApplicationValidationStatus>> updateValidationStatus(@PathVariable("case_number") String caseNumber, 
			@RequestParam(value = "source", required = false, defaultValue = "EXCHANGE" ) Source source,
			@Valid @RequestBody Integer lastUpdatedUserId) {
		
		if (lastUpdatedUserId == null || caseNumber == null){
			throw new InvalidIncomingValueException(INVALID_CASE_NUMBER_OR_LAST_UPDATED_USER_ID);
		}
		
		Optional<ApplicationValidationStatus> status = qleValidationService.runValidationEngine(caseNumber, source, lastUpdatedUserId);

		return new ResponseEntity<Optional<ApplicationValidationStatus>>(status , HttpStatus.OK);
	}
	
	@RequestMapping(value = "/applicantevent/{applicant_event_id}", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.PUT) 
	@ResponseBody
	public ResponseEntity<Optional<Boolean>> updateApplicantEvent(@PathVariable("applicant_event_id") Long applicantEventID, 
			@Valid @RequestBody SsapApplicantEvent event) {
		
		boolean result = qleValidationService.updateApplicantEventAndRerunValidationEngine(applicantEventID, event);
		Optional<Boolean> response = Optional.of(result);
		return new ResponseEntity<Optional<Boolean>>(response , HttpStatus.OK);
		
	}
	
	@RequestMapping(value = {"/application/{case_number}/cancel"}, 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Void> cancelEvents(@PathVariable("case_number") String caseNumber,
			@Valid @RequestBody Integer lastUpdatedUserId) {
		
		if (lastUpdatedUserId == null || caseNumber == null){
			throw new InvalidIncomingValueException(INVALID_CASE_NUMBER_OR_LAST_UPDATED_USER_ID);
		}
		
		qleValidationService.discardApp(caseNumber, ApplicationStatus.CANCELLED, lastUpdatedUserId);
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	
	@RequestMapping(value = {"/application/{case_number}/close"}, 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<Void> closeEvents(@PathVariable("case_number") String caseNumber,
			@Valid @RequestBody Integer lastUpdatedUserId) {
		
		if (lastUpdatedUserId == null || caseNumber == null){
			throw new InvalidIncomingValueException(INVALID_CASE_NUMBER_OR_LAST_UPDATED_USER_ID);
		}
		
		qleValidationService.discardApp(caseNumber, ApplicationStatus.CLOSED, lastUpdatedUserId);
		return new ResponseEntity<Void>(HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/application/{case_number}/deny", 
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			method = RequestMethod.POST) 
	@ResponseBody
	public ResponseEntity<Optional<Boolean>> denySep(@PathVariable("case_number") String caseNumber,
			@Valid @RequestBody Integer lastUpdatedUserId) {
		
		if (lastUpdatedUserId == null || caseNumber == null){
			throw new InvalidIncomingValueException(INVALID_CASE_NUMBER_OR_LAST_UPDATED_USER_ID);
		}
		
		boolean result = qleValidationService.denySep(caseNumber, lastUpdatedUserId);
		Optional<Boolean> response = Optional.of(result);
		return new ResponseEntity<Optional<Boolean>>(response , HttpStatus.OK);
		
	}
	
	

}