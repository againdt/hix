<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<ul class="nav nav-list">
	<input type="hidden" id="currentPage" value='<%=request.getParameter("pageName")%>'>

    <li id="issuerDetailsTab"><a id="issuerDetailsLink" href="<c:url value="/planmgmt/issuer/account/profile/issuerprofile"/>"><spring:message  code="pgheader.issuerProfile"/></a></li>
    <c:if test="${STATE_CODE != 'ID' && STATE_CODE != 'MN' }">
            <li id="financialInfoTab"><a id="financialInfoLink" href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
    </c:if>
    <li id="companyProfileTab"><a id="companyProfileLink" href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
    <li id="individualProfileTab"><a id="individualProfileLink" href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info"/>"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
    <li id="accreditationTab"><a id="accreditationLink" href="<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/view" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
    <li id="certStatusTab"><a id="certStatusLink" href="<c:url value="/planmgmt/issuer/certification/status"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
    <li id="issuerHistoryTab"><a id="issuerHistoryLink" href="<c:url value="/planmgmt/issuer/account/profile/viewhistory"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
    <li id="crossWalkTab"><a id="crossWalkLink" href="<c:url value="/planmgmt/issuer/crossWalkStatus"/>"><spring:message  code="pgheader.planIdCrosswalk"/></a></li>
    <c:if test="${STATE_CODE == 'ID'}">
		<li id="netTransTab"><a id="netTransLink" href="<c:url value="/planmgmt/issuer/displayNetworkTransparencyData"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
    </c:if>
</ul>

<script type="text/javascript">
  $(document).ready(function(){
	$("#"+$("#currentPage").val()+"Tab").addClass("active");
	$("#"+$("#currentPage").val()+"Link").attr("href", "#");
  });
</script>