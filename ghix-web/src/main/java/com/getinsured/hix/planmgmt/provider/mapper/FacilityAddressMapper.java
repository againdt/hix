package com.getinsured.hix.planmgmt.provider.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.model.FacilityAddress;
import com.getinsured.hix.model.FacilityAddress.Status;
import com.getinsured.hix.platform.file.mapper.Mapper;

public class FacilityAddressMapper extends Mapper<FacilityAddress> {

	private static Map<String, Integer> columnKeys = null;
	
	private static final String ADDRESS = "Address";
	private static final String CITY = "City";
	private static final String STATE = "State";
	private static final String ZIP = "Zip";
	private static final String EXTZIP = "Ext Zip";
	private static final String PHONE = "Phone";
	private static final String FAX = "Fax";
	private static final String COUNTY = "County";
	private static final String STATUS = "Status";
	
	/* Provider Table Column Entries */
	private static final String DATASOURCE_REF_ID = "Ref ID";
	
	private static final int ADDRESS_VAL = 1;
	private static final int CITY_VAL = 2;
	private static final int STATE_VAL = 3;
	private static final int ZIP_VAL = 4;
	private static final int EXTZIP_VAL = 5;
	private static final int PHONE_VAL = 6;
	private static final int FAX_VAL = 7;
	private static final int COUNTY_VAL = 8;
	private static final int STATUS_VAL = 9;
	
	/* Provider Table Column Entry key's */
	private static final int DATASOURCE_REF_ID_VAL = 10;
	
	static {
		columnKeys = new HashMap<String, Integer>();
		columnKeys.put(ADDRESS, ADDRESS_VAL);
		columnKeys.put(CITY, CITY_VAL);
		columnKeys.put(STATE, STATE_VAL);
		columnKeys.put(ZIP, ZIP_VAL);
		columnKeys.put(EXTZIP, EXTZIP_VAL);
		columnKeys.put(PHONE, PHONE_VAL);
		columnKeys.put(FAX, FAX_VAL);
		columnKeys.put(COUNTY, COUNTY_VAL);
		columnKeys.put(STATUS, STATUS_VAL);
		columnKeys.put(DATASOURCE_REF_ID, DATASOURCE_REF_ID_VAL);
	}

	public FacilityAddress mapData(final List<String> data, final List<String> columns) 
	{
		FacilityAddress facilityAddress = new FacilityAddress();
		
		
		int index = 0;
		for (String column : columns)
		{
			mapData(data.get(index), column,facilityAddress);
			index++;
		}
	
		return facilityAddress;
	}
	
	private void mapData(String data, String column,FacilityAddress facilityAddress)
	{ 
		
		
		int colVal = columnKeys.get(column.replace("\"", ""));
		String trimData = data.trim();
		
		switch (colVal)
		{
			
			case ADDRESS_VAL:
				facilityAddress.setAddress1(trimData);
				
			break;
			case CITY_VAL:
				facilityAddress.setCity(trimData);
				
			break;
			case STATE_VAL:
				facilityAddress.setState(trimData);
				
			break;
			case ZIP_VAL:
				facilityAddress.setZip(trimData);
				
			break;
			case EXTZIP_VAL:
				facilityAddress.setExtendedZipcode(trimData);
				
			break;
			case PHONE_VAL:
				facilityAddress.setPhoneNumber(trimData);
				
			break;
			case FAX_VAL:
				facilityAddress.setFax(trimData);
				
			break;
			case COUNTY_VAL:
				facilityAddress.setCounty(trimData);
				
			break;
			case STATUS_VAL:
				if(trimData.equalsIgnoreCase(Status.ACTIVE.toString())){
					facilityAddress.setStatus(Status.ACTIVE);
				}
				else if(trimData.equalsIgnoreCase(Status.INACTIVE.toString())){
					facilityAddress.setStatus(Status.INACTIVE);
				}
				
				
			break;
			case DATASOURCE_REF_ID_VAL:
				facilityAddress.setDatasourceRefId(trimData);
				
			break;
			default:
				break;
		}
	}
}
