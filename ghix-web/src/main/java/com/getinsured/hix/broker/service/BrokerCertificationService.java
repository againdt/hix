package com.getinsured.hix.broker.service;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.model.Broker;

/**
 * BrokerCertificationService is DAL Service class for Broker Certification
 * uses {@link BrokerService} to retrieve data. 
 */
public class BrokerCertificationService {

	@Autowired
	private BrokerService brokerServiceObj;

	/**
	 * Retrieves Broker's License number
	 * 
	 * @param appData
	 * @return {@link Map} of String, String
	 */
	public Map<String, String> brokerService(Map<String, String> appData) {
		Broker brokerObj;
		brokerObj = brokerServiceObj.findBrokerByLicenseNumber(appData
				.get("licenseNumber").toString());

		Map<String, String> responseData = new HashMap<String, String>();
		responseData.put("licenseNumber", brokerObj.getLicenseNumber());
		responseData.put("Result", "SUCCESS");
		return responseData;
	}
}
