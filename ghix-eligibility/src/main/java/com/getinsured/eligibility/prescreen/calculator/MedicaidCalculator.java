package com.getinsured.eligibility.prescreen.calculator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.eligibility.prescreen.repository.IStateChipMedicaidRepository;
import com.getinsured.hix.model.estimator.mini.FamilyMember;
import com.getinsured.hix.model.estimator.mini.MedicaidRequest;
import com.getinsured.hix.model.estimator.mini.MedicaidResponse;
import com.getinsured.hix.model.estimator.mini.StateChipMedicaid;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;


/**
 * State specific Medicaid/CHIP calculator for the mini-estimator
 * JIRA: HIX-12653
 * 
 * @author Nikhil Talreja
 * @since 08 July, 2013
 *
 */

@Component
public class MedicaidCalculator{
	
	private static final Logger LOGGER = Logger.getLogger(MedicaidCalculator.class);

	//Static Map containing Medicaid/CHIP data
	private static Map<String, Map<String,String>> stateSpecificMedicaidInfo;
	
	//Constants
	//private static final String CHIP = "CHIP Limit";
	private static final String CHIP_THRESHOLD_0_1 = "CHIP0to1";
	private static final String CHIP_THRESHOLD_1_5 = "CHIP1to5";
	private static final String CHIP_THRESHOLD_6_18 = "CHIP6to18";
	private static final String CHIP_THRESHOLD_18_19 = "CHIP18to19";
	//private static final String MEDICAID = "Medicaid Threshold";
	private static final String MEDICAID_PARENTS = "Medicaid Parents Threshold";
	private static final String MEDICAID_PREGNANT = "Medicaid Pregnant Member Threshold";
	private static final String MEDICAID_OTHERS = "Medicaid Other Adult Threshold";
	private static final String SBE_URL = "SBE Web Site";
	private static final String SBE_PHONE = "SBE Phone Number";
	private static final String MEDICAID_EXPANSION = "Medicaid Expansion";
	private static final int FIFTEEN = 15;
	
	private static final int CHIP_AGE = 19;
	private static final int MEDICARE_AGE_YEARS = 65; //64 years and 270 days
	private static final int MEDICARE_AGE_MONTHS = 0; //64 years and 270 days
	private static final double APTC_THRESHOLD = 400.0;
	protected static final double HUNDRED_POINT_ZERO = 100.0;
	private int chipCounter = 0;
	private int medicareCounter = 0;
	private int nativeAmericanCounter=0;
	private int medicaidCounter=0;
	
	//Error Codes
	private static final int NULL_REQUEST_ERROR_CODE = 10001;
	private static final int MISSING_STATE_CODE_ERROR_CODE = 10002;
	private static final int INVALID_STATE_CODE_ERROR_CODE = 10003;
	private static final int EXCEPTION_ERROR_CODE = 99999;
	
	//Error Messages
	private static final String NULL_REQUEST_ERROR_MESSAGE = "Request object is null";
	private static final String MISSING_STATE_CODE_ERROR_MESSAGE = "State Code is blank";
	private static final String INVALID_STATE_CODE_ERROR_MESSAGEE = "Invalid State or State information not available";

	//private static final int FOUR = 4;
	//private static final int SIXTY = 60;
	//private static final int THOUSAND = 1000;
	//private static final int NO_OF_HOURS_IN_DAY = 24;
	//private static final int LEAP_YEAR_DAYS = 366;
	//private static final int NON_LEAP_YEAR_DAYS = 365;
	
	@Autowired private IStateChipMedicaidRepository iStateChipMedicaidRepository;
	
	//Populate Map
	public void populateStateChipMedicaidMap(){
		
		if(stateSpecificMedicaidInfo != null){
			return;
		}
		
		LOGGER.info("Populating Medicaid/CHIP information");
		
		stateSpecificMedicaidInfo = new HashMap<String, Map<String,String>>();
	
		List<StateChipMedicaid> records =  iStateChipMedicaidRepository.findAll();
		
		for(StateChipMedicaid record : records){
			
			Map<String,String> values = new HashMap<String, String>();
			//values.put(CHIP, new Double(record.getChipIncomeLimit()).toString());
			values.put(CHIP_THRESHOLD_0_1, new Double(record.getChipIncomeLimit0to1()).toString());
			values.put(CHIP_THRESHOLD_1_5, new Double(record.getChipIncomeLimit1to5()).toString());
			values.put(CHIP_THRESHOLD_6_18, new Double(record.getChipIncomeLimit6to18()).toString());
			if(record.getChipIncomeLimit18to19()>0 )
			{
				values.put(CHIP_THRESHOLD_18_19, new Double(record.getChipIncomeLimit18to19()).toString() );
			}
			else
			{
				values.put(CHIP_THRESHOLD_18_19, new Double(0).toString());	
			}
			//values.put(MEDICAID, new Double(record.getMedicaidIncomeThreshold()).toString());
			values.put(MEDICAID_PARENTS, new Double(record.getMedicaidParentThreshold()).toString());
			//New Medicaid thresholds for Other Adults
			values.put(MEDICAID_OTHERS, new Double(record.getMedicaidOthersThreshold()).toString());
			values.put(MEDICAID_PREGNANT, new Double(record.getMedicaidPregnantThreshold()).toString());
			values.put(SBE_URL, record.getSbeWebsite());
			values.put(SBE_PHONE, record.getSbePhone());
			values.put(MEDICAID_EXPANSION, record.getMedicaidExpansion());
			stateSpecificMedicaidInfo.put(record.getStateCode(), values);
			
		}
	}
	
	/**
	 * Calculates household's Medicaid/CHIP eligibility
	 * 
	 * @author - Nikhil Talreja
	 * @since - 08 July, 2013
	 */
	public MedicaidResponse calculateMedicaidAndCHIPEligibility(MedicaidRequest request){
		
		MedicaidResponse response = new MedicaidResponse();
		
		try{
			
			response.startResponse();
			
			//Populate Medicaid/CHIP info map
			populateStateChipMedicaidMap();
			
			//Validate Request
			boolean isRequestValid = validateMedicaidRequest(request, response,stateSpecificMedicaidInfo);
			if(!isRequestValid){
				return response;
			}
			
			String state = request.getStateCode();
			Map<String,String> values = stateSpecificMedicaidInfo.get(state);
			
			//Set SBE Website and URL
			response.setSbeWebsite(values.get(SBE_URL));
			response.setSbePhone(values.get(SBE_PHONE));
			
			/*
			 * Step 1: Check if entire household is eligible for Medicaid
			 * This is true if household FPL % is less than State Medicaid Income Threshold
			 * and State has elected to expand medicaid to 138% FPL
			 */
			
			if(checkForMedicaidHousehold(request, response, values)){
				return response;
			}
			
			LOGGER.info("Entire household is not Medicaid Eligible");
			
			
			/*
			 * Step 2: Loop through each member to assign specific eligibility (CHIP/APTC/NA)
			 * IF 
			 * 1. all members are eligible for CHIP, set corresponding flag to true
			 * 2. all members are eligible for Medicare set corresponding flag to true 
			 */
			
			chipCounter = 0;
			medicareCounter = 0;
			nativeAmericanCounter=0;
			medicaidCounter=0;
			checkForMemberEligibility(request, response,values);
			
			int noOfApplicants = getNoOfApplicants(request);
			// Is Household NativeAmerican
			if(nativeAmericanCounter==noOfApplicants)
			{
				response.setNativeAmericanHousehold(true);
			}
			
			//Entire household is eligible for CHIP
			if(chipCounter == noOfApplicants){
				response.setHouseholdCHIPEligible(true);
			}
			
			//Entire household is eligible for Medicare
			if(medicareCounter == noOfApplicants){
				response.setHouseholdMedicareEligible(true);
			}
			//Entire household is eligible for Medicaid
			if(medicaidCounter == noOfApplicants){
				response.setHouseholdMedicaidEligible(true);
			}
			
			
			response.setMembers(request.getMembers());
		}
		catch(Exception e){
			response.setErrCode(EXCEPTION_ERROR_CODE);
			response.setErrMsg("Exception occured while calulating Medicaid/CHIP eligibility");
			LOGGER.error(e.getMessage(),e);
		}
		
		response.endResponse();
		return response;
	}
	
	/**
	 * Method to check if member-wise eligibility
	 * JIRA: HIX-12653
	 * 
	 * @author Nikhil Talreja
	 * @since 02 September, 2013
	 */
	public void checkForMemberEligibility(MedicaidRequest request, MedicaidResponse response, Map<String,String> values){
		
		//String chipThreshold = values.get(CHIP);
		String chipThreshold0to1 = values.get(CHIP_THRESHOLD_0_1);
		String chipThreshold1to5 = values.get(CHIP_THRESHOLD_1_5);
		String chipThreshold6to18 = values.get(CHIP_THRESHOLD_6_18);
		String chipThreshold18to19 = values.get(CHIP_THRESHOLD_18_19);
		String medicaidPregnantThreshold = values.get(MEDICAID_PREGNANT);
		
				
					
				
		/*LOGGER.debug("State CHIP Threshold : " + chipThreshold);
		double chipFpl = 0.0;
		chipFpl = Double.parseDouble(chipThreshold);*/
		
		/* String medicaidThreshold = values.get(MEDICAID);
		double medicaidFpl = 0.0; 
		medicaidFpl = Double.parseDouble(medicaidThreshold); */
		
		double medicaidFpl =getMedicaidThreshold(request,values);
		int sequenceNumber = 1;
		double age = 0.0;
		
		for(FamilyMember member : request.getMembers()){
			
			member.setSequenceNumber(sequenceNumber++);
			
			if(member.isNativeAmerican())
			{
				nativeAmericanCounter++;
			}
			//Member is not seeking coverage
			if(StringUtils.isBlank(member.getDateOfBirth())||!member.isSeekingCoverage()){
				member.setMemberEligibility("NA");
				continue;
			}
			//Member is seeking coverage
			else if(member.isSeekingCoverage()){
				//member.setSeekingCoverage(true);
			
				age = getAgeFromDob(member.getDateOfBirth(), request);
				
				
				
				//CHIP eligible
				if(age>=18 && age < CHIP_AGE && request.getFplPercentage() >= medicaidFpl 
					&& request.getFplPercentage() <= APTC_THRESHOLD){
						member.setMemberEligibility("APTC");
						if(request.getFplPercentage() <  Double.parseDouble(chipThreshold18to19)){
							member.setMemberEligibility("CHIP");
							chipCounter++;
						}
					}
				else if(age>5 && age <=18 && request.getFplPercentage() >= medicaidFpl 
						&& request.getFplPercentage() <= APTC_THRESHOLD){
					
					member.setMemberEligibility("APTC");
					if(request.getFplPercentage() <  Double.parseDouble(chipThreshold6to18)){
						member.setMemberEligibility("CHIP");
						chipCounter++;
					}
					
				}
				else if(age>1 && age <=5 && request.getFplPercentage() >= medicaidFpl 
						&& request.getFplPercentage() <= APTC_THRESHOLD){
					// When Age is b/w 1 and 5
					
					member.setMemberEligibility("APTC");
					if(request.getFplPercentage() <  Double.parseDouble(chipThreshold1to5)){
						member.setMemberEligibility("CHIP");
						chipCounter++;
				} 
				}
				else if(age>=0 && age <=1 && request.getFplPercentage() >= medicaidFpl 
						&& request.getFplPercentage() <= APTC_THRESHOLD){
					// When Age is b/w 0 and 1
					
					member.setMemberEligibility("APTC");
					if(request.getFplPercentage() <  Double.parseDouble(chipThreshold0to1)){
						member.setMemberEligibility("CHIP");
						chipCounter++;
					}
			
					
				}
				
				//Medicare Eligible
				else if (checkForMedicare(member.getDateOfBirth(), request)){
					member.setMemberEligibility("Medicare");
					medicareCounter++;
				}
				else if(member.isPregnant() && request.getFplPercentage() < Double.parseDouble(medicaidPregnantThreshold) )
				{
				// Check Pregnant Members are eligible for Medicaidif(request.getFplPercentage() < medicaidPregnantFpl) {
				
					member.setMemberEligibility("Medicaid");
					medicaidCounter++;
				}
				//APTC Eligible
				else if(age > CHIP_AGE && !checkForMedicare(member.getDateOfBirth(), request) &&
						request.getFplPercentage() >= medicaidFpl && request.getFplPercentage() >=HUNDRED_POINT_ZERO &&  request.getFplPercentage() <= APTC_THRESHOLD){
					member.setMemberEligibility("APTC");
				} 
			}
			
		}
		
	}
	
	public double getMedicaidThreshold(MedicaidRequest request,  Map<String,String> values)
	{
		double medicaidFpl = 0.0;
		if (request.isHouseholdParent())
		{	
		String medicaidParentsThreshold = values.get(MEDICAID_PARENTS);
		medicaidFpl = Double.parseDouble(medicaidParentsThreshold);
		}else
		{
		String medicaidOthersThreshold = values.get(MEDICAID_OTHERS);
		medicaidFpl = Double.parseDouble(medicaidOthersThreshold);
		}
		return medicaidFpl;
	}
	/**
	 * Method to check if a household is medicaid eligible
	 * JIRA: HIX-12653
	 * 
	 * @author Nikhil Talreja
	 * @since 02 September, 2013
	 */
	public boolean checkForMedicaidHousehold(MedicaidRequest request, MedicaidResponse response, Map<String,String> values){
		
		//Setting medicaid expansion flag
		String medicaidExpansionFlag = values.get(MEDICAID_EXPANSION);
		response.setMedicaidExpansion(medicaidExpansionFlag);
		
		/*
		String medicaidThreshold = values.get(MEDICAID);
	/*	double medicaidFpl = 0.0; 
		medicaidFpl = Double.parseDouble(medicaidThreshold); */
		double medicaidFpl =getMedicaidThreshold(request,values);
		
		
		LOGGER.debug("Household FPL : " + request.getFplPercentage() +"%");
		LOGGER.debug("State Medicaid Threshold : " + medicaidFpl);
		LOGGER.debug("State Medicaid Expansion flag : " + medicaidExpansionFlag);
		
		int sequenceNumber = 1;
		
		if(request.getFplPercentage() < medicaidFpl){
			response.setHouseholdMedicaidEligible(true);
			LOGGER.info("Entire household is Medicaid Eligible");
			/*
			 * Step 1.1 : if entire household is medicaid eligible, 
			 * loop through each member to check medicare eligibility and return
			 */
			for(FamilyMember member : request.getMembers()){
				
				member.setSequenceNumber(sequenceNumber++);
		
				//Member is not seeking coverage
				if(StringUtils.isBlank(member.getDateOfBirth())){
					member.setMemberEligibility("NA");
				}
				else{
					member.setMemberEligibility("Medicaid");
					//Medicare Eligible
					if (checkForMedicare(member.getDateOfBirth(), request)){
						member.setMemberEligibility("Medicare");
					}
				}
			}
			
			response.setMembers(request.getMembers());
			response.endResponse();
			return true;
		}
		
		return false;
	}
	
	/**
	 * Method to validate a request for medicaid/CHIP calculator
	 * JIRA: HIX-12653
	 * 
	 * @author Nikhil Talreja
	 * @since 09 July, 2013
	 */
	
	public boolean validateMedicaidRequest(MedicaidRequest request, MedicaidResponse response, Map<String, Map<String, String>> stateSpecificMedicaidInfo){
		
		if(request == null){
			response.setErrCode(NULL_REQUEST_ERROR_CODE);
			response.setErrMsg(NULL_REQUEST_ERROR_MESSAGE);
			return false;
		}
		
		String state = request.getStateCode();
		LOGGER.debug("Household State " + state);
		
		if(StringUtils.isBlank(state)){
			response.setErrCode(MISSING_STATE_CODE_ERROR_CODE);
			response.setErrMsg(MISSING_STATE_CODE_ERROR_MESSAGE);
			return false;
		}
		Map<String,String> values = stateSpecificMedicaidInfo.get(state);
		if(CollectionUtils.isEmpty(values)){
			response.setErrCode(INVALID_STATE_CODE_ERROR_CODE);
			response.setErrMsg(INVALID_STATE_CODE_ERROR_MESSAGEE);
			return false;
		}
		
		return true;
	}
	
	/**
	 * Utility method to get age of a member
	 *
	 * @author Nikhil Talreja
	 * @since May 20, 2013
	 * @param String - The DOB in MM/dd/yyyy format
	 * @return double - age of the member
	 */
	public int getAgeFromDob(String dob, MedicaidRequest request){

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		//Implementation change as per Joda Time
		Calendar cal = computeEffectiveDate(request);
		LocalDate now = new LocalDate(cal.getTimeInMillis());
				
		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());
				
		Years years = Years.yearsBetween(birthdate, now);
		int age =  years.getYears();
		LOGGER.debug("Age as per effective date " + age);
				
		return age;
		
		/*Calendar cal = computeEffectiveDate();
		long today = cal.getTimeInMillis();
	    Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
	    long birthDate = dobDate.getTime();
	    
	    long diff = today - birthDate;
	    double diffYears = 0;
	    long millisinYear = 0;
	    
	    if(cal.get(Calendar.YEAR) % FOUR == 0){
	    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
	    	        * NO_OF_HOURS_IN_DAY * LEAP_YEAR_DAYS;
	    }
	    else{
	    	millisinYear = (long)THOUSAND * SIXTY * SIXTY
	    	        * NO_OF_HOURS_IN_DAY * NON_LEAP_YEAR_DAYS;
	    	
	    }
	    
	    diffYears =  (double)diff / millisinYear;
	    
	    return diffYears;*/

	}
	
	/**
	 * Utility method to check if member is eligible for
	 * Medicare on plan effective date
	 * 
	 * <br><br>Medicare eligibility is above 64 years and 9 months
	 * 
	 * <br><br>
	 * For effective date 1st Jan 2015, any person born
	 * on or before 1st April 1950 will be medicare eligible
	 *
	 * @author Nikhil Talreja
	 * @param dob -The DOB in MM/dd/yyyy format
	 * @return boolean - true/false
	 */
	public boolean checkForMedicare(String dob, MedicaidRequest request){
		
		//Implementation change as per Joda Time
		Calendar cal = computeEffectiveDate(request);
		LocalDate now = new LocalDate(cal.getTimeInMillis());
						
		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());
		
		Period period = new Period(birthdate, now);
		int years = period.getYears();
		int months = period.getMonths();
		
		/*
		 * If age is 65 years or more, don't check for months 
		 * If age is 64 years, check for months (should be 9 or more)
		 */
		if(years > MEDICARE_AGE_YEARS ||
				(years == MEDICARE_AGE_YEARS && months >= MEDICARE_AGE_MONTHS)){
			LOGGER.debug("Applicant is medicare eligible");
			return true;
		}

		return false;
	}
	
	/**
	 * Utility method to find no. of applicants in a household
	 *
	 * @author Nikhil Talreja
	 * @since May 20, 2013
	 */
	public int getNoOfApplicants(MedicaidRequest request){
		
		if(request == null || CollectionUtils.isEmpty(request.getMembers())){
			return 0;
		}
		
		int noOfApplicants = 0;
		for(FamilyMember member : request.getMembers()){
			
			if(member.isSeekingCoverage()){
				noOfApplicants++;
			}
			
		}
		return noOfApplicants;
	}
	
	/**
	 * Computes effective date for coverage start
	 * 
	 * Following logic is implemented as part of HIX-24255
	 * 
	 * 1. If current date is on or before 15th of a month, effective date is 1st of next month
	 * 2. If current date is after 15th of a month, effective date is 1st of next to next month
	 * 
	 * Exmaples:
	 * 1. Current Date - 12th July 2014 , Effective Date - 1st August 2014
	 * 2. Current Date - 19th July 2014, Effective Date - 1st September 2014
	 * 3. Current Date - 28th November 2013, Effective Date - 1st January 2014
	 * 4. Current Date - 25th December 2013, Effective Date - 1st February 2014
	 * 
	 * @author - Sunil Desu, Nikhil Talreja
	 */
	
	private Calendar computeEffectiveDate(MedicaidRequest request) {

		if(request.getEffectiveStartDate()!=null && request.getEffectiveStartDate().trim().length()>0){
			
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			try {
				Date effectiveDate = df.parse(request.getEffectiveStartDate());
				Calendar cal = TSCalendar.getInstance();
				cal.setTime(effectiveDate);
				return cal;
			} catch (ParseException e) {
				
				LOGGER.error("Unable to parse effective date ", e);
			}			

			
		}
		/*
		 * HIX-55453 Modify computeEffectiveDate base on 15 day rule
		 * @author - Nikhil Talreja
		 */
		Calendar effectiveDate = TSCalendar.getInstance();
		Calendar currentDate = TSCalendar.getInstance();
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);

		// Current Date is on or before 15 of month
		if (currentDate.get(Calendar.DAY_OF_MONTH) <= FIFTEEN) {
			effectiveDate.set(Calendar.MONTH,
					currentDate.get(Calendar.MONTH) + 1);
		}
		// Current Date is after 15 of month
		else {
			effectiveDate.set(Calendar.MONTH,
					currentDate.get(Calendar.MONTH) + 2);
		}

				effectiveDate.set(Calendar.DAY_OF_MONTH, 1);
				effectiveDate.set(Calendar.HOUR_OF_DAY, 0);
				effectiveDate.set(Calendar.MINUTE, 0);
				effectiveDate.set(Calendar.SECOND, 0);
				effectiveDate.set(Calendar.MILLISECOND, 0);

		return effectiveDate;
	}
}
