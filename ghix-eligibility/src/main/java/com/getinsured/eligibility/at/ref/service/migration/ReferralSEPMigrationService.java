package com.getinsured.eligibility.at.ref.service.migration;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.iex.ssap.model.SsapApplication;

public interface ReferralSEPMigrationService {
Map<String,String> persistSEPAndQEPEventforApplicantandApplication(LceActivityDTO lceActivityDTO);
	
	LceActivityDTO populateApplicantsforSEPEvents(String caseNumber);
	
	Map<String,List<SepEvents>> fetchSepandQEPEvents();

	void persistQEPEventforApplicantandApplication(SsapApplication ssapApplication, Date eventDate, String qualifyEventSelected);

	boolean checkForDenial(SsapApplication application, Date eventdate);

	SepEvents retrieveEvent(long applicationId, SepEvents sepEvent, Date eventDate, String selectedEventName);
}
