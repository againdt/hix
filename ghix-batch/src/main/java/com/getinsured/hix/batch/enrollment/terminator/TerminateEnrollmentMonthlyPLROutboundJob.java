package com.getinsured.hix.batch.enrollment.terminator;

import java.util.Arrays;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentPLROut;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.JiraUtil;
//import com.getinsured.hix.batch.util.GhixBatchConstants;

public class TerminateEnrollmentMonthlyPLROutboundJob  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminateEnrollmentMonthlyPLROutboundJob.class);
	private EnrollmentPLROut enrollmentPLROut;
	private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		try{
			
			if(enrollmentPLROut!=null && enrollmentPLROut.getSkippedHouseholdMap()!=null && !enrollmentPLROut.getSkippedHouseholdMap().isEmpty()){
				//StepExecution stepExecution=chunkContext.getStepContext().getStepExecution();
				logBug(enrollmentPLROut,chunkContext.getStepContext().getStepExecution().getJobExecutionId());
			
				LOGGER.error("TerminateEnrollmentMonthlyPLROutboundJob :: The Households for which PLR Monthly failed are "+enrollmentPLROut.getSkippedHouseholdMap());
			}
			
		}catch(Exception e){
			LOGGER.error("Error while logging skip and success details "+EnrollmentConstants.ENROLLMENT_PLR_OUT_JOB + " :: "+e.toString());
		}finally{
			if(enrollmentPLROut!=null){
				
				enrollmentPLROut.resetAllFields();
			}
		}
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

}
	private void logBug(EnrollmentPLROut enrollmentPLROut2, Long executionId) {
		String environment = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRSREPORTENVIRONMENT);
		Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		StringBuilder descriptionstringBuilder = new StringBuilder();
         for (Entry<String, String> entry : enrollmentPLROut2.getSkippedHouseholdMap().entrySet()) {
                 descriptionstringBuilder.append(" Record skipped While running PLR out batch : "+ entry.getKey()+"\n"
                                  +" Exception Stack : "+ entry.getValue());
                 descriptionstringBuilder.append("\n");
         }
         
         String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
		 String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
		 
		 if(environment!=null && environment.equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_ENVIRONMENT_PRODUCTION) && isJiraEnabled){
			 JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
    		         Arrays.asList(fixVersion), 
        		         descriptionstringBuilder.toString(), 
        		         "Record skipped While running PLR out batch : "+ executionId,
        		         null);
	}
		 enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.PLR,
				 "Record skipped While running PLR out batch : "+ executionId, descriptionstringBuilder.toString());
	}
	public EnrollmentPLROut getEnrollmentPLROut() {
		return enrollmentPLROut;
	}
	public void setEnrollmentPLROut(EnrollmentPLROut enrollmentPLROut) {
		this.enrollmentPLROut = enrollmentPLROut;
	}
	/**
	 * @return the enrollmentGIMonitorUtil
	 */
	public EnrollmentGIMonitorUtil getEnrollmentGIMonitorUtil() {
		return enrollmentGIMonitorUtil;
	}
	/**
	 * @param enrollmentGIMonitorUtil the enrollmentGIMonitorUtil to set
	 */
	public void setEnrollmentGIMonitorUtil(EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		this.enrollmentGIMonitorUtil = enrollmentGIMonitorUtil;
	}
	
}
