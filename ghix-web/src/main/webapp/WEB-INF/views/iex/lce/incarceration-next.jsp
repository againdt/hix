<script type="text/ng-template" id="incarceration-next.jsp">
  <div class="gutter10">
    <spring:message code="label.lce.incarcerationComplete"/>
  </div>
    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <!-- <a class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>-->
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/> {{nextButtonLifeEvent}}</a>
    </dl>

</script>