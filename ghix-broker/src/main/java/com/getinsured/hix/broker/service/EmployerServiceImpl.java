package com.getinsured.hix.broker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.broker.repository.IEmployerRepository;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Implements {@link EmployerService} to find employers
 * 
 * @author kanthi_v
 * 
 */
@Service("employerBrokerService")
public class EmployerServiceImpl implements EmployerService {
	@Autowired
	private IEmployerRepository employerRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployerServiceImpl.class);

	/**
	 * Returns employer for given employer external id
	 * 
	 * @param externalId
	 * @return Employer
	 */
	@Override
	public Employer findEmployerByExternalId(String externalId) throws GIException {
		LOGGER.info("findEmployerByExternalId : START");
		if (externalId == null || externalId.equals("")) {
			LOGGER.error("Employer Id is Missing.");
			throw new GIException("Employer Id is Missing. ");
		}
		Employer employer = employerRepository.findByExternaId(externalId);
		if (employer == null) {
			LOGGER.error("Employer not found for given ExternalId.");
			throw new GIException("Employer not found for given ExternalId: " + externalId);
		}
		LOGGER.info("findEmployerByExternalId : END");
		return employer;
	}

}
