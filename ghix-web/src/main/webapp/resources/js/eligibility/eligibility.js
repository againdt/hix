//1.7. Will current applicant be claimed as a dependent on someone else's federal income tax return next year? 

function toggleDiv(questionId) {
	if (questionId == '0point7') {
		$('input:radio[name="householdCompositions0point7"]').change(
				function() {
					
					var currentNodeValue = $('input:radio[name="householdCompositions0point7"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers0point7').show();
					} else {
						$('.householdMembers0point7').hide();
						$('.infoForm0point7').hide();
						$('input:radio[name="householdComposition0point7"]').prop('checked', false);
					}
				});

		$('input:radio[name="householdComposition0point7"]').change(function() {
			
			var currentNodeValue = $('input:radio[name="householdComposition0point7"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'else') {
				$('.infoForm0point7').show();
			} else {
				$('.infoForm0point7').hide();
			}
		});
		
	} else if (questionId == '4point0') {
		$('.infoForm').hide();
		$('input:radio[name="householdComposition4point0"]').change(function() {
			var currentNodeValue = $('input:radio[name="householdComposition4point0"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'else') {
				$('.infoForm').show();
			} else {
				$('.infoForm').hide();
			}
		});
		
	} else if (questionId == '5point4') {
		$('.nonApplicantSpouse').hide();
		$('input:radio[name="householdComposition5point4"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdComposition5point4"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'else') {
						$('.nonApplicantSpouse').show();
					} else {
						$('.nonApplicantSpouse').hide();
					}
				});

		// 6. Will [Household contact] [and spouse name (if 'a' was selected in
		// item
		// 3)]
		// claim dependents on next year's federal income tax return?

	} else if (questionId == '6point0') {
		$('.householdMembers').hide();
		$('input:radio[name="householdCompositions6point0"]').change(function() {
			var currentNodeValue = $("input:radio[name='householdCompositions6point0']:checked").val();
			
			if (currentNodeValue && currentNodeValue == 'Y') {
				$('.householdMembers').show();
			} else {
				$('.householdMembers').hide();
				$('.infoForm6').hide();
				$('input:checkbox[name="householdComposition6point0"]').removeAttr('checked');
			}
		});
		$('.infoForm6').hide();
		$('input:checkbox[name="householdComposition6point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm6').is(':visible')){
					$('.infoForm6').hide();
				}else{
					$('.infoForm6').show();
				}
			}
		});

		// 7. Will [household contact] be claimed as a dependent on someone
		// else's
		// federal income tax return next year?

	} else if (questionId == '7point0') {
		$('.householdMembers7').hide();
		$('input:radio[name="householdCompositions7point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions7point0"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers7').show();
					} else {
						$('.householdMembers7').hide();
						$('.infoForm7').hide();
						$('input:radio[name="householdComposition7point0"]').prop('checked', false);
					}
				});
		$('.infoForm7').hide();
		$('input:radio[name="householdComposition7point0"]').change(function() {
			var currentNodeValue = $('input:radio[name="householdComposition7point0"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'else') {
				$('.infoForm7').show();
			} else {
				$('.infoForm7').hide();
			}
		});

		// 8. How is [Denendent FNLNS] related to [tax filer FNLNS]?
	} else if (questionId == '8point0') {
		$('#dropDown8point0').change(function() {

			if ($(this).val() == 'other') {
				$('.other').show();
				$(this).prop('selectedIndex', 0);
			}

		});

		// 9. Is [Claiming tax filer FNLNS] married?

	} else if (questionId == '9point0') {
		$('.householdMembers9').hide();
		$('input:radio[name="householdCompositions9point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions9point0"]:checked').val();
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers9').show();
					} else {
						$('.householdMembers9').hide();
						$('.infoForm9').hide();
						$('input:radio[name="householdComposition9point0"]').prop('checked', false);
					}
				});
		$('.infoForm9').hide();
		$('input:radio[name="householdComposition9point0"]').change(function() {
			var currentNodeValue = $('input:radio[name="householdComposition9point0"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'else') {
				$('.infoForm9').show();
			} else {
				$('.infoForm9').hide();
			}
		});

		// 11. Do [Tax filer] and [Spouse] plan to file a joint federal income
		// tax
		// return next year?
	} else if (questionId == '11point0') {
		$('input:radio[name="householdCompositions11point0"]').change(function() {
			var currentNodeValue = $('input:radio[name="householdCompositions11point0"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'DN') {
				$('.taxAlert').show();
			} else {
				$('.taxAlert').hide();
			}

		});

		// 13. Will [Tax filer (and spouse (if 'a' was selected in item 10)]
		// claim
		// any other dependents next year?
	} else if (questionId == '13point0') {
		$('.householdMembers13').hide();
		$('input:radio[name="householdCompositions13point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions13point0"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers13').show();
					} else {
						$('.householdMembers13').hide();
						$('.infoForm13').hide();
						$('input:checkbox[name="householdComposition13point0"]').removeAttr('checked');
					}
				});

		// 13. Will [Tax filer (and spouse (if 'a' was selected in item 10)]
		// claim
		// any other dependents next year?
		$('.infoForm13').hide();
		$('input:checkbox[name="householdComposition13point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm13').is(':visible')){
					$('.infoForm13').hide();
				}else{
					$('.infoForm13').show();
				}
			}
		});

		// 14. How are these dependents related to [Claiming tax filer FNLNS]?
	} else if (questionId == '14point0') {
		$('#dropDown14point0').change(function() {

			if ($(this).val() == 'other') {
				$('.other').show();
				$(this).prop('selectedIndex', 0);
			}

		});

		// 16. Does [Dependent FNLNS] live with a parent or stepparent other
		// than
		// [tax filer(s)]?
	} else if (questionId == '16point0') {
		$('.householdMembers16').hide();
		$('input:radio[name="householdCompositions16point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions16point0"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers16').show();
					} else {
						$('.householdMembers16').hide();
						$('.infoForm16').hide();
						$('input:checkbox[name="householdComposition16point0"]').prop('checked', false);
					}
				});
		$('.infoForm16').hide();
		$('input:checkbox[name="householdComposition16point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm16').is(':visible')){
					$('.infoForm16').hide();
				}else{
					$('.infoForm16').show();
				}
			}
		});

		// 19. Who is/are [Applicant dependent FNLNS]'s parent or stepparent(s)?

	} else if (questionId == '19point0') {
		$('.infoForm19').hide();
		$('input:checkbox[name="householdComposition19point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm19').is(':visible')){
					$('.infoForm19').hide();
				}else{
					$('.infoForm19').show();
				}
			}
		});

		// 21. Who is/are [Applicant dependent FNLNS]'s parent or stepparent(s)?

	} else if (questionId == '21point0') {
		$('.infoForm21').hide();
		$('input:checkbox[name="householdComposition21point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm21').is(':visible')){
					$('.infoForm21').hide();
				}else{
					$('.infoForm21').show();
				}
			}
		});

		// 22. Does [Applicant dependent FNLNS] live with their son, daughter,
		// stepson or stepdaughter?

	} else if (questionId == '22point0') {
		$('.householdMembers22').hide();
		$('input:radio[name="householdCompositions22point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions22point0"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers22').show();
					} else {
						$('.householdMembers22').hide();
						$('.infoForm22').hide();
						$('input:radio[name="householdComposition22point0"]').prop('checked', false);
					}
				});
		$('.infoForm22').hide();
		$('input:checkbox[name="householdComposition22point0"]').change(function() {
			
			if ($(this).val() == 'else') {
				if($('.infoForm22').is(':visible')){
					$('.infoForm22').hide();
				}else{
					$('.infoForm22').show();
				}
			}
		});

		// 24. Does [Selected name FNLNS] live with a spouse?

	} else if (questionId == '24point0') {
		$('.householdMembers24').hide();
		$('input:radio[name="householdCompositions24point0"]').change(
				function() {
					var currentNodeValue = $('input:radio[name="householdCompositions24point0"]:checked').val();
					
					if (currentNodeValue && currentNodeValue == 'Y') {
						$('.householdMembers24').show();
					} else {
						$('.householdMembers24').hide();
						$('.infoForm24').hide();
						$('input:radio[name="householdComposition24point0"]').prop('checked', false);
					}
				});
		$('.infoForm24').hide();
		$('input:radio[name="householdComposition24point0"]').change(function() {
			var currentNodeValue = $('input:radio[name="householdComposition24point0"]:checked').val();
			
			if (currentNodeValue && currentNodeValue == 'else') {
				$('.infoForm24').show();
			} else {
				$('.infoForm24').hide();
			}
		});
	}

}

function completeQuestion(questionData) {

	if (questionData) {
		jsonParseData = $.parseJSON(questionData);

		$.each(jsonParseData, function(key, value) {

			if (key && value) {

				if (key == "applicantSpouse") {
					$(".applicantSpouse").html(value);
				} else if (key == "taxfiler") {
					$(".taxfiler").html(value);
				} else if (key == "taxfilerSpouse") {
					if(value){
						$(".taxFilerSpouseExist").show();
						$(".taxfilerSpouse").show();
						$(".taxfilerSpouse").html(value);
					}
					
				} else if (key == "hhContact") {
					$(".hhContact").html(value);
				} else if (key == "nfParent") {
					$(".nfParent").html(value);
				} else if (key == "allTaxFiler") {
					$(".allTaxFiler").html(value);
				}
			}
		});
	}
}

function validateAnswer(questionId) {

	$('#error' + questionId).html('');
	if (questionId) {

		// list of question which don't have and householdCompositions name
		var questionArray1 = [ "4point0", "5point4" ];
		var questionArray2 = [ "8point0", "14point0" ];
		var questionArray3 = [ "19point0", "21point0" ];
		// list of question which have both householdCompositions &
		// householdComposition name
		var checkboxQustnArray = [ "6point0", "13point0", "16point0","22point0" ];
		var radioQustnArray = [ "0point7", "7point0", "9point0", "24point0" ];

		if (jQuery.inArray(questionId, questionArray1) > -1) {
			var currentNodeValue = $(
					'input:radio[name="householdComposition' + questionId
							+ '"]:checked').val();
			if (currentNodeValue) {
				return true;
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}

		if (jQuery.inArray(questionId, questionArray2) > -1) {
			var currentNodeValue = $('#dropDown' + questionId).val();
			if (currentNodeValue && currentNodeValue != "other") {
				return true;
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}

		if (jQuery.inArray(questionId, questionArray3) > -1) {
			var currentNodeValue = $(
					'input:checkbox[name="householdComposition' + questionId
							+ '"]:checked').val();
			if (currentNodeValue) {
				return true;
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}
		var outerSelectedAnswer = false;
		
		if(questionId != "17point0"){
			var currentNodeValue = $(
					'input:radio[name="householdCompositions' + questionId
							+ '"]:checked').val();
			if (currentNodeValue) {
				if(currentNodeValue == 'Y'){
					outerSelectedAnswer = true;
				}
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}
		

		if (jQuery.inArray(questionId, checkboxQustnArray) > -1 && outerSelectedAnswer) {
			var currentNodeValue = $(
					'input:checkbox[name="householdComposition' + questionId
							+ '"]:checked').val();
			if (currentNodeValue) {
				return true;
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}

		if (jQuery.inArray(questionId, radioQustnArray) > -1 && outerSelectedAnswer) {
			var currentNodeValue = $(
					'input:radio[name="householdComposition' + questionId
							+ '"]:checked').val();
			if (currentNodeValue) {
				return true;
			} else {
				$('#error' + questionId)
						.html(
								'<label class="error"><span><em class="excl">!</em>Please select an answer before clicking "Next".</span></label>');
				return false;
			}
		}
	}
	return true;
}

function doSubmit(formName, url) {

	if (formName && url) {
		document.forms[formName].method = "post";
		document.forms[formName].action = url;
		document.forms[formName].submit();
	}
}

function addSomeoneElse(questionId){
	
	if(questionId){
		$('.infoForm'+ questionId).append($('.someoneElseTemplate' + questionId).html());
	}
}