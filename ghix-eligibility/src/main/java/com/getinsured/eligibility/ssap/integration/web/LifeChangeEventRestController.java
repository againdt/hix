package com.getinsured.eligibility.ssap.integration.web;

import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;

@Controller
@RequestMapping("/lce")
public class LifeChangeEventRestController {

	@Autowired
	SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired private RestTemplate restTemplate;
	
	@RequestMapping(value = "/test", method = RequestMethod.GET)
	public void test(){

		String url = "http://localhost:8080/ghix-eligibility/" + "lce/updateSsapApplicant?id={id}";
		
		MultiValueMap<String, Long> uriParams = new LinkedMultiValueMap<String, Long>();
		uriParams.add("id", 11300l);
		
		//restTemplate.postForObject(url, uriParams, String.class);
		
		//restTemplate.getForObject(url, String.class, "11300");
		restTemplate.postForObject(url, null, String.class, "11300");
	}

	@RequestMapping(value = "/updateSsapApplicant", method = RequestMethod.POST)
	public @ResponseBody String updateSsapApplicant(@RequestParam("id") Long id){

		SsapApplicant ssapApplicant = ssapApplicantRepository.findOne(id);

		if (ssapApplicant != null) {
			ssapApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
			ssapApplicant.setSsapApplication(null);
			ssapApplicantRepository.save(ssapApplicant);
		}
		
		return "";
	}
	
}
