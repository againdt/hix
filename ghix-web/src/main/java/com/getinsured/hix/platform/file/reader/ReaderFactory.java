package com.getinsured.hix.platform.file.reader;

import com.getinsured.hix.platform.file.mapper.Mapper;
import com.getinsured.hix.platform.file.mapper.MapperFactory;

/**
 * A factory class to return the 
 *
 */
@SuppressWarnings("rawtypes")
public class ReaderFactory 
{
	@SuppressWarnings("unchecked")
	public static <T> FileReader getReader(String fileName,char separator,Class T)
	{
		Mapper mapper = getMapper(T.getSimpleName());
		FileReader<T> reader = null;
		if(fileName.endsWith("csv"))
		{
			reader = new CsvReader<T>();
			reader.setFileName(fileName);
			reader.setDelimiter(separator);
			reader.setMapper(mapper);
		}
		
		return reader;
	}
	
	private static Mapper getMapper(String mapperName)
	{
		 return MapperFactory.getMapper(mapperName);
	}
}
