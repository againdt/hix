package com.getinsured.hix.batch.referral.service;

import java.util.List;

import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;

public interface LceReminderNotice {
	List<PendingLCEDTO> appDetailsForRecurringLCENotice(String query);
	
	List<PendingLCEDTO> appDetailsForRecurringLCESEPNotice(String query);

	boolean isNoticeSent(PendingLCEDTO pendingLCEDTO, Integer INTERVAL, Integer BUFFER_DAYS_TO_RETRY);

	String generate(String caseNumber, String noticeTemplateName) throws NoticeServiceException;
}
