package com.getinsured.ahbx.shop;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.ahbx.shop.employercaseorderid.CaseOrderIdSaveResponse;
import com.getinsured.ahbx.shop.employerdetails.EmployerInfoResponse;
import com.getinsured.ahbx.shop.service.EmployerWsService;
import com.getinsured.hix.dto.shop.ShopResponse;
import com.getinsured.hix.dto.shop.ShopResponse.ResponseType;


@Controller
@RequestMapping(value = "/employer")
public class EmployerWsController {
	private static final Logger LOGGER = Logger.getLogger(EmployerWsController.class);
	
	@Autowired private EmployerWsService employerWsService; 
	
	@RequestMapping(value="getEmployer",method = RequestMethod.GET)
	@ResponseBody public String getEmployer(HttpServletRequest request) throws HTTPException{
		//LOGGER.info("GHIX-AHBX getEmployer method invoked with Params:");
		
		String externalEmployerId = request.getParameter("externalid");
		//LOGGER.info(" externalid = "+externalEmployerId);
		
		 
		ShopResponse shopResponse= new ShopResponse();
		Map<String, Object> responseMap = new HashMap<String, Object>();
		try{
			EmployerInfoResponse employerInfoResponse = employerWsService.getEmployerDetails(externalEmployerId);
			if(employerInfoResponse.getResponseDTO().getResponseCode().equals("200")){
				responseMap = employerWsService.createEmployerData(employerInfoResponse, ResponseType.XML); 
				shopResponse.setResponseData(responseMap);
				shopResponse.setStatus("SUCCESS");
				shopResponse.setErrCode(0);
				//LOGGER.info("RESPONSE "+ responseMap.toString());
			}else{
				shopResponse.setStatus("FAILURE");
				shopResponse.setErrCode(1);
				shopResponse.setErrMsg(employerInfoResponse.getResponseDTO().getResponseDescription());
				//LOGGER.info("Error "+employerInfoResponse.getResponseDTO().getResponseDescription());
			}
		}catch(Exception ex){
			shopResponse.setStatus("FAILURE");
			shopResponse.setErrCode(1);
			shopResponse.setErrMsg(ex.getMessage());
			LOGGER.error("Error ", ex);
		}
		return shopResponse.sendResponse(ResponseType.XML);
	}

	@RequestMapping(value="/sendOrderId",method = RequestMethod.GET)
	@ResponseBody public String sendOrderId(HttpServletRequest request) throws HTTPException{
		//LOGGER.info("GHIX-AHBX sendOrderId method invoked with Param:");
		ShopResponse shopResponse= new ShopResponse();
		try{
			String employerCaseID = request.getParameter("employerCaseID");
			String orderID = request.getParameter("orderID");
			
			//LOGGER.info(" employerCaseID = "+employerCaseID);
			//LOGGER.info(" orderID = "+orderID);
			CaseOrderIdSaveResponse caseOrderIdSaveResponse = employerWsService.sendCaseOrderID(Integer.valueOf(employerCaseID), Integer.valueOf(orderID));
			Map<String, Object> responseMap = new HashMap<String, Object>();
			if(caseOrderIdSaveResponse.getResponse().getResponseCode().equals("000") || caseOrderIdSaveResponse.getResponse().getResponseCode().equals("502")){
				responseMap.put("RESPONSE", caseOrderIdSaveResponse.getResponse().getResponseDescription());
				shopResponse.setResponseData(responseMap);
				shopResponse.setStatus("SUCCESS");
				shopResponse.setErrCode(0);
				//LOGGER.info("RESPONSE "+ caseOrderIdSaveResponse.getResponse().getResponseDescription());
			}else{
				shopResponse.setStatus("FAILURE");
				shopResponse.setErrCode(1);
				shopResponse.setErrMsg(caseOrderIdSaveResponse.getResponse().getResponseDescription());
				//LOGGER.info("Error "+ caseOrderIdSaveResponse.getResponse().getResponseDescription());
			}
		}catch(Exception ex){
			shopResponse.setStatus("FAILURE");
			shopResponse.setErrCode(1);
			shopResponse.setErrMsg(ex.getMessage());
			LOGGER.error("Error ", ex);
		}
		return shopResponse.sendResponse(ResponseType.XML);
	}

}
