package com.getinsured.iex.hub.ssac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ssac.extension.ResponseMetadataType;

public class ResponseMetadataWrapper implements JSONAware{

	private String responseCode;
	private String responseText;
	private String tdsResponse;

	public ResponseMetadataWrapper(ResponseMetadataType metadata) {
		if(metadata != null){
			this.responseCode = metadata.getResponseCode();
			this.responseText = metadata.getResponseDescriptionText();
			this.tdsResponse = metadata.getTDSResponseDescriptionText();
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject metadaObj = new JSONObject();
		metadaObj.put("ResponseCode", this.responseCode);
		metadaObj.put("ResponseDescriptionText", this.responseText);
		metadaObj.put("TDSResponseDescriptionText", this.tdsResponse);
		return metadaObj.toJSONString();
	}

	public String getResponseCode() {
		return responseCode;
	}

	public String getResponseText() {
		return responseText;
	}

	public String getTdsResponse() {
		return tdsResponse;
	}

}
