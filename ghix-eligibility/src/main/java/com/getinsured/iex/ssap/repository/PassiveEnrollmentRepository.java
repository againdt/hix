package com.getinsured.iex.ssap.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.enrollment.PassiveEnrollment;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollment;

@Repository("passiveEnrollmentRepository")
@Transactional
public interface PassiveEnrollmentRepository extends JpaRepository<PassiveEnrollment, Long> {
    
    @Query("select pe from PassiveEnrollment pe where batchId = :batchId")
    List<PassiveEnrollment> getAllRecordsForBatchId(@Param("batchId") Long batchId);
    
    @Query("Select distinct en " +
			" FROM Enrollment as en " +
			" inner join fetch en.enrollees " +
			" where en.createdBy in (:users) " +
			" and en.insuranceTypeLkp.lookupValueCode = 'HLT' " +
			" and en.createdOn >= :startDate and en.createdOn < :endDate " +
			" and en.ssapApplicationid != null ")
	List<Enrollment> getEnrollmentsByCreatedUser(@Param("users") List<AccountUser> users, @Param("startDate") Date startDate, @Param("endDate") Date endDate);
    
    @Query("Select distinct en " +
			" FROM Enrollment as en " +
			" inner join fetch en.enrollees " +
			" where en.insuranceTypeLkp.lookupValueCode = 'HLT' " +
			" and en.ssapApplicationid = :ssapApplicationId ")
	List<Enrollment> getEnrollmentsByCreatedUser(@Param("ssapApplicationId") Long ssapApplicationId);
   
    @Query("Select distinct en " +
			" FROM Enrollment as en " +
			" inner join fetch en.enrollees " +
			" where en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM') " + 
			"and en.ssapApplicationid = :ssapApplicationid ")
	List<Enrollment> findActiveEnrollmentBySsapApplicationId(@Param("ssapApplicationid") Long ssapApplicationid);

    @Query("Select en " +
			" FROM Enrollment as en " +
			" inner join fetch en.enrollees " +
			" where en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM') " + 
			"and en.ssapApplicationid = :ssapApplicationid ")
	List<Enrollment> findActiveEnrolleesBySsapApplicationId(@Param("ssapApplicationid") Long ssapApplicationid);
    
    @Query("Select distinct en " +
 			" FROM Enrollment as en " +
 			" inner join fetch en.enrollees " +
 			" where en.enrollmentStatusLkp.lookupValueCode IN ('PENDING','CONFIRM') " + 
 			" and en.insuranceTypeLkp.lookupValueCode IN (:insuranceType) " + 
 			"and en.ssapApplicationid = :ssapApplicationid ")
 	List<Enrollment> findActiveEnrollmentBySsapApplicationIdAndInsuranceType(@Param("ssapApplicationid") Long ssapApplicationid, @Param("insuranceType") List<String> insuranceType);

}
