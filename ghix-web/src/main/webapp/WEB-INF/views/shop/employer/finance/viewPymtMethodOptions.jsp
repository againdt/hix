<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<%@ page isELIgnored="false"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/datepicker.css"/>" />

<div class="gutter10">
<div class="row-fluid">

	<ul class="breadcrumb txt-left">
	<li><a href="<c:url value="/shop/employer/" />">Employer Portal </a> <span class="divider">/</span></li>
	<li><a href="<c:url value="/shop/employer/viewpayment" />">Payments </a> <span class="divider">/</span></li>
	<li class="active">Add Payment Method</li>
	</ul>
			<h1 id="Nplans">Add Payment Method <a tabindex="-1" href="#" rel="tooltip" data-placement="top"
			data-original-title="You can add a new method of payment based on the different options 
			supported by the system. Based on your selection of preferred payment method, you would need 
			to specify additional details like bank account, credit/debit card or billing address 
			information." class="info"><i class="icon-question-sign margin5-t"></i></a></h1>
</div>
<!--titlebar-->

<div class="row-fluid">
	<div id="sidebar" class="span3">
		<h4 class="graydrkbg"> Steps</h4>
			<ul class="nav nav-list">
				<li class="active"><a href="#">Preferred Payment Method</a></li>
				<li><a href="#">Payment Method Details</a></li>
			</ul>
	</div>
	<!--sidebar-->

	<div id="rightpanel" class="span9">

		<div class="">

			<input type="hidden" value="<encryptor:enc value = "${employerid}"/>" name="employer.id"
				id="employerid" />
			<input type="hidden" value="${electronicPaymentOnly}" name="electronicPaymentOnly" id="electronicPaymentOnly"/> 	
			<div id="paymenttypediv">
				<h4 class="graydrkbg gutter10">Preferred Payment Method</h4>
				<div class="gutter10">
				
				<form class="form-vertical gutter10" name="fromaddpayment"
					action="addpayment" method="POST">
					<df:csrfToken/>
					<df:csrfToken/>
					<fieldset>
						<legend class="control-label"><h5>What is your preferred payment method?</h5></legend>
						
							<div class="control-group">
								<label class="radio inline "> <input type="radio"
									name="paymentType" value="BANK" checked> Direct Bank
									Withdrawal <a tabindex="-1" class="info" data-placement="top"
									rel="tooltip" href="#"
									data-original-title="Withdraw funds from your bank account">
										<i class="icon-question-sign"></i>
								</a>
								</label>
							</div>
							
							
		
							<div class="control-group" id="manualPayment">
								<label class="radio inline"> <input type="radio"
									name="paymentType" value="MANUAL"> Manual (Check payment)
									<a tabindex="-1" class="info addpayment" data-placement="top" rel="tooltip"
									href="#"
									data-original-title="Write a check for the payment">
										<i class="icon-question-sign"></i>
								</a>
								</label>
							</div>
							
							<div class="control-group" id="creditCard">
		
								<label class="radio inline"> <input type="radio"
									name="paymentType" value="CREDITCARD"> Credit/Debit card
									<a tabindex="-1" class="info addpayment" data-placement="top" rel="tooltip"
									href="#"
									data-original-title="Charge your credit card or withdraw from bank using debit card">
										<i class="icon-question-sign"></i>
								</a>
		
								</label>
							</div>
		
							<div class="form-actions">
								<a class="btn" role="button"
									href="<c:url value="/shop/employer/paymentmethods"/>">Cancel</a>
								<!-- <input type="button" value="Cancel" class="btn btn-primary">-->
								<input type="submit" name="submitBtn" id="submitBtn"
									Value="Continue" class="btn btn-primary" title="Continue"><br /> <br />
							</div>
					</fieldset>
				</form>
				
				</div>
			</div>
			<!-- start: this only shows when you come from 'edit payment' -->



		</div>
	</div>
</div>
</div>
<script type="text/javascript">
$('.info').tooltip()
if (null != '${electronicPaymentOnly}' && '${electronicPaymentOnly}' == 'YES')
{
	$('#manualPayment').hide();
}

if(null != '${acceptCreditCard}' && '${acceptCreditCard}' == 'NO')
{
	$('#creditCard').hide();
}

</script>

