package com.getinsured.iex.hub.consumers.hub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.rabbitmq.client.Channel;

public class ESIMECQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate mecApplicantServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPQueueConsumer.class);

	private void invokeESIMEC() throws HubMappingException,
			BridgeException, HubServiceException, InputDataValidationException {
	
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
			String nMECRequestJSON = "{\"taxHousehold\":[{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"897581061\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"578813959\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"CA\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"599785474\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"150994958\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"CA\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"503316496\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"603979795\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"CA\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"679477268\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"248862041\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"CA\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"411041816\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"327155739\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"CA\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}},{\"householdMember\":{\"person\":{\"dateOfBirth\":\"04/09/1950\",\"name\":{\"firstName\":\"Don\",\"middleName\":\"C\",\"lastName\":\"Johnson\",\"personNameSuffixText\":\"\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"327155679\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AL\"}},\"insurance\":{\"insuranceApplicantRequestedCoverageStartDate\":\"01/01/2013\",\"insuranceApplicantRequestedCoverageEndDate\":\"12/31/2013\"}}}]}";
		HubServiceBridge nesiMecBridge = HubServiceBridge.getHubServiceBridge("MECApplicantRequest");
		handler = nesiMecBridge.getServiceHandler();
		handler.setJsonInput(nMECRequestJSON);
		
		Object payLoad = handler.getRequestpayLoad();
		if(payLoad != null)
		{
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("MEC");
			requestRecord.setEndpointOperationName("MEC Eligibility");
			response = this.executeRequest(requestRecord, handler, mecApplicantServiceTemplate);
		}	
	}

	@Override
	public void onMessage(Message message, Channel channel) throws BridgeException  {
		String threadId = Thread.currentThread().getName();
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming MEC message, don't know how to process it");
		}
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ routingKey
				+ " Received Message:" + new String(message.getBody())
				+ " from channel:" + channel.getChannelNumber());

		try {
			invokeESIMEC();
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " message:"
					+e.getMessage()
					+ "And fault code:"+e.getFaultCode(),e);
		} catch (HubServiceException e) {
			LOGGER.info("Exception", e);
		} catch (HubMappingException e) {
			LOGGER.info("Exception", e);
		} catch (InputDataValidationException ie) {
			LOGGER.info("Exception", ie);
		}
	}
		
}
