<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>


<script type="text/javascript">
var employeeId=${employee_id};
var hasEditedSsn = false;
	function editSsn(){
		$("#showEncrSsn").hide();
		$("#ssn1").show();
		$("#ssn2").show();
		$('#ssn3').show();
		hasEditedSsn = true;
	}

	function shiftbox(element, nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if (element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}
	
	jQuery.validator.addMethod("phonecheck",function(value, element, param) {
		phone1 = $("#phone1").val();
		phone2 = $("#phone2").val();
		phone3 = $("#phone3").val();
	  	var intPhone1= parseInt( phone1);
	  	if( 
	  			(phone1 == "" || phone2 == "" || phone3 == "")  || 
	  			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
	  			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)){ 
	  		return false; 
	  	}else if(intPhone1<100){
	  		return false;
	  	} 
	  	
		var phonenumber = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
			
		var phoneno = /^\d{10}$/;
		  if(phonenumber.match(phoneno))      {
	        return true;
	      }
		return false;
	});

	jQuery.validator.addMethod("ssn1check", function(value, element, param) {
		ssn1 = $("#ssn1").val();
		return areaCodeCheck(ssn1); 
			
		 });
		 
		 jQuery.validator.addMethod("invalidssn1", function(value, element, param) {
			    ssn1 = $("#ssn1").val();
				ssn2 = $("#ssn2").val();
				ssn3 = $("#ssn3").val(); 
				return notAllowedSSN1(ssn1,ssn2,ssn3);
			 });
		 jQuery.validator.addMethod("invalidssn2", function(value, element, param) {
			    ssn1 = $("#ssn1").val();
				ssn2 = $("#ssn2").val();
				ssn3 = $("#ssn3").val(); 
				return notAllowedSSN2(ssn1,ssn2,ssn3);
			 });


		jQuery.validator.addMethod("ssncheck", function(value, element, param) {
			ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val();
		 	return validateSSN(ssn1,ssn2,ssn3);
		});

			jQuery.validator.addMethod("duplicateSSN", function(value, element, param) {
			ssn1 = $("#ssn1").val();
			ssn2 = $("#ssn2").val();
			ssn3 = $("#ssn3").val(); 
			currentSSN=ssn1+'-'+ssn2+'-'+ssn3;

			var csrfParameter = "";
			csrfParameter ='<c:url value="">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
					
			return isUniqueSSN(currentSSN, employeeId,0,'employee',csrfParameter);
			
			});
	
	jQuery.validator.addMethod("zipcheck", function(value, element, param) {
	  	elementId = element.id; 
	  	var numzip = new Number($("#"+elementId).val());
	    var filter = /^[0-9]+$/;
		if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() )|| numzip<=0 || (!filter.test($("#"+elementId).val()))){
	  		return false;	
	  	}return true;
	});

	function submitLocation() {
		var subUrl = '<c:url value="/shop/employer/roster/addWorksites">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : $("#frmworksites").serialize(),
			dataType : 'json',
			beforeSend: function( xhr ) {
				$('#submitbutton').attr("disabled", true);
			},
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateLocations(response);
				application.remove_modal();
				$('#submitbutton').attr("disabled", false);
			},
			error : function(e) {
				alert("Failed to Add Address");
				$('#submitbutton').attr("disabled", false);
			}
		});
	}

	/* function validateAddressWS() {
		var enteredAddress = $("#address1_0").val()+","+$("#address2_0").val()+","+$("#city_0").val()+","+$("#state_0").val()+","+$("#zip_0").val()+ ", " +  $("#lat_0").val() + "," + $("#lon_0").val();
		var idsText='address1_0~address2_0~city_0~state_0~zip_0~lat_0~lon_0';
		$.ajax({
					url : "/hix/platform/validateaddress",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = '/hix/platform/address/viewvalidaddress';
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame" tabindex="-1" role="dialog"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:380px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click", submitLocation);
						} else {
							submitLocation();
						}
					}
				});
	} */

	function validateLocation() {
		if ($("#frmworksites").validate().form()) {
			//validateAddressWS();
			submitLocation();
		}
	}

	/* function validateHomeAddressWS() {
		var enteredAddress = $("#address1").val()+","+$("#address2").val()+","+$("#city").val()+","+$("#state").val()+","+$("#zip").val()+ ", " +  $("#lat").val() + "," + $("#lon").val();
		var idsText='address1~address2~city~state~zip~lat~lon';
		$.ajax({
					url : "/hix/platform/validateaddress",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = '/hix/platform/address/viewvalidaddress';
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:380px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click",
									submitFrmEditEmployee);
						} else {
							submitFrmEditEmployee();
						}
					}
				});
	} */

	function submitFrmEditEmployee() {
		
		/*to check the text for address line 2 for IE browsers*/
		$("input[id^=address2_]").each(function(){
			if($(this).val() === "Address Line 2"){
				$(this).val("");
			}
		});	
		$("#frmeditemployee").submit();
	}

	function validateForm(btnValue) {
		$("#clickedBtnname").val(btnValue);

		if ($("#frmeditemployee").validate().form()) {
			var name = $("#firstName").val() + " " + $("#lastName").val();
			var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-"
					+ $("#phone3").val();
			var ssn = "";
			if(hasEditedSsn){
				ssn = $("#ssn1").val() + "-" + $("#ssn2").val() + "-" + $("#ssn3").val();
			}
			$("#name").val(name);
			$("#contactNumber").val(phone);
			$('#ssn').val(ssn);
	
			//validateHomeAddressWS();
			submitFrmEditEmployee();
		}
	}

	jQuery.validator.addMethod("dobcheck", function(value, element, param) {
		var today=new Date();
		var dob = $("#dob").val();
	    dob_arr = dob.split(/\//);
		dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
		var birthDate=new Date();
		birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
		if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
		if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
		if(today.getTime() < birthDate.getTime()){ return false; }
		return true;
	});
	  
	function postPopulateIndex(indexValue, zipCodeValue, zipId){
		//console.log(indexValue, zipCodeValue);
		jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
		getCountyList( '_'+indexValue,zipCodeValue, '' );
	}

	function getCountyList(eIndex,zip,county) {
		var subUrl = '<c:url value="/shop/employer/roster/addCounties">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				populateCounties(response, eIndex,county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, eIndex,county){
		
		if(!startsWith(eIndex,'_')){
			eIndex = '_' + eIndex;
		}
		
		$('#county'+eIndex).html('');
		var optionsstring = '<option value="">Select County...</option>';
		var i =0;
		$.each(response, function(key, value) {
			var optionVal = key+'#'+value;
			var selected = (county == key) ? 'selected' : '';
			var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$('#county'+eIndex).html(optionsstring);
	}
	
	function startsWith(str, prefix) {
	    return str.lastIndexOf(prefix, 0) === 0;
	}
	
</script>

<!--titlebar-->
<div class="gutter10">
<div class="row-fluid">
	<div class="span9">
		<h1>
			<spring:message  code="label.employerroasterinfoh1"/> <small> ${employeeCount} <spring:message  code="label.currentemployees"/></small>
		</h1>
	</div>
</div>
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<util:nav pageName="Edit Employee" navStepNo="2"></util:nav>
	</div>
	<!--sidebar-->

	<div class="span9" id="rightpanel">
		<div class="row-fluid space" id="titlebar">
			<h4 class="graydrkbg gutter10"><spring:message  code="label.editemployeesinfo"/></h4>
		</div>
		<!--titlebar-->
		<div class="gutter10">
			<h4 class="lightgray"><spring:message  code="label.emprroasteraddemph4"/></h4>
			<form class="form-horizontal" id="frmeditemployee" name="frmeditemployee" action="editemployeesubmit" method="post">
				<df:csrfToken/>
			      <input type="hidden" id="name" name="name" value="" /> 
			      <input type="hidden" name="employeeDetails[0].ssn" id="ssn" /> 
			      <input type="hidden" name="employeeDetails[0].contactNumber" id="contactNumber" /> 
			      <input type="hidden" name="employeeDetails[0].type" id="type" value="EMPLOYEE" /> 
			      <input type="hidden" id="employee_id" name="id" value="${employee_id}" /> 
			      <input type="hidden" id="clickedBtnname" name="clickedBtnname" /> 
			      <input type="hidden" name="employeeDetails[0].location.id" id="location_id" value="${employeeDetailsObj.location.id}" /> 
			      <input type="hidden" name="employeeDetails[0].id" id="employeeDetailsId" value="${employeeDetailsObj.id}" /> 
			      <input type="hidden" id="employer_id" name="employer.id" value="${employer_id}" /> 
		
				<div class="control-group">
					<label class="control-label" for="firstName"><spring:message  code="label.emplFirstName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="firstName"
							value="${employeeDetailsObj.firstName}"
							name="employeeDetails[0].firstName" /> <span id="firstName_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="lastName"><spring:message  code="label.emplLastName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" id="lastName"
							value="${employeeDetailsObj.lastName}"
							name="employeeDetails[0].lastName" /> <span id="lastName_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="location"><spring:message  code="label.employerworksiteaddr"/></label>
					<div class="controls">
						<select name="employerLocation.id" id="location"></select> 
						<%-- <a href="#worksite" id="worksiteLink" class="gutter10" data-toggle="modal"><span><i class="icon-plus-sign"></i> <spring:message  code="label.employeraddworksiteaddr"/></span></a> --%>
						<small>
							<a id="worksite_ModalLink" role="button" class="" data-toggle="modal" tabindex="0">
							<i class="icon-plus-sign"></i>
							<spring:message  code="label.employeraddworksiteaddr"/></a>
						</small>
					</div>
				</div>
				<div class="control-group" style="display:none;">
					<label class="control-label"><spring:message  code="label.employmentdate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<fmt:formatDate pattern="MM/dd/yyyy"
							value="${employee.employmentDate}" var="employmentDate" />
						<div class="input-append date date-picker"
							data-date="${employmentDate}" data-date-format="mm/dd/yyyy">
							<input class="span10" type="text" name="employmentDate"
								value="${employmentDate}" id="employmentDate"> <span
								class="add-on"><i class="icon-calendar"></i></span>
						</div>
						<span id="employmentDate_error"></span>
					</div>
				</div>
				<c:if test="${employee.terminationDate != null}">
				<div class="control-group">
					<label class="control-label"><spring:message  code="label.editemployeesterdate"/></label>
					<div class="controls">
						<fmt:formatDate pattern="MM/dd/yyyy" value="${employee.terminationDate}" var="terminationDate" />
						${terminationDate}
					</div>
				</div>
				</c:if>
				<h4 class="lightgray"><spring:message  code="label.emprroasteraddemph7"/></h4>
				<div class="control-group">
					<label class="control-label" for="dob"><spring:message  code="label.dob"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<fmt:formatDate pattern="MM/dd/yyyy"
							value="${employeeDetailsObj.dob}" var="dob" />
						<div class="input-append date date-picker" data-date="${dob}"
							data-date-format="mm/dd/yyyy">
							<input class="span10" type="text" name="employeeDetails[0].dob"
								value="${dob}" id="dob"> <span class="add-on"><i
								class="icon-calendar"></i></span>
						</div>
						<span id="dob_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="ssn"><spring:message  code="label.taxidnumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#"
						rel="tooltip" data-placement="top"
						data-original-title="<spring:message  code="label.taxidnumbertooltip"/>"
						class="info"><i class="icon-question-sign"></i> </a>
					</label>
					<div class="controls">
                    	<c:if test="${employeeDetailsObj.ssn != null && employeeDetailsObj.ssn != ''}">
							<c:set var="ssnParts" value="${fn:split(employeeDetailsObj.ssn,'-')}" />
							<span id="showEncrSsn">***-**-${ssnParts[2]}</span>
								
							<input type="text" class="span2 inline" name="ssn1" id="ssn1" value="" maxlength="3" style="display:none;"/>
							<input type="text" class="span1 inline" name="ssn2" id="ssn2" value="" maxlength="2" style="display:none;"/>
							<input type="text" class="span2 inline" name="ssn3" id="ssn3" value="" maxlength="4" style="display:none;"/>
							<input type="button" class="btn btn-small pull-right" id="editSsnButton" name="editSsnButton" onclick="javascript:editSsn();" value='<spring:message  code="label.edit"/> <spring:message  code="label.taxidnumber"/>'/> 
							<span id="ssn1_error"></span>
 							<span id="ssn3_error"></span>
						</c:if>
					</div>

					<%-- <div class="controls">
						<c:if test="condition"></c:if>
						<c:choose>
							<c:when
								test="${employeeDetailsObj.ssn != null && employeeDetailsObj.ssn != ''}">
								<c:set var="ssn" value="${employeeDetailsObj.ssn}" />
								<c:set var="ssnParts" value="${fn:split(ssn,'-')}" />

								<c:set var="ssn1" value="${ssnParts[0]}" />
								<c:set var="ssn2" value="${ssnParts[1]}" />
								<c:set var="ssn3" value="${ssnParts[2]}" />
							</c:when>
						</c:choose>
						<input type="hidden" name="org_ssn1" id="org_ssn1" value="${ssn1}">
						<input type="hidden" name="org_ssn2" id="org_ssn2" value="${ssn2}">
						<input type="text" class="span2 inline" name="ssn1" id="ssn1" onfocus="javascript:assignOrgSsnValue();" value="***" maxlength="3" />
						<input type="text" class="span1 inline" name="ssn2" id="ssn2" onfocus="javascript:assignOrgSsnValue();" value="**" maxlength="2" />
						<input type="text" class="span2 inline" name="ssn3" id="ssn3" value="${ssn3}" maxlength="4" />
						 <span id="ssn1_error"></span>
						 <span id="ssn3_error"></span>
					</div> --%>
				</div>
				<div class="control-group">
					<fieldset>
						<legend class="aria-hidden"><spring:message  code="label.gender"/></legend>
						<label class="control-label"><spring:message  code="label.gender"/></label>
						<div class="controls">
							<label class="radio inline"> <input type="radio"
								name="employeeDetails[0].gender"
								${fn:contains(employeeDetailsObj.gender, 'MALE') ? 'checked="checked"' : ''}
								id="gender" value="MALE"> Male
							</label> <label class="radio inline"> <input type="radio"
								name="employeeDetails[0].gender"
								${fn:contains(employeeDetailsObj.gender, 'FEMALE') ? 'checked="checked"' : ''}
								id="gender" value="FEMALE"> Female
							</label> <span id="gender_error"></span>
						</div>
					</fieldset>
				</div>
				<c:set var="tobaccoDisplay" value="" />
				<c:if test="${displayTobaccoUse == 'HIDE'}">
				 <c:set var="tobaccoDisplay" value="style=\"display:none;\"" />
				</c:if>
				<div class="control-group" ${tobaccoDisplay}>
					<label class="control-label"><spring:message  code="label.tobaccouser"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<label class="radio inline"> <input type="radio"
							name="employeeDetails[0].smoker"
							${fn:contains(employeeDetailsObj.smoker, 'YES') ? 'checked="checked"' : ''}
							id="smoker" value="YES"> Yes
						</label> <label class="radio inline"> <input type="radio"
							name="employeeDetails[0].smoker"
							${fn:contains(employeeDetailsObj.smoker, 'NO') ? 'checked="checked"' : ''}
							id="smoker" value="NO"> No
						</label> <span id="smoker_error"></span>
					</div>
				</div>
				<c:set var="nativeAmrDisplay" value="" />
				<c:if test="${displayNativeAmerican == 'HIDE'}">
				 <c:set var="nativeAmrDisplay" value="style=\"display:none;\"" />
				</c:if>
				<div class="control-group" ${nativeAmrDisplay}>
					<label class="control-label"><spring:message  code="label.nativeAmr"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<label class="radio inline"> <input type="radio"
							name="employeeDetails[0].nativeAmr"
							${fn:contains(employeeDetailsObj.nativeAmr, 'YES') ? 'checked="checked"' : ''}
							id="nativeAmr" value="YES"> Yes
						</label> <label class="radio inline"> <input type="radio"
							name="employeeDetails[0].nativeAmr"
							${fn:contains(employeeDetailsObj.nativeAmr, 'NO') ? 'checked="checked"' : ''}
							id="nativeAmr" value="NO"> No
						</label> <span id="nativeAmr_error"></span>
					</div>
				</div>
				<h4 class="lightgray"><spring:message  code="label.contact"/></h4>
				<div class="control-group">
					<label class="control-label" for="email"><spring:message  code="label.emplEmail"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="employeeDetails[0].email"
							value="${employeeDetailsObj.email}" id="email" /> <span
							id="email_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message  code="label.homephone"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<c:if test="condition"></c:if>
						<c:choose>
							<c:when
								test="${employeeDetailsObj.contactNumber != null && employeeDetailsObj.contactNumber != ''}">
								<c:set var="phone" value="${employeeDetailsObj.contactNumber}" />
								<c:set var="phoneParts" value="${fn:split(phone,'-')}" />

								<c:set var="phone1" value="${phoneParts[0]}" />
								<c:set var="phone2" value="${phoneParts[1]}" />
								<c:set var="phone3" value="${phoneParts[2]}" />
							</c:when>
						</c:choose>
						<input type="text" class="span2 inline" value="${phone1}"
							name="phone1" id="phone1" maxlength="3"
							 /> <input type="text"
							class="span2 inline" value="${phone2}" name="phone2" id="phone2"
							maxlength="3" /> <input
							type="text" class="span2 inline" value="${phone3}" name="phone3"
							id="phone3" maxlength="4" /> <span id="phone3_error"></span>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="address1_Home"><spring:message  code="label.homeaddress"/></label> 
					<input type="hidden" name="address2_Home" id="address2_Home" >
					<input type="hidden" name="lat" id="lat_Home" value="0.0"  />
					<input type="hidden" name="lon" id="lon_Home" value="0.0"  />	
					<input type="hidden" name="rdi" id="rdi_Home" value="" />
						
					<div class="controls">
						<input type="text" value="${employeeDetailsObj.location.address1}"
							name="employeeDetails[0].location.address1" id="address1_Home" /> <span
							id="address1_Home_error"></span>
							<input type="hidden" id="address1_Home_hidden" name="address1_Home_hidden" value="${employeeDetailsObj.location.address1}">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="city_Home"><spring:message  code="label.emplCity"/></label>
					<div class="controls">
						<input type="text" value="${employeeDetailsObj.location.city}"
							name="employeeDetails[0].location.city" id="city_Home" /> <span
							id="city_Home_error"></span>
						<input type="hidden" id="city_Home_hidden" name="city_Home_hidden" value="${employeeDetailsObj.location.city}">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="state_Home"><spring:message  code="label.emplState"/></label>
					<div class="controls">
						<select size="1" class="input-medium"
							name="employeeDetails[0].location.state" id="state_Home">
							<option value="">Select</option>
							<c:forEach var="state" items="${statelist}">
								<option id="${state.code}"
									<c:if test="${state.code == employeeDetailsObj.location.state}"> SELECTED </c:if>
									value="${state.code}">${state.name}</option>
							</c:forEach>
						</select> <span id="state_Home_error"></span>
						<input type="hidden" id="state_Home_hidden" name="state_Home_hidden" >
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="zip_Home"><spring:message  code="label.emplZip"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
					<div class="controls">
						<input type="text" name="employeeDetails[0].location.zip"
							value="${employeeDetailsObj.location.zip}" id="zip_Home" class="input-small zipCode" maxlength="5" />
						<span id="zip_Home_error"></span>
						<input type="hidden" value="0.0" id="zip_Home_hidden" name="zip_Home_hidden" value="${employeeDetailsObj.location.zip}">
					</div>
				</div>
				<div class="control-group" id="county_Home_div" style="">
					<label for="county_Home" class="control-label required"><spring:message  code="label.country"/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/></label>
					<div class="controls">
						<select size="1" class="input-large" name="employeeDetails[0].location.county" id="county_Home">
							<option value="">Select County...</option>
						</select>
						<div id="county_Home_error"></div>		
					</div>
				</div>
				<%-- <h4 class="lightgray"><spring:message  code="label.dependents"/></h4> --%>
				<div class="control-group" style="display: none;">
					<fieldset>
						<legend class="aira-hidden"><spring:message  code="label.isMarried"/></legend>
						<label class="control-label"><spring:message  code="label.isMarried"/></label>
						<div class="controls">
						<label class="radio inline"> ${employee.isMarried} </label>
						<input type="hidden"
								name="isMarried" id="isMarried"	value="${employee.isMarried}">
							<%-- <label class="radio inline"> <input type="radio"
								name="isMarried" id="isMarried"
								${fn:contains(employee.isMarried, 'YES') ? 'checked="checked"' : ''}
								value="YES"> Yes
							</label> <label class="radio inline"> <input type="radio"
								name="isMarried" id="isMarried"
								${fn:contains(employee.isMarried, 'NO') ? 'checked="checked"' : ''}
								value="NO"> No
							</label> --%>
						</div>
					</fieldset>
				</div>
				<div class="control-group" style="display: none;">
					<label class="control-label" for="childCount"><spring:message  code="label.childCount"/> </label>
					<div class="controls">
					<label class="radio inline"> ${employee.childCount} </label>
						 <input type="hidden" id="childCount" name="childCount"
							value="${employee.childCount}" maxlength="2" /><%-- <span
							id="childCount_error"></span> --%>
					</div>
				</div>
				<div class="form-actions">
					<div class="controls">
							<input type="button" name="mainSubmitButton" id="mainSubmitButton" onclick="javascript:validateForm('Done');" class="btn btn-primary" value="<spring:message  code='label.donebtn'/>">
					</div>
				</div>
				
				<input type="hidden" name="jsonStateValue" id="jsonStateValue" value='${doctorsJSON}' />
				<input type="hidden" name="defaultStateValue" id="defaultStateValue" value='${defaultStateCode}' />
				<input type="hidden" name="employerId" id="employerId" value="${employerOBJ.id}"/>
			</form>

		</div>
		<!--gutter10-->
	</div>
	<!-- rightpanel -->
</div>
<!-- row-fluid -->
</div>

<script type="text/javascript">
var empDetailsJSON = ${empDetailsJSON};
var empEmployerLocationID = ${employerLocationID};
var	empid = '${employerOBJ.id}';
	
populateLocations(empDetailsJSON, empEmployerLocationID);


	function populateLocations(empDetailsJSON, empEmployerLocationID) {
		application.populate_location_on_parent_page(empDetailsJSON, empEmployerLocationID);
	}

	jQuery.validator.addMethod("currency", function(value, element, param) {
		var pattern = /^(\\$)?([0-9\,])+(\.\d{0,2})?$/;
		return pattern.test(value);
	});

	jQuery.validator.addMethod("namecheck", function(value, element, param) {
	  	if(namecheck(value) == false){
		  	return false;	
		}
		return true;
	});
	
	var validator = $("#frmeditemployee")
			.validate(
					{
						rules : {
							'employeeDetails[0].firstName' : {
								required : true , namecheck : true
							},
							'employeeDetails[0].lastName' : {
								required : true , namecheck : true
							},
							'employeeDetails[0].dob' : {
								required : true,
								date : true,
								dobcheck : true
							},
						
							'employeeDetails[0].email' : {
								required : true,
								email : true
							},
							phone3 : {
								phonecheck : true
							},
							 
							ssn3 : {
								ssncheck : true,
								ssn1check : true,
								invalidssn1 :true,
								invalidssn2 :true,
								duplicateSSN : true
							},
							'employeeDetails[0].location.zip' : {
								required : true,
								zipcheck : true
							},
							childCount : {
								digits : true
							},
							'employeeDetails[0].location.county' : {
								required : true
							}
						},
						messages : {
							'employeeDetails[0].firstName' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>",
								namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].lastName' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>",
								namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>" 
							},
						
							'employeeDetails[0].dob' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>",
								date : "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
								dobcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].email' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>",
								email : "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"
							},
							phone3 : {
								phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
							},
							ssn3 : {
								ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempdigit' javaScriptEscape='true'/></span>",
								ssn1check : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstthreedigit' javaScriptEscape='true'/></span>",
								invalidssn1 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn1' javaScriptEscape='true'/></span>",
								invalidssn2 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn2' javaScriptEscape='true'/></span>",
								duplicateSSN: "<span> <em class='excl'>!</em><spring:message code='label.validateemptaxid' javaScriptEscape='true'/></span>"
							},
							ssn1 : {
								ssn1check : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstthreedigit' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.zip' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
								zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
							},
							childCount : {
								digits : "<span> <em class='excl'>!</em><spring:message code='label.validatechild' javaScriptEscape='true'/></span>"
							},
							'employeeDetails[0].location.county' : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class',
									'error span10');
							$('#dob').unbind('focus');
						}
					});

	/* var validator2 = $('#frmworksites')
			.validate(
					{
						onkeyup : false,
						onclick : false,
						onSubmit : true,
						onfocusout : false,
						rules : {
							'locations[0].location.address1' : {
								required : true
							},
							'locations[0].location.zip' : {
								required : true,
								zipcheck : true
							},
							'locations[0].location.city' : {
								required : true
							},
							'locations[0].location.state' : {
								required : true
							},
							'locations[0].location.county' :{
								required : true
							}
						},
						messages : {
							'locations[0].location.address1' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.zip' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateZip' javaScriptEscape='true'/></span>",
								zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validatezipdigit' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.city' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateCity' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.state' : {
								required : "<span> <em class='excl'>!</em> <spring:message code='label.validateState' javaScriptEscape='true'/></span>"
							},
							'locations[0].location.county' :{
								required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class',
									'error help-inline');
						}
					}); */

	
	/*focus out event for the text input to call the lightbox*/
	/* $('.zipCode').focusout(function(e) {
		var startindex = (e.target.id).indexOf("_");
		var index = (e.target.id).substring(startindex,(e.target.id).length);
		var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		address1_e=address1_e+index;
		address2_e=address2_e+index;
		city_e=city_e+index;
		state_e=state_e+index;
		zip_e=zip_e+index;
		lat_e=lat_e+index;
		lon_e=lon_e+index;
		rdi_e+=index;
		county_e+=index;
		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		
		getCountyList(index, $('#'+ zip_e).val(), '');
				
		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}	
			if(index == "_Home"){
				if( $('#'+ state_e).val() != "" && $('#'+ zip_e).val() != "" && $('#'+ address1_e).val() == "" &&  
					$('#'+ address2_e).val() == "" && $('#'+ city_e).val() == ""){
				}else{
					viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(), idsText);	
				};
			}else if(index != '_Home'){
				viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(), idsText);	
			};
		};
		
		$('#'+address1_e).focusin(function(e) {
			if($('#'+zip_e).val().length >= 5){
				getCountyList(eindex, $('#'+ zip_e).val(), '');
				$('#'+address1_e).unbind( "focusin" );
			 }   
		});
	}); */
</script>
<script type="text/javascript">
$(document).ready(function(){
	$('.date-picker').datepicker({
		autoclose:true,
		forceParse: false
		}).on('changeDate', function(ev) {
		});
	$('.info').tooltip();
	var zip = '${employeeDetailsObj.location.zip}';
	var county = '${employeeDetailsObj.location.county}';
	getCountyList('_Home', zip, county);
});

</script>
