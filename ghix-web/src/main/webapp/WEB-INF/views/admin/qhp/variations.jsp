<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<div class="row-fluid">
		<!-- start of secondary navbar -->

			<ul class="nav nav-pills">
			    <li><a href="#"><spring:message code="label.manageQHPS"/></a></li>
			    <li class="active"><a href="#"><spring:message code='label.addnewQHPapplication'/></a></li>
			</ul>
	 	 	<!-- end of secondary navbar -->
	 	 </div>
	
	<div class="row-fluid">
	
		<div class="span3" id="sidebar">
		<div class="header">
	        	<!--  beginning of side bar -->
                <h4 class="margin0"><a name="skip"></a><spring:message code='label.addnewQHPapplication'/></h4>
            </div>
	
				<!--  beginning of side bar -->
				<ul class="nav nav-list">
					<li><a>1. <spring:message  code='label.provideplandetails'/></a></li>
					<li><a>2. <spring:message  code='label.uploadplanbenefits'/></a></li>
					<li><a>3. <spring:message  code='label.uploadplanrates'/></a></li>
					<li><a>4. <spring:message  code='label.selectoraddprovidernetworks'/></a></li>
					<li class="active"><a>5. <spring:message  code='label.addcostsharingvariations'/></a></li>
					<li><a>6. <spring:message  code='label.reviewandconfirm'/></a></li>    
	            </ul>						
				<!-- end of side bar -->
		
		</div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			
				<div class="header">
		            <h4 class="margin0"><spring:message code='label.step5'/> &nbsp; <spring:message  code='label.costSharing'/></h4>
				</div><!-- end of page-header -->
		
			<div class="gutter10">
					<!--<form class="form-vertical" id="frmassisterreg" name="frmassisterreg" action="signup" method="POST">-->
					<!-- <form class="form-horizontal gutter10"> -->
						<table class="table table-border-none">
                            <thead>
                                <tr>
                                <td class="span4 txt-right"><spring:message code='label.issuerName'/></td>
                                <td><strong>${issuerName}</strong></td>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                <td class="txt-right"><spring:message code="label.planName"/></td>
                                <td><strong>${planName}</strong></td>
                                </tr>
                                <tr>
                                <td class="txt-right"><spring:message  code="label.planNumber"/></td>
                                <td><strong>${planNumber}</strong></td>
                                </tr>
                            </tbody>
                        </table>
						
                			<div id="accordion2" class="accordion">
                                <div class="accordion-group">
                                  <div class="accordion-heading">
                                    <a href="#collapseOne" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">
                                      <strong>&#91;${planName}&#93;</strong> <spring:message  code="label.withMessage"/>&#37; <spring:message code="label.actuarialVal"/>.
                                      <img id="loadingImg1" style="display:none;" src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
                                    </a>
                                  </div>
                                  <div class="accordion-body in collapse" id="collapseOne" name="collapseOne" style="height: auto;" >
                                    <div class="accordion-inner">
                                    	<form class="form-horizontal" id="frmCerti1" name="frmCerti1" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                                        <df:csrfToken/>     
	                                        <div class="control-group">
	                                            <label for="firstName" class="required control-label" for="fileInput">
	                                            	<spring:message  code="label.valueCertificate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#"><i class="icon-question-sign"></i></a></label>
	                                                <div class="controls">
	                                                    <!--<input type="file" id="fileInput" class="input-file">
	                                                    &nbsp; <button class="btn">Upload</button>
	                                                    <div id="firstName_error" class="help-inline"></div>-->
															<span>
																<input type="file" id="certi1" name="certi1"  class="input-file">&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
																<input type="hidden" name="fileType" value="certificates"/>
																<input type="hidden" name="fileId" value="certi1"/>
	                          								</span>
																<div id="certiFile1_error" class=""></div>
															<div id="certi1FileName" class="help">${actCerti1}</div>
	                                                </div>	<!-- end of controls-->
	                                        </div>	<!-- end of control-group -->
                                        </form>
                                        <form class="form-vertical" id="frmSilverPlan1" name="frmSilverPlan1" action="<c:url value="/admin/planmgmt/submitcostsharing" />" method="POST">
           									<input type="hidden" id="certiFile1" name="certiFile1"/>
           									<input type="hidden" id="benefitsFile1" name="benefitsFile1"/>
           									<input type="hidden" id="planId1" name="planId1" value="${planId}" />
           									<input type="hidden" id="costSharing" name="costSharing" value="CS4"/>
           									<df:csrfToken/>
               							</form>
               							<form class="form-horizontal" id="frmBenefits1" name="frmBenefits1" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                                        <df:csrfToken/>
	                                        <div class="control-group">
	                                            <label for="firstName" class="required control-label" for="fileInput">
	                                            	<spring:message  code="label.linkPlanBenefits"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#"><i class="icon-question-sign"></i></a></label>
	                                                <div class="controls">
	                                                    <!--<input type="file" id="fileInput" class="input-file">
	                                                    &nbsp; <button class="btn">Upload</button>
	                                                    <span><a href=<c:url value="/resources/sampleTemplates/benefits.csv" />>Download template</a></span> <spring:message code='label.upload'/>
	                                                    <div id="firstName_error" class="help-inline"></div>-->
															<span>
																<input type="file" id="benefits1" name="benefits1"  class="input-file">&nbsp; <input type="button" class="btn" value="<spring:message code='label.upload'/>" onclick="validCsv('#benefits1','#frmBenefits1','#benefitsFile1_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/benefits.csv" />><spring:message  code="label.downloadTemplate"/></a>
																<input type="hidden" name="fileType" value="benefits"/>
																<input type="hidden" name="fileId" value="benefits1"/>
	                          								</span>
																<div id="benefitsFile1_error" class=""></div>
															<div id="benefits1FileName" class="help">${benefitFile1}</div>
	                                                </div>	<!-- end of controls-->
	                                        </div>	<!-- end of control-group -->
	                                    </form>
                                        <div class="form-actions">
                                            <!--<a class="btn btn-primary" data-toggle="modal" href="#myModal" >Save &amp; Continue</a>-->
                                            <input type="button" name="submit1" id="submit1" title="<spring:message  code='label.saveAndContinue'/>"  value="<spring:message  code='label.saveAndContinue'/>" class="btn btn-primary" onclick="processPlan('frmSilverPlan1','collapseOne','collapseTwo')" />
                                        </div><!-- end of form-actions -->	                                        	
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-group">
                                  <div class="accordion-heading">
                                    <a href="#collapseTwo" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">
                                      <strong>&#91;${planName}&#93;</strong> <spring:message code="label.withMessage2"/>&#37; <spring:message code="label.actuarialVal"/>
                                      <img id="loadingImg2" style="display:none;"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
                                    </a>
                                  </div>
                                  <div class="accordion-body collapse" id="collapseTwo" name="collapseTwo" style="height: 0px;">
                                    <div class="accordion-inner">
                                        <form class="form-horizontal" id="frmCerti2" name="frmCerti2" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                                        <df:csrfToken/>
	                                        <div class="control-group">
	                                            <label for="firstName" class="required control-label" for="fileInput">
	                                            	<spring:message code="label.valueCertificate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#"><i class="icon-question-sign"></i></a></label>
	                                                <div class="controls">
	                                                    <!--<input type="file" id="fileInput" class="input-file">
	                                                    &nbsp; <button class="btn">Upload</button>
	                                                    <div id="firstName_error" class="help-inline"></div>-->
															<span>
																<input type="file" id="certi2" name="certi2"  class="input-file">&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
																<input type="hidden" name="fileType" value="certificates"/>
																<input type="hidden" name="fileId" value="certi2"/>
															</span>
																<div id="certiFile2_error" class=""></div>
															<div id="certi2FileName" class="help">${actCerti2}</div>
	                                                </div>	<!-- end of controls-->
	                                        </div> <!-- end of control-group -->
                                        </form>
                               			<form class="form-vertical" id="frmSilverPlan2" name="frmSilverPlan2" action="<c:url value="/admin/planmgmt/submitcostsharing" />" method="POST">
           									<df:csrfToken/>
           									<input type="hidden" id="certiFile2" name="certiFile2"/>
           									<input type="hidden" id="benefitsFile2" name="benefitsFile2"/>
           									<input type="hidden" id="planId2" name="planId2" value="${planId}" />
           									<input type="hidden" id="costSharing" name="costSharing" value="CS5"/>
   										</form>  
   										<form class="form-horizontal" id="frmBenefits2" name="frmBenefits2" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                                        <df:csrfToken/>
	                                        <div class="control-group">
	                                            <label for="firstName" class="required control-label" for="fileInput">
	                                            	<spring:message  code="label.linkPlanBenefits"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#"><i class="icon-question-sign"></i></a></label>
	                                                <div class="controls">
	                                                    <!--<input type="file" id="fileInput" class="input-file">
	                                                    &nbsp; <button class="btn">Upload</button>
	                                                    <span><a href=<c:url value="/resources/sampleTemplates/benefits.csv" />>Download template</a></span>
	                                                    <div id="firstName_error" class="help-inline"></div>-->
															<span>
																<input type="file" id="benefits2" name="benefits2"  class="input-file">&nbsp; <input type="button" class="btn" value="<spring:message code='label.upload'/>" onclick="validCsv('#benefits2','#frmBenefits2','#benefitsFile2_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/benefits.csv" />><spring:message  code="label.downloadTemplate"/></a>
																<input type="hidden" name="fileType" value="benefits"/>
																<input type="hidden" name="fileId" value="benefits2"/>
	                          								</span>
																<div id="benefitsFile2_error" class=""></div>
															<div id="benefits2FileName" class="help">${benefitFile2}</div>
	                                                </div>	<!-- end of controls-->
	                                        </div>	<!-- end of control-group -->
                                        </form>
                                        <div class="form-actions">
                                            <!--<a class="btn btn-primary" data-toggle="modal" href="#myModal" >Save &amp; Continue</a>-->
                                            <input type="button" name="submit2" id="submit2" value="<spring:message code='label.saveAndContinue'/>" title="<spring:message  code='label.saveAndContinue'/>" class="btn btn-primary" onclick="processPlan('frmSilverPlan2','collapseTwo','collapseThree')" />
                                        </div><!-- end of form-actions -->	
                                    </div>
                                  </div>
                                </div>
                                <div class="accordion-group">
                                  <div class="accordion-heading">
                                    <a href="#collapseThree" data-parent="#accordion2" data-toggle="collapse" class="accordion-toggle">
                                      <strong>&#91;${planName}&#93;</strong> <spring:message code='label.withMessage3'/>&#37; <spring:message code="label.actuarialVal"/>
                                      <img id="loadingImg3" style="display:none;"  src="<c:url value="/resources/images/loader.white.gif" />" alt="loader"/>
                                    </a>
                                  </div>
                                  <div class="accordion-body collapse" id="collapseThree">
                                    <div class="accordion-inner">
                                       	<form class="form-horizontal" id="frmCerti3" name="frmCerti3" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
                                        <df:csrfToken/>
                                        <div class="control-group">
                                            <label for="firstName" class="required control-label" for="fileInput">
                                            	<spring:message code="label.valueCertificate"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#"><i class="icon-question-sign"></i></a></label>
                                                <div class="controls">
                                                    <!--<input type="file" id="fileInput" class="input-file">
                                                    &nbsp; <button class="btn">Upload</button>
                                                    <div id="firstName_error" class="help-inline"></div>-->
														<span>
															<input type="file" id="certi3" name="certi3"  class="input-file">&nbsp; <button class="btn"><spring:message code="label.upload"/></button>
															<input type="hidden" name="fileType" value="certificates"/>
															<input type="hidden" name="fileId" value="certi3"/>
                          								</span>
															<div id="certiFile3_error" class=""></div>
														<div id="certi3FileName" class="help">${actCerti3}</div>
                                                </div>	<!-- end of controls-->
                                        </div>	<!-- end of control-group -->
                                        </form>
                                        <form class="form-vertical" id="frmSilverPlan3" name="frmSilverPlan3" action="<c:url value="/admin/planmgmt/submitcostsharing" />" method="POST">
           									<df:csrfToken/>
           									<input type="hidden" id="certiFile3" name="certiFile3"/>
           									<input type="hidden" id="benefitsFile3" name="benefitsFile3"/>
           									<input type="hidden" id="planId3" name="planId3" value="${planId}" />
           									<input type="hidden" id="costSharing" name="costSharing" value="CS6"/>
               							</form>
               							<form class="form-horizontal" id="frmBenefits3" name="frmBenefits3" action="<c:url value="/planmgmt/uploadData" />" method="post" enctype="multipart/form-data">
	                                        <df:csrfToken/>
	                                        <div class="control-group">
	                                            <label for="firstName" class="required control-label" for="fileInput">
	                                            	<spring:message code="label.linkPlanBenefits"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> <a href="#"><i class="icon-question-sign"></i></a></label>
	                                                <div class="controls">
	                                                    <!--<input type="file" id="fileInput" class="input-file">
	                                                    &nbsp; <button class="btn">Upload</button>
	                                                    <span><a href=<c:url value="/resources/sampleTemplates/benefits.csv" />>Download template</a></span>
	                                                    <div id="firstName_error" class="help-inline"></div>-->
															<span>
																<input type="file" id="benefits3" name="benefits3"  class="input-file">&nbsp; <input type="button" class="btn" value="Upload" onclick="validCsv('#benefits3','#frmBenefits3','#benefitsFile3_error')"/>&nbsp; <a href=<c:url value="/resources/sampleTemplates/benefits.csv" />>Download template</a>
																<input type="hidden" name="fileType" value="benefits"/>
																<input type="hidden" name="fileId" value="benefits3"/>
	                          								</span>
																<div id="benefitsFile3_error" class=""></div>
															<div id="benefits3FileName" class="help">${benefitFile3}</div>
	                                                </div>	<!-- end of controls-->
	                                        </div>	<!-- end of control-group -->
                                        </form>
                                        <div class="form-actions">
                                            <!--<a class="btn btn-primary" data-toggle="modal" href="#myModal" >Save &amp; Continue</a>-->
                                            <input type="button" name="submit3" id="submit3" value="<spring:message code='label.saveAndContinue'/>" title="<spring:message  code='label.saveAndContinue'/>" class="btn btn-primary" onclick="processPlan('frmSilverPlan3','collapseThree','')" />
                                        </div><!-- end of form-actions -->	
                                    </div>
                                  </div>
                            	</div>
 						<div class="form-actions">
 							<div id="all_error"></div>
							<!--<a class="btn btn-primary" data-toggle="modal" href="#myModal" >Submit</a>-->
                            <form class="form-horizontal" id="frmSubmitAll" name="frmSubmitAll" action="<c:url value="/admin/planmgmt/submitqhpvariations" />" method="post" enctype="multipart/form-data">
								<df:csrfToken/>
								<input type="hidden" id="planId" name="planId" value="${planId}"/>
                            </form>
							<input type="button" name="submitAll" id="submitAll" value="<spring:message code='label.btnSubmit'/>" title="<spring:message code='label.btnSubmit'/>" class="btn btn-primary" onclick="submitAll()" />
						</div><!-- end of form-actions -->
						
					</div><!-- end accordian2 -->
				<!-- </form> -->
				</div>		          					
			</div><!-- end of span9 -->
	
		</div><!--  end of row-fluid -->
</div>
<script type="text/javascript">
var form1Uploaded = false;
var form2Uploaded = false;
var form3Uploaded = false;
var formEdit = ('${section}' > 0);

function updateSubmit()
{

	if (form1Uploaded && form2Uploaded && form3Uploaded)
	{
		$("#submitAll").removeAttr("disabled");
	}
	else
	{
		$("#submitAll").attr("disabled", "disabled");
	}
}
function  submitAll()
{
	var str = "";
	var req = "Required ";
	var decorStr1 = "<span> <em class='excl'>!</em> ";
	var decorStr2 = "</span>";
	var isPending = false;
	var isEdit = ('${section}' > 0);
	if (!isEdit)
	{
		if (form1Uploaded == false)
		{	
			str = "<strong>&#91;c&#93;</strong> <spring:message code='label.withMessage4'/>";
			isPending = true;
			showSection('collapseOne');
		}
		else if (form2Uploaded == false)
		{	
			str = "<strong>&#91;c&#93;</strong> <spring:message code='label.withMessage5'/>";
			isPending = true;
			showSection('collapseTwo');
		}
		else if (form3Uploaded == false)
		{	
			str = "<strong>&#91;c&#93;</strong> <spring:message code='label.withMessage6'/>";
			isPending = true;
			showSection('collapseThree');
		}
	}
	isPending=false;
	if (!isPending || isEdit)
	{
		formSubmit('#frmSubmitAll');
	}
	else
	{
		$("#all_error").html(decorStr1+req+str+decorStr2);
	}
	
}
function  formSubmit(formName)
{
	//if($("#optionsCheckbox").is(":checked"))
	{
		$(formName).submit();
	}
}


$(document).ready(function(){
	//getIssuerNames();
	$('#loadingImg1').hide();
	$('#loadingImg2').hide();
	$('#loadingImg3').hide();
	isFileReq();
	updateSubmit();
	var certi1Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitCerti1Request,  // pre-submit callback 
	        success:       submitCerti1Response  // post-submit callback 
	    };
	var certi2Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitCerti2Request,  // pre-submit callback 
	        success:       submitCerti2Response  // post-submit callback 
	    };
	var certi3Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitCerti3Request,  // pre-submit callback 
	        success:       submitCerti3Response  // post-submit callback 
	    };
    $('#frmCerti1').ajaxForm(certi1Upload);
    $('#frmCerti2').ajaxForm(certi2Upload);
    $('#frmCerti3').ajaxForm(certi3Upload);

    var benefits1Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitBenefits1Request,  // pre-submit callback 
	        success:       submitBenefits1Response  // post-submit callback 
	    };
    var benefits2Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitBenefits2Request,  // pre-submit callback 
	        success:       submitBenefits2Response  // post-submit callback 
	    };
    var benefits3Upload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitBenefits3Request,  // pre-submit callback 
	        success:       submitBenefits3Response  // post-submit callback 
	    };
    $('#frmBenefits1').ajaxForm(benefits1Upload);
    $('#frmBenefits2').ajaxForm(benefits2Upload);
    $('#frmBenefits3').ajaxForm(benefits3Upload);


    var silverPlan1 = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitSilverPlan1Request,  // pre-submit callback 
	        success:       submitSilverPlan1Response  // post-submit callback 
	    };
    var silverPlan2 = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitSilverPlan2Request,  // pre-submit callback 
	        success:       submitSilverPlan2Response  // post-submit callback 
	    };
    var silverPlan3 = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitSilverPlan3Request,  // pre-submit callback 
	        success:       submitSilverPlan3Response  // post-submit callback 
	    };
    $('#frmSilverPlan1').ajaxForm(silverPlan1);
    $('#frmSilverPlan2').ajaxForm(silverPlan2);
    $('#frmSilverPlan3').ajaxForm(silverPlan3);
    checkDefaultSection();
});

function checkDefaultSection()
{
    var sectionId = '${section}';
    if (sectionId == 1)
    {
		//showSection('collapseOne');
    	hideSection('collapseTwo');
		hideSection('collapseThree');
    } else if(sectionId == 2)
    {
		showSection('collapseTwo');
    } else if(sectionId == 3)
    {
		showSection('collapseThree');
    }
}
function showReview()
{
	var isEdit = ('${section}' > 0);
	if(isEdit)
	{
		submitAll();
	}
}

function submitSilverPlan1Response(responseText, statusText, xhr, $form)  {
	form1Uploaded = true;
	updateSubmit();
	showReview();
	updateLoadingIcon1(false);
}

function submitSilverPlan1Request(formData, jqForm, options) {
	updateLoadingIcon1(true);
	return true;
}

function submitSilverPlan2Response(responseText, statusText, xhr, $form)  {
	form2Uploaded = true;
	updateSubmit();
	showReview();
	updateLoadingIcon2(false);
}

function submitSilverPlan2Request(formData, jqForm, options) {
	updateLoadingIcon2(true);
	return true;
}

function submitSilverPlan3Response(responseText, statusText, xhr, $form)  {
	form3Uploaded = true;
	updateSubmit();
	showReview();
	updateLoadingIcon3(false);
}

function submitSilverPlan3Request(formData, jqForm, options) {
	updateLoadingIcon3(true);
	return true;
}

function submitCerti1Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");

	if(val[0] != 'Error')
	{
		$("#certi1FileName").text(val[1]);
		$("#certiFile1").val(val[2]);
		$("#certiFile1_error").html("");
	}
	else
	{
		$("#certiFile1_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function updateLoadingIcon1(showLoading)
{
	if(showLoading)
	{
		$('#loadingImg1').show();
	}
	else
	{
		$('#loadingImg1').hide();
	}
}

function updateLoadingIcon2(showLoading)
{
	if(showLoading)
	{
		$('#loadingImg2').show();
	}
	else
	{
		$('#loadingImg2').hide();	
	}
}

function updateLoadingIcon3(showLoading)
{
	if(showLoading)
	{
		$('#loadingImg3').show();
	}
	else
	{
		$('#loadingImg3').hide();	
	}
}

function submitCerti1Request(formData, jqForm, options) {
	var fileName = $('#certi1').val();
	if (fileName.length > 0)
	{	    return true;	
	} 
	return false;
}


function submitCerti2Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{
		$("#certi2FileName").text(val[1]);
		$("#certiFile2").val(val[2]);
		$("#certiFile2_error").html("");
	}
	else
	{
		$("#certiFile2_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitCerti2Request(formData, jqForm, options) {
	var fileName = $('#certi2').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}


function submitCerti3Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{
		$("#certi3FileName").text(val[1]);
		$("#certiFile3").val(val[2]);
		$("#certiFile3_error").html("");
	}
	else
	{
		$("#certiFile3_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitCerti3Request(formData, jqForm, options) {
	var fileName = $('#certi3').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function submitBenefits1Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{
		$("#benefits1FileName").text(val[1]);
		$("#benefitsFile1").val(val[2]);
		$("#benefitsFile1_error").html("");
	}
	else
	{
		$("#benefitsFile1_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitBenefits1Request(formData, jqForm, options) {
	var fileName = $('#benefits1').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function submitBenefits2Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{
		$("#benefits2FileName").text(val[1]);
		$("#benefitsFile2").val(val[2]);
		$("#benefitsFile2_error").html("");
	}
	else
	{
		$("#benefitsFile2_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitBenefits2Request(formData, jqForm, options) {
	var fileName = $('#benefits2').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}

function submitBenefits3Response(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{
		$("#benefits3FileName").text(val[1]);
		$("#benefitsFile3").val(val[2]);
		$("#benefitsFile3_error").html("");
	}
	else
	{
		$("#benefitsFile3_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitBenefits3Request(formData, jqForm, options) {
	var fileName = $('#benefits3').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}
</script>

<script type="text/javascript">
function showNext(divName1,divName2)
{
	$('#' + divName1).collapse('hide');
	$('#' + divName2).collapse('show');
}

function hideSection(divName)
{
	$('#' + divName).collapse('hide');
	//$('#' + divName).hide();
}
function showSection(divName)
{
	$('#' + divName).collapse('show');	
	hideOthers(divName);
}

function hideOthers(divName)
{
	if(divName == 'collapseOne')
	{
		hideSection('collapseTwo');
		hideSection('collapseThree');
	}
	else if(divName == 'collapseTwo')
	{
		hideSection('collapseOne');
		hideSection('collapseThree');
	}
	else if(divName == 'collapseThree')
	{
		hideSection('collapseOne');
		hideSection('collapseTwo');
	}
}
</script>

<script type="text/javascript">
function processPlan(formId, divName1, divName2)
{
	if (validatePlan(formId))
	{
		formSubmit('#'+formId);
		if(divName2 !=  null && divName2.length>0)
			showNext(divName1, divName2);
		else
			hideSection(divName1);
	}
}
function validatePlan(formId)
{
	var val =  $("#"+formId).valid();
	return val;
}

function validCsv(fileElementId, formElementId, errorElementId)
{
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if(jQuery.inArray(ext, allow) == -1) {
	    $(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileTypeError'/></span></label>");
	   return false;
	}else{
		$(formElementId).submit();
	  } 
}
</script>

<script type="text/javascript">
function isFileReq()
{
	if("false" == '${isEdit}')
		{
			$("#certiFile1").rules("add", "required");
			$("#benefitsFile1").rules("add", "required");
			
			$("#certiFile2").rules("add", "required");
			$("#benefitsFile2").rules("add", "required");
			
		
			$("#certiFile3").rules("add", "required");
			$("#benefitsFile3").rules("add", "required");
		}
	else
		{
			if("false" == '${hasActCerti1}')
			{
				$("#certiFile1").rules("add", "required");
			}
			if("false" == '${hasBenefitFile1}')
			{
				$("#benefitsFile1").rules("add", "required");
			}
			if("false" == '${hasActCerti2}')
			{
				$("#certiFile2").rules("add", "required");
			}
			if("false" == '${hasBenefitFile2}')
			{
				$("#benefitsFile2").rules("add", "required");
			}
			if("false" == '${hasActCerti3}')
			{
				$("#certiFile3").rules("add", "required");
			}
			if("false" == '${hasBenefitFile3}')
			{
				$("#benefitsFile3").rules("add", "required");
			}
		}
}

var validator1 = $("#frmSilverPlan1").validate({
	 ignore: "",
	rules : {certiFile1 : { required : false},
		benefitsFile1 : { required : false}
	}, 
	messages : {
		certiFile1 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadCertificate'/></span>"},
		benefitsFile1 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadBenefitData'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		$("#" + elementId + "_error").html("");
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});

var validator2 = $("#frmSilverPlan2").validate({
	 ignore: "",
	rules : {certiFile2 : { required : false},
		benefitsFile2 : { required : false}
	}, 
	messages : {
		certiFile2 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadCertificate'/></span>"},
		benefitsFile2 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadBenefitData'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		$("#" + elementId + "_error").html("");
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});

var validator3 = $("#frmSilverPlan3").validate({
	 ignore: "",
	rules : {certiFile3 : { required : false},
		benefitsFile3 : { required : false}
	}, 
	messages : {
		certiFile3 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadCertificate'/></span>"},
		benefitsFile3 : { required : "<span> <em class='excl'>!</em><spring:message code='label.uploadBenefitData'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		$("#" + elementId + "_error").html("");
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});
</script>
