package com.getinsured.consumer.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.transaction.TransactionConfiguration;


public class ConsumerServiceTest
{
	
//	@Autowired private ConsumerService consumerService;
//	private static final String INDIVIDUAL_ID = "individualId";
//	private static final String MODULE_NAME = "moduleName";
//	private static final String MODULE_ID = "moduleId";
//	private static final String START_PAGE = "startPage";
//	private static final String ENTITY = "ENTITY";
//	private static final String BROKER = "BROKER";
//	private static final String ASSISTER = "ASSISTER";
//	private static final String CONSUMERS = "consumers";
//
//	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerServiceTest.class);
//	
//	/**
//	 * Simpliest method.
//	*/
//	@Test
//	public void testConsumerAPIForEntity()
//	{		
//		final String individualId = "203";
//		final Integer moduleId =  63;
//		Map<String, Object> searchCriteria = new HashMap<String, Object>();
//		searchCriteria.put(MODULE_NAME, ENTITY);
//		searchCriteria.put(INDIVIDUAL_ID, individualId);
//		searchCriteria.put(MODULE_ID, moduleId);
//		searchCriteria.put(START_PAGE, 0);
//		
//		Map<String, Object> map = new HashMap<>();
//		map.put("searchCriteria", searchCriteria);
//		getConsumer(map);
//	} 
//	
//	@Test
//	public void testConsumerAPIForAgent()
//	{		
//		final String individualId = "203";
//		final Integer moduleId =  63;
//		Map<String, Object> searchCriteria = new HashMap<String, Object>();
//		searchCriteria.put(MODULE_NAME, BROKER);
//		searchCriteria.put(INDIVIDUAL_ID, individualId);
//		searchCriteria.put(MODULE_ID, moduleId);
//		searchCriteria.put(START_PAGE, 0);
//		
//		Map<String, Object> map = new HashMap<>();
//		List<IndividualComposite> individualList = new ArrayList<IndividualComposite>();		
//		map.put("searchCriteria", searchCriteria);		
//		getConsumer(map);
//	} 
//	
//	@Test
//	public void testConsumerAPIForAssister()
//	{		
//		final String individualId = "203";
//		final Integer moduleId =  63;
//		Map<String, Object> searchCriteria = new HashMap<String, Object>();
//		searchCriteria.put(MODULE_NAME, ASSISTER);
//		searchCriteria.put(INDIVIDUAL_ID, individualId);
//		searchCriteria.put(MODULE_ID, moduleId);
//		searchCriteria.put(START_PAGE, 0);
//		
//		Map<String, Object> map = new HashMap<>();
//		map.put("searchCriteria", searchCriteria);
//		getConsumer(map);
//		
//	}
//	
//	private void getConsumer(Map<String, Object> map)
//	{
//		List<IndividualComposite> individualList = new ArrayList<IndividualComposite>();
//		IndividualComposite hhIndividual = null;
//		try {
//			Map<String,Object>  consumers = consumerService.getConsumerList(map);
//		
//			if(consumers != null && consumers.size() > 0) {
//				
//				individualList = (List<IndividualComposite>) consumers.get(CONSUMERS);
//						
//				hhIndividual =  new IndividualComposite();
//				hhIndividual = individualList.get(0);
//				
//				LOGGER.info("=======================Individual Details=====================================");
//				LOGGER.info("First Name of Individual :: " +hhIndividual.getFirstName());
//				LOGGER.info("Last Name of Individual :: " +hhIndividual.getLastName());
//				LOGGER.info("Address1 of Individual :: " +hhIndividual.getAddress1());
//				LOGGER.info("Address2 of Individual :: " +hhIndividual.getAddress2());
//				LOGGER.info("City of Individual :: " +hhIndividual.getCity());
//				LOGGER.info("State of Individual :: " +hhIndividual.getState());
//				LOGGER.info("ZipCode of Individual :: " +hhIndividual.getZip());
//				LOGGER.info("Phone Number of Individual :: " +hhIndividual.getPhoneNumber());
//				LOGGER.info("Email Address of Individual :: " +hhIndividual.getEmailAddress());
//				LOGGER.info("Eligibility Status of Individual :: " +hhIndividual.getEligibilityStatus());
//				LOGGER.info("Application Status of Individual :: " +hhIndividual.getApplicationStatus());
//				LOGGER.info("==================================================================================");
//				
//			}
//		} catch(Exception exception){
//    		Assert.fail();
//    	}
//	}
//	
	

}