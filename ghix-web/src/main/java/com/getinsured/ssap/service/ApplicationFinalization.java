package com.getinsured.ssap.service;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * Interface for SSAP finalization flow.
 *
 * @author Yevgen Golubenko
 * @since 2019-09-23
 */
public interface ApplicationFinalization {
  /**
   * Submit SSAP for finalization.
   *
   * @param application             {@link SingleStreamlinedApplication} object.
   * @param parentSsapApplicationId ID of SSAP application that this was cloned from.
   * @param specialEnrollmentEvent  special enrollment event (married, divorced, had child, etc.)
   * @param specialEnrollmentDate   special enrollment date.
   * @param csrOverride             is this CSR override submission or not
   */
  void finalizeApplication(SingleStreamlinedApplication application, Long parentSsapApplicationId, String specialEnrollmentEvent,
                                   String specialEnrollmentDate, boolean csrOverride, AccountUser currentUser);
}
