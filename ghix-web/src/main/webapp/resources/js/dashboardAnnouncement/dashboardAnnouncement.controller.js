(function(){
	'use strict';
	
	angular.module("dashboardAnnouncementApp").controller('DashboardAnnouncementController', DashboardAnnouncementController);

	DashboardAnnouncementController.$inject=['$state','$http','$scope','$rootScope','$timeout','anouncementData','editMode'];
	
	function DashboardAnnouncementController($state,$http,$scope,$rootScope,$timeout,anouncementData,editMode){
		var vm = this;
		var url = null;
		var validationSuccess = false;
		vm.successModel = false;
		vm.failureModel = false;
		vm.startDateError = false;
		vm.endDateError = false;
		vm.announcementError = false;
		vm.announcementTextError = false;
		vm.announcementTextLengthError = false;
		vm.editMode = editMode;
		if(editMode==true)
		{
			$scope.htmlEditor=anouncementData.data.announcementText;
			
			vm.announcementName = anouncementData.data.announcementName;
			vm.effectiveStartDate = anouncementData.data.effectiveStartDate;
			vm.effectiveEndDate = anouncementData.data.effectiveEndDate;
			vm.moduleName = anouncementData.data.moduleName;
			vm.updatedDate = anouncementData.data.updatedDate;
			url = '../announcements/'+anouncementData.data.id;
		}else{
			$scope.htmlEditor="";
			url = '../announcements/';
		}
		
		vm.goToAnnouncementList = goToAnnouncementList;
		vm.publishAnnouncement = publishAnnouncement;
		vm.validateDate = validateDate;
		vm.validateAnnouncementData = validateAnnouncementData;
		
		try{
			for(name in CKEDITOR.instances)
			{
			    CKEDITOR.instances[name].destroy(true);
			}
		}catch(e){
			console.log("Error in the CK Editor");
		}
		
		/*$timeout(function(){
			ckEditorInst.setData(vm.htmlEditor);
		},600)*/
		
		
		function goToAnnouncementList(){
			$(".modal-backdrop").remove();
			$state.go('announcementList');
		}
		
		vm.cancel = function(){
			vm.successModel = false;
			vm.failureModel = false; 
			$(".modal-backdrop").remove();
		}
		
		function validateAnnouncementData(announcementText){
			
			if (vm.announcementName === undefined || vm.announcementName === "") {
				vm.announcementError = true;
			}else{
				vm.announcementError = false;
			}
			
			vm.validateDate();
			
			if(announcementText === undefined || announcementText === ""){
				vm.announcementTextError = true;
			}else{
				vm.announcementTextError = false;
			}

			//length with HTML tag check
			if(announcementText.length > 1020){
				vm.announcementTextLengthError = true;
			}else{
				//length without HTML tag check
				var messageLength = announcementText.replace(/<.*?>/g, "").replace(/&nbsp;/g, " ").length;
				if(messageLength > 400){
					vm.announcementTextLengthError = true;
				}else{
					vm.announcementTextLengthError = false;
				}
			}
			
			
			if(vm.announcementError===false && (vm.startDateError===false && vm.endDateError===false) && vm.announcementTextError===false && vm.announcementTextLengthError === false){
				return true;
			}
		}
		
		function validateDate(){
			
			if (vm.effectiveStartDate === undefined || vm.effectiveStartDate === "" && editMode===false) {
				vm.startDateError = true;
			}else if (vm.effectiveStartDate != "" && vm.effectiveEndDate === undefined || vm.effectiveEndDate === "" && editMode===false) {
				var startDate = new Date(vm.effectiveStartDate);
				var today = new Date();
				if (startDate.toString() === "Invalid Date") {
					vm.startDateError = true;
				}else if (startDate.getFullYear() < today.getFullYear() 
						|| startDate.getMonth() < today.getMonth() 
						|| startDate.getMonth() == today.getMonth() && startDate.getDate() < today.getDate()) {
					vm.startDateError = true;
					vm.endDateError = false;
			    }else if(vm.effectiveEndDate === undefined || vm.effectiveEndDate === "" && editMode===false){
			    	vm.startDateError = false;
			    	vm.endDateError = true;
			    }
			}else if (vm.effectiveStartDate != "" && vm.effectiveEndDate != "") {
				var startDate = new Date(vm.effectiveStartDate);
				var endDate = new Date(vm.effectiveEndDate);
				var today = new Date();
				if (startDate.toString() === "Invalid Date") {
					vm.startDateError = true;
				}else if(endDate.toString() === "Invalid Date"){
					vm.endDateError = true;
				}else if (editMode===false && (startDate.getFullYear() < today.getFullYear() 
						|| startDate.getMonth() < today.getMonth() 
						|| startDate.getMonth() == today.getMonth() && startDate.getDate() < today.getDate())) {
					vm.startDateError = true;
			    }else if (endDate < startDate) {
					vm.endDateError = true;
			    }else{
			    	vm.startDateError = false;
					vm.endDateError = false;
			    }
			}
		}
		
		function publishAnnouncement(){
			
			/*vm.htmlEditor=ckEditorInst.getData();*/
			
			validationSuccess = validateAnnouncementData($scope.htmlEditor);
			
			if(validationSuccess){
				//HTTP call
				var data = {"announcementName":vm.announcementName, "announcementText": $scope.htmlEditor, "effectiveStartDate": vm.effectiveStartDate, "effectiveEndDate": vm.effectiveEndDate};	
				var config = {
					headers: { 'Content-Type': 'application/json; charset=UTF-8'}
				};
				
				config.headers[csrfToken.paramName]=csrfToken.paramValue;
				
				var response = $http.post(url, data, config);
				
				response.success(function(data, status, headers, config) {
					if (data === 'success') {
						vm.successModel = true;
						$('<div class="modal-backdrop"></div>').appendTo(document.body);
					} else if (data == 'failure') {
						vm.failureModel = true; 
						$('<div class="modal-backdrop"></div>').appendTo(document.body);
					}
				});
				
				response.error(function() { 
					vm.failureModel = true; 
					$('<div class="modal-backdrop"></div>').appendTo(document.body);
				});
			}
		}
	}
		
})();
/*var ckEditorInst=null;*/