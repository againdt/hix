/**
 * 
 */
package com.getinsured.iex.ssap.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.ind71G.client.Ind71GClientAdapter;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutDependant;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutNoticeDTO;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

/**
 * Age out service implementation
 * @author negi_s
 *
 */
@Service
public class AgeOutDisEnrollServiceImpl implements AgeOutDisEnrollService {

	private static final Logger LOGGER = Logger.getLogger(AgeOutDisEnrollServiceImpl.class);

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	@Autowired
	@Qualifier("ind71GClientAdapter")
	private Ind71GClientAdapter ind71GClientAdapter;
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	@Autowired
	private AptcHistoryService aptcHistoryService;
	@Autowired
	private Gson platformGson;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private PreferencesService preferencesService;

	private static final String DATE_FORMAT_MMMM_DD_YYYY = "MMMM dd, YYYY";
	private static final String DATE_FORMAT_MMMM_D_YYYY = "MMMM d, YYYY";
	private static final String DISENROLL_FROM_DENTAL = "disenrollFromDental";
	private static final String DISENROLL_FROM_HEALTH = "disenrollFromHealth";
	private static final String HEALTH_ENROLLMENT_DTO = "HealthEnrollmentDTO";
	private static final String DENTAL_ENROLLMENT_DTO = "DentalEnrollmentDTO";
	private static final String AGE_OUT_DISENROLL_NOTIFICATION = "AgeOutDisenrollNotification";
	private static final String IS_AGE_MORE_THAN26 = "isAgeMoreThan26";
	private static final String IS_AGE_MORE_THAN19 = "isAgeMoreThan19";
	private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

	public void handleAgeOutDisenroll(long ssapApplicationId, long enrolledSsapApplicationId,
			Map<String, List<EnrollmentShopDTO>> enrollmentIdMap, Map<String, Set<String>> disenrollMap) throws GIException {
		SsapApplication currentApplication = loadCurrentApplication(ssapApplicationId);
		Date coverageStartDate = updateEffectiveDate(currentApplication, ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		String changePlan = "N";
		currentApplication = loadCurrentApplication(currentApplication.getId());//// HIX-108047
		final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, enrolledSsapApplicationId,
				coverageStartDate);
		if (aptcUpdate == null) {
			LOGGER.error("APTC redistribution failure - " + currentApplication.getId());
		} else {
			final boolean status = ind71GCall(enrolledSsapApplicationId, currentApplication.getId(),
					currentApplication.getCoverageYear(), aptcUpdate, changePlan, enrollmentIdMap, disenrollMap);
			if (status) {
				LOGGER.info("IND71G update successful for  " + currentApplication.getId());
				currentApplication.setEffectiveDate(
						EligibilityUtils.getDateWithoutTimeUsingFormat(aptcUpdate.getAptcEffectiveDate()));
				triggerAgeOutNotice(currentApplication, disenrollMap, enrollmentIdMap);
			} else {
				LOGGER.info("IND71G update failed for  " + currentApplication.getId());
				throw new GIException("IND71G update failed for  " + currentApplication.getId());
			}
		}

	}

	public boolean ind71GCall(long enrolledAppId, long currentAppId, long coverageYear, AptcUpdate aptcUpdate,
			String changePlan, Map<String, List<EnrollmentShopDTO>> enrollmentIdMap,
			Map<String, Set<String>> disenrollMap) {

		boolean status = false;
		SsapApplication enrolledApplication = loadCurrentApplication(enrolledAppId);//// HIX-108047
		String enrollmentResponseStr = getEnrollmentsByMember(enrolledApplication.getSsapApplicants(),
				String.valueOf(coverageYear));
		if (StringUtils.isNotBlank(enrollmentResponseStr)) {
			EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr,
					EnrollmentResponse.class);
			if (enrollmentResponse != null) {
				status = executeIND71G(currentAppId, aptcUpdate, enrollmentResponse, changePlan, enrollmentIdMap,
						disenrollMap);
			} else {
				LOGGER.info("Member enrollment mapping call failure - " + currentAppId);
			}
		} else {
			LOGGER.info("Member enrollment mapping call failure - " + currentAppId);
		}
		return status;
	}

	public AptcUpdate populateAptcUpdateRequest(SsapApplication currentApplication, long enrolledApplicationId,
			Date finEffectiveDate) {
		AptcUpdate aptcUpdate = new AptcUpdate();
		String appGroupResponseJson = null;
		AppGroupResponse appGroupResponse = null;
		try {
			AppGroupRequest appGroupRequest = new AppGroupRequest();
			appGroupRequest.setApplicationId(currentApplication.getId());

			if (finEffectiveDate == null) {
				throw new GIRuntimeException(
						"APTC redistribution financial Effective date can not be null : " + currentApplication.getId());
			}

			getUpdatedDateBasedonEnrollment(enrolledApplicationId, finEffectiveDate, aptcUpdate);
			if (aptcUpdate.getAptcEffectiveDate() != null) {
				finEffectiveDate = aptcUpdate.getAptcEffectiveDate();
			}
			String coverageStartDate = new SimpleDateFormat("MM/dd/yyyy").format(finEffectiveDate);

			appGroupRequest.setCoverageStartDate(coverageStartDate);

			try {
				/*
				 * this had to be changed as when called from web the logged in user is lost
				 * this is done same as when called from LCE auto flow we pass exadmin user
				 */
				appGroupResponseJson = ghixRestTemplate
						.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/api/getAppGroups/",
								ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON,
								String.class, appGroupRequest)
						.getBody();
				aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
			} catch (Exception e) {
				LOGGER.error("Exception occured for calling getAppGroups api", e);
				throw new GIRuntimeException(
						"Error occoured while getting aptc redistribution: " + appGroupResponseJson);
			}
			try {
				appGroupResponse = platformGson.fromJson(appGroupResponseJson, AppGroupResponse.class);
				aptcUpdate.setAppGroupResponse(appGroupResponse);
			} catch (Exception e) {
				throw new GIRuntimeException(
						"Invalid response in APTC redistribution for application id : " + currentApplication.getId());
			}
		} catch (Exception e) {
			/** Eat the Exception */
		}
		return aptcUpdate;
	}

	private void getUpdatedDateBasedonEnrollment(long enrolledApplicationId, Date finEffectiveDate,
			AptcUpdate aptcUpdate) {
		final Map<String, List<EnrollmentShopDTO>> enrollmentDetails = ssapEnrolleeHandler
				.fetchMultipleEnrollmentID(enrolledApplicationId);

		List<EnrollmentShopDTO> healthList = enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO);
		if (enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO) != null) {
			healthList.addAll(enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO));
		}

		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if (healthList != null) {
				for (EnrollmentShopDTO health : healthList) {
					String healthEnrollmentStartDate = df.format(health.getCoverageStartDate());
					if (StringUtils.isNotEmpty(healthEnrollmentStartDate)
							&& isAptcStartDateBeforeEnrollmentStartDate(finEffectiveDate, healthEnrollmentStartDate)) {
						finEffectiveDate = df.parse(healthEnrollmentStartDate);
					}
				}
			}
			aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
			aptcUpdate.setEnrollmentList(healthList);
		} catch (Exception e) {
			LOGGER.error("Exception occured while parsing new financial effective date and enrollment start date", e);
			throw new GIRuntimeException(
					"Exception occured while parsing new financial effective date and enrollment start date: "
							+ enrolledApplicationId);
		}
	}

	public boolean isAptcStartDateBeforeEnrollmentStartDate(Date aptcEffectiveDate, String enrollmentEffectiveStartDate)
			throws ParseException {
		boolean isAptcStartDateBeforeEnrollmentStartDate = false;

		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		org.joda.time.LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		org.joda.time.LocalDate enrollmentStartDate = new DateTime(mmddyyyy.parseObject(enrollmentEffectiveStartDate))
				.toLocalDate();
		if (aptcStartDate.isBefore(enrollmentStartDate)) {
			isAptcStartDateBeforeEnrollmentStartDate = true;
		}
		return isAptcStartDateBeforeEnrollmentStartDate;
	}

	public String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}

		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);

		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);

		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate
				.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE,
						HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,
						platformGson.toJson(enrollmentRequest))
				.getBody();

		return enrollmentResponseStr;
	}

	private boolean executeIND71G(long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse,
			String isChangePlan, Map<String, List<EnrollmentShopDTO>> enrollmentIdMap,
			Map<String, Set<String>> disenrollMap) {
		String ind71GResponse = GhixConstants.RESPONSE_FAILURE;
		try {
			/* Call IND71G client */
			ind71GResponse = ind71GClientAdapter.execute(ssapApplicationId, aptcUpdate, enrollmentResponse,
					isChangePlan, disenrollMap);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}

		boolean status = false;
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(ind71GResponse)) {
			status = true;
		}
		return status;
	}

	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

	public SsapApplication loadApplication(Long applicationId) {
		return ssapApplicationRepository.findOne(applicationId);
	}

	public Date updateEffectiveDate(SsapApplication currentApplication, SsapApplicationEvent ssapApplicationEvent) {
		Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEvent);
		aptcHistoryService.updateEffectiveDate(coverageStartDate, new Long(currentApplication.getId()));
		return coverageStartDate;
	}

	private void triggerAgeOutNotice(SsapApplication ssapApplication, Map<String, Set<String>> disenrollMap,
			Map<String, List<EnrollmentShopDTO>> enrollmentIdMap) {

		List<EnrollmentShopDTO> healthEnrollmentShopDTOList = enrollmentIdMap.get(HEALTH_ENROLLMENT_DTO);
		List<EnrollmentShopDTO> dentalEnrollmentShopDTOList = enrollmentIdMap.get(DENTAL_ENROLLMENT_DTO);
		prepareAndSendNotification(healthEnrollmentShopDTOList, ssapApplication, disenrollMap, true);
		prepareAndSendNotification(dentalEnrollmentShopDTOList, ssapApplication, disenrollMap, false);
	}

	private void prepareAndSendNotification(List<EnrollmentShopDTO> enrollmentShopDTOList,
			SsapApplication ssapApplication, Map<String, Set<String>> disenrollMap, boolean isHealth) {
		Set<String> disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental = disenrollMap.get(DISENROLL_FROM_DENTAL);
		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOList) {
			AgeOutNoticeDTO ageOutNoticeDTO = new AgeOutNoticeDTO();
			List<AgeOutDependant> dependants = new ArrayList<AgeOutDependant>();
			for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
				boolean isApplicantPresent = enrollmentShopDTO.getEnrolleeShopDTOList().stream()
						.anyMatch(e -> isActiveEnrollee(e)
								&& ssapApplicant.getApplicantGuid().equalsIgnoreCase(e.getExchgIndivIdentifier()));

				// disenroll from health
				if (isHealth && isApplicantPresent && disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())) {
					createAgeOutDependent(dependants, enrollmentShopDTO, ssapApplicant, 26);
				}
				// disenroll from dental
				else if (!isHealth && isApplicantPresent && disenrollFromDental.contains(ssapApplicant.getApplicantGuid())) {
					Map<String, Boolean> ageComparisonResult = checkForAge(ssapApplicant.getBirthDate());
					boolean isAgeMoreThan26 = ageComparisonResult.get(IS_AGE_MORE_THAN26);
					if (isAgeMoreThan26) {
						createAgeOutDependent(dependants, enrollmentShopDTO, ssapApplicant, 26);
					} else {
						createAgeOutDependent(dependants, enrollmentShopDTO, ssapApplicant, 19);
					}
				}
			}
			ageOutNoticeDTO.setApplicationID(ssapApplication.getCaseNumber());
			SsapApplicant primaryApplicant = getPrimaryApplicantDB(ssapApplication);
			ageOutNoticeDTO.setPrimaryFirstName(primaryApplicant.getFirstName());
			ageOutNoticeDTO.setPrimaryLastName(primaryApplicant.getLastName());
			ageOutNoticeDTO.setEmailId(primaryApplicant.getEmailAddress());
			ageOutNoticeDTO.setsEPendDate(formatDate(getLastDayOfCurrentMonth().getTime(), DATE_FORMAT_MMMM_DD_YYYY));
			// ageOutNoticeDTO.setHouseholdId(ssapApplication.getCmrHouseoldId().intValue());
			if (isHealth) {
				ageOutNoticeDTO.setHealthDependants(dependants);
			} else {
				ageOutNoticeDTO.setDentalDependants(dependants);
			}
			if(!dependants.isEmpty()) {
				sendNotification(ageOutNoticeDTO, AGE_OUT_DISENROLL_NOTIFICATION, ssapApplication);	
			}
		}
	}

	private void createAgeOutDependent(List<AgeOutDependant> healthDependants,
			EnrollmentShopDTO healthEnrollmentShopDTO, SsapApplicant ssapApplicant, int age) {
		AgeOutDependant ageOutDependant = new AgeOutDependant();
		ageOutDependant.setFirstName(ssapApplicant.getFirstName());
		ageOutDependant.setLastName(ssapApplicant.getLastName());
		ageOutDependant.setPlanId(healthEnrollmentShopDTO.getPlanName());
		if ("MN".equalsIgnoreCase(STATE_CODE)) {
			ageOutDependant.setDropOutDate(formatDate(getLastDayOfCurrentMonth().getTime(), DATE_FORMAT_MMMM_D_YYYY));
		}else {
			ageOutDependant.setDropOutDate(formatDate(getLastDayOfCurrentMonth().getTime(), DATE_FORMAT_MMMM_DD_YYYY));
		}
		ageOutDependant.setDropOutReasonText(ssapApplicant.getFirstName() + " will be " + age + " years old on %s",
				getAgeOutDate(ssapApplicant.getBirthDate(), age));
		healthDependants.add(ageOutDependant);
	}

	private Calendar getLastDayOfCurrentMonth() {
		Calendar lastDayOfMonth = Calendar.getInstance();
		lastDayOfMonth.set(Calendar.DAY_OF_MONTH, lastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
		lastDayOfMonth.set(Calendar.HOUR_OF_DAY, 23);
		lastDayOfMonth.set(Calendar.MINUTE, 59);
		lastDayOfMonth.set(Calendar.SECOND, 59);
		lastDayOfMonth.set(Calendar.MILLISECOND, 999);
		return lastDayOfMonth;
	}

	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if ("MN".equalsIgnoreCase(STATE_CODE) && applicant.isPrimaryTaxFiler()) {
				primaryApplicantDB = applicant;
				break;
			}else if(applicant.getPersonId() == 1){
				primaryApplicantDB = applicant;
				break;
			}
		}
		return primaryApplicantDB;
	}

	public void sendNotification(AgeOutNoticeDTO ageOutNoticeDTO, String templateName,
			SsapApplication ssapApplication) {
		List<String> sendToEmailList = new LinkedList<String>();
		;
		Map<String, Object> tokens = new HashMap<String, Object>();
		Location location = null;
		try {
			tokens.put("primaryApplicantName",
					ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName());
			tokens.put("ApplicationID", ssapApplication.getCaseNumber());
			tokens.put("Date", DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
			tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			tokens.put(TemplateTokens.EXCHANGE_PHONE,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
			tokens.put("exgCityName", DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
			tokens.put("exgStateName", DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
			tokens.put("zip", DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
			tokens.put(TemplateTokens.EXCHANGE_URL,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			tokens.put("exchangeFax",
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
			tokens.put("exchangeAddressEmail", DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			tokens.put(TemplateTokens.EXCHANGE_NAME,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			tokens.put("ageOutDependents", ageOutNoticeDTO);
			SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
			tokens.put("spanishDate", formatter.format(new Date()));
			tokens.put("caseNumber", ssapApplication.getCaseNumber());
			tokens.put("ssapApplicationId", ssapApplication.getId());
			String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
			String fileName = templateName + "_" + String.valueOf(ssapApplication.getCmrHouseoldId())
					+ System.currentTimeMillis() + ".pdf";
			String relativePath = "cmr/" + String.valueOf(ssapApplication.getCmrHouseoldId()) + "/ssap/"
					+ ssapApplication.getId() + "/notifications/";

			// preference communication logic
			PreferencesDTO preferencesDTO = preferencesService
					.getPreferences(ssapApplication.getCmrHouseoldId().intValue(), false);
			location = getLocationFromDTO(preferencesDTO.getLocationDto());
			sendToEmailList.add(preferencesDTO.getEmailAddress());

			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, tokens, relativePath, fileName,
					moduleName, ssapApplication.getCmrHouseoldId().longValue(), sendToEmailList,
					DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
					ageOutNoticeDTO.getPrimaryFirstName() + " " + ageOutNoticeDTO.getPrimaryLastName(), location,
					preferencesDTO.getPrefCommunication());
		} catch (Exception exception) {
			LOGGER.error("Error occurred while sending age out notification:" + ssapApplication.getId(), exception);
		}

	}

	private Location getLocationFromDTO(LocationDTO locationDto) {
		if (locationDto == null) {
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}

	private String formatDate(Date date, String pattern) {
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}

	private String getAgeOutDate(Date birthDate, int age) {
		String dateFormat = DATE_FORMAT_MMMM_DD_YYYY;
		if ("MN".equalsIgnoreCase(STATE_CODE)) {
			dateFormat = DATE_FORMAT_MMMM_D_YYYY;
		}
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		cal.setTime(birthDate);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.add(Calendar.YEAR, age);
		// if birth year is leap year add 1 day
		if (!(new GregorianCalendar().isLeapYear(currentYear)) && day == 29 && month == 1) {
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return formatDate(cal.getTime(), dateFormat);
	}
	
	private boolean isActiveEnrollee(final EnrolleeShopDTO enrolleeShopDTO) {
		return !enrolleeShopDTO.getEnrolleeLookUpValue().equals("CANCEL")
				&& !enrolleeShopDTO.getEnrolleeLookUpValue().equals("ABORTED")
				&& (enrolleeShopDTO.getEffectiveEndDate().after(getLastDayOfCurrentMonth().getTime()));
	}
	
	private Map<String, Boolean> checkForAge(Date birthDate) {
		Calendar lastDayOfMonth = getLastDayOfCurrentMonth();
		int age = getAgeOnDate(birthDate, lastDayOfMonth.getTime());
		boolean isAgeMoreThan19 = (age >= 19);
		boolean isAgeMoreThan26 = (age >= 26);
		Map<String, Boolean> result = new HashMap<String, Boolean>();
		result.put(IS_AGE_MORE_THAN19, isAgeMoreThan19);
		result.put(IS_AGE_MORE_THAN26, isAgeMoreThan26);
		return result;
	}

	private int getAgeOnDate(Date birthDate, Date onDate) {
		Calendar dob = Calendar.getInstance();
		dob.setTime(birthDate);
		Calendar toDate = Calendar.getInstance();
		toDate.setTime(onDate);
		int age = toDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if (toDate.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
			age--;
		} else if (toDate.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
				&& toDate.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
			age--;
		}
		return age;
	}
}
