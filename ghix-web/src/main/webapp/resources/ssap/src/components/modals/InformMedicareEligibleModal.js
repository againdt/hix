import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Components

import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

// ActionCreators

import { hideModal } from '../../actions/actionCreators';

// Constants

const InformMedicareEligibleModal = props => {

    const {
        modal: {
            modalOwner,
            data: [
                modalData
            ],
            callbacks
        }
    } = props;

    const handleRightBtnClick = () => {
        callbacks.map(callback => {
            callback();
        });
    };

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__seeking__for__coverage">
                <h2>
                    Information About Medicare
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body txt-left">
                { modalData &&
                    modalData.map((member, i) => {

                        return(
                            <div className='' key={`${member}_${i}`}>
                                <p>
                                    <strong>{member}&nbsp;</strong>looks like you may be eligible for Medicare.
                                    If you have Medicare, you cannot enroll in Exchange coverage
                                </p>
                                <p>
                                    If someone has a Medicare coverage, he or she cannot enroll in Marketplace coverage.
                                </p>
                            </div>
                        );
                    })
                }
                <div className='modal__controls'>
                    <Button
                        className='modal-cancel-btn usa-button-secondary'
                        onClick={handleHideClick}
                    >
                        CLOSE
                    </Button>
                    <Button
                        className='modal-confirm-btn'
                        onClick={handleRightBtnClick}
                    >
                        Ok
                    </Button>
                </div>
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(InformMedicareEligibleModal);
