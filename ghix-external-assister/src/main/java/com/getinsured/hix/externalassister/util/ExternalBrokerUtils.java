package com.getinsured.hix.externalassister.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public final class ExternalBrokerUtils {
	
	private static Gson gsonStatic;

	 /**
	  * 
	  * @param e
	  * @param maxLines
	  * @return
	  */
	 public static String shortenedStackTrace(Exception e, int maxLines) {
		 if(e==null){
			 return "";
		 }
	     StringWriter writer = new StringWriter();
	     e.printStackTrace(new PrintWriter(writer));
	     String[] lines = writer.toString().split("\n");
	     StringBuilder sb = new StringBuilder();
	     for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
	         sb.append(lines[i]).append("\n");
	     }
	     return sb.toString();
	 }
	 
	 public static Gson enrollmentGson()
		{
			if(gsonStatic == null)
			{
				GsonBuilder builder = new GsonBuilder();
				builder.serializeNulls();
				builder.disableHtmlEscaping();
				builder.setDateFormat(ExternalBrokerConstants.JSON_DATE_FORMAT);
				gsonStatic = builder.create();
			}
			return gsonStatic;
		}
	 
	 /**@param e
		 * @return boolean
		 * the below is takes input as object type (e) like (Integer, String, Character, List<String>, List<Integer>...etc)
		 * and return true when the input object is not null/empty/"null" else return false"
		 **/
		public static <T> boolean isNotNullAndEmpty(T e){
			boolean isNotNUll=false;
			if((e!=null) && !(e.toString().isEmpty()) && !(e.toString().equalsIgnoreCase("null"))){
				isNotNUll=true;
			}
			return isNotNUll;
		}
	

}
