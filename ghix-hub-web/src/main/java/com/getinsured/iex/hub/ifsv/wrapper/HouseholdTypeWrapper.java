package com.getinsured.iex.hub.ifsv.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.HouseholdType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.VerificationDetailType;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.HouseholdType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class HouseholdTypeWrapper implements JSONAware{
	
	private HouseholdType householdType;
	
	private String income;
	private List<VerificationDetailTypeWrapper> applicantVerification;
	private List<VerificationDetailTypeWrapper> dependentVerification;
	
	private static final String INCOME_KEY = "Income";
	private static final String APPLICANT_KEY = "ApplicantVerification";
	private static final String DEPENDENT_KEY = "DependentVerification";
	
	public HouseholdTypeWrapper(HouseholdType householdType){

		if(householdType.getIncome() != null){
			this.income = householdType.getIncome().getIncomeAmount().getValue().toString();
		}
		VerificationDetailTypeWrapper wrapper = null;
		for(VerificationDetailType applicant : householdType.getApplicantVerification()){
			wrapper = new VerificationDetailTypeWrapper(applicant);
			this.getApplicantVerification().add(wrapper);
		}
		
		for(VerificationDetailType dependent : householdType.getDependentVerification()){
			wrapper = new VerificationDetailTypeWrapper(dependent);
			this.getDependentVerification().add(wrapper);
		}
	}

	public HouseholdType getHouseholdType() {
		return householdType;
	}

	public String getIncome() {
		return income;
	}

	public List<VerificationDetailTypeWrapper> getApplicantVerification() {
		
		if(this.applicantVerification == null){
			this.applicantVerification = new ArrayList<VerificationDetailTypeWrapper>();
		}
		
		return applicantVerification;
	}

	public List<VerificationDetailTypeWrapper> getDependentVerification() {
		
		if(this.dependentVerification == null){
			this.dependentVerification = new ArrayList<VerificationDetailTypeWrapper>();
		}
		
		return dependentVerification;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(INCOME_KEY,this.income);
		
		if(this.applicantVerification != null && !this.applicantVerification.isEmpty()){
			JSONArray applicantVerificationArray = new JSONArray();
			for(VerificationDetailTypeWrapper data : this.applicantVerification){
				applicantVerificationArray.add(data);
			}
			obj.put(APPLICANT_KEY,applicantVerificationArray);
		}
		
		
		if(this.dependentVerification != null && !this.dependentVerification.isEmpty()){
			JSONArray dependentVerificationArray = new JSONArray();
			for(VerificationDetailTypeWrapper data : this.dependentVerification){
				dependentVerificationArray.add(data);
			}
			obj.put(DEPENDENT_KEY,dependentVerificationArray);
		}
		return obj.toJSONString();
	}
	
}
