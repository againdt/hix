package com.getinsured.hix.planmgmt.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import au.com.bytecode.opencsv.CSVWriter;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Esignature;
import com.getinsured.hix.model.Formulary;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.PlanCompareReport;
import com.getinsured.hix.model.PlanHealthBenefit;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.Plan.EnrollmentAvail;
//import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.PlanCompareReport.IS_DELETED;
import com.getinsured.hix.model.PlanCompareReport.PlanReportStatus;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.repository.IFormularyRepository;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.EsignatureService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.getinsured.hix.util.TicketMgmtUtils;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerQHPController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerController.class);
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private IssuerService issuerService;
	@Autowired private UserService userService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private IFormularyRepository iFormularyRepository;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	@Autowired private EsignatureService esignatureService;
	@Autowired private GIMonitorService giMonitorService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	@Autowired private RoleService roleService;
	@Autowired private TicketMgmtUtils ticketMgmtUtils;
	
	@Value("#{configProp.uploadPath}") private String uploadPath;
	
	private static final String ISSUER_REP_ROLE = "ISSUER_REPRESENTATIVE";
	private static final String TICKET_CATEGORY = "Issues";
	private static final String TICKET_TYPE = "Issuer Problem";
	
	
	@SuppressWarnings(PlanMgmtConstants.STATIC_ACCESS)
	// Issuer Portal : Manage QHP plans
	@RequestMapping(value = "/issuer/manageqhp", method = { RequestMethod.GET,RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String manageQHP(@ModelAttribute(PlanMgmtConstants.PLAN_CLASS) Plan plan, Model model, HttpServletRequest manageQHPrequest) {
		try {
			// redirect suspended issuer representative to suspend screen
			if(issuerService.isIssuerRepSuspended()){
				return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
			}

			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
	
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			Integer startRecord = 0;
			int currentPage = 1;
			String selectedPlanLevel = PlanMgmtConstants.EMPTY_STRING;
			
			// Search attributes
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute(PlanMgmtConstants.STATUS_LIST, plan.getPlanStatuses());
			model.addAttribute("marketList", plan.getMarketList());
			model.addAttribute("levelList", plan.getLevelList());
			model.addAttribute("enrollmentAvailList", plan.getEnrollmentAvailList());
			List<Map<String, Integer>> planYearsList = planMgmtService.getPlanYears(PlanMgmtConstants.HEALTH, issuerObj.getId());
	
			// Search attributes
			manageQHPrequest.setAttribute(PlanMgmtConstants.ISSUERID, issuerObj.getId());
	
			if (manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER) != null) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));
			}
			/*if (manageQHPrequest.getParameter(PlanLevel.GOLD.toString()) != null) {
				manageQHPrequest.setAttribute(manageQHPrequest.getParameter(PlanLevel.GOLD.toString()), manageQHPrequest.getParameter(PlanLevel.GOLD.toString()));
			}
			if (manageQHPrequest.getParameter(PlanLevel.BRONZE.toString()) != null) {
				manageQHPrequest.setAttribute(manageQHPrequest.getParameter(PlanLevel.BRONZE.toString()), manageQHPrequest.getParameter(PlanLevel.BRONZE.toString()));
			}
			if (manageQHPrequest.getParameter(PlanLevel.CATASTROPHIC.toString()) != null) {
				manageQHPrequest.setAttribute(manageQHPrequest.getParameter(PlanLevel.CATASTROPHIC.toString()), manageQHPrequest.getParameter(PlanLevel.CATASTROPHIC.toString()));
			}
			if (manageQHPrequest.getParameter(PlanLevel.PLATINUM.toString()) != null) {
				manageQHPrequest.setAttribute(manageQHPrequest.getParameter(PlanLevel.PLATINUM.toString()), manageQHPrequest.getParameter(PlanLevel.PLATINUM.toString()));
			}
			if (manageQHPrequest.getParameter(PlanLevel.SILVER.toString()) != null) {
				manageQHPrequest.setAttribute(manageQHPrequest.getParameter(PlanLevel.SILVER.toString()), manageQHPrequest.getParameter(PlanLevel.SILVER.toString()));
			}
			*/
			// Parsing all plan levels
			if (manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL).isEmpty()) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.PLAN_LEVEL, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL));
				selectedPlanLevel = manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_LEVEL);
			}
			if (manageQHPrequest.getParameter(PlanMgmtConstants.MARKET) != null
					&& !manageQHPrequest.getParameter(PlanMgmtConstants.MARKET).equals(PlanMgmtConstants.EMPTY_STRING)) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.MARKET, manageQHPrequest.getParameter(PlanMgmtConstants.MARKET));
			}
			if (manageQHPrequest.getParameter(PlanMgmtConstants.STATUS) != null
					&& !manageQHPrequest.getParameter(PlanMgmtConstants.STATUS).equals(PlanMgmtConstants.EMPTY_STRING)) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.STATUS, manageQHPrequest.getParameter(PlanMgmtConstants.STATUS));
			}
			if (!StringUtils.isEmpty(manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY))) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.ENROLLMENTAVAILABILITY, manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENTAVAILABILITY));
			}
			if (manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENT) != null
					&& !manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENT).equals(PlanMgmtConstants.EMPTY_STRING)) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.ENROLLMENT, manageQHPrequest.getParameter(PlanMgmtConstants.ENROLLMENT));
			}
			
			if (manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null && !manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).equals("")) {			
 	 	 	 	Map<String, Integer> selectedYearMap = new HashMap<String, Integer>();
 	 	 	 	selectedYearMap.put(PlanMgmtConstants.PLAN_YEARS_KEY, Integer.parseInt(manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
 	 	 	 	model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, selectedYearMap);
 	 	 	 	LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(selectedYearMap.toString()));
 	 	 	 	manageQHPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (manageQHPrequest.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR).toString()));
			}else{
				if(planYearsList.size() > PlanMgmtConstants.ZERO){
 	 	 	 		model.addAttribute(PlanMgmtConstants.SELECTED_PLAN_YEAR, planYearsList.get(0));
 	 	 	 		LOGGER.debug("Fetching Plans for Year : " +  SecurityUtil.sanitizeForLogging(planYearsList.get(0).toString()));
 	 	 	 		manageQHPrequest.setAttribute(PlanMgmtConstants.FETCH_FOR_PLAN_YEAR, (planYearsList.get(0).get(PlanMgmtConstants.PLAN_YEARS_KEY)));
				}else{
 	 	 	 		return PlanMgmtConstants.ISSUER_MANAGEQHP;
				}
			}
	
			// Sorting attributes
			if (manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY) != null) {
				manageQHPrequest.setAttribute(PlanMgmtConstants.SORT_BY, manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY));
			}
			String sortOrder = (manageQHPrequest.getParameter(PlanMgmtConstants.SORT_ORDER) == null) ? PlanMgmtConstants.SORT_ORDER_ASC : ((manageQHPrequest.getParameter(PlanMgmtConstants.SORT_ORDER).equalsIgnoreCase(PlanMgmtConstants.SORT_ORDER_ASC) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC));
			sortOrder = (sortOrder.equals(PlanMgmtConstants.EMPTY_STRING)) ? PlanMgmtConstants.SORT_ORDER_ASC : sortOrder;
			
			String changeOrder = (manageQHPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER) == null) ? PlanMgmtConstants.FALSE_STRING : ((manageQHPrequest.getParameter(PlanMgmtConstants.CHANGE_ORDER).equalsIgnoreCase(PlanMgmtConstants.FALSE_STRING) ? PlanMgmtConstants.FALSE_STRING : PlanMgmtConstants.TRUE_STRING));
			sortOrder = (changeOrder.equals(PlanMgmtConstants.TRUE_STRING)) ? ((sortOrder.equals(PlanMgmtConstants.SORT_ORDER_DESC)) ? PlanMgmtConstants.SORT_ORDER_ASC : PlanMgmtConstants.SORT_ORDER_DESC) : sortOrder;
			
			manageQHPrequest.removeAttribute(PlanMgmtConstants.CHANGE_ORDER);
			manageQHPrequest.setAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
			manageQHPrequest.setAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			String param = manageQHPrequest.getParameter(PlanMgmtConstants.PAGE_NUMBER);
			if (param != null) {
				startRecord = (Integer.parseInt(param) - 1)
						* GhixConstants.PAGE_SIZE;
				currentPage = Integer.parseInt(param);
			}
			manageQHPrequest.setAttribute("startRecord", startRecord);
	
			// retrieving the plans
			List<Plan> planList = planMgmtService.getPlanList(manageQHPrequest, PlanMgmtService.HEALTH);
	
			/*// To persist the selected plan levels after plan search
			Map<PlanLevel, String> planLevels = plan.getLevelList();
			List<String> selectedPlanLevels = new ArrayList<String>();
			if(null != planLevels){
				for (String planLevel : planLevels.values()) {
					if (manageQHPrequest.getParameter(planLevel.toUpperCase()) != null) {
						selectedPlanLevels.add(planLevel.toUpperCase());
					}
				}
			}
			model.addAttribute("selectedPlanLevels", selectedPlanLevels);*/
	
			// template/view variables
			model.addAttribute("plans", planList);
			model.addAttribute(PlanMgmtConstants.STATUS, manageQHPrequest.getParameter(PlanMgmtConstants.STATUS));
			model.addAttribute(PlanMgmtConstants.PLAN_NUMBER, manageQHPrequest.getParameter(PlanMgmtConstants.PLAN_NUMBER));
			model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
			model.addAttribute("issuerCertStatusList", issuerObj.getIssuerStatuses());
			model.addAttribute(PlanMgmtConstants.MARKET, manageQHPrequest.getParameter(PlanMgmtConstants.MARKET));
			model.addAttribute("pendingStatus", planMgmtService.ISSUER_VERIFICATION_STATUS_PENDING);
			model.addAttribute("certifyStatus", planMgmtService.STATUS_CERTIFIED);
			model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
			model.addAttribute(PlanMgmtConstants.SORT_ORDER, sortOrder);
			model.addAttribute(PlanMgmtConstants.SORT_BY, manageQHPrequest.getParameter(PlanMgmtConstants.SORT_BY));
			model.addAttribute(PlanMgmtConstants.SORT_ORDER_EXCEPTFIRSTCOLUMN, sortOrder);
			model.addAttribute("displayDtFmt", GhixConstants.DISPLAY_DATE_FORMAT);
			model.addAttribute(PlanMgmtConstants.PLAN_YEARS_LIST, planYearsList);
			model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));
			model.addAttribute("selectedPlanLevel", selectedPlanLevel);
			
			return PlanMgmtConstants.ISSUER_MANAGEQHP;
		} catch (Exception e) {
			LOGGER.error("Exception in manageQHP ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_QHP_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Plan details
	@RequestMapping(value = "/issuer/qhp/plandetails/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_PLAN_DETAILS)
	public String qhpPlanDetails(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				model.addAttribute("acturialValueCertificate", plan.getPlanHealth().getActurialValueCertificate());
				try {
					String enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, plan.getEnrollmentAvail()));

					Calendar cal = TSCalendar.getInstance();
					Date today = cal.getTime();
					if (!plan.getEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())
							&& (plan.getEnrollmentAvailEffDate().compareTo(today) >= 0)) {
						enrollmentAvial = plan.getEnrollmentAvailList().get(Enum.valueOf(EnrollmentAvail.class, EnrollmentAvail.NOTAVAILABLE.toString()));
					}
					model.addAttribute("enrollAvailability", enrollmentAvial);

					Formulary formulary = new Formulary();
					if (StringUtils.isNotBlank(plan.getFormularlyId())) {
						formulary = iFormularyRepository.findById(Integer.parseInt(plan.getFormularlyId()));
					}
					model.addAttribute("formulary", formulary);
					if(plan.getEhbPremiumFraction() != null){
//						Integer ehbPremiumFraction =  (Integer)Math.round(plan.getEhbPremiumFraction() * 100);
						model.addAttribute("ehbPremiumFraction", plan.getEhbPremiumFraction());
					}
					
					// HIX-31861 Code changes
					model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
					model.addAttribute(PlanMgmtConstants.FUTURE_DATE, plan.getFutureErlAvailEffDate());
					model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));

					Date sysDate = new TSDate();
					if (plan.getFutureEnrollmentAvail() == null) {
						model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, "");
					} else if (plan.getFutureErlAvailEffDate() != null && plan.getFutureErlAvailEffDate().compareTo(sysDate) > 0) {
						if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.AVAILABLE.toString())) {
							model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.AVAILABLE);
						} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.NOTAVAILABLE.toString())) {
							model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.NOTAVAILABLE);
						} else if (plan.getFutureEnrollmentAvail().equalsIgnoreCase(EnrollmentAvail.DEPENDENTSONLY.toString())) {
							model.addAttribute(PlanMgmtConstants.FUTURE_STATUS, PlanMgmtConstants.DEPENDENTSONLY);
						}
					}
					// HIX-31861 end changes

					if(plan.getServiceAreaId() != null){
						String serfServiceAreaId = issuerService.getSerfServiceAreaIdById(plan.getServiceAreaId());
						model.addAttribute(PlanMgmtConstants.SERVICE_AREA_ID, serfServiceAreaId);
					}
					
					model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
					model.addAttribute("levelList", plan.getLevelList());
					
				} catch (Exception e) {
					LOGGER.error("Error in Enrollment Availability Value / Effective Data" + e.getMessage());
				}
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			return "issuer/qhp/plandetails";
		} catch (Exception e) {
			LOGGER.error("Exception in qhpPlanDetails ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_PLANDETAILS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Download QHP plan Benefits
	@RequestMapping(value = "/issuer/qhp/viewbenefits/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String qhpBenefitsRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (planId != null && !planId.isEmpty()) {
				List<Map<String, Object>> planBenefitRateHistory = planMgmtService.loadPlanBenefitsHistory(Integer.parseInt(planId), PlanMgmtConstants.ISSUER_ROLE);
				// fetching plan details
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.STATUS_HISTORY_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute("data", planBenefitRateHistory);
				if (planBenefitRateHistory != null && !planBenefitRateHistory.isEmpty()) {
					Map<String, Object> lastUpdatedMap = planBenefitRateHistory.get(0);
					model.addAttribute(PlanMgmtConstants.LAST_UPDATED, lastUpdatedMap.get(PlanMgmtConstants.UPDATED));
				}

				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				model.addAttribute(PlanMgmtConstants.TIMEZONE,TimeZone.getTimeZone(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE)));

				// Code changes for HIX-29035
				Calendar cal = TSCalendar.getInstance();
				Date today = cal.getTime();
				SimpleDateFormat ft = new SimpleDateFormat ("MMddyyyy");
				if(today.compareTo(plan.getStartDate()) < 0){
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(plan.getStartDate()));
				}
				else if(today.compareTo(plan.getEndDate()) > 0){
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(plan.getEndDate()));
				} else {
					model.addAttribute(PlanMgmtConstants.PLAN_DIS_DATE, ft.format(today));
				}
				model.addAttribute(PlanMgmtConstants.STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));
				// End changes
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			return "issuer/qhp/viewbenefits";
		} catch (Exception e) {
			LOGGER.error("Exception in qhpBenefitsRates ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHP_BENEFITS_RATES_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	@RequestMapping(value = "/issuer/qhp/dowloadqhpbenefits/{id}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public ModelAndView downloadQhpBenefit(HttpServletResponse response, @PathVariable("id") Integer planId) {
		// return null for suspended issuer representative
		if(issuerService.isIssuerRepSuspended()){
			return null;
		}
		
		FileInputStream fis = null;
		CSVWriter writer = null;
		
		try {
			Plan plan = planMgmtService.findPlanByIdAndIssuerId(planId, issuerService.getIssuerId());
			List<PlanHealthBenefit> plansData = planMgmtService.getPlanHelthBenefit(plan.getId());
			if (plansData != null) {
				String constname;
				String name;
				Map<String, String> healthBenefitMap = new HashMap<String, String>();
				
				for(PlanmgmtConfiguration.PlanmgmtConfigurationEnum pcE : PlanmgmtConfiguration.PlanmgmtConfigurationEnum.values()){
					 constname = pcE.name().endsWith(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY)?pcE.name().replace(PlanMgmtConstants.PLANMGMT_DUPLICATE_KEY,PlanMgmtConstants.EMPTY_STRING):pcE.name();
					 name = pcE.getValue();
					if(name.contains(PlanMgmtConstants.PLANMGMT_HEALTHBENEFITS)){
						healthBenefitMap.put(constname,DynamicPropertiesUtil.getPropertyValue(name));
					}
				}
				String fileName = plan.getId() + PlanMgmtConstants.PH_BENEFIT_FILE_NAME + TimeShifterUtil.currentTimeMillis() + ".csv";
				String filePath = uploadPath + File.separator + fileName;
				boolean isValidPath = planMgmtUtils.isValidPath(filePath);
				if(isValidPath){
					writer = new CSVWriter(new FileWriter(filePath), ',');
					writer.close();
					response.setHeader("Content-Disposition", "attachment;filename=" + fileName);
					response.setContentType("application/octet-stream");
					File file = new File(filePath);
					fis = new FileInputStream(file);
					FileCopyUtils.copy(fis, response.getOutputStream());
					if(file.exists()){
						file.delete();
					}
					fis.close();
					LOGGER.info("File downloaded successfully!!!");
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
			}
			return null;
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.DOWNLOAD_QHPBENEFIT_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}finally{
			 IOUtils.closeQuietly(fis);
			 try {
					if(writer != null){
						writer.close();
					}
			} catch (IOException e) {
					//Ignoring
			}
		}
	}

	// Issuer Portal : QHP plan Rates
	@RequestMapping(value = "/issuer/qhp/viewrates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String viewRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String decryptPlanId = null;
		Integer planId = null;
		try {
			decryptPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptPlanId);
			if (planId != null && planId > 0) {
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(planId, issuerService.getIssuerId());	
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				List<Map<String, Object>> logData = planMgmtService.loadPlanRateGroupByEffectiveDate(plan.getId(), PlanMgmtConstants.ISSUER_ROLE);
				model.addAttribute("rates", logData);
				model.addAttribute("recordSize", logData.size());
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));				
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			return "issuer/qhp/viewrates";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.VIEWRATES_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	// Issuer Portal : QDP plan Rates
	@RequestMapping(value = "/issuer/qdp/viewrates/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String viewQDPRates(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		
		Integer planId = null;
		String decryptedPlanId = null;
		try {
			decryptedPlanId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			planId = Integer.parseInt(decryptedPlanId);
			if (planId != null && planId > 0) {
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(planId, issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				List<Map<String, Object>> logData = planMgmtService.loadPlanRateGroupByEffectiveDate(plan.getId(), PlanMgmtConstants.ISSUER_ROLE);
				model.addAttribute("rates", logData);
				model.addAttribute("recordSize", logData.size());
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));				
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qdp screen
				return PlanMgmtConstants.ISSUER_MANAGEQDP;
			}
			return "issuer/qdp/viewrates";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEWQDPRATES_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}
	
	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Download QHP plan history
	@RequestMapping(value = "/issuer/qhp/qhphistory/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String qhpHistory(Model model, @PathVariable("encPlanId") String encPlanId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				// fetching plan details
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				// fetching the plan status history
				List<Map<String, Object>> planStatusHistory = planMgmtService.loadPlanStatusHistory(plan.getId());
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.STATUS_HISTORY_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.PLAN_STATUS_HISTORY, planStatusHistory);
				
				if (planStatusHistory != null && !planStatusHistory.isEmpty()) {
					Map<String, Object> lastUpdatedMap = planStatusHistory.get(0);
					model.addAttribute(PlanMgmtConstants.LAST_UPDATED, lastUpdatedMap.get(PlanMgmtConstants.LAST_UPDATE_TIMESTAMP));
					model.addAttribute("currentStatus", lastUpdatedMap.get(PlanMgmtConstants.STATUS));
					model.addAttribute("enrollmentStatus", lastUpdatedMap.get(PlanMgmtConstants.ENROLLMENT_AVAIL));
				}
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			return "issuer/qhp/viewhistory";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPHISTORY_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Verify QHP plan
	@RequestMapping(value = "/issuer/qhp/verifyplan/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qhpVerifyPlan(Model model, @PathVariable("encPlanId") String encPlanId,  HttpServletRequest request,RedirectAttributes redirectAttributes) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		String planId = null;
		try {
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_FIRST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_FIRST_NAME);
			}			
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_LAST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_LAST_NAME);
			}
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not // issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if (planId != null && !planId.isEmpty()) {
				if (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null) {
					redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
				}
				Date sysDate = new TSDate();
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				model.addAttribute(PlanMgmtConstants.PLAN_NAME, plan.getName());
				model.addAttribute(PlanMgmtConstants.PLAN_ID, plan.getId());
				model.addAttribute("moduleName", ModuleUserService.ISSUER_MODULE);
				model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
				model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getFirstName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_FIRST_NAME, userService.getLoggedInUser().getFirstName());
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getLastName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_LAST_NAME, userService.getLoggedInUser().getLastName());
			}
			return "issuer/qhp/verifyplan";
		} catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPVERIFYPLAN_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}

	/*
	 * @Suneel: Secured the method with permission
	 */
	// Issuer Portal : Verify QHP plan : Submit form
	@RequestMapping(value = "/issuer/qhp/verifyplan", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qhpIssuerVerifyPlan(@ModelAttribute("Esignature") Esignature esignatureObj, BindingResult result, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) 
	{
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try 
		{
		if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not issuer
			return PlanMgmtConstants.ISSUER_LOGIN_PATH;
		} else {
			//server side validation
			Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
			 Set<ConstraintViolation<Esignature>> violations = validator.validate(esignatureObj, Esignature.VerifyPlan.class);
			 if(violations != null && !violations.isEmpty()){
				 throw new GIRuntimeException(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				 }
			// Save the eSignature
			try {
				esignatureObj.setUser(userService.getLoggedInUser());
			} catch (InvalidUserException e) {
				LOGGER.error("Invalid User : " + e.getMessage());
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			Date sysDate = new TSDate();
			esignatureObj.setModuleName(ModuleUserService.PLAN_MODULE);
			esignatureObj.setEsignDate(sysDate);
			esignatureObj.setStatements(request.getParameter("statements"));

			try {
				// Saving the esignature
				esignatureService.saveEsignature(esignatureObj);
			} catch (Exception e) {
				LOGGER.error("Error while saving the esignature. Exception in : "
						+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
				if(esignatureObj.getRefId() != null){
					String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(esignatureObj.getRefId()));
					return "redirect:/issuer/qhp/verifyplan/"+encryptedPlanId;
				}else{
					return "redirect:/issuer/manageqhp";
				}
				
			}

			// after saving the esignature, update the issuer verification status of the qhp plan to VERIFIED
			String isBulkAction = request.getParameter("isBulkAction");
			String planIds = request.getParameter("planIds");
			if (request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR) != null) {
				redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
			}
			if(isBulkAction != null && isBulkAction.equals("YES") && StringUtils.isNotEmpty(planIds)){
				String[] planIdsArry = planIds.split(",");
				
				for(int i=0; i<planIdsArry.length; i++){
					String planId = planIdsArry[i];
					Plan planToVerify = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId.trim()), issuerService.getIssuerId());
					planToVerify.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED);
					try {
						planMgmtService.savePlan(planToVerify);
		
					} catch (Exception e) {
						LOGGER.error("Error while updating plan. Exception in : "
								+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
						return "redirect:/issuer/manageqhp";
					}
				}
				
			}else{				
				Plan planToVerify = planMgmtService.findPlanByIdAndIssuerId(esignatureObj.getRefId(), issuerService.getIssuerId());
				planToVerify.setIssuerVerificationStatus(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED);
				try {
					
					planMgmtService.savePlan(planToVerify);
	
				} catch (Exception e) {
					LOGGER.error("Error while updating plan. Exception in : "
							+ this.getClass() + PlanMgmtConstants.EXCEPTION_STRING_2 + e.getMessage());
					String encryptedPlanId = ghixJasyptEncrytorUtil.encryptStringByJasypt(Integer.toString(esignatureObj.getRefId()));
					return "redirect:/issuer/qhp/verifyplan/" + encryptedPlanId;
				}
			}
			

			return "redirect:/issuer/manageqhp";
		}
	} catch (Exception e) {		
		throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPISSUERVERIFYPLAN_EXCEPTION.getCode(),
				null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
	}}

	
	
	/*	New Method for Generate Plan Report		*/
	@RequestMapping(value = "/issuer/qhp/generatePlanReport", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	@ResponseBody
	public String addToGeneratePlanReport(Model model, HttpServletRequest request, @RequestParam(value = "planIdList", required = false) String strPlanIdList) {
		String successString = null;
		List<Integer> planIdList = new ArrayList<Integer>();
		try{
			AccountUser user = userService.getLoggedInUser();
			Issuer issuerObj = issuerService.getIssuer(user);
			
			LOGGER.debug("Generate Report for: " +  SecurityUtil.sanitizeForLogging(strPlanIdList));
			String planIDArray[] = strPlanIdList.split(PlanMgmtConstants.COMMA_STRING);
			for(int i =0; i< planIDArray.length; i++){
				if(planIDArray[i] != null){
					planIdList.add(Integer.parseInt(planIDArray[i]));
				}
			}
			String insuranceType = planMgmtService.getInsuranceType(planIdList);
			LOGGER.debug("insuranceType: " +  SecurityUtil.sanitizeForLogging(insuranceType));
			
			PlanCompareReport planCompareReport = new PlanCompareReport();
			planCompareReport.setIssuer(issuerObj);
			planCompareReport.setPlanIds(strPlanIdList);
			if(insuranceType.equalsIgnoreCase(PlanMgmtConstants.HEALTH)){
				planCompareReport.setPlanReportType("Q");
			}else if (insuranceType.equalsIgnoreCase(PlanMgmtConstants.DENTAL)) {
				planCompareReport.setPlanReportType("S");
			}
			planCompareReport.setPlanReportStatus(String.valueOf(PlanReportStatus.INQUEUE));
			planCompareReport.setIsDeleted(String.valueOf(IS_DELETED.N));
			planCompareReport.setCreatedBy(user.getId());
			planCompareReport.setCreationTimestamp(new TSDate());
			planCompareReport.setLastUpdateTimestamp(new TSDate());
			planMgmtService.savePlanReportInfo(planCompareReport);
			
			if(insuranceType.equalsIgnoreCase(PlanMgmtConstants.HEALTH)){
				successString = PlanMgmtConstants.QHP_GENERATE_PALN_REPORT_MESSAGE + PlanMgmtConstants.DASH_SIGN + PlanMgmtConstants.SUCCESS;	
			}else{
				successString = PlanMgmtConstants.SADP_GENERATE_PALN_REPORT_MESSAGE + PlanMgmtConstants.DASH_SIGN + PlanMgmtConstants.SUCCESS;
			}
			return successString;
		}catch(Exception ex){
			LOGGER.error("Exception @addToGeneratePlanReport : ", ex);
			giMonitorService.saveOrUpdateErrorLog(PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_LIST_EXCEPTION.toString(), new TSDate(), Exception.class.getName(),ex.getStackTrace().toString(), null);
			return PlanMgmtConstants.ERROR_WHILE_ADDING_IN_QUEUE;
		}
	}
	
	
	@RequestMapping(value = "/issuer/qhp/verifyplansBulk", method = RequestMethod.POST)
	@PreAuthorize(PlanMgmtConstants.VERIFY_PLAN)
	public String qhpVerifyPlansFromBulkOperationSymbol(Model model, @RequestParam(value = "planIdList", required = false) String strPlanIdList,HttpServletRequest request, RedirectAttributes redirectAttributes) {
		try {
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_FIRST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_FIRST_NAME);
			}			
			if(request.getSession().getAttribute(PlanMgmtConstants.VALID_LAST_NAME) != null) {
				request.getSession().removeAttribute(PlanMgmtConstants.VALID_LAST_NAME);
			}
			if (!(issuerService.isIssuerRepLoggedIn())) { // if logged in user is not // issuer
				return PlanMgmtConstants.ISSUER_LOGIN_PATH;
			}
			if(request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR)!= null){
				redirectAttributes.addAttribute(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR, request.getParameter(PlanMgmtConstants.GET_SELECTED_PLAN_YEAR));
			}
			
			if (strPlanIdList != null && !strPlanIdList.isEmpty()) {
				List<Integer> planIdList = new ArrayList<Integer>();
				
				String planIDArray[] = strPlanIdList.split(PlanMgmtConstants.COMMA_STRING);
				for(int i =0; i< planIDArray.length; i++){
					if(planIDArray[i] != null){
						planIdList.add(Integer.parseInt(planIDArray[i]));
					}
				}
				
				List<Plan> plans = planMgmtService.getPlans(planIdList);
				StringBuilder planNumbers = new StringBuilder();
				StringBuilder planIds = new StringBuilder();
				for(int i=0; i < plans.size(); i++){
					Plan plan = plans.get(i);
					if(plan.getIssuerVerificationStatus().equalsIgnoreCase(PlanMgmtService.ISSUER_VERIFICATION_STATUS_VERIFIED)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.IS_VERIFIED, "YES");
						return "redirect:/" + PlanMgmtConstants.ISSUER_MANAGEQHP;
					}else if(!plan.getStatus().equalsIgnoreCase(PlanMgmtService.STATUS_CERTIFIED)){
						redirectAttributes.addFlashAttribute(PlanMgmtConstants.IS_CERTIFIED, "NO");
						return "redirect:/" + PlanMgmtConstants.ISSUER_MANAGEQHP;
					}else{
						planNumbers.append(plan.getIssuerPlanNumber());
						planIds.append(plan.getId());
						if(i != plans.size()-1){
							planNumbers.append(", ");
							planIds.append(", ");
						}
					}
				}
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				Date sysDate = new TSDate();
				model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				model.addAttribute(PlanMgmtConstants.PLAN_COUNT, plans.size());
				model.addAttribute(PlanMgmtConstants.PLAN_NUMBERS, planNumbers);
				model.addAttribute(PlanMgmtConstants.PLAN_ID_LIST, planIds);
				model.addAttribute(PlanMgmtConstants.IS_BULK_ACTION, "YES");
				model.addAttribute(PlanMgmtConstants.MODULENAME, ModuleUserService.ISSUER_MODULE);
				model.addAttribute(PlanMgmtConstants.CURR_DATE, DateUtil.dateToString(sysDate, GhixConstants.REQUIRED_DATE_FORMAT));
			} else {
				// if plan id is not given in the request, then redirect to manage qhp screen
				return PlanMgmtConstants.ISSUER_MANAGEQHP;
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getFirstName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_FIRST_NAME, userService.getLoggedInUser().getFirstName());
			}
			if(StringUtils.isNotBlank(userService.getLoggedInUser().getLastName())) {
				request.getSession().setAttribute(PlanMgmtConstants.VALID_LAST_NAME, userService.getLoggedInUser().getLastName());
			}
			return "issuer/qhp/verifyplan";
		} catch (Exception e) {
			LOGGER.error("Exception in qhpVerifyPlan ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPVERIFYPLAN_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
	}
	
	
	@RequestMapping(value = "/issuer/qhp/createTicket/{encPlanId}", method = RequestMethod.GET)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String createQHPTicket(Model model, @PathVariable("encPlanId") String encPlanId) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		String planId = null;
		try {
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);			
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep	
				String email = userService.getLoggedInUser().getEmail();
				Plan plan = planMgmtService.findPlanByIdAndIssuerId(Integer.parseInt(planId), issuerService.getIssuerId());
				if(null != plan) {
					model.addAttribute(PlanMgmtConstants.PLAN_OBJ, plan);
				}
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				if(null != issuerObj) {
					model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
				}
				IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByEmail(email);
				if(null != issuerRepObj) {
					model.addAttribute(PlanMgmtConstants.ISSUER_REP_OBJ, issuerRepObj);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error occurred in IssuerController.createQHPTicket():: ",e);
		}
		return "planmgmt/issuer/qhp/createTicket";		
	}
	
	
	@RequestMapping(value = "/planmgmt/issuer/qhp/save", method = RequestMethod.POST)
	@PreAuthorize("hasPermission(#model, 'ISSUER_PORTAL_MANAGE_PLAN')")
	public String saveQHPCreateTicket(Model model, HttpServletRequest request) throws InvalidUserException, GIException {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		TkmTicketsRequest tkmRequest = new TkmTicketsRequest();		
		Integer userId = null;
		String planId = null;
		String encPlanId = null;
		String encIssuerId = null;
		String issuerId = null;
		try {	
			AccountUser user =userService.getLoggedInUser();
			String email = userService.getLoggedInUser().getEmail();
			userId = user.getId();
			String priority = request.getParameter("priority");
			String description = request.getParameter("description");
			encPlanId = request.getParameter("planId");
			String planName = request.getParameter("planName");
			planId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encPlanId);
			tkmRequest.setCategory(TICKET_CATEGORY);
			tkmRequest.setLastUpdatedBy(userId);			
			tkmRequest.setType(TICKET_TYPE);
			tkmRequest.setUserRoleId(roleService.findRoleByName(ISSUER_REP_ROLE).getId());
			tkmRequest.setDescription(description);
			tkmRequest.setModuleName(ISSUER_REP_ROLE);
			tkmRequest.setPriority(priority);
			tkmRequest.setSubject(planId + " " + planName);	
			tkmRequest.setRequester(userId);
			tkmRequest.setComment(planId + " " + planName);
			tkmRequest.setCreatedBy(userId);
			encIssuerId = request.getParameter("issuerId");
			issuerId = ghixJasyptEncrytorUtil.decryptStringByJasypt(encIssuerId);
			Issuer issuerObj = issuerService.getIssuerById(Integer.parseInt(issuerId));
			if(null != issuerObj) {
				IssuerRepresentative issuerRepObj = iIssuerRepresentativeRepository.findByEmail(email);
				if(null != issuerRepObj) {
					tkmRequest.setModuleId(issuerRepObj.getId());					
				}
			}
						
			TkmTickets tkmResponse = ticketMgmtUtils.createNewTicket(tkmRequest);
			LOGGER.debug("Ticket ID is " + (tkmResponse!=null ? SecurityUtil.sanitizeForLogging(tkmResponse.getNumber()) : "null"));
		}catch(InvalidUserException iui) {
			LOGGER.error("Invalid User IssuerController.saveQHPCreateTicket():: ",iui);
		}catch(Exception e) {
			LOGGER.error("Exception Occurred in IssuerController.saveQHPCreateTicket():: ", e);
		}		
		
		return "redirect:/issuer/qhp/plandetails/"+ghixJasyptEncrytorUtil.encryptStringByJasypt(planId);
	}
	
	
	@RequestMapping(value="/planmgmt/issuer/reports/qhpratebenefitreport", method = RequestMethod.GET)
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String qhpRateBenefitReport(Model model, HttpServletRequest request){
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
			
		try{
			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			LOGGER.info("Admin :: View Issuer History");
			List<Map<String, String>> rateBenefitReportData = issuerService.loadRateBenefitReportList(request, issuerObj.getId(), PlanMgmtConstants.QHP);
			model.addAttribute(PlanMgmtConstants.ISSUER_OBJ, issuerObj);
			model.addAttribute("ratebenefits", rateBenefitReportData);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
			model.addAttribute(PlanMgmtConstants.EXCHANGE_TYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE));
		}catch(Exception ex){
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME, ex, null, Severity.HIGH);
		}
		return "planmgmt/issuer/reports/qhpratebenefitreport";
	}
	
	
	
	
	public TicketMgmtUtils getTicketMgmtUtils() {
		return ticketMgmtUtils;
	}
	

}
