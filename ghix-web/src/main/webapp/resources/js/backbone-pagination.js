(function (backbone) {
	/**
	 * @class
	 * Pagination
	 */
	backbone.Pagination = {
		/**  how many items to show per page */
		perPage :3,
		
		/** page to start off on */
		page : 1,
	
		/**
		 *
		 */
		initialize: function(){
			this.benchmarkplanId="";
			this.sortColumn = "";
			this.sortColumnList = [];
			var sortDirectionList = [];
			this.sortDirection = "desc";
			this.lastSortColumn = "";
			this._sorted = false;
			
			this.fieldFilterRules = [];
			this.lastFieldFilterRules = [];

			this.filterFields = "";
			this.filterExpression = "";
			this.lastFilterExpression = "";

			this.showCompare = false;
			this.compareModels=[];
			
			this.cartSummary = [];
			this.cartSummary.fetched = false;
			this.cartSummary.planCount = 0;
			this.cartSummary.platinum = 0;
			this.cartSummary.gold = 0;
			this.cartSummary.silver = 0;
			this.cartSummary.bronze = 0;
			this.cartSummary.platinumLevel = 0;
			this.cartSummary.goldLevel = 0;
			this.cartSummary.silverLevel = 0;
			this.cartSummary.bronzeLevel = 0;
			this.cartItems = [];
			
			this.issuerList = [];
			this.issuerCount = [];
			
			this.totalPlanCount = 0;
			this.filteredPlanCount = 0;
			
			this.prevItem = 0;
			this.nextItem = 0;
			this.start = 0;
			this.stop = 0;
		},
		
		nextPage : function () {
			var self = this;
			
			self.page = ++self.page;
			self.pager();
		},
		
		previousPage : function () {
			var self = this;
			
			self.page = --self.page || 1;
			self.pager();
		},
		
		goTo : function (page) {
			var self = this;
			
			self.page = parseInt(page,10);
			self.pager();
		},
		
		howManyPer : function (perPage) {
			var self = this;
			var lastPerPage = self.perPage;
			self.perPage = parseInt(perPage, 10);
			self.perPage = Math.ceil( ( lastPerPage * ( self.page - 1 ) + 1 ) / perPage);;
			self.pager();
		},
		
		setSort : function (column, direction) {
			var self = this;
			if(column !==undefined && direction !==undefined){
				self.lastSortColumn = this.sortColumn;
				self.sortColumn = column;
				self.sortDirection = direction;
				self.pager();
				self.info();	
			}
		},
		
		addToCompare: function (planId) {
			if($.inArray(planId, this.compareModels)== -1)
				this.compareModels.push(planId);
		},
		
		removeCompare: function (planId,viewType) {
			var self = this;
			var i=0;
			_.each(self.compareModels, function(remId){
				if(remId == planId){
					self.compareModels.splice(i,1);
				}
					i++;
			});
			/*in compare view*/
			if(viewType == 'cview'){
				if(self.compareModels.length==0)
					self.showCompare=false;
				else	
					self.showCompare=true;
				
				self.pager();	
			}
		},
		
		// setFieldFilter is used to filter each value of each model
		// according to `rules` that you pass as argument.
		// Example: You have a collection of books with 'release year' and 'author'.
		// You can filter only the books that were released between 1999 and 2003
		// And then you can add another `rule` that will filter those books only to
		// authors who's name start with 'A'.
		setFieldFilter: function ( fieldFilterRules ) {
			if( !_.isEmpty( fieldFilterRules ) ) {
				this.lastFieldFilterRules = this.fieldFilterRules;
				this.fieldFilterRules = fieldFilterRules;
				this.pager();
				this.info();
			}
		},
		
		resetFieldFilter: function () {
			this.fieldFilterRules = [];
			this.pager();
			this.info();
		},
		
		setCartSummary: function ( planId,planLevel ) {
			this.cartSummary.planCount++; 
			if(planId == ""){
				this.cartItems.push(planLevel);
				this.cartSummary[planLevel+"Level"]++;
			}else{
				this.cartItems.push(planId);
				this.cartSummary[planLevel]++;
			}
		},
		
		removeFromCart: function (planId,planLevel) {
			this.cartSummary.planCount--; 
			if(planId == ""){
				var index = jQuery.inArray(planLevel, this.cartItems);
				this.cartItems.splice(index, 1);
				this.cartSummary[planLevel+"Level"]--;
			}else{
				var index = jQuery.inArray(planId, this.cartItems);
				this.cartItems.splice(index, 1);
				this.cartSummary[planLevel]--;
			}
		},
		
		loadCartSummary: function ( cartCollectionSummary,cartItems ) {
			this.cartSummary.planCount = cartCollectionSummary.planCount;
			this.cartItems = cartItems;
			this.cartSummary.platinum = cartCollectionSummary.platinum;
			this.cartSummary.gold = cartCollectionSummary.gold;
			this.cartSummary.silver = cartCollectionSummary.silver;
			this.cartSummary.bronze = cartCollectionSummary.bronze;
			this.cartSummary.platinumLevel = cartCollectionSummary.platinumLevel;
			this.cartSummary.goldLevel = cartCollectionSummary.goldLevel;
			this.cartSummary.silverLevel = cartCollectionSummary.silverLevel;
			this.cartSummary.bronzeLevel = cartCollectionSummary.bronzeLevel;
		},
    
		// setFilter is used to filter the current model. After
		// passing 'fields', which can be a string referring to
		// the model's field or an array of strings representing
		// all of the model's fields you wish to filter by and
		// 'filter', which is the word or words you wish to 
		// filter by, pager() and info() will be called automatically.
		setFilter: function ( fields, filter ) {
			if( fields !== undefined && filter !== undefined ){
				this.filterFields = fields;
				this.lastFilterExpression = this.filterExpression;
				this.filterExpression = filter;
				this.pager();
				this.info();
			}
		},
		
		loadIssuerList: function (models) {
			var self = this;
			self.issuerList = [];
			self.issuerCount = [];
			_.each(models, function(model){
				if( jQuery.inArray(model.get("issuer"),self.issuerList) === -1 ) {
					self.issuerList.push(model.get("issuer"));
					self.issuerCount[model.get("issuer")+"Count"] = 1;
				}else{
					self.issuerCount[model.get("issuer")+"Count"]++;
				}
			});
		},
		
		pager : function () {
			var self = this;
			
			if(self.nextItem){
				self.start = self.start+1,
				self.stop  = self.stop+1;
				self.nextItem = 0;
			} else if(self.prevItem){
				self.start = self.start-1,
				self.stop  = self.stop-1;
				self.prevItem = 0;
			} else {
				self.start = (self.page-1)*this.perPage;
				self.stop  = self.start+self.perPage;
			}
			
			if (self.orgmodels === undefined) {
				self.orgmodels = self.models;
				self.totalPlanCount = self.orgmodels.length;
			}
			
			self.models = self.orgmodels;

			if(self.stop >= self.models.length && self.models.length > this.perPage){
				self.start = self.models.length - this.perPage;
				self.stop = self.models.length;
			}

			/*if (!_.isEmpty( this.sortColumnList)) { 
				self._sortList(self.models);
			}*/
			if (!_.isEmpty( this.sortColumnList)) { 
				self.models = self._sortBy(self.models, this.sortColumnList);
				this._sorted = true;
			}
			
			if (self.sortColumn) {
				self.models = self._sort(self.models, self.sortColumn, self.sortDirection);
			}

			// Check if field-filtering was set using setFieldFilter
			if ( !_.isEmpty( this.fieldFilterRules ) ) {
				self.models = self._fieldFilter(self.models, this.fieldFilterRules);
			}

			// Check if filtering was set using setFilter.
			if ( this.filterExpression !== "" ) {
				self.models = self._filter(self.models, this.filterFields, this.filterExpression);
			}
			
			//check whether compare items called or not.
			if ( this.showCompare == true ) {
				self.models = self.orgmodels;
				self.models = self._compare(self.models, this.compareModels);
			}
      
			// If the sorting or the filtering was changed go to the first page
			if ( this.lastSortColumn !== this.sortColumn || this.lastFilterExpression !== this.filterExpression || !_.isEqual(this.fieldFilterRules, this.lastFieldFilterRules) ) {
				self.start = 0;
				self.stop = self.start + this.perPage;
				self.currentPage = 1;

				this.lastSortColumn = this.sortColumn;
				this.lastFieldFilterRules = this.fieldFilterRules;
				this.lastFilterExpression = this.filterExpression;
			}
			
			// We need to save the sorted and filtered models collection
			// because we'll use that sorted and filtered collection in info().
			self.sortedAndFilteredModels = self.models;
			self.filteredPlanCount = self.sortedAndFilteredModels.length;
			self.loadIssuerList(self.models);

			self.reset(
				self.models.slice(self.start,self.stop)
			);
			
			//this.goTo(1);
		},
		
		_sortList:function(models){
			var self = this;
			for(var j=0;j<this.sortColumnList.length;j++){
				
				models = self._sort(models, this.sortColumnList[j], this.sortDirectionList[j]);
			}
			return models;
		},
		_sort : function (models, sort, direction) {
			models = models.sort(function(a,b) {
				var a = a.get(sort),
					b = b.get(sort);
					
				if (direction === 'desc') {
					if (a > b) {
						return -1;
					}
					
					if (a < b) {
						return 1;
					}
				}
				else {
					if (a < b) {
						return -1;
					}
					
					if (a > b) {
						return 1;
					}
				}
				
				return 0;
			});
			
			return models;
			
		},
		
		sortBy : function(sortAttributes){
			if (arguments.length) {
				this._sortAttributes = arguments;
			}
			this.models = this._sortBy(this.models, this._sortAttributes);
			this._sorted = true;
		},
		_sortBy : function(models,attributes){
			var attr, that = this;

			if (!attributes.length) {
				return this.models;
			}
			attr = attributes[0];
			// Base case
			if (attributes.length === 1) {
				return _(models).sortBy(function(model) {
					return model.get(attr);
				});
			}
			// Splits up models by sort attribute,
			// and then does a recursive sort on each group
			else {
				models = _(models).chain().sortBy(function(model) {
					return model.get(attr);
				}).groupBy(function(model) {
					return model.get(attr);
				}).toArray().value();

				attributes = _.last(attributes, attributes.length - 1);
				_(models).each(function(modelSet, index) {
					models[index] = that._sortBy(models[index], attributes);
				});
				return _(models).flatten();
			}
		},
			// The actual place where the collection is field-filtered.
		// Check setFieldFilter for arguments explicacion.
		_fieldFilter: function( models, rules ) {

			// Check if there are any rules
			if ( _.isEmpty(rules) ) {
				return models;
			}
			
			var filteredModels = [];

			// Iterate over each rule
			_.each(models, function(model){

				var should_push = true;
			
				// Apply each rule to each model in the collection
				_.each(rules, function(rule){

					// Don't go inside the switch if we're already sure that the model won't be included in the results
					if( !should_push ){
						return false;
					}
					if(!rule.value){
						return true;
					}
					if( _.isArray( rule.value ) && rule.value.length == 0){
						return true;
					}
					should_push = false;
				
					// The field's value will be passed to a custom function, which should
					// return true (if model should be included) or false (model should be ignored)
					if(rule.type === "function"){
						var f = _.wrap(rule.value, function(func){
							return func( model.get(rule.field) );
						});
						if( f() ){
							should_push = true;
						}

					// The field's value is required to be non-empty
					}else if(rule.type === "required"){
						if( !_.isEmpty( model.get(rule.field).toString() ) ) {
							should_push = true;
						}

					// The field's value is required to be greater tan N (numbers only)
					}else if(rule.type === "min"){
						if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							!_.isNaN( Number( rule.value ) ) &&
							Number( model.get(rule.field) ) >= Number( rule.value ) ) {
							should_push = true;
						}

					// The field's value is required to be smaller tan N (numbers only)
					}else if(rule.type === "max"){
						if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							!_.isNaN( Number( rule.value ) ) &&
							Number( model.get(rule.field) ) <= Number( rule.value ) ) {
							should_push = true;
						}

					// The field's value is required to be between N and M (numbers only)
					}else if(rule.type === "range"){
						if( !_.isNaN( Number( model.get(rule.field) ) ) &&
							_.isObject( rule.value ) &&
							!_.isNaN( Number( rule.value.min ) ) &&
							!_.isNaN( Number( rule.value.max ) ) &&
							Number( model.get(rule.field) ) >= Number( rule.value.min ) &&
							Number( model.get(rule.field) ) <= Number( rule.value.max ) ) {
							should_push = true;
						}

					// The field's value is required to be more than N chars long
					}else if(rule.type === "minLength"){
						if( model.get(rule.field).toString().length >= rule.value ) {
							should_push = true;
						}

					// The field's value is required to be no more than N chars long
					}else if(rule.type === "maxLength"){
						if( model.get(rule.field).toString().length <= rule.value ) {
							should_push = true;
						}

					// The field's value is required to be more than N chars long and no more than M chars long
					}else if(rule.type === "rangeLength"){
						if( _.isObject( rule.value ) &&
							!_.isNaN( Number( rule.value.min ) ) &&
							!_.isNaN( Number( rule.value.max ) ) &&
							model.get(rule.field).toString().length >= rule.value.min &&
							model.get(rule.field).toString().length <= rule.value.max ) {
							should_push = true;
						}

					// The field's value is required to be equal to one of the values in rules.value
					}else if(rule.type === "oneOf"){
						if( _.isArray( rule.value ) &&
							_.include( rule.value, model.get(rule.field) ) ) {
							should_push = true;
						}

					// The field's value is required to be equal to the value in rules.value
					}else if(rule.type === "equalTo"){
						if( rule.value === model.get(rule.field) ) {
							should_push = true;
						}

					// The field's value is required to match the regular expression
					}else if(rule.type === "pattern"){
						if( model.get(rule.field).toString().match(rule.value) ) {
							should_push = true;
						}

					//Unknown type
					}else{
						should_push = false;
					}

				});
				
				if( should_push ){
					filteredModels.push(model);
				}

			});
			self.filteredPlanCount = filteredModels.length;
			return filteredModels;
		},
    
		// The actual place where the collection is filtered.
		// Check setFilter for arguments explicacion.
		_filter: function ( models, fields, filter ) {
		
			//  For example, if you had a data model containing cars like { color: '', description: '', hp: '' },
			//  your fields was set to ['color', 'description', 'hp'] and your filter was set
			//  to "Black Mustang 300", the word "Black" will match all the cars that have black color, then
			//  "Mustang" in the description and then the HP in the 'hp' field.
			//  NOTE: "Black Musta 300" will return the same as "Black Mustang 300"

			// We accept fields to be a string or an array,
			// but if string is passed we need to convert it
			// to an array.
			
			var self = this;
			
			if( _.isString( fields ) ) {
				var tmp_s = fields;
				fields = [];
				fields.push(tmp_s);
			}
			
			
			
			// 'filter' can be only a string.
			// If 'filter' is string we need to convert it to 
			// a regular expression. 
			// For example, if 'filter' is 'black dog' we need
			// to find every single word, remove duplicated ones (if any)
			// and transform the result to '(black|dog)'
			if( filter === '' || !_.isString(filter) ) {
				return models;
			} else {
				filter = filter.match(/\w+/ig);
				filter = _.uniq(filter);
				var pattern = "(" + filter.join("|") + ")";
				var regexp = new RegExp(pattern, "igm");
			}
			
			var filteredModels = [];

			// We need to iterate over each model
			_.each( models, function( model ) {

				var matchesPerModel = [];
			
				// and over each field of each model
				_.each( fields, function( field ) {

					var value = model.get( field );

					if( value ) {
					
						// The regular expression we created earlier let's us to detect if a
						// given string contains each and all of the words in the regular expression
						// or not, but in both cases match() will return an array containing all 
						// the words it matched.
						var matchesPerField;
						
						matchesPerField = value.toString().match( regexp );
						
						matchesPerField = _.map(matchesPerField, function(match) {
							return match.toString().toLowerCase();
						});
						
						_.each(matchesPerField, function(match){
							matchesPerModel.push(match);
						});
						
					}

				});

				// We just need to check if the returned array contains all the words in our
				// regex, and if it does, it means that we have a match, so we should save it.
				matchesPerModel = _.uniq( _.without(matchesPerModel, "") );

				if(  _.isEmpty( _.difference(filter, matchesPerModel) ) ) {
					filteredModels.push(model);
				}
				
			});

			return filteredModels;
		},
		
		//multiple items Compare Functionality
		// arguments
		// models: initial collection of items
		//selPlans : array of selected items
		_compare: function( models, selPlans ) {

			// Check if there are any rules
			if ( _.isEmpty(selPlans) ) {
				return models;
			}

			var comparedModels = [];

			// Iterate over each rule
			_.each(models, function(model){
				// Apply each rule to each model in the collection
				_.each(selPlans, function(cmp){
						
					//if selected id matches with id from model, then push to comparedModels
					if(model.id == cmp.replace("compare_","")){
						comparedModels.push(model);
						
					}	
				});
			});
			return comparedModels;
		},
		
		info : function () {
			
			var self = this,
				info = {},
				totalRecords = (self.sortedAndFilteredModels) ? self.sortedAndFilteredModels.length : self.length,
				totalPages = Math.ceil(totalRecords/self.perPage);

			info = {
				totalRecords  : totalRecords,
				page          : self.page,
				perPage       : self.perPage,
				totalPages    : totalPages,
				lastPage      : totalPages,
				lastPagem1    : totalPages-1,
				previous      : false,
				next          : false,
				page_set      : [],
				startRecord   : (self.page - 1) * self.perPage + 1,
				endRecord     : Math.min(totalRecords, self.page * self.perPage)
			};
			
			if (self.page > 1) {
				info.prev = self.page - 1;
			}
			
			if (self.page < info.totalPages) {
				info.next = self.page + 1;
			}
			
			info.pageSet = self.setPagination(info);
			
			self.information = info;
			return info;
		},
		
		
		
		updateCompareSummary : function () {
			var self = this;
			/*if compare view */
			if(this.showCompare===true){
				$('.addToCompare').hide();
				$('.comparePlans').hide();
				$('.backToAll').show();
				$('#totalCount').html(this.totalPlanCount);
				$('#totalCompareCount').html(this.compareModels.length);
				$('#planTR').hide();
				$('#compareTR').show();
				$('#sort').hide();
 	 	 	 	$('#filter').hide();
 	 	 	 	$('.pagination ul').hide();
 	 	        $('[rel=tooltip]').tooltip('destroy');
				$('.tooltip').hide();
			}else{/*if no compare view*/
				$('.comparePlans').show();
				$('.backToAll').hide();
				$('#sort').show();
 	 	 	 	$('#filter').show();
 	 	 	 	$('.pagination ul').show();
				$('.addToCompare').show();
				$('#planTR').show();
				$('#compareTR').hide();
				
				$('#filteredPlanCount').html(this.filteredPlanCount);
				$('#compareCount').html(this.compareModels.length);
				$('#totalPlanCount').html(this.totalPlanCount);
			}
			
			/*if plans in compare then show remove button*/
			_.each(this.compareModels, function(remId){
				$('#compare_'+remId + '.addToCompare').hide();
				$('#remove_'+remId + '.removeCompare').show();
			});
		},
		
		preserveOtherOptions: function () {
			var self=this;
			if(this.fieldFilterRules.planType){
				$('#planType').val(this.fieldFilterRules.planType.value);
				$('#planName').val(this.fieldFilterRules.planName.value);
				_.each(this.fieldFilterRules.issuer.value, function(issuer){
					$("#issuer_"+issuer.replace(/[\s\.?]/g,"_")).attr('checked',true);
				});
			}
			var currentSort = this.sortColumn;
			
			if(currentSort =='indvPremium'){
				if(this.sortDirection == 'asc')
					$('#'+currentSort+'Order').html("(High to Low)");
				else if(this.sortDirection == 'desc')
					$('#'+currentSort+'Order').html("(Low to High)");
			}
			else if(currentSort =='issuerText')
				$('#'+currentSort+'Order').html("("+this.sortDirection+")");
			
			var info = this.info();
			var prevdisabled = info.page == 1 ? true : false;
			var nextdisabled = info.page == info.lastPage ? true :false;
			$('#prevLink').attr('disabled',prevdisabled);
			$('#nextLink').attr('disabled',nextdisabled);
			
			this.updateCompareSummary();
			_.each(this.cartItems, function(cartItem){
				if(!isNaN(cartItem)){
					$("#cart_"+cartItem).attr("class","addToCart addedItem btn btn-primary  btn-small icon-shopping-cart");
					$("#cart_"+cartItem).text(" Remove");
				}
			});
			
			var selectedPlanId = this.benchmarkplanId;
			_.each(this.models, function(model){
				
				if((selectedPlanId!= "") && selectedPlanId == model.get("planId")){
					$('#'+'plan_'+ model.get("planId")).hide();
					$('#'+'removeplan_'+ model.get("planId")).show();
				}
				else if(($('#benchmarkPlan').val() !='') && $('#benchmarkPlan').val() == model.get("planId") && (selectedPlanId == "")){
					$('#'+'plan_'+ model.get("planId")).hide();
					$('#'+'removeplan_'+ model.get("planId")).show();
						selectedPlanId = $('#benchmarkPlan').val();
				}
				else{
					$('#'+'plan_'+ model.get("planId")).show();
					$('#'+'removeplan_'+ model.get("planId")).hide();
				}	
			});
			this.benchmarkplanId = selectedPlanId;
		},
		
		loadIssuers : function () {
			var self = this;
			$('#issuerList').html("");
			_.each(self.issuerList, function(issuerFilter){
				var html = '<input type="checkbox" id="issuer_'+issuerFilter.replace(/[\s\.?]/g,"_")+'" class="issuers" value="'+issuerFilter+'" /> '+issuerFilter+' ('+self.issuerCount[issuerFilter+'Count']+')<br/>';
				$('#issuerList').append(html);
			});
		},
				
		setPagination : function (info) {
			var pages = [];
			// How many adjacent pages should be shown on each side?
			var ADJACENT = 3;
			var ADJACENTx2 = ADJACENT*2;
			var LASTPAGE = Math.ceil(info.totalRecords/info.perPage);
			var LPM1 = -1;
			
			if (LASTPAGE > 1) {
				//not enough pages to bother breaking it up
				if (LASTPAGE < (7 + ADJACENTx2)) {
					for (var i=1,l=LASTPAGE; i <= l; i++) {
						pages.push(i);
					}
				}
				// enough pages to hide some
				else if (LASTPAGE > (5 + ADJACENTx2)) {

					//close to beginning; only hide later pages
					if (info.page < (1 + ADJACENTx2)) {
						for (var i=1, l=4+ADJACENTx2; i < l; i++) {
							pages.push(i);				
						}
					}

					//in middle; hide some front and some back
					else if(LASTPAGE - ADJACENTx2 > info.page && info.page > ADJACENTx2) {
						for (var i = info.page - ADJACENT; i <= info.page + ADJACENT; i++) {
							pages.push(i);				
						}	
					}
					//close to end; only hide early pages
					else{
						for (var i = LASTPAGE - (2 + ADJACENTx2); i <= LASTPAGE; i++) {
							pages.push(i);					
						}
					}
				}
			}

			return pages;
		}
	};
	
})(App.backbone);