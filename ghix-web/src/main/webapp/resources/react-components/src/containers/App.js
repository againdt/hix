import React, { useEffect, Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import DashboardExt from "./DashboardExt";
import AdditionInfoContainer from './AdditionInfoContainer';
import '../assets/common.css';
import ConnectedIntlProvider from '../utils/ConnectedIntlProvider';
import { setMessages, setLanguage } from '../actions/actionCreators';

const App = ({ stateCode, language, pageContainer, messages, actions }) => {

    /**
     * Formats language to 'es' or 'en' instead of 'English'/'Spanish'
     * Currently there is no API to provide a locale, and the active language is stored in session storage.
     * @returns {string}
     */
    const getFormattedLanguage = () => {
        const languageSessionProp = sessionStorage.getItem('language');
        let language = 'en';

        switch (languageSessionProp) {
            case 'English':
                language = 'en';
                break;
            case 'Spanish':
                language = 'es';
                break;
            default:
                language = 'en';
                break;
        }
        return language;
    };

    useEffect(() => {
        const language = getFormattedLanguage();
        actions.setLanguage(language);
    }, []);

    /**
     * Once a language has been changed - loads corresponding messages.
     */
    useEffect(() => {
        const stateLocale = stateCode.toLowerCase();
        import(/* webpackChunkName: "test" */ /* webpackMode: "eager" */
            `../i18n/${stateLocale}/messages_${language}.json`)
            .then((messages) => {
                actions.setMessages(messages);
            });
    }, [language]);

    // TODO: Refactor this to use Router
    let content = '';
    switch (pageContainer) {
        case 'DASHBOARD':
            content = <DashboardExt/>;
            break;
        case 'VERIFICATION':
            content = <AdditionInfoContainer/>;
            break;
    }

    return (
        <Fragment>
            {
                messages && <ConnectedIntlProvider>
                    {content}
                </ConnectedIntlProvider>
            }
        </Fragment>
    )
};

const mapStateToProps = ({ app, pageContainer, messages, language }) => ({
    stateCode: app.stateCode,
    language,
    pageContainer,
    messages
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
            setMessages,
            setLanguage
        }, dispatch
    )
});

export default connect(mapStateToProps, mapDispatchToProps)(App);