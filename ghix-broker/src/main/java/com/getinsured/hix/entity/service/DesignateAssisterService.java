package com.getinsured.hix.entity.service;

import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Designate Assister Service to designate / de-designate individuals
 */
public interface DesignateAssisterService {

	/**
	 * De-designates assister for external individual id.
	 * 
	 * @param assisterId
	 *            Identification number for assister
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateAssister}
	 * @throws Exception 
	 */
	DesignateAssister deDesignateAssisterForIndividual(int assisterId, int extIndividualId) throws GIException;
	
	/**
	 * Finds record by given assister and individual Id
	 * 
	 * @param assisterId
	 *            Identification number for assister
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateAssister}
	 */
	DesignateAssister findByIndividualAndAssisterId(int assisterId, int extIndividualId);

	/**
	 * Finds record by given assister id, individual Id and status
	 * 
	 * @param assisterId
	 *            Identification number for assister
	 * @param extIndividualId
	 *            AHBX individual id
	 * @param status
	 *            
	 * @return {@link DesignateAssister}
	 */
	DesignateAssister findByIndividualAndAssisterIdAndStatus(int assisterId, int extIndividualId, Status status);

	/**
	 * Designates assister for external individual id.
	 * 
	 * @param assisterId
	 *            Identification number for assister
	 * @param extIndividualId
	 *            AHBX individual id
	 *            
	 * @return {@link DesignateAssister}
	 */
	DesignateAssister designateAssisterForIndividual(int assisterId,
			int extIndividualId) throws GIException;
}
