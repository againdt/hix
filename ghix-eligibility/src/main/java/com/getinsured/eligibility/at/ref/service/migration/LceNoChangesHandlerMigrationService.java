package com.getinsured.eligibility.at.ref.service.migration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component("lceNoChangesHandlerMigrationService")
public class LceNoChangesHandlerMigrationService extends LceProcessHandlerBaseMigrationService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceNoChangesHandlerMigrationService.class);

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceNoChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		createNoChangeEvent(currentApplication, enrolledApplication);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);
		
		copyEHBAmount(currentApplication, enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		
		LOGGER.info("LceNoChangesHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createNoChangeEvent(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Create No change SEP event - " + currentApplication.getId());
		ssapApplicationEventMigrationService.createDuplicateOnlyApplicationEvent(currentApplication.getId(), enrolledApplication.getId());
	}
}
