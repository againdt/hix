package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * Other Insurance Coverage.
 *
 * @author Yevgen Golubenko
 * @since 4/26/19
 */
public class OtherInsuranceCoverage
{
  @Expose
  private String insuranceName;

  @Expose
  private String policyNumber;

  @Expose
  private boolean limitedBenefit;

  public String getInsuranceName()
  {
    return insuranceName;
  }

  public void setInsuranceName(final String insuranceName)
  {
    this.insuranceName = insuranceName;
  }

  public String getPolicyNumber()
  {
    return policyNumber;
  }

  public void setPolicyNumber(final String policyNumber)
  {
    this.policyNumber = policyNumber;
  }

  public boolean isLimitedBenefit()
  {
    return limitedBenefit;
  }

  public void setLimitedBenefit(final boolean limitedBenefit)
  {
    this.limitedBenefit = limitedBenefit;
  }
}
