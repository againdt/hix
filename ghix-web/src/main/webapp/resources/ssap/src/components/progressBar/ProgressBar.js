import React, { PureComponent} from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { changeLanguage, clearState } from '../../actions/actionCreators';

class ProgressBar extends PureComponent {

    handleClick = () => {

        if (this.props.language === 'en') {
            this.props.actions.changeLanguage('es');
        } else {
            this.props.actions.changeLanguage('en');
        }
    };

    render() {
        const { page, language, actions: { clearState } } = this.props;
        return (
            <div className="usa-width-one-whole progress-bar">
                <div>
                    <button
                        onClick={this.handleClick}
                    >
                        {language === 'en' ? 'Eng' : 'Spn'}
                    </button>
                    <button
                        onClick={() => {
                            clearState();
                        }}
                    >
                        {'Clear State'}
                    </button>
                </div>
                <span>YOUR PROGRESS</span>
                <input
                    id="range-slider"
                    type="range"
                    min="0"
                    max="10"
                    value={page}
                    onChange={() => {}}
                />
            </div>
        );
    }
}

const mapStateToProps = state => ({
    page: state.page,
    language: state.language
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeLanguage,
        clearState
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ProgressBar);