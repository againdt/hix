(function() {
    'use strict';

    angular.module('agencyApp')
    .directive('enableLink', enableLink);

    function enableLink(){
        var ddo = {
            restrict: 'A',
            scope: {
                enabled: '=enableLink'
            },
            link: function(scope, element, attrs) {
                element.bind('click', function(event) {
                    if(!scope.enabled) {
                      event.preventDefault();
                    }
                });
            }
        };
        return ddo;
    }



})();