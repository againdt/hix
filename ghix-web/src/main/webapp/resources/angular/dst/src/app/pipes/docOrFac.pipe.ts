import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'docOrFac'
})
export class DocOrFac implements PipeTransform {

    transform(text: "doctor" | "hospital"): string {
        if (text === "doctor") {
            return "DOCTOR_CARD.title.doctor";
        } else if (text === "hospital") {
            return "DOCTOR_CARD.title.facility";
        } else {
            return "";
        }
    }

}
