package com.getinsured.iex.hub.apc.wrapper;

import java.math.BigDecimal;

import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_ee.InsurancePremiumType;
import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_ee.ObjectFactory;
import com.getinsured.iex.hub.apc.niem.niem.niem_core._2.AmountType;


public class InsurancePremiumTypeWrapper {
	private com.getinsured.iex.hub.apc.niem.niem.niem_core._2.ObjectFactory niemFactory;
	private InsurancePremiumType insurancePremiumAmountType;
	
	private String insurancePremiumAmount;
	
	public InsurancePremiumTypeWrapper()
	{
		ObjectFactory objectFactory = new ObjectFactory();
		niemFactory = new com.getinsured.iex.hub.apc.niem.niem.niem_core._2.ObjectFactory();
		insurancePremiumAmountType = objectFactory.createInsurancePremiumType();
	}
	
	public InsurancePremiumType getSystemRepresentation()
	{
		return insurancePremiumAmountType;
	}
	public String getInsurancePremiumAmount()
	{
		return insurancePremiumAmount;
	}
	
	public void setInsurancePremiumAmount(String insurancePremiumAmount)
	{
		AmountType amountType = niemFactory.createAmountType();
		amountType.setValue(BigDecimal.valueOf(Double.valueOf(insurancePremiumAmount)));
		insurancePremiumAmountType.setInsurancePremiumAmount(amountType);
		this.insurancePremiumAmount = insurancePremiumAmount;
	}
}
