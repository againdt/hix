package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.NatrOfCertDocumentID20Type;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;


/**
 * Wrapper Class for CertOfCitDocumentID23Type
 * 
 * @author Abhai Chaudhary, Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class NatrOfCertDocumentID20Wrapper implements JSONAware{
	
	private ObjectFactory factory;
	private NatrOfCertDocumentID20Type natrOfCertDocumentID20Type;
	
	private String alienNumber;
	private String naturalizationNumber;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String NATURALIZATION_NUMBER_KEY = "NaturalizationNumber";
	

	public NatrOfCertDocumentID20Wrapper(){
		this.factory = new ObjectFactory();
		this.natrOfCertDocumentID20Type = factory.createNatrOfCertDocumentID20Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.natrOfCertDocumentID20Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setNaturalizationNumber(String naturalizationNumber) throws HubServiceException{
		
		if(naturalizationNumber == null){
			throw new HubServiceException("No Naturalization number provided");
		}
		
		this.naturalizationNumber = naturalizationNumber;
		this.natrOfCertDocumentID20Type.setNaturalizationNumber(naturalizationNumber);
	}
	
	public String getNaturalizationNumber() {
		return naturalizationNumber;
	}
	
	public NatrOfCertDocumentID20Type getNatrOfCitDocumentID23Type(){
		return this.natrOfCertDocumentID20Type;
	}
	
	/**
	 * Converts the JSON String to NatrOfCertDocumentID20Wrapper object
	 * 
	 * @param jsonString - String representation for NatrOfCertDocumentID20Wrapper
	 * @return NatrOfCertDocumentID20Wrapper Object
	 */
	public static NatrOfCertDocumentID20Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create NatrOfCertDocumentID20Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		NatrOfCertDocumentID20Wrapper wrapper = new NatrOfCertDocumentID20Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.natrOfCertDocumentID20Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(NATURALIZATION_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.naturalizationNumber = (String)tmpObj;
			wrapper.natrOfCertDocumentID20Type.setNaturalizationNumber(wrapper.naturalizationNumber);
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(NATURALIZATION_NUMBER_KEY, this.naturalizationNumber);
		return obj.toJSONString();
	}
	
	public static NatrOfCertDocumentID20Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		NatrOfCertDocumentID20Wrapper wrapper = new NatrOfCertDocumentID20Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setNaturalizationNumber((String) jsonObj.get(NATURALIZATION_NUMBER_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(NATURALIZATION_NUMBER_KEY, this.naturalizationNumber);
		return obj;
	}
}
