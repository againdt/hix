<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@page contentType="text/html" import="java.util.*" %>
<%@ page isELIgnored="false"%>

<link href="/hix/resources/css/chosen.css" rel="stylesheet" type="text/css" media="screen,print">
<script src="../resources/js/jquery.autoSuggest.minified.js" type="text/javascript"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-tooltip.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<script src="../resources/js/jquery-ui.min.js" type="text/javascript"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-template.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-address-util.js" />"></script>



<script type="text/javascript">
  $(document).ready(function(){
	  $('.brktip').tooltip();
	  var fileCheck='${sizeExceed}';
	  if(fileCheck =="1"){
		  $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4>Please Select a File with Size Less than 5 MB</h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});  
	  }
          var config = {
                      '.chosen-select'           : {},
                      '.chosen-select-deselect'  : {allow_single_deselect:true},
                      '.chosen-select-no-single' : {disable_search_threshold:10},
                      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                      '.chosen-select-width'     : {width:"95%"}
                    }
                    for (var selector in config) {
                      $(selector).chosen(config[selector]);
                    }
  });
    
  </script>
<!-- <script>$(function(){
   var data = {items: [
{value: "1", clientsServed: "Employer"},
{value: "2", clientsServed: "Individual"}
]};
   
$("#clientsServed").autoSuggest(data.items, {selectedItemProp: "clientsServed",selectedValuesProp:"clientsServed", searchObjProps: "clientsServed"});
})
	</script> -->
	
<script type="text/javascript">
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            }
		}
	}
function removecomma(objName)
{
	//$("#clientsServed").onfocus();
  var lstLetters = objName;
  str = String(lstLetters);
  ie8Trim();
  str = str.trim();
 // alert(str);
  var temp = "";
  var temp1 = "";
   temp1 = str.substring(str.length-1, str.length);

  if(temp1 == ",")
	  {
  temp = str.substring(0, str.length-1);
	  }
  
  else
	  {
	  	
	  temp = str;
	  }


//  alert(temp);
  return temp;
}

$(document).ready(function() {
	$('.dropdown-toggle').dropdown();
	$("#brkProfile").removeClass("link");
	$("#brkProfile").addClass("active");
	
	$("a[title='Account']").parent().addClass("active");
	
	var brkId = ${broker.id};
	if(brkId != 0) {
		$('#certificationInfo').attr('class', 'done');
		$('#buildProfile').attr('class', 'done');
	} else {
		$('#certificationInfo').attr('class', 'visited');
		$('#buildProfile').attr('class', 'visited');
	};
	
	var paymentid = '${paymentId}';
	if (paymentid != 0) {
		$('#paymentinfo').attr('class', 'done');			
	} else {
		$('#paymentinfo').attr('class', 'visited');			
	};

	
	var brkName = '${broker.user.firstName}';
	brkName= brkName + ' ';
	brkName = brkName + '${broker.user.lastName}';
	$('#brkName').attr('value', brkName);	
});

function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}
//Function for autocompte of spoken languages
$(document).ready(function() {
    $( "#languagesSpoken").autocomplete({
        source: function (request, response) {
            $.getJSON("${pageContext. request. contextPath}/getLanguageSpokenList", {
                term: extractLast(request.term)
            }, response);
        },
        search: function () {
            // custom minLength
            var term = extractLast(this.value);
            if (term.length < 1) {
                return false;
            }
        },
        focus: function () {
            // prevent value inserted on focus
            return false;
        },
        select: function (event, ui) {
            var terms = split(this.value);
            // remove the current input
            terms.pop();
            // add the selected item
            terms.push(ui.item.value);
            // add placeholder to get the comma-and-space at the end
            terms.push("");
            this.value = terms.join(", ");
            return false;
        }
    });
     
});

function checkLanguageSpoken(){
	$.post('buildProfile/checkLanguagesSpoken', 
			{languagesSpoken: $("#languagesSpoken").val()},
             function(response){
                    if(response == true){
                            $("#languagesSpokenCheck").val(0);
                            
                    }
                    else{
                            $("#languagesSpokenCheck").val(1);
                    }
                    
                return true;
            }
        );
	}

jQuery.validator.addMethod("languagesSpokenCheck", function(value, element, param) {
	if(value == '') {
		return true;
	}
	checkLanguageSpoken();
	return $("#languagesSpokenCheck").val() == 1 ? false : true;
});


</script>

<div class="gutter10">
	<c:choose>
			<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<div class="l-page-breadcrumb hide"></div>
			</c:when>				
			<c:otherwise>
				<div class="l-page-breadcrumb">
					<!--start page-breadcrumb -->
					<div class="row-fluid">
						<ul class="page-breadcrumb">
							<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
							<li><a
								href="<c:url value="/broker/viewcertificationinformation"/>"><spring:message code="label.account" /></a></li>
							<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
						</ul>
					</div>
					<!--  end of row-fluid -->
				</div>
			</c:otherwise>
	</c:choose>
	<div class="row-fluid">
	<c:choose>
			<c:when test="${broker.certificationStatus == 'Pending' || broker.certificationStatus == null || broker.certificationStatus ==''}">
				<h1 id="skip"><spring:message code="label.newBrokerReg"/></h1>
			</c:when>				
			<c:otherwise>	
				<h1>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1> 
			</c:otherwise>
	    </c:choose>
	</div>

	<div class="row-fluid broker-panel">
		<!-- #sidebar -->      
	       <jsp:include page="brokerLeftNavigationMenu.jsp" />
	    <!-- #sidebar ENDS -->
		
		<div class="col-xs-12 col-sm-8 col-md-9 col-lg-9" id="rightpanel">
			<div class="header">
				<h4 class=""><spring:message code="label.profile"/></h4>
			</div> 
	<div class="">
	<div class="row-wrapper">
				<div class="row-fluid">
			            <div style="font-size: 14px; color: red">
			              <table>
			              	<c:forEach items="${fieldError}" var="violation">
			                    <tr>
			                        <td><p> 
			                         		<c:catch var="exception"> <c:if test="${violation.defaultMessage !=null}"> </c:if>   </c:catch>
											<c:if test="${empty exception}">
												 <c:choose>
													 	<c:when  test="${violation.defaultMessage != null && fn:indexOf(violation.defaultMessage, '{') eq 0}">
										       			  <spring:message  code="${fn:replace(fn:replace(violation.defaultMessage,'{',''),'}','')}"/>
										    		</c:when>
										    		<c:otherwise>
										    		  ${violation.field}	${violation.defaultMessage}
										    		</c:otherwise>
												 </c:choose> 
											</c:if>
											<c:catch var="exception"> <c:if test=" ${violation.message !=null}"> </c:if>    </c:catch>
											<c:if test="${empty exception}">
												 <spring:message  code="${fn:replace(fn:replace(violation.message,'{',''),'}','')}"/>
											</c:if>
			                        	<p/>
			                        </td>
			                    </tr>
			                </c:forEach>
			                </table>
			            </div>

			            </div>
			</div>
				
				
			<form onload="checkLanguageSpoken();" class="form-horizontal gutter10 brokerAddressValidation" id="frmbrokerProfile" name="frmbrokerProfile" enctype="multipart/form-data"  action="editprofile" method="POST">
	<div class="row-wrapper">
			<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
			 <df:csrfToken/>
				<spring:url value="/broker/photo/${encBrokerId}"  var="photoUrl"/>
				<div class="form-group">
				<c:choose>
	 	 	 	 	<c:when test = "${CA_STATE_CODE}">
                        <div class="col-xs-12 col-sm-3 col-sm-offset-2 col-md-3 col-md-offset-2 col-lg-3 col-lg-offset-2">
                            <img id="brokerPhoto" src="${photoUrl}" alt="Agent photo thumbnail" class="brokerprofilephoto thumbnail" />
                        </div>
                        <div class="col-xs-12 col-sm-7 col-md-6 col-lg-5">
                            <p class="gutter10">
					 	 	 	 		<spring:message code="label.photoCaption"/>
                            </p>
                        </div>
	 	 	 	 	</c:when>
	 	 	 	 	<c:otherwise>
	 	 	 	 		<img id="brokerPhoto" src="${photoUrl}" alt="Agent photo thumbnail" class="brokerprofilephoto thumbnail" />
	 	 	 	 	</c:otherwise>
				</c:choose>
				</div>
				<div class="form-group">
					<label for="brkChangephoto" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkChangephoto"/></label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="file" class="input-file" id="fileInputPhoto" name="fileInputPhoto">
						<input type="hidden"  id="fileInputPhoto_Size" name="fileInputPhoto_Size" value="1">	                	
                  		<input type="submit" id="btn_UploadPhoto" name="btn_UploadPhoto" value="<spring:message code="label.upload"/>" class="btn"  />                  	                  		
					    <div>
                  			<spring:message code="label.uploadCaption"/>
                  		</div>
					</div>	<!-- end of controls -->
					<div id="fileInputPhoto_error" class="controls"></div>
				</div><!-- end of  control-group -->
								
				<div class="form-group">
					<label for="brkName" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkBrokerName"/></label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" name="brkName" id="brkName" value="${broker.user.firstName} ${broker.user.lastName}" readonly class="input-large" size="30" />
						<div id="brkName_error"></div>
					</div>	<!-- end of controls -->
				</div><!-- end of  control-group -->		
				
			
				<h4 class="heading_border_b"><spring:message code="label.brkBusinessAddress"/></h4>
						<input type="hidden" id="lat" name="location.lat" value="${broker.location.lat}">
						<input type="hidden" id="lon" name="location.lon" value="${broker.location.lon}">
						<input type="hidden" id="rdi" name="location.rdi" value="${broker.location.rdi}">
						<input type="hidden" id="county" name="location.county" value="${broker.location.county}">
						
						<div class="form-group">
						<label for="address1" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkAddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" <c:if test="${broker.certificationStatus == 'Certified' && CA_STATE_CODE }"> readonly='true' </c:if> placeholder="Address Line 1" name="location.address1" id="address1" value="${broker.location.address1}" class="input-large" size="30" />
							<div id="address1_error"></div>
							<input type="hidden" id="address1_hidden" name="address1_hidden" value="${broker.location.address1}">
						</div>
					</div>	<!-- end of form-group -->
					
					<div class="form-group">
						<label for="address2" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkAddress2"/></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" <c:if test="${broker.certificationStatus == 'Certified' && CA_STATE_CODE }"> readonly='true' </c:if> placeholder="Apt, Suite, Unit, Bldg, Floor, etc" name="location.address2" id="address2" value="${broker.location.address2}" class="input-large" size="30" />
							<input type="hidden" id="address2_hidden" name="address2_hidden" value="${broker.location.address2}">
						</div>
					</div>	<!-- end of form-group -->
					

					<div class="form-group">
						<label for="city" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkCity"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" <c:if test="${broker.certificationStatus == 'Certified' && CA_STATE_CODE }"> readonly='true' </c:if>placeholder="City, Town" name="location.city" id="city" value="${broker.location.city}" class="input-large" size="30" />
							<input type="hidden" id="city_hidden" name="city_hidden" value="${broker.location.city}">
							<div id="city_error"></div>
					</div>
					</div> <!-- end of form-group -->

					<div class="form-group">
						<label for="state" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkState"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<select size="1" id="state" <c:if test="${broker.certificationStatus == 'Certified' && CA_STATE_CODE  }"> disabled </c:if>  name="location.state" path="statelist" class="input-medium">
								<option value=""><spring:message code="label.brkSelect"/></option>
								<c:forEach var="state" items="${statelist}">
									<option <c:if test="${state.code == broker.location.state}"> SELECTED </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_hidden" name="state_hidden" value="${broker.location.state}">
							<div id="state_error"></div>
						</div>
					</div>

					<div class="form-group">
						<label for="zip" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkZip"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" <c:if test="${broker.certificationStatus == 'Certified' && CA_STATE_CODE }"> readonly='true' </c:if> placeholder="" name="location.zip" id="zip" value="${broker.location.zip}" class="input-mini zipCode" maxlength="5"/>
							<input type="hidden" id="zip_hidden" name="zip_hidden" value="${broker.location.zip}">	
							<div id="zip_error"></div>
						</div>
					</div>	<!-- end of form-group -->

					<div class="form-group">
					<fieldset>
						<legend class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.brkClientServed"/> <a class="brktip" rel="tooltip" href="#" alt="tooltip to view profile" data-original-title="<spring:message code="label.broker.toolTip3"/>">&nbsp;<i class="icon-question-sign"></i></a></legend>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<c:set var="clientsServedList" value="${broker.clientsServed}"/> 													
	 							<c:if test="${fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Both') || fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Individual')}">
                 					 <label class="checkbox"> <input type="checkbox" value="Individuals &#47; Families" id="Individuals &#47; Families"  name="clientsServed"  ${fn:contains(clientsServedList, 'Individuals') ? 'checked="checked"' : '' } >Individuals &#47; Families</label> </br>
                 				</c:if>
                  				<c:if test="${fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('Both') || fn:toLowerCase(stateExchangeType) eq fn:toLowerCase('SHOP')}">
                 					 <label class="checkbox"> <input type="checkbox" value="Employers" id="Employers" name="clientsServed"  ${fn:contains(clientsServedList, 'Employers') ? 'checked="checked"' : '' } >Employers</label> </br>
                				</c:if>
							<div id="clientsServed_error"></div>
						</div>
					</fieldset>
					</div>

		
					<div class="form-group">
						<label for="languagesSpoken" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.brkLanguagesSpoken"/></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                         <select id="languagesSpoken" name="languagesSpoken"  class="input-large chosen-select" multiple tabindex="4" ></select>
                                 <div id="languagesSpoken_error"></div>
                         </div>
					</div><!-- end of form-group -->
					
					<div class="form-group">
					<fieldset>
						<legend class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.brkProductExpertise"/><a class="brktip" rel="tooltip" href="#" alt="Broker Tip" data-original-title="<spring:message code="label.broker.toolTip2"/>">&nbsp;<i class="icon-question-sign"></i></a></legend>
							<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
								<label class="checkbox" for="health">
									<input id="health" type="checkbox" name="productExpertise" value="Health" ${fn:contains(broker.productExpertise,'Health') ? 'checked="checked"' : ''} />  Health
						        </label>
						        <label class="checkbox" for="dental">
						            <input id="dental" type="checkbox" name="productExpertise" value="Dental" ${fn:contains(broker.productExpertise,'Dental') ? 'checked="checked"' : ''} />  Dental
						        </label>
						        <label class="checkbox" for="vision">
						            <input type="checkbox" name="productExpertise" value="Vision" ${fn:contains(broker.productExpertise,'Vision') ? 'checked="checked"' : ''} />  Vision
						        </label>
						        <label class="checkbox" for="life">
						            <input id="life" type="checkbox" name="productExpertise" value="Life" ${fn:contains(broker.productExpertise,'Life') ? 'checked="checked"' : ''} />  Life
						        </label>
						        <label class="checkbox" for="Medicare">
						            <input id="Medicare" type="checkbox" name="productExpertise" value="Medicare" ${fn:contains(broker.productExpertise,'Medicare') ? 'checked="checked"' : ''} />  Medicare
						        </label>
						        <label class="checkbox" for="Medicaid">
						            <input id="Medicaid" type="checkbox" name="productExpertise" value="Medicaid" ${fn:contains(broker.productExpertise,'Medicaid') ? 'checked="checked"' : ''} />  Medicaid
						        </label>
						        <label class="checkbox">
						            <input type="checkbox" name="productExpertise" value="CHIP"${fn:contains(broker.productExpertise,'CHIP') ? 'checked="checked"' : ''} />  CHIP
								</label>
								<label class="checkbox">
						            <input type="checkbox" name="productExpertise" value="Workers Compensation"${fn:contains(broker.productExpertise,'Workers Compensation') ? 'checked="checked"' : ''} /> Workers Compensation
								</label>
								<label class="checkbox">
						            <input type="checkbox" name="productExpertise" value="Property/Casualty"${fn:contains(broker.productExpertise,'Property/Casualty')||fn:contains(broker.productExpertise,'Property/Causality') ? 'checked="checked"' : ''} />  Property/Casualty
								</label>
								<div id="productExpertise_error"></div>
							</div>
					</fieldset>
					</div>

 					<div class="form-group">
						<label for="yourWebSite" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.brkYourWebsite"/><a class="brktip" rel="tooltip" href="#" alt="Website address" data-original-title="<spring:message code="label.broker.toolTip1"/>">&nbsp;<i class="icon-question-sign"></i></a></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" id="yourWebSite" class="input-large" name="yourWebSite" size="50" value="${broker.yourWebSite}" />
							<div id="yourWebSite_error"></div>
						</div>
					</div><!-- end of form-group -->
					
					<div class="form-group">
                      <label class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message  code="label.brkpublicEmail"/><a class="brktip" rel="tooltip" href="#" data-original-title="<spring:message code="label.brkYourPublicEmailToolTip" htmlEscape="true"/>">&nbsp;<i class="icon-question-sign"></i></a></label>
                      <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                          <input type="text" name="yourPublicEmail" id="yourPublicEmail" value='<c:choose><c:when test="${broker.certificationStatus =='Incomplete'}">${broker.user.email}</c:when><c:otherwise>${broker.yourPublicEmail}</c:otherwise></c:choose>' class="input-large" size="30" maxlength="100"  />
						  <div id="yourPublicEmail_error" class="help-inline"></div>
                      </div>
                    </div>
									
					<c:if test="${!CA_STATE_CODE}">
					<div class="form-group">
						<label for="education" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.brkEducation"/></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<select id="education" class="input-large" name="education">
								<option value="">--</option>
								<c:forEach items="${educationList}" var="currentEducation">
									<option value="${currentEducation.key}" <c:if test="${currentEducation.key eq broker.education}"> SELECTED </c:if>>${currentEducation.value}</option>
								</c:forEach>
							</select>
							<div id="education_error"></div>
						</div>
					</div>							
					</c:if>							
										
					<div class="form-group">
						<label for="aboutMe" class="required col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.aboutYourself"/></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<textarea name="aboutMe" id="aboutMe" class="input-large" rows="4" maxlength="4000">${broker.aboutMe}</textarea>
						</div>
					</div>	<!-- end of form-group -->


				<div class="form-actions form_actions">
				<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
					<c:choose>					
						<c:when test="${broker.certificationStatus != 'Pending' && broker.id != 0 && broker.certificationStatus != 'Incomplete'}">	
							<a class="btn btn_cancel" href="<c:url value="/broker/viewprofile?cancel=true"/>"><spring:message code="label.brkCancel"/></a>
							<input type="submit" name="saveAgentProfile" id="saveAgentProfile" value="<spring:message code="label.agent.employee.addNewComment.save-button"/>" class="btn btn-primary btn_save"/>
						</c:when>
						<c:otherwise>
							<input type="submit" name="next" id="next" value="<spring:message  code='label.brkNext'/>" class="btn btn-primary btn_save"/>
						</c:otherwise>
					</c:choose>
				</div>
				</div>				
				<input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="1" />	
	</div>
			</form>
		</div>
		</div><!-- end of #rightpanel -->
	</div>


		
		<div class="notes" style="display: none">
			<div class="row">
				<div class="span">
					<p><spring:message code="label.brkBrokerEligibilityInfo"/></p>
				</div>
			</div>
		</div><!--  end of .notes -->
	

<script type="text/javascript">

var validator = $("#frmbrokerProfile").validate({ 
	onkeyup: false,
	rules : {
			brkName : {required : true},
			"location.address1" : {required : true},
			"location.city" : {required : true},
			"location.state" : {required : true},
			"location.zip" : {required : true, number: true,ZipCodecheck:true },
			"mailingLocation.address1" : {required : true},
			"mailingLocation.city" : {required : true},
			"mailingLocation.state" : {required : true},
			"mailingLocation.zip" : {required : true, number: true,MailingZipCodecheck:true },
			phone1 : {phonecheck1 : true},
			phone2 : {phonecheck2 : true},
			phone3 : {phonecheck : true},	
			languagesSpoken : {required : false,number:false,languagesSpokenCheck:true},		
			education : {required : false},
			fileInputPhoto : {PhotoUploadCheck: true,sizeCheck:true},
			yourWebSite : {checkurl:true},
			yourPublicEmail : { required : false, validateEmail: true}
	},
	messages : {
		brkName : { required : "<span> <em class='excl'>!</em><spring:message code='label.broker.exceBrokerNameisRequired' javaScriptEscape='true'/></span>"},
		phone1: { phonecheck1 : "<span> <em class='excl'>!</em><spring:message code='label.broker.validatePhoneNo' javaScriptEscape='true'/></span>"},
		phone2: { phonecheck2 : "<span> <em class='excl'>!</em><spring:message code='label.broker.validatePhoneNo' javaScriptEscape='true'/></span>"},
		phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.broker.validatePhoneNo' javaScriptEscape='true'/></span>"},
		"location.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		"location.city" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		"location.state" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateState' javaScriptEscape='true'/></span>"},
		"location.zip" :"<span> <em class='excl'>!</em><spring:message code='label.broker.excePleaseEnterValidZipCode' javaScriptEscape='true'/></span>", 
		"mailingLocation.address1" : { required : "<span><em class='excl'>!</em> <spring:message code='label.broker.excePleaseEnterMail1' javaScriptEscape='true'/></span>"},
		"mailingLocation.city" : { required : "<span><em class='excl'>!</em> <spring:message code='label.broker.excePleaseEnterMailingAddressCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state" : { required : "<span><em class='excl'>!</em> <spring:message code='label.broker.excePleaseEnterMailingAddressState' javaScriptEscape='true'/></span>"},
		"mailingLocation.zip" :"<span> <em class='excl'>!</em><spring:message code='label.broker.excePleaseEnterValidZipCode' javaScriptEscape='true'/></span>",
		education : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEducation' javaScriptEscape='true'/></span>"},
		fileInputPhoto : {PhotoUploadCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePhoto' javaScriptEscape='true'/></span>",
                                sizeCheck :"<span> <em class='excl'>!</em><spring:message code='label.brkvalidatePleaseSelectFileWithSizeLessThan5MB' javaScriptEscape='true'/> </span>"	},
		yourWebSite : {checkurl : "<span><em class='excl'>!</em><spring:message  code='err.validateYourWebSite' javaScriptEscape='true'/></span>"},
		languagesSpoken : {languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.brokerValidatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/></span>"},
		yourPublicEmail : {validateEmail:"<span> <em class='excl'>!</em><spring:message code='label.validatePublicEmail' javaScriptEscape='true'/></span>"}
	},

	errorClass: "error",
	errorPlacement : function(error, element) {
		var elementId = element.attr('id');
		error.appendTo($("#" + elementId + "_error"));
		$("#" + elementId + "_error").addClass('error help-inline');
	}
});

jQuery.validator.addMethod("validateEmail", function(value, element) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if($.trim(value) == ""){
		return true;
	}
	if (filter.test(value)) {
	    return true;
	}
	else {
	    return false;
	}
});

jQuery.validator.addMethod("checkurl", function(value, element) {
	//Should validate only if a value has been entered 
	if(value == '') {
		return true;
	} 
	if((value.indexOf('..') != -1 )|| (value[value.length-1]=='.')){
		 return false;
	}
	if(!(value.indexOf('http://') == 0 || value.indexOf('https://') == 0 || value.indexOf('www.') == 0)){
		 return false;
	}
	// now check if valid url
	var regx = "(((https?:[/][/](www.)?)|(www.))([-]|[a-z]|[A-Z]|[0-9]|[/.]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	if(value.indexOf('http://www') == 0 || value.indexOf('https://www')==0){
		 regx = "(((https?:[/][/])((www)))[.]([-]|[a-z]|[A-Z]|[0-9]|[~])+[.]([a-z]|[A-Z]|[0-9]|[~])+([a-z]|[A-Z]|[0-9]|[/.]|[~])*)";
	}
	var regexp = new RegExp(regx);
	return regexp.test(value);
	
});	

$(function(){
	 $('#fileInputPhoto').change(function(){
			var rv = -1; // Return value assumes failure.
			 if (navigator.appName == 'Microsoft Internet Explorer')
			 {
			    var ua = navigator.userAgent;
			    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
			    if (re.exec(ua) != null)
			       rv = parseFloat( RegExp.$1 );
			 }
			 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
			 var f=this.files[0];
			if((f.size > 5242880)||(f.fileSize > 5242880)){
				$("#fileInputPhoto_Size").val(0);
			}
			else{
				$("#fileInputPhoto_Size").val(1);	
			} 
			}
			else{
				$("#fileInputPhoto_Size").val(1);	
			} 

		});
	});
jQuery.validator.addMethod("sizeCheck", function(value, element, param) { 
	  ie8Trim();
	  if($("#fileInputPhoto_Size").val()==1){
		  return true;
	  }
	  else{
		  return false;
	  }
});

jQuery.validator.addMethod("ZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 || zip == '00000') )){ 
		return false; 
	}
	return true;
});
jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#mailingZip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || zip == '00000')){ 
		return false; 
	}
	return true;
});
function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	ie8Trim();
	phone1 = $("#phone1").val().trim(); 
	phone2 = $("#phone2").val().trim(); 
	phone3 = $("#phone3").val().trim(); 


	if( (phone1 == "" || phone2 == "" || phone3 == "")  || (isNaN(phone1)) || (phone1.length < 3 ) || (isNaN(phone2)) || (phone2.length < 3 ) || (isNaN(phone3)) || (phone3.length < 4 )  ) { return false; }
	return true;
});

jQuery.validator.addMethod("phonecheck1", function(value, element, param) {
	ie8Trim();
	phone1 = $("#phone1").val().trim(); 
	if((phone1.length < 3 ) || (phone1 == "") || (isNaN(phone1))) {
		 	return false;
		}
	return true;
});

jQuery.validator.addMethod("phonecheck2", function(value, element, param) {
	ie8Trim();
	phone2 = $("#phone2").val().trim(); 
	if((phone2.length < 3 ) || (phone2 == "") || (isNaN(phone2)) ){
			return false;
	}
	return true;
});


//call this function on blur of zip code and if address is valid invoke validateAddress
function validateAddress(e){
	var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
	var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';

	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
		
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	
	viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);
}

jQuery.validator.addMethod("PhotoUploadCheck", function(value, element, param) { 
	  ie8Trim();
    var file = $('input[type="file"]').val();
    var exts = ['jpg','jpeg','gif','png','bmp'];
    if ( file ) {
      var get_ext = file.split('.');
      get_ext = get_ext.reverse();

      if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
        return true;
      } else {
        return false;
      }
   }
   return true;
});

$('#languagesSpoken').focusout(function() {
    county=$("#languagesSpoken").val().trim();
    var test= (county).substring((county).length-1,(county).length);
	 if(test==","){
		 var index = (county).substring(0,(county).length-1);
        $('#languagesSpoken').val(index);
	 }
	 else{
		  }
 });
</script>

<script type="text/javascript">
function split(val) {
    return val.split(/,\s*/);
}
function extractLast(term) {
    return split(term).pop();
}

$(document).ready(function(){
	
	$("#btn_UploadPhoto").click(function() {
		var file = $('input[type="file"]').val();
		if(file!=""){
			var file1 = $('input[type="file"]').val();
		    var exts = ['jpg','jpeg','gif','png','bmp'];
		    var get_ext = file1.split('.');
		      get_ext = get_ext.reverse();
			 if ( $.inArray ( get_ext[0].toLowerCase(), exts ) > -1 ){
				 if(document.getElementById('fileInputPhoto_Size').value==1){		 
		$('#frmbrokerProfile').ajaxSubmit({				
			url: "<c:url value='/broker/uploadphoto/${encBrokerId}'/>",
			dataType:'text',
			success: function(responseText) { 
				if(responseText.indexOf('Missing CSRF token') != -1){
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
					return false;
				}
				if(responseText=="SUCCESS"){
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
					$("#brokerPhoto").attr("src", "<c:url value='/broker/photo/${encBrokerId}?ts=" + ((new Date()).getTime()) + "'/>");								
				}
				else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}			
			},
			error : function(responseText) {
				//alert("Failed to Upload File");	
				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
				 }
			});
				 }
		else{
			//alert("Please Select a File Before upload");
             $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
             }
            }
        else{
				//alert("Please Select a File Before upload");
            $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        }		 
         }
        else{
              //alert("Please Select a File Before upload");
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
            }
		
			return false; 
	});	// button click close
	
	function loadUsers() {
        var matched = $("#languagesSpoken option:selected").text();
        var pathURL = "/hix/broker/buildprofile/getlanguage";
        $('#errorMsg').val("");
        $.ajax({
                url : pathURL,
                type : "GET",
                data : {
                        matched : matched
                },
                success: function(response){
                        loadData(response);
        },
                error: function(e){
                alert("Failed to load Languages");
        		},
        });
	}
        
      //load the jquery chosen plugin with the AJAX data
        function populatelanguages() {
               $("#languagesSpoken").html('');
               var respData = $.parseJSON('${languagesList}');
               var languages='${broker.languagesSpoken}';
               for ( var key in respData) {
               var isSelected = false;
                    if(languages!=null){
                             isSelected = checkLanguages(respData[key]);
                     }
                     if(isSelected){
                         $('#languagesSpoken').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
                     } else {
                         $('#languagesSpoken').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
                     }
                }
           
               $('#languagesSpoken').trigger("liszt:updated");
       }
      
        function checkLanguages(county){
            var languages='${broker.languagesSpoken}';
            var countiesArray=languages.split(',');
            var found = false;
                    for (var i = 0; i < countiesArray.length && !found; i++) {
                            var countyTocompare=countiesArray[i].replace(/^\s+|\s+$/g, '');
                           if (countyTocompare.toLowerCase() == county.toLowerCase()) {
                                found = true;
                               
                            }
                        }
                  return found;
    	}
		
        $(document).ready(function() {
        	populatelanguages();
        });
});

</script>
<!-- >script>
 $('.brktip').tooltip();
</script -->
