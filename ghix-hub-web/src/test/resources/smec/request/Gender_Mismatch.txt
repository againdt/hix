{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "organizationCode": "GHIX"
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "dateOfBirth": "09/07/1997",
            "name": {
              "firstName": "STEPHEN",
              "lastName": "FRAZIER",
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "601730899"
            },
            "gender": "F",
           
            "insurance": {
              "insurancePolicyEffectiveDate": "10/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }             
          }
        ]
      }
    ]
  }
}