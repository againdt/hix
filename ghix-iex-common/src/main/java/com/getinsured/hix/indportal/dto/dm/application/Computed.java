package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;
import java.util.Map;

public class Computed {
	private Map<String,Member> members;
	private  Application application;
	private  Map<String,TaxHousehold> taxHouseholds;
	private  ArrayList < Object > variableDeterminations;


	 // Getter Methods 

	 
	 public Application getApplication() {
	  return application;
	 }

	 // Setter Methods 


	 public void setApplication(Application application) {
	  this.application = application;
	 }

	 public Map<String, Member> getMembers() {
		return members;
	}

	public void setMembers(Map<String, Member> members) {
		this.members = members;
	}

	public Map<String, TaxHousehold> getTaxHouseholds() {
		return taxHouseholds;
	}

	public void setTaxHouseholds(Map<String, TaxHousehold> taxHouseholds) {
		this.taxHouseholds = taxHouseholds;
	}
	}
