package com.getinsured.eligibility.ssap.documentverification.exceptions;

public class SsapDocumentsServiceException extends Exception {
	private String message;
	
	public SsapDocumentsServiceException(String message) {
		super();
		this.message = message;
	}

	private static final long serialVersionUID = 1L;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
