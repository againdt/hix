package com.getinsured.hix.account.service.jpa;

import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.model.InboxMsgDoc;
import com.getinsured.hix.model.InboxMsgResponse;
import com.getinsured.hix.model.InboxUserAddressBookResp;
import com.getinsured.hix.platform.account.repository.IInboxMsgRepository;
import com.getinsured.hix.platform.account.service.InboxMsgService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.util.ObjectClonner;
import com.getinsured.hix.platform.util.exception.GIException;
import com.google.gson.Gson;

import static org.junit.Assert.assertTrue;

/**
 * <p>
 * mvn -Dtest=InboxMsgServiceImplTest test
 * 
 * @author talreja_n
 * @since 22 November, 2012 JUnit test class for Secure Inbox
 */

@TransactionConfiguration(defaultRollback = false)
public class InboxMsgServiceImplTest extends BaseTest {

	private static final String CLASS_NAME = "InboxMsgServiceImplTest";
	@Autowired
	private InboxMsgService inboxMsgService;
	@Autowired
	private IInboxMsgRepository iInboxMsgRepository;

	private static final Logger logger = LoggerFactory
			.getLogger(InboxMsgServiceImplTest.class);

	@BeforeClass
	public static void setUp() {

		// Setting GHIX HOME
		// System.setProperty("GHIX_HOME", "D:/Project/ghix-git/");
		// System.setProperty("GHIX_HOME", "D:/VimoProject/GHIX");

		// Configuring logger
		// String rootPath = System.getProperty("GHIX_HOME");
		// DOMConfigurator.configure(rootPath +
		// "/ghix-setup/conf/ghix-log4j.xml");
		// DOMConfigurator.configure(rootPath +
		// "/ghix/ghix-setup/conf/ghix-log4j.xml");

		//logger.info("Setting GHIX_HOME to : D:/VimoProject/GHIX");
	}

	@Test
	@Rollback(false)
	@Transactional
	public void composeMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".composeMessage Begin");

		// Test Case 1 : To Test new message
		InboxMsg msg = new InboxMsg();
		msg.setOwnerUserId(3);
		msg.setOwnerUserName("Exchange Admin");
		msg.setFromUserId(3);
		msg.setFromUserName("Exchange Admin");
		msg.setToUserIdList("6,7");
		msg.setPriority(InboxMsg.PRIORITY.H);
		msg.setStatus(InboxMsg.STATUS.C);

		InboxMsgResponse response = null;
		try {
			response = inboxMsgService.saveMessage(msg);
		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}

		Gson gson = new Gson();
		String json = gson.toJson(response);

		logger.info("Compose Message Response==> " + json);
		logger.info("Response time : " + response.getExecDuration() + " ms.");
		assertTrue(response.getErrCode() == 0);

		// Test Case 2 : To save existing message in compose state
		ObjectClonner cloner = new ObjectClonner();
		msg = iInboxMsgRepository.findOne(92L);
		InboxMsg anotherMsg = new InboxMsg();
		anotherMsg = (InboxMsg) cloner.clone(msg);
		anotherMsg.setStatus(InboxMsg.STATUS.C);
		anotherMsg.setMsgSub("Testing compose message");
		try {
			response = inboxMsgService.saveMessage(anotherMsg);
		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}
		gson = new Gson();
		json = gson.toJson(response);

		logger.info("Compose Message Response==> " + json);
		logger.info("Response time : " + response.getExecDuration() + " ms.");
		assertTrue(response.getErrCode() == 0);

		logger.info("JUnit " + CLASS_NAME + ".composeMessage End");
	}

	@Test
	@Rollback(false)
	@Transactional
	public void sendMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".sendMessage Begin");

		// Test Case 3 : To send a message
		InboxMsg msg = new InboxMsg();
		msg = iInboxMsgRepository.findOne(92L);
		InboxMsg anotherMsg = new InboxMsg();
		ObjectClonner cloner = new ObjectClonner();
		anotherMsg = (InboxMsg) cloner.clone(msg);
		anotherMsg.setMsgBody("Sending message");
		anotherMsg.setStatus(InboxMsg.STATUS.S);

		InboxMsgResponse response = null;
		try {
			response = inboxMsgService.saveMessage(anotherMsg);
		} catch (Exception e) {
			logger.error("Exception has occured : ", e);
		}

		Gson gson = new Gson();
		String json = gson.toJson(response);

		logger.info("Send Message Response==> " + json);
		logger.info("Response time : " + response.getExecDuration() + " ms.");

		assertTrue(response.getErrCode() == 0);

		logger.info("JUnit " + CLASS_NAME + ".sendMessage End");

	}

	@Test
	public void replyMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".replyMessage Begin");

		// Test Case 5 : To send a message
		InboxMsg msg = new InboxMsg();
		msg = iInboxMsgRepository.findOne(51L);
		msg.setStatus(InboxMsg.STATUS.P);

		InboxMsgResponse response = null;
		try {
			response = inboxMsgService.saveMessage(msg);
		} catch (Exception e) {
			logger.error("Exception has occured : ", e);
		}

		Gson gson = new Gson();
		String json = gson.toJson(response);

		logger.info("replyMessage Response==> " + json);
		logger.info("Response time : " + response.getExecDuration() + " ms.");

		assertTrue(response.getErrCode() == 0);

		logger.info("JUnit " + CLASS_NAME + ".replyMessage End");

	}

	@Test
	public void forwardMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".forwardMessage Begin");

		// Test Case 6 : To send a message
		InboxMsg msg = new InboxMsg();
		msg = iInboxMsgRepository.findOne(51L);
		msg.setStatus(InboxMsg.STATUS.F);

		InboxMsgResponse response = null;
		try {
			response = inboxMsgService.saveMessage(msg);
		} catch (Exception e) {
			logger.error("Exception has occured : ", e);
		}

		Gson gson = new Gson();
		String json = gson.toJson(response);

		logger.info("forwardMessage Response==> " + json);
		logger.info("Response time : " + response.getExecDuration() + " ms.");

		assertTrue(response.getErrCode() == 0);

		logger.info("JUnit " + CLASS_NAME + ".forwardMessage End");

	}

	@Test
	public void archiveMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".archiveMessage Begin");

		InboxMsg msg = new InboxMsg();
		InboxMsgResponse response = null;
		Gson gson = null;
		String json = null;

		// Test Case 6 : To check archived message
		msg = iInboxMsgRepository.findOne(60L);
		msg.setStatus(InboxMsg.STATUS.A);
		try {
			response = inboxMsgService.saveMessage(msg);
			gson = new Gson();
			json = gson.toJson(response);
			logger.info("archiveMessage Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");
			assertTrue(response.getErrCode() != 0);

		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}

		logger.info("JUnit " + CLASS_NAME + ".archiveMessage End");
	}

	@Test
	public void deleteMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".deleteMessage Begin");

		InboxMsg msg = new InboxMsg();
		InboxMsgResponse response = null;
		Gson gson = null;
		String json = null;

		// Test Case 7 : To check deleted message
		msg = iInboxMsgRepository.findOne(60L);
		msg.setStatus(InboxMsg.STATUS.D);
		try {
			response = inboxMsgService.saveMessage(msg);
			gson = new Gson();
			json = gson.toJson(response);
			logger.info("deleteMessage Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");
			assertTrue(response.getErrCode() != 0);

		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}

		logger.info("JUnit " + CLASS_NAME + ".deleteMessage End");
	}

	@Test
	public void checkingInvalidMessages() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".checkingInvalidMessages Begin");

		InboxMsg msg = null;
		InboxMsgResponse response = null;
		Gson gson = null;
		String json = null;

		// Test Case 8 : Checking invalid messages
		try {
			response = inboxMsgService.saveMessage(msg);
			gson = new Gson();
			json = gson.toJson(response);
			logger.info("checkingInvalidMessages Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");
			assertTrue(response.getErrCode() != 0);

		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}

		logger.info("JUnit " + CLASS_NAME + ".checkingInvalidMessages End");

	}

	@Test
	public void searchMessage() {

		logger.info("Testing " + CLASS_NAME + ".searchMessage API");

		InboxMsg msg = new InboxMsg();
		msg.setOwnerUserId(3);
		msg.setStatus(InboxMsg.STATUS.C);
		msg.setType(InboxMsg.TYPE.M);
		msg.setPriority(InboxMsg.PRIORITY.L);
		InboxMsgResponse response = new InboxMsgResponse();

		Calendar startDate = new GregorianCalendar();
		startDate.set(Calendar.DATE, 25);

		try {
			response = inboxMsgService.searchMessages(msg, startDate, null);
			Gson gson = new Gson();
			String json = gson.toJson(response);
			logger.info("Search Message Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");

			msg.setType(null);
			msg.setPriority(InboxMsg.PRIORITY.L);
			response = inboxMsgService.searchMessages(msg, null, null);
			gson = new Gson();
			json = gson.toJson(response);
			logger.info("Search Message Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");

			msg.setPriority(null);
			msg.setMsgSub("Welcome");
			response = inboxMsgService.searchMessages(msg, null, null);
			gson = new Gson();
			json = gson.toJson(response);
			logger.info("Search Message Response==> " + json);
			logger.info("Response time : " + response.getExecDuration()
					+ " ms.");

		} catch (Exception e) {
			logger.error("Exception has occured : ", e);
		}

		assertTrue(response.getMessageList().size() != 0);

		logger.info("Finished testing searchMessage API");

	}

	/**
	 * Method is used run JUnit test on cancelMessage of
	 * InboxMsgServiceImpl.java class.
	 */
	@Test
	@Rollback(false)
	public void cancelMessage() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".cancelMessage Begin");

		InboxMsg msg = null;
		InboxMsgResponse response = null;

		try {
			logger.info("TestCase - 1: START");

			response = inboxMsgService.cancelMessage(msg);

			Gson gson = new Gson();
			String json = gson.toJson(response);
			logger.info("cancelMessage Response==>" + json);
			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 1: END");

			logger.info("TestCase - 2: START");
			msg = new InboxMsg();
			// Add existing value of InboxMsg table.
			msg.setId(1);
			msg.setStatus(InboxMsg.STATUS.S);

			response = inboxMsgService.cancelMessage(msg);

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("cancelMessage Response==>" + json);
			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 2: END");

			logger.info("TestCase - 3: START");
			msg = new InboxMsg();
			// Add existing value of InboxMsg table.
			msg.setId(1);
			msg.setStatus(InboxMsg.STATUS.C);

			response = inboxMsgService.cancelMessage(msg);

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("cancelMessage Response==>" + json);
			assertTrue(response.getErrCode() == 0);
			logger.info("TestCase - 3: END");

		} catch (GIException e) {
			logger.error("InboxMsgException has occured : " + e.getMessage());

		} finally {
			msg = null;
			response = null;
			logger.info("JUnit " + CLASS_NAME + ".cancelMessage End");
		}
	}

	@Test
	public void getAddressBook() {

		logger.info("JUnit " + CLASS_NAME + ".getAddressBook Begin");

		InboxUserAddressBookResp inboxUserAddressBookResp = null;

		try {
			inboxUserAddressBookResp = inboxMsgService.getUserAddressBook(9L,
					ModuleUserService.EMPLOYER_MODULE);
		} catch (Exception e) {
			logger.error("Exception has occured : " + e.getMessage());
		}

		Gson gson = new Gson();
		String json = gson.toJson(inboxUserAddressBookResp);
		logger.info("getAddressBook Response==> " + json);
		assertTrue(inboxUserAddressBookResp.getAddressBook() != null);

		logger.info("JUnit " + CLASS_NAME + ".getAddressBook End"
				+ "=============");
	}

	/**
	 * Method is used run JUnit test on addAttachment of
	 * InboxMsgServiceImpl.java class.
	 */
	@Test
	@Rollback(false)
	@Transactional
	public void addAttachment() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".addAttachment Begin");

		// FIXME: Before executing this test case you need to check below file
		// name is exist at Alfresco or not.
		// If it is exist then you have to remove first & then run this unit
		// test.
		final String documentname = "JUnitAddAttachment1.txt";
		String documentId = null;
		InboxMsgResponse response = null;
		InboxMsg msg = null;

		try {

			logger.info("TestCase - 1: START");
			logger.info("Desc: set null to InboxMsg Object");
			response = inboxMsgService.addAttachment(null, null); // set null to
																	// InboxMsg
																	// Object

			Gson gson = new Gson();
			String json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 1: END");

			logger.info("TestCase - 2: START");
			logger.info("Desc: set null to byte[] attachment data");
			response = inboxMsgService.addAttachment(new InboxMsg(), null); // set
																			// null
																			// to
																			// byte[]
																			// attachment
			logger.info("response is null: " + (response == null));

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 2: END");

			logger.info("TestCase - 3: START");
			logger.info("Desc: set null status to InboxMsg object");
			msg = new InboxMsg();
			msg.setStatus(null); // set null InboxMsg Status

			String data = "Hello World from to InboxMsgService";
			response = inboxMsgService.addAttachment(msg, data.getBytes());
			logger.info("response is null: " + (response == null));

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 3: END");

			logger.info("TestCase - 4: START");
			logger.info("Desc: set Invalid status to InboxMsg object");
			msg = new InboxMsg();
			msg.setStatus(InboxMsg.STATUS.S); // set Invalid Status

			data = "Hello World from to InboxMsgService";
			response = inboxMsgService.addAttachment(msg, data.getBytes());
			logger.info("response is null: " + (response == null));

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 4: END");

			logger.info("TestCase - 5: START");
			logger.info("Desc: set null to List<InboxMsgDoc> object");
			msg = new InboxMsg();
			msg.setStatus(InboxMsg.STATUS.C); // Set null to InboxMsgDoc object

			msg.setMessageDocs(null);
			response = inboxMsgService.addAttachment(msg, data.getBytes());
			logger.info("response is null: " + (response == null));

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 5: END");

			logger.info("TestCase - 6: START");
			logger.info("Desc: set null to InboxMsgDoc object");
			msg = new InboxMsg();
			msg.setStatus(InboxMsg.STATUS.C);

			data = "Hello World from to InboxMsgService";
			ArrayList<InboxMsgDoc> inboxMsgDocList = new ArrayList<InboxMsgDoc>();
			inboxMsgDocList.add(null); // Set null to InboxMsgDoc object

			msg.setMessageDocs(inboxMsgDocList);
			response = inboxMsgService.addAttachment(msg, data.getBytes());
			logger.info("response is null: " + (response == null));

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 6: END");

			logger.info("TestCase - 7: START");
			logger.info("Desc: Correct value to each object");
			msg = new InboxMsg();
			msg.setId(1);
			msg.setOwnerUserId(4);
			msg.setStatus(InboxMsg.STATUS.C);

			String relativePath = msg.getOwnerUserId() + "/inbox/"
					+ msg.getId() + "/";
			InboxMsgDoc inboxMsgDoc = new InboxMsgDoc();
			inboxMsgDoc.setDocName(relativePath + documentname);
			inboxMsgDoc.setCreatedOn(new TSDate());
			inboxMsgDoc.setDocDesc("JUnit Test");
			inboxMsgDoc.setDocType(InboxMsgDoc.TYPE.BINARY);
			inboxMsgDoc.setMessage(msg);

			data = "Hello World from to InboxMsgService";
			inboxMsgDocList = new ArrayList<InboxMsgDoc>();
			inboxMsgDoc.setDocSize(data.getBytes().length);
			inboxMsgDocList.add(inboxMsgDoc);

			msg.setMessageDocs(inboxMsgDocList);
			response = inboxMsgService.addAttachment(msg, data.getBytes());
			logger.info("response is null: " + (response == null));

			documentId = msg.getMessageDocs().get(0).getDocId();
			//logger.info("documentId: " + documentId);

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() == 0);
			logger.info("TestCase - 7: END");

		} catch (ContentManagementServiceException e) {
			logger.error("ContentManagementServiceException has been occured: "
					+ e.getMessage());

		} catch (GIException e) {
			logger.error("GIException has been occured : " + e.getMessage());

		} finally {
			msg = null;
			response = null;
			logger.info("JUnit " + CLASS_NAME + ".addAttachment End");
		}
	}

	/**
	 * Method is used run JUnit test on getAttachment of
	 * InboxMsgServiceImpl.java class.
	 */
	@Test
	@Rollback(false)
	@Transactional
	public void getAttachment() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".getAttachment Begin");

		InboxMsgResponse response = null;
		byte[] attachment = null;

		try {

			logger.info("TestCase - 1: START");
			logger.info("Desc: set null to Document ID");
			response = new InboxMsgResponse();
			attachment = inboxMsgService.getAttachment(response, null);
			logger.info("attachment data is null: " + (attachment == null));

			Gson gson = new Gson();
			String json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 1: END");

			logger.info("TestCase - 2: START");
			logger.info("Desc: set correct value to Document ID");
			response = new InboxMsgResponse();

			InboxMsg inboxMsg = iInboxMsgRepository.findOne(1L);
			logger.info("inboxMsg is null: " + (inboxMsg == null));

			if (null != inboxMsg) {

				List<InboxMsgDoc> inboxMsgDocList = inboxMsg.getMessageDocs();
				logger.info("inboxMsgDocList is null: "
						+ (inboxMsgDocList == null));

				InboxMsgDoc inboxMsgDoc = null != inboxMsgDocList
						&& !inboxMsgDocList.isEmpty() ? inboxMsgDocList.get(0)
						: null;
				logger.info("inboxMsgDoc is null: " + (inboxMsgDoc == null));

				if (null != inboxMsgDoc) {

					String documentID = inboxMsgDoc.getDocId();
					//logger.info("documentID: " + documentID);

					attachment = inboxMsgService.getAttachment(response,
							documentID);
					logger.info("attachment data is null: "
							+ (attachment == null));

					gson = new Gson();
					json = gson.toJson(response);
					logger.info("InboxMsgResponse==> " + json);

				} else {
					logger.info("InboxMsgDoc has been empty.");
				}
			} else {
				logger.info("InboxMsg has been empty.");
			}
			assertTrue(response.getErrCode() == 0);
			logger.info("TestCase - 2: END");

		} catch (ContentManagementServiceException e) {
			logger.error("ContentManagementServiceException has been occured: "
					+ e.getMessage());

		} catch (GIException e) {
			logger.error("GIException has been occured : " + e.getMessage());

		} finally {
			attachment = null;
			response = null;
			logger.info("JUnit " + CLASS_NAME + ".getAttachment End");
		}
	}

	/**
	 * Method is used run JUnit test on searchAttachment of
	 * InboxMsgServiceImpl.java class.
	 */
	@Test
	@Rollback(false)
	@Transactional
	public void searchAttachment() throws GIException {

		logger.info("JUnit " + CLASS_NAME + ".searchAttachment Begin");

		InboxMsgResponse response = null;

		try {

			logger.info("TestCase - 1: START");
			logger.info("Desc: set null to InboxMsg");
			response = new InboxMsgResponse();
			response = inboxMsgService.searchAttachments(null, null, null);

			Gson gson = new Gson();
			String json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 1: END");

			logger.info("TestCase - 2: START");
			logger.info("Desc: set zero to OwnerUserId");
			response = new InboxMsgResponse();

			InboxMsg msg = new InboxMsg();
			msg.setStatus(InboxMsg.STATUS.C);
			response = inboxMsgService.searchAttachments(msg, null, null);

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 2: END");

			logger.info("TestCase - 3: START");
			logger.info("Desc: set null to inboxMsgDocList");
			response = new InboxMsgResponse();

			msg = new InboxMsg();
			msg.setOwnerUserId(4L);
			msg.setStatus(InboxMsg.STATUS.C);

			msg.setMessageDocs(null);
			response = inboxMsgService.searchAttachments(msg, null, null);

			gson = new Gson();
			json = gson.toJson(response);
			logger.info("InboxMsgResponse==> " + json);

			assertTrue(response.getErrCode() != 0);
			logger.info("TestCase - 3: END");

			logger.info("TestCase - 4: START");
			logger.info("Desc: set correct value to Document ID");
			response = new InboxMsgResponse();

			msg = new InboxMsg();
			msg.setOwnerUserId(4L);
			msg.setStatus(InboxMsg.STATUS.C);

			InboxMsgDoc inboxMsgDoc = new InboxMsgDoc();
			// FIXME: set existing file name of Alfresco API
			inboxMsgDoc
					.setDocId("workspace://SpacesStore/b7115fbd-af7c-492a-9e28-3fb164771927;1.0");

			ArrayList<InboxMsgDoc> inboxMsgDocList = new ArrayList<InboxMsgDoc>();
			inboxMsgDocList.add(inboxMsgDoc);
			msg.setMessageDocs(inboxMsgDocList);
			response = inboxMsgService.searchAttachments(msg, null, null);

			
			
			logger.info("InboxMsgResponse.getStatus()==> "
					+ response.getStatus());

			assertTrue(response.getErrCode() == 0);
			logger.info("TestCase - 4: END");

			logger.info("TestCase - 5: START");
			logger.info("Desc: set correct value to Document ID with setting startDate using Calendar");
			response = new InboxMsgResponse();

			msg = new InboxMsg();
			msg.setOwnerUserId(4L);
			msg.setStatus(InboxMsg.STATUS.C);

			inboxMsgDoc = new InboxMsgDoc();
			// FIXME: set existing file name of Alfresco API
			inboxMsgDoc
					.setDocId("workspace://SpacesStore/b7115fbd-af7c-492a-9e28-3fb164771927;1.0");

			inboxMsgDocList = new ArrayList<InboxMsgDoc>();
			inboxMsgDocList.add(inboxMsgDoc);
			msg.setMessageDocs(inboxMsgDocList);

			Calendar startDate = new GregorianCalendar();
			startDate.set(Calendar.DATE, -25);

			response = inboxMsgService.searchAttachments(msg, startDate, null);

			logger.info("InboxMsgResponse.getStatus()==> "
					+ response.getStatus());

			assertTrue(response.getErrCode() == 0);
			logger.info("TestCase - 5: END");

		} catch (GIException e) {
			logger.error("GIException has been occured : " + e.getMessage());

		} finally {
			response = null;
			logger.info("JUnit " + CLASS_NAME + ".searchAttachment End");
		}
	}
	
	/**
	 * @author chandran_g
	 * Method is used run JUnit test on getMessageClob of
	 * InboxMsgServiceImpl.java class.
	 */
	@Test
	@Rollback(false)
	@Transactional
	public void getMessageClob() {
		// Creating sample message containing CLOB
		InboxMsg msg = new InboxMsg();
		msg.setOwnerUserId(3);
		msg.setOwnerUserName("Exchange Admin");
		msg.setFromUserId(3);
		msg.setFromUserName("Exchange Admin");
		msg.setToUserIdList("6,7");
		msg.setPriority(InboxMsg.PRIORITY.L);
		msg.setStatus(InboxMsg.STATUS.C);
		msg.setMsgBody("Spring WebFlow Spring Web Flow SWF aims to be the best solution for the management of web application page flow. SWF integrates with existing frameworks like Open for extension... One of the overarching design principles in Spring Web MVC and in Spring in general is the Open for extension, closed for modification principle. The reason that this principle is being mentioned here is because a number of methods in the core classes in Spring Web MVC are marked final. This means of course that you as a developer cannot override these methods to supply your own behavior... this is by design and has not been done arbitrarily to annoy. The book Expert Spring Web MVC and Web Flow by Seth Ladd and others explains this principle and the reasons for adhering to it in some depth on page 117 first edition in the section entitled A Look At Design. If you dont have access to the aforementioned book, then the following article may be of interest the next time you find yourself going Gah Why cant I override this method? if indeed you ever do. Bob Martin, The OpenClosed Principle PDF Note that you cannot add advice to final methods using Spring MVC. This means it wont be possible to add advice to for example the AbstractController.handleRequest method. Refer to Section 6.6.1, Understanding AOP proxies for more information on AOP proxies and why you cannot add advice to final methods. Chapter 13. Web MVC framework 13.1. Introduction Springs Web MVC framework is designed around a DispatcherServlet that dispatches requests to handlers, with configurable handler mappings, view resolution, locale and theme resolution as well as support for upload files. The default handler is a very simple Controller interface, just offering a ModelAndView handleRequestrequest,response method. This can already be used for application controllers, but you will prefer the included implementation hierarchy, consisting of, for example AbstractController, AbstractCommandController and SimpleFormController. Application controllers will typically be subclasses of those. Note that you can choose an appropriate base class if you dont have a form, you dont need a form controller. This is a major difference to Struts. Spring Web MVC allows you to use any object as a command or form object  there is no need to implement a frameworkspecific interface or base class. Springs data binding is highly flexible for example, it treats type mismatches as validation errors that can be evaluated by the application, not as system errors. All this means that you dont need to duplicate your business objects properties as simple, untyped strings in your form objects just to be able to handle invalid submissions, or to convert the Strings properly. Instead, it is often preferable to bind directly to your business objects. This is another major difference to Struts which is built around required base classes such as Action and ActionForm. Compared to WebWork, Spring has more differentiated object roles. It supports the notion of a Controller, an optional command or form object, and a model that gets passed to the view. The model will normally include the command or form object but also arbitrary reference data; instead, a WebWork Action combines all those roles into one single object. WebWork does allow you to use existing business objects as part of your form, but only by making them bean properties of the respective Action class. Finally, the same Action instance that handles the request is used for evaluation and form population in the view. Thus, reference data needs to be modeled as bean properties of the Action too. These are arguably too many roles for one object. Springs view resolution is extremely flexible. A Controller implementation can even write a view directly to the response by returning null for the ModelAndView. In the normal case, a ModelAndView instance consists of a view name and a model Map, which contains bean names and corresponding objects like a command or form, containing reference data. View name resolution is highly configurable, either via bean names, via a properties file, or via your own ViewResolver implementation. The fact that the model the M in MVC is based on the Map interface allows for the complete abstraction of the view technology. Any renderer can be integrated directly, whether JSP, Velocity, or any other rendering technology. The model Map is simply transformed into an appropriate format, such as JSP request attributes or a Velocity template model. 13.1.1. Pluggability of other MVC implementations There are several reasons why some projects will prefer to use other MVC implementations. Many teams expect to leverage their existing investment in skills and tools. In addition, there is a large body of knowledge and experience avalailable for the Struts framework. Thus, if you can live with Struts architectural flaws, it can still be a viable choice for the web layer; the same applies to WebWork and other web MVC frameworks. If you dont want to use Springs web MVC, but intend to leverage other solutions that Spring offers, you can integrate the web MVC framework of your choice with Spring easily. Simply start up a Spring root application context via its ContextLoaderListener, and access it via its ServletContext attribute or Springs respective helper method from within a Struts or WebWork action. Note that there arent any plugins involved, so no dedicated integration is necessary. From the web layers point of view, youll simply use Spring as a library, with the root application context instance as the entry point. All your registered beans and all of Springs services can be at your fingertips even without Springs web MVC. Spring doesnt compete with Struts or WebWork in this scenario, it just addresses the many areas that the pure web MVC frameworks dont, from bean configuration to data access and transaction handling. So you are able to enrich your application with a Spring middle tier andor data access tier, even if you just want to use, for example, the transaction abstraction with JDBC or Hibernate. 13.1.2. Features of Spring Web MVC Springs web module provides a wealth of unique web support features, including Clear separation of roles  controller, validator, command object, form object, model object, DispatcherServlet, handler mapping, view resolver, etc. Each role can be fulfilled by a specialized object. Spring MVC, Struts, and JSF, in both servlet and portlet environments. If you have a business process or processes that would benefit from a conversational model as opposed to a purely request model, then SWF may be the solution. SWF allows you to capture logical page flows as selfcontained modules that are reusable in different situations, and as such is ideal for building web application modules that guide the user through controlled navigations that drive business processes. For more information about SWF, consult the Spring WebFlow site. Powerful and straightforward configuration of both framework and application classes as JavaBeans, including easy referencing across contexts, such as from web controllers to business objects and validators. Adaptability, nonintrusiveness. Use whatever controller subclass you need plain, command, form, wizard, multiaction, or a custom one for a given scenario instead of deriving from a single controller for everything. Reusable business code  no need for duplication. You can use existing business objects as command or form objects instead of mirroring them in order to extend a particular framework base class. Customizable binding and validation  type mismatches as applicationlevel validation errors that keep the offending value, localized date and number binding, etc instead of Stringonly form objects with manual parsing and conversion to business objects. Customizable handler mapping and view resolution  handler mapping and view resolution strategies range from simple URLbased configuration, to sophisticated, purposebuilt resolution strategies. This is more flexible than some web MVC frameworks which mandate a particular technique. Flexible model transfer  model transfer via a namevalue Map supports easy integration with any view technology. Customizable locale and theme resolution, support for JSPs with or without Spring tag library, support for JSTL, support for Velocity without the need for extra bridges, etc. A simple yet powerful JSP tag library known as the Spring tag library that provides support for features such as data binding and themes. The custom tags allow for maximum flexibility in terms of markup code. For information on the tag library descriptor, see the appendix entitled Appendix D, spring.tld A JSP form tag library, introduced in Spring 2.0, that makes writing forms in JSP pages much easier. For information on the tag library descriptor, see the appendix entitled Appendix E, springform.tld Beans whose lifecycle is scoped to the current HTTP request or HTTP Session. This is not a specific feature of Spring MVC itself, but rather of the WebApplicationContext containers that Spring MVC uses. These bean scopes are described in detail in the section entitled Section 3.4.4, The other scopes 13.2. The DispatcherServlet Springs web MVC framework is, like many other web MVC frameworks, requestdriven, designed around a central servlet that dispatches requests to controllers and offers other functionality facilitating the development of web applications. Springs DispatcherServlet however, does more than just that. It is completely integrated with the Spring IoC container and as such allows you to use every other feature that Spring has. The request processing workflow of the Spring Web MVC DispatcherServlet is illustrated in the following diagram. The patternsavvy reader will recognize that the DispatcherServlet is an expression of the Front Controller design pattern this is a pattern that Spring Web MVC shares with many other leading web frameworks. The requesting processing workflow in Spring Web MVC high level The DispatcherServlet is an actual Servlet it inherits from the HttpServlet base class, and as such is declared in the web.xml of your web application. Requests that you want the DispatcherServlet to handle will have to be mapped using a URL mapping in the same web.xml file. This is standard J2EE servlet configuration; an example of such a DispatcherServlet declaration and mapping can be found below. webapp servlet servletnameexampleservletname servletclassorg.springframework.web.servlet.DispatcherServletservletclass loadonstartup1loadonstartup servlet servletmapping servletnameexampleservletname urlpattern.formurlpattern servletmapping webapp In the example above, all requests ending with .form will be handled by the example DispatcherServlet. This is only the first step in setting up Spring Web MVC... the various beans used by the Spring Web MVC framework over and above the DispatcherServlet itself now need to be configured. As detailed in the section entitled Section 3.8, The ApplicationContext, ApplicationContext instances in Spring can be scoped. In the web MVC framework, each DispatcherServlet has its own WebApplicationContext, which inherits all the beans already defined in the root WebApplicationContext. These inherited beans defined can be overridden in the servletspecific scope, and new scopespecific beans can be defined local to a given servlet instance. Context hierarchy in Spring Web MVC The framework will, on initialization of a DispatcherServlet, look for a file named [servletname]servlet.xml in the WEBINF directory of your web application and create the beans defined there overriding the definitions of any beans defined with the same name in the global scope. Consider the following DispatcherServlet servlet configuration in the web.xml file. webapp ... servlet servletnamegolfingservletname servletclassorg.springframework.web.servlet.DispatcherServletservletclass loadonstartup1loadonstartup servlet servletmapping servletnamegolfingservletname NEWSLETTER Search DocumNeEnWtaStiLoEnTTER SUBSCRIPTIONall urlpattern.dourlpattern servletmapping webapp With the above servlet configuration in place, you will need to have a file called WEBINFgolfingservlet.xml in your application; this file will contain all of your Spring Web MVCspecific components beans. The exact location of this configuration file can be changed via a servlet initialization parameter see below for details. The WebApplicationContext is an extension of the plain ApplicationContext that has some extra features necessary for web applications. It differs from a normal ApplicationContext in that it is capable of resolving themes see Section 13.7, Using themes, and that it knows which servlet it is associated with by having a link to the ServletContext. The WebApplicationContext is bound in the ServletContext, and by using static methods on the RequestContextUtils class you can always lookup the WebApplicationContext in case you need access to it. The Spring DispatcherServlet has a couple of special beans it uses in order to be able to process requests and render the appropriate views. These beans are included in the Spring framework and can be configured in the WebApplicationContext, just as any other bean would be configured. Each of those beans is described in more detail below. Right now, well just mention them, just to let you know they exist and to enable us to go on talking about the DispatcherServlet. For most of the beans, sensible defaults are provided so you dont initially have to worry about configuring them. Table 13.1. Special beans in the WebApplicationContext Bean type Explanation Controllers Controllers are the components that form the C part of the MVC. Handler mappings Handler mappings handle the execution of a list of pre and postprocessors and controllers that will be executed if they match certain criteria for instance a matching URL specified with the controller View resolvers View resolvers are components capable of resolving view names to views Locale resolver A locale resolver is a component capable of resolving the locale a client is using, in order to be able to offer internationalized views Theme resolver A theme resolver is capable of resolving themes your web application can use, for example, to offer personalized layouts multipart file resolver A multipart file resolver offers the functionality to process file uploads from HTML forms Handler exception resolvers Handler exception resolvers offer functionality to map exceptions to views or implement other more complex exception handling code When a DispatcherServlet is set up for use and a request comes in for that specific DispatcherServlet, said DispatcherServlet starts processing the request. The list below describes the complete process a request goes through when handled by a DispatcherServlet The WebApplicationContext is searched for and bound in the request as an attribute in order for the controller and other elements in the process to use. It is bound by default under the key DispatcherServlet.WEB_APPLICATION_CONTEXT_ATTRIBUTE. The locale resolver is bound to the request to let elements in the process resolve the locale to use when processing the request rendering the view, preparing data, etc. If you dont use the resolver, it wont affect anything, so if you dont need locale resolving, you dont have to use it. The theme resolver is bound to the request to let elements such as views determine which theme to use. The theme resolver does not affect anything if you dont use it, so if you dont need themes you can just ignore it. If a multipart resolver is specified, the request is inspected for multiparts; if multiparts are found, the request is wrapped in a MultipartHttpServletRequest for further processing by other elements in the process. See the section entitled Section 13.8.2, Using the MultipartResolver for further information about multipart handling. An appropriate handler is searched for. If a handler is found, the execution chain associated with the handler preprocessors, postprocessors, and controllers will be executed in order to prepare a model for rendering. If a model is returned, the view is rendered. If no model is returned which could be due to a pre or postprocessor intercepting the request, for example, for security reasons, no view is rendered, since the request could already have been fulfilled. Exceptions that are thrown during processing of the request get picked up by any of the handler exception resolvers that are declared in the WebApplicationContext. Using these exception resolvers allows you to define custom behaviors in case such exceptions get thrown. The Spring DispatcherServlet also has support for returning the lastmodificationdate, as specified by the Servlet API. The process of determining the last modification date for a specific request is straightforward the DispatcherServlet will first lookup an appropriate handler mapping and test if the handler that is found implements the interface LastModified interface. If so, the value of the long getLastModifiedrequest method of the LastModified interface is returned to the client. You can customize Springs DispatcherServlet by adding context parameters in the web.xml file or servlet initialization parameters. The possibilities are listed below. parameters. The possibilities are listed below. Table 13.2. DispatcherServlet initialization parameters Parameter Explanation contextClass Class that implements WebApplicationContext, which will be used to instantiate the context used by this servlet. If this parameter isnt specified, the XmlWebApplicationContext will be used. contextConfigLocation String which is passed to the context instance specified by contextClass to indicate where contexts can be found. The string is potentially split up into multiple strings using a comma as a delimiter to support multiple contexts in case of multiple context locations, of beans that are defined twice, the latest takes precedence. namespace the namespace of the WebApplicationContext. Defaults to [servletname]servlet. 13.3. Controllers The notion of a controller is part of the MVC design pattern more specifically it is the C in MVC. Controllers provide access to the application behavior which is typically defined by a service interface. Controllers interpret user input and transform such input into a sensible model which will be represented to the user by the view. Spring has implemented the notion of a controller in a very abstract way enabling a wide variety of different kinds of controllers to be created. Spring contains formspecific controllers, commandbased controllers, and controllers that execute wizardstyle logic, to name but a few. Springs basis for the controller architecture is the org.springframework.web.servlet.mvc.Controller interface, the source code for which is listed below. public interface Controller  Process the request and return a ModelAndView object which the DispatcherServlet will render. ModelAndView handleRequest HttpServletRequest request, HttpServletResponse response throws Exception; As you can see, the Controller interface defines a single method that is responsible for handling a request and returning an appropriate model and view. These three concepts are the basis for the Spring MVC implementation  ModelAndView and Controller. While the Controller interface is quite abstract, Spring offers a lot of Controller implementations out of the box that already contain a lot of the functionality you might need. The Controller interface just defines the most basic responsibility required of every controller; namely handling a request and returning a model and a view. 13.3.1. AbstractController and WebContentGenerator To provide a basic infrastructure, all of Springs various Controller inherit from AbstractController, a class offering caching support and, for example, the setting of the mimetype. Table 13.3. Features offered by the AbstractController Feature Explanation supportedMethods indicates what methods this controller should accept. Usually this is set to both GET and POST, but you can modify this to reflect the method you want to support. If a request is received with a method that is not supported by the controller, the client will be informed of this expedited by the throwing of a ServletException. requiresSession indicates whether or not this controller requires a HTTP session to do its work. If a session is not present when such a controller receives a request, the user is informed of this by a ServletException being thrown. synchronizeSession use this if you want handling by this controller to be synchronized on the users HTTP session. cacheSeconds when you want a controller to generate a caching directive in the HTTP response, specify a positive integer here. By default the value of this property is set to 1 so no caching directives will be included in the generated response. useExpiresHeader tweaks your controllers to specify the HTTP 1.0 compatible Expires header in the generated response. By default the value of this property is true. useCacheHeader tweaks your controllers to specify the HTTP 1.1 compatible CacheControl header in the generated response. By default the value of this property is true. When using the AbstractController as the baseclass for your controllers you only have to override the handleRequestInternalHttpServletRequest, HttpServletResponse method, implement your logic, and return a ModelAndView object. Here is short example consisting of a class and a declaration in the web application context. package samples; public class SampleController extends AbstractController  public ModelAndView handleRequestInternal HttpServletRequest request, HttpServletResponse response throws Exception  ModelAndView mav  new ModelAndViewhello; mav.addObjectmessage, Hello World; return mav; bean idsampleController classsamples.SampleController property namecacheSeconds value120 bean The above class and the declaration in the web application context is all you need besides setting up a handler mapping see the section entitled Section 13.4, Handler mappings to get this very simple controller working. This controller will generate caching directives telling the client to cache things for 2 minutes before rechecking. This controller also returns a hardcoded view which is typically considered bad practice. 13.3.2. Other simple controllers Although you can extend AbstractController, Spring provides a number of concrete implementations which offer functionality that is commonly used in simple MVC applications. The ParameterizableViewController is basically the same as the example above, except for the fact that you can specify the view name that it will return in the web application context and thus remove the need to hardcode the viewname in the Java class. The UrlFilenameViewController inspects the URL and retrieves the filename of the file request and uses that as a viewname. For example, the filename of httpwww.springframework.orgindex.html request is index. 13.3.3. The MultiActionController Spring offers a multiaction controller with which you aggregate multiple actions into one controller, thus grouping functionality together. The multiaction controller lives in a separate package  org.springframework.web.servlet.mvc.multiaction  and is capable of mapping requests to method names and then invoking the right method name. Using the multiaction controller is especially handy when you have a lot of common functionality in one controller, but want to have multiple entry points to the controller, for example, to tweak behavior. Table 13.4. Features offered by the MultiActionController Feature Explanation delegate there are two usagescenarios for the MultiActionController. Either you subclass the MultiActionController and specify the methods that will be resolved by the MethodNameResolver on the subclass in which case you dont need to set the delegate, or you define a delegate object, on which methods resolved by the MethodNameResolver will be invoked. If you choose this scenario, you will have to define the delegate using this configuration parameter as a collaborator. methodNameResolver the MultiActionController needs a strategy to resolve the method it has to invoke, based on the incoming request. This strategy is defined by the MethodNameResolver interface; the MultiActionController exposes a property sp that you can supply a resolver that is capable of doing that. Methods defined for a multiaction controller need to conform to the following signature anyMeaningfulName can be replaced by any methodname public [ModelAndView | Map | void] anyMeaningfulNameHttpServletRequest, HttpServletResponse [, Exception | AnyObject]; Please note that method overloading is not allowed since it would confuse the MultiActionController. Furthermore, you can define exception handlers capable of handling exceptions that are thrown by the methods you specify. The optional Exception argument can be any exception, as long as its a subclass of java.lang.Exception or java.lang.RuntimeException. The optional AnyObject argument can be any class. Request parameters will be bound onto this object for convenient consumption. Find below some examples of valid MultiActionController method signatures. The standard signature mirrors the Controller interface method. public ModelAndView doRequestHttpServletRequest, HttpServletResponse This signature accepts a Login argument that will be populated bound with parameters stripped from the request public ModelAndView doLoginHttpServletRequest, HttpServletResponse, Login The signature for an Exception handling method. public ModelAndView processExceptionHttpServletRequest, HttpServletResponse, IllegalArgumentException This signature has a void return type see the section entitled Section 13.11, Convention over configuration below. public void goHomeHttpServletRequest, HttpServletResponse This signature has a Map return type see the section entitled Section 13.11, Convention over configuration below. public Map doRequestHttpServletRequest, HttpServletResponse The MethodNameResolver is responsible for resolving method names based on the request coming in. Find below details about the three MethodNameResolver implementations that Spring provides out of the box. ParameterMethodNameResolver  capable of resolving a request parameter and using that as the method name httpwww.sf.netindex.view?testParamtestIt will result in a method testItHttpServletRequest, HttpServletResponse being called. The paramName property specifies the request parameter that is to be inspected. InternalPathMethodNameResolver  retrieves the filename from the request path and uses that as the method name httpwww.sf.nettesting.view will result in a method testingHttpServletRequest, HttpServletResponse being called. PropertiesMethodNameResolver  uses a userdefined properties object with request URLs mapped to method names. When the properties contain indexwelcome.htmldoIt and a request to indexwelcome.html comes in, the doItHttpServletRequest, HttpServletResponse method is called. This method name resolver works with the PathMatcher, so if the properties contained welcom?.html, it would also have worked Here are a couple of examples. First, an example showing the ParameterMethodNameResolver and the delegate property, which will accept requests to URLs with the parameter method included and set to retrieveIndex bean idparamResolver classorg....mvc.multiaction.ParameterMethodNameResolver property nameparamName valuemethod bean bean idparamMultiController classorg....mvc.multiaction.MultiActionController property namemethodNameResolver refparamResolver property namedelegate refsampleDelegate bean bean idsampleDelegate classsamples.SampleDelegate ## together with public class SampleDelegate  public ModelAndView retrieveIndexHttpServletRequest req, HttpServletResponse resp  return new ModelAndViewindex, date, new LongTimeShifterUtil.currentTimeMillis; When using the delegates shown above, we could also use the PropertiesMethodNameResolver to match a couple of URLs to the method we defined bean idpropsResolver classorg....mvc.multiaction.PropertiesMethodNameResolver property namemappings value indexwelcome.htmlretrieveIndex notwelcome.htmlretrieveIndex user?.htmlretrieveIndex value property bean bean idparamMultiController classorg....mvc.multiaction.MultiActionController property namemethodNameResolver refpropsResolver property namedelegate refsampleDelegate bean 13.3.4. Command controllers Springs command controllers are a fundamental part of the Spring Web MVC package. Command controllers provide a way to interact with data objects and dynamically bind parameters from the HttpServletRequest to the data object specified. They perform a somewhat similar.");

		InboxMsgResponse response = null;
		try {
			response = inboxMsgService.saveMessage(msg);
		} catch (Exception e) {
			logger.info("Exception has occured : ", e);
		}

		logger.info("Clob data inserted");
		List<InboxMsg> msgList = response.getMessageList();

		long messageId;
		if (null != msgList) {
			messageId = msgList.get(0).getId();
			logger.info("Clob data inserted for message id " + messageId);

			String clob = inboxMsgService.getMessageCLOB(messageId);
			
			if(null != clob){
			//logger.info("Clob data found =" + clob);
			assertTrue(clob.length() > 4000);
			}
			else{
				logger.info("Clob data not found for message id = " + messageId);
			}
		}
		else {
			logger.info("Error inserting sample data to test getMessageClob.");
		}
	}
}
