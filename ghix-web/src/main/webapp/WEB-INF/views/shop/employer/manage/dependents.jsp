<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jstl/fmt_rt" prefix="fmt"%>

<div class="gutter10">
<div class="row-fluid">
	<ul class="breadcrumb txt-left">
		 <li><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}" />" > &lt; Back</a> <span class="divider">|</span></li>
		<li><a href="<c:url value="/shop/employer/" />">Employees</a> <span class="divider">/</span></li>
		<li><a href="<c:url value="/shop/employer/manage/list" />">Manage List</a> <span class="divider">/</span></li>
		<li><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}" />">${employee.name}</a> <span class="divider">/</span></li>		
		<li class="active">View Dependents</li>
	</ul>
	<h3 id="skip">${employee.name} <spring:message  code='label.dependents'/></h3>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <div class="accordion graysidebar" id="accordion2">
          <div class="accordion-group">
            <div class="accordion-heading graydrkbg"> <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne"> <spring:message  code='label.aboutthisemp'/></a> </div>
            <div id="collapseOne" class="accordion-body collapse in">
              <div class="accordion-inner">
                <ul class="nav nav-list">
                  <li><a href="<c:url value="/shop/employer/manage/employeeinfo/${employee.id}?appId=${empappID}" />">Employee Information</a></li>
                  <li class="active"><a>Dependent Information (${dependentCount})</a></li>
                  <li><a href="<c:url value="/shop/employer/manage/enrollment/${employee.id}?appId=${empappID}" />">Employee Coverage</a></li>
                 <%--  <li><a href="<c:url value="/shop/employer/manage/notifications/${employee.id}" />">Notification History</a></li> --%>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <h4 class="graydrkbg"><i class="icon-cog icon-white"></i> <spring:message  code='label.actions'/></h4>
        <ul class="nav nav-list">
        <%-- <c:if test="${(employee.status != 'ENROLLED')}">
          <li><a href="javascript:window.location.href ='/hix/shop/employer/manage/editdependents?id=${employee.id}' "> <i class="icon-pencil"></i> <spring:message  code='label.edit'/></a></li>
        </c:if> --%>
        
        <li><a href="javascript:window.location.href ='/hix/shop/employer/manage/editdependents?id=${employee.id}&appId=${empappID}' "> <i class="icon-pencil"></i> <spring:message  code='label.edit'/></a></li>
        
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <h4 class="graydrkbg gutter10"><spring:message  code='label.dependentsumm'/></h4>
        <div class="gutter10">
          <table class="table table-border-none">
            <tr>
              <td width="40%" align="right"><spring:message  code='label.associateemp'/></td>
              <td>${employee.name}</td>
            </tr>
          </table>
          <c:if test="${employeespouse != null}">
          <h4 class="lightgray"><spring:message  code='label.spouse'/></h4>
          <table class="table table-border-none">
            <tr>
              <td width="40%" align="right"><spring:message  code='label.emprname'/></td>
              <td>${employeespouse.firstName} &nbsp; ${employeespouse.lastName}</td>
            </tr>
            <tr>
              <td width="40%" align="right"><spring:message  code='label.dob'/></td>
              <td>
	              <c:if test="${employeespouse.dob != null}"> 
	              	<fmt:formatDate type="date" value="${employeespouse.dob}"  pattern="MM/dd/yyyy" />
	              </c:if>
               </td>
            </tr>
            <tr>
              <td width="40%" align="right"><spring:message  code='label.homeAddress'/></td>
              <td>
              <c:if test="${employeespouse.location != null}"> 
             	 ${employeespouse.location.address1}<br> ${employeespouse.location.city}, ${employeespouse.location.state} ${employeespouse.location.zip}</td>
              </c:if>
            </tr>
          </table>
          </c:if>
          <c:if test="${employee.childCount > 0 }">
          <c:forEach items="${employeechilds}" var="empchilds" varStatus="status">
          
          <h4 class="lightgray">Child/Ward ${status.count}</h4>
          <table class="table table-border-none">
            <tr>
              <td width="40%" align="right"><spring:message  code='label.emprname'/></td>
              <td>${empchilds.firstName} &nbsp; ${empchilds.lastName}</td>
            </tr>
            <tr>
              <td width="40%" align="right"><spring:message  code='label.dob'/></td>
              <td> <c:if test="${empchilds.dob != null}"> 
              <fmt:formatDate type="date" value="${empchilds.dob}" pattern="MM/dd/yyyy" />
              </c:if>
              </td>
            </tr>
            <tr>
              <td width="40%" align="right"><spring:message  code='label.homeAddress'/></td>
              <td>
              <c:if test="${empchilds.location != null}"> 
              		${empchilds.location.address1}<br> ${empchilds.location.city}, ${empchilds.location.state} ${empchilds.location.zip}</td>
              </c:if>
            </tr>
          </table>
          
          </c:forEach>
          </c:if>
         </div>
        <!--gutter10--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  </div> <!--gutter10--> 
