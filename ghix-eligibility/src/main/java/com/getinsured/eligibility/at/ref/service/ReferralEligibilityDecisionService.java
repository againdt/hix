package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;

/**
 * @author chopra_s
 * 
 */
public interface ReferralEligibilityDecisionService {
	String NF_LINK_HANDLER = "NF_LINK_HANDLER";
	String SUCCESS_HANDLER = "SUCCESS_HANDLER";
	String ERROR_HANDLER = "ERROR_HANDLER";
	String NF_CONVERSION_HANDLER = "NF_CONVERSION_HANDLER";
	String NF_APTC_UPDATE_HANDLER = "NF_APTC_UPDATE_HANDLER";
	
	boolean execute(long applicationId, boolean cmrAutoLinking);

	boolean execute(long applicationId, ApplicationExtension applicationExtension, boolean cmrAutoLinking);
}
