package com.getinsured.eligibility.rrv.service.impl;

import com.getinsured.eligibility.model.RrvSubmission;
import com.getinsured.eligibility.rrv.model.RrvHousehold;
import com.getinsured.eligibility.rrv.model.RrvMember;
import com.getinsured.eligibility.rrv.model.RrvMetadata;
import com.getinsured.eligibility.rrv.model.RrvRenewalRequest;
import com.getinsured.eligibility.rrv.repository.RrvSubmissionRepository;
import com.getinsured.eligibility.rrv.service.RrvService;
import com.getinsured.eligibility.rrv.service.SingleStreamlinedApplicationService;
import com.getinsured.eligibility.rrv.utility.LocalDateSerializer;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.SecureHttpClient;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.enums.OtherStateOrFederalProgramType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("rrvService")
@SuppressWarnings("Duplicates")
public class RrvServiceImpl implements RrvService {
    private static final Logger logger = LoggerFactory.getLogger(RrvServiceImpl.class);

    private static final String applicationTypeNonFinancial = "NF";
    private static final String applicationTypeFinancial = "FA";

    private static final String lookupValueTypeRelationship = "RELATIONSHIP";
    private static final String lookupValueLabelSpouse = "Spouse";

    private static final String applicantPersonTypePrimary = "PTF";
    private static final String taxFilerCodePrimary = "PRIMARY";
    private static final String taxFilerCodeSpouse = "SPOUSE";
    private static final String taxFilerCodeDependent = "DEPENDENT";

    private static final String statusPending = "PENDING";
    private static final String statusUpdated = "UPDATED";

    private static final String statusVerified = "VERIFIED";
    private static final String statusNotVerified = "NOT_VERIFIED";

    private static final Set<String> statusToUpdateSet = Stream.of(statusVerified, statusNotVerified).collect(Collectors.toCollection(HashSet::new));

    private static final String flagY = "Y";

    private static final String medicareType = "Medicare";

    private final LookupService lookupService;
    private final SingleStreamlinedApplicationService singleStreamlinedApplicationService;
    private final SsapApplicantRepository ssapApplicantRepository;
    private final SsapApplicationRepository ssapApplicationRepository;
    private final RrvSubmissionRepository rrvSubmissionRepository;
    private final GhixRestTemplate ghixRestTemplate;

    @Autowired
    RrvServiceImpl(LookupService lookupService, SingleStreamlinedApplicationService singleStreamlinedApplicationService, SsapApplicantRepository ssapApplicantRepository, SsapApplicationRepository ssapApplicationRepository, RrvSubmissionRepository rrvSubmissionRepository, GhixRestTemplate ghixRestTemplate) {
        this.lookupService = lookupService;
        this.singleStreamlinedApplicationService = singleStreamlinedApplicationService;
        this.ssapApplicantRepository = ssapApplicantRepository;
        this.ssapApplicationRepository = ssapApplicationRepository;
        this.rrvSubmissionRepository = rrvSubmissionRepository;
        this.ghixRestTemplate = ghixRestTemplate;
    }

    /**
     * Method used for getting Batch ID from DVS. We can submit multiple RRV requests using the same Batch ID.
     * @author nguyen_h
     * @return Batch ID
     */
    @Override
    public long getBatchId() throws Exception{

        CloseableHttpClient closeableHttpClient = SecureHttpClient.getHttpClient();
        HttpGet httpget = new HttpGet(GhixEndPoints.HubIntegrationEndpoints.CREATE_RRV_BATCH_ID);

        String rrvBatchRespString;
        JSONObject rrvBatchRespJson;
        long batchId = 0L;

        try {
            CloseableHttpResponse closeableHttpResponse = closeableHttpClient.execute(httpget);
            HttpEntity httpEntity = closeableHttpResponse.getEntity();

            rrvBatchRespString = EntityUtils.toString(httpEntity);

            rrvBatchRespJson = new JSONObject(rrvBatchRespString);

            batchId = rrvBatchRespJson.getLong("batch_id");

            logger.info("Batch Id: {}", rrvBatchRespJson.getLong("batch_id"));
        }
        catch(JSONException | IOException e){
            logger.error(e.getMessage());
            throw e;
        }

        return batchId;
    }

    /**
     * Method to find all eligible SSAP Application ID's which fit the parameter criterias.
     * @author nguyen_h
     * @param coverageYear SSAP_APPLICATIONS.COVERAGE_YEAR
     * @param applicationStatus SSAP_APPLICATIONS.APPLICATION_STATUS
     * @param pageable This method uses pagination since DVS request will timeout after 30 seconds
     * @return Eligible SSAP Application ID's
     */
    @Override
    public Page<Long> findAllEligibleSsapApplication(long coverageYear, String applicationStatus, Pageable pageable) {

        return ssapApplicationRepository.findAllEligibleSsapApplication(coverageYear, applicationStatus, pageable);
    }

    /**
     * Method to process a list of SSAP Application ID's for RRV Submission.
     * @author nguyen_h
     * @param applicationIdList List of SSAP Application ID's that have the proper coverage year and status
     * @param batchId Batch ID to use for the RRV Submissions
     * @param coverageYear Coverage year for the applications
     * @return JSON String representation of request sent to DVS
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public String process(List<Long> applicationIdList, long batchId, long coverageYear) {

        logger.info("Processing RRV Batch {}", batchId);

        //Household Map to keep track of households since we loop based on applicants
        Map<BigDecimal, RrvHousehold> householdMap = new HashMap<>();

        //Getting list of applicants that fit the criteria based on applications
        List<SsapApplicant> ssapApplicantList = getApplicantBasedOnApplicationId(applicationIdList);

        List<SsapApplicant> filteredSsapApplicantList = new ArrayList<>();

        for(SsapApplicant ssapApplicant : ssapApplicantList){
            if(rrvSubmissionRepository.existsByHouseholdId(coverageYear, ssapApplicant.getSsapApplication().getCmrHouseoldId().longValue())){
                continue;
            }
            filteredSsapApplicantList.add(ssapApplicant);
        }

        buildHouseholds(filteredSsapApplicantList, householdMap);

        LocalDateSerializer dateSerializer = new LocalDateSerializer();
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, dateSerializer).create();

        RrvRenewalRequest renewalRequest = new RrvRenewalRequest();
        List<RrvHousehold> householdsList = new ArrayList<>();

        for (Map.Entry<BigDecimal, RrvHousehold> entry : householdMap.entrySet()) {
            householdsList.add(entry.getValue());
        }

        //Pass all households
        renewalRequest.setHouseholds(householdsList);

        RrvMetadata metadata = new RrvMetadata();
        metadata.setBatchId(batchId);
        metadata.setHouseholdCount(householdsList.size());

        //Pass metadata
        renewalRequest.setMetadata(metadata);

        //Building request json
        String jsonString = gson.toJson(renewalRequest);

        try {
            sendRrvRequest(jsonString);

            insertRrvSubmissionHouseholds(coverageYear, batchId, householdsList);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }

        return jsonString;
    }

    /**
     * This method is used in ghix-batch to process a list of Ssap Application ID's.
     * @author nguyen_h
     * @param ssapApplicationIds List of SSAP Application ID's that have the proper coverage year and status
     * @param batchId Batch ID to use for the RRV Submissions
     * @param coverageYear Coverage year for the applications
     * @return List of Rrv Submission records that may or may not have been changed
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public List<RrvSubmission> rrvBatchProcess(List<? extends Long> ssapApplicationIds, long batchId, long coverageYear) throws Exception {

        logger.info("Processing RRV Batch {}", batchId);

        //Household Map to keep track of households since we loop based on applicants
        Map<BigDecimal, RrvHousehold> householdMap = new HashMap<>();

        //Getting list of applicants that fit the criteria based on applications
        List<SsapApplicant> ssapApplicantList = getApplicantBasedOnApplicationIds(ssapApplicationIds);

        buildHouseholds(ssapApplicantList, householdMap);

        LocalDateSerializer dateSerializer = new LocalDateSerializer();
        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, dateSerializer).create();

        RrvRenewalRequest renewalRequest = new RrvRenewalRequest();
        List<RrvHousehold> householdsList = new ArrayList<>();

        for (Map.Entry<BigDecimal, RrvHousehold> entry : householdMap.entrySet()) {
            householdsList.add(entry.getValue());
        }

        //Pass all households
        renewalRequest.setHouseholds(householdsList);

        RrvMetadata metadata = new RrvMetadata();
        metadata.setBatchId(batchId);
        metadata.setHouseholdCount(householdsList.size());

        //Pass metadata
        renewalRequest.setMetadata(metadata);

        //Building request json
        String jsonString = gson.toJson(renewalRequest);

        try {
            sendRrvRequest(jsonString);

            return prepareRrvSubmission(coverageYear, batchId, householdsList);

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
    }

    /**
     * Method to check status of pending RRV submissions.
     * We will only check submissions at least 3 days old since there's no point due to minimum SLA's.
     * @author nguyen_h
     * @param coverageYear Coverage year to check
     */
    @Override
    public void checkStatusOfPendingSubmissions(long coverageYear) {

        int page = 0;
        int pageSize = 5000;

        Page<RrvSubmission> rrvSubmissionPage;

        do {
            rrvSubmissionPage = rrvSubmissionRepository.findAllPendingSubmissionsPageable(coverageYear, new PageRequest(page, pageSize));

            List<RrvSubmission> rrvSubmissionList = rrvSubmissionPage.getContent();

            try {
                getVerificationResult(rrvSubmissionList);
            } catch(Exception e){
                logger.error(e.getMessage());
            }

            page++;
        } while (!rrvSubmissionPage.isLast());

    }

    /**
     * This method is used in ghix-batch to get results back from DVS for a given list of Household ID's.
     * The method will also  update SSAP_APPLICATION, SSAP_APPLICANTS and RRV_SUBMISSION.
     * @author nguyen_h
     * @param rrvSubmissionList List of RRV Submission records that have at least one TDS source pending
     */
    @Transactional(rollbackFor = {IOException.class, JSONException.class})
    @Override
    public void getVerificationResult(List<? extends RrvSubmission> rrvSubmissionList) throws Exception {

        CloseableHttpClient closeableHttpClient = SecureHttpClient.getHttpClient();

        try {
            for (RrvSubmission rrvSubmission : rrvSubmissionList) {
                tryUpdatingResponseFromRRV(closeableHttpClient, rrvSubmission);
            }
        } catch(Exception e){
            logger.error(e.getMessage());
            throw e;
        }
    }

    private void tryUpdatingResponseFromRRV(CloseableHttpClient closeableHttpClient, RrvSubmission rrvSubmission) throws Exception {
        HttpGet httpget = null;
        CloseableHttpResponse closeableHttpResponse = null;

        try{
            String url = String.format("%s/%s", GhixEndPoints.HubIntegrationEndpoints.GET_VERIFICATION_RESULT, rrvSubmission.getHouseholdId());
            httpget = new HttpGet(url);

            closeableHttpResponse = closeableHttpClient.execute(httpget);
            HttpEntity httpEntity = closeableHttpResponse.getEntity();

            String rrvBatchRespString = EntityUtils.toString(httpEntity);
            logger.debug("Response: " + rrvBatchRespString);

            JSONObject rrvResponseJsonObject = new JSONObject(rrvBatchRespString);
            JSONArray householdsJsonArray = rrvResponseJsonObject.getJSONArray("households");
            JSONObject householdJsonObject = householdsJsonArray.getJSONObject(0);

            long householdId = householdJsonObject.getLong("householdId");

            List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findLatestSsapApplicationByHouseholdId(new BigDecimal(householdId));
            SsapApplication ssapApplication = ssapApplicationList.get(0);

            String incomeVerified = householdJsonObject.getString("incomeVerified");
            String failureToReconcileFlag = householdJsonObject.getString("ftrVerified");

            JSONArray householdMembersJsonArray = householdJsonObject.getJSONArray("householdMembers");

            HashMap<SsapApplicant, JSONObject> ssapApplicantHashMap = new HashMap<>();

            for (int i = 0; i < householdMembersJsonArray.length(); i++) {
                JSONObject householdMember = householdMembersJsonArray.getJSONObject(i);
                String applicantId = householdMember.getString("applicantId");

                SsapApplicant ssapApplicant = ssapApplicantRepository.findBySsapApplicantGuidAndSsapApplicationId(applicantId, ssapApplication.getId());

                if(null != ssapApplicant) {
                    ssapApplicantHashMap.put(ssapApplicant, householdMember);
                }
            }

            updateRrvSubmissionSsapApplicant(rrvSubmission, ssapApplicantHashMap, ssapApplication, incomeVerified, failureToReconcileFlag);
            logger.debug(rrvBatchRespString);
        }catch(Exception e){
            logger.error("RRV Error processing: {}", e);
            rrvSubmission.setEqfxStatus("ERROR");
            rrvSubmissionRepository.save(rrvSubmission);
        }finally{
            if (closeableHttpResponse != null) {
                closeableHttpResponse.close();
            }
            if (httpget != null) {
                httpget.releaseConnection();
            }
        }
    }

    /**
     * This method is used to build households based on a list of Ssap Applicants.
     * @author nguyen_h
     * @param ssapApplicantList Input of Ssap Applicants that need to be grouped based by household
     * @param householdMap HashMap of each household. Key is the Household ID and values are RRV Household POJO
     */
    private void buildHouseholds(List<SsapApplicant> ssapApplicantList, Map<BigDecimal, RrvHousehold> householdMap){
        for (SsapApplicant ssapApplicant : ssapApplicantList) {

            BigDecimal householdId = ssapApplicant.getSsapApplication().getCmrHouseoldId();
            String applicationType = applicationTypeNonFinancial;

            String financialAssistanceFlag = ssapApplicant.getSsapApplication().getFinancialAssistanceFlag();

            if (flagY.equalsIgnoreCase(financialAssistanceFlag)) {
                applicationType = applicationTypeFinancial;
            }

            RrvHousehold household;

            if (householdMap.containsKey(householdId)) {
                household = householdMap.get(householdId);
            } else {
                household = new RrvHousehold();
                householdMap.put(householdId, household);

                //Calculate household income for financial applications
                if (applicationTypeFinancial.equalsIgnoreCase(applicationType)) {
                    Long currentApplicationId = ssapApplicant.getSsapApplication().getId();

                    try {
                        SingleStreamlinedApplication application = singleStreamlinedApplicationService.getSingleStreamlinedApplication(currentApplicationId);

                        Double householdIncomeDouble = application.getTaxHousehold().get(0).getHouseHoldIncome();

                        household.setHouseholdIncome(new BigDecimal(householdIncomeDouble).setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                    catch(Exception e){
                        household.setHouseholdIncome(new BigDecimal(0).setScale(2, BigDecimal.ROUND_HALF_UP));
                    }
                }

                household.setHouseholdId(householdId);
            }

            List<RrvMember> memberList;

            if (null != household.getHouseholdMembers()) {
                memberList = household.getHouseholdMembers();
            } else {
                memberList = new ArrayList<>();
                household.setHouseholdMembers(memberList);
            }

            RrvMember member = new RrvMember();
            member.setApplicantId(ssapApplicant.getApplicantGuid());
            member.setFirstName(ssapApplicant.getFirstName());
            member.setMiddleName(ssapApplicant.getMiddleName());
            member.setLastName(ssapApplicant.getLastName());
            member.setTaxIdentifier(ssapApplicant.getSsn());

            member.setDateOfBirth(((java.sql.Date) ssapApplicant.getBirthDate()).toLocalDate());

            //Set tax filer code for financial applications
            if (applicationTypeFinancial.equalsIgnoreCase(applicationType)) {
                if (null != ssapApplicant.getPersonType()) {
                    String applicantPersonType = ssapApplicant.getPersonType().getPersonType();
                    String taxFilerCode;

                    if (applicantPersonType.contains(applicantPersonTypePrimary)) {
                        taxFilerCode = taxFilerCodePrimary;
                    } else {
                        List<LookupValue> lookupValues = lookupService.getLookupValueList(lookupValueTypeRelationship);

                        LookupValue spouseLookupValue = lookupValues.stream()
                                .filter(lookupValue -> lookupValueLabelSpouse.equalsIgnoreCase(lookupValue.getLookupValueLabel()))
                                .findAny()
                                .orElse(null);

                        String relationshipCode = ssapApplicant.getRelationship();
                        if (spouseLookupValue != null && spouseLookupValue.getLookupValueCode().equalsIgnoreCase(relationshipCode)) {
                            taxFilerCode = taxFilerCodeSpouse;
                        } else {
                            taxFilerCode = taxFilerCodeDependent;
                        }
                    }

                    member.setTaxFilerCode(taxFilerCode);
                }
            }

            memberList.add(member);
        }
    }

    /**
     * This method is used to find all applicants that fit a criteria of given eligibility type and indicator based on given Ssap Application ID list.
     * @author nguyen_h
     * @param applicationIdList List of Ssap Application ID's
     * @return List of Ssap Applicants that fit criteria
     */
    private List<SsapApplicant> getApplicantBasedOnApplicationId(List<Long> applicationIdList) {

        return ssapApplicantRepository.findAllEligibleBasedOnApplicationId(applicationIdList);
    }

    /**
     * This method is used in ghix-batch to find all applicants that fit a criteria
     * of given eligibility type and indicator based on given Ssap Application ID list.
     * @author nguyen_h
     * @param applicationIdList List of Ssap Application ID's
     * @return List of Ssap Applicants that fit criteria
     */
    private List<SsapApplicant> getApplicantBasedOnApplicationIds(List<? extends Long> applicationIdList) {

        return ssapApplicantRepository.findAllEligibleBasedOnApplicationIds(applicationIdList);
    }

    /**
     * This method is used to send RRV request to DVS.
     * @author nguyen_h
     * @param jsonString JSON String representation of RRV Request
     * @throws Exception
     */
    private void sendRrvRequest(String jsonString) throws Exception {

        CloseableHttpClient closeableHttpClient = SecureHttpClient.getHttpClient();
        HttpPost httpPost = null;
        CloseableHttpResponse closeableHttpResponse = null;

        try {
            logger.info("Sending RRV Request");
            httpPost = new HttpPost(GhixEndPoints.HubIntegrationEndpoints.SUBMIT_RRV_BATCH_RECORDS);
            StringEntity httpBody = new StringEntity(jsonString);

            httpPost.setEntity(httpBody);

            closeableHttpResponse = closeableHttpClient.execute(httpPost);

            StatusLine statusLine = closeableHttpResponse.getStatusLine();

            if (HttpStatus.SC_ACCEPTED != statusLine.getStatusCode()) {

                logger.error("RRV Submission returns: " + statusLine.getStatusCode());
                throw new Exception("RRV Submission not 202");
            }
        }catch(Exception e){
            logger.error("RRV Sending Request Error: {}", e);

        } finally{
            if (closeableHttpResponse != null) {
                closeableHttpResponse.close();
            }
            if (httpPost != null) {
                httpPost.releaseConnection();
            }
        }
    }

    /**
     * This method is used to update SSAP_APPLICANT and RRV_SUBMISSION when changes are detected from DVS.
     * @author nguyen_h
     * @param rrvSubmission RRV_SUBMISSION record
     * @param ssapApplicantHashMap HashMap of SSAP_APPLICANTS and the JSON Object returned from DVS for that applicant
     * @param ssapApplication SSAP_APPLICATION for given applicants
     * @param incomeVerified Income verification for a household. However, this is updated on the applicant level.
     * @param failureToReconcileFlag Failure to Reconcile flag is set at a household level in SSAP_APPLICATIONS.APPLICATION_DATA JSON
     * @throws Exception
     */
    private void updateRrvSubmissionSsapApplicant(RrvSubmission rrvSubmission, Map<SsapApplicant, JSONObject> ssapApplicantHashMap, SsapApplication ssapApplication, String incomeVerified, String failureToReconcileFlag) throws Exception {
        boolean rrvChange = false;
        boolean updatedFTRFlag = false;
        String ifsvStatus = rrvSubmission.getIfsvStatus();
        String ssacStatus = rrvSubmission.getSsacStatus();
        String eqfxStatus = rrvSubmission.getEqfxStatus();
        String smecStatus = rrvSubmission.getSmecStatus();

        SingleStreamlinedApplication singleStreamlinedApplication = getSingleStreamlinedApplication(ssapApplication);

        for(Map.Entry<SsapApplicant, JSONObject> entry : ssapApplicantHashMap.entrySet()) {

            SsapApplicant ssapApplicant = entry.getKey();
            JSONObject householdMember = entry.getValue();

            boolean ssapApplicantChange = false;

            String ssnVerification = householdMember.getString("ssnVerification").toUpperCase();
            String deathVerification = householdMember.getString("deathVerification").toUpperCase();
            String nmecVerification = householdMember.getString("nmecVerification").toUpperCase();

            if (!statusUpdated.equalsIgnoreCase(ifsvStatus) &&
                    statusToUpdateSet.contains(incomeVerified) &&
                    statusToUpdateSet.contains(failureToReconcileFlag)) {
                ssapApplicantChange = true;
                rrvChange = true;
                rrvSubmission.setIfsvStatus(statusUpdated);

                if(ssapApplicant.getPersonType() != null) {
                    String applicantPersonType = ssapApplicant.getPersonType().getPersonType();

                    if (flagY.equalsIgnoreCase(ssapApplication.getFinancialAssistanceFlag()) &&
                            applicantPersonType.contains(applicantPersonTypePrimary)) {
                        ssapApplicant.setIncomeVerificationStatus(incomeVerified);
                    }
                }

                if(!updatedFTRFlag) {
                    boolean failureToReconcileBoolean = false;

                    if (statusVerified.equalsIgnoreCase(failureToReconcileFlag)) {
                        failureToReconcileBoolean = true;
                    }

                    setFailureToReconcileFlag(ssapApplication, singleStreamlinedApplication, failureToReconcileBoolean);

                    updatedFTRFlag = true;
                }
            }

            if (!statusUpdated.equalsIgnoreCase(ssacStatus) &&
                    statusToUpdateSet.contains(deathVerification) &&
                    statusToUpdateSet.contains(ssnVerification)) {
                ssapApplicantChange = true;
                rrvChange = true;
                rrvSubmission.setSsacStatus(statusUpdated);
                ssapApplicant.setDeathStatus(deathVerification);
                ssapApplicant.setSsnVerificationStatus(ssnVerification);
            }

            if (!statusUpdated.equalsIgnoreCase(eqfxStatus)) {
                //Currently Not Using
                rrvSubmission.setEqfxStatus(statusUpdated);
            }

            if (!statusUpdated.equalsIgnoreCase(smecStatus) &&
                    statusToUpdateSet.contains(nmecVerification)) {
                ssapApplicantChange = true;
                rrvChange = true;
                rrvSubmission.setSmecStatus(statusUpdated);
                ssapApplicant.setNonEsiMecVerificationStatus(nmecVerification);
                setMedicareJson(ssapApplication, singleStreamlinedApplication, ssapApplicant.getApplicantGuid(), nmecVerification);
            }

            if(ssapApplicantChange){
                ssapApplicantRepository.save(ssapApplicant);
            }
        }

        if (rrvChange) {
            rrvSubmission.setLastUpdateDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            rrvSubmissionRepository.save(rrvSubmission);

            if(statusUpdated.equalsIgnoreCase(rrvSubmission.getIfsvStatus()) &&
                    statusUpdated.equalsIgnoreCase(rrvSubmission.getSsacStatus()) &&
                    statusUpdated.equalsIgnoreCase(rrvSubmission.getEqfxStatus()) &&
                    statusUpdated.equalsIgnoreCase(rrvSubmission.getSmecStatus())){

                ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/updateEligibilityStatus",
                        "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, ssapApplication.getCaseNumber());
            }
        }
    }

    /**
     * This method is used to set the Failure to Reconcile flag in SSAP_APPLICATIONS.APPLICATION_DATA JSON.
     * @author nguyen_h
     * @param ssapApplication SSAP_APPLICATION we want to update
     * @param singleStreamlinedApplication APPLICATION_DATA to update SSAP_APPLICATION with
     * @param failureToReconcile Failure to Reconcile flag
     * @throws Exception
     */
    private void setFailureToReconcileFlag(SsapApplication ssapApplication, SingleStreamlinedApplication singleStreamlinedApplication, boolean failureToReconcile) throws Exception{
        logger.debug("Setting FailureToReconcile Flag");

        if(ssapApplication != null && singleStreamlinedApplication != null){

            List<TaxHousehold> taxHouseholdList = singleStreamlinedApplication.getTaxHousehold();
            TaxHousehold taxHousehold = taxHouseholdList.get(0);

            if(taxHousehold != null){
                taxHousehold.setFailureToReconcile(failureToReconcile);
                singleStreamlinedApplicationService.saveSingleStreamlinedApplication(ssapApplication, singleStreamlinedApplication);

            }else{
                logger.error("Empty SSAP_APPLICATION.APPLICATION_DATA.TAX_HOUSEHOLD for Household ID: {}", ssapApplication.getCmrHouseoldId());
            }
        }else{
            logger.error("SSAP_APPLICATION/APPLICATION_DATA Not Found");
            throw new Exception("SSAP_APPLICATION/APPLICATION_DATA NOT FOUND");
        }
    }

    /**
     * This method is used to set ___ in SSAP_APPLICATIONS.APPLICATION_DATA JSON.
     * @author nguyen_h
     * @param ssapApplication SSAP_APPLICATION we want to update
     * @param singleStreamlinedApplication APPLICATION_DATA to update SSAP_APPLICATION with
     * @param applicantGuid Applicant GUID to Update
     * @param nmecVerification Medicare verification status
     */
    private void setMedicareJson(SsapApplication ssapApplication, SingleStreamlinedApplication singleStreamlinedApplication, String applicantGuid, String nmecVerification){

        TaxHousehold taxHousehold = singleStreamlinedApplication.getTaxHousehold().get(0);

        if(taxHousehold != null) {
            HouseholdMember householdMember = null;

            for (HouseholdMember hm : taxHousehold.getHouseholdMember()) {
                if (applicantGuid.equals(hm.getApplicantGuid())) {
                    householdMember = hm;
                    break;
                }
            }

            if (householdMember != null) {
                HealthCoverage healthCoverage = householdMember.getHealthCoverage();
                CurrentOtherInsurance currentOtherInsurance = healthCoverage.getCurrentOtherInsurance();

                List<OtherStateOrFederalProgram> otherStateOrFederalPrograms = currentOtherInsurance.getOtherStateOrFederalPrograms();

                OtherStateOrFederalProgram medicare = null;

                for (OtherStateOrFederalProgram otherStateOrFederalProgram : otherStateOrFederalPrograms) {
                    if (medicareType.equalsIgnoreCase(otherStateOrFederalProgram.getName())) {
                        medicare = otherStateOrFederalProgram;
                        break;
                    }
                }

                boolean medicareEligible = statusNotVerified.equalsIgnoreCase(nmecVerification);
                
                currentOtherInsurance.setHasNonESI(medicareEligible);
                healthCoverage.setCurrentlyHasHealthInsuranceIndicator(medicareEligible);

                if (medicare != null) {
                    medicare.setEligible(medicareEligible);
                } else{
                    medicare = new OtherStateOrFederalProgram();
                    medicare.setName(medicareType);
                    medicare.setType(OtherStateOrFederalProgramType.MEDICARE);
                    medicare.setEligible(medicareEligible);

                    otherStateOrFederalPrograms.add(medicare);
                }

                singleStreamlinedApplicationService.saveSingleStreamlinedApplication(ssapApplication, singleStreamlinedApplication);
            }
        }

    }

    /**
     * This method is used to get APPLICATION_DATA JSON from SSAP_APPLICATION.
     * @author nguyen_h
     * @param ssapApplication SSAP_APPLICATION we want APPLICATION_DATA for
     * @return APPLICATION_DATA JSON as SingleStreamlinedApplication Object
     * @throws Exception
     */
    private SingleStreamlinedApplication getSingleStreamlinedApplication(SsapApplication ssapApplication) throws Exception{
        SingleStreamlinedApplication singleStreamlinedApplication;

        if(ssapApplication != null) {
            singleStreamlinedApplication = singleStreamlinedApplicationService.getSingleStreamlinedApplication(ssapApplication);
        }
        else{
            logger.error("SSAP_APPLICATION Not Found");
            throw new Exception("SSAP_APPLICATION NOT FOUND");
        }
        return singleStreamlinedApplication;
    }

    /**
     * This method is used to insert new RRV_SUBMISSION records.
     * @author nguyen_h
     * @param coverageYear Coverage Year for records
     * @param batchId Batch ID for records
     * @param rrvHouseholds List of households
     */
    private void insertRrvSubmissionHouseholds(Long coverageYear, Long batchId, List<RrvHousehold> rrvHouseholds) {
        List<RrvSubmission> rrvSubmissionList = new ArrayList<>();
        for (RrvHousehold rrvHousehold : rrvHouseholds) {
            RrvSubmission rrvSubmission = new RrvSubmission();
            rrvSubmission.setCoverageYear(coverageYear);
            rrvSubmission.setRrvBatchId(batchId);
            rrvSubmission.setHouseholdId(rrvHousehold.getHouseholdId().longValue());
            rrvSubmission.setIfsvStatus(statusPending);
            rrvSubmission.setSsacStatus(statusPending);
            rrvSubmission.setEqfxStatus(statusUpdated); //We're not doing EQFX currently
            rrvSubmission.setSmecStatus(statusPending);
            rrvSubmission.setCreationDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            rrvSubmissionList.add(rrvSubmission);
        }

        rrvSubmissionRepository.save(rrvSubmissionList);
    }

    /**
     * This method is used to save a list of RRV_SUBMISSION records.
     * @author nguyen_h
     * @param rrvSubmissions RRV_SUBMISSION records to save
     */
    @Override
    public void saveRrvSubmissions(List<? extends RrvSubmission> rrvSubmissions) {
        rrvSubmissionRepository.save(rrvSubmissions);
    }

    /**
     * This method is used in ghix-batch to return the RRV_SUBMISSION records to be written.
     * @author nguyen_h
     * @param coverageYear Coverage Year for records
     * @param batchId Batch ID for records
     * @param rrvHouseholds List of households
     * @return List of RRV_SUBMISSION records
     */
    private List<RrvSubmission> prepareRrvSubmission(Long coverageYear, Long batchId, List<RrvHousehold> rrvHouseholds) {
        List<RrvSubmission> rrvSubmissionList = new ArrayList<>();
        for (RrvHousehold rrvHousehold : rrvHouseholds) {
            RrvSubmission rrvSubmission = new RrvSubmission();
            rrvSubmission.setCoverageYear(coverageYear);
            rrvSubmission.setRrvBatchId(batchId);
            rrvSubmission.setHouseholdId(rrvHousehold.getHouseholdId().longValue());
            rrvSubmission.setIfsvStatus(statusPending);
            rrvSubmission.setSsacStatus(statusPending);
            rrvSubmission.setEqfxStatus(statusUpdated); //We're not doing EQFX currently
            rrvSubmission.setSmecStatus(statusPending);
            rrvSubmission.setCreationDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
            rrvSubmissionList.add(rrvSubmission);
        }

            return rrvSubmissionList;
    }
}
