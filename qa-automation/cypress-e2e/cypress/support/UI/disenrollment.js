Cypress.Commands.add("disenrollMedicalCustomGrouping", () => {
    for (const eligGroup of cy.h.groups.eligible) {
        cy.navigateTo(cy.MN.URL + '/indportal#/customGrouping')
            .find('#rightpanel > .ps-detail > .tabs > li > #tab-0')
        //Wait for "Shop for Medical Plans for 2020" to be clickable
        cy.wait(3000)
        cy.get('#rightpanel > .ps-detail > .tabs > li > #tab-0').click()
        cy.get('#aid_btn_cancel_coverage0').click()
        cy.get('#disaffordability').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
    }
})

Cypress.Commands.add("disenrollDentalCustomGrouping", () => {
    cy.navigateTo(cy.MN.URL + '/indportal#/customGrouping')
    cy.get('#rightpanel > .ps-detail > .tabs > li > #tab-1').click()
    cy.wait(3000)
    cy.get('#aid_dental_btn_cancel_coverage_nrg_0').click()
    cy.get('#disaffordability').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
})

Cypress.Commands.add("disenrollMedicalMyEligibility", () => {
    for (const eligGroup of cy.h.groups.eligible) {
        cy.navigateTo(cy.MN.URL + '/indportal#')
        cy.get('.row-fluid > #sidebar > .nav > li:nth-child(2) > a').click()
        cy.wait(3000)
        cy.get('#currentApp > .row-fluid > .alert > .col-xs-12 > #planSummary').click()
        cy.get('body > #body-individual > #container-wrap > #container').click()
        cy.get('.activeEnrollment:nth-child(1) > .ng-scope > .row-fluid > .span6 > .center > #disenrollPlan > .ng-scope > .ng-scope').click()
        cy.get('.modal-content > .modal-body > .form-horizontal > .margin30-l > .radio:nth-child(1)').click()
        cy.get('#disenrollDialog > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
    }
})

Cypress.Commands.add("disenrollDentalMyEligibility", () => {
    cy.navigateTo(cy.MN.URL + '/indportal#')
    cy.get('.row-fluid > #sidebar > .nav > li:nth-child(2) > a').click()
    cy.wait(3000)
    cy.get('#currentApp > .row-fluid > .alert > .col-xs-12 > #planSummary').click()
    cy.get('[ng-if="!activePlanDetails.validCoverageDental"]').click()
    cy.get('.modal-content > .modal-body > .form-horizontal > .margin30-l > .radio:nth-child(1)').click()
    cy.get('#disenrollDialog > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
})

Cypress.Commands.add("disenrollAllMyEligibility", () => {
    cy.navigateTo(cy.MN.URL + '/indportal#')
    cy.get('.row-fluid > #sidebar > .nav > li:nth-child(2) > a').click()
    cy.wait(3000)
    cy.get('#currentApp > .row-fluid > .alert > .col-xs-12 > #planSummary').click()
    cy.get('.row-fluid > #rightpanel > .ng-scope > .pull-left > #disenrollBoth').click()
    cy.get('.modal-content > .modal-body > .form-horizontal > .margin30-l > .radio:nth-child(1)').click()
    cy.get('#disenrollDialog > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
})


Cypress.Commands.add("disenrollHealthMyEnrollments", () => {
    for (const eligGroup of cy.h.groups.eligible) {
        cy.navigateTo(cy.MN.URL + '/indportal#')
        cy.get('.row-fluid > #sidebar > .nav > li:nth-child(3) > a').click()
        cy.wait(3000)
        cy.get('#activeEnrollment_0 > .ng-scope > .enrollmentTemplate > .margin20-r > #disenrollFromPlan > .btn > #disenrollFromPlan_Health_02').click()
        cy.get('.modal-content > .modal-body > .form-horizontal > .margin30-l > .radio:nth-child(1)').click()
        cy.get('#disenrollDialog > .modal-dialog > .modal-content > .modal-footer > #aid_continue').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
        cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
    }
})

Cypress.Commands.add("disenrollDentalMyEnrollments", () => {
    cy.navigateTo(cy.MN.URL + '/indportal#')
    cy.get('.row-fluid > #sidebar > .nav > li:nth-child(3) > a').click()
    cy.wait(3000)
    cy.get('.enrollmentTemplate > .margin20-r > #disenrollFromPlan > .btn > #disenrollFromPlan_Dental_02').click()
    cy.get('.modal-content > .modal-body > .form-horizontal > .margin30-l > .radio:nth-child(1)').click()
    cy.get('#disenrollDialog > .modal-dialog > .modal-content > .modal-footer > #aid_continue').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn-primary').click()
    cy.get('.in > .modal-dialog > .modal-content > .modal-footer > .btn').click()
})






