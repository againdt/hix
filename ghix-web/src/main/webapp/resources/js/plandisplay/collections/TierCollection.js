(function (collections, pagination, model) {
	collections.Tiers = Backbone.Collection.extend({
		model : model,
		url : 'gettiers?'+(Math.random()*10),
		
		/**
		 * @param resp the response returned by the server
		 * @returns (Array) plans
		 */
		parse : function (resp) {
			var plans = resp;
			return plans; 
		}
	});
	
	_.extend(collections.Tiers.prototype, pagination);
})(App.collections, App.backbone.Pagination, App.models.Tier);