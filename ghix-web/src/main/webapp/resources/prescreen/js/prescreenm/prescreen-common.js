/*
 * Script to include common functionality in prescreen page
 */

//Method to convert form data to JSON
$.fn.serializeObject = function() {
	var o = {};
	var a = this.serializeArray();
	$.each(a, function() {
		if (o[this.name] !== undefined) {
			if (!o[this.name].push) {
				o[this.name] = [ o[this.name] ];
			}
			o[this.name].push(this.value || '');
		} else {
			o[this.name] = this.value || '';
		}
	});
	return o;
};

//Prescreen Request model
PrescreenRequestModel = Backbone.Model.extend({

	initialize : function() {
	},

	defaults: {
		prescreenProfile: null,
		prescreenHousehold : null,
		prescreenIncomes : null,
		prescreenDeductions : null,
		planId : '123',
		aptcValue : 0.0,
		isAnyMemberDisabled : false,
		prescreenRecordId : 0,
		benchmarkPlanPremium:0.0,
		currentTab:'1'
  	},

});

var prescreenRequestSaved;
var userFirstTimeNavigation = true;
//Method to generate household section dynamically
function generateHouseholdSection(){

	var dependent =
	'<div class="accordion-group dependentAccordion removeAccordion" id="dependent_FID_accordion_div">'+
	'   <div class="accordion-heading">'+
	'      <a href="#dependent_FID_accordion" data-parent="#householdAccordion"'+
	'         data-toggle="collapse" class="accordion-toggle"><i class="icon-chevron-right"></i>&nbsp; Dependent FID'+
	'      </a>'+
	'   </div>'+
	'   <div class="accordion-body collapse" id="dependent_FID_accordion"'+
	'      style="height: 0px;">'+
	'      <div class="accordion-inner">'+
	'         <div class="row-fluid">'+
	'            <div class="span3">'+
	'               <label>Date of Birth</label>'+
	'					<input id="dependent_FID_dob" name="dependent_FID_dob" type="text" maxlength="10"'+
	'                  		placeholder="mm/dd/yyyy" class="input-small dob">'+
	'					<div id="dependent_FID_dob_error"></div>'+
	'					<div id="dependent_FID_dob_message"></div>'+
	'            </div>'+
	//Is Pregnant
	'            <div class="span3" style="display: none;">'+
	'               <label>Pregnant?</label> '+
	'               <label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span>'+
	'									<input type="radio" class="fancy" value="true" name="isDependent_FID_pregnant" style="opacity: 0;" >'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  Yes'+
	'               </label>'+
	'				<label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span class="checked">'+
	'									<input type="radio" class="fancy pregnant-no" value="false" name="isDependent_FID_pregnant" style="opacity: 0;" checked="checked" >'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  No'+
	'               </label>'+
	'            </div>'+
	//Is Seeking Coverage
	'            <div class="span3">'+
	'               <label>Seeking Insurance Coverage?</label> '+
	'               <label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span class="checked">'+
	'									<input type="radio" class="fancy seeking-yes" value="true" name="isDependent_FID_seekingCoverage" style="opacity: 0;" checked="checked">'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  Yes'+
	'               </label>'+
	'				<label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span>'+
	'									<input type="radio" class="fancy seeking-no" value="false" name="isDependent_FID_seekingCoverage" style="opacity: 0;">'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  No'+
	'               </label>'+
	'            </div>'+
	//Relationship
	'            <div class="span3 seekingCoverage" style="display: none;">'+
	'               <label>Relationship?</label> '+
	'               <label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span class="checked">'+
	'									<input type="radio" class="fancy rel-child" value="child" name="dependent_FID_relationship" style="opacity: 0;" checked="checked">'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  Child'+
	'               </label>'+
	'				<label class="radio" style="display: inline-block;">'+
	'                  <div class="radio">'+
	'                     	<span>'+
	'					  		<div class="radio">'+
	'								<span>'+
	'									<input type="radio" class="fancy" value="relative" name="dependent_FID_relationship" style="opacity: 0;">'+
	'								</span>'+
	'							</div>'+
	'						</span>'+
	'                  </div>'+
	'                  Relative'+
	'               </label>'+
	'            </div>'+

	//Medicare Eligible Div
	'			<div class="span3" style="display:none">'+
	'				<label>'+
	'					<span data-html="true" data-content="In general, individuals who are 65 or older cannot seek premium tax credits for private plans because they are likely eligible for Medicare, a federally administered program for the elderly." data-placement="right" data-toggle="popover" data-container="body" class="haspopover">2 Medicare Eligible <i class="icon-ok icon-white icon-question-sign"></i> </span>'+
	'				</label>'+
	'			</div>'+

	'         </div>'+
	'      </div>'+
	'   </div>'+
	'</div>';



	//Populating first name from My Profile page
	if($('#claimerName').val()){
		$('#selfName').html('<i class="icon-chevron-right"></i>&nbsp;'+$('#claimerName').val());
	}
	else{
		$('#selfName').html('<i class="icon-chevron-right"></i>&nbsp'+'Claiming Tax Filer');
	}

	//Showing spouse accordion if married
	if($('input:checkbox[name="isMarried"]').is(':checked')){
		$('#spouseAccordionDiv').show();
	}
	else{
		$('#spouseAccordionDiv').hide();
	}

	//Adding/Removing dependents
	var totalDependents = parseInt($.trim($('#numberOfDependents').val()));
	$('.dependentAccordion').addClass('removeAccordion');
	if(totalDependents != 0){
		for(var i=0; i<totalDependents; i++){

			var depndentDiv = $('#dependent_'+(i+1)+'_accordion_div');
			if(depndentDiv.html() != null){
				depndentDiv.removeClass('removeAccordion');
				continue;
			}

			$('#householdAccordion').append(dependent.replace(/FID/g,i+1));
			$('#dependent_'+(i+1)+'_accordion_div').removeClass('removeAccordion');

			//Adding validations to DOB
			$("#dependent_"+(i+1)+"_dob").rules("add",{
					required:true,
					dob:true,
					ageMore26:true,
					messages : {
						required:"<span class='alert alert-error'>Date of Birth is required</span>",
						ageMore26:"<span class='alert alert-error'>Dependents must be under 26 for this version of the estimator</span>"
					}
				}
			);

		}
		$('.removeAccordion').remove();

	}
	else{
		$('.dependentAccordion').remove();
	}

	$('input[type=radio]', '#householdForm').click(function(){
		var radio = $(this).attr('name');
		$('input[name='+radio+']').closest('span').removeAttr('class');
		$(this).closest('span').attr('class','checked');
	});

	//If age is more than 65, change coverage checkbox to "Medicare Eligible."

	/* toggle accordion icons */

	$('#householdAccordion').siblings('.accordion-heading').find('.accordion-toggle').find('i').addClass('icon-chevron-down');

	$('.accordion-body').on('show',function(){
	     $(this).siblings('.accordion-heading').find('.accordion-toggle').find('i').addClass('icon-chevron-down');
	});

	$('.accordion-body').on('hide',function(){
	     $(this).siblings('.accordion-heading').find('.accordion-toggle').find('i').removeClass('icon-chevron-down');
	});

	$(".dob").mask("99/99/9999");
}


//Method to generate income section dynamically
function generateIncomeSection(){

	//$('#calculateincome').html('');
	var refinedIncome = $('#taxHouseholdIncome').val().replace(/\$/g, '');
	$('#refineincome').attr('data-value',formatIncome(refinedIncome));
	$('#refineincome').attr('data-text',formatIncome(refinedIncome));
	$('#refineincome').html(formatIncome(refinedIncome));
	$('#refineincome').text(formatIncome(refinedIncome));
	$('#refineincome').value = formatIncome(refinedIncome) ;

	var name = "Claiming Tax Filer";
	if($('#claimerName').val()){
		name = $('#claimerName').val();
	}

	//Income Table first Row
	var firstRow =
		'<table class="table incometable" >'+
		'   <thead>'+
		'      <tr>'+
		'         <th>#</th>'+
		'         <th>Modify</th>'+
		'         <th>Action</th>'+
		'      </tr>'+
		'   </thead>'+
		'   <tbody>'+
		'      <tr>'+
		'         <td>1</td>'+
		//'         <td>'+name+'</td>'+
		//'         <td class="sliderRow"><a href="#income-modal_FID" data-toggle="modal" >'+
		'         <td class="sliderRow">'+
		'				<a href="#" id="claimantNameIncome" onclick="hs.htmlExpand(this, { contentId: \'highslide-html_FID\' } )"'+
		'           			class="highslide">'+name+'&#39;s Income &nbsp;'+
		'								<i class="icon icon-edit"></i></a></td>'+
		'         <td><span id="selfIncome">'+$('#taxHouseholdIncome').val()+' </span>&nbsp;<a href="#" onclick="javascript:clearIncome(\'highslide-html_FID\')">'+
		'					Clear</a></td>'+
		'      </tr>';

	//Template for income popup
	var incomePopup =
		//'<div class="modal hide fade bigModal" id="income-modal_FID" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="min-width:460px;">'+
		'<div class="highslide-html-content removePopup" id="highslide-html_FID" style="min-width:460px;">'+
		//'   <div class="modal-header">'+
		'   <div class="highslide-header">'+
		'      <h4> Calculate <span id="memberFID"></span> Income</h4>'+
		'      <ul>'+
		'         <li class="highslide-move">'+
		'            <a href="#" onclick="return false">Move</a>'+
		'         </li>'+
		'         <li class="highslide-close">'+
		'            <a href="#" onclick="return hs.close(this)">Close</a>'+
		'         </li>'+
		'      </ul>'+
		'   </div>'+
		'   <div class="highslide-body" >'+
		'      <!--open -->'+
		'      <ul style="list-style:none; margin:10px">'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Job income includes wages or salaries from an employer for any full-time or part-time jobs." data-placement="right" data-toggle="popover" class="haspopover">Job Income <i class="icon-question-sign"></i></span>'+
		'                  <p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Job income includes wages or salaries from an employer for any full-time or part-time jobs. </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="jobIncome_FID" name="jobIncome_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="jobIncomeSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'               <div id="jobIncome_FID_error"></div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Self-employment income includes earnings that you get from a business that you own or from your work as an independent contractor. Please provide net income (profits once expenses are paid). If the costs for this self-employment are more than the amount expected to earn, you can provide a negative number." data-placement="right" data-toggle="popover" class="haspopover">Self-employment <i class="icon-question-sign"></i></span>'+
		'                  <p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Self-employment income includes earnings that you get from a business that you own or from your work as an independent contractor. Please provide net income (profits once expenses are paid). If the costs for this self-employment are more than the amount expected to earn, you can provide a negative number. </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="selfEmployment_FID" name="selfEmployment_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="selfEmploymentSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p>'+
		'                  <p><span data-content="Social Security Benefits include Social Security retirement, disability, and survivors benefits." data-placement="right" data-toggle="popover" class="haspopover">Social Security <i class="icon-question-sign"></i></span>'+
		'                     Benefits '+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Social Security Benefits include Social Security retirement, disability, and survivors benefits. </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="socialSecurityBenefits_FID" name="socialSecurityBenefits_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="socialSecurityBenefitsSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Unemployment compensation generally includes any amounts received under the unemployment compensation laws of the United States or of a state. It includes railroad unemployment compensation benefits, but not worker\'s compensation benefits." data-placement="right" data-toggle="popover" class="haspopover">Unemployment <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Unemployment compensation generally includes any amounts received under the unemployment compensation laws of the United States or of a state. It includes railroad unemployment compensation benefits, but not worker\'s compensation benefits.</div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="unemployment_FID" name="unemployment_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="unemploymentSlider_FID" class="slider-info income-slider" ></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Retirement/pension income includes amounts received from a retirement account, pension, or as a distribution from a retirement investment (even if recipient is not retired). " data-placement="right" data-toggle="popover" class="haspopover">Retirement/pension <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Retirement/pension income includes amounts received from a retirement account, pension, or as a distribution from a retirement investment (even if recipient is not retired). </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="retirementPension_FID" name="retirementPension_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="retirementPensionSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="An increase in the value of a capital asset (investment or real estate) that gives it a higher worth than the purchase price. The gain is not realized until the asset is sold." data-placement="right" data-toggle="popover" class="haspopover">Capital gains <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">An increase in the value of a capital asset (investment or real estate) that gives it a higher worth than the purchase price. The gain is not realized until the asset is sold. </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="capitalGains_FID" name="capitalGains_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="capitalGainsSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Examples of investment income are interest and dividends." data-placement="right" data-toggle="popover" class="haspopover">Investment income <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Examples of investment income are interest and dividends.  </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="investmentIncome_FID" name="investmentIncome_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="investmentIncomeSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Alimony includes spousal support from a divorce but does not include child support. " data-placement="right" data-toggle="popover" class="haspopover">Alimony received <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Alimony includes spousal support from a divorce but does not include child support.  </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="alimonyReceived_FID" name="alimonyReceived_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="alimonyReceivedSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Income from property that you rent out and from royalty property (such as oil and gas wells)." data-placement="right" data-toggle="popover" class="haspopover">Rental or royalty income<i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Income from property that you rent out and from royalty property (such as oil and gas wells). </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="rentalRoyaltyIncome_FID" name="rentalRoyaltyIncome_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="rentalRoyaltyIncomeSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Only disclose profit (i.e., after subtracting costs). " data-placement="right" data-toggle="popover" class="haspopover">Farming or fishing income <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Only disclose profit (i.e., after subtracting costs).  </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="farmingFishingIncome_FID" name="farmingFishingIncome_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="farmingFishingIncomeSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'         <li>'+
		'            <div class="row-fluid">'+
		'               <div class="span4">'+
		'                  <p><span data-content="Other income includes canceled debts, court awards or jury duty pay." data-placement="right" data-toggle="popover" class="haspopover">Other income  <i class="icon-question-sign"></i></span>'+
		'                  </p>'+
		'                  <div class="popover fade bottom in" style="display:none">'+
		'                     <div class="arrow"></div>'+
		'                     <div class="popover-content">Other income includes canceled debts, court awards or jury duty pay.  </div>'+
		'                  </div>'+
		'               </div>'+
		'               <div class="span2">'+
		'                  <input type="text" id="otherIncome_FID" name="otherIncome_FID" maxlength="9" class="input-mini incomeType">'+
		'               </div>'+
		'               <div class="span6">'+
		'                  <div id="eq1" class="gutter10">'+
		'                     <div id="otherIncomeSlider_FID" class="slider-info income-slider"></div>'+
		'                  </div>'+
		'               </div>'+
		'            </div>'+
		'         </li>'+
		'      </ul>'+
		'      <!--open-->'+
		'   </div>'+
		'   <div style="height:60px">'+
		'      <div class="row-fluid">'+
		'         <div class="right">'+
		'            <div class="gutter10">'+
		'               <a href="#" class="right btn btn-info" onclick="return hs.close(this)">Done</a>'+
		//'				<a href="#" class="right btn btn-info" data-dismiss="modal" aria-hidden="true">Done</a>'+
		'            </div>'+
		'         </div>'+
		'      </div>'+
		'      <div>'+
		'         <span class="highslide-resize" title="Resize">'+
		'         <span></span>'+
		'         </span>'+
		'      </div>'+
		'   </div>'+
		'</div>';

	$('.highslide-html-content').addClass('removePopup');

	var row = $('#highslide-html_self').html();

	if(row == null){
		$('#calculateincomeBox').append(firstRow.replace(/FID/g,'self'));
		$('#incomePopUps').append(incomePopup.replace(/FID/g,'self'));
		createSliders('self');
	}

	//Populating first name from My Profile page
	$('#memberself').html('Claiming Tax Filer\'s');
	if($('#claimerName').val()){
		$('#memberself').html($('#claimerName').val()+'\'s');
		$('#claimantNameIncome').html(name + '\'s Income &nbsp; <i class="icon icon-edit"></i>');
	}

	//Job income should be populated from My Profile page for Claimant if income page is not latest
	if(!profileModel.get('isIncomePageLatest')){
		$('#jobIncome_self').val('');
		if($("#taxHouseholdIncome").val()){
			$('#jobIncome_self').val('$' + $("#taxHouseholdIncome").val().replace(/\$/g, ''));
		}
	}
	$('#highslide-html_self').removeClass('removePopup');

	//Appending row for the spouse
	if($('input:checkbox[name="isMarried"]').is(':checked')){
		row = $('#spouseRow').html();
		if(row == null){
			$('.incometable tbody').append(
					'<tr id="spouseRow">'+
			         '<td>2</td>'+
			         //'<td>Spouse</td>'+
			         //'<td><a href="#income-modal_spouse" data-toggle="modal">'+
			         '<td><a href="#" onclick="return hs.htmlExpand(this, { contentId: \'highslide-html_spouse\' } )"'+
			            'class="highslide">'+'Spouse&#39;s Income &nbsp;'+
			         '<i class="icon icon-edit"></i></a></td>'+
			         '<td><span id="spouseIncome">$0</span>&nbsp;<a href="#" onclick="javascript:clearIncome(\'highslide-html_spouse\')">'+
			         '			Clear</a></td></td>'+
			      	'</tr>'
			);
		}
		else{
			$('#spouseRow').show();
		}
		row = $('#highslide-html_spouse').html();
		if(row == null){
			$('#incomePopUps').append(incomePopup.replace(/FID/g,'spouse'));
			$('#memberspouse').html('Spouse\'s');
			createSliders('spouse');
			$('#highslide-html_spouse').removeClass('removePopup');
		}
	}
	else{
		$('#spouseRow').hide();
	}

	//Appending rows for the dependents
	var totalDependents = parseInt($('#numberOfDependents').val());
	var dependentsSerialNumberIncrement = 2;
	if($('input:checkbox[name="isMarried"]').is(':checked')){
		dependentsSerialNumberIncrement=3;
	}
	$('.dependentRows').hide();
	if(totalDependents){
		for(var i=0; i<totalDependents; i++){
			row = $('#dependent'+(i+1)+'row').html();

			if(row == null){
				$('.incometable tbody').append(
						'<tr class="dependentRows" id="dependent'+(i+1)+'row">'+
				         '<td>'+(i+dependentsSerialNumberIncrement)+'</td>'+
				         //'<td>Dependent '+(i+1)+'</td>'+
				         //'<td><a href="#income-modal_dependent'+(i+1)+'" data-toggle="modal">'+
				         '<td><a href="#" onclick="return hs.htmlExpand(this, { contentId: \'highslide-html_dependent'+(i+1)+'\' } )"'+
				            'class="highslide">'+'Dependent '+(i+1)+'&#39;s Income &nbsp;'+
				            '<i class="icon icon-edit"></i></a></td>'+
				         '<td><span id="dependent_'+(i+1)+'_income">$0</span>&nbsp;<a href="#" onclick="javascript:clearIncome(\'highslide-html_dependent'+(i+1)+'\')">'+
				         		'Clear</a></td></td>'+
				      	'</tr>'
				);
			}
			else{
				$('#dependent'+(i+1)+'row').show();
				continue;
			}

			row = $('#highslide-html_dependent'+(i+1)).html();

			if(row == null){
				$('#incomePopUps').append(incomePopup.replace(/FID/g,'dependent'+(i+1)));
				$('#memberdependent' + (i+1)).html('Dependent '+(i+1)+'\'s');
				createSliders('dependent'+(i+1));
			}

		}

	}
	//Adding validations to incomes using masking
	$.mask.definitions['~']='[+-]?';
	$('.incomeType').mask("$?99999999",{placeholder:""});

	//Adding change listeners to income texts
	$('.incomeType').each(function(){

		if($(this)){
			$(this).on('change',function(){
				calculateRefinedIncome();
			});
		}

	});

	//Setting income page as latest
	profileModel.set({
		isIncomePageLatest : true
	});

	calculateRefinedIncome();

	//Initialize pop over texts
	$('.haspopover').popover({
		trigger:'hover',
		title:""
	});

	userFirstTimeNavigation = false;
}

//Method to initialize editable income
function initEditable(){

	// Editable	 income
	$.fn.editable.defaults.mode = 'inline';

	$('#refineincome').editable({
			value : $('#taxHouseholdIncome').val(),
			emptytext : formatIncome($('#taxHouseholdIncome').val().replace(/\$/g, '')),
			validate: function(value) {

				if(isNaN(parseFloat(value,10))){
					return 'Invalid income amount';
				}

				if(! /^\d{1,9}(\.\d{1,2})?$/.test($.trim(value)) ){
					return 'Invalid income amount';
				}
			},
			display : function (value, sourceData){

				var income = Math.round($.trim(value.replace(/\\$/g, '')));

				if(!isNaN(income)){

					income = formatIncome (income);

					$(this).html(income);
					$(this).text(income);

				}
				else{
					$(this).html('0');
					$(this).text('0');
				}
			},
			success : function(response, newValue){

				$('.incomeType').each(function(){
					$(this).val('$0');
				});

				$('.income-slider').each(function(){
					$(this).slider("value",0);
				});

				$('#alimonyPaid').val('$0');
				$("#alimonyPaidSlider").slider("value",0);

				$('#studentLoans').val('$0');
				$("#studentLoansSlider").slider("value",0);

				$('#jobIncome_self').val('$' + newValue);
				$('#jobIncomeSlider_self').slider("value",newValue);

				calculateRefinedIncome();

				$('#taxHouseholdIncome').val('$' + newValue);
				$("#taxHouseholdIncomeSlider").slider("value",newValue);

				$(this).value = newValue;
				$(this).text(newValue);
				$(this).html(newValue);

				calculateAPTC();
			}
		});

}

//Method to clear income of a member
function clearIncome(memberType){

	$('#'+memberType+' .incomeType').each(function(){
		$(this).val('$0');
	});

	calculateRefinedIncome();

}

//Method to format income with commas
function formatIncome (income){
	while (/(\d+)(\d{3})/.test(income.toString())){
		income = income.toString().replace(/(\d+)(\d{3})/, '$1'+','+'$2');
	 }
	return income;
}

//Method to create income sliders for a member
function createSliders(memberType){

	//Slider 1 - Job Income
	var locateSlider = $("#jobIncomeSlider_" + memberType);
	$(locateSlider).slider({
	    range: "min",
		min: 0,
		max: 150000,
		value: 0,
		step: 1000,
		slide: function (event, ui) {
			$("#jobIncome_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#jobIncome_" + memberType).val("$0");

	// Slider 2 - Self Employment
	var locateSlider = $("#selfEmploymentSlider_" + memberType);
	$(locateSlider).slider({
	    range: "min",
		min: -100000,
		max:  100000,
		value: 0,
		step: 1000,
		slide: function (event, ui) {
			$("#selfEmployment_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#selfEmployment_" + memberType).val("$0");


	// Slider 3 - Social Security Benefits
	var locateSlider = $("#socialSecurityBenefitsSlider_" + memberType);;
	$(locateSlider).slider({
	    range: "min",
		min: 0,
		max:  150000,
		value: 0,
		step: 1000,
		slide: function (event, ui) {
			$("#socialSecurityBenefits_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#socialSecurityBenefits_" + memberType).val("$0");


	// Slider 4 - Unemployment
	var locateSlider = $("#unemploymentSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#unemployment_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#unemployment_" + memberType).val("$0");


	// Slider 5 - Retirement/Pension
	var locateSlider = $("#retirementPensionSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#retirementPension_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#retirementPension_" + memberType).val("$0");


	// Slider 6 - Capital Gains
	var locateSlider = $("#capitalGainsSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#capitalGains_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#capitalGains_" + memberType).val("$0");

	// Slider 7 - Investment Income
	var locateSlider = $("#investmentIncomeSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#investmentIncome_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#investmentIncome_" + memberType).val("$0");

	// Slider 8 - Alimony
	var locateSlider = $("#alimonyReceivedSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#alimonyReceived_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#alimonyReceived_" + memberType).val("$0");

	// Slider 9 - Rental or Royalty
	var locateSlider = $("#rentalRoyaltyIncomeSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#rentalRoyaltyIncome_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#rentalRoyaltyIncome_" + memberType).val("$0");

	// Slider 10 - Farming or Fishing
	var locateSlider = $("#farmingFishingIncomeSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#farmingFishingIncome_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#farmingFishingIncome_" + memberType).val("$0");

	// Slider 11 - Other
	var locateSlider = $("#otherIncomeSlider_" + memberType);
	$(locateSlider).slider({
		range : "min",
		min : 0,
		max : 150000,
		value : 0,
		step : 1000,
		slide : function(event, ui) {
			$("#otherIncome_" + memberType).val('$' + ui.value);
			calculateRefinedIncome();
		}
	});
	$("#otherIncome_" + memberType).val("$0");


}

//Method to calculate refined income
function calculateRefinedIncome(){

	var claimantIncome = 0;
	var spouseIncome = 0;
	var dependentsTotalIncome = 0;
	var totalIncome = 0;

	//To find claimant's income
	$('#highslide-html_self .incomeType').each(function(){
		if($(this)){
			var income = Math.round($.trim($(this).val().replace(/\$/g, '')));
			if(!isNaN(income)){
				claimantIncome += income;
			}
		}
	});
	$('#selfIncome').html('$' + formatIncome(claimantIncome));

	//To find spouse's income
	if($('input:checkbox[name="isMarried"]').is(':checked')){
		$('#highslide-html_spouse .incomeType').each(function(){
			if($(this)){
				var income = Math.round($.trim($(this).val().replace(/\$/g, '')));
				if(!isNaN(income)){
					spouseIncome += income;
				}
			}
		});
		$('#spouseIncome').html('$' + formatIncome(spouseIncome));
	}

	var totalDependents = parseInt($('#numberOfDependents').val());
	if(totalDependents){
		for(var i=0; i<totalDependents; i++){

			var dependentIncome = 0;

			$('#highslide-html_dependent'+(i+1)+' .incomeType').each(function(){
				if($(this)){
					var income = Math.round($.trim($(this).val().replace(/\$/g, '')));
					if(!isNaN(income)){
						dependentIncome += income;
					}
				}
			});

			$('#dependent_'+(i+1)+'_income').html('$' + formatIncome(dependentIncome));
			dependentsTotalIncome += dependentIncome;

			//If Job income + Self-employment < 5950 for dependent, we should not count them in the total income
			/*var dependentJobIncome = $('#jobIncome_dependent'+(i+1)).val();
			dependentJobIncome = dependentJobIncome.replace(/\$/g, '');
			if(isNaN(dependentJobIncome)){
				dependentJobIncome = 0;
			}


			var dependentSelfEmploymentIncome = $('#selfEmployment_dependent'+(i+1)).val();
			dependentSelfEmploymentIncome = dependentSelfEmploymentIncome.replace(/\$/g, '');
			if(isNaN(dependentSelfEmploymentIncome)){
				dependentSelfEmploymentIncome = 0;
			}

			if((parseInt(dependentJobIncome) + parseInt(dependentSelfEmploymentIncome)) <= 5950){
				dependentsTotalIncome -= (parseInt(dependentJobIncome) + parseInt(dependentSelfEmploymentIncome));
			}*/
		}
	}

	//Finding deductions
	var alimonyDeduction = Math.round($.trim($('#alimonyPaid').val().replace(/\$/g, '')));
	if(isNaN(alimonyDeduction)){
		alimonyDeduction = 0.0;
	}

	//Maximum cap on student loan deduction is $2500
	var studentLoanDeduction = Math.round($.trim($('#studentLoans').val().replace(/\$/g, '')));
	if(isNaN(studentLoanDeduction)){
		studentLoanDeduction = 0.0;
	}
	else if(studentLoanDeduction > 2500){
		studentLoanDeduction = 2500.0;
	}

	totalIncome =  claimantIncome + spouseIncome + dependentsTotalIncome - (alimonyDeduction + studentLoanDeduction);

	$('#refineincome').html(formatIncome(totalIncome));
	$('#refineincome').attr('data-value',formatIncome(totalIncome));
	$('#refineincome').attr('data-text',formatIncome(totalIncome));
	$('#refineincome').text(formatIncome(totalIncome));
	$('#refineincome').value = formatIncome(totalIncome) ;

	//Change Household income in my profile page to refined income
	$('#taxHouseholdIncome').val('');
	$('#taxHouseholdIncome').val('$' + totalIncome);
	$("#taxHouseholdIncomeSlider").slider("value",totalIncome);

	initEditable();
}

//Check medicare eligibility
function checkMedicare(){

	var totalMembers = 1;
	if($('input:checkbox[name="isMarried"]').is(':checked')){
		totalMembers = 2;
	}
	var totalDependents = parseInt($.trim($('#numberOfDependents').val()));
	if(isNaN(totalDependents)){
		totalDependents = 0;
	}
	totalMembers +=  totalDependents;
	var medicareEligible = 0;
	$('.medicareEligible').each(function(){
		if($(this).is(':visible')){
			medicareEligible++;
		}
	});


	if(totalMembers == medicareEligible){
		return true;
	}

	return false;
}

//Method to find age from DOB
function getAge(value){

	var today = new Date();
	dob_arr = value.split('/');

	dob_mm = dob_arr[0];
	dob_dd = dob_arr[1];
	dob_yy = dob_arr[2];

	var birthDate = new Date();
	birthDate.setFullYear(dob_yy, dob_mm - 1, dob_dd);

	if ((today.getFullYear() - 100) > birthDate.getFullYear()) {return 0;}

	if ((dob_dd != birthDate.getDate())
				|| (dob_mm - 1 != birthDate.getMonth())
				|| (dob_yy != birthDate.getFullYear())) {
			return 0;
	}

	var age = Math.ceil(today.getTime() - birthDate.getTime()) / (1000 * 60 * 60 * 24 * 365);
	return parseInt(age);
}


//Method to check age criteria for primary applicant and spouse
function validateAgeCriteria(){

	$('#householdError').hide();

	//If medicare eligible, return
	if(checkMedicare()){
		return true;
	}

	var selfAge = getAge($('#selfDob').val());
	var spouseAge = getAge($('#spouseDob').val());


	if( (selfAge >= 19 && selfAge < 65 && $('input[name=isSelfSeekingCoverage].seeking-yes').parent('span').attr("class")=="checked")
			||
				(spouseAge >= 19 && spouseAge < 65 && $('input[name=isSpouseSeekingCoverage].seeking-yes').parent('span').attr("class")=="checked") ) {

		return true;
	}
	else{
		$('#householdError').show();
		return false;
	}
}

var noOfApplicants = 0;

//Method to find no. of applicants
function getNoOfApplicants(household){

	noOfApplicants = 0;

	var members =  JSON.stringify(household);
	var householdModel = new PrescreenHousehold(JSON.parse(members));
	var householdMembers = new PrescreenHouseholdCollection(householdModel.get('householdMembers'));
	householdMembers.each(function(member){
	    if(member.get('seekingCoverage')){
	    	noOfApplicants++;
	    }
	});

	return parseInt(noOfApplicants);
}

//function to show/hide plans and counter
function renderView(currentTab, aptc, fpl,totalApplicants,benchmarkPlanPremium){
	
	//User is not in results section
	if(currentTab != '4'){
		$("#sampleplan").fadeOut(200);
		$("#showplans").fadeOut(200);
		$('#flipCounterDiv').show();
		$('#medicaidplan').hide();
		$('#healthplan').hide();
		return;
	}
	
	//User is in results section
	
	//Medicare eligible
	if(checkMedicare()){
		$('#case4').show();
		$('#medicareResults').show();
		$('#flipCounterDiv').show();
		$("#sampleplan").fadeOut(200);
		return;
	}
	
	//Medicaid eligible
	else if(fpl < 138.0){
		$('#case5').show();
		$('#fpl138Results').show();
		$("#sampleplan").fadeIn(200);
		$('#medicaidplan').fadeIn(200);
		$('#flipCounterDiv').hide();
		return;
	}

	//APTC Eligible Only (250-420% of FPL) STARTS
	else if(fpl >= 250.0 && fpl <= 420.0 && aptc >0){
		$('#case1').show();
		$('#case1Results').show();
	}
	
	//This case is not in specs
	else if(fpl >= 250.0 && fpl <= 420.0 && aptc <= 0){
		$('#case3').show();
		$('#case3Results').show();
	}
	
	//APTC/CSR Eligible (138-250% of FPL)
	else if(fpl >= 138.0 && fpl < 250.0 && aptc > 0){
		$('#case2').show();
		$('#case2Results').show();
	}

	//Not APTC Eligible, but CSR Eligible (APTC equation returns negative or 0, but 138-250% of FPL)
	else if(fpl >= 138.0 && fpl < 250.0 && aptc <= 0){
		$('#case6').show();
		$('#case6Results').show();
	}

	//Not APTC/CSR Eligible (>420% of FPL) STARTS
	else if(fpl > 420.0){
		$('#case3').show();
		$('#case3Results').show();
	}

	var revisedPremium = benchmarkPlanPremium;

	// Apply APTC only for FPL < 420
	if (fpl <= 420.0) {
		
		if (parseInt(aptc) >= 0) {
				revisedPremium = benchmarkPlanPremium - parseInt(aptc);
		} 
		else{
			revisedPremium = benchmarkPlanPremium;
		}
		
		$('.crossedAptc').show();
	} 
	else{
		$('.crossedAptc').hide();
	}
	
	if (aptc <= 0) {
		$('.crossedAptc').hide();
	} 
	else {
		$('.crossedAptc').show();
	}
	
	// CS1 Plan
	if(fpl >= 250.0){

		if(totalApplicants > 1){
			$('#cs1Family').show();
			$('#cs1Family').find('h4').html('$'+revisedPremium+'/mo');
		}
		else{
			$('#cs1Individual').show();
			$('#cs1Individual').find('h4').html('$'+revisedPremium+'/mo');
		}
		$('#sampleplan').fadeIn(200);
		$('#healthplan').show();
		$('#showplans').show();
		$('#flipCounterDiv').show();
	}

	//CS4 Plan
	if(fpl >=200.0 && fpl < 250.0){

		if(totalApplicants > 1){
			$('#cs4Family').show();
			$('#cs4Family').find('h4').html('$'+revisedPremium+'/mo');

		}
		else{
			$('#cs4Individual').show();
			$('#cs4Individual').find('h4').html('$'+revisedPremium+'/mo');
		}
		$('#sampleplan').fadeIn(200);
		$('#healthplan').show();
		$('#showplans').show();
		$('#flipCounterDiv').show();
	}

	//CS5 Plan
	if(fpl >=150.0 && fpl < 200.0){

		if(totalApplicants > 1){
			$('#cs5Family').show();
			$('#cs5Family').find('h4').html('$'+revisedPremium+'/mo');
		}
		else{
			$('#cs5Individual').show();
			$('#cs5Individual').find('h4').html('$'+revisedPremium+'/mo');
		}
		$('#sampleplan').fadeIn(200);
		$('#healthplan').show();
		$('#showplans').show();
		$('#flipCounterDiv').show();
	}

	//CS6 Plan
	if(fpl >=138 && fpl < 150.0){

		if(totalApplicants > 1){
			$('#cs6Family').show();
			$('#cs6Family').find('h4').html('$'+revisedPremium+'/mo');
		}
		else{
			$('#cs6Individual').show();
			$('#cs6Individual').find('h4').html('$'+revisedPremium+'/mo');
		}
		$('#sampleplan').fadeIn(200);
		$('#healthplan').show();
		$('#showplans').show();
		$('#flipCounterDiv').show();
	}
}

// AJAX call to find APTC
function calculateAPTC(){

	var statecodeByZip = $('#stateCode').val();
	var householdIncome = $('#taxHouseholdIncome').val();
	var name = 'Claiming Tax Filer';
	if($('#claimerName').val()){
		name = $('#claimerName').val();
	}
	profileModel.set({
		claimerName : name,
		taxHouseholdIncome: householdIncome.replace(/\$/g, ''),
		stateCode : statecodeByZip
	});
	$('#taxHouseholdIncome').val('$' + $('#taxHouseholdIncome').val().replace(/\$/g, ''));

	//Binding name to claimant model
	claimantModel.set({ memberDesc: 'Claiming Tax Filer'});
	if($('#claimerName').val()){
		claimantModel.set({ memberDesc: $('#claimerName').val()});
	}

	//Creating income model for claimant
	incomes.reset();
	if($('#jobIncome_self').val()){
		claimantIncome = new IncomeModel({
			jobIncome: $('#jobIncome_self').val().replace(/\$/g, ''),
			selfEmployment: $('#selfEmployment_self').val().replace(/\$/g, ''),
			socialSecurityBenefits: $('#socialSecurityBenefits_self').val().replace(/\$/g, ''),
			unemployment: $('#unemployment_self').val().replace(/\$/g, ''),
			retirementPension: $('#retirementPension_self').val().replace(/\$/g, ''),
			capitalGains: $('#capitalGains_self').val().replace(/\$/g, ''),
			investmentIncome: $('#investmentIncome_self').val().replace(/\$/g, ''),
			alimonyReceived: $('#alimonyReceived_self').val().replace(/\$/g, ''),
			rentalRoyaltyIncome: $('#rentalRoyaltyIncome_self').val().replace(/\$/g, ''),
			farmingFishingIncome: $('#farmingFishingIncome_self').val().replace(/\$/g, ''),
			otherIncome: $('#otherIncome_self').val().replace(/\$/g, '')
		});
		claimantIncome.set({ memberDesc: 'Claiming Tax Filer'});
		if($('#claimerName').val()){
			claimantIncome.set({ memberDesc: $('#claimerName').val()});
		}

		incomes.add([claimantIncome]);
	}

	prescreenHouseholdMembers.reset();
	prescreenHouseholdMembers.add([claimantModel]);

	if($('input:checkbox[name="isMarried"]').is(':checked')){

		//Model for spouse
		spouseModel = new PrescreenHouseholdMemberModel({
			memberDesc : 'Spouse',
			relationshipWithClaimer : 'spouse',
			dateOfBirth: $('#spouseDob').val(),
			pregnant : $('input[name=isSpousePregnant]:checked').val(),
			seekingCoverage : $('input[name=isSpouseSeekingCoverage]:checked').val()
		});
		prescreenHouseholdMembers.add([spouseModel]);

		//Model for spouse income
		if($('#jobIncome_spouse').val()){
			spouseIncome = new IncomeModel({
				memberDesc : 'Spouse',
				jobIncome: $('#jobIncome_spouse').val().replace(/\$/g, ''),
				selfEmployment: $('#selfEmployment_spouse').val().replace(/\$/g, ''),
				socialSecurityBenefits: $('#socialSecurityBenefits_spouse').val().replace(/\$/g, ''),
				unemployment: $('#unemployment_spouse').val().replace(/\$/g, ''),
				retirementPension: $('#retirementPension_spouse').val().replace(/\$/g, ''),
				capitalGains: $('#capitalGains_spouse').val().replace(/\$/g, ''),
				investmentIncome: $('#investmentIncome_spouse').val().replace(/\$/g, ''),
				alimonyReceived: $('#alimonyReceived_spouse').val().replace(/\$/g, ''),
				rentalRoyaltyIncome: $('#rentalRoyaltyIncome_spouse').val().replace(/\$/g, ''),
				farmingFishingIncome: $('#farmingFishingIncome_spouse').val().replace(/\$/g, ''),
				otherIncome: $('#otherIncome_spouse').val().replace(/\$/g, '')
			});

			//Creating Income Collection
			incomes.add([spouseIncome]);
		}

	}
	else {
		var spouseModelPresent = prescreenHouseholdMembers.where({memberDesc : 'Spouse'});

		if(spouseModelPresent.length != 0){
			prescreenHouseholdMembers.remove([spouseModel]);
		}
	}

	//Adding dependents
	var totalDependents = parseInt($.trim($('#numberOfDependents').val()));

	for(var i=0; i<totalDependents; i++){

		//Creating model for dependent and adding it to household collection
		var dependentModel = new PrescreenHouseholdMemberModel({
			memberDesc : 'Dependent ' + (i+1),
			relationshipWithClaimer : $('input[name=dependent_'+(i+1)+'_relationship]:checked').val(),
			dateOfBirth: $('#dependent_'+(i+1)+'_dob').val(),
			pregnant : $('input[name=isDependent_'+(i+1)+'_pregnant]:checked').val(),
			seekingCoverage : $('input[name=isDependent_'+(i+1)+'_seekingCoverage]:checked').val()
		});

		prescreenHouseholdMembers.add([dependentModel]);

		//Model for dependent income
		if($('#jobIncome_dependent'+(i+1)).val()){
			dependentIncome = new IncomeModel({
				memberDesc : 'Dependent ' + (i+1),
				jobIncome: $('#jobIncome_dependent'+(i+1)).val().replace(/\$/g, ''),
				selfEmployment: $('#selfEmployment_dependent'+(i+1)).val().replace(/\$/g, ''),
				socialSecurityBenefits: $('#socialSecurityBenefits_dependent'+(i+1)).val().replace(/\$/g, ''),
				unemployment: $('#unemployment_dependent'+(i+1)).val().replace(/\$/g, ''),
				retirementPension: $('#retirementPension_dependent'+(i+1)).val().replace(/\$/g, ''),
				capitalGains: $('#capitalGains_dependent'+(i+1)).val().replace(/\$/g, ''),
				investmentIncome: $('#investmentIncome_dependent'+(i+1)).val().replace(/\$/g, ''),
				alimonyReceived: $('#alimonyReceived_dependent'+(i+1)).val().replace(/\$/g, ''),
				rentalRoyaltyIncome: $('#rentalRoyaltyIncome_dependent'+(i+1)).val().replace(/\$/g, ''),
				farmingFishingIncome: $('#farmingFishingIncome_dependent'+(i+1)).val().replace(/\$/g, ''),
				otherIncome: $('#otherIncome_dependent'+(i+1)).val().replace(/\$/g, '')
			});

			incomes.add([dependentIncome]);
		}
	}

	deductionsModel.set({
		alimonyPaid: $('#alimonyPaid').val().replace(/\$/g, ''),
		studentLoans: $('#studentLoans').val().replace(/\$/g, '')
	});

	var prescreenRequest = new PrescreenRequestModel({
			prescreenProfile : profileModel,
			prescreenHousehold : new PrescreenHousehold({
				householdMembers : prescreenHouseholdMembers
			}),
			isAnyMemberDisabled : $('input[name=isAnyMemberDisabled]:checked').val(),
			prescreenIncomes : incomes,
			prescreenDeductions : deductionsModel,
			prescreenRecordId : $('#prescreenRecordId').val(),
			currentTab : $('#currentTab').val()
	});


	
	//AJAX call to caluclate APTC
	var pathURL = $('#prescreenVersionName').val()+"/evaluateApplication";
	if(prescreenRequestSaved){
		var previous = JSON.parse(prescreenRequestSaved);
		previous["currentTab"] = $('#currentTab').val();
		prescreenRequestSaved = JSON.stringify(previous);
	}
	if( prescreenRequestSaved!=JSON.stringify(prescreenRequest) || startOver){
		
		startOver = false;
		
		$('.no-margin-b').hide();
		$('.results').hide();
		$('.planData').hide();
		$('#showplans').hide();
		$('#medicaidplan').hide();
		$('#healthplan').hide();
		
		$.ajax({
				type: "POST",
			    url:     pathURL,
			    contentType: "application/json; charset=utf-8",
			    data:    JSON.stringify(prescreenRequest),
			    success: function(data) {
			    	prescreenRequestSaved=JSON.stringify(prescreenRequest);
			    	savedData = JSON.stringify(data);
			    	var response = new PrescreenRequestModel(JSON.parse(savedData));

			    	aptc = response.get('aptcValue');
			    	applicablePercentage = response.get('applicablePercentage');
			    	fpl = response.get('fplValue');
			    	benchmarkPlanPremium = response.get('benchmarkPlanPremium');
			    	benchmarkPlanPremium = Math.round(parseInt(benchmarkPlanPremium));

			    	$('.benchmarkPremium').html('$'+benchmarkPlanPremium);

			    	if(!isNaN(aptc) && aptc >= 0){
			    		myCounter.incrementTo(aptc);

			    		//Showing APTC in results section
			    		$('#aptcCase1').html(aptc);
			    		$('#aptcCase2').html(aptc);
			    		$('#aptcCase1Results').html(aptc);
			    		$('#aptcCase2Results').html(aptc);

			    	}else {
			    		$('#aptcCase1').html(0);
			    		$('#aptcCase2').html(0);
			    		$('#aptcCase1Results').html(0);
			    		$('#aptcCase2Results').html(0);
			    		myCounter.incrementTo(0);
			    	}
			    	if(!isNaN(applicablePercentage) && applicablePercentage >= 0){
			    		$('.uncleSamPercentage').text(applicablePercentage.toFixed(2));
			    	}else {
			    		applicablePercentage=0.00;
			    		$('.uncleSamPercentage').text(applicablePercentage.toFixed(2));
			    	}
			    	$('#prescreenRecordId').val(response.get('prescreenRecordId'));
			    
			    	//Showing results section and displaying plan data

			    	fpl = parseFloat(fpl).toFixed(2);

			    	if(!isNaN(fpl)){

			    		totalApplicants = getNoOfApplicants(response.get('prescreenHousehold'));
			    		aptc = parseInt(aptc);

			    		renderView( $('#currentTab').val(),aptc,fpl,totalApplicants,benchmarkPlanPremium);
			    	}


			     },
			     error: function(jqXHR, textStatus, errorThrown) {
			    	 alert("There was an error communicating with the server."+errorThrown);
			     }
			});
	}
	else{
		renderView( $('#currentTab').val(),aptc,fpl,totalApplicants,benchmarkPlanPremium);
	}


}