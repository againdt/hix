package com.getinsured.hix.provider.service.jpa;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.Facility;
import com.getinsured.hix.model.FacilityAddress;
import com.getinsured.hix.model.Network;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.PhysicianAddress;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.ProviderNetwork;
import com.getinsured.hix.model.Specialty;
import com.getinsured.hix.platform.file.reader.FileReader;
import com.getinsured.hix.platform.file.reader.ReaderFactory;
import com.getinsured.hix.planmgmt.provider.repository.IFacilityAddressRepository;
import com.getinsured.hix.planmgmt.provider.repository.IFacilityRepository;
import com.getinsured.hix.planmgmt.provider.repository.INetworkRepository;
import com.getinsured.hix.planmgmt.provider.repository.IPhysicianAddressRepository;
import com.getinsured.hix.planmgmt.provider.repository.IPhysicianRepository;
import com.getinsured.hix.planmgmt.provider.repository.IProviderNetworkRepository;
import com.getinsured.hix.planmgmt.provider.repository.IProviderRepository;
import com.getinsured.hix.planmgmt.provider.repository.ISpecialtyRepository;
import com.getinsured.hix.provider.service.ProviderSearchNetworkService;


@Service("providerSearchNetworkService")
@Repository
@Transactional
public class ProviderSearchNetworkServiceImpl implements ProviderSearchNetworkService{

	private static final Logger logger = LoggerFactory.getLogger(ProviderSearchNetworkServiceImpl.class);
	private static final String UNCHECKED = "unchecked";
	
	@Autowired private IPhysicianRepository iPhysicianRepository;
	@Autowired private IFacilityRepository iFacilityRepository;
	@Autowired private INetworkRepository iNetworkRepository;
	@Autowired private IProviderRepository iProviderRepository;
	@Autowired private IProviderNetworkRepository iProviderNetworkRepository;
	@Autowired private ISpecialtyRepository iSpecialtyRepository;
	@Autowired private IFacilityAddressRepository iFacilityAddressRepository;
	@Autowired private IPhysicianAddressRepository iPhysicianAddressRepository;
	
	
	@Override
	public boolean uploadFile(String fileLocation, MultipartFile mfile, String fileName) {
		// TODO Auto-generated method stub
		
		InputStream inputStream = null;
        OutputStream outputStream = null;
        boolean fileSaved=false;
        
			try {
	           
	            
		           if (mfile.getSize() > 0) {
		                                    
		            	logger.info("Location of the file : "+fileLocation);  
		                            
		                
		        	      File fileDir = new File(fileLocation); 
		                  
		                  // create Directory if not exists.
		                  if(!fileDir.exists()){ 
		                	  fileDir.mkdir(); 
		                  }
		                  	                  
		                   
		                  String newFileName = fileLocation + "/" + fileName;
		                  inputStream = mfile.getInputStream();
		                  outputStream = new FileOutputStream(newFileName);
		                  int readBytes = 0;
		                  byte[] buffer = new byte[10000];
		                  while ((readBytes = inputStream.read(buffer, 0, 10000)) != -1) {
		                         outputStream.write(buffer, 0, readBytes);
		                   }
		                  
		                  outputStream.close();
		                  inputStream.close();
		                  fileSaved = true;
		                  
		                }           
	          } catch (Exception e) {
	                 logger.error("Error uploading file");
	                
	          }
			return fileSaved;	
	}
	
	@SuppressWarnings(UNCHECKED)
	@Override	
	public List<List> getPhysicians(String filePath){
		try{
			FileReader<List> reader =  ReaderFactory.<List>getReader(filePath, ',', Physician.class); 
			List<List> physicians = reader.readData();			
			return physicians;
		}catch(Exception e){			
			logger.error("Error reading PHYSICIAN data " +filePath);
			return null;
		}	
		
	}
	
	
	@SuppressWarnings(UNCHECKED)
	@Override	
	public List<List> getFacilities(String filePath){
		List<List> facilities =null;
		try{
		FileReader<List> reader =  ReaderFactory.<List>getReader(filePath, ',' , Facility.class); 
		facilities = reader.readData();
		}catch(Exception e){
			logger.error("Error reading Facilities data " +filePath);
		}
		return facilities;
	}
	
	@Override
	@Transactional
	public void savePhysicians(List<Physician> physicians) {
		for(Physician physicianObj : physicians){
			iPhysicianRepository.save(physicianObj);
		}		 
	
	}

	@Override
	@Transactional
	public void saveFacilities(List<Facility> facilities) throws Exception {
	
		for(Facility facilityObj : facilities){
		
			if(facilityObj!=null){
				iFacilityRepository.save(facilityObj);
			}
			
		}		 
	}
	@Override
	@Transactional
	public Facility saveFacility(Facility facility) throws Exception {
	
	 return iFacilityRepository.save(facility);
			 
	}

	@Override
	@Transactional
	public Specialty saveSpecialty(Specialty specialty) throws Exception {
	
		return iSpecialtyRepository.save(specialty);	
	}
	
	
	@Override
	@Transactional
	public FacilityAddress saveFacilityAddress(FacilityAddress facilityAddress) throws Exception {
			return iFacilityAddressRepository.save(facilityAddress);	
	}
	
	@Override
	public boolean isFacilityAddressExists(String county, String address, int facilityId) throws Exception{
		 if(getFacilityAddress(county,address,facilityId)!=null){
				return true; 
		 }
		return false;
	}
	@Override
	@Transactional
	public FacilityAddress getFacilityAddress(String county,String address, int facilityId) throws Exception{
		return iFacilityAddressRepository.getFacilityAddress(county,address,facilityId);
	}
	
	@Override
	@Transactional
	public void saveProvider(Provider provider) throws Exception {
	
		iProviderRepository.save(provider);	
	}

	@Override
	@Transactional
	public Network saveNetwork(Network network) throws Exception {
	
	 return iNetworkRepository.save(network);
	}
	@Override
	@Transactional
	public void saveProviderNetwork(ProviderNetwork providerNetwork) throws Exception {
			
		iProviderNetworkRepository.save(providerNetwork);
	}
	
	@Override
	public boolean isNetworkExists(String netType, String netName, int issuerId)
			throws Exception {
		 if(iNetworkRepository.countNetworkByTypeAndName(netType, netName, issuerId)>0){
			return true; 
		 }
	return false;
	}

	@Override
	public Physician savePhysician(Physician physician) throws Exception {
		 return iPhysicianRepository.save(physician);
	}

	@Override
	public boolean isPhysicianAddressExists(String county,String address, int physicianId) throws Exception {
		 if(getPhysicianAddress(county,address,physicianId)!=null){
				return true; 
		 }
		return false;
	}

	@Override
	public PhysicianAddress getPhysicianAddress(String county,String address, int physicianId)
			throws Exception {
		return iPhysicianAddressRepository.getPhysicianAddress(county,address,physicianId);
	}

	@Override
	public PhysicianAddress savePhysicianAddress(PhysicianAddress physicianAddress) throws Exception {
		return iPhysicianAddressRepository.save(physicianAddress);
	}

	@Override
	public List<Network> getAllProviderNetwork() {
	    return iNetworkRepository.findAll();
	}

	@Override
	public boolean isProviderExists(String type, String name, String datasourceRefId) throws Exception {
		 if(getProvider(type, name, datasourceRefId)!=null){
				return true; 
			 }
	return false;
	}
	
	@Override
	@Transactional	
	public Provider getProvider(String type, String name, String datasourceRefId) throws Exception
	{
	  
		return  iProviderRepository.getProviderByTypeNameRefId(type, name, datasourceRefId);
	  
	}

	@SuppressWarnings(UNCHECKED)
	@Override	
	public List<FacilityAddress> getFacilitiesAddress(String filePath) {
		List<FacilityAddress> facilitiesAddress =null;
		try{
		FileReader<FacilityAddress> reader =  ReaderFactory.<FacilityAddress>getReader(filePath, ',' , FacilityAddress.class); 
		facilitiesAddress = reader.readData();
		}catch(Exception e){
			logger.error("Error reading Faclities Address Data");
		}
		return facilitiesAddress;
	}
	
	@SuppressWarnings(UNCHECKED)
	@Override	
	public List<PhysicianAddress> getPhysiciansAddress(String filePath) {
		List<PhysicianAddress> physicianAddress =null;
		try{
		FileReader<PhysicianAddress> reader =  ReaderFactory.<PhysicianAddress>getReader(filePath, ',' , PhysicianAddress.class); 
		physicianAddress = reader.readData();
		}catch(Exception e){
			logger.error("Error reading Physician Address Data");
		}
		return physicianAddress;
	}
	
	@SuppressWarnings(UNCHECKED)
	@Override
	public List<Specialty> getFacilitiesSpecialty(String filePath) {
		List<Specialty> facilitiesSpecialty =null;
		try{
		FileReader<Specialty> reader =  ReaderFactory.<Specialty>getReader(filePath, ',' , Specialty.class); 
		facilitiesSpecialty = reader.readData();
		}catch(Exception e){
			logger.error("Error reading facilties specialty data");
		}
		return facilitiesSpecialty;
	}

	@Override
	public Facility getFacilityByDataSourceRefId(String dataSourceRefId, int networkId)
			throws Exception {
		return iFacilityRepository.getFacilityByDataSourceRefId(dataSourceRefId, networkId);
	
	}
	
	@Override
	public boolean isSpecialtyExists(String name, String groupName)
			throws Exception {
		
		if(getSpecialty(name,groupName)!=null){
			return true;
		}
		return false;
	}
	@Override
	public Specialty getSpecialty(String name, String groupName)
			throws Exception {
		
		return iSpecialtyRepository.getSpecialtyByNameGroupName(name,groupName);
	}

	@Override
	public Physician getPhysicianByDataSourceRefId(String dataSourceRefId,int networkId)
			throws Exception {
		return iPhysicianRepository.getPhysicianByDataSourceRefId(dataSourceRefId,networkId);
	}
	
	@SuppressWarnings(UNCHECKED)
	@Override
	public List<Specialty> getPhysiciansSpecialty(String filePath) {
		List<Specialty> physiciansSpecialty =null;
		try{
		FileReader<Specialty> reader =  ReaderFactory.<Specialty>getReader(filePath, ',' , Specialty.class); 
		physiciansSpecialty = reader.readData();
		}catch(Exception e){
			logger.error("Error reading physician specialty data");
		}
		return physiciansSpecialty;
	}

	@Override
	public Network getNetwork(int id) throws Exception {
	 return iNetworkRepository.findOne(id);
	}

	
	@Override
	public List<Network> getProviderNetworkByIssuerId(int issuerId) {
		return iNetworkRepository.getProviderNetworkByIssuerId(issuerId);		
	}
	
	public Object[] getProvNetDetailsById(int providerNetworkId)
	{
		return iProviderNetworkRepository.getProvNetDetailsById(providerNetworkId);
	}

	@Override
	public Page<Network> getProviderNetworkByIssuerId(int issuerId,
			Pageable pageable) {
		
		return iNetworkRepository.getProviderNetworkByIssuerId(issuerId, pageable);
	}
	
	@Override
	public void deleteInBatch(List<Network> entities) {
		iNetworkRepository.deleteInBatch(entities);
	}

	@Override
	public Specialty getSpecialtyByCode(String specialtyCode) {
		return iSpecialtyRepository.getSpecialtyByCode(specialtyCode);
	}

	@Override
	public List<Network> getNetworkByNetworkId(String networkId, String hiosId) {
		if(StringUtils.isNotBlank(hiosId)){
			return iNetworkRepository.getProviderNeworkByNetworkAndHiosValue(networkId, hiosId);
		}else{
			return null;
		}
	}
	
	
	
}
