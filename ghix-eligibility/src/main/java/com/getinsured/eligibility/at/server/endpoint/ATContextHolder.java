package com.getinsured.eligibility.at.server.endpoint;

import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

public final class ATContextHolder {

	private static ThreadLocal<AccountTransferRequestPayloadType> threadLocal =
			new ThreadLocal<AccountTransferRequestPayloadType>() {
		@Override
		protected AccountTransferRequestPayloadType initialValue() {
			return null;
		}
	};

	private ATContextHolder() {
	}

	public static AccountTransferRequestPayloadType getAccountTransferRequestPayloadType() {
		return threadLocal.get();
	}

	public static void setAccountTransferRequestPayloadType(AccountTransferRequestPayloadType context) {
		threadLocal.set(context);
	}

	public static void removeAccountTransferRequestPayloadType() {
		threadLocal.remove();
	}
}