
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page isELIgnored="false"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>

<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.uploadCrossWalk"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1 id="skip">${issuerObj.name}</h1>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
               <h4><spring:message  code="pgheader.issuerAbout"/></h4>
            </div>
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="paymentInfo"/>
				</jsp:include>
			</div>
			<!-- end of span3 -->
					
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left">Issuer Payment Info</h4>
                 <%--  <spring:message  code="pgheader.crossWalkStatus"/> --%>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/editIssuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="label.edit"/> </a>
                  
                </div>
             	<form class="form-horizontal" id="frmIssuerHistory" name="frmIssuerHistory" action="#" method="POST">
                    <df:csrfToken/>
                  
					<div class="profile">
						<table class="table table-border-none verticalThead">							
								<tr>
									<th class="txt-right span4"><spring:message  code="label.HIOSIssuerId"/></th>
									<td><strong>${issuerPaymentInfo.issuer.hiosIssuerId}</strong></td>
								</tr>
								<tr>
									<th class="txt-right span4"><spring:message  code="label.securityCertName"/></th>
									<td><strong>${issuerPaymentInfo.securityCertName}</strong></td>
								</tr>	
								<tr>
									<th class="txt-right span4"><spring:message  code="label.keyStoreFileLocation"/></th>
									<td><strong>${issuerPaymentInfo.keyStoreFileLocation}</strong></td>
								</tr>							
								<tr>
									<th class="txt-right"><spring:message  code="label.privateKeyName"/></th>
									<td><strong>${issuerPaymentInfo.privateKeyName}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.password"/></th>
									<td>
									<c:set var="paymentInfoPass" value="${issuerPaymentInfo.password}"/>
	 	 	 	 	                <c:if test="${not empty paymentInfoPass}">
	 	 	 	 	                <strong>**********</strong>
	 	 	 	 	                </c:if>
									</td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.passwordSecuredKey"/></th>
									<td><strong>${issuerPaymentInfo.passwordSecuredKey}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.securityDnsName"/></th>
									<td><strong>${issuerPaymentInfo.securityDnsName}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.securityAddress"/> </th>
									<td><strong>${issuerPaymentInfo.securityAddress}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.securityKeyInfo"/></th>
									<td><strong>${issuerPaymentInfo.securityKeyInfo}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.issuerAuthURL"/></th>
									<td><strong>${issuerPaymentInfo.issuerAuthURL}</strong></td>
								</tr>
								<tr>
									<th class="txt-right"><spring:message  code="label.createdOn"/></th>
									<td><strong>${issuerPaymentInfo.creationTimestamp}</strong></td>
								</tr>
							</tbody>
						</table><br />
                       
					</div>			
				</form>
		</div>
			
	</div><!--  end of span9 -->
</div><!-- end row-fluid -->
		