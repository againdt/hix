package com.getinsured.eligibility.at.resp.service;

import com.getinsured.iex.ssap.model.SsapApplicantEvent;


public interface SsapApplicantEventService {

	SsapApplicantEvent createDoBApplicantEvent();
	SsapApplicantEvent createApplicantEvent();
}
