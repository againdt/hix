package com.getinsured.ssap.model.tribe;

import java.util.List;

import com.getinsured.hix.ssap.models.AmericanAlaskaTribe;

/**
 * Response for State -> Tribe list.
 *
 * @author Yevgen Golubenko
 * @since 4/16/19
 */
public class TribeResponse
{
  /**
   * State Name.
   * For example California.
   */
  private String name;

  /**
   * Two letter state code.
   * For example CA.
   */
  private String code;

  /**
   * List of tribes for current state.
   */
  private List<AmericanAlaskaTribe> tribes;

  public String getName()
  {
    return name;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

  public String getCode()
  {
    return code;
  }

  public void setCode(final String code)
  {
    this.code = code;
  }

  public List<AmericanAlaskaTribe> getTribes()
  {
    return tribes;
  }

  public void setTribes(final List<AmericanAlaskaTribe> tribes)
  {
    this.tribes = tribes;
  }
}
