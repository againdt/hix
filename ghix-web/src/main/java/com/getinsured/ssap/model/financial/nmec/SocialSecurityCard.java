
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "socialSecurityNumber"
})
public class SocialSecurityCard implements Serializable
{

    @JsonProperty("socialSecurityNumber")
    private String socialSecurityNumber;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -4232108731803107272L;

    public SocialSecurityCard(String socialSecurityNumber) {
		this.socialSecurityNumber=socialSecurityNumber;
	}

	@JsonProperty("socialSecurityNumber")
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    @JsonProperty("socialSecurityNumber")
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("socialSecurityNumber", socialSecurityNumber).append("additionalProperties", additionalProperties).toString();
    }

}
