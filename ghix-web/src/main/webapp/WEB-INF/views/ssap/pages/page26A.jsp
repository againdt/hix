<%@ include file="../include.jsp" %>
 
<div id="appscr76P1" style="display: none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page26.healthInsuranceFor" text="Health Insurance Information for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr76P1Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="control-group alert alert-info">
				<p>
					<spring:message code="ssap.page27A.needHelpForAnswering" text="Need help answering these questions?" />
					<spring:message code="ssap.page27A.clickTheButtonToDownloadHelpForm" text="Click the button to download a form that can help you collect the information from your employer." />
				</p>
				<a href="resources/employer-coverage-tool.pdf" class="btn btn-primary pull-right"  target="_blank">
					<spring:message code="ssap.page27A.ESIForm" text="ESI Form" />
				</a>
			</div><!-- control-group ends -->
			<div class="gutter10">
			<%-- <div class="control-group">
				<p>
					<spring:message code="ssap.was" text="Was" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page26A.uninsuredWithin" text="uninsured within the last six months?" />
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<label>
						<input type="radio" id="wasUninsuredFromLast6MonthYes" name="wasUninsuredFromLast6Month" value="yes" required parsley-error-message="<spring:message code='ssap.required.field' text='This value is required.' />" />
						<spring:message code="ssap.yes" text="Yes" />
					</label>
					<label>
						<input type="radio" id="wasUninsuredFromLast6MonthNo" name="wasUninsuredFromLast6Month" value="no"><spring:message code="ssap.no" text="No" />
					</label>
					<span class="errorMessage"></span>
				</div><!-- end of controls-->
			</div><!-- control-group ends --> --%>
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page26A.Is" text="Is" />
					<strong class="nameOfHouseHold"></strong> 
					<spring:message code="ssap.page26A.OfferedHealthCoverage" text="offered health coverage through a job (even if it's from another person's job, like a spouse)?" />
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<label class="radio" for="isHealthCoverageThroughJobYes" >
						<input id="isHealthCoverageThroughJobYes" name="isHealthCoverageThroughJob" onclick="displayWillBeOfferedFromJob()" type="radio" value="yes" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#isHealthCoverageThroughJobYesErrorDiv" />
						<spring:message code="ssap.yes" text="Yes" /> 
					</label>
					<label class="radio" for="isHealthCoverageThroughJobNo">
						<input id="isHealthCoverageThroughJobNo" name="isHealthCoverageThroughJob" onclick="displayWillBeOfferedFromJob()" type="radio" value="no"> <spring:message code="ssap.no" text="No" />
					</label>
					<div id="isHealthCoverageThroughJobYesErrorDiv"></div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group hide" id="willBeOfferedFromJob">
				<p>
					<spring:message code="ssap.page26.will" text="Will" />
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page26A.beOfferedHealthCoverage" text="be offered health coverage from a job (even if it's from another person's job, like a " />
					<spring:message code="ssap.page26A.spouseGuardian" text="spouse or parent/guardian) in" />
					<span class='coverageYear'></span>?
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</p><!-- end of label -->
				<div class="margin5-l controls">
					<label for="willHealthCoverageThroughJobYes" class="radio">
						<input id="willHealthCoverageThroughJobYes" name="willHealthCoverageThroughJob" type="radio" value="yes" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#willHealthCoverageThroughJobYesErrorDiv" />
						<spring:message code="ssap.yes" text="Yes" /> 
					</label>
					<label class="radio" for="willHealthCoverageThroughJobNo">
						<input id="willHealthCoverageThroughJobNo" name="willHealthCoverageThroughJob" type="radio" value="no"/>
						<spring:message code="ssap.no" text="No" />
					</label>
					<div id="willHealthCoverageThroughJobYesErrorDiv"></div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div id="appscr76P1FirstHideAndShowDiv" class="hide">
				<div class="control-group">
					<label for="appscr76P1HealthCoverageDate">
						<spring:message code="ssap.page26A.date" text="Date" />
						<strong class="nameOfHouseHold"></strong>
						<spring:message code="ssap.page26A.couldStart" text="could start coverage:" />
						<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					</label><!-- end of label -->
					<div class="margin5-l controls">
						<div data-date=""  class="input-append date date-picker">
							<input type="text" class="input-medium" name="appscr76P1HealthCoverageDate" id="appscr76P1HealthCoverageDate" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr76P1HealthCoverageDateErrorDiv" data-parsley-pattern = "^(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d$" data-parsley-pattern-message ="<spring:message code = 'ssap.required.date.format'/>" />
							<span class="add-on dateSpan" ><i class="icon-calendar"></i></span>
						</div>
						<div id="appscr76P1HealthCoverageDateErrorDiv"></div>
					</div><!-- end of controls-->
					<%-- <label class="checkbox clearfix margin5-l">
						<input type="checkbox" /> <span><spring:message code="ssap.iDontKnow" text="I don't know" /></span>
					</label> --%>
				</div><!-- control-group ends -->
			
			<div id="memberNameDataDivHide">
				<h4>
					<spring:message code="ssap.page26A.tellUsWho" text="Tell us who" />
					<!-- <strong class="houseHoldNameHereForAll"></strong> -->
					<spring:message code="ssap.page26A.offerHealth" text=" offer health coverage:" />
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
				</h4><!-- end of label -->
			</div><!-- control-group ends -->
			
			<div class="memberNameDataDiv control-group" id="memberNameDataDiv">
				<div class="gutter10-t gutter10-b gutter10-l">
				<div id="employerOfJobDiv" class="hide">
					<div class="margin5-l">
						<label class="checkbox" for="appscr76P1CheckBoxForEmployeeName">
							<input type="checkbox" id="appscr76P1CheckBoxForEmployeeName" name="healthCoverageEmployee" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr76P1CheckBoxForEmployeeNameErrorDiv" />
							<strong class="houseHoldNameHere"><spring:message code="ssap.page26.employerName" text="Employer Name" /> </strong>
						</label>
					</div><!-- controls ends -->
				
					<div id="memberNameDataDivPart1">
						<div class="control-group">
							<label class="control-label" for="memberNameDataEinText">
								<spring:message code="ssap.page26.employerIdentificatiuonNumber" text="Employer Identification Number" />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text" placeholder="<spring:message code='ssap.page26A.employerEIN' text='Employer EIN' />" id="memberNameDataEinText" data-parsley-pattern="(^[0-9]{9}$)" data-parsley-pattern-message="<spring:message code="ssap.error.employerIdentificationNumValid"/>"/>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						
						<h4 class="margin20-b">
							<spring:message code="ssap.page26.employerAddress" text="Employer Address" />
						</h4>
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataAddress1">
								<spring:message code='ssap.address1' text='Address 1' />
								<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text"  id="memberNameDataAddress1" placeholder="<spring:message code='ssap.address1' text='Address 1' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\,]{1,30}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.addressRequired'/>"/>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataAddress2">
								<spring:message code='ssap.address2' text='Address 2' />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text"  id="memberNameDataAddress2" placeholder="<spring:message code='ssap.address2' text='Address 2' />" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\,]{1,30}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>"/>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataCity">
								<spring:message code='ssap.city' text='City' />
								<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text" id="memberNameDataCity" placeholder="<spring:message code='ssap.city' text='City' />"  data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cityRequired'/>"  data-parsley-pattern="^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="<spring:message code='ssap.error.cityValid'/>" data-parsley-length="[1, 20]" data-parsley-length-message="<spring:message code='ssap.error.cityValid'/>" />
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataZip">
								<spring:message code='ssap.zip' text='Zip' />
								<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text" id="memberNameDataZip" placeholder="<spring:message code='ssap.zip' text='Zip' />" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.zipRequired'/>" data-parsley-pattern="^[0-9]{5}(\-[0-9]{4})?$"  data-parsley-pattern-message="<spring:message code='ssap.error.zipValid'/>" />
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataState">
								<spring:message code="ssap.State" text="State" />
								<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
							</label><!-- end of label -->
							<div class="controls">
								<select id="memberNameDataState" class="input-large" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.stateRequired'/>">
									<option value=''><spring:message code="ssap.State" text="State" /></option>
									<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
									<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
									<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
									<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
									<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
									<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
									<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
									<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
									<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
									<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
									<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
									<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
									<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
									<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
									<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
									<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
									<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
									<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
									<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
									<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
									<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
									<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
									<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
									<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
									<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
									<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
									<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
									<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
									<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
									<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
									<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
									<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
									<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
									<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
									<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
									<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
									<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
									<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
									<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
									<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
									<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
									<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
									<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
									<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
									<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
									<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
									<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
									<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
									<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
									<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
									<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
									<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
									<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
									<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
									<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
									<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>
								</select>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						
						<div class="control-group">
							<label class="control-label" for="memberNameDataPhnNum">
								<spring:message code="ssap.page26.employerPhoneNumber" text="Employer Phone Number" />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium pull-left phoneNumber" type="text" id="memberNameDataPhnNum" placeholder="(xxx) xxx-xxxx" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#memberNameDataPhnNumErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
								<label class="control-label aria-hidden" for="memberNameDataExt">
								<spring:message code="ssap.page26.employerPhoneNumber" text="Employer Phone Number" />
							</label><!-- end of label --> 
								<input class="input-small pull-left margin5-l phoneNumberExt" type="text" id="memberNameDataExt" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#memberNameDataExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>">
								<div id="memberNameDataPhnNumErrorDiv"></div>
								<div id="memberNameDataExtErrorDiv"></div>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
						
						
						
						<h4 class="margin20-b">
							<spring:message code="ssap.page26.whoCanWeContact" text="Who can we contact at this employer?" />
							<small><spring:message code="ssap.page26.ifYouAreNot" text="If you are not sure, ask your employer (optional)" /></small>
						</h4>
						
						<div class="control-group">
							<label class="control-label" for="appscr76P1EmployerContactName">
								<spring:message code="ssap.page26.employerContactName" text="Employer Contact Name" />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text" id="appscr76P1EmployerContactName" placeholder="<spring:message code='ssap.page26.employerContactName' text='Employer Contact Name' />" name="appscr76P1EmployerContactName" data-parsley-length="[1, 256]" data-parsley-length-message="<spring:message code='ssap.error.employerNameValid'/>">
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
	
										
						<div class="control-group">
							<label class="control-label" for="appscr76P1_firstPhoneNumber">
								<spring:message code="ssap.phoneNo" text="Phone No" />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium phoneNumber pull-left" type="text" value="" placeholder="(xxx) xxx-xxxx" id="appscr76P1_firstPhoneNumber" name="appscr76P1_firstPhoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr76P1_firstPhoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>">
								<label class="control-label aria-hidden" for="appscr76P1_firstExt">
									<spring:message code="ssap.phoneNo" text="Phone No" />
								</label><!-- end of label -->
								<input class="input-small pull-left margin5-l phoneNumberExt" type="text" value="" placeholder="Ext." id="appscr76P1_firstExt" name="appscr76P1_firstExt" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr76P1_firstExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>">
								<label class="control-label aria-hidden" for="appscr76P1_firstPhoneType">
									<spring:message code="ssap.phoneNo" text="Phone No" />
								</label><!-- end of label -->
								<select id="appscr76P1_firstPhoneType" class="input-medium margin5-l">
									<option value="0"><spring:message code="ssap.phoneType" text="Phone Type" /></option>
									<option value="Cell"><spring:message code="ssap.cellPhone" text="Cell" /></option>
									<option value="Home"><spring:message code="ssap.homePhone" text="Home" /></option>
									<option value="Work"><spring:message code="ssap.workPhone" text="Work" /></option>
								</select>
								<div id="appscr76P1_firstPhoneNumberErrorDiv"></div>
								<div id="appscr76P1_firstExtErrorDiv"></div>
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
	
						
						<div class="control-group">
							<label class="control-label">
								<spring:message code="ssap.email" text="Email" />
							</label><!-- end of label -->
							<div class="controls">
								<input class="input-medium" type="text" id="appscr76P1_emailAddress" value="" placeholder="<spring:message code='ssap.page26.emailAddress' text='Email Address' />" name="appscr76P1_emailAddress" data-parsley-pattern="(^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$)"  data-parsley-pattern-message="<spring:message code='ssap.error.emailValid'/>" data-parsley-length="[5, 100]" data-parsley-length-message="<spring:message code='ssap.error.emailValid'/>" >
							</div><!-- end of controls-->
						</div><!-- control-group ends -->
					
					</div><!-- memberNameDataDivPart1 ends -->
				</div><!-- employerOfJobDiv ends -->
				
				<div class="margin5-l">
					<label class="checkbox" for="appscr76P1CheckBoxForOtherEmployee">
						<input type="checkbox"  id="appscr76P1CheckBoxForOtherEmployee"  name="healthCoverageEmployee" data-parsley-required data-parsley-required-message="<spring:message code='ssap.required.field' />" data-parsley-errors-container="#appscr76P1CheckBoxForEmployeeNameErrorDiv"/>
						<strong class="houseHoldNameHere"><spring:message code="ssap.page26.otherEmployer" text="Other Employer" /></strong>
					</label>
				</div><!-- controls ends -->
				
				<div id="memberNameDataDivPart2" class="margin20-t">
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerName">
							<spring:message code="ssap.page26.employerName" text="Employer Name" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" id="appscr76P1_otherEmployerName" type="text" placeholder="<spring:message code='ssap.page26.employerName' text='Employer Name' />" name="appscr76P1_otherEmployerName" data-parsley-length="[8, 256]" data-parsley-length-message="<spring:message code='ssap.error.employerNameValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.employerNameRequired'/>" />
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_employerEIN">
							<spring:message code="ssap.page26.employerIdentificatiuonNumber" text="Employer Identification Number" />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_employerEIN" placeholder="<spring:message code='ssap.page26A.employerEIN' text='Employer EIN' />" name="appscr76P1_employerEIN" data-parsley-pattern="(^[0-9]{9}$)" data-parsley-pattern-message="<spring:message code='ssap.error.employerIdentificationNumValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<h4 class="margin20-b">
						<spring:message code="ssap.page26.employerAddress" text="Employer Address" />
					</h4>
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerAddress1">
							<spring:message code='ssap.address1' text='Address 1' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployerAddress1" placeholder="<spring:message code='ssap.address1' text='Address 1' />" name="appscr76P1_otherEmployerAddress1" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\,]{1,30}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.addressRequired'/>"/>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerAddress2">
							<spring:message code='ssap.address2' text='Address 2' />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployerAddress2" placeholder="<spring:message code='ssap.address2' text='Address 2' />" name="appscr76P1_otherEmployerAddress2" data-parsley-pattern="^[A-Za-z0-9'\.\-\s\,]{1,30}$"  data-parsley-pattern-message="<spring:message code='ssap.error.addressValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerCity">
							<spring:message code='ssap.city' text='City' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployerCity" placeholder="<spring:message code='ssap.city' text='City' />" name="appscr76P1_otherEmployerCity" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.cityRequired'/>"  data-parsley-pattern="^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="<spring:message code='ssap.error.cityValid'/>" data-parsley-length="[1, 20]" data-parsley-length-message="<spring:message code='ssap.error.cityValid'/>"/>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerZip">
							<spring:message code='ssap.zip' text='Zip' />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployerZip" placeholder="<spring:message code='ssap.zip' text='Zip' />" name="appscr76P1_otherEmployerZip" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.zipRequired'/>" data-parsley-pattern="^[0-9]{5}(\-[0-9]{4})?$"  data-parsley-pattern-message="<spring:message code='ssap.error.zipValid'/>"/>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerState">
							<spring:message code="ssap.State" text="State" />
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
						</label><!-- end of label -->
						<div class="controls">
							<select id="appscr76P1_otherEmployerState" class="input-large" name="appscr76P1_otherEmployerState" data-parsley-required data-parsley-required-message="<spring:message code='ssap.error.stateRequired'/>">
								<option value=''><spring:message code="ssap.State" text="State" /></option>
								<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
								<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
								<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
								<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
								<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
								<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
								<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
								<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
								<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
								<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
								<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
								<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
								<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
								<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
								<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
								<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
								<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
								<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
								<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
								<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
								<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
								<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
								<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
								<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
								<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
								<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
								<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
								<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
								<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
								<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
								<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
								<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
								<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
								<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
								<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
								<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
								<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
								<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
								<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
								<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
								<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
								<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
								<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
								<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
								<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
								<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
								<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
								<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
								<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
								<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
								<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
								<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
								<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
								<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
								<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
								<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>
							</select>
							<span class="errorMessage"></span>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_secondPhoneNumber" >
							<spring:message code="ssap.page26.employerPhoneNumber" text="Employer Phone Number" />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium pull-left phoneNumber" type="text" placeholder="(xxx) xxx-xxxx" id="appscr76P1_secondPhoneNumber" name="appscr76P1_secondPhoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr76P1_secondPhoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/> 
							<label class="control-label aria-hidden" for="appscr76P1_secondExt">
								<spring:message code="ssap.page26.employerPhoneNumber" text="Employer Phone Number" />
							</label><!-- end of label -->
							<input class="input-small pull-left margin5-l phoneNumberExt" id="appscr76P1_secondExt" type="text" value="" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" name="appscr76P1_secondExt" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr76P1_secondExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>">
							<div id="appscr76P1_secondPhoneNumberErrorDiv"></div>
							<div id="appscr76P1_secondExtErrorDiv"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<h4 class="margin20-b">
						<spring:message code="ssap.page26.whoCanWeContact" text="Who can we contact at this employer?" />
						<small><spring:message code="ssap.page26.ifYouAreNot" text="If you are not sure, ask your employer (optional)" /></small>
					</h4>
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployerContactName">
							<spring:message code="ssap.page26B.employerContactName" text="Employer Contact Name" />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployerContactName" placeholder="<spring:message code='ssap.page26B.employerContactName' text='Employer Contact Name' />" name="appscr76P1_otherEmployerContactName" data-parsley-length="[1, 256]" data-parsley-length-message="<spring:message code='ssap.error.employerNameValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_thirdPhoneNumber">
							<spring:message code="ssap.phoneNo" text="Phone No" />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium pull-left phoneNumber" type="text" placeholder="(xxx) xxx-xxxx" id="appscr76P1_thirdPhoneNumber" name="appscr76P1_thirdPhoneNumber" data-parsley-pattern="^\(\d{3}\) \d{3}-\d{4}$" data-parsley-errors-container="#appscr76P1_thirdPhoneNumberErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumValid'/>"/>
							<label class="control-label aria-hidden" for="appscr76P1_thirdExt">
								<spring:message code="ssap.phoneNo" text="Phone No" />
							</label><!-- end of label -->
							<input class="input-small pull-left margin5-l phoneNumberExt" type="text" placeholder="<spring:message code='ssap.phoneExt' text='Ext.' />" id="appscr76P1_thirdExt" name="appscr76P1_thirdExt" data-parsley-pattern="^\d{4}$" data-parsley-errors-container="#appscr76P1_thirdExtErrorDiv" data-parsley-pattern-message="<spring:message code='ssap.error.phoneNumExtValid'/>"/>
							<label class="control-label aria-hidden" for="appscr76P1_thirdPhoneType">
								<spring:message code="ssap.phoneNo" text="Phone No" />
							</label><!-- end of label -->
							<select id="appscr76P1_thirdPhoneType" class="input-medium margin5-l">
								<option value="0"><spring:message code="ssap.phoneType" text="Phone Type" /></option>
								<option value="Cell"><spring:message code="ssap.cellPhone" text="Cell" /></option>
								<option value="Home"><spring:message code="ssap.homePhone" text="Home" /></option>
								<option value="Work"><spring:message code="ssap.workPhone" text="Work" /></option>
							</select>
							<div id="appscr76P1_thirdPhoneNumberErrorDiv"></div>
							<div id="appscr76P1_thirdExtErrorDiv"></div>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
					<div class="control-group">
						<label class="control-label" for="appscr76P1_otherEmployer_emailAddress">
							<spring:message code="ssap.email" text="Email" />
						</label><!-- end of label -->
						<div class="controls">
							<input class="input-medium" type="text" id="appscr76P1_otherEmployer_emailAddress" placeholder="<spring:message code='ssap.page26.emailAddress' text='Email Address' />" name="appscr76P1_otherEmployer_emailAddress" data-parsley-pattern="(^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$)"  data-parsley-pattern-message="<spring:message code='ssap.error.emailValid'/>" data-parsley-length="[5, 100]" data-parsley-length-message="<spring:message code='ssap.error.emailValid'/>"/>
						</div><!-- end of controls-->
					</div><!-- control-group ends -->
					
				</div><!-- memberNameDataDivPart2 ends -->
				
				<div id="appscr76P1CheckBoxForEmployeeNameErrorDiv"></div>
				</div><!-- gutter10 -->
			</div><!-- memberNameDataDiv ends -->
		</div><!-- appscr76P1FirstHideAndShowDiv ends -->
		</div><!-- .gutter10 -->
		</form>
	</div>
</div>