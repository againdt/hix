package com.getinsured.referraltask;

/**
 * @author chopra_s
 * 
 */
public interface ReferralTasks {

	void processRidpAfterDocumentApproval(Long referralId);

}
