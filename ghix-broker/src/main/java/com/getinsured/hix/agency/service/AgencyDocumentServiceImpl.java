package com.getinsured.hix.agency.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.getinsured.hix.agency.repository.IAgencyDocumentRepository;
import com.getinsured.hix.model.agency.AgencyDocument;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;

@Service
public class AgencyDocumentServiceImpl implements AgencyDocumentService{
	
	private static final String ORACLE = "ORACLE";
	
	@Autowired
	private ContentManagementService ecmService;

	@Value("#{configProp['ecm.type']}")
	private String ecmType;
	
	@Autowired
	IAgencyDocumentRepository iAgencyDocumentRepository;
	
	@Override
	public AgencyDocument saveAgencyDocument(AgencyDocument agencyDocument) {
		return iAgencyDocumentRepository.save(agencyDocument);
	}

	@Override
	public AgencyDocument findById(Long id) {
		return iAgencyDocumentRepository.findById(id);
	}
	
	@Override
	public List<AgencyDocument> findByAgencyId(Long agencyId){
		return iAgencyDocumentRepository.findByAgencyId(agencyId);
	}
	
}
