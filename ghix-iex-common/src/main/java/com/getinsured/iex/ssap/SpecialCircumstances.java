
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class SpecialCircumstances {

    @Expose
    private Integer ageWhenLeftFosterCare;
    @Expose
    private Boolean everInFosterCareIndicator=false;
    @Expose
    private Boolean fosterChild;
    @Expose
    private Integer numberBabiesExpectedInPregnancy;
    @Expose
    private Boolean pregnantIndicator=false;
    @Expose
    private Boolean gettingHealthCareThroughStateMedicaidIndicator=false;
    @Expose
    private String fosterCareState;
    @Expose
    private Boolean tribalManualVerificationIndicator=false;
    @Expose
    private Boolean americanIndianAlaskaNativeIndicator=false;

    @Expose
    private boolean metFosterCareConditions;

    public Integer getAgeWhenLeftFosterCare() {
        return ageWhenLeftFosterCare;
    }

    public void setAgeWhenLeftFosterCare(Integer ageWhenLeftFosterCare) {
        this.ageWhenLeftFosterCare = ageWhenLeftFosterCare;
    }

    public Boolean getEverInFosterCareIndicator() {
        return everInFosterCareIndicator;
    }

    public void setEverInFosterCareIndicator(Boolean everInFosterCareIndicator) {
        this.everInFosterCareIndicator = everInFosterCareIndicator;
    }

    public Boolean getFosterChild() {
        return fosterChild;
    }

    public void setFosterChild(Boolean fosterChild) {
        this.fosterChild = fosterChild;
    }

    public Integer getNumberBabiesExpectedInPregnancy() {
        return numberBabiesExpectedInPregnancy;
    }

    public void setNumberBabiesExpectedInPregnancy(Integer numberBabiesExpectedInPregnancy) {
        this.numberBabiesExpectedInPregnancy = numberBabiesExpectedInPregnancy;
    }

    public Boolean getPregnantIndicator() {
        return pregnantIndicator;
    }

    public void setPregnantIndicator(Boolean pregnantIndicator) {
        this.pregnantIndicator = pregnantIndicator;
    }

    public Boolean getGettingHealthCareThroughStateMedicaidIndicator() {
        return gettingHealthCareThroughStateMedicaidIndicator;
    }

    public void setGettingHealthCareThroughStateMedicaidIndicator(Boolean gettingHealthCareThroughStateMedicaidIndicator) {
        this.gettingHealthCareThroughStateMedicaidIndicator = gettingHealthCareThroughStateMedicaidIndicator;
    }

    public String getFosterCareState() {
        return fosterCareState;
    }

    public void setFosterCareState(String fosterCareState) {
        this.fosterCareState = fosterCareState;
    }

    public Boolean getTribalManualVerificationIndicator() {
        return tribalManualVerificationIndicator;
    }

    public void setTribalManualVerificationIndicator(Boolean tribalManualVerificationIndicator) {
        this.tribalManualVerificationIndicator = tribalManualVerificationIndicator;
    }

    public Boolean getAmericanIndianAlaskaNativeIndicator() {
        return americanIndianAlaskaNativeIndicator;
    }

    public void setAmericanIndianAlaskaNativeIndicator(Boolean americanIndianAlaskaNativeIndicator) {
        this.americanIndianAlaskaNativeIndicator = americanIndianAlaskaNativeIndicator;
    }

    public boolean isMetFosterCareConditions() {
        return metFosterCareConditions;
    }

    public void setMetFosterCareConditions(boolean metFosterCareConditions) {
        this.metFosterCareConditions = metFosterCareConditions;
    }
}
