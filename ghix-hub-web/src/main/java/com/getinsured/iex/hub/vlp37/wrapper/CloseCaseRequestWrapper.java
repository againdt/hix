package com.getinsured.iex.hub.vlp37.wrapper;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import com.getinsured.iex.hub.platform.FDSHDataTypeWrapper;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlpiav.InitiateAdditionalVerifRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseRequestType;

public class CloseCaseRequestWrapper implements JSONAware, FDSHDataTypeWrapper {
	
	private JAXBElement<CloseCaseRequestType> closeCaseRequestTypeElement  = null;
	private CloseCaseRequestType requestType;
	
	private String caseNumber;
	private static final String CASE_NUMBER = "caseNumber";
	
	public CloseCaseRequestWrapper(){
		ObjectFactory factory = new ObjectFactory();
		this.requestType = factory.createCloseCaseRequestType();
		this.closeCaseRequestTypeElement = new JAXBElement<CloseCaseRequestType>(new QName("http://vclpcc.ee.sim.dsh.cms.hhs.gov", "CloseCaseRequest"), CloseCaseRequestType.class, this.requestType);
	}
	
	public void setCaseNumber(String caseNumber) throws HubServiceException{
		
		if(caseNumber == null){
			throw new HubServiceException("No case number provided");
		}
		
		this.caseNumber = caseNumber;
		this.requestType.setCaseNumber(caseNumber);
	}
	
	public CloseCaseRequestType getRequest() throws HubServiceException {
		return this.requestType;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}

	/**
	 * Converts the JSON Object to CloseCaseRequestWrapper object
	 * 
	 * @param obj - JSON representation for CloseCaseRequestWrapper
	 * @return CloseCaseRequestWrapper Object
	 */
	public static CloseCaseRequestWrapper fromJSONObject(JSONObject obj) throws HubServiceException {
		CloseCaseRequestWrapper wrapper = new CloseCaseRequestWrapper();
		wrapper.setCaseNumber((String)obj.get(CASE_NUMBER));
		return wrapper;
	}
	
	/**
	 * Converts the JSON String to CloseCaseRequestWrapper object
	 * 
	 * @param jsonString - String representation for CloseCaseRequestWrapper
	 * @return CloseCaseRequestWrapper Object
	 */
	public static CloseCaseRequestWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create CloseCaseRequestWrapper from null or empty input");
		}

		Object tmpObj = null;
		JSONParser parser = new JSONParser();
		CloseCaseRequestWrapper wrapper = new CloseCaseRequestWrapper();
		JSONObject obj;
		try {
			obj = (JSONObject) parser.parse(jsonString);
		} catch (ParseException e) {
			throw new HubServiceException("Failed to parse the input",e);
		}
		
		tmpObj = obj.get(CASE_NUMBER);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create CloseCaseRequestWrapper, No case number found");
		}
		wrapper.caseNumber = (String)tmpObj;
		wrapper.requestType.setCaseNumber(wrapper.caseNumber);
		
		return wrapper;
	}
	
	public JAXBElement<CloseCaseRequestType> getSystemRepresentation(){
		return this.closeCaseRequestTypeElement;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNumber);
		return obj.toJSONString();
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNumber);
		return obj;
	}
}
