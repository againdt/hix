import { APP_STATUSES, APP_TYPES, VALIDATION_STATUSES } from  '../../constants/dashboardModelTypes';
import { isEnrolledInDental, isAIAN, isPeriodWithinDates, isPriorToOEStartDate, isChangePlans, isSuperUser, isRenewalStatusSet, isQHPEligible } from '../processDashboardModel';

const conditions = {
    // INSIDE OE or OUTSIDE OE
    'c_tabIsInsideOE': (sm, { isInsideOEWindow }) => isInsideOEWindow,
    'c_tabIsOutsideOE': (sm, { isInsideOEWindow }) => !isInsideOEWindow,

    'c_tabIsInsideOEEnrollment': (sm, {isInsideOEEnrollmentWindow}) => isInsideOEEnrollmentWindow,
    'c_tabIsOutsideOEEnrollment': (sm, {isInsideOEEnrollmentWindow}) => !isInsideOEEnrollmentWindow,

    //APPLICATION STATUSES
    'c_AppStatusER': (sm, { applicationStatus }) => applicationStatus === APP_STATUSES.ER,
    'c_AppStatusPN': (sm, { applicationStatus }) => applicationStatus === APP_STATUSES.PN,
    'c_AppStatusEN': (sm, { applicationStatus }) => applicationStatus === APP_STATUSES.EN,
    'c_AppStatusOP': (sm, { applicationStatus }) => applicationStatus === APP_STATUSES.OP,
    'c_AppStatusQU': (sm, { applicationStatus }) => applicationStatus === APP_STATUSES.QU,
    'c_AppStatusEN_PN_ER': (sm, model) => {
        const appStatuses = [APP_STATUSES.EN, APP_STATUSES.PN, APP_STATUSES.ER];
        return appStatuses.includes(model.applicationStatus);
    },
    'c_AppStatusEN_PN': (sm, model) => {
        const appStatuses = [APP_STATUSES.EN, APP_STATUSES.PN];
        return appStatuses.includes(model.applicationStatus);
    },
    'c_AppStatusSG_SU': (sm, model) => {
        const appStatuses = [APP_STATUSES.SG, APP_STATUSES.SU];
        return appStatuses.includes(model.applicationStatus);
    },
    'c_AppStatusGroup1': (sm, model) => {
        if (!model.applicationStatus) {
            return true;
        }
        const appStatuses = [APP_STATUSES.CL, APP_STATUSES.UC, APP_STATUSES.CC];
        return appStatuses.includes(model.applicationStatus);
    },
    'c_AppStatusGroup2': (sm, model) => {
        if (!model.applicationStatus) {
            return true;
        }
        const appStatuses = [ APP_STATUSES.OP, APP_STATUSES.CL, APP_STATUSES.UC, APP_STATUSES.CC ];
        return appStatuses.includes(model.applicationStatus);
    },

    // APPLICATION TYPES
    'c_AppTypeQEP': (sm, { applicationType }) => applicationType === APP_TYPES.QEP,
    'c_AppTypeSEP': (sm, { applicationType }) => applicationType === APP_TYPES.SEP,
    'c_AppTypeOEP': (sm, { applicationType }) => applicationType === APP_TYPES.OE,

    // EVENTS
    'c_eventIdentified': (sm, { sepEvent }) => sepEvent.isIdentified,
    'c_eventNotIdentified': (sm, { sepEvent }) => !sepEvent.isIdentified,

    'c_eventVerified': (sm, { sepEvent }) => sepEvent.isVerified,
    'c_eventNotVerified': (sm, { sepEvent }) => !sepEvent.isVerified,

    'c_verificationStatusPending': (sm, { validationStatus }) => validationStatus === VALIDATION_STATUSES.PENDING,
    'c_verificationStatusPendingInfo': (sm, { validationStatus }) => validationStatus === VALIDATION_STATUSES.PENDING_INFO,

    // OTHER
    'c_SEPOpen': (sm, { isSepOpen }) => isSepOpen,
    'c_SEPNotOpen': (sm, { isSepOpen }) => !isSepOpen,

    'c_enrolledInDental': (sm, { enrollments }, config) => isEnrolledInDental(enrollments, config),
    'c_notEnrolledInDental': (sm, { enrollments }, config) => !isEnrolledInDental(enrollments, config),

    'c_dentalAppStatusEN': (sm, { applicationDentalStatus }) => applicationDentalStatus === 'EN',
    'c_dentalAppStatusNotEN': (sm, { applicationDentalStatus }) => applicationDentalStatus !== 'EN',

    'c_AIAN': (sm, { applicants }) => isAIAN(applicants),
    'c_notAIAN': (sm, { applicants }) => !isAIAN(applicants),

    'c_changePlans': (sm, { enrollments }, config) => isChangePlans(enrollments, config),
    'c_notChangePlans': (sm, { enrollments }, config) => !isChangePlans(enrollments, config),

    'c_supervisor': (sm, { defaultRoleName }) => isSuperUser(defaultRoleName),
    'c_notSupervisor': (sm, { defaultRoleName }) => !isSuperUser(defaultRoleName),


    // DATES
    'c_periodWithinDates': (sm, model, config) => isPeriodWithinDates(config),
    'c_periodNotWithinDates': (sm, model, config) => !isPeriodWithinDates(config),
    'c_priorToOEStartDate': (sm, {applicationYear}, config) => isPriorToOEStartDate(applicationYear, config),
    'c_notPriorToOEStartDate': (sm, {applicationYear}, config) => !isPriorToOEStartDate(applicationYear, config),

    // RENEWALS
    'c_renewalStatusSet': (sm, { healthRenewalStatus, dentalRenewalStatus }) => isRenewalStatusSet(healthRenewalStatus, dentalRenewalStatus),
    'c_renewalStatusNotSet': (sm, { healthRenewalStatus, dentalRenewalStatus }) => !isRenewalStatusSet(healthRenewalStatus, dentalRenewalStatus),

    'c_QHPEligible': (sm, {applicants}) => isQHPEligible(applicants),
    'c_QHPNotEligible': (sm, {applicants}) => !isQHPEligible(applicants),
};

export default conditions;