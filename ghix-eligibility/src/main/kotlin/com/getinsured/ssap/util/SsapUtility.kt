package com.getinsured.ssap.util

import com.getinsured.iex.ssap.HouseholdMember
import com.getinsured.iex.ssap.TaxHousehold
import java.util.*

/**
 * SSAP utility methods.
 *
 * @author Yevgen Golubenko
 * @since 2019-11-21
 */
class SsapUtility {
  companion object {
    /**
     * Returns primary tax household member by checking [TaxHousehold.primaryTaxFilerPersonId].
     * If there is no member found with personId equal [TaxHousehold.primaryTaxFilerPersonId], then it finds
     * member where [HouseholdMember.personId] equals to 1, if that fails, it just returns 1st member in the list.
     * @param taxHousehold [TaxHousehold] that contains household members.
     */
    @JvmStatic
    fun getPrimaryTaxHouseholdMember(taxHousehold: TaxHousehold): HouseholdMember {
      return Optional.ofNullable(
          taxHousehold.householdMember.find {
            hhm -> hhm.personId == taxHousehold.primaryTaxFilerPersonId
          })
          .orElseGet {
            Optional.ofNullable(taxHousehold.householdMember.find { hhm -> hhm.personId == 1 })
                .orElseGet {
                  taxHousehold.householdMember[0]
                }
          }
    }
  }
}
