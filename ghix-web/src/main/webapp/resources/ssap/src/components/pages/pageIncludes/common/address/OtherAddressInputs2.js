import React, { Fragment } from 'react';
import _ from 'lodash';

// Components

import UniversalInput from '../../../../../lib/UniversalInput';

// Constants

import DROPDOWN_OPTIONS from '../../../../../constants/dropdownOptions';
import INPUT_TYPES from '../../../../../constants/inputTypes';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';

const OtherAddressInputs2 = props => {

    const {
        postalCode,
        countyCode,
        streetAddress1,
        streetAddress2,
        state,
        city,
        currentData,
        counties,
        pageName,
        indexInArray,
        ...restProps
    } = props;

    const getOptions = () => {
        return DROPDOWN_OPTIONS.COUNTIES(counties, pageName);
    };

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <Fragment>
            <UniversalInput
                label={{
                    text: 'Address 1',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress1',
                    helperClasses: [],
                    value: streetAddress1,
                    placeholder: 'Address 1',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE1,
                    currentData: currentData,
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Address 2',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'streetAddress2',
                    helperClasses: [],
                    value: streetAddress2,
                    placeholder: 'Address 2',
                    validationType: VALIDATION_TYPES.ADDRESS_LINE2,
                    currentData: currentData,
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'City',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'city',
                    helperClasses: [],
                    value: city,
                    placeholder: 'City',
                    validationType: VALIDATION_TYPES.CITY,
                    currentData: currentData,
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Zip',
                    labelClasses: []
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'postalCode',
                    helperClasses: [],
                    value: postalCode,
                    placeholder: 'Zip',
                    validationType: VALIDATION_TYPES.ZIP_CODE,
                    currentData: currentData,
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'State',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'state',
                    value: state,
                    options: DROPDOWN_OPTIONS.STATE,
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: JSON.parse(JSON.stringify(currentData)),
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'County',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'countyCode',
                    value: countyCode,
                    options: getOptions(),
                    validationType: VALIDATION_TYPES.CHOSEN,
                    currentData: currentData,
                    fields: []
                }}
                indexInArray={indexInArray}
                {...cleanedRestProps}
            />
        </Fragment>
    );
};

export default OtherAddressInputs2;