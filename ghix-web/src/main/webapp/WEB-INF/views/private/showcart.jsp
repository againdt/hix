<%@ page import="com.getinsured.hix.model.GroupList,java.util.List,java.util.Map" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">
<script type="text/javascript">
  var plan_display = new Array();

  plan_display['pd.label.title.removeDentalPlan'] = "<spring:message code='pd.label.title.removeDentalPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.hideCompare'] = "<spring:message code='pd.label.title.hideCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.removeDental'] = "<spring:message code='pd.label.title.removeDental' javaScriptEscape='true'/>";
  plan_display['pd.label.selectOneMember'] = "<spring:message code='pd.label.selectOneMember' javaScriptEscape='true'/>";
  plan_display['pd.label.adultDentalSelection'] = "<spring:message code='pd.label.adultDentalSelection' javaScriptEscape='true'/>";
  plan_display['pd.label.selectOnePlan'] = "<spring:message code='pd.label.selectOnePlan' javaScriptEscape='true'/>";
  plan_display['pd.label.dentalPlanAlreadyAdded'] = "<spring:message code='pd.label.dentalPlanAlreadyAdded' javaScriptEscape='true'/>";
  plan_display['pd.label.removeThisPlan'] = "<spring:message code='pd.label.removeThisPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.selectPlanAlreadyInCart'] = "<spring:message code='pd.label.selectPlanAlreadyInCart' javaScriptEscape='true'/>";
  plan_display['pd.label.goToCart'] = "<spring:message code='pd.label.goToCart' javaScriptEscape='true'/>";
  plan_display['pd.label.title.addPlan'] = "<spring:message code='pd.label.title.addPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.alert.noItemsInCart'] = "<spring:message code='pd.label.alert.noItemsInCart' javaScriptEscape='true'/>";
  plan_display['pd.label.cart.platinumPlans'] = "<spring:message code='pd.label.cart.platinumPlans' javaScriptEscape='true'/>";
  plan_display['pd.label.cart.goldPlans'] = "<spring:message code='pd.label.cart.goldPlans' javaScriptEscape='true'/>";
  plan_display['pd.label.cart.silverPlans'] = "<spring:message code='pd.label.cart.silverPlans' javaScriptEscape='true'/>";
  plan_display['pd.label.cart.bronzePlans'] = "<spring:message code='pd.label.cart.bronzePlans' javaScriptEscape='true'/>";
  plan_display['pd.label.cart.catastrophiPlans'] = "<spring:message code='pd.label.cart.catastrophiPlans' javaScriptEscape='true'/>";
  plan_display['pd.label.dollarSign'] = "<spring:message code='pd.label.dollarSign' javaScriptEscape='true'/>";

</script>
<style>
	div {
		position: static;
	}
	.tooltip-inner {
		max-width:24em;
	  overflow: hidden;
	}
	body.modal-open {
	    overflow: hidden;
	}
	.view-broker {
		top: 2px!important;
		z-index: 100;
	}
	.topnav {
		width: 940px;
		height: 40px;
		z-index: 1000;
		border-top: none;
	}
	.top-nav {
		box-shadow: none;
		border-bottom: none;
		width: 960px;
		margin: 30px 0 0 0;
	}
	.top-nav h1 {
		margin-top: 0;
		padding-bottom: 0;
		display: inline-block;
	}

	.border-dash {
		  border: 1px dashed #ddd;
	}
	.skip-btn--add-space-left {
		margin-left: 1em;
	}
	.cart-total-heading {font-size: 14px; color: #3e3e3e; padding: 20px 5px 20px 20px !important;}
	.cart-total-heading a {color: #3e3e3e;}
	#show-cart table tr th{padding-right:5px;}
	#myCarousel h4 { padding:0; color: inherit; font-family: inherit; font-size: inherit;font-weight:600;}
	.showcart.modal {
		border: none;
		border-radius: 0;
		box-shadow: none;
	}
    #myModal.modal.fade.in{
        top: 0%;
    }
	.gtm_removecart {
	    float: right;
		font-weight: bold !important;
		padding-right: 10px;
	}

	.cart-spacer {
		padding-top: 1px;
		border-bottom: 1px solid #eee;
	}
</style>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.css"/>">
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css" />">

<script type="text/javascript">
function resizeimage(imageObj){
	var maxWidth = 140;
	var maxHeight = 40;
	if(imageObj != null){
      var width = imageObj.width;
      var height = imageObj.height;

      // Check if the current width is larger than the max
      if(width > maxWidth){
        ratio = maxWidth / width; // get ratio for scaling image
        imageObj.style.width =  maxWidth+'px'; // Set new width
        imageObj.style.height = (height * ratio)+'px'; // Scale height based on ratio
        height = height * ratio; // Reset height to match scaled image
        width = width * ratio; // Reset width to match scaled image
      }
      // Check if current height is larger than max
      if(height > maxHeight){
        ratio = maxHeight / height; // get ratio for scaling image
        imageObj.style.height= maxHeight+'px'; // Set new height
        imageObj.style.width = (width * ratio) +'px'; // Scale width based on ratio
      }
      imageObj.style.display = "block";
	}
 }
</script>

<c:if test="${stateCode == 'MN'}">
	<div style="margin-top: 80px !important; padding-right: 10px;"></div>
</c:if>

<c:if test="${healthPlanPresent == 'N' && dentalPlanPresent == 'N'}">
  <div class="shopCartEmptyBlock clearfix">
  	<h1><spring:message code="pd.label.shopcart.message.yourCart"/></h1>
	<div class="form-action gutter10">
		<p><spring:message code="pd.label.warning.showcart.emptycart"/></p>
		<c:if test="${flowType == 'PREELIGIBILITY'}">
			<c:choose>
  				<c:when test="${loggedInUser != null}">
  					<a
                        href="<c:url value='/private/preShoppingComplete'/>"
                        title="<spring:message code='pd.label.title.skipFinalize'/>"
                        id="aid_skipFinalize"
                        data-automation-id="skipFinalize"
                        class="btn btn-primary pull-right skip-btn--add-space-left gtm_skipFinalize"
                        onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.title.skipFinalize"/> Button Click', 'eventLabel': '<spring:message code="pd.label.title.skipFinalize"/> - GI'});">
                        <spring:message code="pd.label.title.skipFinalize"/>
                    </a>
  				</c:when>
  				<c:when test="${exchangeCode=='STATE' && stateCode=='CA'}">
  					<a
                        href="javascript:void(0)"
                        title="<spring:message code='pd.label.title.skipSignUp'/>"
                        class="btn btn-primary pull-right skip-btn--add-space-left js-btnApply gtm_apply"
                        id="aid_apply"
                        data-automation-id="apply"
                        onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': 'Apply Button Click', 'eventLabel': '<spring:message code="pd.label.title.skipSignUp"/> - GI'});">
                        <spring:message code="pd.label.title.skipSignUp"/>
                    </a>
					</c:when>
					<c:when test="${exchangeCode=='STATE' && stateCode=='MN'}">
						<a
												href="https://www.mnsure.org/acct-access/create-account-check.jsp"
                        title="<spring:message code='pd.label.title.skipSignUp'/>"
                        class="btn btn-primary pull-right skip-btn--add-space-left js-btnApply gtm_apply"
                        id="aid_skipSignUp_MN"
                        data-automation-id="apply"
                        onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': 'Apply Button Click', 'eventLabel': '<spring:message code="pd.label.title.skipSignUp"/> - GI'});">
                        <spring:message code="pd.label.title.skipSignUp"/>
                    </a>
  				</c:when>
  				<c:otherwise>
  					<a
                        href="<c:url value='/private/preShoppingComplete'/>"
                        title="<spring:message code='pd.label.title.skipSignUp'/>"
                        class="btn pull-right skip-btn--add-space-left gtm_preShoppingComplete"
                        id="aid_skipSignUp"
                        data-automation-id="skipSignUp"
                        onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.title.skipSignUp"/> Button Click', 'eventLabel': '<spring:message code="pd.label.title.skipSignUp"/> - GI'});">
                        <spring:message code="pd.label.title.skipSignUp"/></a>
  				</c:otherwise>
  			</c:choose>
		</c:if>
		<c:choose>
			<c:when test="${productType == 'D'}">
				<a
                    class="btn btn-primary pull-right gtm_backtoshopping"
                    id="aid_backtoshopping_dental"
                    type="button"
                    title="<spring:message code='pd.label.title.shopPlans'/>"
                    href="<c:url value='/private/planselection?insuranceType=DENTAL'/>">
                    <spring:message code="pd.label.title.shopPlans"/>
                </a>
			</c:when>
			<c:otherwise>
				<a
                    class="btn btn-primary pull-right gtm_backtoshopping"
                    id="aid_backtoshopping_health"
                    type="button"
                    title="<spring:message code='pd.label.title.shopPlans'/>"
                    href="<c:url value='/private/planselection?insuranceType=HEALTH'/>">
                    <spring:message code="pd.label.title.shopPlans"/>
                </a>
			</c:otherwise>
		</c:choose>
	</div>
  </div>
</c:if>

<c:if test="${healthPlanPresent == 'Y' || dentalPlanPresent == 'Y'}">

    <div id="shopping-modal" class="modal hide fade shoppingForModal" tabindex="-1" role="dialog" aria-labelledby="shoppingforHeader" aria-hidden="true">
		<div class="modal-header">
	        <h3 id="shoppingforHeader"><spring:message code="pd.label.title.shoppingFor"/></h3>
		</div>
	    <div class="modal-body">
	    	<table class="table table-striped margin0">
	        	<tr>
	            	<th scope="col" class="noBorder"><spring:message code="pd.label.title.applicant"/></th>
	            	<th scope="col" class="noBorder txt-center"><spring:message code="pd.label.title.dob"/></th>
	            	<th scope="col" class="noBorder txt-center"><spring:message code="pd.label.title.seekingCoverage"/></th>
	            </tr>
	            <c:set var="seekingCoverageDentalObj" value=""></c:set>
	            <c:forEach var="person" items="${personDataList}" varStatus="loop">
			    	<tr>
			        	<td scope="row" style="text-transform: capitalize;"><c:choose><c:when test="${person.firstName != null}">${fn:toLowerCase(person.firstName)}</c:when></c:choose></td>
			            <td scope="row" class="txt-center">${person.birthDay}</td>
			            <td scope="row" class="txt-center"><input aria-label="Seeking Coverage" type="checkbox" name="chkSCoverage" id="chkSCoverage${loop.count}"/></td>

			            <c:set var="temp" value='{"chk_id":"chkSCoverage${loop.count}","seekingCoverageDental":"${person.seekCoverage}","age":"${person.age}","relationship":"${person.relationship}","personId":"${person.id}"}'></c:set>
			            <c:choose>
			            	<c:when test="${loop.first}">
			            		<c:set var="seekingCoverageDentalObj" value="${temp}"></c:set>
			            	</c:when>
			            	<c:otherwise>
			            		<c:set var="seekingCoverageDentalObj" value="${seekingCoverageDentalObj},${temp}"></c:set>
			            	</c:otherwise>
			            </c:choose>
			        </tr>
				</c:forEach>
			</table>
		</div>
	    <div class="modal-footer clearfix">
	    	<div id="shopping-modal-error" class="app-error hide"></div>
	        <button class="btn btn-default pull-left" title="<spring:message code='pd.label.title.cancel'/>" data-dismiss="modal"><spring:message code='pd.label.title.cancel'/></button>
	    	<button class="btn btn-primary pull-right" title="<spring:message code='pd.label.title.updateResults'/>" data-automation-id="shopping-modal-update" id="shopping-modal-update" onclick="dentalCustomGroupUpdate();"><spring:message code="pd.label.title.updateResults"/></button>
		</div>
	</div>

	<div class="row-fluid top-nav">
		<c:choose>
  		  <c:when test="${flowType == 'PREELIGIBILITY'}">
  		  	<h1 data-automation-id="your_cart" id="aid_yourCart"><spring:message code="pd.label.shopcart.message.yourCart"/></h1>
  		  	 <div class="alert margin20-t">
  		  	 	<spring:message code="pd.label.shopcart.message.yourCart.preshopping1" htmlEscape="false"/>
  		  	 	<br/><br/><spring:message code="pd.label.shopcart.message.yourCart.preshopping2" htmlEscape="false"/>

  		  	 	<c:if test="${stateCode == 'ID' || stateCode == 'NV'}">
  		  	 	<c:choose>
  		  	 	 <c:when test="${loggedInUser != null}">
  		  	 		<spring:message code="pd.label.shopcart.message.yourCart.preshopping.finalEligility" htmlEscape="false"/>
  		  	 	</c:when>
  		  	 	 <c:otherwise>
  		  	 		<spring:message code="pd.label.shopcart.message.yourCart.preshopping.register" htmlEscape="false"/>
  		  	 	</c:otherwise>
  		  	 	</c:choose>
  		  	 	<spring:message code="pd.label.shopcart.message.yourCart.preshopping2.1" htmlEscape="false"/>
  		  	 	</c:if>
				
  		  	 	<c:if test="${(maxAptc != ''  && maxAptc > 0) || (maxAptc == 0 && exchangeCode=='STATE' && stateCode=='CA')}">
  		  	 	<br/><br/><spring:message code="pd.label.shopcart.message.yourCart.preshopping3" htmlEscape="false"/>
				</c:if>

  		  	 	<c:if test="${stateCode == 'ID'}">
					<br/><br/><spring:message code="pd.label.shopcart.message.yourCart.preshopping4" htmlEscape="false"/>
				</c:if>


  		  	 </div>
  		  </c:when>
  		  <c:when test="${enrollmentType == 'A'}">
  		  	<%-- <h1 class="span10 choose-plan"><spring:message code="pd.label.shopcart.message.header.manualRenewal"/></h1> --%>
  		  	<h1 class="span10 choose-plan"><spring:message code="pd.label.shopcart.message.header.manualRenewal"/></h1>
  		  </c:when>
  		  <c:otherwise>
  		  	<h1 class="span10 choose-plan"><spring:message code="pd.label.shopcart.message.header"/></h1>
  		  </c:otherwise>
  		</c:choose>

	</div>

	<div class="row-fluid "> <!-- Update box section for special enrollment and renewal -->
		<c:choose>
			<%-- <c:when test="${enrollmentType == 'A' && isHealthPlanChanged == 'N' && isDentalPlanChanged == 'N' && (wasHealthPlanAvailable=='Y' || wasDentalPlanAvailable=='Y')}">
			  <div class="alert margin20-t">
				<b>34<spring:message code="pd.label.showcart.text.update"/>: </b><spring:message code="pd.label.showcart.instruction.renewal"/>
			  </div>
			</c:when> --%>
			<c:when test="${enrollmentType == 'S' && (wasHealthPlanAvailable=='Y' || wasDentalPlanAvailable=='Y') && (isHealthPlanChanged == 'Y' || isDentalPlanChanged == 'Y')}">
			  <div class="alert margin20-t">
				<b><spring:message code="pd.label.showcart.text.update"/>: </b><spring:message code="pd.label.shopcart.special.message.addedNewPlan"/>
				<a href="/hix/private/setSpecEnrFlowType?shopForDifPlan=N"> <spring:message code="pd.label.shopcart.special.message.switchBack"/></a>
			  </div>
			</c:when>
		</c:choose>

		<c:if test="${isHealthPlanChanged == 'N' && isDentalPlanChanged == 'N' && (wasHealthPlanAvailable=='Y' || wasDentalPlanAvailable=='Y')}">
  		  <c:if test="${(healthPlanPresent =='Y' && healthPriceChange !='') || (healthPlanPresent =='N') || (dentalPlanPresent =='Y' && dentalPriceChange!='') || (dentalPlanPresent =='N' && oldDentalPlanId!=null) || (taxCreditChange != '')}">
			<div class="alert margin20-t">
				<c:if test="${enrollmentType == 'S'}">
					<spring:message code="pd.label.showcart.special.message.header"/>
					<spring:message code="pd.label.showcart.special.message.note"/>
				</c:if>
				<c:if test="${enrollmentType == 'I'}">
					<spring:message code="pd.label.showcart.initial.message.header"/>
				</c:if>
				<c:if test="${enrollmentType == 'A'}">
					<spring:message code="pd.label.showcart.renewal.message.header"/>
				</c:if>
				<ul style="list-style-type:disc">
				  <c:choose>
					<c:when test="${healthPlanPresent =='Y'}">
					  <c:choose>
						<c:when test="${healthPriceChange != ''}">
							<c:if test="${healthPriceChange == 'decreased'}">
								<spring:message code="pd.label.showCart.decreased"  var="priceChange" htmlEscape="false"/>
							</c:if>
							<c:if test="${healthPriceChange == 'increased'}">
								<spring:message code="pd.label.showCart.increased"  var="priceChange" htmlEscape="false"/>
							</c:if>
							<li><spring:message arguments="${priceChange},${oldHealthPremium},${healthPremium}" code="pd.label.showcart.special.message.healthPriceChange" htmlEscape="false"/></li>
						</c:when>
					  </c:choose>
					   <c:choose>
					   <c:when test="${showHealthSubscriberChanged eq true}"><li><spring:message code="pd.label.shopcart.message.yourCart.HealthSubscriberChange"/></li></c:when>
					   </c:choose>

				 	</c:when>
					<c:when test="${productType !='D'}">
						<li><spring:message code="pd.label.showcart.special.message.healthNotAvaliable"/></li>
					</c:when>
				  </c:choose>
				  <c:choose>
					<c:when test="${dentalPlanPresent =='Y'}">
					  <c:choose>
						<c:when test="${dentalPriceChange != ''}">
							<c:if test="${dentalPriceChange == 'decreased'}">
								<spring:message code="pd.label.showCart.decreased"  var="priceChange" htmlEscape="false"/>
							</c:if>
							<c:if test="${dentalPriceChange == 'increased'}">
								<spring:message code="pd.label.showCart.increased"  var="priceChange" htmlEscape="false"/>
							</c:if>
						  <li><spring:message arguments="${priceChange},${oldDentalPremium},${dentalPremium}" code="pd.label.showcart.special.message.dentalPriceChange" htmlEscape="false"/></li>
						</c:when>
					  </c:choose>
					  <c:choose>
					  <c:when test="${showDentalSubscriberChanged eq true}"><li><spring:message code="pd.label.shopcart.message.yourCart.DentalSubscriberChange"/></li></c:when>
					  </c:choose>

					</c:when>
					<c:otherwise><c:if test="${oldDentalPlanId!=null}"><li><spring:message code="pd.label.showcart.special.message.dentalNotAvaliable"/></li></c:if></c:otherwise>
				  </c:choose>
				  <c:if test="${stateSubsidyChange != '' && productType != 'D'}">
                        <c:if test="${stateSubsidyChange == 'decreased'}">
                            <spring:message code="pd.label.showCart.decreased"  var="subsidyChange" htmlEscape="false"/>
                        </c:if>
                        <c:if test="${stateSubsidyChange == 'increased'}">
                            <spring:message code="pd.label.showCart.increased"  var="subsidyChange" htmlEscape="false"/>
                        </c:if>
                        <li><spring:message arguments="${subsidyChange},${oldStateSubsidy},${stateSubsidy}" code="pd.label.showcart.special.message.stateSubsidyChange" htmlEscape="false"/></li>
                  </c:if>
				  <c:if test="${taxCreditChange != '' && productType != 'D'}">
						<c:if test="${taxCreditChange == 'decreased'}">
						    <spring:message code="pd.label.showCart.decreased"  var="taxChange" htmlEscape="false"/>
						</c:if>
						<c:if test="${taxCreditChange == 'increased'}">
							<spring:message code="pd.label.showCart.increased"  var="taxChange" htmlEscape="false"/>
						</c:if>
			            <li><spring:message arguments="${taxChange},${oldTotalSubsidy},${maxAptc}" code="pd.label.showcart.special.message.taxCreditChange" htmlEscape="false"/></li>
				  </c:if>

				</ul>
			</div>
		  </c:if>
		</c:if>
	</div>

	<div class="row-fluid" id="show-cart">
		<div id="rightpanel" class=" margin10-b margin30-t">
		    <div class="margin10-b">
		      <input type="hidden" name="aptc" id="aptc" value="${aptc}"/>
		      <input type="hidden" name="maxAptc" id="maxAptc" value="${maxAptc}"/>
		      <input type="hidden" name="maxAppliedAptc" id="maxAppliedAptc" value="${maxAppliedAptc}"/>
		      <input type="hidden" name="maxUsedAptc" id="maxUsedAptc" value="${maxUsedAptc}"/>
		      <input type="hidden" name="totalPremiumBeforeCreditVal" id="totalPremiumBeforeCreditVal" value="0"/>
		      <input type="hidden" name="totalMemberCount" id="totalMemberCount" value="${totalMemberCount}"/>
		      <input type="hidden" name="orderId" id="orderId" value="${orderId}"/>
		 	  <input type="hidden" name="exchangeType" id="exchangeType" value="${exchangeType}"/>
		 	  <input type="hidden" name="healthAPTC" id="healthAPTC" value="${healthAPTC}"/>
		 	  <input type="hidden" name="dentalAPTC" id="dentalAPTC" value="${dentalAPTC}"/>
		 	  <input type="hidden" name="healthPremium" id="healthPremium" value="${healthPremium}"/>
		 	  <input type="hidden" name="dentalPremium" id="dentalPremium" value="${dentalPremium}"/>
		 	  <input type="hidden" name="KEEP_ONLY" id="KEEP_ONLY" value="${KEEP_ONLY}">
		 	  <input type="hidden" name="enrollmentType" id="enrollmentType" value="${enrollmentType}"/>
		 	  <input type="hidden" name="lightBoxOnOff" id="lightBoxOnOff" value="${lightBoxOnOff}"/>
			  <input type="hidden" name="renewalType" id="renewalType" value="${renewalType}"/>
		 	  <input type="hidden" name="healthPlanPresent" id="healthPlanPresent" value="${healthPlanPresent}"/>
		 	  <input type="hidden" name="dentalPlanPresent" id="dentalPlanPresent" value="${dentalPlanPresent}"/>

				<table class="table table-condensed carttable margin0 cart">
				  	<c:if test="${productType != 'D'}">
					 <!-- <h4 class="margin0"><spring:message code="pd.label.commontext.health"/></h4> -->
					 <c:choose>
						 <c:when test="${healthPlanPresent=='N' && dentalPlanPresent=='Y'}">
						  <div class="text-center gutter10 border-dash" align=center>
						    <div class="gutter10">
						      <c:choose>
								<c:when test="${enrollmentType == 'S'}">
								  <a href="<c:url value='/private/setSpecialEnrollmentType?insuranceType=HEALTH'/>" id="aid_shopForHealth_01" data-automation-id="shopForHealth" title="<spring:message code='pd.label.ShopForHealth'/>" class="btn btn-primary"><spring:message code="pd.label.ShopForHealth"/></a>
								</c:when>
								<c:otherwise>
								  <a href="<c:url value='/private/planselection?insuranceType=HEALTH'/>" id="aid_shopForHealth_02" data-automation-id="shopForHealth" title="<spring:message code='pd.label.ShopForHealth'/>" class="btn btn-primary"><spring:message code="pd.label.ShopForHealth"/></a>
								</c:otherwise>
							  </c:choose>
				            </div>
				          </div>
						 </c:when>
						 <c:otherwise>
					  		<tbody class="health-cart">
					  			<tr class="caption">
									<th colspan="5">
										<span style="padding-right: 10px;"><spring:message code="pd.label.title.cart.healthPlan"/></span>
										<c:forEach var="person" items="${personList}" varStatus="loop">
											<c:if test="${fn:length(person.firstName) > 0}" >
				            					<span class="person" style="text-transform: capitalize;">${fn:toLowerCase(person.firstName)}</span>
				                				<c:if test="${!loop.last}">,</c:if>
				                			</c:if>
				            			</c:forEach>
										<c:if test="${enrollmentType != 'S'}">
											<a href="#" class="gtm_removecart" id="remove_${cartItemHealth.id}" onclick="removeCartItemById('${cartItemHealth.orderItemId}','H')">
												<span><spring:message code="pd.label.title.cart.remove"/></span>
											</a>
										</c:if>
									</th>
								</tr>
								<tr scope="row" class="plan__details">
								  <td class="txt-center">
								    <div class="planimg">
									    <a target="_blank" id="aid_planImg" data-automation-id="planImg" href="planinfo/${fn:join(fn:split(coverageDate, '/'),'')}/${cartItemHealth.id}">
									        <img class="carrierlogo hide" src='${cartItemHealth.issuerLogo}' onload="resizeimage(this)" alt="${cartItemHealth.issuer} ${cartItemHealth.name}"/>
										</a>
									</div>
								  </td>
								  <td style="padding-right: 10px;"></td>
								  <td class="span4 txt-left vertical-align-top">
								  	<p data-automation-id="planPrice" id="aid_planPrice"><spring:message code="pd.label.title.cart.monthlyPremium"/></p>
								  	<c:if test="${(maxAptc != ''  && maxAptc > 0) || (maxAptc == 0 && exchangeCode=='STATE' && stateCode=='CA')}">
									<p data-automation-id="planAPTC" id="aid_planAPTC"><spring:message code="pd.label.title.cart.monthlyTaxCrAPTC"/></p>
									</c:if>
									<c:if test="${stateSubsidy != ''  && stateSubsidy >= 0 && stateCode=='CA'}">
										<p data-automation-id="planAPTC" id="aid_planAPTC"><spring:message code="pd.label.title.cart.monthlyTaxCrStateSubsidy"/></p>
									</c:if>
								  </td>
								  <td class="span4 txt-center vertical-align-top">
									<c:choose>
										<c:when test="${stateCode == 'MN'}">
											<p><a id="health-details-btn"
												class="gtm_details" href="#" rel="tooltip" data-html="true" data-placement="bottom" 
												>
												<spring:message code="pd.label.title.cart.details"/>
											</a></p>
										</c:when>
										<c:otherwise>
											<p style="padding: 10px;"></p>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${maxAptc > 0 && flowType == 'PLANSELECTION'}">
											<p><a title="<spring:message code='pd.label.title.cart.adjust'/>" id="adjust-btn"
												class="gtm_adjust" href="#myModal" role="button" data-toggle="modal"
												onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.title.cart.adjust"/> Button Click', 'eventLabel': '<spring:message code="pd.label.title.cart.adjust"/>'});">
												<spring:message code="pd.label.title.cart.adjust"/>
											</a></p>
										</c:when>
										<c:otherwise>
											<p style="padding: 10px;"></p>
										</c:otherwise>
									</c:choose>
								  </td>
								  <td class="span4 txt-right vertical-align-top">
							    	<p data-automation-id="planPremium" id="aid_planPremium">$<span class="numberToFormat">${cartItemHealth.premiumBeforeCredit}</span></p>
									<c:if test="${(maxAptc != '' && maxAptc > 0) || (maxAptc == 0 && exchangeCode=='STATE' && stateCode=='CA')}">
									<p>-$<span class="numberToFormat">${cartItemHealth.aptc}</span></p>
									</c:if>
									<c:if test="${stateSubsidy != ''  && stateSubsidy >= 0 && stateCode=='CA'}">
										<p>-$<span class="numberToFormat">${cartItemHealth.stateSubsidy}</span></p>
									</c:if>
								  </td>
								</tr>
								<tr scope="row">
									<td class="span4 txt-center">
						  			  <a target="_blank" href="planinfo/${fn:join(fn:split(coverageDate, '/'),'')}/${cartItemHealth.id}">${cartItemHealth.issuer}<br>${cartItemHealth.name}</a>
									</td>
									<td style="padding-right: 10px;"></td>
                                    <td colspan="3" class="txt-left">
                                        <p class="cart-spacer"></p>
                                    </td>
								</tr>
								<tr scope="row" class="individual-subtotal">
									<td class="span4 coverage-start-date txt-center">
										<c:choose>
											<c:when test="${enrollmentType == 'S'}" >
												<spring:message code="pd.label.title.cart.changeEffectiveDate"/>:
											</c:when>
											<c:otherwise>
												<spring:message code="pd.label.title.cart.coverageStartDate"/>:
											</c:otherwise>
										</c:choose>
										<c:choose>
											<c:when test="${(enrollmentType == 'S' && isHealthPlanChanged == 'N' && showHealthSubscriberChanged eq false)}" >
												<c:choose>
													<c:when test="${exchangeCode=='STATE' && stateCode=='ID' }">
													<c:choose>
														<c:when test="${healthPlanPresent =='Y' && healthPriceChange == '' && dentalPriceChange == '' }">
					                						${financialEffectiveDate}
					                					</c:when>
					                					<c:otherwise>
															${coverageDate}
														</c:otherwise>
														</c:choose>
					                				</c:when>
													<c:otherwise>
														${financialEffectiveDate}
													</c:otherwise>
												</c:choose>
				                			</c:when>
											<c:otherwise>
												${coverageDate}
											</c:otherwise>
										</c:choose>
									</td>
									<td style="padding-right: 10px;"></td>
									<td colspan="2" class="span4 txt-left vertical-align-top">
										<span class="gutter0-r"><spring:message code="pd.label.title.cart.healthMonthlyPaymt" htmlEscape="false"/></span>
									</td>
									<td class="span4 txt-right vertical-align-top">
										<span class="gutter10-l"><spring:message code='pd.label.dollarSign'/></span><span class="numberToFormat">${cartItemHealth.premiumAfterCredit}</span>
									</td>
								</tr>
								<tr scope="row">
									<td colspan="5" class="txt-left">
										<p class="cart-spacer"></p>
									</td>
								</tr>
							</tbody><!-- end of health cart-->
						  </c:otherwise>
					 </c:choose>
					</c:if> <!-- End test="${productType != 'D'}" -->

					<c:if test="${isSeekingCoverage != false && productType != 'H' }">
					    	<!-- <h3 class="margin0"><spring:message code="pd.label.commontext.dental"/></h3> -->
					    	<c:choose>
					    		<c:when test="${dentalPlanPresent == 'Y'}">

				        			<tbody  class="dental-cart">
				        				<tr class="caption">
											<th colspan="5">
												<span style="padding-right: 10px;"><spring:message code="pd.label.title.cart.dentalPlan"/></span>
												<c:forEach var="person" items="${dentalPersonList}" varStatus="loop">
				          							<c:if test="${fn:length(person.firstName) > 0}" >
				              							<span class="person" style="text-transform: capitalize;">${fn:toLowerCase(person.firstName)}</span>
				         	  							<c:if test="${!loop.last}">,</c:if>
				         	  						</c:if>
				            					</c:forEach>
												<c:if test="${insuranceType == 'DENTAL' && customGroupingOnOff != 'OFF' && (loggedInUser != null && flowType == 'PLANSELECTION')}">
													<a href="#" class="changeLink active" data-placement="bottom" data-html="false">
														<span><spring:message code="pd.label.title.cart.changeEnrollees"/></span>
													</a>
												</c:if>
												<c:if test="${enrollmentType != 'S'}">
													<a href="#" class="gtm_removecart" id="remove_${cartItemDental.id}" onclick="removeCartItemById('${cartItemDental.orderItemId}','D')">
														<span><spring:message code="pd.label.title.cart.remove"/></span>
													</a>
												</c:if>
											</th>
										</tr>
										<tr scope="row">
											<td class="txt-center">
												<div class="planimg">
													<a target="_blank" href="planinfo/${fn:join(fn:split(coverageDate, '/'),'')}/${cartItemDental.id}">
														<img class="carrierlogo hide" src='${cartItemDental.issuerLogo}' onload="resizeimage(this)" alt='${cartItemDental.issuer} - ${cartItemDental.name}' />
													</a>
												</div>
											</td>
											<td style="padding-right: 10px;"></td>
											<td class="span4 txt-left vertical-align-top">
												<p><spring:message code="pd.label.title.cart.monthlyPremium"/></p>
											  	<c:if test="${maxAptc != '' && maxAptc > 0 && stateCode!='CA'}">
												<p><spring:message code="pd.label.title.cart.monthlyTaxCrAPTC"/></p>
												</c:if>
											</td>
											<td class="span4 txt-center vertical-align-top">
												<c:choose>
													<c:when test="${stateCode == 'MN'}">
														<a id="dental-details-btn"
															class="gtm_details" href="#" rel="tooltip" data-html="true" data-placement="bottom" 
															>
															<spring:message code="pd.label.title.cart.details"/>
														</a>
													</c:when>
													<c:otherwise>
														<p style="padding-right: 10px;"></p>
													</c:otherwise>
												</c:choose>
												<p style="padding-right: 10px;"></p>
											  </td>
											  <td class="span4 txt-right vertical-align-top">
										    	<p>$<span class="numberToFormat">${cartItemDental.premiumBeforeCredit}</span></p>
										    	<c:if test="${maxAptc != '' && maxAptc > 0 && stateCode!='CA'}">
												<p>-$<span class="numberToFormat">${cartItemDental.aptc}</span></p>
												</c:if>
											</td>
										</tr>
										<tr scope="row">
											<td class="span4 txt-center">
												<a target="_blank" href="planinfo/${fn:join(fn:split(coverageDate, '/'),'')}/${cartItemDental.id}">${cartItemDental.issuer}<br>${cartItemDental.name}</a>
											</td>
											<td style="padding-right: 10px;"></td>
											<td colspan="3" class="txt-left">
												<p class="cart-spacer"></p>
											</td>
										</tr>
											<tr scope="row" class="individual-subtotal">
												<td class="span4 coverage-start-date txt-center">
													<c:choose>
														<c:when test="${enrollmentType == 'S'}" >
															<spring:message code="pd.label.title.cart.changeEffectiveDate"/>:
														</c:when>
														<c:otherwise>
															<spring:message code="pd.label.title.cart.coverageStartDate"/>:
														</c:otherwise>
													</c:choose>
													<c:choose>
														<c:when test="${(enrollmentType == 'S' && isDentalPlanChanged == 'N' && showDentalSubscriberChanged eq false)}" >
															<c:choose>
																<c:when test="${exchangeCode=='STATE' && stateCode=='ID' }">
																	<c:choose>
																		<c:when test="${dentalPlanPresent =='Y' && dentalPriceChange == '' && healthPriceChange == '' }">
																			${financialEffectiveDate}
																		</c:when>
																		<c:otherwise>
																			${coverageDate}
																		</c:otherwise>
																	</c:choose>
																</c:when>
																<c:otherwise>
																	${financialEffectiveDate}
																</c:otherwise>
															</c:choose>
														</c:when>
														<c:otherwise>
															${coverageDate}
														</c:otherwise>
													</c:choose>
												</td>
												<td style="padding-right: 10px;"></td>
												<td colspan="2" class="span4 txt-left vertical-align-top">
													<span class="gutter0-r">
														<spring:message code="pd.label.title.cart.dentalMonthlyPaymt" htmlEscape="false"/>
													</span>
												</td>
												<td class="span4 txt-right vertical-align-top">
													<spring:message code='pd.label.dollarSign'/><span class="numberToFormat">${cartItemDental.premiumAfterCredit}</span>
												</td>
											</tr>
										<tr scope="row">
											<td colspan="5" class="txt-left">
												<p class="cart-spacer"></p>
											</td>
										</tr>
									</tbody><!-- end of dental cart-->

				        		</c:when>
					    		<c:otherwise>
									<div id="removemessage" class="removemsg txt-center gutter10 border-dash" >
										<div class="gutter10">
										  <c:choose>
										  	<c:when test="${enrollmentType == 'S'}">
										  		<%-- <a href="<c:url value='/private/setSpecialEnrollmentType?insuranceType=DENTAL'/>" class="btn btn-primary" title="<spring:message code='pd.button.showcart.shopForDental'/>"><spring:message code="pd.button.showcart.shopForDental"/></a> --%>
                                                <button id="aid-shopDentalBtn1" data-automation-id="shopDentalBtn" title="<spring:message code='pd.button.showcart.shopForDental'/>" class="btn btn-primary btn_res gtm_shopforDental" type="button"><spring:message code="pd.button.showcart.shopForDental"/></button>
                                            </c:when>
										  	<c:otherwise>
										  		<%-- <a href="<c:url value='/private/planselection?insuranceType=DENTAL'/>" class="btn btn-primary" title="<spring:message code='pd.button.showcart.shopForDental'/>"><spring:message code="pd.button.showcart.shopForDental"/></a> --%>
                                                <button id="aid-shopDentalBtn2" data-automation-id="shopDentalBtn" title="<spring:message code='pd.button.showcart.shopForDental'/>" class="btn btn-primary btn_res gtm_shopforDental" type="button"><spring:message code="pd.button.showcart.shopForDental"/></button>
                                            </c:otherwise>
										  </c:choose>
										</div>
				  	    			</div>
				  	    		</c:otherwise>
				  	    	</c:choose>
				  	</c:if>
				  	<tbody class="" id="summary_table">
						<tr class="caption">
							<th colspan="5">
								<spring:message code="pd.label.title.cart.total"/>
							</th>
						</tr>
						<c:if test="${cartItemHealth.premiumAfterCredit != null && cartItemHealth.premiumAfterCredit > 0}">
					        <tr scope="row">
								<td colspan="2" style="padding-right: 10px;"></td>
								<td scope="col" colspan="2" class="span4 txt-left vertical-align-top">
									<spring:message code="pd.label.title.cart.total.health.monthlyPaymt" htmlEscape="false"/>
								</td>
								<td scope="col" class="span4 txt-right vertical-align-top">
									<spring:message code='pd.label.dollarSign'/><span class="numberToFormat">${cartItemHealth.premiumAfterCredit}</span>
								</td>
					        </tr>
				        </c:if>
				        <c:if test="${cartItemDental.premiumAfterCredit != null && cartItemDental.premiumAfterCredit > 0}">
					        <tr scope="row">
								<td colspan="2" style="padding-right: 10px;"></td>
								<td scope="col" colspan="2" class="span4 txt-left vertical-align-top">
									<spring:message code="pd.label.title.cart.total.dental.monthlyPaymt" htmlEscape="false"/>
								</td>
								<td scope="col" class="span4 txt-right vertical-align-top">
									<spring:message code='pd.label.dollarSign'/><span class="numberToFormat">${cartItemDental.premiumAfterCredit}</span>
								</td>
					        </tr>
				        </c:if>
						<tr scope="row">
							<td colspan="2" style="padding-right: 10px;"></td>
							<td colspan="3" class="txt-left">
								<p class="cart-spacer"></p>
							</td>
						</tr>
				        <tr scope="row">
							<td colspan="2" style="padding-right: 10px;"></td>
							<td scope="col" colspan="2" class="span4 txt-left vertical-align-top">
								<span id="emp-pays" data-toggle="popover" data-trigger="hover" data-content="<spring:message code="pd.label.tooltip.showcart.amountEmployerPays" htmlEscape="true"/>">
									<spring:message code="pd.label.title.cart.total.monthlyPaymt"/>
								</span>
							</td>
							<td scope="col" class="span4 txt-right vertical-align-top">
								<spring:message code='pd.label.dollarSign'/><span class="numberToFormat">${cartItemHealth.premiumAfterCredit + cartItemDental.premiumAfterCredit}</span>
							</td>
				        </tr>
						<tr scope="row">
							<td colspan="5" class="txt-left">
								<p class="cart-spacer"></p>
							</td>
						</tr>
					</tbody>
					<tbody>
						<tr>
						<td class="form-actions margin-top50 form-horizontal" colspan="5">
							<form name="showEsign" id="showEsign" action="<c:url value="/enrollment/showesignature" />" method="POST">
						      <df:csrfToken/>
						       <input type="hidden" id="csrfToken" value ='<df:csrfToken plainToken="true"/>'>
							  <input type="hidden" name="cart_id" id="cart_id" value="<encryptor:enc value="${pdHouseholdId}"/>">
						      <input type="hidden" name="enrollment_type" id="enrollment_type" value="FI">
						      <input type="hidden" name="coverage_date" id="coverage_date" value="${coverageDate}">
						      <input type="hidden" name="aptcVal" id="aptcVal" value="${not empty maxAptc ? maxUsedAptc : ''}">
						      <input type="hidden" name="stateSubsidyVal" id="stateSubsidyVal" value="${stateSubsidy}">
						      <input type="hidden" name="seekingCoverageDentalObj" id="seekingCoverageDentalObj" value='[${seekingCoverageDentalObj}]'/>
						      <input type="hidden" name="customGroupingOnOff" id="customGroupingOnOff" value="${customGroupingOnOff}" />
						      <input type="hidden" name="insuranceType" id="insuranceType" value="${insuranceType}" />
							  <c:if test="${cartItemHealth != null}">
						      	<input type="hidden" name="healthPlanIssuer" id="healthPlanIssuer" value="${cartItemHealth.issuer}" />
						      	<input type="hidden" name="healthPlanName" id="healthPlanName" value="${cartItemHealth.name}" />
						      	<input type="hidden" name="healthPlanLevel" id="healthPlanLevel" value="${cartItemHealth.level}" />
						      </c:if>

						      <c:if test="${cartItemDental != null}">
						      	<input type="hidden" name="dentalPlanIssuer" id="dentalPlanIssuer" value="${cartItemDental.issuer}" />
						      	<input type="hidden" name="dentalPlanName" id="dentalPlanName" value="${cartItemDental.name}" />
						      	<input type="hidden" name="dentalPlanLevel" id="dentalPlanLevel" value="${cartItemDental.level}" />
						      </c:if>
						  	</form>

						  	<c:choose>
								<c:when test="${loggedInUser != null}">
									<c:set var="enrollmentTypeVal" value="${enrollmentType=='I'?'Initial':(enrollmentType=='S'?'Special':(enrollmentType=='A'?'Renewal':''))}"> </c:set>
								</c:when>
								<c:otherwise>
									<c:set var="enrollmentTypeVal" value="Plan Preview"> </c:set>
								</c:otherwise>
							</c:choose>
						  	<c:choose>
						  	<c:when test="${KEEP_ONLY == null}">
						  			<c:choose>
						  				<c:when test="${productType == 'D'}">
						  					<%-- <a id="backtoshopping" href="/hix/private/planselection?insuranceType=DENTAL" onclick="triggerGoogleTrackingEvent(['_trackEvent', 'Continue Shopping', 'click', '${enrollmentTypeVal}', 1]);" class="btn pull-left" title="<spring:message code='pd.label.info.continue'/>"><spring:message code="pd.label.info.continue"/></a> --%>
                                            <button
                                                id="backtoshopping"
                                                class="btn pull-left js-backtoshoppingDental gtm_backtoshoppingDental"
                                                title="<spring:message code='pd.label.info.continue'/>"
                                                type="button"
                                                onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.info.continue"/> Button Click', 'eventLabel': '<spring:message code="pd.label.info.continue"/>'});">
                                                <spring:message code="pd.label.info.continue"/>
                                            </button>
                                        </c:when>
						  				<c:otherwise>
						  					<%-- <a href="/hix/private/planselection" onclick="triggerGoogleTrackingEvent(['_trackEvent', 'Continue Shopping', 'click', '${enrollmentTypeVal}', 1]);" class="btn pull-left" title="<spring:message code='pd.label.info.continue'/>"><spring:message code="pd.label.info.continue"/></a> --%>
                                            <button
                                                id="backtoshopping"
                                                class="btn btn_res pull-left js-backtoshopping gtm_backtoshopping"
                                                title="<spring:message code='pd.label.info.continue'/>"
                                                type="button"
                                                onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.info.continue"/> Button Click', 'eventLabel': '<spring:message code="pd.label.info.continue"/>'});">
                                                <spring:message code="pd.label.info.continue"/>
                                            </button>
						  				</c:otherwise>
						  			</c:choose>

						  	</c:when>
						    <c:when test="${KEEP_ONLY == 'N'}"><a href="#newplan" role="button"  data-toggle="modal" onclick="triggerGoogleTrackingEvent(['_trackEvent', '<spring:message code='pd.label.title.shopDifferentPlan'/>', 'click', '${enrollmentTypeVal}', 1]);" title="<spring:message code='pd.label.title.shopDifferentPlan'/>" class="btn pull-left"><spring:message code="pd.label.title.shopDifferentPlan"/></a></c:when>
						    </c:choose>

						    <c:choose>
						  		<c:when test="${flowType == 'PLANSELECTION'}">
						  			<%-- <a id="checkout" onclick="validateCart()" href="#" class="btn btn-primary pull-right" title="<spring:message code='pd.label.title.signApplication'/>" aria-label="<spring:message code="pd.label.title.signApplication"/>"><spring:message code="pd.label.title.signApplication"/></a> --%>
                                    <button
                                        id="checkout"
                                        class="btn btn-primary btn_res pull-right gtm_checkout"
                                        title="<spring:message code='pd.label.title.signApplication'/>"
                                        type="button"
                                        aria-label="<spring:message code="pd.label.title.signApplication"/>"
                                        onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.title.signApplication"/> Button Click', 'eventLabel': '<spring:message code="pd.label.title.signApplication"/>'});">
                                            <spring:message code="pd.label.title.signApplication"/>
                                    </button>
                                </c:when>
						  		<c:when test="${flowType == 'PREELIGIBILITY'}">
						  			<c:choose>
						  				<c:when test="${exchangeCode=='STATE' && stateCode=='CA'}">
						  					<c:choose>
							  				  <c:when test="${loggedInUser != null}">
                            <button id="aid-btnEnroll" class="btn btn-primary pull-right js-btnApply" title="<spring:message code='pd.label.button.enroll'/>" type="button"><spring:message code='pd.label.button.enroll'/></button>
							  				  </c:when>
							  				  <c:otherwise>
														<button
															id="aid-btnRegister"
															class="btn btn-primary btn_res pull-right js-btnApply gtm_apply"
															onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.button.register"/> Button Click', 'eventLabel': '<spring:message code="pd.label.button.register"/> - GI'});"
															title="<spring:message code="pd.label.button.register"/>" type="button">
															<spring:message code="pd.label.button.register"/>&nbsp;<i class="icon-caret-right"></i>
														</button>
							  				  </c:otherwise>
							  				</c:choose>
						  				</c:when>
						  				<c:when test="${loggedInUser != null}">
						  					<a href="<c:url value='/private/preShoppingComplete'/>" class="btn btn-primary btn_res pull-right gtm_apply" window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.button.finalizeEligibility"/> Button Click', 'eventLabel': '<spring:message code="pd.label.button.finalizeEligibility"/> - GI'});" title="<spring:message code='pd.label.button.finalizeEligibility'/>">  <spring:message code="pd.label.button.finalizeEligibility"/> <i class="icon-caret-right"></i></a>
											</c:when>
											<c:when test="${exchangeCode=='STATE' && stateCode=='MN'}">
												<a id="enrollNowBtn" href="#" onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.enrollNow"/> Button Click', 'eventLabel': '<spring:message code="pd.label.enrollNow"/> - GI'});" title="<spring:message code='pd.label.enrollNow'/>" class="btn btn-primary btn_res pull-right gtm_apply"> <spring:message code="pd.label.enrollNow"/></a>
											</c:when>
						  				<c:otherwise>
						  					<a href="<c:url value='/private/preShoppingComplete'/>" onclick="window.dataLayer.push({'event': 'shopandcompareEvent', 'eventCategory': 'Plan Selection - Shop & Compare', 'eventAction': '<spring:message code="pd.label.button.register"/> Button Click', 'eventLabel': '<spring:message code="pd.label.button.register"/> - GI'});" title="<spring:message code='pd.label.button.register'/>" class="btn btn-primary btn_res pull-right gtm_apply"> <spring:message code="pd.label.button.register"/></a>
						  				</c:otherwise>
						  			</c:choose>

						    	</c:when>
						    </c:choose>
		   					</td> <!-- ./form-action -->
		   				</tr>
						<tr scope="row">
							<td colspan="5" class="txt-left">
								<p class="cart-spacer"></p>
							</td>
						</tr>
	   				</tbody>
				</table> <!-- ./cart -->
		    </div><!-- ./margin10- -->
		</div><!-- #rightpanel- -->
	</div>
</c:if><!-- close condition for healthPlanPresent == 'Y' || dentalPlanPresent == 'Y' --><!--gutter10-->

	<!-- modal incomplete cart -->
	<div id="incompletecart" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="incompletecart" aria-hidden="true">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    <h3><spring:message code="pd.label.info.incompleteCart"/></h3>
	  </div>
	  <div class="modal-body">
	    <p><spring:message code="pd.label.para.cartNotComplete"/></p>
	    <p><spring:message code="pd.label.para.mustContinueShopping"/></p>
	    <p><a href="#"><spring:message code="pd.label.commontext.anchor"/></a></p>
	  </div>
	  <div class="modal-footer">
	    <button class="btn btn-primary" title="<spring:message code='pd.label.button.keepShopping'/>" data-dismiss="modal" aria-hidden="true" type="button"><spring:message code="pd.label.button.keepShopping"/></button>
	  </div>
	</div>
	<!-- /modal -->

<!-- modal incomplete cart -->
<c:if test="${KEEP_ONLY != 'Y'}">
<div id="newplan" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="newplan" aria-hidden="true">
  <div class="modal-body">
    <p><strong><spring:message code="pd.label.title.pleaseNote"/>: </strong> <spring:message code="pd.label.title.message32"/> ${coverageDate}.
    <spring:message code="pd.label.title.message33"/> </p>
  </div>
  <div class="modal-footer">
  <a href="setSpecEnrFlowType?shopForDifPlan=Y" title="<spring:message code='pd.label.title.message34'/>" class="btn pull-left"><spring:message code="pd.label.title.message34"/></a>
    <button class="btn btn-primary" title="<spring:message code='pd.label.title.returnToCart'/>" data-dismiss="modal" aria-hidden="true" type="button"><spring:message code="pd.label.title.returnToCart"/></button>
  </div>
</div></c:if>
<!-- /modal -->

<div id="no-health-plan-error" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="no-health-plan-error" aria-hidden="true">
	<div class="modal-header">
	   <!--  <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>  -->
	    <h3 id="nohealthplanerror"><spring:message code="pd.label.title.planSelectionIncomplete"/></h3>
	 </div>
	 <div class="modal-body">
	    <p><spring:message code="pd.label.title.message35"/></p>
	 </div>
	 <div class="modal-footer">
	   <button class="btn pull-left" title="<spring:message code='pd.label.Close'/>" data-dismiss="modal" type="button"><spring:message code="pd.label.Close"/></button>
	</div>
</div>

<div id="no-health-plan-dentalOnly-msg" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="no-health-plan-dentalOnly-msg" aria-hidden="true">
  <div class="modal-header">
    <!--   <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
		<h3 id="nohealthplanmsg"><spring:message code="pd.label.title.healthPlanSelected"/></h3>
  </div>
  <div class="modal-body">
    <p>
			<c:choose>
				<c:when test="${enrollmentType == 'I' || healthEnrollmentPresent == 'Y'}">
					<!-- TODO -->
					<spring:message code="pd.label.title.healthPlanSelected.message"/>
					<br></br><spring:message code="pd.label.title.healthPlanSelected.message2"/>
        </c:when>
        <c:otherwise>
          <spring:message code="pd.label.title.healthPlanSelected.message.spec"/>
          <br></br><spring:message code="pd.label.title.healthPlanSelected.message.spec1"/>
        </c:otherwise>
			</c:choose>
		</p>
		<c:choose>
    	<c:when test="${maxAptc>0}"><p><spring:message code="pd.label.title.healthPlanSelected.aptcMsg"/></p></c:when>
    </c:choose>
    </div>
    	<div class="modal-footer">
      	<button class="btn pull-left" data-dismiss="modal" type="button"><spring:message code="pd.button.showcart.goBack"/></button>

      	<c:choose>
       		<c:when test="${enrollmentType == 'I' || healthEnrollmentPresent == 'Y'}">
       		 <%-- <a href="#" class="enrollToDental btn btn-primary pull-right" title="<spring:message code='pd.label.button.enrollToDental'/>" id="checkout" data-dismiss="modal" onclick='$("#showEsign").submit();'>
       			<spring:message code="pd.label.button.enrollToDental"/>
       		</a> --%>
            <button id="checkout-modal" class="enrollToDental btn btn-primary pull-right js-enrollToDental" title="<spring:message code='pd.label.button.enrollToDental'/>" type="button"><spring:message code="pd.label.button.enrollToDental"/></button>
       		</c:when>
       		<c:otherwise>
       		 <%-- <a href="#" class="enrollToDental btn btn-primary pull-right" title="<spring:message code='pd.label.button.enrollToDental.spec'/>" id="checkout" data-dismiss="modal" onclick='$("#showEsign").submit();'>
       			<spring:message code="pd.label.button.enrollToDental.spec"/>
       		</a> --%>
            <button id="checkout-modal" class="enrollToDental btn btn-primary pull-right js-enrollToDental" title="<spring:message code='pd.label.button.enrollToDental.spec'/>" ><spring:message code="pd.label.button.enrollToDental.spec"/></button>
       		</c:otherwise>
       	</c:choose>
    </div>
</div>

<!-- aptc slider -->
<c:set var="enrollmentYear" value="${fn:substring(coverageDate, 6, 10)}"/>
<div class="modal hide fade showcart showcart-modal" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
 <div class="modal-dialog">
 	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" id="closeAdjustPopup" class="close" data-dismiss="modal" aria-hidden="true" onClick="closeAptc();"><spring:message code="pd.label.showcart.times"/></button>
	  	<h3 id="myModalLabel"><spring:message code="pd.label.info.taxCredit"/></h3>
	  </div>

	  <div class="modal-body">
	    <div id="myCarousel" class="carousel showCartCarousel">
	    <!-- Carousel items -->
	      <div class="carousel-inner">
	<!-- item 1 -->
	        <div class="active item">
	          <h4>
	      <spring:message code="pd.label.showcart.head1" />
	      </h4>

	          <p><spring:message code="pd.label.showcart.para1"/></p>

	          <table class="table table-border-none">
	          <thead>
	              <tr>
	                <th scope="col"><spring:message code="pd.label.info.monthlyAdvance"/></th>
	                <th scope="col"><spring:message code="pd.label.info.annualCredit"/></th>
	              </tr>
	            </thead>
	            <tbody>
	              <tr>
	                <td scope="row">
	                  <p><spring:message code="pd.label.showcart.para4"/></p>
	                </td>
	                <td scope="row">
	                  <p><spring:message code="pd.label.showcart.para7"/></p>
	                </td>
	              </tr>

                <tr>
	                <td scope="row">

	                  <p><strong><spring:message code="pd.label.commontext.pros"/>: </strong><spring:message code="pd.label.showcart.para5"/></p>

	                </td>
	                <td scope="row">

	                  <p><strong><spring:message code="pd.label.commontext.pros"/>: </strong><spring:message code="pd.label.showcart.para8"/></p>

	                </td>
	              </tr>

	                <tr>
	                <td scope="row">

	                  <p><strong><spring:message code="pd.label.commontext.cons"/>: </strong><spring:message code="pd.label.showcart.para6"/></p>
	                </td>
	                <td scope="row">

	                  <p><strong><spring:message code="pd.label.commontext.cons"/>: </strong><spring:message code="pd.label.showcart.para9"/></p>
	                </td>
	              </tr>

	            </tbody>
						</table>
						<c:if test="${stateCode!='MN'}">
							<p class="showcart-note"><strong><spring:message code="pd.label.showcart.para11"/></strong></p>
						</c:if>
	        </div>
	<!-- item 1 ends -->

	<!-- item 2 -->
	        <div class="item">
	      <h4><spring:message code="pd.label.showcart.item2.head"/></h4>
	      <!-- SLIDER -->
	          <div class="well well-small">
	            <input type="hidden" name="newtax" id="newtax" value="${aptc}">
	            <div class="row-fluid">
	              <div class="span12">
	                <div class="span3">
	                  <label for="montax"><spring:message code="pd.label.info.monthlyTaxCredit" htmlEscape="false"/> <br />
	                  <span class="val"> <spring:message code='pd.label.dollarSign'/> <input type="text" id="montax" class="input-mini txt-center margin0" value="${maxUsedAptc}" readonly/></span></label>
	                </div>
	                <div class="span5 margin0" aria-hidden="true">
	                  <div id="slider-range-max" style="width:auto; height:28px;margin:20px 0"></div>
	                </div>
	                <div class="span3 pull-right">
	                  <label for="anntax" class="margin0"><spring:message code="pd.label.info.annualTaxCredit" htmlEscape="false"/> <br />
	                  <span class="val"><spring:message code='pd.label.dollarSign'/> <input type="text" id="anntax" class="input-mini txt-center margin0" value="${(maxAppliedAptc-maxUsedAptc)}" readonly/></span></label>
	                </div>
	              </div>
	            </div>
	          </div>

	          <c:if test="${stateCode!='CA'}">
				<c:choose>
					<c:when test="${healthPlanPresent == 'Y'}">
						<spring:message code="pd.label.title.effectiveStartDate" /> :
						<c:choose>
							<c:when test="${(enrollmentType == 'S' && isHealthPlanChanged == 'N' && showHealthSubscriberChanged eq false)}">
								<c:choose>
									<c:when test="${exchangeCode=='STATE' && stateCode=='ID' }">
									<c:choose>
										<c:when test="${healthPriceChange == '' && dentalPriceChange == '' }">
											${financialEffectiveDate}
										</c:when>
										<c:otherwise>
											${coverageDate}
										</c:otherwise>
										</c:choose>
									</c:when>
									<c:otherwise>
										${financialEffectiveDate}
									</c:otherwise>
								</c:choose>
	        				</c:when>
							<c:otherwise>
								${coverageDate}
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<spring:message code="pd.label.title.effectiveStartDate" /> :
						<c:choose>
							<c:when test="${dentalPlanPresent == 'Y'}">
								<c:choose>
									<c:when test="${(enrollmentType == 'S' && isDentalPlanChanged == 'N' && showDentalSubscriberChanged eq false)}">
										<c:choose>
											<c:when test="${exchangeCode=='STATE' && stateCode=='ID' }">
												<c:choose>
													<c:when test="${dentalPriceChange == '' && healthPriceChange == '' }">
														${financialEffectiveDate}
													</c:when>
													<c:otherwise>
														${coverageDate}
													</c:otherwise>
												</c:choose>
											</c:when>
											<c:otherwise>
												${financialEffectiveDate}
											</c:otherwise>
										</c:choose>
			        				</c:when>
									<c:otherwise>
										${coverageDate}
								</c:otherwise>
								</c:choose>
							</c:when>
						</c:choose>
					</c:otherwise>
				</c:choose>
			</c:if>
	         <hr>
	          <div class="alert alert-info hide">
	            <label for="netpremium" class="margin0">
	              <strong class="gutter10-lr"><spring:message code="pd.label.info.netMonthly"/></strong>
	              <input type="text" id="netpremium" class="input-mini margin0" style="color:#f6931f; font-weight:bold;"/>
	            </label>
	          </div>
	          <p><strong><spring:message code="pd.label.showcart.item2.para1"/></strong></p>
	          <c:if test="${stateCode!='CA'}">
		          <ul>
		            <li><spring:message code="pd.label.showcart.item2.para2"/></li>
		            <li><spring:message arguments="${fn:substring(coverageDate, 6, 10)}" code="pd.label.showcart.item2.para3" htmlEscape="false"/></li>
		            <li><spring:message arguments="${fn:substring(coverageDate, 6, 10)}" code="pd.label.showcart.item2.para4"/></li>
		          </ul>
	          </c:if>
	      </div>
	<!-- item 2 ends -->
	      </div>
	    </div>
	  </div>

	  <div class="modal-footer">
	    <div id="carousel-nav" class="pull-left">
	      <a id="slide1" href="#myCarousel" data-to="1" title="<spring:message code='pd.label.showcart.button.1'/>" class="active" onclick="textChange($(this))"><spring:message code="pd.label.showcart.button.1"/></a>
	      <a id="slide2" href="#myCarousel" data-to="2" title="<spring:message code='pd.label.showcart.button.2'/>" onclick="textChange($(this))"><spring:message code="pd.label.showcart.button.2"/></a>
	    </div>
	    <div class="pull-right">
	      <a href="#" title="<spring:message code='pd.label.Close'/>" data-dismiss="modal" id="modalClose" aria-hidden="true" class="btn" onClick="closeAptc();"><spring:message code="pd.label.Close"/></a>
				<a href="#" title="<spring:message code='pd.label.confirm'/>" class="btn btn-primary" id="textSwapConfirm" onClick="textChange($(this))"><spring:message code="pd.label.confirm"/></a>
				<a href="#" title="<spring:message code='pd.label.adjustTaxCredit'/>" class="btn btn-primary" id="textSwapAdjust" onClick="textChange($(this))"><spring:message code="pd.label.adjustTaxCredit"/></a>
	    </div>
	  </div>
	 </div><!-- end of modal content -->
	</div><!-- end of modal dialog -->
</div><!-- modal -->

<!-- Enroll Now modal -->
<div id="enrollNow" class="modal bigger-modal hide fade" tabindex="-1" role="dialog" aria-labelledby="enrollNowHeader" aria-hidden="true">
  <div class="modal-header">
    <button class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="enrollNowHeader"><spring:message code="pd.label.enrollNow.modal.header"/></h3>
  </div>
  <div class="modal-body">
    <h4><spring:message code="pd.label.enrollNow.modal.content1"/></h4>
    <p class="margin10-l">
			1.<spring:message code="pd.label.enrollNow.modal.content1.1"/><br>
			2.
			<spring:message code="pd.label.enrollNow.modal.content1.2"/>
    </p>

    <h4 class="margin30-t"><spring:message code="pd.label.enrollNow.modal.content2"/></h4>

    <div class="cta-container">
			<div class="cta-container__cta">
				<p><spring:message code="pd.label.enrollNow.modal.signIn.content"/></p>
				<a class="btn btn-primary" href="https://auth.mnsure.org/login/Login.jsp" target="_blank"><spring:message code="pd.label.enrollNow.modal.signIn"/></a>
    	</div>
			<div class="cta-container__cta">
				<p><spring:message code="pd.label.enrollNow.modal.createAccount.content"/></p>
				<a class="btn btn-primary" href="https://www.mnsure.org/acct-access/create-account-check.jsp" target="_blank">
				<spring:message code="pd.label.enrollNow.modal.createAccount"/></a>
			</div>
			<div class="cta-container__cta">
				<p><spring:message code="pd.label.enrollNow.modal.searchHelp.content"/></p>
				<a class="btn btn-primary" href="https://www.mnsure.org/help/index.jsp" target="_blank"><spring:message code="pd.label.enrollNow.modal.searchHelp"/></a>
			</div>
		</div>
	</div>
</div>
<!-- Enroll Now modal end -->

<div id="catch-all-light-box" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="catchAllLightBox" aria-hidden="true">
	<div class="modal-body">
	<h3 id="catchAllLightBox"><spring:message code="pd.label.showcart.lightBox.header"/></h3>
	<ul style="list-style-type:disc">
		<c:if test="${(enrollmentType == 'I' && healthPlanPresent == 'Y') || (stateCode == 'MN' && enrollmentType == 'I') || (enrollmentType == 'S' && isHealthPlanChanged == 'Y')}">
			<li>
				<!-- TODO like this -->
				<spring:message code="pd.label.showcart.lightBox.text1"/>
			</li>
			<li>
				<spring:message code="pd.label.showcart.lightBox.text3"/>
			</li>
		</c:if>
		<c:if test="${((enrollmentType == 'I' && dentalPlanPresent == 'Y') || (enrollmentType == 'S' && isDentalPlanChanged == 'Y')) && stateCode != 'MN'}">
			<li>
				<spring:message code="pd.label.showcart.lightBox.text5"/>
			</li>
		</c:if>
		<c:if test="${(healthPlanPresent == 'Y' && cartItemHealth.hsa != null && fn:toLowerCase(cartItemHealth.hsa) == 'yes') && stateCode == 'ID'}">
			<li>
				<spring:message code="pd.label.showcart.lightBox.text7"/>
			</li>
		</c:if>
	</ul>
	<p><strong><spring:message code="pd.label.showcart.lightBox.text6"/></strong></p>
	</div>
	<div class="modal-footer">
	<button class="btn pull-left btn_res" title="<spring:message code='pd.button.showcart.goBack'/>" data-dismiss="modal"><spring:message code="pd.button.showcart.goBack"/></button>
    <button class="btn btn-primary btn_res pull-right js-enrollToDental title="<spring:message code='pd.button.showcart.readyToEnroll'/>" data-dismiss="modal"><spring:message code="pd.button.showcart.readyToEnroll"/></button>
    <%-- <a href="#" title="<spring:message code='pd.button.showcart.readyToEnroll'/>" onclick="$('#showEsign').submit();" class="btn btn-primary pull-right" id="checkout" data-dismiss="modal"><spring:message code="pd.button.showcart.readyToEnroll"/></a> --%>
	</div>
</div>

<script type="text/javascript" src="<c:url value='/resources/js/underscore-min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/json2.js' />"></script>


<script type="text/javascript">
		$("#textSwapConfirm").hide();

    $('.numberToFormat').each(function(index,item){
        $(item).html(parseFloat($(item).html()).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,"));
    });
    $('body').tooltip({
        selector: '[rel=tooltip]'
    });

    $.ajaxSetup ({
        // Disable caching of AJAX responses
        cache: false
    });

    // Script to keep Prevent BODY from scrolling when a modal is opened
    $("#myModal").on("show", function () {
      $("body").addClass("modal-open");
    }).on("hidden", function () {
      $("body").removeClass("modal-open")
    });

    $('#aid-shopDentalBtn1').click(function(){
        window.dataLayer.push({
            'event': 'shopandcompareEvent',
            'eventCategory': 'Plan Selection - Shop & Compare',
            'eventAction': '<spring:message code='pd.button.showcart.shopForDental'/> Button Click',
            'eventLabel': '<spring:message code='pd.button.showcart.shopForDental'/>"'});
    	window.location = "/hix/private/setSpecialEnrollmentType?insuranceType=DENTAL";
    });
    $('#aid-shopDentalBtn2').click(function(){
        window.dataLayer.push({
            'event': 'shopandcompareEvent',
            'eventCategory': 'Plan Selection - Shop & Compare',
            'eventAction': '<spring:message code='pd.button.showcart.shopForDental'/> Button Click',
            'eventLabel': '<spring:message code='pd.button.showcart.shopForDental'/>"'});
    	window.location = "/hix/private/planselection?insuranceType=DENTAL";
    });
    $('.js-backtoshoppingDental').click(function(){
    	window.location = "/hix/private/planselection?insuranceType=DENTAL";
    })
    $('.js-backtoshopping').click(function(){
    	window.location = "/hix/private/planselection";
    });
    $("#checkout").click(function() {
        validateCart();
    });
    $(".js-btnApply").click(function() {
        //triggerGoogleTrackingEvent(['_trackEvent', 'Register', 'click', 'Plan Preview', 1]);
        window.parent.open('${anonymousApplyURL}', '_self');
    });
    $(".js-enrollToDental").click(function() {
        $("#showEsign").submit();
    });

function validateCart() {

	var healthPlanPresent = $('#healthPlanPresent').val();
	var dentalPlanPresent = $('#dentalPlanPresent').val();
	var stateCode = $('#stateCode').val();
    var healthPlanInfo = '${cartItemHealth.issuer} | ${cartItemHealth.name} | ${cartItemHealth.level}';
    var dentalPlanInfo = '${cartItemDental.issuer} | ${cartItemDental.name} | ${cartItemDental.level}';

    var enrollmentType = $('#enrollmentType').val();
    var lightBoxOnOff = $('#lightBoxOnOff').val();
    var renewalType = $('#renewalType').val();
    var selectDentalOnly = '${selectDentalOnly}';
    var productType = '${productType}';

    var enrollType = enrollmentType=='I'?'Initial':(enrollmentType=='S'?'Special':(enrollmentType=='A'?'Renewal':''));

	if(healthPlanPresent == 'N' && dentalPlanPresent == 'N'){
		alert('<spring:message code="pd.label.alert.noItemsInCart" javaScriptEscape="true"/>');
		return false;
	}

	if(stateCode != 'MN' && ${productType != 'D'}){
	    if(selectDentalOnly == 'ON' && healthPlanPresent == 'N' && dentalPlanPresent == 'Y'){
	        $('#no-health-plan-dentalOnly-msg').modal('show');
	        return false;
		}

		if(healthPlanPresent == 'N' && dentalPlanPresent == 'Y'){
			$('#no-health-plan-error').modal('show');
			return false;
		}
	}

	if(lightBoxOnOff == 'ON' && (enrollmentType == 'I' || (enrollmentType == 'S' && renewalType == 'NEW') || enrollmentType == 'A'))
	{
		$('.nmhide').hide();
		$('#catch-all-light-box').modal('show');
		return false;
	}

	// Checkout Step 2
    if(healthPlanPresent == 'Y' && dentalPlanPresent == 'Y'){
        window.dataLayer.push({
            'event': 'checkout',
            'eventCategory': 'Plan Selection - ecommerce',
            'eventAction': '<spring:message code="pd.label.title.signApplication"/> Button Click',
            'eventLabel': healthPlanInfo + ' :: ' + dentalPlanInfo,
            'ecommerce': {
                'checkout': {
                    'actionField': {'step': 2}
                }
            }
        });
    }
    if(healthPlanPresent == 'Y' && dentalPlanPresent == 'N'){
        window.dataLayer.push({
            'event': 'checkout',
            'eventCategory': 'Plan Selection - ecommerce',
            'eventAction': '<spring:message code="pd.label.title.signApplication"/> Button Click',
            'eventLabel': healthPlanInfo,
            'ecommerce': {
                'checkout': {
                    'actionField': {'step': 2}
                }
            }
        });
    }
    if(healthPlanPresent == 'N' && dentalPlanPresent == 'Y'){
        window.dataLayer.push({
            'event': 'checkout',
            'eventCategory': 'Plan Selection - ecommerce',
            'eventAction': '<spring:message code="pd.label.title.signApplication"/> Button Click',
            'eventLabel': dentalPlanInfo,
            'ecommerce': {
                'checkout': {
                    'actionField': {'step': 2}
                }
            }
        });
    }

    $("#showEsign").submit();
}




function removeCartItemById(itemId, planType) {
	$.ajax({
		  type: "POST",
		  url: '/hix/private/individualCartItems/'+itemId,
		  data: {"csrftoken": $('#csrfToken').val()},
		  dataType: "text",

		  success: function(response) {

		    if (response!=null && response!="FAILED") {
				
				var responseObj = JSON.parse(response);
				
				if(responseObj.result == 'SUCCESS'){
					location.href="showCart";

					var planInfo = '';
					var productInfo = [];

					if (planType == 'H') {
						planInfo = '${cartItemHealth.issuer} | ${cartItemHealth.name} | ${cartItemHealth.level}';
						productInfo = [{
					        'id': '${cartItemHealth.issuerPlanNumber}',
					        'name': '${cartItemHealth.name}',
					        'brand': '${cartItemHealth.issuer}',
					        'category': '${cartItemHealth.level}',
					        'variant': '${cartItemHealth.networkType}',
					        'price': '${cartItemHealth.premiumBeforeCredit}'
					    }];
					}

					else if (planType == 'D') {
						planInfo = '${cartItemDental.issuer} | ${cartItemDental.name} | ${cartItemDental.level}';
					    productInfo = [{
					        'id': '${cartItemDental.issuerPlanNumber}',
					        'name': '${cartItemDental.name}',
					        'brand': '${cartItemDental.issuer}',
					        'category': '${cartItemDental.level}',
					        'variant': '${cartItemDental.networkType}',
					        'price': '${cartItemDental.premiumBeforeCredit}'
					    }];
					}

					window.dataLayer.push({
						'event': 'removeFromCart', 
						'eventCategory': 'Plan Selection - Shop & Compare', 
						'eventAction': 'Remove from Cart', 
						'eventLabel': planInfo + ' - ' + document.title,
						'ecommerce': {
				            'remove': {
				                'products': productInfo
				            }
				        }
					});
					
				}
		    }

		    else {
			    $('#shopping-modal-error').show();
				$('#shopping-modal-error').html('<spring:message code="pd.label.showcart.err.errorWhileProcessingDate" javaScriptEscape="true"/>');
			}

		  }
	});
}

function closeAptc() {
	var oldtax = $("#maxUsedAptc").val(); //Value before Adjusting
	var newtax = parseFloat($("#montax").val()); //Adjusted value
	if(oldtax != newtax) {
  		if(confirm('<spring:message code="pd.label.alert.youHaveMadeChanges" javaScriptEscape="true"/>')){
    		saveAptc();
    	} else {
    		$("#montax").val($("#maxUsedAptc").val());
    		var anntax = $("#maxAppliedAptc").val() - $("#maxUsedAptc").val();
            $("#anntax").val(anntax.toFixed(2));
      		$("#slider-range-max").slider("value", ($("#maxUsedAptc").val()*100));
    	}
	}
}

function saveAptc() {
	var montax = parseFloat($("#montax").val());
	var validateUrl = '<c:url value="applyNewAptc"/>';
	$.ajax({
		type: "POST",
	    data: {montax: montax,"csrftoken": $('#csrftoken').val()},
	    url: validateUrl,
	    success : function(response, xhr) {
	    	if(isInvalidCSRFToken(xhr)){
	        	return;
	    	}else{
	    		location.href="showCart";
	    	}
	    },
	},500).done(function( msg ) {
    });
}

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {
		alert($('<spring:message code="pd.label.alert.sessionIsInvalid" javaScriptEscape="true"/>').text());
	    rv = true;
	}
	return rv;
}

function textChange(targetItem){
	if(targetItem.attr("id") == 'slide1'){
		// $("#textSwap").text('<spring:message code="pd.label.adjustTaxCredit" htmlEscape="true"/>');
		// $("#textSwap").attr('title','<spring:message code="pd.label.adjustTaxCredit"/>');
		$("#textSwapConfirm").hide();
		$("#textSwapAdjust").show();
	}else if(targetItem.attr("id") == 'slide2'){
		$("#textSwapConfirm").show();
		$("#textSwapAdjust").hide();
		// $("#textSwap").text('<spring:message code="pd.label.confirm" htmlEscape="true"/>');
		// $("#textSwap").attr('title','<spring:message code="pd.label.confirm"/>');
	}else if(targetItem.attr('id') == 'textSwapConfirm' || targetItem.attr('id') == 'textSwapAdjust'){
		if(targetItem.text().trim() == '<spring:message code="pd.label.confirm" htmlEscape="true"/>'){
			saveAptc();
	    }else{
	    	$('#slide2').click();
	    }
	}
}

$(document).ready(function() {

		setPageTitle('<spring:message code="pd.label.shopcart.message.yourCart"/>');

    // Data layer variable to be pushed at the end of shell.jsp
    dl_pageCategory = 'Checkout';

    // Checkout Step 1

    var healthPlanPresent = $('#healthPlanPresent').val();
	var dentalPlanPresent = $('#dentalPlanPresent').val();
    var healthPlanInfo = '${cartItemHealth.issuer} | ${cartItemHealth.name} | ${cartItemHealth.level}';
    var dentalPlanInfo = '${cartItemDental.issuer} | ${cartItemDental.name} | ${cartItemDental.level}';

    var healthProductInfo = {
        'id': '${cartItemHealth.issuerPlanNumber}',
        'name': '${cartItemHealth.name}',
        'brand': '${cartItemHealth.issuer}',
        'category': '${cartItemHealth.level}',
        'variant': '${cartItemHealth.networkType}',
        'price': '${cartItemHealth.premiumBeforeCredit}'
    }

    var dentalProductInfo = {
        'id': '${cartItemDental.issuerPlanNumber}',
        'name': '${cartItemDental.name}',
        'brand': '${cartItemDental.issuer}',
        'category': '${cartItemDental.level}',
        'variant': '${cartItemDental.networkType}',
        'price': '${cartItemDental.premiumBeforeCredit}'
    }

    var tempEventLabel = '';
    var tempProducts = [];

    
    if(healthPlanPresent == 'Y' && dentalPlanPresent == 'Y'){
    	tempEventLabel = healthPlanInfo + " :: " + dentalPlanInfo;
    	tempProducts = [healthProductInfo, dentalProductInfo];
    }
    
    else if(healthPlanPresent == 'Y' && dentalPlanPresent == 'N'){
		tempEventLabel = healthPlanInfo;
    	tempProducts = [healthProductInfo];
    }
    
    else if(healthPlanPresent == 'N' && dentalPlanPresent == 'Y'){
		tempEventLabel = dentalPlanInfo;
    	tempProducts = [dentalProductInfo];
    }

    dl_eventStack.push({
        'event': 'checkout',
        'eventCategory': 'Plan Selection - ecommerce',
        'eventAction': 'Cart Pageview',
        'eventLabel': tempEventLabel,
        'ecommerce': {
            'checkout': {
                'actionField': {'step': 1},
                'products': tempProducts
            }
        }
    });

	/* enroll now */
	$('#enrollNowBtn').click(function(q) {
		$('#enrollNow').modal('show');
	});
  /*carousel */

	$("#health-details-btn").hover(function() {
		$(this).attr('data-original-title', getMemberLevelPremiumHtml(${healthPlanDetailsByMember}, '${cartItemHealth.premiumBeforeCredit}'));
	});

	$("#dental-details-btn").hover(function() {
		$(this).attr('data-original-title', getMemberLevelPremiumHtml(${dentalPlanDetailsByMember}, '${cartItemDental.premiumBeforeCredit}'));
	});

  $('#myCarousel').carousel({
	interval: false
  });
  $('#emp-pays').popover();
  $('#carousel-nav a').click(function(q){
	q.preventDefault();
	targetSlide = $(this).attr('data-to')-1;
	$('#myCarousel').carousel(targetSlide);
	$(this).addClass('active').siblings().removeClass('active');
  });

  /*carousel */

  $("input").each(function(i) {
	$(this).attr('checked', false);
  });

  $('i.icon-ok').parents('li').css('list-style','none');
  $('span.icon-ok').parents('li').css('list-style', 'none');
});

$(function() {
	$("#slider-range-max").slider({
		orientation: "horizontal",
	    range: "min",
	    min: 0,
	    max: ($("#maxAppliedAptc").val())*100,
	    value: ($("#maxUsedAptc").val())*100,

	    slide: function( event, ui ) {
	      var montax = (ui.value/100);
	      var anntax = parseFloat($("#maxAppliedAptc").val()) - parseFloat(montax);

	      $("#montax").val(montax.toFixed(2));
	      $("#anntax").val(anntax.toFixed(2));
	    }
	});
});

var oldcheckedIds = null;

 $('.changeLink').click(function() {
	if ($(this).hasClass('active') ){
		var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
		for(var i = 0; i < seekingCoverageDentalObj.length; i++)
		{
			if(seekingCoverageDentalObj[i].seekingCoverageDental=="Y")
			{
				$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", true );
			}
			else
			{
				$("#"+seekingCoverageDentalObj[i].chk_id).prop( "checked", false );
			}
		}
		$('#shopping-modal-error').hide();
		$('#shopping-modal-error').html("");
		$('#shopping-modal').modal({
			backdrop: 'static',
		  	keyboard: false
		});

		oldcheckedIds = $(":checkbox:checked").map(function() {
	        return this.id;
	    }).get();
	}
});

function dentalCustomGroupUpdate () {
	var result = this.validateCustomDentalGroup();
 	if(result==false) {
		return result;
	}

 	var orderItemId = '${cartItemDental.orderItemId}';
 	var dentalPlanId = '${cartItemDental.id}';
	var dataStr = this.getSeekingCoverageDataString();

	$.ajax({
		  type: "POST",
		  url: '/hix/private/checkAndAddAvailablePlan',
		  data: {dataString:dataStr, itemId:orderItemId, dentalId:dentalPlanId,"csrftoken": $('#csrfToken').val()},
		  dataType: "text",
		  success:function( response ) {
		    if(response!=null && response=="SUCCESS") {
		      		//location.href="planselection?insuranceType=DENTAL&closedPopup=Y";
		      	$('#shopping-modal').modal('hide');
		    	location.href="showCart";
		    } else {
			    $('#shopping-modal-error').show();
				$('#shopping-modal-error').html('<spring:message code="pd.label.showcart.err.errorWhileProcessingDate" javaScriptEscape="true"/>');
			}
		  }
	});
	return false;
}

function getSeekingCoverageDataString(){

	var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
	var dataStr="";
	var seekingCoverageDental="";
	for(var i = 0; i < seekingCoverageDentalObj.length; i++) {
		if($('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==true ) {
			seekingCoverageDental="<spring:message code="pd.label.yes"/>";
		} else {
			seekingCoverageDental="<spring:message code="pd.label.no"/>";
		}

		if(dataStr=="") {
			dataStr=seekingCoverageDentalObj[i].personId+"="+seekingCoverageDental;
		} else {
			dataStr=dataStr+","+seekingCoverageDentalObj[i].personId+"="+seekingCoverageDental;
		}
	}
	return dataStr;
}

function validateCustomDentalGroup(){
	var count =$('input[name=chkSCoverage]:checked').length;
	if(count==0) {
		$('#shopping-modal-error').show();
		$('#shopping-modal-error').html('<spring:message code="pd.label.selectOneMember" htmlEscape="true"/>');
		return false;
	}
	var newcheckedIds = $(":checkbox:checked").map(function() {
  				return this.id;
		}).get();

	 var olddIds = oldcheckedIds;
	 var flag =  $(newcheckedIds).not(olddIds).length === 0 && $(olddIds).not(newcheckedIds).length === 0;
	 if(flag){
		 $('#shopping-modal').modal('hide');
		 return false;
	 }
	var seekingCoverageDentalObj = JSON.parse($('#seekingCoverageDentalObj').val());
	var isAnyMemberOlderThan_19=false;
	var isSelfUnchecked=false;
	var age=0;
	for(var i = 0; i < seekingCoverageDentalObj.length; i++) {
		if(seekingCoverageDentalObj[i].relationship=="Self" && $('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==false ) {
			isSelfUnchecked=true;
		}

		age=parseInt( seekingCoverageDentalObj[i].age,10);

		if(seekingCoverageDentalObj[i].relationship!="Self" && age>19 && $('#'+seekingCoverageDentalObj[i].chk_id).is(':checked')==true ) {
			isAnyMemberOlderThan_19=true;
		}
	}

	if(isSelfUnchecked==true && isAnyMemberOlderThan_19==true ) {
		$('#shopping-modal-error').show();
		$('#shopping-modal-error').html('<spring:message code="pd.label.adultDentalSelection" htmlEscape="true"/>');
		return false;
	}

	return true;
}


function getMemberLevelPremiumHtml(planDetailsByMember, premium) {

	var tooltipHtml = '<div class=\'margin5-b\'><strong><spring:message code="pd.label.memberPremium.title" javaScriptEscape="true"/></strong></div>';

	for (var i = 0; i < planDetailsByMember.length; i ++) {
		tooltipHtml += '<spring:message code="pd.label.memberPremium.memberAge" javaScriptEscape="true"/> ' + planDetailsByMember[i].age + ' <span class=\'margin20-l pull-right\'>$' + planDetailsByMember[i].premium + '</span><br>';
	}
	tooltipHtml += '<hr class=\'margin0\'>';
	tooltipHtml += '<spring:message code="pd.label.memberPremium.total" javaScriptEscape="true"/><strong class=\'pull-right\'>$' + premium + '</strong>';
	return tooltipHtml;
}
</script>
