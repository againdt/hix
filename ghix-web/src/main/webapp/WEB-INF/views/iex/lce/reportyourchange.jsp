<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
  pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<link rel="stylesheet"
  href='<c:url value="/resources/js/templates/lce/dist/toggle-switch.css" />'>
<link rel="stylesheet"
  href='<c:url value="/resources/js/templates/lce/bower_components/prism/themes/prism.css" />'>
<link rel="stylesheet"
  href='<c:url value="/resources/js/templates/lce/bower_components/prism/themes/prism-okaidia.css" />'>
<script
  src='<c:url value="/resources/js/templates/lce/bower_components/prism/prism.js" />'></script>
<script src='<c:url value="/resources/js/lce/utility.js" />'></script>
<script src='<c:url value="/resources/js/ssap/ssap-zipcode.js" />'></script>
<script src='<c:url value="/resources/js/ssap/ssap-zipcode-utils.js" />'></script>
<script
  src='<c:url value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>'></script>
<script src="../../resources/js/lce/bootstrap-datepicker.js"></script>
<script src="../../resources/js/lce/maskedinput.js"></script>
<script
  src="<c:url value="/resources/angular/jquery.maskedinput.min.js"/>"></script>
<script src="../../resources/js/lce/ui-utils.js"></script>
<script src="../../resources/js/lce/sidebar.js"></script>

<script src='<c:url value="/resources/js/addressValidation.module.js" />'></script>
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>


<link href="<c:url value="/resources/css/lce/home.css" />" media="screen" rel="stylesheet" type="text/css" />
<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
<script>

	
	
	var mailingAddressUpdate = '${mailingAddressUpdate}';

	//redirect to default page if reloaded in between
   if(window.location.href.indexOf("/next", window.location.href.length - '/next'.length) != -1){
	   		if(mailingAddressUpdate=='Y'){
	   			window.location.href='<c:url value="/iex/lce/reportyourchange?coverageYear=${coverageYear}&mau=Y" />'+'#/';
	   		}else{
	   			window.location.href='<c:url value="/iex/lce/reportyourchange?coverageYear=${coverageYear}" />'+'#/';
	   		}
   }  

  
  var ssapJson = ${ssapJson};
  var ssapJsonCloned = ${ssapJson};
  var MARITAL_STATUS=${MARITAL_STATUS};
  var UPDATE_DEPENDENTS=${UPDATE_DEPENDENTS};
  var INCARCERATION=${INCARCERATION_CHANGE};
  var IMMIGRATION_STATUS_CHANGE=${IMMIGRATION_STATUS_CHANGE};
  var ADDRESS_CHANGE=${ADDRESS_CHANGE};
  var LOSS_OF_MEC=${LOSS_OF_MEC};
  var GAIN_MEC=${GAIN_MEC};
  var TRIBE_STATUS=${TRIBE_STATUS};
  var OTHER=${OTHER};
  var DEMOGRAPHICS = ${DEMOGRAPHICS};
  var globalStateCode= '${stateCode}';
  var addTaxDepReason={reason:''};
  $(document).ready(function(){
    $('body').tooltip({
        selector: '[rel=tooltip]',
        placement: 'right',
    });

    //Block to prevent double page reload.
   if( window.localStorage ) {
        if( !localStorage.getItem('ignoreReload') ) {
          localStorage['ignoreReload'] = true;
        }
   }
    var applicationInProgress = '${applicationInProgress}';
    if(applicationInProgress == 'true'){
    	$('#applicationInProgressDiv').show();
    	$('#set-1').html('');
    } 
  });
</script>

<style>
#menu {
  display: none;
}

/* .dropdown-menu table tbody td, .dropdown-menu table thead th  , .dropdown-menu table tbody td button{
	outline: none;
    border: 0px;padding:0px;
    background-color: #FFFFFF;
    padding: 2px;
 }
 .dropdown-menu table tbody td button.btn.btn-default {
 background: #FFFFFF;
 border-color:  #FFFFFF;
 }
 .dropdown-menu{padding: 0;}
  .dropdown-menu table{width: 100%;}
 .dropdown-menu table tbody td button.btn.btn-default.active {
 background: #0000FF;
 border-color:  #FFFFFF;
 }
 .dropdown-menu table tbody td button.btn.btn-default.active span {
 color:  #ffffff;
 }
 .dropdown-menu table tbody td button span  {
 color:  #000000;
 }
 .dropdown-menu table tbody td button span.text-muted  {
 color:  #565655;
 }  */

</style>

<form id="reportYourChangeForm" name="reportYourChangeForm">
  <df:csrfToken />
  <input type="hidden" name="coverageYear" id="coverageYear" value="<encryptor:enc value="${coverageYear}" />" />
</form>

<div ng-app="newMexico">
  <%-- This is not needed <script src='<c:url value="/resources/js/lce/script.js" />'></script> --%>

  <div id="main">
    <div ng-view></div>
    <jsp:include page="home.jsp" />
    <jsp:include page="next.jsp" />

    <script>
      //store dateOfChange
      var eventInfo = { "eventCategory": "",
        "eventSubCategory":"",
        "changedApplicants":[]
      };
      var eventInfoArray=[];
      //variable to tracking new household member added in Marital Status
      var maritalHouseholdAdded=false;
      //global variable to hold dependent Added
      var dependentForHouseholdAdded =  false;
      var dependentHousholdStarted = 0;
	  var dependentStack = [];
	  var dependentFlowLength = 0;

      var eventInfoArrayTemp= [];
      var ageOver26NonSeeking= [];
      var ageOver26NonSeekingDobChange= [];
      

      function getStates(){
        return       [{stateCode:'',stateName:'Select State'},
                         {stateCode:'AL',stateName:'<spring:message code="ssap.state.Albama" text="Alabama" />'},
                         {stateCode:'AK',stateName:'<spring:message code="ssap.state.Alaska" text="Alaska" />'},
                         {stateCode:'AS',stateName:'<spring:message code="ssap.state.AmericanSamoa" text="American Samoa" />'},
                         {stateCode:'AZ',stateName:'<spring:message code="ssap.state.Arizona" text="Arizona" />'},
                         {stateCode:'AR',stateName:'<spring:message code="ssap.state.Arkansas" text="Arkansas" />'},
                         {stateCode:'CA',stateName:'<spring:message code="ssap.state.California" text="California" />'},
                         {stateCode:'CO',stateName:'<spring:message code="ssap.state.Colorado" text="Colorado" />'},
                         {stateCode:'CT',stateName:'<spring:message code="ssap.state.Connecticut" text="Connecticut" />'},
                         {stateCode:'DE',stateName:'<spring:message code="ssap.state.Delaware" text="Delaware" />'},
                         {stateCode:'DC',stateName:'<spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" />'},
                         {stateCode:'FL',stateName:'<spring:message code="ssap.state.Florida" text="Florida" />'},
                         {stateCode:'GA',stateName:'<spring:message code="ssap.state.Georgia" text="Georgia" />'},
                         {stateCode:'GU',stateName:'<spring:message code="ssap.state.Guam" text="Guam" />'},
                         {stateCode:'HI',stateName:'<spring:message code="ssap.state.Hawaii" text="Hawaii" />'},
                         {stateCode:'ID',stateName:'<spring:message code="ssap.state.Idaho" text="Idaho" />'},
                         {stateCode:'IL',stateName:'<spring:message code="ssap.state.Illinois" text="Illinois" />'},
                         {stateCode:'IN',stateName:'<spring:message code="ssap.state.Indiana" text="Indiana" />'},
                         {stateCode:'IA',stateName:'<spring:message code="ssap.state.Iowa" text="Iowa" />'},
                         {stateCode:'KS',stateName:'<spring:message code="ssap.state.Kansas" text="Kansas" />'},
                         {stateCode:'KY',stateName:'<spring:message code="ssap.state.Kentucky" text="Kentucky" />'},
                         {stateCode:'LA',stateName:'<spring:message code="ssap.state.Louisiana" text="Louisiana" />'},
                         {stateCode:'ME',stateName:'<spring:message code="ssap.state.Maine" text="Maine" />'},
                         {stateCode:'MD',stateName:'<spring:message code="ssap.state.Maryland" text="Maryland" />'},
                         {stateCode:'MA',stateName:'<spring:message code="ssap.state.Massachusetts" text="Massachusetts" />'},
                         {stateCode:'MH',stateName:'<spring:message code="ssap.state.Michigan" text="Michigan" />'},
                         {stateCode:'MN',stateName:'<spring:message code="ssap.state.Minnesota" text="Minnesota" />'},
                         {stateCode:'MS',stateName:'<spring:message code="ssap.state.Mississippi" text="Mississippi" />'},
                         {stateCode:'MO',stateName:'<spring:message code="ssap.state.Missouri" text="Missouri" />'},
                         {stateCode:'MT',stateName:'<spring:message code="ssap.state.Montana" text="Montana" />'},
                         {stateCode:'NE',stateName:'<spring:message code="ssap.state.Nebraska" text="Nebraska" />'},
                         {stateCode:'NV',stateName:'<spring:message code="ssap.state.Nevada" text="Nevada" />'},
                         {stateCode:'NH',stateName:'<spring:message code="ssap.state.NewHampshire" text="New Hampshire" />'},
                         {stateCode:'NJ',stateName:'<spring:message code="ssap.state.NewJersey" text="New Jersey" />'},
                         {stateCode:'NM',stateName:'<spring:message code="ssap.state.NewMexico" text="New Mexico" />'},
                         {stateCode:'NY',stateName:'<spring:message code="ssap.state.NewYork" text="New York" />'},
                         {stateCode:'NC',stateName:'<spring:message code="ssap.state.NorthCarolina" text="North Carolina" />'},
                         {stateCode:'ND',stateName:'<spring:message code="ssap.state.NorthDakota" text="North Dakota" />'},
                         {stateCode:'MP',stateName:'<spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" />'},
                         {stateCode:'OH',stateName:'<spring:message code="ssap.state.Ohio" text="Ohio" />'},
                         {stateCode:'OK',stateName:'<spring:message code="ssap.state.Oklahoma" text="Oklahoma" />'},
                         {stateCode:'OR',stateName:'<spring:message code="ssap.state.Oregon" text="Oregon" />'},
                         {stateCode:'PW',stateName:'<spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" />'},
                         {stateCode:'PR',stateName:'<spring:message code="ssap.state.PuertoRico" text="Puerto Rico" />'},
                         {stateCode:'RI',stateName:'<spring:message code="ssap.state.RhodeIsland" text="Rhode Island" />'},
                         {stateCode:'SC',stateName:'<spring:message code="ssap.state.SouthCarolina" text="South Carolina" />'},
                         {stateCode:'SD',stateName:'<spring:message code="ssap.state.SouthDakota" text="South Dakota" />'},
                         {stateCode:'TN',stateName:'<spring:message code="ssap.state.Tennessee" text="Tennessee" />'},
                         {stateCode:'TX',stateName:'<spring:message code="ssap.state.Texas" text="Texas" />'},
                         {stateCode:'UT',stateName:'<spring:message code="ssap.state.Utah" text="Utah" />'},
                         {stateCode:'VT',stateName:'<spring:message code="ssap.state.Vermont" text="Vermont" />'},
                         {stateCode:'VA',stateName:'<spring:message code="ssap.state.Virginia" text="Virginia" />'},
                         {stateCode:'VI',stateName:'<spring:message code="ssap.state.VirginIslands" text="Virgin Islands" />'},
                         {stateCode:'WA',stateName:'<spring:message code="ssap.state.Washington" text="Washington" />'},
                         {stateCode:'WV',stateName:'<spring:message code="ssap.state.WestVirginia" text="West Virginia" />'},
                         {stateCode:'WI',stateName:'<spring:message code="ssap.state.Wisconsin" text="Wisconsin" />'},
                         {stateCode:'WY',stateName:'<spring:message code="ssap.state.Wyoming" text="Wyoming" />'}];

      }
      
      function getStatesForTribe(){
          return       [{stateCode:'',stateName:'Select State'},
                        {stateCode:'AL',stateName:'<spring:message code="ssap.state.Albama" text="Alabama" />'},
                        {stateCode:'AK',stateName:'<spring:message code="ssap.state.Alaska" text="Alaska" />'},
                        {stateCode:'AZ',stateName:'<spring:message code="ssap.state.Arizona" text="Arizona" />'},
                        {stateCode:'CA',stateName:'<spring:message code="ssap.state.California" text="California" />'},
                        {stateCode:'CO',stateName:'<spring:message code="ssap.state.Colorado" text="Colorado" />'},
                        {stateCode:'CT',stateName:'<spring:message code="ssap.state.Connecticut" text="Connecticut" />'},
                        {stateCode:'FL',stateName:'<spring:message code="ssap.state.Florida" text="Florida" />'},
                        {stateCode:'ID',stateName:'<spring:message code="ssap.state.Idaho" text="Idaho" />'},
                        {stateCode:'IA',stateName:'<spring:message code="ssap.state.Iowa" text="Iowa" />'},
                        {stateCode:'KS',stateName:'<spring:message code="ssap.state.Kansas" text="Kansas" />'},
                        {stateCode:'LA',stateName:'<spring:message code="ssap.state.Louisiana" text="Louisiana" />'},
                        {stateCode:'ME',stateName:'<spring:message code="ssap.state.Maine" text="Maine" />'},
                        {stateCode:'MA',stateName:'<spring:message code="ssap.state.Massachusetts" text="Massachusetts" />'},
                        {stateCode:'MN',stateName:'<spring:message code="ssap.state.Minnesota" text="Minnesota" />'},
                        {stateCode:'MS',stateName:'<spring:message code="ssap.state.Mississippi" text="Mississippi" />'},
                        {stateCode:'MT',stateName:'<spring:message code="ssap.state.Montana" text="Montana" />'},
                        {stateCode:'NE',stateName:'<spring:message code="ssap.state.Nebraska" text="Nebraska" />'},
                        {stateCode:'NV',stateName:'<spring:message code="ssap.state.Nevada" text="Nevada" />'},
                        {stateCode:'NM',stateName:'<spring:message code="ssap.state.NewMexico" text="New Mexico" />'},
                        {stateCode:'NY',stateName:'<spring:message code="ssap.state.NewYork" text="New York" />'},
                        {stateCode:'NC',stateName:'<spring:message code="ssap.state.NorthCarolina" text="North Carolina" />'},
                        {stateCode:'ND',stateName:'<spring:message code="ssap.state.NorthDakota" text="North Dakota" />'},
                        {stateCode:'OK',stateName:'<spring:message code="ssap.state.Oklahoma" text="Oklahoma" />'},
                        {stateCode:'OR',stateName:'<spring:message code="ssap.state.Oregon" text="Oregon" />'},
                        {stateCode:'RI',stateName:'<spring:message code="ssap.state.RhodeIsland" text="Rhode Island" />'},
                        {stateCode:'SC',stateName:'<spring:message code="ssap.state.SouthCarolina" text="South Carolina" />'},
                        {stateCode:'SD',stateName:'<spring:message code="ssap.state.SouthDakota" text="South Dakota" />'},
                        {stateCode:'TX',stateName:'<spring:message code="ssap.state.Texas" text="Texas" />'},
                        {stateCode:'UT',stateName:'<spring:message code="ssap.state.Utah" text="Utah" />'},
                        {stateCode:'WA',stateName:'<spring:message code="ssap.state.Washington" text="Washington" />'},
                        {stateCode:'WI',stateName:'<spring:message code="ssap.state.Wisconsin" text="Wisconsin" />'},
                        {stateCode:'WY',stateName:'<spring:message code="ssap.state.Wyoming" text="Wyoming" />'}];

        }
      
      function getEthnicities(){
    	  return  [{ "label": 'Cuban',"code":'2182-4',"index":"0"},
                   { "label": 'Mexican, Mexican American, or Chicano/a',"code":'2148-5',"index":"1"},
                   { "label": 'Puerto Rican',"code":'2180-8',"index":"2"},
                   { "label": 'Other',"code":'000-0',"index":"3"}
                 ];
      }
      
 	  function getRaces(){
    	  return  [ { "label": '<spring:message code="label.lce.americanindianalaskannative"/>',"code":'1002-5',"index":"0"},
    	           { "label": '<spring:message code="label.lce.asianindian"/>',"code":'2029-7',"index":"1"},
    	           { "label":'<spring:message code="label.lce.blackorafricanamerican"/>',"code":'2054-5',"index":"2"},
    	           { "label":'<spring:message code="label.lce.chinese"/>',"code":'2034-7',"index":"3"},
    	           { "label":'<spring:message code="label.lce.filipino"/>',"code":'2036-2',"index":"4"},
    	           { "label":'<spring:message code="label.lce.guamanianchamorro"/>',"code":'2086-7',"index":"5"},
    	           { "label":'<spring:message code="label.lce.japanese"/>',"code":'2039-6',"index":"6"},
    	           { "label":'<spring:message code="label.lce.korean"/>',"code":'2040-4',"index":"7"},
    	           { "label":'<spring:message code="label.lce.nativehawaiian"/>',"code":'2079-2',"index":"8"},
    	           { "label":'<spring:message code="label.lce.otherasian"/>',"code":'2028-9',"index":"9"},
    	           { "label":'<spring:message code="label.lce.otherpacificislander"/>',"code":'2500-7',"index":"10"},
    	           { "label":'<spring:message code="label.lce.samoan"/>',"code":'2080-0',"index":"11"},
    	           { "label":'<spring:message code="label.lce.vietnamese"/>',"code":'2047-9',"index":"12"},
    	           { "label":'<spring:message code="label.lce.whitecaucasian"/>',"code":'2106-3',"index":"13"},
    	           { "label":'<spring:message code="label.lce.other"/>',"code":'000-0',"index":"14"}
    	         ];
      }

      // 1. Module named 'newMexico'
      var newMexico = angular.module('newMexico', ['ngRoute', 'ui.bootstrap', 'ui.mask', 'addressValidationModule', 'spring-security-csrf-token-interceptor']);

      newMexico.filter('capitalize', function() {
        return function(input, all) {
          return (!!input) ? input.replace(/([^\W_]+[^\s-]*) */g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();}) : '';
        }
      });

      newMexico.config(['datepickerConfig', function(datepickerConfig) {
    	    datepickerConfig.showWeeks = false;
      }]);
      
      
      //=========================================================================
      //// RootScope: START //
      //=========================================================================
       //add wrappers for common methods in root scope
       newMexico.run(function($rootScope) {
    	
    	
    	//refresh the page on back click
    	$rootScope.$on('$locationChangeStart', function (event, to, current) {
    	  if(current != undefined && current.indexOf("/next", current.length - '/next'.length)!=-1 && to.indexOf("#/", to.length - '#/'.length)!=-1 ){
    		  event.preventDefault();
    		  if(mailingAddressUpdate=='Y'){
    			  window.location.href='<c:url value="/indportal#/mypreferences" />';
    		  }else{
    			  window.location.href='<c:url value="/iex/lce/reportyourchange?coverageYear=${coverageYear}" />';  
    		  }
    	  }
		});
    	 
        //add saveSSAPJSON to root scope as this will be common for all controllers
        //$rootScope.saveSSAPJSON = saveSSAPJSON;
        $rootScope.showButton = true;
        $rootScope.counter = 0;
        $rootScope.lceCode = "N/A";
        $rootScope.lceEvent = "N/A"
        $rootScope.lceCounter = 0;
        $rootScope.isBackFromAdressPage = false;
        $rootScope.countries = [
          {value: 'AFG', name: 'AFGHANISTAN'},
          {value: 'ALB', name: 'ALBANIA'},
          {value: 'DZA', name: 'ALGERIA'},
          {value: 'ASM', name: 'AMERICAN SAMOA'},
          {value: 'AND', name: 'ANDORRA'},
          {value: 'AGO', name: 'ANGOLA'},
          {value: 'AIA', name: 'ANGUILLA'},
          {value: 'ATA', name: 'ANTARCTICA'},
          {value: 'ATG', name: 'ANTIGUA AND BARBUDA'},
          {value: 'ARG', name: 'ARGENTINA'},
          {value: 'ARM', name: 'ARMENIA'},
          {value: 'ABW', name: 'ARUBA'},
          {value: 'AUS', name: 'AUSTRALIA'},
          {value: 'AUT', name: 'AUSTRIA'},
          {value: 'AZE', name: 'AZERBAIJAN'},
          {value: 'BHS', name: 'BAHAMAS'},
          {value: 'BHR', name: 'BAHRAIN'},
          {value: 'BGD', name: 'BANGLADESH'},
          {value: 'BRB', name: 'BARBADOS'},
          {value: 'BLR', name: 'BELARUS'},
          {value: 'BEL', name: 'BELGIUM'},
          {value: 'BLZ', name: 'BELIZE'},
          {value: 'BEN', name: 'BENIN'},
          {value: 'BMU', name: 'BERMUDA'},
          {value: 'BTN', name: 'BHUTAN'},
          {value: 'BOL', name: 'BOLIVIA, PLURINATIONAL STATE OF'},
          {value: 'BIH', name: 'BOSNIA AND HERZEGOVINA'},
          {value: 'BWA', name: 'BOTSWANA'},
          {value: 'BVT', name: 'BOUVET ISLAND'},
          {value: 'BRA', name: 'BRAZIL'},
          {value: 'IOT', name: 'BRITISH INDIAN OCEAN TERRITORY'},
          {value: 'BRN', name: 'BRUNEI DARUSSALAM'},
          {value: 'BGR', name: 'BULGARIA'},
          {value: 'BFA', name: 'BURKINA FASO'},
          {value: 'BDI', name: 'BURUNDI'},
          {value: 'KHM', name: 'CAMBODIA'},
          {value: 'CMR', name: 'CAMEROON'},
          {value: 'CAN', name: 'CANADA'},
          {value: 'CPV', name: 'CAPE VERDE'},
          {value: 'CYM', name: 'CAYMAN ISLANDS'},
          {value: 'CAF', name: 'CENTRAL AFRICAN REPUBLIC'},
          {value: 'CIV', name: 'CETE D\'IVOIRE'},
          {value: 'TCD', name: 'CHAD'},
          {value: 'CHL', name: 'CHILE'},
          {value: 'CHN', name: 'CHINA'},
          {value: 'CXR', name: 'CHRISTMAS ISLAND'},
          {value: 'CCK', name: 'COCOS (KEELING) ISLANDS'},
          {value: 'COL', name: 'COLOMBIA'},
          {value: 'COM', name: 'COMOROS'},
          {value: 'COG', name: 'CONGO'},
          {value: 'COD', name: 'CONGO, DEMOCRATIC REPUBLIC OF THE'},
          {value: 'COK', name: 'COOK ISLANDS'},
          {value: 'CRI', name: 'COSTA RICA'},
          {value: 'HRV', name: 'CROATIA'},
          {value: 'CUB', name: 'CUBA'},
          {value: 'CYP', name: 'CYPRUS'},
          {value: 'CZE', name: 'CZECH REPUBLIC'},
          {value: 'DNK', name: 'DENMARK'},
          {value: 'DJI', name: 'DJIBOUTI'},
          {value: 'DMA', name: 'DOMINICA'},
          {value: 'DOM', name: 'DOMINICAN REPUBLIC'},
          {value: 'ECU', name: 'ECUADOR'},
          {value: 'EGY', name: 'EGYPT'},
          {value: 'SLV', name: 'EL SALVADOR'},
          {value: 'ALA', name: 'ELAND ISLANDS'},
          {value: 'GNQ', name: 'EQUATORIAL GUINEA'},
          {value: 'ERI', name: 'ERITREA'},
          {value: 'EST', name: 'ESTONIA'},
          {value: 'ETH', name: 'ETHIOPIA'},
          {value: 'FLK', name: 'FALKLAND ISLANDS (MALVINAS)'},
          {value: 'FRO', name: 'FAROE ISLANDS'},
          {value: 'FJI', name: 'FIJI'},
          {value: 'FIN', name: 'FINLAND'},
          {value: 'FRA', name: 'FRANCE'},
          {value: 'GUF', name: 'FRENCH GUIANA'},
          {value: 'PYF', name: 'FRENCH POLYNESIA'},
          {value: 'ATF', name: 'FRENCH SOUTHERN TERRITORIES'},
          {value: 'GAB', name: 'GABON'},
          {value: 'GMB', name: 'GAMBIA'},
          {value: 'GEO', name: 'GEORGIA'},
          {value: 'DEU', name: 'GERMANY'},
          {value: 'GHA', name: 'GHANA'},
          {value: 'GIB', name: 'GIBRALTAR'},
          {value: 'GRC', name: 'GREECE'},
          {value: 'GRL', name: 'GREENLAND'},
          {value: 'GRD', name: 'GRENADA'},
          {value: 'GLP', name: 'GUADELOUPE'},
          {value: 'GUM', name: 'GUAM'},
          {value: 'GTM', name: 'GUATEMALA'},
          {value: 'GGY', name: 'GUERNSEY'},
          {value: 'GIN', name: 'GUINEA'},
          {value: 'GNB', name: 'GUINEA-BISSAU'},
          {value: 'GUY', name: 'GUYANA'},
          {value: 'HTI', name: 'HAITI'},
          {value: 'HMD', name: 'HEARD ISLAND AND MCDONALD ISLANDS'},
          {value: 'VAT', name: 'HOLY SEE (VATICAN CITY STATE)'},
          {value: 'HND', name: 'HONDURAS'},
          {value: 'HKG', name: 'HONG KONG'},
          {value: 'HUN', name: 'HUNGARY'},
          {value: 'ISL', name: 'ICELAND'},
          {value: 'IND', name: 'INDIA'},
          {value: 'IDN', name: 'INDONESIA'},
          {value: 'IRN', name: 'IRAN, ISLAMIC REPUBLIC OF'},
          {value: 'IRQ', name: 'IRAQ'},
          {value: 'IRL', name: 'IRELAND'},
          {value: 'IMN', name: 'ISLE OF MAN'},
          {value: 'ISR', name: 'ISRAEL'},
          {value: 'ITA', name: 'ITALY'},
          {value: 'JAM', name: 'JAMAICA'},
          {value: 'JPN', name: 'JAPAN'},
          {value: 'JEY', name: 'JERSEY'},
          {value: 'JOR', name: 'JORDAN'},
          {value: 'KAZ', name: 'KAZAKHSTAN'},
          {value: 'KEN', name: 'KENYA'},
          {value: 'KIR', name: 'KIRIBATI'},
          {value: 'PRK', name: 'KOREA, DEMOCRATIC PEOPLE\'S REPUBLIC OF'},
          {value: 'KOR', name: 'KOREA, REPUBLIC OF'},
          {value: 'KVO', name: 'KOSOVO'},
          {value: 'KWT', name: 'KUWAIT'},
          {value: 'KGZ', name: 'KYRGYZSTAN'},
          {value: 'LAO', name: 'LAO PEOPLE\'S DEMOCRATIC REPUBLIC'},
          {value: 'LVA', name: 'LATVIA'},
          {value: 'LBN', name: 'LEBANON'},
          {value: 'LSO', name: 'LESOTHO'},
          {value: 'LBR', name: 'LIBERIA'},
          {value: 'LBY', name: 'LIBYAN ARAB JAMAHIRIYA'},
          {value: 'LIE', name: 'LIECHTENSTEIN'},
          {value: 'LTU', name: 'LITHUANIA'},
          {value: 'LUX', name: 'LUXEMBOURG'},
          {value: 'MAC', name: 'MACAO'},
          {value: 'MKD', name: 'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF'},
          {value: 'MDG', name: 'MADAGASCAR'},
          {value: 'MWI', name: 'MALAWI'},
          {value: 'MYS', name: 'MALAYSIA'},
          {value: 'MDV', name: 'MALDIVES'},
          {value: 'MLI', name: 'MALI'},
          {value: 'MLT', name: 'MALTA'},
          {value: 'MHL', name: 'MARSHALL ISLANDS'},
          {value: 'MTQ', name: 'MARTINIQUE'},
          {value: 'MRT', name: 'MAURITANIA'},
          {value: 'MUS', name: 'MAURITIUS'},
          {value: 'MYT', name: 'MAYOTTE'},
          {value: 'MEX', name: 'MEXICO'},
          {value: 'FSM', name: 'MICRONESIA, FEDERATED STATES OF'},
          {value: 'MDA', name: 'MOLDOVA, REPUBLIC OF'},
          {value: 'MCO', name: 'MONACO'},
          {value: 'MNG', name: 'MONGOLIA'},
          {value: 'MNE', name: 'MONTENEGRO'},
          {value: 'MSR', name: 'MONTSERRAT'},
          {value: 'MAR', name: 'MOROCCO'},
          {value: 'MOZ', name: 'MOZAMBIQUE'},
          {value: 'MMR', name: 'MYANMAR'},
          {value: 'NAM', name: 'NAMIBIA'},
          {value: 'NRU', name: 'NAURU'},
          {value: 'NPL', name: 'NEPAL'},
          {value: 'NLD', name: 'NETHERLANDS'},
          {value: 'ANT', name: 'NETHERLANDS ANTILLES'},
          {value: 'NCL', name: 'NEW CALEDONIA'},
          {value: 'NZL', name: 'NEW ZEALAND'},
          {value: 'NIC', name: 'NICARAGUA'},
          {value: 'NER', name: 'NIGER'},
          {value: 'NGA', name: 'NIGERIA'},
          {value: 'NIU', name: 'NIUE'},
          {value: 'NFK', name: 'NORFOLK ISLAND'},
          {value: 'MNP', name: 'NORTHERN MARIANA ISLANDS'},
          {value: 'NOR', name: 'NORWAY'},
          {value: 'OMN', name: 'OMAN'},
          {value: 'PAK', name: 'PAKISTAN'},
          {value: 'PLW', name: 'PALAU'},
          {value: 'PSE', name: 'PALESTINIAN TERRITORY, OCCUPIED'},
          {value: 'PAN', name: 'PANAMA'},
          {value: 'PNG', name: 'PAPUA NEW GUINEA'},
          {value: 'PRY', name: 'PARAGUAY'},
          {value: 'PER', name: 'PERU'},
          {value: 'PHL', name: 'PHILIPPINES'},
          {value: 'PCN', name: 'PITCAIRN'},
          {value: 'POL', name: 'POLAND'},
          {value: 'PRT', name: 'PORTUGAL'},
          {value: 'PRI', name: 'PUERTO RICO'},
          {value: 'QAT', name: 'QATAR'},
          {value: 'REU', name: 'REUNION'},
          {value: 'ROU', name: 'ROMANIA'},
          {value: 'RUS', name: 'RUSSIAN FEDERATION'},
          {value: 'RWA', name: 'RWANDA'},
          {value: 'BLM', name: 'SAINT BARTHELEMY'},
          {value: 'SHN', name: 'SAINT HELENA, ASCENSION AND TRISTAN DA CUNHA'},
          {value: 'KNA', name: 'SAINT KITTS AND NEVIS'},
          {value: 'LCA', name: 'SAINT LUCIA'},
          {value: 'MAF', name: 'SAINT MARTIN (FRENCH PART)'},
          {value: 'SPM', name: 'SAINT PIERRE AND MIQUELON'},
          {value: 'VCT', name: 'SAINT VINCENT AND THE GRENADINES'},
          {value: 'WSM', name: 'SAMOA'},
          {value: 'SMR', name: 'SAN MARINO'},
          {value: 'STP', name: 'SAO TOME AND PRINCIPE'},
          {value: 'SAU', name: 'SAUDI ARABIA'},
          {value: 'SEN', name: 'SENEGAL'},
          {value: 'SRB', name: 'SERBIA'},
          {value: 'SYC', name: 'SEYCHELLES'},
          {value: 'SLE', name: 'SIERRA LEONE'},
          {value: 'SGP', name: 'SINGAPORE'},
          {value: 'SVK', name: 'SLOVAKIA'},
          {value: 'SVN', name: 'SLOVENIA'},
          {value: 'SLB', name: 'SOLOMON ISLANDS'},
          {value: 'SOM', name: 'SOMALIA'},
          {value: 'ZAF', name: 'SOUTH AFRICA'},
          {value: 'SGS', name: 'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS'},
          {value: 'SSD', name: 'SOUTH SUDAN'},
          {value: 'ESP', name: 'SPAIN'},
          {value: 'LKA', name: 'SRI LANKA'},
          {value: 'STL', name: 'STATELESS'},
          {value: 'SDN', name: 'SUDAN'},
          {value: 'SUR', name: 'SURINAME'},
          {value: 'SJM', name: 'SVALBARD AND JAN MAYEN'},
          {value: 'SWZ', name: 'SWAZILAND'},
          {value: 'SWE', name: 'SWEDEN'},
          {value: 'CHE', name: 'SWITZERLAND'},
          {value: 'SYR', name: 'SYRIAN ARAB REPUBLIC'},
          {value: 'TWN', name: 'TAIWAN, PROVINCE OF CHINA'},
          {value: 'TJK', name: 'TAJIKISTAN'},
          {value: 'TZA', name: 'TANZANIA, UNITED REPUBLIC OF'},
          {value: 'THA', name: 'THAILAND'},
          {value: 'TLS', name: 'TIMOR-LESTE'},
          {value: 'TGO', name: 'TOGO'},
          {value: 'TKL', name: 'TOKELAU'},
          {value: 'TON', name: 'TONGA'},
          {value: 'TTO', name: 'TRINIDAD AND TOBAGO'},
          {value: 'TUN', name: 'TUNISIA'},
          {value: 'TUR', name: 'TURKEY'},
          {value: 'TKM', name: 'TURKMENISTAN'},
          {value: 'TCA', name: 'TURKS AND CAICOS ISLANDS'},
          {value: 'TUV', name: 'TUVALU'},
          {value: 'UGA', name: 'UGANDA'},
          {value: 'UKR', name: 'UKRAINE'},
          {value: 'ARE', name: 'UNITED ARAB EMIRATES'},
          {value: 'GBR', name: 'UNITED KINGDOM'},
          {value: 'USA', name: 'UNITED STATES'},
          {value: 'UMI', name: 'UNITED STATES MINOR OUTLYING ISLANDS'},
          {value: 'URY', name: 'URUGUAY'},
          {value: 'UZB', name: 'UZBEKISTAN'},
          {value: 'VUT', name: 'VANUATU'},
          {value: 'VEN', name: 'VENEZUELA, BOLIVARIAN REPUBLIC OF'},
          {value: 'VNM', name: 'VIET NAM'},
          {value: 'VGB', name: 'VIRGIN ISLANDS (BRITISH)'},
          {value: 'VIR', name: 'VIRGIN ISLANDS (U.S.)'},
          {value: 'WLF', name: 'WALLIS AND FUTUNA'},
          {value: 'ESH', name: 'WESTERN SAHARA *'},
          {value: 'YEM', name: 'YEMEN'},
          {value: 'ZMB', name: 'ZAMBIA'},
          {value: 'ZWE', name: 'ZIMBABWE'}
        ];

        $rootScope.activeDeathFilter = function (householdMember) {
            var active= householdMember.active;
            var deathIndicator=householdMember.socialSecurityCard.deathIndicator;//if active is not present OR active is true && death false  true ELSE false

            if((active == undefined || active == null || active == true) && deathIndicator==false ){
              return true;
            }else{
              return false;
            }

       };


        $rootScope.updateGlobalIncarcerationFlag=function() {
           var isHouseholdIncarcerated = false;
           var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
           for(var i=0;i<householdArray.length;i++){
             if($rootScope.activeDeathFilter(householdArray[i])  && householdArray[i].incarcerationStatus.incarcerationStatusIndicator){
               isHouseholdIncarcerated = true;
             }
           }
           ssapJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator =  isHouseholdIncarcerated;
        };
        
        $rootScope.UIDateformat = function(dt){
			if($.trim(dt)=="") return dt;
			if(dt==undefined) return "";
			var formattedDate = new Date(dt);
			var dd = formattedDate.getDate();
			var mm =  formattedDate.getMonth();
			mm += 1;  // JavaScript months are 0-11
			var yyyy = formattedDate.getFullYear();
			if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
			formatteddate = mm + "/" + dd + "/" + yyyy;
			return formatteddate;
		
		};
        
		
		$rootScope.checkAgeOver16FromDate = function(birthDate){
			var effectiveDate = new Date();
			
			var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

			if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
				years--;
			}
			
			if(years >=16){
				return true;
			}else{
				return false;
			}
		};
		
        $rootScope.checkIfAgeOver16 = function(householdMember){
			var birthDate = new Date($rootScope.UIDateformat(householdMember.dateOfBirth));
			return $rootScope.checkAgeOver16FromDate(birthDate);
			
		 };
		 
		 

       });
      //=========================================================================
      ////RootScope: END //
      //=========================================================================


      //2. Configure our routes
       newMexico.config(function($routeProvider, $locationProvider) {
       $routeProvider
         .when('/', { templateUrl : 'home.jsp', controller  : 'lifeChangeCtrl' })
         .when('/next', { templateUrl : 'next.jsp', controller  : 'nextCtrl' })
         .when('/incacerationComplete', { templateUrl : 'nextIncarceration.jsp', controller  : 'nextCtrl' });
       });


      // 3. Factory
      newMexico.factory('myService', function() {
        //variable to track parialCount
        var partialCount=0;

        // Declare variable in the entire scope
        var lifeEventSelection=[];
        // Declare list of incomes for income selection
        var incomeTypes = [
          {type: "<spring:message code='label.lce.job'/>", url: "income-job.html", value: false},
          {type: "<spring:message code='label.lce.selfemployed'/>", url: "income-self.html", value: false},
          {type: "<spring:message code='label.lce.ssb'/>", url: "income-ssb.html", value: false},
          {type: "<spring:message code='label.lce.unemployment'/>", url: "income-unemployment.html", value: false},
          {type: "<spring:message code='label.lce.retirepension'/>", url: "income-retire.html", value: false},
          {type: "<spring:message code='label.lce.capitalgains'/>", url: "income-capitalgains.html", value: false},
          {type: "<spring:message code='label.lce.investmentincome'/>", url: "income-invest.html", value: false},
          {type: "<spring:message code='label.lce.rentalroyalincome'/>", url: "income-rental.html", value: false},
          {type: "<spring:message code='label.lce.farmfishingincome'/>", url: "income-farming.html", value: false},
          {type: "<spring:message code='label.lce.alimonymoney'/>", url: "income-alimony.html", value: false},
          {type: "<spring:message code='label.lce.other'/>", url: "income-other.html", value: false},
        ];
        incomeTypes.selected = 0;

        // Array of partials for adding dependents
        var dependentForms = ['maritalstatus-details.jsp',
          'maritalstatus-address.jsp',
          'maritalstatus-ssn.jsp',
          'maritalstatus-citizenship.jsp',
          'maritalstatus-ethnicityrace.jsp',
          'maritalstatus-incarceration.jsp',
          'maritalstatus-special.jsp',
			    'shared-relationship.jsp',
          /* 'maritalstatus-summary.jsp', */
          /* 'maritalstatus-income.jsp',
          'maritalstatus-incomededuction.jsp',
          'maritalstatus-incomesummary.jsp', */
          /* 'maritalstatus-other.jsp' */
          ];
        var addDependents = [{ partials: dependentForms, value: false}];
        dependentFlowLength = dependentForms.length; 

        var currentEventService;
        // Factory returns the 2 functions:
        // 1. getFiltered..
        // 2. setLifeEvent...
        // 3.set and get currentEvent
        return {
          getFilteredEvents: function() {
            // Declare empty array 'filtered'
            var filtered = [];
            // Iterate through 'lifeEventSelection', which consists of
            for (var i = 0; i < lifeEventSelection.length; i++) {
              if (lifeEventSelection[i].value) {
                filtered.push(lifeEventSelection[i]);
              }
            }
            return filtered;
          },
          // Set function takes in the $scope.lifeEvents as argument and makes 'lifeEventSelection' equal to that
          // We filter this above in the for loop and push to empty array 'filterd'
          setLifeEventSelection: function (cat) {
            lifeEventSelection = cat;
          },
          // This is for the income selection process
          getIncomeEvents: function() {
              return incomeTypes;
          },
          editIncomeEvents: function(event) {
              for (var i = 0; i < incomeTypes.length; i++) {
                  if (incomeTypes[i].type === event.type) {
                    incomeTypes[i].value = !incomeTypes[i].value;
                    return incomeTypes;
                  }
              }
          },
          getDepForms: function() {
              return addDependents;
          },
          setDepForms: function(event) {
              for (var i = 0; i < addDependents.length; i++) {
                addDependents[i].value = !addDependents[i].value;
              }
              return addDependents;
          },
          setCurrentEvent:function(event) {
            currentEventService=event;
            },
          getCurrentEvent:function(){
            return currentEventService;
          },
          setMember: function(event) {
            currentMember = event;
          },
          getMember: function() {
            return currentMember;
          },
          getPartialCount:function(){
            return partialCount;
          },
          setPartialCount:function(patialCnt){
            partialCount=patialCnt;
          }

        };
      }); // End of Factory


        //change it global as this will be used in Number of dependents controller
        //Array of number of dependents partials that user has to traverse through
        var depedentsArray = [
          // 'numofdep-add.jsp',
          // 'numofdep-remove.jsp',
          'numofdep-complete.jsp',
        ];

      //4. Controller 1 'lifeChangeCtrl':
       newMexico.controller('lifeChangeCtrl', ['$scope', '$http','$location', 'myService', '$rootScope', '$timeout', function($scope, $http, $location, myService, $rootScope, $timeout) {

         function reLoadHomeView() {
           if( window.localStorage ) {
              if( !localStorage.getItem('ignoreReload') ) {
                localStorage['ignoreReload'] = true;
                window.location.reload();
              }
              else {
                localStorage.removeItem('ignoreReload');
              }
           }
         }

        $timeout(reLoadHomeView, 0);


         // Array of marital status partials that user has to traverse
         var maritalArray = ['maritalstatus-details.jsp',
           'maritalstatus-address.jsp',
           'maritalstatus-ssn.jsp',
           'maritalstatus-citizenship.jsp',
           'maritalstatus-ethnicityrace.jsp',
           'maritalstatus-incarceration.jsp',
           'maritalstatus-special.jsp',
			     'shared-relationship.jsp',
           /* 'maritalstatus-summary.jsp', */
           /* 'maritalstatus-income.jsp',
           'maritalstatus-incomededuction.jsp',
           'maritalstatus-incomesummary.jsp', */
           /* 'maritalstatus-other.jsp', */
           'maritalstatus-complete.jsp'
         ];
         // Array of incarceration partials that user has to traverse through
         var incarcerationArray = ['incarceration-next.jsp'];
         var lossArray = [
              // 'lossofmin-summary.jsp',
              'lossofmin-complete.jsp'];
         var gainArray = [
              // 'gainofmin-summary.jsp',
              'gainofmin-complete.jsp'];
         var addressArray = ['changeinaddress-complete.jsp'];
         var lawArray = ['lawfulpresence-complete.jsp'];
         var additionalDependents = [];
         var incomeArray = ['income-summary.jsp','income-complete.jsp'];
         var nameArray = [
                          // 'namechange-summary.jsp',
                          'namechange-complete.jsp'];
         var relationshipArray= ['relationshipcomplete.jsp'];
         var tribeArray = [
                          // 'tribechange-summary.jsp',
                          'tribechange-complete.jsp'];
         var otherChangesArray = ['other-complete.jsp'];

         // Array of sub tasks to go to the sidebar
         var eventdate = ['<spring:message code="label.lce.eventdate"/>'];
         var eventArray = ['<spring:message code="label.lce.tableofdependents"/>'];
         var subMarital = ['<spring:message code="label.lce.personalinformation"/>',
                           '<spring:message code="label.lce.address"/>',
                           '<spring:message code="label.lce.ssn"/>',
                           '<spring:message code="label.lce.citizenship"/>',
                           '<spring:message code="label.lce.ethnicityandrace"/>',
                           '<spring:message code="label.lce.incarceration"/>',
                           '<spring:message code="label.lce.additionalquestions"/>',
			                     'Household Relationship',
                           '<spring:message code="label.lce.individualsummary"/>'
                           /* '<spring:message code="label.lce.income"/>',
                           '<spring:message code="label.lce.incomededuction"/>',
                           '<spring:message code="label.lce.incomesummary"/>',  */
                           /* '<spring:message code="label.lce.additionalquestions"/>', */ ];
         var subDependents = ['<spring:message code="label.lce.additionaldep"/>',
                              '<spring:message code="label.lce.depinformation"/>'];
         var subarray = ['<spring:message code="label.lce.editincome"/>',
                         '<spring:message code="label.lce.incomesummary"/>'];
         var otherarray = ['<spring:message code="label.lce.complete"/>'];
         var summaries = [
                          // '<spring:message code="label.lce.summary"/>'
                          ];


         // Reasons for each life event
         var maritalStatusReason = "Did you recently get married? Did you recently get divorced? Or did a spouse pass away?";
         var dependentReason = "Did you give birth or adopt a child? Was a child placed in your home for foster care? Did one of your children turn 26? Or do you have any other changes to dependents/wards in your care? Did a dependent pass away?";
         var addressReason = "Did you or anyone in your household move?"
         var immigrationReason = "Did someone in your household recently experience a change in immigration status?"
         var demographicReason = "Do you need to report a correction such as name, date of birth or Social Security number for someone in your household?"
         var relationshipReason = "Do you need to report a change to relationship for someone in your household?"
         var incarcerationReason = "Was anyone in your household released from incarceration (prison or detention) or did anyone become incarcerated?"
         var nativeTribeReason = "Did you or anyone in your household recently gain status as a member of a federally-recognized tribe?"
         var loseCoverageReason = "The type of coverage an individual needs to have to meet the individual responsibility requirement under the Affordable Care Act. This includes individual market policies, job-based coverage, Medicare, Medicaid, CHIP, TRICARE and certain other coverage."
         var gainCoverageReason = "The type of coverage an individual needs to have to meet the individual responsibility requirement under the Affordable Care Act. This includes individual market policies, job-based coverage, Medicare, Medicaid, CHIP, TRICARE and certain other coverage."
         var otherReason = "Did you experience technical issues when trying to enroll, or any other type of misunderstanding that caused you not to get the coverage you intended to select? Did you have another situation that prevented you from enrolling during the open enrollment period?";
         //var dummyReason = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
		 $scope.otherCategory = OTHER[0].category;
         // Scope variable that contains the life event types and the initial page that the user sees upon selection
         $scope.lifeEvents = [{eventCode:MARITAL_STATUS[0].category,eventName:  MARITAL_STATUS[0].categoryLabel, value: false, eventdate: eventdate, subcategories: subMarital, url: 'maritalstatus.jsp', partials: maritalArray, reason: maritalStatusReason, image: '../../resources/img/lce/maritalstatus.png',tooltipHeading:true},
                              {eventCode:UPDATE_DEPENDENTS[0].category,eventName: UPDATE_DEPENDENTS[0].categoryLabel, value: false, eventdate: eventdate, subcategories: subDependents, url: 'numberofdependents.jsp', partials: depedentsArray, reason: dependentReason, image: '../../resources/img/lce/dependents.png',tooltipHeading:true},
                              /* {eventCode:INCOME[0].category,eventName: categoryLabel"<spring:message code='label.lce.incomechange'/>", value: false, eventdate: eventArray, subcategories: subarray, url: 'income.jsp', partials: incomeArray, reason: dummyReason,tooltipHeading:true}, */
                              {eventCode:ADDRESS_CHANGE[0].category,eventName:ADDRESS_CHANGE[0].categoryLabel, value: false, eventdate: eventArray, subcategories: otherarray, url: 'changeinaddress.jsp', partials: addressArray, reason: addressReason, image: '../../resources/img/lce/addresschange.png',tooltipHeading:true},
                              {eventCode:IMMIGRATION_STATUS_CHANGE[0].category,eventName:IMMIGRATION_STATUS_CHANGE[0].categoryLabel, value: false, eventdate: eventArray, subcategories: otherarray, url: 'lawfulpresence.jsp', partials: lawArray, reason: immigrationReason, image: '../../resources/img/lce/citizenship.png',tooltipHeading:true},
                              {eventCode:DEMOGRAPHICS[0].category,eventName:DEMOGRAPHICS[0].categoryLabel, value: false, eventdate: eventArray, subcategories: summaries, url: 'namechange.jsp', partials: nameArray, reason: demographicReason, image: '../../resources/img/lce/namechange.png',tooltipHeading:true},
                              {eventCode:DEMOGRAPHICS[0].category,eventName:DEMOGRAPHICS[8].label, value: false, eventdate: eventArray, subcategories: summaries, url: 'relationshipchange.jsp', partials: relationshipArray, reason: relationshipReason, image: '../../resources/img/lce/relationship.png',tooltipHeading:true},
                              {eventCode:INCARCERATION[0].category,eventName:INCARCERATION[0].categoryLabel, value: false, eventdate: eventArray, subcategories: otherarray, url: 'incarceration.jsp', partials: incarcerationArray, reason: incarcerationReason, image: '../../resources/img/lce/incarceration.png',tooltipHeading:true},
                              {eventCode:TRIBE_STATUS[0].category,eventName:TRIBE_STATUS[0].categoryLabel, value: false, eventdate: eventArray, subcategories: summaries, url: 'tribechange.jsp', partials: tribeArray, reason: nativeTribeReason, image: '../../resources/img/lce/tribe.png',tooltipHeading:true},
                              {eventCode:LOSS_OF_MEC[0].category,eventName:LOSS_OF_MEC[0].categoryLabel, value: false, eventdate: eventArray, subcategories: summaries, url: 'lossofmin.jsp', partials: lossArray, reason: loseCoverageReason, image: '../../resources/img/lce/losemec.png',tooltipHeading:false},
                              {eventCode:GAIN_MEC[0].category,eventName:GAIN_MEC[0].categoryLabel, value: false, eventdate: eventArray, subcategories: summaries, url: 'gainothermin.jsp', partials: gainArray, reason: gainCoverageReason, image: '../../resources/img/lce/gainmec.png',tooltipHeading:false},
                              {eventCode:OTHER[0].category,eventName:OTHER[0].categoryLabel, value: false, partials: otherChangesArray, eventdate: eventArray, subcategories: otherarray, url: 'other.jsp', reason: otherReason, image: '../../resources/img/lce/other.png',tooltipHeading:true}
                              ];

        //code to set previously selected values true.
        //This code get selected events from service and set  $scope.lifeEvents[].value to true.
         try{
           $scope.checkedElements= myService.getFilteredEvents();
           for(var loopVar=0;loopVar< $scope.lifeEvents.length;loopVar++){
             for(var j=0;j< $scope.checkedElements.length;j++){
              if( $scope.lifeEvents[loopVar].eventCode==$scope.checkedElements[j].eventCode){
                $scope.lifeEvents[loopVar].value=true;
                break;
              }
             }
           }

         }catch(err){
           //very first time getFilteredEvents throws exception because var lifeEventSelection in service is null
         }

         // Scope function for the next button on the page with the life event changes
         // This goes through the route because we give it a path in the html
        var lifeEventCounter = 0;

        $scope.currentLifeEventSeelcted = 'Nothing';

        var counter = 0;
        $scope.next = function(path) {
          scroll(0,0);
        counter++;
        $scope.currentLifeEventSeelcted = $scope.lifeEvents[lifeEventCounter].eventName;

          myService.setLifeEventSelection($scope.lifeEvents);
          $location.path(path);
          $scope.selectedEvents = myService.getFilteredEvents();

          //Code to check if marital status selected or not
          var maritalStatusAdded=false;
          for (var i = 0; i < $scope.selectedEvents.length; i++) {
            if($scope.selectedEvents[i].eventName=="Marital Status"){
              maritalStatusAdded=true;
            }
          }
          if(!maritalStatusAdded){
            removeHousehold();//If marital status is not selected then remove household member (If added)
          }

          $scope.lifeEventCounter = lifeEventCounter;


          //>> Hide all subcategories
          function hideSubbies() {
              for(var a = 1; a < 20; a++) {
              $('#stuff'+a).find('.subcategories').hide();
              $('#stuff'+a).find('.subcategories2').hide();
              //>> The first subcategory that displays will have a cheveron
              $('#stuff0').find('.subcategories').html('Enter Event Details '+ ' &nbsp; <i class="icon-chevron-right"></i>');
            }
            };
            window.setTimeout(hideSubbies, 150);

          //>> Set the rootScope variables to update the header and the omnious sidebar
          $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
          $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;


          }; // End of Next function
          
          if(mailingAddressUpdate == 'Y'){
        	  $scope.lifeEvents[2].value = true;
        	  $scope.next('/next');
          }
          
      }]); // End of Controller 1

      //Set lifeEventCounter to 0 - later to increment
      var lifeEventCounter = 0;

      //5. Controller 2 'nextCtrl':
       newMexico.controller('nextCtrl', ['$scope', '$http','$location', 'myService', '$rootScope', function($scope, $http, $location, myService, $rootScope) {


         // Relationships and codes
         $scope.relationships = [
								 {code: '', type:'Select' }, 
                                 {code: '01', type:'<spring:message code="label.lce.spouse"/>' },
                                 {code: '03', type:'<spring:message code="label.lce.parent"/>' },
			                           {code: '03a', type:'Parent of adopted child' },
			                           {code: '03w', type:'Parent/Caretaker of the ward ' },
			                           // {code: '04', type:'<spring:message code="label.lce.grandparents"/>' },
			                           // {code: '05', type:'<spring:message code="label.lce.grandchild"/>' },
			                           // {code: '06', type:'<spring:message code="label.lce.uncleaunt"/>' },
			                           // {code: '07', type:'<spring:message code  ="label.lce.nephewniece"/>' },
			                           // {code: '08', type:'<spring:message code="label.lce.firstcousin"/>' },
			                           {code: '09', type:'<spring:message code="label.lce.adoptedchild"/>' },			                           // {code: '10', type:'<spring:message code="label.lce.fosterchild"/>' },
			                           // {code: '11', type:'<spring:message code="label.lce.sonsisterinlaw"/>' },
			                           // {code: '12', type:'<spring:message code="label.lce.brothersisterinlaw"/>' },
			                           // {code: '13', type:'<spring:message code="label.lce.motherfatherinlaw"/>' },
			                           {code: '14', type:'<spring:message code="label.lce.sibling"/>' },
			                           // {code: '15', type:'<spring:message code="label.lce.ward"/>' },
			                           {code: '15c', type:'Ward of court-appointed guardian' },
			                           {code: '15p', type:'Ward of parent/caretaker' },
                                 {code: '16', type:'<spring:message code="label.lce.stepparent"/>' },
                                 {code: '17', type:'<spring:message code="label.lce.stepchild"/>' },
			                           // {code: '18', type:'<spring:message code="label.lce.self"/>' },
                                 {code: '19', type:'<spring:message code="label.lce.child"/>' },
			                           // {code: '23', type:'<spring:message code="label.lce.sponsereddependent"/>' },
			                           // {code: '24', type:'<spring:message code="label.lce.depdendentofaminor"/>' },
			                           // {code: '25', type:'<spring:message code="label.lce.formerspouse"/>' },
			                           // {code: '26', type:'<spring:message code="label.lce.guardian"/>' },
                                 {code: '31', type:'<spring:message code="label.lce.courtappointedguardian"/>' },
			                           // {code: '38', type:'<spring:message code="label.lce.collateraldependent"/>' },
			                           // {code: '53', type:'<spring:message code="label.lce.domesticpartner"/>' },
			                           // {code: '60', type:'<spring:message code="label.lce.annuitant"/>' },
			                           // {code: 'D2', type:'<spring:message code="label.lce.trustee"/>' },
			                           // {code: 'G8', type:'<spring:message code="label.lce.unspecifiedrelationship"/>' },
			                           // {code: 'G9', type:'<spring:message code="label.lce.unspecifiedrelative"/>' },
			                           // {code: '03-53', type: "<spring:message code="label.lce.parentsdomesticpartner"/>"},
			                           // {code: '53-19', type:'<spring:message code="label.lce.childofdomesticpartner"/>'}
                                 ];

        $scope.suffix = ['Jr.', 'Sr.', 'III', 'IV'];
       
        // Set counter to 0 - later to increment...
        // Set lifeEventCounter to 0 - later to increment...
        var counter = 0;
        myService.setPartialCount(counter);
        var summaryPageAdded=false;
        var nextButtonLifeEventCounter = 0;

        //When next controller loads... Populate global array of all households.
        //While adding new dependents...While Marriage event add new entry to this array
        //Add required variables to object
        eventInfoArrayTemp = [];
        //add temp array to controller scope
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;

        //push the elements in the temp event array
        for(var i=0;i<householdArray.length;i++){
			var tempEventInfo = {
                changeInAddressDate:'',
                citizenShipChangeDate:'',
                tribeChangeDate:'',
                changeInLossDate:'',
                nameChangeDate:'',
                dobChangeDate:'',
                ssnChangeDate:'',
                incomeChangeDate:'',
                incomeChangeIndicator:'',
                incomeTypes:angular.copy(myService.getIncomeEvents()),
                hasOtherIncomeTypes:'No',
                lossEventSubCategory:'',
                changeInGainDate:'',
                gainEventSubCategory:'',
                dateOfChange:'',
                lawfulChangeDate:'',
                removeDepEventName:''
                
              };
            eventInfoArrayTemp.push(tempEventInfo);
		}

        //$scope.incomeTypes = myService.getIncomeEvents();

        // Now that the scope was filtered in the above function in the factory, we create a scope variable
        $scope.selectedEvents = myService.getFilteredEvents();
        // Create a scope variable 'currentPage' to equal the $scope.selectedEvents and by index: 'lifeCounter'
        $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
        //this variable is used to store event level temporary information(such as eventDate, which will be passed to applicantDate)
        $scope.currentEvent=$scope.selectedEvents[lifeEventCounter];
        myService.setCurrentEvent($scope.currentEvent);
        // SSAP JSON object that we need to save to
         $scope.jsonObject = ssapJson;
         $scope.eventInfo= eventInfo;
         //wrapper for save method
         $scope.saveSSAPJSON = function(eventSubCategory) {
           saveSSAPJSON($scope,lifeEventCounter);
         };

      //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////// currentPageSidebar
         $scope.test = $scope.lifeEventCounter;

         // Scope function 'next' for all partials after the initial partial that the user sees after they select life events
         $scope.nextButtonLifeEvent;
         var sideBarCounter = 0;
         $scope.next = function() {
           scroll(0,0);

         //>> Updating the rootScope variables
         if (counter == $scope.selectedEvents[lifeEventCounter].partials.length) {
           if ($scope.selectedEvents[lifeEventCounter+1] == undefined) {
             $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
             $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;
           } else {
             $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter+1].eventCode;
             $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter+1].eventName;
             $rootScope.lceCounter++;
           }
         } else if (counter == $scope.selectedEvents[lifeEventCounter].partials.length -1){
        	 $rootScope.showButton = false; 
         } else {
           $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
           $rootScope.lceEvent = $scope.selectedEvents[lifeEventCounter].eventName;
         }

        // SIDEBAR FUNCTION
          sideBarCounter++;
          var subcat = $scope.selectedEvents[lifeEventCounter].subcategories.length + 2;
          if ( $rootScope.lceCounter === 0) {
            for (var i = 0; i < subcat; i++) {
                if (sideBarCounter == i) {
                  var subId = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
                  var subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i-1);
                  $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] + ' &nbsp; <i class="icon-ok"></i>');
                  if ( $scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] == undefined ) {
                  $(subIdRPrevious).html("Enter Event Details" + ' &nbsp; <i class="icon-ok"></i>');
                  }
                  $(subId).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-1)] + ' &nbsp; <i class="icon-chevron-right"></i>');
                  $('.subcategories2').addClass('show');
                }
            }
          //>> The first subcategory that displays will have a cheveron
            $('#stuff1').find('.subcategories').html('Enter Event Details '+ ' &nbsp; <i class="icon-chevron-right"></i>');
            //console.log("=======");
            //console.log(i);
        } else if ( $rootScope.lceCounter === 1) {
          $('#stuff0').find('.subcategories').hide();
          $('#stuff0').find('.subcategories2').hide();
          $('#stuff1').find('.subcategories').show();
          if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
            $('#stuff1').find('.subcategories2').show();
            //>>>
            for (var i = 0; i < 10; i++) {
              if (counter == i) {
                subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
                if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>');
                  } else {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
                  }
                $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>');
              }
            }
          } else {
            $('#stuff1').find('.subcategories2').hide();
          }
          //>> The first subcategory that displays will have a cheveron
            $('#stuff2').find('.subcategories').html('Enter Event Details '+ ' &nbsp; <i class="icon-chevron-right"></i>');
            //console.log("=======");
            //console.log(i);
        } else if ( $rootScope.lceCounter  === 2) {
          $('#stuff1').find('.subcategories').hide();
          $('#stuff1').find('.subcategories2').hide();
          $('#stuff2').find('.subcategories').show();
          if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
            $('#stuff2').find('.subcategories2').show();
            //>>>
            for (var i = 0; i < 10; i++) {
              if (counter == i) {
                subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
                if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>')
                  } else {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
                  }
                $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>')
              }
            }
          } else {
            $('#stuff2').find('.subcategories2').hide()
          }
        } else if ( $rootScope.lceCounter  === 3) {
          $('#stuff2').find('.subcategories').hide();
          $('#stuff2').find('.subcategories2').hide();
          $('#stuff3').find('.subcategories').show();
          if (counter < $scope.selectedEvents[lifeEventCounter].subcategories.length) {
            $('#stuff3').find('.subcategories2').show();
            //>>>
            for (var i = 0; i < 10; i++) {
              if (counter == i) {
                subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
                if ( $scope.selectedEvents[lifeEventCounter].eventdate[i] == undefined ) {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].subcategories[i-1] + ' &nbsp; <i class="icon-ok"></i>')
                  } else {
                    $(subIdRPrevious).html($scope.selectedEvents[lifeEventCounter].eventdate[i] + ' &nbsp; <i class="icon-ok"></i>');
                  }
                $('#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i+1)).html($scope.selectedEvents[lifeEventCounter].subcategories[i] + ' &nbsp; <i class="icon-chevron-right"></i>')
              }
            }
          } else {
            $('#stuff3').find('.subcategories2').hide()
          }
        }
         // SIDEBAR FUNCTION


           //if counter is lenght-1 then set the next event in the scope as success page will be partials.length-1
           if (counter === $scope.selectedEvents[lifeEventCounter].partials.length-1) {
             nextButtonLifeEventCounter++;
             if(nextButtonLifeEventCounter < $scope.selectedEvents.length){
               $scope.nextButtonLifeEvent = ": "+$scope.selectedEvents[nextButtonLifeEventCounter].eventName;
             }
             else{
               $scope.nextButtonLifeEvent="";
             }

             if(!summaryPageAdded){
               eventInfo.eventCategory = $scope.selectedEvents[lifeEventCounter].eventCode;


                 var tmpEventInfo= {changedApplicants:[],eventCategory: '',eventSubCategory:''};
                     tmpEventInfo.changedApplicants=eventInfo.changedApplicants;
                     tmpEventInfo.eventCategory=eventInfo.eventCategory;
                     tmpEventInfo.eventSubCategory=eventInfo.eventSubCategory;
					 if(tmpEventInfo.eventSubCategory === 'MAILING_ADDRESS_CHANGE'){
						 tmpEventInfo.eventCategory = 'DEMOGRAPHICS';
					 }
					 if((eventInfo.eventCategory == MARITAL_STATUS[0].category && eventInfo.eventSubCategory == MARITAL_STATUS[0].name) || (eventInfo.eventCategory == UPDATE_DEPENDENTS[0].category && !(eventInfo.eventSubCategory == UPDATE_DEPENDENTS[1].name || eventInfo.eventSubCategory == UPDATE_DEPENDENTS[2].name ))){
						var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
						//var lastHousehold =  householdArray[householdArray.length -1];
						updateNewlyAddedHouseholdToOriginalJson(householdArray,eventInfo.changedApplicants);
					 }
                     //eventInfoArray.push(tmpEventInfo);
                     addEventInfo(tmpEventInfo)
               		 initEventInfo();
             }


           }

           // Loop through 'lifeEventCounter' (earlier set to 0) to increment if counter equals the number of selected life event partials
           // This condition is to check if we reach the end of the current partial list and need to go to the next life event
           if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
              lifeEventCounter++;
              counter = 0;
              myService.setPartialCount(counter);
              if(lifeEventCounter==$scope.selectedEvents.length){
                //console.log(lifeEventCounter==$scope.selectedEvents.length);
                if(!summaryPageAdded){
                  summaryPageAdded=true;

                  lifeEventCounter--;
                    $scope.currentEvent.partials.push(['summary.jsp', 'review-dummy.jsp']);
                    flatten($scope.currentEvent.partials);
                    $scope.currentEvent.partials = flatten($scope.currentEvent.partials);
                    counter=$scope.currentEvent.partials.length- 2;
                   $scope.currentPage =$scope.currentEvent.partials[counter];// 'review-dummy.jsp';
                   counter++;
                   myService.setPartialCount(counter);
  //                 debugger
                }

              }else{
                //debugger
                $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
                //debugger
              }
              //this variable is used to store event level temporary information(such as eventDate, which will be passed to applicantDate)
              $scope.currentEvent=$scope.selectedEvents[lifeEventCounter];
              myService.setCurrentEvent($scope.currentEvent);
              return;
           }
           $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
           $scope.currentPageSidebar = $scope.currentPage;
           ///////////////////////////////////////////////////////////////////////////////
         //  console.log("currentPage >>>> "+ $scope.currentPage);


           counter++;
           myService.setPartialCount(counter);
           //debugger;
           var runSum = 0;
           for (var i = 0; i < $scope.depCounter + 1; i++) {
            $scope['person'+i+'array'] = $scope['person'+i+'array'] || myService.getIncomeEvents();
            runSum += $scope['person'+i+'array'].selected;
           // console.log('selected income events: ',$scope['person'+i+'array'].selected);
           }

           $scope.dependentForms = $scope.dependentForms || [];
           $scope.depCounter += Math.floor( counter / ($scope.dependentForms.length * ($scope.depCounter + 1) + runSum + 1));
           //console.log($scope.depCounter);
           //alert($scope.currentPage);
           //alert(counter)
           //console.log("Partial #: "+counter)
           //var subcatLength = $('.sidebar').find('.subcategories').length
           //console.log("There are " + subcatLength + " subcategories")
           $scope.changeMe = $scope.selectedEvents[lifeEventCounter].eventCode;
           $scope.counter = counter;
           $scope.lifeEventCounter = lifeEventCounter;

      //  $rootScope.counter++;
      //  console.log("rootscope counter controller 1: " + $rootScope.counter);

  //        debugger
      //  if ( counter ==  $scope.selectedEvents[lifeEventCounter].partials.length ) {
  //        $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter+1].eventCode;
      // //   console.log("rootscope lceCode controller 2: " + $rootScope.lceCode)
      //  } else {
  //        $rootScope.lceCode = $scope.selectedEvents[lifeEventCounter].eventCode;
      // //   console.log("rootscope lceCode controller 2: " + $rootScope.lceCode)
      //  };
           if ($scope.currentPage === "maritalstatus-complete.jsp" && $scope.currentEvent.eventCode === MARITAL_STATUS[0].category)//"MARITAL_STATUS"
                 $('#stuff0').find('li:not(:first)').hide();

         }; // End of Next Function

         // This function will send the user last partial of the current life event
         $scope.lastSuccess = function ()  {
           var arrayofpartials = $scope.selectedEvents[lifeEventCounter].partials;
           var poppedlastpartial = arrayofpartials.pop();
           $scope.currentPage = poppedlastpartial;
         };

         // This function will send the user first partial of the current life event
         $scope.firstPartial = function () {
         var currentPartials =  $scope.selectedEvents[lifeEventCounter].partials;
           //find the index marital status in cuurent arrray
         var maritalStatusUrl = 'maritalstatus-details.jsp';
         counter =$.inArray(maritalStatusUrl,currentPartials,dependentHousholdStarted);
         myService.setPartialCount(counter);
         if(counter != -1){
           $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter++];
         }
         };

         // This function is JUST for the income summary table so user can go back to income partial
         $scope.editIncome = function () {
           //get the current parital with lifeEventCounter
           var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
           //get the number of income templates in current parital
           var noOfIncomeTemplatesInPartials = getNoOfIncomeTemplatesInPartials($scope.incomeTypes,currentPartials);
           //change the counter to index of income page
           counter = counter - (noOfIncomeTemplatesInPartials+3);
           myService.setPartialCount(counter);
           $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter++];
        };

         // This function will send the user back by index-1
         $scope.back = function() {
            // SIDEBAR FUNCTION
             sideBarCounter--;
             var subcat = $scope.selectedEvents[lifeEventCounter].subcategories.length;

            for (var i = 0; i < subcat; i++) {
                if (sideBarCounter == i) {
                  var subIdRPrevious = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i-1);
                  var subId = '#sub'+$scope.selectedEvents[lifeEventCounter].eventCode+(i);
                  if ( $scope.selectedEvents[lifeEventCounter].subcategories[(i-2)] == 'undefined' ) {
                    $(subIdRPrevious).html("Enter Event Details" + ' &nbsp; <i class="icon-chevron-left"></i>');
                    }
                    if ($scope.selectedEvents[lifeEventCounter].subcategories[(i-1)] == undefined) {
                      $(subId).html('Enter Event Details' + ' &nbsp; <i class="icon-chevron-left"></i>');
                    } else {
                      $(subId).html($scope.selectedEvents[lifeEventCounter].subcategories[(i-1)] + ' &nbsp; <i class="icon-chevron-left"></i>');
                    }
                }
                if ( $(subId).html() == 'Enter Event Details &nbsp; <i class="icon-chevron-left"></i>' ) {
                  $(subId).html("Enter Event Details" + ' &nbsp; <i class="icon-chevron-left"></i>');
                }
            }
            // SIDEBAR FUNCTION
            //if(counter == $scope.selectedEvents[lifeEventCounter].partials.length -1){
        	//	// find already added event for active event
        	//	if(eventInfoArray.length !=0){
		    //        	eventInfo = eventInfoArray[eventInfoArray.length -1];
		    //        	$scope.eventInfo= eventInfo;
		    //        	eventInfoArray.splice(eventInfoArray.length -1,1);
            //	}
            //}

           if (counter === 1) {
             $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
           } else if (counter > 1) {
             $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter-2];
           } else {
             lifeEventCounter--;
             if (lifeEventCounter < 0) {
            	$location.path('/');
          // lifeEventCounter = 0;
          // counter = 0;
             } else {

               counter = $scope.selectedEvents[lifeEventCounter].partials.length-1;
               myService.setPartialCount(counter);
               $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
             }
           }
           counter--;
           myService.setPartialCount(counter);
         };//End of Back function..

        var flatten = function(array) {
              var result = [], self = arguments.callee;
              array.forEach(function(item) {
                Array.prototype.push.apply(
                  result,
                  Array.isArray(item) ? self(item) : [item]
                );
              });
              return result;
            };


          // Adding a dependent
          $scope.truIds = [];
          $scope.addDep = function(numParam) {
            var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
            $scope.depCounter=0;
            $scope.numDependents = numParam;
            // Get number of dependents and save into this variable: Integer
            var num = parseInt(numParam);
            // Forms that are reused from Marital Status section (details-other html pages/partials): Arraylist
            $scope.dependentForms = myService.setDepForms(this)[0].partials;
            // Empty array that will contain ($scope.dependentForms) x num and be a multidimensional array
            // If user enters 2 dependents, the array will have 2 arrays: [Array[11], Array[11]]
            var newPartials = [];

            var trueIds = [];
            if ( $scope.currentEvent.eventCode === 'INCOME') {
              //Delete all partials except last two. Because here we are goinf to add new partials as per selection.
              currentPartials.splice(0,currentPartials.length-2);
              var copies = 0;
              $scope.truIds = trueIds;
              for (var i = 0; i < eventInfoArrayTemp.length; i++) {
                if (eventInfoArrayTemp[i].incomeChangeIndicator === true) {
                  copies++;
                  trueIds.push(eventInfoArrayTemp[i].index);
                }
              }
              $scope.numCopies = copies;
            //  console.log($scope.numCopies);
            //  console.log(trueIds);

             // console.log($scope.householdMember);
                // For loop that will push the forms that are reused from Marital Status section according to the number of dependents entered
              for (var i = 0; i < $scope.numCopies; i++) {
                newPartials.push("maritalstatus-income.html");
              }
            currentPartials.splice(0,0,newPartials);
            currentPartials = flatten(currentPartials);
  //          $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[trueIds[0]].householdMember[trueIds[0]];
  //          console.log(newPartials);
            } else {
              // For loop that will push the forms that are reused from Marital Status section according to the number of dependents entered
              for (var i = 0; i < num; i++) {
                newPartials.push($scope.dependentForms.slice(0));
              }
              // Splice in the multidimensional array into the current partial
              currentPartials.splice(1,0,newPartials);
              // After the marital status arrays have been spliced into the original array we splice the 'partials/numofdep-remove.jsp' partial
              //==============================| currentPartials is a MULTIDIMENSIONAL ARRAY | ===================================
              currentPartials.splice(-2,1);
            }

            // Now we flatten the multidimensional array
            var temp3 = flatten(currentPartials);
            temp3.splice(temp3.length, 0, "numofdep-complete.jsp");
            // Reassign the flattened multidimensional array to be the '$scope.selectedEvents[0].partials' so that the user can traverse back and fourth thru the new array
            $scope.selectedEvents[lifeEventCounter].partials = temp3;
            selectPartialsCopy = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
           
          }; // End of Adding a dependent

          var whichOne = 0;
          $scope.truIds;
          $scope.whichDep = function () {
        //  console.log("====="+whichOne);
  //          $scope.householdMember = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember[$scope.truIds[whichOne]];

            $scope.householdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[$scope.truIds[whichOne]];
          myService.setMember($scope.householdMember);

            whichOne++;
          //debugger;
          };

          // Remove a Depedent
          /* $scope.removeDependents = function() {
              $scope.selectedEvents[lifeEventCounter].partials.splice(0,1);
              if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
                lifeEventCounter++;
                counter = 0;
                myService.setPartialCount(counter);
                $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
                return;
                // alert(lifeEventCounter);
              }
              $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
              counter++;
              myService.setPartialCount(counter);
            }; */

         //add income array into scope
         $scope.incomeTypes = myService.getIncomeEvents();

         //to init income select on other income No Selection
         $scope.initIncomes = function(){
          // initIncomeTypes($scope);
           var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
          var individualIncomeTypes=[];
          var currentHshld= myService.getMember();
           for(var i=0;i<houseHoldMemberArray.length;i++){
            if(houseHoldMemberArray[i].personId    === currentHshld.personId){
              individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
              break;
            }
          }

           for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
             individualIncomeTypes[i].value = false;
            }
         };

         //find the selected income count
         $scope.selectedIncomeCount = function(){
           var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
          var individualIncomeTypes=[];
          var currentHshld= myService.getMember();
           for(var i=0;i<houseHoldMemberArray.length;i++){
            if(houseHoldMemberArray[i].personId    === currentHshld.personId){
              individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
              break;
            }
          }
           totalSelectedCount = 0;
           for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
               if (individualIncomeTypes[i].value) {
                 totalSelectedCount++;
               }
             }
           return totalSelectedCount;
         };

         //TODO - Select Income
         $scope.selectIncome = function() {
         var currentPartials = $scope.selectedEvents[lifeEventCounter].partials.slice(0);
         //if user came from income summary then of income page will be already in partial
         //need to remove them from partial and add again to avoid duplicate income pages
         var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
         var individualIncomeTypes=[];
         var currentHshld= myService.getMember();
         for(var i=0;i<houseHoldMemberArray.length;i++){
          if(houseHoldMemberArray[i].personId    === currentHshld.personId){
            individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
            break;
          }
        }


         var noOfIncomePagesInTemplates = getNoOfIncomeTemplatesInPartials(individualIncomeTypes,currentPartials,counter);
         //remove the income page from the current partials
         if(noOfIncomePagesInTemplates>0){
           currentPartials.splice(counter, noOfIncomePagesInTemplates);
         }
         var newPartials = [];
       //  console.log("newPartials: "+ newPartials);
           var copies =angular.copy( currentPartials);// currentPartials.slice(0);
           //$scope.incomeTypes = myService.editIncomeEvents(this.income);
           for (var i = individualIncomeTypes.length -1; i >= 0; i--) {
             if (individualIncomeTypes[i].value) {
               // currentPartials.splice(counter,0, $scope.incomeTypes[i].url);
               // newPartials.push($scope.incomeTypes[i].url);
               copies.splice(counter, 0, individualIncomeTypes[i].url);
             }
           }
           // $scope.selectedEvents[0].partials = currentPartials.slice(0);
           // copies.splice(counter, 0 , newPartials);
           $scope.selectedEvents[lifeEventCounter].partials = copies;
         }; // Select your income


          // This function redirects user to the second  partial of the above array for Number of Depedents, which is the remove page
          $scope.removeDependents = function() {
          $scope.selectedEvents[lifeEventCounter].partials=   [
                                                                // 'numofdep-remove.jsp',
                                                                  'numofdep-complete.jsp',
                                                                ];
          eventInfo.eventCategory = $scope.selectedEvents[lifeEventCounter].eventCode;


          var eventInfoEventTypeArray={};
          for(var i=0;i<eventInfo.changedApplicants.length;i++){
	        	var originalIndexInArray=-1;
	        	var clonedHouseHoldArray=ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
	        	$.each(clonedHouseHoldArray, function(key,value) {
	        		if(value.personId === eventInfo.changedApplicants[i].personId){
 		       			 originalIndexInArray=key;
 		       			// break;
      		  		}
        		});
        		var strProperty = eventInfoArrayTemp[originalIndexInArray].removeDepEventName;
        		var tmpEventInfo = eventInfoEventTypeArray[strProperty];
        		if(tmpEventInfo === undefined || tmpEventInfo === null){
        			tmpEventInfo= {changedApplicants:[],eventCategory: '',eventSubCategory:''};
        			eventInfoEventTypeArray[strProperty] = tmpEventInfo;
        		}

            	tmpEventInfo.changedApplicants.push({"personId":eventInfo.changedApplicants[i].personId,"eventDate":eventInfo.changedApplicants[i].eventDate});
            	tmpEventInfo.eventCategory=eventInfo.eventCategory;
            	tmpEventInfo.eventSubCategory = eventInfoArrayTemp[originalIndexInArray].removeDepEventName;
                    //eventInfoArray.push(tmpEventInfo);
                    //addEventInfo(tmpEventInfo)
            }
          	$.each(eventInfoEventTypeArray, function(key,value) {
        	  addEventInfo(value);
  			});
			// reset event info to default value
			initEventInfo();
             if (counter === $scope.selectedEvents[lifeEventCounter].partials.length) {
              lifeEventCounter++;
              counter = 0;
              myService.setPartialCount(counter);
              $scope.currentPage = $scope.selectedEvents[lifeEventCounter].url;
              return;
               //alert(lifeEventCounter);
            }
             
             nextButtonLifeEventCounter++;
             if(nextButtonLifeEventCounter < $scope.selectedEvents.length){
               $scope.nextButtonLifeEvent = ": "+$scope.selectedEvents[nextButtonLifeEventCounter].eventName;
             }
             else{
               $scope.nextButtonLifeEvent="";
             }
             
             $scope.currentPage = $scope.selectedEvents[lifeEventCounter].partials[counter];
            counter++;
            myService.setPartialCount(counter); 
          };

       //add addChangedApplicants to scope methods
       $scope.addChangedApplicants = addChangedApplicants;
       $scope.removeChangedApplicants = removeChangedApplicants;
       //next button function for death/divorse and legal seperation
       $scope.deathDivorseLegalNext= function(changeDate){
         //TODO: When Multiple Spous..then which spous will be specified as dead?
         if(eventInfo.eventSubCategory=== MARITAL_STATUS[2].name || eventInfo.eventSubCategory== MARITAL_STATUS[1].name ){  //'DEATH_OF_SPOUSE'    'DIVORCE_OR_ANULLMENT'
           var localHouseholdMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
           for(var i=0;i < localHouseholdMemberArray.length ; i++){
            var relationShipWithPrimaryHousehold = $scope.getRelationShipCodeWithPrimaryHousehold(localHouseholdMemberArray[i].personId);
            if(relationShipWithPrimaryHousehold =='01'){//01 is Spous relationship code.
               if(eventInfo.eventSubCategory=== MARITAL_STATUS[2].name){//if death of spouse mark as inactive
            	   localHouseholdMemberArray[i].socialSecurityCard.deathIndicator=true;
            	   localHouseholdMemberArray[i].active=false;
            	   
            	  if(eventInfo.eventSubCategory === 'DEATH'){
            		 localHouseholdMemberArray[i].applyingForCoverageIndicator = false;//Applied as part of HIX-60808
            	  }
            	  
               }
            	
               if (eventInfo.eventSubCategory== MARITAL_STATUS[1].name && localHouseholdMemberArray[i].applyingForCoverageIndicator){
            	   eventInfo.eventSubCategory = MARITAL_STATUS[1].name+'_OTHER';
               }
               addChangedApplicants(localHouseholdMemberArray[i].personId,changeDate);
                 break;
               }
           }
        }
        //update partial counter
         counter =$scope.selectedEvents[lifeEventCounter].partials.length-1;
         myService.setPartialCount(counter);
         $scope.next();
         //calling save function.This will save data and load next page
        // saveSSAPJSON($scope,lifeEventCounter);
       };

        //Added to false the
        $scope.nextHouseholdDependent = function(){
          //if event it Number of Dependent then add a Dependent household
          if( $scope.selectedEvents[lifeEventCounter].eventCode== UPDATE_DEPENDENTS[0].category ){ //'UPDATE_DEPENDENTS'
            if(dependentForHouseholdAdded){
              dependentForHouseholdAdded = false;
            }
            //assign counter to dependent start index
            dependentHousholdStarted = counter;
          }else if($scope.selectedEvents[lifeEventCounter].eventCode== MARITAL_STATUS[0].category){ //'MARITAL_STATUS'
            var householdMemberArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
          }
          //initialize the income array to default values
         // initIncomeTypes($scope);
          //go to next page
          
          $scope.next();

        };



      //Filter for income summary screens [MARRIAGE/CHANGE IN INCOME/ADD DEPENDENT]
        $scope.incomeSummaryFilter = function (householdMember) {
            var active= householdMember.active;
            var deathIndicator=householdMember.socialSecurityCard.deathIndicator;//if active is not present OR active is true && death false  true ELSE false
            var activeDeathIndicator=false;
            var incomeChangeSelectedIndicator=false;
            if((active == undefined || active == null || active == true) && deathIndicator==false ){
              activeDeathIndicator=true;
            }else{
              activeDeathIndicator=false;
            }
            if(eventInfo.eventSubCategory==='INCOME'){
              //check if change in income has been changed for this house hold or not?

                var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
                for(var i=0;i<householdArray.length;i++){
                  if(householdArray[i].personId === householdMember.personId){
                    if(eventInfoArrayTemp[i].incomeChangeIndicator){
                      incomeChangeSelectedIndicator=true;
                    }
                    break;
                  }
                }
               return (activeDeathIndicator && incomeChangeSelectedIndicator);
            }
            return activeDeathIndicator;
          };

        //numofdep-remove.html..code for death OR active indicator
        $scope.activeDeathStatusChange = function (householdMember) {
           if(eventInfo.eventSubCategory==  UPDATE_DEPENDENTS[2].name ){ //'death'
             householdMember.socialSecurityCard.deathIndicator=true;
          }else if(eventInfo.eventSubCategory== UPDATE_DEPENDENTS[1].name){ // 'remove'
            householdMember.active=false;
          }
        };

        $scope.showAdditionalJobs=function(household){
          var showAddtionalJobs = false;
          for(var i=1;i< household.detailedIncome.jobIncome.length;i++){
            if(household.detailedIncome.jobIncome[i].incomeAmountBeforeTaxes > 0){
              showAddtionalJobs= true;
              break;
            }
          }
          return showAddtionalJobs;
        };


        $scope.getRelationShipWithPrimaryHousehold= function(personId){
        	var relationShipCode = $scope.getRelationShipCodeWithPrimaryHousehold(personId);
        	if(relationShipCode ==""){
        		return "N/A";
        	}
        	return $scope.getRelation(relationShipCode);
        }

        $scope.getRelationShipCodeWithPrimaryHousehold= function(personId){
          return $scope.getRelationShipCodeBetweenHousehold(personId,1);
        };

        //wrapper over getRelationShipCodeBetweenHousehold to get relatiohship name from relationShipCode.
        $scope.getRelationShipBetweenHousehold = function(fromHouseholdPersonId,toHousholdPersonId){
        	var relationShipCode = $scope.getRelationShipCodeBetweenHousehold(fromHouseholdPersonId,toHousholdPersonId);
        	if(relationShipCode == ""){
        		return "N/A";
        	}
        	return $scope.getRelation(relationShipCode);
        };
        //to relationship code between household
        $scope.getRelationShipCodeBetweenHousehold = function(fromHouseholdPersonId,toHousholdPersonId){
            var bloodRelationship = findHouseholdByPersonId(1).bloodRelationship;
            for(var i=0; i<bloodRelationship.length;i++){
              if(bloodRelationship[i].relatedPersonId == toHousholdPersonId && bloodRelationship[i].individualPersonId == fromHouseholdPersonId){
                return bloodRelationship[i].relation;
              }
            }
            return "";
        };




        $scope.getRelation=function(relationShipCode){
          if (relationShipCode=="01"){
            return "Spouse";
          } else if(relationShipCode=="03"){
            return "Parent of child";
          }else if(relationShipCode=="03a"){
            return "Parent of adopted child";
          }else if(relationShipCode=="03f"){
            return "Parent of foster child";
          }else if(relationShipCode=="03w"){
            return "Parent/Caretaker of the ward";
          }else if(relationShipCode=="04"){
            return "Grandparent";
          }else if(relationShipCode=="05"){
            return "Grandchild";
          }else if(relationShipCode=="06"){
            return "Uncle/aunt";
          }else if(relationShipCode=="07"){
            return "Nephew/niece";
          }else if(relationShipCode=="08"){
            return "First cousin";
          }else if(relationShipCode=="09"){
            return "Adopted child (son or daughter)";
          }else if(relationShipCode=="10"){
            return "Foster child (foster son or foster daughter)";
          }else if(relationShipCode=="11"){
            return "Son-in-law or daughter-in-law";
          }else if(relationShipCode=="12"){
            return "Brother-in-law or sister-in-law";
          }else if(relationShipCode=="13"){
            return "Mother-in-law or father-in law";
          }else if(relationShipCode=="14"){
            return "Sibling (brother or sister)";
          }else if(relationShipCode=="15"){
            return "Ward of guardian";
          }else if(relationShipCode=="15c"){
            return "Ward of court-appointed guardian";
          }else if(relationShipCode=="15p"){
            return "Ward of parent/caretaker";
          }else if(relationShipCode=="16"){
            return "Stepparent (stepfather or stepmother)";
          }else if(relationShipCode=="17"){
            return "Stepchild (stepson or stepdaughter)";
          }else if(relationShipCode=="18"){
            return "Self";
          }else if(relationShipCode=="19"){
            return "Child (son or daughter)";
          }else if(relationShipCode=="23"){
            return "Sponsored dependent";
          }else if(relationShipCode=="24"){
            return "Dependent of a minor dependent";
          }else if(relationShipCode=="25"){
            return "Former spouse";
          }else if(relationShipCode=="26"){
            return "Guardian";
          }else if(relationShipCode=="31"){
            return "Court-appointed guardian";
          }else if(relationShipCode=="38"){
            return "Collateral dependent";
          }else if(relationShipCode=="53"){
            return "Domestic partner";
          }else if(relationShipCode=="60"){
            return "Annuitant";
          }else if(relationShipCode=="D2"){
            return "Trustee";
          }else if(relationShipCode=="G8"){
            return "Unspecified relationship";
          }else if(relationShipCode=="G9"){
            return "Unspecified relative";
          }else if(relationShipCode=="03-53"){
            return "Parent's domestic partner";
          }else if(relationShipCode=="53-19"){
              return "Child of domestic partner";
          }
        };

       }]); // End of Controller 2


       function addEventInfo(eventInfoParam){
    	   var eventInfoAlreadyExist = false;
    	   if(eventInfoParam.eventCategory == "" && eventInfoParam.eventSubCategory == "" ){
    		   return;
    	   }
    	   for(var i = 0; i<eventInfoArray.length;i++){
    		   if(eventInfoArray[i].eventCategory == eventInfoParam.eventCategory && eventInfoArray[i].eventSubCategory == eventInfoParam.eventSubCategory){
    			   eventInfoAlreadyExist = true;
    			   break;
    		   }
    	   }
    	   if(!eventInfoAlreadyExist){
    		   eventInfoArray.push(eventInfoParam);
    	   }
       }

	  function addTempEventInfo(){
		  var tempEventInfo = {
                changeInAddressDate:'',
                citizenShipChangeDate:'',
                tribeChangeDate:'',
                changeInLossDate:'',
                nameChangeDate:'',
                incomeChangeDate:'',
                incomeChangeIndicator:'',
                incomeTypes:[],
                hasOtherIncomeTypes:'No',
                lossEventSubCategory:'',
                changeInGainDate:'',
                gainEventSubCategory:'',
                dateOfChange:'',
                lawfulChangeDate:'',
                removeDepEventName:''
              };
            eventInfoArrayTemp.push(tempEventInfo);
	  }
      //for padding of number with leading zeros
      Number.prototype.pad = function(size) {
         var s = String(this);
         if(typeof(size) !== "number"){size = 2;}

         while (s.length < size) {s = "0" + s;}
             return s;
      };


      function getDateInMMDDYYYY(val){

      if(val !=undefined && val != "" && val.length>20 ){
            return formateDateForUI(val);
        }

        if(val !=undefined && val != "" && val.length>10 ){
          var year = val.substr(8,4);
            var day = val.substr(4,2);
            var month = ($.inArray(val.substr(0,3),monthNames)+1).pad(2);
            return month+"/"+day+"/"+year;
        }

        return val;

      }

      function formateDateForUI(dt){
        var formattedDate = new Date(dt);
        var dd = formattedDate.getDate();
        var mm =  formattedDate.getMonth();
        mm += 1;  // JavaScript months are 0-11
        var yyyy = formattedDate.getFullYear();
        if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
        formatteddate = mm + "/" + dd + "/" + yyyy;
        return formatteddate;

      }


      //date formatter for non-input elements
      newMexico.directive('dateFormatterNonInput', function() {
         return {
             restrict: 'A',
             link: function(scope, element, attrs, ngModelCtrl) {
              var dateInMMDDYYYY =  getDateInMMDDYYYY(scope.$eval(attrs.date));
              $(element).html(dateInMMDDYYYY);
          }
      }});
      //date formatter for non-input elements
      newMexico.directive('dateFormatter', function() {
         return {
             restrict: 'A',
             link: function(scope, element, attrs,ngModelCtrl) {
			 var dateInMMDDYYYY =  getDateInMMDDYYYY(scope.$eval(attrs.date));
			 $(element).val(dateInMMDDYYYY);
			 
			}
      }});
      
    //fix angular ui-mask copying placeholder into value
      newMexico.directive('initMask', function() {
         return {
             restrict: 'A',
             require: 'ngModel',
             link: function(scope, element, attrs,ngModelCtrl) {
            	 ngModelCtrl.$formatters.unshift(function(inputValue){
                     if(inputValue  == '' || inputValue  == null || inputValue  == undefined)
                       return '';
                     else
                       return inputValue;
                    });
                
            	/* var inputValue  = scope.$eval(attrs.ngModel);
			 	if(inputValue  == '' || inputValue  == null || inputValue  == undefined){
			 		//set view value to blank
			 		//ngModelCtrl.$setViewValue('');
			 		$(element).val('');
			 	} */
             	
             }
      }});

      //format zero as blank
      newMexico.directive('formatZeroAsBlank', function() {
         return {
             restrict: 'A',
             link: function(scope, element, attrs,ngModelCtrl) {
            	 var inputValue  = scope.$eval(attrs.ngModel);
				// this next if is necessary for when using ng-required on your input.
				if (inputValue == undefined || inputValue==''){
				  return;
				}
				if(inputValue != '0'){
					return;
				}
				if(inputValue == '0'){
					scope.$eval(attrs.ngModel+'=""');
				}
				
				
		}}});
      //date formatter for non-input elements
      newMexico.directive('ssnFormatterNonInput', function() {
         return {
             restrict: 'A',
             link: function(scope, element, attrs, ngModelCtrl) {
              var ssn = scope.$eval(attrs.ssn);
              var ssnLastFourDigits = ssn.substring(ssn.length - 4);
              $(element).html("***-**-"+ssnLastFourDigits);
          }
      }});

      //date formatter for non-input elements
      newMexico.directive('selectLabelModel', function() {
         return {
           require: 'ngModel',
             restrict: 'A',
             link: function(scope, element, attrs, ngModelCtrl) {
             element.bind('change',function(){
              var selectedLabel =element.find("option:selected").text();
                scope.$eval(attrs.selectLabelModel+"=\""+selectedLabel+"\"");
            });
           }
      }});


      var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                         "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

       // Directive for Datepicker
       newMexico.directive('datetimez', function() {
          return {
              restrict: 'A',
              require : 'ngModel',
              link: function(scope, element, attrs, ngModelCtrl) {

               element.datetimepicker({
                 dateFormat:'dd-MM-yyyy',
                 language: 'en',
                 pickTime: false
                 //commented date range as Date of birth can be less than 2013
                 //startDate: '01-11-2013',      // set a minimum date
                 //endDate: '01-11-2030'          // set a maximum date
                }).on('changeDate', function(e) {
                //concat prpoerties of date object as per required format Mar 13, 2014 00:00:00 AM
                if(e.localDate == null || e.localDate == undefined){
                  ngModelCtrl.$setViewValue('');
                  scope.$apply();
                  return;
                }
				formatDate(this,ngModelCtrl,e.localDate,scope);
				$(element).trigger('change');
			});

               $(element).change(function(){
            	   var date =  $(element).data().datetimepicker.getLocalDate();
            	   var textBoxVal =  $(element).find('input:first').val();
            	   if(textBoxVal == ""){
            		   date = null;
           		   }
            	   formatDate(this,ngModelCtrl,date,scope);
           	  	});

               $(element).find('input:first').mask('00/00/0000');
               
               	/* HIX-59762 - Issue6 The calendar wizard shouldn't move when scrolled down fix  */
               	/* HIX-63595 - 2.4 NF: Other event: Calendar wizard should be hidden when page is scrolling. */
               	 $(window).scroll(function(){
               			var resetToBlank = false;
               			 var textBox =  $(element).find('input:first')
			    		var calOnUI =  $('.bootstrap-datetimepicker-widget');
			    		if(calOnUI.length > 0  && $(calOnUI[0]).css('display') != 'none'){
			    			
			    			if($(textBox).val()== ''){
			    				resetToBlank = true;
			    			}
			    			$(element).datetimepicker('hide');
			    			if(resetToBlank){
			    				$(textBox).val('');
			    			}
			    			$(element).trigger('change');
			    			
			    		}
			    	});
			   	//}
               	
               	/* HIX-59762 - Issue5: Checking if DOB range set on element for 100 days range. Datepicker required check-befor eattribue to work this code*/
               	var strDobRange = $(element).attr('dobrange');
               	
               	if(strDobRange != 'undefined' && strDobRange != null){

                   	if(strDobRange.toUpperCase() == 'YES'){
                   		
           			    var today= new Date();
           			    var yyyy = today.getFullYear() - 100;
           			    var backDate= new Date( 01+"/"+01+"/"+yyyy)

           			    var difference = today.getTime() - backDate.getTime();
           			    var daysDifference = Math.floor(difference/1000/60/60/24);

           			    $(element).attr('check-before', daysDifference);
                   	}
               	}
               	/* HIX-59762 */
    			
              }//end of link funcion
          };//end of return statement
      }); // End of Datepicker directive


      function formatDate(element,ngModelCtrl,date,scope){
    	  var ngModel = $(element).attr('ng-model');
    	  if(date == null || date == undefined){
    		  ngModelCtrl.$setViewValue('');
    		  return;
    	  }
          if(ngModel.indexOf('eventInfo') == -1 && ngModel.indexOf('currentEvent') == -1){
            //ngModelCtrl.$setViewValue((e.date.getMonth()+1).pad(2)+"/"+e.date.getUTCDate().pad(2)+"/"+e.date.getFullYear());
            ngModelCtrl.$setViewValue(monthNames[date.getMonth()]+" "+date.getDate().pad(2)+", "+date.getFullYear()+" 12:00:00 PM" );

          }else{
            ngModelCtrl.$setViewValue((date.getMonth()+1).pad(2)+"/"+date.getDate().pad(2)+"/"+date.getFullYear());
          }
          scope.$apply();
     }

      /*
      Directive for checking before date START
      */
       newMexico.directive('checkBefore', function() {
         return {
            restrict: 'A',
                require: 'ngModel',
                link: function (scope, elm, attrs, ctrl) {
                    var validateDateRange = function (inputValue) {
                      var isValid =true;
                      var required=scope.$eval($(elm).attr("required"));
                      var ngRequired=scope.$eval($(elm).attr("ng-required"));
                      var validate=false;

                      var isCorrectInput= true;
                      try{
                         var parsedDate=Date.parse(inputValue);
                         if(isNaN(parsedDate)){
                           isCorrectInput=false;
                         }
                      }catch(err){
                        isCorrectInput= false;
                      }

                      if( (typeof required  === 'undefined') &&  (typeof ngRequired  === 'undefined')&& ( isCorrectInput  ) ){
                        validate=true;
                      }else if(   ((required ===true) || (ngRequired === true)) && isCorrectInput  ){
                        validate=true;
                      }

                      if(validate){
                        var howManyDaysBefore=parseInt( $(elm).attr("check-before"));

                         var differenceCount=      new Date() - Date.parse(inputValue);
                         differenceCount = Math.floor(differenceCount / (1000  * 60 * 60 * 24));
                         if(differenceCount <= howManyDaysBefore){
                           isValid =true;
                         }else{
                           isValid =false;
                         }
                      }
                        ctrl.$setValidity('checkBefore', isValid);
                        return inputValue;
                    };

                    ctrl.$parsers.unshift(validateDateRange);
                    ctrl.$formatters.push(validateDateRange);
                    attrs.$observe('checkBefore', function () {
                        validateDateRange(ctrl.$viewValue);
                    });

                }
            };
       });
       /*
        Directive for checking before date END
       */
       
       
       
       /*
       Directive for checking after date START
       */
       
       
        newMexico.directive('ageOver16', function() {
          return {
             restrict: 'A',
                 require: 'ngModel',
                 link: function (scope, elm, attrs, ctrl) {
                	 var result = eval(attrs.ageOver16);
                	 var isRelationshipDropdown=attrs.isRelationshipDropdown;
                     var multipleSpouseCheck = attrs.multipleSpouseCheck;
                     var fromHousehold=scope.$eval(attrs.fromHousehold);
                     var toHousehold=scope.$eval(attrs.toHousehold);
                	   var validateDateRange = function (inputValue) {
                       var isValid =true;
                       var required=scope.$eval($(elm).attr("required"));
                       var ngRequired=scope.$eval($(elm).attr("ng-required"));
                      
                      
                       var validate=false;

                       if( (typeof required  === 'undefined') &&  (typeof ngRequired  === 'undefined')  ){
                         validate=true;
                       }else if(   ((required ===true) || (ngRequired === true)) ){
                         validate=true;
                       }
                       var relationshipCode = '';
                       var isHouseholdAgeOver16 = true;
                       var isSingleSpouse = true;
                       var isRelatedHouseholdAgeOver16 = true;
                       if(validate && inputValue!=undefined && inputValue != null && inputValue!=''){
                    	  if(isRelationshipDropdown !=undefined && isRelationshipDropdown=='Y'){
                    		  //get value from dropdown
                    		  relationshipCode = inputValue;
                    	  }else{	
                    	  	relationshipCode = scope.getRelationShipCodeBetweenHousehold(fromHousehold.personId,toHousehold.personId);
                    	  }
                         if(relationshipCode=='01'){
                        	 if(result){
	                        	 isHouseholdAgeOver16 = scope.checkIfAgeOver16(fromHousehold); 
	                        	 isRelatedHouseholdAgeOver16 = scope.checkIfAgeOver16(toHousehold); 
	                        	 isValid = (isHouseholdAgeOver16 && isRelatedHouseholdAgeOver16);
                        	 }
                        	 if(isRelationshipDropdown=='Y' && multipleSpouseCheck=='Y'){
                        		 var bloodRelationship = findHouseholdByPersonId(1).bloodRelationship;
                                 for(var i=0; i<bloodRelationship.length;i++){
                                   if((bloodRelationship[i].relatedPersonId == fromHousehold.personId || bloodRelationship[i].individualPersonId == fromHousehold.personId || bloodRelationship[i].relatedPersonId == toHousehold.personId || bloodRelationship[i].individualPersonId == toHousehold.personId  ) && bloodRelationship[i].relation == '01'){
                                	  if( ! ((bloodRelationship[i].relatedPersonId == fromHousehold.personId && bloodRelationship[i].individualPersonId == toHousehold.personId) || (bloodRelationship[i].relatedPersonId ==  toHousehold.personId && bloodRelationship[i].individualPersonId == fromHousehold.personId))  ){
                                		  	isSingleSpouse =false;
                                   	   		break; 
                                	  }
                                   }
                                 }
                        	 }
                         }
                            
                       }
                       ctrl.$setValidity('isHouseholdAgeOver16', isHouseholdAgeOver16);
                       ctrl.$setValidity('isRelatedHouseholdAgeOver16', isRelatedHouseholdAgeOver16);
                       ctrl.$setValidity('ageOver16', isValid);
                       ctrl.$setValidity('singleSpouse',isSingleSpouse);
                       return inputValue;
                     };

                     ctrl.$parsers.push(validateDateRange);
                     ctrl.$formatters.unshift(validateDateRange);
                     
                     if(isRelationshipDropdown=='Y' && multipleSpouseCheck=='Y'){
                    	 scope.$watch("bloodRelationshipModel", function(newArr, oldArr) {
                             var inputVal=scope.getRelationShipCodeBetweenHousehold(fromHousehold.personId,toHousehold.personId);
                             validateDateRange(inputVal);
                           },true); 
                     }
                   
                 }
             };
        });
       
       /*
        Directive for checking after date START
        */
         newMexico.directive('checkAfter', function() {
           return {
              restrict: 'A',
                  require: 'ngModel',
                  link: function (scope, elm, attrs, ctrl) {
                      var validateDateRange = function (inputValue) {
                        var isValid =true;
                        var required=scope.$eval($(elm).attr("required"));
                        var ngRequired=scope.$eval($(elm).attr("ng-required"));
                        var validate=false;

                        var isCorrectInput= true;
                        try{
                           var parsedDate=Date.parse(inputValue);
                           if(isNaN(parsedDate)){
                             isCorrectInput=false;
                           }
                        }catch(err){
                          isCorrectInput= false;
                        }

                        if( (typeof required  === 'undefined') &&  (typeof ngRequired  === 'undefined')&& ( isCorrectInput  ) ){
                          validate=true;
                        }else if(   ((required ===true) || (ngRequired === true)) && isCorrectInput  ){
                          validate=true;
                        }
                        if(validate){
                          var howManyDaysAfter=parseInt( $(elm).attr("check-after"));

                           var differenceCount=  Date.parse(inputValue)-new Date();
                           differenceCount = Math.floor(differenceCount / (1000  * 60 * 60 * 24));
                           if(differenceCount < howManyDaysAfter){
                             isValid =true;
                           }else{
                             isValid =false;
                           }
                        }
                          ctrl.$setValidity('checkAfter', isValid);
                          return inputValue;
                      };

                      ctrl.$parsers.unshift(validateDateRange);
                      ctrl.$formatters.push(validateDateRange);
                      attrs.$observe('checkAfter', function () {
                          validateDateRange(ctrl.$viewValue);
                      });

                  }
              };
         });
         /*
          Directive for checking after date END
         */

       newMexico.controller('settings', ['$scope', function($scope) {
         $scope.contacts = [
           {type: 'job', value: ''},
         ];
          $scope.addContact = function() {
            this.contacts.push({type: 'job'});
          };
          $scope.removeContact = function(contactToRemove) {
           var index = this.contacts.indexOf(contactToRemove);
            this.contacts.splice(index, 1);
          };
       }]); // End of special controller to add addition jobs in Job Income partial

       var dependentAdddedAtCounter = 0;

       newMexico.controller('maritalStatusDetail', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
        //if event it Number of Dependent then add a Dependent household
		$scope.calOpened = false ;	
			$scope.eventInfo= eventInfo;
			$scope.eventForSpouseOrDependent = '';
		$scope.primaryHousehold = findHouseholdByPersonId(1);	
        if( $scope.selectedEvents[lifeEventCounter].eventCode== UPDATE_DEPENDENTS[0].category){ //'UPDATE_DEPENDENTS'
        	$scope.eventForSpouseOrDependent = "Dependent";
        	//add a household member
           var currentPartial = myService.getPartialCount();
           var newHouseHold = null;
          if(!dependentForHouseholdAdded){
        	 
        	  if(dependentStack.length == 0 && currentPartial > dependentAdddedAtCounter){
	            newHouseHold=getDefaultHousehold();
	
	            var curEvent=myService.getCurrentEvent();
	            addChangedApplicants(newHouseHold.personId,curEvent.dateOfChange);
	            
	            //add self relationship
	            addRelationship(newHouseHold.personId,newHouseHold.personId,'18');
	            //If event is BIRT event then give date of birth as event date.
	            if($scope.eventInfo.eventSubCategory ==='BIRTH'){
	            	var dobLocal=new Date(curEvent.dateOfChange);
	            	newHouseHold.dateOfBirth=monthNames[dobLocal.getMonth()]+" "+dobLocal.getDate().pad(2)+", "+dobLocal.getFullYear()+" 12:00:00 PM"
	            	$scope.dummyBirthDate=curEvent.dateOfChange;
	            }
        	}else if(!$rootScope.isBackFromAdressPage){
        		var dependentRecord = dependentStack.pop();
        		newHouseHold = dependentRecord.household;
        		var relationship = dependentRecord.relationship;
        		var primaryHousehold = findHouseholdByPersonId(1);
        		for(var i=0;i<relationship.length;i++){
        			primaryHousehold.bloodRelationship.push(relationship[i]);
        		}
        		
        	}
        	if(newHouseHold != null){
	            ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length]=newHouseHold;
	            dependentForHouseholdAdded = true;
	            dependentAdddedAtCounter = myService.getPartialCount();
        	}
          }else{//If dependent is already added then 
        	  if($scope.eventInfo.eventSubCategory ==='BIRTH'){
        		  $scope.dummyBirthDate=  $scope.eventInfo.changedApplicants[0].eventDate;
        	  }
          }
        }else{
        	$scope.eventForSpouseOrDependent = "Spouse";
        }
		
        $scope.updateRelation=function( personId  ) {
          updateRelationship(newHouseHold.personId,$scope.changedRelationShip);
        };
		
        $scope.removeHouseholdIfRequired = function(){
        	
        	if($scope.selectedEvents[lifeEventCounter].eventCode== UPDATE_DEPENDENTS[0].category && (myService.getPartialCount()-1) % (dependentFlowLength) == 0 && myService.getPartialCount() >= dependentFlowLength){
        		       		
        		//remove from current household
        		var lastHousehold = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length -1];
        		var relationship = [];
        		//push to stack
        		
                //if(dependentForHouseholdAdded==true){
                  ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
                  //dependentForHouseholdAdded =false;
                  //dependentAdddedAtCounter=0;
                  //remove relationship
         		  var primaryHousehold = findHouseholdByPersonId(1);
                  var bloodRelationship = primaryHousehold.bloodRelationship ;
                  
                  for(var i=0; i<bloodRelationship.length;i++){
                     if(bloodRelationship[i].relatedPersonId == lastHousehold.personId ||  bloodRelationship[i].individualPersonId == lastHousehold.personId){
                    	 relationship.push(bloodRelationship[i]);
                       	 bloodRelationship.splice(i--, 1);
                     }
                   }
                  //push to array
                  dependentStack.push({household:lastHousehold,relationship:relationship});
                //}
           }
        };
        $scope.openCal= function($event){
		 	$event.preventDefault();
		    $event.stopPropagation();
		    $scope.calOpened = true;
		};

       }]);
       
       
       function findHouseholdByPersonIdInOldData(personId){
    	   var householdArr = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	   for(var i=0; i<householdArr.length; i++){
    		   if(householdArr[i].personId == personId){
    			   return householdArr[i]; 
    		   }
    	   }
    	   return null;
       }
       function findHouseholdByPersonId(personId){
    	   var householdArr = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	   for(var i=0; i<householdArr.length; i++){
    		   if(householdArr[i].personId == personId){
    			   return householdArr[i]; 
    		   }
    	   }
    	   return null;
       }
       
       function getIndexByHouseholdByPersonId(personId){
    	   var householdArr = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	   for(var i=0; i<householdArr.length; i++){
    		   if(householdArr[i].personId == personId){
    			   return i; 
    		   }
    	   }
    	   return null;
       }

       //New controller to get Marital Status subtypes
       newMexico.controller('maritalStatus', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
          var bloodrelationshipofprimary = findHouseholdByPersonIdInOldData(1).bloodRelationship;
          var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
          for (var i = 0 ; i < bloodrelationshipofprimary.length; i++) {
            if (bloodrelationshipofprimary[i].relation === '01') {
              $scope.marriedIndicator = true;
              break;
            }
          };
          $scope.modalShown = false;
          $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
        	  $scope.modalEventType ="marital"
            $scope.modalHouseholdMember=angular.copy(houseHoldFromParent);
            $scope.modalShown = !$scope.modalShown;
            $scope.originalIndex=originalIndex;
          };
          $scope.closeModal = function() {
              $scope.modalShown = false;
          };
          $scope.modelOK = function(householdCloned) {
        	householdArray[$scope.originalIndex] = householdCloned;
          	$scope.closeModal();
          };
          $scope.getOriginalIndex=function(content,array){
              return  $.inArray(content,array);
          };
         $scope.maritaStatusSubCategory= [ {label:"<spring:message code='label.lce.marriageorcivilunion'/>",value:"MARRIAGE"},
                                           {label:"<spring:message code='label.lce.divorceorlegalseperation'/>",value:"DIVORCE_OR_ANULLMENT"},
                                           {label:"<spring:message code='label.lce.deathofspouse'/>",value:"DEATH_OF_SPOUSE"},
                                           // {label:"Legal Separation",value:"LEGAL_SEPERATION"}
                                           ];

         var marriedgeEventArrayIndex=0;
         $.each($scope.maritaStatusSubCategory, function(key,value) {
           value.value=MARITAL_STATUS[marriedgeEventArrayIndex].name;
           value.label=MARITAL_STATUS[marriedgeEventArrayIndex].label;
           marriedgeEventArrayIndex++;
         });

         $scope.editable=false;
         $scope.removeeditable=function() {
           $scope.editable=false
         };




         $scope.$watch("eventInfo.eventSubCategory",function( newCategory ) {
           var primaryHousehold = findHouseholdByPersonId(1);
             if(newCategory=='MARRIAGE' || newCategory=='CIVIL_UNION'){//if selected marriage then add new household member.
               if(maritalHouseholdAdded==false){
                 var defaultHousehold=getDefaultHousehold();
                 ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length]=defaultHousehold;

                // add self relationship for new member
                addRelationship(defaultHousehold.personId,defaultHousehold.personId,"18");
                //add spouse relationship for marital status
             	addRelationship(defaultHousehold.personId,1,"01");
             	 //add reverse relationship for marital status
             	addRelationship(1,defaultHousehold.personId,getReverseRelationship("01"));

                 maritalHouseholdAdded=true;

                 var curEvent=myService.getCurrentEvent();

                addChangedApplicants(defaultHousehold.personId,curEvent.dateOfChange);

               }
             }else{//In other cases remove household member if newly added
               removeHousehold();
             }
             //TODO:set other values to default.
          }
         );//watch ended
        //This function is being called from marital status next button
         $scope.maritalStatusNext=function( newDate ) {
           //get all added applicants and set there date to newDate
           for(var i=0;i<eventInfo.changedApplicants.length;i++){
             eventInfo.changedApplicants[i].eventDate=newDate;
           }
         };


         $scope.filterNewlyAddedHousehold=function(household) {

           //if new houlhold is added then ignore in the list for maritalstaus page only.
          if(maritalHouseholdAdded){
            for(var i=0;i<eventInfo.changedApplicants.length;i++){
                 if(eventInfo.eventSubCategory == "MARRIAGE" && eventInfo.changedApplicants[i].personId==household.personId){
                 return false;
               }
            }
          }
          return true;
         }

       }]);//End of "maritalStatus" controller.

       //add RelationShip
       function addRelationship(fromPersonId,relatedPersonId,relationShipCode){
         var primaryHousehold = findHouseholdByPersonId(1);
         var bloodRelationShip = {
              "individualPersonId":""+fromPersonId,
              "relatedPersonId": ""+relatedPersonId,
              "relation": ""+relationShipCode,
              "textDependency": null
        };
        primaryHousehold.bloodRelationship.push(bloodRelationShip);
      }
      //update RelationShip
       function updateRelationship(personId,relationShipCode){
         var primaryHousehold = findHouseholdByPersonId(1);
         var bloodRelationship =primaryHousehold.bloodRelationship;
         var loopIndex=0;
         for(loopIndex=0;loopIndex < bloodRelationship.length ;loopIndex++){
           if((bloodRelationship[loopIndex].individualPersonId === (""+personId+"")) && bloodRelationship[loopIndex].relatedPersonId === "1" ){
             bloodRelationship[loopIndex].relation=relationShipCode;
             break;
           }
         }
      }

       //remove RelationShip
       function removeRelationship(personId){
         var primaryHousehold = findHouseholdByPersonId(1);
         var bloodRelationship = primaryHousehold.bloodRelationship ;
         for(var i=0; i<bloodRelationship.length;i++){
            if(bloodRelationship[i].relatedPersonId == personId ||  bloodRelationship[i].individualPersonId == personId){
              bloodRelationship.splice(i--, 1);
            }
          }


      }

     //get Reverse Relationships
       function getReverseRelationship(rel){
       	var reversedRel = '';

       	switch(rel) {
       		case '01':
       	    	reversedRel = '01';
       	        break;
       		case '03':
       	    	reversedRel = '19';
       	        break;
       		case '03a':
       	    	reversedRel = '09';
       	        break;
       		case '03f':
       	    	reversedRel = '10';
       	        break;
       		case '03w':
       	    	reversedRel = '15';
       	        break;
       		case '04':
       	    	reversedRel = '05';
       	        break;
       	    case '05':
       	    	reversedRel = '04';
       	        break;
       	    case '06':
       	    	reversedRel = '07';
       	        break;
       	    case '07':
       	    	reversedRel = '06';
       	        break;
       	    case '08':
       	    	reversedRel = '08';
       	        break;
       	    case '09':
       	    	reversedRel = '03';
       	        break;
       	    case '10':
       	    	reversedRel = '03';
       	        break;
       	    case '11':
       	    	reversedRel = '13';
       	        break;
       	    case '12':
       	    	reversedRel = '12';
       	        break;
       	    case '13':
       	    	reversedRel = '11';
       	        break;
       	    case '14':
       	    	reversedRel = '14';
       	        break;
       	    case '15':
       	    	reversedRel = '26';
       	        break;
       	    case '15c':
       	    	reversedRel = '31';
       	        break;
       	    case '15p':
       	    	reversedRel = '03';
       	        break;
       	    case '16':
       	    	reversedRel = '17';
       	        break;
       	    case '17':
       	    	reversedRel = '16';
       	        break;
       	    case '18':
       	    	reversedRel = '18';
       	        break;
       	    case '19':
       	    	reversedRel = '03';
       	        break;
       	    case '25':
       	    	reversedRel = '25';
       	        break;
       	    case '26':
       	    	reversedRel = '15';
       	        break;
       	    case '31':
       	    	reversedRel = '15';
       	        break;
       	    case '53':
       	    	reversedRel = '53';
       	        break;
       	    case 'G8':
       	    	reversedRel = 'G8';
       	        break;
       	    case 'G9':
       	    	reversedRel = 'G9';
       	        break;
       	    case '03-53':
       	    	reversedRel = '53-19';
       	        break;
       	    case '53-19':
       	    	reversedRel = '03-53';
       	    	break;
       	    //following relationship does not have reversed mapping, need update from Abhinav
       	    case '23':
       	    	reversedRel = '23';
       	    	break;
       	    case '24':
       	    	reversedRel = '24';
       	    	break;
       	    case '38':
       	    	reversedRel = '38';
       	    	break;
       	    case '60':
       	    	reversedRel = '60';
       	    	break;
       	    case 'D2':
       	    	reversedRel = 'D2';
       	        break;
       }

       	return reversedRel;
       }

       function getDefatultJson() {
          var jsonData = {
              "applicantGuid": "",
              "name": {
                "firstName": "",
                "middleName": "",
                "lastName": "",
                "suffix": ""
              },
              "dateOfBirth": null,
              "householdContactIndicator": true,
              "applyingForCoverageIndicator": true,
              "under26Indicator": true,
              "householdContact": {
                "homeAddressIndicator": false,
                "homeAddress": {
                  "streetAddress1": "",
                  "streetAddress2": "",
                  "city": "",
                  "state": "",
                  "postalCode": "",
                  "county": "",
                  "primaryAddressCountyFipsCode": ""
                },
                "mailingAddressSameAsHomeAddressIndicator": true,
                "mailingAddress": {
                  "streetAddress1": "",
                  "streetAddress2": "",
                  "city": "",
                  "state": "",
                  "postalCode": "",
                  "county": ""
                },
                "phone": {
                  "phoneNumber": "",
                  "phoneExtension": "",
                  "phoneType": "CELL"
                },
                "otherPhone": {
                  "phoneNumber": "",
                  "phoneExtension": "",
                  "phoneType": "HOME"
                },
                "contactPreferences": {
                  "preferredSpokenLanguage": "",
                  "preferredWrittenLanguage": "",
                  "emailAddress": "",
                  "preferredContactMethod": ""
                },
                "stateResidencyProof": ""
              },
              "livesWithHouseholdContactIndicator": true,
              "taxFiler": {
                "liveWithSpouseIndicator": null,
                "spouseHouseholdMemberId": 0,
                "claimingDependantsOnFTRIndicator": null,
                "taxFilerDependants": [{
                  "dependantHouseholdMemeberId": 0
                }]
              },
              "taxFilerDependant": {
                "taxFilerDependantIndicator": null
              },
              "planToFileFTRIndicator": null,
              "planToFileFTRJontlyIndicator": null,
              "marriedIndicator": null,
              "gender": "",
              "tobaccoUserIndicator": false,
              "livesAtOtherAddressIndicator": null,
              "otherAddress": {
                "address": {
                  "streetAddress1": "",
                  "streetAddress2": "",
                  "city": "",
                  "state": "",
                  "postalCode": "",
                  "county": ""
                },
                "livingOutsideofStateTemporarilyIndicator": false,
                "livingOutsideofStateTemporarilyAddress": {
                  "streetAddress1": "",
                  "streetAddress2": "",
                  "city": "",
                  "state": "",
                  "postalCode": "",
                  "county": ""
                }
              },
              "specialEnrollmentPeriod": {
                "birthOrAdoptionEventIndicator": false
              },
              "socialSecurityCard": {
                "socialSecurityCardHolderIndicator": null,
                "socialSecurityNumber": "",
                "ssnManualVerificationIndicator": true,
                "nameSameOnSSNCardIndicator": null,
                "firstNameOnSSNCard": "",
                "middleNameOnSSNCard": "",
                "lastNameOnSSNCard": "",
                "suffixOnSSNCard": "",
                "deathIndicatorAsAttested": false,
                "deathIndicator": false,
                "deathManualVerificationIndicator": false,
                "useSelfAttestedDeathIndicator": true,
                "individualMandateExceptionIndicator": null,
                "individualManadateExceptionID": null
              },
              "specialCircumstances": {
                "americanIndianAlaskaNativeIndicator": false,
                "tribalManualVerificationIndicator": true,
                "pregnantIndicator": false,
                "numberBabiesExpectedInPregnancy": 0,
                "fosterChild": false,
                "everInFosterCareIndicator": false,
                "fosterCareState": "",
                "ageWhenLeftFosterCare": 50,
                "gettingHealthCareThroughStateMedicaidIndicator": true
              },
              "americanIndianAlaskaNative": {
                "memberOfFederallyRecognizedTribeIndicator": false,
                "state": "",
                "tribeName": ""
              },
              "citizenshipImmigrationStatus": {
                "citizenshipAsAttestedIndicator": false,
                "citizenshipStatusIndicator": null,
                "citizenshipManualVerificationStatus": null,
                "naturalizedCitizenshipIndicator": null,
                "naturalizationCertificateIndicator": null,
                "naturalizationCertificateAlienNumber": "",
                "naturalizationCertificateNaturalizationNumber": "",
                "eligibleImmigrationStatusIndicator": null,
                "livedIntheUSSince1996Indicator": null,
                "lawfulPresenceIndicator": true,
                "honorablyDischargedOrActiveDutyMilitaryMemberIndicator": null,
                "eligibleImmigrationDocumentSelected": "",
                "eligibleImmigrationDocumentType": [{
                  "I551Indicator": false,
                  "TemporaryI551StampIndicator": false,
                  "MachineReadableVisaIndicator": false,
                  "I766Indicator": false,
                  "I94Indicator": false,
                  "I94InPassportIndicator": false,
                  "I797Indicator": false,
                  "UnexpiredForeignPassportIndicator": false,
                  "I327Indicator": false,
                  "I571Indicator": false,
                  "I20Indicator": false,
                  "DS2019Indicator": false,
                  "OtherDocumentTypeIndicator": false,

                }],
                "otherImmigrationDocumentType": {
                  "ORRCertificationIndicator": false,
                  "ORREligibilityLetterIndicator": false,
                  "CubanHaitianEntrantIndicator": false,
                  "WithholdingOfRemovalIndicator": false,
                  "AmericanSamoanIndicator": false,
                  "StayOfRemovalIndicator": false
                },
                "citizenshipDocument": {
                  "alienNumber": "",
                  "i94Number": "",
                  "cardNumber": "",
                  "documentExpirationDate": null,
                  "visaNumber": "",
                  "SEVISId": "",
                  "foreignPassportOrDocumentNumber": "",
                  "foreignPassportCountryOfIssuance": "0",
                  "sevisId": "",
                  "documentDescription": "",
                  "nameSameOnDocumentIndicator": null,
                  "nameOnDocument": {
                    "firstName": "",
                    "middleName": "",
                    "lastName": "",
                    "suffix": ""
                  }
                },

              },
              "parentCaretakerRelatives": {
                "mainCaretakerOfChildIndicator": null,
                "personId": [],
                "anotherChildIndicator": false,
                "name": {
                  "firstName": "",
                  "middleName": "",
                  "lastName": "",
                  "suffix": ""
                },
                "dateOfBirth": null,
                "relationship": "",
                "liveWithAdoptiveParentsIndicator": true
              },
              "ethnicityAndRace": {
                "hispanicLatinoSpanishOriginIndicator": null,
                "ethnicity": [],
                "race": []
              },
              "expeditedIncome": {
                "irsReportedAnnualIncome": null,
                "irsIncomeManualVerificationIndicator": false,
                "expectedAnnualIncome": null,
                "expectedAnnualIncomeUnknownIndicator": null,
                "expectedIncomeVaration": null
              },
              "healthCoverage": {
                "haveBeenUninsuredInLast6MonthsIndicator": null,
                "isOfferedHealthCoverageThroughJobIndicator": null,
                "employerWillOfferInsuranceIndicator": null,
                "employerWillOfferInsuranceCoverageStartDate": null,
                "currentEmployer": [{
                  "employer": {
                    "phone": {
                      "phoneNumber": "",
                      "phoneExtension": "",
                      "phoneType": "HOME"
                    },
                    "employerContactPersonName": "",
                    "employerContactEmailAddress": ""
                  },
                  "currentEmployerInsurance": {
                    "isCurrentlyEnrolledInEmployerPlanIndicator": false,
                    "willBeEnrolledInEmployerPlanIndicator": false,
                    "willBeEnrolledInEmployerPlanDate": null,
                    "expectedChangesToEmployerCoverageIndicator": true,
                    "employerWillNotOfferCoverageIndicator": false,
                    "employerCoverageEndingDate": null,
                    "memberPlansToDropEmployerPlanIndicator": true,
                    "memberEmployerPlanEndingDate": null,
                    "employerWillOfferPlanIndicator": true,
                    "employerPlanStartDate": null,
                    "memberPlansToEnrollInEmployerPlanIndicator": true,
                    "memberEmployerPlanStartDate": null,
                    "currentLowestCostSelfOnlyPlanName": "",
                    "currentPlanMeetsMinimumStandardIndicator": false,
                    "comingLowestCostSelfOnlyPlanName": "",
                    "comingPlanMeetsMinimumStandardIndicator": false
                  }
                }],
                "currentlyEnrolledInCobraIndicator": null,
                "currentlyEnrolledInRetireePlanIndicator": null,
                "currentlyEnrolledInVeteransProgramIndicator": null,
                "currentlyEnrolledInNoneIndicator": null,
                "willBeEnrolledInCobraIndicator": null,
                "willBeEnrolledInRetireePlanIndicator": null,
                "willBeEnrolledInVeteransProgramIndicator": null,
                "willBeEnrolledInNoneIndicator": null,
                "hasPrimaryCarePhysicianIndicator": false,
                "primaryCarePhysicianName": "",
                "formerEmployer": [{
                  "employerName": "",
                  "address": {
                    "streetAddress1": "",
                    "streetAddress2": "",
                    "city": "",
                    "state": "",
                    "postalCode": ""
                  },
                  "phone": {
                    "phoneNumber": "",
                    "phoneExtension": "",
                    "phoneType": "CELL"
                  },
                  "employerIdentificationNumber": "",
                  "employerContactPersonName": "",
                  "employerContactPhone": {
                    "phoneNumber": "",
                    "phoneExtension": "",
                    "phoneType": "HOME"
                  },
                  "employerContactEmailAddress": ""
                }],
                "currentOtherInsurance": {
                  "currentOtherInsuranceSelected": "",
                  "otherStateOrFederalProgramType": "",
                  "otherStateOrFederalProgramName": "",

                },
                "currentlyHasHealthInsuranceIndicator": false,
                "otherInsuranceIndicator": null,
                "medicaidInsurance": {
                  "requestHelpPayingMedicalBillsLast3MonthsIndicator": false
                },
                "chpInsurance": {
                  "insuranceEndedDuringWaitingPeriodIndicator": false,
                  "reasonInsuranceEndedOther": ""
                }
              },
              "incarcerationStatus": {
                "incarcerationAsAttestedIndicator": false,
                "incarcerationStatusIndicator": false,
                "incarcerationPendingDispositionIndicator": false,
                "incarcerationManualVerificationIndicator": false,
                "useSelfAttestedIncarceration": false
              },
              "detailedIncome": {
                "jobIncomeIndicator": null,
                "jobIncome": [{
                  "employerName": "",
                  "incomeAmountBeforeTaxes": null,
                  "incomeFrequency": "",
                  "workHours": null
                }],
                "otherIncomeIndicator": null,
                "otherIncome": [{
                  "otherIncomeTypeDescription": "Canceled debts",
                  "incomeAmount": null,
                  "incomeFrequency": ""
                },
                {
                  "otherIncomeTypeDescription": "Court Awards",
                  "incomeAmount": null,
                  "incomeFrequency": ""
                },
                {
                  "otherIncomeTypeDescription": "Jury duty pay",
                  "incomeAmount": null,
                  "incomeFrequency": ""
                }],
                "capitalGainsIndicator": null,
                "capitalGains": {
                  "annualNetCapitalGains": null
                },
                "rentalRoyaltyIncomeIndicator": null,
                "rentalRoyaltyIncome": {
                  "netIncomeAmount": null,
                  "incomeFrequency": ""
                },
                "farmFishingIncomeIndictor": null,
                "farmFishingIncome": {
                  "netIncomeAmount": null,
                  "incomeFrequency": ""
                },
                "alimonyReceivedIndicator": null,
                "alimonyReceived": {
                  "amountReceived": null,
                  "incomeFrequency": ""
                },
                "selfEmploymentIncomeIndicator": null,
                "selfEmploymentIncome": {
                  "typeOfWork": "",
                  "monthlyNetIncome": null
                },
                "deductions": {
                  "deductionType": ["ALIMONY",
                  "STUDENT_LOAN_INTEREST",
                  "OTHER"],
                  "deductionAmount": null,
                  "deductionFrequency": "MONTHLY",
                  "alimonyDeductionAmount": null,
                  "alimonyDeductionFrequency": "",
                  "studentLoanDeductionAmount": null,
                  "studentLoanDeductionFrequency": "",
                  "otherDeductionAmount": null,
                  "otherDeductionFrequency": ""
                },
                "unemploymentBenefitIndicator": null,
                "unemploymentBenefit": {
                  "stateGovernmentName": "",
                  "benefitAmount": null,
                  "incomeFrequency": "",
                  "unemploymentDate": null
                },
                "retirementOrPensionIndicator": null,
                "retirementOrPension": {
                  "taxableAmount": null,
                  "incomeFrequency": ""
                },
                "socialSecurityBenefitIndicator": null,
                "socialSecurityBenefit": {
                  "benefitAmount": null,
                  "incomeFrequency": ""
                },
                "investmentIncomeIndicator": null,
                "investmentIncome": {
                  "incomeAmount": null,
                  "incomeFrequency": ""
                },
                "discrepancies": {
                  "hasChangedJobsIndicator": false,
                  "stopWorkingAtEmployerIndicator": null,
                  "didPersonEverWorkForEmployerIndicator": null,
                  "hasHoursDecreasedWithEmployerIndicator": null,
                  "hasWageOrSalaryBeenCutIndicator": null,
                  "explanationForJobIncomeDiscrepancy": "",
                  "seasonalWorkerIndicator": null,
                  "explanationForDependantDiscrepancy": "",
                  "otherAboveIncomeIncludingJointIncomeIndicator": null
                },
                "wageIncomeProof": "VERIFIED",
                "scholarshipIncomeProof": "VERIFIED",
                "dividendsIncomeProof": "VERIFIED",
                "taxableInterestIncomeProof": "VERIFIED",
                "annuityIncomeProof": "VERIFIED",
                "pensionIncomeProof": "VERIFIED",
                "royaltiesIncomeProof": "VERIFIED",
                "unemploymentCompensationIncomeProof": "VERIFIED",
                "foreignEarnedIncomeProof": "VERIFIED",
                "rentalRealEstateIncomeProof": "VERIFIED",
                "sellingBusinessPropertyIncomeProof": "VERIFIED",
                "farmIncomeProof": "VERIFIED",
                "partnershipAndSwarpIncomeIncomeProof": "VERIFIED",
                "businessIncomeProof": "VERIFIED",
                "childNaTribe": "VERIFIED",
                "taxExemptedIncomeProof": "VERIFIED",
                "socialSecurityBenefitProof": "VERIFIED",
                "selfEmploymentTaxProof": "VERIFIED",
                "studentLoanInterestProof": "VERIFIED",
                "receiveMAthrough1619": true,
                "receiveMAthroughSSI": true,
                "foreignEarnedIncome": {
                  "incomeAmount": null
                },
                "rentalRealEstateIncome": {
                  "incomeAmount": null
                },
                "sellingBusinessProperty": {
                  "incomeAmount": null
                },
                "farmIncom": {
                  "incomeAmount": null
                },
                "partnershipsCorporationsIncome": {
                  "incomeAmount": null
                },
                "businessIncome": {
                  "incomeAmount": null
                },
                "selfEmploymentTax": {
                  "incomeAmount": null
                },
                "studentLoanInterest": {
                  "incomeAmount": null
                }
              },
              "bloodRelationship": [{
                "individualPersonId": null,
                "relatedPersonId": null,
                "relation": "",
                "textDependency": null
              }],
              "migrantFarmWorker": true,
              "birthCertificateType": "NO_CRETIFICATE",
              "infantIndicator": false,
              "PIDIndicator": false,
              "PIDVerificationIndicator": false,
              "livingArrangementIndicator": false,
              "IPVIndicator": false,
              "strikerIndicator": false,
              "drugFellowIndicator": false,
              "SSIIndicator": false,
              "residencyVerificationIndicator": false,
              "disabilityIndicator": false,
              "shelterAndUtilityExpense": null,
              "dependentCareExpense": null,
              "childSupportExpense": null,
              "medicalExpense": null,
              "heatingCoolingindicator": false
            };
          return jsonData;

        }

       //Added method to get default household
       function getDefaultHousehold(){
          var defaultHousehold = getDefatultJson();
          var householdArr = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
          var calculatedPersonId=getNewPersonId(householdArr);
          defaultHousehold.personId=calculatedPersonId;
		  addTempEventInfo();
          return defaultHousehold;
        }
       
       function getNewPersonId(householdArr){
    	   var maxPersonId = householdArr[0].personId;
    	   for(var i=1; i<householdArr.length ;i++){
    		   if(maxPersonId<householdArr[i].personId){
					maxPersonId = householdArr[i].personId;
				}
    	   }
		   return maxPersonId+1;
	  }

		function updateNewlyAddedHouseholdToOriginalJson(modifiedHouseHoldArray,changedApplicants){
			var alreadyAdded = false;
			var i=0;
			var j=0;
			var k=0;
			var householdArray = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
			for(i=0;i<modifiedHouseHoldArray.length;i++){
				//match with changed applicants
				for(j=0;j < changedApplicants.length;j++){
					if(modifiedHouseHoldArray[i].personId == changedApplicants[j].personId){
						//check if its  there in cloned array?
							alreadyAdded = false;
							for(k=0;k < householdArray.length;k++){
								if(householdArray[k].personId === modifiedHouseHoldArray[i].personId){
									alreadyAdded = true;
									break;
								} 
							}
							if(alreadyAdded){
								householdArray[k]= angular.copy(modifiedHouseHoldArray[i]);
							}else{
								householdArray.push(angular.copy(modifiedHouseHoldArray[i]));
							}
						break;
					}
				}
			}
			
		}

       newMexico.controller('maritalStatusSSN', ['$scope', '$http','$location', '$timeout', 'myService', function($scope, $http, $location, $timeout, myService) {

         function trim(rawStr) {
                return rawStr.replace(/^\s+|\s+$/g,"");
          }

          function populateSSNFieldsFromJSON() {
            var socialSecurityNumber = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].socialSecurityCard.socialSecurityNumber;

            if(null != socialSecurityNumber && socialSecurityNumber != undefined  ) {
              var matches = socialSecurityNumber.match(/(\d{3})(\d{2})(\d{4})*/);

              if(null != matches && matches != undefined && matches.length > 0) {
                if(null != matches[1] && matches[1] != undefined) {
                  $("#ssn1").val("***");
                }

                if(null != matches[2] && matches[2] != undefined) {
                  $("#ssn2").val("**");
                }

                if(null != matches[3] && matches[3] != undefined) {
                  $("#ssn3").val(trim(matches[3]));
                }
              }
            }else {
              $("#ssn1").val("");
              $("#ssn2").val("");
              $("#ssn3").val("");
            }
          }
          /** Populate SSN fields from JSON on load.*/
          $timeout(populateSSNFieldsFromJSON, 0);

         } ] );//End of ssn controller

       //START Controller for marital-status-citizenship page
         newMexico.controller('maritalStatusCitizenship', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
              var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
              $scope.dummySevisIdA='';
              $scope.dummySevisIdB='';
              if(lastHouseholdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && lastHouseholdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'NaturalizationCertificate'){
                $scope.tempNaturalizedOrCOC=true;
                $scope.dummySevisIdA=lastHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId;
                lastHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId='';
              }else if(lastHouseholdMember.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && lastHouseholdMember.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'CitizenshipCertificate'){
                $scope.tempNaturalizedOrCOC=false;
                $scope.dummySevisIdB=lastHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId;
                lastHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId='';
              }

             $scope.showInvalidFields = function() {
               var error = $scope.formCitizenship.$error;
                angular.forEach(error.pattern, function(field){
                    if(field.$invalid){
                        var fieldName = field.$name;
                        alert(fieldName);
                    }
                });
                angular.forEach(error.required, function(field){
                    if(field.$invalid){
                        var fieldName = field.$name;
                        alert(fieldName);
                    }
                });
             };
			 $scope.resetDocumentTypes= function(selectedHouseHold){
 				var selectedType=selectedHouseHold.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected;
             	var documentTypeObject = selectedHouseHold.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0];
             	var alienNumber = 1;
             	var cardNumber = 2;
             	var expirationDate =4;
             	var ForeignPassportCountryofIssuance = 8;
             	var i94Number =16;
             	var passportNumber = 32;
             	var SEVISid = 64;
             	var visaNumber  =128;
             	var appliedNumber=0;
             	//Assign false to eligibleImmigrationDocumentType variables.
             	 for(var key in documentTypeObject){
                      //var attrName = key;
                      documentTypeObject[key]=false;
                      //var attrValue = ;
                  }
         	    	switch(selectedType) {
         	    		case "I551" :{ //permResident
             				appliedNumber= 7  ;
             				documentTypeObject.I551Indicator=true;
             				break;
             			}
 	            		case "TempI551" :{//temp
     	        			appliedNumber= 45  ;
     	        			documentTypeObject.TemporaryI551StampIndicator =true;
         	    			break;
         	    		}
             			case "MacReadI551" :{ //machineReadable
             				appliedNumber= 173  ;
             				documentTypeObject.MachineReadableVisaIndicator =true;
             				break;
             			}
             			case "I766" :{//employAuth
             				appliedNumber= 7  ;
             				documentTypeObject.I766Indicator =true;
             				break;
             			}
             			case "I94" :{//i94
             				appliedNumber= 84  ;
             				documentTypeObject.I94Indicator =true;
             				break;
             			}
             			case "I94UnexpForeignPassport" :{//    i94Unexpired
             				appliedNumber= 252  ;
             				documentTypeObject.I94InPassportIndicator =true;
             				break;
             			}
             			case "I797" :{ //noticeofaction
             				appliedNumber= 17  ;
             				documentTypeObject.I797Indicator =true;
             				selectedHouseHold.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator = false;
             				break;
             			}
             			case "UnexpForeignPassport" :{//UnexpForeignPassport
             				appliedNumber= 124  ;
             				documentTypeObject.UnexpiredForeignPassportIndicator =true;
             				break;
             			}
             			case "I327" :{// reentryPermit
             				appliedNumber= 5  ;
             				documentTypeObject.I327Indicator =true;
             				break;
             			}
             			case "I571" :{//  refugeeTravel
             				appliedNumber=  5 ;
             				documentTypeObject.I571Indicator =true;
             				break;
             			}
             			case "I20" :{//nonimmigrantStudent
             				appliedNumber= 124  ;
             				documentTypeObject.I20Indicator =true;
             				break;
             			}
             			case "DS2019" :{//exchangeVisitor
             				appliedNumber= 116 ;
             				documentTypeObject.DS2019Indicator =true;
             				break;
             			}
             		}//End switch
         	    	if( !((alienNumber & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = undefined;
         	    	}
         	    	if( !((cardNumber & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.cardNumber = undefined;
         	    	}
         	    	if( !((expirationDate & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = undefined;
         	    	}
         	    	if( !((ForeignPassportCountryofIssuance & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = undefined;
         	    	}
         	    	if( !((i94Number & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.i94Number = undefined;
         	    	}
         	    	if( !((passportNumber & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = undefined;
         	    	}
         	    	if( !((SEVISid & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = undefined;
         	    	}
         	    	if( !((visaNumber  & appliedNumber)>0)){
         	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = undefined;
         	    	}

             };
             $scope.updateNatuCertNumber=function(memberFromUI){
            	 
            	 if(memberFromUI.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && memberFromUI.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'NaturalizationCertificate'){
            		 memberFromUI.citizenshipImmigrationStatus.citizenshipDocument.SEVISId=$scope.dummySevisIdA;
                   }else if(memberFromUI.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && memberFromUI.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'CitizenshipCertificate'){
                	   memberFromUI.citizenshipImmigrationStatus.citizenshipDocument.SEVISId= $scope.dummySevisIdB;
                   }
             };
         }]);//END Controller for marital-status-citizenship page


        newMexico.controller('signAndSubmit', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
        	$rootScope.lceEvent = 'Sign & Submit';
        	$scope.householdMember = findHouseholdByPersonId(1); 
        	$scope.toggleModal = function(houseHoldFromParent,originalIndex) {
            $scope.modalHouseholdMember=houseHoldFromParent;
            $scope.modalShown = !$scope.modalShown;
            $scope.originalIndex=originalIndex;
          };
        	//identify events
        	identifyEvents();

        	$scope.resetNoOneIncarcerated = function(incarceration,householdMember){
            if(incarceration==true){
              householdMember.incarcerationStatus.incarcerationStatusIndicator=false;
              householdMember.incarcerationStatus.incarcerationPendingDispositionIndicator=false;
           }
          };

          $scope.resetDisposition = function(householdMember){
            if(householdMember.incarcerationStatus.incarcerationStatusIndicator==false){
              householdMember.incarcerationStatus.incarcerationPendingDispositionIndicator=false;
           }
          };

          $scope.validationESignName = function(householdMember){
            var isValid = false;
            if($scope.eSignName == '' || $scope.eSignName == undefined)
            {
              $scope.signAndSubmit.eSignName.$setValidity('validESignature', true);
              return;
            }
            var middleName = householdMember.name.middleName;
            //handle Json null 
            if(middleName == null){
            	middleName = "";
            }else if(middleName != ""){
            	middleName = middleName+" "; 
            }
            if($scope.eSignName.toUpperCase()== (householdMember.name.firstName+" "+middleName+householdMember.name.lastName).toUpperCase()){
              isValid=true;
           }
           $scope.signAndSubmit.eSignName.$setValidity('validESignature', isValid);
         };
		
        
         $scope.toggleNoChangePopup = function(){
        	 $scope.showNotificationForNoChange = !$scope.showNotificationForNoChange;
        }

        }]);


        newMexico.controller('maritalStatusSC', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {

          $scope.states= getStatesForTribe();

            var newHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];


            $scope.ajaxRequestForTribes =  function ajaxRequestForTribes(stateCode){
              $.ajax({
              type : "POST",
              url :  '<c:url value="/ssap/getAlaskaAmericanStatesTribe"/>?${df:csrfTokenParameter()}='+'<df:csrfToken plainToken="true"/>',
              data : {stateCode : stateCode},
              dataType:'json',
              success : function(response) {
                 $scope.tribes =  response;
                 $scope.$apply();
              },error : function(e) {
                $scope.tribes =  {};
                $scope.$apply();
              }
            });

            };

          $scope.loadTribes = function(stateCode){
             newHouseholdMember.americanIndianAlaskaNative.tribeName='';
             newHouseholdMember.americanIndianAlaskaNative.tribeFullName='';
             $scope.ajaxRequestForTribes(stateCode);
          };

        if(newHouseholdMember.americanIndianAlaskaNative.state != undefined && newHouseholdMember.americanIndianAlaskaNative.state !=''){
          $scope.ajaxRequestForTribes(newHouseholdMember.americanIndianAlaskaNative.state);
         }

          $scope.otherStateOrFederalProgramIndicator= function(){
            var checked = $("#otherStateOrFederalProgramIndicator").is(":checked");
            var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
            if(!checked){
              lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramType='';
              lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramName='';
            }
          };

          $scope.noneOfTheAbove = function(){
            var checked = $("#noneOfTheAbove").is(":checked");
            var lastHouseholdMember = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
            if(checked){
              lastHouseholdMember.currentOtherInsurance.medicareEligibleIndicator=false;
              lastHouseholdMember.currentOtherInsurance.tricareEligibleIndicator=false;
              lastHouseholdMember.currentOtherInsurance.peaceCorpsIndicator=false;
              lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramIndicator=false;
              lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramType='';
              lastHouseholdMember.currentOtherInsurance.otherStateOrFederalProgramName='';
            }

          };
         }]);

        newMexico.controller('bloodRelations', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
          $scope.toggleModalForAgeOver26 = function(houseHoldFromParent,originalIndex) {
            $scope.modalHouseholdMember=houseHoldFromParent;
            $scope.modalShown = !$scope.modalShown;
            $scope.originalIndex=originalIndex;
          };
        	var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
        	var primaryHouseholdObj=findHouseholdByPersonId(1);
        	var bloodRelationsArr =primaryHouseholdObj.bloodRelationship;
        	
        	//Add logic of newly added applicant will be added here if required.
        	$scope.newlyAddedApplicantPersonId = householdArray[householdArray.length - 1].personId;
        	$scope.newlyAddedHousehold = householdArray[householdArray.length - 1];
        	//disable primary applicant to spouse relationship as in case of Add a spouse - spouse will be default relationship
        	$scope.disablePrimaryToNewHouseholdRelationship = false;
        	var currentEvent = myService.getCurrentEvent();
        	if(currentEvent.eventCode == 'MARITAL_STATUS'){
        		$scope.disablePrimaryToNewHouseholdRelationship = true;
        	}
			
			//set PrimaryHousehold for Popup
			$scope.primaryHousehold = householdArray[0];
			//set ageOver26 array to blank
			$scope.ageOver26Array = [];

        	//find reverse relationship and return relationshipCode for dropdown
        	$scope.getRelationshipCodeForDropdown = function(fromPersonId,toPersonId){
        		var processedRelationshipCode = '';
        		var relationshipCode = $scope.getRelationShipCodeBetweenHousehold(fromPersonId,toPersonId);
        		if(relationshipCode == '03' || relationshipCode == '15'){
        			//get reverse relationship
        			var reverseRelationshipCode = $scope.getRelationShipCodeBetweenHousehold(toPersonId,fromPersonId);
        			//get relation based on its reversed relation
        			switch(reverseRelationshipCode){
	        			case'09' : 
	        				processedRelationshipCode = '03a';
	        				break;
	        			case'10' : 
	        				processedRelationshipCode = '03f';
	        				break;
	        			case'19' : 
	        				processedRelationshipCode = '03';
	        				break;
	        			case'26' : 
	        				processedRelationshipCode = '15';
	        				break;
	        			case'31' : 
	        				processedRelationshipCode = '15c';
	        				break;
	        			case'15' : 
	        				processedRelationshipCode = '03w';
	        				break;
	        			case'03' : 
	        				processedRelationshipCode = '15p';
	        				break;
	        			default : 
	        				processedRelationshipCode = relationshipCode;
	        				break;
        			}
        			
        		}else{
        			processedRelationshipCode  = relationshipCode;
        		}
        		return processedRelationshipCode;
        	};
        	
        	$scope.bloodRelationshipModel = new Array(householdArray.length);
        	for(var i = 0 ; i< $scope.bloodRelationshipModel.length; i++){
        		$scope.bloodRelationshipModel[i] = new Array(householdArray.length);
        		for(var j = 0 ; j< $scope.bloodRelationshipModel[i].length; j++){
        			$scope.bloodRelationshipModel[i][j] = $scope.getRelationshipCodeForDropdown(householdArray[i].personId,householdArray[j].personId);
        		}
        	}
        	
        	$scope.getReverseRelationsForDisplay = function(reverseRelationshipCode){
        		var processedRelationshipCode = '';
        		switch(reverseRelationshipCode){
    			case'09' : 
    				processedRelationshipCode = '03a';
    				break;
    			case'10' : 
    				processedRelationshipCode = '03f';
    				break;
    			case'19' : 
    				processedRelationshipCode = '03';
    				break;
    			case'26' : 
    				processedRelationshipCode = '15';
    				break;
    			case'31' : 
    				processedRelationshipCode = '15c';
    				break;
    			case'15' : 
    				processedRelationshipCode = '03w';
    				break;
    			case'03' : 
    				processedRelationshipCode = '15p';
    				break;
    			default : 
    				processedRelationshipCode = relationshipCode;
    				break;
				}
        		return processedRelationshipCode;
        	};
        	
        	
        	

        	$scope.updateRelationshipDetails = function(individualHousehold,relatedHousehold,relationshipCode) {
        		//find Relations between from household and last household already in array
        		var forwardRelationship = false;
        		var reverseRelationship = false;
        		for(var i = 0; i< bloodRelationsArr.length ; i++ ){
        			//check if forward relationship already added
        			if(bloodRelationsArr[i].individualPersonId == individualHousehold.personId && bloodRelationsArr[i].relatedPersonId == relatedHousehold.personId){
        				forwardRelationship = true;
        				//if already relationship added just update relationship code
        				bloodRelationsArr[i].relation = $scope.getRelationshipForJSON(relationshipCode);
        			}
        			//check if reverse relationship already added
        			if(bloodRelationsArr[i].relatedPersonId == individualHousehold.personId && bloodRelationsArr[i].individualPersonId == relatedHousehold.personId){
        				reverseRelationship = true;
        				//if already relationship added just update reverse relationship code
        				bloodRelationsArr[i].relation = getReverseRelationship(relationshipCode);
        			}
        		}
        		//if forward relationship not added
        		if(!forwardRelationship){
        			addRelationship(individualHousehold.personId,relatedHousehold.personId,$scope.getRelationshipForJSON(relationshipCode));
        		}
        		//if reverse relationship not added
        		if(!reverseRelationship){
        			addRelationship(relatedHousehold.personId,individualHousehold.personId,getReverseRelationship(relationshipCode));
        		}
        	
        	};
        	
        	$scope.getRelationshipForJSON = function(relationshipCode) {
        		var processedRealtionshipCode = relationshipCode;
        		if(relationshipCode == '03a' || relationshipCode == '03f' || relationshipCode == '03w'){
        			processedRealtionshipCode = '03';
        		}else if(relationshipCode == '15c' || relationshipCode == '15p'){
        			processedRealtionshipCode = '15';
        		}
        		return processedRealtionshipCode;
        	};
        	
        	
        	$scope.processAgeOver26 = function(){
				$scope.ageOver26Array=[];
        		//as relationshipChanges for New member only
				//calculateAgeOver26 Indicator for new member only
				for(var i=0;i<householdArray.length;i++){
					if(!$scope.calculateAgeGreateThan26(householdArray[i]) && householdArray[i].applyingForCoverageIndicator){
						//update the UI
						$scope.ageOver26Array.push(householdArray[i]);
					}	
				}
        		if($scope.ageOver26Array.length>0){
					//update the UI
					$scope.toggleModalForAgeOver26();
				}else{
					$scope.nextHouseholdDependent();
				}
				
        	};
			
			$scope.calculateAgeGreateThan26 = function(householdMember){
				var relationshipCode = $scope.getRelationShipCodeBetweenHousehold(1,householdMember.personId);
				 if(relationshipCode == '18' || relationshipCode == '01' || relationshipCode == '31'){
					 householdMember.under26Indicator = true;	 
				 }else if(relationshipCode == '03'){
					 var dateOfBirth = $scope.UIDateformat(householdMember.dateOfBirth);
					 var eligibleForCoverage = false;
					 relationshipCode = $scope.getRelationShipCodeBetweenHousehold(householdMember.personId,1);
					 if(!$scope.ageGreaterThan26(dateOfBirth) || relationshipCode == '15'){
						eligibleForCoverage = true;
					}
					
					householdMember.under26Indicator = eligibleForCoverage;
				 }else{
					var dateOfBirth = $scope.UIDateformat(householdMember.dateOfBirth);
					// if they are 26 years or over, mark under26Indicator as false
					if($scope.ageGreaterThan26(dateOfBirth)){
						//not eligible for coverage 
						householdMember.under26Indicator = false;
						//householdMember.applyingForCoverageIndicator = false;
					}
					else{
						householdMember.under26Indicator = true;
					}
				}
				
				return householdMember.under26Indicator;
			};
			
			$scope.ageGreaterThan26 = function(dateOfBirth){
				var birthDate = new Date(dateOfBirth);
				
				var effectiveDate = $scope.getEffectiveDate();
				
				var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

				if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
					years--;
				}

				if(years >=26){
					return true;
				}else{
					return false;
				}
			 };
			 
			 $scope.UIDateformat = function(dt){
				if($.trim(dt)=="") return dt;
				if(dt==undefined) return "";
				var formattedDate = new Date(dt);
				var dd = formattedDate.getDate();
				var mm =  formattedDate.getMonth();
				mm += 1;  // JavaScript months are 0-11
				var yyyy = formattedDate.getFullYear();
				if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
				formatteddate = mm + "/" + dd + "/" + yyyy;
				return formatteddate;
			
			};
			
			$scope.getEffectiveDate = function(){
				var applicationDate = new Date();
				var applicationDayOfMonth = applicationDate.getDate();


				var effectiveMonth = applicationDate.getMonth();
				var effectiveYear = applicationDate.getFullYear();
				var effectiveDate;

				var openEnrollmentDate = new Date('11/15/2014'); 
				
				if(applicationDate < openEnrollmentDate){
				    effectiveDate = new Date('01/01/2015'); 
				}else{
				    if(applicationDayOfMonth < 16){
				        //getMonth starts from 0
				        effectiveMonth += 2;
				    }else{
				        effectiveMonth += 3;
				    }
				    
				    if(effectiveMonth > 12){
				        effectiveMonth -= 12;
				        effectiveYear++;
				    }
				    
				    effectiveDate = new Date(effectiveMonth + "/01/" + effectiveYear);
				}
				
				
				return effectiveDate;

			};
			$scope.updateCoverageFlag=function (ageOver26ArrayFromUi){
				ageOver26NonSeeking = [];
				$.each(ageOver26ArrayFromUi, function( index, value ) {
					value.applyingForCoverageIndicator=false;
					ageOver26NonSeeking.push(value.personId); 
				});
			};
			
			$scope.getActiveHouseholdCount = function(){
				var count = 0;
				for(var i=0;i<householdArray.length;i++){
					if($scope.activeDeathFilter(householdArray[i])){
						count++;
					}
				}
				return count;
			};
			
			$scope.activeHouseholdCount = $scope.getActiveHouseholdCount(); 
        	
    	}]);
        
        
        newMexico.controller('relationshipChange', ['$scope', '$http','$location', 'myService','$controller', function($scope, $http, $location, myService,$controller){
        	angular.extend(this, $controller('bloodRelations', {$scope: $scope}));
        	$scope.eventInfoArrayTemp=eventInfoArrayTemp;
        	eventInfo.eventSubCategory = DEMOGRAPHICS[8].name;
        }]);

        newMexico.controller('incomeJob', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {

          if(eventInfo.eventSubCategory=='INCOME'){

            $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }

          /** Function to add Job JSON in income job array */
            $scope.addIncomeJob = function () {
              //ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].detailedIncome.jobIncome[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].detailedIncome.jobIncome.length] = addDefaultMaritalStatusIncomeJob();
               $scope.householdMember.detailedIncome.jobIncome.push(addDefaultMaritalStatusIncomeJob());

            };

            /** Function to remove the current Job JSON from income job array */
            $scope.removeIncomeJob = function (currentJob) {

                var incomeJobs = $scope.householdMember.detailedIncome.jobIncome;

                if(1 < incomeJobs.length) {
                  var index = incomeJobs.indexOf(currentJob);
                  incomeJobs.splice(index, 1);

                  $scope.householdMember.detailedIncome.jobIncome = incomeJobs;
                  incomeJobs = $scope.householdMember.detailedIncome.jobIncome;
                }

            };

            //to check all child forms in the screen are valid
            $scope.isAllFormValid=function(){
              var isValid = true;
              var listOfFormsResult =  $('.jobIncomeformValidation');
              for(var i=0;i<listOfFormsResult.length;i++){
                if($(listOfFormsResult[i]).html() == 'false'){
                  isValid = false;
                  break;
                }
              }
              return isValid;
            };

        }]);


         /** Function to add empty Marital Status's Income's Job. */
         function addDefaultMaritalStatusIncomeJob() {
           var defaultIncomeJobJSON = {"employerName": "", "incomeAmountBeforeTaxes": "", "incomeFrequency": ""};//job hours
           return defaultIncomeJobJSON;
         }


         newMexico.controller('incomeSelf', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeSSB', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeUnemployment', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);


         newMexico.controller('incomeRetire', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);


         newMexico.controller('incomeCapGains', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeInvest', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeRental', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeFarming', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeAlimony', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);

         newMexico.controller('incomeOther', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
           if(eventInfo.eventSubCategory=='INCOME'){

             $scope.householdMember = myService.getMember();

          }else{
            $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          }
         }]);


         //controller for summary page
         newMexico.controller('summary', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
		   $rootScope.lceEvent = 'Final Review and Confirmation';	
           $scope.mailingAddress = findHouseholdByPersonId(1).householdContact.mailingAddress;
           
           $scope.generatePDF=function(){

        	   $.ajax({
                   url:"/ghix-eligibility/reviewSummary/fetchPDF",
                   type:"POST",
                    data:{ssapJson :JSON.stringify(ssapJson),csrftoken:reportYourChangeForm.csrftoken.value}
                }).done(function(data) {
                   
                	   window.open( data ) ;
                		
                	   
                   
                }).fail(function() {
                 alert( "error occured while saving." );
                 // enableSave($scope);
             });

             };


         }]);

       //New controller added for custom logic of ethnicityRace page and having states races tribes values from look up tables.
      newMexico.controller('ethnicityRace', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
         $scope.states= ['Alabama', 'Alaska', 'Arizona', 'Arkansas', 'California', 'Colorado', 'Connecticut', 'Delaware', 'District Of Columbia', 'Florida', 'Georgia', 'Hawaii', 'Idaho', 'Illinois', 'Indiana', 'Iowa', 'Kansas', 'Kentucky', 'Louisiana', 'Maine', 'Maryland', 'Massachusetts', 'Michigan', 'Minnesota', 'Mississippi', 'Missouri', 'Montana', 'Nebraska', 'Nevada', 'New Hampshire', 'New Jersey', 'New Mexico', 'New York', 'North Carolina', 'North Dakota', 'Ohio', 'Oklahoma', 'Oregon', 'Pennsylvania', 'Rhode Island', 'South Carolina', 'South Dakota', 'Tennessee', 'Texas', 'Utah', 'Vermont', 'Virginia', 'Washington', 'West Virginia', 'Wisconsin', 'Wyoming'];

         $scope.tribes= ['Lakota','Navaho','Omaha'];

         $scope.races= getRaces();

         $scope.ethnicities=getEthnicities();

         $scope.selectedraces=[];
         try{
           $scope.selectedraces=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.race;
         }catch(err){
           ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace =   { "hispanicLatinoSpanishOriginIndicator": null,"ethnicity": [], "race": [] };
           $scope.selectedraces=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.race;
         }

         $scope.selectedEthnicities =  ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1].ethnicityAndRace.ethnicity;
         $scope.uncheckAllEthnicities = function() {
           $scope.selectedEthnicities = [];

         };
         //Populate selected ethnicity checkboxes
          var index=0;
           var loopCounter=0;
           for(index=0  ; index <  $scope.selectedEthnicities.length ; index++){
             for(loopCounter=0  ; loopCounter <  $scope.ethnicities.length ; loopCounter++){
              if( $scope.selectedEthnicities[index].code ===  $scope.ethnicities[loopCounter].code){
                $scope.ethnicities[loopCounter].checked=true;
                //if Other is selected then populate text-box value
                if($scope.selectedEthnicities[index].code === '000-0'){
                  $('#otherEthnicityText').val($scope.selectedEthnicities[index].label);
                  $scope.tempEthnicityHolder=$scope.selectedEthnicities[index].label;
                  $scope.ethnicities[loopCounter].value=$scope.ethnicities[loopCounter].label;
                }
                break;
              }
             }
           }
         //Populate selected race checkboxes
           for(index=0  ; index <  $scope.selectedraces.length ; index++){
             for(loopCounter=0  ; loopCounter <  $scope.races.length ; loopCounter++){
              if( $scope.selectedraces[index].code ===  $scope.races[loopCounter].code){
                $scope.races[loopCounter].checked=true;
              //if Other is selected then populate text-box value
                if($scope.selectedraces[index].code === '000-0'){
                  $('#otherRaceText').val($scope.selectedraces[index].label);
                  $scope.tempRaceHolder=$scope.selectedraces[index].label;
                  $scope.races[loopCounter].value=$scope.races[loopCounter].label;
                }
                break;
              }
             }
           }


         //On click of race check boxes ... call this function.
         $scope.updateRace = function(paramRace) {
          //get array in scope
          var scope = angular.element($("#ethnicityYes")).scope();
          var id= 'RACE'+paramRace.index;
          var raceArray=scope.householdMember.ethnicityAndRace.race;
          var code=paramRace.code;
          var label=paramRace.label;
          //check id add or delete operation
          if( $('#' + id).is(":checked")){
            //Add operation
            var i;
            var isAlreadyPresent=false;
            for (i = 0; i < raceArray.length; ++i) {
              if(raceArray[i].code == code){
                isAlreadyPresent=true;
                break;
              }
            }
            if(!isAlreadyPresent){
              //If already not present then add new array element.
              raceArray.push( { "label":label ,"code":code,"otherLabel": null  }  );
            }
          }else{
            //Delete operation
            var i;
            for (i = 0; i < raceArray.length; ++i) {
              if(raceArray[i].code == code){
                raceArray.splice(i,1);
                break;
              }
            }
          }
        };
        //On click of ethnicity check boxes ...call this function.
          $scope.updateEthnicity = function(paramEthnicity) {
          //get array in scope
            var scope = angular.element($("#ethnicityYes")).scope();
            var id= 'ETHNICITY'+paramEthnicity.index;
            var ethnicityArray=scope.householdMember.ethnicityAndRace.ethnicity;
            var code=paramEthnicity.code;
            var label=paramEthnicity.label;
            //check id add or delete operation
            if( $('#' + id).is(":checked")){
              //Add operation
              var i;
              var isAlreadyPresent=false;
              for (i = 0; i < ethnicityArray.length; ++i) {
                if(ethnicityArray[i].code == code){
                  isAlreadyPresent=true;
                  break;
                }
              }
              if(!isAlreadyPresent){
                //If already not present then add new array element.
                ethnicityArray.push( { "label":label ,"code":code,"otherLabel": null  }  );
              }
            }else{
              //Delete operation
              var i;
              for (i = 0; i < ethnicityArray.length; ++i) {
                if(ethnicityArray[i].code == code){
                  ethnicityArray.splice(i,1);
                  break;
                }
              }
            }
          };

       }]);
      //New controller added for address to have states and counties values from look-up table
      newMexico.controller('addressCtrl', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
		var primaryHousehold = findHouseholdByPersonId(1);
        $scope.primaryhouseholdfirstName = primaryHousehold.name.firstName;
        $scope.primaryhouseholdmiddleName = primaryHousehold.name.middleName;
        $scope.primaryhouseholdlastName = primaryHousehold.name.lastName;

        $scope.states=getStates();

        $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
        //$scope.selectedState=$scope.states[0];
        /* //Code to load selected state.
        for(var i=0;i<$scope.states.length;i++){
          if($scope.householdMember.otherAddress.address.state ===   $scope.states[i].code){
            $scope.selectedState=$scope.states[i];
            break;
          }
        } */





          //function to copy the home address
        $scope.changeAddress=function(){

          var lastHousehold = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
          changeAddressOfHouseHold(lastHousehold);

        };

        $scope.changeTempAddress=function(){

            var lastHousehold = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
            changeTempAddressOfHouseHold(lastHousehold);

          };

        $scope.ajaxRequestForLoadCounties = function (stateCode, zipCode){
           var urlForCounties = '<c:url value="/ssap/addCountiesForStates"/>?';
			var localCSRF = {csrftoken : '<df:csrfToken plainToken="true"/>'};
             $http({method: 'POST', url: urlForCounties,params:{stateCode : stateCode,zipCode:zipCode},data:localCSRF}).
                  success(function(data, status, headers, config) {
                      $scope.counties = data;
                  }).
                  error(function(data, status, headers, config) {
                      $scope.counties = {};
                //console.log(data || "Request failed");
                      $scope.status = status;
                  });
        }


          /** Method to asynchronous populate counties for selected state. */
          $scope.loadCounties = function(stateCode,zipCode) {
            if((stateCode == undefined || stateCode == '') || zipCode == undefined || zipCode == '' ){
              return;
            }

          $scope.ajaxRequestForLoadCounties(stateCode,zipCode);

          };


          if($scope.householdMember.otherAddress.address.state!= undefined && $scope.householdMember.otherAddress.address.state != "" && $scope.householdMember.otherAddress.address.postalCode != undefined && $scope.householdMember.otherAddress.address.postalCode != '' ){
            $scope.ajaxRequestForLoadCounties($scope.householdMember.otherAddress.address.state,$scope.householdMember.otherAddress.address.postalCode);
          }

          $scope.assignCounty= function() {
            var currCounty = $scope.selectedCounty.code;
            $scope.householdMember.otherAddress.address.county=currCounty;
          };

          $scope.updatePostalCode= function(postalCodeId){
            $('#'+postalCodeId).val("");

          };

      }]);

      //controller for incarceration
      newMexico.controller('incarcerationController', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
        //sets default value for event sub category as incarceration has only one sub category
        eventInfo.eventSubCategory = INCARCERATION[0].name;//'INCARCERATION'
		$scope.eventInfo=eventInfo
        //add temp array to controller scope
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        $scope.ssapJsonCloned=ssapJsonCloned;

        $scope.getOriginalIndex=function(content,array){
            return  $.inArray(content,array);
        };


        //on click of next button compare previous status and then add changed applicants into array.
       $scope.updateChangedHouseHold= function (){
          $scope.updateGlobalIncarcerationFlag();
       };
       //Update 'applyingForCoverageIndicator' flag as part of HIX-60559  
       $scope.updateApplyingCoverage=function(){
    	  var primaryHousehold = findHouseholdByPersonId(1);
    	  if( (findHouseholdByPersonIdInOldData(1).incarcerationStatus.incarcerationStatusIndicator != primaryHousehold.incarcerationStatus.incarcerationStatusIndicator)&&
    			  primaryHousehold.incarcerationStatus.incarcerationStatusIndicator  ){
    		  var arrayIndex=0;
    		  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    		  for (arrayIndex=0;arrayIndex < householdArray.length;arrayIndex ++){
    			  householdArray[arrayIndex].applyingForCoverageIndicator= false;
	          }
    	  }   
       };
       

	}]);

      /*                        *
       *  Controller for Number of depedents starts *
       *                        *
       */

      //Controller for number of dependent primary page.
      newMexico.controller('numOfDepController', ['$scope', '$http','$location', 'myService','$rootScope', function($scope, $http, $location, myService,$rootScope) {
        $scope.modalShown = false;
        $scope.birthFlag=false;
        $scope.oldEventValue = '';
        $scope.addTaxDepReason=addTaxDepReason;
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        dependentStack = [];
        $scope.addTaxDepeReasons={reasonArray:[{code:'<spring:message code="label.lce.catastrophic.coverage"/>',label:'<spring:message code="label.lce.catastrophic.coverage"/>'},{code:'<spring:message code="label.lce.aged.out"/>',label:'<spring:message code="label.lce.aged.out"/>'},{code:'<spring:message code="label.lce.exemption.expired"/>',label:'<spring:message code="label.lce.exemption.expired"/>'},{code:'<spring:message code="label.lce.loss.mec"/>',label:'<spring:message code="label.lce.loss.mec"/>'},{code:'<spring:message code="label.lce.loss.primary.filer"/>',label:'<spring:message code="label.lce.loss.primary.filer"/>'},
                                               {code:'<spring:message code="label.lce.loss.primary.filer.death"/>',label:'<spring:message code="label.lce.loss.primary.filer.death"/>'},{code:'<spring:message code="label.lce.loss.plan.divorce"/>',label:'<spring:message code="label.lce.loss.plan.divorce"/>'},{code:'<spring:message code="label.lce.moved.into.state"/>',label:'<spring:message code="label.lce.moved.into.state"/>'}]};
        
        //defined adde dependents if not defined
        if($rootScope.numOfDependents == null || $rootScope.numOfDependents ==  undefined){
        	$rootScope.numOfDependents = {addDepdendents:false,table:'true'};
        }

          $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
            $scope.modalHouseholdMember=houseHoldFromParent;
            $scope.modalShown = !$scope.modalShown;
            $scope.originalIndex=originalIndex;
          };
          $scope.closeModal = function(houseHoldFromParent,originalIndex) {
              $scope.modalShown = false;
          };
          $scope.getOriginalIndex=function(content,array){
              return  $.inArray(content,array);
          };
        $scope.householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
        $scope.clonedHouseholdArray = angular.copy(ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember);
        //to reinitialize the partials array
        $scope.selectedEvents[lifeEventCounter].partials = depedentsArray;
        $scope.dependentEventArray=UPDATE_DEPENDENTS;
        $scope.table='true';
        if( ($scope.eventInfo.eventSubCategory === UPDATE_DEPENDENTS[0].name) || ($scope.eventInfo.eventSubCategory === UPDATE_DEPENDENTS[1].name) || ($scope.eventInfo.eventSubCategory === UPDATE_DEPENDENTS[2].name)){ //'ADD_DEPENDENT'  'REMOVE_DEPENDENT'   'DEATH_OF_DEPENDENT'
          $scope.table='false';
        }
        //remove newly added household if user clicks back from details pages

        removeDependentFromJson();
        //Function to set future date validation on click of birth event
        $scope.setFutureDate= function(dayValue){
        	 $("#dependentDate").attr("check-after",dayValue);
        	 if(dayValue === '-1'){
        		$scope.birthFlag=true; 
        	 }else{
        		 $scope.birthFlag=false;  
        	 }
        };
        if(eventInfo.eventSubCategory === 'BIRTH'){
        	$scope.birthFlag=true; 
        }
        $scope.updateSeekCoverageFlag= function(modalHouseholdMemberFromView){
        	if((modalHouseholdMemberFromView.personId == 1) ||(modalHouseholdMemberFromView.personId === '1')){
        	//If primary applicants then make flag false for all applicants.
        		var arrayIndex=0;
	        	 for (arrayIndex=0;arrayIndex < $scope.householdArray.length;arrayIndex ++){
	        		 $scope.householdArray[arrayIndex].applyingForCoverageIndicator= false;
	        	 }
        	}
        };
        
        $scope.resetData = function (eventName){
          	var dataChanged = false;
        	
          	if($scope.oldEventValue == eventName){
          		return;
          	}
          	
			for(var i=0;i<$scope.householdArray.length;i++){
				for(var j=0;j<eventInfo.changedApplicants.length;j++){
							
						if(eventInfo.changedApplicants[j].personId == $scope.householdArray[i].personId){
							dataChanged=true;
						}
					
					}
			}
			
			if($scope.oldEventValue != "" && dataChanged){
				//reset data
				ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember = angular.copy($scope.clonedHouseholdArray);
				$scope.householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
				$scope.previousEventName = $scope.getEventNameByCode($scope.oldEventValue);
				$scope.currentEventName = $scope.getEventNameByCode(eventName);
				eventInfo.changedApplicants=[];
				$scope.showWarningOnChange = true;
			}
			
			$scope.oldEventValue=eventName;
          };
          
          $scope.hideDataChangeModel = function(){
        	  $scope.showWarningOnChange = false;
          }
          
          $scope.getEventNameByCode = function(eventCode){
        	  var eventName = '';
        	  switch(eventCode){
        	  	case UPDATE_DEPENDENTS[1].name:
        	  		eventName = UPDATE_DEPENDENTS[1].label;
        	  		break;
        	  	case UPDATE_DEPENDENTS[2].name:
        	  		eventName = UPDATE_DEPENDENTS[2].label
        	  		break;
        	  	case UPDATE_DEPENDENTS[0].name:
        	  		eventName = UPDATE_DEPENDENTS[0].label
        	  		break;
        	  	case 'addADependent':
        	  		eventName = 'Add a Household Member(s)';
        	  		break;
        	  }
        	  return eventName;
          }
          
	
        
      }]);

      //Controller for number of dependent add page.
      newMexico.controller('noOfDependentsAdd', ['$scope', '$http','$location', 'myService', function($scope, $http, $location, myService) {
        if(dependentForHouseholdAdded){
          ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
          dependentForHouseholdAdded=false;
          eventInfo.changedApplicants =[];
        }
      }]);







      /*                        *
       *  Controller for Number of depedents ends   *
       *                        *
       */

      //global function to initialize income array when event is changed

      function initIncomeTypes($scope){
        for (var i = $scope.incomeTypes.length -1; i >= 0; i--) {
            $scope.incomeTypes[i].value = false;
        }
      }

      //global function to find number of income templates in partial
      function getNoOfIncomeTemplatesInPartials(incomeTypes,currentPartials,afterIndex){
        var noOfIncomePagesInTemplates = 0;
        for (var i = incomeTypes.length -1; i >= 0; i--) {
             //used $.inArray jquery method to find element in array, as array.indexOf didn't work below IE 9

          if(eventInfo.eventSubCategory === 'INCOME'){
            if (($.inArray(incomeTypes[i].url,currentPartials,afterIndex) !=-1)){
                   noOfIncomePagesInTemplates++;
            }
          }else{
            if (($.inArray(incomeTypes[i].url,currentPartials,dependentHousholdStarted) !=-1)){
                   noOfIncomePagesInTemplates++;
            }
          }

        }
        return noOfIncomePagesInTemplates;
      }


      function identifyEvents(){
    	  
		   var fieldsToCompare =  [];
		   var dateAttr = undefined;
		   var changeInAddressProcessed = false;
		   var lawfulPresenceProcessed = false;
		   var gainInMECProcessed = false;
		   var lossOfMECProcessed = false;
		   var changeInBasicInformation = false;
		   for(var i=0;i<eventInfoArray.length;i++){
			var eventMatchedToUpdateApplicants = true;
			switch(eventInfoArray[i].eventCategory){
    		  	case IMMIGRATION_STATUS_CHANGE[0].category:
					if(!lawfulPresenceProcessed){
						eventInfoArray.splice(i,1);
						i--;
						fieldsToCompare = ["citizenshipImmigrationStatus"];
						var changedApplicantList = compareHouseholds(fieldsToCompare,'lawfulChangeDate');
						fillEventArrayForLawful(changedApplicantList);
						lawfulPresenceProcessed = true;
					}
					eventMatchedToUpdateApplicants = false;
					break;
				case DEMOGRAPHICS[0].category :
					if(eventInfoArray[i].eventSubCategory == DEMOGRAPHICS[2].name){
						eventInfoArray[i].changedApplicants = compareMailingAddress();
					}else if(eventInfoArray[i].eventSubCategory == DEMOGRAPHICS[8].name){
						eventInfoArray[i].changedApplicants  = compareRelationship();
					}else if(eventInfoArray[i].eventSubCategory == DEMOGRAPHICS[9].name){
						continue;//do nothing skip record
					}else if(!changeInBasicInformation){
						eventInfoArray.splice(i,1);
						i--;
						//change in basic information
						fieldsToCompare = ["name"];
						var changedApplicants = compareHouseholds(fieldsToCompare,'nameChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[0].name);
						//change in gender
						fieldsToCompare = ["gender"];
						var changedApplicants = compareHouseholds(fieldsToCompare,'nameChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[5].name);
						//change in phone
						fieldsToCompare = ["householdContact.phone.phoneNumber","householdContact.otherPhone"];
						var changedApplicants = compareHouseholds(fieldsToCompare,'nameChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[7].name);
						//change in email
						fieldsToCompare = ["householdContact.contactPreferences.emailAddress"];
						var changedApplicants = compareHouseholds(fieldsToCompare,'nameChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[6].name);
						//change in dob
						fieldsToCompare = ["dateOfBirth"];
						changedApplicants = compareHouseholds(fieldsToCompare,'dobChangeDate');
						fillArrayForDOB(changedApplicants);
						
						//change in SSN
						fieldsToCompare = ["socialSecurityCard.middleNameOnSSNCard",
						                   "socialSecurityCard.socialSecurityNumber",
						                   "socialSecurityCard.suffixOnSSNCard",
						                   "socialSecurityCard.individualMandateExceptionIndicator",
						                   "socialSecurityCard.ssnManualVerificationIndicator",
						                   "socialSecurityCard.deathIndicatorAsAttested",
						                   "socialSecurityCard.nameSameOnSSNCardIndicator",
						                   "socialSecurityCard.firstNameOnSSNCard",
						                   "socialSecurityCard.socialSecurityCardHolderIndicator",
						                   "socialSecurityCard.useSelfAttestedDeathIndicator",
						                   "socialSecurityCard.lastNameOnSSNCard",
						                   "socialSecurityCard.reasonableExplanationForNoSSN"];
						changedApplicants = compareHouseholds(fieldsToCompare,'ssnChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[1].name);
						//change in ethnicity and race
						fieldsToCompare = ["ethnicityAndRace"];
						changedApplicants = compareHouseholds(fieldsToCompare,'ethnicityRaceChangeDate');
						fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[4].name);
						
						changeInBasicInformation = true;
					}
					eventMatchedToUpdateApplicants = false;
					
					/* switch(eventInfoArray[i].eventSubCategory){
						case DEMOGRAPHICS[0].name : //change in Name
							fieldsToCompare = ["name","gender","householdContact.phone.phoneNumber","householdContact.otherPhone"];
							break;
						case DEMOGRAPHICS[3].name : //change in date of Birth
							fieldsToCompare = ["dateOfBirth"];
							break;
						case DEMOGRAPHICS[1].name : //change in SSN
							fieldsToCompare = ["socialSecurityCard.middleNameOnSSNCard",
							                   "socialSecurityCard.socialSecurityNumber",
							                   "socialSecurityCard.suffixOnSSNCard",
							                   "socialSecurityCard.individualMandateExceptionIndicator",
							                   "socialSecurityCard.ssnManualVerificationIndicator",
							                   "socialSecurityCard.deathIndicatorAsAttested",
							                   "socialSecurityCard.nameSameOnSSNCardIndicator",
							                   "socialSecurityCard.firstNameOnSSNCard",
							                   "socialSecurityCard.socialSecurityCardHolderIndicator",
							                   "socialSecurityCard.useSelfAttestedDeathIndicator",
							                   "socialSecurityCard.lastNameOnSSNCard",
							                   "socialSecurityCard.reasonableExplanationForNoSSN"];
							break;
						case DEMOGRAPHICS[2].name : //change in Mailing Address
							eventInfoArray[i].changedApplicants = compareMailingAddress();
							eventMatchedToUpdateApplicants = false;
							break;
					}
					dateAttr = 'nameChangeDate'; */
					break;
				case INCARCERATION[0].category : //Change in Incarceration
						fieldsToCompare = ["incarcerationStatus.incarcerationStatusIndicator"];
						dateAttr = 'dateOfChange';
					break;
				case TRIBE_STATUS[0].category: //Change in Native American Status
					fieldsToCompare = ["specialCircumstances.americanIndianAlaskaNativeIndicator","americanIndianAlaskaNative"];
					dateAttr = 'tribeChangeDate';
					break;
				case ADDRESS_CHANGE[0].category:
					if(!changeInAddressProcessed){
						eventInfoArray.splice(i,1);
						i--;
						fieldsToCompare = ["otherAddress","livesAtOtherAddressIndicator","householdContact.homeAddress"];
						var changedApplicantList = compareHouseholds(fieldsToCompare,'changeInAddressDate');
						fillEventArrayForAddress(changedApplicantList);
						changeInAddressProcessed = true;
					}
					eventMatchedToUpdateApplicants = false;
					break;


				case GAIN_MEC[0].category: //gain MEC
					if(!gainInMECProcessed){
						var tempEventInfo = eventInfoArray[i];
						eventInfoArray.splice(i--,1);
						compareLossOfMECAndGain(tempEventInfo);
						gainInMECProcessed = true;
					}
					eventMatchedToUpdateApplicants = false;
					break;
				case LOSS_OF_MEC[0].category: //loss of MEC
						if(!lossOfMECProcessed){
						var tempEventInfo = eventInfoArray[i];
						eventInfoArray.splice(i--,1);
						compareLossOfMECAndGain(tempEventInfo);
						lossOfMECProcessed = true;
						}
					eventMatchedToUpdateApplicants = false;
					break;
				default :
					eventMatchedToUpdateApplicants = false;
					break;
    		  }
			  if(eventMatchedToUpdateApplicants){
				eventInfoArray[i].changedApplicants = compareHouseholds(fieldsToCompare,dateAttr);
			  }
    		  //lawful presence
    		  /* if(eventInfoArray.eventCategory == IMMIGRATION_STATUS_CHANGE[0].category){

    		  } */
    		  
    	  }
		 //update MailingAddressSameAsHomeAddressIndicator
		   updateMailingAddressSameAsHomeAddressIndicator();
      }

      
      function fillArrayForDOB(changedApplicants){
    	  var dobChangedApplicantRemove = [];
    	  var dobChangedApplicant = [];
    	  if(ageOver26NonSeekingDobChange.length > 0 ){
    		  for(i=0;i<changedApplicants.length;i++){
    			  
    			  if(ageOver26NonSeekingDobChange.indexOf(changedApplicants[i].personId) != -1){
    				  dobChangedApplicantRemove.push(changedApplicants[i]);
    			  }else{
    				  dobChangedApplicant.push(changedApplicants[i]);
    			  }
    	      }
    		  fillArrayWithEvent(dobChangedApplicant,DEMOGRAPHICS[0].category,DEMOGRAPHICS[3].name);
    		  fillArrayWithEvent(dobChangedApplicantRemove,DEMOGRAPHICS[0].category,DEMOGRAPHICS[10].name);
    		  
    	  }else{
    		  fillArrayWithEvent(changedApplicants,DEMOGRAPHICS[0].category,DEMOGRAPHICS[3].name);
    	  }
       }
      
      function compareRelationship(){
    	  var changedApplicants = [];
    	  var changedApplicantsRemove= [];
    	  var householdMember = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var householdMemberModified = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var bloodRelationsModified = findHouseholdByPersonId(1).bloodRelationship;
    	  var bloodRelations = findHouseholdByPersonIdInOldData(1).bloodRelationship;
    	  var isChanged =  false
    	  for(var i=0; i<bloodRelations.length;i++){
    		  if((bloodRelations[i].individualPersonId == bloodRelationsModified[i].individualPersonId) && (bloodRelations[i].relatedPersonId == bloodRelationsModified[i].relatedPersonId) && (bloodRelations[i].relation != bloodRelationsModified[i].relation)){
    			  isChanged = true;
    			  break;
    		  }
    	  }
    	  if(isChanged){
    		   changedApplicants.push({personId:1,eventDate:eventInfoArrayTemp[0].relationshipDate});
    	  }
    	  if(isChanged && ageOver26NonSeeking.length > 0){
    		  for(i=0;i<ageOver26NonSeeking.length;i++){
    			  changedApplicantsRemove.push({personId:ageOver26NonSeeking[i],eventDate:eventInfoArrayTemp[0].relationshipDate});
    		  }
    		  fillArrayWithEvent(changedApplicantsRemove,DEMOGRAPHICS[0].category,DEMOGRAPHICS[9].name);
    	  }
    	  return changedApplicants;
      }
      
      function isDemographicDataChanged(originalHousehold,newHousehold){
    	
    	  var arrFormChanged = [];
    	  
    	  fieldsToCompare = ["name","householdContact.contactPreferences.emailAddress","gender","householdContact.phone.phoneNumber","householdContact.otherPhone"];
    	  if(compareBetweenHousehold(originalHousehold,newHousehold,fieldsToCompare)){
    		  arrFormChanged.push("namechangeform")
    	  }
    	  fieldsToCompare = ["dateOfBirth"];
    	  if(compareBetweenHousehold(originalHousehold,newHousehold,fieldsToCompare)){
    		  arrFormChanged.push("dobchange")
    	  }
    	  fieldsToCompare = ["socialSecurityCard.middleNameOnSSNCard",
			                   "socialSecurityCard.socialSecurityNumber",
			                   "socialSecurityCard.suffixOnSSNCard",
			                   "socialSecurityCard.individualMandateExceptionIndicator",
			                   "socialSecurityCard.ssnManualVerificationIndicator",
			                   "socialSecurityCard.deathIndicatorAsAttested",
			                   "socialSecurityCard.nameSameOnSSNCardIndicator",
			                   "socialSecurityCard.firstNameOnSSNCard",
			                   "socialSecurityCard.socialSecurityCardHolderIndicator",
			                   "socialSecurityCard.useSelfAttestedDeathIndicator",
			                   "socialSecurityCard.lastNameOnSSNCard",
			                   "socialSecurityCard.reasonableExplanationForNoSSN"];
    	  if(compareBetweenHousehold(originalHousehold,newHousehold,fieldsToCompare)){
    		  arrFormChanged.push("maritalStatusSSNForm")
    	  }
    	  
    	  fieldsToCompare = ["ethnicityAndRace"];
    	  if(compareBetweenHousehold(originalHousehold,newHousehold,fieldsToCompare)){
    		  arrFormChanged.push("ethnicityRaceChangeForm")
    	  }
    	  return arrFormChanged;
    	  
	  }
      
      function compareBetweenHousehold(originalHousehold,newHousehold,arr){
    	  var comparisonResult = true;
			for(var j=0;j<arr.length;j++){
				comparisonResult = angular.equals(eval('originalHousehold.'+arr[j]),eval('newHousehold.'+arr[j]));
				if(!comparisonResult){
					break;
				}
			}
		  return !comparisonResult;
      }

      function compareLossOfMECAndGain(eventInfo){
    	  	var eventInfoEventTypeArray={};
    	  	for(var i=0;i<eventInfo.changedApplicants.length;i++){
 	        	var originalIndexInArray=-1;
 	        	var clonedHouseHoldArray=ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
 	        	$.each(clonedHouseHoldArray, function(key,value) {
 	        		if(value.personId === eventInfo.changedApplicants[i].personId){
  		       			 originalIndexInArray=key;
  		       			// break;
       		  		}
         		});
 	        	if((eventInfo.eventCategory === LOSS_OF_MEC[0].category)){  //'LOSS_OF_MEC'
           			strProperty=eventInfoArrayTemp[originalIndexInArray].lossEventSubCategory;
                  }else{
                	  strProperty=eventInfoArrayTemp[originalIndexInArray].gainEventSubCategory;
                }
 	        	var tmpEventInfo = eventInfoEventTypeArray[strProperty];
           		if(tmpEventInfo === undefined || tmpEventInfo === null){
           			tmpEventInfo= {changedApplicants:[],eventCategory: '',eventSubCategory:''};
           			eventInfoEventTypeArray[strProperty] = tmpEventInfo;
           		}
           		tmpEventInfo.changedApplicants.push({"personId":eventInfo.changedApplicants[i].personId,"eventDate":eventInfo.changedApplicants[i].eventDate});
               	tmpEventInfo.eventCategory=eventInfo.eventCategory;
               	tmpEventInfo.eventSubCategory = strProperty;
    	  	}
               	$.each(eventInfoEventTypeArray, function(key,value) {
                 	  addEventInfo(value);
           		});
      }

      function compareMailingAddress(){
    	  var changedApplicants = [];
    	  var householdMember = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var householdMemberModified = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  
    	  for(var i=0;i<householdMember.length;i++){
    	  var householdMemberMailing = householdMember[i];
    	  var householdMemberMailingModified = householdMemberModified[i];
    	  var result = angular.equals(householdMemberMailing.householdContact.mailingAddress,householdMemberMailingModified.householdContact.mailingAddress);
	    	  if(!result){
	    		  changedApplicants.push({personId:householdMemberMailingModified.personId,eventDate:eventInfoArrayTemp[0].changeInAddressDate});
	    		  //Compare logic for mailing address and house-hold address
	    		  var mailingAddressComprArray=['postalCode','county','streetAddress1','streetAddress2','state','city'];
	    		  for(var j=0; j < mailingAddressComprArray.length;j++){
	    			  result = angular.equals(eval('householdMemberMailing.householdContact.homeAddress.'+mailingAddressComprArray[j]),eval('householdMemberMailingModified.householdContact.mailingAddress.'+mailingAddressComprArray[j]));
						if(!result){
							break;
						}
					}
	    		   if(!result){
	    			   householdMemberMailingModified.householdContact.mailingAddressSameAsHomeAddressIndicator=false;
	    		  }
	    	  	}
    	  }
    	  return changedApplicants;
      }

      function fillEventArrayForLawful(changedApplicantList){
    	  var householdMember = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var householdMemberModified = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var gainCitzenship = [];
    	  var loseCitizenship = [];
    	  var gainLPR = [];
    	  var changeLPR = [];

    	  for(var i = 0; i< householdMember.length;i++){
    		  for(var j=0;j< changedApplicantList.length;j++){
    			  if(changedApplicantList[j].personId == householdMember[i].personId){
    				  // Same State - CHANGE_IN_ADDRESS_WITHIN_STATE
    				  var event = compareLawful(householdMemberModified[i],householdMember[i]);
    				  switch(event){

    				  	case IMMIGRATION_STATUS_CHANGE[0].name://GAIN_CITIZENSHIP
    				  		gainCitzenship.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  		break;
    				  	case IMMIGRATION_STATUS_CHANGE[1].name://GAIN_LPR
    				  		gainLPR.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  		break;
    				  	case IMMIGRATION_STATUS_CHANGE[2].name: //LOSE_CITIZENSHIP
    				  		loseCitizenship.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  		householdMemberModified[i].citizenshipImmigrationStatus.citizenshipAsAttestedIndicator=false;
    				  		break;
    				  	case IMMIGRATION_STATUS_CHANGE[3].name: //CHANGE_LPR
    				  		changeLPR.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  		break;
    				  }
    				  break;
    			  }
    		  }
    	  }

    	  //fill eventInfoArray if above event happened
    	  fillArrayWithEvent(gainCitzenship,IMMIGRATION_STATUS_CHANGE[0].category,IMMIGRATION_STATUS_CHANGE[0].name);
    	  fillArrayWithEvent(gainLPR,IMMIGRATION_STATUS_CHANGE[0].category,IMMIGRATION_STATUS_CHANGE[1].name);
    	  fillArrayWithEvent(loseCitizenship,IMMIGRATION_STATUS_CHANGE[0].category,IMMIGRATION_STATUS_CHANGE[2].name);
    	  fillArrayWithEvent(changeLPR,IMMIGRATION_STATUS_CHANGE[0].category,IMMIGRATION_STATUS_CHANGE[3].name);
     }

      function fillArrayWithEvent(changedApplicants,category,subCategory){
    	  if(changedApplicants.length > 0){
    		  var eventInfoTemp = { "eventCategory": category,
    			        "eventSubCategory":subCategory,
    			        "changedApplicants":[]
    			      };
	    	  for(var j=0;j<changedApplicants.length;j++){
	    		  eventInfoTemp.changedApplicants.push(changedApplicants[j]);
	    	  }
	    	  eventInfoArray.push(eventInfoTemp);
    	  }
      }

      function compareLawful(household,clonedHousehold){
    	  var event = '';
    	  if(household.citizenshipImmigrationStatus.citizenshipStatusIndicator && !clonedHousehold.citizenshipImmigrationStatus.citizenshipStatusIndicator ){
    		  //GAIN_CITIZENSHIP
    		  event = IMMIGRATION_STATUS_CHANGE[0].name;
    	  }else if(!household.citizenshipImmigrationStatus.citizenshipStatusIndicator && clonedHousehold.citizenshipImmigrationStatus.citizenshipStatusIndicator){
    		  //LOSE_CITIZENSHIP
    		  event = IMMIGRATION_STATUS_CHANGE[2].name;
    	  }else if(household.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && (clonedHousehold.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==null || clonedHousehold.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator==undefined || !clonedHousehold.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator)){
    		//GAIN_LPR
    		  event = IMMIGRATION_STATUS_CHANGE[1].name;
    	  }else{
    		//GAIN_CITIZENSHIP
    		event = IMMIGRATION_STATUS_CHANGE[3].name; //for now till we don't know default event for lawful presence.
    	  }
    	  return event;
      }

      function fillEventArrayForAddress(changedApplicantList){
    	  var householdMember = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var householdMemberModified = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
    	  var changeWithInState = [];
    	  var movedOutOfState = [];
    	  var movedOutOfStateDependents = [];
    	  var movedIntoState = [];
    	  for(var i = 0; i< householdMember.length;i++){
    		  for(var j=0;j< changedApplicantList.length;j++){
    			  if(changedApplicantList[j].personId == householdMember[i].personId){
    				  // Same State - CHANGE_IN_ADDRESS_WITHIN_STATE
    				  if(findState(householdMember[i]) == globalStateCode &&  findState(householdMemberModified[i]) == globalStateCode){
					  
						  if(householdMember[i].personId == 1){
							var isZipCountyChange = compareZipCountyChange(householdMember[i],householdMemberModified[i]);
							changeWithInState.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate,isChangeInZipCodeOrCounty:isZipCountyChange});
						  }else{
    					  changeWithInState.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
						  }
    				  }else if(findState(householdMember[i]) != globalStateCode && findState(householdMemberModified[i]) == globalStateCode ){//moved into state.
    					  movedIntoState.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  
    				  }else{//MOVED_OUT_OF_STATE
    					
    					  
    				  	if((householdMemberModified[i].personId ==1 ) ||(householdMemberModified[i].personId ==='1' )){
    				  		var loopVar=0;
    				  		for (loopVar=0;loopVar < householdMemberModified.length;loopVar++){
    				  			householdMemberModified[loopVar].applyingForCoverageIndicator = false; //Added as part of HIX-60559 
      					    }
    				  		movedOutOfState.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});
    				  	}else{
    				  		householdMemberModified[i].applyingForCoverageIndicator = false; //Added as part of HIX-71079 
    				  		movedOutOfStateDependents.push({personId:householdMember[i].personId,eventDate:changedApplicantList[j].eventDate});//Added as part of HIX-64198
    				  	}
    				  }
    				  break;
    			  }
    		  }
    	  }
    	  //create event for change with in state
    	  fillArrayWithEvent(changeWithInState,ADDRESS_CHANGE[0].category,ADDRESS_CHANGE[0].name);
    	 //create event for moved into state
    	  fillArrayWithEvent(movedIntoState,ADDRESS_CHANGE[0].category,ADDRESS_CHANGE[1].name);
		  //create event for moved out of state
    	  fillArrayWithEvent(movedOutOfState,ADDRESS_CHANGE[0].category,ADDRESS_CHANGE[2].name);
    	//create event for moved out of state dependents.
    	  fillArrayWithEvent(movedOutOfStateDependents,ADDRESS_CHANGE[0].category,ADDRESS_CHANGE[3].name);
      }

      function findState(householdMember){
    	  var state = '';
    	  if(householdMember.livesAtOtherAddressIndicator){
    		  state = householdMember.otherAddress.address.state;
    	  }else{
    	  	  state = householdMember.householdContact.homeAddress.state;
    	  }

    	  return state;
      }
	  
	   function compareZipCountyChange(householdMember,householdMemberModified){
    	  var zipCountyChange = true;
		  if(householdMemberModified.personId ==1){
				 var zip = householdMemberModified.householdContact.homeAddress.postalCode;
				 var county = householdMemberModified.householdContact.homeAddress.county;
				 var zipOld = householdMember.householdContact.homeAddress.postalCode;
				 var countyOld = householdMember.householdContact.homeAddress.county;
				 if(zip ==  zipOld && county==countyOld){
					var zipCountyChange = false;
				}
		 }
		 return zipCountyChange;
      }

      function compareHouseholds(arr,dateAttribute){
    	var changedApplicants = [];
    	for(var i=0;i<ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember.length;i++){
				var comparisonResult = true;
				for(var j=0;j<arr.length;j++){
					comparisonResult = angular.equals(eval('ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember['+i+'].'+arr[j]),eval('ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember['+i+'].'+arr[j]));
					if(!comparisonResult){
						break;
					}
				}
				if(!comparisonResult){
					changedApplicants.push({personId:ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i].personId,eventDate:eval('eventInfoArrayTemp[i].'+ dateAttribute)});
				}

		  }
		  return changedApplicants;
      }
      
      function updateMailingAddressSameAsHomeAddressIndicator(){
    	    var arr = ["postalCode","county","streetAddress1","streetAddress2","state","city"];
        	for(var i=0;i<ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;i++){
        		    var householdMemberObj = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
        		    var comparisonResult = true;
        		    for(var j=0;j<arr.length;j++){
    					comparisonResult = angular.equals(eval('householdMemberObj.householdContact.homeAddress.'+arr[j]),eval('householdMemberObj.householdContact.mailingAddress.'+arr[j]));
    					if(!comparisonResult){
    						break;
    					}
    				}
        			if(!comparisonResult){
        				householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = false;
        			}
    				
    		  }
        }

      function compareDataChanges(){
    	  var dataChanges = false;
    	  for(i=0;i<eventInfoArray.length;i++){
    		  if(eventInfoArray[i].changedApplicants.length > 0){
    			  dataChanges= true;
    			  break;
    		  }
    	  }
    	  return dataChanges;
      }
      
      
      // Ajax Request to save after second-to-last page in life event
      function saveSSAPJSON($scope,lifeEventCounter){
        //eventInfo.eventCategory = $scope.selectedEvents[lifeEventCounter].eventCode;
        
        if(!compareDataChanges()){
        	 //$scope.saveDisabled =  false;
      	     //angular.element('[ng-controller=signAndSubmit]').scope().modalShown = false;
      	   	 $scope.$root.showNotificationForNoChange = true;
      	   	 enableSave($scope);
			 return;
        }
        
        
        //set application type to LCE
        ssapJson.singleStreamlinedApplication.ApplicationType = "SEP";
        $scope.saveDisabled =  true;
        //$scope.$apply();
        // build name param from eSign Name
        var firstName = "";
		var middleName = "";
		var lastName = "";

		var eSignName = angular.element('[ng-controller=signAndSubmit]').scope().eSignName;
		var arrName = eSignName.split(" ");
		if(arrName.length == 2){
			firstName = arrName[0]; 
			lastName = arrName[1];
		}else if(arrName.length == 3){
			firstName = arrName[0];
			middleName = arrName[1];
			lastName = arrName[2];
		}
        $.ajax({
                url:"/hix/iex/lce/savessapapplication",
                type:"POST",
               /*  dataType:"json html", */
                data:{ssapJson :JSON.stringify(ssapJson),eventInfo:JSON.stringify(eventInfoArray),csrftoken:reportYourChangeForm.csrftoken.value,ESIGN_FIRST_NAME:firstName,ESIGN_MIDDLE_NAME:middleName,ESIGN_LAST_NAME:lastName,ADD_TAX_DEPENDENT_REASON:addTaxDepReason.reason,coverageYear:$('#coverageYear').val()}
             }).done(function(data) {
                if(data.status != undefined){
                  if(data.status =="success"){
                    //alert(data.message);
                    window.location = '<c:url value="/indportal" />';
                  }else if(data.status =="sepDenied"){
                	  window.location = '<c:url value="/iex/lce/lceDenied" />'; 
                  }else{
                    alert(data.message);
                    enableSave($scope);
                  }
                }else{
                  //alert("error occured while saving.");
                  if(data.indexOf("application-error") != -1 || data.indexOf("Application Error") != -1){
                   document.write(data);
        		   document.close();
        	   }else{
                	  alert("Unexpected error occurred during the operation.")
                	  enableSave($scope);
                  }
                  
                }
             }).fail(function() {
               //alert( "error occured while saving." );
               enableSave($scope);
          });
      }

      function enableSave($scope){
    	  $scope.saveDisabled =  false;
    	  angular.element('[ng-controller=signAndSubmit]').scope().modalShown = false;
    	  if ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
    		    $scope.$apply();
    		}
      }

       function addChangedApplicants(householdPersonId,changeDate){
        var alreadyInChangedApplicants =  false;
        //if changedApplicants is empty then add to array and enable the next button
        if(eventInfo.changedApplicants.length == 0){
          if(changeDate != undefined && changeDate != null){
            eventInfo.changedApplicants.push({"personId":householdPersonId,"eventDate":changeDate});
          }else{
            eventInfo.changedApplicants.push({"personId":householdPersonId});
          }
        }else{
          //iterated the changedApplicants array and find if household already added
          for(var i=0;i<eventInfo.changedApplicants.length;i++){
              //if household is already in changedApplicatnts
              if(householdPersonId == eventInfo.changedApplicants[i].personId){
                if(changeDate != undefined && changeDate != null){
                  eventInfo.changedApplicants[i].eventDate=changeDate;
                }
                alreadyInChangedApplicants = true;
                break;
              }
          }
          //if household is not in changedApplicatnts then add it
          if(!alreadyInChangedApplicants){
            if(changeDate != undefined && changeDate != null){
              eventInfo.changedApplicants.push({"personId":householdPersonId,"eventDate":changeDate});
            }else{
              eventInfo.changedApplicants.push({"personId":householdPersonId});
            }

          }
        }
      }

       function removeChangedApplicants(householdPersonId){

           //if changedApplicants is empty then return from here
           if(eventInfo.changedApplicants.length == 0){
             return;
           }else{
             //iterated the changedApplicants array and find if household already added
             for(var i=0;i<eventInfo.changedApplicants.length;i++){
                 //if household is  in changedApplicatnts then remove it.
                 if(householdPersonId == eventInfo.changedApplicants[i].personId){
                     eventInfo.changedApplicants.splice(i,1);
                   break;
                 }
             }
           }
         }

      function initEventInfo(){
        eventInfo.eventCategory='';
        eventInfo.eventSubCategory = '';
        eventInfo.changedApplicants =[];
      }

      function removeHousehold(){
         var lastHouseholdPersonId = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length -1].personId;
         if(maritalHouseholdAdded==true){
           ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
           maritalHouseholdAdded=false;
           eventInfo.changedApplicants =[];
           removeRelationship(lastHouseholdPersonId);
         }
      }

      function removeDependentFromJson(){
    	  var lastHouseholdPersonId = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length -1].personId;
          if(dependentForHouseholdAdded==true){
            ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.splice(ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1, 1);
            dependentForHouseholdAdded =false;
            dependentAdddedAtCounter=0;
            eventInfo.changedApplicants =[];
            removeRelationship(lastHouseholdPersonId);
          }
          return lastHouseholdPersonId;
      }

      //This function takes household and updates its address same as first member of household array
      function changeAddressOfHouseHold( lastHousehold){
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
        var primaryHouseholdObj=findHouseholdByPersonId(1);
        if(!lastHousehold.livesAtOtherAddressIndicator){

        // update flags for Address
        lastHousehold.livesWithHouseholdContactIndicator = true;
        lastHousehold.householdContact.homeAddressIndicator = true;
        
          
          lastHousehold.householdContact.homeAddress.streetAddress1 = primaryHouseholdObj.householdContact.homeAddress.streetAddress1;
          lastHousehold.householdContact.homeAddress.streetAddress2 = primaryHouseholdObj.householdContact.homeAddress.streetAddress2;
          lastHousehold.householdContact.homeAddress.city = primaryHouseholdObj.householdContact.homeAddress.city;
          lastHousehold.householdContact.homeAddress.state = primaryHouseholdObj.householdContact.homeAddress.state;
          lastHousehold.householdContact.homeAddress.postalCode = primaryHouseholdObj.householdContact.homeAddress.postalCode;
          lastHousehold.householdContact.homeAddress.county = primaryHouseholdObj.householdContact.homeAddress.county;
          lastHousehold.householdContact.homeAddress.primaryAddressCountyFipsCode = primaryHouseholdObj.householdContact.homeAddress.primaryAddressCountyFipsCode;
          //change otherAddress to default
          lastHousehold.otherAddress.address.streetAddress1 = '';
          lastHousehold.otherAddress.address.streetAddress2 = '';
          lastHousehold.otherAddress.address.city = '';
          lastHousehold.otherAddress.address.state = '';
          lastHousehold.otherAddress.address.postalCode = '';
          lastHousehold.otherAddress.address.county = '';
          lastHousehold.otherAddress.address.primaryAddressCountyFipsCode = null;
          changeTempAddressOfHouseHold(lastHousehold);
        }
        else{
        // update flags for Address
        lastHousehold.livesWithHouseholdContactIndicator = false;
        lastHousehold.householdContact.homeAddressIndicator = false;
        lastHousehold.householdContact.mailingAddressSameAsHomeAddressIndicator = false;
          lastHousehold.householdContact.homeAddress.streetAddress1 = '';
          lastHousehold.householdContact.homeAddress.streetAddress2 = '';
          lastHousehold.householdContact.homeAddress.city = '';
          lastHousehold.householdContact.homeAddress.state = '';
          lastHousehold.householdContact.homeAddress.postalCode = '';
          lastHousehold.householdContact.homeAddress.county = '';
          lastHousehold.householdContact.homeAddress.primaryAddressCountyFipsCode=null;
        }

      //copy mailing address from Primary household, this will always be executed
        lastHousehold.householdContact.mailingAddress.streetAddress1 = primaryHouseholdObj.householdContact.mailingAddress.streetAddress1;
        lastHousehold.householdContact.mailingAddress.streetAddress2 = primaryHouseholdObj.householdContact.mailingAddress.streetAddress2;
        lastHousehold.householdContact.mailingAddress.city = primaryHouseholdObj.householdContact.mailingAddress.city;
        lastHousehold.householdContact.mailingAddress.state = primaryHouseholdObj.householdContact.mailingAddress.state;
        lastHousehold.householdContact.mailingAddress.postalCode = primaryHouseholdObj.householdContact.mailingAddress.postalCode;
        lastHousehold.householdContact.mailingAddress.county = primaryHouseholdObj.householdContact.mailingAddress.county;

      }

      function changeTempAddressOfHouseHold( lastHousehold){
        try {
          if(!lastHousehold.otherAddress.livingOutsideofStateTemporarilyIndicator | (!lastHousehold.livesAtOtherAddressIndicator)){
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyIndicator=false;
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode='';
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.county='';
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.primaryAddressCountyFipsCode=null;
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.streetAddress1='';
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.streetAddress2='';
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.state='';
            lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.city='';
          }
        }catch(err) {
          //Try catch is required when we access properties on undefined fields.
        }
      }

      ///////////////////////MODAL FOR LAWFUL PRESENCE/////////////////////////////

      newMexico.directive('modalDialog', function() {
        return {
          restrict: 'E',
          scope: {
            show: '=',
            modalHouseholdMember:'=',
          },
          replace: true, // Replace with the template below
          transclude: true, // we want to insert custom content inside the directive
          link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
              scope.dialogStyle.width = attrs.width;
            if (attrs.height)
              scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
              scope.show = false;
            };
          },
          templateUrl: "modaltemplate.jsp"
        };
      });

      newMexico.directive('modalDialogStaticBackdrop', function() {
          return {
            restrict: 'E',
            scope: {
              show: '=',
              modalHouseholdMember:'=',
              modalEventType:'=',
            },
            replace: true, // Replace with the template below
            transclude: true, // we want to insert custom content inside the directive
            link: function(scope, element, attrs) {
              scope.dialogStyle = {};
              if (attrs.width)
                scope.dialogStyle.width = attrs.width;
              if (attrs.height)
                scope.dialogStyle.height = attrs.height;
              scope.dialogStyle.backdrop='static';
              scope.hideModal = function() {
                scope.show = false;
              };
            },
            templateUrl: "modaltemplate.jsp"
          };
       });

      newMexico.directive('modalSmall', function() {
        return {
          restrict: 'E',
          scope: {
            show: '=',
            modalHouseholdMember:'=',
            modalEventType:'='
          },
          replace: true, // Replace with the template below
          transclude: true, // we want to insert custom content inside the directive
          link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
              scope.dialogStyle.width = attrs.width;
            if (attrs.height)
              scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
              scope.show = false;
            };
          },
          templateUrl: "modaltemplatesmall.jsp"
        };
      });

      newMexico.directive('modalTiny', function() {
        return {
          restrict: 'E',
          scope: {
            show: '=',
            modalHouseholdMember:'=',
            modalEventType:'='
          },
          replace: true, // Replace with the template below
          transclude: true, // we want to insert custom content inside the directive
          link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
              scope.dialogStyle.width = attrs.width;
            if (attrs.height)
              scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
              scope.show = false;
            };
          },
          templateUrl: "modaltemplatetiny.jsp"
        };
      });


      newMexico.directive('modalSaving', function() {
        return {
          restrict: 'E',
          scope: {
            show: '=',
            modalHouseholdMember:'=',
            modalEventType:'='
          },
          replace: true, // Replace with the template below
          transclude: true, // we want to insert custom content inside the directive
          link: function(scope, element, attrs) {
            scope.dialogStyle = {};
            if (attrs.width)
              scope.dialogStyle.width = attrs.width;
            if (attrs.height)
              scope.dialogStyle.height = attrs.height;
            scope.hideModal = function() {
              scope.show = false;
            };
          },
          templateUrl: "modaltemplatetiny-saving.jsp"
        };
      });

      //////////////////////////Controller for change in address
      newMexico.controller('changeInAddress', ['$scope', '$http','$timeout', function($scope, $http,$timeout) {
    	//eventInfo.eventSubCategory = ADDRESS_CHANGE[0].name;// 'ADDRESS_CHANGE';
     	
      $scope.eventInfo=eventInfo;
      $scope.disableHomeAddress = false;
      $scope.stateArray=getStates();
      $scope.compareMailingAddress = function (){
    	  compareMailingAddress();  
      };
      var primaryHouseholdObj=findHouseholdByPersonId(1);
      $scope.countyArray=null;
      $scope.modalShown = false;
      $scope.eventInfoArrayTemp=eventInfoArrayTemp;
      $scope.originalIndex=0;
      $scope.householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
      var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
	  var appliedZIndex=false;
	  $scope.dummyPrimaryApplicant=angular.copy(primaryHouseholdObj);

        /** Method to asynchronous populate counties for selected state. */
        $scope.ajaxRequestForLoadCounties = function (stateCode, zipCode,countyType){
            var urlForCounties = '<c:url value="/ssap/addCountiesForStates"/>?';
            var localCSRF = {csrftoken : '<df:csrfToken plainToken="true"/>'};
              $http({method: 'POST', url: urlForCounties,params:{stateCode : stateCode,zipCode:zipCode},data:localCSRF}).
                   success(function(data, status, headers, config) {
                	   switch (countyType) {
                	   		case 0:{
                	   		 	$scope.countiesother = data;
                	   		 	break;
                	   		}
                	   		case 1:{
                	   			$scope.counties = data;
                	   			break;
                	   		}
                	   }

                   }).
                   error(function(data, status, headers, config) {
                       $scope.counties = {};
                       $scope.status = status;
                   });
         };


        $(eventInfoArray).each(function(key,value) {
        	   if(value.eventCategory === 'ADDRESS_CHANGE' || value.eventCategory === 'DEMOGRAPHICS'  ){
        		   $scope.eventInfo.eventSubCategory=value.eventCategory;
        		   $scope.eventInfo.eventSubCategory = value.eventSubCategory;
        		   if($scope.eventInfo.eventSubCategory === 'MAILING_ADDRESS_CHANGE'){
        	        	$scope.newOptions=false;
        	        	$scope.dummyPrimaryApplicant=angular.copy(findHouseholdByPersonId(1));
        	        	$scope.dummyPriCountyFips=$scope.dummyPrimaryApplicant.householdContact.mailingAddress.county;
        	        	$scope.ajaxRequestForLoadCounties($scope.dummyPrimaryApplicant.householdContact.mailingAddress.state,$scope.dummyPrimaryApplicant.householdContact.mailingAddress.postalCode,0);
        	       }else{
        	    	   $scope.newOptions=true;
        	       }
        		   eventInfoArray.splice(key,1);
        		   return false;
        	   }
        });


        function appalyZindex() {
        	$('#wrapperToHomeAddress').find('*').each(function () {
        		 $(this).css("z-index", 1048);
        	});
         };

         $scope.modelOK = function(householdCloned,indexFromModel,addressChangeDate) {
        	 householdArray[indexFromModel] = householdCloned;//here we are assigning updated cloned household to original houseHold in array.
             
        	 //If this event is Home address change && PrimaryApplicant && mailing address is same as home address... Then  update mailing address as home address.
        	 if(  (!($scope.eventInfo.eventSubCategory === 'MAILING_ADDRESS_CHANGE')) && householdArray[indexFromModel].personId==1 && (householdArray[indexFromModel].householdContact.mailingAddressSameAsHomeAddressIndicator)    ){
        		 $scope.compareMailingAddress();
        		 
        		 /* householdArray[indexFromModel].householdContact.mailingAddress.streetAddress1 	=	householdArray[indexFromModel].householdContact.homeAddress.streetAddress1;
        		 householdArray[indexFromModel].householdContact.mailingAddress.streetAddress2 	=	householdArray[indexFromModel].householdContact.homeAddress.streetAddress2;
        		 householdArray[indexFromModel].householdContact.mailingAddress.city 			=	householdArray[indexFromModel].householdContact.homeAddress.city;
        		 householdArray[indexFromModel].householdContact.mailingAddress.state			=	householdArray[indexFromModel].householdContact.homeAddress.state;
        		 householdArray[indexFromModel].householdContact.mailingAddress.postalCode		=	householdArray[indexFromModel].householdContact.homeAddress.postalCode;
        		 householdArray[indexFromModel].householdContact.mailingAddress.county			=	householdArray[indexFromModel].householdContact.homeAddress.county;
        		 householdArray[indexFromModel].householdContact.mailingAddress.primaryAddressCountyFipsCode			=	householdArray[indexFromModel].householdContact.homeAddress.primaryAddressCountyFipsCode;
        		 $scope.updateMailingAddressOfSecondaryHouseHolds(); */ 
        	 }
             $scope.submitted=false;
             $scope.closeModal();
          };

          $scope.toggleModal = function(houseHoldFromParent,originalIndex) {

        	 if( (houseHoldFromParent.personId === 1 || houseHoldFromParent.personId === '1') || ( houseHoldFromParent.livesAtOtherAddressIndicator === false || houseHoldFromParent.livesAtOtherAddressIndicator === 'false' || houseHoldFromParent.livesAtOtherAddressIndicator === undefined || houseHoldFromParent.livesAtOtherAddressIndicator === null  )  ){
        		 $scope.ajaxRequestForLoadCounties(houseHoldFromParent.householdContact.homeAddress.state,houseHoldFromParent.householdContact.homeAddress.postalCode,0);
        	 }else {
        		 $scope.ajaxRequestForLoadCounties(houseHoldFromParent.otherAddress.address.state,houseHoldFromParent.otherAddress.address.postalCode,0);
        	 }

           $scope.modalHouseholdMember=angular.copy(houseHoldFromParent);
           $scope.modalShown = !$scope.modalShown;
           $scope.originalIndex=originalIndex;
           $scope.submitted=false;
           if(!appliedZIndex){
        	   appliedZIndex=true;
        	   $timeout(appalyZindex, 2000);
           }
			setTimeout(function(){
 	          $("#changeInAddressCal").children('input').each(function () {
 	              if(!(this.value)){
 	               $("#changeInAddressCal").datetimepicker('setDate',new Date());
 	               $("#changeInAddressCal").datetimepicker('update');
 	               this.value='';
 	               return;
 	              }
 	          });
			
 	          $("#changeInAddressCal").datetimepicker('update'); 
 	             
           }, 700);
         };
         $scope.closeModal= function() {
        	 $scope.showWarningOnChange = false;
        	 $scope.submitted=false;
             $scope.modalShown=false;
           };
         $scope.getOriginalIndex=function(content,array){
           return  $.inArray(content,array);
         };
 		$scope.getDummyPrimaryApplicant=function(){
 			 $scope.dummyPrimaryApplicant=angular.copy(findHouseholdByPersonId(1));
 			 $scope.dummyPriCountyFips=$scope.dummyPrimaryApplicant.householdContact.mailingAddress.county;
 			 $scope.ajaxRequestForLoadCounties($scope.dummyPrimaryApplicant.householdContact.mailingAddress.state,$scope.dummyPrimaryApplicant.householdContact.mailingAddress.postalCode,0);
         };

        $scope.changeAddressOfMember=function(lastHousehold){
          var primaryHouseholdObjTemp=findHouseholdByPersonId(1);
          if(!lastHousehold.livesAtOtherAddressIndicator){//This means secondary household stays with primary house-hold

        	lastHousehold.livesWithHouseholdContactIndicator = true;
            lastHousehold.householdContact.homeAddressIndicator = true;
			
            lastHousehold.householdContact.homeAddress.streetAddress1 = primaryHouseholdObjTemp.householdContact.homeAddress.streetAddress1;
            lastHousehold.householdContact.homeAddress.streetAddress2 = primaryHouseholdObjTemp.householdContact.homeAddress.streetAddress2;
            lastHousehold.householdContact.homeAddress.city = primaryHouseholdObjTemp.householdContact.homeAddress.city;
            lastHousehold.householdContact.homeAddress.state = primaryHouseholdObjTemp.householdContact.homeAddress.state;
            lastHousehold.householdContact.homeAddress.postalCode = primaryHouseholdObjTemp.householdContact.homeAddress.postalCode;
            lastHousehold.householdContact.homeAddress.county = primaryHouseholdObjTemp.householdContact.homeAddress.county;
            lastHousehold.householdContact.homeAddress.primaryAddressCountyFipsCode = primaryHouseholdObjTemp.householdContact.homeAddress.primaryAddressCountyFipsCode;

            //change otherAddress to default
            lastHousehold.otherAddress.address.streetAddress1 = '';
            lastHousehold.otherAddress.address.streetAddress2 = '';
            lastHousehold.otherAddress.address.city = '';
            lastHousehold.otherAddress.address.state = '';
            lastHousehold.otherAddress.address.postalCode = '';
            lastHousehold.otherAddress.address.county = '';
            lastHousehold.otherAddress.address.primaryAddressCountyFipsCode = null;

            changeTempAddressOfHouseHold(lastHousehold);
          }else{
        	  lastHousehold.livesWithHouseholdContactIndicator = false;
              lastHousehold.householdContact.homeAddressIndicator = false;
              lastHousehold.householdContact.mailingAddressSameAsHomeAddressIndicator = false;
              lastHousehold.householdContact.homeAddress.streetAddress1 = '';
              lastHousehold.householdContact.homeAddress.streetAddress2 = '';
              lastHousehold.householdContact.homeAddress.city = '';
              lastHousehold.householdContact.homeAddress.state = '';
              lastHousehold.householdContact.homeAddress.postalCode = '';
              lastHousehold.householdContact.homeAddress.county = '';
              lastHousehold.householdContact.homeAddress.primaryAddressCountyFipsCode = null;
          }

        /* if (lastHousehold.householdContact.homeAddressIndicator == false  ) {


            lastHousehold.householdContact.homeAddress.streetAddress1 = householdArray[0].householdContact.homeAddress.streetAddress1;
            lastHousehold.householdContact.homeAddress.streetAddress2 = householdArray[0].householdContact.homeAddress.streetAddress2;
            lastHousehold.householdContact.homeAddress.city = householdArray[0].householdContact.homeAddress.city;
            lastHousehold.householdContact.homeAddress.state = householdArray[0].householdContact.homeAddress.state;
            lastHousehold.householdContact.homeAddress.postalCode = householdArray[0].householdContact.homeAddress.postalCode;
            lastHousehold.householdContact.homeAddress.county = householdArray[0].householdContact.homeAddress.county;

          }
          if (lastHousehold.householdContact.mailingAddressSameAsHomeAddressIndicator == false) {
            lastHousehold.householdContact.mailingAddress.streetAddress1 = householdArray[0].householdContact.homeAddress.streetAddress1;
            lastHousehold.householdContact.mailingAddress.streetAddress2 = householdArray[0].householdContact.homeAddress.streetAddress2;
            lastHousehold.householdContact.mailingAddress.city = householdArray[0].householdContact.homeAddress.city;
            lastHousehold.householdContact.mailingAddress.state = householdArray[0].householdContact.homeAddress.state;
            lastHousehold.householdContact.mailingAddress.postalCode = householdArray[0].householdContact.homeAddress.postalCode;
            lastHousehold.householdContact.mailingAddress.county = householdArray[0].householdContact.homeAddress.county;

          } */

        }

        $scope.changeTempAddressOfHouseHold=function(lastHousehold) {
          
          try {
            if (!lastHousehold.otherAddress.livingOutsideofStateTemporarilyIndicator
                | (!lastHousehold.livesAtOtherAddressIndicator)) {
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyIndicator = false;
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = '';
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.county = '';
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.primaryAddressCountyFipsCode = null;
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.streetAddress1 = '';
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.streetAddress2 = '';
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.state = '';
              lastHousehold.otherAddress.livingOutsideofStateTemporarilyAddress.city = '';
            }
          } catch (err) {
            //Try catch is required when we access properties on undefined fields.
          }
        };

		$scope.updateHomeAddressOfSecondaryHouseHolds=function(updateAllHouseholdAddress) {
			var primaryHouseholdObjTemp=findHouseholdByPersonId(1);
        	 $(householdArray).each(function(key,value) {
        		if( (!(value.personId === 1 || value.personId === '1')) && ( updateAllHouseholdAddress ) && $scope.activeDeathFilter(value)){
        			eventInfoArrayTemp[key].changeInAddressDate = eventInfoArrayTemp[0].changeInAddressDate;
        		 	value.livesWithHouseholdContactIndicator = true;
        		 	value.householdContact.homeAddressIndicator = true;
        		 	value.livesAtOtherAddressIndicator=false;

        		 	value.householdContact.homeAddress.streetAddress1 = primaryHouseholdObjTemp.householdContact.homeAddress.streetAddress1;
        			value.householdContact.homeAddress.streetAddress2 = primaryHouseholdObjTemp.householdContact.homeAddress.streetAddress2;
        			value.householdContact.homeAddress.city = primaryHouseholdObjTemp.householdContact.homeAddress.city;
        			value.householdContact.homeAddress.state = primaryHouseholdObjTemp.householdContact.homeAddress.state;
        			value.householdContact.homeAddress.postalCode = primaryHouseholdObjTemp.householdContact.homeAddress.postalCode;
        			value.householdContact.homeAddress.county = primaryHouseholdObjTemp.householdContact.homeAddress.county;
        			value.householdContact.homeAddress.primaryAddressCountyFipsCode = primaryHouseholdObjTemp.householdContact.homeAddress.primaryAddressCountyFipsCode;
        			
        			value.otherAddress.address.streetAddress1 = '';
        			value.otherAddress.address.streetAddress2 = '';
        			value.otherAddress.address.city = '';
        			value.otherAddress.address.state = '';
        			value.otherAddress.address.postalCode = '';
        			value.otherAddress.address.county = '';
        			value.otherAddress.address.primaryAddressCountyFipsCode = null;

        		}
          	});
        };
		
        $scope.updateMailingAddressOfSecondaryHouseHolds=function() {
        	var primaryHouseholdObjTemp=findHouseholdByPersonId(1);
          	 $(householdArray).each(function(key,value) {
          		if( (!(value.personId === 1 || value.personId === '1'))) {
          			//eventInfoArrayTemp[key].changeInAddressDate = eventInfoArrayTemp[0].changeInAddressDate;
          		 	//value.livesWithHouseholdContactIndicator = true;
          		 	//value.householdContact.homeAddressIndicator = true;
          		 	//value.livesAtOtherAddressIndicator=false;

          		 	value.householdContact.mailingAddress.streetAddress1 = primaryHouseholdObjTemp.householdContact.mailingAddress.streetAddress1;
          			value.householdContact.mailingAddress.streetAddress2 = primaryHouseholdObjTemp.householdContact.mailingAddress.streetAddress2;
          			value.householdContact.mailingAddress.city = primaryHouseholdObjTemp.householdContact.mailingAddress.city;
          			value.householdContact.mailingAddress.state = primaryHouseholdObjTemp.householdContact.mailingAddress.state;
          			value.householdContact.mailingAddress.postalCode = primaryHouseholdObjTemp.householdContact.mailingAddress.postalCode;
          			value.householdContact.mailingAddress.county = primaryHouseholdObjTemp.householdContact.mailingAddress.county;
          			value.householdContact.mailingAddress.primaryAddressCountyFipsCode = primaryHouseholdObjTemp.householdContact.mailingAddress.primaryAddressCountyFipsCode;
          			

          		}
            	});
          };
        
          /** Method to asynchronous populate counties for selected state. */
          $scope.loadCounties = function(stateCode,zipCode,countyType) {
            if((stateCode == undefined || stateCode == '') || zipCode == undefined || zipCode == '' || zipCode.length != 5){
              return;
            }

          $scope.ajaxRequestForLoadCounties(stateCode,zipCode,countyType);

          };
			
          $scope.resetValues = function (){
          	var dataChanged = false;
          	var dataChangedInEvent=0;
          	var householdCloned = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
          	var primaryHouseholdObjTemp=findHouseholdByPersonId(1);
  			for(var i=0;i<householdArray.length;i++){
  				if($scope.activeDeathFilter(householdArray[i])){
  					if(   (!(angular.equals(householdCloned[i].householdContact,householdArray[i].householdContact))) || (!(angular.equals(householdCloned[i].otherAddress,householdArray[i].otherAddress)))  ){
  						dataChanged = true;
  						dataChangedInEvent=0;
  						//reset
  						householdArray[i].householdContact = angular.copy(householdCloned[i].householdContact);
  						householdArray[i].otherAddress = angular.copy(householdCloned[i].otherAddress);
  						householdArray[i].livesAtOtherAddressIndicator= angular.copy(householdCloned[i].livesAtOtherAddressIndicator);
  					}else if(    !angular.equals(  $scope.dummyPrimaryApplicant.householdContact.mailingAddress,primaryHouseholdObjTemp.householdContact.mailingAddress   )   ){
  						dataChanged = true;
  						dataChangedInEvent=1;
  						$scope.dummyPrimaryApplicant=angular.copy(primaryHouseholdObjTemp);
  					}
  				}
  	        }
  			if(dataChanged){
  				$scope.showWarningOnChange = true;
  				switch(dataChangedInEvent){
  				case 0:
  					$scope.previousEventName = 'Change of home address';
  	  				$scope.currentEventName = 'Change of mailing address';
  					break;
  				case 1:
  					$scope.previousEventName ='Change of mailing address';
  	  				$scope.currentEventName =  'Change of home address';
  					break;
  				}
  				
  			}
  	
          };
          $scope.changeTempAddress=function(popUpHousehold){
              changeTempAddressOfHouseHold(popUpHousehold);
          };
          
          //mailing address update from comm preferences
          $scope.mailingAddressUpdate=false;
          if(mailingAddressUpdate == 'Y'){
        	  $scope.eventInfo.eventSubCategory = 'MAILING_ADDRESS_CHANGE';
        	  $scope.disableHomeAddress = true;
        	  $scope.getDummyPrimaryApplicant();
        	  $scope.mailingAddressUpdate=true;
          }
          
          
          $scope.handleAddressSelect = function(dummyPrimaryApplicant,date){
        	  $scope.loadCounties(dummyPrimaryApplicant.householdContact.mailingAddress.state,dummyPrimaryApplicant.householdContact.mailingAddress.postalCode,0);
        	  var index = getIndexByHouseholdByPersonId(1);
        	  $scope.modelOK(dummyPrimaryApplicant,index,date);
    		  $scope.compareMailingAddress();
    		  $scope.updateMailingAddressOfSecondaryHouseHolds();
    		  $scope.next();  
          }
          
          $scope.updateMailingAddress = function(dummyPrimaryApplicant,index,date){
        	  $('#home_primary_zip').trigger('validateAddress',[dummyPrimaryApplicant,index,date]);
    	  }
          
          
          

      }]);
      
      
      

      //////////////////////////Controller for lawful presence
      newMexico.controller('lawfulModal', ['$scope', 'myService', function($scope, myService) {
        eventInfo.eventSubCategory = IMMIGRATION_STATUS_CHANGE[0].name;//'LAWFUL_PRESENCE'
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        $scope.modalShown = false;
        $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
          $scope.modalHouseholdMember= angular.copy(houseHoldFromParent);
          $scope.modalEventType = 'immigrationStatus';
          $scope.modalShown = !$scope.modalShown;
          $scope.originalIndex=originalIndex;
          $scope.dummySevisIdA='';
          $scope.dummySevisIdB=''; 
          if(houseHoldFromParent.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && houseHoldFromParent.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'NaturalizationCertificate'){
            $scope.tempNaturalizedOrCOC=true;
            $scope.dummySevisIdA=houseHoldFromParent.citizenshipImmigrationStatus.citizenshipDocument.SEVISId;
            $scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId='';
          }else if(houseHoldFromParent.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && houseHoldFromParent.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'CitizenshipCertificate'){
            $scope.tempNaturalizedOrCOC=false;
            $scope.dummySevisIdB=houseHoldFromParent.citizenshipImmigrationStatus.citizenshipDocument.SEVISId;
            $scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.SEVISId='';
          }
          
          
          setTimeout(function(){
              $("#lawfulPresenceDateDiv").children('input').each(function () {
                  if(!(this.value)){
                   $("#lawfulPresenceDateDiv").datetimepicker('setDate',new Date());
                   $("#lawfulPresenceDateDiv").datetimepicker('update');
                   this.value='';
                   return;
                  }
              });
              
              $("#lawfulPresenceDateDiv").datetimepicker('update'); 
                 
             }, 700);
          
          
        };
        $scope.setFormScope= function(scope){
        	   this.formScope = scope;
        	}
        $scope.showInvalidFields = function() {
            var error = this.formScope.formCitizenship.$error;
             angular.forEach(error.pattern, function(field){
                 if(field.$invalid){
                     var fieldName = field.$name;
                     alert(fieldName);
                 }
             });
             angular.forEach(error.required, function(field){
                 if(field.$invalid){
                     var fieldName = field.$name;
                     alert(fieldName);
                 }
             });
          };



        $scope.closeModal= function() {
          $scope.modalShown=false;
        };

        $scope.getOriginalIndex=function(content,array){
            return  $.inArray(content,array);
        };

        //add temp array to controller scope... this is code is required if event-date is to be inserted for each individual.

          var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
          $scope.modelOK = function(householdCloned,indexFromModel) {
        	  if(householdCloned.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && householdCloned.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'NaturalizationCertificate'){
        		  householdCloned.citizenshipImmigrationStatus.citizenshipDocument.SEVISId=$scope.dummySevisIdA;
                }else if(householdCloned.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator && householdCloned.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected === 'CitizenshipCertificate'){
                  householdCloned.citizenshipImmigrationStatus.citizenshipDocument.SEVISId= $scope.dummySevisIdB;
                }
          	householdArray[indexFromModel] = householdCloned;
          };

        $scope.updateNameOnDocument= function(sameName){
            	if(sameName){
            		$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = $scope.modalHouseholdMember.name.firstName;
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName= $scope.modalHouseholdMember.name.middleName;
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = $scope.modalHouseholdMember.name.lastName;
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix= $scope.modalHouseholdMember.name.suffix;
            	}else{
            		$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = '';
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName= '';
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = '';
               	 	$scope.modalHouseholdMember.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix= '';
            	}

            };

            $scope.resetDocumentTypes= function(selectedHouseHold){
				var selectedType=selectedHouseHold.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected;
            	var documentTypeObject = selectedHouseHold.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0];
            	var alienNumber = 1;
            	var cardNumber = 2;
            	var expirationDate =4;
            	var ForeignPassportCountryofIssuance = 8;
            	var i94Number =16;
            	var passportNumber = 32;
            	var SEVISid = 64;
            	var visaNumber  =128;
            	var appliedNumber=0;
            	//Assign false to eligibleImmigrationDocumentType variables.
            	 for(var key in documentTypeObject){
                     //var attrName = key;
                     documentTypeObject[key]=false;
                     //var attrValue = ;
                 }
        	    	switch(selectedType) {
        	    		case "I551" :{ //permResident
            				appliedNumber= 7  ;
            				documentTypeObject.I551Indicator=true;
            				break;
            			}
	            		case "TempI551" :{//temp
    	        			appliedNumber= 45  ;
    	        			documentTypeObject.TemporaryI551StampIndicator =true;
        	    			break;
        	    		}
            			case "MacReadI551" :{ //machineReadable
            				appliedNumber= 173  ;
            				documentTypeObject.MachineReadableVisaIndicator =true;
            				break;
            			}
            			case "I766" :{//employAuth
            				appliedNumber= 7  ;
            				documentTypeObject.I766Indicator =true;
            				break;
            			}
            			case "I94" :{//i94
            				appliedNumber= 84  ;
            				documentTypeObject.I94Indicator =true;
            				break;
            			}
            			case "I94UnexpForeignPassport" :{//    i94Unexpired
            				appliedNumber= 252  ;
            				documentTypeObject.I94InPassportIndicator =true;
            				break;
            			}
            			case "I797" :{ //noticeofaction
            				appliedNumber= 17  ;
            				documentTypeObject.I797Indicator =true;
            				selectedHouseHold.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator = false;
            				break;
            			}
            			case "UnexpForeignPassport" :{//UnexpForeignPassport
            				appliedNumber= 124  ;
            				documentTypeObject.UnexpiredForeignPassportIndicator =true;
            				break;
            			}
            			case "I327" :{// reentryPermit
            				appliedNumber= 5  ;
            				documentTypeObject.I327Indicator =true;
            				break;
            			}
            			case "I571" :{//  refugeeTravel
            				appliedNumber=  5 ;
            				documentTypeObject.I571Indicator =true;
            				break;
            			}
            			case "I20" :{//nonimmigrantStudent
            				appliedNumber= 124  ;
            				documentTypeObject.I20Indicator =true;
            				break;
            			}
            			case "DS2019" :{//exchangeVisitor
            				appliedNumber= 116 ;
            				documentTypeObject.DS2019Indicator =true;
            				break;
            			}
            		}//End switch
        	    	if( !((alienNumber & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = undefined;
        	    	}
        	    	if( !((cardNumber & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.cardNumber = undefined;
        	    	}
        	    	if( !((expirationDate & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = undefined;
        	    	}
        	    	if( !((ForeignPassportCountryofIssuance & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = undefined;
        	    	}
        	    	if( !((i94Number & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.i94Number = undefined;
        	    	}
        	    	if( !((passportNumber & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = undefined;
        	    	}
        	    	if( !((SEVISid & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = undefined;
        	    	}
        	    	if( !((visaNumber  & appliedNumber)>0)){
        	    		selectedHouseHold.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = undefined;
        	    	}

            };

      }]);

      //////////////////////////Controller for tribe change
      newMexico.controller('tribeChangeController', ['$scope', 'myService', function($scope, myService) {
        eventInfo.eventSubCategory = TRIBE_STATUS[0].name;// 'TRIBE_STATUS'
        $scope.tribeChangeStatusArray= TRIBE_STATUS;
        $scope.stateArray=getStatesForTribe();
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
        // $scope.tribes= ['Lakota','Navaho','Omaha'];
        $scope.ajaxRequestForTribes =  function ajaxRequestForTribes(stateCode){
            $.ajax({
            type : "POST",
            url :  '<c:url value="/ssap/getAlaskaAmericanStatesTribe"/>?${df:csrfTokenParameter()}='+'<df:csrfToken plainToken="true"/>',
            data : {stateCode : stateCode},
            dataType:'json',
            success : function(response) {
               $scope.tribes =  response;
               $scope.$apply();
            },error : function(e) {
              $scope.tribes =  {};
              $scope.$apply();
            }
          });

          };
        $scope.modalShown = false;
        $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
        	if(houseHoldFromParent.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator === true || houseHoldFromParent.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator === 'true'){
        		$scope.ajaxRequestForTribes(houseHoldFromParent.americanIndianAlaskaNative.state);
        	}
            $scope.modalHouseholdMember = angular.copy(houseHoldFromParent);
            $scope.modalEventType = 'tribe'; 
            $scope.modalShown = !$scope.modalShown;
            $scope.originalIndex=originalIndex;

            setTimeout(function(){

            	$("#tribeChangeEventDate").children('input').each(function () {
            	    if(!(this.value)){
            	    	$("#tribeChangeEventDate").datetimepicker('setDate',new Date());
            	    	$("#tribeChangeEventDate").datetimepicker('update');
            	    	this.value='';
            	    	return;
            	    }
            	});

            	$("#tribeChangeEventDate").datetimepicker('update');

            }, 700);

        };

        $scope.modelOK = function(householdCloned) {
        	householdArray[$scope.originalIndex] = householdCloned;
        	$scope.closeModal();
        };

        $scope.getOriginalIndex=function(content,array){
          return  $.inArray(content,array);
        };

        $scope.closeModal= function() {
          $scope.modalShown=false;;
        };
        $scope.appalyZindex= function(event) {
          $('.bootstrap-datetimepicker-widget').each(function() {
            $(this).css("z-index", 300006);
          });
        };
        //added for loading tribes start
        $scope.ajaxRequestForTribes =  function ajaxRequestForTribes(stateCode){
            $.ajax({
            type : "POST",
            url :  '<c:url value="/ssap/getAlaskaAmericanStatesTribe"/>?${df:csrfTokenParameter()}='+'<df:csrfToken plainToken="true"/>',
            data : {stateCode : stateCode},
            dataType:'json',
            success : function(response) {
               $scope.tribes =  response;
               $scope.$apply();
            },error : function(e) {
              $scope.tribes =  {};
              $scope.$apply();
            }
          });

          };

        $scope.loadTribes = function(stateCode){
           //newHouseholdMember.americanIndianAlaskaNative.tribeName='';
           //newHouseholdMember.americanIndianAlaskaNative.tribeFullName='';
           $scope.ajaxRequestForTribes(stateCode);
        };

        //added for loading tribes end


      //add temp array to controller scope
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;


      }]);
      //////////////////////////Controller for name change
      newMexico.controller('incomeChangeController', ['$scope', function($scope) {
          eventInfo.eventSubCategory='INCOME';

          $scope.eventInfoArrayTemp=eventInfoArrayTemp;
          var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;

          $scope.getOriginalIndex=function(content,array){
            return  $.inArray(content,array);
          };

          $scope.updateEventInfo=function(){

            for (var i = 0; i < eventInfoArrayTemp.length; i++) {
              if (eventInfoArrayTemp[i].incomeChangeIndicator === true) {

                addChangedApplicants(householdArray[i].personId,eventInfoArrayTemp[i].incomeChangeDate);
               }
            }

          };

      }]);
      newMexico.controller('mainIncomeController', ['$scope', 'myService', function($scope, myService) {

        //Populate global array
        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
          var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;

        if(eventInfo.eventSubCategory=='INCOME'){
          var curEvent=myService.getCurrentEvent();
          var partialCount=myService.getPartialCount();
          var mainIncomePageCount=0;
          for(var i=0;i< partialCount;i++){
            if(curEvent.partials[i]=== 'maritalstatus-income.jsp'){
              mainIncomePageCount++;
            }
          }

          $scope.householdMember=findHouseholdByPersonId(1);

          var individualSelectionRank=0;
          for(var i=0;i<eventInfoArrayTemp.length;i++){
            if(eventInfoArrayTemp[i].incomeChangeIndicator === true){

              individualSelectionRank++;
              if(mainIncomePageCount === individualSelectionRank){
                $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[i];

                break;
              }

            }
          }

        }else{
          $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1];
        }
        myService.setMember($scope.householdMember);
        var houseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
        for(var i=0;i<houseHoldMemberArray.length;i++){
          if(houseHoldMemberArray[i].personId    === $scope.householdMember.personId){
            $scope.individualIncomeTypes= eventInfoArrayTemp[i].incomeTypes;
            $scope.hasOtherIncomeTypes=   eventInfoArrayTemp[i].hasOtherIncomeTypes;
            $scope.originalIndexHsHldIndex=i;
            break;
          }
        }


        $scope.loadPreviousHouseHold=function(){
          if(eventInfo.eventSubCategory=='INCOME'){
            //////////////////////
            var curEvent=myService.getCurrentEvent();
            var partialCount=myService.getPartialCount();
            partialCount--;
            //delete sub-income pages after current partial
            //Count pages
             var noOfIncomePagesInTemplates = getNoOfIncomeTemplatesInPartials(myService.getIncomeEvents(),curEvent.partials,partialCount);
            //remove the income page from the current partials
            if(noOfIncomePagesInTemplates>0){
              curEvent.partials.splice((partialCount+1), noOfIncomePagesInTemplates);
            }


            if(partialCount>0){
                  var mainIncomePageCount=0;
                  for(var i=0;i< partialCount;i++){
                    if(curEvent.partials[i]=== 'maritalstatus-income.jsp'){
                      mainIncomePageCount++;
                    }
                  }

                  $scope.householdMember=findHouseholdByPersonId(1);

                  var individualSelectionRank=0;
                  for(var i=0;i<eventInfoArrayTemp.length;i++){
                    if(eventInfoArrayTemp[i].incomeChangeIndicator === true){

                      individualSelectionRank++;
                      if(mainIncomePageCount === individualSelectionRank){
                        $scope.householdMember=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember[i];

                        break;
                      }

                    }
                  }


                  myService.setMember($scope.householdMember);
            }

          }
        };

      }]);


      //////////////////////////Controller for name change
      newMexico.controller('nameChangeController', ['$scope','$timeout','$rootScope', function($scope,$timeout,$rootScope) {
    	  
        $scope.demographics =DEMOGRAPHICS;//'CHANGE_IN_NAME'
        //household array
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
        $scope.modalShown = false;
        $scope.modalShownDOB = false;
        $scope.modalShownSSN = false;
        $scope.submitted = false;
        $scope.showWarningOnChange = false;
        $scope.oldEventValue = '';
		$scope.showRelChangeRadio=false;
		$scope.showRelChangeSelect=false;
		$scope.relationAction='';
		$scope.modalEventType = 'test';
		
		
		
		$scope.formValidations = {
				namechangeform:false,
				dobchange:false,
				maritalStatusSSNForm:false
	  };
	  $scope.tabs= {
				namechangeform:true,
				dobchange:false,
				maritalStatusSSNForm:false,
				ethnicityRaceChangeForm:false
	  };
        $scope.showModal = function(houseHoldFromParent,originalIndex,eventType) {
          $scope.formValidations = {
    				namechangeform:false,
    				dobchange:false,
    				maritalStatusSSNForm:false
    	  };
         
       	  var modalHouseholdMember = angular.copy(houseHoldFromParent);
          $scope.modalHouseholdMember= modalHouseholdMember;
          $scope.originalIndex=originalIndex;
    	  $scope.modalEventType= eventType; 
          switch(eventType){
          case 'tab':
            	//$scope.dateOfBirth = getDateInMMDDYYYY($scope.modalHouseholdMember.dateOfBirth);
            	$scope.modalShownTab = true;
            	populateEthnicityAndRaceInformation($scope.modalHouseholdMember);
            	setTimeout(function(){
                  $("#dobChangeCalenderDiv").children('input').each(function () {
                      if(!(this.value)){
                       $("#dobChangeCalenderDiv").datetimepicker('setDate',new Date());
                       $("#dobChangeCalenderDiv").datetimepicker('update');
                       this.value='';
                       return;
                      }
                  });
                  
                  $("#dobChangeCalenderDiv").datetimepicker('update'); 
                     
                 }, 700);
            	setTimeout(function(){
            		$scope.scrollToTop();
            	},100);
            	/** Populate SSN fields from JSON on load.*/
                $timeout(populateSSNFieldsFromJSON, 0);
              	setTimeout(function(){
                    $("#ssnChangeCalenderDiv").children('input').each(function () {
                        if(!(this.value)){
                         $("#ssnChangeCalenderDiv").datetimepicker('setDate',new Date());
                         $("#ssnChangeCalenderDiv").datetimepicker('update');
                         this.value='';
                         return;
                        }
                    });
                    
                    $("#ssnChangeCalenderDiv").datetimepicker('update'); 
                       
                   }, 700);
            	//$scope.$apply();
            	break;
          case 'dob':
          	//$scope.dateOfBirth = getDateInMMDDYYYY($scope.modalHouseholdMember.dateOfBirth);
          	$scope.modalShownDOB = true;
          	setTimeout(function(){
                $("#dobChangeCalenderDiv").children('input').each(function () {
                    if(!(this.value)){
                     $("#dobChangeCalenderDiv").datetimepicker('setDate',new Date());
                     $("#dobChangeCalenderDiv").datetimepicker('update');
                     this.value='';
                     return;
                    }
                });
                
                $("#dobChangeCalenderDiv").datetimepicker('update'); 
                   
               }, 700);
          	//$scope.$apply();
          break;
          case 'name':

          	$scope.modalShown = true;
          	setTimeout(function(){
                $("#nameChangeCalenderDiv").children('input').each(function () {
                    if(!(this.value)){
                     $("#nameChangeCalenderDiv").datetimepicker('setDate',new Date());
                     $("#nameChangeCalenderDiv").datetimepicker('update');
                     this.value='';
                     return;
                    }
                });
                
                $("#nameChangeCalenderDiv").datetimepicker('update'); 
                   
               }, 700);
          break;
          case 'ssn':
        	/** Populate SSN fields from JSON on load.*/
            $timeout(populateSSNFieldsFromJSON, 0);
          	$scope.modalShownSSN = true;
          	setTimeout(function(){
                $("#ssnChangeCalenderDiv").children('input').each(function () {
                    if(!(this.value)){
                     $("#ssnChangeCalenderDiv").datetimepicker('setDate',new Date());
                     $("#ssnChangeCalenderDiv").datetimepicker('update');
                     this.value='';
                     return;
                    }
                });
                
                $("#ssnChangeCalenderDiv").datetimepicker('update'); 
                   
               }, 700);
          break;
          case 'relChange':
        	  $scope.currentUserRelation= $scope.getRelation($scope.getRelationShipCodeBetweenHousehold(1,modalHouseholdMember.personId));
        	  $scope.relationAction='';
        	  	$scope.showRelChangeRadio=true;
      			//$scope.showRelChangeSelect=true;
          break;
          }
          
        };
		function populateEthnicityAndRaceInformation( modalHouseholdMember){
			   $scope.races= getRaces();
	           $scope.ethnicities=getEthnicities();
			   $scope.selectedraces=[];
               try{
                 $scope.selectedraces = modalHouseholdMember.ethnicityAndRace.race;
               }catch(err){
                 modalHouseholdMember.ethnicityAndRace =   { "hispanicLatinoSpanishOriginIndicator": null,"ethnicity": [], "race": [] };
                 $scope.selectedraces= modalHouseholdMember.ethnicityAndRace.race;
               }

               $scope.selectedEthnicities =  modalHouseholdMember.ethnicityAndRace.ethnicity;

  
               //Populate selected ethnicity checkboxes
               var index=0;
                var loopCounter=0;
                for(index=0  ; index <  $scope.selectedEthnicities.length ; index++){
                  for(loopCounter=0  ; loopCounter <  $scope.ethnicities.length ; loopCounter++){
                   if( $scope.selectedEthnicities[index].code ===  $scope.ethnicities[loopCounter].code){
                     $scope.ethnicities[loopCounter].checked=true;
                     //if Other is selected then populate text-box value
                     if($scope.selectedEthnicities[index].code === '000-0'){
                       $('#otherEthnicityText').val($scope.selectedEthnicities[index].label);
                       $scope.tempEthnicityHolder=$scope.selectedEthnicities[index].label;
                       $scope.ethnicities[loopCounter].value=$scope.ethnicities[loopCounter].label;
                     }
                     break;
                   }
                  }
                }
              //Populate selected race checkboxes
                for(index=0  ; index <  $scope.selectedraces.length ; index++){
                  for(loopCounter=0  ; loopCounter <  $scope.races.length ; loopCounter++){
                   if( $scope.selectedraces[index].code ===  $scope.races[loopCounter].code){
                     $scope.races[loopCounter].checked=true;
                   //if Other is selected then populate text-box value
                     if($scope.selectedraces[index].code === '000-0'){
                       $('#otherRaceText').val($scope.selectedraces[index].label);
                       $scope.tempRaceHolder=$scope.selectedraces[index].label;
                       $scope.races[loopCounter].value=$scope.races[loopCounter].label;
                     }
                     break;
                   }
                  }
                }
			
		}//End of populateEthnicityAndRaceInformation function.
        $scope.hideModal = function(eventType) {
        	 switch(eventType){
             case 'tab':
             	$scope.modalShownTab = false;
             break;
             case 'dob':
             	$scope.modalShownDOB = false;
             break;
             case 'name':
             	$scope.modalShown = false;
             break;
             case 'ssn':
             	$scope.modalShownSSN = false;
             break;
             case 'warningOnChange' :
             	$scope.showWarningOnChange = false;
             }
        	 $scope.submitted = false;
        };


        $scope.modelOK = function(householdCloned,originalIndex,eventType) {
        	  
        	 if(!$scope.isDataChanged(householdCloned,originalIndex)){
        		return;
        	}
        	if($scope.checkIf26YearOld(householdCloned) && householdCloned.applyingForCoverageIndicator){
        		$scope.showModal(householdCloned,originalIndex,'relChange');
        		return;
        	}  
        	
        	householdArray[originalIndex] = householdCloned;
        	$scope.hideModal(eventType);
        };


		$scope.updateNameOnSSNCard = function(householdMember){
				var scope = angular.element($("#firstNameOnSSNCard")).scope();
				householdMember.socialSecurityCard.firstNameOnSSNCard = householdMember.name.firstName;
				householdMember.socialSecurityCard.middleNameOnSSNCard = householdMember.name.middleName;
				householdMember.socialSecurityCard.lastNameOnSSNCard = householdMember.name.lastName;
				householdMember.socialSecurityCard.suffixOnSSNCard = householdMember.name.suffix;
		};

        $scope.getOriginalIndex=function(content,array){
            return  $.inArray(content,array);
         };

        $scope.eventInfoArrayTemp=eventInfoArrayTemp;


         //method for Update SSN
       function trim(rawStr) {
              return rawStr.replace(/^\s+|\s+$/g,"");
      }

        function populateSSNFieldsFromJSON() {
          var socialSecurityNumber = $scope.modalHouseholdMember.socialSecurityCard.socialSecurityNumber;

          if(socialSecurityNumber != null && socialSecurityNumber != undefined  && socialSecurityNumber !="") {
            var matches = socialSecurityNumber.match(/(\d{3})(\d{2})(\d{4})*/);

            if(null != matches && matches != undefined && matches.length > 0) {
              if(null != matches[1] && matches[1] != undefined) {
                $("#ssn1").val("***");
              }

              if(null != matches[2] && matches[2] != undefined) {
                $("#ssn2").val("**");
              }

              if(null != matches[3] && matches[3] != undefined) {
                $("#ssn3").val(trim(matches[3]));
              }
            }
          }else {
            $("#ssn1").val("");
            $("#ssn2").val("");
            $("#ssn3").val("");
          }
        }

        $scope.removeSSN = function(householdMember){
        	householdMember.socialSecurityCard.socialSecurityNumber = '';
        	$scope.ssn1 = '';
        	$scope.ssn2 = '';
        	$scope.ssn3 = '';
        	$('#ssn1').val('');
        	$('#ssn2').val('');
        	$('#ssn3').val('');
        	householdMember.socialSecurityCard.firstNameOnSSNCard = '';
        	householdMember.socialSecurityCard.middleNameOnSSNCard = '';
        	householdMember.socialSecurityCard.lastNameOnSSNCard = '';
        	householdMember.socialSecurityCard.suffixOnSSNCard = '';
        };


        $scope.resetValues = function (){
        	var dataChanged = false;
        	var householdCloned = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember;
			for(var i=0;i<householdArray.length;i++){
				if($scope.activeDeathFilter(householdArray[i])){
					if(!(angular.equals(householdCloned[i].name,householdArray[i].name) && angular.equals(householdCloned[i].socialSecurityCard,householdArray[i].socialSecurityCard) && angular.equals(householdCloned[i].dateOfBirth,householdArray[i].dateOfBirth))){
						dataChanged = true;
						//reset Name
						householdArray[i].name = angular.copy(householdCloned[i].name);
						//reset SSN
						householdArray[i].socialSecurityCard =angular.copy(householdCloned[i].socialSecurityCard);
						//reset DOB
						householdArray[i].dateOfBirth =angular.copy(householdCloned[i].dateOfBirth);
					}
				}
	        }
			if($scope.oldEventValue != "" && dataChanged){
				
				$scope.showWarningOnChange = true;
				$scope.previousEventName = $scope.getEventNameByCode($scope.oldEventValue);
				$scope.currentEventName = $scope.getEventNameByCode(eventInfo.eventSubCategory);
			}
			
			$scope.oldEventValue=eventInfo.eventSubCategory;
	
        };
        $scope.getEffectiveDate = function(){
			var applicationDate = new Date();
			var applicationDayOfMonth = applicationDate.getDate();


			var effectiveMonth = applicationDate.getMonth();
			var effectiveYear = applicationDate.getFullYear();
			var effectiveDate;

			var openEnrollmentDate = new Date('11/15/2014'); 
			
			if(applicationDate < openEnrollmentDate){
			    effectiveDate = new Date('01/01/2015'); 
			}else{
			    if(applicationDayOfMonth < 16){
			        //getMonth starts from 0
			        effectiveMonth += 2;
			    }else{
			        effectiveMonth += 3;
			    }
			    
			    if(effectiveMonth > 12){
			        effectiveMonth -= 12;
			        effectiveYear++;
			    }
			    
			    effectiveDate = new Date(effectiveMonth + "/01/" + effectiveYear);
			}
			
			
			return effectiveDate;

		};
		$scope.getEventNameByCode = function(eventCode){
			var eventName = "";
			switch(eventCode){
				case "NAME_CHANGE":
					eventName = "Name";
					break;
				case "SSN_CHANGE":
					eventName = "Social Security Number";
					break;
				case "DOB_CHANGE":
					eventName = "Date of Birth";
					break;
					
			}
			return eventName;
			
		};
		$scope.ageGreaterThan26 = function(dateOfBirth){
			var birthDate = new Date(dateOfBirth);
			
			var effectiveDate = $scope.getEffectiveDate();
			
			var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

			if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
				years--;
			}

			if(years >=26){
				return true;
			}else{
				return false;
			}
		 };
		$scope.UIDateformat = function(dt){
			if($.trim(dt)=="") return dt;
			if(dt==undefined) return "";
			var formattedDate = new Date(dt);
			var dd = formattedDate.getDate();
			var mm =  formattedDate.getMonth();
			mm += 1;  // JavaScript months are 0-11
			var yyyy = formattedDate.getFullYear();
			if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
			formatteddate = mm + "/" + dd + "/" + yyyy;
			return formatteddate;
		
		};
		//to relationship code between household
        $scope.getRelationShipCodeBetweenHousehold = function(fromHouseholdPersonId,toHousholdPersonId){
            var bloodRelationship = findHouseholdByPersonId(1).bloodRelationship;
            for(var i=0; i<bloodRelationship.length;i++){
              if(bloodRelationship[i].relatedPersonId == toHousholdPersonId && bloodRelationship[i].individualPersonId == fromHouseholdPersonId){
                return bloodRelationship[i].relation;
              }
            }
            return "";
        };
		$scope.calculateAgeGreateThan26 = function(householdMember){
			var relationshipCode = $scope.getRelationShipCodeBetweenHousehold(1,householdMember.personId);
			 if(relationshipCode == '18' || relationshipCode == '01' || relationshipCode == '31'){
				 householdMember.under26Indicator = true;	 
			 }else if(relationshipCode == '03'){
				 var dateOfBirth = $scope.UIDateformat(householdMember.dateOfBirth);
				 var eligibleForCoverage = false;
				 relationshipCode = $scope.getRelationShipCodeBetweenHousehold(householdMember.personId,1);
				 if(!$scope.ageGreaterThan26(dateOfBirth) || relationshipCode == '15'){
					eligibleForCoverage = true;
				}
				
				householdMember.under26Indicator = eligibleForCoverage;
			 }else{
				var dateOfBirth = $scope.UIDateformat(householdMember.dateOfBirth);
				// if they are 26 years or over, mark under26Indicator as false
				if($scope.ageGreaterThan26(dateOfBirth)){
					//not eligible for coverage 
					householdMember.under26Indicator = false;
				}
				else{
					householdMember.under26Indicator = true;
				}
			}
			
			return householdMember.under26Indicator;
		};
		$scope.checkIf26YearOld = function (houseHoldFromPopUpUi){
			 if(!$scope.calculateAgeGreateThan26(houseHoldFromPopUpUi)){
					return true;
			}else{
					return false;
			}
		};
		
		$scope.changeRelation=function(relationAction,houseHoldFromUpperUI){
			var bloodRelationship = findHouseholdByPersonId(1).bloodRelationship;
            
			 switch(relationAction){
             case 'keepSame':
            	 houseHoldFromUpperUI.applyingForCoverageIndicator=false ;
            	 ageOver26NonSeekingDobChange.push(houseHoldFromUpperUI.personId);
             break;
             case 'parentWard':
            	 for(var i=0; i<bloodRelationship.length;i++){
                     if(bloodRelationship[i].relatedPersonId == '1' && bloodRelationship[i].individualPersonId == houseHoldFromUpperUI.personId ){
                      bloodRelationship[i].relation='15';
                     }else if(bloodRelationship[i].relatedPersonId == houseHoldFromUpperUI.personId && bloodRelationship[i].individualPersonId =='1'){
                    	 bloodRelationship[i].relation='03'; 
                     }
                   }
             break;
             case 'CourtAppointed':
            	 for(var i=0; i<bloodRelationship.length;i++){
                     if(bloodRelationship[i].relatedPersonId == '1' && bloodRelationship[i].individualPersonId == houseHoldFromUpperUI.personId ){
                      bloodRelationship[i].relation='15';
                     }else if(bloodRelationship[i].relatedPersonId == houseHoldFromUpperUI.personId && bloodRelationship[i].individualPersonId =='1'){
                    	 bloodRelationship[i].relation='31'; 
                     }
                   }
             break;
             
             }
			 $scope.showRelChangeRadio=false;
   			 $scope.showRelChangeSelect=false;
		};
		
	    $scope.isDataChanged = function(modalHouseholdMember,originalIndex){
	    	var originalHouseholdArr = ssapJsonCloned.singleStreamlinedApplication.taxHousehold[0].householdMember
	    	var formToValidate = isDemographicDataChanged(originalHouseholdArr[originalIndex],modalHouseholdMember);
        	 $scope.formValidations = {
     				namechangeform:false,
     				dobchange:false,
     				maritalStatusSSNForm:false,
     				ethnicityRaceChangeForm:false
     	    };
        	
        	 
        	var validForm = true;
        	var formActive = false;
        	for(var i=0;i< formToValidate.length;i++){	
        		if($(angular.element('[name='+formToValidate[i]+']')[0]).hasClass("ng-invalid")){
        			eval("$scope.formValidations."+formToValidate[i]+"=true");
        			if(!formActive){
        				$scope.tabs = {
        	      				namechangeform:false,
        	      				dobchange:false,
        	      				maritalStatusSSNForm:false,
        	      				ethnicityRaceChangeForm:false
        	      	    };
        				eval("$scope.tabs."+formToValidate[i]+"=true");
        				$scope.scrollToTop();
        				$('[name='+formToValidate[i]+'] .ng-invalid')[0].focus();
        				formActive = true;
        			}
        			
        			validForm = false;
        		}
        	}
        	return validForm;
        };
        
        $scope.scrollToTop = function(){
        	$('#modalTemplate').scrollTop(0);
			$(window).scrollTop(0);
        }
        //On click of NO radio button remove all ethnicities.
        $scope.uncheckAllEthnicities = function() {
            $scope.selectedEthnicities = [];
        };
        
        //On click of race check boxes ... call this function.
        $scope.updateRace = function(paramRace) {
         //get array in scope
         var scope = angular.element($("#ethnicityYes")).scope();
         var id= 'RACE'+paramRace.index;
         var raceArray=$scope.modalHouseholdMember.ethnicityAndRace.race;
         var code=paramRace.code;
         var label=paramRace.label;
         //check id add or delete operation
         if( $('#' + id).is(":checked")){
           //Add operation
           var i;
           var isAlreadyPresent=false;
           for (i = 0; i < raceArray.length; ++i) {
             if(raceArray[i].code == code){
               isAlreadyPresent=true;
               break;
             }
           }
           if(!isAlreadyPresent){
             //If already not present then add new array element.
             raceArray.push( { "label":label ,"code":code,"otherLabel": null  }  );
           }
         }else{
           //Delete operation
           var i;
           for (i = 0; i < raceArray.length; ++i) {
             if(raceArray[i].code == code){
               raceArray.splice(i,1);
               break;
             }
           }
         }
       };
       
       //On click of ethnicity check boxes ...call this function.
         $scope.updateEthnicity = function(paramEthnicity) {
         //get array in scope
           var scope = angular.element($("#ethnicityYes")).scope();
           var id= 'ETHNICITY'+paramEthnicity.index;
           var ethnicityArray=$scope.modalHouseholdMember.ethnicityAndRace.ethnicity;
           var code=paramEthnicity.code;
           var label=paramEthnicity.label;
           //check id add or delete operation
           if( $('#' + id).is(":checked")){
             //Add operation
             var i;
             var isAlreadyPresent=false;
             for (i = 0; i < ethnicityArray.length; ++i) {
               if(ethnicityArray[i].code == code){
                 isAlreadyPresent=true;
                 break;
               }
             }
             if(!isAlreadyPresent){
               //If already not present then add new array element.
               ethnicityArray.push( { "label":label ,"code":code,"otherLabel": null  }  );
             }
           }else{
             //Delete operation
             var i;
             for (i = 0; i < ethnicityArray.length; ++i) {
               if(ethnicityArray[i].code == code){
                 ethnicityArray.splice(i,1);
                 break;
               }
             }
           }
         };
        
        
      }]);//End of name change controller

	       //////////////////////////Controller for loss of minimum coverage
      newMexico.controller('lossOfMinimumCoverageController', ['$scope','$controller', function($scope,$controller) {
        // Extend blood Relations Controller
        angular.extend(this, $controller('bloodRelations', {$scope: $scope}));
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
        $scope.ageOver26Members=[];
   		//as relationshipChanges for New member only
		//calculateAgeOver26 Indicator for new member only
		for(var i=0;i<householdArray.length;i++){
			if(!$scope.calculateAgeGreateThan26(householdArray[i])){
				//update the UI
				 $scope.ageOver26Members.push(householdArray[i].personId);
			}	
		}
   		
   		
   	 	$scope.enableLossOfMEC = function(personId){
   	 		if( $scope.ageOver26Members.indexOf(personId) != -1){
   	 			return false;
   	 		}
   	 		var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
	        for(var i=0;i<householdArray.length;i++){//Added as part of HIX-76252
	          if( householdArray[i].personId == personId  && findState(householdArray[i]) != globalStateCode){
	        	  return false;
	          }
	        }
   	 		return true;
   	 	}
        
    	//eventInfo.eventSubCategory = 'LOSS_OF_MEC';
        $scope.lossMinEventArray=LOSS_OF_MEC;
        //$scope.desinationArray=[];
        $scope.modalShown = false;
        $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
          $scope.modalHouseholdMember=houseHoldFromParent;
          $scope.modalShown = !$scope.modalShown;
          $scope.originalIndex=originalIndex;
         // $scope.desinationArray= eventInfoArrayTemp[originalIndex].lossReasons;
          
          setTimeout(function(){
              $("#lossMinCalenderDiv").children('input').each(function () {
                  if(!(this.value)){
                   $("#lossMinCalenderDiv").datetimepicker('setDate',new Date());
                   $("#lossMinCalenderDiv").datetimepicker('update');
                   this.value='';
                   return;
                  }
              });
              
              $("#lossMinCalenderDiv").datetimepicker('update'); 
                 
             }, 700);
        };


        $scope.appalyZindex= function(event) {
          $('.bootstrap-datetimepicker-widget').each(function() {
            $(this).css("z-index", 300006);
          });
        };
        $scope.closeModal= function() {
          $scope.modalShown=false;;
        };

        $scope.modalOk = function(houseHoldFromParent) {
            $scope.modalShown = !$scope.modalShown;
            houseHoldFromParent.applyingForCoverageIndicator = true;
            addChangedApplicants(houseHoldFromParent.personId);
          };

       /* $scope.addRemoveElement= function(event) {
          var elementString= $(event.currentTarget).attr('array-content') ;
           var elementIndex=$scope.desinationArray.indexOf(elementString);

           if(event.currentTarget.checked){
             if (elementIndex === -1) $scope.desinationArray.push(elementString);
           }else{
             if (elementIndex !== -1) $scope.desinationArray.splice(elementIndex, 1);
           }

         };*/

        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;

        $scope.getOriginalIndex=function(content,array){
          return  $.inArray(content,array);
        };

        $scope.updateDates=function(){

          for(var i=0;i<householdArray.length;i++){

            for(var j=0;j<eventInfo.changedApplicants.length;j++){
              if(eventInfo.changedApplicants[j].personId == householdArray[i].personId){
                eventInfo.changedApplicants[j].eventDate=eventInfoArrayTemp[i].changeInLossDate;
                eventInfo.eventSubCategory =eventInfoArrayTemp[i].lossEventSubCategory;
                break;
                   }
             }
            }
          return;
        };

        $scope.updateEventSubCategory=function(subCategory){
          eventInfo.eventSubCategory = subCategory;
        };
        
      	

      }]);

      //////////////////////////Controller for gain  Other Minimum Essential Coverage
      newMexico.controller('gainMinimumCoverageController', ['$scope', function($scope) {
        //eventInfo.eventSubCategory = 'GAIN_MEC';
        $scope.modalShown = false;
        $scope.toggleModal = function(houseHoldFromParent,originalIndex) {
          $scope.modalHouseholdMember = angular.copy(houseHoldFromParent);
          $scope.modalShown = !$scope.modalShown;
          $scope.originalIndex=originalIndex;
          
          setTimeout(function(){
              $("#gainCalenderDiv").children('input').each(function () {
                  if(!(this.value)){
                   $("#gainCalenderDiv").datetimepicker('setDate',new Date());
                   $("#gainCalenderDiv").datetimepicker('update');
                   this.value='';
                   return;
                  }
              });
              
              $("#gainCalenderDiv").datetimepicker('update'); 
                 
             }, 700);
          
        };

        $scope.gainMinEventArray=GAIN_MEC;

        $scope.eventInfoArrayTemp=eventInfoArrayTemp;
        var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;

        $scope.modelOK = function(householdCloned,indexFromModel) {
        	householdCloned.applyingForCoverageIndicator = false;
        	householdArray[indexFromModel] = householdCloned;
          addChangedApplicants(householdCloned.personId,$scope.eventInfoArrayTemp[indexFromModel].changeInGainDate);

        };

        $scope.closeModal= function() {
            $scope.modalShown=false;
          };

        $scope.getOriginalIndex=function(content,array){
          return  $.inArray(content,array);
        };

        $scope.appalyZindex= function(event) {
          $('.bootstrap-datetimepicker-widget').each(function() {
            $(this).css("z-index", 300006);
          });
        };
        $scope.updateSeekCoverageFlag= function(modalHouseholdMemberFromView){
        	if((modalHouseholdMemberFromView.personId == 1) ||(modalHouseholdMemberFromView.personId === '1')){
        	//If primary applicants then make flag false for all applicants.
        		var arrayIndex=0;
	        	 for (arrayIndex=0;arrayIndex < householdArray.length;arrayIndex ++){
	        		 householdArray[arrayIndex].applyingForCoverageIndicator= false;
	        	 }
        	}
        };
      }]);

      //////////////////////////Controller for income-deductions
      newMexico.controller('incomeDeductions', ['$scope', function($scope) {


         var localHouseHoldMemberArray=ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
         $scope.deductionArray=localHouseHoldMemberArray[localHouseHoldMemberArray.length-1].detailedIncome.deductions.deductionType;

         //Load other deduction value
         $scope.otherDeductionType='';
         for (var i=0;i<$scope.deductionArray.length;i++){
           if($scope.deductionArray[i] != 'ALIMONY' &&  $scope.deductionArray[i] != 'STUDENT_LOAN_INTEREST'){$scope.otherDeductionType=$scope.deductionArray[i];break;}
          }


         $scope.addRemoveElement= function(event) {
            var elementString= $(event.currentTarget).attr('array-content') ;
            var elementIndex=$scope.deductionArray.indexOf(elementString);

             if(event.currentTarget.checked){
               if (elementIndex === -1) $scope.deductionArray.push(elementString);
                  }else{
               if (elementIndex !== -1) $scope.deductionArray.splice(elementIndex, 1);
                  }

        };

        $scope.displayDeduction= function(deduction) {
              var index=$scope.deductionArray.indexOf(deduction);
              if(index==-1){
                return false;
                  }else{
                return true;
                  }
        };

        $scope.displayOtherDeduction= function() {
          for (var i=0;i<$scope.deductionArray.length;i++){
             if($scope.deductionArray[i] != 'ALIMONY' &&  $scope.deductionArray[i] != 'STUDENT_LOAN_INTEREST'){return true;break;}
                            }
          return false;
        };

        $scope.addOtherTextToArray=function(){
          //This function is on blur of other text box
          //First remove any "Other" value from array and then add text box value to array
          var indexFound=-1;
          for(var i=0;i<$scope.deductionArray.length;i++){
            if($scope.deductionArray[i]!='ALIMONY' && $scope.deductionArray[i]!='STUDENT_LOAN_INTEREST'){
              indexFound=i;
                              break;
                            }
                          }
          if(indexFound!=-1){
            $scope.deductionArray.splice(indexFound, 1);
                        }
          $scope.deductionArray.push( $scope.otherDeductionType);
        };

        $scope.areOtherEmpty=function(){
          if($scope.deductionArray.length==0){return true;}else{return false;}
        };
        $scope.clearOthers=function(){
          $scope.deductionArray=[];
          localHouseHoldMemberArray[localHouseHoldMemberArray.length-1].detailedIncome.deductions.deductionType=[];
        };


      }]);

      //////////////////////////Controller for OTHER event
      newMexico.controller('otherController', ['$scope', function($scope) {
    	  $scope.otherEventArray=OTHER;
    	  $scope.eventInfoArrayTemp=eventInfoArrayTemp;
    	  var householdArray = ssapJson.singleStreamlinedApplication.taxHousehold[ssapJson.singleStreamlinedApplication.taxHousehold.length-1].householdMember;
    	  $scope.updateDates = function(){
    		  eventInfo.changedApplicants[0].eventDate=eventInfoArrayTemp[0].otherEventDate;
   	          return;
    	  };

      }]);
      /*
      ***********************************
          * Directive for updating name attribute
          of dynamically generated form fields
          ***********************************
          */
         newMexico.directive("dynamicName",function($compile){
            return {
                restrict:"A",
                terminal:true,
                priority:1000,
                link:function(scope,element,attrs){
                 var newName= $(element).attr("placeholder")+ '' +scope.$eval(attrs.dynamicName) ;
                    element.attr('name', newName);
                    element.removeAttr("dynamic-name");
                    $compile(element)(scope);
                }
             };
          });

      /*
      ***********************************
      * Directive for validations starts
      ***********************************
      */


      
      
      newMexico.directive("addressValidator",function($http){
    	  return {
              require: 'ngModel',
              link: function(scope, element, attrs, modelCtrl){
            	  
            	  	var mailingAddress = scope.$eval(attrs.addressValidator);
            	  	var addressCallback = attrs.addressCallback;
            	  	var href = '/hix/platform/address/viewvalidaddressnew';
            	  	var applicant = null;
            	  	var idx = null;
            	  	var eventDate = null;
            	  	
            	  
            	  	
            	  	addressSelected = function(){
            	  		var address  = $('#addressModal input:radio:checked').val();
            	  		if(address == undefined){
            	  			alert("Please select address.");
            	  			return;
            	  		}
            	  		hideModal();
            	  		var addressArr = $('#addressModal input:radio:checked').val().split(",");
            	  		mailingAddress.streetAddress1 = $.trim(addressArr[0]);
            	  		mailingAddress.streetAddress2 = $.trim(addressArr[1]);
            	  		mailingAddress.city = $.trim(addressArr[2]); 
            	  		mailingAddress.postalCode = $.trim(addressArr[4]);
            	  		if(addressArr.length>8){
            	  			mailingAddress.county = $.trim(addressArr[8]);
            	  		}
            			eval("scope."+addressCallback+"(applicant,idx,eventDate)");
            			scope.$apply();
            	  	}
            	  	
            	  	
            	  	addressSameSelected = function(){
            	  		hideModal();
            	  		eval("scope."+addressCallback+"(applicant,idx,eventDate)");
            	  	}
            	  	
            	  	
            	  	function hideModal(){
            	  		$('#addressModal').modal('hide');
            	  		$('.modal-backdrop').remove();
            	  		$('body').removeClass('modal-open');
            	  	}
            	  	
            	  	
            	  	
            	  	
            		//$('#modalOk').on('click',addressSelected);
            	  	
            	  	$(element).on('validateAddress',function(event,dummyPrimaryApplicant,index,date){
            	  		applicant = dummyPrimaryApplicant;
            	  		idx = index;
            	  		eventDate = date;
            	  		var imgpath = '/hix/resources/img/ajax_loader_blue_128.gif';
            	  		$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
            	  		/*lock the screen and inform user about validation 'Please wait... while we validate your address...'*/
            	  		$('<div class="modal popup-address" id="addressProgressPopup"><div class="modal-header" style="border-bottom:0; "><h4 class="margin0">Please wait... while we validate your address...</h4></div><div class="modal-body center" style="max-height:470px; padding: 10px 0px;"><img src=\"' + imgpath + '\"></div></div>').modal({backdrop:"static",keyboard:false});
            	  		
            	  		var enteredAddress = mailingAddress.streetAddress1 + ',' + (mailingAddress.streetAddress2 == undefined || mailingAddress.streetAddress2 == null? '': mailingAddress.streetAddress2) + ',' + mailingAddress.city+ ',' + mailingAddress.state+ ',' + mailingAddress.postalCode;
               			var startindex = (attrs.id).indexOf("_");
               			var prefix = (attrs.id).substring(0,startindex);
               			var ids = prefix+"_addressLine1"+"~"+prefix+"_addressLine2"+"~"+prefix+"_primary_city"+"~"+prefix+"_primary_state"+"~"+prefix+"_primary_zip"+"~"+prefix+"lat"+"~"+prefix+"lon"+prefix+"rdi"+"~"+prefix+"county";
               			
               		  	$http.get('/hix/platform/validateaddressnew?enteredAddress=' + enteredAddress+'&ids=' + ids).then(
               			function success(response){
               				var data =  response.data;
               				var retVal=data.split("~");
               				$('#addressProgressPopup').modal('hide');
               				$('#addressProgressPopup').remove();
               				zipIdFocused = prefix+"_primary_zip";
               				if(data == 'SUCCESS'){	
            					//add to the call (new Date()).getTime() to clear cache in IE
            					$http.get(href).then(function(response){
            						var html = $(response.data).find("#frmviewAddressList").html();
            						$('#addressModal .modal-body').html(html);
            						$('#addressModal').find('#submitAddr').remove();
        							$('#addressModal').find('#back_button_from_iframe').remove();
									$('#addressModal').modal({
										keyboard: false,
										backdrop: 'static'
									});
            						
            					});
            					
            					
            				}else if(retVal[0]=="FAILURE"){	
           			   			/*console.log('FAILURE');*/
            					$('#addressErrorModal .modal-body').html('<p>'+retVal[1]+'</p>');
            					$('#addressErrorModal').modal();
           			   		}else if(retVal[0]=="MATCH_FOUND"){
           			   			addressSameSelected();
           			   		}
            				
               			},
               			function failure(){
               				$('#addressProgressPopup').modal('hide');
               				$('#addressProgressPopup').remove();
               				alert('Unable to validate address!');
               			}
               			);
               			
               		});
               		
               		
               		
               		
              }
           };
        });
      
      
    
      newMexico.directive('numbersOnly', function(){
           return {
             require: 'ngModel',
             link: function(scope, element, attrs, modelCtrl) {
             //formatter to remove zero and show it as blank
             var formatZeroAsBlank = $(element).attr('format-zero-as-blank');
             if(formatZeroAsBlank == undefined || (formatZeroAsBlank != undefined && formatZeroAsBlank == 'true')){
               modelCtrl.$formatters.push(function(val){
                  if(val=='0')
                    return '';
                  else
                    return val;
                 });
             }
               modelCtrl.$parsers.push(function (inputValue) {
                   // this next if is necessary for when using ng-required on your input.
                   // In such cases, when a letter is typed first, this parser will be called
                   // again, and the 2nd time, the value will be undefined
                   if (inputValue == undefined)
                     return undefined ;


                   //if starts with 0
                   //check 0 replace attr
                   var replaceZero = $(element).attr('numbers-only-replace-zero');
                   if(replaceZero == undefined || (replaceZero != undefined && replaceZero == 'true') ){
                     if(inputValue.indexOf('0')==0){
                       //if length is 1
                       if(inputValue.length == 1){
                         modelCtrl.$setViewValue('');
                         modelCtrl.$render();
                         return undefined;
                       }else if(inputValue.length > 1){//if length greater than 1
                         var numericValue = Number(inputValue);//parse to number
                         if(numericValue != 'NaN' && numericValue> 0 ) //if number is greater than 0 or a valid number
                         {
                           modelCtrl.$setViewValue(numericValue+''); //replace with numeric value
                           modelCtrl.$render();
                           return parseInt(inputValue);
                         }else{
                         modelCtrl.$setViewValue('');//replce with blank if value is zero
                         modelCtrl.$render();
                         return undefined;
                         }
                       }

                     }
                   }

                   //max
                   var max = $(element).attr('numbers-only-max');
                   if(max != undefined){
                     var numericValue = Number(inputValue);//parse to number
                     var maxNumeric =  Number(max);
                     if(numericValue != 'NaN' && maxNumeric != 'NaN'){
                       if(numericValue>maxNumeric){
                         modelCtrl.$setValidity('maxNumber',false);
                       }else{
                         modelCtrl.$setValidity('maxNumber',true);
                      }

                     }else{
                       modelCtrl.$setValidity('maxNumber',true);
                     }
                   }
                   //min
                   var min = $(element).attr('numbers-only-min');
                   if(min != undefined){
                     var numericValue = Number(inputValue);//parse to number
                     var minNumeric =  Number(min);
                     if(numericValue != 'NaN' && minNumeric != 'NaN'){
                       if(numericValue<minNumeric){
                         modelCtrl.$setValidity('minNumber',false);
                       }else{
                         modelCtrl.$setValidity('minNumber',true);
                       }

                     }else{
                       modelCtrl.$setValidity('minNumber',true);
                     }
                   }

                   var transformedInput = inputValue.replace(/[^0-9]/g, '');
                   if (transformedInput!=inputValue) {
                      modelCtrl.$setViewValue(transformedInput);
                      modelCtrl.$render();
                   }

                   if(transformedInput!="" && !isNaN(transformedInput)){
                     return parseInt(transformedInput);;
                   }else{
                     return undefined;
                   }

             });
           }
         };
      });




      newMexico.directive('validSsn', function(){
          return {
            require: 'ngModel',
            link: function(scope, element, attrs, modelCtrl) {
           //formatter to remove zero and show it as blank
              modelCtrl.$formatters.push(function(val){
                if(val=='0')
                  return '';
                else
                  return val;
              });
              modelCtrl.$parsers.unshift(function (inputValue) {
                  // this next if is necessary for when using ng-required on your input.
                  // In such cases, when a letter is typed first, this parser will be called
                  // again, and the 2nd time, the value will be undefined
                   if (inputValue == undefined)
                     return '' ;
                   //if starts with 0
                   //check 0 replace attr

                  var arr  = inputValue.split('-');
                  if(arr.length == 3){
                    var ssn1 = Number(arr[0]);
                    var ssn2 = Number(arr[1]);
                    var ssn3 = Number(arr[2]);


                    if(ssn1 != 'NaN' && ssn2 != 'NaN' && ssn3 != 'NaN'){
                      // should not be 666 or 900-999 in the first digit group
                      if(!(ssn1 == 666 || (ssn1 >=900 && ssn1 <= 999)) ){
                        modelCtrl.$setValidity('ssn',true);
                        return inputValue;
                      }
                  }

                  }
                  modelCtrl.$setValidity('ssn',false);
                  return inputValue;


            });
          }
        };
      });



      /*
       ***********************************
       * Directive for validations ends
       ***********************************
       */
      /*
       ***********************************
       * Directive for checkbox
       * (Where checking/unchecking will insert/remove @string data into @array)
       * CheckBox should have @array in which @string will be inserted
       * For adding element to destinationArray function should be present in controller.
       * This function shoud be called on click of checkbox by providing event as parameter.
       * If want to clear all checkboxes then empty array in controller.
       * @array=destinationArray
       * @string=arrayContent
       ***********************************
       */
      newMexico.directive("customCheckbox", function () {
          return {
              restrict: "A",
              scope: {childCheckBoxArray: "=destinationArray",
                    arrayContent:"@arrayContent"},


              link: function (scope, elem, attrs) {
                  if (scope.childCheckBoxArray.indexOf(scope.arrayContent) !== -1) {
                      elem[0].checked = true;
                  }else{
                    elem[0].checked = false;
                  }

                  scope.$parent.$watchCollection(attrs.destinationArray, function() {
                    if( scope.$parent[attrs.destinationArray].indexOf(scope.arrayContent) !== -1){
                     elem[0].checked = true;
                   }else{
                     elem[0].checked = false;
                   }

                  }, true);

              }
          };
      });

      function postPopulateIndex(zipCodeValue, zipId){
        //$('#suggestion-box').close();
         $("#suggestion-box, .modal-backdrop").remove();
          /* console.log(zipCodeValue,'---page---',zipId); */
          jQuery('#'+zipId).trigger("change");
          jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
          
          var startindex = zipId.indexOf("_");
  		  var index = zipId.substring(0,startindex);
  		
  		
  		var address1_e='_addressLine1'; var address2_e='_addressLine2'; var city_e= '_primary_city'; var state_e='_primary_state'; var zip_e='_primary_zip';
  		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
  		/*check for the underscore in the id of the focus out event of the zipcode. for current scenarios id is "zip_0. hence startindex  ===3 "*/
  		address1_e=index+address1_e;
		address2_e=index+address2_e;
		city_e=index+city_e;
		state_e=index+state_e;
		zip_e=index+zip_e;
		county_e+=index;
		
		$('#'+address1_e).trigger('change');
		$('#'+address2_e).trigger('change');
		$('#'+city_e).trigger('change');
		$('#'+state_e).trigger('change');
		$('#'+address1_e).trigger('change');
		
		//$('#'+county_e).trigger('change');
		
  		
          //getCountyList(zipCodeValue, '', zipId);
      }
      
      function getCountyList(zip, county, zipId) {
          /* console.log(zip, county, 'get county list', zipId); */
          var subUrl =  '/hix/shop/employee/addCounties';
          $.ajax({
                 type : "POST",
                 url : subUrl,
                 data : {zipCode : zip},
                 dataType:'json',
                 success : function(response) {
                        populateCounties(response, county, zipId);
                 },error : function(e) {
                        //alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
                 }
          });
      }

      function populateCounties(response, county, zipIdFocused) {
          /* console.log(zipIdFocused,'populate counties'); */

        var optionsstring = '<option value="">Select County</option>';
                 var i =0;
                 $.each(response, function(key, value) {
                        var selected = (county == key) ? 'selected' : '';
                        var optionVal = key;/*+'#'+value;*/
                        var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
                        optionsstring = optionsstring + options;
                        i++;
                 });
                 //$('#'+zipIdFocused).parents('.addressBlock').find('select[data-rel="countyPopulate"]').html(optionsstring);
      }
      
      function redirectToDashboard(){
    	  window.location = '<c:url value="/indportal" />';
      }
    </script>
  </div>
</div>
<div class='ng-modal' id="applicationInProgressDiv" style="display:none">
    <div class='ng-modal-overlay'></div>
    <div class='ng-modal-dialog' ng-style='dialogStyle' style="height:200px">
      <div class='modal-header'>
        <h3> Report a Change</h3>
      </div>
      <div class='modal-body'>You have another application which has a pending action from you, please take action on that application before proceeding with reporting another change.</div>
      <div class='pull-right gutter20'>
        <a class='btn btn-primary' onclick='redirectToDashboard();'>Ok</a>
      </div>
    </div>
  </div>


