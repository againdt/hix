import React, { useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import ControlsContainer from './ControlsContainer';
import ButtonContainer from './ButtonContainer';

// ActionCreators

import { changePage, adjustFlow, removeException, setError, updateException } from '../actions/actionCreators';

//Utils

import { familyAnalyzer } from '../utils/familyAnalizer';

// Constants

import EXCEPTIONS from '../constants/exceptionList';
import { PAGES } from '../constants/pagesList';
import MASKS from '../constants/masksConstants';
import normalizeNameOfPerson from '../utils/normalizeNameOfPerson';





/**
 *
 * @param {Object} props
 * @returns {React.Component} - Page Container
 *
 */
const PagesContainer = props => {

    const {
        adjustedFlow,
        currentHhm,
        data: {
            singleStreamlinedApplication: {
                taxHousehold
            }
        },
        direction,
        exception: {
            exceptionType,
            exceptionPages
        },
        familyStatus,
        loadedRelations,
        flow: {
            isFlowLoaded
        },
        hhmIndex,
        householdMembers,
        householdMembers: {
            length: hhmsQty
        },
        maxAchievedPage,
        page,
        scroll
    } = props;

    const {
        primaryTaxFilerPersonId
    } = taxHousehold[0];

    const bloodRelationshipsForWatch = householdMembers[0]
        .bloodRelationship
        .map(br => br.relation)
        .toLocaleString();

    const addressesForWatch = householdMembers
        .map(hhm => hhm.addressId)
        .toLocaleString();

    const watchedPropsInHHM = householdMembers.map(hhm => {
        const {
            americanIndianAlaskaNative: {
                memberOfFederallyRecognizedTribeIndicator
            },
            applyingForCoverageIndicator,
            dateOfBirth,
            gender,
            incomes,
            taxHouseholdComposition: {
                married,
                taxFiler,
                taxRelationship
            }
        } = hhm;
        return `${memberOfFederallyRecognizedTribeIndicator},
                    ${married},
                    ${taxFiler},
                    ${taxRelationship},
                    ${addressesForWatch},
                    ${dateOfBirth},
                    ${bloodRelationshipsForWatch},
                    ${exceptionType},
                    ${hhmsQty},
                    ${adjustedFlow.length},
                    ${primaryTaxFilerPersonId},
                    ${gender},
                    ${incomes.length},
                    ${applyingForCoverageIndicator}`;
    }).toLocaleString();



    useEffect(() => { // Exceptions remover
        const conditions = [
            [
                EXCEPTIONS.EDITING_INCOMES_AND_DEDUCTIONS,
                PAGES.PERSONAL_INCOME_SUMMARY_PAGE,
            ],
            [
                EXCEPTIONS.REVIEW_INCOME_SECTION,
                PAGES.INCOME_INFORMATION_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER,
                PAGES.HOUSEHOLD_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.EDITING_HOUSEHOLD_MEMBER_ADDRESS,
                PAGES.HOUSEHOLD_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.EDITING_PRIMARY_HOUSEHOLD_CONTACT,
                PAGES.HOUSEHOLD_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION,
                PAGES.FAMILY_AND_HOUSEHOLD_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.REVIEW_BEFORE_SUBMIT,
                PAGES.FINAL_APPLICATION_REVIEW_PAGE
            ],
            [
                EXCEPTIONS.FIXING_TAX_RELATIONSHIP,
                PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE
            ],
            [
                EXCEPTIONS.EDITING_ADDITIONAL_INFORMATION,
                PAGES.ADDITIONAL_INFORMATION_SUMMARY_PAGE
            ],
            [
                EXCEPTIONS.MANAGE_SPOUSE,
                [
                    PAGES.HOUSEHOLD_SUMMARY_PAGE,
                    PAGES.MARITAL_STATUS
                ]
            ]
        ];
        conditions.forEach(([ exceptionTypeToSwitchOff, pageWhereToSwitchOff ]) => {

            const shouldExceptionBeSwitched = (pageWhereToSwitchOff) => {
                if(Array.isArray(pageWhereToSwitchOff)) {
                    return pageWhereToSwitchOff.some(p => p === adjustedFlow[page].pageName);
                } else {
                    return pageWhereToSwitchOff === adjustedFlow[page].pageName;
                }
            };

            if(exceptionTypeToSwitchOff === exceptionType && shouldExceptionBeSwitched(pageWhereToSwitchOff)) {
                props.actions.removeException();
            }
        });
    }, [ exceptionType ]);

    useEffect(() => {
        if(scroll === null) {
            window.scrollTo({ top: 0, behavior: 'smooth' });
        }
    }, [ page ]);

    useEffect(() => {

        // const exc = (exception => exception ? exception.value : null)(exception);

        const isAnyoneFosterCareAgeAndApplyingForCoverage = householdMembers.some(hhm => {
            const age = moment().diff(hhm.dateOfBirth, 'years');
            return age >= 18 && age <= 25 && hhm.applyingForCoverageIndicator;
        });

        const isFullTimeStudent = householdMembers.some(hhm => {
            const age = moment().diff(hhm.dateOfBirth, 'years');
            return age >= 18 && age <= 22 && hhm.applyingForCoverageIndicator;
        });

        const output = adjustedFlow.map(el => {

            const currentHhmIndex = (idx => ~idx ? idx : null)(el.hhm);

            const hhm = (idx => !~idx ? { name: {} } : householdMembers[el.hhm] ? householdMembers[el.hhm] : { name: {}  })(el.hhm);

            const shouldAskAboutPregnancy = () => {
                let pregnantCounter = 0;
                householdMembers.forEach((hhm) => {
                    let age = moment().diff(hhm.dateOfBirth, 'years');
                    if (
                        (age >= 9 && age <= 66) &&
                        (hhm.gender && hhm.gender.toUpperCase() === 'FEMALE')
                    ) {
                        pregnantCounter++;
                    }
                });
                return !(pregnantCounter > 0);
            };

            const {
                americanIndianAlaskaNative: {
                    memberOfFederallyRecognizedTribeIndicator
                },
                applyingForCoverageIndicator,
                incomes,
                personId,
                addressId
            } = ~el.hhm && el.hhm <= hhmsQty - 1 ? householdMembers[el.hhm] : {
                americanIndianAlaskaNative: {},
                taxHouseholdComposition: {},
                flagBucket: {}
            };

            const checkIfTaxFilersOnApplication = () => {
                return (hhms => hhms.length < 1)(householdMembers
                    .map(hhm => hhm.taxHouseholdComposition.taxFiler ? hhm.personId : null)
                    .filter(el => el));
            };
            const familyConfig = {
                loadedRelations,
                bloodRelationships: taxHousehold[0].householdMember[0].bloodRelationship,
                currentMemberIndex: currentHhmIndex,
                householdMembers,
                MASKS
            };
            const familyStatusForCurrentHHM = familyAnalyzer(familyConfig);

            const checkIfAdditionalInfoPageToBeDisplayed = () => {

                const childrenHasHealthCoverage = () => {
                    if (!familyStatusForCurrentHHM.relatedPersonsIds.children || primaryTaxFilerPersonId !== personId) return null;

                    const filteredChildren = familyStatusForCurrentHHM.relatedPersonsIds.children.filter(ch => !householdMembers[ch - 1].applyingForCoverageIndicator);

                    return filteredChildren.length > 0;
                };

                const isMemberTheChildWithoutParent = (()=> {
                    const analyzeIfNotAllParents = value => !Array.isArray(value) ? true : value.length < 2;
                    return applyingForCoverageIndicator && !familyStatusForCurrentHHM.currentHhm.isOlderThanChildAge && analyzeIfNotAllParents(familyStatusForCurrentHHM.relatedPersonsIds.parents);
                })();

                const payload = [ !hhm.applyingForCoverageIndicator, !(hhm.applyingForCoverageIndicator && !hhm.memberOfFederallyRecognizedTribeIndicator) , !childrenHasHealthCoverage() , !isMemberTheChildWithoutParent];
                return {
                    payload: payload.every(cond => cond),
                    explanation: 'Person does not qualify'
                };
            };

            const checkParentCareTakerAge = () => {
                const dependantOfTheHousehold = [];
                if(!familyStatusForCurrentHHM.relatives) return false;
                familyStatusForCurrentHHM.relatives.forEach(function (dependant) {
                    householdMembers.filter(member => {
                        if(member.personId === dependant.personId && moment().diff(member.dateOfBirth, 'years') < 19 && addressId === dependant.addressId) {
                            dependantOfTheHousehold.push(normalizeNameOfPerson(member.name));
                            return dependantOfTheHousehold;
                        }
                    });
                });
                return moment().diff(hhm.dateOfBirth, 'years') >= 19 && hhm.applyingForCoverageIndicator && Array.isArray(dependantOfTheHousehold) && dependantOfTheHousehold.length > 0;
            };
            /**
             *
             *
             */

            const conditions = [

                // Skipping "Household relationships" and "Household addresses" pages
                // if the only one household member

                [
                    hhmsQty === 1,
                    [
                        PAGES.OTHER_ADDRESSES_PAGE,
                        PAGES.RELATIONSHIPS_PAGE
                    ],
                    '- only one hhm'
                ],

                // Skipping "Citizenship/immigration status", "Other Health Coverage", "Employer Coverage Detail" pages
                // if household member do not seek for coverage

                [
                    !hhm.applyingForCoverageIndicator,
                    [
                        PAGES.CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
                        PAGES.OTHER_HEALTH_COVERAGE_PAGE,
                        PAGES.EMPLOYER_COVERAGE_DETAIL_PAGE
                    ],
                    '- not applied for coverage'
                ],
                // Skipping "Parent/ Caretaker" page
                // if household member is not older than 19
                [
                    !checkParentCareTakerAge(),
                    [
                        PAGES.PARENT_CARETAKER_INFORMATION_PAGE
                    ],
                    '- non-applicant AND or OR age is less than 19 '
                ],
                // Skipping "Tribal income" page
                // if household member is not an AI/AN representative or in case if is - have not got any income

                [
                    !memberOfFederallyRecognizedTribeIndicator ||
                    (memberOfFederallyRecognizedTribeIndicator && incomes && incomes.length === 0),
                    [
                        PAGES.TRIBAL_SOURCE_INCOME_PAGE
                    ],
                    incomes && incomes.length === 0 && memberOfFederallyRecognizedTribeIndicator ?
                        '- household member is an AI/AN but do not have income' :
                        '- household member not an AI/AN'
                ],

                // Skipping "Reconciliation of APTC" page
                // if household member is not primary household contact

                [
                    !(primaryTaxFilerPersonId === personId),
                    [
                        PAGES.RECONCILIATION_IN_APTC_PAGE
                    ],
                    '- applicable only if hhm matches primaryTaxFilerPersonId'
                ],

                // Skipping "Foster care" page
                // if household member's age is not in rage 18 - 25

                [
                    !isAnyoneFosterCareAgeAndApplyingForCoverage,
                    [
                        PAGES.FOSTER_CARE_INFORMATION_PAGE
                    ],
                    '- no one HHM age of 18 - 25 y.o.'
                ],

                // Skipping "FullTime Student" page
                // if applicants's age is not in rage 18 - 22

                [
                    !isFullTimeStudent,
                    [
                        PAGES.FULL_TIME_STUDENT_PAGE
                    ],
                    '- no one HHM age of 18 - 22 y.o.'
                ],

                // Skipping "Pregnancy Information" page
                // if there no any household members who match criteria

                [
                    shouldAskAboutPregnancy(),
                    [
                        PAGES.PREGNANCY_INFORMATION_PAGE
                    ],
                    '- there are no females age of 9 - 65 y.o.'
                ],

                // Skipping pages for other household members in Income section
                // in review mode started from "Income summary"

                [
                    exceptionType === EXCEPTIONS.REVIEW_INCOME_SECTION
                    && el.hhm > adjustedFlow[exceptionPages[0]].hhm,

                    [
                        PAGES.INCOME_SOURCES_PAGE,
                        PAGES.TRIBAL_SOURCE_INCOME_PAGE,
                        PAGES.DEDUCTION_SOURCES_PAGE,
                        PAGES.EXPECTED_INCOME_PAGE,
                        PAGES.PERSONAL_INCOME_SUMMARY_PAGE
                    ],
                    '- reviewing another household member'
                ],

                // Skipping pages for other household members in Family and Household section
                // in review mode started from "Family and Household summary"

                [
                    exceptionType === EXCEPTIONS.REVIEW_HOUSEHOLD_SECTION
                    && el.hhm > adjustedFlow[exceptionPages[0]].hhm,

                    [
                        PAGES.PERSONAL_INFORMATION_PAGE,
                        PAGES.CITIZENSHIP_IMMIGRATION_STATUS_PAGE,
                        PAGES.TAX_HOUSEHOLD_COMPOSITION_PAGE,
                        PAGES.ETHNICITY_AND_RACE_PAGE
                    ],
                    '- reviewing another household member'
                ],

                // Skipping "Primary Tax Filer" page
                // in case if we don't have 1 or more household members (attested as tax filers)

                [
                    checkIfTaxFilersOnApplication(),
                    [
                        PAGES.PRIMARY_TAX_FILER_PAGE
                    ],
                    '- there are no tax filer on SSAP'
                ],

                // Skipping "State Employee Health Benefit" page
                // in case if consumer is NOT seeking coverage AND NOT requesting financial assistance

                [
                    !hhm.applyingForCoverageIndicator,
                    [
                        PAGES.STATE_EMPLOYEE_HEALTH_BENEFIT_PAGE
                    ],
                    '- person does not apply for coverage'
                ],

                // Skipping "Additional Information" page
                // in different cases

                [
                    checkIfAdditionalInfoPageToBeDisplayed().payload,
                    [
                        PAGES.ADDITIONAL_INFORMATION_PAGE
                    ],
                    checkIfAdditionalInfoPageToBeDisplayed().explanation
                ],

                // Skipping "Marital Status" page
                // in case if household member is younger than 14 y.o. OR married

                [
                    !familyStatusForCurrentHHM.currentHhm.isOlderThanMarriageAge ||
                    familyStatusForCurrentHHM.currentHhm.spouseIndex !== null,
                    [
                        PAGES.MARITAL_STATUS
                    ],
                    '- person is married OR less than 14 y.o.'
                ]

            ];

            let reasons = [];

            conditions.map(([ value, pages, message ]) => {
                if(value && pages.some(page => page === el.pageName)) {
                    if(Array.isArray(message)) {
                        reasons = [
                            ...reasons,
                            ...message
                        ];
                    } else {
                        reasons = [
                            ...reasons,
                            message
                        ];
                    }
                }
            });

            return {
                ...el,
                isActive: !reasons.length > 0,
                reasons
            };
        });
        props.actions.adjustFlow(output);
    }, [
        watchedPropsInHHM
    ]);

    if(adjustedFlow.length === 0) {
        return null;
    }

    let CurrentPage;

    if(isFlowLoaded) {
        try {
            if(adjustedFlow[page].isActive === false && !(direction === 'exact')) {
                if(exceptionType && adjustedFlow[exceptionPages[0]].isActive === false ) {
                    props.actions.updateException([ page + 1 ]);
                }
                props.actions.changePage(direction);
                return null;
            }

            if(adjustedFlow[page].hhm !== -1 && currentHhm === undefined) {
                return null;
            }

            const {
                component: Component,
                pageName
            } = adjustedFlow[page].page;

            CurrentPage = React.cloneElement(Component,
                {
                    buttonContainer: ButtonContainer,
                    isFirstVisit: page >= maxAchievedPage,
                    pageName: pageName,
                    familyStatus,
                    hhmIndex,
                    currentHhm,
                    PAGES
                });
        } catch {
            props.actions.setError({
                errorMessage: 'Wrong page number (it does not exist in current flow) has been provided',
                errorType: 'Handling flows'
            });
        }
    }

    return (
        <div className="usa-grid fade-in page-container">
            { CurrentPage }
            <ControlsContainer/>
        </div>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,

    data: state.data,
    direction: state.direction,
    exception: state.exception,
    flow: state.flow,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    key: `page_key_${state.page}`, // For fade-in updating then page is changed

    maxAchievedPage: state
        .data
        .singleStreamlinedApplication
        .maxAchievedPage,

    page: state.page,
    loadedRelations: state.relations.loadedRelations,
    scroll: state.scroll
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        adjustFlow,
        changePage,
        removeException,
        setError,
        updateException
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(PagesContainer);
