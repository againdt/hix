package com.getinsured.iex.util;

import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeSimpleType;

public class ReferralQualifyingEventCode {
	public static final String CHANGE_IN_CITIZENSHIP = "CHANGE_IN_CITIZENSHIP";
	public static final String MOVED_INTO_STATE = "MOVED_INTO_STATE";
	public static final String DIVORCE = "DIVORCE";
	public static final String BIRTH = "BIRTH";
	public static final String ADOPTION = "ADOPTION";
	public static final String REMOVE_DEPENDENT = "REMOVE_DEPENDENT";
	public static final String LOST_OTHER_MIN_ESSENTIAL_COVERAGE = "LOST_OTHER_MIN_ESSENTIAL_COVERAGE";
	public static final String OTHER = "OTHER";
	public static final String PRIMARY_IN_ANOTHER_APPLICATION_INCARCERATED = "PRIMARY_IN_ANOTHER_APPLICATION_INCARCERATED";
	public static final String PRIMARY_IN_ANOTHER_APPLICATION_DIED = "PRIMARY_IN_ANOTHER_APPLICATION_DIED";
	public static final String EXEMPTION_CANCELLED = "EXEMPTION_CANCELLED";
	public static final String CHANGE_IN_HOUSEHOLD_SIZE = "CHANGE_IN_HOUSEHOLD_SIZE";
	public static final String CHANGE_IN_ADDRESS = "CHANGE_IN_ADDRESS";
	public static final String MARRIAGE = "MARRIAGE";
	public static final String INCOME_CHANGE_CSR = "INCOME_CHANGE_CSR";
	public static final String BIRTH_OR_ADOPTION = "BIRTH_OR_ADOPTION";
	public static final String OTHER_ELIGIBILITY_CHANGE = "OTHER_ELIGIBILITY_CHANGE";
	public static final String LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED = "LOSS_OF_MEC_PRESUMPTIVELY_VERIFIED";

}
