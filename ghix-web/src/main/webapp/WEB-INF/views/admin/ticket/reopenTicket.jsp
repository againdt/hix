<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
    
<div id="reopenTicketModal" class="modal" style="display:none">
	
	 <div class="modal-body">
        <h3>Are you sure you want to Reopen this ticket?</h3>
      </div>
      <div class="modal-footer">
        <button type="button" id="cancelReopenBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="#" type="button" id="submitReopenBtn" class="btn btn-primary" id="btnReopenTicket" >Reopen</a>
      </div>
      <div id="reopenAjaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>
<script>
    	function reopenTicketFunction(ticketid){
    		$("#reopenTicketModal #submitReopenBtn").unbind("click",reopenTicket).bind("click",{ticketid:ticketid},reopenTicket);
   		 	$("#reopenTicketModal").modal();
		}

    	function reopenTicket(e)
    	{   
    		$("#cancelReopenBtn").attr("disabled",true); 	
			$("#submitReopenBtn").attr("disabled",true); 	
    		
    		var ticketId = e.data.ticketid;
    		
    		var validateUrl ='<c:url value="/ticketmgmt/ticket/reopen"></c:url>';
    		var redirectUrl ='<c:url value="/ticketmgmt/ticket/ticketdetail/'+ticketId+'"> </c:url>';
    		$("#reopenAjaxloader").show();
    		$.ajax({
    			url : validateUrl,
    			type : "POST",
    			data : {
    				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
					ticketId : ticketId
    			},
    			success : function(response) {
    				if (response !=null && response != "") {
    					window.location.href = redirectUrl;
    					return false;
    				  
    				} else {
    					alert("Could not reopen");
    					//show the error
    					$("#cancelReopenBtn").attr("disabled",false); 	
    					$("#submitReopenBtn").attr("disabled",false);
    				}
    				
    				
    			},
    			error : function(error) {
    				alert("reopen error");
    				$("#cancelReopenBtn").attr("disabled",false); 	
    				$("#submitReopenBtn").attr("disabled",false);    				
    				//show the error
    			}
    		});
    	}
    	
</script>