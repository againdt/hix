﻿define(function () {
    var basicLeftMenu = new baseComponent();
    basicLeftMenu.type = "basicLeftMenu";
    basicLeftMenu.$holder = null;
    basicLeftMenu.getHTML = function (cgfObj) {
        var defCfg = {
        };
        var vm = this;
        var pageData  = cgfObj;
        var $menuHolder = vm.$holder = $("<div></div>");
        $menuHolder.append('<h4 class="margin0"></h4>');
        var $ul = $('<ul class="nav nav-list"></ul>');
        $menuHolder.append($ul);
        for (var m = 0; m < pageData.length; m++) {
            var mPageData = pageData[m];
            if (mPageData.caption != undefined) {
                var $li = $('<li class="navmenu" ></li>');
                $li.addClass(mPageData.id);
                $ul.append($li);
                var $a = $('<a href="#">' + mPageData.caption + '</a>');
                if (mPageData.type == "singlePage") {
                    $a.attr("href", "#" + mPageData.id);
                } else if (mPageData.type == "redirect") {
                    $a.attr("href", mPageData.url);
                }
                $li.append($a);
            };
        };
        return $menuHolder[0].outerHTML;
    },
    basicLeftMenu.activate = function (e) {
        log("===================");
        var vm = this;
        var path = String(e).replace(/\//ig, "");
        //log(path);
        $(".active").removeClass("active");
        $("." + path).addClass("active");
    };
    return basicLeftMenu;
});