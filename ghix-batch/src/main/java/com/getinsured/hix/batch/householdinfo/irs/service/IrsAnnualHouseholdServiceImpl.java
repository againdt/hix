/**
 * 
 */
package com.getinsured.hix.batch.householdinfo.irs.service;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentAnnualIrsRecipientDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentIrsCoveredIndividualDTO;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * @author negi_s
 *
 */
@Service("irsAnnualHouseholdService")
public class IrsAnnualHouseholdServiceImpl implements IrsAnnualHouseholdService{

	private static final Logger LOGGER = Logger.getLogger(IrsAnnualHouseholdServiceImpl.class);

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	/* (non-Javadoc)
	 * @see com.getinsured.hix.batch.householdinfo.irs.service.IrsAnnualHouseholdService#getRecipientInformationForAnnualReport(Long)
	 */
	@Override
	public EnrollmentAnnualIrsRecipientDTO getRecipientInformationForAnnualReport(Long ssapApplicationId,  Set<Long> ssapApplicationIds) throws GIException {
		LOGGER.info("----------Eligibility Section------------------");
		EnrollmentAnnualIrsRecipientDTO enrollmentAnnualIrsRecipientDTO = new EnrollmentAnnualIrsRecipientDTO();
		try{
			List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);
			if(null != ssapApplications && !ssapApplications.isEmpty()){
				SsapApplication ssapApp = ssapApplications.get(0);
				String ssapJson = null;
				ssapJson = ssapApp.getApplicationData();
				LOGGER.info(ssapJson);
				SingleStreamlinedApplication ssapApplication = null;
				if(null != ssapJson){
					ssapApplication = ssapJsonBuilder.transformFromJson(ssapJson);
					int spouseId = 0;
					int primaryTaxpersonId = 1;
					if(null !=   ssapApplication){
						if (ssapApp.getFinancialAssistanceFlag().equals("Y") && ssapApplication.getPrimaryTaxFilerPersonId() != null) {
							primaryTaxpersonId = ssapApplication.getPrimaryTaxFilerPersonId();
						}
						spouseId = getSpouseId(ssapApplication, primaryTaxpersonId);
						List<HouseholdMember> householdMembers = ssapApplication.getTaxHousehold().get(0).getHouseholdMember();
						enrollmentAnnualIrsRecipientDTO = getRecipientInformation(householdMembers, primaryTaxpersonId, spouseId);
						if((enrollmentAnnualIrsRecipientDTO.getRecipientSpouse() == null || spouseId == 0)
								&& (null != ssapApplicationIds && !ssapApplicationIds.isEmpty())){
							enrollmentAnnualIrsRecipientDTO.setRecipientSpouse(getSpouse(ssapApplicationIds));
						}
					}
				}
			}
		}catch(Exception e){
			LOGGER.error("Error in fetching recipient details from ssap application table" ,e);
			throw new GIException("Error in fetching recipient details from ssap application table" ,e);
		}
		return enrollmentAnnualIrsRecipientDTO;
	}
	
	/**
	 * Get Recipient Information
	 * @param householdMembers
	 * @param primaryTaxpersonId
	 * @param spouseId
	 * @return EnrollmentAnnualIrsRecipientDTO
	 * @throws GIException 
	 */
	private EnrollmentAnnualIrsRecipientDTO getRecipientInformation( List<HouseholdMember> householdMembers, int primaryTaxpersonId, int spouseId) throws GIException {

		EnrollmentAnnualIrsRecipientDTO enrollmentAnnualIrsRecipientDTO = new EnrollmentAnnualIrsRecipientDTO();
		EnrollmentIrsCoveredIndividualDTO recipient = null;
		EnrollmentIrsCoveredIndividualDTO recipientSpouse = null;

		for (HouseholdMember householdMember : householdMembers) {
			int personId = householdMember.getPersonId();
			String firstName = householdMember.getName().getFirstName();
			String lastName = householdMember.getName().getLastName();
			String middleName = householdMember.getName().getMiddleName();
			String suffix = householdMember.getName().getSuffix();
			String ssn = householdMember.getSocialSecurityCard().getSocialSecurityNumber();
			Date dobirth = householdMember.getDateOfBirth();
			String street1 = householdMember.getHouseholdContact().getMailingAddress().getStreetAddress1();
			String street2 = householdMember.getHouseholdContact().getMailingAddress().getStreetAddress2();
			String city = householdMember.getHouseholdContact().getMailingAddress().getCity();
			String stateCode = null;
			if ((householdMember.getHouseholdContact().getMailingAddress().getState() != null) && !(householdMember.getHouseholdContact().getMailingAddress().getState().toString().isEmpty())) {
				stateCode = householdMember.getHouseholdContact().getMailingAddress().getState();
			}
			String zip = householdMember.getHouseholdContact().getMailingAddress().getPostalCode();
			String countyCode = householdMember.getHouseholdContact().getMailingAddress().getCountyCode();
			String personGuId = householdMember.getApplicantGuid();

			// if name on SSN card is different use that name
			if (householdMember.getSocialSecurityCard().getNameSameOnSSNCardIndicator() != null && !householdMember.getSocialSecurityCard().getNameSameOnSSNCardIndicator()) {
				firstName = householdMember.getSocialSecurityCard().getFirstNameOnSSNCard();
				lastName = householdMember.getSocialSecurityCard().getLastNameOnSSNCard();
				middleName = householdMember.getSocialSecurityCard().getMiddleNameOnSSNCard();
				suffix = householdMember.getSocialSecurityCard().getSuffixOnSSNCard();
			}
			if (primaryTaxpersonId == personId) {
				recipient = new EnrollmentIrsCoveredIndividualDTO();
				recipient.setFirstName(firstName);
				recipient.setLastName(lastName);
				recipient.setMiddleName(middleName);
				recipient.setSuffixName(suffix);
				recipient.setDob(dobirth);
				if (ssn != null && ssn.trim().length() > 0) {
					recipient.setSsn(ssn);
				}
				recipient.setAddressLine1Txt(street1);
				recipient.setAddressLine2Txt(street2);
				recipient.setCityNm(city);
				recipient.setUsStateCd(stateCode);
				recipient.setUszipCd(zip);
				recipient.setUszipExtensionCd(countyCode);
				if (personGuId != null && personGuId.trim().length() > 0) {
					recipient.setMemberId(personGuId);
				}
			} else if (spouseId != 0 && spouseId == personId) {
				recipientSpouse = new EnrollmentIrsCoveredIndividualDTO();
				recipientSpouse.setFirstName(firstName);
				recipientSpouse.setLastName(lastName);
				recipientSpouse.setMiddleName(middleName);
				recipientSpouse.setSuffixName(suffix);
				recipientSpouse.setDob(dobirth);
				if (ssn != null && ssn.trim().length() > 0) {
					recipientSpouse.setSsn(ssn);
				}
				if (personGuId != null && personGuId.trim().length() > 0) {
					recipientSpouse.setMemberId(personGuId);
				}
			} else if(null != recipientSpouse && null != recipient){
				break;
			}
		}
		if(null != recipient){
			enrollmentAnnualIrsRecipientDTO.setRecipient(recipient);
			enrollmentAnnualIrsRecipientDTO.setRecipientSpouse(recipientSpouse);
		}else{
			throw new GIException("No primary recipient found for application");
		}
		return enrollmentAnnualIrsRecipientDTO;
	}
	
	/**
	 * Returns Spouse if not present in main application
	 * @param ssapApplicationIds
	 * @return EnrollmentIrsCoveredIndividualDTO
	 */
	private EnrollmentIrsCoveredIndividualDTO getSpouse(Set<Long> ssapApplicationIds) {
		EnrollmentIrsCoveredIndividualDTO spouse = null;
		for(Long ssapId : ssapApplicationIds){
			try{
				List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapId);
				if(null != ssapApplications && !ssapApplications.isEmpty()){
					SsapApplication ssapApp = ssapApplications.get(0);
					String ssapJson = null;
					ssapJson = ssapApp.getApplicationData();
					LOGGER.info(ssapJson);
					SingleStreamlinedApplication ssapApplication = null;
					if(null != ssapJson){
						ssapApplication = ssapJsonBuilder.transformFromJson(ssapJson);
						int spouseId = 0;
						int primaryTaxpersonId = 1;
						if(null !=   ssapApplication){
							if (ssapApp.getFinancialAssistanceFlag().equals("Y") && ssapApplication.getPrimaryTaxFilerPersonId() != null) {
								primaryTaxpersonId = ssapApplication.getPrimaryTaxFilerPersonId();
							}
							spouseId = getSpouseId(ssapApplication, primaryTaxpersonId);
							if(spouseId != 0){
								List<HouseholdMember> householdMembers = ssapApplication.getTaxHousehold().get(0).getHouseholdMember();
								spouse = getRecipientInformation(householdMembers, primaryTaxpersonId, spouseId).getRecipientSpouse();
								if(spouse != null){
									break;
								}
							}
						}
					}
				}
			}catch(Exception e){
				LOGGER.error("Error in fetching spouse details from ssap application table :: " + ssapId ,e);
			}
		}
		return spouse;
	}
	
	/**
	 * Get spouse id from application data
	 * @param ssapApplication
	 * @param primaryTaxpersonId
	 * @return spouse Id
	 * 
	 */
	private int getSpouseId(SingleStreamlinedApplication ssapApplication, int primaryTaxpersonId) {

		int spouseId = 0;
		try {
			if(null != ssapApplication){
				List<HouseholdMember> householdMembers = ssapApplication.getTaxHousehold().get(0).getHouseholdMember();
				for (HouseholdMember householdMember : householdMembers) {
					if (householdMember.getPersonId() == primaryTaxpersonId) {
						for (BloodRelationship relation : householdMember.getBloodRelationship()) {
							if (relation.getIndividualPersonId().equals(String.valueOf(householdMember.getPersonId())) && ("01").equals(relation.getRelation())) {
								spouseId = Integer.parseInt(relation.getRelatedPersonId());

							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception during getSpouseId" + e.getMessage(), e);

		}
		return spouseId;
	}
}
