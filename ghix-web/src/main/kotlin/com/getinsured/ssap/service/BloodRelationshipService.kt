package com.getinsured.ssap.service

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.getinsured.ssap.model.Relationship
import com.getinsured.ssap.util.JsonUtil
import com.getinsured.web.configuration.StateConfiguration
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.io.ClassPathResource
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

/**
 * Blood Relationship service.
 * @author Yevgen Golubenko
 * @since 6/20/19
 */
open class BloodRelationshipService {

    companion object {
        private val log = LoggerFactory.getLogger(BloodRelationshipService::class.java)
    }

    @Autowired
    lateinit var stateConfiguration: StateConfiguration

    private var relationships : List<Relationship> = ArrayList();

    @PostConstruct
    open fun postConstruct() {
        if(relationships.isEmpty()) {

            log.info("Loading relationships JSON file")
            if(stateConfiguration.stateCode != null) {
                val fileName = String.format("/configuration/%s/relationships.json", stateConfiguration.stateCode.toLowerCase())
                val resource = ClassPathResource(fileName)
                val relationshipJson = JsonUtil.readFile(resource);

                if(relationshipJson != null) {
                    val om  = ObjectMapper();
                    val typeRef = object : TypeReference<List<Relationship>>() {}

                    try {
                        relationships = om.readValue(relationshipJson, typeRef);
                    }
                    catch (e : Exception) {
                        log.error("Unable to load relationship.json file: {}", e.message)
                    }
                }
            }
            else {
                log.error("Unable to load relationship.json file")
            }
        }
    }

    /**
     * Returns List of [Relationship] objects for current state exchange.
     * @return List of [Relationship]
     */
    open fun getBloodRelationships() = relationships

    /**
     * Returns [Relationship] for given code.
     *
     * @param code relationship code.
     * @return [Relationship] for given code.
     */
    open fun getBloodRelationship(code : String) = relationships.find { it.code == code }

    /**
     * Returns [Relationship] for given type.
     *
     * @param type relationship type.
     * @return [Relationship] for given type.
     */
    open fun getBloodRelationshipByType(type : String) = relationships.find { it.type == type }

    /**
     * Returns [Relationship] by given mirror type.
     * @param mirrorType mirror type
     * @return [Relationship] by given mirror type.
     * @see getMirrorRelationshipByCode
     */
    open fun getBloodRelationshipByMirrorType(mirrorType: String) = relationships.find { it.mirrorType == mirrorType }


    /**
     * Returns mirror [Relationship] for given code.
     *
     * @param code relationship code
     * @return Mirror [Relationship] for given code.
     */
    open fun getMirrorRelationshipByCode(code : String) : Relationship? {
        val relationship : Relationship? = getBloodRelationship(code)

        if(relationship != null) {
            val mirrorRelationship : Relationship? = getBloodRelationshipByMirrorType(relationship.mirrorType)

            if(mirrorRelationship != null) {
                return mirrorRelationship
            }
        }

        return null;
    }

    /**
     * Returns mirror [Relationship] code
     *
     * @param code relationship code
     * @return [Relationship.code] for given code.
     */
    open fun getMirrorRelationshipCode(code : String) : String? {
        val relationship : Relationship? = getBloodRelationship(code)

        if(relationship != null) {
            val mirrorRelationship : Relationship? = getBloodRelationshipByMirrorType(relationship.mirrorType)

            if(mirrorRelationship != null) {
                return mirrorRelationship.code
            }
        }

        return null;
    }
}


