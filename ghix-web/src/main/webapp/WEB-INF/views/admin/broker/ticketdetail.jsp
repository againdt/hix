<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript">
$('.my-link').click(function(e) { e.preventDefault(); }); 
</script>
<div class="gutter10-lr">
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<!--start page-breadcrumb -->
		<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.lastName}</li>
			</ul>
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p>
						<c:out value="${errorMsg}"></c:out>
					<p />
				</c:if>
				<br>
			</div>
			
		</div><!--  end of row-fluid -->
	</div><!--  end l-page-breadcrumb -->
	
	<div class="row-fluid">
		<h1><a name="skip"></a>${broker.user.firstName}&nbsp;${broker.user.lastName}</h1>
	</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header"></div>
			<ul class="nav nav-list">						
				<li><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>
				<li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
				<li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>
				
				<li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>	
				<li class="active"><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
				<%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li>	 --%>		
			</ul>
			
			<br>
			
			<div class="header"><h4><i class="icon-cog icon-white"></i>Actions</h4></div>
			<ul class="nav nav-list">
			<c:if test="${checkDilog == null}">  
				<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Agent Account</a></li>
			</c:if>
			<c:if test="${checkDilog != null}">  
				<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${broker.user.firstName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i>View Agent Account</a></li>
			</c:if>
            </ul>   
		</div><!-- end of span3 -->
    	
        <div class="span9">		
            
                <div class="header">
                    <h4>Ticket Summary</h4>
             
                </div>
                <div class="row-fluid">
                	<form method="POST" action="#" name="frmCrmTicketDetailInfo" id="frmCrmTicketDetailInfo" class="form-horizontal">
                		<table class="table table-border-none verticalThead">							
								<tbody><tr>
									<th class="txt-right span4">Task</th>
									<td><strong>${tkmTicketsObj.subject} </strong></td>
								</tr>
								<tr>
									<th class="txt-right span4">Ticket Type</th>
									<td><strong>${tkmTicketsObj.tkmWorkflows.type}</strong></td>
								</tr>							
								<tr>
									<th class="txt-right">Requestor</th>
									<td><strong>${tkmTicketsObj.role.firstName}  ${tkmTicketsObj.role.lastName} </strong></td>
								</tr>
								<tr>
									<th class="txt-right">Queue Assigned</th>
									<td><strong>${tkmTicketsObj.tkmWorkflows.description}</strong>
									</td>
								</tr>
								<tr>
									<th class="txt-right">Created On</th>
									<td><strong>${tkmTicketsObj.created}</strong></td>
								</tr>
								<tr>
									<th class="txt-right">Description</th>
									<td><strong>${tkmTicketsObj.description}</strong></td>
								</tr>
								
							</tbody>
						</table>

                    </form>
                </div>
        </div>
    </div><!--  end of row-fluid -->
</div>    	<!--  end of gutter10 -->


<script type="text/javascript">
function saveComment(ticketId)
{
	var validateUrl = "<c:url value='/admin/ticketmgmt/savecomment'/>";
	var commentData = $("#comment_text").val();
	$.ajax({
		url: validateUrl,
		data: {ticketId : ticketId,
			comment : commentData},
		success: function(response)
		{
			if(response)
			{
				$('#markCompleteDialog').modal('hide');
				$('#markItComplete a').removeAttr('href')
			}
		}
		});
}




</script>
<form name="dialogForm" id="dialogForm" action="<c:url value="/admin/broker/dashboard" />" novalidate="novalidate">
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Agent Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Agent View" will take you to the Agent's portal for ${broker.user.firstName}.<br/>
					Through this portal you will be able to take actions on behalf of the agent.<br/>
					Proceed to Agent view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkAgentView" name="checkAgentView"  > 
    <div class="pull-left">&nbsp; Don't show this message again.</div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
   <button class="btn btn-primary" type="submit">Agent View</button>
    <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="broker"/>" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${broker.id}"/>" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${broker.user.firstName}" />
    <!-- a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
