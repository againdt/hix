package com.getinsured.eligibility.ind19.client;


import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.eligibility.indportal.indCreation.HouseholdFactory;
import com.getinsured.eligibility.indportal.indCreation.IHousehold;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.DisenrollMembers;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.individualinformation.IndividualInformationResponse;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ObjectFactory;
import com.getinsured.hix.webservice.plandisplay.individualinformation.ProductType;
import com.getinsured.hix.webservice.plandisplay.individualinformation.RoleTypeVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesNoVal;
import com.getinsured.hix.webservice.plandisplay.individualinformation.YesVal;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.dto.ind19.Ind19ClientResponse;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

@Component
public class Ind19ClientUtil {

	private static final Logger LOGGER = Logger.getLogger(Ind19ClientUtil.class);
	
	private	final ObjectFactory objectFactory = new ObjectFactory();
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private ResourceCreator resourceCreator;

	@Autowired
	private WebServiceTemplate ind19WebServiceTemplate;
	
	@Autowired
	private SoapWebClient<?> soapWebClient;

	@Autowired private HouseholdFactory householdFactory;
	

	@Autowired private CoverageStartDateService coverageStartDateService;
	@Autowired private GIWSPayloadService giwsPayloadService;
		
	private static final String SHORT_DATE_FORMAT = "MM/dd/yyyy";
	
	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public ResourceCreator getResourceCreator() {
		return resourceCreator;
	}
	
	public WebServiceTemplate getInd19WebServiceTemplate() {
		return ind19WebServiceTemplate;
	}

	public SoapWebClient<?> getSoapWebClient() {
		return soapWebClient;
	}

	public Ind19ClientResponse consumeInd19(Ind19ClientRequest ind19ClientRequest){
		
		Ind19ClientResponse ind19ClientResponse = new Ind19ClientResponse();
		ind19ClientResponse.setIsInd19CallSuccess(false);
		
		if(ind19ClientRequest==null || ind19ClientRequest.getCaseNumber()==null || ind19ClientRequest.getCaseNumber().trim().length()==0){
			LOGGER.error("Error occured while consuming IND19");
			//ind19ClientResponse.setIsInd19CallSuccess(false);
			Map<String, String> errorCodesAndMessages = new HashMap<String, String>();
			errorCodesAndMessages.put(Ind19ClientConstants.IND19_INVALID_REQUEST_ERROR_CODE, Ind19ClientConstants.IND19_INVALID_REQUEST_ERROR_MESSAGE);
			ind19ClientResponse.setErrorCodesAndMessages(errorCodesAndMessages);
			return ind19ClientResponse;
		}

		try{
			/**
			 * 1. Retrieve application json from SSAP_APPLICATIONS
			 * 2. Retrieve applicants from SSAP_APPLICANTS
			 * 3. Retrieve Eligibility Results
			 * 4. Construct Ind19Request
			 * 5. Post to Ind19
			 * 6. Extract shoppingId
			 * 
			 */
			
			List<SsapApplication> ssapApplications = this.getSsapApplicationRepository().findByCaseNumber(ind19ClientRequest.getCaseNumber());
			if(ssapApplications != null && !ssapApplications.isEmpty() && ssapApplications.get(0)!=null){
				SsapApplication ssapForInd19 = ssapApplications.get(0);
//				if(ssapForInd19.getCmrHouseoldId()!=null && ssapForInd19.getCmrHouseoldId().longValue()!=ind19ClientRequest.getHouseHoldId()){
//					throw new GIRuntimeException("Access to unauthorized application ");
//				}
				IndividualInformationRequest individualInformationRequest = objectFactory.createIndividualInformationRequest();
				populateIndividualInformationRequest(individualInformationRequest, ssapForInd19, ind19ClientRequest);
				IndividualInformationResponse individualInformationResponse = invokeInd19(individualInformationRequest);
				ind19ClientResponse.setPlanShoppingId(individualInformationResponse.getGiHouseholdId());
				ind19ClientResponse.setIsInd19CallSuccess(true);
				if(!("200".equalsIgnoreCase(individualInformationResponse.getResponseCode()))){
					Map<String, String> errorCodesAndMessages = new HashMap<String, String>(1);
					errorCodesAndMessages.put(individualInformationResponse.getResponseCode(), individualInformationResponse.getResponseDescription());
					ind19ClientResponse.setErrorCodesAndMessages(errorCodesAndMessages);
					ind19ClientResponse.setIsInd19CallSuccess(false);
				}
			}			
			
		}
		catch (GIException ex){
			Map<String, String> errorCodesAndMessages = new HashMap<String, String>();
			errorCodesAndMessages.put("500", ex.getMessage()!=null?ex.getMessage():Ind19ClientConstants.IND19_GENERIC_ERROR_MESSAGE);
			ind19ClientResponse.setErrorCodesAndMessages(errorCodesAndMessages);
		}
		catch(Exception ex){
			LOGGER.error("Error occured while consuming IND19",ex);
			//ind19ClientResponse.setIsInd19CallSuccess(false);
			Map<String, String> errorCodesAndMessages = new HashMap<String, String>();
			errorCodesAndMessages.put(Ind19ClientConstants.IND19_GENERIC_ERROR_CODE, Ind19ClientConstants.IND19_GENERIC_ERROR_MESSAGE);
			ind19ClientResponse.setErrorCodesAndMessages(errorCodesAndMessages);
		}
		
		return ind19ClientResponse;
	}
	
	/***
	 * 
	 */
	
	@SuppressWarnings("unchecked")
	public void populateIndividualInformationRequest(IndividualInformationRequest individualInformationRequest, SsapApplication ssapForInd19, Ind19ClientRequest ind19ClientRequest) throws GIException {

		LOGGER.debug("Constructing request for Ind19 invocation");
		String ssapJSON = ssapForInd19.getApplicationData();
		LOGGER.debug(ssapJSON);
		try {
			// HouseHold
			IndividualInformationRequest.Household houseHold = objectFactory.createIndividualInformationRequestHousehold();
			IndividualInformationRequest.Household.Members members = objectFactory.createIndividualInformationRequestHouseholdMembers();
			
			houseHold.setSADPFlag(YesNoVal.valueOf("N"));
						
			setUserRole(ind19ClientRequest, houseHold);
			
			IHousehold ind19Household  = householdFactory.getObject("IND19Household");	
			
			HouseholdData householdData = new HouseholdData();
			String productType = ind19Household.getProductType(ind19ClientRequest.getGroup(), ssapForInd19.getId(), ssapForInd19.getApplicationType());
			houseHold.setProductType(ProductType.fromValue(productType));
			householdData.setProductType(productType);
			
			ind19Household.setHouseholdData(ssapForInd19, ind19ClientRequest, householdData);			
			setKeepOnlyFlag(householdData, houseHold);			
			setTerminationFlag(householdData, houseHold);			
			setDentalTerminationFlag(householdData, houseHold);	
						
			ResponsiblePerson responsiblePerson = (ResponsiblePerson) ind19Household.createResponsiblePerson(householdData);
			houseHold.setResponsiblePerson(responsiblePerson);
			
			Date coverageStartDate = coverageStartDateService.computeCoverageStartDateIncludingTermMembers(BigDecimal.valueOf(ssapForInd19.getId()), ind19ClientRequest.getIsChangePlanFlow(), ind19ClientRequest.getIsSEPPlanFlow(),householdData.getPriorSsapApplicationId());
			householdData.setCoverageDate(coverageStartDate);
			
			HouseHoldContact houseHoldContact = (HouseHoldContact) ind19Household.createHouseHoldContact(householdData);
			houseHold.setHouseHoldContact(houseHoldContact);

			Individual individual = (Individual) ind19Household.createIndividual(householdData, ssapForInd19, ind19ClientRequest);
			houseHold.setIndividual(individual);
			
			Members memberList = (Members) ind19Household.createMembers(members, ssapForInd19, ind19ClientRequest, householdData, null, null);
			houseHold.setMembers(memberList);
			
			IndividualInformationRequest.Household.DisenrollMembers disenrollMembers = (DisenrollMembers) ind19Household.createDisenrollMembers(ind19ClientRequest, householdData);
			if(disenrollMembers != null && disenrollMembers.getDisenrollMember() != null && !disenrollMembers.getDisenrollMember().isEmpty()){
				houseHold.setDisenrollMembers(disenrollMembers);
			}
			
			
			houseHold.setApplicationType(ssapForInd19.getApplicationType());
			houseHold.setApplicationId(ssapForInd19.getId());
			if(ind19ClientRequest.getIsChangePlanFlow() || ind19ClientRequest.getIsSEPPlanFlow()){				
				houseHold.setKeepOnlyFlag(YesNoVal.N);				
			}
			
			individualInformationRequest.setHousehold(houseHold);
			
			
		} 
		catch (Exception e) {
			LOGGER.error("Exception during IND19 request creation, mapping", e);
			throw new GIException(e.getMessage() != null ? e.getMessage() : "Exception during IND19 request creation, mapping", e);
		}

	}
	
	private void setDentalTerminationFlag(HouseholdData householdData, Household houseHold) {
		if(StringUtils.isNotBlank(householdData.getDentalTerminationFlag()) && "Y".equalsIgnoreCase(householdData.getDentalTerminationFlag())){
			houseHold.setDentalTerminationFlag(YesVal.Y);
		}	
	}
	
	private void setTerminationFlag(HouseholdData householdData, Household houseHold) {
		if(StringUtils.isNotBlank(householdData.getTerminationFlag()) && "Y".equalsIgnoreCase(householdData.getTerminationFlag())){
			houseHold.setTerminationFlag(YesVal.Y);
		}		
	}
	
	private void setKeepOnlyFlag(HouseholdData householdData, Household houseHold) {
		if(StringUtils.isNotBlank(householdData.getKeepOnlyFlag()) && "Y".equalsIgnoreCase(householdData.getKeepOnlyFlag())){
			houseHold.setKeepOnlyFlag(YesNoVal.Y);
		}else if(StringUtils.isNotBlank(householdData.getKeepOnlyFlag()) && "N".equalsIgnoreCase(householdData.getKeepOnlyFlag())){
			houseHold.setKeepOnlyFlag(YesNoVal.N);
		}		
	}

	private void setUserRole(Ind19ClientRequest ind19ClientRequest, Household houseHold) {
		if (ind19ClientRequest.getBrokerId() != null && ind19ClientRequest.getBrokerId() > 0) {
			houseHold.setUserRoleType(RoleTypeVal.AGENT);
			houseHold.setUserRoleId(String.valueOf(ind19ClientRequest.getBrokerId()));
		} else if (ind19ClientRequest.getAssisterId() != null && ind19ClientRequest.getAssisterId() > 0) {
			houseHold.setUserRoleType(RoleTypeVal.ASSISTER);
			houseHold.setUserRoleId(String.valueOf(ind19ClientRequest.getAssisterId()));
		}		
	}

	public String changeDateFormatString(String inputDateStr) throws GIException {
		final String OLD_FORMAT = "MMM dd, yyyy hh:ww:ss";
		final String NEW_FORMAT = SHORT_DATE_FORMAT;
		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date d = null;
		try {
			d = sdf.parse(inputDateStr);
		} catch (Exception e) {

			LOGGER.error(" exception during parsing date ", e);
			GIException gie = new GIException(e);
			gie.setErrorMsg("exception during parsing date" + inputDateStr
					+ "expected format is MMM dd, yyyy hh:ww:ss");
			throw gie;
		}

		sdf.applyPattern(NEW_FORMAT);
		return sdf.format(d);
	}

	public IndividualInformationResponse invokeInd19(IndividualInformationRequest individualInformationRequest){
		IndividualInformationResponse individualInformationResponse = objectFactory.createIndividualInformationResponse();
		try{
			ind19WebServiceTemplate.setDefaultUri(GhixEndPoints.PLANDISPLAY_URL+"webservice/plandisplay/endpoints/IndividualInformationService.wsdl");
			individualInformationResponse = (IndividualInformationResponse) ind19WebServiceTemplate.marshalSendAndReceive(individualInformationRequest);
		}catch(SoapFaultClientException e){
			String validationMessage = e.getMessage();
			if(e.getSoapFault() != null && e.getSoapFault().getSource() != null){
				validationMessage = getSaopFaultDetails(e.getSoapFault().getSource());				
			}
				    
			LOGGER.error("Error while invoking the IndividualInformationService ", e);
			LOGGER.error("fault string"+e.getFaultStringOrReason());
			LOGGER.error("msg"+e.getMessage());
			LOGGER.error("fault code "+e.getFaultCode());
			LOGGER.error("root cause"+e.getRootCause());
			individualInformationResponse.setResponseDescription("Soap Validation Failure Exception");
			individualInformationResponse.setResponseCode("400");
			populateGiWSPayload(individualInformationRequest, validationMessage);
		}catch(Exception ex){
			LOGGER.error("IND19 from ghix-plandisplay failed", ex);
			individualInformationResponse.setResponseDescription("IND19 from ghix-plandisplay failed");
			individualInformationResponse.setResponseCode("500");
			populateGiWSPayload(individualInformationRequest, ex.getMessage());
		}
		
		return individualInformationResponse;
	}
	
	
	public int getAgeFromDob(String dob, Date coverageStartDate){

		if(StringUtils.isBlank(dob)){
			return 0;
		}

		//Implementation change as per Joda Time
		LocalDate now = new LocalDate(coverageStartDate.getTime());
		
		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());
				
		Years years = Years.yearsBetween(birthdate, now);
		int age =  years.getYears();

		return age;
		
		/*Calendar cal = TSCalendar.getInstance();
		long today = cal.getTimeInMillis();
	    Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
	    long birthDate = dobDate.getTime();
	    
	    long diff = today - birthDate;
	    long diffYears = 0;
	    long millisinYear = 0;
	    
	    if(cal.get(Calendar.YEAR) % 4 == 0){
	    	millisinYear = (long)1000 * 60 * 60
	    	        * 24 * 366;
	    }
	    else{
	    	millisinYear = (long)1000 * 60 * 60
	    	        * 24 * 365;
	    	
	    }
	    diffYears =  diff / millisinYear;
	    
	    return Long.valueOf(diffYears).intValue();*/

	}
	
		
	/**
	 * Story HIX-53174 Invoke IND 19 with correct mapping and MRC codes for the SEP event
	 * 
	 * Sub-task HIX-55913
	 * Populate enrollment and disenrollment information in IND19
	 * 
	 * @author Nikhil Talreja, Sunil Desu
	 * @param coverageStartDate2 
	 * 
	 */
	public Date getDisenrolledMembers(DisenrollMembers disenrollMembers, 
			List<SsapApplicantEvent> applicantEvents, Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, Date coverageStartDate){
		Date newCoverageStartDate = null;
		if(applicantEvents == null || enrollmentsByTypeMap == null){
			return newCoverageStartDate;
		}
		
		
		Map<String, List<String>> memberIdEnrollmentListMap = new HashMap<String, List<String>>();

		//Create the list of enrollments for disenrolled members
		for (Map.Entry<String, EnrollmentShopDTO> entry : enrollmentsByTypeMap.entrySet()){
			EnrollmentShopDTO enrollmentShopDTO = entry.getValue();
			List<EnrolleeShopDTO> enrollees = enrollmentShopDTO.getEnrolleeShopDTOList();
			if(enrollees != null && !enrollees.isEmpty()){
				for(EnrolleeShopDTO enrollee:enrollees){
					if(enrollee.getExchgIndivIdentifier() != null){
						if(memberIdEnrollmentListMap.get(enrollee.getExchgIndivIdentifier()) == null){
							memberIdEnrollmentListMap.put(enrollee.getExchgIndivIdentifier(),new ArrayList<String>());
						}
						memberIdEnrollmentListMap.get(enrollee.getExchgIndivIdentifier()).add(String.valueOf(enrollmentShopDTO.getEnrollmentId()));
					}					
				}
			}
		}
		
		
		List<IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember> disenrolledMembers = 
				new ArrayList<IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember>();
		
		/*
		 * HIX-59232
		 * Multiple Terminations with different dates is causing 
		 * inconsistent behavior in Enrollment + 
		 * Enrollment Overlap when consumers change plans during SEP
		 */
		Map<Integer, List<Timestamp>> eventTimestamps = getSEPRemoveEventTimestamps(applicantEvents);
		List<Timestamp> type1Timestamps = eventTimestamps.get(Ind19ClientConstants.TYPE_1_EVENT);
		Calendar terminationDate = TSCalendar.getInstance();
		
		//Check for type 1 event
		if(type1Timestamps != null && !type1Timestamps.isEmpty()){
			List<Timestamp> allEventTimestamps = new ArrayList<Timestamp>();
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_1_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_2_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_3_EVENT));
			allEventTimestamps.addAll(eventTimestamps.get(Ind19ClientConstants.TYPE_4_EVENT));
			allEventTimestamps.add(new Timestamp(coverageStartDate.getTime()));

			Collections.sort(allEventTimestamps,Collections.reverseOrder());
			terminationDate.setTimeInMillis(allEventTimestamps.get(0).getTime());
			
			Calendar covStart = TSCalendar.getInstance();
			covStart.setTime(terminationDate.getTime());
			if(covStart.getTime().before(new TSDate())){
				covStart.add(Calendar.DATE, 1);
			}
			newCoverageStartDate = covStart.getTime();
		}
		//There is no type 1 event
		else{
			//Termination date is 1 day before computed coverage start date
			terminationDate.setTime(coverageStartDate);
			terminationDate.add(Calendar.DATE, -1);
		}
		
		//Check for Removal of applicants during SEP
		for(SsapApplicantEvent applicantEvent : applicantEvents){
			SepEvents sepEvent = applicantEvent.getSepEvents();
			if(sepEvent != null){
				String changeType = sepEvent.getChangeType();
				/*
				 * HIX-70832 IND 19 Improvement to Support Dental CR, 
				 * SEPs in OE Overlap Period, and future dated LCE
				 * 
				 * If the individual is not enrolled either in health or dental, 
				 * but is marked as delete by the change reporting modules (both Financial and Non-Financial) 
				 * then do not populate that person in the dis-enroll loop. 
				 */
				String applicantGuId = applicantEvent.getSsapApplicant().getApplicantGuid();
				List<String> memberEnrollments = 
						memberIdEnrollmentListMap.get(applicantGuId);
				if(StringUtils.equalsIgnoreCase(changeType, "REMOVE") 
						&& memberEnrollments != null && !memberEnrollments.isEmpty()){
					
					IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember disenrolledMember = 
							objectFactory.createIndividualInformationRequestHouseholdDisenrollMembersDisenrollMember();
					
					disenrolledMember.setMemberId(applicantGuId);
					//Event type is Death
					String mrcCode = sepEvent.getMrcCode();
					if(StringUtils.equalsIgnoreCase(mrcCode,"03")){
						disenrolledMember.setDeathDate(dateToString(SHORT_DATE_FORMAT,applicantEvent.getEventDate()));
					}
					disenrolledMember.setTerminationDate(dateToString(SHORT_DATE_FORMAT,new Timestamp(terminationDate.getTimeInMillis())));
					disenrolledMember.setTerminationReasonCode(mrcCode);
					
					IndividualInformationRequest.Household.DisenrollMembers.DisenrollMember.Enrollments enrollments = 
							objectFactory.createIndividualInformationRequestHouseholdDisenrollMembersDisenrollMemberEnrollments();
					enrollments.getEnrollmentId().addAll(memberEnrollments);
					
					disenrolledMember.setEnrollments(enrollments);
					//Add memeber to list of disenrolledMembers list
					disenrolledMembers.add(disenrolledMember);
				}
			}
		}
		
		//Add all disenrolled members to the disenrollMembers list
		disenrollMembers.getDisenrollMember().addAll(disenrolledMembers);
		
		return newCoverageStartDate;
	}
	
	/**
	 * Converts timestamp to string for a particular format
	 * 
	 * Does a null safe operation
	 * 
	 * @param format - Format of response string
	 * @param date - Timestamp Object to be formatted
	 * @return String - Formatted String or null
	 * 
	 * @author Nikhil Talreja
	 * 
	 */
	private String dateToString(String format, Timestamp date){
		
		if(date == null || StringUtils.isBlank(format)){
			return null;
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		return formatter.format(date);
	}
	
	public SsapApplication getEnrolledSsapForCoverageYearAndHousehold(BigDecimal consumerHouseholdId,int year) {
		//Year is from IND19ClientRequest
		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYear(consumerHouseholdId,year);
		if(ssapApplications != null && !ssapApplications.isEmpty()){
			for(SsapApplication ssapApplicationResource: ssapApplications){
				if(StringUtils.equalsIgnoreCase(ssapApplicationResource.getApplicationStatus(),"EN")){
					return ssapApplicationResource;
				}
			}
		}		
		return null;
	}
	
	/**
	 * Improvement HIX-59232 Multiple Terminations with different dates 
	 * is causing inconsistent behavior in Enrollment + 
	 * Enrollment Overlap when consumers change plans during SEP
	 * 
	 * @author Nikhil Talreja, Sunil Desu
	 * 
	 * @param applicantEvents - List of SSAP Applicant Events
	 * @return Map<String, Integer> containing counts
	 */
	public Map<Integer, List<Timestamp>> getSEPRemoveEventTimestamps(List<SsapApplicantEvent> applicantEvents){
		
		LOGGER.debug("Getting Event times for SSAP Applicant events");
		Map<Integer, List<Timestamp>> eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		
		if(applicantEvents == null || applicantEvents.isEmpty()){
			return eventTimestamps;
		}
		
		List<Timestamp> type1EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type2EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type3EventDates = new ArrayList<Timestamp>(0);
		List<Timestamp> type4EventDates = new ArrayList<Timestamp>(0);
		
		for (SsapApplicantEvent event : applicantEvents) {

			SepEvents sepEvent = event.getSepEvents();
			if (sepEvent != null && StringUtils.equalsIgnoreCase(sepEvent.getChangeType(), "REMOVE")) {
				switch (sepEvent.getType()) {
				case Ind19ClientConstants.TYPE_1_EVENT:
					type1EventDates.add(event.getEventDate());
					break;
				case Ind19ClientConstants.TYPE_2_EVENT:
					type2EventDates.add(event.getEventDate());
					break;
				case Ind19ClientConstants.TYPE_3_EVENT:
					type3EventDates.add(event.getEventDate());
					break;
				case Ind19ClientConstants.TYPE_4_EVENT:
					type4EventDates.add(event.getEventDate());
					break;
				default:
					LOGGER.warn("Invalid Event Type");
				}
			}
		}
		eventTimestamps = new HashMap<Integer, List<Timestamp>>();
		eventTimestamps.put(Ind19ClientConstants.TYPE_1_EVENT, type1EventDates);
		eventTimestamps.put(Ind19ClientConstants.TYPE_2_EVENT, type2EventDates);
		eventTimestamps.put(Ind19ClientConstants.TYPE_3_EVENT, type3EventDates);
		eventTimestamps.put(Ind19ClientConstants.TYPE_4_EVENT, type4EventDates);
		
		LOGGER.debug("Event times: " + eventTimestamps);
		return eventTimestamps;
	}
	
	public void populateGiWSPayload(IndividualInformationRequest individualInformationRequest, String exceptionMessage) {
		try {
			JAXBContext context  = JAXBContext.newInstance(IndividualInformationRequest.class);
			StringWriter s = new StringWriter();
			context.createMarshaller().marshal(individualInformationRequest, s);
			GIWSPayload newGiWSPayload = new GIWSPayload();
			newGiWSPayload.setEndpointUrl(GhixEndPoints.PLANDISPLAY_URL+"webservice/plandisplay/endpoints/IndividualInformationService.wsdl");
			newGiWSPayload.setEndpointFunction("invokeInd19");
			newGiWSPayload.setRequestPayload(s.toString());
			newGiWSPayload.setStatus("FAILURE");		
			newGiWSPayload.setExceptionMessage(exceptionMessage);
			newGiWSPayload.setSsapApplicationId(individualInformationRequest.getHousehold().getApplicationId());
			giwsPayloadService.save(newGiWSPayload);;
		} catch (Exception ex) {				
			LOGGER.error("Error while coverting to XML", ex);
		}		
	}
	
	private String getSaopFaultDetails(Source source){
		String validationMessage = null;
		try{
			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			StringWriter writer = new StringWriter();	   
			transformer.transform(source, new StreamResult(writer));
			if(writer != null){
				validationMessage = writer.toString();
			}
		}catch(Exception e) {
			LOGGER.error("Transformer Exception "+e.getMessage());
			validationMessage = e.getMessage();
		}
		return validationMessage;
	}
	
}
