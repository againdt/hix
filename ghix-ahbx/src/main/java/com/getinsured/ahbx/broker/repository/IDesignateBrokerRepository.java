package com.getinsured.ahbx.broker.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.DesignateBroker;

/**
 * Spring-JPA repository that encapsulates methods that interact with the DesignateBroker table
 */
public interface IDesignateBrokerRepository extends JpaRepository<DesignateBroker, Integer>{

	/**
	 * Fetches the designateBroker record against the broker Id and external employer Id
	 * 
	 * @param brokerId 				the broker id
	 * @param externalEmployerId 	the external employer id
	 * @return the designateBroker record against the broker Id and external employer Id
	 */
	DesignateBroker findByBrokerIdAndExternalEmployerId(int brokerId, int externalEmployerId);

	/**
	 * Fetches the designateBroker record against the broker Id and external individual Id
	 * 
	 * @param brokerId				the broker id
	 * @param externalIndividualId	the external individual id
	 * @return the designateBroker record against the broker Id and external individual Id
	 */
	DesignateBroker findByBrokerIdAndExternalIndividualId(int brokerId, int externalIndividualId);
}