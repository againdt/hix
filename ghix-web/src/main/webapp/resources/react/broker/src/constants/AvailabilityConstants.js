
export const SELECT_CHOICE = 'select';
export const CLOSED_VALUE = 'closed';
export const BROKER_ACTIVE = 'ACTIVE';
export const BROKER_INACTIVE = 'INACTIVE';
export const MONDAY = 'Monday';
export const TUESDAY = 'Tuesday';
export const WEDNESDAY = 'Wednesday';
export const THURSDAY = 'Thursday';
export const FRIDAY = 'Friday';
export const SATURDAY = 'Saturday';
export const SUNDAY = 'Sunday';
export const CLOSED_NAME = 'closed_';
export const START = 'Start';
export const END = 'End';