package com.getinsured.eligibility.ssap.validation.service;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.sep.repository.SepEventsRepository;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.ssap.documentverification.exceptions.SsapDocumentsServiceException;
import com.getinsured.eligibility.ssap.documentverification.service.SsapApplicantDocumentService;
import com.getinsured.hix.model.CancelTicketRequest;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.dto.QleApplicantsDto;
import com.getinsured.iex.dto.QleEventDto;
import com.getinsured.iex.dto.QleRequestDto;
import com.getinsured.iex.dto.QleResponeDto;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.IConsumerDocumentRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;



@Component
public class QleValidationServiceImpl implements QleValidationService {

	private static final String INDIVIDUAL_MODULE_USER = "individual";
	private static final String NON_FINANCIAL_APPLICATION = "Non Financial Application";
	private static final String FINANCIAL_APPLICATION = "Financial Application";
	private static final String APPLICATION_TYPE = "Application Type : ";
	private static final String N = "N";
	private static final String Y = "Y";
	private static final String CASE_NUMBER = "Case Number : ";
	private static final String EVENT_NAME = "Event Name : ";
	private static final String SPECIAL_ENROLLMENT_START_DATE = "Special Enrollment Start Date : ";
	private static final String SPECIAL_ENROLLMENT_END_DATE = "Special Enrollment End Date : ";
	private static final String MM_DD_YYYY = "MM/dd/YYYY";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QleValidationServiceImpl.class);
	@Autowired private Gson platformGson;
	@Autowired
	SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	SsapApplicantEventRepository  ssapApplicantEventRepository;
	@Autowired
	SepEventsRepository sepEventsRepository;
	@Autowired
	private IConsumerDocumentRepository consumerDocumentRepository;
	@Autowired 
	private ContentManagementService ecmService;
	@Autowired 
	private SsapApplicantDocumentService ssapApplicantDocumentService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private CommentTargetService commentTargetService;
	@Autowired
	private QLEValidationService qleValidationService;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	private RoleService roleService;
	
	@Override
	public QleResponeDto retrieveEventDataByApplicationId(String caseNumber,long moduleId,String gated) {
		//fetch applicants with event details
		
		QleResponeDto qleResponeDto = new QleResponeDto();
		List<QleApplicantsDto> qleApplicantDtos = new ArrayList<>();
		qleResponeDto.setApplicants(qleApplicantDtos);
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		QleApplicantsDto qleValidationDto = null;
		QleEventDto qleEventDTO = null;
		boolean isHouseholdValidated = false;
		SsapApplication ssapApplication = null;
		try{
			Set<SsapApplicant> ssapApplicants = ssapApplicantRepository.findApplicantForApplication(caseNumber,getGatedStatus(gated));
			for(SsapApplicant ssapApplicant : ssapApplicants){
				//this block is executed only once in this loop
				if(!isHouseholdValidated){
					isHouseholdValidated = true;
					if(ssapApplicant.getSsapApplication().getCmrHouseoldId().longValue() != moduleId){
						throw new GIException("unauthorized access.");
					}
					//set only once 
					ssapApplication = ssapApplicant.getSsapApplication();
					qleResponeDto.setApplicationId(String.valueOf(ssapApplication.getId()));
					qleResponeDto.setValidationStatus(ssapApplication.getValidationStatus()!= null ? ssapApplication.getValidationStatus().toString():null);
					qleResponeDto.setFinancialAssistanceFlag(ssapApplication.getFinancialAssistanceFlag());
					if(ssapApplication.getEligibilityStatus() !=null){
						qleResponeDto.setEligibilityStatus(ssapApplication.getEligibilityStatus().toString());
					}
				}
				qleValidationDto = new QleApplicantsDto();
				qleValidationDto.setFirstName(ssapApplicant.getFirstName());
				qleValidationDto.setMiddleName(ssapApplicant.getMiddleName());
				qleValidationDto.setLastName(ssapApplicant.getLastName());
				qleValidationDto.setPersonId(ssapApplicant.getPersonId());
				qleValidationDto.setEvents(new ArrayList<>());
				for(SsapApplicantEvent ssapApplicantEvent : ssapApplicant.getSsapApplicantEvents()){
					qleEventDTO = new QleEventDto();
					
					qleEventDTO.setApplicantEventId(String.valueOf(ssapApplicantEvent.getId()));
					qleEventDTO.setEventDate(df.format(ssapApplicantEvent.getEventDate()));
					qleEventDTO.setEventId(String.valueOf(ssapApplicantEvent.getSepEvents().getId()));;
					qleEventDTO.setEventName(ssapApplicantEvent.getSepEvents().getName());
					qleEventDTO.setEventLabel(ssapApplicantEvent.getSepEvents().getLabel());
					qleEventDTO.setEventCategory(ssapApplicantEvent.getSepEvents().getCategory()); 
					qleEventDTO.setEventCategoryLabel(ssapApplicantEvent.getSepEvents().getCategoryLabel());
					qleEventDTO.setChangeType(ssapApplicantEvent.getSepEvents().getChangeType());
					qleEventDTO.setType(ssapApplicantEvent.getSepEvents().getType());
					qleEventDTO.setValidationStatus( ssapApplicantEvent.getValidationStatus() != null ?ssapApplicantEvent.getValidationStatus().toString():null);
					
					List<String> vDList = new ArrayList<String>();
					vDList=platformGson.fromJson(ssapApplicantEvent.getSepEvents().getReqdDocuments(),  new TypeToken<List<String>>(){}.getType() );
					qleEventDTO.setValidDocumentList(vDList);
					List<Map<String,String>> uploadedDocumentList = new ArrayList<Map<String,String>>();
					Map<String,String> docMap;
					List<ConsumerDocument> cDocList = getUploadedDocByTargetNameAndTargetId("SSAP_APPLICANT_EVENTS",ssapApplicantEvent.getId());
					for(ConsumerDocument cDoc:cDocList){
						docMap = new HashMap<String,String>();
						
						docMap.put("documentName",cDoc.getDocumentName());
						docMap.put("documentStatus", "Y".equalsIgnoreCase(cDoc.getAccepted()) ?"ACCEPTED":"N".equalsIgnoreCase(cDoc.getAccepted())?"REJECTED":"SUBMITTED");
						docMap.put("documentType", cDoc.getDocumentType());
						SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",new Locale("EN"));
						docMap.put("documentDate",dateFormat.format(cDoc.getCreatedDate()));
						
						uploadedDocumentList.add(docMap);
					}
					
					qleEventDTO.setUploadedDocumentList(uploadedDocumentList);
					
					qleValidationDto.getEvents().add(qleEventDTO);
				}
				qleApplicantDtos.add(qleValidationDto);
			}
			qleResponeDto.setStatus("200");
		}catch(Exception ex){
			LOGGER.error("Exception occured while fetching events for application:"+caseNumber,ex);
			qleResponeDto.setStatus("500");
		}
		return qleResponeDto;
		
	}
	private List<Gated> getGatedStatus(String gated) {
		List<Gated> gatedStatus = new ArrayList<Gated>();
		if("y".equalsIgnoreCase(gated)){
			gatedStatus.add(Gated.Y);
		}else if("n".equalsIgnoreCase(gated)) {
			gatedStatus.add(Gated.N);
		}else{
			gatedStatus.add(Gated.Y);
			gatedStatus.add(Gated.N);
		}
		return gatedStatus;
	}
	@Override
	public List<ConsumerDocument> getUploadedDocByTargetNameAndTargetId(String targetName,long targetId) {
		List<ConsumerDocument> cDocList = consumerDocumentRepository.findByTargetNameAndTargetId("SSAP_APPLICANT_EVENTS",targetId);
		return cDocList;
	}

	@Override
	public String retrieveDocumentListForEvent(Integer sepEventId) {
		SepEvents sepEvents =  sepEventsRepository.findOne( sepEventId);
		String docList = sepEvents.getReqdDocuments();
		return docList;
	}

	@Override
	public String saveValidationDocument(String relativePath,String fileName, byte[] fileContents, String fileCategory, String fileSubCategory,String fileType) throws ContentManagementServiceException {
		String documentId=null;
		try {
			documentId = ecmService.createContent(relativePath,fileName, fileContents ,fileCategory , fileSubCategory, fileType);
		} catch (ContentManagementServiceException e) {
			LOGGER.error("Exception occured while saving document in saveValidationDocument method for file :"+fileName + " and relative path : "+relativePath,e);
			throw e;
		}
		return documentId;
	}
	
	public void validateRequest(QleRequestDto qleRequestDto) throws AccessDeniedException {
		boolean isValid = false;
		SsapApplicantEvent ssapApplicantEvent = ssapApplicantEventRepository.findOne(qleRequestDto.getApplicantEventId());
		if(ssapApplicantEvent != null && ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId() != null) {
			int cmrHouseholdId = ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication().getCmrHouseoldId().intValue();
			if(cmrHouseholdId == qleRequestDto.getModuleId().intValue() && INDIVIDUAL_MODULE_USER.equalsIgnoreCase(qleRequestDto.getModuleName())) {
				isValid =  true; 
			}
		}
		if(!isValid) {
			throw new AccessDeniedException("Please refresh the page and try again.");
		}
	}

	@Override
	public Long saveDocumentRecord(String ecmDocumentId,String documentCategory,String documentName,String documentType,Long targetId,String targetName,Long createdBy) {
		 
		ConsumerDocument consumerDocument = new ConsumerDocument();
		
		consumerDocument.setEcmDocumentId(ecmDocumentId);
		
		consumerDocument.setDocumentCategory(documentCategory);
		consumerDocument.setDocumentName(documentName);
		consumerDocument.setDocumentType(documentType);
		
		consumerDocument.setTargetId(targetId);
		consumerDocument.setTargetName(targetName);
		
		consumerDocument.setCreatedDate(new TSDate());
		consumerDocument.setCreatedBy(createdBy);
		
		try {
			ConsumerDocument savedConsumerDocument = ssapApplicantDocumentService.saveSsapApplicantDocument(consumerDocument);
			return savedConsumerDocument.getId();
		} catch (SsapDocumentsServiceException e) {
			LOGGER.error("Exception occured while saving document record in saveDocumentRecord method for ECM file :"+consumerDocument.getEcmDocumentId()  ,e);
		}
		return null;
	}

	@Override
	public TkmTickets createTicket(QleRequestDto docSaveRequest,String ecmDocumentId,Long consumerDocumentId) {
		 
		List<String> docLinks = new ArrayList<String>();
		try {
			if(consumerDocumentId!=null){
				
				docLinks.add(ecmDocumentId);
				
				SsapApplicantEvent ssapApplicantEvent = ssapApplicantEventRepository.findOne(docSaveRequest.getApplicantEventId());
				 
			   TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
			   
			   tkmTicketsRequest.setSubject(docSaveRequest.getTicketSubject());
			   
			   tkmTicketsRequest.setDescription(buildDescriptionData(ssapApplicantEvent));
			   tkmTicketsRequest.setComment(docSaveRequest.getTicketComment());
			   tkmTicketsRequest.setDetailedHistoryView(false);
			  
			   tkmTicketsRequest.setCategory(docSaveRequest.getTicketCategory());  //"QLE"
			   tkmTicketsRequest.setType(docSaveRequest.getTicketType());  //"Qle Validation"
			  
			  // tkmTicketsRequest.setRequester(docSaveRequest.getUserId().intValue());
			  //fetch household user id 
			   Integer userId = cmrHouseholdRepository.findUserByCmrId(docSaveRequest.getModuleId());
			   
			   tkmTicketsRequest.setRequester(userId==null?docSaveRequest.getUserId().intValue():userId);
			   tkmTicketsRequest.setUserRoleId(roleService.findRoleByName("INDIVIDUAL").getId());
			   tkmTicketsRequest.setModuleId(docSaveRequest.getModuleId());
			   tkmTicketsRequest.setModuleName("HOUSEHOLD");
			   
			  
			   tkmTicketsRequest.setSubModuleId((int)ssapApplicantEvent.getId());
			   
			   tkmTicketsRequest.setDocumentLinks(docLinks);  
			   tkmTicketsRequest.setCaseNumber(docSaveRequest.getCaseNumber());
			   
			   tkmTicketsRequest.setCreatedBy(docSaveRequest.getUserId().intValue());  
			   tkmTicketsRequest.setLastUpdatedBy(docSaveRequest.getUserId().intValue()); 
			   
			   Map<String,String> ticketFormPropertiesMap=new HashMap<String,String>();
			   
			   ticketFormPropertiesMap.put("docId", consumerDocumentId.toString()); 
			   ticketFormPropertiesMap.put("subModuleId",String.valueOf(ssapApplicantEvent.getId())); 
			   ticketFormPropertiesMap.put("subModuleName", CancelTicketRequest.SubModuleName.APPLICANTEVENT.name());
			   
			   tkmTicketsRequest.setTicketFormPropertiesMap(ticketFormPropertiesMap);
			                        
			   
			   ResponseEntity<String> get_resp =  ghixRestTemplate.exchange(GhixEndPoints.TICKETMGMT_URL + "ticketmgmt/tickets/ticket/create",docSaveRequest.getUserName() , HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,tkmTicketsRequest);
			   XStream xstream = GhixUtils.getXStreamStaxObject();
			   TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp.getBody());
			   
			   if (tkmTicketsResponse.getStatus().equals(GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.error("Error while creating verfication document ticket",tkmTicketsResponse.getErrMsg());
					return null;
				}
				
				updateApplicantEventStatus(ApplicantEventValidationStatus.SUBMITTED, docSaveRequest.getApplicantEventId(), docSaveRequest.getUserId().intValue(), docSaveRequest.getUserName());

				return tkmTicketsResponse.getTkmTickets();
			}
			
		}
		catch(Exception e) {
			 LOGGER.error("Error creating verfication document ticket",e);
			 
		}
		return null;
	}
	private String buildDescriptionData(SsapApplicantEvent ssapApplicantEvent) {
		  //build description
		   SsapApplication ssapApplication = ssapApplicantEvent.getSsapAplicationEvent().getSsapApplication();
		   StringBuilder sb = new StringBuilder();
		   sb.append(APPLICATION_TYPE).append(ssapApplication.getApplicationType());
		   String applicationFinacialOrNF = null;
		   if(Y.equals(ssapApplication.getFinancialAssistanceFlag())){
			   applicationFinacialOrNF  = FINANCIAL_APPLICATION;
		   }else if(N.equals(ssapApplication.getFinancialAssistanceFlag())){
			   applicationFinacialOrNF = NON_FINANCIAL_APPLICATION;
		   }
		   if(applicationFinacialOrNF != null){
			   sb.append(" ( "+ applicationFinacialOrNF+" )");
		   }
		   sb.append(System.lineSeparator());
		   sb.append(CASE_NUMBER+ ssapApplication.getCaseNumber()).append(System.lineSeparator());
		   sb.append(EVENT_NAME+ ssapApplicantEvent.getSepEvents().getLabel()).append(System.lineSeparator());
		   sb.append(SPECIAL_ENROLLMENT_START_DATE+ DateUtil.dateToString(ssapApplicantEvent.getSsapAplicationEvent().getEnrollmentStartDate(),MM_DD_YYYY) ).append(System.lineSeparator());
		   sb.append(SPECIAL_ENROLLMENT_END_DATE+ DateUtil.dateToString(ssapApplicantEvent.getSsapAplicationEvent().getEnrollmentEndDate(),MM_DD_YYYY) ).append(System.lineSeparator());
		return sb.toString();
	}
	
	@Override
	public String adminQleOverride(QleRequestDto qleRequestDto){
		String status = "500";
		try{
			//save comments in db
			saveComments(qleRequestDto);
			//call validation service
			updateApplicantEventStatus(ApplicantEventValidationStatus.OVERRIDDEN,qleRequestDto.getApplicantEventId(),qleRequestDto.getUserId().intValue(),qleRequestDto.getUserName());
			status = "200";
		/*	Optional<Boolean> result = response.getBody();
			if(result.isPresent() && result.get()){
				status = "200";
			}*/
		}catch(Exception ex){
			 LOGGER.error("Unexpected error occured while overriding qle validation for applicant event : "+qleRequestDto.getApplicantEventId(),ex);
		}
		
		return status;
	}
	private ResponseEntity<String> updateApplicantEventStatus(ApplicantEventValidationStatus status,Long applicantEventId,Integer userId,String userName  ) {
		com.getinsured.hix.dto.ssap.SsapApplicantEvent sae = new com.getinsured.hix.dto.ssap.SsapApplicantEvent();
		sae.setId(applicantEventId);//qleRequestDto.getApplicantEventId()
		sae.setLastUpdatedUserId(userId);//qleRequestDto.getUserId().intValue()
		sae.setStatus(status);//qleRequestDto.getUserName()
		ResponseEntity<String> result = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"/ssap/applicantevent/"+applicantEventId, userName, HttpMethod.PUT, MediaType.APPLICATION_JSON, String.class, sae);
		return result;
	}
	private void saveComments(QleRequestDto qleRequestDto) {
		CommentTarget commentTarget = null;
		
		TargetName targetName = null;
		List<Comment> comments = null;
		targetName = TargetName.valueOf("QLE_ADMIN_OVERRIDE");
		Long targetId = qleRequestDto.getApplicantEventId();
		commentTarget = commentTargetService.findByTargetIdAndTargetType(targetId, targetName);
		if(commentTarget == null){
			commentTarget =  new CommentTarget();
			commentTarget.setTargetId(qleRequestDto.getApplicantEventId().longValue());
			commentTarget.setTargetName(TargetName.valueOf("QLE_ADMIN_OVERRIDE"));
		}
		comments = commentTarget.getComments();
		if(comments == null){
			comments = new ArrayList<Comment>();
		}
		Comment comment = new Comment();
		comment.setComentedBy(qleRequestDto.getUserId().intValue());
		comment.setCommenterName(qleRequestDto.getName());
		comment.setComment(qleRequestDto.getOverrideComment());
		comment.setCommentTarget(commentTarget);
		comments.add(comment);
		commentTarget.setComments(comments);
		commentTargetService.saveCommentTarget(commentTarget);
	}
	
	@Override
	public boolean triggerQleValidation(String caseNumber,Integer userId){
		boolean result = true;
		Optional<ApplicationValidationStatus> applicationValidationStatus = qleValidationService.runValidationEngine(caseNumber, Source.EXCHANGE, userId);
		//SsapApplication ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
		//if validation status is pending
		if(applicationValidationStatus.isPresent() && applicationValidationStatus.get() == ApplicationValidationStatus.PENDING){
			//set allow enrollment false
			//ssapApplication.setAllowEnrollment("N");
			result=false;
		}else{
			//ssapApplication.setAllowEnrollment("Y");
			result= true;
		}
		//ssapApplicationRepository.saveAndFlush(ssapApplication);
		
		return result;
		
	}
	
	@Override
	public void processQleForSepDenial(String caseNumber,Integer userId){ 
		qleValidationService.runValidationEngine(caseNumber, Source.EXCHANGE, userId);
		qleValidationService.cancelEventsAndTickets(caseNumber, userId);
	}
	
	
	
	

	
}
