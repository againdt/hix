#!/bin/bash

MAIN_DIR=$(pwd)
set -x
WORK_PATH=$HOME/testrun/$1
TEST_ENV=$2
#TEST_CASES=$3
echo "Build #$1; working directory: ${WORK_PATH}; current directory: ${MAIN_DIR}";

TEST_CASES=(\
CiC_1_aptc_csr_hh2_increase_aptc_max_med_dental \
CiC_2_aptc_csr_hh2_decrease_aptc_to_0_med \
CiC_3_aptc_hh3_ssn_change_med_den \
CiC_4_aptc_hh3_ssn_change_med \
CiC_5_uqhp_hh2_change_qhp_to_false_med_dent \
CiC_6_uqhp_hh2_change_qhp_to_false_med \
CiC_7_update_csr_level_CS2_to_CS3 \
CiC_8_update_csr_level_CS4_to_CS6_Silver_med_dental \
CiC_9_UQHP_GainingAPTC \
CiC_10_DOB_change_Person2_HH_Enrolled_in_different_plans \
CiC_11_APTC_Only_Gaining_CSR_silver_med_dental \
CiC_12_Adding_Plus_RemovingMember \
CiC_13_Adding_Plus_RemovingMember_Medical_DentalEnrollment \
CiC_14_Adding_Plus_APTC_Update \
CiC_15_Add_member_Plus_Update_csr_level_CS4_to_CS6 \
CiC_16_Add_member_Plus_Update_csr_level_CS4_to_CS6 \
CiC_17_APTC_CSR_to_UQHP_Plus_AddMember \
CiC_18_DOB_change_AddMember \
CiC_19_DOB_change_RemoveMember \
CiC_20_UQHP_To_APTC_CSR_RemoveMember \
CiC_21_APTC_CSR_to_UQHP_Plus_RemoveMember \
CiC_22_update_csr_level_CS4_to_CS6_RemoveMember \
CiC_23_APTC_CSR_to_UQHP_DOB_Update \
CiC_26_update_csr_level_CS4_to_CS2 \
CiC_27_Loss_of_Member_Level_CSR \
CiC_28_Gain_of_Member_Level_CSR \
CiC_29_Loss_of_Household_Level_CSR_DOB_Update \
CiC_30_Gain_of_Household_Level_CSR_DOB_Update \
CiC_38_hh2_enroll_all_disenroll_medical_via_eligibility_history \
CiC_39_hh4_enroll_all_disenroll_dental_via_my_eligibility_history \
CiC_41_hh2_2groups_enroll_medical_disenroll_medical_via_eligibility_history \
CiC_43_hh2_2groups_enroll_all_disenroll_medical_via_custom_grouping_page \
CiC_44_hh4_enroll_all_disenroll_dental_via_custom_grouping_page \
CiC_45_hh2_enroll_all_disenroll_all_via_custom_grouping_page \
CiC_46_hh2_2groups_enroll_medical_disenroll_medical_via_custom_grouping_page \
CiC_47_hh2_enroll_dental_disenroll_dental_via_custom_grouping_page \
CiC_48_hh2_enroll_all_disenroll_medical_via_my_enrollments \
CiC_49_hh2_enroll_all_disenroll_dental_via_my_enrollments \
CiC_50_hh2_2groups_enroll_all_disenroll_all_via_my_enrollments \
CiC_51_hh2_2groups_enroll_medical_disenroll_medical_via_my_enrollments \
CiC_52_hh2_enroll_dental_disenroll_dental_via_my_enrollments)

exit_codes=0
function sec {
        exit_codes=$((exit_codes + $1))
}

echo "GIT commands"
cd $MAIN_DIR/iex
#git fetch; sec $?
#git reset --hard origin/master; sec $?
#git clean -fdx; sec $?
#git checkout HIX-119487-disenrollment; sec $?
#git pull; sec $?
#git status

echo "Navigate to WORK_PATH"
cd $MAIN_DIR
cp -r $MAIN_DIR/iex/qa-automation $WORK_PATH
cd $WORK_PATH/cypress-e2e

echo "NPM run"
npm install; sec $?

#npm run runSpec
npm run cleanup; sec $?

#npx concurrently npm:runDataGen npm:runEnrollMedDent npm:runEnrollMed npm:runEnrollDent npm:runUpdateDeAPTC npm:runUpdateInAPTC npm:runUpdateCSR npm:runUpdateNames; sec $?
#npx concurrently npm:runSpec npm:default_enrollment npm:default_enrollment1; sec $?

for tc in ${TEST_CASES[@]}; do
		npx cypress run --spec cypress/integration/regression/${tc}.js --env configFile=$TEST_ENV --reporter mochawesome; sec $?
done

npm run merge_reports; sec $?
npm run generate_mochawesome_report; sec $?

echo "Exit codes: ${exit_codes}"
exit ${exit_codes}
