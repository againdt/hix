package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ObjectFactory;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.PassportCountryType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vilpsav.ReverificationRequestType;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;
import com.getinsured.iex.hub.vlp37.wrapper.util.ReVerificationRequestTypeWrapperUtil;


public class ReVerificationRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	
	private ReverificationRequestType request = null;
	
    private String caseNumber;
    private String receiptNumber;
    private String alienNumber;
    private String i94Number;
    private String sevisid;
    private ReVerifyPassportCountryWrapper passportCountry;
    private String naturalizationNumber;
    private String citizenshipNumber;
    private String visaNumber;
    private String lastName;
    private String firstName;
    private String middleName;
    private String dateOfBirth;
    private boolean fiveYearBarApplicabilityIndicator;
    private String requesterCommentsForHub;
    
	
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String CASE_NUMBER = "CaseNumber";                          
	private static final String RECEIPT_NUMBER =  "ReceiptNumber";                       
	private static final String ALIEN_NUMBER = "AlienNumber";                         
	private static final String I94_NUMBER = "I94Number";                           
	private static final String SEVIS_ID = "SEVISID";                             
	private static final String PASSPORT_COUNTRY = "PassportCountry";     
	private static final String NATURALIZATION_NUMBER = "NaturalizationNumber";                
	private static final String CITIZENSHIP_NUMBER = "CitizenshipNumber";                   
	private static final String VISA_NUMBER = "VisaNumber";                          


	
	
	public ReVerificationRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createReverificationRequestType();
	}
	
	public void setRequest(ReverificationRequestType request) {
		this.request = request;
	}
	
	/**
	 * This method will also do the request specific validations
	 */
	public ReverificationRequestType getRequest() throws HubServiceException {
		
		if(this.request.getFirstName() == null){
			throw new HubServiceException("First Name is required for ReVerificationRequest");
		}
		
		if(this.request.getLastName() == null){
			throw new HubServiceException("Last Name is required for ReVerificationRequest");
		}
		
		if(this.request.getDateOfBirth() == null){
			throw new HubServiceException("Date of Birth is required for ReVerificationRequest");
		}
		
		return this.request;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.request.setFirstName(firstName);
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.request.setMiddleName(middleName);
		this.middleName = middleName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.request.setLastName(lastName);
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) throws HubServiceException {
		
		try{
			this.request.setDateOfBirth(VLPServiceUtil.stringToXMLDate(dateOfBirth, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		this.dateOfBirth = dateOfBirth;
	}

	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}

	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
		this.request.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.requesterCommentsForHub = requesterCommentsForHub;
		this.request.setRequesterCommentsForHub(requesterCommentsForHub);
	}


	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
		this.request.setCaseNumber(caseNumber);
	}

	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) {
		this.receiptNumber = receiptNumber;
		this.request.setReceiptNumber(receiptNumber);
	}

	public String getAlienNumber() {
		return alienNumber;
	}

	public void setAlienNumber(String alienNumber) {
		this.alienNumber = alienNumber;
		this.request.setAlienNumber(alienNumber);
	}

	public String getI94Number() {
		return i94Number;
	}

	public void setI94Number(String i94Number) {
		this.i94Number = i94Number;
		this.request.setI94Number(i94Number);
	}

	public String getSevisid() {
		return sevisid;
	}

	public void setSevisid(String sevisid) {
		this.sevisid = sevisid;
		this.request.setSEVISID(sevisid);
	}

	public ReVerifyPassportCountryWrapper getPassportCountry() {
		return passportCountry;
	}

	public void setPassportCountry(ReVerifyPassportCountryWrapper passportCountry) {
		this.passportCountry = passportCountry;
		PassportCountryType value = this.passportCountry.getPassportCountryType();
		this.request.setPassportCountry(value);
	}

	public String getNaturalizationNumber() {
		return naturalizationNumber;
	}

	public void setNaturalizationNumber(String naturalizationNumber) {
		this.naturalizationNumber = naturalizationNumber;
		this.request.setNaturalizationNumber(naturalizationNumber);
	}

	public String getCitizenshipNumber() {
		return citizenshipNumber;
	}

	public void setCitizenshipNumber(String citizenshipNumber) {
		this.citizenshipNumber = citizenshipNumber;
		this.request.setCitizenshipNumber(citizenshipNumber);
	}

	public String getVisaNumber() {
		return visaNumber;
	}

	public void setVisaNumber(String visaNumber) {
		this.visaNumber = visaNumber;
		this.request.setVisaNumber(visaNumber);
	}

	/**
	 * Converts the JSON String to ReVerificationRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static ReVerificationRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
		
		ReVerificationRequestTypeWrapper wrapper = new ReVerificationRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASE_NUMBER);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
	
		param = obj.get(CITIZENSHIP_NUMBER);
		if(param != null){
			wrapper.setCitizenshipNumber((String)param);
		}
		
		param = obj.get(RECEIPT_NUMBER);
		if(param != null){
			wrapper.setReceiptNumber((String)param);
		}

		param = obj.get(ALIEN_NUMBER);
		if(param != null){
			wrapper.setAlienNumber((String)param);
		}

		param = obj.get(I94_NUMBER);
		if(param != null){
			wrapper.setI94Number((String)param);
		}
		
		param = obj.get(SEVIS_ID);
		if(param != null){
			wrapper.setSevisid((String)param);
		}

		param = obj.get(PASSPORT_COUNTRY);
		if(param != null){
			JSONObject passportCountry = (JSONObject)param;
			wrapper.setPassportCountry(ReVerifyPassportCountryWrapper.fromJSONObject(passportCountry));
		}

		param = obj.get(NATURALIZATION_NUMBER);
		if(param != null){
			wrapper.setNaturalizationNumber((String)param);
		}
	
		param = obj.get(VISA_NUMBER);
		if(param != null){
			wrapper.setVisaNumber((String)param);
		}
			
		ReVerificationRequestTypeWrapperUtil.addNameToWrapper(obj, wrapper);
		
		ReVerificationRequestTypeWrapperUtil.addIndicatorsToWrapper(obj, wrapper);
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASE_NUMBER, this.caseNumber);	         
		obj.put(RECEIPT_NUMBER, this.receiptNumber);		          
		obj.put(ALIEN_NUMBER, this.alienNumber);		         
		obj.put(I94_NUMBER, this.i94Number);		         
		obj.put(SEVIS_ID, this.sevisid);
		
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY, this.passportCountry.toJSONObject());
		}
		
		obj.put(NATURALIZATION_NUMBER, this.naturalizationNumber);	         
		obj.put(CITIZENSHIP_NUMBER, this.citizenshipNumber);	         
		obj.put(VISA_NUMBER, this.visaNumber);	
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		return obj.toJSONString();

	}
	
	public ReVerificationRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		ReVerificationRequestTypeWrapper wrapper = new ReVerificationRequestTypeWrapper();
		
		wrapper.setCaseNumber((String)jsonObj.get(CASE_NUMBER));
		wrapper.setCitizenshipNumber((String)jsonObj.get(CITIZENSHIP_NUMBER));
		wrapper.setReceiptNumber((String)jsonObj.get(RECEIPT_NUMBER));
		wrapper.setAlienNumber((String)jsonObj.get(ALIEN_NUMBER));
		wrapper.setI94Number((String)jsonObj.get(I94_NUMBER));
		wrapper.setSevisid((String)jsonObj.get(SEVIS_ID));
		wrapper.setPassportCountry(ReVerifyPassportCountryWrapper.fromJSONObject((JSONObject) jsonObj.get(PASSPORT_COUNTRY)));
		wrapper.setNaturalizationNumber((String)jsonObj.get(NATURALIZATION_NUMBER));
		wrapper.setVisaNumber((String)jsonObj.get(VISA_NUMBER));		
		wrapper.setFirstName((String)jsonObj.get(FIRSTNAME_KEY));
		wrapper.setMiddleName((String)jsonObj.get(MIDDLENAME_KEY));
		wrapper.setLastName((String)jsonObj.get(LASTNAME_KEY));
		wrapper.setDateOfBirth((String)jsonObj.get(DATEOFBIRTH_KEY));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(CASE_NUMBER, this.caseNumber);
		obj.put(CITIZENSHIP_NUMBER, this.citizenshipNumber);
		obj.put(RECEIPT_NUMBER, this.receiptNumber);
		obj.put(ALIEN_NUMBER, this.alienNumber);
		obj.put(I94_NUMBER, this.i94Number);
		obj.put(SEVIS_ID, this.sevisid);
		obj.put(PASSPORT_COUNTRY, this.passportCountry.toJSONObject());
		obj.put(NATURALIZATION_NUMBER, this.naturalizationNumber);
		obj.put(VISA_NUMBER, this.visaNumber);
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		return obj;
	}
}
