package com.getinsured.hix.admin.ticketmgmt.web;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.cap.util.CapPortalConfigurationUtil;
import com.getinsured.hix.cap.util.CapRequestValidatorUtil;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.dto.tkm.ReportDto.CATEGORIES;
import com.getinsured.hix.filter.xss.XssHelper;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIErrorCode;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmQuickActionDto;
import com.getinsured.hix.model.TkmTicketDto;
import com.getinsured.hix.model.TkmTicketTaskResponse;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTickets.TicketPriority;
import com.getinsured.hix.model.TkmTickets.ticket_status;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.TkmTicketsTaskRequest;
import com.getinsured.hix.model.TkmUserDTO;
import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.security.CustomPermissionEvaluator;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.RestfulResponseException;
import com.getinsured.hix.util.TicketMgmtUtils;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import au.com.bytecode.opencsv.CSVWriter;

/**
 *
 * @author sharma_k
 *
 */
@Controller
@DependsOn("dynamicPropertiesUtil")
public class TicketListController {
    private static final String UPLOAD_FILE_TYPE_ERROR = "Your file could not be uploaded. Use one of these file types: BMP, GIF, JPG, JPEG, PNG, or PDF";

    private  final Logger LOGGER = LoggerFactory
            .getLogger(TicketListController.class);

    private static final String TICKET_CANNOT_BE_EDITED = "Some thing went wrong.Ticket cannot be edited";
    @Autowired
    private UserService userService;
    @Autowired
    private TicketMgmtUtils ticketMgmtUtils;
    @Autowired
    private RoleService roleService;
    @Autowired
    private ContentManagementService ecmService;
    @Autowired
    private IssuerService issuerService;
    @Autowired
    private GIMonitorService giMonitorService;
    @Autowired
    private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
    @Autowired
    private CapRequestValidatorUtil capRequestValidatorUtil;
    @Autowired
    private Gson platformGson;
    @Autowired private CapPortalConfigurationUtil capPortalConfigurationUtil;
    
    @Autowired
    private CustomPermissionEvaluator permissionEvaluator;
    
    private static final Gson gson = new Gson();

    private static final String REDIRECT_ACCOUNT_USER_LOGINFAILED = "redirect:/account/user/loginfailed";
    private static final int LISTING_NUMBER = 10;
    private static final String SORT_ORDER = "sortOrder";
    private static final String DESC = "DESC";
    private static final String TICKET_SUBJECT = "ticketSubject";
    private static final String TICKET_TYPE = "ticketType";
    private static final String TICKET_QUEUE = "ticketQueues";
    private static final String ARCHIVED_FLAG_VALUE = "archiveFlagValue";
    private static final String APP_JSON_CONTENT = "application/json";
    private static final String ATTR_PAGE_TITLE = "page_title";
    private static final String TICKET_NUMBER = "ticketNumber";
    private static final String TICKET_COMMENTS = "ticketComments";
    private static final String TICKET_LIST_MAPPING = "/ticketmgmt/ticket/ticketlist";
    private static final String UNCHECKED = "unchecked";
    private static final String FALSE = "false";
    private static final String TRUE = "true";
    private static final String ACTIVITI_TASK_ID = "ActivitiTaskId";
    private static final String ASSIGNEE = "assignee";
    private static final String ERROR_MSG = "errorMsg";
    private static final String FLOW = "flow";
    private static final String IS_DETAILED_VIEW = "isDetailedView";
    private static final String REDIRECT = "redirect:";
    private static final String TASK_ID = "TaskId";
    private static final String TICKET_STATUS = "TICKET_STATUS";
    private static final String TICKET_HISTORY_LIST = "ticketHistoryList";
    private static final String TICKET_ID = "ticketId";
    private static final String CREATE_TICKET_MAPPING = "ticketmgmt/ticket/createpage";
    private static final String UPLOAD_FAILURE = "Upload_Failure";
    private final Map<String, Map<String, List<String>>> mapTicketWorkflowByPermission = new HashMap<>();
    private static final int BYTE = 1024;
    private static final String TKM_VIEW_TICKET = "hasPermission(#model, 'TKM_VIEW_TICKET')";
    private static final String TKM_SAVE_TICKET = "hasPermission(#model, 'TKM_SAVE_TICKET')";
    private static final String TKM_LIST_TICKET = "hasPermission(#model, 'TKM_LIST_TICKET')";
    private static final String TKM_EDIT_TICKET = "hasPermission(#model, 'TKM_EDIT_TICKET')";
    private static final String TKM_REPORT = "hasPermission(#model, 'TKM_REPORT')";
    private static final String TKM_AGEING_REPORT = "hasPermission(#model, 'TKM_AGEING_REPORT')";
    private static final String UPLOAD_MSG = "Upload_Msg";
    private static final String TKM_WORKFLOW_PERMISSION_SUBSTRING = "TKM_WORKFLOW";

    private static final String FILE_EXTENTION = ".csv";
    private static final String AGEING_REPORT = "AGEING_REPORT";

    public static final String REPORT_CRITERIA_TYPE = "PRIORITY";
    private static final String NO_OF_TICKETS = "No. of Tickets";

    public static final String UNSECURE_CHARS_REGEX = "[\n\r]";
    public static final String CONTENT_TYPE = "application/json";

    @Value("#{configProp.outputpath}")
    private String outputPath;

    private static final String TICKET_STATS_MAPPING = "ticketmgmt/ticket/showpiereport";
    private static final String CRITERIA = "criteria";
    private static final String TYPE = "type";
    private static final String PRIORITY = "priority";
    private static final String SUBMITTED_BEFORE = "submittedBefore";
    private static final String SUBMITTED_AFTER = "submittedAfter";
    private static final String TICKET_STATUS_LIST = "ticketStatus";
    private static final String ASSIGNED_TO_ME = "assignedToMe";
    private static final String UNCLAIMED_TICKET = "unClaimedTicket";

    private static final String STATE_EXCHANGE = DynamicPropertiesUtil
            .getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
    private static final Object INVALID_COMMENT_DATA = "Cannot Process, invalid comment data. Please try again.";

    @GiAudit(transactionName = "DisplayTickets", eventType = EventTypeEnum.TICKET_MANAGEMENT, eventName = EventNameEnum.PII_READ)
    @RequestMapping(value = TICKET_LIST_MAPPING)
    @PreAuthorize(TKM_LIST_TICKET)
    public String ticketList(Model model, HttpServletRequest request)
            throws InvalidUserException {

        capRequestValidatorUtil.validateTicketList(request);

        AccountUser user = userService.getLoggedInUser();

        LOGGER.info("TicketList Page ");

        Set<String> userPermissions = getUserPermissions(request, user);
        boolean isSimplifiedView = isSimplifiedView(userPermissions);
        Map<String, Object> searchCriteria = this.createSearchCriteria(request,
                user, userPermissions, isSimplifiedView);
        model.addAttribute("searchCriteria", searchCriteria);

        List<Role> roleList = null;
        roleList = this.getRolesForTickets();

        model.addAttribute("addTicketPermission",
                userPermissions.contains("TKM_SAVE_TICKET"));
        model.addAttribute("isArchivedAllowed",
                userPermissions.contains("TKM_ARCHIVE_ALLOWED"));
        model.addAttribute("isAssignAllowed",
                userPermissions.contains("TKM_ASSIGN_ALLOWED"));
        model.addAttribute("stateExchenge", STATE_EXCHANGE);
        model.addAttribute(ATTR_PAGE_TITLE,
                "Getinsured Health Exchange: List Of Tickets");
        model.addAttribute("roleList", roleList);
        model.addAttribute("pageSize", LISTING_NUMBER);
        model.addAttribute("submit", "submit");
        model.addAttribute("isSimplifiedView", isSimplifiedView);

        List<String> ticketType = null;
        List<TkmQueues> assignQueue = new ArrayList<TkmQueues>();
        try {
            ticketType = ticketMgmtUtils.getTicketType();
            assignQueue = (isSimplifiedView) ? ticketMgmtUtils
                    .getQueuesList(user.getId()) : ticketMgmtUtils
                    .getQueuesList();
        }
        catch(RestfulResponseException ex){
            // If exception caught then intialize this list.
            ticketType = new ArrayList<String>();
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket list", ex.getMessage());
                    break;
            }
        }

        model.addAttribute(TICKET_TYPE, ticketType);
        model.addAttribute(TICKET_QUEUE, assignQueue);

        return "ticketmgmt/ticket/ticketlist";
    }

    @SuppressWarnings(UNCHECKED)
    @GiAudit(transactionName = "DisplayTickets", eventType = EventTypeEnum.TICKET_MANAGEMENT, eventName = EventNameEnum.PII_READ)
    @RequestMapping(value = "/ticketmgmt/ticket/list")
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String showTicketList(Model model, HttpServletRequest request)
            throws InvalidUserException {

        capRequestValidatorUtil.validateTicketList(request);

        AccountUser user = userService.getLoggedInUser();

        LOGGER.info("TicketList Page ");

        Set<String> userPermissions = getUserPermissions(request, user);
        boolean isSimplifiedView = isSimplifiedView(userPermissions);
        List<Object> ticketList = null;

        try {
            Map<String, Object> searchCriteria = this.createSearchCriteria(request,
                    user, userPermissions, isSimplifiedView);
            Map<String, Object> receiveableInfoListAndRecordCount = ticketMgmtUtils
                    .searchTickets(searchCriteria);
            // TODO catch exception if tkm is not deployed...


            // List<String> roleNameList = null;
            Long iResultCt = 0l;

            ticketList = (List<Object>) receiveableInfoListAndRecordCount
                    .get("ticketList");
            List<TkmTicketDto> tkmTicketList =  new ArrayList<TkmTicketDto>();
            if(null!=ticketList && ticketList.size()>0){
                tkmTicketList = mapObjectToDto(ticketList);
            }

            Number cnt = (Number) receiveableInfoListAndRecordCount
                    .get("recordCount");
            if (null != cnt) {
                iResultCt = cnt.longValue();
            }

            if (StringUtils.isNotEmpty(request.getParameter("period"))) {
                if (StringUtils
                        .isNotEmpty((String) receiveableInfoListAndRecordCount
                                .get(SUBMITTED_AFTER))) {
                    searchCriteria.put(SUBMITTED_AFTER,
                            receiveableInfoListAndRecordCount
                                    .get(SUBMITTED_AFTER));
                }
                if (StringUtils
                        .isNotEmpty((String) receiveableInfoListAndRecordCount
                                .get(SUBMITTED_BEFORE))) {
                    searchCriteria.put(SUBMITTED_BEFORE,
                            receiveableInfoListAndRecordCount
                                    .get(SUBMITTED_BEFORE));
                }
            }

            String[] ticketStatus = (String[]) searchCriteria
                    .get(TICKET_STATUS_LIST);

            ObjectMapper mapper = new ObjectMapper();

            searchCriteria.put(TICKET_STATUS_LIST,
                    mapper.writeValueAsString(ticketStatus));

            String ticketListJson = mapper.writeValueAsString(tkmTicketList);
            String searchCrteriaJson = mapper
                    .writeValueAsString(searchCriteria);

            Map<String, Object> ticketSearchResult = new HashMap<String, Object>();

            ticketSearchResult.put("ticketListJson", ticketListJson);
            ticketSearchResult.put("searchCriteriaJson", searchCrteriaJson);
            ticketSearchResult.put("count", iResultCt);

            return mapper.writeValueAsString(ticketSearchResult);

        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket list", ex.getMessage());
                    break;
            }
            throw ex;
        }
        catch (JsonProcessingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        if (ticketList == null) {
            ticketList = new ArrayList<Object>();
        }

        int pageNumber = StringUtils
                .isEmpty(request.getParameter("pageNumber")) ? 1 : Integer
                .parseInt(request.getParameter("pageNumber"));

        request.getSession().setAttribute("gotoPageNumber", pageNumber);

        return "ticketmgmt/ticket/ticketlist";
    }

    private List<TkmTicketDto> mapObjectToDto(List<Object> ticketList) {
        List<TkmTicketDto> list = new ArrayList<TkmTicketDto>();
        for (Object obj : ticketList) {
            Object[] rowArray = (Object[]) obj;
            TkmTicketDto tkmTicketDto = new TkmTicketDto();
            String ticketId = ((BigDecimal) rowArray[0]).toString();
            tkmTicketDto.setId(ticketId);
            tkmTicketDto.setEncryptedId(ghixJasyptEncrytorUtil
                    .encryptStringByJasypt(ticketId));

            tkmTicketDto.setSubject((String) rowArray[1]);
            tkmTicketDto.setNumber((String) rowArray[2]);
            tkmTicketDto.setUser((String) rowArray[3]);
            if (null != rowArray[4]) {
                String creationTime = new SimpleDateFormat("MM-dd-yyyy")
                        .format(rowArray[4]);
                tkmTicketDto.setCreationTime(creationTime);
            }
            tkmTicketDto.setQueueName((String) rowArray[5]);
            tkmTicketDto.setPriority((String) rowArray[6]);
            tkmTicketDto.setStatus((String) rowArray[7]);
            tkmTicketDto.setTaskStatus((String) rowArray[8]);

            if (null != rowArray[9]) {
                String dueDate = new SimpleDateFormat("MM-dd-yyyy hh:mm aa")
                        .format(rowArray[9]);
                tkmTicketDto.setDueDate(dueDate);
            }

            tkmTicketDto.setArchiveFlag((String) rowArray[10]);
            tkmTicketDto.setRequestorFullName((String) rowArray[11]);

            if (null != rowArray[12]) {
                tkmTicketDto.setPriorityOrder(((BigDecimal) rowArray[12])
                        .toString());
            }

            tkmTicketDto.setRequestorFirstName((String) rowArray[13]);

            if (null != rowArray[14]) {
                tkmTicketDto.setCreatedBy(((BigDecimal) rowArray[12])
                        .toString());
            }
            
            if (null != rowArray[15]) {
                tkmTicketDto.setTicketType((String) rowArray[15]);
            }

            if (null != rowArray[16]) {
                tkmTicketDto.setTicketCategory((String) rowArray[16]);
            }

            list.add(tkmTicketDto);
        }
        return list;
    }

    private Map<String, Object> createSearchCriteria(
            HttpServletRequest request, AccountUser user,
            Set<String> userPermissions, boolean isSimplifiedView) {
        HashMap<String, Object> searchCriteria = new HashMap<String, Object>();

        String sortBy = request.getParameter("sortBy");
        sortBy = XssHelper.stripXSS(sortBy);
        sortBy = StringUtils.isEmpty(sortBy) ? "created" : sortBy;

        searchCriteria.put("sortBy", sortBy);

        request.removeAttribute("changeOrder");

        String sortOrder = getSortOrder(request);

        searchCriteria.put(SORT_ORDER, sortOrder);

        // filter by subject
        filterBySubject(searchCriteria, request);
        // filter by requested by
        filterByRequestedBy(searchCriteria, request);
        // filter by Queue
        filterByQueue(searchCriteria, request);
        // filter by priority
        filterByPriority(searchCriteria, request);
        // filter by ticker number
        filterByTicketNumber(searchCriteria, request);
        // filter by ticket status
        if (!isSimplifiedView) {
            filterByTicketStatus(searchCriteria, request);
        } else {
            filterByTicketStatusForSimplifiedView(searchCriteria, request);
        }

        // filter by Submitted Before
        filterBySubmittedBeforeDate(searchCriteria, request);

        // filter by Submitted After
        filterBySubmittedAfterDate(searchCriteria, request);

        // filter by period
        filterByPeriod(searchCriteria, request);

        // filter by Task Status
        filterByTaskStatus(searchCriteria, request);

        // filter by Ticket Type
        filterByTicketType(searchCriteria, request);

        // filter by Assignee
        filterByAssignee(searchCriteria, request);

        // filter by CreatedFor
        filterByCreatedFor(searchCriteria, request);

        // filter by archiveType
        filterByArchiveFlag(searchCriteria, request);

        // filter by ticker comments
        filterByTicketComments(searchCriteria, request);

        // HIX-85783 - Get the role for the current user
        // Scenario: L2 can change the role to Admin
        String currentRole = String.valueOf(request.getSession().getAttribute("userActiveRoleName"));
        if(StringUtils.isEmpty(currentRole)) {
            Role userRole = userService.getDefaultRole(user);
            currentRole = (userRole != null) ? userRole.getName() : currentRole;
        }

        boolean isNonSuperUser = false;
        searchCriteria.put("isNonSuperUser", isNonSuperUser);
		/*if (("L2_CSR").equalsIgnoreCase(currentRole)) {
			isNonSuperUser = true;
			searchCriteria.put("isNonSuperUser", isNonSuperUser);
		}*/

        String strPageNum = request.getParameter("pageNumber");
        int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer
                .parseInt(strPageNum) - 1);
        int startRecord = iPageNum * LISTING_NUMBER;

        searchCriteria.put("startRecord", startRecord);
        searchCriteria.put("pageSize", LISTING_NUMBER);
        if (!isExtendedViewAllowed(userPermissions) || isNonSuperUser) {
            searchCriteria.put("userId", user.getId());
            try{
                List<Integer> userQueueId = ticketMgmtUtils.getQueuesForUser(String
                        .valueOf(user.getId()));
                if (userQueueId != null && userQueueId.size() != 0) {
                    searchCriteria.put("userQueueId", userQueueId);
                }
            }
            catch(RestfulResponseException ex){
                switch(ex.getHttpStatus()){
                    case INTERNAL_SERVER_ERROR:
                        LOGGER.error("Internal server error in retrieving queues for user id="+user.getId(),ex.getMessage());
                        break;
                    default:
                        LOGGER.error("Unknown error in retrieving queues for user id="+user.getId(), ex.getMessage());
                        break;
                }
            }

        }

        return searchCriteria;
    }

    @PreAuthorize(TKM_REPORT)
    @RequestMapping(value = "/ticketmgmt/ticket/showpiereport")
    public String showTicketStats(Model model, HttpServletRequest request)
            throws InvalidUserException, IOException {
        LOGGER.info("Displaying Ticket Pie Report Page ");
        // Map<String,Integer> ticketCountMap = new HashMap<String,Integer>();
        StringBuilder strResp = new StringBuilder("[");

        String criteria = request.getParameter(CRITERIA) != null ? TYPE
                : PRIORITY;

        try {
            Map<String, Integer> ticketCountMap = ticketMgmtUtils
                    .getTicketCountForPieReport(criteria);

            for (Entry<String, Integer> entry : ticketCountMap.entrySet()) {

                strResp.append("['").append(entry.getKey()).append("',")
                        .append(entry.getValue()).append("],");
            }
            strResp.append("]");

            String pieDataJson = platformGson.toJson(ticketCountMap);
            model.addAttribute("pieDataJson", pieDataJson);
            model.addAttribute("pieDataString", strResp);

            if (TYPE.equals(criteria)) {

                model.addAttribute("countBy", TYPE);
            } else {

                model.addAttribute("countBy", PRIORITY);
            }

        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving Ticket Count By Priority.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving Ticket Count By Priority.",ex.getMessage());
                    break;
            }
        }
        return TICKET_STATS_MAPPING;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/ageingreport", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_AGEING_REPORT)
    public String getTicketAgeingReport(Model model, HttpServletRequest request)
            throws InvalidUserException {

        HashMap<String, Object> reportCriteria = new HashMap<String, Object>();
        String strResp = "";
        String category = "";


        reportCriteria.put(TYPE,
                request.getParameter(TYPE) != null ? request.getParameter(TYPE)
                        : PRIORITY);
        try {
            List<ReportDto> reportDtoList = ticketMgmtUtils
                    .getTicketsAgeingReport(reportCriteria);

            List<CATEGORIES> categoryEnumList = Arrays.asList(ReportDto.CATEGORIES
                    .values());
            List<String> categoryList = new ArrayList<String>();

            for (CATEGORIES catObject : categoryEnumList) {
                categoryList.add(catObject.getCategories());
            }

            strResp = platformGson.toJson(reportDtoList);
            category = platformGson.toJson(categoryList);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving Ticket aging report.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving Ticket aging report.",ex.getMessage());
                    break;
            }
        }
        catch (Exception e) {
            LOGGER.error("Unable to parse to JSON string");
        }
        model.addAttribute("reportDtoListJSon", strResp);
        model.addAttribute("categories", category);
        model.addAttribute(TYPE, REPORT_CRITERIA_TYPE);
        model.addAttribute(ATTR_PAGE_TITLE,
                "Getinsured Health Exchange: Reports Of Tickets");

        return "ticketmgmt/ticket/ageingreport";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/downloadAgeingReport", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_AGEING_REPORT)
    public void downloadAgeingReport(Model model, HttpServletRequest request,
                                     HttpServletResponse response) {
        LOGGER.info("======================== download ageingReport() =======================");
        try {
            String graphData = request.getParameter("graphData");
            String categoryList = request.getParameter("categoryList");
            String type = request.getParameter(TYPE);
            List<String[]> csvRowDataList = new ArrayList<String[]>();
            // for graph data
            List<Object> listGraphData = new ArrayList<Object>();
            String jsonGraphData = StringEscapeUtils.unescapeHtml(graphData);
            listGraphData = gson.fromJson(jsonGraphData,
                    listGraphData.getClass());

            // category list
            String jsonCatogiryhData = StringEscapeUtils
                    .unescapeHtml(categoryList);
            List<String> listCatogiryhData = new ArrayList<String>();
            listCatogiryhData = gson.fromJson(jsonCatogiryhData,
                    listCatogiryhData.getClass());
            listCatogiryhData.add(0, type);

            String[] headers = listCatogiryhData
                    .toArray(new String[listCatogiryhData.size()]);
            csvRowDataList.add(headers);

            for (Object graphElement : listGraphData) {
                Map<String, Object> graphElemMap = new HashMap<String, Object>();
                graphElemMap = gson.fromJson(graphElement.toString(),
                        graphElemMap.getClass());
                List<String> csvRowData = new ArrayList<String>();

                // fetch name
                csvRowData.add((String) graphElemMap.get("name"));

                // fetch data list json
                ArrayList<Double> ticketCntList = new ArrayList<Double>();
                ticketCntList = gson.fromJson(graphElemMap.get("data")
                        .toString(), ticketCntList.getClass());

                for (int i = 1; i < ticketCntList.size() + 1; i++) {
                    csvRowData.add(ticketCntList.get(i - 1).toString());
                }

                // form a row in final report
                String[] string = csvRowData.toArray(new String[csvRowData
                        .size()]);
                csvRowDataList.add(string);
            }
            String fileName = AGEING_REPORT + TimeShifterUtil.currentTimeMillis()
                    + FILE_EXTENTION;
            String filePath = outputPath + File.separator + AGEING_REPORT;
            generateCsvReport(fileName, filePath, csvRowDataList, response);
        } catch (IOException e) {
            LOGGER.error("IOException occurred while downloading data");
            e.getMessage();
        } catch (Exception e) {
            LOGGER.error("Error occurred while downloading data");
            e.getMessage();

        }

    }

    @RequestMapping(value = "/ticketmgmt/ticket/downloadTicketReport", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_REPORT)
    public void downloadTicketReport(Model model, HttpServletRequest request,
                                     HttpServletResponse response) {
        LOGGER.info("======================== download TicketReport() =======================");
        try {
            String ticketData = request.getParameter("ticketData");
            String type = request.getParameter(TYPE);
            ticketData = StringEscapeUtils.unescapeHtml(ticketData);
            @SuppressWarnings("rawtypes")
            Map map = new HashMap();
            map = gson.fromJson(StringEscapeUtils.unescapeHtml(ticketData),
                    map.getClass());
            List<String[]> data = new ArrayList<String[]>();
            data.add(new String[] { type, NO_OF_TICKETS });
            @SuppressWarnings({ "rawtypes", "unchecked" })
            Set<Entry> set = map.entrySet();
            for (@SuppressWarnings("rawtypes")
                    Entry entry : set) {
                data.add(new String[] { entry.getKey().toString(),
                        entry.getValue().toString() });
            }
            String fileName = request.getParameter("reportName")
                    + TimeShifterUtil.currentTimeMillis() + FILE_EXTENTION;
            String filePath = outputPath + File.separator + AGEING_REPORT;
            generateCsvReport(fileName, filePath, data, response);

        } catch (IOException e) {
            LOGGER.error("IOException occurred while downloading data");
            e.getMessage();
        } catch (Exception e) {
            LOGGER.error("Error occurred while downloading data");
            e.getMessage();

        }
    }

    private void generateCsvReport(String fileName, String filePath,
                                   List<String[]> dataList, HttpServletResponse response)
            throws IOException {

        if (!GhixUtils.isGhixValidPath(filePath)
                || !GhixUtils.isGhixValidPath(outputPath)) {
            throw new RestfulResponseException(
                    "Application trying to reach a blacklisted folder or filename or extension type.");
        }
        File outputPathFileDir = new File(outputPath);
        File fileDir = new File(filePath);
        if (!outputPathFileDir.exists() || !fileDir.exists()) {
            fileDir.mkdirs();
        }
        FileWriter fw = null;

        try {
            fw = new FileWriter(filePath + File.separator + fileName);
            CSVWriter writer = new CSVWriter(fw, ',');

            writer.writeAll(dataList);
            writer.close();

            fileName = fileName.replaceAll(UNSECURE_CHARS_REGEX,
                    StringUtils.EMPTY);
            fileName = XssHelper.stripXSS(fileName);

            response.setHeader("Content-Disposition", "attachment;filename="
                    + fileName);
            response.setContentType("application/octet-stream");
            File file = new File(filePath + File.separator + fileName);
            FileCopyUtils.copy(new FileInputStream(file),
                    response.getOutputStream());
            LOGGER.info("File downloaded successfully!!!");
        } finally {
            IOUtils.closeQuietly(fw);
        }

    }

    private void filterByAssignee(Map<String, Object> searchCriteria,
                                  HttpServletRequest request) {
        String selectedAssignee = request.getParameter("assigneeId");
        String selectedAssigneeName = request.getParameter(ASSIGNEE);
        selectedAssignee = XssHelper.stripXSS(selectedAssignee);
        selectedAssigneeName = XssHelper.stripXSS(selectedAssigneeName);
        if (!StringUtils.isEmpty(selectedAssignee)
                && !StringUtils.isEmpty(selectedAssigneeName)) {
            searchCriteria.put("assigneeId", selectedAssignee);
        }
        searchCriteria.put(ASSIGNEE, selectedAssigneeName);

    }

    private void filterByTicketType(Map<String, Object> searchCriteria,
                                    HttpServletRequest request) {
        String selectedTicketType = request.getParameter(TICKET_TYPE);
        if (!StringUtils.isEmpty(selectedTicketType)) {
            searchCriteria.put(TICKET_TYPE,
                    XssHelper.stripXSS(selectedTicketType));
        }
    }

    private void filterByTaskStatus(Map<String, Object> searchCriteria,
                                    HttpServletRequest request) {
        String selectedTaskStatus = request.getParameter("taskStatus");
        if (!StringUtils.isEmpty(selectedTaskStatus)) {
            searchCriteria.put("taskStatus",
                    XssHelper.stripXSS(selectedTaskStatus));
        }
    }

    private void filterByPeriod(Map<String, Object> searchCriteria,
                                HttpServletRequest request) {
        String period = request.getParameter("period");
        if (!StringUtils.isEmpty(period)) {
            searchCriteria.put("period", period);
        }
    }

    private void filterBySubmittedBeforeDate(
            Map<String, Object> searchCriteria, HttpServletRequest request) {
        String submittedBefore = request.getParameter(SUBMITTED_BEFORE);
        if (!StringUtils.isEmpty(submittedBefore)) {
            searchCriteria.put(SUBMITTED_BEFORE,
                    XssHelper.stripXSS(submittedBefore));
        }

    }

    private void filterBySubmittedAfterDate(Map<String, Object> searchCriteria,
                                            HttpServletRequest request) {
        String submittedAfter = request.getParameter(SUBMITTED_AFTER);
        if (!StringUtils.isEmpty(submittedAfter)) {
            searchCriteria.put(SUBMITTED_AFTER,
                    XssHelper.stripXSS(submittedAfter));
        }

    }

    private void filterByQueue(Map<String, Object> searchCriteria,
                               HttpServletRequest request) {

        String[] selectedTicketQueue = request.getParameterValues(TICKET_QUEUE);
        if (null != selectedTicketQueue
                && StringUtils.isNotEmpty(selectedTicketQueue[0])) {
            String[] queues = null;
            if (selectedTicketQueue.length == 1) {
                queues = XssHelper.stripXSS(selectedTicketQueue[0]).split(",");
            }
            if (selectedTicketQueue.length > 1) {
                queues = selectedTicketQueue;
            }
            List<String> queueList = new ArrayList<>();
            for (String queue : queues) {
                String queueName = queue.trim();
                if ("Verification Workgroup".equalsIgnoreCase(queueName)) {
                    queueName = "Document Verification Workgroup";
                }
                queueList.add(queueName);
            }
            searchCriteria.put(TICKET_QUEUE, queueList);
        }
    }

    private void filterByTicketStatus(Map<String, Object> searchCriteria,
                                      HttpServletRequest request) {
        String selectedTicketStatus = request.getParameter(TICKET_STATUS_LIST);
        if (!StringUtils.isEmpty(selectedTicketStatus)) {
            searchCriteria.put(TICKET_STATUS_LIST,
                    XssHelper.stripXSS(selectedTicketStatus).split(","));
        } else {
            String[] anyStatus = { "Any" };
            searchCriteria.put(TICKET_STATUS_LIST, anyStatus);
        }
    }

    private void filterByTicketStatusForSimplifiedView(
            Map<String, Object> searchCriteria, HttpServletRequest request) {
        boolean assignedToMe = Boolean.parseBoolean(request
                .getParameter(ASSIGNED_TO_ME));
        boolean uncalimedTicket = Boolean.parseBoolean(request
                .getParameter(UNCLAIMED_TICKET));

        if (assignedToMe && !uncalimedTicket) {
            String[] status = { ticket_status.Canceled.getStatusCode(),
                    ticket_status.Completed.getStatusCode(),
                    ticket_status.Open.getStatusCode(),
                    ticket_status.Resolved.getStatusCode() };
            ;
            searchCriteria.put(TICKET_STATUS_LIST, status);
        } else if (!assignedToMe && uncalimedTicket) {
            String[] status = { ticket_status.UnClaimed.getStatusCode() };
            searchCriteria.put(TICKET_STATUS_LIST, status);
        }
    }

    private void filterByRequestedBy(Map<String, Object> searchCriteria,
                                     HttpServletRequest request) {
        String requestedByQueues = request.getParameter("requestedBy");
        String requestedByName = request.getParameter("requestedByName");
        requestedByQueues = XssHelper.stripXSS(requestedByQueues);
        requestedByName = XssHelper.stripXSS(requestedByName);
        searchCriteria.put("requestedByName", requestedByName);
        if (!StringUtils.isEmpty(requestedByQueues)
                && !StringUtils.isEmpty(requestedByName)) {
            searchCriteria.put("requestedBy", requestedByQueues);
        }

    }

    private void filterByCreatedFor(Map<String, Object> searchCriteria,
                                    HttpServletRequest request) {
        String moduleId = request.getParameter("roleId");
        String createdForName = request.getParameter("requester");
        String userRole = request.getParameter("userRole");

        if (!StringUtils.isEmpty(userRole)) {
            searchCriteria.put("userRole", userRole);
        }

        if (!StringUtils.isEmpty(moduleId)) {
            searchCriteria.put("moduleId", moduleId);
        }
        if (!StringUtils.isEmpty(createdForName)) {
            searchCriteria.put("createdForName", createdForName);
        }

    }

    private void filterByPriority(Map<String, Object> searchCriteria,
                                  HttpServletRequest request) {

        String[] selectedTicketPriority = request
                .getParameterValues("ticketPriority");
        if (null != selectedTicketPriority
                && StringUtils.isNotEmpty(selectedTicketPriority[0])) {
            String[] queues = null;
            if (selectedTicketPriority.length == 1) {
                queues = XssHelper.stripXSS(selectedTicketPriority[0]).split(
                        ",");
            }
            if (selectedTicketPriority.length > 1) {
                queues = selectedTicketPriority;
            }
            List<String> priorityList = new ArrayList<>();
            for (String priority : queues) {
                priorityList.add(priority.trim());
            }
            searchCriteria.put("ticketPriority", priorityList);
        }
    }

    private void filterByTicketNumber(Map<String, Object> searchCriteria,
                                      HttpServletRequest request) {
        String selectedTicketNum = request.getParameter(TICKET_NUMBER);
        if (!StringUtils.isEmpty(selectedTicketNum)) {
            searchCriteria.put(TICKET_NUMBER,
                    XssHelper.stripXSS(selectedTicketNum));
        }
    }

    private void filterByTicketComments(Map<String, Object> searchCriteria,
                                        HttpServletRequest request) {
        String ticketComments = request.getParameter(TICKET_COMMENTS);
        if (!StringUtils.isEmpty(ticketComments)) {
            searchCriteria.put(TICKET_COMMENTS,
                    XssHelper.stripXSS(ticketComments));
        }
    }

    private void filterBySubject(Map<String, Object> searchCriteria,
                                 HttpServletRequest request) {
        String selectedTicketSubject = request.getParameter(TICKET_SUBJECT);
        if (!StringUtils.isEmpty(selectedTicketSubject)) {
            searchCriteria.put(TICKET_SUBJECT,
                    XssHelper.stripXSS(selectedTicketSubject));
        }
    }

    private void filterByArchiveFlag(Map<String, Object> searchCriteria,
                                     HttpServletRequest request) {
        boolean archivedTicket = Boolean.parseBoolean(request
                .getParameter(ARCHIVED_FLAG_VALUE));
        String selectedArchivedTicket = "N";
        if (archivedTicket) {
            selectedArchivedTicket = "Y";
        }
        searchCriteria.put(ARCHIVED_FLAG_VALUE,
                XssHelper.stripXSS(selectedArchivedTicket));
    }

    private String getSortOrder(HttpServletRequest request) {

        String sortOrder = (StringUtils.isEmpty(request
                .getParameter(SORT_ORDER))) ? "DESC" : (request.getParameter(
                SORT_ORDER).equalsIgnoreCase("ASC") ? "ASC" : "DESC");
        String changeOrder = request.getParameter("changeOrder");
        changeOrder = XssHelper.stripXSS(changeOrder);
        changeOrder = StringUtils.isEmpty(changeOrder) ? FALSE : changeOrder;

        if (changeOrder.equals(TRUE)) {
            sortOrder = (sortOrder.equals(DESC)) ? "ASC" : DESC;
        }

        return sortOrder;
    }

    private void addViewMemberAccountDetails(Model model, HttpServletRequest request, TkmTickets tkmTickets) {
        try {
            LOGGER.debug("addViewMemberAccountDetails::getting nav details");
            // HIX-112739 - Add "impersonate member" action into ticket manager functionality
            capPortalConfigurationUtil.setMemberNavMenuPermissions(model, request);
            LOGGER.debug("addViewMemberAccountDetails::getting household for id {}", tkmTickets.getRole().getId());
            Household household = ticketMgmtUtils.getHouseholdByuserId(tkmTickets.getRole().getId());
            if(household != null) {
            	model.addAttribute("household", household);
            	model.addAttribute("householdId", household.getId());
            }
        } catch (InvalidUserException e) {
            LOGGER.error("addViewMemberAccountDetails::InvalidUserException while getting member nav permissions or household for id", e);
        } catch (Exception e) {
            LOGGER.error("addViewMemberAccountDetails::exception while getting member nav permissions or household for id", e);
        }
    }

    @RequestMapping(value = "/ticketmgmt/ticket/ticketdetail/{encTicketId}", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_VIEW_TICKET)
    public String getTicketDetailById(Model model, HttpServletRequest request,
                                      @PathVariable("encTicketId") String encTicketId) {

        clearSessionVariables(request);

        capRequestValidatorUtil.validateGetTicketDetailById(encTicketId);

        String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
        Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);

        // TkmTickets tkmTickets =
        // ticketMgmtUtils.getTkmTicketsById(IntEncTicketId);
        TkmTicketsResponse tkmTicketsResponse = ticketMgmtUtils
                .getTkmTicketsDetailsById(IntEncTicketId);
        TkmTickets tkmTickets = tkmTicketsResponse.getTkmTickets();
        model.addAttribute("tkmTicketsObj", tkmTickets);
        model.addAttribute("createdForName",
                tkmTicketsResponse.getCreatedForName());
        model.addAttribute(ATTR_PAGE_TITLE,
                "Getinsured Health Exchange: Details Of Tickets");
        model.addAttribute(TICKET_STATUS, tkmTickets.getStatus());
        List<TkmTicketTasks> ticketTasks = tkmTickets.getTkmtickettasks();
        request.getSession().setAttribute("TASK_STATUS",
                ticketTasks.get(ticketTasks.size() - 1).getStatus());
        request.getSession().setAttribute("TASK_STATUS",
                ticketTasks.get(0).getStatus());
        try {
            // Quick action bar
            TkmQuickActionDto quickAction = ticketMgmtUtils
                    .getDetailsForQuickActionBar(IntEncTicketId);
            AccountUser user;

            user = userService.getLoggedInUser();
            Set<String> userPermissions = getUserPermissions(request, user);
            model.addAttribute("isAssignAllowed",
                    userPermissions.contains("TKM_ASSIGN_ALLOWED"));
            model.addAttribute("quickAction", quickAction);
            addViewMemberAccountDetails(model, request, tkmTickets);
        } catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()) {
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving quick action bar details.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving quick action bar details.", ex.getMessage());
                    break;
            }
        }
        catch (InvalidUserException e) {
            LOGGER.info("Error while getting permission for TKM_ASSIGN_ALLOWED ");
        }
        return "ticketmgmt/ticket/ticketdetail";
    }

    private void clearSessionVariables(HttpServletRequest request) {
        request.getSession().removeAttribute(TICKET_HISTORY_LIST);
        request.getSession().removeAttribute(IS_DETAILED_VIEW);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/showcomments/{encTicketId}", method = RequestMethod.GET)
    @PreAuthorize(TKM_VIEW_TICKET)
    public String showComment(@PathVariable("encTicketId") String encTicketId,
                              Model model, HttpServletRequest request) {
        LOGGER.info("Ticket Comment page");
        capRequestValidatorUtil.validateShowComment(encTicketId);
        TkmTickets tkmTickets = null;

        try {
            String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
            Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);
            tkmTickets = ticketMgmtUtils.getTkmTicketsById(IntEncTicketId);

            // Quick action bar
            TkmQuickActionDto quickAction = ticketMgmtUtils
                    .getDetailsForQuickActionBar(IntEncTicketId);
            model.addAttribute("quickAction", quickAction);
            AccountUser user;
            try {
                user = userService.getLoggedInUser();
                Set<String> userPermissions = getUserPermissions(request, user);
                model.addAttribute("isAssignAllowed",
                        userPermissions.contains("TKM_ASSIGN_ALLOWED"));
                addViewMemberAccountDetails(model, request, tkmTickets);
            } catch (InvalidUserException e) {
                LOGGER.info("Error while getting permission for TKM_ASSIGN_ALLOWED "
                        + e);
            }
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving quick action bar details",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving quick action bar details",ex.getMessage());
                    break;
            }
            throw ex;
        }

        model.addAttribute("ticketdtl", tkmTickets);
        model.addAttribute(TICKET_STATUS, tkmTickets.getStatus());

        return "ticketmgmt/ticket/showcomments";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/createpage")
    @PreAuthorize(TKM_SAVE_TICKET)
    public String showCreateTicketPage(Model model, HttpServletRequest request)
            throws InvalidUserException {
        LOGGER.info("Displaying Create Ticket Page ");
        capRequestValidatorUtil.validateShowCreateTicketPage(request);

        List<String> typeList = new ArrayList<String>();
        List<Role> roleList = null;

        List<TkmQueues> queuesList = null;
        TicketPriority[] priorityList = null;
        Role role = null;

        AccountUser user = userService.getLoggedInUser();
        String requestorId = request.getParameter("requestorId");
        String requestorName = request.getParameter("requestorName");
        String prefillUser = request.getParameter("prefillUser");
        String roleName = request.getParameter("roleName");
        String moduleId = request.getParameter("moduleId");
        String moduleName = request.getParameter("moduleName");

        if (!StringUtils.isEmpty(roleName)) {
            role = roleService.findRoleByName(roleName);
        }

        try {
            mapTicketWorkflowByPermission.clear();
            Set<String> userPermissions = getUserPermissions(request, user);
            Set<String> workflowPermissions = getWorkFlowPermissions(userPermissions);
            if (getTicketTypeList()) {
                for (String permission : mapTicketWorkflowByPermission.keySet()) {
                    if (workflowPermissions.contains(permission)) {
						//HIX-112346 : Merging ticket type and subtype in to one list to remove ticket subtype dropdown from UI
                    	Collection<List<String>> keySet = mapTicketWorkflowByPermission.get(permission).values();
                    	keySet.forEach(subMenus -> typeList.addAll(subMenus));
                    }
                }
            }
            roleList = this.getRolesForTickets();
            queuesList = ticketMgmtUtils.getQueuesList();
            priorityList = TkmTickets.TicketPriority.values();

        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving queue list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving queue list", ex.getMessage());
                    break;
            }
            throw ex;
        }

        model.addAttribute("stateExchenge", STATE_EXCHANGE);
        model.addAttribute("typeList", typeList);
        model.addAttribute("roleList", roleList);
        model.addAttribute("queuesList", queuesList);
        model.addAttribute("priorityList", priorityList);
        model.addAttribute(FLOW, "add");
        model.addAttribute("prevPage", TICKET_LIST_MAPPING);
        model.addAttribute("prefillUser", prefillUser);
        model.addAttribute("moduleId", moduleId);
        model.addAttribute("moduleName", moduleName);

        // role = roleService.findRoleByName(roleName);
        if (!StringUtils.isEmpty(roleName)) {

            model.addAttribute("roleName", roleName);
            model.addAttribute("roleId", role.getId());
        }

        if (!StringUtils.isEmpty(requestorId)) {
            model.addAttribute("requestorId", Integer.parseInt(requestorId));
        } else {
            model.addAttribute("requestorId", user.getId());
        }

        model.addAttribute("requestorName", requestorName);

        return CREATE_TICKET_MAPPING;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/subtype", method = RequestMethod.POST)
    @PreAuthorize(TKM_SAVE_TICKET)
    @ResponseBody
    public String getTicketSubtypeByPermission(@RequestParam String ticketType,
                                               HttpServletResponse response, HttpServletRequest request)
            throws InvalidUserException {
        // LOGGER.info("Get the subtype of the ticket for ticket type: " +
        // ticketType);
        response.setContentType(APP_JSON_CONTENT);

        String strResp = "";
        List<String> typeList = null;

        if (mapTicketWorkflowByPermission.isEmpty()) {
            getTicketTypeList();
        }

        AccountUser user = userService.getLoggedInUser();
        Set<String> userPermissions = getUserPermissions(request, user);
        Set<String> workflowPermissions = getWorkFlowPermissions(userPermissions);

        for (String permission : mapTicketWorkflowByPermission.keySet()) {
            if (workflowPermissions.contains(permission)) {
                Map<String, List<String>> workflowType = mapTicketWorkflowByPermission
                        .get(permission);
                if (workflowType.containsKey(ticketType)) {
                    typeList = workflowType.get(ticketType);
                    break;
                }

            }
        }

        if (typeList == null) {
            // should not come here
            typeList = new ArrayList<String>();
            // LOGGER.error("Unable to get the ticket sub type for type : " +
            // ticketType);
        }

        try {
            strResp = platformGson.toJson(typeList);
        } catch (Exception e) {
            LOGGER.error("Unable to parse to JSON string");
        }

        return strResp;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/type", method = RequestMethod.POST)
    @PreAuthorize(TKM_SAVE_TICKET)
    @ResponseBody
    public String getTicketSubtype(@RequestParam String ticketType,
                                   HttpServletResponse response) throws InvalidUserException {
        // LOGGER.info("Get the subtype of the ticket for ticket type: " +
        // ticketType);
        response.setContentType(APP_JSON_CONTENT);

        String strResp = "";
        List<String> typeList = null;

        if (mapTicketWorkflowByPermission.isEmpty()) {
            getTicketTypeList();
        }

        for (String permission : mapTicketWorkflowByPermission.keySet()) {

            Map<String, List<String>> workflowType = mapTicketWorkflowByPermission
                    .get(permission);
            if (workflowType.containsKey(ticketType)) {
                typeList = workflowType.get(ticketType);
                break;
            }

        }

        if (typeList == null) {
            // should not come here
            typeList = new ArrayList<String>();
            // LOGGER.error("Unable to get the ticket sub type for type : " +
            // ticketType);
        }
        try {
            strResp = gson.toJson(typeList);
        } catch (Exception e) {
            LOGGER.error("Unable to parse to JSON string");
        }
        return strResp;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/requesters", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getRequestersByRoleType(@RequestParam String roleType,
                                          @RequestParam String moduleId, @RequestParam String moduleName,
                                          @RequestParam String userName, HttpServletResponse response) {
        // LOGGER.info("Get the list of users for the role: " + roleType);
        response.setContentType(APP_JSON_CONTENT);

        String strResp = "";
        List<Object[]> requesterList = new ArrayList<Object[]>();
        Map<Integer, String> mapRequester = null;
        boolean onlyUserName = false;

        try {

            if (StringUtils.isEmpty(moduleId)) {
            	
                // requesterList =
                // userService.getAccountUsersInfoByUserRole(Integer.parseInt(roleType));
            	 if(null == roleType || roleType.trim().length() == 0) {
                 	requesterList = userService.getAccountUsersInfoByName(userName);
                 	onlyUserName=true;
                 } else {
                 	requesterList = userService.getAccountUsersInfoByRoleAndName(
                             Integer.parseInt(roleType), userName);
                 }
            } else {
                Role role = roleService.findById(Integer.parseInt(moduleId));
                String roleName = (role != null) ? role.getName() : moduleName;
                // get it from module user table
                List<ModuleUser> moduleUser = userService.getModuleUsers(
                        Integer.parseInt(moduleId), roleName.toLowerCase());
                AccountUser accountUser = moduleUser.get(0).getUser();
                Object user[] = {
                        accountUser,
                        accountUser.getFirstName() + " "
                                + accountUser.getLastName() };
                requesterList.add(user);
            }

        } catch (Exception e) {
            LOGGER.error("Unable to get the user list by role type", e);
            return strResp;
        }

        if(!onlyUserName) {
        	mapRequester = (mapRequester == null) ? getIdNameMapForUser(requesterList)
        			: mapRequester;

        }

        try {
            strResp = gson.toJson(requesterList);
        } catch (Exception e) {
            LOGGER.error("Unable to parse the user map to JSON");
        }

        return strResp;
    }

    private boolean isAllowed(Model model, HttpServletRequest request) {
        AccountUser user = null;
        HashMap<String, Object> errorMsg = new HashMap<String, Object>();

        try {
            user = userService.getLoggedInUser();
            if (user == null) {
                errorMsg.put("message", "User not authorized.");
                model.addAttribute("authfailed", errorMsg.get("message"));
                request.getSession().setAttribute(
                        "SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
                return false;
            }
        } catch (InvalidUserException ex) {
            LOGGER.error("User not logged in");
            errorMsg.put("message", "User not logged in.");
            model.addAttribute("authfailed", errorMsg.get("message"));
            request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION",
                    errorMsg);
            return false;
        }
        return true;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/createpage/submit", method = RequestMethod.POST)
    @PreAuthorize(TKM_SAVE_TICKET)
    public String saveTicket(Model model,
                             @Valid @ModelAttribute("TkmTickets") TkmTickets ticketFormObj,
                             BindingResult result, HttpServletRequest request)
            throws InvalidUserException {

        LOGGER.info("Ticket save functionality initialized");
        capRequestValidatorUtil.validateSaveTicket(request);

        TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
        String errorMsg = "";

        if (!this.isAllowed(model, request)) {
            return REDIRECT_ACCOUNT_USER_LOGINFAILED;
        }
        List<String> defaultMsgs = new ArrayList<String>();
        if (result.hasErrors()) {
            for (FieldError fieldError : result.getFieldErrors()) {

                defaultMsgs.add(fieldError.getField() + " "
                        + fieldError.getDefaultMessage());

            }

            request.getSession().setAttribute(ERROR_MSG, defaultMsgs);
            return "redirect:/ticketmgmt/ticket/createpage";
        } else {
            AccountUser user = userService.getLoggedInUser();

            tkmTicketsRequest.setCreatedBy(user.getId());
            tkmTicketsRequest.setLastUpdatedBy(user.getId());
            tkmTicketsRequest.setSubject(StringEscapeUtils
                    .unescapeHtml(capRequestValidatorUtil.removeSpecialCharacters(ticketFormObj.getSubject())));
            tkmTicketsRequest.setDescription(StringEscapeUtils
                    .unescapeHtml(capRequestValidatorUtil.removeSpecialCharacters(ticketFormObj.getDescription())));
            tkmTicketsRequest.setCategory(ticketFormObj.getTkmWorkflows()
                    .getCategory());
            tkmTicketsRequest
                    .setType(ticketFormObj.getTkmWorkflows().getType());
            // tkmTicketsRequest.setUserRoleId(ticketFormObj.getUserRole().getId());
            tkmTicketsRequest.setRequester(ticketFormObj.getRole().getId());

            AccountUser objUser = userService.findById(ticketFormObj.getRole()
                    .getId());
            Role currUserDefRole = userService.getDefaultRole(objUser);

            if (null != currUserDefRole) {
                tkmTicketsRequest.setUserRoleId(currUserDefRole.getId());
            }
            tkmTicketsRequest.setPriority(ticketFormObj.getPriority());
            String rolename = null;

            Integer moduleId = ticketFormObj.getModuleId();
            String moduleName = ticketFormObj.getModuleName();

            if (null != ticketFormObj.getUserRole()) {
                Role role = roleService.findById(ticketFormObj.getUserRole()
                        .getId());
                rolename = role.getName();
            }

            if (null != moduleId && !StringUtils.isEmpty(moduleName)) {
                tkmTicketsRequest.setModuleId(moduleId);
                tkmTicketsRequest.setModuleName(moduleName);
            } else if ("INDIVIDUAL".equalsIgnoreCase(rolename)
                    && StringUtils.isEmpty(moduleName) && null == moduleId) {
                // query
                try{
                    Household household = ticketMgmtUtils
                            .getHouseholdByuserId(ticketFormObj.getRole().getId());
                    tkmTicketsRequest.setModuleId(household.getId());
                    tkmTicketsRequest.setModuleName("HOUSEHOLD");

                }
                catch(RestfulResponseException ex){
                    switch(ex.getHttpStatus()){
                        case INTERNAL_SERVER_ERROR:
                            LOGGER.error("Internal server error in retrieving household by user id ="+ticketFormObj.getRole().getId(),ex.getMessage());
                            break;
                        default:
                            LOGGER.error("Unknown error in retrieving household by user id ="+ticketFormObj.getRole().getId(), ex.getMessage());
                            break;
                    }
                    throw ex;
                }

            } else {
                tkmTicketsRequest.setModuleId(ticketFormObj.getRole().getId());
                tkmTicketsRequest.setModuleName("ACCOUNTUSER");

            }

            /*
             * if(!StringUtils.isEmpty(moduleName)) {
             * tkmTicketsRequest.setModuleName(moduleName); }
             *
             * else { tkmTicketsRequest.setModuleName("ACCOUNTUSER"); }
             */

            try {
                ticketMgmtUtils
                        .addNewTicket(tkmTicketsRequest);
            }
            catch(RestfulResponseException ex){
                switch(ex.getHttpStatus()){
                    case INTERNAL_SERVER_ERROR:
                        LOGGER.error("Internal server error in add new ticket",ex.getMessage());
                        break;
                    default:
                        LOGGER.error("Unknown error in in add new ticket",ex.getMessage());
                        break;
                }
                throw ex;
            }

            if (!StringUtils.isEmpty(errorMsg)) {
                request.getSession().setAttribute(ERROR_MSG, errorMsg);
                return "redirect:/ticketmgmt/ticket/createpage";
            }

            String prevPage = TICKET_LIST_MAPPING;
            return REDIRECT + prevPage;
        }

    }

    public boolean getTicketTypeList() {
        boolean bStatus = false;
        List<TkmWorkflows> ticketTypeList = null;
        if (!mapTicketWorkflowByPermission.isEmpty()) {
            return !bStatus;
        }
        try{
            ticketTypeList = ticketMgmtUtils.getTicketTypeList();
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket list", ex.getMessage());
                    break;
            }
        }

        if (ticketTypeList != null) {
            for (TkmWorkflows tkmWorkflows : ticketTypeList) {
                String key = tkmWorkflows.getCategory().toString();
                String value = tkmWorkflows.getType();
                String permission = tkmWorkflows.getPermission();
                if (mapTicketWorkflowByPermission.containsKey(permission)) {
                    Map<String, List<String>> mapTicketWorkflow = mapTicketWorkflowByPermission
                            .get(permission);
                    if (mapTicketWorkflow.containsKey(key)) {
                        mapTicketWorkflow.get(key).add(value);
                    } else {
                        List<String> categoryList = new ArrayList<>();
                        categoryList.add(value);
                        mapTicketWorkflow.put(key, categoryList);
                    }

                } else {
                    List<String> categoryList = new ArrayList<>();
                    categoryList.add(value);
                    Map<String, List<String>> workFlowTypeMap = new HashMap<>();
                    workFlowTypeMap.put(key, categoryList);
                    mapTicketWorkflowByPermission.put(permission,
                            workFlowTypeMap);
                }
            }

            bStatus = true;
        }

        return bStatus;
    }

    /**
     * @author Sharma_k
     * @since 03rd May 2013
     * @param ticketId
     * @param response
     * @return Ajax call to fetch TkmTicketTasks on the basis of TicketId
     */
    @RequestMapping(value = "/ticketmgmt/ticket/tickettasklist", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String ticketTaskList(@RequestParam String ticketId,
                                 HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);
        List<TkmTicketTasks> tkmTicketTaskList = null;
        try{
            tkmTicketTaskList = ticketMgmtUtils.getTicketTaskList(ticketId);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket task list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket task list", ex.getMessage());
                    break;
            }
        }

        List<Map<String, String>> ticketData = new ArrayList<Map<String, String>>();
        for (TkmTicketTasks tkmTicketTasks : tkmTicketTaskList) {
            Map<String, String> taskData = new HashMap<String, String>();

            taskData.put("TaskName",
                    String.valueOf(tkmTicketTasks.getTaskName()));
            taskData.put("Status", tkmTicketTasks.getStatus());
            taskData.put("StartDate",
                    (tkmTicketTasks.getStartDate() != null ? tkmTicketTasks
                            .getStartDate().toString() : ""));
            taskData.put("EndDate",
                    (tkmTicketTasks.getEndDate() != null ? tkmTicketTasks
                            .getEndDate().toString() : ""));
            // Set Assignee as blank if not provided.
            taskData.put(ASSIGNEE,
                    (tkmTicketTasks.getAssignee() != null ? tkmTicketTasks
                            .getAssignee().getFirstName() : ""));

            ticketData.add(taskData);
        }
        return gson.toJson(ticketData);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/savecomment", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String saveTicketComments(@RequestParam(TICKET_ID) String ticketId,
                                     @RequestParam("comment") String comment, HttpServletRequest request)
            throws InvalidUserException {
        String errorMsg = null;
        AccountUser user = userService.getLoggedInUser();
        try {
            String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
            ticketMgmtUtils.saveComment(Integer.valueOf(StrEncTicketId),
                    comment.trim(), user.getId());
        }catch(RestfulResponseException ex){
            errorMsg=ex.getMessage();
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in saving ticket comments.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in saving ticket comments", ex.getMessage());
                    break;
            }
        }
        return errorMsg;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/edit/{encTicketId}")
    @PreAuthorize(TKM_EDIT_TICKET)
    public String showEditTicketPage(Model model, HttpServletRequest request,
                                     @PathVariable("encTicketId") String encTicketId)
            throws InvalidUserException {
        // LOGGER.info("Displaying Edit Ticket Page :" + encTicketId);
        // Get the previous page URL
        capRequestValidatorUtil.validateShowEditTicketPage(encTicketId);
        String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
        String prevPage = request.getHeader("referer");
        String contextPath = request.getContextPath();
        prevPage = prevPage.substring(prevPage.indexOf(contextPath)
                + contextPath.length());
        if (StringUtils.isEmpty(prevPage)) {
            prevPage = "/hix/ticketmgmt/ticket/ticketlist";
        }
        List<String> typeList = new ArrayList<String>();
        List<Role> roleList = new ArrayList<Role>();
        List<TkmQueues> queuesList = new ArrayList<TkmQueues>();
        TkmTickets tkmTickets = null;
        try{
            tkmTickets = ticketMgmtUtils.getTkmTicketsById(Integer.parseInt(StrEncTicketId));
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket by ticket id="+StrEncTicketId,ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket by ticket id="+StrEncTicketId, ex.getMessage());
                    break;
            }
        }

        TicketPriority[] priorityList = TkmTickets.TicketPriority.values();

        model.addAttribute("typeList", typeList);
        model.addAttribute("roleList", roleList);
        model.addAttribute("queuesList", queuesList);
        model.addAttribute("ticket", tkmTickets);
        model.addAttribute("priorityList", priorityList);
        model.addAttribute(FLOW, "edit");
        model.addAttribute("prevPage", prevPage);

        return CREATE_TICKET_MAPPING;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/edit/submit", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    public String editTicketSubmit(Model model,
                                   @Valid @ModelAttribute("TkmTickets") TkmTickets ticketFormObj,
                                   BindingResult result, HttpServletRequest request)
            throws InvalidUserException {

        capRequestValidatorUtil.validateEditTicketSubmit(request);

        String prevPage = request.getParameter("prevPage");
        if (StringUtils.isEmpty(prevPage)) {
            prevPage = TICKET_LIST_MAPPING;
        }

        LOGGER.info("Save the edited ticket in the database");
        String errorMsg = "";

        if (!this.isAllowed(model, request)) {
            return REDIRECT_ACCOUNT_USER_LOGINFAILED;
        }
        List<String> defaultMsgs = new ArrayList<String>();
        if (result.hasErrors()) {
            for (FieldError fieldError : result.getFieldErrors()) {

                defaultMsgs.add(fieldError.getField() + " "
                        + fieldError.getDefaultMessage());

            }

            request.getSession().setAttribute(ERROR_MSG,
                    TICKET_CANNOT_BE_EDITED);
            return REDIRECT + prevPage;
        } else {
            AccountUser user = userService.getLoggedInUser();

            TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
            tkmTicketsRequest.setLastUpdatedBy(user.getId());
            tkmTicketsRequest.setId(ticketFormObj.getId());
            tkmTicketsRequest.setSubject(capRequestValidatorUtil.removeSpecialCharacters(ticketFormObj.getSubject()));
            tkmTicketsRequest.setDescription(capRequestValidatorUtil.removeSpecialCharacters(ticketFormObj.getDescription()));
            tkmTicketsRequest.setPriority(ticketFormObj.getPriority());

            try {
                ticketMgmtUtils.editTicket(tkmTicketsRequest);
            }
            catch(RestfulResponseException ex){
                switch(ex.getHttpStatus()){
                    case INTERNAL_SERVER_ERROR:
                        LOGGER.error("Internal server error in editing ticket. id="+ticketFormObj.getId(),ex.getMessage());
                        break;
                    default:
                        LOGGER.error("Unknown error in editing ticket. id="+ticketFormObj.getId(),ex.getMessage());
                        break;
                }
                throw ex;
            }

            if (!StringUtils.isEmpty(errorMsg)) {
                model.addAttribute(ERROR_MSG, errorMsg);
                request.getSession().setAttribute(ERROR_MSG, errorMsg);
                return "redirect: " + ticketFormObj.getId();
            }

            // LOGGER.info("Ticket got edited successfully with ticket id: " +
            // tkmTickets.getId());
            return REDIRECT + prevPage;
        }
    }

    /**
     * @author Sharma_k
     * @since 03rd May 2013
     * @param ticketId
     * @param response
     * @return Ajax call to fetch TkmTicketTasks on the basis of TicketId
     */
    @RequestMapping(value = "/ticketmgmt/ticket/userTickettasklist", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getUserTicketTaskList(@RequestParam String ticketId,
                                        HttpServletResponse response) {
        capRequestValidatorUtil.validateEncryptedId(ticketId);

        response.setContentType(APP_JSON_CONTENT);

        List<TkmTicketTasks> tkmTicketTaskList = null;
        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        try {
            tkmTicketTaskList = ticketMgmtUtils
                    .getTicketTaskList(StrEncTicketId.toString());
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket task list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket task list", ex.getMessage());
                    break;
            }
            throw ex;
        }

        List<Map<String, String>> ticketData = new ArrayList<Map<String, String>>();
        for (TkmTicketTasks tkmTicketTasks : tkmTicketTaskList) {
            Map<String, String> taskData = new HashMap<String, String>();
            taskData.put("TaskName",
                    String.valueOf(tkmTicketTasks.getTaskName()));
            taskData.put("Status", tkmTicketTasks.getStatus());
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
            String startDate = null;
            String endDate = null;

            if (tkmTicketTasks.getStartDate() != null) {
                startDate = formatter.format(tkmTicketTasks.getStartDate());
            }

            if (tkmTicketTasks.getEndDate() != null) {
                endDate = formatter.format(tkmTicketTasks.getEndDate());
            }
            taskData.put("StartDate", startDate);
            taskData.put("EndDate", (endDate == null) ? "" : endDate);
            taskData.put("Assignee", (tkmTicketTasks.getAssignee() == null ? ""
                    : tkmTicketTasks.getAssignee().getFirstName()));
            taskData.put("QueueId", (tkmTicketTasks.getQueue() == null ? ""
                    : tkmTicketTasks.getQueue().getId().toString()));
            taskData.put("UserId", (tkmTicketTasks.getAssignee() == null ? ""
                    : String.valueOf(tkmTicketTasks.getAssignee().getId())));
            taskData.put(TASK_ID, tkmTicketTasks.getTaskId().toString());
            taskData.put(ACTIVITI_TASK_ID, tkmTicketTasks.getActivitiTaskId());
            taskData.put("ProcessInstanceId",
                    tkmTicketTasks.getProcessInstanceId());
            taskData.put("Comments", tkmTicketTasks.getComments());
            ticketData.add(taskData);
        }
        return gson.toJson(ticketData);
    }

    @SuppressWarnings("unchecked")
	@RequestMapping(value = "/ticketmgmt/ticket/claimUserTask", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String claimUserTask(Model model, @RequestParam String taskObject,
                                HttpServletRequest request, HttpServletResponse response)
            throws InvalidUserException {
        response.setContentType(APP_JSON_CONTENT);
        Map<String, String> map = new HashMap<String, String>();
        String newTaskObject = StringEscapeUtils.unescapeHtml(taskObject);
        map = gson.fromJson(newTaskObject, map.getClass());
        TkmTicketsTaskRequest ticketsTaskRequest = new TkmTicketsTaskRequest();
        String taskId = map.get(ACTIVITI_TASK_ID).toString();
        String StrEncTaskId = (StringUtils.isNumeric(taskId)) ? taskId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(taskId);
        ticketsTaskRequest.setActivitiTaskId(StrEncTaskId);
        ticketsTaskRequest.setTaskId(Integer.parseInt(map.get(TASK_ID)));
        AccountUser user = userService.getLoggedInUser();
        // Integer taskId = ticketsTaskRequest.getTaskId();
        ticketsTaskRequest.setUserId(user.getId());
        String errorMsg = StringUtils.EMPTY;
        try {
            ticketMgmtUtils.claimUserTask(ticketsTaskRequest);
            request.getSession().setAttribute("TASK_STATUS", "Claimed");

        }
        catch(RestfulResponseException ex){
            errorMsg = ex.getMessage();
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in claiming user task.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in  claiming user task.", ex.getMessage());
                    break;
            }
            throw ex;
        }

        return errorMsg;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/completeUserTask", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    @SuppressWarnings(UNCHECKED)
    public String completeUserTask(@RequestParam String taskObject,
                                   @RequestParam String comments, @RequestParam String commentLength,
                                   HttpServletRequest request, HttpServletResponse response)
            throws InvalidUserException {
        response.setContentType(APP_JSON_CONTENT);
        Map map = new HashMap();
        StringBuffer returnMsg = new StringBuffer("SUCCESS");
        String newTaskObject = StringEscapeUtils.unescapeHtml(taskObject);
        String commentsText = StringEscapeUtils.unescapeHtml(comments);
        map = gson.fromJson(newTaskObject, map.getClass());
        TkmTicketsTaskRequest ticketsTaskRequest = new TkmTicketsTaskRequest();
        Map propertiesMap = (Map) map.get("formPropertiesMap");

        capRequestValidatorUtil.validateCompleteUserTask(map, comments,
                commentLength);

        // set logged in user
        AccountUser user = userService.getLoggedInUser();
        ticketsTaskRequest.setUserId(user.getId());

        String taskId = map.get(ACTIVITI_TASK_ID).toString();
        String StrEncTaskId = (StringUtils.isNumeric(taskId)) ? taskId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(taskId);
        ticketsTaskRequest.setActivitiTaskId(StrEncTaskId);
        ticketsTaskRequest.setActivitiTaskId(String.valueOf(StrEncTaskId));
        ticketsTaskRequest.setTaskId(Integer.valueOf(String.valueOf(map
                .get(TASK_ID))));
        ticketsTaskRequest.setProcessInstanceId(String.valueOf(map
                .get("ProcessInstanceId")));
        ticketsTaskRequest.setTaskVariables(propertiesMap);

        try {
            boolean isCommentValid = TicketMgmtUtils.isCommentValid(
                    commentsText, commentLength);
            if (!isCommentValid) {
                returnMsg = new StringBuffer("ERROR|")
                        .append(INVALID_COMMENT_DATA);
                return returnMsg.toString();
            }

            ticketsTaskRequest.setComments(commentsText);
            TkmTicketTaskResponse tkmTicketTaskResponse = ticketMgmtUtils
                    .completeUserTask(ticketsTaskRequest);
            List<TaskFormProperties> formProperties = tkmTicketTaskResponse
                    .getTkmTaskProperties();
            if (formProperties != null) {
                String redirectUrl = null;
                String caseNumber = null;
                for (TaskFormProperties taskFormProperties : formProperties) {
                    if (taskFormProperties.getId().equals("redirectUrl")) {
                        if (taskFormProperties.getValues().size() > 0) {
                            redirectUrl = taskFormProperties.getValues().get(0);
                        }
                    }
                    if (taskFormProperties.getId().equals("caseNumber")) {
                        if (taskFormProperties.getValues().size() > 0) {
                            caseNumber = taskFormProperties.getValues().get(0);
                        }
                    }
                }
                if (redirectUrl != null) {
                    returnMsg.append("|").append(redirectUrl).append("|")
                            .append(caseNumber);
                }
                //HIX-116415 Calling application eligibility status update api
                LOGGER.info("Calling updateEligibilityStatus API with caseNumber " + caseNumber);
                ticketMgmtUtils.updateApplicationEligibility(caseNumber);

            }
            
        }
        catch(RestfulResponseException ex){
            returnMsg = new StringBuffer("ERROR|").append(ex.getMessage());
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in completing user task.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in completing user task.", ex.getMessage());
                    break;
            }
        }
        return returnMsg.toString();
    }

    @RequestMapping(value = "ticketmgmt/ticket/redirection", method = RequestMethod.POST)
    public String redirect(@RequestParam("redirectUrl") String redirectUrl,
                           @RequestParam("caseNumber") String caseNumber,
                           final RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("caseNumber", caseNumber);
        return "redirect:" + redirectUrl;

    }

    @SuppressWarnings(UNCHECKED)
    @RequestMapping(value = "/ticketmgmt/ticket/tickethistory/{encTicketId}", method = RequestMethod.GET)
    @PreAuthorize(TKM_VIEW_TICKET)
    public String getTicketHistory(Model model, HttpServletRequest request,
                                   @PathVariable("encTicketId") String encTicketId) {
        LOGGER.info("TicketHistory page");
        capRequestValidatorUtil.validateGetTicketHistory(encTicketId, request);
        String strPageNum = request.getParameter("pageNumber");
        int iPageNum = StringUtils.isEmpty(strPageNum) ? 0 : (Integer
                .parseInt(strPageNum) - 1);
        int startRecord = iPageNum * LISTING_NUMBER;
        int showList = startRecord + LISTING_NUMBER;

        String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
        Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);

        try {
            boolean isDetailedView = false;
            List<HistoryDto> ticketHistoryList = null;
            List<HistoryDto> finalHistoryList = new ArrayList<>();

            if (null != request.getSession().getAttribute(TICKET_HISTORY_LIST)) {
                ticketHistoryList = (List<HistoryDto>) request.getSession()
                        .getAttribute(TICKET_HISTORY_LIST);
                isDetailedView = (Boolean) request.getSession().getAttribute(
                        IS_DETAILED_VIEW);

//				request.getSession().removeAttribute(TICKET_HISTORY_LIST);
//				request.getSession().removeAttribute(IS_DETAILED_VIEW);
            } else {
                ticketHistoryList = ticketMgmtUtils.getTicketHistory(
                        IntEncTicketId, false);

            }

            String timezone = DynamicPropertiesUtil
                    .getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);

            for (HistoryDto historyDto : ticketHistoryList) {
                String convertedDate = TicketMgmtUtils
                        .convertDateByTimeZone(historyDto.getDisDate(),
                                "MMM dd, yyyy hh:mm a", timezone);
                historyDto.setDisDate(convertedDate);
            }
            for (int i = startRecord; i < showList; i++) {
                if (ticketHistoryList.size() <= i) {
                    break;
                }
                finalHistoryList.add(ticketHistoryList.get(i));
            }
            // Quick action bar
            TkmQuickActionDto quickAction = ticketMgmtUtils
                    .getDetailsForQuickActionBar(IntEncTicketId);
            model.addAttribute("quickAction", quickAction);

            TkmTickets tkmTickets = ticketMgmtUtils
                    .getTkmTicketsById(IntEncTicketId);
            model.addAttribute("ticket", tkmTickets);
            model.addAttribute(TICKET_HISTORY_LIST, finalHistoryList);
            model.addAttribute(ATTR_PAGE_TITLE,
                    "Getinsured Health Exchange: History Of Tickets");
            model.addAttribute(TICKET_STATUS, tkmTickets.getStatus());
            model.addAttribute(IS_DETAILED_VIEW, isDetailedView);
            model.addAttribute("PageSize", LISTING_NUMBER);
            model.addAttribute("resultSize", ticketHistoryList.size());
            AccountUser user;
            try {
                user = userService.getLoggedInUser();
                Set<String> userPermissions = getUserPermissions(request, user);
                model.addAttribute("isAssignAllowed",
                        userPermissions.contains("TKM_ASSIGN_ALLOWED"));
                addViewMemberAccountDetails(model, request, tkmTickets);
            } catch (InvalidUserException e) {
                LOGGER.info("Error while getting permission for TKM_ASSIGN_ALLOWED ");
            }
        } catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Failed to load ticket history.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Failed to load ticket history.", ex.getMessage());
                    break;
            }
            throw ex;
        }

        return "ticketmgmt/ticket/tickethistory";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/tickethistory", method = RequestMethod.POST)
    @PreAuthorize(TKM_VIEW_TICKET)
    public String switchHistoryView(@RequestParam String encTicketId,
                                    @RequestParam boolean isDetailedView, Model model,
                                    HttpServletRequest request) {
        LOGGER.info("TicketHistory page");
        try {
            // boolean isDetailedView = (request.getParameter(IS_DETAILED_VIEW)
            // == null) ? false : true;
            String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
            Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);
            List<HistoryDto> ticketHistoryList = ticketMgmtUtils
                    .getTicketHistory(IntEncTicketId, isDetailedView);
            request.getSession().setAttribute(TICKET_HISTORY_LIST,
                    ticketHistoryList);
            request.getSession().setAttribute(IS_DETAILED_VIEW, isDetailedView);
            if(!isDetailedView){
                clearSessionVariables(request);
            }
            TkmTickets tkmTickets = ticketMgmtUtils
                    .getTkmTicketsById(IntEncTicketId);
            model.addAttribute("ticket", tkmTickets);
            model.addAttribute(ATTR_PAGE_TITLE,
                    "Getinsured Health Exchange: History Of Tickets");
            model.addAttribute(TICKET_STATUS, tkmTickets.getStatus());
            model.addAttribute(IS_DETAILED_VIEW, isDetailedView);
            model.addAttribute("PageSize", LISTING_NUMBER);
            model.addAttribute("resultSize", ticketHistoryList.size());
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Failed to load ticket history.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Failed to load ticket history.", ex.getMessage());
                    break;
            }
        }
        return "ticketmgmt/ticket/tickethistory";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/getTaskFormProperties", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getTaskFormProperties(@RequestParam String taskId,
                                        HttpServletResponse response, HttpServletRequest request) {
        capRequestValidatorUtil.validateEncryptedId(taskId);
        response.setContentType(APP_JSON_CONTENT);
        String StrEncTaskId = (StringUtils.isNumeric(taskId)) ? taskId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(taskId);
        List<TaskFormProperties> taskFormPropertiesList = null;
        try {
            taskFormPropertiesList = ticketMgmtUtils
                    .getTaskFormProperties(StrEncTaskId);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket task properties.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket task properties.", ex.getMessage());
                    break;
            }
            throw ex;
        }
        return gson.toJson(taskFormPropertiesList);

    }

    @RequestMapping(value = "/ticketmgmt/ticket/getEncValue", method = RequestMethod.GET)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getEncValue(@RequestParam String id,
                              HttpServletResponse response, HttpServletRequest request) {
        response.setContentType(APP_JSON_CONTENT);

        String strEncId = (id == null) ? id : ghixJasyptEncrytorUtil
                .encryptStringByJasypt(id);
        return gson.toJson(strEncId);

    }

    @RequestMapping(value = "/ticketmgmt/ticket/getTicketDocuments/{encTicketId}", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_VIEW_TICKET)
    public String loadTicketDocuments(
            @PathVariable("encTicketId") String encTicketId, Model model,
            HttpServletRequest request) throws InvalidUserException {
        capRequestValidatorUtil.validateLoadTicketDocuments(encTicketId);
        String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
        Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);

        AccountUser user = userService.getLoggedInUser();

        List<TkmDocuments> ticketDocumentList = null;
        TkmTickets tkmTicket = null;
        try {
            tkmTicket = ticketMgmtUtils
                    .getTkmTicketsById(IntEncTicketId);
            model.addAttribute("Subject", tkmTicket.getSubject());
            model.addAttribute(TICKET_ID, IntEncTicketId.toString());
            model.addAttribute("tkmTicketsObj", tkmTicket);

            ticketDocumentList = ticketMgmtUtils
                    .getTicketDocuments(IntEncTicketId.toString());
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket or ticket document.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket or ticket document.", ex.getMessage());
                    break;
            }
            throw ex;
        }

        List<Map<String, String>> attachmentList = new ArrayList<Map<String, String>>();

        if (null != ticketDocumentList && !ticketDocumentList.isEmpty()) {
            for (TkmDocuments tkmDocuments : ticketDocumentList) {
                try {
                    Content content = ecmService.getContentById(tkmDocuments
                            .getDocumentPath());
                    Map<String, String> attachmentMap = new HashMap<String, String>();
                    attachmentMap.put("DocName", content.getOriginalFileName());
                    attachmentMap
                            .put("DocPath", tkmDocuments.getDocumentPath());
                    attachmentMap.put("DocId",
                            String.valueOf(tkmDocuments.getId()));
                    if(content.getModifiedDate() != null){
                        attachmentMap.put("UpdatedDate", content.getModifiedDate()
                                .toString());
                    }

                    if (user.getId() == tkmDocuments.getUserId()) {
                        attachmentMap.put("DocUploader", TRUE);
                    } else {
                        attachmentMap.put("DocUploader", FALSE);
                    }
                    long docSize = 0;
                    if (null != content.getCustomAttributeList().get(
                            "contentSize")) {
                        docSize = Long.parseLong(content
                                .getCustomAttributeList().get("contentSize"));
                    }
                    attachmentMap.put("DocSize", readableFileSize(docSize));
                    attachmentList.add(attachmentMap);
                    sortListOnUpdatedDate(attachmentList);

                } catch (ContentManagementServiceException e) {
                    LOGGER.error(
                            "Error is getting attachment content at ECM service ",
                            e);
                }catch (Exception e) {
                    LOGGER.error(
                            "document retreived from ECM doesn't have required rendering information",
                            e);
                }
            }
            model.addAttribute("DOC_LIST", attachmentList);
        }
        String uploadFail = (String) request.getSession().getAttribute(
                UPLOAD_FAILURE);
        if (null != uploadFail
                && uploadFail.contains("File could not be uploaded")) {

            model.addAttribute(UPLOAD_FAILURE, uploadFail);
            request.getSession().removeAttribute(UPLOAD_FAILURE);
        }
        model.addAttribute(TICKET_STATUS, tkmTicket.getStatus());

        try{
            // Quick action bar
            TkmQuickActionDto quickAction = ticketMgmtUtils
                    .getDetailsForQuickActionBar(IntEncTicketId);
            model.addAttribute("quickAction", quickAction);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving quick action bar details.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving quick action bar details.", ex.getMessage());
                    break;
            }
        }

        addViewMemberAccountDetails(model, request, tkmTicket);

        Set<String> userPermissions = getUserPermissions(request, user);
        model.addAttribute("isAssignAllowed",
                userPermissions.contains("TKM_ASSIGN_ALLOWED"));

        return "ticketmgmt/ticket/ticketAttachment";

    }

    private void sortListOnUpdatedDate(List<Map<String, String>> attachmentList) {
        Collections.sort(attachmentList, new Comparator<Map<String, String>>() {
            @Override
            public int compare(Map<String, String> o1, Map<String, String> o2) {
                DateFormat df = new SimpleDateFormat(
                        "EEE MMM dd kk:mm:ss z yyyy", Locale.ENGLISH);
                Date dt1 = null;
                Date dt2 = null;
                try {
                    dt1 = df.parse(o1.get("UpdatedDate"));
                    dt2 = df.parse(o2.get("UpdatedDate"));
                } catch (ParseException e) {
                    LOGGER.error(
                            "Error While comparing dates of attachments in ticketlistcontroller",
                            e);
                }

                return (dt2 != null) ? dt2.compareTo(dt1) : 0;
            }
        });
    }

    private String readableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[] { "B", "KB", "MB", "GB", "TB" };
        int digitGroups = (int) (Math.log10(size) / Math.log10(BYTE));
        return new DecimalFormat("#,##0.#").format(size
                / Math.pow(BYTE, digitGroups))
                + " " + units[digitGroups];
    }

    @RequestMapping(value = "/ticketmgmt/ticket/viewDocument", method = RequestMethod.GET)
    @PreAuthorize(TKM_VIEW_TICKET)
    public void getAttachment(@RequestParam String documentId, Model model,
                              HttpServletResponse response) {
        // LOGGER.info("################### "+documentId);
        byte[] attachment = null;
        String errorMsg = "";
        Content content = null;
        try {
            attachment = ecmService.getContentDataById(documentId);
            content = ecmService.getContentById(documentId);
            response.setContentType(content.getMimeType());
            response.setContentLength(attachment.length);
            String fileName = content.getOriginalFileName();
            fileName = fileName.replaceAll(UNSECURE_CHARS_REGEX,
                    StringUtils.EMPTY);
            fileName = XssHelper.stripXSS(fileName);
            response.setHeader("Content-Disposition", "inline;filename="
                    + fileName);
            FileCopyUtils.copy(attachment, response.getOutputStream());
        } catch (ContentManagementServiceException e) {
            errorMsg = e.getMessage();
            model.addAttribute(ERROR_MSG, errorMsg);
            LOGGER.error("", e);
        } catch (IOException e) {
            errorMsg = e.getMessage();
            model.addAttribute(ERROR_MSG, errorMsg);
            LOGGER.error("", e);
        }

    }

    @RequestMapping(value = { "/ticketmgmt/ticket/getAllUsersForQueue" }, method = RequestMethod.GET)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getAllUsersForQueue(@RequestParam String qName,
                                      HttpServletResponse response) throws InvalidUserException {
        response.setContentType(APP_JSON_CONTENT);
        // List<AccountUser> requesterList = null;
        List<Object[]> requesterList = null;
        String queueName = null;
        try {

            if (StringUtils.isNotEmpty(qName)) {
                queueName = qName.trim();
                if ("Verification Workgroup".equalsIgnoreCase(queueName)) {
                    queueName = "Document Verification Workgroup";
                }
            }

            requesterList = ticketMgmtUtils.getUsersForQueueForReassign(queueName);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving users for queue reassigning.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving  users for queue reassigning.", ex.getMessage());
                    break;
            }
            return ex.getMessage();
        }

        Map<String, String> mapRequester = new LinkedHashMap<String, String>();
        for (Object[] accountUser : requesterList) {
            StringBuffer details = new StringBuffer();
            details.append(accountUser[1]);
            // details.append(" ");
            // details.append(accountUser.getLastName());
            mapRequester.put(ghixJasyptEncrytorUtil
                            .encryptStringByJasypt(String.valueOf(accountUser[0])),
                    details.toString());
        }

        return gson.toJson(mapRequester);

    }

    @RequestMapping(value = "/ticketmgmt/ticket/reassignTicket", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String reassignTicket(@RequestParam("taskId") String taskId,
                                 HttpServletRequest request,
                                 @RequestParam("queueName") String queueName,
                                 @RequestParam("assignee") String assignee,
                                 @RequestParam("ticketId") String ticketId,
                                 @RequestParam(required = false) String action)
            throws InvalidUserException {
        String errorMsg = (request.getSession().getAttribute(ERROR_MSG) == null || request
                .getSession().getAttribute(ERROR_MSG).equals("")) ? ""
                : (String) request.getSession().getAttribute(ERROR_MSG);
        try {
            ticketId = StringUtils.isNumeric(ticketId) ? ticketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
            if (StringUtils.isNotEmpty(assignee)) {
                assignee = StringUtils.isNumeric(assignee) ? assignee
                        : ghixJasyptEncrytorUtil
                        .decryptStringByJasypt(assignee);
            }
            taskId = StringUtils.isNumeric(taskId) ? taskId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(taskId);

            queueName = StringUtils.isNumeric(queueName) ? queueName
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(queueName);
            if (StringUtils.isNotEmpty(action)
                    && action.equalsIgnoreCase("Assign")) {
                ticketMgmtUtils.assignTicket(taskId, queueName, assignee,
                        ticketId);
                return errorMsg;
            }
            ticketMgmtUtils.reassignTicket(taskId, queueName, assignee,
                    ticketId);
            ticketMgmtUtils.reassignTicket(taskId, queueName, assignee,
                    ticketId);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in assign/reassign ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in assign/reassign ticket.", ex.getMessage());
                    break;
            }
            errorMsg= ex.getMessage();
        }

        return errorMsg;
    }

    /**
     *
     * @author hardas_d
     * @return String
     * @param ticketId
     * @param comment
     * @return Empty string if success or error message.
     */
    @RequestMapping(value = "/ticketmgmt/ticket/cancelticket", method = { RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String cancelTicket(@RequestParam(TICKET_ID) String ticketId,
                               @RequestParam("comment") String comment,
                               @RequestParam("commentLength") String commentLength,
                               HttpServletRequest request) throws InvalidUserException {

        capRequestValidatorUtil.validateEncryptedId(ticketId);
        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        // LOGGER.info("Cancel ticket call initialized for ticket id: " +
        // StrEncTicketId);
        String commentsText = StringEscapeUtils.unescapeHtml(comment);
        // String response = "Invalid params";
        AccountUser user = userService.getLoggedInUser();
        String errorMsg = (request.getSession().getAttribute(ERROR_MSG) == null || request
                .getSession().getAttribute(ERROR_MSG).equals("")) ? ""
                : (String) request.getSession().getAttribute(ERROR_MSG);

        try {
            boolean isCommentValid = TicketMgmtUtils.isCommentValid(
                    commentsText, commentLength);
            if (!isCommentValid) {
                errorMsg = "Cannot Process, invalid comment data. Please try again.";
                return errorMsg;
            }

            ticketMgmtUtils.cancelTicket(Integer.valueOf(StrEncTicketId),
                    commentsText, user.getId());
            LOGGER.info("Ticket cancelled successfully");
        } catch (NumberFormatException e) {
            errorMsg = e.getMessage();
            LOGGER.error("Exception whicn cancelling ticket:", e);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in cancel ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in cancel ticket.", ex.getMessage());
                    break;
            }
            errorMsg= ex.getMessage();
        }
        return errorMsg;
    }

    /**
     * @author kaul_S
     * @since 31st June 2013
     * @param ticketId
     * @param response
     * @return Ajax call to fetch TkmTicketTasks on the basis of TicketId
     */
    @RequestMapping(value = { "/ticketmgmt/ticket/getticketTaskList/{encTicketId}" }, method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_VIEW_TICKET)
    public String getTicketTaskList(
            @PathVariable("encTicketId") String encTicketId, Model model,
            HttpServletRequest request) throws InvalidUserException {
        capRequestValidatorUtil.validateGetTicketTaskList(encTicketId);
        List<Integer> userQueueId = null;
        AccountUser user = userService.getLoggedInUser();
        List<TkmTicketTasks> tkmTicketTaskList = null;
        TkmTickets tkmTicket = null;
        boolean isExtendedView = false;
        String StrEncTicketId = (StringUtils.isNumeric(encTicketId)) ? encTicketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(encTicketId);
        Integer IntEncTicketId = Integer.parseInt(StrEncTicketId);

        try {

            Set<String> userPermissions = getUserPermissions(request, user);
            isExtendedView = isExtendedViewAllowed(userPermissions);

            if (isExtendedView) {
                List<TkmQueues> queuelist = ticketMgmtUtils.getQueuesList();
                userQueueId = new ArrayList<Integer>();

                for (TkmQueues tkmQueues : queuelist) {
                    userQueueId.add(tkmQueues.getId());
                }
            } else {
                userQueueId = ticketMgmtUtils.getQueuesForUser(String
                        .valueOf(user.getId()));
            }

            tkmTicketTaskList = ticketMgmtUtils
                    .getTicketTaskList(IntEncTicketId.toString());
            tkmTicket = ticketMgmtUtils.getTkmTicketsById(IntEncTicketId);

            // Quick action bar
            TkmQuickActionDto quickAction = ticketMgmtUtils
                    .getDetailsForQuickActionBar(IntEncTicketId);
            model.addAttribute("quickAction", quickAction);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error.Unable to get data from TKM module",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error.Unable to get data from TKM module", ex.getMessage());
                    break;
            }
            throw ex;
        }

        addViewMemberAccountDetails(model, request, tkmTicket);

        List<Map<String, Object>> taskDataList = prepareTaskListMap(
                tkmTicketTaskList, user, isExtendedView, userQueueId);
        Set<String> userPermissions = getUserPermissions(request, user);
        model.addAttribute("isAssignAllowed",
                userPermissions.contains("TKM_ASSIGN_ALLOWED"));
        model.addAttribute("Task_Data", taskDataList);
        model.addAttribute(TICKET_ID, IntEncTicketId.toString());
        model.addAttribute("Subject",
                (tkmTicket != null) ? tkmTicket.getSubject() : "");
        model.addAttribute(TICKET_STATUS,
                (tkmTicket != null) ? tkmTicket.getStatus() : "");
        model.addAttribute("QUEUE_ID", userQueueId);
        model.addAttribute("tkmTicketsObj", tkmTicket);

        return "ticketmgmt/ticket/ticketTasks";
    }

    private List<Map<String, Object>> prepareTaskListMap(
            List<TkmTicketTasks> tkmTicketTaskList, AccountUser user,
            boolean isExtendedView, List<Integer> userQueueId) {
        List<Map<String, Object>> taskDataList = new ArrayList<Map<String, Object>>();
        for (TkmTicketTasks tkmTicketTasks : tkmTicketTaskList) {
            Map<String, Object> taskDataMap = new HashMap<String, Object>();

            taskDataMap.put("TaskName",
                    String.valueOf(tkmTicketTasks.getTaskName()));
            taskDataMap.put("Status", tkmTicketTasks.getStatus());
            taskDataMap.put("Assignee",
                    (tkmTicketTasks.getAssignee() == null ? "" : tkmTicketTasks
                            .getAssignee().getFullName()));
            taskDataMap.put(ACTIVITI_TASK_ID,
                    tkmTicketTasks.getActivitiTaskId());
            taskDataMap.put(TASK_ID, tkmTicketTasks.getTaskId().toString());
            taskDataMap.put(
                    "UserId",
                    (tkmTicketTasks.getAssignee() == null ? "" : String
                            .valueOf(tkmTicketTasks.getAssignee().getId())));
            taskDataMap.put("ProcessInstanceId",
                    tkmTicketTasks.getProcessInstanceId());
            taskDataMap.put("Comments", tkmTicketTasks.getComments());
            taskDataMap.put("Subject", tkmTicketTasks.getTicket().getSubject());

            setTaskUser(tkmTicketTasks, taskDataMap, user, isExtendedView);

            setTaskDates(tkmTicketTasks, taskDataMap);

            if (null != tkmTicketTasks.getQueue() && null != userQueueId) {
                taskDataMap.put("TaskQueue", userQueueId
                        .contains(tkmTicketTasks.getQueue().getId()) ? TRUE
                        : FALSE);
            }

            taskDataList.add(taskDataMap);
        }
        return taskDataList;
    }

    private void setTaskUser(TkmTicketTasks tkmTicketTasks,
                             Map<String, Object> taskDataMap, AccountUser user,
                             boolean isExtendedView) {
        if (null != tkmTicketTasks.getAssignee() && null != user) {
            if (isExtendedView) {
                taskDataMap.put("TaskUser", TRUE);
            } else {
                taskDataMap.put("TaskUser", tkmTicketTasks.getAssignee()
                        .getId() == user.getId() ? TRUE : FALSE);
            }
        }

    }

    private void setTaskDates(TkmTicketTasks tkmTicketTasks,
                              Map<String, Object> taskDataMap) {
        /*
         * SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
         * String startDate = null; String endDate = null;
         *
         * if (tkmTicketTasks.getStartDate() != null) { startDate =
         * formatter.format(tkmTicketTasks.getStartDate()); }
         *
         * if (tkmTicketTasks.getEndDate() != null) { endDate =
         * formatter.format(tkmTicketTasks.getEndDate()); }
         */
        taskDataMap.put(
                "StartDate",
                (tkmTicketTasks.getStartDate() == null) ? "" : tkmTicketTasks
                        .getStartDate());
        taskDataMap.put("EndDate", (tkmTicketTasks.getEndDate() == null) ? ""
                : tkmTicketTasks.getEndDate());
    }

    @RequestMapping(value = "/ticketmgmt/ticket/managequeue", method = {
            RequestMethod.POST, RequestMethod.GET })
    @PreAuthorize("hasPermission(#model, 'TKM_LIST_QUEUE')")
    public String showManageQueue(Model model, HttpServletRequest request)
            throws InvalidUserException {

        List<TkmQueues> queuesList = null;
        try {
            queuesList = ticketMgmtUtils.getQueuesList();
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving queue list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving queue list", ex.getMessage());
                    break;
            }
            throw ex;
        }


        model.addAttribute("queueList", queuesList);
        return "ticketmgmt/ticket/managequeue";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/getqueueusers", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getQueueUsers(@RequestParam String queueName,
                                HttpServletResponse response) throws InvalidUserException {

        response.setContentType(APP_JSON_CONTENT);

        List<AccountUser> users = null;
        List<AccountUser> existingUser = null;
        try {
            users = userService.getAdminAndCSRRoleUsers();
            existingUser = ticketMgmtUtils.getAllUsersForQueue(queueName);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error.Unable to get the valid data",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error.Unable to get the valid data", ex.getMessage());
                    break;
            }
            return ex.getMessage();
        }

        Map<Integer, String> usersMap = getIdNameMap(users);
        Map<Integer, String> existingUserMap = getIdNameMap(existingUser);

        for (AccountUser accountUser : existingUser) {
            if (usersMap.containsKey(accountUser.getId())) {
                usersMap.remove(accountUser.getId());
            }
        }

        Map<String, Map<Integer, String>> data = new HashMap<String, Map<Integer, String>>();
        data.put("otherUsers", usersMap);
        data.put("existingUsers", existingUserMap);

        return gson.toJson(data);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/updatequeueusers", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String updateQueueUsers(@RequestParam String queueId,
                                   @RequestParam String users, HttpServletResponse response)
            throws InvalidUserException {

        Integer id = null;
        List<Integer> userList = new ArrayList<Integer>();
        String errorMsg = StringUtils.EMPTY;

        try {
            id = Integer.parseInt(queueId);
        } catch (NumberFormatException e) {
            LOGGER.error("Unable to parse the queueId", e);
            return e.getMessage();
        }

        response.setContentType(APP_JSON_CONTENT);
        String userJSON = StringEscapeUtils.unescapeHtml(users);

        try {
            @SuppressWarnings(UNCHECKED)
            List<String> fromJson = gson.fromJson(userJSON, List.class);
            for (String strUserId : fromJson) {
                userList.add(Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(strUserId)));
            }
        } catch (JsonSyntaxException e) {
            LOGGER.error("Unable to parse user list", e);
            return e.getMessage();
        }

        try {
            ticketMgmtUtils.updateQueue(id, userList);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error.Unable to update queue",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error. Unable to update queue.", ex.getMessage());
                    break;
            }
            errorMsg = ex.getMessage();
        }
        catch (Exception e) {
            LOGGER.error("Unable to update the queue", e);
            return e.getMessage();
        }
        return errorMsg;
    }

    private Map<Integer, String> getIdNameMap(List<AccountUser> userList) {
        Map<Integer, String> usersMap = new LinkedHashMap<Integer, String>();

        for (AccountUser accountUser : userList) {
            StringBuffer details = new StringBuffer();
            details.append(accountUser.getFirstName());
            details.append(" ");
            details.append(accountUser.getLastName());
            usersMap.put(accountUser.getId(), details.toString());
        }

        return usersMap;
    }

    private Map<String, String> getEncryptedIdNameRoleMap(
            List<Object[]> userList) {

        Map<String, String> usersMap = new LinkedHashMap<String, String>();
        for (Object[] accountUser : userList) {
            StringBuffer details = new StringBuffer();
            details.append("{\"name\":\"" + accountUser[1] + "\",\"role\":\""
                    + accountUser[2] + "\"}");
            String encId = ghixJasyptEncrytorUtil.encryptStringByJasypt(String
                    .valueOf(accountUser[0]));
            usersMap.put(encId, details.toString());
        }
        return usersMap;

    }

    private Map<Integer, String> getIdNameMapForUser(List<Object[]> userList) {
        Map<Integer, String> usersMap = new LinkedHashMap<Integer, String>();
        for (Object[] accountUser : userList) {
            Integer userId = (Integer) accountUser[0];
            usersMap.put(userId, (String) accountUser[1]);
        }
        return usersMap;
    }

    private boolean isExtendedViewAllowed(Set<String> userPermissions) {
        return !userPermissions.isEmpty()
                && userPermissions.contains("TKM_EXTENDEDLIST_TICKET");
    }

    private boolean isSimplifiedView(Set<String> userPermissions) {
        return !userPermissions.isEmpty()
                && userPermissions.contains("TKM_SIMPLIFIEDVIEW_TICKET");
    }

    private Set<String> getUserPermissions(HttpServletRequest request,
                                           AccountUser user) {
    	Set<String> perms = new HashSet<>();
    	if(user == null) {
    		return perms;
    	}
        String userActiveRoleName = request.getSession()
                .getAttribute("userActiveRoleName").toString();
        if(userActiveRoleName == null) {
        	LOGGER.info("User Active Role name not available, using default role");
        	Role def = userService.getDefaultRole(user);
        	return this.permissionEvaluator.getPermissionsForRole(def.getName());
        }
        return user.getUserPermission(userActiveRoleName);
    }

    private Set<String> getWorkFlowPermissions(Set<String> permissions) {
        Set<String> workFlowPermission = new HashSet<>();
        for (String permission : permissions) {
            if (permission.contains(TKM_WORKFLOW_PERMISSION_SUBSTRING)) {
                workFlowPermission.add(permission);
            }
        }
        return workFlowPermission;
    }

    @RequestMapping(value = "ticketmgmt/ticket/uploadTicketAttachments", method = RequestMethod.POST)
    public String saveAppealFiles(@RequestParam(TICKET_ID) String ticketId,
                                  @RequestParam("files[]") MultipartFile file,
                                  HttpServletRequest request) throws InvalidUserException {

        capRequestValidatorUtil.validateSaveAppealFiles(ticketId,
                file.getOriginalFilename());

        String documentId = "";
        AccountUser user = userService.getLoggedInUser();
        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        try {
            List<TkmDocuments> documents = ticketMgmtUtils
                    .getTicketDocuments(StrEncTicketId);

            if (null != documents) {
                for (TkmDocuments tkmDocuments : documents) {
                    Content content = ecmService.getContentById(tkmDocuments
                            .getDocumentPath());
                    if (null != content
                            && content.getOriginalFileName().equals(
                            file.getOriginalFilename())) {
                        request.getSession()
                                .setAttribute(UPLOAD_FAILURE,
                                        "File could not be uploaded. File already exists");
                        return "redirect:/ticketmgmt/ticket/getTicketDocuments/"
                                + ticketId;
                    }
                }
            }
            String relativePath = ticketId + "/ticketmgmt/ticket/attachments";
            documentId = ecmService.createContent(relativePath,
                    getFileName(file.getOriginalFilename()), file.getBytes()
                    , ECMConstants.TicketMgmt.DOC_CATEGORY, ECMConstants.TicketMgmt.ATTACHMENT, null);
            if (!documentId.isEmpty()) {
                TkmTicketsRequest ticketsRequest = new TkmTicketsRequest();
                ticketsRequest.setTicketId(Integer.valueOf(StrEncTicketId));
                ticketsRequest.setCreatedBy(user.getId());
                ArrayList<String> docLinks = new ArrayList<String>();
                docLinks.add(documentId);
                ticketsRequest.setDocumentLinks(docLinks);
                ticketMgmtUtils.saveTicketDocuments(ticketsRequest);
            }

            // LOGGER.info("APPEAL DOCUMENT SAVED:"+documentId);
        } catch (RestfulResponseException ex) {
            request.getSession().setAttribute(UPLOAD_FAILURE,UPLOAD_FILE_TYPE_ERROR);
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Interenal server ERROR IN APPEAL FILE UPLOAD:", ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown ERROR IN APPEAL FILE UPLOAD:", ex.getMessage());
                    break;
            }

        } catch (Exception e) {
            LOGGER.error("ERROR IN APPEAL FILE UPLOAD:", e);
            request.getSession().setAttribute(UPLOAD_FAILURE,UPLOAD_FILE_TYPE_ERROR);

        }
        return "redirect:/ticketmgmt/ticket/getTicketDocuments/" + ticketId;
    }

    private String getFileName(String originalFileName) {

        /*
         * LOGGER.info("Old file name : " + originalFileName); if
         * (StringUtils.isEmpty(originalFileName)) { return originalFileName; }
         *
         * String[] file = originalFileName.split("\\.");
         * LOGGER.info("Length : " + file.length); LOGGER.info("Name : " +
         * file[0]); LOGGER.info("Extension : " + file[1]); String newFileName =
         * null; if (file != null && file.length >= 2) { newFileName = file[0] +
         * "." + file[1].toLowerCase(); }
         *
         * LOGGER.info("New file name : " + newFileName); return newFileName;
         */

        return (FilenameUtils.getBaseName(originalFileName) + "." + FilenameUtils
                .getExtension(originalFileName));
    }

    /**
     * @author Sharma_k
     * @since 08th August 2013
     * @param assigneeName
     * @param response
     * @return Jira Id: HIX-14168 Manage ticket
     * @throws InvalidUserException
     */
    @RequestMapping(value = "/ticketmgmt/ticket/fetchassigneelist", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String fetchAssigneeList(@RequestParam String assigneeName,
                                    HttpServletResponse response) throws InvalidUserException {
        String jsonString = "";
        try {
            response.setContentType(APP_JSON_CONTENT);

            if (!StringUtils.isEmpty(assigneeName)) {
                List<TkmUserDTO> tkmUserDTOs = ticketMgmtUtils
                        .getTicketTaskByAssignee(assigneeName);
                Map<String, String> mapRequester = new HashMap<String, String>();
                for (TkmUserDTO tkmUserDTO : tkmUserDTOs) {
                    mapRequester.put(String.valueOf(tkmUserDTO.getUserId()),
                            tkmUserDTO.getFullName());
                }
                jsonString = gson.toJson(mapRequester);
            }
        } catch (RestfulResponseException ex) {
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in fetching assignee  list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in fetching assignee  list.",ex.getMessage());
                    break;
            }
            return ex.getMessage();
        }
        return jsonString;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/deleteTicketAttachments", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String deleteTicketAttachments(@RequestParam String documentId,
                                          @RequestParam String documentPath, HttpServletResponse response,
                                          HttpServletRequest request) throws InvalidUserException {

        String fileName = null;
        String errorMsg = StringUtils.EMPTY;
        try {
            Content content = ecmService.getContentById(documentPath);

            if (null != content) {
                fileName = content.getOriginalFileName();
                ecmService.deleteContent(documentPath);
            }

            ticketMgmtUtils.deleteDocumnet(documentId);
            request.getSession().setAttribute("ticketAttachmentDeleted",
                    "Document <b>" + fileName + "</b> deleted successfully");
        } catch (ContentManagementServiceException e) {
            e.getStackTrace();
            errorMsg = e.getMessage();
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in deleting document id= " + documentId,ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in deleting document id= " + documentId,ex.getMessage());
                    break;
            }
            errorMsg = ex.getMessage();
        }

        return errorMsg;
    }

    @RequestMapping(value = { "/ticketmgmt/ticket/getTicketActiveTasks" }, method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getTicketActiveTasks(@RequestParam String ticketId,
                                       HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);
        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        List<TkmQueues> tkmQueues = null;

        try{

            List<TkmTicketTasks> tkmTicketTasks = ticketMgmtUtils
                    .getTicketActiveTask(StrEncTicketId.toString());

            Map<String, String> mapRequester = new HashMap<String, String>();
            if (null != tkmTicketTasks && tkmTicketTasks.size() > 0) {

                mapRequester.put("Task_Status",
                        tkmTicketTasks.get(tkmTicketTasks.size() - 1)
                                .getStatus());

                mapRequester.put("Task_Id", ghixJasyptEncrytorUtil
                        .encryptStringByJasypt(tkmTicketTasks.get(0)
                                .getTaskId().toString()));
                tkmQueues = ticketMgmtUtils.getQueuesList();


                if (null != tkmQueues) {
                    for (TkmQueues tkmQueue : tkmQueues) {
                        mapRequester.put(ghixJasyptEncrytorUtil
                                .encryptStringByJasypt(String.valueOf(tkmQueue
                                        .getId())), tkmQueue.getName());
                    }
                }

                String string = gson.toJson(mapRequester);

                return string;

            }
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving queue list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving queue list", ex.getMessage());
                    break;
            }
            throw ex;
        }

        return null;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/issuers", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getIssuers(@RequestParam String roleName,
                             HttpServletResponse response) throws InvalidUserException {
        // LOGGER.info("Get the list of issuer for the role: " + roleName);
        response.setContentType(APP_JSON_CONTENT);
        AccountUser user = userService.getLoggedInUser();
        String strResp = "";
        List<Issuer> issuerList = null;
        List<Integer> moduleIdList = null;

        try {
            issuerList = issuerService.findAllIssuers();
            moduleIdList = userService.getModuleIdsByModuleName(roleName
                    .toLowerCase());
        } catch (Exception e) {
            LOGGER.error("Unable to get the employer list ", e);
            giMonitorService.saveOrUpdateErrorLog(null, new TSDate(),
                    Exception.class.getName(), e.getStackTrace().toString(),
                    user);
            return strResp;
        }

        Map<Integer, String> mapIssuers = new HashMap<Integer, String>();
        if (moduleIdList.size() > 0) {
            for (Issuer issuer : issuerList) {
                if (moduleIdList.contains(issuer.getId())) {
                    mapIssuers.put(issuer.getId(), issuer.getName());

                    moduleIdList.remove(moduleIdList.indexOf(issuer.getId()));
                    if (moduleIdList.isEmpty()) {
                        break;
                    }
                }
            }

            try {
                strResp = gson.toJson(mapIssuers);
            } catch (Exception e) {
                LOGGER.error("Unable to parse the employer map to JSON", e);
            }
        }

        return strResp;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/ticketSubject", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getTicketSubjects(HttpServletRequest request,
                                    HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);
        List<String> ticketSubjects = null;
        String subjectList = "";

        AccountUser user;
        try {
            user = userService.getLoggedInUser();
            Set<String> userPermissions = getUserPermissions(request, user);
            ticketSubjects = isExtendedViewAllowed(userPermissions) ? ticketMgmtUtils
                    .getTicketSubject(null) : ticketMgmtUtils
                    .getTicketSubject(user);
            subjectList = gson.toJson(ticketSubjects);
            return subjectList;
        } catch (InvalidUserException e) {
            LOGGER.error("Unable to Get Ticket Subject", e);
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket subjects.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket subjects", ex.getMessage());
                    break;
            }
        }

        return null;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/ticketNumber", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getTicketNumbers(HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);
        List<String> ticketNumber = null;
        String ticketNumberList = "";

        try {
            ticketNumber = ticketMgmtUtils.getTicketNumbers();
            ticketNumberList = gson.toJson(ticketNumber);
            return ticketNumberList;
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket numbers.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket numbers", ex.getMessage());
                    break;
            }
        }


        return null;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/ticketRequesters", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getTicketRequesters(HttpServletResponse response,
                                      @RequestParam String userText) {
        response.setContentType(APP_JSON_CONTENT);
        String requesterList = "";

        List<AccountUser> requestedBy = null;
        Map<String, String> requestedByMap = new HashMap<String, String>();
        try {
            requestedBy = ticketMgmtUtils.getRequesters(userText);
            for (AccountUser accountUser : requestedBy) {
                requestedByMap.put(String.valueOf(accountUser.getId()),
                        accountUser.getFullName());
            }
            requesterList = gson.toJson(requestedByMap);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving requester list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving requester list", ex.getMessage());
                    break;
            }
        }


        return requesterList;
    }

    @RequestMapping(value = "/template/uploadTemplate", method = RequestMethod.GET)
    public String uploadTemplate(Model model, HttpServletRequest request) {

        return "template/uploadTemplate";
    }

    @RequestMapping(value = "/template/uploadTemplate", method = RequestMethod.POST)
    public String saveTemplate(@RequestParam("files[]") MultipartFile file,
                               Model model, HttpServletRequest request) {
        boolean fileExists = true;
        Content content = null;
        String relativePath = "resources/"
                + request.getParameter("templateProfile")
                + "/notificationTemplate";
        try {
            content = ecmService.getContentByPath(relativePath + "/"
                    + file.getOriginalFilename());
        } catch (Exception e) {
            fileExists = false;
        }
        try {
            if (fileExists) {

                ecmService.updateContent(content.getContentId(),
                        file.getOriginalFilename(), file.getBytes());
                request.setAttribute(UPLOAD_MSG,
                        "Template Updated Successfully");

            } else {
                ecmService.createContent(relativePath,
                        file.getOriginalFilename(), file.getBytes()
                        , ECMConstants.TicketMgmt.DOC_CATEGORY, ECMConstants.TicketMgmt.TEMPLATE, null);
                request.setAttribute(UPLOAD_MSG,
                        "Template Uploaded Successfully");
            }

        } catch (ContentManagementServiceException e) {
            request.getSession().setAttribute(UPLOAD_MSG,
                    "Error Uploading Template" + e);
        } catch (IOException e) {
            request.getSession().setAttribute(UPLOAD_MSG,
                    "Error Uploading Template" + e);
        }
        return "template/uploadTemplate";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/verifydocument", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    public String verifyDocumentUpload(
            @RequestParam("files[]") MultipartFile file,
            HttpServletRequest request) {

        try {
            AccountUser user = userService.getLoggedInUser();
            String documentType = request.getParameter("documentType");
            String comment = request.getParameter("comments");
            String relativePath = "DocumentVerification/" + documentType + "/"
                    + user.getId() + "/" + new TSDate().getTime();/*
             * DocumentVerification
             * / <subtype>
             * _uid_timestamp
             */
            String documentId = ecmService.createContent(relativePath,
                    getFileName(file.getOriginalFilename()), file.getBytes()
                    , ECMConstants.TicketMgmt.DOC_CATEGORY, ECMConstants.TicketMgmt.DOCUMENT_VERFICATION, null);

            if (!StringUtils.isEmpty(documentId)) {
                // Set the Alfresco ECM document id
                ArrayList<String> docLinks = new ArrayList<String>();
                docLinks.add(documentId);

                // fill ticket data
                TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
                tkmTicketsRequest.setCreatedBy(user.getId());
                tkmTicketsRequest.setLastUpdatedBy(user.getId());
                tkmTicketsRequest.setSubject(documentType);
                tkmTicketsRequest.setComment(comment);
                tkmTicketsRequest.setCategory("Document Verification");
                tkmTicketsRequest.setType(documentType);
                tkmTicketsRequest.setUserRoleId(user.getDefRole().getId());
                tkmTicketsRequest.setRequester(user.getId());
                tkmTicketsRequest.setDocumentLinks(docLinks);

                // add document id for workflow
                Map<String, String> ticketFormPropertiesMap = new HashMap<String, String>();
                ticketFormPropertiesMap.put("docId", documentId);
                tkmTicketsRequest
                        .setTicketFormPropertiesMap(ticketFormPropertiesMap);

                // create a ticket
                ticketMgmtUtils.createNewTicket(tkmTicketsRequest);
            }

            // LOGGER.info("VERIFY DOCUMENT SAVED:"+documentId);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in adding new ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in adding new ticket.", ex.getMessage());
                    break;
            }
            throw ex;
        } catch (Exception e) {
            LOGGER.error("Unknown error in adding new ticket.", e.getMessage());
        }
        String prevPage = TICKET_LIST_MAPPING;
        return REDIRECT + prevPage;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/fetchTicketDocuments", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String fetchTicketDocuments(
            @RequestParam(value = "ticketId") String ticketId, Model model,
            HttpServletRequest request) throws InvalidUserException {

        Map<String, Object> returnMap = new HashMap<>();
        List<String> docPathList = new ArrayList<String>();
        Map<String, List<String>> formMap = new HashMap<>();
        try {
            String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
            TkmTicketsResponse tkmTicketResponse = ticketMgmtUtils
                    .getTicketDocumentsAndFormParams(StrEncTicketId.toString());

            for (TkmDocuments tkmDocuments : tkmTicketResponse
                    .getDocumentList()) {
                docPathList.add(tkmDocuments.getDocumentPath());
            }

            for (TaskFormProperties taskFormProperties : tkmTicketResponse
                    .getTkmTicketFomProperties()) {
                formMap.put(taskFormProperties.getId(),
                        taskFormProperties.getValues());
            }

            returnMap.put("docPathlist", docPathList);
            returnMap.put("formParams", formMap);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket document.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket document", ex.getMessage());
                    break;
            }
            throw ex;
        }

        return gson.toJson(returnMap);

    }

    /**
     *
     * @param ticketId
     * @return Ticket details in a map to be displayed in JSP
     * @throws InvalidUserException
     */
    @RequestMapping(value = "/ticketmgmt/ticket/sendSecureMsg", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String sendSecureMsg(@RequestParam(TICKET_ID) String ticketId) {
        capRequestValidatorUtil.validateEncryptedId(ticketId);
        Map<String, String> ticketData = new HashMap<String, String>();
        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);

        try {
            TkmTickets tkmTickets = ticketMgmtUtils.getTkmTicketsById(Integer
                    .parseInt(StrEncTicketId));
            ticketData.put("from", userService.getLoggedInUser().getUsername());
            ticketData.put("to", tkmTickets.getRole().getUsername());
            ticketData.put("subject", tkmTickets.getSubject());
            ticketData.put("num", tkmTickets.getNumber());
            ticketData.put("toUserId",
                    (String.valueOf(tkmTickets.getRole().getId())));
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket details.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket details", ex.getMessage());
                    break;
            }
            throw ex;
        }

        catch (Exception e) {
            LOGGER.error("Unable to get details from ticket", e);
            e.getMessage();
        }

        return gson.toJson(ticketData);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/fetchTicketTypes", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String fetchTicketTypes(Model model, HttpServletRequest request)
            throws InvalidUserException {
        getTicketTypeList();
        Map<String, List<String>> workflowType = new HashMap<>();
        for (String permission : mapTicketWorkflowByPermission.keySet()) {
            Map<String, List<String>> ticketTypeMap = mapTicketWorkflowByPermission
                    .get(permission);
            for (String ticketType : ticketTypeMap.keySet()) {
                if (workflowType.containsKey(ticketType)) {
                    List<String> ticketSubType = workflowType.get(ticketType);
                    workflowType.put(ticketType, this.getUniqueSubTypeList(
                            ticketTypeMap.get(ticketType), ticketSubType));
                } else {
                    workflowType.put(ticketType, ticketTypeMap.get(ticketType));
                }
            }
        }
        return gson.toJson(workflowType);

    }

    private List<String> getUniqueSubTypeList(List<String> oldList,
                                              List<String> newList) {
        HashSet<String> uniqueSubTypeList = new HashSet<>();
        uniqueSubTypeList.addAll(oldList);

        if (!oldList.isEmpty() && !newList.isEmpty())
            for (int i = 0; i < newList.size(); i++) {
                if (!uniqueSubTypeList.contains(newList.get(i))) {
                    oldList.add(newList.get(i));
                }
            }
        return oldList;

    }

    /**
     *
     * @author kaul_s
     * @param ticket
     *            Category
     * @param ticket
     *            Sub Type
     * @param ticket
     *            Id
     * @return This REST method will return all the merged form properties
     *         between the existing and new work flow for reclassify
     */
    @RequestMapping(value = "/ticketmgmt/ticket/fetchReclassfiedTargetProperties", method = {
            RequestMethod.POST, RequestMethod.GET })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String getReclassfiedTargetProperties(
            @RequestParam String ticketType,
            @RequestParam String ticketSubType, @RequestParam String ticketId,
            HttpServletResponse response, HttpServletRequest request) {
        response.setContentType(APP_JSON_CONTENT);
        List<TaskFormProperties> taskFormPropertiesList = null;
        try {

            if (StringUtils.isNotEmpty(ticketType)) {
                ticketType = ticketType.trim();
                if ("Verification".equalsIgnoreCase(ticketType)) {
                    ticketType = "Document Verification";
                }
            }
            String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                    : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);

            taskFormPropertiesList = ticketMgmtUtils
                    .getReclassfiedTargetProperties(ticketType, ticketSubType,
                            StrEncTicketId);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving task properties.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving task properties.", ex.getMessage());
                    break;
            }
        }

        return platformGson.toJson(taskFormPropertiesList);

    }

    /**
     *
     * @author kaul_s
     * @param TkmTicketsRequest
     * @return This REST method will reclassify the ticket to a new work flow
     */

    @RequestMapping(value = "/ticketmgmt/ticket/submitTicketReclassifyAction", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    @SuppressWarnings(UNCHECKED)
    public String postTicektReclassifyAction(@RequestParam String ticketType,
                                             @RequestParam String ticketSubType, @RequestParam String ticketId,
                                             @RequestParam String propertiesMap, HttpServletResponse response,
                                             HttpServletRequest request) {

        response.setContentType(APP_JSON_CONTENT);

        AccountUser user = null;
        List<TaskFormProperties> taskFormPropertiesList = null;

        // get the additional form properties entered by user for the new
        // workflow
        ticketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        Map map = new HashMap();
        String newTaskObject = StringEscapeUtils.unescapeHtml(propertiesMap);
        map = platformGson.fromJson(newTaskObject, map.getClass());
        Map userInputProperties = (Map) map.get("formPropertiesMap");


        try {
            user = userService.getLoggedInUser();
            TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
            tkmTicketsRequest.setId(Integer.valueOf(ticketId));
            tkmTicketsRequest.setLastUpdatedBy(user.getId());
            tkmTicketsRequest.setComment("Reclassified Ticket");

            if (StringUtils.isNotEmpty(ticketType)) {
                ticketType = ticketType.trim();
                if ("Verification".equalsIgnoreCase(ticketType)) {
                    ticketType = "Document Verification";
                }
            }

            tkmTicketsRequest.setCategory(ticketType);
            tkmTicketsRequest.setType(ticketSubType);
            tkmTicketsRequest.setTicketFormPropertiesMap(userInputProperties);
            taskFormPropertiesList = ticketMgmtUtils
                    .performReclassifyTicketWorkflow(tkmTicketsRequest);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in performing ticket reclassification.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket reclassification.", ex.getMessage());
                    break;
            }
            return ex.getMessage();
        }catch (Exception e) {
            LOGGER.info("Exception" + e);
        }

        return platformGson.toJson(taskFormPropertiesList);

    }

    /**
     * TO get the list for CREATED FOR IND -> module id , module name ->
     * cmr_household list OTHERS -> role id, account user
     *
     * @param response
     * @return
     */

    /**
     * roleType : userRole, // IND ROLE ID moduleName : userRoleName, //
     * 'INDIVIDUAL' userName : userName // 'USer text for name'
     *
     */
    @RequestMapping(value = "/ticketmgmt/ticket/createdFor", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getCreatedFor(@RequestParam String roleId,
                                @RequestParam String moduleName, @RequestParam String userText,
                                HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);
        String requesterList = "";

        try {
            requesterList = ticketMgmtUtils.getCreatedFor(roleId, moduleName,
                    userText);
            // for (AccountUser accountUser : requestedBy) {
            // requestedByMap.put(String.valueOf(accountUser.getId()),
            // accountUser.getFullName());
            // }
            // requesterList = gson.toJson(requestedByMap);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error.Unable to Get Ticket Requesters.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error.Unable to Get Ticket Requesters.", ex.getMessage());
                    break;
            }
        }
        return requesterList;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/archive", method = { RequestMethod.POST })
    @ResponseBody
    public String archiveTicket(@RequestParam(TICKET_ID) String ticketId,
                                HttpServletRequest request) throws InvalidUserException {

        capRequestValidatorUtil.validateEncryptedId(ticketId);

        String StrEncTicketId = (StringUtils.isNumeric(ticketId)) ? ticketId
                : ghixJasyptEncrytorUtil.decryptStringByJasypt(ticketId);
        // LOGGER.info("archive ticket call initialized for ticket id: " +
        // StrEncTicketId);
        // String response = "Invalid params";
        AccountUser user = userService.getLoggedInUser();
        String errorMsg = (request.getSession().getAttribute(ERROR_MSG) == null || request
                .getSession().getAttribute(ERROR_MSG).equals("")) ? ""
                : (String) request.getSession().getAttribute(ERROR_MSG);

        try {
            ticketMgmtUtils.archiveTicket(Integer.valueOf(StrEncTicketId),
                    user.getId());
            LOGGER.info("Ticket archived successfully");
        } catch (NumberFormatException e) {
            errorMsg = e.getMessage();
            LOGGER.error("Exception whicn archiving ticket:", e);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in archiving ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in archiving ticket.", ex.getMessage());
                    break;
            }
            errorMsg = ex.getMessage();
        }

        return errorMsg;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/getexistingqueueusers", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getExistingQueueUsers(@RequestParam String queueName,
                                        HttpServletResponse response) throws InvalidUserException {

        response.setContentType(APP_JSON_CONTENT);

        Map<String, String> existingUserMap = new HashMap<String, String>();
        if(StringUtils.isBlank(queueName)) {
            return platformGson.toJson(existingUserMap);
        }

        List<Object[]> existingUser = null;
        try {

            if (StringUtils.isNotEmpty(queueName)) {
                queueName = queueName.trim();
                if ("Verification Workgroup".equalsIgnoreCase(queueName)) {
                    queueName = "Document Verification Workgroup";
                }
            }

            existingUser = ticketMgmtUtils.getUsersForQueue(queueName);

        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving users for queue.queueName="+queueName,ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving users for queue.queueName="+queueName,ex.getMessage());
                    break;
            }
            return ex.getMessage();
        }


        existingUserMap = getEncryptedIdNameRoleMap(existingUser);
        return platformGson.toJson(existingUserMap);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/getworkgroupuserstoadd", method = RequestMethod.POST)
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public String getWorkgroupUsersToAdd(@RequestParam String username,
                                         HttpServletResponse response) throws InvalidUserException {

        response.setContentType(APP_JSON_CONTENT);

        AccountUser user = userService.getLoggedInUser();
        Map<Integer, String> users = null;
        Map<String, String> encUsers = new HashMap<String, String>();


        try {
            users = userService.getWorkgroupUsersToAdd(username);
            if(null!=users){
                Set<Entry<Integer, String>> entries = users.entrySet();
                for (Entry<Integer, String> entry : entries) {
                    encUsers.put(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(entry.getKey())),entry.getValue());
                }

            }
        } catch (Exception e) {
            LOGGER.error("Unable to get the valid data: ", e);
            GIMonitor gobj = giMonitorService.saveOrUpdateErrorLog(null,
                    new TSDate(), Exception.class.getName(), e.getStackTrace()
                            .toString(), user);
            GIErrorCode errorCodeObject = gobj.getErrorCode();
            return errorCodeObject.getMessage();
        }

        Map<String, Map<String, String>> data = new HashMap<String, Map<String, String>>();
        data.put("otherUsers", encUsers);

        return platformGson.toJson(data);
    }

    private List<Role> getRolesForTickets() {
        List<Role> roleList = new ArrayList<Role>();
        List<String> roleNameList = null;
        // get the role list from global property
        String roleListProp = DynamicPropertiesUtil
                .getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.ROLE_LIST);
        if (roleListProp != null) {
            Role tmpRole = null;
            roleNameList = Arrays.asList(roleListProp.split("\\s*,\\s*"));
            if (roleNameList != null && roleNameList.size() > 0) {
                for (String tmpRoleName : roleNameList) {
                    tmpRole = this.roleService.findRoleByName(tmpRoleName);
                    if (tmpRole != null) {
                        roleList.add(tmpRole);
                    }
                }
            }
        } else {
            roleList = this.roleService.findAll();
        }
        Collections.sort(roleList);
        return roleList;
    }

    /**
     * Call to Reopen the ticket. This will initiate the default workflow.
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/ticketmgmt/ticket/reopen", method = {
            RequestMethod.POST, RequestMethod.GET })
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String repoen(HttpServletRequest request,
                         HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);

        TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
        String errorMsg = StringUtils.EMPTY;

        try {
            String ticketId = (StringUtils.isNumeric(request
                    .getParameter("ticketId"))) ? request
                    .getParameter("ticketId") : ghixJasyptEncrytorUtil
                    .decryptStringByJasypt(request.getParameter("ticketId"));
            AccountUser user = userService.getLoggedInUser();
            tkmRequest.setTicketId(Integer.parseInt(ticketId));
            tkmRequest.setLastUpdatedBy((user != null) ? user.getId() : null);
            errorMsg = ticketMgmtUtils.reopen(tkmRequest);
        } catch (NumberFormatException e) {
            LOGGER.error("Reopen: Ticket Id is invalid", e);
        } catch (InvalidUserException e) {
            LOGGER.error("Reopen: Invalid user", e);
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in reopening ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in reopening ticket", ex.getMessage());
                    break;
            }
        }

        String errJson = platformGson.toJson(errorMsg);
        return errJson;
    }

    /**
     * Call to restart the ticket. This will re-initialize the ticket workflow.
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/ticketmgmt/ticket/restart", method = RequestMethod.POST)
    @PreAuthorize(TKM_EDIT_TICKET)
    @ResponseBody
    public String restart(HttpServletRequest request,
                          HttpServletResponse response) {
        response.setContentType(APP_JSON_CONTENT);

        TkmTicketsRequest tkmRequest = new TkmTicketsRequest();
        String errorMsg = StringUtils.EMPTY;

        try {
            String ticketId = (StringUtils.isNumeric(request
                    .getParameter("ticketId"))) ? request
                    .getParameter("ticketId") : ghixJasyptEncrytorUtil
                    .decryptStringByJasypt(request.getParameter("ticketId"));
            AccountUser user = userService.getLoggedInUser();
            tkmRequest.setTicketId(Integer.parseInt(ticketId));
            tkmRequest.setLastUpdatedBy((user != null) ? user.getId() : null);
            errorMsg = ticketMgmtUtils.restart(tkmRequest);
        } catch (NumberFormatException e) {
            LOGGER.error("Restart: Ticket Id is invalid", e);
        } catch (InvalidUserException e) {
            LOGGER.error("Restart: Invalid user", e);
        } catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in restarting ticket.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in restarting ticket", ex.getMessage());
                    break;
            }
        }

        String errJson = platformGson.toJson(errorMsg);
        return errJson;
    }

    @RequestMapping(value = "/ticketmgmt/ticket/listreport", method = {
            RequestMethod.GET, RequestMethod.POST })
    @PreAuthorize(TKM_LIST_TICKET)
    @ResponseBody
    public void downloadListReport(Model model, HttpServletRequest request,
                                   HttpServletResponse response) {
        LOGGER.info("======================== download list report () =======================");
        try {
            AccountUser user = userService.getLoggedInUser();
            Set<String> userPermissions = getUserPermissions(request, user);
            boolean isSimplifiedView = isSimplifiedView(userPermissions);

            Map<String, Object> searchCriteria = this.createSearchCriteria(
                    request, user, userPermissions, isSimplifiedView);
            searchCriteria.put("reportSearch", true);

            // Ticket Number Ticket Type Ticket Subtype Ticket Workgroup Subject
            // Ticket Details Ticket Status Requested By
            // Created For Priority Date of Request Due Date

            Map<String, Object> ticketListAndCnt = ticketMgmtUtils
                    .list(searchCriteria);
            @SuppressWarnings("unchecked")
            List<Object> ticketList = (List<Object>) ticketListAndCnt
                    .get("ticketList");

            // generate CSV
            List<String[]> data = new ArrayList<String[]>();
            String[] headers = new String[] { "Ticket Number", "Ticket Type",
                    "Ticket Subtype", "Ticket Workgroup", "Subject",
                    "Ticket Details", "Ticket Status", "Requested By",
                    "Created For", "Priority", "Date of Request", "Due Date" };
            data.add(headers);

            // get data in CSV
            for (Object dataRow : ticketList) {
                List<String> dataList = new ArrayList<>();
                List<?> rowArray = (List<?>) dataRow;
                dataList.add((String) rowArray.get(1)); // Ticket Number
                dataList.add((String) rowArray.get(2)); // Ticket Type
                dataList.add((String) rowArray.get(3)); // Ticket Subtype
                dataList.add((String) rowArray.get(4)); // Ticket Workgroup
                dataList.add((String) rowArray.get(5)); // Subject
                dataList.add((String) rowArray.get(6)); // Ticket Details
                dataList.add((String) rowArray.get(7)); // Ticket Status
                dataList.add((String) rowArray.get(8)); // Requested By
                dataList.add((String) rowArray.get(9)); // Created For
                dataList.add((String) rowArray.get(10)); // Priority
                dataList.add(convertTimestampToDate((Long)rowArray.get(11))); // Date of Request

                String dueDate = convertTimestampToDate((Long)rowArray.get(12));
                if (!StringUtils.isEmpty(dueDate) && dueDate.contains("1970")) {
                    dueDate = "NA";
                }

                dataList.add(dueDate); // Due Date

                String[] arrData = dataList
                        .toArray(new String[dataList.size()]);
                data.add(arrData);
            }

            String fileName = "ExportTicketData_" + TimeShifterUtil.currentTimeMillis()
                    + FILE_EXTENTION;
            String filePath = outputPath + File.separator + AGEING_REPORT;
            generateCsvReport(fileName, filePath, data, response);

        } catch (IOException e) {
            LOGGER.error("IOException occurred while downloading data");
            e.getMessage();
        }catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving ticket list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving ticket list", ex.getMessage());
                    break;
            }
        } catch (Exception e) {
            LOGGER.error("Error occurred while downloading data");
            e.getMessage();

        }


        LOGGER.info("CSV report download end");
    }

    public String convertTimestampToDate(Long timestamp) {
        Date date = new TSDate(timestamp);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("MM/dd/yyyy' 'HH:mm");
        return simpleDateFormat.format(date);
    }

    @RequestMapping(value = "/ticketmgmt/ticket/defaultqueue", method = {RequestMethod.POST, RequestMethod.GET })
    @PreAuthorize("hasPermission(#model, 'TKM_EDIT_QUEUE')")
    public String showDefaultQueue(Model model, HttpServletRequest request) {

        List<TkmQueues> queuesList = null;
        try {
            queuesList = ticketMgmtUtils.getQueuesList();
        } catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in retrieving queue list.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in retrieving queue list", ex.getMessage());
                    break;
            }
            throw ex;
        }

        model.addAttribute("queueList", platformGson.toJson(queuesList));
        return "ticketmgmt/ticket/defaultqueue";
    }

    @RequestMapping(value = "/ticketmgmt/ticket/savedefaultqueue", method = RequestMethod.POST)
    @ResponseBody
    @PreAuthorize("hasPermission(#model, 'TKM_EDIT_QUEUE')")
    public String saveDefaultQueue(@RequestParam String queueIds,
                                   HttpServletResponse response) throws InvalidUserException {

        String status = "FAILURE";
        response.setContentType(CONTENT_TYPE);

        try {
            if (queueIds != null) {
                ticketMgmtUtils.updateDefaultQueues(queueIds);
                status = "SUCCESS";
            }
        }
        catch(RestfulResponseException ex){
            switch(ex.getHttpStatus()){
                case INTERNAL_SERVER_ERROR:
                    LOGGER.error("Internal server error in updateDefaultQueues.",ex.getMessage());
                    break;
                default:
                    LOGGER.error("Unknown error in updateDefaultQueues", ex.getMessage());
                    break;
            }
            status = "FAILURE";
        }
        catch (Exception exception) {
            LOGGER.error("Error in saving ticket workgroups : "
                    + exception.getMessage() + "Cause : "
                    + exception.getCause());
            status = "FAILURE";
        }

        return platformGson.toJson(status);
    }
}
