package com.getinsured.eligibility.at.ref.service;



import static com.getinsured.eligibility.at.ref.service.LceProcessHandlerService.APP_EVENT_TYPE;

import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapter;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

/**
 * @author chopra_s
 * 
 */
public abstract class LceAllChangesHandlerBaseService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceAllChangesHandlerBaseService.class);
	private static final String APP_EVENT_ADD_APPLICANT = "ADD_APPLICANT";
	private static final String APP_EVENT_REMOVE_APPLICANT = "REMOVE_APPLICANT";
	@Autowired
	@Qualifier("planAvailabilityAdapter")
	private PlanAvailabilityAdapter planAvailabilityAdapter;

	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private AppEventService appEventService;
	
	@Autowired private Gson platformGson;
	
	@Autowired
	private UserService userService;
	
	    PlanAvailabilityAdapterResponse executePlanAvailability(LCEProcessRequestDTO lceProcessRequestDTO) {
		PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest = new PlanAvailabilityAdapterRequest();
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());
		planAvailabilityAdapterRequest.setHealthEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollees());
		planAvailabilityAdapterRequest.setDentalEnrollees(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollees());
		planAvailabilityAdapterRequest.setHealthEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getHealthEnrollmentId());
		planAvailabilityAdapterRequest.setDentalEnrollmentId(lceProcessRequestDTO.getEnrolledApplicationAttributes().getDentalEnrollmentId());
		planAvailabilityAdapterRequest.setSsapApplicationId(lceProcessRequestDTO.getCurrentApplicationId());

		PlanAvailabilityAdapterResponse response = null;
		try {
			response = planAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		if (response == null) {
			response = new PlanAvailabilityAdapterResponse();
			response.setStatus(GhixConstants.RESPONSE_FAILURE);
		}
		return response;
	}

	boolean executeEnrollment(long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees, PlanAvailabilityAdapterResponse planResponse) {
		String enrollmentResponse = GhixConstants.RESPONSE_FAILURE;
		try {
			enrollmentResponse = enrollmentUpdateAdapter.execute(ssapApplicationId, healthEnrollees, dentalEnrollees, planResponse);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		boolean status = false;
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse)) {
			status = true;
		}
		return status;
	}
	
	
	void processCSFailure(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		processMixed(currentApplication, true, lceProcessRequestDTO);
	}

	void processSingleSuccess(LCEProcessRequestDTO lceProcessRequestDTO) {
		final SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		SsapApplication currentApplication = loadCurrentApplication(lceProcessRequestDTO.getCurrentApplicationId());
		final String previousApplicationStatus = enrolledApplication.getApplicationStatus();
		LOGGER.info("PreviousApplicationStatus before closing previous application :" + previousApplicationStatus);
		closeSsapApplication(enrolledApplication);
		LOGGER.info("PreviousApplicationStatus after closing previous application :" + previousApplicationStatus);
		LOGGER.info("Current ApplicationStatus : " + previousApplicationStatus +" and ApplicationDentalStatus : "+ enrolledApplication.getApplicationDentalStatus());
		
		boolean isActive = isActiveEnrollment(currentApplication.getId());
		if(isActive) {
			
			// Update status in SsapApplication is already done Enrollment callback API
			String enrollmentType = super.getEnrollmentType(currentApplication.getId());
			if(StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.DENTAL)){ 
				LOGGER.info("IN ONLY DENTAL ENROLLMENT FLOW.");
				if(currentApplication.getApplicationDentalStatus() == null) {
					currentApplication.setApplicationDentalStatus(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
					updateSsapApplication(currentApplication);
				}
			}
			else{
				if(StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.HEALTH) || StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.BOTH)){
					LOGGER.info("IN HEALTH/BOTH ENROLLMENT FLOW.");
					if(currentApplication.getApplicationStatus() == null) {
						currentApplication.setApplicationStatus(previousApplicationStatus);
						updateSsapApplication(currentApplication);
					}
				}
			}
			
			logAddRemoveApplicantEvent(currentApplication,enrolledApplication,lceProcessRequestDTO);
			updateAllowEnrollment(currentApplication, Y);
			if (LceAllChangesHandlerDetermination.hasAddMember(currentApplication.getSsapApplicants())) {
				ssapApplicationEventService.setChangePlan(lceProcessRequestDTO.getCurrentApplicationId());
			}
			// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
			String stateCode = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if (stateCode.equalsIgnoreCase("MN")) {
				// check if its a demo flow and only zip/country is changed for entire HH then trigger EE060
				boolean checkForDemoChange = checkDemoChange(currentApplication);
				if(checkForDemoChange) {
					triggerAutomationCSNotice(currentApplication.getCaseNumber());
				}else {
					triggerAutomationAddRemoveNotice(currentApplication.getCaseNumber());	
				}	
			} else {
				// trigger as it is for other states
				triggerAutomationAddRemoveNotice(currentApplication.getCaseNumber());	
			}
		}else {
			handleDisenrollmentResponse(currentApplication, enrolledApplication, "SUCCESS");
		}
	}

	void processSingleFailure(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		if(checkIfEntireHouseholdElgLost(currentApplication))
		{
			handleDisenrollmentResponse(currentApplication, null, "FAILURE");
		}else {
			updateCurrentAppToER(currentApplication);
			updateAllowEnrollment(currentApplication, Y);
			triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);
	    }
	}
	
	private boolean checkIfEntireHouseholdElgLost(SsapApplication currentApplication) {
		boolean blnFlag = true;
        final List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
				if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, ssapApplicant.getEligibilityStatus())) {
					blnFlag = false;
					break;
				}
		}
		
        return blnFlag;
	}

	void processMixed(SsapApplication currentApplication, boolean email57, LCEProcessRequestDTO lceProcessRequestDTO) {
		updateCurrentAppToER(currentApplication);
		updateAllowEnrollment(currentApplication, Y);
		triggerEmailForMixedProcessing(currentApplication, email57, lceProcessRequestDTO);
	}
	
	void triggerEmailForMixedProcessing(SsapApplication currentApplication, boolean email57, LCEProcessRequestDTO lceProcessRequestDTO) {
		if (email57) {
			// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
			String stateCode = DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			if (stateCode.equalsIgnoreCase("MN")) {
				// check for AI/AN change
				boolean nativeAmericanChange = checkAIANChange(currentApplication,lceProcessRequestDTO.getEnrolledApplicationId());
				// check for Loss of CSR for a member and trigger EE065  new notice 
				boolean lossofCSR_ind = checkLossofCSR_IND(currentApplication,lceProcessRequestDTO.getEnrolledApplicationId());
				if(nativeAmericanChange) {
					triggerNativeAmericanStatusNotice(currentApplication);	
				} 				
				if(lossofCSR_ind) {
					triggerLossofCSRNotice(currentApplication);	
				}
				if(!nativeAmericanChange && !lossofCSR_ind) {
					triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);	
				}
				
			} else {
				// for all other states continue with the old notice
				triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);	
			}			
		} else {
			triggerSEPEmail(currentApplication);
		}
	}

	void handlePendingValidation(SsapApplication currentApplication) {
		updateAllowEnrollment(currentApplication, Y);
		updateCurrentAppToER(currentApplication);
		triggerSEPEmail(currentApplication);
	}

	private void logAddRemoveApplicantEvent(SsapApplication currentApplication, SsapApplication enrolledApplication, LCEProcessRequestDTO lceProcessRequestDTO){
		EventInfoDto eventInfoDto = new EventInfoDto();
		Map<String, String> mapEventParam = new HashMap<String, String>();
		
		//event type and category
		LookupValue lookupValue = null;
		
		String addedApplicantNames="";
		String removedApplicantNames="";
		for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				addedApplicantNames += " " + ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName() + ",";
			}
			else if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				removedApplicantNames += " " + ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName() + ",";
			}
			
		}
		if(addedApplicantNames.length() > 1){
			addedApplicantNames = addedApplicantNames.substring(0,addedApplicantNames.length()-1);
			lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_ADD_APPLICANT);
			mapEventParam.put("Added Applicants", addedApplicantNames);
		}
		else if(removedApplicantNames.length()> 1){
			removedApplicantNames = removedApplicantNames.substring(0,removedApplicantNames.length()-1);
			lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_REMOVE_APPLICANT);
			mapEventParam.put("Removed Applicants",removedApplicantNames);
		}
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(currentApplication.getCmrHouseoldId().intValue());
		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
		/**
		 *  this user is stored in app event table and is retrieved from dto -
		 *  lastupdatedUserId - user through which the api is called from web
		 *  else null user will go and system user will be stored as default in the table
		 */
	
		AccountUser accountUser = null;
		if(0 != lceProcessRequestDTO.getLastUpdatedUserId()) {
			// it is a valid user id so get the account user and pass it to the record method else pass null
			accountUser = userService.findById(lceProcessRequestDTO.getLastUpdatedUserId());
		}
		appEventService.record(eventInfoDto, mapEventParam,accountUser);
	}
	
	
	
	protected boolean sameCSLevel(SsapApplication currentApplication) {
		/*
		 * boolean sameCS = false; List<SsapApplicant> ssapApplicants =
		 * currentApplication.getSsapApplicants(); Set<String> csLevel = new
		 * HashSet<String>(); if(CollectionUtils.isNotEmpty(ssapApplicants)) {
		 * for(SsapApplicant ssapApplicant : ssapApplicants) {
		 * if("QHP".equalsIgnoreCase(ssapApplicant.getEligibilityStatus()) &&
		 * "Y".equalsIgnoreCase(ssapApplicant.getOnApplication()) &&
		 * "Y".equalsIgnoreCase(ssapApplicant.getApplyingForCoverage())) {
		 * csLevel.add(ssapApplicant.getCsrLevel()); } } } if(csLevel.size()==1 ||
		 * csLevel.size()==0) { sameCS = true; } return sameCS;
		 */
		//To allow automation in multiple CS levels too
		return true;
	}
	
	protected boolean isCustomGroupingAllowed() {
		boolean isCustomGrouping = false;
		if(StringUtils.isNotBlank(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CUSTOM_GROUPING))){
			String customGrouping = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CUSTOM_GROUPING);
			if("ON".equalsIgnoreCase(customGrouping)){
				isCustomGrouping = true;
			}
		}
		return isCustomGrouping;
	}
	
	protected void handleAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Date coverageStartDate, String isChangePlan) {
		
		boolean doAutomation = true;
		
		boolean isCustomGrouping = isCustomGroupingAllowed();
		
		if(isCustomGrouping && !sameCSLevel(currentApplication)){
			doAutomation = false;
		}
		
		if(doAutomation) {
		final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId(),coverageStartDate);
		if (aptcUpdate == null) {
			LOGGER.info("APTC redistribution failure - " + currentApplication.getId());
			processMixed(currentApplication, true, lceProcessRequestDTO);
		} else {
			final boolean status = ind71GCall(lceProcessRequestDTO.getEnrolledApplicationId(), lceProcessRequestDTO.getCurrentApplicationId(),currentApplication.getCoverageYear(), aptcUpdate,isChangePlan) ;
					if (status) {
						LOGGER.info("IND71G update successful for  " + currentApplication.getId());
						currentApplication.setEffectiveDate(EligibilityUtils.getDateWithoutTimeUsingFormat(aptcUpdate.getAptcEffectiveDate() != null ? aptcUpdate.getAptcEffectiveDate(): coverageStartDate));
						processSingleSuccess(lceProcessRequestDTO);
					} else {
						LOGGER.info("IND71G update failed for  " + currentApplication.getId());
						processSingleFailure(currentApplication, lceProcessRequestDTO);
					}
				}
			}else {
			LOGGER.info("Different CS level so no auto add - " + currentApplication.getId());
			processMixed(currentApplication, true, lceProcessRequestDTO);
		}
		
	}
}
