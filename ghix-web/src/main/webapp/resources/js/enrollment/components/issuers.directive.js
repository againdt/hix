(function() {
    'use strict';
    angular
        .module('reconciliationWorkbenchApp')
        .directive('issuerSelect', issuerSelect);

    function issuerSelect(){
        return {
            restrict: 'E',
            replace: true,
            scope: '=',
            template: '<select id="isserSelect"><option ng-repeat="issuer in issuers" value="{{issuer.hiosIssuerId}}" >{{issuer.issuerName}}</option></select>',
            controller: ["$scope", "$element", "$attrs", function (scope, element, attrs) {
            }]
        }
    }
})();