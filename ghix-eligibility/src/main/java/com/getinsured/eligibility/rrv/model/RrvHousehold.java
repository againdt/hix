package com.getinsured.eligibility.rrv.model;

import java.math.BigDecimal;
import java.util.List;

public class RrvHousehold {
    private BigDecimal householdId;
    private BigDecimal householdIncome;
    private List<RrvMember> householdMembers;
    private String applicationType;

    public BigDecimal getHouseholdId() {
        return householdId;
    }

    public void setHouseholdId(BigDecimal householdId) {
        this.householdId = householdId;
    }

    public BigDecimal getHouseholdIncome() {
        return householdIncome;
    }

    public void setHouseholdIncome(BigDecimal householdIncome) {
        this.householdIncome = householdIncome;
    }

    public List<RrvMember> getHouseholdMembers() {
        return householdMembers;
    }

    public void setHouseholdMembers(List<RrvMember> householdMembers) {
        this.householdMembers = householdMembers;
    }

    public String getApplicationType() {
        return applicationType;
    }

    public void setApplicationType(String applicationType) {
        this.applicationType = applicationType;
    }
}
