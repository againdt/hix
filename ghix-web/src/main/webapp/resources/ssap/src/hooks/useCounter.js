import { useState } from 'react';
const actionTypes = {
    INCREMENT: 'INCREMENT',
    DECREMENT: 'DECREMENT',
    MANUAL: 'MANUAL'
};

export const initialState = { count: 0 };

export const counterReducer = (state=initialState, action) => {
    switch (action.type) {
    case actionTypes.INCREMENT:
        return { count: state.count + 1 };
    case actionTypes.DECREMENT:
        return { count: state.count - 1 };
    case actionTypes.MANUAL:
        return action.payload;
    default:
        return state;
    }
};

export const useCounterReducer = (counterReducer, initialState) => {

    const [count, setCount] = useState(initialState);
    function dispatch(action) {
        const currentCount = counterReducer(count, action);
        //Add validation here
        setCount(currentCount);
    }
    return [count, dispatch];
};