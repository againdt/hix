package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.hix.indportal.dto.dm.application.Attestations;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.Computed;
import com.getinsured.hix.indportal.dto.dm.application.TaxHousehold;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxDependentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.TaxReturnFilingStatusCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;

import gov.niem.niem.niem_core._2.NumericType;

public class TaxHouseHoldMapper {
	
	private static Logger lOGGER = Logger.getLogger(TaxHouseHoldMapper.class);

	public TaxHouseHoldMapper(){
	}
	
	//PTF
	//spouse
	//household count
	//dependent link
	
	public TaxReturnType createTaxReturns(Attestations attestations, Computed computedData,String primaryFilerId,int coverageYear,AccountTransferRequestPayloadType returnObj, Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas) {
		lOGGER.debug(coverageYear);
		TaxReturnType taxReturnType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxReturnType();
		Map<String,AttestationsMember> members = attestations.getMembers();
		
		boolean isJointTaxFiling = false;
		String spousePersonId = null; 
		for(Entry<String,AttestationsMember> memberEntry : members.entrySet())
		{
			if("MARRIED_FILING_JOINTLY".equalsIgnoreCase(memberEntry.getValue().getFamily().getTaxReturnFilingStatusType()))
			{
				isJointTaxFiling = true;
				if(!AccountTransferUtil.getPersonTrackingNumber(memberEntry.getKey(), computedData.getMembers()).equals(primaryFilerId))
				{
					spousePersonId = AccountTransferUtil.getPersonTrackingNumber(memberEntry.getKey(), computedData.getMembers());
				}
			}
		}
		int householdSize = 0;
		if(computedData.getTaxHouseholds() != null)
			{
				householdSize = computedData.getTaxHouseholds().size();
			}
		/*if(isJointTaxFiling){
			householdSize++;
		}*/
		//tax return year
		GYear taxReturnYear = AccountTransferUtil.basicFactory.createGYear();
		XMLGregorianCalendar taxyear = AccountTransferUtil.getSystemDateYear();
		taxyear.setYear(coverageYear);
		taxReturnYear.setValue(taxyear);
		taxReturnType.setTaxReturnYear(taxReturnYear);
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean hasDependants = AccountTransferUtil.basicFactory.createBoolean();
		int dependents = householdSize;
		if(primaryFilerId != null )
		{
			dependents--;
		}
		if(spousePersonId != null)
		{
			dependents--;
		}
		boolean hasDependents = (dependents>0);
		hasDependants.setValue(hasDependents);
		taxReturnType.setTaxReturnIncludesDependentIndicator(hasDependants);
		
		TaxReturnFilingStatusCodeType taxReturnFilingStatusCodeType = AccountTransferUtil.hixTypeFactory.createTaxReturnFilingStatusCodeType();
		if(isJointTaxFiling){
			taxReturnFilingStatusCodeType.setValue("2");
		}else if(hasDependents){
			taxReturnFilingStatusCodeType.setValue("1");
		}else{
			taxReturnFilingStatusCodeType.setValue("4");
		}
		taxReturnType.setTaxReturnFilingStatusCode(taxReturnFilingStatusCodeType);

		// TaxReturn/TaxHousehold
		TaxHouseholdType taxHousehold = AccountTransferUtil.insuranceApplicationObjFactory.createTaxHouseholdType();
		BigDecimal taxHouseholdIncomeAmount = new BigDecimal(0);
		if(computedData.getTaxHouseholds() != null && computedData.getTaxHouseholds().size()>0 ){
			TaxHousehold taxHouseholdJson = computedData.getTaxHouseholds().values().iterator().next();
			if(taxHouseholdJson.getAnnualIncome() !=  null && taxHouseholdJson.getAnnualIncome().getAttestedAnnualTaxHouseholdIncomeAmount() != null) {
			taxHouseholdIncomeAmount = new BigDecimal(taxHouseholdJson.getAnnualIncome().getAttestedAnnualTaxHouseholdIncomeAmount()).setScale(2,RoundingMode.HALF_UP);
			}
		}
			
			IncomeType houseHoldIncome = AccountTransferUtil.hixCoreFactory.createIncomeType();
		    AmountType amount = AccountTransferUtil.niemCoreFactory.createAmountType();
		    amount.setValue(taxHouseholdIncomeAmount);
		    houseHoldIncome.setIncomeAmount(amount);
		    //Map<String, VerificationMetadataType> personVerificationMetadata = verificationMetadatas.get(1L);
            /*VerificationMetadataType incomeMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.ANNUAL_INCOME.value());
            if(incomeMetadata != null) {
                houseHoldIncome.getMetadata().add(incomeMetadata);
            } */
		    JAXBElement<IncomeType> jaxbhouseholdincome = AccountTransferUtil.insuranceApplicationObjFactory.createHouseholdIncome(houseHoldIncome);
		    taxHousehold.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI().add(jaxbhouseholdincome);
		    //com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType frequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
			//FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
			//frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType("Monthly"));
			//frequency.setFrequencyCode(frequencyCodeType);
			//houseHoldIncome.setIncomeFrequency(frequency);
			
			//com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType daysPerWeekNT = AccountTransferUtil.niemCoreFactory.createNumericType();
			//daysPerWeekNT.setValue(new BigDecimal(5));
			//houseHoldIncome.setIncomeDaysPerWeekMeasure(daysPerWeekNT);
			
			//com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType hoursPerPayPeriod = AccountTransferUtil.niemCoreFactory.createNumericType();
			//hoursPerPayPeriod.setValue(new BigDecimal(160));
			//houseHoldIncome.setIncomeHoursPerPayPeriodMeasure(hoursPerPayPeriod);
			
			//com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType hoursPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
			//hoursPerWeek.setValue(new BigDecimal(40));
			//houseHoldIncome.setIncomeHoursPerWeekMeasure(hoursPerWeek);
			
			//IncomeCategoryCodeType incomeCategoryCodeType = AccountTransferUtil.hixTypeFactory.createIncomeCategoryCodeType();
			//incomeCategoryCodeType.setValue(IncomeCategoryCodeSimpleType.WAGES);
			//houseHoldIncome.setIncomeCategoryCode(incomeCategoryCodeType);
			
			/*DateRangeType incomeEarnedDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
			DateType incomeEarnedEndDate = AccountTransferUtil.niemCoreFactory.createDateType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDate = AccountTransferUtil.basicFactory.createDate();
			endDate.setValue(AccountTransferUtil.getSystemDate());
			incomeEarnedEndDate.setDate(endDate);
			incomeEarnedDateRange.setEndDate(incomeEarnedEndDate);
			houseHoldIncome.setIncomeEarnedDateRange(incomeEarnedDateRange);*/
			
			//houseHoldIncome.setIncomeEmploymentDescriptionText(AccountTransferUtil.createTextType("Description"));
			
			//houseHoldIncome.setIncomeUnemploymentSourceText(AccountTransferUtil.createTextType("Description"));

			/*DateType incomeDate = AccountTransferUtil.niemCoreFactory.createDateType();

			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date incomeDate1 = AccountTransferUtil.basicFactory.createDate();
			incomeDate1.setValue(AccountTransferUtil.getSystemDate());
			incomeDate.setDate(incomeDate1);
			houseHoldIncome.setIncomeDate(incomeDate);*/
			
		if (StringUtils.isEmpty(primaryFilerId)) {
			primaryFilerId = AccountTransferUtil.getPersonTrackingNumber(
					attestations.getApplication().getContactMemberIdentifier(), computedData.getMembers());
		}
		taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent()
				.add(createTaxFiler("primary", primaryFilerId, returnObj));
		if (isJointTaxFiling && spousePersonId != null) {
			taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent()
					.add(createTaxFiler("spouse", spousePersonId, returnObj));
		}
		if (attestations.getHousehold() != null) {
			ArrayList<ArrayList<String>> taxRelationships = attestations.getHousehold().getTaxRelationships();
			if (taxRelationships != null) {
				for (ArrayList<String> dependent : taxRelationships) {
					if (dependent != null && dependent.size() == 2 && !(dependent.get(0).equals(dependent.get(2)))) {
						String dependentRelId = dependent.get(2);
						String dependentId = AccountTransferUtil.getPersonTrackingNumber(dependentRelId,
								computedData.getMembers());
						TaxDependentType depTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory
								.createTaxDependentType();
						ReferenceType dependantRefType = AccountTransferUtil.niemStructFactory.createReferenceType();
						dependantRefType.setRef(getPersonById(returnObj.getPerson(), dependentId));
						depTaxFilerType.setRoleOfPersonReference(dependantRefType);
						IdentificationType ssnIdType = createSSNIdentificationType(dependentId, returnObj);
						if (ssnIdType != null) {
							depTaxFilerType.setTINIdentification(ssnIdType);
						}
						JAXBElement<TaxDependentType> dependantTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory
								.createTaxDependent(depTaxFilerType);
						taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent().add(dependantTaxFilerType);
					}
				}
			}
		}
		QuantityType householdSizeQuantity= AccountTransferUtil.niemCoreFactory.createQuantityType();
		householdSizeQuantity.setValue(new BigDecimal(householdSize));
		taxHousehold.setHouseholdSizeQuantity(householdSizeQuantity);
		
	    taxReturnType.setTaxHousehold(taxHousehold);
	    lOGGER.debug("taxReturnType "+ taxReturnType.toString());
	    return taxReturnType;
	}
	
	private Object getPersonById(List<PersonType> person, String dependentId) {
		for(PersonType personType : person)
		{
			if(dependentId.equalsIgnoreCase(personType.getId()))
			{
				return personType;
			}
		}
		return null;
	}

	private JAXBElement<TaxFilerType> createTaxFiler(String primaryOrSpouse,String primaryorSpouseTaxFilerId, AccountTransferRequestPayloadType atRequest){
		TaxFilerType headTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxFilerType();
		ReferenceType refType = AccountTransferUtil.niemStructFactory.createReferenceType();
		
		refType.setRef(getPersonById(atRequest.getPerson(),primaryorSpouseTaxFilerId));
		headTaxFilerType.setRoleOfPersonReference(refType);
	    IdentificationType ssnIdType = createSSNIdentificationType(primaryorSpouseTaxFilerId,atRequest);
		if(ssnIdType != null){
			headTaxFilerType.setTINIdentification(ssnIdType);
		}
		JAXBElement<TaxFilerType> taxFilerType = null;
		if("primary".equalsIgnoreCase(primaryOrSpouse)){
			taxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createPrimaryTaxFiler(headTaxFilerType);
		}
		if("spouse".equalsIgnoreCase(primaryOrSpouse)){
			taxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createSpouseTaxFiler(headTaxFilerType);
		}
		return taxFilerType;		
	}
	
	private IdentificationType createSSNIdentificationType(String filerId, AccountTransferRequestPayloadType atRequest){
		IdentificationType ssnIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String ssnIdentificationValue = AccountTransferUtil.basicFactory.createString();
		if(!atRequest.getPerson().isEmpty()){
			PersonType person =(PersonType)getPersonById(atRequest.getPerson(), filerId);
			if(person != null && person.getPersonSSNIdentification()!= null && !person.getPersonSSNIdentification().isEmpty()){
				ssnIdentificationValue.setValue(person.getPersonSSNIdentification().get(0).getIdentificationID().getValue());
	        	ssnIdentificationType.setIdentificationID(ssnIdentificationValue);
		    }else{
		    	return null;
		    }
		}        
		return ssnIdentificationType;
	}
}
