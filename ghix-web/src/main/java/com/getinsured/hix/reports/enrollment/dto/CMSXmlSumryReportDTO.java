package com.getinsured.hix.reports.enrollment.dto;

import java.util.Date;

/**
 * @author vishwanath_s
 *
 */
public class CMSXmlSumryReportDTO {

	public CMSXmlSumryReportDTO() {}

	private Date creationTimeStamp;
	private Integer	reportMonth;
	private Integer reportYear;
	private Integer fileCoverageYear;
	private String hoisIssuerId;
	private String fileName;
	private Long issuerFileSetId;
	private Integer policyCount;
	private String skipRecords; 
	private String status;
	private String nextAction;
	private String sbmsSbmrRcvd;
	private Integer acceptedCount;
	private Integer acceptedErrorWarnCount;
	private String missingPolicyIds;
	private Integer totalRecordsProcessed; 
	private Integer totalRecordsRejected;
	private Integer totalPolicyrecordsApproved;
	
	public Date getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(Date creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	public Integer getReportMonth() {
		return reportMonth;
	}
	public void setReportMonth(Integer month) {
		this.reportMonth = month;
	}
	public Integer getReportYear() {
		return reportYear;
	}
	public void setReportYear(Integer year) {
		this.reportYear = year;
	}
	public Integer getFileCoverageYear() {
		return fileCoverageYear;
	}
	public void setFileCoverageYear(Integer coverageYear) {
		this.fileCoverageYear = coverageYear;
	}
	public String getHoisIssuerId() {
		return hoisIssuerId;
	}
	public void setHoisIssuerId(String hoisIssuerId) {
		this.hoisIssuerId = hoisIssuerId;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String outboundFileName) {
		this.fileName = outboundFileName;
	}
	public Long getIssuerFileSetId() {
		return issuerFileSetId;
	}
	public void setIssuerFileSetId(Long issuerFileSetId) {
		this.issuerFileSetId = issuerFileSetId;
	}
	public Integer getPolicyCount() {
		return policyCount;
	}
	public void setPolicyCount(Integer outEnrollmentCount) {
		this.policyCount = outEnrollmentCount;
	}
	public String getSkipRecords() {
		return skipRecords;
	}
	public void setSkipRecords(String skipRecords) {
		this.skipRecords = skipRecords;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String inboundStatus) {
		this.status = inboundStatus;
	}
	public String getNextAction() {
		return nextAction;
	}
	public void setNextAction(String inboundAction) {
		this.nextAction = inboundAction;
	}
	public String getSbmsSbmrRcvd() {
		return sbmsSbmrRcvd;
	}
	public void setSbmsSbmrRcvd(String sbmsSbmrRcvd) {
		this.sbmsSbmrRcvd = sbmsSbmrRcvd;
	}
	public Integer getAcceptedCount() {
		return acceptedCount;
	}
	public void setAcceptedCount(Integer acceptedCount) {
		this.acceptedCount = acceptedCount;
	}
	public Integer getAcceptedErrorWarnCount() {
		return acceptedErrorWarnCount;
	}
	public void setAcceptedErrorWarnCount(Integer acceptedErrorWarnCount) {
		this.acceptedErrorWarnCount = acceptedErrorWarnCount;
	}
	public String getMissingPolicyIds() {
		return missingPolicyIds;
	}
	public void setMissingPolicyIds(String missingPolicyIds) {
		this.missingPolicyIds = missingPolicyIds;
	}
	public Integer getTotalRecordsProcessed() {
		return totalRecordsProcessed;
	}
	public void setTotalRecordsProcessed(Integer totalRecordsProcessed) {
		this.totalRecordsProcessed = totalRecordsProcessed;
	}
	public Integer getTotalRecordsRejected() {
		return totalRecordsRejected;
	}
	public void setTotalRecordsRejected(Integer totalRecordsRejected) {
		this.totalRecordsRejected = totalRecordsRejected;
	}
	public Integer getTotalPolicyrecordsApproved() {
		return totalPolicyrecordsApproved;
	}
	public void setTotalPolicyrecordsApproved(Integer totalPolicyrecordsApproved) {
		this.totalPolicyrecordsApproved = totalPolicyrecordsApproved;
	}
	
}
