<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- The following stylesheet link has already been moved to shell.jsp. 
	If you are working on this page try removing the stylesheet link.
	If the page works fine without it, just delete the entire line along with this comment. -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ssap/jquery.mask.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/ghixcustom.js" />"></script>
<style>
.control-group
{
	margin-bottom:0;
}
.form-border{
	    border: 1px solid #d3d3d3;
    border-top: none;
}
.jsMemberSearchResultTable th{
	word-break:normal;
}
.jsMemberSearchResultTable td:nth-of-type(5),.jsMemberSearchResultTable td:nth-of-type(8)
{
	border-right:1px solid #d3d3d3;
}
</style>

<%@ page isELIgnored="false"%>
<%
String stateCode = (String) DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
%>
<c:set var="state" value="<%=stateCode%>"/>
<div class="gutter10">
	<div class="row-fluid">

		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>

			<li><a href="<c:url value="/crm/member/managemembers" />">Members
			</a> <span class="divider">/</span></li>

			<li class="active">Search</li>
		</ul>
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p>
					<c:out value="${errorMsg}"></c:out>
				<p />
				<c:remove var="errorMsg"/>
			</c:if>
			<br>
		</div>
		<div class="">
			<h1>
				Members &nbsp;
				<c:if test="${onPageLoad=='onSearchTrue'}">
				<font size="4%"><small> <fmt:formatNumber
							type="number" value="${totalConsumer}" /> total members:
				</c:if>		
			</h1>
		</div>

	</div>

	<div class="row-fluid">
		<div class="row-fluid">
			<div class="header">
				<h4><spring:message code="label.searchBy" /></h4>
			</div>
		</div>
		<div class="row-fluid">
			<form  method="POST"	action="<c:url value="/crm/member/managemembers" />" id="consumerSearch" name="consumerSearch" class="form-vertical gutter10 lightgray form-border" >
				<df:csrfToken/>
				<div class="row-fluid">
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase"><spring:message	code="label.firstName" /></label>
							<div class="controls">
								<input class="" type="text" placeholder="<spring:message	code="label.firstName" />" name="firstName" id="firstName" value="${searchCriteria.firstName}" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase"><spring:message	code="label.lastName" /></label>
							<div class="controls">
								<input class="" type="text" placeholder="<spring:message	code="label.lastName" />" name="lastName" id="lastName" value="${searchCriteria.lastName}" maxlength="40" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase"><spring:message	code="label.columnHeaderPhone" /></label>
							<div class="controls">
								<input class="" type="text" placeholder="<spring:message	code="label.columnHeaderPhone" />" name="contactNumber" id="contactNumber" value="${searchCriteria.contactNumber}" maxlength="10" />
							</div>
						</div>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase"><spring:message	code="label.Email" /></label>
							<div class="controls">
								<input class="" type="text" placeholder="<spring:message	code="label.Email" />" name="householdEmail" id="householdEmail" value="${searchCriteria.householdEmail}" maxlength="60" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label" text-transform="uppercase"><spring:message	code="label.columnHeaderZip" /></label>
							<div class="controls">
								<input class="" type="text" placeholder="<spring:message	code="label.columnHeaderZip" />" name="zipCode" id="zipCode" value="${searchCriteria.zipCode}" maxlength="5" />
							</div>
						</div>						
					</div>
					<div class="span4">
						<div class="control-group">
							<label class="control-label"><spring:message code="label.dob"/></label>
							<div class="controls">
								<div class="input-append date-picker date">
									<input type="text" title="MM/DD/YYYY" value="${searchCriteria.dateOfBirth}" class="datepick input-medium date-format" name="dateOfBirth" id="dateOfBirth" placeholder="MM/DD/YYYY" />
									<span class="add-on"><i class="icon-calendar"></i></span>
							</div>
						</div>
					</div>
				</div>
				</div>
				
				<div class="txt-right">
					<a class="btn" href="<c:url value="/crm/member/managemembers" />"><spring:message code="label.crmResetAll" /></a>
					<button type="submit" class="btn btn-primary margin10-l"><spring:message  code='label.go'/></button>
				</div>
				
				<input  type="hidden" id="pageNumber" name="pageNumber">
			            <input  type="hidden" id="sortOrder" name="sortOrder">
			            <input  type="hidden" id="changeOrder" name="changeOrder">
			            <input  type="hidden" id="sortBy" name="sortBy">
			</form>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12" id="rightpanel">
			<form class="form-horizontal" id="frmSearchConsumer"
				name="frmSearchConsumer"
				action="<c:url value="/crm/member/managemembers" />"
				method="POST">
				<df:csrfToken/>
				<!-- below hidden field is to avoid fetching data on first load -->
				<c:choose>
					<c:when test="${onPageLoad!='onSearchTrue'}">
						<div class="alert alert-info">
							<spring:message code="label.filterSearchCriteria" />
						</div>
					</c:when>
					<c:otherwise>
						<c:choose>
							<c:when test="${resultSize >= 1}">
								<c:choose>
									<c:when test="${fn:length(householdList) > 0}">
										<table class="table table-striped jsMemberSearchResultTable">
											<thead>
												<tr class="graydrkbg jsMemberHeaderRow memberHeaderRow">
													<spring:message code='label.columnHeaderName'
														var="firstName" />

													<th class="section-one">&nbsp;</th>
													<th class="section-one">Household Id</th>
													<th class="sortable section-one" scope="col"><dl:sort
															title="${firstName}" sortBy="firstName" customFunctionName="sortMembers"
															sortOrder="${searchCriteria.sortOrder}"></dl:sort> <c:choose>
															<c:when
																test="${searchCriteria.sortOrder == 'ASC' && searchCriteria.sortBy == 'firstName'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-up.png'/>"
																	alt="State sort ascending" />
															</c:when>
															<c:when
																test="${searchCriteria.sortOrder == 'DESC' && searchCriteria.sortBy == 'firstName'}">
																<img
																	src="<c:url value='/resources/images/i-aro-blu-sort-dwn.png'/>"
																	alt="State sort descending" />
															</c:when>
															<c:otherwise></c:otherwise>
														</c:choose></th>
												
													<th class="section-one"><spring:message code='label.columnHeaderZip' /></th>
													<th class="section-one"><spring:message code='label.columnHeaderPhone' /></th>
													<th class="section-two">App. Id</th>
													<th class="section-two">Financial</th>
													<th class="section-two">App. Status</th>
													
													<sec:accesscontrollist hasPermission="MEMBER_SEARCH_VIEW_ACCOUNT_ID" domainObject="user">
														<th class="section-three">Account Id</th>
													</sec:accesscontrollist>
													
													<sec:accesscontrollist hasPermission="MEMBER_SEARCH_VIEW_ACCOUNT_EMAIL" domainObject="user">
														<th class="section-three">Account Email</th> 
													</sec:accesscontrollist>
													
												<!-- 	<th>Acct. Create Date</th> -->
													
													
												</tr>
											</thead>

											<% 
											int index = 0;%>
											
											<c:forEach items="${householdList}" var="household"
												varStatus="vs">
												<c:set var="encHouseholdid" ><encryptor:enc value="${household[0]}" isurl="true"/> </c:set>
												<tr>
													<td class="jsRelinkChkTd" align="center">
													<c:if test="${isRelinkAllowed == 'true'}">
														<input type="checkbox" name="chkRelink" class="jsRelinkCheckbox" />
													</c:if>
														<input type="hidden" name="hidHouseholdId" class="jsHidHouseholdId" value="${encHouseholdid}" />
													</td>		
													<td>
														${household[0]}
													</td>										
													<td>													  
														<a href="<c:url value="/crm/member/viewmember/${encHouseholdid}"/>">${household[1]} ${household[2]}</a>
														
														<!-- this table is for additional applicants listed under head of household -->
														<table class="subFamily hide">
                                                			<tbody>  
                                                    			<tr>
                                                        			<td class="dependentHousehold"><i class="icon-level-down"></i> <a href="#">Laura Smith</a></td>
                                                    			</tr>
                                                			</tbody>
                                            			</table>
                                            			<!-- table ends -->
													</td>
													<td>${household[5]}</td>
													<td>${household[6]}</td>
													<td>
														${household[14]}
													</td>
													<td>${household[15]}</td>
													<td>${household[7]}				
												  		<input type="hidden" name="hidUserId" class="jsHidUserId" value="${household[11]}" />
												  	</td>
												  	
												 <sec:accesscontrollist hasPermission="MEMBER_SEARCH_VIEW_ACCOUNT_ID" domainObject="user">
												 	<td class="jsDynamicEmailId">${household[11]}</td>   
												 </sec:accesscontrollist>
												 
												 <sec:accesscontrollist hasPermission="MEMBER_SEARCH_VIEW_ACCOUNT_EMAIL" domainObject="user">
												 	 <td class="breakword">  ${household[9]}</td> 
												 </sec:accesscontrollist>
											
												</tr>
											</c:forEach>
										
										</table>
										<div class="pagination txt-center">
											<dl:paginate resultSize="${resultSize + 0}"
												pageSize="${pageSize + 0}" customFunctionName="GoToSearchMembersPage"/>
										</div>
										<c:if test="${isRelinkAllowed == 'true'}">
													<div class="relink-wrap">
														<input type="button" name="btnRelinkAccount" id="btnRelinkAccount" class="btn btn-primary" value="Relink" />
													</div>
												</c:if>
										
									</c:when>
								</c:choose>
							</c:when>
							<c:otherwise>
								<h4 class="alert alert-info">
									<spring:message code='label.norecords' />
								</h4>
							</c:otherwise>
						</c:choose>
					</c:otherwise>
				</c:choose>
				<%-- </c:when>
			<c:otherwise>
				<h4 class="alert alert-info">
						<spring:message code="label.filterSearchCriteria"/>
				</h4>
			</c:otherwise>
			</c:choose> --%>
			</form>
		</div>
	</div>



	
	<!-- row-fluid -->
</div>

<!-- Relink AccountModal -->
<div id="relinkAccountModal" class="modal hide fade bigModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      <h3>Relink</h3>
    </div>
 	<div class="modal-body gutter10 ">
 		<div><ul>
				<li>First select <strong>Name</strong></li>
				<li>Then select <strong>Account Id</strong> to link to</li> 		
 			</ul>
 		</div>
 		<form name="frmRelinkAccount" id="frmRelinkAccount" class="jsRelinkAccountForm" method="POST">
 		<df:csrfToken/>
			<table class="table table-striped">
				<thead>
					<tr class="graydrkbg">
						<th class="section-one">&nbsp;</th>
						<th class="section-one">Household Id</th>
						<th scope="col" class="section-one">${firstName}</th>				
						<th class="section-one"><spring:message code='label.columnHeaderZip' /></th>
						<th class="section-one"><spring:message code='label.columnHeaderPhone' /></th>		
						<th class="section-two">App. Id</th>			
						<th class="section-two">Financial</th>
						<th class="section-two">App. Status</th>
						<th class="section-three">&nbsp;</th>
						<c:if test="${state != 'MN'}">
					    	<th class="section-three">Account Id</th>	
							<th class="section-three breakword">Account Email</th> 
						</c:if>
					</tr>
				</thead>
				<tbody class="jsRelinkConfirmTable">
				</tbody>
			</table>
		</form>
	</div>
	<div class="modal-footer margin0-t">
       <button class="btn btn-action pull-left" data-dismiss="modal">Close</button>
       <button class="btn btn-primary" name="btnSaveRelink" id="btnSaveRelink">Relink</button>
     </div>
</div>
<script type="text/javascript">

	$(function(){
		
		//Relink account code:
		var relinkPageBtn = $('#btnRelinkAccount'),
			relinkAccountModal = $('#relinkAccountModal'), 
			memberSearchResultTable = $('.jsMemberSearchResultTable'),
			relinkModalConfirmTable = $('.jsRelinkConfirmTable'),
			relinkCheckbox = $('.jsRelinkCheckbox'),
			relinkChkTd = $('.jsRelinkChkTd'),
			relinkModalRadioBtn = $('.jsModalRadioBtn'),			
			relinkModalSaveBtn = $('#btnSaveRelink');
		
		//Reset checkboxes on every reload
		relinkCheckbox.attr('checked',false);
	
		//On checkbox button clicked
		relinkCheckbox.on('click',function(){
			var me = $(this);
			if(me.is(':checked')){
				me.parents('tr').addClass('rowSelected');
			}else{
				me.parents('tr').removeClass('rowSelected');
			}
		});
		
		//On Relink button clicked on the page
		relinkPageBtn.on('click',function(){
			if(!relinkCheckbox.is(':checked')){
				alert('Please select member to relink');
			}
			else{
				//Clone rows from selected rows from member search result table
				var clonedRow = memberSearchResultTable.find('.rowSelected').clone();
				
				//Empty first checkbox column append new radio button 
				clonedRow.find('.jsRelinkChkTd').find('.jsRelinkCheckbox').remove();				
				clonedRow.find('.jsRelinkChkTd').append('<input type="radio" name="rdoRelinkNameBtn" class="jsRelinkNameRdoBtn jsModalRadioBtn" />');
				
				//Append new column before email address to choose the options
				clonedRow.find('.jsDynamicEmailId').before('<td align="center"><input type="radio" name="rdoRelinkEmailBtn" class="jsRelinkEmailRdoBtn jsModalRadioBtn" /></td>');
				
				//Append html to modal relink table from clone element
				relinkModalConfirmTable.html(clonedRow);
				relinkAccountModal.modal('show');
				
				$(document).on('click', '.jsModalRadioBtn', function(){
					var me = $(this);
					me.parents('tr').find('input[type="radio"]').attr('checked',false);
					me.attr('checked',true);
				});
			}
		});
		
		//On jsRelinkNameRdoBtn clicked
		$(document).on('click', '.jsRelinkNameRdoBtn', function(){ 
			var me = $(this);
			var currentHouseholdIdEle = me.parents('tr').find('.jsHidHouseholdId');			
			//console.log(currentHouseholdIdEle.val());
			
			me.val(currentHouseholdIdEle.val());
		});
		
		//On jsRelinkEmailRdoBtn clicked
		$(document).on('click', '.jsRelinkEmailRdoBtn', function(){ 
			var me = $(this);
			var curUserIdEle = me.parents('tr').find('.jsHidUserId');
			
			//console.log(curUserIdEle.val());
			me.val(curUserIdEle.val());
		});
		
		//On btnSaveRelink submit modal
		relinkModalSaveBtn.on('click', function(){ 
			if(!relinkModalRadioBtn.is(':checked') && $('.jsModalRadioBtn:checked').length <2){
				alert('Please choose option to relink');
			}
			else{
				
				var selectedHHId = $('.jsRelinkNameRdoBtn:checked').val(),
					selectedUserId = $('.jsRelinkEmailRdoBtn:checked').val(),
					selectedUserIdHHId = $('.jsRelinkEmailRdoBtn:checked').parents('tr').find('.jsHidHouseholdId').val();
				if(selectedHHId!='' && selectedUserId!='' && selectedUserIdHHId!=''){
					var validateUrl = '<c:url value="/crm/member/relinkMember/">  </c:url>';
					var formActionURL = validateUrl+'/'+selectedHHId+'/'+selectedUserIdHHId+'/'+selectedUserId;
					
					$('#frmRelinkAccount').attr('action',formActionURL);
					$('#btnSaveRelink').attr('disabled', true);
					$('#frmRelinkAccount').submit();
				}
				else{
					alert ("First select Name. Then select Account Id to link to.");
				}
				//console.log(selectedHHId+ '-' +selectedUserIdHHId+'-'+selectedUserId);
				
			}
		});

		$('.date-format').mask("00/00/0000");
		$('.date-picker').datepicker({
			autoclose: true
		});
	});
	
	var validator = $("#consumerSearch")
			.validate(
					{
						rules : {
							'contactNumber' : {
								minlength : 10,
								number : true
							},
							
							'zipCode' : {
								minlength : 5,
								number : true
							},
							
							'householdEmail' : {
								email : true
							}
						},
						messages : {
							'contactNumber' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validatePhoneNoFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							
							'zipCode' : {
								minlength : "<span> <em class='excl'>!</em><spring:message code='label.validateZipFilterLength' javaScriptEscape='true'/></span>",
								number : "<span> <em class='excl'>!</em><spring:message code='label.validateNumericFilterValue' javaScriptEscape='true'/></span>"
							},
							'householdEmail' : {
								email : "<span> <em class='excl'>!</em>Please provide correct email</span>"
							}
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							if ($("#" + elementId + "_error").html() != null) {
								$("#" + elementId + "_error").html('');
							}
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});
	
	
	function openDilogForPhone(){
		//var afterSave = '<% request.getAttribute("afterSave"); %>';
	//	 if(afterSave!='null'){
		        $('#markCompleteDialog').modal({
		            backdrop: true 
		        });
	//    } 
		
		
	};	


	function showConfirmDialog(index) {
		//alert('selected index :: '+index);
		var selectedEligLead = $('#eligLead'+index).val();
		
		//alert(selectedEligLead);
		$('#eligLeadId').val(selectedEligLead);
		//alert($('#eligLeadId').val());
//		var objPath = document.consumerSearch.eligLeadId+index;
	//	objPath.value = document.leadForm.eligLeadId.value;
	       $('#leadDialog').modal({
	            backdrop: true 
	        });
	       
	}
	
	function GoToSearchMembersPage (url)
	{
		var pageNumber=GetParameterByName(url,"pageNumber");
		$("#pageNumber").val(pageNumber);
		$("#consumerSearch").submit();
	};
	
	function sortMembers (url)
	{
		var pageNumber=GetParameterByName(url,"pageNumber");
		var sortBy=GetParameterByName(url,"sortBy");
		var changeOrder=GetParameterByName(url,"changeOrder");
		var sortOrder=GetParameterByName(url,"sortOrder");
		$("#pageNumber").val(pageNumber);
		$("#sortBy").val(sortBy);
		$("#changeOrder").val(changeOrder);
		$("#sortOrder").val(sortOrder);
		
		$("#consumerSearch").submit();
	};

	/* function setdate(){
		$("#submittedBefore").val($("#submittedBefore").val());
	}
	
	$(function() {
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true
			});
		});
	});	 */
</script>


