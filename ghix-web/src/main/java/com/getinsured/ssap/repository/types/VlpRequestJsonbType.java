package com.getinsured.ssap.repository.types;

import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SessionImplementor;
import org.hibernate.usertype.UserType;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.ssap.model.hub.vlp.VlpRequest;

/**
 * JSONB type DB definition.
 *
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
public class VlpRequestJsonbType implements UserType
{
  /**
   * Return the SQL type codes for the columns mapped by this type. The
   * codes are defined on <tt>java.sql.Types</tt>.
   * @see Types* @return int[] the typecodes
   */
  @Override
  public int[] sqlTypes() {
    return new int[] { Types.JAVA_OBJECT };
  }

  /**
   * The class returned by <tt>nullSafeGet()</tt>.
   *
   * @return Class
   */
  @Override
  public Class returnedClass() {
    return VlpRequest.class;
  }

  /**
   * Compare two instances of the class mapped by this type for persistence "equality".
   * Equality of the persistent state.
   *
   * @param x first object.
   * @param y second object
   * @return boolean value if first equals to second.
   */
  @Override
  public boolean equals(final Object x, final Object y) throws HibernateException {
    return x.equals(y);
  }

  /**
   * Get a hashcode for the instance, consistent with persistence "equality"
   */
  @Override
  public int hashCode(final Object x) throws HibernateException {
    if(x != null)
      return x.hashCode();
    throw new HibernateException("Can't return hash code from null object: " + x);
  }

  /**
   * Retrieve an instance of the mapped class from a JDBC resultset. Implementors
   * should handle possibility of null values.
   *
   *
   * @param rs a JDBC result set
   * @param names the column names
   * @param session
   * @param owner the containing entity  @return Object
   * @throws HibernateException* @throws SQLException
   */
  @Override
  public VlpRequest nullSafeGet(final ResultSet rs, final String[] names, final SessionImplementor session, final Object owner) throws HibernateException, SQLException
  {
    final String cellContent = rs.getString(names[0]);

    if (cellContent == null) {
      return null;
    }
    try {
      final ObjectMapper mapper = new ObjectMapper();
      return mapper.readValue(cellContent.getBytes(StandardCharsets.UTF_8), VlpRequest.class);
    } catch (final Exception ex) {
      throw new RuntimeException("Failed to convert column value " + cellContent + " to given class: " + returnedClass() + " => " + ex.getMessage(), ex);
    }
  }

  /**
   * Write an instance of the mapped class to a prepared statement. Implementors
   * should handle possibility of null values. A multi-column type should be written
   * to parameters starting from <tt>index</tt>.
   *
   *
   * @param st a JDBC prepared statement
   * @param value the object to write
   * @param index statement parameter index
   * @param session Hibernate session.
   * @throws HibernateException* @throws SQLException
   */
  @Override
  public void nullSafeSet(final PreparedStatement st, final Object value, final int index, final SessionImplementor session) throws HibernateException, SQLException
  {
    if (value == null) {
      st.setNull(index, Types.OTHER);
      return;
    }
    try {
      final ObjectMapper mapper = new ObjectMapper();
      final String val = mapper.writeValueAsString(value);
      st.setObject(index, val, Types.OTHER);
    } catch (final Exception ex) {
      throw new RuntimeException("Failed to convert Invoice to String: " + ex.getMessage(), ex);
    }
  }

  /**
   * Return a deep copy of the persistent state, stopping at entities and at
   * collections. It is not necessary to copy immutable objects, or null
   * values, in which case it is safe to simply return the argument.
   *
   * @param value the object to be cloned, which may be null
   * @return Object a copy
   */
  @Override
  public Object deepCopy(final Object value) throws HibernateException
  {
    return value;
  }

  /**
   * Are objects of this type mutable?
   *
   * @return boolean
   */
  @Override
  public boolean isMutable() {
    return false;
  }

  /**
   * Transform the object into its cacheable representation. At the very least this
   * method should perform a deep copy if the type is mutable. That may not be enough
   * for some implementations, however; for example, associations must be cached as
   * identifier values. (optional operation)
   *
   * @param value the object to be cached
   * @return a cachable representation of the object
   * @throws HibernateException
   */
  @Override
  public Serializable disassemble(final Object value) throws HibernateException {
    return (Serializable) value;
  }

  /**
   * Reconstruct an object from the cacheable representation. At the very least this
   * method should perform a deep copy if the type is mutable. (optional operation)
   *
   * @param cached the object to be cached
   * @param owner the owner of the cached object
   * @return a reconstructed object from the cachable representation
   * @throws HibernateException
   */
  @Override
  public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
    return cached;
  }

  /**
   * During merge, replace the existing (target) value in the entity we are merging to
   * with a new (original) value from the detached entity we are merging. For immutable
   * objects, or null values, it is safe to simply return the first parameter. For
   * mutable objects, it is safe to return a copy of the first parameter. For objects
   * with component values, it might make sense to recursively replace component values.
   *
   * @param original the value from the detached entity being merged
   * @param target the value in the managed entity
   * @param owner @return the value to be merged
   */
  @Override
  public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
    return original;
  }
}
