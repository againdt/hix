package com.getinsured.eligibility.rrv.repository;

import com.getinsured.eligibility.model.RrvSubmission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import java.util.List;


public interface RrvSubmissionRepository extends PagingAndSortingRepository<RrvSubmission, Long> {

    @Query("SELECT rs " +
            "FROM RrvSubmission rs " +
            "WHERE rs.coverageYear = :coverageYear AND ( rs.ifsvStatus = 'PENDING' " +
            "          OR rs.ssacStatus = 'PENDING' " +
            "          OR rs.eqfxStatus = 'PENDING' " +
            "          OR rs.smecStatus = 'PENDING' ) ")
    List<RrvSubmission> findAllPendingSubmissions(@Param("coverageYear") long coverageYear);

    @Query("SELECT CASE WHEN COUNT(rs) > 0 THEN TRUE ELSE FALSE END " +
            "FROM RrvSubmission rs " +
            "where rs.coverageYear = :coverageYear and rs.householdId = :householdId ")
    boolean existsByHouseholdId(@Param("coverageYear") long coverageYear, @Param("householdId") long householdId);

    @Query("SELECT rs " +
            "FROM RrvSubmission rs " +
            "WHERE rs.coverageYear = :coverageYear AND rs.creationDate < CURRENT_DATE - 3 AND ( rs.ifsvStatus = 'PENDING' " +
            "          OR rs.ssacStatus = 'PENDING' " +
            "          OR rs.eqfxStatus = 'PENDING' " +
            "          OR rs.smecStatus = 'PENDING' ) ")
    Page<RrvSubmission> findAllPendingSubmissionsPageable(@Param("coverageYear") long coverageYear, Pageable pageable);

    @Query("SELECT rs " +
            "FROM RrvSubmission rs " +
            "WHERE rs.coverageYear = :coverageYear AND rs.householdId = :householdId ")
    RrvSubmission findByCoverageYearHouseholdId(@Param("coverageYear") long coverageYear, @Param("householdId") long householdId);
}
