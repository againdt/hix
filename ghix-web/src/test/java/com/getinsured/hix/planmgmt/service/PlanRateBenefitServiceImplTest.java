package com.getinsured.hix.planmgmt.service;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Provider.ProviderType;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;


public class PlanRateBenefitServiceImplTest extends BaseTest{
		
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanRateBenefitServiceImplTest.class);
	private static final String ZIP = "zip";
	private static final String AGE = "age";
	private static final String RELATION = "relation";
	private static final String COUNTY = "county";
	private static final String PROVIDER_TYPE = "type";
	private static final String PROVIDER_ID = "providerId";
	private static final String IS_SUPPORTED = "isSupported"; 
	
	@PersistenceUnit private EntityManagerFactory emf;
	
	
	@Test
	public void testProcessMemberData(){
		
		ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		ArrayList<Map<String, String>> modifiedData = new ArrayList<Map<String, String>>();
		
		Map<String, String> member1 = new HashMap<String, String>();
		member1.put(AGE, "45");
		member1.put(ZIP, "92222");
		member1.put(RELATION, "member");
		
		Map<String, String> member7 = new HashMap<String, String>();
		member7.put(AGE, "41");
		member7.put(ZIP, "92222");
		member7.put(RELATION, "spouce");
		
		Map<String, String> member2 = new HashMap<String, String>();
		member2.put(AGE, "15");
		member2.put(ZIP, "92227");
		member2.put(RELATION, "child");
		
		Map<String, String> member3 = new HashMap<String, String>();
		member3.put(AGE, "16");
		member3.put(ZIP, "861");
		member3.put(RELATION, "child");
		
		Map<String, String> member4 = new HashMap<String, String>();
		member4.put(AGE, "17");
		member4.put(ZIP, "861");
		member2.put(RELATION, "child");
		
		Map<String, String> member5 = new HashMap<String, String>();
		member5.put(AGE, "18");
		member5.put(ZIP, "92222");
		member5.put(RELATION, "child");
		
		Map<String, String> member6 = new HashMap<String, String>();
		member6.put(AGE, "25");
		member6.put(ZIP, "92222");
		member6.put(RELATION, "child");
		
		
		memberList.add(member1);
		memberList.add(member2);
		memberList.add(member3);
		memberList.add(member4);
		memberList.add(member5);
		memberList.add(member7);
		
		String childMaxAge = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERCHILDAGE);
		String ConsiderMaxChilds = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERMAXCHILDREN);
		int cMaxAge = 0;
		int childCount = 0;
		int considerMaxChildrens = 0;
		
		try {
			cMaxAge = Integer.parseInt(childMaxAge);
			considerMaxChildrens = Integer.parseInt(ConsiderMaxChilds);
			
			for(int i=0; i< memberList.size(); i++){
				if(memberList.get(i).get(RELATION) != null){
					if(memberList.get(i).get(RELATION).equalsIgnoreCase("child")){
						if (Integer.parseInt(memberList.get(i).get(AGE)) < cMaxAge && childCount < considerMaxChildrens) {
							modifiedData.add(memberList.get(i));
							childCount++;
						}else if (Integer.parseInt(memberList.get(i).get(AGE)) >= cMaxAge) {
							modifiedData.add(memberList.get(i));	
						} 
					}else{
						modifiedData.add(memberList.get(i));
					}
				}	
			}
		
		}catch (NumberFormatException e) {
			LOGGER.error("error while pasring config param"+e.getMessage());
		}
			
		LOGGER.info("Result:- "+memberList);
		LOGGER.info("test completes success");
	}
	
	@Test
	public void testGetHouseholdHealthPlanRateBenefits() {
		
		String effectiveDate = "01/05/2012";
		String costSharing = "1000";
		String planIdStr ="1";
		boolean showCatastrophicPlan = false;
		
		ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		
		LOGGER.info("add test data");
		
		Map<String, String> member1 = new HashMap<String, String>();
		member1.put(AGE, "45");
		member1.put(ZIP, "92222");
		member1.put(RELATION, "member");
		
		Map<String, String> member7 = new HashMap<String, String>();
		member7.put(AGE, "41");
		member7.put(ZIP, "92222");
		member7.put(RELATION, "spouce");
		
		Map<String, String> member2 = new HashMap<String, String>();
		member2.put(AGE, "15");
		member2.put(ZIP, "92227");
		member2.put(RELATION, "child");
		
		Map<String, String> member3 = new HashMap<String, String>();
		member3.put(AGE, "16");
		member3.put(ZIP, "861");
		member3.put(RELATION, "child");
		
		Map<String, String> member4 = new HashMap<String, String>();
		member4.put(AGE, "17");
		member4.put(ZIP, "861");
		member2.put(RELATION, "child");
		
		Map<String, String> member5 = new HashMap<String, String>();
		member5.put(AGE, "18");
		member5.put(ZIP, "92222");
		member5.put(RELATION, "child");
		
		Map<String, String> member6 = new HashMap<String, String>();
		member6.put(AGE, "25");
		member6.put(ZIP, "92222");
		member6.put(RELATION, "child");
		
		
		memberList.add(member1);
		memberList.add(member2);
		memberList.add(member3);
		memberList.add(member4);
		memberList.add(member5);
		memberList.add(member7);

		String familyTier = getFamilyTier(memberList);
		
		LOGGER.info("Test executaion is in progress....");
		
		String buildquery = "SELECT p.id AS plan_id, p.name AS plan_name, plan_level, sum(pre) AS total_premium, p.network_type AS network_type, i.name AS issuer_name, phealth_id, parent_id, ehb_covered FROM plan p, issuers i, (" ;	
		
		for(int i=0; i< memberList.size(); i++){
	  		buildquery += "( SELECT phealth.id AS phealth_id, phealth.parent_plan_id AS parent_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, phealth.ehb_covered as ehb_covered  " + 
  				  		  " FROM plan_health phealth, plan_health_service_region pregion, plan_health_rate prate, region region " +
	  					  " WHERE " +
	  					  " phealth.id = pregion.plan_health_id " ;	  		
	  		buildquery += (costSharing.equals("") || (costSharing == null)) ?  " AND phealth.parent_plan_id = 0 " : " AND ((phealth.parent_plan_id = 0 and phealth.plan_level !='SILVER')  OR phealth.cost_sharing=UPPER('" + costSharing  +"'))" ;	  						  
			buildquery += " AND pregion.id = prate.service_region_id " +
						  " AND region.id = pregion.region_id " +	
	  					 // " AND prate.tobacco = '" + memberList.get(i).get("tobacco").toLowerCase() + "' " +
	  					  " AND " + memberList.get(i).get(AGE) +" >= prate.min_age AND " + memberList.get(i).get(AGE) + " <= prate.max_age " +
	  					  " AND prate.family_tier = '"+ familyTier +"'"+
	  					  " AND region.zip = " + memberList.get(i).get(ZIP);
			
			if( showCatastrophicPlan == false){
	  			buildquery += " AND phealth.plan_level !='CATASTROPHIC'";
	  		}
	  		
	  		if( memberList.get(i).get(COUNTY)!=null){
	  			buildquery += " AND region.county = '" + memberList.get(i).get(COUNTY) + "'";
	  		}
	  		buildquery += " )";
	  		
	  		if(i+1 !=  memberList.size()){
	  			buildquery += " UNION ALL ";
	  		}	
	  		 
		 }
		
		buildquery += ") temp WHERE p.id = temp.plan_id AND TO_DATE ('" + effectiveDate + "', 'YYYY-MM-DD') between p.start_date and p.end_date " +
				" AND p.status IN ('" + Plan.PlanStatus.CERTIFIED.toString() + "') " +
  				" AND p.issuer_verification_status='" + Plan.IssuerVerificationStatus.VERIFIED.toString() + "' AND p.enrollment_avail='" + Plan.EnrollmentAvail.AVAILABLE.toString() + "'  AND TO_DATE ('" + effectiveDate + "', 'YYYY-MM-DD') >= p.enrollment_avail_effdate " +
  				" AND p.market = '" + Plan.PlanMarket.INDIVIDUAL.toString() +"' AND p.issuer_id = i.id ";
		
		if((planIdStr != null) && !planIdStr.equals("")){
			buildquery += " AND p.id in (" + planIdStr + ") " ;
		}
		buildquery += " GROUP BY  p.id, p.name, phealth_id, parent_id, plan_level, i.name, p.network_type, ehb_covered";		
		
		LOGGER.info("SQL == "+ buildquery);
		
		EntityManager em = emf.createEntityManager();
		List rsList = em.createNativeQuery(buildquery).getResultList();		 
		Iterator rsIterator = rsList.iterator();
		
		while(rsIterator.hasNext()){
			System.out.println("Result : "+rsIterator.next());				
		}
		
		LOGGER.info("Test compltes successfully");
		
	}	

	@Ignore
	private String getFamilyTier(ArrayList<Map<String, String>> memberList){		
		
		String considerOnlyMemberPMPM = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERONLYMEMBERPMPM);
		
		if (considerOnlyMemberPMPM.equalsIgnoreCase("YES")) {
			return "MEMBER";
		}
		
		Boolean isMember = false;
		Boolean isSpouse = false;
		Boolean isChild = false;
		
		for(int i=0; i< memberList.size(); i++){
			if(memberList.get(i).get(RELATION) != null){
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("member") || memberList.get(i).get(RELATION).equalsIgnoreCase("self")){
					isMember = true;
				}	
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("spouse")){
					isSpouse = true;
				}	
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("child")){
					isChild = true;
				}	
			}	
		}
		
		if(isMember && memberList.size() == 1){		// when only self quoted
			return "MEMBER";
		}	
		if(isSpouse && memberList.size() == 1){		// when only spouse quoted
			return "MEMBER";
		} else if(isMember && isSpouse && memberList.size() == 2){		// when only self + spouse quoted
			return "MEMBERSPOUSE";
		} else if(isMember && isSpouse && isChild){	// when only self + spouse + child quoted
			return "FAMILY";
		} else if(!isMember && !isSpouse && isChild){	// when only child quoted
			return "CHILDONLY";
		} else if(isMember && !isSpouse && isChild){	// when only self + child quoted
			return "MEMBERCHILDREN";
		} else if(!isMember && isSpouse && isChild){	// when only spouse + child quoted
			return "MEMBERCHILDREN";
		} else{
			return "MEMBER";
		}	
		
	}
	
	@Test
	public void checkProviderInPlan() {
		
		List<Map<String, String>> providers = new ArrayList<Map<String,String>>();
		
		Map<String ,String>  p1 = new HashMap<String, String>();
		Map<String ,String>  p2 = new HashMap<String, String>();
		Map<String ,String>  p3 = new HashMap<String, String>();
		Map<String ,String>  p4 = new HashMap<String, String>();
		
		p1.put(PROVIDER_ID, "19");
		p1.put(PROVIDER_TYPE, "DOCTOR");
		p1.put(IS_SUPPORTED, "");
		
		p2.put(PROVIDER_ID, "4");
		p2.put(PROVIDER_TYPE, "DENTIST");
		p2.put(IS_SUPPORTED, "");
		
		p3.put(PROVIDER_ID, "17");
		p3.put(PROVIDER_TYPE, "DOCTOR");
		p3.put(IS_SUPPORTED, "");
		
		p4.put(PROVIDER_ID, "16");
		p4.put(PROVIDER_TYPE, "FACILITY");
		p4.put(IS_SUPPORTED, "");
		
		
		List<Map<String, String>> modifiedProviders = new ArrayList<Map<String,String>>();
		
		boolean isPlanSupports;
		int planId = 1;
		
		for (Iterator iterator = providers.iterator(); iterator.hasNext();) {
			
			try {
				Map<String, String> providerMap = (Map<String, String>) iterator.next();
				isPlanSupports = false;
				
				isPlanSupports = checkProviderInPlan(planId,Integer.parseInt(providerMap.get(PROVIDER_ID)),providerMap.get(PROVIDER_TYPE));
				
				providerMap.put(IS_SUPPORTED,String.valueOf(isPlanSupports));
				
				modifiedProviders.add(providerMap);
			
			} catch (Exception e) {
				LOGGER.error("Error occurred please find reason "+e.getMessage());
			}
		}
		
		LOGGER.debug(modifiedProviders.toString());
		
		LOGGER.info("Test executed successfully");
	}
	
	private boolean checkProviderInPlan(int planId,int providerId,String physicianType){
		String buildquery = "SELECT COUNT(pln.id) FROM plan pln, physician phy, facility f, provider_network pn, provider prd, network n "+		
				"WHERE " +
				"pln.provider_network_id = pn.id " +
				"AND pn.provider_id = prd.id " +	  		
				"AND pn.network_id = n.id " + 	  						  
				"AND phy.provider_id = prd.id " +
				"AND f.provider_id = prd.id " +
			    "AND pln.id = "+ planId +
				" AND prd.id = "+ providerId;
				if (ProviderType.FACILITY.toString().equalsIgnoreCase(physicianType)) {
					buildquery = buildquery + " AND prd.type = '"+physicianType +"'";
				}else {
					buildquery = buildquery + " AND phy.type = '"+physicianType +"'";
				}
		EntityManager em = emf.createEntityManager();
		List rsList = em.createNativeQuery(buildquery).getResultList();
		Iterator rsIterator = rsList.iterator();
		
		System.out.println("Query :- "+buildquery);
		BigDecimal count = null;

		while(rsIterator.hasNext()){
			count = (BigDecimal) rsIterator.next();						
		}
		
		if (count!= null && count.intValue()>0) {
			return true;
		}else {
			return false;
		}
	}
	
}