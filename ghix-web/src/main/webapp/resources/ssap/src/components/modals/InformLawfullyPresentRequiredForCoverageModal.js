import React, { Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

// Components

import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

// ActionCreators

import { hideModal } from '../../actions/actionCreators';

// Constants

const InformLawfullyPresentRequiredForCoverageModal = props => {

    const {
        modal: {
            modalOwner,
            callbacks
        }
    } = props;

    const handleRightBtnClick = () => {
        callbacks.map(callback => {
            callback();
        });
    };

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__lawfully_presence__for__coverage">
                <h2>
                    Information About Citizenship or Lawfully Presence
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body txt-left">
                <p>
                    In order to apply for coverage, you have to be a US citizen or be Lawfully present in US.
                    Please select an option that is applicable to you. If you do not provide this information,
                    you may not be eligible for health coverage on Exchange.
                </p>
                <div className='modal__controls'>
                    <Button
                        className='modal-cancel-btn usa-button-secondary'
                        onClick={handleHideClick}
                    >
                        CLOSE
                    </Button>
                    <Button
                        className='modal-confirm-btn'
                        onClick={handleRightBtnClick}
                    >
                        Ok
                    </Button>
                </div>
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(InformLawfullyPresentRequiredForCoverageModal);
