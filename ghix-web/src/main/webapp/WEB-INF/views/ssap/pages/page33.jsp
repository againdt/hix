<%@ include file="../include.jsp" %>
<div id="appscr83"  style='display: none;'>
	<div class="row-fluid">
	    
		<div class="page-title">
			<h5 class="titleForPages">
				<spring:message code="ssap.page33.review" text="Review" />&nbsp;&amp;&nbsp;<spring:message code="ssap.page33.sign" text="Sign" />
			</h5>
		</div>
		
		<div class="container-fluid">
			<div class="control-group">
				<div class="alert alert-info">
					<p class="margin10-t">
						<strong><spring:message code="ssap.page33.takeAFewMinutes" text="Take a few minutes to review the information you gave us." /></strong>
					</p>
					<p class="margin10-t">
						<spring:message code="ssap.page33.thisIsYourChance" text="This is your chance to go back and make changes before you submit your final application." /> 
					</p>
				</div>
				<%-- <label>
					<em>
						<spring:message code="ssap.page33.allFieldsOn" text="All fields on this Review" />&nbsp;&amp;&nbsp;<spring:message code="ssap.page33.signSectionRequired" text="Sign section are required unless otherwise indicated." />  
					</em>
				</label> --%>
			</div>
			
			<%-- <div class="control-group margin30-t">
				<label class="span9">
					<strong>
						<img alt="Time estimate" src="<c:url value="/resources/images/icons/2icon1.png" />" class="arrow_ui_design" />
						<spring:message code="ssap.page2.youMayNeed10" text="Estimated time for this section: 45 Minutes &ndash; 60 Minutes" />
					</strong>
				</label>
				
				<img alt="help" src="<c:url value="/resources/images/icons/2button.png" />" />
			</div> --%>
		</div>
	</div>
</div>