/**
 * 
 */
package com.getinsured.hix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.TenantPlan;

/**
 * @author gorai_k
 *
 */
public interface ITenantPlanRepository extends JpaRepository<TenantPlan, Integer>, RevisionRepository<TenantPlan, Integer, Integer>{

	@Query("SELECT tp FROM TenantPlan tp WHERE tp.tenant.id= :tenantId and tp.plan.id= :planid")
	TenantPlan getTenantPlanByTenantId(@Param("tenantId") Long tenantId,@Param("planid") int planid);
}
