package com.getinsured.eligibility.redetermination.service;

public interface RenewalNoticeService {
	
	String triggerRenewalNotice(String caseNumber,String templateName,String fileName);

}
