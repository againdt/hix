package com.getinsured.fars2.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.fars2.cms.dsh.ridp.extension._1.IDProofedResponseType;
import com.getinsured.fars2.cms.dsh.ridp.extension._1.ResponsePayloadType;


public class FarsResponsePayloadTypeWrapper implements JSONAware {
	
	private ResponseMetadataWrapper responseMetadata;
	private String finalDecisionCode;
	
	public static final String RESPONSE_METADATA_KEY = "ResponseMetadata";
	public static final String FINAL_DECISION_CODE_KEY = "FinalDecisionCode";
	
	public FarsResponsePayloadTypeWrapper(ResponsePayloadType responsePayloadType){
		
		if(responsePayloadType == null){
			return;
		}
		
		this.responseMetadata = new ResponseMetadataWrapper(responsePayloadType.getResponseMetadata());
		IDProofedResponseType idProofResponse = responsePayloadType.getIDProofedResponse();
		if(idProofResponse != null){
			this.finalDecisionCode = idProofResponse.getFinalDecisionCode().value();
		}
		
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public String getFinalDecisionCode() {
		return finalDecisionCode;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.responseMetadata != null){
			obj.put(RESPONSE_METADATA_KEY, this.responseMetadata);
		}
		obj.put(FINAL_DECISION_CODE_KEY, this.finalDecisionCode);
		return obj.toJSONString();
	}
}
