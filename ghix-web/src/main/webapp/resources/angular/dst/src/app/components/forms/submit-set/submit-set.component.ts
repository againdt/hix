import { DataKeepService } from './../../../services/data-keep.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
    selector: 'app-submit-set',
    templateUrl: './submit-set.component.html',
    styleUrls: ['./submit-set.component.scss']
})
export class SubmitSetComponent implements OnInit {

    public disableSubmit = false;
    constructor(
        public datakeep: DataKeepService,
    ) {
    }

    ngOnInit() {

    }

    public redirect1(): void {
        let url: string;
        try {
            url = (<any>document.querySelector("#jsp_dstCancelRedirectUrl")).value;
        } catch (err) {
            url = ""
        }
        window.location.href = url
    }

    public redirect(): void {
        if(this.datakeep.dosageCard){
           let url = location.href;
            url = url.indexOf("#pres-search-field")>0 ? url  : url + "#pres-search-field";
            location.href = url;
            this.datakeep.publishData(true);
            this.datakeep.readErrorMessage("dosage-card", "select-dosage-alert");
            //console.log("submit alert");
        }else
        {
            this.datakeep.scoreMyPlan().subscribe(
                response => {
                    if (response.status === 200 && (<any>response)._body === "SUCCESS")
                    {
                        let url: string;
                        try {
                            url = (<any>document.querySelector("#jsp_dstSubmitRedirectUrl")).value;
                        } catch (err) {
                            url = "";
                        }
                        this.disableSubmit = true;
                        window.location.href = url;
                    } else {
                        console.error(response);
                    }
                },
                err => {
                    console.error(err);
                }
            )
        }
    }

}
