export default {
    getAdjustedPhoneNumber: phone => {
        if(phone === null) {
            return '';
        }
        const phoneToString = String(phone);
        if(phoneToString.length === 10 && phoneToString.match(/^[0-9]/)) {
            return `(${phoneToString.slice(0, 3)}) ${phoneToString.slice(3, 6)}-${phoneToString.slice(6)}`;
        } else {
            return phoneToString;
        }

    },

    cleanedUpPhone: phone => {
        return typeof phone === 'string' ?
            phone.split('').filter(ch => ch.match(/^[0-9]*$/)).join('') : null;
    },

    autoFillPhone: (phone, previousPhoneValue) => {
        let result;
        switch(true) {
        case phone.length < previousPhoneValue.length:
            switch (phone.length) {
            case 1:
                result = ''; break;
            case 5:
                result = phone.slice(0, phone.length - 1); break;
            case 6:
                result = phone.slice(0, phone.length - 2); break;
            case 10:
                result = phone.slice(0, phone.length - 1); break;
            default:
                result = phone;
            } break;
        case phone.length > previousPhoneValue.length:
            switch (phone.length) {
            case 1:
                result = `(${phone}`; break;
            case 4:
                result = `${phone}) `; break;
            case 5:
                result = `${phone.slice(0, phone.length - 1)}) ${phone.slice(-1)}`; break;
            case 9:
                result = `${phone}-`; break;
            case 10:
                result = `${phone.slice(0, phone.length - 1)}-${phone.slice(-1)}`; break;
            default:
                result = phone;
            } break;
        default:
            result = phone; break;
        }
        return result;
    }
};