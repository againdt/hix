package com.getinsured.hix.ssap.util

import com.getinsured.iex.ssap.HouseholdMember
import com.getinsured.iex.ssap.Name
import com.getinsured.ssap.util.BloodRelationshipCode
import com.getinsured.ssap.util.FamilyBuilder
import com.getinsured.ssap.util.HouseholdUtil
import org.junit.Assert
import org.junit.Test
import java.util.*

/**
 * @author Yevgen Golubenko
 * @since 6/20/19
 */
class HouseholdUtilTest {

  @Test
  fun membersLivingWith() {
    val member = HouseholdMember()
    val member2 = HouseholdMember()
    val member3 = HouseholdMember()
    val member4 = HouseholdMember()
    val member5 = HouseholdMember()

    val name = Name()
    val name2 = Name()
    val name3 = Name()
    val name4 = Name()
    val name5 = Name()

    name.firstName = "Member 1 First Name"
    name.lastName = "Member 1 Last Name"
    name2.firstName = "Member 2 First Name"
    name2.lastName = "Member 2 Last Name"
    name3.firstName = "Member 3 First Name"
    name3.lastName = "Member 3 Last Name"
    name4.firstName = "Member 4 First Name"
    name4.lastName = "Member 4 Last Name"
    name5.firstName = "Member 5 First Name"
    name5.lastName = "Member 5 Last Name"

    member.name = name
    member2.name = name
    member3.name = name
    member4.name = name
    member5.name = name

    member.personId = 1
    member2.personId = 2
    member3.personId = 3
    member4.personId = 4
    member5.personId = 5

    member.addressId = 1
    member2.addressId = 1
    member3.addressId = 1
    member4.addressId = 2
    member5.addressId = 1

    val members = listOf(member, member2, member3, member4, member5)

    var livingWith = HouseholdUtil.membersLivingWith(members, member)
    // - given member, - # members not living with given member.
    Assert.assertEquals("# of members living with is not correct", 3, livingWith.size)

    Assert.assertNull("1 member found, but shouldn't be", livingWith.find { it.personId == member.personId })
    Assert.assertNotNull("2 member not found", livingWith.find { it.personId == member2.personId })
    Assert.assertNotNull("3 member not found", livingWith.find { it.personId == member3.personId })
    Assert.assertNull("4 member found, but shouldn't be", livingWith.find { it.personId == member4.personId })
    Assert.assertNotNull("5 member not found", livingWith.find { it.personId == member5.personId })


    livingWith = HouseholdUtil.membersLivingWith(members, member4)
    // - given member, - # members not living with given member.
    Assert.assertEquals("# of members living with is not correct", 0, livingWith.size)

    Assert.assertNull("1 member found", livingWith.find { it.personId == member.personId })
    Assert.assertNull("2 member found", livingWith.find { it.personId == member2.personId })
    Assert.assertNull("3 member found", livingWith.find { it.personId == member3.personId })
    Assert.assertNull("4 member found", livingWith.find { it.personId == member4.personId })
    Assert.assertNull("5 member found", livingWith.find { it.personId == member5.personId })
  }

  @Test
  fun membersNotLivingWith() {
    val member = HouseholdMember()
    val member2 = HouseholdMember()
    val member3 = HouseholdMember()
    val member4 = HouseholdMember()
    val member5 = HouseholdMember()

    val name = Name()
    val name2 = Name()
    val name3 = Name()
    val name4 = Name()
    val name5 = Name()

    name.firstName = "Member 1 First Name"
    name.lastName = "Member 1 Last Name"
    name2.firstName = "Member 2 First Name"
    name2.lastName = "Member 2 Last Name"
    name3.firstName = "Member 3 First Name"
    name3.lastName = "Member 3 Last Name"
    name4.firstName = "Member 4 First Name"
    name4.lastName = "Member 4 Last Name"
    name5.firstName = "Member 5 First Name"
    name5.lastName = "Member 5 Last Name"

    member.name = name
    member2.name = name
    member3.name = name
    member4.name = name
    member5.name = name

    member.personId = 1
    member2.personId = 2
    member3.personId = 3
    member4.personId = 4
    member5.personId = 5

    member.addressId = 1
    member2.addressId = 1
    member3.addressId = 1
    member4.addressId = 2
    member5.addressId = 1

    val members = listOf(member, member2, member3, member4, member5)

    var notLivingWith = HouseholdUtil.membersNotLivingWith(members, member)
    // - given member, - # members not living with given member.
    Assert.assertEquals("# of members living with is not correct", 1, notLivingWith.size)

    Assert.assertNull("1 member found ${member.name.firstName} ${member.name.lastName}", notLivingWith.find { it.personId == member.personId })
    Assert.assertNull("2 member found ${member2.name.firstName} ${member2.name.lastName}", notLivingWith.find { it.personId == member2.personId })
    Assert.assertNull("3 member found ${member3.name.firstName} ${member3.name.lastName}", notLivingWith.find { it.personId == member3.personId })
    Assert.assertNotNull("4 member not found ${member4.name.firstName} ${member4.name.lastName}", notLivingWith.find { it.personId == member4.personId })
    Assert.assertNull("5 member  found ${member5.name.firstName} ${member5.name.lastName}", notLivingWith.find { it.personId == member5.personId })

    notLivingWith = HouseholdUtil.membersNotLivingWith(members, member4)
    // - given member, - # members not living with given member.
    Assert.assertEquals("# of members living with is not correct", 4, notLivingWith.size)

    Assert.assertNotNull("1 member found ${member.name.firstName} ${member.name.lastName}", notLivingWith.find { it.personId == member.personId })
    Assert.assertNotNull("2 member found ${member2.name.firstName} ${member2.name.lastName}", notLivingWith.find { it.personId == member2.personId })
    Assert.assertNotNull("3 member found ${member3.name.firstName} ${member3.name.lastName}", notLivingWith.find { it.personId == member3.personId })
    Assert.assertNull("4 member found ${member4.name.firstName} ${member4.name.lastName}", notLivingWith.find { it.personId == member4.personId })
    Assert.assertNotNull("5 member  found ${member5.name.firstName} ${member5.name.lastName}", notLivingWith.find { it.personId == member5.personId })
  }

  @Test
  fun isLivingWith() {
    val members: List<HouseholdMember> = FamilyBuilder.buildTestCase_E_Household();

    var living: Boolean = HouseholdUtil.isLivingWith(members, members[2], BloodRelationshipCode.PARENT_OF_CHILD, 1)
    Assert.assertTrue("Child should live with the Father", living)

    living = HouseholdUtil.isLivingWith(members, members[2], BloodRelationshipCode.UNSPECIFIED_RELATIONSHIP, 1)
    Assert.assertFalse("Child should live with the UNSPECIFIED_RELATIONSHIP", living)

    living = HouseholdUtil.isLivingWith(members, members[1], BloodRelationshipCode.CHILD, 1)
    Assert.assertTrue("Father should live with his child", living)

    living = HouseholdUtil.isLivingWith(members, members[1], BloodRelationshipCode.PARENT_OF_CHILD, 1)
    Assert.assertFalse("Father should not live with this Father (Grandpa)", living)

    living = HouseholdUtil.isLivingWith(members, members[0], BloodRelationshipCode.CHILD, 1)
    Assert.assertFalse("Grand Father should not live with this son", living)

    living = HouseholdUtil.isLivingWith(members, members[0], BloodRelationshipCode.GRANDCHILD, 1)
    Assert.assertFalse("Grand Father should not live with this grand-child", living)
  }

  @Test
  fun getRelationBetween() {
    val members: List<HouseholdMember> = FamilyBuilder.buildTestCase_E_Household();

    // Grandpa => Son
    var relationship = HouseholdUtil.getRelationOf(members, members[0], members[1])
    Assert.assertEquals("Relationship should be CHILD", BloodRelationshipCode.CHILD, relationship)

    // Grandpa => Grandchild
    relationship = HouseholdUtil.getRelationOf(members, members[0], members[2])
    Assert.assertEquals("Relationship should be GRANDCHILD", BloodRelationshipCode.GRANDCHILD, relationship)

    // Son => Grandpa
    relationship = HouseholdUtil.getRelationOf(members, members[1], members[0])
    Assert.assertEquals("Relationship should be PARENT_OF_CHILD", BloodRelationshipCode.PARENT_OF_CHILD, relationship)

    // Son => Grandson (child of son)
    relationship = HouseholdUtil.getRelationOf(members, members[1], members[2])
    Assert.assertEquals("Relationship should be CHILD", BloodRelationshipCode.CHILD, relationship)

    // Grandchild => Grandpa
    relationship = HouseholdUtil.getRelationOf(members, members[2], members[0])
    Assert.assertEquals("Relationship should be GRANDPARENT", BloodRelationshipCode.GRANDPARENT, relationship)

    // Grandchild => Son (their Father)
    relationship = HouseholdUtil.getRelationOf(members, members[2], members[1])
    Assert.assertEquals("Relationship should be CHILD", BloodRelationshipCode.PARENT_OF_CHILD, relationship)

    // Grandpa => Grandpa
    relationship = HouseholdUtil.getRelationOf(members, members[0], members[0]);
    Assert.assertEquals("Relationship should be SELF", BloodRelationshipCode.SELF, relationship)
  }

  @Test
  fun medicaidChipVerificationExclusion() {
    val application = FamilyBuilder.sampleApplicationFromJson("ssap-1-MedicaidChipEligible.json")
    val taxHousehold = application.taxHousehold[0];

    val membersForVerificationExceptMedicareChip = HouseholdUtil.filterHouseholdMembersForVerifications(application)
    Assert.assertNotNull("There was no members for verification", membersForVerificationExceptMedicareChip)

    // In JSON, applicant id 9202 is CHIP eligible, and they have not selected to still shop for QHP
    Assert.assertEquals("Should be only 3 members for verification", 3, membersForVerificationExceptMedicareChip.size)

    // Now add 9202 applicant (guid 1000003983) to the array to simulate that they want to shop for QHP
    val chipEligibleApplicant = Optional.ofNullable(taxHousehold.householdMember.find { hm -> hm.applicantGuid == "1000003983" }).orElse(null)
    Assert.assertNotNull("Could not find CHIP eligible applicant", chipEligibleApplicant)
    chipEligibleApplicant.isSeeksQhp = true

    // Now we should pass all members to verifications.
    val membersForVerificationIncludingMedicareChip = HouseholdUtil.filterHouseholdMembersForVerifications(application)
    Assert.assertNotNull("There was no members for verification", membersForVerificationIncludingMedicareChip)
    Assert.assertEquals("Number of members for verification should be 4", 4, membersForVerificationIncludingMedicareChip.size)
  }
}
