package com.getinsured.hix.batch.enrollment.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.enrollment.repository.IEnrollment1095Repository;
import com.getinsured.hix.enrollment.repository.IEnrollmentOut1095Repository;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095.MemberType;
import com.getinsured.hix.model.enrollment.EnrollmentOut1095;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Abstract class for 1095 XML generation
 * @author negi_s
 *
 */
public abstract class Enrollment1095Xml {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095Xml.class);

	protected static final String SUCCESS = "SUCCESS";
	protected static final String SKIPPED = "SKIPPED";
	protected static final String ZERO_AMOUNT = "0.00";

	@Autowired
	protected IEnrollment1095Repository enrollment1095Repository;
	@Autowired
	protected IEnrollmentOut1095Repository enrollmentOut1095Repository;

	/**
	 * Generate 1095 XML
	 * @param enrollment1095Ids
	 * @param docSeqId
	 * @param wipFolder
	 * @param recordSequenceIdMap
	 * @param enrollment1095XmlWrapper
	 * @param batchCategoryCode
	 * @return
	 * @throws GIException
	 */
	abstract public String generateXml(List<Integer> enrollment1095Ids, Integer docSeqId, String wipFolder,
			Map<Integer, String> recordSequenceIdMap, Enrollment1095XmlWrapper enrollment1095XmlWrapper,
			String batchCategoryCode) throws GIException;

	/**
	 * Convert normal date to XMLGregorianCalendar date
	 * 
	 * @param date
	 * @return XMLGregorianCalendar
	 */
	protected XMLGregorianCalendar getXmlGregorianCalendarDate(Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		XMLGregorianCalendar xmlGregorianDate = null;
		cal.setTime(date);
		try {
			xmlGregorianDate = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(cal.get(Calendar.YEAR),
					cal.get(Calendar.MONTH) + 1, cal.get(Calendar.DAY_OF_MONTH), DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException e) {
			LOGGER.error("Error converting to XMLGregorianCalendar Date", e);
		}
		return xmlGregorianDate;
	}

	/**
	 * Get members based on type
	 * 
	 * @param enrollmentMembers
	 * @param memberType
	 * @return
	 */
	protected List<EnrollmentMember1095> getMemberFromList(List<EnrollmentMember1095> enrollmentMembers,
			MemberType memberType) {
		List<EnrollmentMember1095> members = new ArrayList<EnrollmentMember1095>();
		for (EnrollmentMember1095 member : enrollmentMembers) {
			if (memberType.toString().equalsIgnoreCase(member.getMemberType())) {
				members.add(member);
			}
		}
		return members;
	}

	/**
	 * Converts float to BigDecimal
	 * 
	 * @param amount
	 * @return
	 */
	protected String getBigDecimalStringFromFloat(Float amount) {

		BigDecimal bigDecimalAmount = null;
		String bdString = null;
		if (amount != null) {
			try {
				bigDecimalAmount = new BigDecimal(Float.toString(amount)).setScale(2, BigDecimal.ROUND_HALF_UP);
				bdString = bigDecimalAmount.toPlainString();
			} catch (Exception e) {
				LOGGER.error("Error parsing float amount", e);
			}
		}
		return bdString;
	}

	/**
	 * Remove occurrences of period and extra spaces
	 * 
	 * @param str
	 * @return String null if input string is null
	 */
	protected String removePeriodAndExtraSpaces(String str) {
		String result = null;
		if (StringUtils.isNotBlank(str)) {
			result = EnrollmentUtils.removeExtraSpaces(str).replaceAll("\\.", "");
		}
		return result;
	}

	/**
	 * Sets the IRS file details into the IRS Outbound Transmission table
	 * 
	 * @param householdIds
	 * @param generatedFileName
	 * @param enrollment1095XmlWrapper
	 */
	protected void logEnrollmentOut1095(String enrollmentIds, String generatedFileName,
			Enrollment1095XmlWrapper enrollment1095XmlWrapper) {

		EnrollmentOut1095 enrollmentOut1095 = new EnrollmentOut1095();
		enrollmentOut1095.setDocumentFileName(generatedFileName);
		enrollmentOut1095.setEnrollmentIds(enrollmentIds);
		enrollmentOut1095.setBatchId(enrollment1095XmlWrapper.getBatchId());
		if (enrollment1095XmlWrapper.isFreshRun()) {
			enrollmentOut1095.setSubmissionType("I");// Single Character
		} else {
			enrollmentOut1095.setSubmissionType("R");
		}
		try {
			enrollmentOut1095Repository.save(enrollmentOut1095);
		} catch (Exception e) {
			LOGGER.error("Error saving data in IRSOutBoundTransmission table @ logIRSOutBoundTransmissionInformation",
					e);
		}
	}

	/**
	 * Update Staging table with XML generation results
	 * @param successSkipMap
	 * @param generatedFileName
	 * @param docSeqId
	 * @param recordSequenceIdMap
	 * @param batchCategoryCode
	 * @param enrollment1095XmlWrapper
	 */
	protected void updateStagingTable(Map<String, List<Enrollment1095>> successSkipMap, String generatedFileName,
			Integer docSeqId, Map<Integer, String> recordSequenceIdMap, String batchCategoryCode,
			Enrollment1095XmlWrapper enrollment1095XmlWrapper) {
		Date generationDate = new Date();
		List<Enrollment1095> successList = successSkipMap.get(SUCCESS);
		List<Enrollment1095> skipList = successSkipMap.get(SKIPPED);
		enrollment1095XmlWrapper.incrementRecordCount(successList.size());
		enrollment1095XmlWrapper.incrementSkippedCount(skipList.size());

		// Update successful enrollments
		for (Enrollment1095 enrollment1095 : successList) {
			enrollment1095.setCmsXmlFileName(generatedFileName);
			enrollment1095.setCmsXmlGeneratedOn(generationDate);
			if (null == enrollment1095.getOriginalBatchId()) {
				enrollment1095.setOriginalBatchId(enrollment1095XmlWrapper.getBatchId());
			}
			enrollment1095.setCorrectionIndicatorXml(null);
			// enrollment1095.setInboundErrIndicator(null);
			enrollment1095.setCorrectedRecordSeqNum(enrollment1095XmlWrapper.getBatchId() + "|"
					+ String.format("%05d", docSeqId + 1) + "|" + recordSequenceIdMap.get(enrollment1095.getId()));
			enrollment1095.setBatchCategoryCode(batchCategoryCode);
			if ("MANUAL_CORRECTION".equalsIgnoreCase(enrollment1095.getInboundAction())
					|| "REGEN_XML".equalsIgnoreCase(enrollment1095.getInboundAction())
					|| "REGEN_ZIP".equalsIgnoreCase(enrollment1095.getInboundAction())) {
				enrollment1095.setInboundAction("ACTION_TAKEN");
			}
			enrollment1095.setResubCorrectionInd(null);
		}
		try {
			enrollment1095Repository.save(successList);
			enrollment1095Repository.save(skipList);
		} catch (Exception e) {
			LOGGER.error("Error updateStagingTable @ Enrollment1095XmlBatchServiceImpl", e);
		}
	}

	/**
	 * 
	 * Returns Current Year
	 * 
	 * @author Aditya-S
	 * @since 30-07-2014
	 * 
	 * @return
	 */
	protected XMLGregorianCalendar getSubmissionYear(int year) {

		// Calendar calender = Calendar.getInstance();
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(GregorianCalendar.YEAR, year);
		XMLGregorianCalendar xmlGregorianCalendar = null;
		try {
			xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendarDate(year,
					DatatypeConstants.FIELD_UNDEFINED, DatatypeConstants.FIELD_UNDEFINED,
					DatatypeConstants.FIELD_UNDEFINED);
		} catch (DatatypeConfigurationException dex) {
			LOGGER.error("DatatypeConfigurationException @ getSubmissionYear", dex);
		}
		return xmlGregorianCalendar;
	}

	/**
	 * Converts float to BigDecimal
	 * 
	 * @param amount
	 * @return
	 */
	protected BigDecimal getBigDecimalFromFloat(Float amount) {

		BigDecimal bigDecimalAmount = null;
		if (amount != null) {
			try {
				bigDecimalAmount = new BigDecimal(Float.toString(amount)).setScale(2, BigDecimal.ROUND_HALF_UP);
			} catch (Exception e) {
				LOGGER.error("Error parsing float amount", e);
			}
		}
		return bigDecimalAmount;
	}
}
