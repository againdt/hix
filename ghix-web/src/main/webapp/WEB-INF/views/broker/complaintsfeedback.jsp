<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip" class="skip">Feedback and Complaints</a></h1> 
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
		<div class="header">
			<h4>Agent Home</h4>
		</div>
		<!--  beginning of side bar -->
			<ul class="nav nav-list">
				<li><a href="../Agent/notifications">Notifications</a></li>
				<li><a href="../broker/viewprofile">Profile</a></li>
				<li><a href="../broker/certificationstatus">Certification Status</a></li>
				<li class="active"><a href="../broker/complaintsfeedback">Complaints and Feedback</a></li>
			</ul><!-- end of side bar -->

		</div><!-- end of span3 -->
		<div class="span9" id="rightpanel">
			
			<p class="gutter10">Please fill out this form to file your complaint or send feedback to the State Health Benefit Exchange.</p>
			<form class="form-vertical" id="frmasstrfeedback" name="frmasstrfeedback" action="complaintsfeedback" method="POST">
				<div class="control-group feedback-item">
					<div class="controls">
						<fieldset>
							<legend class="hidden">feedback type</legend>
							<ul class="hztl-list feedback-type">
								<li>
									<label class="radio" for="optionsRadios1">
										<input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked>
										Feedback
									</label>
								</li>
								<li>
									<label class="radio" for="optionsRadios2">
										<input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
										Complaint
									</label>
								</li>
							</ul>
						</fieldset>
					</div><!-- end of controls -->
				</div><!--end of control-group -->

				<div class="control-group feedback-item">
					<label class="control-label" for="complaint-relation">What is this feedback or complaint related to?</label>
					<div class="controls docs-input-sizes">
						<select class="span3" id="complaint-relation">
							<option>HIX</option>
							<option>Agent</option>
							<option>Health Plan</option>
							<option>Carrier</option>
							<option>Other</option>
						</select>
					</div><!-- end of control -->
				</div><!-- end of control-group -->

				<div class="control-group">
					<label class="control-label" for="feedback-additions">Please provide additional details and relevant information.</label>
					<textarea cols="500" rows="5" class="feedback-additions span12" id="feedback-additions">Please type here.</textarea>
				</div>

				<div class="form-actions">
					<input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" title="Submit"/> 
				</div><!-- end of form-actions -->
			</form>

		</div><!-- end of span9 -->
		<!-- Agent registration -->
	</div><!--  end of row-fluid -->
</div>
