
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class MedicaidInsurance {

    @Expose
    private Boolean requestHelpPayingMedicalBillsLast3MonthsIndicator=false;

    public Boolean getRequestHelpPayingMedicalBillsLast3MonthsIndicator() {
        return requestHelpPayingMedicalBillsLast3MonthsIndicator;
    }

    public void setRequestHelpPayingMedicalBillsLast3MonthsIndicator(Boolean requestHelpPayingMedicalBillsLast3MonthsIndicator) {
        this.requestHelpPayingMedicalBillsLast3MonthsIndicator = requestHelpPayingMedicalBillsLast3MonthsIndicator;
    }

}
