import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DocWhoComponent } from './doc-who.component';

describe('DocWhoComponent', () => {
    let component: DocWhoComponent;
    let fixture: ComponentFixture<DocWhoComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [DocWhoComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(DocWhoComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it("should have a length of 0 for listedDoctor at the start", () => {
        expect(component.listedDoctor.length === 0)
    })
});
