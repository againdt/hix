<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<c:set var="encPaymentId" ><encryptor:enc value="${paymentMethodsObj.id}" isurl="true"/> </c:set>
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
        	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.financialInformation"/></li>
        </ul><!--page-breadcrumb ends-->
		<%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
		<h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
    </div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/issuerprofile" />"><spring:message  code="pgheader.issuerProfile"/></a></li>
                    <c:if test="${STATE_CODE != 'ID' && STATE_CODE != 'MN'}">
                    	<li class="active"><a href="#"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info" />"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info" />"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
                    <%--  <c:if test="${STATE_CODE != 'ID'}">
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/shop/info" />"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    </c:if> --%>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/view" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/certification/status" />"><spring:message  code="pgheader.certificationStatus"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/viewhistory" />"><spring:message  code="pgheader.issuerHistory"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/crossWalkStatus" />"><spring:message  code="pgheader.planIdCrosswalk"/></a></li>
                    <c:if test="${STATE_CODE == 'ID'}">
                    	<li><a href="<c:url value="/planmgmt/issuer/displayNetworkTransparencyData" />"><spring:message code="pgheader.networkTransparencyStatus"/></a></li>
                    </c:if>
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="span9"><spring:message  code="pgheader.financialInformation"/></h4>
                    <a class="btn btn-small" href="<c:url value="/planmgmt/issuer/account/profile/financial/info/list"/>"><spring:message  code="label.cancel"/></a> 
                    <a class="btn btn-primary btn-small margin5-lr" href="<c:url value="/planmgmt/issuer/account/profile/financial/edit/${encPaymentId}"/>"><spring:message  code="label.edit"/></a>
                </div>
				<div class="gutter10">

				<form class="form-horizontal" id="frmIssuerAcctFinInfo" name="frmIssuerAcctFinInfo" action="#" method="POST">
                    <div class="control-group">
						<label class="control-label" for="account-name"><spring:message  code="label.paymentMethodName"/></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.paymentMethodName}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-name"><spring:message  code="label.bankName"/></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.bankName}</strong></div>
					</div><!-- end of control-group-->                    
                   
                   <div class="control-group">
						<label class="control-label" for="bank-routing-num"><spring:message  code="label.abaRoutingNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.routingNumber}</strong></div>
					</div><!-- end of control-group-->
					
                    <div class="control-group">
						<label class="control-label" for="bank-acc-num"><spring:message  code="label.bankAcctNumber"/><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.accountNumber}</strong></div>
					</div><!-- end of control-group-->
					
					<div class="control-group">
						<label class="control-label" for="bank-acc-num"><spring:message code="label.nameOnAccount" /></label>
						<div class="controls paddingT5"><strong>${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}</strong></div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="bank-routing-num"><spring:message  code="label.bankAcctType"/></label>
						<div class="controls paddingT5"><strong>
							<c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> <spring:message code="label.bankAcctTypeChecking"/> </c:if>
							<c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> <spring:message code="label.bankAcctTypeSaving"/> </c:if>						
						</strong></div>
					</div><!-- end of control-group-->
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript">
$('.code').popover({ html: true,
	  trigger: 'hover',
	  placement: 'top',
	  content: function(){return '<img src="'+$(this).data('img') + '" />';}
});
</script>
