package com.getinsured.eligibility.ssap.pdf.generation;

public class HouseholdMembersDTO {
	
	private String firstName;

	private String middleName;

	private String lastName;
	
	private String fullName;
	private String suffix;
	
	private String dateOfBirth;
	
	private String gender;
	
	private String ssn;
	
	private String nameOnSsnCard;
	private String citizenshipStatusIndicator;
	private String planToFileFTRIndicator;
	private String homeAddress;
	private String mailingAddress;
	
	
	
	private String applyingForCoverageIndicator;
	private String relationshipWithPrimary;
	
	private String spouseInformation;
	
	


	private String taxdependentApplyingfor;
	
	private String requestHelpPayingMedicalBillsLast3MonthsIndicator;
	private String americanIndianAlaskaNativeIndicator;
	private String pregnantIndicator;
	
	private String disabilityIndicator;
	
	
	public String getFirstName() {
		return firstName;
	}


	public String getRequestHelpPayingMedicalBillsLast3MonthsIndicator() {
		return requestHelpPayingMedicalBillsLast3MonthsIndicator;
	}


	public void setRequestHelpPayingMedicalBillsLast3MonthsIndicator(String requestHelpPayingMedicalBillsLast3MonthsIndicator) {
		this.requestHelpPayingMedicalBillsLast3MonthsIndicator = requestHelpPayingMedicalBillsLast3MonthsIndicator;
	}


	public String getAmericanIndianAlaskaNativeIndicator() {
		return americanIndianAlaskaNativeIndicator;
	}


	public void setAmericanIndianAlaskaNativeIndicator(String americanIndianAlaskaNativeIndicator) {
		this.americanIndianAlaskaNativeIndicator = americanIndianAlaskaNativeIndicator;
	}


	public String getPregnantIndicator() {
		return pregnantIndicator;
	}


	public void setPregnantIndicator(String pregnantIndicator) {
		this.pregnantIndicator = pregnantIndicator;
	}


	public String getDisabilityIndicator() {
		return disabilityIndicator;
	}


	public void setDisabilityIndicator(String disabilityIndicator) {
		this.disabilityIndicator = disabilityIndicator;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getMiddleName() {
		return middleName;
	}


	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}


	public String getLastName() {
		return lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getGender() {
		return gender;
	}


	public void setGender(String gender) {
		this.gender = gender;
	}


	public String getSsn() {
		return ssn;
	}


	public void setSsn(String ssn) {
		this.ssn = ssn;
	}


	public String getNameOnSsnCard() {
		return nameOnSsnCard;
	}


	public void setNameOnSsnCard(String nameOnSsnCard) {
		this.nameOnSsnCard = nameOnSsnCard;
	}


	public String getCitizenshipStatusIndicator() {
		return citizenshipStatusIndicator;
	}


	public void setCitizenshipStatusIndicator(String citizenshipStatusIndicator) {
		this.citizenshipStatusIndicator = citizenshipStatusIndicator;
	}


	public String getPlanToFileFTRIndicator() {
		return planToFileFTRIndicator;
	}


	public void setPlanToFileFTRIndicator(String planToFileFTRIndicator) {
		this.planToFileFTRIndicator = planToFileFTRIndicator;
	}


	public String getHomeAddress() {
		return homeAddress;
	}


	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}


	public String getMailingAddress() {
		return mailingAddress;
	}


	public void setMailingAddress(String mailingAddress) {
		this.mailingAddress = mailingAddress;
	}


	public String getSpouseInformation() {
		return spouseInformation;
	}



	public void setSpouseInformation(String spouseInformation) {
		this.spouseInformation = spouseInformation;
	}

	public String getApplyingForCoverageIndicator() {
		return applyingForCoverageIndicator;
	}


	public void setApplyingForCoverageIndicator(String applyingForCoverageIndicator) {
		this.applyingForCoverageIndicator = applyingForCoverageIndicator;
	}


	


	public void setSuffix(String suffix) {
		this.suffix = suffix;
		// TODO Auto-generated method stub
		
	}


	public String getSuffix() {
		return suffix;
	}


	public String getRelationshipWithPrimary() {
		return relationshipWithPrimary;
	}


	public void setRelationshipWithPrimary(String relationshipWithPrimary) {
		this.relationshipWithPrimary = relationshipWithPrimary;
	}


	public String getFullName() {
		return fullName;
	}


	public void setFullName(String fullName) {
		this.fullName = fullName;
	}


	public String getTaxdependentApplyingfor() {
		return taxdependentApplyingfor;
	}


	public void setTaxdependentApplyingfor(String taxdependentApplyingfor) {
		this.taxdependentApplyingfor = taxdependentApplyingfor;
	}


	
	
	
}
