import React from 'react';
import { FormattedMessage } from 'react-intl';
import {
    Accordion,
    AccordionItem,
    AccordionItemHeading,
    AccordionItemPanel,
    AccordionItemButton,
    AccordionItemState
} from 'react-accessible-accordion';

import '../assets/Verification.css';
import { VERIFICATION_DATA } from '../constants/constants';

const Verification = (props) => {
    const {
        applicants,
        issuers,
        validationStatus,
        isValidationStatusVerified,
        selectedApplicantData,
        errors,
        showOverride,
        onClickHandlerBtnBack,
        onActionHandler,
        onChangeHandler
    } = props;

    const { ISSUER_CODE, GROUP_NUMBER, POLICY_NUMBER, ACTION_SUBMIT, ACTION_SKIP, ACTION_OVERRIDE } = VERIFICATION_DATA;

    return (
        <div className="addition-info__wrap">
            <a className="addition-info__link" onClick={onClickHandlerBtnBack}><i
                className="icon-chevron-sign-left"/>
                <FormattedMessage id="verification.back_to_dashboard"/>
            </a>
            <h4 className="addition-info__header">
                <FormattedMessage id="verification.for_household"/>
            </h4>
            <div className="addition-info__content">
                <aside className="addition-info__aside">
                    <ul className="addition-info__applicants">
                        {applicants}
                    </ul>
                </aside>
                <section className="addition-info__tab-wrap">
                    <h3 className="addition-info__tab-title">
                        <FormattedMessage id="verification.QLE"/>
                    </h3>
                    <div className="addition-info__tab-content">
                        {!isValidationStatusVerified ?
                            <Accordion allowMultipleExpanded={true} allowZeroExpanded={true}
                                       preExpanded={['loss-MEC-0']}>
                                <AccordionItem uuid="loss-MEC-0">
                                    <AccordionItemHeading>
                                        <AccordionItemState>
                                            {({ expanded }) => (
                                                <AccordionItemButton>
                                                    <div>
                                                        <h5 className="addition-info__accordion-title">
                                                            <i className={expanded ? 'icon--mr icon-minus-sign' : 'icon--mr icon-plus-sign'}/>
                                                            <span><FormattedMessage
                                                                id="verification.loss_of_MEC"/></span>
                                                            <span> ({validationStatus}) </span>
                                                            <i className={'icon--ml icon-exclamation-sign'}/>
                                                        </h5>
                                                        {showOverride &&
                                                        <button
                                                            type="submit"
                                                            className="ds-c-button--danger addition-info__btn addition-info__btn--override"
                                                            onClick={(e) => {
                                                                onActionHandler(e, ACTION_OVERRIDE)
                                                            }}
                                                        >
                                                            <i className='icon--mr icon-edit-sign'/>
                                                            <FormattedMessage id="verification.btn_override"/>
                                                        </button>
                                                        }

                                                    </div>
                                                </AccordionItemButton>
                                            )}
                                        </AccordionItemState>
                                    </AccordionItemHeading>
                                    <AccordionItemPanel>
                                        <p className="ds-text">
                                            <FormattedMessage id="verification.prove_addition_info"/>
                                        </p>
                                        <form className="addition-info__form"
                                              onSubmit={(e) => {
                                                  onActionHandler(e, ACTION_SUBMIT)
                                              }}
                                              noValidate={true}
                                        >
                                            <div className="input-label-wrap">
                                                <label htmlFor="auto_issuer_name">
                                                    <FormattedMessage id="verification.issuer_name"/>
                                                    <i className="icon-asterisk"/>
                                                </label>
                                                <div className="input-wrap">
                                                    <select id="auto_issuer_name"
                                                            className={errors[ISSUER_CODE] ? 'addition-info__input addition-info__input--invalid' : 'addition-info__input'}
                                                            onChange={onChangeHandler}
                                                            value={selectedApplicantData[ISSUER_CODE] ?
                                                                selectedApplicantData[ISSUER_CODE] : ''}
                                                            name={ISSUER_CODE}
                                                            required
                                                    >
                                                        <option value="" name={ISSUER_CODE}>
                                                            Select One
                                                        </option>
                                                        {issuers}
                                                    </select>
                                                    {errors[ISSUER_CODE] &&
                                                    <p className="error-msg">{errors[ISSUER_CODE]}</p>}
                                                </div>
                                            </div>
                                            <div className="input-label-wrap">
                                                <label htmlFor="auto_group_number">
                                                    <FormattedMessage id="verification.group_number"/>
                                                    <i className="icon-asterisk"/>
                                                </label>
                                                <div className="input-wrap">
                                                    <input
                                                        id="auto_group_number"
                                                        className={errors[GROUP_NUMBER] ? 'addition-info__input addition-info__input--invalid' : 'addition-info__input'}
                                                        name={GROUP_NUMBER}
                                                        type="text"
                                                        required
                                                        maxLength="20"
                                                        onChange={onChangeHandler}
                                                        value={selectedApplicantData[GROUP_NUMBER] ?
                                                            selectedApplicantData[GROUP_NUMBER] : ''}
                                                    />
                                                    {errors[GROUP_NUMBER] &&
                                                    <p className="error-msg">{errors[GROUP_NUMBER]}</p>}
                                                </div>
                                            </div>
                                            <div className="input-label-wrap">
                                                <label htmlFor="policy_number">
                                                    <FormattedMessage id="verification.policy_number"/>
                                                    <i className="icon-asterisk"/>
                                                </label>
                                                <div className="input-wrap">
                                                    <input
                                                        id="policy_number"
                                                        className={errors[POLICY_NUMBER] ? 'addition-info__input addition-info__input--invalid' : 'addition-info__input'}
                                                        name={POLICY_NUMBER}
                                                        type="text" required
                                                        maxLength="20"
                                                        onChange={onChangeHandler}
                                                        value={selectedApplicantData[POLICY_NUMBER] ?
                                                            selectedApplicantData[POLICY_NUMBER] : ''}
                                                    />
                                                    {errors[POLICY_NUMBER] &&
                                                    <p className="error-msg">{errors[POLICY_NUMBER]}</p>}
                                                </div>
                                            </div>
                                            <div className="addition-info__controls">
                                                <button
                                                    className="ds-c-button ds-u-margin--1 addition-info__btn addition-info__btn--skip"
                                                    type="button"
                                                    onClick={(e) => {
                                                        onActionHandler(e, ACTION_SKIP)
                                                    }}
                                                >
                                                    <FormattedMessage id="verification.btn_skip"/>
                                                </button>
                                                <button
                                                    className="ds-c-button--primary ds-u-margin--1 addition-info__btn addition-info__btn--submit">
                                                    <FormattedMessage id="verification.btn_submit"/>
                                                </button>
                                            </div>
                                        </form>
                                    </AccordionItemPanel>

                                </AccordionItem>
                            </Accordion> :
                            <div className="addition-info__verified-block">
                                <h5>
                                        <span><FormattedMessage
                                            id="verification.loss_of_MEC"/></span>
                                    <span> ({validationStatus}) </span>
                                    <i className='icon--ml icon-ok-sign'/>
                                </h5>
                            </div>
                        }
                    </div>
                </section>
            </div>
        </div>
    )
};

export default Verification;