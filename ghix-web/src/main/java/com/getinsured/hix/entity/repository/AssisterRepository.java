package com.getinsured.hix.entity.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.exception.GIException;

@Repository
@Transactional(readOnly = true)
public class AssisterRepository implements IGHIXCustomRepository<Assister, Integer>{

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterRepository.class);
	
	@Autowired
	private IAssisterRepository iAssisterRepository;
	@Autowired 
	private UserService userService;
	
	@Override
	public boolean recordExists(Integer id) {
		return iAssisterRepository.exists(id);
	}

	@Override
	@Transactional(readOnly=false)
	public void updateUser(AccountUser accountUser, Integer id) {
		
		try {
			   userService.createModuleUser(id, ModuleUserService.ASSISTER_MODULE, accountUser, true);
			 } catch (GIException e) {
			   LOGGER.error("Exception occured while creating module user for assister: ", e);
			 }
	}
	
}
