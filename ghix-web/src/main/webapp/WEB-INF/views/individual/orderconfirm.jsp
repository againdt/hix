<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page	import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@page	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<jsp:useBean id="now" class="java.util.Date" />

<style>
ul.inline,ol.inline {
	margin-bottom: 0;
}

.graydrkbg {
	margin-bottom: 0;
}

.form-actions {
	border-top: none;
}
i.icon-print {
	color: inherit;
}
.darkblue {
	background: #0c9bd2;
	color: #fff;
}

.darkgreen {
	background: #008080;
	color: #fff;
}
.table td{border: none;}
</style>
<script type="text/javascript">
	window.history.forward();
	function noBack() {
		window.history.forward();
		$("#submitButton").removeAttr("disabled");
	}
</script>
<body onload="noBack();" onpageshow="if (event.persisted) noBack();"
	onunload=""></body>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<div class="gutter10">
	<div class="row-fluid">
		<div class="topnav">
			<ul class="inline">
				<li><a href="#">Personalise</a></li>
				<li><a href="#">Shop</a></li>
				<li><a href="#">Apply</a></li>
				<li class="active"><a href="#" class="last">Checkout</a></li>
			</ul>
		</div>
	</div>

	<div class="row-fluid">
		<div class="gutter10 span12">

			<input type="hidden" name="cart_id" id="cart_id" value="${cartId}" />
			<input type="hidden" name="fininfoId" id="fininfoId"
				value="${fininfoId}" /> <input type="hidden"
				name="employeeFullName" id="employeeFullName"
				value="${employeeFullName}" />

			<div id="confirmation-text">
				<h2>Confirmation</h2>
				<p>Congratulations! You&rsquo;ve completed the checkout process
					from the Marketplace. Your information will be sent to the
					insurance company that carryyour plan.</p>
				<div class="row-fluid">
					<h4 class="graydrkbg gutter10">Health Plans</h4>
					<div class="gutter10">
						<span class="span7">Test Employer 18 March Sponsored plan
							for : <strong>Bill Themes</strong>
						</span> <span class="span5 pagination-right">Effective Date: <strong>06/01/2014</strong></span>
					</div>
					<table class="table">
						<tbody>
							<tr>
								<td rowspan="2">New MS</td>
								<td><strong> New MS </strong></td>
								<td>Monthly Premium</td>
								<td><strong>$ 285.69</strong></td>
							</tr>
							<tr>

								<td>Choice Connect Silver $2,000 PPO</td>
								<td>Tax Credit (APTC)</td>
								<td><strong>-$ 142.85</strong></td>
							</tr>
						</tbody>
					</table>
					<table class="table">
						<tbody>
							<tr class="darkblue">
								<td class="txt-right">Total Monthly Premiums :</td>
								<td class="txt-right">$ 285.69</td>
							</tr>
							<tr class="darkblue">
								<td class="txt-right">Tax Credits :</td>
								<td class="txt-right">-$ 142.85</td>
							</tr>
							<tr class="darkgreen">
								<td class="txt-right">Your monthly Payment</td>
								<td class="txt-right">$ 142.84 /MO</td>
							</tr>
						</tbody>
					</table>


				</div>

				<div class="form-actions center-text">
					<a data-original-title="" href="#" class="btn btn-small"
						onclick="printContent('confirmation-text');"><i
						class="icon-print"></i>&nbsp;Print Page
					</a> <input type="button" class="btn btn-primary"
						value="Go To Dashboard" />
				</div>
			</div>
			<!--rightpanel-->

		</div>
		<!-- End Form -->
	</div>
	<!--row-fluid-->
</div>

<script type="text/javascript">
	
	function printContent(id) {
		var str = "<html>";
		str = str + "<head></head>";
		str = str + "<body>";
		str = str + "<table align='center'>";
		str = str + "<tr><td align='center'>Confirmation</td></tr>";
		str = str + "<tr><td>" + document.getElementById(id).innerHTML
				+ "</td></tr>";
		str = str + "</table>";
		str = str + "</body>";
		str = str + "</html>";

		window.frames["print_frame"].document.body.innerHTML = str;
		window.frames["print_frame"].window.focus();
		window.frames["print_frame"].window.print();
	}
</script>