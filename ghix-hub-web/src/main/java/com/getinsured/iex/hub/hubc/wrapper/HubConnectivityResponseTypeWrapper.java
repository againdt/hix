package com.getinsured.iex.hub.hubc.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.HubConnectivityResponseType;

/**
 * Wrapper for com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.HubConnectivityResponseType;
 * 
 * @author Nikhil Talreja
 * @since 05-Jun-2014
 * 
 */
public class HubConnectivityResponseTypeWrapper implements JSONAware {
	
	private ResponseMetadataWrapper responseMetadata;
	
	public HubConnectivityResponseTypeWrapper(HubConnectivityResponseType hubConnectivityResponseType){
		
		if(hubConnectivityResponseType == null){
			return;
		}
		
		this.responseMetadata = new ResponseMetadataWrapper(hubConnectivityResponseType.getResponseMetadata());
		
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.responseMetadata != null){
			obj.put("ResponseMetadata", this.responseMetadata);
		}
		return obj.toJSONString();
	}

}
