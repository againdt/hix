package com.getinsured.hix.batch.platform.processor;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.AfterJob;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.platform.skip.PrintPaperNoticeSkip;
import com.getinsured.hix.model.NoticeDTO;
import com.getinsured.hix.platform.paper.PrintNoticeService;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * @since 05th January 2015
 * @version 1.0
 * Processor class for printPaperNoticeJob
 *
 */
@Component("printPaperNoticeProcessor")
public class PrintPaperNoticeProcessor implements ItemProcessor<NoticeDTO, NoticeDTO>{
	private PrintNoticeService printNoticeService;
	private PrintPaperNoticeSkip printPaperNoticeSkip;
	private JdbcTemplate jdbcTemplate;
	private Connection connection;
	private PreparedStatement statement;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrintPaperNoticeProcessor.class);
	private static final int PERMANENT_FAILURE = -2;
	private static final int TRY_AGAIN = -1;
	private ExecutionContext executionContext;
	private static final String FLAG_Y = "Y";
	private static final String FLAG_N = "N";
	private static final String UPDATE_QUERY = "UPDATE NOTICES SET IS_PRINTED = ? WHERE ID = ?";
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		executionContext = stepExecution.getJobExecution().getExecutionContext();
	}
	
	
	@Override
	public NoticeDTO process(NoticeDTO noticeDTO) throws Exception {
		int attempt = 0;
		int update = 0;
		boolean paperNoticesQueued = false;
		if(noticeDTO != null){
			try {
				printNoticeService.paperNoticeToPrintQueue(noticeDTO);
				paperNoticesQueued = true;
				while (attempt < 3) {
					update = updateNoticeStatus(FLAG_Y, noticeDTO.getNoticeId());
					if (update > 0) {
						// Positive number indicates Update is successfull
						break;
					}
					if (update == PERMANENT_FAILURE) {
						throw new GIException("DB Update failed");
					}
					attempt++;
				}
			}
			catch (GIException e) {
				LOGGER.error("Failed to update the notice, attempt # "+attempt, e);
				//Set the IS_PRINTED FLAG as N
				updateNoticeStatus(FLAG_N, noticeDTO.getNoticeId());
				String errMsg = e.getMessage() != null ?e.getMessage() :shortenedStackTrace(e, 3);
				if(paperNoticesQueued){
					errMsg += ("Update failed but Print Queue processed Successfully");
				}
				printPaperNoticeSkip.putToSkippedHouseholdMap(Integer.toString(noticeDTO.getNoticeId()), errMsg);
				//Putting comment across exception to avoid Re-Processing by Spring.
				
				if(executionContext != null){
					executionContext.put("jobExecutionStatus", "failed");
					executionContext.put("errorMessage", e.getErrorMsg());
				}
			}
		}
		return noticeDTO;
	}
	
	/**
	 * 
	 * @return
	 * @throws GIException
	 */
	private PreparedStatement getPreparedStatement() throws GIException {
		int connectionAttempt = 0;
		SQLException sqe  = null;
		String errMsg = null;
		PreparedStatement stmt = null;
		try {
			// Check if existing connection is there and its valid, wait for 1
			// sec for the validity check, if not close and get a new one
			if (this.connection != null && !this.connection.isValid(1000)) {
					this.connection.close();
					this.connection = null;
			}
			if (this.connection == null) {
		this.connection = this.jdbcTemplate.getDataSource().getConnection();
		}
				stmt = connection.prepareStatement(UPDATE_QUERY);
		} catch (SQLException se) {
			LOGGER.error("SQL Exception occurred while processing PrintPaperNotice ",se);
				sqe = se;
			while (connectionAttempt < 3) {
				if (sqe != null) {
					if (!this.isConnectionRecoverable(sqe.getSQLState())) {
						break;
					}
				}
				connectionAttempt++;
			}
			LOGGER.error("PrintPaperNoticeProcessor::Exception initializing the conection");
		}
		if (stmt == null) {
			if (sqe != null) {
				errMsg = "SQL State:" + sqe.getSQLState() + " SQL Code" + sqe.getErrorCode() + " SQL Exception Message:"
						+ sqe.getMessage();
			}
			throw new GIException(
					"Non recoverable error, Failed to initize the prepared statement, Err Message" + errMsg);
		}
		return stmt;
	}
	
	private boolean isConnectionRecoverable(String sqlState){
		if(sqlState.startsWith("0800".intern())){
			return true;
		}
		return false;
	}
	
	@AfterJob
	public void close(){
			if(this.statement != null){
				try{
					this.statement.close();
				}catch(SQLException ex){
					LOGGER.warn("PrintPaperNoticeProcessor:: Exception "+ex.getMessage()+" encountered while closing the statement, Ignoring");
				}
			}
			if(this.connection != null){
				try{
					this.connection.close();
				}catch(SQLException ex){
					LOGGER.warn("PrintPaperNoticeProcessor:: Exception "+ex.getMessage()+" encountered while closing the connection, Ignoring");
				}
			}
		this.connection = null;
		this.statement = null;
	}
	
	public int updateNoticeStatus(String isPrintedFlag, Integer noticeID){
		Exception e = null;
		try{
				this.statement = this.getPreparedStatement();
			this.statement.setString(1, isPrintedFlag);
			this.statement.setInt(2, noticeID);
			return this.statement.executeUpdate();
		}
		catch(Exception ex){
			// Either GI Exception or an SQL Exception will be thrown
			e = ex;
			if(ex instanceof SQLException){
				SQLException sql = (SQLException)ex;
				int status = sql.getErrorCode();
				String state = sql.getSQLState();
				if(this.isConnectionRecoverable(state)){
					LOGGER.error("PrintPaperNoticeProcessor:: encountered SQL Exception wih status"+status+" State Code:"+state+" retrying connecton");
					return TRY_AGAIN;
				}
			}
		}
		LOGGER.error("Failed to update the Notice with error: ", e);
		return PERMANENT_FAILURE;
	}
	
	/**
	 * 
	 * @param e
	 * @param maxLines
	 * @return
	 */
	private static String shortenedStackTrace(Exception e, int maxLines) {
		if (e == null) {
			return "";
		}
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		String[] lines = writer.toString().split("\n");
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < Math.min(lines.length, maxLines); i++) {
			sb.append(lines[i]).append("\n");
		}
		return sb.toString();
	}

	/**
	 * @return the printNoticeService
	 */
	public PrintNoticeService getPrintNoticeService() {
		return printNoticeService;
	}

	/**
	 * @param printNoticeService the printNoticeService to set
	 */
	public void setPrintNoticeService(PrintNoticeService printNoticeService) {
		this.printNoticeService = printNoticeService;
	}

	/**
	 * @return the printPaperNoticeSkip
	 */
	public PrintPaperNoticeSkip getPrintPaperNoticeSkip() {
		return printPaperNoticeSkip;
	}

	/**
	 * @param printPaperNoticeSkip the printPaperNoticeSkip to set
	 */
	public void setPrintPaperNoticeSkip(PrintPaperNoticeSkip printPaperNoticeSkip) {
		this.printPaperNoticeSkip = printPaperNoticeSkip;
	}

	/**
	 * @return the jdbcTemplate
	 */
	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}

	/**
	 * @param jdbcTemplate the jdbcTemplate to set
	 */
	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}	
}
