
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for TitleII yearly income information
 *             
 * 
 * <p>Java class for SSATitleIIYearlyInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SSATitleIIYearlyInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}IncomeDate"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}IncomeAmount"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SSATitleIIYearlyInformationType", propOrder = {
    "incomeDate",
    "incomeAmount"
})
public class SSATitleIIYearlyInformationType
    extends ComplexObjectType
{

    @XmlElement(name = "IncomeDate", required = true)
    protected String incomeDate;
    @XmlElement(name = "IncomeAmount", required = true)
    protected BigDecimal incomeAmount;

    /**
     * Gets the value of the incomeDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncomeDate() {
        return incomeDate;
    }

    /**
     * Sets the value of the incomeDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncomeDate(String value) {
        this.incomeDate = value;
    }

    /**
     * Gets the value of the incomeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getIncomeAmount() {
        return incomeAmount;
    }

    /**
     * Sets the value of the incomeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setIncomeAmount(BigDecimal value) {
        this.incomeAmount = value;
    }

}
