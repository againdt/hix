package com.getinsured.hix.cap.util;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

//@ravi: TODO: Talk to Vinayak and move to platform 
public class ValidationUtility {

	private static final Logger LOGGER = Logger.getLogger(ValidationUtility.class);
	//private static final String REGEXP_EMAIL= "^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$";
	private static final String REGEXP_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	private static final String REGEXP_PHONENUMBER  ="\\d{10}";
	private static Pattern sEmailPattern;
	private static Pattern sPhoneNumberPattern;
	private static final String REGEXP_ZIPCODE  ="\\d{5}";
	private static Pattern sZipCodePattern;
	
	static
	{
		sEmailPattern = Pattern.compile(REGEXP_EMAIL);
		sPhoneNumberPattern = Pattern.compile(REGEXP_PHONENUMBER);
		sZipCodePattern = Pattern.compile(REGEXP_ZIPCODE);
	}
	//TODO: try and use StringUtils.isBlank()
	public static boolean isNullOrEmpty(String s)
	{
		//StringUtils.isBlank("sdf");
		return ( s== null || s.isEmpty());
	}
	public static void throwIfEmpty(String s, String message)
	{
		if(isNullOrEmpty(s))
		{
			throw new RuntimeException(message);
		}
	}
	public static boolean isPresentIfRequired(String s, boolean isRequired)
	{
		return (!isRequired || (isRequired && !isNullOrEmpty(s)));
	}
	
	public static boolean isValidEmail(String email) 
	{
		return !isNullOrEmpty(email) && sEmailPattern.matcher(email).matches();
	}

	public static boolean isValidPhoneNumber(String phoneNumber) 
	{
		return !isNullOrEmpty(phoneNumber) && sPhoneNumberPattern.matcher(phoneNumber).matches();
	}
	public static boolean isValidZipCode(String zipcode) {
		return !isNullOrEmpty(zipcode) && sZipCodePattern.matcher(zipcode).matches();
	}
	public static boolean isValidDate(String dateToValidate, String dateFormat) {
		if(dateToValidate == null){
			return false;
		}
 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		sdf.setLenient(false);
 
		try {
 			//if not valid, it will throw ParseException
			Date date = sdf.parse(dateToValidate);
			System.out.println(date);
 		} catch (ParseException e) {
 			e.printStackTrace();
			return false;
		}
 
		return true;
	}
}
