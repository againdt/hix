package com.getinsured.ahbx.enrollment.payload;

import org.apache.log4j.Logger;
import org.springframework.ws.context.MessageContext;

import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.payload.GIWSPayloadHanlder;

public class IND21GIPayloadHandler implements GIWSPayloadHanlder {

	protected static final Logger LOGGER = Logger.getLogger(IND21GIPayloadHandler.class);
	
	private final String  ENROLLMENT_NODE_PATH = "saveEnrollmentConfirmation/enrollmentConfirmationRequest/enrollmentId";
	private final String  MEMBER_NODE_PATH = "saveEnrollmentConfirmation/enrollmentConfirmationRequest/memberId"; 
	
	public GIWSPayload handlePayload(GIWSPayload giwsPayload,MessageContext messageContext) {
		try{
			String payload= giwsPayload.getRequestPayload();
			if(payload!=null){
				String enrollment_id = AHBXUtils.searchXML(ENROLLMENT_NODE_PATH, payload);
			if (enrollment_id != null) {
				giwsPayload.setCustomKeyId1(GhixConstants.ENROLLMENT_ID);
				giwsPayload.setCustomKeyValue1(enrollment_id);
			}
			
			String household_id = AHBXUtils.searchXML(MEMBER_NODE_PATH, payload);
			if (household_id != null) {
				giwsPayload.setCustomKeyId2(GhixConstants.MEMBER_ID);
				giwsPayload.setCustomKeyValue2(household_id);
			}
			}
		}catch(Exception e){
			return giwsPayload;
		}
		return giwsPayload;
	}

	@Override
	public void handlePayload(GIWSPayload giwsPayload, Object response) {
		try{
			giwsPayload = handlePayload(giwsPayload, (MessageContext) response);
		}catch(Exception e){
			LOGGER.error("Failed to log IND21GIPayloadHandler DATA : "+e.getMessage());
		}
	}

	/*@Override
	public GIWSPayload handlePayload(GIWSPayload giwsPayload,String payload) {
		
		String enrollment_id = GhixUtils.searchXML(ENROLLMENT_NODE_PATH, payload);
		if (enrollment_id != null) {
			giwsPayload.setCustomKeyId1(GhixConstants.ENROLLMENT_ID);
			giwsPayload.setCustomKeyValue1(enrollment_id);
		}
		
		String household_id = GhixUtils.searchXML(MEMBER_NODE_PATH, payload);
		if (enrollment_id != null) {
			giwsPayload.setCustomKeyId2(GhixConstants.MEMBER_ID);
			giwsPayload.setCustomKeyValue2(household_id);
		}
		
		return giwsPayload;
	}
*/
}
