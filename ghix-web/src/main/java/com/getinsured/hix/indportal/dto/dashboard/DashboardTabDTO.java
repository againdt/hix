package com.getinsured.hix.indportal.dto.dashboard;

import java.util.List;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;

public class DashboardTabDTO {
	
	private String applicationYear = null;
	private String applicationStatus = null;
	private String applicationType = null;
	private String caseNumber = null;
	private String encyptedCaseNumber = null;
	private String whereUserLeftOff;
	private Float aptc;
	private Float stateSubsidy;
	private boolean renewalApplication;
	private boolean isFinancial;
	private List<DashboardApplicantDTO> applicants;
	private DashboardPreEligibilityDTO preEligibility;
	private List<DashboardEnrollmentDTO> enrollments;
	private DashboardSepEventDTO sepEvent;
	private boolean isSepOpen;
	private boolean isInsideOEEnrollmentWindow;
	private ApplicationValidationStatus validationStatus;
	private String applicationDentalStatus;
	private boolean isEligibilityConditional;
	private Long ssapApplicationId;
	private String healthRenewalStatus;
	private String dentalRenewalStatus;
	private boolean isInsideOEWindow;
	
	public boolean isEligibilityConditional() {
		return isEligibilityConditional;
	}

	public void setEligibilityConditional(boolean isEligibilityConditional) {
		this.isEligibilityConditional = isEligibilityConditional;
	}

	public String getApplicationYear() {
		return applicationYear;
	}

	public void setApplicationYear(String applicationYear) {
		this.applicationYear = applicationYear;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}
	
	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public String getWhereUserLeftOff() {
		return whereUserLeftOff;
	}

	public void setWhereUserLeftOff(String whereUserLeftOff) {
		this.whereUserLeftOff = whereUserLeftOff;
	}

	public Float getAptc() {
		return aptc;
	}

	public void setAptc(Float aptc) {
		this.aptc = aptc;
	}

	public boolean getRenewalApplication() {
		return renewalApplication;
	}

	public void setRenewalApplication(boolean renewalApplication) {
		this.renewalApplication = renewalApplication;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getEncyptedCaseNumber() {
		return encyptedCaseNumber;
	}

	public void setEncyptedCaseNumber(String encyptedCaseNumber) {
		this.encyptedCaseNumber = encyptedCaseNumber;
	}

	public boolean getIsFinancial() {
		return isFinancial;
	}

	public void setIsFinancial(boolean isFinancial) {
		this.isFinancial = isFinancial;
	}

	public List<DashboardApplicantDTO> getApplicants() {
		return applicants;
	}

	public void setApplicants(List<DashboardApplicantDTO> applicants) {
		this.applicants = applicants;
	}

	public DashboardPreEligibilityDTO getPreEligibility() {
		return preEligibility;
	}

	public void setPreEligibility(DashboardPreEligibilityDTO preEligibility) {
		this.preEligibility = preEligibility;
	}

	public List<DashboardEnrollmentDTO> getEnrollments() {
		return enrollments;
	}

	public void setEnrollments(List<DashboardEnrollmentDTO> enrollments) {
		this.enrollments = enrollments;
	}

	public DashboardSepEventDTO getSepEvent() {
		return sepEvent;
	}

	public void setSepEvent(DashboardSepEventDTO sepEvent) {
		this.sepEvent = sepEvent;
	}

	public boolean getIsSepOpen() {
		return isSepOpen;
	}

	public void setIsSepOpen(boolean isSepOpen) {
		this.isSepOpen = isSepOpen;
	}

	public boolean getIsInsideOEEnrollmentWindow() {
		return isInsideOEEnrollmentWindow;
	}

	public void setIsInsideOEEnrollmentWindow(boolean isInsideOEEnrollmentWindow) {
		this.isInsideOEEnrollmentWindow = isInsideOEEnrollmentWindow;
	}
	
	public boolean getIsInsideOEWindow() {
		return isInsideOEWindow;
	}

	public void setIsInsideOEWindow(boolean isInsideOEWindow) {
		this.isInsideOEWindow = isInsideOEWindow;
	}
	

	public ApplicationValidationStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(ApplicationValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}

	public Float getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(Float stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}
	
	public String getApplicationDentalStatus() {
		return applicationDentalStatus;
	}

	public void setApplicationDentalStatus(String applicationDentalStatus) {
		this.applicationDentalStatus = applicationDentalStatus;
	}

	public Long getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(Long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getHealthRenewalStatus() {
		return healthRenewalStatus;
	}

	public void setHealthRenewalStatus(String healthRenewalStatus) {
		this.healthRenewalStatus = healthRenewalStatus;
	}

	public String getDentalRenewalStatus() {
		return dentalRenewalStatus;
	}

	public void setDentalRenewalStatus(String dentalRenewalStatus) {
		this.dentalRenewalStatus = dentalRenewalStatus;
	}
}
