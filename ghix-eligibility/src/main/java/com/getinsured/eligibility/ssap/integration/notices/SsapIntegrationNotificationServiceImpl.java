package com.getinsured.eligibility.ssap.integration.notices;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.joda.time.LocalDate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Service("ssapIntegrationNotificationService")
@Transactional(readOnly = true)
public class SsapIntegrationNotificationServiceImpl implements
		SsapIntegrationNotificationService {

	private static final String NOT_VERIFIED = "Not Verified";

	private static final String VERIFIED = "Verified";

	@Autowired
	private NoticeService noticeService;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;
	
	@Autowired
	PreferencesService preferencesService;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(SsapIntegrationNotificationServiceImpl.class);

	@Override
	public String generateAdditionalInformationDocument(String caseNumber)
			throws Exception {

		return generate(caseNumber, "AdditionalInformationForNonFinancialApp");
	}

	private String generate(String caseNumber, String noticeTemplateName)
			throws NoticeServiceException {
		SsapApplication ssapApplication = getApplicationData(caseNumber);

		int moduleId = fetchModuleId(ssapApplication);
		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		Location location = null;
		PreferencesDTO preferencesDTO  = preferencesService.getPreferences(ssapApplication.getCmrHouseoldId().intValue(), false);
		location = getLocationFromDTO(preferencesDTO.getLocationDto());
		String relativePath = "cmr/" + moduleId + "/ssap/"
				+ ssapApplication.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId
				+ (new TSDate().getTime()) + ".pdf";

		String emailId = preferencesDTO.getEmailAddress();

		List<String> validEmails = emailId != null ? Arrays.asList(emailId)
				: null;

		Notice notice = noticeService
				.createModuleNotice(
						noticeTemplateName,
						GhixLanguage.US_EN,
						getReplaceableObjectData(ssapApplication),
						relativePath,
						ecmFileName,
						moduleName,
						moduleId,
						validEmails,
						DynamicPropertiesUtil
								.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
						fullName, location, preferencesDTO.getPrefCommunication());

		return notice.getEcmId();
	}

	private Location fetchPrimarySsapApplicantAddress(
			List<SsapApplicant> ssapApplicants) {
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return fetchPrimarySsapApplicantAddress(ssapApplicant);
			}
		}

		return null;

	}
	
	public Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}



	private Location fetchPrimarySsapApplicantAddress(
			SsapApplicant ssapApplicant) {
		int locationid = ssapApplicant.getMailiingLocationId() != null ? ssapApplicant
				.getMailiingLocationId().intValue() : 0;
		if (locationid == 0) {
			locationid = ssapApplicant.getOtherLocationId() != null ? ssapApplicant
					.getOtherLocationId().intValue() : 0;
		}
		return iLocationRepository.findOne(locationid);

	}

	private int fetchModuleId(SsapApplication ssapApplication) {

		int cmrId = ssapApplication.getCmrHouseoldId() != null ? ssapApplication
				.getCmrHouseoldId().intValue() : 0;

		if (cmrId == 0) {
			throw new GIRuntimeException(
					"Cannot generate notification! CMR Household ID not found for case number - "
							+ ssapApplication.getCaseNumber());
		}
		return cmrId;
	}

	private SsapApplication getApplicationData(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository
				.findByCaseNumber(caseNumber);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException(
				"SSAP Id is null.Unable to fetch household id. Email cannot be triggered for"
						+ caseNumber);
	}

	private Map<String, Object> getReplaceableObjectData(
			SsapApplication ssapApplication) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("primaryApplicantName", getName(ssapApplication));
		tokens.put("verificationDtoList",
				populateVerificationDtoList(ssapApplication));
		tokens.put("ApplicationID", ssapApplication.getCaseNumber());
		tokens.put("date", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new TSDate()));
		tokens.put(
				TemplateTokens.EXCHANGE_FULL_NAME,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put(
				TemplateTokens.EXCHANGE_PHONE,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(
				TemplateTokens.EXCHANGE_ADDRESS_1,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(
				TemplateTokens.EXCHANGE_ADDRESS_2,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put(
				"exgCityName",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put(
				"exgStateName",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put(
				"zip",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(
				TemplateTokens.EXCHANGE_URL,
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put(
				"exchangeFax",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put(
				"exchangeAddressEmail",
				DynamicPropertiesUtil
						.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		
		//calculate 90 days in future
		LocalDate date = LocalDate.fromDateFields(ssapApplication.getEsignDate());
		date = date.plusDays(89);
		String formatedDate = DateUtil.dateToString(date.toDate(), "MMMM dd, YYYY");
		tokens.put("calculatedDate",formatedDate);
		Date esignDate = ssapApplication.getEsignDate();
		long timeDiff = new TSDate().getTime() - esignDate.getTime();
		long days = 90 - (timeDiff/ (1000*60*60*24));
		tokens.put("calculatedDays",days);
		tokens.put("ssapApplicationId", ssapApplication.getId());
		return tokens;
	}

	
	
	
	private List<SsapApplicantVerificationNoticeDTO> populateVerificationDtoList(
			SsapApplication ssapApplication) {
		List<SsapApplicantVerificationNoticeDTO> applicantVerificationNoticeDTOs = new ArrayList<SsapApplicantVerificationNoticeDTO>();

		for (SsapApplicant ssapaplApplicant : ssapApplication
				.getSsapApplicants()) {
			SsapApplicantVerificationNoticeDTO applicantVerificationNoticeDTO = new SsapApplicantVerificationNoticeDTO();
			applicantVerificationNoticeDTO.setName(ssapaplApplicant
					.getFirstName() + " " + ssapaplApplicant.getLastName());
			applicantVerificationNoticeDTO
					.setSsnStatus(getStatus(ssapaplApplicant
							.getSsnVerificationStatus()));
			applicantVerificationNoticeDTO
					.setDeathStatus(getStatus(ssapaplApplicant.getDeathStatus()));
			applicantVerificationNoticeDTO
					.setResidencyStatus(getStatus(ssapaplApplicant
							.getResidencyStatus()));
			applicantVerificationNoticeDTO
					.setIncarcerationStatus(getStatus(ssapaplApplicant
							.getIncarcerationStatus()));
			applicantVerificationNoticeDTO
					.setCitizenshipStatus(getCitizenShipLegalPresenceStatus(
							ssapaplApplicant.getCitizenshipImmigrationStatus(),
							ssapaplApplicant.getVlpVerificationStatus()));
			applicantVerificationNoticeDTOs.add(applicantVerificationNoticeDTO);
		}
		return applicantVerificationNoticeDTOs;
	}

	private String getCitizenShipLegalPresenceStatus(String citizenShip,
			String legalPresence) {

		String citizenShipStatus = getStatus(citizenShip);
		String legalPresenceStatus = getStatus(legalPresence);

		if (VERIFIED.equalsIgnoreCase(legalPresenceStatus)
				|| VERIFIED.equalsIgnoreCase(citizenShipStatus)) {
			return VERIFIED;
		}
		return NOT_VERIFIED;
	}

	private String getStatus(String status) {
		return status != null ? VERIFIED.equalsIgnoreCase(status) ? VERIFIED
				: NOT_VERIFIED : NOT_VERIFIED;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName()+ " "+ssapApplicant.getLastName();
			}
		}

		return null;
	}

}
