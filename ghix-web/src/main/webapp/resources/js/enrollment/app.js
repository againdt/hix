(function() {
    'use strict';

    var reconciliationWorkbenchApp = angular.module('reconciliationWorkbenchApp', ['ui.router','ngTouch', 'ui.grid', 'ui.grid.pagination', 'countTo', 'spring-security-csrf-token-interceptor']);

    reconciliationWorkbenchApp.config(function($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');

        $stateProvider

            .state('home', {
                url: '/home',
                resolve: {
                	check: function($state) {
                		if($('#redirectEnrollmentId').val() !== ''){
                        	$state.go('detail', {enrollmentId: $('#redirectEnrollmentId').val()} );
                        }
                	}
                },
                templateUrl: '/hix/resources/js/enrollment/monthlyactivity/partial-discrepancies.html'
            })

            // nested list with custom controller
            .state('search', {
                cache:false,
                url: '/search',
                params: {
                    issuer: null
                },
                templateUrl: '/hix/resources/js/enrollment/search/partial-reconSearch.html',
                controller: function($scope) {
                }
            })

            .state('detail', {
                cache:false,
                url: '/detail',
                params: {
                    enrollment: null,
                    enrollmentId: null
                },
                templateUrl: '/hix/resources/js/enrollment/activitydetail/partial-discrepancyDetail.html',
                controller: function($scope) {
                }
            })

    });

})();