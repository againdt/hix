import moment from 'moment';

// Utils

import normalizeNameOfPerson from './normalizeNameOfPerson';
import bitWiseTools from './bitWiseOperatorTools';

// Constants

import BLOOD_RELATIONSHIP_TYPES from '../constants/bloodRelationshipCodesList';
import { THC_TYPES } from '../constants/masksConstants';

export const familyAnalyzer = config => {

    const {
        loadedRelations,
        bloodRelationships,
        currentMemberIndex,
        householdMembers,
        primaryTaxFilerPersonId,
        MASKS
    } = config;

    const {
        taxHouseholdComposition: taxHouseholdCompositionMasks,
        taxHouseholdCompositionProgress: taxHouseholdCompositionProgressMasks
    } = MASKS;

    const currentHhmPersonId = currentMemberIndex + 1;

    const {
        dateOfBirth,
        taxHouseholdComposition: {
            claimerIds
        },
        flagBucket: {
            taxHouseholdCompositionFlags,
            taxHouseholdCompositionProgressFlags
        }
    } = (hhm => hhm ? hhm : { taxHouseholdComposition: {}, flagBucket: {} })(householdMembers.find(hhm => hhm.personId === currentHhmPersonId));

    const indicatorsValues = bitWiseTools
        .convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks);

    const progressValues = bitWiseTools
        .convertFlagsToValues(taxHouseholdCompositionProgressFlags, taxHouseholdCompositionProgressMasks);

    const bloodRelationshipsCodes = (() => {
        let output = {};
        const RELATIONS = [
            {
                type: 'spouse',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.SPOUSE
                ]
            },
            {
                type: 'parents',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.PARENT_OF_CHILD,
                    BLOOD_RELATIONSHIP_TYPES.STEPPARENT
                ]
            },
            {
                type: 'children',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.CHILD,
                    BLOOD_RELATIONSHIP_TYPES.ADOPTED_CHILD,
                    BLOOD_RELATIONSHIP_TYPES.STEPCHILD
                ]
            }
        ];
        loadedRelations.forEach(lr => {
            RELATIONS.forEach(r => {
                const {
                    type,
                    enums
                } = r;
                if(enums.some(e => e === lr.type)) {
                    if(output[type]) {
                        output = {
                            ...output,
                            [type]: [
                                ...output[type],
                                lr.code
                            ]
                        };
                    } else {
                        output = {
                            ...output,
                            [type]: [
                                lr.code
                            ]
                        };
                    }
                }

            });
        });
        return output;
    })();
    //console.log('bloodRelationshipsCodes', bloodRelationshipsCodes);


    const relatedPersonsIds = (() => {
        let output = {};
        Object.entries(bloodRelationshipsCodes).forEach(([ codeType, values ]) => {
            let res = [];
            bloodRelationships.forEach(br => {
                if(br.relatedPersonId === String(currentHhmPersonId) && values.some(v => v === br.relation)) {
                    res.push(+br.individualPersonId);
                }
            });

            if(res.length > 0) {
                output = {
                    ...output,
                    [codeType]: res
                };
            } else {
                output = {
                    ...output,
                    [codeType]: null
                };
            }
        });
        return output;
    })();

    const isClaimedBySpouseOrParent =
        indicatorsValues.taxRelationship === THC_TYPES.DEPENDENT &&
        claimerIds.every(id => {
            let counter = 0;
            Object.values(relatedPersonsIds).forEach(value => {
                if(value) {
                    if(value.some(v => v === id)) {
                        counter++;
                    }
                }
            });
            return counter;
        });

    let familyInfo = {
        currentHhm: {},
        relatives: []
    };

    householdMembers.forEach((hhm, idx) => {

        const establishRelation = id => {

            const getRelation = id => {
                const relation = bloodRelationships.find(br => +br.individualPersonId === id && +br.relatedPersonId === currentHhmPersonId);
                return relation ? relation : {};
            };

            let establishedRelation = loadedRelations.find(l => l.code === getRelation(id).relation);

            return establishedRelation ? establishedRelation : {};
        };
        const hasOneParentLivingOutside = (() => {
            const parents = relatedPersonsIds.parents;
            // console.log('familyInfo, parents', familyInfo, parents);

            let addresses = [
                hhm.addressId
            ];
            if (Array.isArray(parents) && parents.length === 1) {
                addresses = [
                    ...addresses,
                    householdMembers[parents[0]-1].addressId
                ];

            }
            // console.log(addresses);
            return addresses.length === 2 && !addresses.every(address => address === addresses[0]);
        })();

        const {
            code,
            text
        } = establishRelation(hhm.personId);

        const summary = {
            personName: normalizeNameOfPerson(hhm.name),
            personId: hhm.personId,
            personIndex: idx,
            addressId: hhm.addressId,
            relationToCurrentHhm: text,
            relationCode: code,
            hasOneParentLivingOutside,
            applyingForCoverage: hhm.applyingForCoverageIndicator
        };
        idx === currentMemberIndex ? familyInfo.currentHhm = summary : familyInfo.relatives.push(summary);
    });

    const isOlderThanMarriageAge = moment().diff(dateOfBirth, 'years') >= 14;
    const isOlderThanChildAge = moment().diff(dateOfBirth, 'years') >= 19;
    const isApplicationFiler = currentMemberIndex === 0;
    const spouseName = (spouse => spouse ? spouse.personName : null)(familyInfo.relatives.find(r => bloodRelationshipsCodes.spouse.some(br => br === r.relationCode)));
    const spouseIndex = (spouse => spouse ? familyInfo.relatives.find(r => r.personName === spouseName).personIndex : null)(spouseName);
    const livesWithSpouse = (spouseName => spouseName ? familyInfo.relatives.find(r => r.personName === spouseName).addressId === familyInfo.currentHhm.addressId : false)(spouseName);
    const primaryTaxFilerHouseholdMembers = (() => {
        const getHouseholdId = (hhm => hhm ? hhm.householdId : null)(householdMembers.find(hhm => hhm.personId === primaryTaxFilerPersonId));
        return (id => id !== null ? householdMembers.filter(hhm => {

            const {
                taxHouseholdComposition: {
                    taxRelationship
                }
            } = hhm;
            return hhm.householdId === id && taxRelationship !== THC_TYPES.UNSPECIFIED;
        }) : [])(getHouseholdId);
    })();

    familyInfo = {
        ...familyInfo,
        currentHhm: {
            ...familyInfo.currentHhm,
            livesWithSpouse,
            spouseIndex,
            spouseName,
            isClaimedBySpouseOrParent,
            isOlderThanMarriageAge,
            isOlderThanChildAge,
            isApplicationFiler
        },
        relatedPersonsIds,
        indicatorsValues,
        progressValues,
        primaryTaxFilerHouseholdMembers
    };

    return familyInfo;
};
