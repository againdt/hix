import _ from 'lodash';
import moment from 'moment';

import normalizeNameOfPerson from './normalizeNameOfPerson';
import { getHhmByPersonId } from './getHhmByPersonId';

import { THC_TYPES } from '../constants/masksConstants';
import BLOOD_RELATIONSHIP_TYPES from '../constants/bloodRelationshipCodesList';



export const analyzeFamily = (householdMembers, loadedRelations) => {

    analyzeFamily.getHhmByPersonId = getHhmByPersonId;

    analyzeFamily.getHhmsGenderByPersonId = id => householdMembers.find(hhm => hhm.personId === id).gender;

    analyzeFamily.checkIfLiveTogether = ids => {
        const addressIds = householdMembers
            .filter(hhm => ids.some(id => id === hhm.personId))
            .map(hhm => hhm.addressId);
        return [...new Set(addressIds)].length === 1;
    };

    analyzeFamily.updateDependentHouseholdId = (dependent, filerPersonId, isMemberChecked) => {

        if(!isMemberChecked) {
            return {
                ...dependent,
                householdId: 0
            };
        }

        const index = Array.isArray(filerPersonId) ? filerPersonId[0] - 1 : filerPersonId - 1;

        return {
            ...dependent,
            householdId: householdMembers[index].householdId
        };
    };

    analyzeFamily.updateHhm = (hhm, objForMerge) => {
        return {
            ...hhm,
            taxHouseholdComposition: {
                ...hhm.taxHouseholdComposition,
                ...objForMerge
            }
        };
    };

    analyzeFamily.getRelationByIds = (...ids) => {
        const [ individualPersonId, relatedPersonId ] = ids;

        return householdMembers[0].bloodRelationship.find(br => {
            if(Number(br.individualPersonId) === individualPersonId && Number(br.relatedPersonId) === relatedPersonId) {
                return br;
            }
        });
    };

    analyzeFamily.hasDependents = personId => {
        const claimers = householdMembers.reduce((claimers, hhm) => claimers.concat(hhm.taxHouseholdComposition.claimerIds), []);
        return claimers.find(claimer => claimer === personId);
    };

    const getHhmIndex = personId => householdMembers.findIndex(hhm => hhm.personId === Number(personId));

    analyzeFamily.isPersonMarried = personId =>
        householdMembers[0]
            .bloodRelationship
            .some(br => br.individualPersonId === String(personId) && br.relation === '01');

    analyzeFamily.getIndicatorsValues = (personId, anyHhm = undefined) => {

        const index = getHhmIndex(personId);

        const {
            taxHouseholdComposition
        } = anyHhm ? anyHhm: householdMembers[index];

        return _.omit(taxHouseholdComposition, ['claimerIds']);
    };

    analyzeFamily.isCountedAsPair = ids => {
        return ids.every(id => moment().diff(analyzeFamily.getHhmByPersonId(Number(id), householdMembers).dateOfBirth, 'years') >= 14);
    };

    const doSpousesFileJointly = ids => {
        return ids
            .map(id => analyzeFamily.getIndicatorsValues(id).taxFilingStatus)
            .every(status => status === THC_TYPES.FILING_JOINTLY);
    };

    const doSpousesFileSeparately = ids => {
        return ids
            .map(id => analyzeFamily.getIndicatorsValues(id).taxFilingStatus)
            .some(status => status === THC_TYPES.FILING_SEPARATELY);
    };

    const hhmData = (dataKey, personId) => {
        const hhmIndex = getHhmIndex(personId);
        return householdMembers[hhmIndex][dataKey];
    };

    const doLiveTogether = ids => {
        const filtered = householdMembers
            .filter(hhm => ids.some(id => Number(id) === hhm.personId))
            .map(hhm => hhm.addressId);

        return filtered.every(hhm => hhm === filtered[0]);
    };

    const filers = (hhms => hhms.map(hhm => {

        const {
            name,
            personId
        } = hhm;

        const {
            taxRelationship,
            taxFilingStatus
        } = analyzeFamily.getIndicatorsValues(personId);

        const isFilingTaxes = taxRelationship === THC_TYPES.FILER || taxRelationship === THC_TYPES.FILER_DEPENDENT;

        return {
            filerIndex: getHhmIndex(personId),
            filerPersonId: personId,
            filerFullName: normalizeNameOfPerson(name),
            isFilingTaxes: isFilingTaxes,
            taxFilingStatus: taxFilingStatus
        };
    }))(householdMembers);



    const filersQty = filers.reduce((acc, el) => acc + (el.isFilingTaxes ? 1 : 0), 0);

    const pairs = (hhms => {
        const spouses = new Map();

        const checkIfAnyOfSpousesFiler = ids =>
            ids.some(id => filers.some(filer => filer.filerPersonId === id && filer.isFilingTaxes));

        hhms[0].bloodRelationship.forEach(br => {

            const {
                individualPersonId,
                relatedPersonId,
                relation
            } = br;

            const bothMembers = [individualPersonId, relatedPersonId];

            if(relation === '01') {
                if(!spouses.has(bothMembers.sort().toString())) {

                    spouses.set(bothMembers.sort().toString(),  {
                        pairId: bothMembers.toString(),
                        isPairAccordingToLaw: analyzeFamily.isCountedAsPair(bothMembers),
                        spouseIndexes: bothMembers.map(m => getHhmIndex(m)),
                        spousesPersonIds: [Number(individualPersonId), Number(relatedPersonId)],
                        spouse1: {
                            spouse1gender: analyzeFamily.getHhmsGenderByPersonId(Number(individualPersonId)),
                            spouse1personId: individualPersonId,
                            spouse1index: getHhmIndex(individualPersonId),
                            spouse1fullName: normalizeNameOfPerson(hhmData('name', individualPersonId)),

                        },
                        spouse2: {
                            spouse2gender: analyzeFamily.getHhmsGenderByPersonId(Number(relatedPersonId)),
                            spouse2personId: relatedPersonId,
                            spouse2index: getHhmIndex(relatedPersonId),
                            spouse2fullName: normalizeNameOfPerson(hhmData('name', relatedPersonId))
                        },
                        liveTogether: doLiveTogether(bothMembers),
                        doSpousesFileTogether: doSpousesFileJointly(bothMembers),
                        doSpousesFileSeparately: doSpousesFileSeparately(bothMembers),
                        isPairMadeChoice: doSpousesFileJointly(bothMembers) || doSpousesFileSeparately(bothMembers),
                        isAnyOfSpousesFiler: checkIfAnyOfSpousesFiler([Number(individualPersonId), Number(relatedPersonId)])
                    });
                }
            }
        });
        return [...spouses].map(([, pairData]) => pairData);
    })(householdMembers);

    analyzeFamily.getSpouseIndex = idx => {
        return (pair => pair.spouseIndexes.filter(spIndex => spIndex !== idx)[0])(pairs.filter(pair => pair.spouseIndexes.some(index => index === idx))[0]);
    };

    const combinedFilers = (filers => {
        const output = [];
        pairs.forEach(pair => {
            if(pair.doSpousesFileTogether) {
                output.push({
                    filerIndex: pair.spouseIndexes,
                    filerPersonId: pair.spousesPersonIds,
                    filerFullName: `${pair.spouse1.spouse1fullName} and ${pair.spouse2.spouse2fullName}`,
                    isFilingTaxes: true,
                    taxFilingStatus: THC_TYPES.FILING_JOINTLY
                });
            }
        });
        filers.forEach(filer => {
            const checkIfNotInOutput = () => {
                return output.every(f => {
                    if(Array.isArray(f.filerIndex)) {
                        return f.filerIndex.every(f => f !== filer.filerIndex);
                    } else {
                        return f.filerIndex !== filer.filerIndex;
                    }
                });
            };
            if(checkIfNotInOutput()) {
                output.push(filer);
            }
        });
        return output;
    })(filers);

    const filteredCombinedFilers = combinedFilers.filter(f => f.isFilingTaxes);

    const bloodRelationshipsCodes = (() => {
        let output = {};
        const RELATIONS = [
            {
                type: 'spouse',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.SPOUSE
                ]
            },
            {
                type: 'parents',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.PARENT_OF_CHILD,
                    BLOOD_RELATIONSHIP_TYPES.STEPPARENT
                ]
            },
            {
                type: 'siblings',
                enums: [
                    BLOOD_RELATIONSHIP_TYPES.SIBLING
                ]
            }
        ];
        loadedRelations.forEach(lr => {
            RELATIONS.forEach(r => {
                const {
                    type,
                    enums
                } = r;
                if(enums.some(e => e === lr.type)) {
                    if(output[type]) {
                        output = {
                            ...output,
                            [type]: [
                                ...output[type],
                                lr.code
                            ]
                        };
                    } else {
                        output = {
                            ...output,
                            [type]: [
                                lr.code
                            ]
                        };
                    }
                }

            });
        });
        return output;
    })();

    const dependents = (hhms => {
        const dependents = [];
        hhms.forEach(hhm => {

            const {
                dateOfBirth,
                name,
                personId,
                addressId,
                taxHouseholdComposition: {
                    claimerIds,
                    taxRelationship
                }
            } = hhm;

            const isDependent = taxRelationship === THC_TYPES.DEPENDENT || taxRelationship === THC_TYPES.FILER_DEPENDENT;

            const isAbleToBeDependent = claimerIds.length === 0 &&
                hhms
                    .filter(hhm => hhm.personId !== personId)
                    .every(hhm => {
                        const {
                            taxHouseholdComposition: {
                                claimerIds
                            }
                        } = hhm;

                        return !claimerIds.some(id => id === personId);

                    });

            const isChild = (() => {
                return moment().diff(dateOfBirth, 'years') < 19;
            })();

            const relatedPersonsIds = (() => {
                let output = {};
                Object.entries(bloodRelationshipsCodes).forEach(([ codeType, values ]) => {
                    let res = [];
                    householdMembers[0].bloodRelationship.forEach(br => {
                        if(br.relatedPersonId === String(personId) && values.some(v => v === br.relation)) {
                            res.push(+br.individualPersonId);
                        }
                    });

                    if(res.length > 0) {
                        output = {
                            ...output,
                            [codeType]: res
                        };
                    } else {
                        output = {
                            ...output,
                            [codeType]: null
                        };
                    }
                });
                return output;
            })();

            const isClaimedByNotParentOrSpouse =
                (taxRelationship === THC_TYPES.DEPENDENT || taxRelationship === THC_TYPES.FILER_DEPENDENT) &&
                claimerIds.every(id => {
                    let counter = 0;
                    Object.values(_.omit(relatedPersonsIds, ['siblings'])).forEach(value => {
                        if(value) {
                            if(value.some(v => v === id)) {
                                counter++;
                            }
                        }
                    });
                    return !counter;
                });

            const isChildLivesWithParentWhoDoesntClaim = (() => {
                return isChild
                    && relatedPersonsIds.parents
                    && relatedPersonsIds.parents.some(parentId => analyzeFamily.checkIfLiveTogether([personId, parentId])
                    && claimerIds.every(id => id !== parentId));
            })();

            const isChildClaimedBySeparatelyLivingParent = (() => {
                return isChild
                    && claimerIds.length === 1
                    && analyzeFamily.getRelationByIds(personId, claimerIds[0]).relation === '19'
                    && !analyzeFamily.checkIfLiveTogether([personId, claimerIds[0]]);
            })();

            const checkIfChildLivesWithParents = ids => {
                const addresses = ids.map(id => getHhmByPersonId(id, householdMembers).addressId);
                return addresses.every(address => address === addresses[0]);
            };

            const childClaimedByFilingSeparatelyParents = (() => {
                return isChild && relatedPersonsIds.parents
                    && relatedPersonsIds.parents.length === 2
                    && checkIfChildLivesWithParents([personId, ...relatedPersonsIds.parents])
                    && (pair => pair ? pair.doSpousesFileSeparately : false)(pairs.find(pair => pair.pairId === relatedPersonsIds.parents.sort().join(',')));
            })();

            dependents.push({
                addressId,
                claimerIds,
                childClaimedByFilingSeparatelyParents,
                dependentIndex: getHhmIndex(personId),
                dependentFullName: normalizeNameOfPerson(name),
                dependentPersonId: personId,
                isAbleToBeDependent,
                isChild,
                isChildLivesWithParentWhoDoesntClaim,
                isClaimedByNotParentOrSpouse,
                isChildClaimedBySeparatelyLivingParent,
                isDependent,
                parents: relatedPersonsIds.parents,
                siblings: relatedPersonsIds.siblings,
                spouse: relatedPersonsIds.spouse
            });
        });
        return dependents;
    })(householdMembers);

    const dependentsQty = dependents.filter(dependent => dependent.isDependent).length;

    const possibleDependents = (() => {
        let counter = 0;
        filers.forEach(filer => {
            const {
                filerPersonId
            } = filer;

            const checkDependent = dependent => {
                const {
                    dependentPersonId
                } = dependent;
                if(Array.isArray(filerPersonId)) {
                    return filerPersonId.every(id => id !== dependentPersonId);
                } else {
                    return dependentPersonId !== filerPersonId;
                }
            };

            const possibleDependentsQty = dependents.filter(dependent => checkDependent(dependent)).length;

            if( possibleDependentsQty > 0) {
                counter += possibleDependentsQty;
            }
        });
        return counter;
    })();

    return {
        combinedFilers,
        dependents,
        dependentsQty,
        filersQty,
        filers,
        filteredCombinedFilers,
        pairs,
        possibleDependents
    };
};