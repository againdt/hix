(function(){
	var log=function(str)
	{
		try{console.log(str);}catch(e){}
	};
	var ticketListNgApp=angular.module("ticketListNgApp",["pageUtils"]);

	ticketListNgApp.config(function($compileProvider){
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|javascript):/);
    });

	var ticketListAppController=function($scope,$http)
	{
		$scope.urls={};
		//$scope.isSimplifiedView=false;

		$scope.isSimplifiedView=$scope.isSimplifiedViewStr=='true'?true:false;
		///ticketmgmt/ticket/list

		$scope.setData=function(resp)
		{
			var response=angular.fromJson(resp);
			processData(response);
		};

		var loadData=function()
		{
			
			processData({});	
		};

		$scope.columns=[
			{title:"Ticket",sortable:true,width:"150px",name:"subject",sortBy:"tkm_ticket_id"},
			{title:"Requested By",sortable:true,width:"120px",name:"requestorFullName",sortBy:"role"},
			{title:"Created for",sortable:false,width:"120px",name:"user",sortBy:null},
			{title:"Workgroup",sortable:true,width:"208px",name:"queueName",sortBy:"tkmQueues"},
			{title:"Ticket Type",sortable:false,width:"208px",name:"ticketType",sortBy:null},
			{title:"Ticket Status",sortable:true,width:"120px",name:"status",sortBy:"status"},
			{title:"Priority",sortable:true,width:"75px",name:"priority",sortBy:"priority_order"},
			{title:"Date of Request",sortable:true,width:"130px",name:"creationTime",sortBy:"creation_timestamp"},
			{title:"Due Date",sortable:true,width:"100px",name:"dueDate",sortBy:"due_date"}
		];

		var processData=function(resp)
		{
			$scope.tickets=[
				{id:1408,ticketNumber:"TIC-1408",subject:"chk",requestedBy:"Boltz",creaedFor:"Jack",workgroup:"Appela Workgroup",status:"Reopened",priority:"Critical",dateOfRequest:"11-03-2015",dueDate:"11-17-2015 02:40 PM"}
			];

			$scope.tickets=[];
	
			$scope.tickets=angular.fromJson(resp.ticketListJson);

			$scope.searchCriteriaJson=angular.fromJson(resp.searchCriteriaJson);

			
			$scope.statusChanged=function(ticket)
			{
				ticket.allowView=true;
				ticket.allowEdit=false;
				ticket.allowCancel=false;
				ticket.allowMarkAsComplete=false;
				ticket.allowClaim=false;
				ticket.allowReassign=false;
				ticket.allowAssign=false;
				ticket.allowReopen=false;
				ticket.allowRestart=false;
				ticket.allowArchive=false;

				if(!$scope.isSimplifiedView)
				{
					if(ticket.status=="Completed" || ticket.status=="Canceled" || ticket.status=="Resolved")
					{
						ticket.allowEdit=false;
						ticket.allowCancel=false;
						ticket.allowAssign=false;
						
					}else{
						ticket.allowEdit=true;
						ticket.allowCancel=true;
						if(ticket.taskStatus=="Claimed")
						{
							ticket.allowReassign=true;

						}else if(ticket.taskStatus=="UnClaimed")
						{
							ticket.allowAssign=true;
						};
					};

				};
				if(ticket.status != "Completed" && ticket.status !="Resolved" && ticket.taskStatus != "Canceled")
				{
					if(ticket.taskStatus=="Claimed")
					{
						ticket.allowMarkAsComplete=true;
					};
					if(ticket.taskStatus=="UnClaimed")
					{
						ticket.allowClaim=true;
					};
					if(ticket.taskStatus=="Open")
					{
						ticket.allowMarkAsComplete=true;
						ticket.allowReassign=true;
					};
					if(ticket.taskStatus=="Reopened" || ticket.taskStatus=="Restarted")
					{
						ticket.allowClaim=true;
						ticket.allowAssign=true;
					};
				};
				if(ticket.status=="Canceled" || ticket.status=="Resolved")
				{
					if(ticket.archiveFlag=='Y')
					{
						ticket.allowReopen=false;
						ticket.allowRestart=false;
					}else{
						ticket.allowReopen=true;
						ticket.allowRestart=true;
					}
				};

				if($scope.isArchivedAllowed==true)
				{
					if( (ticket.status != "Completed" && ticket.status !="Resolved" && ticket.taskStatus != "Canceled") ||ticket.archiveFlag=='Y')
					{
						ticket.allowArchive=false;
					}else{
						ticket.allowArchive=true;
					}
				}else{
					ticket.allowArchive=false;
				};

				if(ticket.allowAssign==true)
					$scope.isAssignAllowed!=true?ticket.allowAssign=false:"";

				
			};

			$scope.archiveTicket=function(ticket)
			{
				ticket.allowView=true;
				ticket.allowEdit=false;
				ticket.allowCancel=false;
				ticket.allowMarkAsComplete=false;
				ticket.allowClaim=false;
				ticket.allowReassign=false;
				ticket.allowAssign=false;
				ticket.allowReopen=false;
				ticket.allowRestart=false;
				ticket.allowArchive=false;
			};

			angular.forEach($scope.tickets,function(ticket){
				$scope.statusChanged(ticket)
			});

			//log($scope.searchCriteriaJson);

			angular.forEach($scope.columns,function(column){
				if(column.sortBy==$scope.searchCriteriaJson.sortBy)
				{
					column.sortDir=$scope.searchCriteriaJson.sortOrder=="ASC"?"DESC":"ASC";
				}else{
					column.sortDir="ASC";
				}
			});

			angular.element("#lblTicketCount").html(resp.count);
			
			if(resp.count > 1){
				$('#lblTicketCounts').show();
			}
			
			$scope.recordCount=resp.count;
			$scope.pageSize=$scope.searchCriteriaJson.pageSize;
			$scope.currentPage=defPage;
		};

		$scope.dosort=function(column)
		{
			sortBy=column.sortBy;
			sortOrder=column.sortDir;
			$("#ticketSearch").submit();
		};

		$scope.exportToExcel=function()
		{
			$("#exportToExcelModal").modal();
		};

		//loadData();
	};
	ticketListAppController.$inject=["$scope","$http"];
	ticketListNgApp.controller("ticketListAppController",ticketListAppController);
	
})();
(function(){
	var log=function(str)
	{
		try{console.log(str);}catch(e){}
	};
	var uiPaginationController=function($scope)
	{
		
		

		$scope.refresh=function()
		{
			var currPage=$scope.currentPage;
			var noOfPage = (($scope.recordCount % $scope.pageSize) > 0 )? ($scope.recordCount/$scope.pageSize)+1 : ($scope.recordCount/$scope.pageSize);
			var noOfPage = Math.ceil($scope.recordCount/$scope.pageSize);
			var startpage=((Math.ceil(currPage/10))*10)-9;
			var endPage=(noOfPage < (startpage+9))? noOfPage : (startpage+9);

			$scope.noOfPage=noOfPage;
			$scope.startpage=startpage;
			$scope.endPage=endPage;

			$scope.hideTotal=false;
		};

		$scope.getNumber=function(start,end)
		{
			var retArray=[];
			for(var i=start;i<=end;i++)
			{
				retArray.push(i);
			}
			return retArray;
		}
		$scope.isCurrentPageEquals=function(num)
		{
			return $scope.currentPage==num?true:false;
		};

		$scope.goToPage=function(num)
		{
			window[$scope.callBack](num);
		}

		$scope.$watch('currentPage',function(newVal){
			if(newVal !=NaN)
			{
				$scope.refresh();
			};
		});
		$scope.$watch('recordCount',function(newVal){
			if(newVal !=NaN)
			{
				$scope.refresh();
			};
		});

	};
	uiPaginationController.$inject=["$scope"];
	var template=function()
	{
		var ret="";
		ret += "<div class=\"pagination txt-center\">";
		ret += "	<div class=\"pagination\">";
		ret += "		<ul ng-if=\"noOfPage > 1\">";
		ret += "			<li ng-if=\"startpage > pageSize\">";
		ret += "				<a href=\"#\" ng-click=\"goToPage(startpage-1)\">Prev</a>";
		ret += "			</li>";
		ret +="				<li ng-repeat=\"i in getNumber(startpage,endPage) track by $index\" ng-class=\"{active:isCurrentPageEquals(i)}\">";
		ret +="					<a href=\"#\" ng-click=\"goToPage(i)\">{{i}}</a>";
		ret +="				</li>";
		ret += "			<li ng-if=\"endPage < noOfPage\">";
		ret += "				<a href=\"#\" ng-click=\"goToPage(endPage+1)\" >Next</a>";
		ret += "			</li>";
		ret += "			<li ng-if=\"!hideTotal\">";
		ret += "				&nbsp;&nbsp;[ Total : {{recordCount}}]";
		ret += "			</li>";
		ret += "		</ul>";
		ret += "	</div>";
		ret += "</div>";
		return ret;
	};
	var paginationTemplate=template();
	angular.module("pageUtils",[]).directive("uiPagination",function(){
		return {
			restrict:"E",
			template:paginationTemplate,
			controller:uiPaginationController,
			scope:{
				pageSize:'=size',
				recordCount:'=total',
				currentPage: '=page',
				callBack: '=callback'
			},
			link:function(scope,element,attrs,parentController)
			{

			}
		}
	});


	angular.bootstrap(angular.element("#ticketListNgApp"),["ticketListNgApp"]);
})();
