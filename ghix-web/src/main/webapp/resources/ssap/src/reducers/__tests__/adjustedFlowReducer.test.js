import adjustedFlowReducer, { initialState } from '../adjustedFlowReducer';
import { adjustedFlowActionTypes } from '../../constants/actionTypes';

describe('Test for adjustedFlowReducer', () => {

    const twoPageFlow = [
        {
            hhm: -1,
            isActive: true,
            page: {
                buttons: {leftButton: 'INITIAL', middleButton: 'INITIAL', rightButton: 'COMBINED'},
                component: 'React Element',
                navBarText: 'Before We Begin',
                pageName: 'BEFORE_WE_BEGIN_PAGE'
            },
            pageIndex: 0,
            pageName: 'BEFORE_WE_BEGIN_PAGE',
            reasons: [],
            sectionIndex: 0,
            sectionName: 'Start Your Application'
        },
        {
            hhm: -1,
            isActive: true,
            page: {
                buttons: {leftButton: 'INITIAL', middleButton: 'INITIAL', rightButton: 'COMBINED'},
                component: 'React Element',
                navBarText: 'Get Ready',
                pageName: 'GET_READY_PAGE'
            },
            pageIndex: 1,
            pageName: 'GET_READY_PAGE',
            reasons: [],
            sectionIndex: 0,
            sectionName: 'Start Your Application'
        },
    ];

    it('This reducer hasn\'t been invoked:', () => {
        expect(adjustedFlowReducer(
            initialState,
            {
                type: 'WRONG_TYPE',
                value: { name: 'Air France' }
            }
        )).toEqual(initialState);
    });

    it('We got with length of 2: ', () => {
        expect(adjustedFlowReducer(
            initialState,
            {
                type: adjustedFlowActionTypes.ADJUST_FLOW,
                value: twoPageFlow
            })).toHaveLength(2);
    });

    it('State should be cleaned', () => {
        expect(adjustedFlowReducer(
            twoPageFlow,
            {
                type: adjustedFlowActionTypes.CLEAR_STATE
            }
        )).toEqual(initialState);
    });
});