package com.getinsured.hix.admin.web;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.serff.service.templates.PlanDocumentsJobService;
import com.getinsured.hix.serff.util.SerffUploadBrochureUtils;

/**
 * @author vardekar_s
 * 
 *         This controller is for Brochure upload functionality.
 * 
 */

@Controller
public class SerffUploadBrochureController {

	private static final Logger LOGGER = Logger
			.getLogger(SerffUploadBrochureController.class);
	private static final String CLASS_NAME = "SerffUploadBrochureController";
	private static final String RETURN_UPLOAD_BROCHURE_PAGE = "admin/serff/getUploadBrochureCount";
	private static final String FTP_HOST_NOT_REACHABLE = "FTP host is not reachanble.";
	private static final String ECM_UPLOAD_SUCCESS = "Process completed successfully. Please check uploaded pdf status for details.";
	private static final String ECM_UPLOAD_FAILED = "Process completed with error. Please check uploaded pdf status for details.";
	private static final String FTP_DIRECTORY_DOES_NOT_EXIST = " does not exist. Please check configuration.";
	private static final String FTP_DIRECTORY = "Configured FTP directory ";

	@Autowired
	private ContentManagementService ecmService;

	@Autowired
	private PlanDocumentsJobService planDocumentsJobService;

	@Autowired
	private GHIXSFTPClient ftpClient;

	@RequestMapping(value = "/admin/serff/getUploadBrochureCount", method = RequestMethod.GET)
	public String getUploadBrochure(Model model) {
		LOGGER.info(CLASS_NAME + ".getfile count Begin");
		int uploadFileCount = 0;

		// Check whether ftpclient is reachable and configured directory is
		// present on FTP server.
		try {
			ftpClient.initConnection();
			String ftpDirectory = GhixConstants.SERFF_CSR_HOME_PATH
					+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + "/"
					+ GhixConstants.FTP_BROCHURE_PATH;
			if (!ftpClient.isRemoteDirectoryExist(ftpDirectory)) {
				LOGGER.error("Configred FTP directory does not exist in server. Check configuration.");
				model.addAttribute("uploadBrochureMessage", FTP_DIRECTORY
						+ GhixConstants.SERFF_CSR_HOME_PATH
						+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + "/"
						+ GhixConstants.FTP_BROCHURE_PATH
						+ FTP_DIRECTORY_DOES_NOT_EXIST);
				return RETURN_UPLOAD_BROCHURE_PAGE;
			}
		} catch (Exception e) {
			LOGGER.error(" FTP host is not reachable " + e.getMessage(), e);
			model.addAttribute("uploadBrochureMessage", FTP_HOST_NOT_REACHABLE);
			return RETURN_UPLOAD_BROCHURE_PAGE;
		}

		try {
			SerffUploadBrochureUtils serffUploadBrochureUtils = SerffUploadBrochureUtils
					.getSerffUploadBrochureUtilsSingleton();
			// Get count of files to be uploaded in ECM.
			String dropboxPath = GhixConstants.SERFF_CSR_HOME_PATH
					+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + "/"
					+ GhixConstants.FTP_BROCHURE_PATH;
			LOGGER.info(" Getting files to be uploaded from location "
					+ dropboxPath);
			uploadFileCount = serffUploadBrochureUtils.getFileCount(
					dropboxPath, ftpClient);
			model.addAttribute("uploadBrochureMessage", "There are "
					+ uploadFileCount + " files to process.");
			model.addAttribute("uploadFileCount", uploadFileCount);
			// model.addAttribute("remoteFileList", remoteFileList);
		} catch (Exception e) {
			LOGGER.error(" FTP host is not reachable " + e.getMessage(), e);
			model.addAttribute("uploadBrochureMessage", FTP_HOST_NOT_REACHABLE);
			LOGGER.error("Exception: " + e.getMessage(), e);
		}

		return RETURN_UPLOAD_BROCHURE_PAGE;
	}

	@RequestMapping(value = "/admin/serff/uploadBrochure", method = RequestMethod.GET)
	public String uploadBrochure(Model model) {
		LOGGER.info(CLASS_NAME + ".upload Begin");
		try {
			if (SerffUploadBrochureUtils.isUploadRunning()) {
				model.addAttribute("uploadBrochureMessage",
						"Upload process is already running.");
				return RETURN_UPLOAD_BROCHURE_PAGE;
			} else {
				// Check whether ftpclient is reachable.
				try {
					ftpClient.initConnection();
					String ftpDirectory = GhixConstants.SERFF_CSR_HOME_PATH
							+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + "/"
							+ GhixConstants.FTP_BROCHURE_PATH;
					if (!ftpClient.isRemoteDirectoryExist(ftpDirectory)) {
						LOGGER.error("Configred FTP directory does not exist in server. Check configuration.");
						model.addAttribute(
								"uploadBrochureMessage",
								FTP_DIRECTORY
										+ GhixConstants.SERFF_CSR_HOME_PATH
										+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH
										+ "/"
										+ GhixConstants.FTP_BROCHURE_PATH
										+ FTP_DIRECTORY_DOES_NOT_EXIST);
						return RETURN_UPLOAD_BROCHURE_PAGE;
					}
				} catch (Exception e) {
					LOGGER.error(
							" FTP host is not reachable " + e.getMessage(), e);
					model.addAttribute("uploadBrochureMessage",
							FTP_HOST_NOT_REACHABLE);
					return RETURN_UPLOAD_BROCHURE_PAGE;
				}
				SerffUploadBrochureUtils serffUploadBrochureUtils = SerffUploadBrochureUtils
						.getSerffUploadBrochureUtilsSingleton();
				// Check whether files to upload count is zero.
				String dropboxPath = GhixConstants.SERFF_CSR_HOME_PATH
						+ GhixConstants.FTP_SERVER_UPLOAD_SUBPATH + "/"
						+ GhixConstants.FTP_BROCHURE_PATH;
				LOGGER.info(" Uploading brochure files. Getting files to be uploaded from location "
						+ dropboxPath);
				int uploadFileCount = serffUploadBrochureUtils.getFileCount(
						dropboxPath, ftpClient);
				if (uploadFileCount == 0) {
					// Do not initiate upload process.
					LOGGER.info(CLASS_NAME
							+ ".Not initiating upload process due to zero file count.");
					model.addAttribute("uploadBrochureMessage", "There are "
							+ uploadFileCount + " files to process.");
					return RETURN_UPLOAD_BROCHURE_PAGE;
				} else if (uploadFileCount > 0) {
					// Initiate upload process.
					boolean uploadStatus = serffUploadBrochureUtils
							.uploadBrochure(ecmService,
									planDocumentsJobService, ftpClient);
					if (uploadStatus) {
						// All files upload process completed successfully as a
						// unit.
						LOGGER.info(CLASS_NAME + ".upload end successfully.");
						model.addAttribute("uploadBrochureMessage",
								ECM_UPLOAD_SUCCESS);
					} else {
						// There is some error while uploading files to ECM.
						LOGGER.info(CLASS_NAME + ".upload end with error.");
						model.addAttribute("uploadBrochureMessage",
								ECM_UPLOAD_FAILED);
					}
				}
				model.addAttribute("uploadFileCount", uploadFileCount);
			}

		} catch (Exception e) {
			// There is some error while uploading files to ECM.
			model.addAttribute("uploadBrochureMessage", ECM_UPLOAD_FAILED);
			LOGGER.error("Exception: " + e.getMessage(), e);
		}
		return RETURN_UPLOAD_BROCHURE_PAGE;
	}

	@RequestMapping(value = "/admin/serff/uploadBrochure", method = RequestMethod.POST)
	public String uploadBrochureforPost(Model model) {
		LOGGER.info(CLASS_NAME + ".upload Begin");

		return RETURN_UPLOAD_BROCHURE_PAGE;
	}

}
