/**
 * 
 */
package com.getinsured.hix.enrollment.web;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.dto.enrollment.AetnaEnrollmentDTO;

/**
 * @author raja
 * @since 08/07/2013
 */

@Controller
@RequestMapping(value = "/aetna")
public class AetnaEnrollmentController {
	
	private static final Logger LOGGER = Logger.getLogger(AetnaEnrollmentController.class);
	
	@Autowired private RestTemplate restTemplate;
	
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody public String welcome(){
		
		LOGGER.info("Welcome to Aetna Enrollment Web Service Controller");
		
		return "Welcome to Aetna Enrollment Web Service Controller";
	}
	
	/**
	 * @author raja
	 * This method is used to test the working of aetna enrollment web service mock page 
	 * @return
	 */
	@RequestMapping(value = "/enrollment/webservice/mock/page", method = RequestMethod.GET)
	public String aetnaEnrollmentWebServicMockPage(Model model){
		LOGGER.info("Aetna Enrollment Web Service Controller :: aetnaEnrollmentWebServicMockPage()");
		return "aetna/enrollment/webservice/mock/page";
	}
	
	@RequestMapping(value = "/enrollment/webservice/mock/submit", method = RequestMethod.POST)
	@ResponseBody public String aetnaEnrollmentWebServicMockSubmit(@ModelAttribute("AetnaEnrollmentDTO") AetnaEnrollmentDTO aetnaEnrollmentDTO){
		
		LOGGER.info("Aetna Enrollment Web Service Controller :: aetnaEnrollmentWebServicMockPageSubmit()");
		
//		HIX-67795 Fix Cross Site Scripting: Persistent issues in enrollment
		
/*		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		String postResp = null;
		String responseStr=null;
		try{
			postResp = restTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.AETNA_ENROLLMENT_APPLICATION_SUBMISSION_URL, xstream.toXML(aetnaEnrollmentDTO),String.class);
			responseStr = "Aetna application submission processed successfully..........!\n"+postResp;
		}
		catch(Exception e){
			responseStr = "Error in processing the aetna application submission";
		}*/
		
	 return null;
	}
	
}
