package com.getinsured.iex.hub.ssac.extension;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * HIX DSH SSA Composite Service
 *
 * This class was generated by Apache CXF 2.7.8
 * 2014-01-23T15:06:17.035-08:00
 * Generated source version: 2.7.8
 * 
 */
@WebServiceClient(name = "SSACompositeService", 
                  wsdlLocation = "verify_ssa_criteria.wsdl",
                  targetNamespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov") 
public class SSACompositeService extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositeService");
    public final static QName SSACompositePort = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositePort");
    static {
        URL url = SSACompositeService.class.getResource("verify_ssa_criteria.wsdl");
        if (url == null) {
            url = SSACompositeService.class.getClassLoader().getResource("verify_ssa_criteria.wsdl");
        } 
        if (url == null) {
            java.util.logging.Logger.getLogger(SSACompositeService.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "verify_ssa_criteria.wsdl");
        }       
        WSDL_LOCATION = url;
    }

    public SSACompositeService(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public SSACompositeService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public SSACompositeService() {
        super(WSDL_LOCATION, SERVICE);
    }
    

    /**
     *
     * @return
     *     returns SsaCompositePortType
     */
    @WebEndpoint(name = "SSACompositePort")
    public SsaCompositePortType getSSACompositePort() {
        return super.getPort(SSACompositePort, SsaCompositePortType.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns SsaCompositePortType
     */
    @WebEndpoint(name = "SSACompositePort")
    public SsaCompositePortType getSSACompositePort(WebServiceFeature... features) {
        return super.getPort(SSACompositePort, SsaCompositePortType.class, features);
    }

}
