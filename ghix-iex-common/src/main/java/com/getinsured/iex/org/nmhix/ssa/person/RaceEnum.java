//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.persistence.Entity;
import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Race-Enum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Race-Enum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AMERICAN_INDIAN_OR_ALASKA_NATIVE"/>
 *     &lt;enumeration value="ASIAN_INDIAN"/>
 *     &lt;enumeration value="AFRICAN_AMERICAN"/>
 *     &lt;enumeration value="CHINESE"/>
 *     &lt;enumeration value="FILIPINO"/>
 *     &lt;enumeration value="GUAMANIAN_OR_CHAMORRO"/>
 *     &lt;enumeration value="JAPANESE"/>
 *     &lt;enumeration value="KOREAN"/>
 *     &lt;enumeration value="NATIVE_HAWAIIAN"/>
 *     &lt;enumeration value="OTHER_ASIAN"/>
 *     &lt;enumeration value="OTHER_PACIFIC_ISLANDER"/>
 *     &lt;enumeration value="SAMOAN"/>
 *     &lt;enumeration value="VIETNAMESE"/>
 *     &lt;enumeration value="WHITE_OR_CAUCASIAN"/>
 *     &lt;enumeration value="OTHER"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Race-Enum")
@XmlEnum


public enum RaceEnum {

    AMERICAN_INDIAN_OR_ALASKA_NATIVE,
    ASIAN_INDIAN,
    AFRICAN_AMERICAN,
    CHINESE,
    FILIPINO,
    GUAMANIAN_OR_CHAMORRO,
    JAPANESE,
    KOREAN,
    NATIVE_HAWAIIAN,
    OTHER_ASIAN,
    OTHER_PACIFIC_ISLANDER,
    SAMOAN,
    VIETNAMESE,
    WHITE_OR_CAUCASIAN,
    OTHER;

    public String value() {
        return name();
    }

    public static RaceEnum fromValue(String v) {
        return valueOf(v);
    }

}
