package com.getinsured.ahbx.broker.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.ahbx.broker.repository.IExternalIndividualRepository;

/**
 * Implements {@link ExternalIndividualService} to find and save external
 * individuals
 * 
 */

@Service("externalIndividualService")
@Transactional
public class ExternalIndividualServiceImpl implements ExternalIndividualService {

	@Autowired
	private IExternalIndividualRepository externalIndividualRepository;

	/**
	 * Saves external individual when external individual bob detail is fetched
	 * from AHBX
	 * 
	 * @see ExternalIndividualService#save(ExternalIndividual)
	 * 
	 * @param externalIndividual
	 * @return ExternalIndividual
	 */

	@Override
	@Transactional
	public ExternalIndividual save(ExternalIndividual externalIndividual) {
		return externalIndividualRepository.save(externalIndividual);
	}

	/**
	 * Finds external individual based on individual case id
	 * 
	 * @see ExternalIndividualService#findByExternalIndividualID(long)
	 * 
	 * @param external_ind_Id
	 * @return ExternalIndividual
	 * 
	 */

	@Override
	@Transactional
	public ExternalIndividual findByExternalIndividualID(long external_ind_Id) {
		return externalIndividualRepository
				.findByIndividualCaseId(external_ind_Id);
	}

	/**
	 * Updates external individual when external individual bob status is
	 * fetched from AHBX
	 * 
	 * @see ExternalIndividualService#update(ExternalIndividual)
	 * 
	 * @param extInd
	 * @return ExternalIndividual
	 */

	@Override
	@Transactional
	public ExternalIndividual update(ExternalIndividual extInd) {

		ExternalIndividual externalIndividual = null;
		ExternalIndividual existingIndividual = externalIndividualRepository
				.findByIndividualCaseId(extInd.getId());
		if (existingIndividual != null) {
			existingIndividual.setEnrollmentStatus(extInd.getEnrollmentStatus());

			existingIndividual.setEligibilityStatus(extInd.getEligibilityStatus());
			existingIndividual.setHeadOfHouseholdName(extInd
					.getHeadOfHouseholdName());
			existingIndividual.setNumberOfHouseholdMembers(extInd
					.getNumberOfHouseholdMembers());
			if (extInd.getHouseholdIncome() != null) {
				existingIndividual.setHouseholdIncome(extInd.getHouseholdIncome());
			}
			externalIndividual = externalIndividualRepository
					.save(existingIndividual);

		}
		return externalIndividual;
	}

}