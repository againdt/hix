package com.getinsured.hix.indportal.dto;

import java.math.BigDecimal;
import java.util.List;

public class Member{
	private String id;
	private String suffix;
	private String firstName;
	private String middleName;
	private String lastName;
	private String dob;
	private Integer ageAtCoverageDate;
	private String relation;
	private BigDecimal aptc;
	private BigDecimal stateSubsidy;
	private List<String> canBeAddedInEnrolledGroup;
	private String csr;
	private String healthEnrollmentId;
	private String dentalEnrollmentId;
	private String encData;
	private String isNativeAmerican;
	private Boolean isSepOpen;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getSuffix() {
		return suffix;
	}
	public void setSuffix(String suffix) {
		this.suffix = suffix;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public Integer getAgeAtCoverageDate() {
		return ageAtCoverageDate;
	}
	public void setAgeAtCoverageDate(Integer ageAtCoverageDate) {
		this.ageAtCoverageDate = ageAtCoverageDate;
	}
	public String getRelation() {
		return relation;
	}
	public void setRelation(String relation) {
		this.relation = relation;
	}
	public BigDecimal getAptc() {
		return aptc;
	}
	public void setAptc(BigDecimal aptc) {
		this.aptc = aptc;
	}
	public List<String> getCanBeAddedInEnrolledGroup() {
		return canBeAddedInEnrolledGroup;
	}
	public void setCanBeAddedInEnrolledGroup(List<String> canBeAddedInEnrolledGroup) {
		this.canBeAddedInEnrolledGroup = canBeAddedInEnrolledGroup;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getEncData() {
		return encData;
	}
	public void setEncData(String encData) {
		this.encData = encData;
	}
	public String getHealthEnrollmentId() {
		return healthEnrollmentId;
	}
	public void setHealthEnrollmentId(String healthEnrollmentId) {
		this.healthEnrollmentId = healthEnrollmentId;
	}
	public String getDentalEnrollmentId() {
		return dentalEnrollmentId;
	}
	public void setDentalEnrollmentId(String dentalEnrollmentId) {
		this.dentalEnrollmentId = dentalEnrollmentId;
	}
	public String getIsNativeAmerican() {
		return isNativeAmerican;
	}
	public void setIsNativeAmerican(String isNativeAmerican) {
		this.isNativeAmerican = isNativeAmerican;
	}
	public Boolean getIsSepOpen() {
		return isSepOpen;
	}
	public void setIsSepOpen(Boolean isSepOpen) {
		this.isSepOpen = isSepOpen;
	}

	public BigDecimal getStateSubsidy() {
		return stateSubsidy;
	}

	public void setStateSubsidy(BigDecimal stateSubsidy) {
		this.stateSubsidy = stateSubsidy;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		if(this == obj){
			return true;
		}
		if((obj == null) || (obj.getClass() != this.getClass())){
			return false;
		}
		Member member = (Member)obj;
		return id.equals(member.id);
	}


}
