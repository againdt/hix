<%@page import="com.getinsured.hix.util.PlanMgmtConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/manageqdpplans"/>"><spring:message code="label.linkManageQDP"/></a></li>
            <li><spring:message  code='label.linkEnrollmentAvail'/></li>
        </ul><!--page-breadcrumb ends-->  
            <h1 class="span10 margin0"><a name="skip"></a><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${plan.name}</h1>
        </div>

	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code='label.aboutPlan'/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}" />"><spring:message  code='label.linkDentalPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpbenefits/${encPlanId}"/>"><spring:message  code='label.linkDentalPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdprates/${encPlanId}" />"><spring:message  code='label.linkDentalPlanRates'/></a></li>
					<c:if test="${displayProviderNetwork == 'YES'}">
						<li><a href="<c:url value="/admin/planmgmt/viewqdpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkDentalProviderNetwork'/></a></li>
					</c:if>
                    <li class="active"><spring:message  code='label.linkEnrollmentAvail'/></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
			   <br><br>
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message  code='label.actions'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqdpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			<form class="form-horizontal" id="frmEnrollAvail" name="frmEnrollAvail" action="<c:url value="/admin/planmgmt/editqdpenrollmentavail" />" method="POST">			
			<df:csrfToken/>
			<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
			<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
			<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>			
			<div class="header">
				<h4 class="pull-left"><spring:message  code='label.editEnrollmentAvail'/></h4>
				<a class="btn btn-small pull-right" title="<spring:message  code='label.btnCancel'/>" href="<c:url value="/admin/planmgmt/viewqdpenrollmentavail/${encPlanId}" />"><spring:message  code='label.btnCancel'/></a>
				<c:if test="${showSaveButton == true}">
					<a class="btn btn-primary btn-small pull-right" title="<spring:message  code='label.btnSave'/>" href="#" onclick="formSubmit()"><spring:message  code='label.btnSave'/></a>
				</c:if> 
				<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
			</div>
			
			<table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><spring:message  code='label.issuerName'/></td>
						<td><strong>${plan.issuer.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planName"/></td>
						<td><strong>${plan.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.planNumber"/></td>
						<td><strong>${plan.issuerPlanNumber}</strong></td>
					</tr>
					<tr>
                   		 		<td class="txt-right"><spring:message  code="label.planStartDate"/></td>
                   		 		<td><strong>${stDate}</strong></td>
                   			</tr>
                   			<tr>
                   				<td class="txt-right"><spring:message  code="label.planEndDate"/></td>
                   				<td><strong>${enDate}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.enrollAvail"/></td>
						<td><select name="enrollmentavail" id="enrollmentavail" size="1" class="valid">								
							<c:forEach var="enrollmentAvailList" items="${enrollmentAvailList}">
								<option value="${enrollmentAvailList.key}" <c:if test="${plan.enrollmentAvail==enrollmentAvailList.key}"><spring:message code='select.option'/></c:if> /> ${enrollmentAvailList.value}</option>								
							</c:forEach>	
						</select></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.effectiveDate"/></td>
						<td>
						<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.enrollmentAvailEffDate}" var="effDate"/>
						<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" />
							<div class="input-append date date-picker" id="date" data-date="${systemDate}" data-date-format="mm/dd/yyyy">
								<input	type="text" id="effectiveDate" name="effectiveDate"	class="datepick input-small" pattern="MM/dd/yyyy" value="<c:out value="${effDate}"/>">
								<span class="add-on"><i class="icon-calendar"></i></span>
							</div>	
							<div id="effectiveDate_error" class="help-inline"></div>
							<%if(null != request.getParameter("futureDateValidationError") && request.getParameter("futureDateValidationError").equalsIgnoreCase(PlanMgmtConstants.TRUE_STRING)) {%>
								<div id="futureDateValidationError_error" class="error help-inline">
									<label class="error"><span> <em class='excl'>!</em><spring:message  code='err.futureStatusEffectiveDate1'/> ${futureDate} <spring:message  code='err.futureStatusEffectiveDate2'/></span></label>
								</div>
	           				<%}%>					
						</td>
					</tr>
					<tr>
						<td class="txt-right vertical-align-top"><spring:message  code="label.changeJustification"/></td>
						<td>							
							<textarea class="input-xlarge" name="comments" id="comments" rows="4" cols="40" style="resize: none;" 
							maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();" onKeyPress="return(this.value.length < 500 );"></textarea>	                       
	                       	<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message  code="label.charLeftValue"/></b></div>
						</td>
					</tr>
				</tbody>
			</table>
			</form>
		</div><!-- end of .span9 -->
	</div>
</div>

<script type="text/javascript">
var commentMaxLen = 500;

$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	});
	/* $('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true,
			minDate: 1
		});
	}); */
	$('.date-picker').datepicker({
		startDate: '+'+ '${systemDate}' + 'd',
	    autoclose: true,
	 	format: 'mm/dd/yyyy'
	});
});


//Update remaining characters for comments
function updateCharCount(){	
	var currentLen = $.trim(document.getElementById("comments").value).length;
	var charLeft = commentMaxLen - currentLen;
	if(currentLen > commentMaxLen) {
		$("#comments").val($("#comments").val().substr(0, commentMaxLen));
		$('#chars_left').html('Characters left <b>' + 0 + '</b>' );
    }
	$('#chars_left').html("<spring:message  code='label.charactersLeft'/> <b>" + charLeft + "</b>" );
}
$.validator.addMethod("greaterThanToday", function(value, element) {
	var startdatevalue = '${currDate}';
	if (value == null || value.length <1)
		return true;
	return Date.parse(startdatevalue) <= Date.parse(value);
	});
	
$.validator.addMethod("greaterThanFutureStatusEffectiveDate", function(value, element) {
	var currDate = '${currDate}';
	var futureStatusEffectiveDate = '${futureDate}';	
	if (value == null || value.length <1)
		return true;
	if (futureStatusEffectiveDate == null || futureStatusEffectiveDate.length <1)
		return true;
			
	if((Date.parse(currDate) <= Date.parse(value)) && (Date.parse(value) <= Date.parse(futureStatusEffectiveDate))) {		
		return true;
	}
	
	return Date.parse(futureStatusEffectiveDate) <= Date.parse(currDate);
});

var validator = $("#frmEnrollAvail").validate({ 
	ignore: "",
	rules : {
		effectiveDate : { 
			required : true,
			greaterThanToday : true,
			greaterThanFutureStatusEffectiveDate : true}
	}, 
	messages : {
		effectiveDate : { 
			required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.validateEfffDate'/></span>", 
			greaterThanToday : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.validateEfffDate'/></span>",
			greaterThanFutureStatusEffectiveDate : "<span> <em class='excl'>!</em><spring:message  code='err.futureStatusEffectiveDate1'/> " + '${futureDate}' + " <spring:message  code='err.futureStatusEffectiveDate2'/></span>"
		}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');			
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	} 
});

function  formSubmit(){	
	$("#frmEnrollAvail").submit();
}


</script>
