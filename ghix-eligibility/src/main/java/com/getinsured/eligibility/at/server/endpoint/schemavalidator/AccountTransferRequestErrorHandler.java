package com.getinsured.eligibility.at.server.endpoint.schemavalidator;
import java.util.ArrayList;
import java.util.List;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


public class AccountTransferRequestErrorHandler implements ErrorHandler {

	private List<String> errors = new ArrayList<String>();

    @Override
	public void warning(SAXParseException exception) throws SAXException {
        errors.add(exception.getMessage());
    }

    @Override
	public void error(SAXParseException exception) throws SAXException {
        errors.add(exception.getMessage());
    }

    @Override
	public void fatalError(SAXParseException exception) throws SAXException {
        errors.add(exception.getMessage());
    }

	public List<String> getErrors() {
		return errors;
	}

}