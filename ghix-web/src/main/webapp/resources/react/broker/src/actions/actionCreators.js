import React from 'react';
import { Link } from 'react-router-dom';

// Services and utils
import API from '../services/API';
import {
  errorRetrieveTitle,
  errorRetrieveBody,
  errorSubmitTitle,
  errorSubmitBody,
  alertRemovalTitle,
  alertRemovalBody,
  errorSubmitAvailabilityTitle,
  errorSubmitAvailabilityBody,
  alertUpdateHoursTitle,
  alertUpdateHoursBody,
  alertUpdatePhoneTitle,
  alertUpdatePhoneBody,
  alertSuccessEnrolledTitle,
  alertSuccessEnrolledBody,
  errorNotEnrolledTitle,
  errorNotEnrolledLink,
  errorNotEnrolledBodyBefore,
  errorNotEnrolledBodyAfter
} from '../constants/alertMessages';

// Constants
import {
  alertActionTypes,
  brokerConnectActionTypes,
  brokerAvailabilityActionTypes,
  messagesActionTypes,
  onDemandActionTypes,
  spinnerActionTypes
} from '../constants/actionTypes';

import { BROKER_INACTIVE } from '../constants/AvailabilityConstants';

export const fetchParticipationDetails = url => async dispatch => {
  dispatch(removeAlert());
  dispatch(startSpinner('Loading'));
  const { data, error, errorType } = await API.getData(url);
  //error: true, errorMessage: 'Error 500', errorType: 'error', data: null
  if (error) {
    dispatch(stopSpinner());
    let alertTitle = errorRetrieveTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorRetrieveBody}</p>
    );

    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
    console.log('ERROR fetchParticipationDetails');
  } else {
    dispatch(stopSpinner());
    console.log('SUCCESS fetchParticipationDetails: ' + JSON.stringify(data));
    dispatch(setParticipationDetails(data));
  }
};

export const submitParticipationDetails = (
  url,
  body,
  history
) => async dispatch => {
  dispatch(removeAlert());
  dispatch(startSpinner('Loading'));
  const { data, error, errorType } = await API.postData(url, body);

  if (error) {
    console.log('Error submitParticipationDetails');
    dispatch(stopSpinner());
    let alertTitle = errorSubmitTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorSubmitBody}</p>
    );

    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
  } else {
    dispatch(stopSpinner());
    console.log('SUCCESS submitParticipationDetails: ' + JSON.stringify(data));
    let updateData = {
      phoneNumber: body.phoneNumber,
      status: body.status,
      isAvailable: body.available
    };
    dispatch(setParticipationDetails(updateData));

    // If enrolling in the program then direct to availability screen, else show unenroll success message
    if (history) {
      let alertTitle = alertSuccessEnrolledTitle;
      let alertMessage = (
        <p className="ds-c-alert__text gutter10">{alertSuccessEnrolledBody}</p>
      );
      dispatch(setAlert({ alertTitle, alertMessage, errorType }));
      history.push('/availability');
    } else {
      let alertTitle = alertRemovalTitle;
      let alertMessage = (
        <p className="ds-c-alert__text gutter10">{alertRemovalBody}</p>
      );
      dispatch(setAlert({ alertTitle, alertMessage, errorType }));
    }
  }
};

export const submitUpdatePhoneNumber = (url, body) => async dispatch => {
  dispatch(removeAlert());
  dispatch(startSpinner('Loading'));
  const { data, error, errorType } = await API.postData(url, body);

  if (error) {
    console.log('Error submitParticipationDetails');
    dispatch(stopSpinner());
    let alertTitle = errorSubmitTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorSubmitBody}</p>
    );

    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
  } else {
    dispatch(stopSpinner());
    let updateData = {
      phoneNumber: body.phoneNumber,
      status: body.status,
      isAvailable: body.available
    };
    dispatch(setParticipationDetails(updateData));
    let alertTitle = alertUpdatePhoneTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{alertUpdatePhoneBody}</p>
    );
    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
  }
};

export const fetchConnectDetails = url => async dispatch => {
  dispatch(startSpinner('Loading'));
  const { data, error, errorType } = await API.getData(url);

  if (error) {
    dispatch(removeAlert());
    dispatch(stopSpinner());
    let alertTitle = errorRetrieveTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorRetrieveBody}</p>
    );
    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
    //console.log('ERROR fetchConnectDetails');
  } else {
    dispatch(stopSpinner());
    //console.log('SUCCESS fetchConnectDetails: ' + JSON.stringify(data));

    // If broker is inactive when trying to fetch connect details, add alert error to screen
    if (data.status === BROKER_INACTIVE) {
      let linkTag = <Link to="participation">{errorNotEnrolledLink}</Link>;
      let message = (
        <p className="ds-c-alert__text gutter10">
          {errorNotEnrolledBodyBefore}
          {linkTag}
          {errorNotEnrolledBodyAfter}
        </p>
      );
      dispatch(
        setAlert({
          alertTitle: errorNotEnrolledTitle,
          alertMessage: message,
          errorType: 'error'
        })
      );
    }
    dispatch(setConnectDetails(data));
  }
};

export const submitConnectDetails = (url, body) => async dispatch => {
  dispatch(removeAlert());
  dispatch(startSpinner('Loading'));
  const { data, error, errorType } = await API.postData(url, body);

  if (error) {
    console.log('Error submitConnectDetails');
    dispatch(stopSpinner());
    let alertTitle = errorSubmitTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorSubmitBody}</p>
    );
    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
  } else {
    dispatch(stopSpinner());
    let alertTitle = alertUpdateHoursTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{alertUpdateHoursBody}</p>
    );
    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
    dispatch(setConnectDetails(body));
  }
};

export const submitAvailability = (url, body) => async dispatch => {
  const { data, error, errorType } = await API.postData(url, body);

  if (error) {
    console.log('Error submitAvailability');
    let alertTitle = errorSubmitAvailabilityTitle;
    let alertMessage = (
      <p className="ds-c-alert__text gutter10">{errorSubmitAvailabilityBody}</p>
    );
    dispatch(setAlert({ alertTitle, alertMessage, errorType }));
  } else {
    dispatch(setAvailableStatus(data));
  }
};

export const removeErrorDetails = () => dispatch => {
  dispatch(removeAlert());
};

export const setAvailableStatus = status => ({
  type: brokerAvailabilityActionTypes.SET_BROKER_AVAILABILITY,
  status
});

export const setConnectDetails = data => ({
  type: brokerConnectActionTypes.SET_BROKER_CONNECT_DATA,
  data
});

export const setParticipationDetails = data => ({
  type: onDemandActionTypes.SET_ON_DEMAND_DATA,
  data
});

export const setMessages = messages => ({
  type: messagesActionTypes.SET_MESSAGES,
  messages
});

export const setAlert = alertData => ({
  type: alertActionTypes.SET_ALERT,
  alertData
});

export const removeAlert = () => ({
  type: alertActionTypes.REMOVE_ALERT
});

export const startSpinner = message => ({
  type: spinnerActionTypes.START_SPINNER,
  message
});

export const stopSpinner = () => ({
  type: spinnerActionTypes.STOP_SPINNER
});
