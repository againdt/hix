package com.getinsured.ssap.model.financial;

import java.util.List;
import java.util.StringJoiner;

/**
 * Contains list of {@link IfsvHouseholdMember} objects.
 *
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
public class IfsvTaxHousehold {
  private List<IfsvHouseholdMember> householdMember;

  public List<IfsvHouseholdMember> getHouseholdMember() {
    return householdMember;
  }

  public void setHouseholdMember(List<IfsvHouseholdMember> householdMember) {
    this.householdMember = householdMember;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", IfsvTaxHousehold.class.getSimpleName() + "[", "]")
        .add("householdMember=" + householdMember)
        .toString();
  }
}
