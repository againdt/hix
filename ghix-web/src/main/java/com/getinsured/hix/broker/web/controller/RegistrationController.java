package com.getinsured.hix.broker.web.controller;

/**
 * Common Controller for all ghix users registration.
 * @author venkata_tadepalli
 * @since 12/08/2012
 */

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.SmartValidator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.ModelAndViewDefiningException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.account.dto.SignUpUser;
import com.getinsured.hix.account.util.UserChangeEmailManager;
import com.getinsured.hix.admin.service.AccountLockNotificationEmail;
import com.getinsured.hix.broker.PasswordRecoveryEmail;
import com.getinsured.hix.broker.PasswordRecoveryForgotPwdEmail;
import com.getinsured.hix.cap.consumer.dto.ConsumerDto;
import com.getinsured.hix.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.dto.account.AccountUserDto;
import com.getinsured.hix.dto.account.QuestionAnswer;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.RemoteValidationFailedException;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.DisplayUtil;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.SecurityQuestionsHelper;
import com.getinsured.hix.platform.util.SecurityQuestionsHelper.QuestionSet;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.ValidationUtility;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.platform.util.exception.WSO2Exception;
import com.getinsured.hix.referral.service.ReferralActivationService;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.iex.ssap.model.SsapApplication;
/*import java.util.regex.Matcher;
import java.util.regex.Pattern;*/
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

import ch.compile.recaptcha.ReCaptchaVerify;
import ch.compile.recaptcha.model.SiteVerifyResponse;

/**
 * Handles requests for the application home page.
 */
@SuppressWarnings("unused")
@Controller
public class RegistrationController {
	
	private static final String REFERRAL_ACTIVATION_ERROR = "ReferralActivationError";
	
	private static final String REDIRECT_EMAILVERIFICATION_URI = "redirect:/account/user/emailverification";

	private static final String IDAHO_STATE_CODE = "ID";
	
	private static final String REFERRAL_ACTIVATION_EXPIRED = "ReferralActivationExpired";

	private static final String ZERO = "0";

	private static final String CHECK_USER_PASSWORD_START = "checkUserPassword : START";

	private static final String ACCOUNT_ACTIVATION_SUBMIT_END = "accountActivationSubmit : END";

	private static final String ACTIVATION_ERROR="accountActivationException"; //HIX-27593
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	
	//private static final String RESET_PASSWORD_MSG = "Your cannot reset password before ";
	/* private Pattern pattern;
	  private Matcher matcher;*/
	private static final String VERIFICATION_CODE = "verificationCode";
	private static final String PHONE_NUMBER2 = "phoneNumber";
	private static final String CREATOR_NAME = "creatorName";
	private static final String EMAIL_ID = "emailId";
	private static final String SSN_REQUIRED = "ssnRequired";
	private static final String CREATED_TYPE = "createdType";
	private static final String CREATOR_TYPE = "creatorType";
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String CODE_SENT = "codeSent";
	private static final String PHONE_NUMBER_LIST = "phoneNumberList";
	private static final String USERNAME2 = "username";
	private static final String NO = "NO";
	private static final String FALSE = "false";
	private static final String ACCOUNT_ACTIVATION_FLOW = "accountActivationFlow";
	private static final String YES = "YES";
	private static final String USER_ID = "userId";
	private static final String CURRENT_SQ_RETRY_COUNT="currentSqRetryCount";
	private static final String MAX_SQ_RETRY_COUNT="maxSqRetryCount";
	private static final String INCORRECT_ANSWERS = "One or more of these answers do not match the answers in our system. Please try again!";
	private static final String UNSUCCESSFUL_ATTEMPTS = "Your account has been locked due to several unsuccessful attempts. Please call the customer service center at " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE) + " to unlock your account.";
	
	
//	private static final String SECURITY_ANSWER3 = "securityAnswer3";
	private static final String SECURITY_ANSWER2 = "securityAnswer2";
	private static final String SECURITY_ANSWER1 = "securityAnswer1";
//	private static final String SECURITY_QUESTION3 = "securityQuestion3";
	private static final String SECURITY_QUESTION2 = "securityQuestion2";
	private static final String SECURITY_QUESTION1 = "securityQuestion1";
	private static final String REDIRECT = "redirect:";
	private static final String ACTIVATION_EMAIL = "activationEmail";
	private static final String ACTIVATION_LAST_NAME = "activationLastName";
	private static final String ACTIVATION_FIRST_NAME = "activationFirstName";
	private static final String STATE_CODE = "STATE_CODE";
	//private static final String IS_CA_CALL = "IS_CA_CALL";
	private static final String CODE_TYPE = "codeType";
	private static final String DEF_USER_ROLE_NAME = "defUserRoleName";
	//private static final String SIGN_UP_ROLE = "signUpRole";
	private static final String PAGE_TITLE = "page_title";
	private static final String ACTIVATION_ID = "activationId";
	private static final String PHONEVERIFICATION_SENDCODE_URL = "account/phoneverification/sendcode";
	private static final String TRUE = "true";
	private static final String ERROR_MESSAGE = "errorMsg";
	private static final String USER_OBJECT = "userObj";
	private static final String FIRST_NAME = "firstName";
	private static final String LAST_NAME = "lastName";
	private static final String PHONE_NUMBER = "phone";
	private static final String USER_START_DATE = "userStartDate";
	private static final String USER_END_DATE = "userendDate";
	//private static final String ROLE_TITLE = "roleTitle";
	private static final String SIGUPUPPREVALUEMAP = "sigupUpPreValueMap";
	private static final String ISUSERFULLYAUTHORIZED = "isUserFullyAuthorized";
	/* HIX-38644 - Changes for State Referral Account Starts*/
	private static final String REDIRECT_TO_LOGIN = "redirect:/account/login/";
	/* HIX-38644 - Changes for State Referral Account Ends Here*/
	private static final String REDIRECT_TO_SIGNUP = "redirect:/account/signup/";
	private static final int THIRTY_TWO = 32;//HIX-30630
	/* private static final String PASSWORD_PATTERN = 
             "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";*/
	private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationController.class);
	private static final String STATEEXCHANGETYPE = "exchangeType";
	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;
	private static boolean isSSOEnabled = System.getProperty("GHIX_SECNTX_TYPE").equalsIgnoreCase("saml");

	@Autowired
	private UserService userService;
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private SCIMUserManager scimUserManager;
	
	@Autowired
	private AccountActivationService accountActivationService;
		
	@Autowired private ValidationUtility validationUtility;
	@Autowired private MessageSource messageSource;

	@Autowired
	private PasswordRecoveryForgotPwdEmail passwordRecoveryEmail;
	
	@Autowired
	private ReCaptchaVerify reCaptchaV2;

	@Autowired private AccountLockNotificationEmail accountLockNotificationEmail;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired SmartValidator validator;
	@Autowired private SessionTrackerService sessionTracker;
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	@Autowired private ReferralActivationService referralActivationService;
	
	@Autowired private UserRepository repository;
	@Autowired
	private ConsumerPortalUtil consumerUtil;
	
	@Autowired private IHouseholdRepository householdRepository;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired	@Qualifier("screenerUtil") private ScreenerUtil screenerUtil;
	
	@Autowired
	private UserChangeEmailManager userChangeEmailManager;
	
	@RequestMapping(value = "/account/signup/{defUserRoleName}", method = RequestMethod.GET)
	@GiAudit(transactionName = "Signup Page", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_READ)
	/**
	 * adds FlashAttribute for redirection to signup page
	 * @param model
	 * @param redirectAttributes
	 * @param defUserRoleName
	 * @return String
	 */
	public String accountSignup(@ModelAttribute("SignUpUser") SignUpUser signUpUserTest,Model model, RedirectAttributes redirectAttributes,
			@PathVariable(DEF_USER_ROLE_NAME) String defUserRoleName) {

		LOGGER.info("accountSignup : START");
		
		@SuppressWarnings("unchecked")
		Map<String, SignUpUser> flashAtrs = (Map<String, SignUpUser>) redirectAttributes.getFlashAttributes();
		
		SignUpUser signUpUser = (SignUpUser) model.asMap().get("signUpUserFlash");
		String bypassPreventAddConsumerConfig = (String) model.asMap().get("bypassPreventAddConsumerConfig");
		

		String decryptValue = ghixJasyptEncrytorUtil.decryptStringByJasypt(defUserRoleName);
		GiAuditParameterUtil.add(new GiAuditParameter("Role Name", decryptValue));
		redirectAttributes.addFlashAttribute("encRoleName", defUserRoleName);
		if(signUpUser==null){
			signUpUser = new SignUpUser();
			signUpUser.setEncRoleName(defUserRoleName);
		}
		//SignUpUser 
		redirectAttributes.addFlashAttribute("signUpUser", signUpUser);
		LOGGER.info("In ROLE based signup user  GET  : "+ bypassPreventAddConsumerConfig);
		redirectAttributes.addFlashAttribute("bypassPreventAddConsumerConfig", bypassPreventAddConsumerConfig);
		return "redirect:/account/signup";
	}


	/**
	 * Display account signup page
	 * HIX-46480 : Configuration to set the number of SecurityQuestion Sets ;  Enhanced to support configurable no of security questions
	 * -Venkata Tadepalli
	 * @param model
	 * @param redirectAttributes
	 * @param request
	 * @return String URL
	 */
	@RequestMapping(value = "/account/signup", method = RequestMethod.GET)
	@GiAudit(transactionName = "Signup Page", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_READ)
	public String accountsignup(@ModelAttribute("SignUpUser") SignUpUser signUpUserTest,Model model, RedirectAttributes redirectAttributes, HttpServletRequest request)
			throws GIException {
		LOGGER.info("accountsignup : START");

		@SuppressWarnings("unchecked")
		Map<String, Object> flashAtrs = (Map<String, Object>) redirectAttributes.getFlashAttributes();
		
		String encRoleName = (String) model.asMap().get("encRoleName");
		//@SuppressWarnings("unchecked")
				//Map<String, SignUpUser> flashAtrs = (Map<String, SignUpUser>) redirectAttributes.getFlashAttributes();
		if (encRoleName == null) {
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Expected encrypted role name for sign-up , found none");
			}
			return "redirect:/";
		}
				
		SignUpUser signUpUser = (SignUpUser) model.asMap().get("signUpUser");
		if(signUpUser==null){
			signUpUser = new SignUpUser();
		}
				
		signUpUser.setEncRoleName(encRoleName);
		
		
		
		String signUpUserRoleName = ghixJasyptEncrytorUtil.decryptStringByJasypt(encRoleName);
		String bypassPreventAddConsumerConfig = (String) model.asMap().get("bypassPreventAddConsumerConfig");
		LOGGER.info("In signup user GET before nosignup  : "+ bypassPreventAddConsumerConfig);
		//HIX-113428 Redirecting consumers to static page if prevent add consumer is true
		String preventAddConsumer = DynamicPropertiesUtil.getPropertyValue("global.preventAddConsumer");
		if(!StringUtils.equalsIgnoreCase("Y", bypassPreventAddConsumerConfig) && StringUtils.equalsIgnoreCase(signUpUserRoleName, RoleService.INDIVIDUAL_ROLE) && StringUtils.equalsIgnoreCase(preventAddConsumer, "true")) {
			return "account/nosignup";
		}
		
		//HIX-52234 - Re-direct to splash page if consumer signup is disabled
		String enableConsumerSignup = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ENABLE_CONSUMER_SIGNUP);
		GiAuditParameterUtil.add(new GiAuditParameter("Sign up user role name", signUpUserRoleName), new GiAuditParameter("enableConsumerSignupFlag", enableConsumerSignup));
		if(StringUtils.equalsIgnoreCase("N", enableConsumerSignup) && 
				StringUtils.equalsIgnoreCase(signUpUserRoleName, RoleService.INDIVIDUAL_ROLE)){
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to signupsplashpage"));
			return "account/signupsplashpage";
		}
		
		String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
		Role signUpRole = roleService.findRoleByName(signUpUserRoleName);
		
		// HIX-36252 
		if ((signUpRole == null)||(signUpRole.getPrivileged()==1 && !isValidActivationData(signUpUserRoleName, activationId))) {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Invalid SignUp Role Name"));
			//throw new GIException("Invalid SignUp Role Name::" + signUpUserRoleName);
			LOGGER.error("Invalid SignUp Role Name::"+signUpUserRoleName);
			return "redirect:/";

		}
		
		signUpUser.setSignUpRole(signUpRole);

		String pageTitle = String.format("Getinsured Health Exchange:  %s Registration", signUpRole.getLabel());

		LOGGER.debug("Registration page ::" + signUpRole.getName());

		
	
		/**
		 * this attribute is added for California Exchange to hide password
		 * details
		 */
		
		//model.addAttribute(IS_CA_CALL, GhixConfiguration.IS_CA_CALL.toString().toLowerCase());
		model.addAttribute(STATE_CODE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

		// Changes done for Jira Id: HIX-13729
		if(StringUtils.isNotBlank(activationId)){
			//if the signup from activation flow
			signUpUser.setActivationFlow(true);
			signUpUser.setActivationId(activationId);
			String activationFirstName = (String) request.getSession().getAttribute(FIRST_NAME);
			if (StringUtils.isNotEmpty(activationFirstName)) {
				//model.addAttribute(ACTIVATION_FIRST_NAME, activationFirstName);
				signUpUser.setFirstName(activationFirstName);
				request.getSession().removeAttribute(FIRST_NAME);
			}
			String activationLastName = (String) request.getSession().getAttribute(LAST_NAME);
			if (StringUtils.isNotEmpty(activationLastName)) {
				//model.addAttribute(ACTIVATION_LAST_NAME, activationLastName);
				signUpUser.setLastName(activationLastName);
				request.getSession().removeAttribute(LAST_NAME);
			}
			String activationEmail = (String) request.getSession().getAttribute(EMAIL_ID);
			if (StringUtils.isNotEmpty(activationEmail)) {
				//model.addAttribute(ACTIVATION_EMAIL, activationEmail);
				signUpUser.setEmail(activationEmail);
				request.getSession().removeAttribute(EMAIL_ID);
			}
			String phone = ((String) request.getSession().getAttribute(PHONE_NUMBER));
			if (StringUtils.isNotEmpty(phone)) {
				signUpUser.setPhone1(phone.substring(0,3));
				signUpUser.setPhone2(phone.substring(3,6));
				signUpUser.setPhone3(phone.substring(6));
				request.getSession().removeAttribute(PHONE_NUMBER);
			}
			try {
				Household household = null;
				
				String houseHoldId = (String) request.getSession().getAttribute(ReferralConstants.HOUSEHOLD_ID);
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("signup householdId = {}", houseHoldId );
				}
				if(houseHoldId != null) {
					household = householdRepository.findById(Integer.valueOf(houseHoldId));
				}
				
				if(household == null && activationEmail != null) {
					household = householdRepository.findByEmailAddress(activationEmail.toLowerCase());
				}
				
				if(household != null) {
					if ((household.getSsn() == null || household.getSsn().isEmpty()) &&
					(household.getSsnNotProvidedReason() == null || household.getSsnNotProvidedReason().isEmpty())) {
						model.addAttribute(SSN_REQUIRED, true);
						model.addAttribute("dob",  DateUtil.dateToString(household.getBirthDate(), DATE_FORMAT));
					}
				}else {
					if(signUpUserRoleName.compareToIgnoreCase("Individual") == 0) {
						model.addAttribute(SSN_REQUIRED, true);
					}
				}
			}catch(Exception e) {
				LOGGER.error("Error in retrieving household based for email address: ", activationEmail);
			}

		}else {
			if(signUpUserRoleName.compareToIgnoreCase("Individual") == 0) {
				model.addAttribute(SSN_REQUIRED, true);
			}
		}

		// START : HIX-46480
		int noOfSecQtns = 2; //default no of questions 2
		List<String>userSecurityQuestion = new ArrayList<String>();
		
		String strNoOfSecQtns = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.NO_OF_SECURITY_QUESTIONS.getValue());
		
		if(StringUtils.isNotBlank(strNoOfSecQtns)){
			int tempNoOfSecQtns = Integer.parseInt(strNoOfSecQtns);
			
			if(tempNoOfSecQtns>0 && tempNoOfSecQtns<4){ //The no of questions should be between 1 -3
				noOfSecQtns = tempNoOfSecQtns;
			}
			
		}
		Hashtable<String,List<QuestionSet>> htQuestionSet = new Hashtable<String,List<QuestionSet>>();
		for(int secQtnIdx=1;secQtnIdx<=noOfSecQtns;secQtnIdx++){
			List<QuestionSet> questionSet = new SecurityQuestionsHelper().getQuestionSet(secQtnIdx);
			htQuestionSet.put("securityQuestionSet"+secQtnIdx, questionSet);
			
		}
		
		model.addAttribute("htQuestionSet",htQuestionSet );
		
		/*model.addAttribute("securityQuestionsList1", new SecurityQuestionsHelper().getQuestionSetOne());
		if(!StringUtils.equalsIgnoreCase(stateCode, IDAHO_STATE_CODE)) {
			model.addAttribute("securityQuestionsList2", new SecurityQuestionsHelper().getQuestionSetTwo());
		}*/
		//	model.addAttribute("securityQuestionsList3", new SecurityQuestionsHelper().getQuestionSetThree());
		
		//END : HIX-464680
		request.getSession().setAttribute(ISUSERFULLYAUTHORIZED, "false"); 

		Object dataMap = request.getSession().getAttribute(SIGUPUPPREVALUEMAP);
		if(dataMap != null) {
			@SuppressWarnings("unchecked")
			Map<String,String> sigupUpPreValueMap = (Map<String,String>) dataMap;
			model.addAttribute(SIGUPUPPREVALUEMAP, sigupUpPreValueMap);
			model.addAttribute("captchaError", "Incorrect Security Code");
			request.getSession().removeAttribute(SIGUPUPPREVALUEMAP);
		}
		
		boolean hideCaptcha = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDECAPTCHA));
		model.addAttribute("hideCaptcha", hideCaptcha);
		
		boolean hideTermsCondition = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDETERMSANDCONDITIONS));
		model.addAttribute("hideTermsCondition", hideTermsCondition);
		
		model.addAttribute("signUpUser", signUpUser);
		model.addAttribute(PAGE_TITLE, pageTitle);
		//model.addAttribute(SIGN_UP_ROLE, titleCaseString);
		//model.addAttribute(DEF_USER_ROLE_NAME, signUpEncRoleName);

		return "account/signup";
	}

	/**
	 * Saves account signup page details
	 *
	 * @param accountUser
	 *            {@link AccountUser}
	 * @param defUserRoleName
	 *            User role name
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link ModelAndViewDefiningException}
	 * @param redirectAttributes
	 *            {@link RedirectAttributes}
	 * @param request
	 *            {@link HttpServerErrorException}
	 * @return URL for navigation
	 * @throws InvalidUserException
	 * @throws GIException 
	 */
	@RequestMapping(value = "/account/signup", method = RequestMethod.POST)
	@GiAudit(transactionName = "Signup Page", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	public String accountsubmit(@ModelAttribute("SignUpUser") @Validated(SignUpUser.SignUpUserData.class) SignUpUser signUpUser,
			 		BindingResult result, Model model,	RedirectAttributes redirectAttributes, HttpServletRequest request, 
			 		@RequestParam(value = "g-recaptcha-response", required = false) String gRecaptchaResponse, 
			 		ServletRequest servletRequest) throws InvalidUserException, GIException {
		LOGGER.info("accountsubmit : START");

		String signUpUserRoleName = ghixJasyptEncrytorUtil.decryptStringByJasypt(signUpUser.getEncRoleName());

		Role signUpRole = roleService.findRoleByName(signUpUserRoleName);
		
		LOGGER.debug("Registration submit page for :: " + signUpRole.getLabel());


		String remoteAddress = servletRequest.getRemoteAddr();	
		GiAuditParameterUtil.add(new GiAuditParameter("SignUp User RoleName", signUpUserRoleName), new GiAuditParameter("Remote Address", remoteAddress));
		
		this.validateSignUpUser(signUpUser, result); //HIX-47101 : if the request had received tampared data the will throw GIRuntimeException
		
		AccountUser accountUser =signUpUser.getAccountUser();
			String errorMsg = "";
			boolean valid = true;
			String houseHoldId = null;
			String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
			   String ssn = request.getParameter("ssn");
			   String birthDate = request.getParameter("birthDate");
			   boolean bCheckSSN = false;
			   if(!validationUtility.checkForPasswordComplexity(accountUser.getPassword(), DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))
	        		  ){
	        	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
	        	   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
	        	   request.getSession().setAttribute("passwordError", errorMsg);
	        	   valid = false;
			   }
			   
			   else if(signUpRole.getName().equalsIgnoreCase("Individual")) {
					
					Household household = null;
					 try {
						 houseHoldId = (String) request.getSession().getAttribute(ReferralConstants.HOUSEHOLD_ID);
							
							if(houseHoldId != null) {
								household = householdRepository.findById(Integer.valueOf(houseHoldId));
							}
							
							if(household == null ) {
								 if(accountUser.getEmail() != null) {
								    	household = householdRepository.findByEmailAddress(accountUser.getEmail().toLowerCase());
								    }
							}
							
					   }catch(Exception e) {
						   LOGGER.error("Error in fetching household based on email address from CMR_Household table, exception:",e);
						   return "error/accountSignupError";
					   }
					
					if(household != null) {
						if (activationId != null){
							//activation flow and ssn required
							if((household.getSsnNotProvidedReason() == null || household.getSsnNotProvidedReason().isEmpty() ) &&
									(household.getSsn() == null || household.getSsn().isEmpty())){
								bCheckSSN = true; 
							}
						}
					} else {
						bCheckSSN = true; // new user creation
					}
					if(bCheckSSN) {
					   if(!Utils.validateSSN(ssn)) {
						   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validateSSN", null, LocaleContextHolder.getLocale()));
			        	   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
			        	   request.getSession().setAttribute("ssnError", errorMsg);
			        	   valid = false;
					   }else {
						   Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
						   household = householdRepository.findBySSN(ssn);
						   if(household != null) { // Household with this SSN and birthdate already exists
							   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validateSSN", null, LocaleContextHolder.getLocale()));
				        	   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
				        	   request.getSession().setAttribute("ssnError", errorMsg);
				        	   valid = false;
						   }
					   }
				   }
			   }
			   if( !Utils.isPureAscii(request.getParameter("firstName")) || !Utils.isPureAscii(request.getParameter("lastName"))) {
				   valid = false;
				   LOGGER.error("Invalid first name/last name provided while adding a User");
			   }
			  
			   if(!valid) {
	        	   Map<String, String> dataMap = new HashMap<String, String>();
	   			
	   			dataMap.put("confirmEmail", request.getParameter("confirmEmail"));
	   			//dataMap.put("phone", request.getParameter(PHONE_NUMBER));
	   			dataMap.put("phone1", request.getParameter("phone1"));
	   			dataMap.put("phone2", request.getParameter("phone2"));
	   			dataMap.put("phone3", request.getParameter("phone3"));
	   			dataMap.put("securityQuestion1", request.getParameter("securityQuestion1"));
	   			dataMap.put("securityAnswer1", request.getParameter("securityAnswer1"));
	   			if(!isScimEnabled) {
	   				dataMap.put("securityQuestion2", request.getParameter("securityQuestion2"));
		   			dataMap.put("securityAnswer2", request.getParameter("securityAnswer2"));
	   			}
	   			dataMap.put("userNameCheck", request.getParameter("userNameCheck"));
	   			dataMap.put("defUserRoleName", request.getParameter("defUserRoleName"));
	   			dataMap.put("activationId", request.getParameter("activationId"));
	   			dataMap.put("ssn", ssn);
	   			dataMap.put("birthDate", request.getParameter("birthDate"));
	   			request.getSession().setAttribute(EMAIL_ID, request.getParameter("email"));
	   			request.getSession().setAttribute(FIRST_NAME, request.getParameter("firstName"));
	   			request.getSession().setAttribute(LAST_NAME, request.getParameter("lastName"));
	   			
	   			request.getSession().setAttribute(SIGUPUPPREVALUEMAP, dataMap);
	   			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to "+REDIRECT_TO_SIGNUP+signUpUser.getEncRoleName()));
	   			return REDIRECT_TO_SIGNUP+signUpUser.getEncRoleName();
	           }
		
		
		boolean isValidCAPTCHA = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDECAPTCHA));
		if(!isValidCAPTCHA) {
			try {
				SiteVerifyResponse siteVerifyResponse = reCaptchaV2.verify(gRecaptchaResponse);
				if ( siteVerifyResponse.isSuccess() ) {
					isValidCAPTCHA = true;
				} else {
					isValidCAPTCHA = false;
				}
			} catch (IOException e) {
				isValidCAPTCHA = true;//setting to true incase of captcha issue.
				LOGGER.error("Error happened while validating recaptcha",e);
			}
		}
		
		// Check for the CAPTCHA code validation
		if(!isValidCAPTCHA) {
			Map<String, String> dataMap = new HashMap<String, String>();
			
			dataMap.put("confirmEmail", request.getParameter("confirmEmail"));
			//dataMap.put("phone", request.getParameter(PHONE_NUMBER));
			dataMap.put("phone1", request.getParameter("phone1"));
			dataMap.put("phone2", request.getParameter("phone2"));
			dataMap.put("phone3", request.getParameter("phone3"));
			dataMap.put("securityQuestion1", request.getParameter("securityQuestion1"));
			dataMap.put("securityAnswer1", request.getParameter("securityAnswer1"));
			if(!isScimEnabled) {
   				dataMap.put("securityQuestion2", request.getParameter("securityQuestion2"));
	   			dataMap.put("securityAnswer2", request.getParameter("securityAnswer2"));
   			}
			dataMap.put("userNameCheck", request.getParameter("userNameCheck"));
			dataMap.put("defUserRoleName", request.getParameter("defUserRoleName"));
			dataMap.put("activationId", request.getParameter("activationId"));
			dataMap.put("ssn", ssn);
			dataMap.put("birthDate", request.getParameter("birthDate"));
			request.getSession().setAttribute(EMAIL_ID, request.getParameter("email"));
			request.getSession().setAttribute(FIRST_NAME, request.getParameter("firstName"));
			request.getSession().setAttribute(LAST_NAME, request.getParameter("lastName"));
			
			request.getSession().setAttribute(SIGUPUPPREVALUEMAP, dataMap);
			redirectAttributes.addFlashAttribute("signUpUserFlash", signUpUser);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "If !isvalid captcha Redirecting to "+REDIRECT_TO_SIGNUP+signUpUser.getEncRoleName()));
			return REDIRECT_TO_SIGNUP+signUpUser.getEncRoleName();
		}
		
		/*
		 * For the GHIX users, user email as username
		 */
		String userEmail = StringUtils.lowerCase(accountUser.getEmail().toLowerCase());
		accountUser.setUserName(userEmail); // Set the userEmail as userName for
											// login
		String phone1=request.getParameter("phone1");
		String phone2=request.getParameter("phone2");
		String phone3=request.getParameter("phone3");
				
		String phone = phone1+phone2+phone3;//(String) request.getSession().getAttribute(PHONE_NUMBER);
		if (phone != null) {
			phone = phone.replaceAll("-", "");
		}
		accountUser.setPhone(phone);
		request.getSession().removeAttribute(PHONE_NUMBER);

		Calendar userStartDate = prepareDBDate((String) request.getSession().getAttribute(USER_START_DATE));
		if (userStartDate != null) {
			accountUser.setStartDate(userStartDate.getTime());
			request.getSession().removeAttribute(USER_START_DATE);
		}

		Calendar userEndDate = prepareDBDate((String) request.getSession().getAttribute(USER_END_DATE));
		if (userEndDate != null) {
			accountUser.setEndDate(userEndDate.getTime());
			request.getSession().removeAttribute(USER_END_DATE);
		}
		String userPwd=accountUser.getPassword();
		
		//MSHIX-735 - Checking the data when signup form is POST
		// HIX-36252 
		
		//Role signUpRole = roleService.findRoleByName(defUserRoleName);		
		if ((signUpRole == null)||(signUpRole.getPrivileged()==1 && !isValidActivationData(signUpRole.getName(), activationId)) ||
			!validationUtility.checkForPasswordComplexity(userPwd, 
					DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))) {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data tampered while signup POST::"+signUpRole.getLabel()));
			LOGGER.error("Data tampered while signup POST::"+signUpRole.getLabel());
			return "redirect:/";
		}
		
		AccountUser newAccountUser = null;
	    AccountUser emailObj = null;
	    
	    if(isScimEnabled) {
	    	emailObj = scimUserManager.findByEmail(accountUser.getEmail().toLowerCase());
	    }
	    
		if(houseHoldId != null && emailObj != null) {
			//Account transfer flow with user already existing on Idalink
			newAccountUser = userService.createLocalUser(accountUser, signUpRole.getName()); 
		}else {
			newAccountUser = userService.createUser(accountUser, signUpRole.getName());
		}
		 
		 if(newAccountUser != null) {
			 if(signUpRole.getName().equalsIgnoreCase("Individual")) {

				 Household household = null;
				 try {
					 houseHoldId = (String) request.getSession().getAttribute(ReferralConstants.HOUSEHOLD_ID);
						
					 if(houseHoldId != null) {
						household = householdRepository.findById(Integer.valueOf(houseHoldId));
					 }
						
					 if(household == null ) {
						if(accountUser.getEmail() != null) {
							household = householdRepository.findByEmailAddress(accountUser.getEmail().toLowerCase());
						}
					 }
						
				 }catch(Exception e) {
					 LOGGER.error("Error in fetching household based on email address from CMR_Household table, exception:",e);
					 return "error/accountSignupError";
				 }

				 if(household == null) { // create new household
					 household = new Household();
					 household.setEmail(newAccountUser.getEmail());

					 ConsumerDto consumer = new ConsumerDto();
					 consumer.setFirstName(newAccountUser.getFirstName());
					 consumer.setLastName(newAccountUser.getLastName());
					 consumer.setEmailAddress(newAccountUser.getEmail());

					 Object eligIdInSession = request.getSession().getAttribute(ScreenerUtil.LEAD_ID);
					 Long  eligLeadId = null;

					 if(eligIdInSession != null	&& StringUtils.isNumeric(eligIdInSession.toString())){
						 eligLeadId = Long.valueOf(eligIdInSession.toString());
					 }

					 if(eligLeadId == null) {
						 EligLead dummyEligLeadRecord =  persistEligLead(consumer, newAccountUser.getId());
						 household.setEligLead(dummyEligLeadRecord);
					 }else {
						 EligLead eligLead = screenerUtil.fetchEligLeadRecord(eligLeadId);
						 household.setEligLead(eligLead);
					 }

					 if(birthDate != null) {
						 Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
						 household.setBirthDate(dob);
					 }
				 }

				 household.setUser(newAccountUser);
				 household.setFirstName(newAccountUser.getFirstName());
				 household.setLastName(newAccountUser.getLastName());
				 household.setPhoneNumber(newAccountUser.getPhone());

				 if(bCheckSSN ) {
					 household.setSsn(ssn);
				 }

				 try {
					 household = consumerUtil.saveHouseholdRecord(household);
				 } catch (IOException | URISyntaxException e) {
					 LOGGER.error("Error in creating household record during individual signup", e);
				 }
			 }
		 }else {
			 LOGGER.error("Error in creating user during individual signup");
			 return "redirect:/";
		 }
		
		// redirectAttributes.addFlashAttribute("newAccountUser",
		// newAccountUser);

		/*
		 * START :: Code to auto login the newly created user /
		 * UsernamePasswordAuthenticationToken
		 * userNamePasswordAuthenticationToken = new
		 * UsernamePasswordAuthenticationToken(accountUser.getUserName(),
		 * accountUser.getPassword()); //
		 * userNamePasswordAuthenticationToken.setAuthenticated(true);
		 * userNamePasswordAuthenticationToken.setDetails(newAccountUser);
		 * SecurityContextHolder
		 * .getContext().setAuthentication(userNamePasswordAuthenticationToken);
		 *
		 * / * END :: Code to auto login the newly created user
		 */

		/**
		 * If activationId is not null, implies the flow is for activation link
		 * through email, set last visited url and username
		 */
		//String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
		if (StringUtils.isNumeric(activationId)) { // update userid
			//Modified as per HIX-21637 : Darshan Hardas : Adding the user information
			AccountActivation accountActivation = accountActivationService.findByActivationId(Integer
					.parseInt(activationId));
			ActivationJson jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());
			accountActivationService.updateUserInCreatedObject(newAccountUser, jsonObject.getCreatedObject()
					.getObjectId(), jsonObject);
			
			accountActivationService.updateLastVisitedUrlAndUsername(AccountActivationService.ACCOUNT_SIGNUP,
					newAccountUser.getUsername(), Integer.parseInt(activationId));
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User Information added successfully"));
		}

		LOGGER.info("accountsubmit : END");
		
		request.getSession().setAttribute("accountActivationFlow", TRUE);
		request.getSession().setAttribute("sUser", newAccountUser);
		//Added by pravin
		request.getSession().setAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING, TRUE);
		
		if (isSSOEnabled) {
			sessionTracker.storeHttpSessionInDB(request, newAccountUser);
		}
		Map<String, String> mapEventParam = new HashMap<>();
		/*mapEventParam.put("User Id", Integer.toString(newAccountUser.getId()));
		mapEventParam.put("User Role", signUpRole.getName());*/
	
		
		// as per the discussion with Abhai and Darshan; the event creation logic has been moved to 
		// post registeration handler. Respective module need to incorporate this in their PR handler.
		// HIX-74509
		//event type and category 
		/*
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode("APPEVENT_ACCOUNT","ACCOUNT_CREATED");
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(newAccountUser.getId());// Here we dont get entry for module user. So setting user id and role name
		eventInfoDto.setModuleName(signUpRole.getName());
		
		appEventService.record(eventInfoDto, mapEventParam);
		*/
		
		String autoLoginUrl = null;
		//String isSSO = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSO_ENABLED);
		if(isSSOEnabled){
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to sso user success as SCIM Enabled"));
			autoLoginUrl = "redirect:/account/user/ssousersuccess";
		} else {
			try {
				autoLoginUrl = userService.getAutoLoginUrl(newAccountUser)+"&j_password="+URLEncoder.encode(userPwd,"UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new GIRuntimeException("Exception occured while encoding URL.",e);
			}
		}
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User " +newAccountUser.getUsername()+" created successfully. Redirecting to loginUrl"));
		return autoLoginUrl;

	}
	
	private EligLead persistEligLead(ConsumerDto consumer, int userId ) throws InvalidUserException {
		EligLead emptyEligLeadRecord = new EligLead();
		emptyEligLeadRecord.setEmailAddress(consumer.getEmailAddress());
		emptyEligLeadRecord.setCreatedBy(userId);
		emptyEligLeadRecord.setName(consumer.getFullName());
		emptyEligLeadRecord.setStage(EligLead.STAGE.WELCOME);
		emptyEligLeadRecord = screenerUtil.createEligLeadRecord(emptyEligLeadRecord);
		if (emptyEligLeadRecord == null) {
		 	throw new RuntimeException("[100] Could not persist to database. Please contact admin.");
		}
		LOGGER.debug("Created & persisted empty EligLead record. EligLeadId="+SecurityUtil.sanitizeForLogging(""+emptyEligLeadRecord.getId()));
		return emptyEligLeadRecord;
	}
	
	/**
	 * HIX-47385 - Redirects to a success page when user is successfully created through SSO
	 * 
	 * @author Nikhil Talreja 
	 */
	@RequestMapping(value = "/account/user/ssousersuccess", method = RequestMethod.GET)
	public String ssoUserCreationSuccess(Model model, HttpServletRequest request) {
		return "account/user/ssousersuccess";
	}
	
	  private void validateSignUpUser(SignUpUser signUpUser, BindingResult result ){
		  boolean isValid = true;

		  //Invalid object if BindingResult has any default validation errors
		  if(result.hasErrors()){
			  isValid=false;
		  }

		  //Check the object custom validations 
		  /*
		   * Check if password and confirm passwords are same
		   */
		  
		  if(!signUpUser.getPassword().equals(signUpUser.getConfirmPassword())){
			  isValid=false;
		  }
		  
		  /*
		   * Check if email and confirm email are same
		   */
		  
		  if(!signUpUser.getEmail().equalsIgnoreCase(signUpUser.getConfirmEmail())){
			  isValid=false;
		  }
		  
		  /*
		   * Check security questions / answers
		   * Note: the number of security questions / answers are configurable 
		   */
		  int noOfSecQtns = 2; //default no of questions 2
		  String strNoOfSecQtns = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.NO_OF_SECURITY_QUESTIONS.getValue());
		  if(StringUtils.isNotBlank(strNoOfSecQtns)){
			  int tempNoOfSecQtns = Integer.parseInt(strNoOfSecQtns);
			  if(tempNoOfSecQtns>0 && tempNoOfSecQtns<4){ //The no of questions should be between 1 -3
				  noOfSecQtns = tempNoOfSecQtns;
				  }
			}
		 if(noOfSecQtns>=0){
			 SecurityQuestionsHelper securityQuestionsHelper = new SecurityQuestionsHelper();
			 for(int idx=0;idx<noOfSecQtns;idx++){
				  String currQstn = signUpUser.getSecurityQuestions()[idx];
				  String currAns = signUpUser.getSecurityAnswers()[idx];
				  if(StringUtils.isBlank(currQstn) || StringUtils.isBlank(currAns)){
					  isValid=false;
				  }
				  if(!securityQuestionsHelper.isValidQuestion(idx+1, currQstn)){
					  isValid=false;
				  }
				  if(!isValid){
					  break;
				  }
			  }
		 }
		  
		  
		  /*
		   * Check security terms flag got tampered
		   * Note: the flag HideTermaAndConditions is configurable 
		   */
		  
		  //Accept+Terms
		  boolean hideTermsCondition = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDETERMSANDCONDITIONS));
		  
		  if(!hideTermsCondition){
			  if(!StringUtils.equals(signUpUser.getTerms(), "Accept Terms") ){
				  isValid=false;
			  }
		  }
		  
		  if(!isValid){
			  Exception exception = new Exception(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
	 	 	  throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		  }
	    }

	/**
	 * HIX-36252 
	 * @author hardas_d
	 * @param signUpUserRoleName
	 * @param activationId
	 * @return true if activation data is validated.
	 * This will check if the activation  is not processed and also 
	 * the role name from the form is same while activation email is triggered.
	 */
	private boolean isValidActivationData(String signUpUserRoleName, String activationId) {
		boolean bStatus = false;
		
		if(StringUtils.isNumeric(activationId) && !StringUtils.isEmpty(signUpUserRoleName)) {
			AccountActivation activation = accountActivationService.findByActivationId(Integer.parseInt(activationId));
			bStatus = (activation!=null) ? ((signUpUserRoleName.equalsIgnoreCase(activation.getCreatedObjectType())) && (activation.getStatus() == STATUS.NOTPROCESSED)) : bStatus;
		}
		return bStatus;
	}
	
	/**
	 * Retrieves security questions
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to security questions
	 * @throws InvalidUserException
	 */
	@RequestMapping(value = "/account/securityquestions", method = RequestMethod.GET)
	public String getQuestions(Model model, HttpServletRequest request) throws InvalidUserException {

		LOGGER.info("get Security Questions : START");
		
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Security Questions");
		
		request.getSession().setAttribute(ISUSERFULLYAUTHORIZED, "false"); 
		
		LOGGER.info("getQuestions : END");
		return "account/securityquestions";
	}

	/**
	 * Saves security question/answers
	 *
	 * @param user
	 *            {@link AccountUser}
	 * @param result
	 *            {@link BindingResult}
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to post registration URL available in database
	 *         against each role
	 * @throws InvalidUserException
	 */
	@RequestMapping(value = "/account/securityquestions", method = RequestMethod.POST)
	public String submitQuestions(@ModelAttribute("AccountUser") AccountUser user, BindingResult result, Model model,
			HttpServletRequest request) throws InvalidUserException {
		LOGGER.info("submitQuestions : START");

		LOGGER.info("Security Questions Update");

		AccountUser accountUser = userService.getLoggedInUser();

		String defUserRoleName = userService.getDefaultRole(accountUser).getName();

		String postRegistrationUrl = userService.getDefaultRole(accountUser).getPostRegistrationUrl();// ghixUtils.getPostRegistrationUrl(defUserRoleName);

		if (user != null) {

			accountUser.setSecurityQuestion1(user.getSecurityQuestion1());
			accountUser.setSecurityAnswer1(user.getSecurityAnswer1());
			
			if(!isScimEnabled) {
				accountUser.setSecurityQuestion2(user.getSecurityQuestion2());
				accountUser.setSecurityAnswer2(user.getSecurityAnswer2());
				accountUser.setSecurityQuestion3(user.getSecurityQuestion3());
				accountUser.setSecurityAnswer3(user.getSecurityAnswer3());
			}
			
			AccountUser rtnUserObj = userService.updateUser(accountUser);

			/**
			 * If activationId is not null, implies the flow is for activation
			 * link through email, set last visited url
			 */
			String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
			if (StringUtils.isNumeric(activationId)) {
				accountActivationService.updateLastVisitedUrlAndUsername(
						AccountActivationService.ACCOUNT_SECURITYQUESTIONS, rtnUserObj.getUsername(),
						Integer.parseInt(activationId));
				LOGGER.info("submitQuestions : END");
				request.getSession().setAttribute(ISUSERFULLYAUTHORIZED, TRUE);
				return userService.getAutoLoginUrl(rtnUserObj);
			}
		}

		LOGGER.debug("Security Questions Update :: Sucess");
		LOGGER.debug("Security Questions Redirecting to (PostRegistrationUrl)  :: " + postRegistrationUrl);

		postRegistrationUrl = REDIRECT + postRegistrationUrl;
		LOGGER.info("submitQuestions : END");
		request.getSession().setAttribute(ISUSERFULLYAUTHORIZED, TRUE);
		return postRegistrationUrl;

	}
	/**
	 * Displays forgot password page
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to forgot password page
	 */
	@RequestMapping(value = "/account/user/forgotpassword", method = RequestMethod.GET)
	public String forgotPassword(Model model, HttpServletRequest request) {
		LOGGER.info("brokerSearchScreen : END");
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Forgot Password");
		boolean hideCaptcha = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDECAPTCHA));
		model.addAttribute("hideCaptcha", hideCaptcha);
		request.getSession().setAttribute("EMAIL_SENT".intern(),false);
		LOGGER.info("forgotPassword : END");
		return "account/user/forgotpassword";

	}

	/**
	 * Save new password information
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 * @throws IOException 
	 */
	@RequestMapping(value = "/account/user/forgotpassword", method = RequestMethod.POST)
	@GiAudit(transactionName = "Forgot Password", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String forgotPasswordSubmit(Model model, HttpServletRequest request,
			@RequestParam(value = "g-recaptcha-response", required = false) String gRecaptchaResponse) throws IOException {
		LOGGER.info("forgotPasswordSubmit : START");
        
        String userName = request.getParameter("j_username");
        GiAuditParameterUtil.add(new GiAuditParameter("userName", userName));
        String errorMSg = StringUtils.EMPTY;
        String idalinkUser = StringUtils.EMPTY;
        AccountUser userObj = null;
        String remoteAddress = request.getRemoteAddr();	
		boolean isValidCAPTCHA = true;
		isValidCAPTCHA = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDECAPTCHA));
		if(!isValidCAPTCHA) {
			try {
				SiteVerifyResponse siteVerifyResponse = reCaptchaV2.verify(gRecaptchaResponse);
				if ( siteVerifyResponse.isSuccess() ) {
					isValidCAPTCHA = true;
				} else {
					isValidCAPTCHA = false;
				}
			} catch (IOException e) {
				isValidCAPTCHA = true;//setting to true incase of captcha issue.
				LOGGER.error("Error happened while validating recaptcha",e);
			}
		}
		if(isValidCAPTCHA){
	        try {
	        	userObj = userService.findByUserName(userName.toLowerCase(), true);
	        	if(userObj == null){
	        		 errorMSg = "If an account exists, an email will be sent to continue the recovery process";
	        	}
	        } catch(RemoteValidationFailedException re){
	        	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not available in remote system"));
	            LOGGER.error("ERR: WHILE FETCHING USR:"+re.getMessage()+" Caused By:"+((re.getCause() == null)? " Not available":re.getCause().getMessage()));
	            errorMSg = "There is a problem with your account, please contact customer support";
	        }
	        catch (Exception e) {
	        	GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while fetching user"));
	            LOGGER.error("ERR: WHILE FETCHING USR:"+e.getMessage()+" Caused By:"+((e.getCause() == null)? " Not available":e.getCause().getMessage()));
	            errorMSg = "If an account exists, an email will be sent to continue the recovery process";
	        }
	        if (userObj != null) {
	               request.getSession().setAttribute(USER_OBJECT, userObj);
	               LOGGER.info("forgotPasswordSubmit : END");
	               if(userObj.getStatus() != null && userObj.getStatus().equals(AccountUser.user_status.Deactivate.toString())) {
	            	   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account is Inactive"));
	                     errorMSg = "Your account is inactive. Please contact Exchange Administrator";
	               }else {
	            	   	if(!userObj.isHasGhixProvisioned()) {
	                            idalinkUser = "true";
	                     } else {
	                            return REDIRECT_EMAILVERIFICATION_URI;
	                     }
	               }
	        } else {
	        	return REDIRECT_EMAILVERIFICATION_URI;
	        }
		}
		else{
		    model.addAttribute("captchaError", "Incorrect Security Code");
		    errorMSg = "Please try again";
		}
        model.addAttribute("idalinkUser", idalinkUser);
        model.addAttribute(ERROR_MESSAGE, errorMSg);
        LOGGER.info("forgotPasswordSubmit : END");
        return "account/user/forgotpassword";
	}

	/**
	 * As per HIX-48664	Change Security Question to be configurable:  At least one secQstn should be selected,
	 *        and the following isSecQuesNotChosen(..) had deprecated
	 * As per HIX-22837 : Checks if the user has selected the security questions.
	 * @author Darshan Hardas 
	 * @param userObj
	 * @return boolean value true if security questions are not selected. 
	 */
	@Deprecated 
	private boolean isSecQuesNotChosen(AccountUser userObj) {
		boolean isQuestionNotChosen = false;
		if(userObj == null) {
			LOGGER.error("Invalid user object");
			isQuestionNotChosen = true;
		} else {
			if(!isScimEnabled) {
				isQuestionNotChosen = StringUtils.isEmpty(userObj.getSecurityQuestion1()) || 
						StringUtils.isEmpty(userObj.getSecurityQuestion2());
			} else {
				isQuestionNotChosen = StringUtils.isEmpty(userObj.getSecurityQuestion1());
			}
		}
		
		return isQuestionNotChosen;
	}
	
	
	/**
	 * Retrieve security questions
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to security questions page
	 */
	@RequestMapping(value = "/account/user/questions", method = {RequestMethod.GET, RequestMethod.POST })
	@GiAudit(transactionName = "Security Question for Forgot Password", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String getUserSecurityQuestions(@ModelAttribute("accountUserDto") AccountUserDto accountUserDto,Model model, HttpServletRequest request) {
		LOGGER.info("getUserSecurityQuestions : START");

	//	model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: User Security Questions");
		AccountUser userObj = (AccountUser) request.getSession().getAttribute(USER_OBJECT);
		

		if(userObj==null){
			return "account/user/forgotpassword";
		}
		GiAuditParameterUtil.add(new GiAuditParameter("userId", userObj.getId()));
		
		int NoOfSecQstnAns=accountUserDto.getUserSecQstnAnsList().size();
		
		if(RequestMethod.GET.name().equals(request.getMethod())){
			// Accessing the uri with GET , as firstime
			//get securityAnswers.
			List<QuestionAnswer> dbUserQstnAnsList= AccountUserDto.getUserSecurityQuestionAnswers(userObj);
			accountUserDto.getUserSecQstnAnsList().clear();
			for(QuestionAnswer dbQuestionAnswer:dbUserQstnAnsList){
				int idx = dbQuestionAnswer.getIdx();
				QuestionAnswer currQuestionAnswer = new QuestionAnswer(idx,dbQuestionAnswer.getQuestion(),"");
				accountUserDto.getUserSecQstnAnsList().add(currQuestionAnswer);
			}
			if(!isScimEnabled) {
				int maxSqRetryCount = userService.getUsersMaxRetryCount(userObj);
				request.getSession().setAttribute(MAX_SQ_RETRY_COUNT, maxSqRetryCount);	
				request.getSession().setAttribute(CURRENT_SQ_RETRY_COUNT, userObj.getSecQueRetryCount());
			}
			
			return "account/user/questions";
		}
		List<QuestionAnswer> dbUserQstnAnsList= AccountUserDto.getUserSecurityQuestionAnswers(userObj);
		boolean hasQstnAnsMatch=accountUserDto.questionAnswersEqualWith(dbUserQstnAnsList);
		String errorMessage = null;
		if(!isScimEnabled) {
			int maxAttempts = (int)request.getSession().getAttribute(MAX_SQ_RETRY_COUNT);
			int currSqRetryCount = (int)request.getSession().getAttribute(CURRENT_SQ_RETRY_COUNT);
			//check securityAnswers provided by user with securityAnswers stored in system. 
			if((userObj.getConfirmed() == 1) && 
					(currSqRetryCount < maxAttempts) &&
					hasQstnAnsMatch){
				//updateSecQueRetryCount to 0 as user provided correct security answers.
				userService.updateSecQueRetryCount(userObj, 0);
				//Remove the session attributes that has used only to the scope of forgot password flow
				request.getSession().removeAttribute(MAX_SQ_RETRY_COUNT);
				request.getSession().removeAttribute(CURRENT_SQ_RETRY_COUNT);
						
				//return REDIRECT_EMAILVERIFICATION_URI;
				return "redirect:reset/" + request.getSession().getAttribute("token_str".intern());
			}else{
				errorMessage = StringUtils.EMPTY;
				
				
				request.getSession().setAttribute(CURRENT_SQ_RETRY_COUNT,currSqRetryCount+1) ;
				if(currSqRetryCount >= maxAttempts){
					//several unsuccessful attempts. make sure re-lock the account and send the email
					errorMessage = UNSUCCESSFUL_ATTEMPTS;
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User Account Locked due as current security count reached max attempts"));
					userService.updateConfirmedAndSecQueRetryCount(userObj, 0, maxAttempts);
					GiAuditParameterUtil.add(new GiAuditParameter("currentSecurityCount", request.getSession().getAttribute(CURRENT_SQ_RETRY_COUNT)),
							new GiAuditParameter("maxSecurityCount", maxAttempts));
					//LOGGER.info(" User '" + userObj.getUsername() + "' is locked. Retry Count " + request.getSession().getAttribute(CURRENT_SQ_RETRY_COUNT) + " reached max retry count " + maxAttempts);
					
				}else{
					// Supplied answers didn't match with our records...
					errorMessage = INCORRECT_ANSWERS;
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Incorrect Answer Supplied"));
					// update no. of retry for user
					userService.updateSecQueRetryCount(userObj, currSqRetryCount+1);
					//LOGGER.info(" User '" + userObj.getUsername() + "' security retry count increased by " + request.getSession().getAttribute(CURRENT_SQ_RETRY_COUNT));
				}
				
				model.addAttribute(ERROR_MESSAGE, errorMessage);
			}
		} else {
			if(hasQstnAnsMatch) {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to email verification link"));
				return "redirect:reset/" + request.getSession().getAttribute("token_str".intern());
			} else {
				errorMessage = StringUtils.EMPTY;
				errorMessage = INCORRECT_ANSWERS;
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Incorrect Answer"));
				model.addAttribute(ERROR_MESSAGE, errorMessage);
			}
		}
		
		
		LOGGER.info("getUserSecurityQuestions : END");
		return "account/user/questions";
	}
	
	
	/**
	 * Retrieve security questions
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to security questions page
	 */
	@RequestMapping(value = "/account/user/scim/questions", method = RequestMethod.GET)
	public String getScimUserSecurityQuestions(Model model, HttpServletRequest request) {
		LOGGER.info("getUserSecurityQuestions : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: User Security Questions");
		AccountUser userObj = (AccountUser) request.getSession().getAttribute(USER_OBJECT);

		if (userObj != null) {
			model.addAttribute(SECURITY_QUESTION1, userObj.getSecurityQuestion1());
			if(!isScimEnabled) {
				model.addAttribute(SECURITY_QUESTION2, userObj.getSecurityQuestion2());
			}
//			model.addAttribute(SECURITY_QUESTION3, userObj.getSecurityQuestion3());
		}
		LOGGER.info("getUserSecurityQuestions : END");
		return "account/user/questions";
	}

	/**
	 * Saves security question / answer information
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/user/scim/questions", method = RequestMethod.POST)
	@GiAudit(transactionName = "SCIM Security Question for Forgot Password", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String userScimSecurityQuestionsSubmit(Model model, HttpServletRequest request) {
		LOGGER.info("userSecurityQuestionsSubmit : START");
		// get userNameObj from session
		AccountUser userObj = (AccountUser) request.getSession().getAttribute(USER_OBJECT);
		GiAuditParameterUtil.add(new GiAuditParameter("userId", userObj.getId()));
		//In case of wso2 the session will contain the object fetched via SCIM. Use security answer from this object.
		String wso2SecurityAnswer = userObj.getSecurityAnswer1();
		// get updated user details - could be updated with security questions retry count
		userObj = userService.findByUserName(userObj.getUsername().toLowerCase());
		// validate answers
		String userAnswer1 = request.getParameter(SECURITY_ANSWER1);
		String userAnswer2 = request.getParameter(SECURITY_ANSWER2);
		//String userAnswer3 = request.getParameter(SECURITY_ANSWER3);

		String errorMSg = StringUtils.EMPTY;
		
		boolean isSecurityCheckCleared = false; 
		
		if(!isScimEnabled) {
			isSecurityCheckCleared = StringUtils.equalsIgnoreCase(userAnswer1, userObj.getSecurityAnswer1())
					&& StringUtils.equalsIgnoreCase(userAnswer2, userObj.getSecurityAnswer2());
		} else {
			isSecurityCheckCleared = StringUtils.equalsIgnoreCase(userAnswer1, wso2SecurityAnswer);
		}

		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("SCRTY CHECK: "+isSecurityCheckCleared+" USR ANS1: "+userAnswer1+" WSO2 ANSWER: "+wso2SecurityAnswer+" CONFIRMED: "+userObj.getConfirmed());
		}
		if (userObj != null && userObj.getConfirmed() == 1 && isSecurityCheckCleared) {
			// success... matching answers found
			model.addAttribute(ERROR_MESSAGE, errorMSg);
			userService.updateSecQueRetryCount(userObj, 0);
			LOGGER.info("userSecurityQuestionsSubmit : END");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to email verification link"));
			return REDIRECT_EMAILVERIFICATION_URI;

		} else {
			if ( userObj != null && isNoOfAttemptsReachedMaxCount(userObj))
			{
				errorMSg = "Your account has been locked due to several unsuccessful attempts. Please call the customer service center at " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE) + " to unlock your account.";
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account Locked due to several incorrect attempts"));
			}
			else
			{
				// Supplied answers didn't match with our records...
				errorMSg = "One or more of these answers do not match the answers in our system. Please try again!";
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMSg));
			}

			model.addAttribute(ERROR_MESSAGE, errorMSg);
			model.addAttribute(SECURITY_QUESTION1, userObj.getSecurityQuestion1());
			model.addAttribute(SECURITY_ANSWER1, userAnswer1);
			
			if(!isScimEnabled) {
				model.addAttribute(SECURITY_QUESTION2, userObj.getSecurityQuestion2());
				model.addAttribute(SECURITY_ANSWER2, userAnswer2);
			}
			
//			model.addAttribute(SECURITY_QUESTION3, userObj.getSecurityQuestion3());
//			model.addAttribute(SECURITY_ANSWER3, userAnswer3);
			LOGGER.info("userSecurityQuestionsSubmit : END");
			return "account/user/questions";
		}
	
	}

	
	/**
	 * 
	 * method to check no. of unsuccessfully security questions attempts based on user name and lock account if it reaches max retry count
	 * 
	 * @author Samir Gundawar
	 * @since 02/07/2014
	 *
	 * @param userDetails
	 */
	private boolean isNoOfAttemptsReachedMaxCount(AccountUser userDetails) 
	{	
		boolean flag = false;
		if (userDetails != null)
		{
			//if user is active/not locked then check for no. of retry
			if (userDetails.getConfirmed() == 1) {
				Set<Role> userRoles = userService.getAllRolesOfUser(userDetails);
				
				if (userRoles != null)
				{
					// increase no. of retry by 1
					int noOfRetry = userDetails.getSecQueRetryCount() + 1;
					int maxRetry = 0;
					
					//if User has multiple roles then set lower limit as the cut-off.
					for (Role userRole : userRoles)
					{
						if (maxRetry == 0)
						{
							maxRetry = userRole.getMaxRetryCount();
						}
						else if (maxRetry > userRole.getMaxRetryCount())
						{
							maxRetry = userRole.getMaxRetryCount();
						}
					}
					
					// set user as locked/inactive if it reaches max no. of retry
					if (noOfRetry >= maxRetry)
					{
						userService.updateConfirmedAndSecQueRetryCount(userDetails, 0, noOfRetry);
						flag = true;
						//HIX-29631 Sends Email on Account Lock
						try {
							sendAccountLockMail(userDetails);
						} catch (NotificationTypeNotFound e) {
							LOGGER.error(e.getMessage());
							LOGGER.error("Account Lock e-mail cannot be sent - ", e);
						}
						LOGGER.debug(" User '" + userDetails.getUsername() + "' is locked. Retry Count " + noOfRetry + " reached max retry count " + maxRetry);
					}
					else
					{
						// update no. of retry for user
						userService.updateSecQueRetryCount(userDetails, noOfRetry);
						LOGGER.debug(" User '" + userDetails.getUsername() + "' security retry count increased by " + noOfRetry);
					}
				}
				else
				{
					LOGGER.debug("Default role is not set for user : " + userDetails.getUsername());
				}
	        }
			else
			{
				flag = true;
				LOGGER.debug(" User '" + userDetails.getUsername() + "' is already locked.");
			}
		}
		return flag;
	}
	
	/**
	 * HIX-29631
	 * Sends email for account UnLock
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void sendAccountLockMail(AccountUser userObj) throws NotificationTypeNotFound {
		LOGGER.info("sendAccountLockMail : START");

		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", userObj);
		Map<String, String> tokens = accountLockNotificationEmail.getTokens(notificationContext);
		Map<String, String> emailData = accountLockNotificationEmail.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = accountLockNotificationEmail.generateEmail(accountLockNotificationEmail.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+accountLockNotificationEmail.getClass().getSimpleName());
		}
		LOGGER.info(noticeObj.getEmailBody());
		accountLockNotificationEmail.sendEmail(noticeObj);
		LOGGER.info("sendAccountLockMail : END");
	}

	/**
	 * Email verification
	 *
	 * @param model
	 *            {@link Model}
	 * @param request
	 *            {@link HttpServletRequest}
	 * @return URL for navigation to email verification page
	 */
	@RequestMapping(value = "/account/user/emailverification", method = RequestMethod.GET)
	@GiAudit(transactionName = "Email verification for Forgot Password", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_READ)
	public String emailVerification(Model model, HttpServletRequest request) {
		LOGGER.info("emailVerification : START");
		String errorMSg = StringUtils.EMPTY;
		Object emailSentObj = request.getSession().getAttribute("EMAIL_SENT".intern());
		boolean emailSent = false;
		if(emailSentObj != null){
			emailSent = (Boolean)emailSentObj;
		}
		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Email Verification");

		// get userNameObj from session
		AccountUser userObj = (AccountUser) request.getSession().getAttribute(USER_OBJECT);
		//HIX-113348 Added check for email already sent
		if(null != userObj && !emailSent) {
			model.addAttribute(EMAIL_ID, userObj.getEmail());
			GiAuditParameterUtil.add(new GiAuditParameter("target_user", userObj.getEmail()));
			String randomToken = GhixEncryptorUtil.generateUUIDHexString(THIRTY_TWO);
			userObj = userService.savePasswordRecoveryDetails(userObj.getEmail(), randomToken);
			if (userObj == null) {
				LOGGER.info("Failed to save the password recovery details");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Sending Password Recovery Email"));
				errorMSg = "Password recovery email can not be sent, Please contact the administrator";
			} else {
				// initiate email sending process...
				LOGGER.info("Sending password recovery e-mail...");
				try {
					sendPasswordRecoveryMail(userObj);
					request.getSession().setAttribute("EMAIL_SENT".intern(),true);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Password Recovery Email Sent"));
				} catch (Exception e) {
					LOGGER.error(e.getMessage());
					LOGGER.error("Password recovery e-mail cannot be sent - ", e);
					errorMSg = "Password recovery e-mail cannot be sent. Please contact the administrator.";
				}
			}
		} else {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Sending Password Recovery Email"));
		}
	
		model.addAttribute(ERROR_MESSAGE, errorMSg);
		LOGGER.info("emailVerification : END");
		return "account/user/emailverification";
		
	}

	/**
	 * Sends email for password recovery
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void sendPasswordRecoveryMail(AccountUser userObj) throws NotificationTypeNotFound {
		LOGGER.trace("sendPasswordRecoveryMail : START");

		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", userObj);
		Map<String, String> tokens = passwordRecoveryEmail.getTokens(notificationContext);
		Map<String, String> emailData = passwordRecoveryEmail.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = passwordRecoveryEmail.generateEmail(PasswordRecoveryEmail.class.getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+passwordRecoveryEmail.getClass().getSimpleName());
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(noticeObj.getEmailBody());
		}
		passwordRecoveryEmail.sendEmail(noticeObj);
		
		LOGGER.trace("sendPasswordRecoveryMail : END");
	}

	
	@RequestMapping(value = "/account/user/reset_pwd/**", method = RequestMethod.GET)
	@GiAudit(transactionName = "User Activated Reset Password Email Link", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String resetPasswordSelf(HttpServletRequest request, Model model) {
		LOGGER.trace("resetPassword : START");

		//model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Reset Password");

		String token = StringUtils.substringAfter(request.getRequestURL().toString(), "/account/user/reset_pwd/");
		// validate token with db & check for expiration
		AccountUser userObj = userService.findByPasswordRecoveryToken(token);
		Date currentDate = new TSDate();
		String errorMSg = StringUtils.EMPTY;
		Date recoveryToken = (userObj != null) ? userObj.getPasswordRecoveryTokenExpiration() : null;
		if (userObj == null || recoveryToken == null || currentDate.after(recoveryToken)) {
			errorMSg = "You have tried to use a one-time login link that has expired. Please request a new one using the below link.";
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "The one time login link has expired"));
		} else {
			// set id in session
			GiAuditParameterUtil.add(new GiAuditParameter("Token", token), new GiAuditParameter("target_user", userObj.getUserName()));
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Passord Expiration date - "
					+ (userObj.getPasswordRecoveryTokenExpiration()));
			}
			int userEncId = userObj.getId();
			
		    request.getSession().setAttribute(USER_ID,userEncId);
		    request.getSession().setAttribute(USER_OBJECT, userObj);
            request.getSession().setAttribute("token_str".intern(), token);
            return "redirect:/account/user/questions";
			
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Reset password model message:"+errorMSg);
		}

		model.addAttribute(ERROR_MESSAGE, errorMSg);
		LOGGER.trace("resetPassword : END");
		return "account/user/reset";
	}

	
	/**
	 * Reset password
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/user/reset/**", method = RequestMethod.GET)
	@GiAudit(transactionName = "User Activated Reset Password Email Link", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
	public String resetPassword(HttpServletRequest request, Model model) {
		LOGGER.trace("resetPassword : START");

		//model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Reset Password");

		String token = StringUtils.substringAfter(request.getRequestURL().toString(), "/account/user/reset/");
		// validate token with db & check for expiration
		AccountUser userObj = userService.findByPasswordRecoveryToken(token);
		Date currentDate = new TSDate();
		String errorMSg = StringUtils.EMPTY;
		Date recoveryToken = (userObj != null) ? userObj.getPasswordRecoveryTokenExpiration() : null;
		if (userObj == null || recoveryToken == null || currentDate.after(recoveryToken)) {
			errorMSg = "You have tried to use a one-time login link that has expired. Please request a new one using the below link.";
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "The one time login link has expired"));
		} else {
			// set id in session
			GiAuditParameterUtil.add(new GiAuditParameter("Token", token), new GiAuditParameter("target_user", userObj.getUserName()));
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Passord Expiration date - "
					+ (userObj.getPasswordRecoveryTokenExpiration()));
			}
			int userEncId = userObj.getId();
			
			model.addAttribute(USER_ID,userEncId );
			
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Reset password model message:"+errorMSg);
		}

		model.addAttribute(ERROR_MESSAGE, errorMSg);
		LOGGER.trace("resetPassword : END");
		return "account/user/reset";
	}

	/**
	 * Redirect to forgot password screen
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/user/submitReset", method = RequestMethod.POST)
	public String invalidPasswordToken(HttpServletRequest request, Model model) {
		LOGGER.info("invalidPasswordToken : START");
		LOGGER.info("invalidPasswordToken : END");
		return "redirect:/account/user/forgotpassword";
	}

	/**
	 * Check if user already exists
	 *
	 * @param userName
	 *            String
	 * @return String 'EXISTS' if user already exists, else return String 'NEW'
	 */
	@RequestMapping(value = "/account/signup/chkUserName")
	@ResponseBody
	public String chkUserName(@RequestParam String userName) {
		LOGGER.info("chkUserName : START");

		AccountUser userNameObj = null;
		
		try {
			if(isScimEnabled) {
				userNameObj = scimUserManager.findByEmail(userName.toLowerCase());
			} else {
				userNameObj = userService.findByUserName(userName.toLowerCase());
			}
		} catch (Exception e) {
			userNameObj = null;
		}
		
		if (userNameObj != null) {
			LOGGER.info("chkUserName : END");
			return "EXIST";
		}
		LOGGER.info("chkUserName : END");
		return "NEW";
	}

	/**
	 * Checks if email exists
	 *
	 * @param email
	 *            email address
	 * @return boolean value 'true' if email not exists, else return 'false'
	 */
	@RequestMapping(value = "/account/signup/checkEmail")
	@ResponseBody
	public Boolean checkEmail(HttpServletRequest request, @RequestParam String email) {
		LOGGER.info("checkEmail : START");

		AccountUser emailObj = null;
		String houseHoldId = (String) request.getSession().getAttribute(ReferralConstants.HOUSEHOLD_ID);
		try {
			if(isScimEnabled && houseHoldId == null) {
				emailObj = scimUserManager.findByEmail(email.toLowerCase());
			} else {
				emailObj = userService.findByUserName(email.toLowerCase());
			}
		} catch (Exception e) {
			emailObj = null;
		}
		
		if (emailObj != null) {
			LOGGER.info("checkEmail : END");
			return false;
		}
		LOGGER.info("checkEmail : END");
		return true;
	}
	
	
	/* Account Activation code - Starts */
	/**
	 * Active account
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param redirectAttributes
	 *            {@link RedirectAttributes}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/user/activation/**", method = RequestMethod.GET)
	@GiAudit(transactionName = "Account Activation", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_READ)
	public String accountActivation(HttpServletRequest request, RedirectAttributes redirectAttributes, Model model) {
		LOGGER.info("accountActivation : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Activate Account");

		String token = StringUtils.substringAfter(request.getRequestURL().toString(), "/account/user/activation/");
		if (token != null && !token.isEmpty()) {
			AccountUser user;
			try {
				user = this.userService.getLoggedInUser();
				if (user != null) {
					// We are not expecting this link to be used by a logged in
					// user, show the error message to do a logout and try again.
					LOGGER.info("Logged in user trying to access account activation hence showing the page again with error message.");
					model.addAttribute(ERROR_MESSAGE, "You are currently loggedin, Please logout from this session and try again");
					model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
					model.addAttribute(STATEEXCHANGETYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE));
					
					LOGGER.info("accountActivation : END");
					return  "account/user/activation";
				}
			} catch (InvalidUserException e) {
				LOGGER.error("Error finding the logged in user, allowing the flow to continue", e);
				request.getSession().invalidate(); // Invalidate the session
													// anyways and let the
													// request processing
													// continue
			}
		}

		// validate token with db & check for expiration
		AccountActivation accountActivation = accountActivationService.findByActivationToken(token);
		Date currentDate = new TSDate();
		String errorMSg = StringUtils.EMPTY;
		String userName = "";
		//LOGGER.debug("token - " + token);
		LOGGER.info("current date - " + currentDate);
		
		GiAuditParameterUtil.add(new GiAuditParameter("Token", token));
		if (accountActivation == null || currentDate.after(accountActivation.getActivationTokenExpirationDate())
				|| accountActivation.getStatus().equals(STATUS.PROCESSED) || !StringUtils.isEmpty(accountActivation.getUsername())) {
			
			//errorMSg = "You have tried to use a one-time account activation link that has expired. Please contact your Employer.";//HIX-27593
			model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			model.addAttribute(STATEEXCHANGETYPE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE));//HIX-46621
			if (accountActivation != null && ActivationJson.OBJECTTYPE.INDIVIDUAL_REFERRAL.name().equals(accountActivation.getCreatedObjectType()) ) {
				if(STATUS.PROCESSED.equals(accountActivation.getStatus())){
					errorMSg = REFERRAL_ACTIVATION_ERROR;//HIX-52395. navigate to error page when link is expired or account created by csr.
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,errorMSg));
				} else if(currentDate.after(accountActivation.getActivationTokenExpirationDate())) {
					errorMSg = REFERRAL_ACTIVATION_EXPIRED;//
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,errorMSg));
				}
			} else {
				errorMSg = ACTIVATION_ERROR;//HIX-27593
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,errorMSg));
			}
			LOGGER.info(errorMSg);
		} else {
			ActivationJson jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());
			
			String roleNameInCapital = WordUtils.capitalize(jsonObject.getCreatedObject().getRoleName());
			
			//HIX-52234 - Re-direct to splash page if consumer signup is disabled
			String enableConsumerSignup = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ENABLE_CONSUMER_SIGNUP);
			GiAuditParameterUtil.add(new GiAuditParameter("Role Name", roleNameInCapital),new GiAuditParameter("Enable Consumer Signup flag", enableConsumerSignup));
			if(StringUtils.equalsIgnoreCase("N", enableConsumerSignup) && 
					(StringUtils.equalsIgnoreCase(roleNameInCapital, RoleService.INDIVIDUAL_ROLE) ||
							StringUtils.equalsIgnoreCase(roleNameInCapital, "INDIVIDUAL_REFERRAL"))){
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,"Redirecting to signupsplashpage"));
				return "account/signupsplashpage";
			}
			
			request.getSession().setAttribute(ACCOUNT_ACTIVATION_FLOW, TRUE);
			request.getSession().setAttribute(ACTIVATION_ID, String.valueOf(accountActivation.getId()));

			setAccountActivationModel(model, accountActivation, jsonObject);
			model.addAttribute(ERROR_MESSAGE, errorMSg);
			// changes done for Jira Id: HIX-13729
			request.getSession().setAttribute(FIRST_NAME, model.asMap().get(FIRST_NAME));
			request.getSession().setAttribute(LAST_NAME, model.asMap().get(LAST_NAME));
			request.getSession().setAttribute(EMAIL_ID, model.asMap().get(EMAIL_ID));
			request.getSession().setAttribute(PHONE_NUMBER, model.asMap().get(PHONE_NUMBER));
			request.getSession().setAttribute(USER_START_DATE, model.asMap().get(USER_START_DATE));
			request.getSession().setAttribute(USER_END_DATE, model.asMap().get(USER_END_DATE));

			String roleName =ghixJasyptEncrytorUtil.encryptStringByJasypt(roleNameInCapital);
			/* HIX-38644 - Changes for State Referral Account Starts*/
			boolean blnFromStateReferral = false;
			if(ghixJasyptEncrytorUtil.decryptStringByJasypt(roleName).equals("INDIVIDUAL_REFERRAL"))
			{
				blnFromStateReferral = true;
				request.getSession().setAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL, "Y");
				request.getSession().setAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID, String.valueOf(accountActivation.getCreatedObjectId()));
				roleName = ghixJasyptEncrytorUtil.encryptStringByJasypt("INDIVIDUAL");
			}
			/* HIX-38644 - Changes for State Referral Account Ends*/
			userName = accountActivation.getUsername() == null ? StringUtils.EMPTY : accountActivation.getUsername();

			// check for url, if not null, then re-direct to that page
			String landingPage = accountActivationService.getLandingPage(accountActivation.getId());
			//LOGGER.info("accountActivation - landing page - " + landingPage);
			GiAuditParameterUtil.add(new GiAuditParameter("Account Activation landing page", landingPage));
			/* HIX-38644 - Changes for State Referral Account Starts*/
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			
			if(blnFromStateReferral)
			{
				boolean goToDynamicSecurityQuestions =true;
				if("NV".equals(stateCode))
				{
					Integer ssapApplicationId = accountActivation.getCreatedObjectId();
					SsapApplication application = ssapApplicationRepository.findOne(Long.valueOf(ssapApplicationId));
					if(application != null && ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equals(application.getApplicationStatus()) && application.getCmrHouseoldId() != null)
					{
						request.getSession().removeAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL);
						application.setApplicationStatus(ApplicationStatus.SIGNED.getApplicationStatusCode());
						ssapApplicationRepository.save(application);
						goToDynamicSecurityQuestions = false;
					}
				}
				if(goToDynamicSecurityQuestions)
				{
				/*if (StringUtils.isEmpty(landingPage)){
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,"Redirecting to"+REDIRECT_TO_SIGNUP + roleName));
					return REDIRECT_TO_SIGNUP + roleName;
				}
				else{
					return REDIRECT_TO_LOGIN;
				}*/
				final String applicantGuid = referralActivationService.getSsapApplicantGuid(String.valueOf(accountActivation.getCreatedObjectId()));
				return "redirect:/referral/dynamicsecurityquestions/"+ghixJasyptEncrytorUtil.encryptStringByJasypt(applicantGuid);
			}
			}
			/* HIX-38644 - Changes for State Referral Account Ends*/
			/*
			 * Modified the logic for:  HIX-17718
			 * Required to deprecate @RequestMapping(value="/account/user/activation",method = RequestMethod.POST)
			 * -Venkata Tadepalli 09/20/2013
			 */
			if (StringUtils.isEmpty(landingPage)){
				LOGGER.info("accountActivation : END");
				//return "account/user/activation";
				// Modified as per HIX-19331 : Darshan Hardas : To redirect it to the first page i.e. verification code.
				//return "redirect:/account/signup/" + roleName;
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,"Redirecting to phone verification page"));
				return "redirect:/account/phoneverification/sendcode"; 
			}
			
			//Modified for HIX-21680 : Darshan Hardas: redirect it to signup page 
			LOGGER.debug(ACCOUNT_ACTIVATION_SUBMIT_END);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE,"Redirecting to"+REDIRECT_TO_SIGNUP + roleName));
			return REDIRECT_TO_SIGNUP + roleName;
			// populate user object do login... and redirect to spring security autologin url...
//			AccountUser userNameObj = userService.findByUserName(userName.toLowerCase());
//			return userService.getAutoLoginUrl(userNameObj);
		}
		model.addAttribute(ERROR_MESSAGE, errorMSg);
		LOGGER.info("accountActivation : END");
		return  "account/user/activation";
	}

	/**
	 * Saves account activation information and redirect to login page
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	 	/*
	 * Modified the logic for:  HIX-17718
	 * Required to deprecate @RequestMapping(value="/account/user/activation",method = RequestMethod.POST)
	 * -Venkata Tadepalli 09/20/2013
	 */
	@RequestMapping(value = "/account/user/activation", method = RequestMethod.POST)
	@GiAudit(transactionName = "Account Activation", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	public String accountActivationSubmit(HttpServletRequest request, Model model) {
		LOGGER.info("accountActivationSubmit : START");

		String accountFlag = request.getParameter("account");
		String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
		GiAuditParameterUtil.add(new GiAuditParameter("Account Flag", accountFlag),new GiAuditParameter("Activation Id", activationId));

		if (StringUtils.isEmpty(activationId)) {
			/*
			 * this is handle explicitly as passcode sending and receiving may
			 * expire the session...
			 */
			model.addAttribute(ERROR_MESSAGE, "Session expired... Please restart activation process...");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Session expired... Please restart activation process..."));
			LOGGER.info(ACCOUNT_ACTIVATION_SUBMIT_END);
			return null;
		}

		if (!StringUtils.isEmpty(accountFlag) && accountFlag.equals(YES)) {
			// reset activation flow
			request.getSession().setAttribute(ACTIVATION_ID, null);
			request.getSession().setAttribute(ACCOUNT_ACTIVATION_FLOW, FALSE);
			// changes done for Jira Id: HIX-13729
			request.getSession().setAttribute(FIRST_NAME, null);
			request.getSession().setAttribute(LAST_NAME, null);
			request.getSession().setAttribute(EMAIL_ID, null);
			// redirect to login page
			LOGGER.info(ACCOUNT_ACTIVATION_SUBMIT_END);
			return "redirect:login";
		} else if (!StringUtils.isEmpty(accountFlag) && accountFlag.equals(NO)) {
			String createdTypeInString = request.getParameter(CREATED_TYPE);
			String roleName =ghixJasyptEncrytorUtil.encryptStringByJasypt(createdTypeInString);
			request.getSession().setAttribute(ACCOUNT_ACTIVATION_FLOW, TRUE);
			request.getSession().setAttribute(ACTIVATION_ID, activationId);
			// changes done for Jira Id: HIX-13729
			request.getSession().setAttribute(
					FIRST_NAME,
					request.getSession().getAttribute(FIRST_NAME) == null ? StringUtils.EMPTY : request.getSession()
							.getAttribute(FIRST_NAME));
			request.getSession().setAttribute(
					LAST_NAME,
					request.getSession().getAttribute(LAST_NAME) == null ? StringUtils.EMPTY : request.getSession()
							.getAttribute(LAST_NAME));
			request.getSession().setAttribute(
					EMAIL_ID,
					request.getSession().getAttribute(EMAIL_ID) == null ? StringUtils.EMPTY : request.getSession()
							.getAttribute(EMAIL_ID));
			LOGGER.info(ACCOUNT_ACTIVATION_SUBMIT_END);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Redirecting to signup page"));
			return REDIRECT_TO_SIGNUP + roleName;
		}

		// populate user object do login... and redirect to spring security
		// autologin url...
		String username = request.getParameter(USERNAME2);
		AccountUser userNameObj = userService.findByUserName(username.toLowerCase());
		LOGGER.info(ACCOUNT_ACTIVATION_SUBMIT_END);
		return userService.getAutoLoginUrl(userNameObj);
	}

	/**
	 * Verify phone number
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param redirectAttributes
	 *            {@link RedirectAttributes}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/phoneverification/sendcode", method = RequestMethod.GET)
	@GiAudit(transactionName = "Phone Verification", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_READ)
	public String phoneVerificationSendCode(HttpServletRequest request, RedirectAttributes redirectAttributes,
			Model model) {

		LOGGER.info("phoneVerificationSendCode : START");

		model.addAttribute(PAGE_TITLE, "Getinsured Health Exchange: Send Code");

		String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);
		if (activationId == null) {
			/*
			 * this is handle explicitly as passcode sending and receiving may
			 * expire the session...
			 */
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Session expired... Please restart activation process..."));
			model.addAttribute(ERROR_MESSAGE, "Session expired... Please restart activation process...");
			LOGGER.info("phoneVerificationSendCode : END");
			return PHONEVERIFICATION_SENDCODE_URL;
		}

		AccountActivation accountActivation = accountActivationService.findByActivationId(Integer
				.parseInt(activationId));
		ActivationJson jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());

		setAccountActivationModel(model, accountActivation, jsonObject);
		model.addAttribute(ERROR_MESSAGE, StringUtils.EMPTY);
		model.addAttribute(PHONE_NUMBER_LIST, jsonObject.getCreatedObject().getPhoneNumbers());
		if (!StringUtils.isEmpty(accountActivation.getActivationCode())) {
			model.addAttribute(CODE_SENT, TRUE); // this flag refreshes JSP with
													// Verify fields
		}
		LOGGER.info("phoneVerificationSendCode : END");
		return PHONEVERIFICATION_SENDCODE_URL;
	}

	/**
	 * Setting account activation model object
	 *
	 * @param model
	 *            {@link Model}
	 * @param accountActivationObj
	 *            {@link AccountActivation}
	 * @param jsonObject
	 *            {@link ActivationJson}
	 */
	private void setAccountActivationModel(Model model, AccountActivation accountActivationObj,
			ActivationJson jsonObject) {
		LOGGER.info("setAccountActivationModel : START");

		model.addAttribute(ACTIVATION_ID, accountActivationObj.getId());
		model.addAttribute(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		model.addAttribute(CREATOR_TYPE,
				WordUtils.capitalize(jsonObject.getCreatorObject().getRoleName().toLowerCase()));
		model.addAttribute(CREATED_TYPE, WordUtils.capitalize(jsonObject.getCreatedObject().getRoleName()));
		model.addAttribute(EMAIL_ID, jsonObject.getCreatedObject().getEmailId());
		model.addAttribute(CREATOR_NAME, jsonObject.getCreatorObject().getFullName());
		model.addAttribute(USERNAME2, accountActivationObj.getUsername() == null ? StringUtils.EMPTY
				: accountActivationObj.getUsername());
		model.addAttribute("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));//HIX-56196
		model.addAttribute(FIRST_NAME,
				WordUtils.capitalize(jsonObject.getCreatedObject().getFirstName()) == null ? StringUtils.EMPTY
						: WordUtils.capitalize(jsonObject.getCreatedObject().getFirstName()));
		model.addAttribute(LAST_NAME,
				WordUtils.capitalize(jsonObject.getCreatedObject().getLastName()) == null ? StringUtils.EMPTY
						: WordUtils.capitalize(jsonObject.getCreatedObject().getLastName()));
		if (null != jsonObject.getCreatedObject().getPhoneNumbers() && jsonObject.getCreatedObject().getPhoneNumbers().size() !=0) {
			model.addAttribute(PHONE_NUMBER, jsonObject.getCreatedObject().getPhoneNumbers().get(0));
		}
		if (null != jsonObject.getCreatedObject().getCustomeFields()) {
			model.addAttribute(USER_START_DATE,
					jsonObject.getCreatedObject().getCustomeFields().get("userStartDate") == null ? StringUtils.EMPTY
							: jsonObject.getCreatedObject().getCustomeFields().get("userStartDate"));
			model.addAttribute(USER_END_DATE,
					jsonObject.getCreatedObject().getCustomeFields().get("userEndDate") == null ? StringUtils.EMPTY
							: jsonObject.getCreatedObject().getCustomeFields().get("userEndDate"));
		}
		LOGGER.info("setAccountActivationModel : END");
	}

	/**
	 * Verify account activation code
	 *
	 * @param request
	 *            {@link HttpServletRequest}
	 * @param model
	 *            {@link Model}
	 * @return URL for navigation
	 */
	@RequestMapping(value = "/account/phoneverification/sendcode", method = RequestMethod.POST)
	@GiAudit(transactionName = "Phone Verification", eventType = EventTypeEnum.ACCOUNT_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	public String phoneVerificationSendCodeSubmit(HttpServletRequest request, Model model) {
		LOGGER.info("phoneVerificationSendCodeSubmit : START");

		String toPhoneNumber = request.getParameter(PHONE_NUMBER2);
		String verificationCode = request.getParameter(VERIFICATION_CODE);
		String codeType = request.getParameter(CODE_TYPE);
		//LOGGER.info("Code Type" + codeType);

		String activationId = (String) request.getSession().getAttribute(ACTIVATION_ID);

		if (activationId == null) {
			/*
			 * this is handle explicitly as passcode sending and receiving may
			 * expire the session...
			 */
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Session expired.. Please restart activation process."));
			model.addAttribute(ERROR_MESSAGE, "Session expired.. Please restart activation process.");
			model.addAttribute(CODE_TYPE, codeType);
			LOGGER.info("phoneVerificationSendCodeSubmit : END");
			return PHONEVERIFICATION_SENDCODE_URL;
		}

		AccountActivation accountActivation = accountActivationService.findByActivationId(Integer
				.parseInt(activationId));
		ActivationJson jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());

		String errorMSg = StringUtils.EMPTY;
		GiAuditParameterUtil.add(new GiAuditParameter("verificationCode", verificationCode), new GiAuditParameter("codeType", codeType));
		if (StringUtils.isEmpty(verificationCode)) {
			// send new sms/call
			String msg = accountActivationService.sendCode(toPhoneNumber, accountActivation.getId(), codeType);
			if (!StringUtils.equals(msg, "SUCCESS")) {
				errorMSg = msg;
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Sms not send due to errors"));
			}
		} else {
			// verify entered verificationCode
			String msg = accountActivationService.verifyCode(verificationCode, accountActivation.getId());
			if (!StringUtils.equals(msg, "SUCCESS")) {
				errorMSg = msg;
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Verification code entered does not match"));
			} else {
				// everything went fine with activation process, now redirect to
				// next valid page

				//Modified as per HIX-19331 & HIX-21637 : Darshan Hardas : Redirect user to 2 page i.e basic info page
				//AccountUser userNameObj = userService.findByUserName(accountActivation.getUsername());
//				accountActivationService.updateUserInCreatedObject(userNameObj, jsonObject.getCreatedObject()
//						.getObjectId(), jsonObject);
				//return userService.getAutoLoginUrl(userNameObj);
				LOGGER.info("phoneVerificationSendCodeSubmit : END");
				accountActivationService.updateLastVisitedUrlAndUsername(null,
						accountActivation.getUsername(),
						accountActivation.getId());
				
				String roleNameInCapital = WordUtils.capitalize(jsonObject.getCreatedObject().getRoleName());
				String roleName =ghixJasyptEncrytorUtil.encryptStringByJasypt(roleNameInCapital);
				
				return REDIRECT_TO_SIGNUP + roleName;
			}
		}

		// refresh objects...
		accountActivation = accountActivationService.findByActivationId(Integer.parseInt(activationId));
		jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());

		setAccountActivationModel(model, accountActivation, jsonObject);
		model.addAttribute(ERROR_MESSAGE, errorMSg);
		request.setAttribute(CODE_TYPE, codeType);
		model.addAttribute(PHONE_NUMBER_LIST, jsonObject.getCreatedObject().getPhoneNumbers());
		model.addAttribute(CODE_TYPE, codeType);
		if (!StringUtils.isEmpty(accountActivation.getActivationCode())) {
			model.addAttribute(CODE_SENT, TRUE); // this flag refreshes JSP with
													// Verify fields
			model.addAttribute(CODE_TYPE, codeType);
		}
		LOGGER.info("phoneVerificationSendCodeSubmit : END");
		return PHONEVERIFICATION_SENDCODE_URL;
	}

	/* Account Activation code - Ends */

	/**
	 * Prepare db date
	 *
	 * @param parameter
	 *            String
	 * @return {@link Calendar}
	 */
	private Calendar prepareDBDate(String parameter) {

		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");

		if (StringUtils.isEmpty(parameter)) {
			return calendarDate;
		}

		try {
			effectiveEndDate = formatter.parse(parameter);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			return calendarDate;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
		calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
		calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));

		return calendarDate;
	}
	
	/**
     * Checks if valid password
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     */
    @RequestMapping(value = "/account/signup/checkUserPassword")
    @ResponseBody
    public String checkNewUserPassword(@RequestParam (required=false) String password) {
           LOGGER.info(CHECK_USER_PASSWORD_START);
           String errorMsg = "";
           if(password == null){
        	   errorMsg= "Password is required, none provided";
           }else{
	           if(!validationUtility.checkForPasswordComplexity(password, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))){
	        	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexity", null, LocaleContextHolder.getLocale()));
	           }
           }
           return errorMsg;
    }

    /**
     * Checks if SSN and BirthDate already exist
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     */
    @RequestMapping(value = "/account/signup/checkSSNAndBirthDate")
    @ResponseBody
    public String checkSSNAndBirthDate(@RequestParam String ssn, @RequestParam String dob) {
           String errorMsg = "";
           if(ssn == null || ssn.isEmpty() || dob == null || dob.isEmpty()){
        	   errorMsg= "SSN and birth date are required, none provided";
           }else{
        	   Date birthDate = DateUtil.StringToDate(dob, DATE_FORMAT);
			  			   
			   List<Household> households = householdRepository.findHouseholdsBySSN(ssn);
				if(null != households && households.size()>0) {
//					household = householdRepository.findBySSNAndBirthDate(ssn, birthDate);
//					
//					if(household != null) {
//						errorMsg =  "Household already exists for the provided SSN and birth date";
//					}else {
//						errorMsg =  "Household already exists for the provided SSN";
//					}
					
					errorMsg =  "Household exists";
				}	
        	    
	       }
           return errorMsg;
    }

  /**
   * Checks if {@link Household} exists with given email.
   * Primarily will be used by the Automation test (Cypress.io).
   *
   * @param ssn SSN number (Jasypt Encypted).
   * @return JSON response where {@code "exists": true} if found, {@code "exists": false} if not.
   */
  @RequestMapping(value = "/account/signup/ssn/check")
  public ResponseEntity<Map<String, Object>> checkSsn(@RequestParam String ssn) {
    final GhixJasyptEncrytorUtil encryptUtil = new GhixJasyptEncrytorUtil();
    final Map<String, Object> response = new HashMap<>(1);

    String decryptedSsn = null;
    Household household = null;

    try {
      decryptedSsn = encryptUtil.decryptStringByJasypt(ssn);
      household = householdRepository.findBySSN(decryptedSsn);
    } catch(GIRuntimeException e) {
      response.put("error", "invalid ssn");
    }

    if (household != null) {
      response.put("exists", true);
      return ResponseEntity.ok(response);
    }

    response.put("exists", false);
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
  }

  /**
   * Checks if {@link Household} exists with given email.
   * Primarily will be used by the Automation test (Cypress.io).
   *
   * @param email Email address (Jasypt Encrypted)
   * @return JSON response where {@code "exists": true} if found, {@code "exists": false} if not.
   */
  @RequestMapping(value = "/account/signup/email/check")
  public ResponseEntity<Map<String, Object>> checkEmail(@RequestParam String email) {
    final GhixJasyptEncrytorUtil encryptUtil = new GhixJasyptEncrytorUtil();
    final Map<String, Object> response = new HashMap<>(1);

    String decryptedEmail = null;
    Household household = null;

    try {
      decryptedEmail = encryptUtil.decryptStringByJasypt(email);
      household = householdRepository.findByEmailAddress(decryptedEmail);
    } catch(GIRuntimeException e) {
      response.put("error", "invalid email");
    }

    if (household != null) {
      response.put("exists", true);
      return ResponseEntity.ok(response);
    }

    response.put("exists", false);
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
  }

  /**
   * Encrypts given string with Jasypt encyption.
   * Primarily will be used by the Automation test (Cypress.io)
   *
   * @param string string to encrypt.
   * @return encrypted string.
   */
  @RequestMapping(value = "/account/signup/encrypt")
  public ResponseEntity<Map<String, String>> encryptString(@RequestParam String string) {
    final GhixJasyptEncrytorUtil encryptUtil = new GhixJasyptEncrytorUtil();
    final String encrypted = encryptUtil.encryptStringByJasypt(string);
    final Map<String, String> response = new HashMap<>(1);
    response.put("value", encrypted);
    return ResponseEntity.ok(response);
  }
    
    @SuppressWarnings("unchecked")
    @RequestMapping(value = "/account/user/checkCurrentPasswordAndEmail", method = RequestMethod.POST)
    @GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    public ResponseEntity<?>  checkCurrentPasswordAndEmail(HttpServletRequest request,@RequestParam String oldpassword, @RequestParam String newEmailAddress) throws InvalidUserException {
    	JSONObject resp = new JSONObject();
    	AccountUser userObj = userService.getLoggedInUser();

    	//Try authenticating on WSO2 using old password
    	if(isScimEnabled){
    		try {
    			if(!scimUserManager.authenticate(userObj.getUsername(), oldpassword)){

    				if(LOGGER.isDebugEnabled()) {
    					LOGGER.debug("The current password doesn't match with our records.".intern());
    				}
    				resp.put("status", "FAILURE");
    				resp.put("code", HttpStatus.UNAUTHORIZED.value());
    				return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(resp);
    			}
    		} catch (GIException e) {
    			LOGGER.info("Error in changing password on WSO2: ".intern() + e.getMessage());
    			resp.put("status", "FAILURE");
    			resp.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value()); // 500 error, we don't know wtf is went wrong
    			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
    		}
    	}else if(!userService.isPasswordSameInDatabase(userObj, oldpassword,userObj.getPassword())) {//HIX-32110
    		if(LOGGER.isDebugEnabled()) {
    			LOGGER.debug("The current password doesn't match with our records.".intern());
    		}
    		resp.put("status", "FAILURE");
    		resp.put("code", HttpStatus.UNAUTHORIZED.value());
    		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(resp);
    	}


    	if(newEmailAddress != null && !newEmailAddress.isEmpty()){
    		AccountUser emailObj = null;	
    		if(isScimEnabled) {
    			try {
    				emailObj = scimUserManager.findByEmail(newEmailAddress.toLowerCase());
    			} catch (GIException e) {
    				LOGGER.info("Error in finding user by Email: ".intern() + e.getMessage());
    				resp.put("status", "FAILURE");
    				resp.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value()); // 500 error, we don't know wtf is went wrong
    				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
    			}
    		}else {
    			emailObj = userService.findByEmail(newEmailAddress.toLowerCase());
    		}

    		if(emailObj != null) {
    			LOGGER.info("User already exists with this email address: {}".intern(), newEmailAddress);
    			resp.put("status", "FAILURE");
    			resp.put("code", HttpStatus.NOT_ACCEPTABLE.value());
    			return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body(resp);
    		}
    	}
    	
    	// Change email as there are no errors.
		try {
			userChangeEmailManager.processEmailChangeRequest(userObj.getUsername(), newEmailAddress);
			resp.put("status", "SUCCESS");
			resp.put("code", HttpStatus.OK.value());
			return ResponseEntity.ok(resp);
		} catch (Exception e) {
			LOGGER.info("Error in processing email change request: {} ".intern() + e.getMessage());
			resp.put("status", "FAILURE");
			resp.put("code", HttpStatus.INTERNAL_SERVER_ERROR.value());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(resp);
		}
    	
    }
    
    /**
     * Checks if valid password
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     */
    @RequestMapping(value = "/account/signup/checkResetUserPassword")
    @ResponseBody
    public String checkResetUserPassword(@RequestParam String password,@RequestParam String token) {
           	String errorMsg = StringUtils.EMPTY;
           
   			// validate token with db & check for expiration
           	String urlToken = StringUtils.substringAfter(token, "/account/user/reset/");
   			AccountUser userObj = userService.findByPasswordRecoveryToken(urlToken);
           if(!validationUtility.checkForPasswordComplexity(password, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))
        	 || validationUtility.isPasswordAvailableInPasswordHistory(userObj.getUsername(), password,  DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))){
        		   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistory", null, LocaleContextHolder.getLocale()));
        		  
           }
           if(StringUtils.isBlank(errorMsg)) {
        	   userService.savePasswordRecoveryDetails(userObj.getEmail(), null);
           }
           return errorMsg;
    }

    @RequestMapping(value = "/account/signup/checkResetQuestionPasswordLoggedInUser")
    @GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    public String checkResetQuestionPasswordLoggedInUser(HttpServletRequest request,@RequestParam String oldpassword) throws InvalidUserException {
            LOGGER.info("checkResetQuestionPasswordLoggedInUser started");
           	String errorMsg = StringUtils.EMPTY;
           	AccountUser userObj = userService.getLoggedInUser();
           	
           	//Try authenticating on WSO2 using old password
           	if(isScimEnabled){
    			try {
    				if(!scimUserManager.authenticate(userObj.getUsername(), oldpassword)){
    					errorMsg = "The current password doesn't match with our records.".intern();
    					return errorMsg;
    				}
    			} catch (GIException e) {
    				if(e.getMessage().contains("17003".intern())){
    				   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account Locked for "+userObj.getUserName()));
    				   return "accountlocked";
    				}else{
    				   LOGGER.info("Error in changing password on WSO2: ".intern() + e.getMessage());
    				   errorMsg = e.getMessage();
       				   return errorMsg;
    				}
    			}
    		}else if(!userService.isPasswordSameInDatabase(userObj, oldpassword,userObj.getPassword())) {//HIX-32110
    			//String errorMSg = "The current password doesn't match with our records.";
    			//model.addAttribute("errorMsg", errorMSg);
    			errorMsg = "The current password doesn't match with our records.";
    			return errorMsg;
    		}
   		
   		 
           return errorMsg;
    }
    /**
     * Checks if valid password
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     * @throws InvalidUserException 
     */
    @RequestMapping(value = "/account/signup/checkResetUserPasswordLoggedInUser")
    @GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_WRITE)
    @ResponseBody
    public String checkResetUserPasswordForLoggedInUser(HttpServletRequest request,@RequestParam String password,@RequestParam String action,@RequestParam String oldpassword) throws InvalidUserException {
           LOGGER.info(CHECK_USER_PASSWORD_START);
           	String errorMsg = StringUtils.EMPTY;
           	AccountUser userObj ;
           	if("USER_FROM_SESSION".intern().equalsIgnoreCase(action)){
           		userObj = (AccountUser)request.getSession().getAttribute("user".intern());
           	}else{
           		userObj = userService.getLoggedInUser();
           	}
           	//Try authenticating on WSO2 using old password
           	if(isScimEnabled){
    			try {
    				if(!scimUserManager.authenticate(userObj.getUsername(), oldpassword)){
    					errorMsg = "The current password doesn't match with our records.".intern();
    					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Incorrect current password provided during password reset for "+userObj.getUserName()));
    					return errorMsg;
    				}
    			} catch (GIException e) {
    				if(e.getMessage().contains("17003".intern())){
    				   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account Locked for "+userObj.getUserName()));
    				   return "accountlocked";
    				}else{
    				   LOGGER.info("Error in changing password on WSO2: ".intern() + e.getMessage());
    				   errorMsg = e.getMessage();
       				   return errorMsg;
    				}
    			}
    		}else if(!userService.isPasswordSameInDatabase(userObj, oldpassword,userObj.getPassword())) {//HIX-32110
    			//String errorMSg = "The current password doesn't match with our records.";
    			//model.addAttribute("errorMsg", errorMSg);
    			errorMsg = "The current password doesn't match with our records.";
    			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Incorrect current password provided during password reset for "+userObj.getUserName()));
    			return errorMsg;
    		}
   		
   		 /* 
   		  * As per HIX-38037 id .Passing passing password minimum Age as 0.
   		  * Commented method can be used in future to fetch property from Database
   		  *  
   		  *   
   		  *  if(!validationUtility.checkForPasswordMinAge(userObj, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_AGE)) &&
   				!"USER_FROM_SESSION".equalsIgnoreCase(action)){
         	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
         	   return errorMsg;
   		   }*/
           	
            if(!validationUtility.checkForPasswordMinAge(userObj,ZERO) &&
       				!"USER_FROM_SESSION".equalsIgnoreCase(action)){
             	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
             	   return errorMsg;
       		   }
           if((!validationUtility.checkForPasswordComplexity(password, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))
        	 || (!isScimEnabled && validationUtility.isPasswordAvailableInPasswordHistory(userObj.getUsername(), password,  DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))))){
        	   Role currUserDefRole = userService.getDefaultRole(userService.getLoggedInUser());
			   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithminage", null, LocaleContextHolder.getLocale()));
			   if(null != currUserDefRole) {
					if ( currUserDefRole.getPrivileged() ==1) {
						errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistory", null, LocaleContextHolder.getLocale()));
					}
				}
        	   return errorMsg;  
           }
           
           if(isScimEnabled && errorMsg.isEmpty()){
        	   try { userService.changePasswordOnWSO2(request, password, oldpassword);
        	   }catch (WSO2Exception ex) {
        		   LOGGER.error("Exception in changing the password for the remote user", ex);
        		   HttpSession session = request.getSession();
        		   errorMsg =  ex.getMessage();
        	   }
        	   catch (Exception ex) {
        		   LOGGER.error("Exception in changing the password", ex);
        		   HttpSession session = request.getSession();
        		   if(ex.getMessage().contains("30000")) {
        			   errorMsg = messageSource.getMessage("label.wso2msgforpasswordhistoryplugin", null, LocaleContextHolder.getLocale());
        		   } else {
        			   errorMsg = "Errors in changing the password";
        		   }
        	   }
           }
           return errorMsg;
    }
    
   
    /**
     * Checks if valid password
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     * @throws InvalidUserException 
     */
    @RequestMapping(value = "/account/signup/checkResetUserPasswordUserLogin", method = RequestMethod.POST)
    @ResponseBody
    public String checkResetUserPasswordUserLogin(HttpServletRequest request,@RequestParam String password,@RequestParam String action,@RequestParam String email) throws InvalidUserException {
           LOGGER.info(CHECK_USER_PASSWORD_START);
           
           	String errorMsg = StringUtils.EMPTY;
           	AccountUser userObj =userService.findByUserName(email);
           	
   			
   		
   		  /*
   		   * As per HIX-38037 id .Passing passing password minimum Age as 0.
   		   * Commented method can be used in future to fetch property from Database.
   		   * 
   		   * 
   		   *  if(!validationUtility.checkForPasswordMinAge(userObj, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_AGE))){
         	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
         	   return errorMsg;
   		   }*/
           	
            if(!validationUtility.checkForPasswordMinAge(userObj, ZERO)){
          	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
          	   return errorMsg;
    		   }
           	
           if((!validationUtility.checkForPasswordComplexity(password, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))
        	 || validationUtility.isPasswordAvailableInPasswordHistory(userObj.getUsername(), password,  DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT)))){
        	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistory", null, LocaleContextHolder.getLocale()));
        	   return errorMsg;  
           }
           
           return errorMsg;
    }
    
    /**
     * Checks if valid password during account unlock
     *
     * @param password
     *            password
     * @return boolean value 'true' if password valid, else return 'false'
     * @throws InvalidUserException 
     */
    @RequestMapping(value = "/account/signup/unlockUserPasswordLoggedInUser")
    @ResponseBody
    public String checkUnlockPasswordForLoggedInUser(HttpServletRequest request,@RequestParam String password,@RequestParam String action) throws InvalidUserException {
           LOGGER.info(CHECK_USER_PASSWORD_START);
           
           
           	String errorMsg = StringUtils.EMPTY;
           	AccountUser userObj ;
           	if("USER_FROM_SESSION".equalsIgnoreCase(action)){
           		userObj = (AccountUser)request.getSession().getAttribute("user");
           	}else{
           		userObj = userService.getLoggedInUser();
           	}
   			
   		
   		 /* 
   		  * As per HIX-38037 id .Passing passing password minimum Age as 0.
   		  * Commented method can be used in future to fetch property from Database
   		  *  
   		  *  
   		  *  if(!validationUtility.checkForPasswordMinAge(userObj, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_AGE)) &&
   				!"USER_FROM_SESSION".equalsIgnoreCase(action)){
         	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
         	   return errorMsg;
   		   }*/
           	
            if(!validationUtility.checkForPasswordMinAge(userObj,ZERO) &&
       				!"USER_FROM_SESSION".equalsIgnoreCase(action)){
             	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
             	   return errorMsg;
       		   }
           if((!validationUtility.checkForPasswordComplexity(password, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX))
        	 || validationUtility.isPasswordAvailableInPasswordHistory(userObj.getUsername(), password,  DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT)))){
        	   errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistory", null, LocaleContextHolder.getLocale()));
        	   return errorMsg;  
           }
           
         
           return errorMsg;
    }

}
