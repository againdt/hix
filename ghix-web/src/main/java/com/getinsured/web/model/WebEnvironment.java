package com.getinsured.web.model;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Describes current web environment.
 *
 * @author Yevgen Golubenko
 * @since 2/11/19
 */
public class WebEnvironment
{
  private long tenantId;
  private boolean devEnvironment;
  private boolean anonymous = true;
  private boolean impersonation;
  private UserJson user;
  private String stateCode = null;
  private String exchangeName = null;
  private Date oepStartDate;
  private Date oepEndDate;
  private Date oepEnrollStartDate;
  private Date oepEnrollEndDate;
  private int coverageYear;
  private Date previousOepStartDate;
  private Date previousOepEndDate;
  private int renewalCoverageYear;
  private String language = Locale.ENGLISH.getLanguage();
  private Map<LinkType, String> links = new HashMap<>();
  private Map<AssetType, String> assets = new HashMap<>();
  private Map<PropertyType, String> properties = new HashMap<>();
  private Map<String, Object> impersonationDetails = null;
  private Long unreadMessageCount = null;

  public long getTenantId()
  {
    return tenantId;
  }

  public void setTenantId(final long tenantId)
  {
    this.tenantId = tenantId;
  }

  public boolean isDevEnvironment()
  {
    return devEnvironment;
  }

  public void setDevEnvironment(final boolean devEnvironment)
  {
    this.devEnvironment = devEnvironment;
  }

  public boolean isAnonymous()
  {
    return anonymous;
  }

  public void setAnonymous(final boolean anonymous)
  {
    this.anonymous = anonymous;
  }

  public UserJson getUser()
  {
    return user;
  }

  public void setUser(final UserJson user)
  {
    this.user = user;
  }

  public String getStateCode()
  {
    return stateCode;
  }

  public void setStateCode(final String stateCode)
  {
    this.stateCode = stateCode;
  }

  public Date getOepStartDate() {
    return oepStartDate;
  }

  public void setOepStartDate(Date oepStartDate) {
    this.oepStartDate = oepStartDate;
  }

  public Date getOepEndDate() {
    return oepEndDate;
  }

  public void setOepEndDate(Date oepEndDate) {
    this.oepEndDate = oepEndDate;
  }

  public Date getOepEnrollStartDate() {
    return oepEnrollStartDate;
  }

  public void setOepEnrollStartDate(Date oepEnrollStartDate) {
    this.oepEnrollStartDate = oepEnrollStartDate;
  }

  public Date getOepEnrollEndDate() {
    return oepEnrollEndDate;
  }

  public void setOepEnrollEndDate(Date oepEnrollEndDate) {
    this.oepEnrollEndDate = oepEnrollEndDate;
  }

  public int getCoverageYear() {
    return coverageYear;
  }

  public void setCoverageYear(int coverageYear) {
    this.coverageYear = coverageYear;
  }

  public Date getPreviousOepStartDate() {
    return previousOepStartDate;
  }

  public void setPreviousOepStartDate(Date previousOepStartDate) {
    this.previousOepStartDate = previousOepStartDate;
  }

  public Date getPreviousOepEndDate() {
    return previousOepEndDate;
  }

  public void setPreviousOepEndDate(Date previousOepEndDate) {
    this.previousOepEndDate = previousOepEndDate;
  }

  public int getRenewalCoverageYear() {
    return renewalCoverageYear;
  }

  public void setRenewalCoverageYear(int renewalCoverageYear) {
    this.renewalCoverageYear = renewalCoverageYear;
  }

  public String getExchangeName()
  {
    return exchangeName;
  }

  public void setExchangeName(final String exchangeName)
  {
    this.exchangeName = exchangeName;
  }

  public String getLanguage()
  {
    return language;
  }

  public void setLanguage(final String language)
  {
    this.language = language;
  }

  public Map<LinkType, String> getLinks()
  {
    return links;
  }

  public void setLinks(final Map<LinkType, String> links)
  {
    this.links = links;
  }

  public Map<AssetType, String> getAssets()
  {
    return assets;
  }

  public void setAssets(final Map<AssetType, String> assets)
  {
    this.assets = assets;
  }

  public Map<PropertyType, String> getProperties()
  {
    return properties;
  }

  public void setProperties(final Map<PropertyType, String> properties)
  {
    this.properties = properties;
  }

  public boolean isImpersonation()
  {
    return impersonation;
  }

  public void setImpersonation(final boolean impersonation)
  {
    this.impersonation = impersonation;
  }

  public Map<String, Object> getImpersonationDetails()
  {
    return impersonationDetails;
  }

  public void setImpersonationDetails(final Map<String, Object> impersonationDetails)
  {
    this.impersonationDetails = impersonationDetails;
  }

  public Long getUnreadMessageCount() {
    return unreadMessageCount;
  }

  public void setUnreadMessageCount(Long unreadMessageCount) {
    this.unreadMessageCount = unreadMessageCount;
  }
}
