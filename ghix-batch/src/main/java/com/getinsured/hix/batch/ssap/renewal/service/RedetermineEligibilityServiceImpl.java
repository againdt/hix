package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.hix.batch.ssap.renewal.util.EligibilityResponse;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.giwspayload.service.GIWSPayloadService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.util.TSDate;

@Service("redetermineEligibilityService")
public class RedetermineEligibilityServiceImpl implements RedetermineEligibilityService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedetermineEligibilityServiceImpl.class);
	
	private static final String PROCESS_ELIGIBILITY = "PROCESS_ELIGIBILITY";
	private static final String PROCESS_OUTBOUND_AT = "PROCESS_OUTBOUND_AT";

	@Autowired
	private SsapCloneApplicationService ssapCloneApplicationService;
	@Autowired
	private GIMonitorService giMonitorService;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired
	private GIWSPayloadService giwsPayloadService;

	@Override
	public List<Long> getSsapAppIdsForRenewalWithSgStatus(Long renewalYear, Long batchSize) {
		return ssapCloneApplicationService.getSsapAppIdsForRenewalWithSgStatus(renewalYear, batchSize);
	}

	@Override
	public List<SsapApplication> getSsapApplicationListByIds(List<Long> applicationIdList) {
		return ssapCloneApplicationService.getSsapApplicationListByIds(applicationIdList);
	}

	@Override
	public void saveAndThrowsErrorLog(String errorMessage, String errorCode) throws UnexpectedJobExecutionException {
		giMonitorService.saveOrUpdateErrorLog(errorCode, new TSDate(), this.getClass().getName(), errorMessage, null,
				null, GIRuntimeException.Component.BATCH.getComponent(), null);
		throw new UnexpectedJobExecutionException(errorMessage);
	}

	@Override
	public EligibilityResponse invokeEligibilityEngine(long ssapApplicationId) {
		String endpointUrl = GhixEndPoints.GHIXHIX_SERVICE_URL + "api/newssap/eligibility/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(Long.toString(ssapApplicationId));
		String response = null;
		String status = RenewalConstants.FAILURE;
		EligibilityResponse eligibilityResponse = null;
		try {
			ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(endpointUrl,
					"exadmin@ghix.com", HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);
			if (responseEntity != null) {
				response = responseEntity.getBody();
				ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EligibilityResponse.class);
				eligibilityResponse = reader.readValue(response);
				if("200".equalsIgnoreCase(eligibilityResponse.getStatus())){
					status = RenewalConstants.SUCCESS;
				}
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while invoking eligibility engine for application - " + ssapApplicationId, ex);
			throw new GIRuntimeException("Exception occured while invoking eligibility engine for application - " + ssapApplicationId, ex);
		} finally {
			logServicePayload(ssapApplicationId, String.valueOf(ssapApplicationId), endpointUrl, response, status, PROCESS_ELIGIBILITY);
		}
		return eligibilityResponse;
	}
	
	@Override
	public String invokeOutboundAT(long ssapApplicationId){
		String endpointUrl = GhixEndPoints.ELIGIBILITY_URL + "ssapapplication/generateAndPushAT?applicationId=" + ssapApplicationId;
		String response = null;
		String status = RenewalConstants.SUCCESS;
		try {
			ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(endpointUrl,
					"exadmin@ghix.com", HttpMethod.GET, MediaType.APPLICATION_JSON, String.class, null);
			if (responseEntity != null) {
				response = responseEntity.getBody();
				return response;
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occured while invoking outbound AT for application - " + ssapApplicationId, ex);
			status = RenewalConstants.FAILURE;
			throw new GIRuntimeException("Exception occured while invoking outbound AT for application - " + ssapApplicationId, ex);
		} finally {
			logServicePayload(ssapApplicationId, String.valueOf(ssapApplicationId), endpointUrl, response, status, PROCESS_OUTBOUND_AT);
		}
		return null;
	}
	
	private void logServicePayload(long ssapApplicationId, String request, String endpointUrl, String response, String status, String operationName) {
		try {
			GIWSPayload giwsPayload = new GIWSPayload();
	        giwsPayload.setSsapApplicationId(ssapApplicationId);
	        giwsPayload.setCreatedTimestamp(new TSDate());
	        giwsPayload.setEndpointFunction(RenewalConstants.REDETERMINE_ELIGIBILITY_BATCH);
	        giwsPayload.setEndpointOperationName(operationName);
	        giwsPayload.setEndpointUrl(endpointUrl);
	        giwsPayload.setRequestPayload(request);
	        giwsPayload.setResponsePayload(response);
	        giwsPayload.setStatus(status);
	        giwsPayloadService.save(giwsPayload);
		} catch(Exception e){
			LOGGER.error("Exception occurred while inserting records in payload table", e);
			throw new GIRuntimeException(e);
		} 
	}
}
