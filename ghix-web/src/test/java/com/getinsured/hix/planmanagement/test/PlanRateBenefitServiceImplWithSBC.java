package com.getinsured.hix.planmanagement.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Provider.ProviderType;
import com.getinsured.hix.planmgmt.config.PlanmgmtConfiguration;
import com.getinsured.hix.planmgmt.repository.IRegionRepository;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;

public class PlanRateBenefitServiceImplWithSBC extends BaseTest {
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanRateBenefitServiceImplWithSBC.class);
	@Autowired private IRegionRepository iRegionRepository;
	private static final String ZIP = "zip";
	private static final String AGE = "age";
	private static final String RELATION = "relation";
	private static final String COUNTY = "county";
	private static final String PROVIDER_TYPE = "type";
	private static final String PROVIDER_ID = "providerId";
	private static final String IS_SUPPORTED = "isSupported"; 
	private static final String COUNTYCODE = "countycode";
	private static final String ID = "id";
    private static final String CS1 = "CS1"; // for base plan of any metal tier
    private static final String CS2 = "CS2"; // for any metal tier, american native FPL plan, when we pull plan from SERFF
    private static final String CS3 = "CS3"; // for any metal tier, american native FPL plan, when we pull plan from SERFF
    private static final String CS4 = "CS4"; // for silver plan, csr variation #1
    private static final String CS5 = "CS5"; // for silver plan, csr variation #2
    private static final String CS6 = "CS6"; // for silver plan, csr variation #3

	@PersistenceUnit private EntityManagerFactory emf;
	
	
	@Test
	public void testProcessMemberData(){
		
		final ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		final ArrayList<Map<String, String>> modifiedData = new ArrayList<Map<String, String>>();
		
		final Map<String, String> member1 = new HashMap<String, String>();
		member1.put(AGE, "45");
		member1.put(ZIP, "94957");
		member1.put(RELATION, "member");
		
		final Map<String, String> member7 = new HashMap<String, String>();
		member7.put(AGE, "41");
		member7.put(ZIP, "94957");
		member7.put(RELATION, "spouce");
		
		final Map<String, String> member2 = new HashMap<String, String>();
		member2.put(AGE, "15");
		member2.put(ZIP, "94960");
		member2.put(RELATION, "child");
		
		final Map<String, String> member3 = new HashMap<String, String>();
		member3.put(AGE, "16");
		member3.put(ZIP, "94960");
		member3.put(RELATION, "child");
		
		final Map<String, String> member4 = new HashMap<String, String>();
		member4.put(AGE, "17");
		member4.put(ZIP, "94960");
		member2.put(RELATION, "child");
		
		final Map<String, String> member5 = new HashMap<String, String>();
		member5.put(AGE, "18");
		member5.put(ZIP, "94957");
		member5.put(RELATION, "child");
		
		final Map<String, String> member6 = new HashMap<String, String>();
		member6.put(AGE, "25");
		member6.put(ZIP, "94957");
		member6.put(RELATION, "child");
		
		
		memberList.add(member1);
		memberList.add(member2);
		memberList.add(member3);
		memberList.add(member4);
		memberList.add(member5);
		memberList.add(member7);
		
		final String childMaxAge = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERCHILDAGE);
		final String ConsiderMaxChilds = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERMAXCHILDREN);
		int cMaxAge = 0;
		int childCount = 0;
		int considerMaxChildrens = 0;
		
		try {
			cMaxAge = Integer.parseInt(childMaxAge);
			considerMaxChildrens = Integer.parseInt(ConsiderMaxChilds);
			
			for(int i=0; i< memberList.size(); i++){
				if(memberList.get(i).get(RELATION) != null){
					if(memberList.get(i).get(RELATION).equalsIgnoreCase("child")){
						if (Integer.parseInt(memberList.get(i).get(AGE)) < cMaxAge && childCount < considerMaxChildrens) {
							modifiedData.add(memberList.get(i));
							childCount++;
						}else if (Integer.parseInt(memberList.get(i).get(AGE)) >= cMaxAge) {
							modifiedData.add(memberList.get(i));	
						} 
					}else{
						modifiedData.add(memberList.get(i));
					}
				}	
			}
		
		}catch (final NumberFormatException e) {
			System.out.println("error while pasring config param"+e.getMessage());
		}
			
		System.out.println("Result for memebersList:- "+memberList);
		System.out.println("test completes success");
	}
	
	@Test
	public void testGetHouseholdHealthPlanRateBenefits() {
		
		final String effectiveDate = "01/05/2013";
		final String costSharing = "1000";
		final String planIdStr ="87";
		final boolean showCatastrophicPlan = false;
		final String planLevel="BRONZE";
		final String ehbCovered="COMPLETE";
		final String isSpecialEnrollment="YES";
		final String marketType="INDIVIDUAL";
		final ArrayList<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
		
		System.out.println("add test data");
		
		final Map<String, String> member1 = new HashMap<String, String>();
		member1.put(AGE, "45");
		member1.put(ZIP, "94957");
		member1.put(RELATION, "member");
		
		final Map<String, String> member7 = new HashMap<String, String>();
		member7.put(AGE, "41");
		member7.put(ZIP, "94957");
		member7.put(RELATION, "spouce");
		
		final Map<String, String> member2 = new HashMap<String, String>();
		member2.put(AGE, "15");
		member2.put(ZIP, "94960");
		member2.put(RELATION, "child");
		
		final Map<String, String> member3 = new HashMap<String, String>();
		member3.put(AGE, "16");
		member3.put(ZIP, "94960");
		member3.put(RELATION, "child");
		
		final Map<String, String> member4 = new HashMap<String, String>();
		member4.put(AGE, "17");
		member4.put(ZIP, "94960");
		member2.put(RELATION, "child");
		
		final Map<String, String> member5 = new HashMap<String, String>();
		member5.put(AGE, "18");
		member5.put(ZIP, "94957");
		member5.put(RELATION, "child");
		
		final Map<String, String> member6 = new HashMap<String, String>();
		member6.put(AGE, "25");
		member6.put(ZIP, "94957");
		member6.put(RELATION, "child");
		
		
		memberList.add(member1);
		memberList.add(member2);
		memberList.add(member3);
		memberList.add(member4);
		memberList.add(member5);
		memberList.add(member7);
		
		

		final String familyTier = getFamilyTier(memberList);
		
		System.out.println("Test executaion is in progress....");
		
		String buildquery = "SELECT p.id AS plan_id, p.name AS plan_name, plan_level, sum(pre) AS total_premium, p.network_type AS network_type, i.name AS issuer_name, phealth_id, parent_id, ehb_covered, i.id as issuer_id, " +
				" listagg(memberid, ',')  within group (order by memberid) as memberlist , listagg(pre, ',')  within group (order by memberid) as memberprelist, listagg(rname, ',')  within group (order by memberid) as memberrelist, sbc_ucm_id, p.brochure, benefits_url" +
 				" FROM plan p, issuers i, (" ;

		for(int i=0; i< memberList.size(); i++){
	  		buildquery += "( SELECT phealth.id AS phealth_id, phealth.parent_plan_id AS parent_id, phealth.plan_id, prate.rate AS pre, phealth.plan_level AS plan_level, phealth.ehb_covered as ehb_covered, " + memberList.get(i).get(ID) +" as memberid, prarea.rating_area as rname, 1 as memberctr, phealth.sbc_ucm_id as sbc_ucm_id, p2.brochure as brochure, phealth.benefits_url as benefits_url" +
  				  		  " FROM plan_health phealth, pm_service_area psarea, pm_plan_rate prate, plan p2, pm_rating_area prarea, pm_zip_county_rating_area pzcrarea " +
	  					  " WHERE " +
	  					  " p2.id = phealth.plan_id " ;

	  		if (costSharing == null ||costSharing.equals("") || costSharing.equalsIgnoreCase(CS1)){
	  	       buildquery += " AND phealth.parent_plan_id = 0 "; // pick all base plan of all metal tier
		  	}else if (costSharing.equalsIgnoreCase(CS2) || costSharing.equalsIgnoreCase(CS3)){
		  	   buildquery += " AND phealth.cost_sharing = UPPER('" + costSharing  +"')" ; // pick all respective cost sharing plan of all metal tier
		  	}else if (costSharing.equalsIgnoreCase(CS4) || costSharing.equalsIgnoreCase(CS5) || costSharing.equalsIgnoreCase(CS6)){
		  		buildquery += " AND ((phealth.parent_plan_id = 0 and phealth.plan_level !='" + Plan.PlanLevel.SILVER.toString() + "')  OR phealth.cost_sharing=UPPER('" + costSharing  +"'))"; // pick base plan of other metal tier except silver + respective cost sharing plan
		  	}

	  		buildquery += " AND p2.service_area_id = psarea.service_area_id " +
	  					  " AND psarea.zip = " + memberList.get(i).get(ZIP) +
	  					  " AND prarea.id = prate.rating_area_id " +
	  					  " AND p2.id = prate.plan_id " +
	  					  " AND pzcrarea.rating_area_id = prate.rating_area_id " +							  
	  					 // " AND prate.tobacco = '" + memberList.get(i).get("tobacco").toLowerCase() + "' " +
	  					  " AND " + memberList.get(i).get(AGE) +" >= prate.min_age AND " + memberList.get(i).get(AGE) + " <= prate.max_age " +
	  					 // " AND prate.family_tier = '" + familyTier + "' " +
	  					  " AND pzcrarea.zip = " + memberList.get(i).get(ZIP) +
	  				      " AND TO_DATE ('" + effectiveDate + "', 'YYYY-MM-DD') BETWEEN prate.effective_start_date and prate.effective_end_date ";


			// if showCatastrophicPlan == false, show all plan tier except CATASTROPHIC
			if(!showCatastrophicPlan){
				  // if we are looking for any specific plan level like gold/silver etc
				if(!planLevel.equals("")){
					buildquery +=  " AND phealth.plan_level = '" + planLevel.toUpperCase() + "'";
				}else{
					// any metal tier except CATASTROPHIC
					buildquery += " AND phealth.plan_level != '" + Plan.PlanLevel.CATASTROPHIC.toString() + "'";
				}
	  		}else{ // if showCatastrophicPlan == true
	  		     // if we are looking for any specific plan level like gold/silver etc
				if(!planLevel.equals("")){
					buildquery +=  " AND (phealth.plan_level = '" + planLevel.toUpperCase() + "' OR phealth.plan_level = '" + Plan.PlanLevel.CATASTROPHIC.toString()+ "')";
				}
	  		}
			// if we are looking for Essential Health Benefits covered  plans
			if(!ehbCovered.equals("")){
				buildquery +=  " AND phealth.ehb_covered = '" + ehbCovered.toUpperCase() +  "'";
			}

			if( memberList.get(i).get(COUNTYCODE) != null && memberList.get(i).get(COUNTYCODE) != ""){
	  			buildquery += " AND psarea.fips = '" + memberList.get(i).get(COUNTYCODE) + "'";
	  		}
	  		buildquery += " )";

	  		if(i+1 !=  memberList.size()){
	  			buildquery += " UNION ALL ";
	  		}

		 }

		buildquery += ") temp WHERE p.id = temp.plan_id AND TO_DATE ('" + effectiveDate + "', 'YYYY-MM-DD') BETWEEN p.start_date AND p.end_date " +
				" AND p.status IN ('" + Plan.PlanStatus.CERTIFIED.toString() + "') " +
  				" AND p.issuer_verification_status='" + Plan.IssuerVerificationStatus.VERIFIED.toString() + "' ";
			if(isSpecialEnrollment.equalsIgnoreCase("YES")){
				buildquery += " AND (p.enrollment_avail='" + Plan.EnrollmentAvail.AVAILABLE.toString() + "' OR p.enrollment_avail='" + Plan.EnrollmentAvail.DEPENDENTSONLY.toString() + "') ";
			}else{
				buildquery += " AND p.enrollment_avail='" + Plan.EnrollmentAvail.AVAILABLE.toString() + "' ";
			}
			buildquery += " AND TO_DATE ('" + effectiveDate + "', 'YYYY-MM-DD') >= p.enrollment_avail_effdate " +
  				" AND p.market = '" + marketType.toUpperCase() +"' AND p.issuer_id = i.id ";

		if((planIdStr != null) && !planIdStr.equals("")){
			buildquery += " AND p.id in (" + planIdStr + ") " ;
		}
		buildquery += " GROUP BY  p.id, p.name, phealth_id, parent_id, plan_level, i.name, p.network_type, ehb_covered, i.id, sbc_ucm_id, p.brochure,benefits_url HAVING sum(memberctr) = " + memberList.size() ;

		System.out.println("SQL == "+ buildquery);
		
		final EntityManager em = emf.createEntityManager();
		final List rsList = em.createNativeQuery(buildquery).getResultList();		 
		final Iterator rsIterator = rsList.iterator();
		
		
		while(rsIterator.hasNext()){
			System.out.println("Result After query Execution : "+rsIterator.next());
		}
		
		System.out.println("Test compltes successfully");
		
	}	

	@Ignore
	private String getFamilyTier(final ArrayList<Map<String, String>> memberList){		
		
		final String considerOnlyMemberPMPM = DynamicPropertiesUtil.getPropertyValue(PlanmgmtConfiguration.PlanmgmtConfigurationEnum.QUOTING_CONSIDERONLYMEMBERPMPM);
		
		if (considerOnlyMemberPMPM.equalsIgnoreCase("YES")) {
			return "MEMBER";
		}
		
		Boolean isMember = false;
		Boolean isSpouse = false;
		Boolean isChild = false;
		
		for(int i=0; i< memberList.size(); i++){
			if(memberList.get(i).get(RELATION) != null){
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("member") || memberList.get(i).get(RELATION).equalsIgnoreCase("self")){
					isMember = true;
				}	
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("spouse")){
					isSpouse = true;
				}	
				if(memberList.get(i).get(RELATION).equalsIgnoreCase("child")){
					isChild = true;
				}	
			}	
		}
		
		if(isMember && memberList.size() == 1){		// when only self quoted
			return "MEMBER";
		}	
		if(isSpouse && memberList.size() == 1){		// when only spouse quoted
			return "MEMBER";
		} else if(isMember && isSpouse && memberList.size() == 2){		// when only self + spouse quoted
			return "MEMBERSPOUSE";
		} else if(isMember && isSpouse && isChild){	// when only self + spouse + child quoted
			return "FAMILY";
		} else if(!isMember && !isSpouse && isChild){	// when only child quoted
			return "CHILDONLY";
		} else if(isMember && !isSpouse && isChild){	// when only self + child quoted
			return "MEMBERCHILDREN";
		} else if(!isMember && isSpouse && isChild){	// when only spouse + child quoted
			return "MEMBERCHILDREN";
		} else{
			return "MEMBER";
		}	
		
	}
	
	@Test
	public void checkProviderInPlan() {
		
		final List<Map<String, String>> providers = new ArrayList<Map<String,String>>();
		
		final Map<String ,String>  p1 = new HashMap<String, String>();
		final Map<String ,String>  p2 = new HashMap<String, String>();
		final Map<String ,String>  p3 = new HashMap<String, String>();
		final Map<String ,String>  p4 = new HashMap<String, String>();
		
		p1.put(PROVIDER_ID, "19");
		p1.put(PROVIDER_TYPE, "DOCTOR");
		p1.put(IS_SUPPORTED, "");
		
		p2.put(PROVIDER_ID, "4");
		p2.put(PROVIDER_TYPE, "DENTIST");
		p2.put(IS_SUPPORTED, "");
		
		p3.put(PROVIDER_ID, "17");
		p3.put(PROVIDER_TYPE, "DOCTOR");
		p3.put(IS_SUPPORTED, "");
		
		p4.put(PROVIDER_ID, "16");
		p4.put(PROVIDER_TYPE, "FACILITY");
		p4.put(IS_SUPPORTED, "");
		
		
		final List<Map<String, String>> modifiedProviders = new ArrayList<Map<String,String>>();
		
		boolean isPlanSupports;
		final int planId = 1;
		
		for (final Object element : providers)
        {
			
			try {
				final Map<String, String> providerMap = (Map<String, String>) element;
				isPlanSupports = false;
				
				isPlanSupports = checkProviderInPlan(planId,Integer.parseInt(providerMap.get(PROVIDER_ID)),providerMap.get(PROVIDER_TYPE));
				
				providerMap.put(IS_SUPPORTED,String.valueOf(isPlanSupports));
				
				modifiedProviders.add(providerMap);
			
			} catch (final Exception e) {
				System.out.println("Error occurred please find reason "+e.getMessage());
			}
		}
		
		LOGGER.debug(modifiedProviders.toString());
		
		System.out.println("Test executed successfully");
	}
	
	private boolean checkProviderInPlan(final int planId,final int providerId,final String physicianType){
		String buildquery = "SELECT COUNT(pln.id) FROM plan pln, physician phy, facility f, provider_network pn, provider prd, network n "+		
				"WHERE " +
				"pln.provider_network_id = pn.id " +
				"AND pn.provider_id = prd.id " +	  		
				"AND pn.network_id = n.id " + 	  						  
				"AND phy.provider_id = prd.id " +
				"AND f.provider_id = prd.id " +
			    "AND pln.id = "+ planId +
				" AND prd.id = "+ providerId;
				if (ProviderType.FACILITY.toString().equalsIgnoreCase(physicianType)) {
					buildquery = buildquery + " AND prd.type = '"+physicianType +"'";
				}else {
					buildquery = buildquery + " AND phy.type = '"+physicianType +"'";
				}
		final EntityManager em = emf.createEntityManager();
		final List rsList = em.createNativeQuery(buildquery).getResultList();
		final Iterator rsIterator = rsList.iterator();
		
		System.out.println("Query :- "+buildquery);
		BigDecimal count = null;

		while(rsIterator.hasNext()){
			count = (BigDecimal) rsIterator.next();						
		}
		
		if (count!= null && count.intValue()>0) {
			return true;
		}else {
			return false;
		}
	}
	
	@Test
	public void isURLValid() {
        try {
        	final String checkURL="https://www.google.com";
        	final String pattern = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
        	final Pattern patt = Pattern.compile(pattern);
            final Matcher matcher = patt.matcher(checkURL);
            final boolean matcherresp= matcher.matches();
            if(matcherresp==true){
            	System.out.println("Given URL is Valid");
            }else{
            	System.out.println("Given URL is InValid");
            }
        }catch (final RuntimeException e) {
        	System.out.println("Problem in validating the URL");
        }
	}
	
	@Test
	public void returnStringFromTwoString(){
		final String sbcDocURL1="FirstString";
		final String benefitURL2="SecondString";
		returnOneString(sbcDocURL1,benefitURL2);
		
		final String sbcDocURL3="";
		final String benefitURL4="SecondString";
		returnOneString(sbcDocURL3,benefitURL4);

		final String sbcDocURL5="FirstString";
		final String benefitURL6="";
		returnOneString(sbcDocURL5,benefitURL6);

		final String sbcDocURL7="";
		final String benefitURL8="";
		returnOneString(sbcDocURL7,benefitURL8);
	}
	
	public void returnOneString(final String sbcDocURL,final String benefitURL){
		if(!(sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())){
			System.out.println("sbcDocURL1: " + sbcDocURL);
		}else if((sbcDocURL.isEmpty()) && !(benefitURL.isEmpty())){
			System.out.println("benefitURL2: " + benefitURL);
		}else if(!(sbcDocURL.isEmpty()) && (benefitURL.isEmpty())){
			System.out.println("sbcDocURL3: " + sbcDocURL);
		}else{
			System.out.println("Nothing to Return");
		}
		
	}

}
