<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<!-- <div class="container"> -->
	<div class="row-fluid">	
		<div class="span12">
			<div style="font-size: 14px; color: red">
				<c:if test="${errorMsg != ''}">
					<p><c:out value="${errorMsg}"></c:out><p/>
				</c:if>
				<br>
			</div>
			<c:if test="${errorMsg == ''}">	
			
				<!-- Other code omitted -->
				
				<%-- <div class="gutter10">
					<div class="page-header">
						<h1><spring:message  code="label.uploadEmployee"/> <a href="/hix/shop/employer/" class="btn pull-right">Back</a> 	</h1>
					</div>
				</div> --%>
				
				<form id="fileUploadForm" class="form-horizontal gutter10" action="uploadempdata" enctype="multipart/form-data"  method="post">
				<df:csrfToken/>
				<div class="well">
					<div class="control-group span7">
						<label class="control-label" for="file"><spring:message  code="label.uploadFile"/></label>
						<div class="controls">
							<input name="file" id="file" type="file"/>
							<input type="submit" name="submit" id="submit" value="Submit" title="Submit" class="btn btn-primary">
							<div id="file_error"></div>
						</div>
					</div>
				<a href="createemployee" class="btn pull-right"><spring:message  code="label.addSingleEmployee"/></a>
				</div>
			<BR />
				<c:if test="condition"></c:if>
				<c:choose>
				   <c:when test="${count >= 1}">
						<div style="float:center;">
								<%-- <fieldset>
									<legend></legend>
									<p></p>
									<div class="profile">
										<div class="control-group">
											  <display:table pagesize="5" export="false" name="employee" sort="list" id="data" class="table" requestURI="uploadempdata">
											  
												  <display:setProperty name="export.excel" value="false" />
												  <display:setProperty name="export.csv" value="false" />
												  <display:setProperty name="export.xml" value="false" />
												  
												  <display:setProperty name="paging.banner.placement">bottom</display:setProperty>
												  <display:column title="Id" sortable="true">
										  				<a href='../employee/editemployee/${data.id}'>${data.id}</a>
										 		  </display:column>
												  <display:column property="person.user.firstName" title="First Name" sortable="true"/>
												  <display:column property="person.user.lastName" title="Last Name" sortable="true"/>
												  <display:column property="person.user.userName" title="Email" sortable="true"/>
												  <display:column property="employer.name" title="Company Name" sortable="true"/>
												  <display:column property="typeOfEmployee" title="Type Of Employee" sortable="true"/>
												  <display:column property="typeOfCoverage" title="Type Of Coverage" sortable="true"/>
												  <display:column property="nativeAmrTribe" title="Native American Tribe" sortable="true"/>
												  <display:column property="hrsPerWeek" title="Hours Per Week" sortable="true"/>
												  <display:column property="created" title="Created" format="{0,date,MM/dd/yyyy}" sortable="true"/>
											</display:table>
										</div>
									</div>
								</fieldset> --%>
						</div>
					</c:when>
				</c:choose>
			</form>
			</c:if>
		</div>
	</div>
<!-- </div> -->
<script type="text/javascript">
jQuery.validator.addMethod("chkExtension", function(value, element, param) {
	var ext = $('#file').val().split('.').slice(-1);
	if(ext != 'csv' &&  ext != 'CSV') {
		return false;
	}
  	return true;
});

$('#file').change(function() {
	$("#file_error").html("");
});

var validator = $("#fileUploadForm").validate({ 
	rules : {
	    'file' : {required: true, chkExtension: true}
	},
	messages : {
		'file': {
				  required : "<span> <em class='excl'>!</em><spring:message  code='label.validatefile' javaScriptEscape='true'/></span>",
			      chkExtension: "<span><em class='excl'>!</em><spring:message  code='label.validatefileExt' javaScriptEscape='true'/></span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});
</script>