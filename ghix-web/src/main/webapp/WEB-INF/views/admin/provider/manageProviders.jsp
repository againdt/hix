<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<%@ page import="java.util.Calendar"%>
<%@ page isELIgnored="false"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">

<style>
	.ui-grid-pager-control input {
	    height: 14px;
	}

	.ui-grid-pager-control .ui-grid-pager-max-pages-number {
	    vertical-align: middle;
	}

	.grid-left-align {
	    text-align: left;
	}

	h1 img {
		height: auto;
		width: auto;
	}
</style>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-animate.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/angular-touch.1.5.6.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/ui-grid.min.4.0.2.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/csv.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/pdfmake.js" />'></script>
<script type="text/javascript" src='<gi:cdnurl value="/resources/js/reports/lib/vfs_fonts.js" />'></script>

<script type="text/javascript" src="<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>"></script>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>"></script>

<script type="text/javascript" src='<gi:cdnurl value="/resources/js/provider/manageProviders.js" />'></script>

<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>

<div class="gutter10">

	<div class="row-fluid">
		<div class="span3">
			<h1 class="pull-left">
				<spring:message code="pgheader.manageProviders" />
			</h1>
		</div> <!-- end of span3 -->
		<div class="span3 pull-right">
			<div class="controls">
				<select id="current" name="current" class="span6 pull-right">
					<option value="current" selected>Current</option>
				</select>
			</div> <!-- end of controls -->
		</div> <!-- end of span3 -->
	</div><!-- end of row-fluid -->

	<div class="row-fluid" id="manageProviderListDiv" ng-app="manageProviderListApp" ng-controller="ManageProviderListCtrl">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4 class="margin0">
					<spring:message code='label.refineresult' />
				</h4>
			</div> <!-- end of header -->

			<div id="leftpanel">
				<form class="form-vertical gutter10 lightgray" id="frmManageProviders" name="frmManageProviders" action="${pageContext.request.contextPath}/admin/provider/manageProviders" method="POST">
					<df:csrfToken/>
					<div class="control-group">
						<label for="nameFilter" class="control-label"><spring:message code='label.providerName' /></label>
						<div class="controls">
							<input type="text" name="nameFilter" id="nameFilter" class="span12" />
						</div> <!-- end of controls-->
					</div> <!-- end of control-group -->

					<div class="control-group">
						<label for="networkIdFilter" class="control-label"><spring:message code='label.providerNetworkId' /></label>
						<div class="controls">
							<input type="text" name="networkIdFilter" id="networkIdFilter" class="span12" />
						</div> <!-- end of controls-->
					</div> <!-- end of control-group -->

					<div class="control-group">
						<label for="specialityFilter" class="control-label"><spring:message code='label.providerSpeciality' /></label>
						<div class="controls">
							<input type="text" name="specialityFilter" id="specialityFilter" class="span12" />
						</div> <!-- end of controls-->
					</div> <!-- end of control-group -->

					<div class="txt-center">
						<input type="button" class="btn btn-primary" ng-click="getProviderReport()" value="<spring:message code='label.go' />" title="<spring:message  code='label.go' />">&nbsp;&nbsp;
						<input type="button" class="btn" ng-click="resetFilter()" id="ResetFilter" name="ResetFilter" value="<spring:message code='label.reset' />" title="<spring:message code='label.reset' />" />
					</div> <!-- end of span9 -->
				</form> <!-- end of frmManageProviders -->
			</div> <!-- end of leftpanel -->
		</div> <!-- end of span3 -->

		<div class="span9" id="rightpanel">
			<div class="row-fluid">
				<div ui-grid="gridOptions" style="height: 100%" class="grid" ui-grid-pagination ui-grid-resize-columns></div>
			</div><!-- end of .row-fluid -->
		</div> <!-- end of span9 -->
	</div> <!-- end of row-fluid -->
</div><!-- end .gutter10 -->
