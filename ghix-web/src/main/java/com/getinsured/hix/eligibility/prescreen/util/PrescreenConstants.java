package com.getinsured.hix.eligibility.prescreen.util;



/**
 * Constant class for Prescreen Eligibility
 * @author Nikhil Talreja
 * @since 17 April, 2013 
 * 
 */
public final class PrescreenConstants {
	
	//The following constants have been moved to GhixEndpoints.java and the references are updated.
//	public static final String SERVER_NAME = "localhost:8080";
//	
//	public static final String CALCULATE_APTC_URL = "http://" + SERVER_NAME + "/ghix-prescreen-svc/eligibility/calculateAptc";
//	public static final String GET_PRESCREEN_RECORD = "http://" + SERVER_NAME + "/ghix-prescreen-svc/eligibility/getPrescreenRecord";
//	public static final String GET_PRESCREEN_RECORD_BY_SESSION = "http://" + SERVER_NAME + "/ghix-prescreen-svc/eligibility/getPrescreenRecordBySessionId";
//	public static final String SAVE_PRESCREEN_RECORD = "http://" + SERVER_NAME + "/ghix-prescreen-svc/eligibility/savePrescreenRecord";
	
	public static final String CONTENT_TYPE = "application/json";
	
	public static final int PROFILE_PAGE = 1;
	public static final int HOUSEHOLD_PAGE = 2;
	public static final int INCOME_PAGE = 3;
	public static final int DEDUCTION_PAGE = 4;
	public static final int RESULTS_PAGE = 5;
	
	private PrescreenConstants(){
		
	}
}
