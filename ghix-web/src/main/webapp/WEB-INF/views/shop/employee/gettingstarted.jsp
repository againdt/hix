<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="gutter10">
  <div class="row-fluid">
        	<h1><spring:message  code="label.applyforempcov"/></h1>
      <div class="row-fluid">
        <div class="span3" id="sidebar">
          <!-- <h4 class="graydrkbg">Steps</h4>
          <ol class="nav nav-list">
	         <li class="active navmenu"><a href="#">Getting Started</a></li>
	         <li class="navmenu"><a href="#">Verify Information</a></li>
	         <li class="navmenu"><a href="#">Verify Dependents</a></li>
	         <li class="navmenu"><a href="#">Affordability Notice</a></li>
	         <li class="navmenu"><a href="#">Select a Plan</a></li>
	         <li class="navmenu"><a href="#">Sign Application</a></li>
          </ol> -->
          <jsp:include page="../../../layouts/employeeOpenEnrollSideBar.jsp" flush="true"></jsp:include>
        </div>
        <!-- /.sidebar-->
        
        <!-- Getting Started v1: Not previously Enrolled  -->
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
            <h4 class="graydrkbg gutter10"><spring:message  code="label.gettingstarted"/></h4>
                <div class="gutter10"> 
                	  <c:choose>
                	  	<c:when test="${fn:contains(invalidDate, true)}">
                	  		<%@include file="gsinactive.jsp" %>
                	  	</c:when>
                	  	<c:otherwise>
                	  		 <c:choose>
		                      	<c:when test="${employee.status == 'ENROLLED'}">
		                      		<%@include file="gsenrolled.jsp" %>
		                      	</c:when>
		                      	<c:when test="${employee.status == 'WAIVED'}">
		                      		<%@include file="gswaived.jsp" %>
		                      	</c:when>
		                      	<c:otherwise>
		                      		<%@include file="gsnotenrolled.jsp" %>
		                      	</c:otherwise>
		                      </c:choose>
                	  	</c:otherwise>
                	  </c:choose>
       			</div> <!--gutter10-->
            </div><!--row-fluid--> 
        </div><!--rightpanel--> 
      </div><!--row-fluid--> 
  </div> <!--main--> 
</div><!--gutter10-->

<script type="text/javascript">
$(document).ready(function(){
$('.info').tooltip();

$('i.icon-ok').parents('li').css('list-style','none');


});

</script>