import { OTHER_PROGRAMS_LIST } from '../constants/otherProgramsList';

export const getCoverageProgramName = programType => {
    return OTHER_PROGRAMS_LIST.find(pr => pr.type === programType).name;
};