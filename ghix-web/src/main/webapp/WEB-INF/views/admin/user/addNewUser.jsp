<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page import="java.util.Date" %>
<%@ page isELIgnored="false"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>

<%

String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>
 
<link rel="stylesheet" type="text/css" href="/hix/resources/css/datepicker.css">
<script type="text/javascript" src="/hix/resources/js/bootstrap-datepicker.js"></script>

	<c:url value="/account/signup/checkEmail" var="theUrltocheckEmail"></c:url>
<div class="row-fluid">
	<div class="gutter10">
			<h1 id="skip">Add New User</h1>
	</div>
</div>

<div class="row-fluid">
	<div class="span3" id="sidebar">
              <div class="header">
                  <h4>Accounts</h4>
              </div>
              
              <ul class="nav nav-list">
                      <li class="active"><a name="skip">Add User</a></li>
                      <li><a href="<c:url value="/admin/manage/users"/>">Manage User</a></li>
              </ul>
	</div>
	<!-- end of span3 -->
	
	<div class="span9" id="rightpanel">
          	<div class="header">
                  <h4 class="pull-left">Enter User Information</h4>
             </div>
		<div class="gutter10">
			<form class="form-horizontal" action="<c:url value="/admin/user/add"/>" method="POST" id="addUserDetails" name="addUserDetails">		
				<df:csrfToken/>
				<div class="control-group">
					<label for="role" class="control-label">Role<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label>
					<div class="controls">
						<select id="role" name="role" class="input-xlarge" onchange="roleRedirect(this)">
							<option value="dummy">Please select a Role</option>
							<c:forEach var="role" items="${roles}">
								<option value="<encryptor:enc value="${role.id}"/>"> ${role.label} </option>
							</c:forEach>
						</select>
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
					<label for="firstName" class="control-label">First Name<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<input type="text" name="firstName" id="firstName" class="input-xlarge" value="${manageUser.firstName}"  onkeypress="resetErrorMsg(this)"/>
						<div id="firstName_error" class="help-inline"></div>				
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
					<label for="lastName" class="control-label">Last Name<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<input type="text" name="lastName" id="lastName" class="input-xlarge" value="${manageUser.lastName}" onkeypress="resetErrorMsg(this)"/>
						<div id="lastName_error" class="help-inline"></div>						
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
					<label for="email" class="control-label">Email<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
					<div class="controls">
						<input type="text" name="email" id="email" class="input-xlarge" value="${manageUser.email}" onkeypress="resetErrorMsg(this)" />
						<div id="email_error" class="help-inline"></div>						
					</div>	<!-- end of controls-->
				</div>	<!-- end of control-group -->
				
				<div class="control-group">
				<label for="phone" class="control-label">Phone<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" aria-hidden="true"></label><!-- end of label -->
				<div class="controls">
					<input
							type="hidden" name="phone" value= "${user.phone}"
							id="phone" />
					<label for="phone1" class="hidden">&nbsp;</label>
								<input type="text" class="span2 inline" name="phone1" value="${manageUser.phone1}" id="phone1" maxlength="3"/>
								
								<label for="phone2" class="hidden">&nbsp;</label>
								<input type="text" class="span2 inline" name="phone2" value="${manageUser.phone2}" id="phone2" maxlength="3"/>
								
								<label for="phone3" class="hidden">&nbsp;</label>
								<input type="text" class="span2 inline" name="phone3"  value="${manageUser.phone3}" id="phone3" maxlength="4" /> 
						<div id="phone3_error"></div>
						<div id="phone_error" class="help-inline"></div>
				</div>				
				</div>	<!-- end of control-group -->
				
				
		<%-- 		<div class="control-group">
					<label for="startDate" class="required control-label">Start Date<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" 
					aria-required="false" aria-hidden="true"></label>
					<div class="controls">
						<div class="input-append date date-picker" id="date" >	
							<input class="span10"  type="text" name="startDate" id="startDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="<%= new Date() %>" timeZone="<%=timeZone%>" />" onkeypress="resetErrorMsg(this)" /> <span class="add-on"><i class="icon-calendar"></i></span>
							<div id="startDate_error" class="help-inline"></div>							
						</div>
					</div>
				</div>

				<div class="control-group">
					<label for="endDate" class="required control-label">End Date<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="Required!" 
					aria-required="false" aria-hidden="true"></label>
					<div class="controls">
						<div class="input-append date date-picker" id="date" >	
							<input class="span10 adj-width"  type="text" name="endDate" id="endDate" value="<fmt:formatDate pattern="MM/dd/yyyy" value="<%= new Date() %>" />" onkeypress="resetErrorMsg(this)" /> <span class="add-on"><i class="icon-calendar"></i></span>
							<div id="endDate_error" class="help-inline"></div>							
						</div>
					</div>
				</div> --%>
				
				<c:if test="${stateCode == 'MN'}">
					<div class="control-group">
						<label for="extnAppUserId" class="control-label">External App Id</label>
						<div class="controls">
							<input type="text" name="extnAppUserId" id="extnAppUserId" value="${manageUser.extnAppUserId}" />	
						</div>
					</div>
				</c:if>
				
				<div class="form-actions">
				<a href="<c:url value="/admin/manage/users"  />" id="cancelEditBtn" class="btn btn-medium"><spring:message  code="button.cancel"/></a>
				<input type="button" name="submitForm" id="submitForm" onClick="javascript:validateForm(this.form);" value="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send Email</c:otherwise></c:choose>" class="btn btn-primary" title="<c:choose><c:when test="${stateCode == 'MN'}">Save</c:when><c:otherwise>Save and Send Email</c:otherwise></c:choose>">
				</div>
		</form>
	</div>
</div><!--  end of span9 -->
</div><!-- end row-fluid -->


					 

<script type="text/javascript">

var theUrltocheckEmail = '<c:out value="${theUrltocheckEmail}"/>';

$(document).ready(function(){
	$('.date-picker').datepicker({
		format:"mm/dd/yyyy"
	});
	
});

function resetErrorMsg(element) {
	var elementId = element.id;
	
	$("#" + elementId + "_error").html('');
}


jQuery.validator.addMethod("validFirstNameCheck", function(value,
		element, param) {
	fName = $("#firstName").val();
	  var asciiReg = /^[a-zA-Z ,.'-]+$/;
	  if( !asciiReg.test(fName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});	

jQuery.validator.addMethod("validLastNameCheck", function(value,
		element, param) {
	lName = $("#lastName").val();
	 var asciiReg = /^[a-zA-Z ,.'-]+$/;
	  if( !asciiReg.test(lName ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});	

jQuery.validator.addMethod("validEmailCheck", function(value,
		element, param) {
	password = $("#email").val();
	  var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,25})?$/;
	  if( !emailReg.test(password ) ) {
		return false;
	  } else {
		return true;
	  }
	return false;
});	

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	 phone1 = $("#phone1").val(); 
	 phone2 = $("#phone2").val(); 
	 phone3 = $("#phone3").val(); 

	if( 
			(phone1 == "" || phone2 == "" || phone3 == "")  || 
			(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) ||  
			(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)
	  ){ 
		return false; 
	}
	var phone = $("#phone1").val() + $("#phone2").val() + $("#phone3").val();
	
	$("#phone").val(phone);
	return true;
});

jQuery.validator.addMethod("validDate", function(value, element, param) {
	var startdatevalue = $("#startDate").val();
	var enddatevalue = $("#endDate").val();
	if(Date.parse(startdatevalue) <= Date.parse(enddatevalue)){
		return true;
	}
	else{
		return false;
	}
	
	});


$("#addUserDetails")
.validate(
		{
			rules : {
				firstName : {
					required : true,
					validFirstNameCheck : true
				},
				lastName : {
					required : true,
					validLastNameCheck : true
				},
				email : {
					required : true,
					validEmailCheck : true
					},
				userName : {
					required : true
				},
				phone3 : {
					phonecheck : true
				},
				endDate :{
					required:true,
					validDate:true
					
				},
				startDate :{
					required:true,
					
				},
							
			},
			messages : {
				firstName : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstName' javaScriptEscape='true'/></span>",
					validFirstNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFirstNameCheck' javaScriptEscape='true'/></span>"
				},
				lastName : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateLastName' javaScriptEscape='true'/></span>" ,
					validLastNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateLastNameCheck' javaScriptEscape='true'/></span>"
				},
				email : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateEmail' javaScriptEscape='true'/></span>",
					validEmailCheck : "<span> <em class='excl'>!</em>Please provide valid email</span>"
					},
					userName : {
	 					required : "<span> <em class='excl'>!</em><spring:message code='label.userName' javaScriptEscape='true'/></span>"
	 					},
	 					phone3 : {
							phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
						},
				endDate: {
					required : "<span> <em class='excl'>!</em>End date required</span>",
					validDate:"<span> <em class='excl'>!</em>End date should be grater than start date</span>"
				},
				startDate: {
					required : "<span> <em class='excl'>!</em>Start date required</span>"
				}
	 					
			},
			onkeyup : false,
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				error
						.appendTo($("#" + elementId
								+ "_error"));
				$("#" + elementId + "_error").attr('class',
						'error help-inline');
			},
			
		});
		
function roleRedirect(select) {
    var selectedRole = select.options[select.selectedIndex].text;
    if(selectedRole == 'Issuer Representative' || selectedRole == 'Issuer Enrollment Representative') {
            document.location.href = "<c:url value='/admin/manageissuer'/>";
    }else if(selectedRole == 'Individual') {
    	document.location.href = "<c:url value='/cap/consumer/addconsumer'/>";
    }
}

function validateForm(form)
{
	checkExistingEmail(form);
}

function isInvalidCSRFToken(xhr) {
    var rv = false;
    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
    	   alert($('Session is invalid').text());
           rv = true;
    }
    return rv;
}
function checkExistingEmail(form)
{
	if(form.role.selectedIndex == 0) {
        alert("Please select role");
		return false;
    }
	var validateUrl = theUrltocheckEmail;//"<c:url value='/account/signup/checkEmail'/>";
	var csrftoken = '${sessionScope.csrftoken}';
	
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			email : $("#email").val(), csrftoken : csrftoken
		},
		success: function(response)
		{
			if(isInvalidCSRFToken(response))                                  
                return; 
			if(!response)
			{
				error = "<label class='error' generated='true'><span> <em class='excl'>!</em><spring:message code='label.validateEmailCheck2' javaScriptEscape='true'/></span></label>";
				$('#email_error').html(error);
				return false;
			}
			else
			{
				$("#addUserDetails").submit();
			}
		},
		
	});
}
</script>