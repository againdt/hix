package com.getinsured.eligibility.at.ref.service;

import com.getinsured.iex.ssap.model.SsapApplication;

public interface ReferralActivationNotificationService {
	
	public String triggerQEAutoLinkingAccountActivation(long ssapAppId) throws Exception;
	
	public String triggerOEAutoLinkingAccountActivation(long ssapAppId) throws Exception;
	
	public void triggerQEWithoutAutoLinkingAccountActivation(SsapApplication ssapApplication) throws Exception;

	public void triggerOEAccountActivation(SsapApplication ssapApplication) throws Exception;
	
	public void initiateActivation(Long applicationId);
	
}
