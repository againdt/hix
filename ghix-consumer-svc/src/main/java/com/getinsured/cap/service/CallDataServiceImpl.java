package com.getinsured.cap.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.consumer.repository.ICallDataRepository;
import com.getinsured.hix.dto.calldata.CallDataIncomingRequestDTO;
import com.getinsured.hix.dto.calldata.CallDataModifiedRequestDTO;
import com.getinsured.hix.model.calldata.CallDataRequestPayload;
import com.getinsured.hix.platform.security.service.UserService;
import com.google.gson.Gson;

@Service("callDataService")
public class CallDataServiceImpl implements CallDataService {
	@Autowired private Gson platformGson;
	@Autowired ICallDataRepository iCallDataRepository;
	@Autowired UserService userServiceImpl;
	public static final String emailAddress = "exadmin@ghix.com";
	
	@Override
	public String modifyAndSaveRequest(String callDataRequestObjJson) throws Exception {
		String status = "FAIL";
		try{
			CallDataIncomingRequestDTO callDataIncomingRequest = platformGson.fromJson(callDataRequestObjJson,CallDataIncomingRequestDTO.class);
			CallDataModifiedRequestDTO modifiedCallDataRequest = this.populateCallDataModifedRequest(callDataIncomingRequest);
			
			CallDataRequestPayload callDataDetails = new CallDataRequestPayload();
			callDataDetails.setModifiedRequest(modifiedCallDataRequest);
			int userId = userServiceImpl.getSuperUserId(emailAddress);
			callDataDetails.setUpdatedBy(userId);
			callDataDetails.setCreatedBy(userId);
			
			CallDataRequestPayload callDataRequestPayload = iCallDataRepository.save(callDataDetails);
			iCallDataRepository.updateRawRequest(callDataRequestObjJson, callDataRequestPayload.getId());
			status = "SUCCESS";
		}
		catch(Exception e){
			throw e;
		}
		
		return status;
	}

	@Override
	public Boolean doesInteractionIdExistInDb(String rawRequestJson) {
		CallDataIncomingRequestDTO callDataIncomingRequest = platformGson.fromJson(rawRequestJson,CallDataIncomingRequestDTO.class);
		int iTotalRecords = iCallDataRepository.findByInteractionId(callDataIncomingRequest.getInteraction_id());
		if(iTotalRecords > 0){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
	
	private CallDataModifiedRequestDTO populateCallDataModifedRequest(CallDataIncomingRequestDTO callDataIncomingRequest) throws ParseException {
		CallDataModifiedRequestDTO modifiedRequest = new CallDataModifiedRequestDTO();


		modifiedRequest.setInteraction_id(callDataIncomingRequest.getInteraction_id());
		modifiedRequest.setClient_id(callDataIncomingRequest.getClient_id());
		modifiedRequest.setClient(callDataIncomingRequest.getClient_name());
		modifiedRequest.setProgram_id(callDataIncomingRequest.getProgram_id());
		modifiedRequest.setProgram_name(callDataIncomingRequest.getProgram_name());
		modifiedRequest.setCampaign_id(callDataIncomingRequest.getCampaign_id());
		modifiedRequest.setCampaign_name(callDataIncomingRequest.getCampaign_name());
		modifiedRequest.setDim_aff_affiliate_key(callDataIncomingRequest.getAffiliateID());
		modifiedRequest.setDim_aff_flow_key(callDataIncomingRequest.getFlowID());
		modifiedRequest.setPhone_number(callDataIncomingRequest.getDnis());
		modifiedRequest.setCaller_ani(callDataIncomingRequest.getCaller_ani());
		modifiedRequest.setLast_agent_id(callDataIncomingRequest.getLast_agent_id());
		modifiedRequest.setLast_agent_full_name(callDataIncomingRequest.getLast_agent_full_name());
		modifiedRequest.setAgent_count(callDataIncomingRequest.getAgent_count());
		modifiedRequest.setRing_time(callDataIncomingRequest.getAgent_ring_time_in_secs());
		modifiedRequest.setHold_length(callDataIncomingRequest.getCaller_hold_length_in_secs());
		modifiedRequest.setQueue_length_in_secs(callDataIncomingRequest.getCaller_queue_length_in_secs());
		modifiedRequest.setCaller_talk_time(callDataIncomingRequest.getCaller_talk_time_in_secs());
		modifiedRequest.setAgent_talk_time_seconds(callDataIncomingRequest.getAgent_talk_time_in_secs());
		modifiedRequest.setAfter_call_work_time(callDataIncomingRequest.getAgent_after_call_work_time_in_secs());
		modifiedRequest.setCall_length(callDataIncomingRequest.getTotal_caller_time_in_secs());
		modifiedRequest.setAgent_total_time_seconds(callDataIncomingRequest.getTotal_agent_time_secs());
		modifiedRequest.setState_code(callDataIncomingRequest.getState_code());
		modifiedRequest.setIn_business_hours(callDataIncomingRequest.getAfter_hours());
		modifiedRequest.setIs_voicemail(callDataIncomingRequest.getVoicemail());
		modifiedRequest.setIs_abandoned(callDataIncomingRequest.isAbandoned());
		modifiedRequest.setIs_external_transfer(callDataIncomingRequest.isExternal_transfer_call());
		modifiedRequest.setLast_segment_call_dispostion(callDataIncomingRequest.getFinal_disposition());
		
    	/*
    	 * Change in out bound dial out details
	    	//Incoming"" --> INBOUND
	    	//Campaign"" --> OUTBOUND
	    	//OutgoingExternal"" --> DIALOUT"
    	 * 
    	 * */
    	
		
    	if(StringUtils.isNotEmpty(callDataIncomingRequest.getIn_outbound_dialout())){
    		String inOutBoundDialOut = callDataIncomingRequest.getIn_outbound_dialout().toUpperCase();
    		switch (inOutBoundDialOut) {
			case "INCOMING":
				modifiedRequest.setIn_outbound_dialout("INBOUND");
				break;
			case "CAMPAIGN":
				modifiedRequest.setIn_outbound_dialout("OUTBOUND");
				break;
			case "OUTGOINGEXTERNAL":
				modifiedRequest.setIn_outbound_dialout("DIALOUT");
				break;
			default:
				break;
			}
    	}
    	
    	/*
    	 * Modify date formats for 
    	 * call start time 
    	 * call end time.
    	 * 
    	 * */
    	
    	final String REQUEST_DATE_FORMAT = "M/dd/yyyy hh:mm:ss a";
		final String REQUIRED_DATE_FORMAT= "yyyy-M-dd hh:mm:ss";
		DateFormat utcFormat = new SimpleDateFormat(REQUEST_DATE_FORMAT);
		DateFormat requiredDateFormat = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		Date dt=null;
		String callTime=null;
		if(StringUtils.isNotEmpty(callDataIncomingRequest.getCall_start_time())){
			dt = utcFormat.parse(callDataIncomingRequest.getCall_start_time());
			callTime = requiredDateFormat.format(dt);
			modifiedRequest.setStart_time(callTime);	
		}
		
		if(StringUtils.isNotEmpty(callDataIncomingRequest.getCall_end_time())){
			dt = utcFormat.parse(callDataIncomingRequest.getCall_end_time());
			callTime = requiredDateFormat.format(dt);
			modifiedRequest.setEnd_time(callTime);
		}
		
    	return modifiedRequest;
	}
}
