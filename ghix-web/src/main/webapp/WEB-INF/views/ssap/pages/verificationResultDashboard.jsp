<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<script src="<c:url value="/resources/js/cap/moment.min.js"/>"></script>
<c:url value="/ssap/verifyApplicantDocCategory" var="theUrl"></c:url>
<c:url value="/ssapdocumentmgmt/documents/uploadDocumentAttachmentsAjax" var="theUrlSecond">
	<c:param name="${df:csrfTokenParameter()}">
		<df:csrfToken plainToken="true" />
	</c:param>
</c:url>
<input id="subUrl" type="hidden" value="<c:out value="${theUrl}"/>">
<input id="documentUploadAjaxUrl" type="hidden" value="<c:out value="${theUrlSecond}"/>">
<input id="tokid" type="hidden" value="${sessionScope.csrftoken}">
<input id="webJson" type="hidden" value='${MAIN_APPLICANT}'>

<div class="gutter10" ng-app="verificationResultDashboardApp" ng-controller="verificationResultDashboardController as vm" ng-init="vm.init()" ng-cloak>
    <div spinning-loader="vm.loader"></div>
    <div class="margin20-b">
        <a href="<c:url value="/indportal" />"><i class="icon-chevron-sign-left"></i> Back to Dashboard</a>
    </div>
    <!-- verificationResults -->
    <div class="verificationResults" >
        <div class="row-fluid">
            <div class="header">
                <h4>Documents for This Household</h4>
            </div>
            <div class="gutter10">
                <div class="tabbable tabbable-header margin20-t">
                    <ul class="nav nav-tabs">
                        <li class="active" ng-if="vm.webJson.qualifiedLifeEvents !== undefined && vm.webJson.qualifiedLifeEvents.validationStatus !== 'INITIAL' && vm.webJson.qualifiedLifeEvents.validationStatus !== 'NOTREQUIRED' && vm.webJson.qualifiedLifeEvents.validationStatus !== null">
                            <a href="#qualifiedLifeEventsTab" data-toggle="tab">Qualified Life Events</a>
                        </li>
                        <li ng-if="vm.stateCode !== 'MN'" ng-class="{'active': vm.webJson.qualifiedLifeEvents === undefined || vm.webJson.qualifiedLifeEvents.validationStatus === 'INITIAL' || vm.webJson.qualifiedLifeEvents.validationStatus === 'NOTREQUIRED' || vm.webJson.qualifiedLifeEvents.validationStatus === null}">
                            <a href="#applicantsTab" data-toggle="tab">Applicant Verifications</a>
                        </li>
                        <li>
                            <a href="#otherDocumentsTab" data-toggle="tab" ng-if="vm.webJson.authorizedRepresentativeIndicator === true">Other Documents</a>
                        </li>
                    </ul>
                </div>
                <div class="tab-content tabbable-content">
                    <div class="tab-pane active" id="qualifiedLifeEventsTab" ng-if="vm.showPage && vm.webJson.qualifiedLifeEvents !== undefined && vm.webJson.qualifiedLifeEvents.validationStatus !== 'INITIAL' && vm.webJson.qualifiedLifeEvents.validationStatus !== 'NOTREQUIRED' && vm.webJson.qualifiedLifeEvents.validationStatus !== null" ng-cloak>
                        <div class="tabbable tabs-left">
                            <ul class="nav nav-tabs">
                                <li ng-repeat="applicant in vm.webJson.qualifiedLifeEvents.applicants" class="ng-cloak" ng-class="{'active': $index === 0}" ng-cloak>
                                    <a href="#qleApplicant{{$index}}" data-toggle="tab">
                                    {{applicant.firstName}} {{applicant.middleName}} {{applicant.lastName}}
                                    <i class="icon-ok-sign" ng-if="applicant.isAllEventsVerified === true"></i>
                                    <i class="icon-exclamation-sign" ng-if="applicant.isAllEventsVerified === false"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <p class="alert alert-info margin20-t">
                                    For document requirements and timelines, click
                                   <strong ng-switch="vm.stateCode">
										<a ng-switch-when='MN' href="https://www.mnsure.org/new-customers/enrollment-deadlines/special-enrollment/" target='_blank' class="link">here</a>
										<a ng-switch-when='NV' href="https://help.nevadahealthlink.com/hc/en-us/articles/360030282931-Submitting-Documents" target='_blank' class="link">here</a>
										<a ng-switch-default href="https://www.yourhealthidaho.org/special-enrollment" target='_blank' class="link">here</a>
									</strong>
                                </p>
                                <div class="tab-pane" ng-class="{'active': thirdIndex === 0}" id="qleApplicant{{thirdIndex}}" ng-repeat="applicant in vm.webJson.qualifiedLifeEvents.applicants" ng-init="thirdIndex = $index">
                                    <div class="accordion" id="qleAccordion{{thirdIndex}}">
                                        <div class="accordion-group margin20-t" ng-repeat="event in applicant.events" ng-init="fourthIndex = $index">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle accordion-toggle-verification" ng-class="event.isOverrideButtonShown? 'accordion-toggle-verification-max-width':''" data-toggle="collapse" data-parent="#qleAccordion{{thirdIndex}}" href="#qleCollapse{{thirdIndex}}{{fourthIndex}}" collapse-icon-plus>
                                                    <div ng-if="event.validationStatus === 'PENDING' || event.validationStatus === 'SUBMITTED' || event.validationStatus === 'REJECTED'" style="display: inline-block;">
                                                        <i class="icon-plus-sign"></i>
                                                    </div>
                                                    {{event.eventLabel}}
                                                    <div ng-if="event.validationStatus === 'PENDING' || event.validationStatus === 'SUBMITTED' || event.validationStatus === 'REJECTED'" style="display: inline-block">
                                                        <small>(Not Verified)</small>
                                                        <spring:message code="help.cap.upload.global" />
                                                    </div>
                                                    <div ng-if="event.validationStatus === 'CANCELLED' || event.validationStatus === 'VERIFIED' || event.validationStatus === 'OVERRIDDEN'" style="display: inline-block;">
                                                        <small>(Verified)</small> <i class="icon-ok-sign"></i>
                                                    </div>
                                                </a>
                                                <sec:accesscontrollist hasPermission="IND_PORTAL_CSR_OVERRIDES" domainObject="user">
                                                    <span class="pull-right margin10-r" ng-if="event.validationStatus !== 'VERIFIED' && event.validationStatus !== 'CANCELLED' && event.validationStatus !== 'OVERRIDDEN'">
                                                    <a href="#" data-toggle="modal" ng-init="event.isOverrideButtonShown = true" class="btn btn-small btn-danger" ng-click="vm.showQleVerificationModal(event, applicant)">
                                                    <i class='icon-edit-sign'></i> Override
                                                    </a>
                                                    </span>
                                                </sec:accesscontrollist>
                                            </div>
                                            <div id="qleCollapse{{thirdIndex}}{{fourthIndex}}" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <p class="alert alert-info margin10-t" ng-if="event.validationStatus === 'REJECTED'">One or more of your documents was invalid.</p>
                                                    <p class="alert alert-info margin10-t" ng-if="event.validationStatus === 'SUBMITTED'">Please allow 30 business days for your documents to be reviewed. Notification will be sent when a determination is made.</p>
                                                    <table class="table table-condensed margin20-t" ng-if="event.uploadedDocumentList.length > 0">
                                                        <thead>
                                                            <tr>
                                                                <th>Document Name</th>
                                                                <!-- <th>Document Type</th> -->
                                                                <th>Document Status</th>
                                                                <th>Uploaded Date</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="documentStatus in event.uploadedDocumentList | orderBy: 'documentDate'">
                                                                <td>{{documentStatus.documentName}}</td>
                                                                <!-- <td>{{documentStatus.documentType}}</td> -->
                                                                <td>{{documentStatus.documentStatus}}</td>
                                                                <td>{{documentStatus.documentDate.split(' ')[0]}}</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                    <form class="form-horizontal margin30-t" ng-if="vm.stateCode !== 'MN' && (event.validationStatus === 'PENDING' || event.validationStatus === 'REJECTED')">
                                                        <div class="control-group" ng-form="qleDocumentUploadForm">
                                                            <label for="qleFileUpload{{thirdIndex}}{{fourthIndex}}" class="required control-label">Choose Document to Upload:</label>
                                                            <div class="controls">
                                                                <label class="btn btn-small" for="qleFileUpload{{thirdIndex}}{{fourthIndex}}">
                                                                <input id="qleFileUpload{{thirdIndex}}{{fourthIndex}}" name="file" type="file" class="hide" max-file-size="10485760" file-model-binding="vm.qleFiles[thirdIndex + '_' + fourthIndex]" ng-model="vm.qleFiles[thirdIndex + '_' + fourthIndex]" required>
                                                                Choose File
                                                                </label>
                                                                <span>{{vm.qleFiles[thirdIndex + '_' + fourthIndex].name}}</span>
                                                                <span class="error-message" ng-if="qleDocumentUploadForm.file.$error.maxFileSize && qleDocumentUploadForm.file.$dirty">
                                                                Please upload file that is less than 10MB in size.
                                                                </span>
                                                            </div>
                                                            <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitQleDoc(event, thirdIndex, fourthIndex)" ng-disabled="qleDocumentUploadForm.$invalid">Submit</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="applicantsTab" ng-class="{'active': vm.webJson.qualifiedLifeEvents === undefined || vm.webJson.qualifiedLifeEvents.validationStatus === 'INITIAL' || vm.webJson.qualifiedLifeEvents.validationStatus === 'NOTREQUIRED' || vm.webJson.qualifiedLifeEvents.validationStatus === null}">
                        <div class="tabbable tabs-left">
                            <ul class="nav nav-tabs applicant-list">
                                <li ng-class="{'active': $index === 0}" ng-repeat='applicant in vm.webJson.mainApplicantList'>
                                    <a href="#applicant{{$index}}" ng-click="vm.getLegalPresenceVerification(applicant)" data-toggle="tab">
                                    {{applicant.applicantDisplayName}}
                                    <i class="icon-ok-sign" ng-if="applicant.isDocumentCategoryVerified"></i>
                                    <i class="icon-exclamation-sign" ng-if="!applicant.isDocumentCategoryVerified"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" ng-class="{'active': firstIndex === 0}" id="applicant{{firstIndex}}" ng-repeat="applicant in vm.webJson.mainApplicantList" ng-init="firstIndex = $index">
                                    <div class="accordion" id="accordion{{firstIndex}}">
                                        <div class="accordion-group margin20-t" ng-repeat="(key, value) in applicant.documentCategoryStatusMap" ng-class="{'margin40-t': secondIndex === 0}" ng-init="secondIndex = $index">
                                            <div class="accordion-heading">
                                                <a class="accordion-toggle accordion-toggle-verification" ng-class="value.isOverrideButtonShown? 'accordion-toggle-verification-max-width':''" data-toggle="collapse" data-parent="#accordion{{firstIndex}}" href="#collapse{{firstIndex}}{{secondIndex}}" collapse-icon-plus>
                                                    <span ng-if="value.isDocumentCategoryVerified === false">
                                                        <i class="icon-plus-sign"></i>
                                                    </span>
                                                    {{key}}
                                                </a>
                                                <div ng-if="value.isDocumentCategoryVerified === false" style="display: inline-block;">
                                                    <small>(Not Verified)</small>
                                                    <spring:message code="help.cap.upload.global"/>
                                                </div>
                                                <span ng-if="value.isDocumentCategoryVerified === true">
                                                    <small>(Verified)</small> <i class="icon-ok-sign"></i>
                                                </span>
                                                <sec:accesscontrollist hasPermission="IND_PORTAL_CSR_OVERRIDES" domainObject="user">
                                                    <span class="pull-right margin10-r" ng-if="value.isDocumentCategoryVerified !== true">
                                                        <a href="#" data-toggle="modal" ng-init="value.isOverrideButtonShown = true" class="btn btn-small btn-danger" ng-click="vm.showVerificationModal(key, applicant)">
                                                            <i class='icon-edit-sign'></i> Override
                                                        </a>
                                                    </span>
                                                </sec:accesscontrollist>
                                            </div>
                                            <div id="collapse{{firstIndex}}{{secondIndex}}" class="accordion-body collapse">
                                                <div class="accordion-inner">
                                                    <table class="table table-condensed margin20-t" ng-if="value.documentCategoryDtlList.length > 0 && key !== 'Legal Presence'">
                                                        <thead>
                                                            <tr>
                                                                <th>Document Name</th>
                                                                <th>Document Type</th>
                                                                <th>Document Status</th>
                                                                <sec:accesscontrollist hasPermission="TKM_VIEW_TICKET" domainObject="user">
                                                                    <th>Ticket</th>
                                                                </sec:accesscontrollist>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <tr ng-repeat="documentStatus in value.documentCategoryDtlList">
																	<td ng-if="documentStatus.showDocument === false">
                                                                        {{documentStatus.consumerDocument.documentName}}
                                                                    </td>
																	<td ng-if="documentStatus.showDocument === true">
																		<a style='cursor: pointer;' href="javascript:void(0);" ng-click="vm.showDetail(documentStatus.encryptedDocPath)" ng-cloak>
																			{{documentStatus.consumerDocument.documentName}}
																		</a>
																	</td>
																	<td>{{documentStatus.consumerDocument.documentType}}</td>
																	<td>{{documentStatus.consumerDocument.accepted}}</td>
                                                                    <sec:accesscontrollist hasPermission="TKM_VIEW_TICKET" domainObject="user">
                                                                        <td>
                                                                            <a style='cursor: pointer;' href="javascript:void(0);" ng-click="vm.switchCSR(documentStatus.encryptedID)" ng-cloak>
                                                                                {{documentStatus.ticketNumber}}
                                                                            </a>
                                                                        </td>
                                                                    </sec:accesscontrollist>
																</tr>
															</tbody>
                                                    </table>
                                                    <form class="form-horizontal" name="documentUploadForm{{firstIndex}}{{secondIndex}}" ng-if="value.isDocumentCategoryVerified === false">
                                                        <input type="hidden" name="applicantionID" value="{{vm.webJson.applicationID}}" ng-model="docForm.applicantionID" ng-init="docForm.applicantionID = vm.webJson.applicationID">
                                                        <input type="hidden" name="case_number" value="{{vm.webJson.caseId}" ng-model="docForm.case_number">
                                                        <input type="hidden" name="applicantID" value="{{applicant.applicantID}}" ng-model="docForm.applicantID">
                                                        <input type="hidden" name="documentCategory" value="{{applicant.documentCategoryName}}" ng-model="docForm.documentCategory">
                                                        <input type="hidden" name="csrftoken" value="${sessionScope.csrftoken}" ng-model="docForm.csrftoken">
                                                        <div ng-if="(key !== 'Legal Presence')">
                                                            <div class="form-group margin10-t">
                                                                <label for="documentType" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">Select Document Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                    <select class="input-xlarge" name="documentType" id="documentType" required ng-model="vm.documentTypes[firstIndex + '_' + secondIndex]" ng-options="documentType for documentType in value.documentTypeList" ng-init="vm.documentTypes[firstIndex + '_' + secondIndex] = value.documentTypeList[0]"></select>
                                                                </div>
                                                            </div>
                                                            <div class="form-group" ng-form="documentUploadForm">
                                                                <label for="fileUpload{{firstIndex}}{{secondIndex}}" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" >Choose Document to Upload</label>
                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                    <label class="btn btn-small" for="fileUpload{{firstIndex}}{{secondIndex}}">
                                                                        <input id="fileUpload{{firstIndex}}{{secondIndex}}" name="file" type="file" class="hide" max-file-size="10485760" file-model-binding="vm.files[firstIndex + '_' + secondIndex]" ng-model="vm.files[firstIndex + '_' + secondIndex]" required>
                                                                        Choose File
                                                                    </label>
                                                                    <span>{{vm.files[firstIndex + '_' + secondIndex].name}}</span>
                                                                    <div class="help-line margin10-t">
                                                                        <span class="error-message" ng-if="documentUploadForm.file.$error.maxFileSize && documentUploadForm.file.$dirty">
                                                                            Please upload file that is less than 10MB in size.
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitDoc(applicant.applicantID, value, firstIndex, secondIndex)" ng-disabled="documentUploadForm.$invalid">Submit</button>
                                                            </div>
                                                        </div>
                                                        <div ng-if="(key === 'Legal Presence') && value.isInSubmitted !== true" ng-switch on="vm.actionResult">
                                                            <div ng-switch-when="RESUBMIT_WITH_SEVISID">
                                                                <div class="alert alert-error">
                                                                    Our records indicate that you are a Student or Foreign Exchange Visitor.
                                                                    Please provide your Student and Exchange Visitor Information System ID (SEVIS).
                                                                </div>
                                                                <div class="form-group" ng-form="resubmitWithSevisIdForm">
                                                                    <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="sevisId_{{firstIndex}}{{secondIndex}}"> SEVIS ID Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                        <input type="text" id="sevisId_{{firstIndex}}{{secondIndex}}" name="sevisId" required ng-pattern="/^[0-9]/" ng-minlength="10" ng-maxlength="10" min="10" max="10" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.sevisId">
                                                                        <div class="help-line margin10-t">
                                                                            <span class="error-message" ng-if="resubmitWithSevisIdForm.sevisId.$error.required || resubmitWithSevisIdForm.sevisId.$error.pattern || resubmitWithSevisIdForm.sevisId.$error.minlength || resubmitWithSevisIdForm.sevisId.$error.maxlength">
                                                                                <em class="excl"></em>Please enter the SEVIS ID number. This always starts with an N followed by 10 numeric digits. This is usually located at the top, right-hand side of the document.  When entering a SEVIS Number, remove the leading N and enter the 10 digits, including the leading 0s.
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID)" ng-disabled="resubmitWithSevisIdForm.$invalid">Submit</button>
                                                            </div>
                                                            <div ng-switch-when="REVERIFY">
                                                                 <div ng-switch on="vm.documentType[0]">
<%--                                                                 <div ng-switch on="true">--%>
                                                                    <div class="alert alert-error">
                                                                        Record not found. Please verify information and resubmit.
                                                                    </div>
                                                                    <div ng-switch-when="I327DocumentID" ng-form="i327Form">
                                                                        <p>Reentry Permit (I-327)</p>

                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="reentry_alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="reentry_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i327Form.alienNumber.$error.required || i327Form.alienNumber.$error.pattern || i327Form.alienNumber.$error.minlength || i327Form.alienNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="reentry_expiration_date{{firstIndex}}{{secondIndex}}">Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="reentry_expiration_date{{firstIndex}}{{secondIndex}}"  name="expirationDate" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i327Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="I551DocumentID" ng-form="i551Form">
                                                                        <p>Permanent Resident Card "Green Card", I-551</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                 <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i551Form.alienNumber.$error.required || i551Form.alienNumber.$error.pattern || i551Form.alienNumber.$error.minlength || i551Form.alienNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="card_number{{firstIndex}}{{secondIndex}}"> Card Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="card_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[a-zA-Z]{3}[0-9]/" ng-minlength="13" ng-maxlength="13" name="cardNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.cardNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i551Form.passportNumber.$error.required || i551Form.passportNumber.$error.pattern || i551Form.passportNumber.$error.minlength || i551Form.passportNumber.$error.maxlength">
                                                                                        <em class="excl"></em> Please enter the Card Number. Card Number is 13 characters long, with the first 3 characters alpha and the remaining 10 characters numeric.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="expiration_date{{firstIndex}}{{secondIndex}}">Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="expiration_date{{firstIndex}}{{secondIndex}}"  name="expirationDate" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i551Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="I571DocumentID" ng-form="i571Form">
                                                                        <p>Refugee Travel Document (I-571) I571DocumentID</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="refugee_alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="refugee_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                     <span class="error-message" ng-if="i571Form.alienNumber.$error.required || i571Form.alienNumber.$error.pattern || i571Form.alienNumber.$error.minlength || i571Form.alienNumber.$error.maxlength">
                                                                                         <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                     </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="refugee_doc_expiration_date{{firstIndex}}{{secondIndex}}">Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="refugee_doc_expiration_date{{firstIndex}}{{secondIndex}}"  name="refugee_doc_expiration_date{{firstIndex}}{{secondIndex}}" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i571Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="I766DocumentID" ng-form="i766Form">
                                                                        <p>Employment Authorization Card (EAD, I-766)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="ead_alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="ead_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                      <span class="error-message" ng-if="i766Form.alienNumber.$error.required || i766Form.alienNumber.$error.pattern || i766Form.alienNumber.$error.minlength || i766Form.alienNumber.$error.maxlength">
                                                                                         <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.. When entering a Alien Number, remove the leading A and enter the 9 digits, including the leading 0s.
                                                                                     </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="ead_card_number{{firstIndex}}{{secondIndex}}"> Card Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="ead_card_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[A-Za-z]{3}[0-9]/" ng-minlength="13" ng-maxlength="13" name="cardNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.cardNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i766Form.cardNumber.$error.required || i766Form.cardNumber.$error.pattern || i766Form.cardNumber.$error.minlength || i766Form.cardNumber.$error.maxlength">
                                                                                        <em class="excl"></em> Please enter the Card Number. Card Number is 13 characters long, with the first 3 characters alpha and the remaining 10 characters numeric.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="ead_passport_expiration_date{{firstIndex}}{{secondIndex}}">Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="ead_passport_expiration_date{{firstIndex}}{{secondIndex}}" required ng-required name="expirationDate" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i766Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="MacReadI551DocumentID" ng-form="macReadi551Form">
                                                                        <p>Machine Readable Immigrant Visa (With Temporary I-551 Language)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="mriv_alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="mriv_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="macReadi551Form.alienNumber.$error.required || macReadi551Form.alienNumber.$error.pattern || macReadi551Form.alienNumber.$error.minlength || macReadi551Form.alienNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="mriv_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="mriv_passport_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" name="passportNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="macReadi551Form.passportNumber.$error.required || macReadi551Form.passportNumber.$error.pattern || macReadi551Form.passportNumber.$error.minlength || macReadi551Form.passportNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                             <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="mriv_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select id="mriv_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="foreignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                    <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                </select>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="macReadi551Form.foreignPassportIssuance.$dirty && macReadi551Form.foreignPassportIssuance.$error.required">
                                                                                        <em class="excl"></em>Please select a country from which your passport was issued.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label required" for="mriv_passport_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="mriv_passport_expiration_date{{firstIndex}}{{secondIndex}}"  name="mriv_passportExpirationDate{{firstIndex}}{{secondIndex}}" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="mriv_visa_number{{firstIndex}}{{secondIndex}}"> Visa Number</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="mriv_visa_number{{firstIndex}}{{secondIndex}}" name="visaNumber" ng-pattern="/^[A-Za-z0-9]/"  ng-minlength="8" ng-maxlength="8" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.cardNumber">
                                                                            </div>
                                                                            <div class="help-line margin10-t">
                                                                                <span class="error-message" ng-if="macReadi551Form.visaNumber.$error.pattern || macReadi551Form.visaNumber.$error.minlength || macReadi551Form.visaNumber.$error.maxlength">
                                                                                    <em class="excl"></em>Please enter your visa number, usually printed in red at the bottom right side of the document. It contains 8 alphanumeric characters.
                                                                                </span>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="macReadi551Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="TempI551DocumentID" ng-form="tempi551Form">
                                                                        <p>Temporary I−551 Stamp (on passport or I−94, I−94A)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="temp_alien_number{{firstIndex}}{{secondIndex}}">Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="temp_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="tempi551Form.alienNumber.$error.required || tempi551Form.alienNumber.$error.pattern || tempi551Form.alienNumber.$error.minlength || tempi551Form.alienNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="temp_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="temp_passport_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" name="passportNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="tempi551Form.passportNumber.$error.required || tempi551Form.passportNumber.$error.pattern || tempi551Form.passportNumber.$error.minlength || tempi551Form.passportNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                             <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="temp_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select id="temp_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="foreignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                    <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                </select>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="tempi551Form.foreignPassportIssuance.$dirty && tempi551Form.foreignPassportIssuance.$error.required">
                                                                                        <em class="excl"></em>Please select a country from which your passport was issued.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="temp_passport_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="temp_passport_expiration_date{{firstIndex}}{{secondIndex}}" name="passportExpirationDate" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="tempi551Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="I94DocumentID" ng-form="i94Form">
                                                                        <p>Arrival/Departure Record (I-94, I-94A)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_number{{firstIndex}}{{secondIndex}}">I-94 Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="i94_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="11" ng-maxlength="11" name="adi94Number" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.i94Number" >
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94Form.adi94Number.$error.required || i94Form.adi94Number.$error.pattern || i94Form.adi94Number.$error.minlength || i94Form.adi94Number.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_sevis_number{{firstIndex}}{{secondIndex}}">SEVIS ID Number</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="i94_sevis_number{{firstIndex}}{{secondIndex}}" name="i94sevisNumber" ng-pattern="/^[0-9]/" ng-minlength="10" ng-maxlength="10"  ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.sevisId">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94Form.i94sevisNumber.$error.pattern || i94Form.i94sevisNumber.$error.minlength || i94Form.i94sevisNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_passport_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="i94_passport_expiration_date{{firstIndex}}{{secondIndex}}" name="passportExpirationDate" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i94Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="I94UnexpForeignPassportDocumentID" ng-form="i94UnexpForeignPassportForm">
                                                                        <p>Arrival/Departure Record in Foreign Passport (I-94)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_number{{firstIndex}}{{secondIndex}}"> I-94 Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="i94_foreign_number{{firstIndex}}{{secondIndex}}" name="adfi94foreignNumber" required ng-pattern="/^[a-zA-Z0-9]/" ng-minlength="11" ng-maxlength="11" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.i94Number">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.adfi94Number.$error.required || i94UnexpForeignPassportForm.adfi94Number.$error.pattern || i94UnexpForeignPassportForm.adfi94Number.$error.minlength || i94UnexpForeignPassportForm.adfi94Number.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="i94_foreign_passport_number{{firstIndex}}{{secondIndex}}" name="adfPassportNumber" required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.adfPassportNumber.$error.required || i94UnexpForeignPassportForm.adfPassportNumber.$error.pattern || i94UnexpForeignPassportForm.adfPassportNumber.$error.minlength || i94UnexpForeignPassportForm.adfPassportNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                             <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select id="i94_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="adfi94ForeignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                    <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                </select>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.adfi94ForeignPassportIssuance.$dirty && i94UnexpForeignPassportForm.adfi94ForeignPassportIssuance.$error.required">
                                                                                        <em class="excl"></em>Please select the country from which your passport was issued.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_passport_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="i94_foreign_passport_expiration_date{{firstIndex}}{{secondIndex}}" required name="i94_foreign_passport_expiration_date" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.i94_foreign_passport_expiration_date.$dirty && i94UnexpForeignPassportForm.i94_foreign_passport_expiration_date.$error.required">
                                                                                        <em class="excl"></em>Please enter your passport expiration date.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_visa_number{{firstIndex}}{{secondIndex}}"> Visa Number</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="i94_foreign_visa_number{{firstIndex}}{{secondIndex}}" name="visaNumber" ng-pattern="/^[A-Za-z0-9]/" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.visaNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.visaNumber.$error.pattern || i94UnexpForeignPassportForm.visaNumber.$error.minlength || i94UnexpForeignPassportForm.visaNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your visa number, usually printed in red at the bottom right side of the document. It contains 8 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i94_foreign_sevis_number{{firstIndex}}{{secondIndex}}">SEVIS ID Number</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="i94_foreign_sevis_number{{firstIndex}}{{secondIndex}}" name="sevisid" ng-pattern="/^[0-9]/" ng-minlength="10" ng-maxlength="10" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.sevisId">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="i94UnexpForeignPassportForm.sevisid.$error.pattern || i94UnexpForeignPassportForm.sevisid.$error.minlength || i94UnexpForeignPassportForm.sevisid.$errormaxlength">
                                                                                        <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i94UnexpForeignPassportForm.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-when="UnexpForeignPassportDocumentID" ng-form="unExpForeignPassportForm">
                                                                        <p>Foreign Passport</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="fp_foreign_number{{firstIndex}}{{secondIndex}}"> I-94 Number</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="fp_foreign_number{{firstIndex}}{{secondIndex}}" name="fpi94Number" ng-pattern="/^[a-zA-Z0-9]/" ng-minlength="11" ng-maxlength="11" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.i94Number">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="unExpForeignPassportForm.fpi94Number.$error.pattern || unExpForeignPassportForm.fpi94Number.$error.minlength || unExpForeignPassportForm.fpi94Number.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="fp_foreign_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="fp_foreign_passport_number{{firstIndex}}{{secondIndex}}" name="fpPassportNumber" required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="unExpForeignPassportForm.fpPassportNumber.$error.required || unExpForeignPassportForm.fpPassportNumber.$error.pattern || unExpForeignPassportForm.fpPassportNumber.$error.minlength || unExpForeignPassportForm.fpPassportNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                             <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="fp_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select id="fp_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="fpForeignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                    <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                </select>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="unExpForeignPassportForm.fpForeignPassportIssuance.$dirty && unExpForeignPassportForm.fpForeignPassportIssuance.$error.required">
                                                                                        <em class="excl"></em>Please select the country from which your passport was issued.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="fp_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="fp_expiration_date{{firstIndex}}{{secondIndex}}" name="fpEpirationDate" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="unExpForeignPassportForm.fpEpirationDate.$dirty && unExpForeignPassportForm.fpEpirationDate.$error.required">
                                                                                        <em class="excl"></em>Please Enter Passport Expiration Date.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div ng-switch-when="I20DocumentID" ng-form="i20DocForm">
                                                                            <p>Certificate of Eligibility for Nonimmigrant (F-1) Student Status (I-20)</p>

                                                                            <div class="form-group">
                                                                                <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i20_sevis_number{{firstIndex}}{{secondIndex}}"> SEVIS ID Number</label>
                                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                    <input type="text" id="i20_sevis_number{{firstIndex}}{{secondIndex}}" name="sevisid" ng-pattern="/^[0-9]/" ng-minlength="10" ng-maxlength="10" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.sevisId">
                                                                                    <div class="help-line margin10-t">
                                                                                        <span class="error-message" ng-if="i20DocForm.sevisid.$error.required || i20DocForm.sevisid.$error.pattern || i20DocForm.sevisid.$error.minlength || i20DocForm.sevisid.$error.maxlength">
                                                                                            <em class="excl">Please enter the SEVIS ID number. This always starts with an N followed by 10 numeric digits. This is usually located at the top, right-hand side of the document.  When entering a SEVIS Number, remove the leading N and enter the 10 digits, including the leading 0s.</em>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i20_foreign_number{{firstIndex}}{{secondIndex}}"> I-94 Number</label>
                                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                    <input type="text" class="input-large" id="i20_foreign_number{{firstIndex}}{{secondIndex}}" name="i94Number" ng-pattern="/^[a-zA-Z0-9]/" ng-minlength="11" ng-maxlength="11" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.i94Number">
                                                                                    <div class="help-line margin10-t">
                                                                                        <span class="error-message" ng-if="i20DocForm.i94Number.$error.pattern || i20DocForm.i94Number.$error.minlength || i20DocForm.i94Number.$error.maxlength">
                                                                                            <em class="excl"></em>Please enter the number from your current I-94. This will contain 11 alphanumeric characters. Go to www.cbp.gov/I94 to get your number.
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i20_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                    <input type="text" id="i20_passport_number{{firstIndex}}{{secondIndex}}" name="i20_fpPassportNumber" required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                    <div class="help-line margin10-t">
                                                                                        <span class="error-message" ng-if="i20DocForm.i20_fpPassportNumber.$error.required || i20DocForm.i20_fpPassportNumber.$error.pattern || i20DocForm.i20_fpPassportNumber.$error.minlength || i20DocForm.i20_fpPassportNumber.$error.maxlength">
                                                                                            <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                 <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i20_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                                 <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                    <select id="i20_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="i20_ForeignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                        <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                    </select>
                                                                                    <div class="help-line margin10-t">
                                                                                        <span class="error-message" ng-if="i20DocForm.i20_ForeignPassportIssuance.$dirty && i20DocForm.i20_ForeignPassportIssuance.$error.required">
                                                                                            <em class="excl"></em>Please select the country from which your passport was issued.
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="i20_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                                <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                    <div class="input-append date">
                                                                                        <input class="input-small" type="text" id="i20_expiration_date{{firstIndex}}{{secondIndex}}" name="i20_expiration_date{{firstIndex}}{{secondIndex}}" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                        <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="i20DocForm.$invalid">Submit</button>

                                                                        </div>
                                                                    <div ng-switch-when="DS2019DocumentID" ng-form="ds2019Form">
                                                                        <p>Temporary I−551 Stamp (on passport or I−94, I−94A)</p>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="DS201_alien_number{{firstIndex}}{{secondIndex}}"> Alien Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" class="input-large" id="DS201_alien_number{{firstIndex}}{{secondIndex}}" required ng-required ng-pattern="/^[0-9]/" ng-minlength="9" ng-maxlength="9" name="DS201_alienNumber" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.alienNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="ds2019Form.DS201_alienNumber.$error.required || ds2019Form.DS201_alienNumber.$error.pattern || ds2019Form.DS201_alienNumber.$error.minlength || ds2019Form.DS201_alienNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your alien number (also known as your alien registration number, USCIS number, or A-Number).When entering an A-Number, include all nine numerical digits. Do not enter other characters or letters. If the number is an older issued A-Number, add leading 0s to ensure nine digits.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="DS201_passport_number{{firstIndex}}{{secondIndex}}"> Passport Number <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <input type="text" id="DS201_passport_number{{firstIndex}}{{secondIndex}}" name="DS201PassportNumber" required ng-pattern="/^[A-Za-z0-9]/" ng-minlength="6" ng-maxlength="12" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportOrDocumentNumber">
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="ds2019Form.DS201PassportNumber.$error.required || ds2019Form.DS201PassportNumber.$error.pattern || ds2019Form.DS201PassportNumber.$error.minlength || ds2019Form.DS201PassportNumber.$error.maxlength">
                                                                                        <em class="excl"></em>Please enter your passport number. This generally contains 6-12 alphanumeric characters.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                             <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="DS201_foreign_passport_issuance{{firstIndex}}{{secondIndex}}"> Foreign Passport Country of Issuance <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                             <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select id="DS201_foreign_passport_issuance{{firstIndex}}{{secondIndex}}" required name="DS201ForeignPassportIssuance" ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance">
                                                                                    <option ng-repeat="country in vm.countries" value="{{country.code}}" ng-selected="{{vm.citizenshipImmigrationStatusData.citizenshipDocument.foreignPassportCountryOfIssuance}}">{{country.name}}</option>
                                                                                </select>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="ds2019Form.DS201ForeignPassportIssuance.$dirty && ds2019Form.DS201ForeignPassportIssuance.$error.required">
                                                                                        <em class="excl"></em>Please select the country from which your passport was issued.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="DS201_expiration_date{{firstIndex}}{{secondIndex}}">Passport Expiration Date</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <div class="input-append date">
                                                                                    <input class="input-small" type="text" id="DS201_expiration_date{{firstIndex}}{{secondIndex}}" name="DS201_expiration_date{{firstIndex}}{{secondIndex}}" datepicker ng-model="vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate">
                                                                                    <span class="add-on"><i class="icon-calendar"></i></span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID, vm.citizenshipImmigrationStatusData.citizenshipDocument.documentExpirationDate)" ng-disabled="ds2019Form.$invalid">Submit</button>

                                                                    </div>
                                                                    <div ng-switch-default ng-form="reverifyDefaultForm">
                                                                        <div class="form-group margin10-t">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="verification-fallback_{{secondIndex}}" >Select Document Type <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <select class="input-large" name="verificationFallback" id="verification-fallback_{{secondIndex}}" required ng-model="vm.documentTypes[firstIndex + '_' + secondIndex]" ng-options="documentType for documentType in value.documentTypeList" ng-init="vm.documentTypes[firstIndex + '_' + secondIndex] = value.documentTypeList[0]"></select>
                                                                            </div>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="verification-fileUpload_{{firstIndex}}{{secondIndex}}">Choose Document to Upload</label>
                                                                            <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                                <label class="btn btn-small" for="verification-fileUpload_{{firstIndex}}{{secondIndex}}">
                                                                                    <input id="verification-fileUpload_{{firstIndex}}{{secondIndex}}" name="file" type="file" class="hide" max-file-size="10485760" file-model-binding="vm.files[firstIndex + '_' + secondIndex]" ng-model="vm.files[firstIndex + '_' + secondIndex]" required>
                                                                                Choose File
                                                                                </label>
                                                                                <span>{{vm.files[firstIndex + '_' + secondIndex].name}}</span>
                                                                                <div class="help-line margin10-t">
                                                                                    <span class="error-message" ng-if="reverifyDefaultForm.file.$error.maxFileSize && reverifyDefaultForm.file.$dirty">
                                                                                        Please upload file that is less than 10MB in size.
                                                                                    </span>
                                                                                </div>
                                                                            </div>
                                                                            <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitDoc(applicant.applicantID, value, firstIndex, secondIndex)" ng-disabled="reverifyDefaultForm.$invalid">Submit</button>
                                                                        </div>
                                                                    </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div ng-switch-when="VERIFY">
                                                                <div class="alert alert-error">
                                                                    Please contact your school or program sponsor to activate your Student and Exchange Visitor Information System ID (SEVIS).  Once active, please resubmit.
                                                                </div>
                                                                <table class="table table-border-none table-condensed">
                                                                    <tbody>
                                                                        <tr ng-repeat="(key, value) in vm.dhsid">
                                                                            <td class="col-sm-4 col-md-4 col-lg-4 txt-right" scope="row">
                                                                                {{key}}
<%--                                                                                <c:if test="${key ==='DocExpirationDate}">Document Expiration Date</c:if>--%>
<%--                                                                                <span ng-switch-when="AlienNumber">Alien Number</span>--%>
<%--                                                                                <span ng-switch-when="CaseNumber">Case Number</span>--%>
<%--                                                                                <span ng-switch-when="CitizenshipNumber">Citizenship Number</span>--%>
<%--                                                                                <span ng-switch-when="CountryOfIssuance">Country Of Issuance</span>--%>
<%--                                                                                <span ng-switch-when="DocDescReq">Document Description</span>--%>
<%--                                                                                <span ng-switch-when="DocExpirationDate">Document Expiration Date</span>--%>
<%--                                                                                <span ng-switch-when="I94Number">I94 Number</span>--%>
<%--                                                                                <span ng-switch-when="NaturalizationNumber">Naturalization Number</span>--%>
<%--                                                                                <span ng-switch-when="PassportCountry">Passport Country</span>--%>
<%--                                                                                <span ng-switch-when="ReceiptNumber">Receipt Number</span>--%>
<%--                                                                                <span ng-switch-when="SEVISID">SEVIS ID</span>--%>
<%--                                                                                <span ng-switch-when="VisaNumber">VisaNumber</span>--%>
<%--                                                                                <span ng-switch-default></span>--%>
                                                                            </td>
                                                                            <td class="col-sm-5 col-md-5 col-lg-5">
                                                                                <strong>{{value}}</strong>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                                <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitData(key, applicant.applicantID)">Submit</button>
                                                           </div>
                                                            <div ng-switch-when="RETRY">
                                                                <div class="alert alert-info">
                                                                    Your information has been submitted. It is currently being processed. Please check back later.
                                                                </div>
                                                            </div>
                                                            <div ng-switch-when="CHECK_CASE_RESOLUTION">
                                                                <div class="alert alert-info">
                                                                    Your information has been submitted. It is currently being processed. Please check back later.
                                                                </div>
                                                                <table class="table table-border-none table-condensed">
                                                                    <tbody>
                                                                        <tr ng-repeat="(key, value) in vm.dhsid">
                                                                            <td class="col-sm-4 col-md-4 col-lg-4 txt-right" scope="row">
                                                                                {{key}}
                                                                            </td>
                                                                            <td class="col-sm-5 col-md-5 col-lg-5">
                                                                                <strong>{{value}}</strong>
                                                                            </td>
                                                                        </tr>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                            <div ng-switch-when="VALIDATION_SUCCESS"></div>
                                                            <div ng-switch-default ng-form="actionResultDefaultForm">
                                                                <div class="form-group margin10-t">
                                                                    <label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="verificationFallback_{{firstIndex}}{{secondIndex}}" >Select Document Type  <img src="/hix/resources/images/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                        <select class="input-large" name="verificationFallback02" id="verificationFallback_{{firstIndex}}{{secondIndex}}" required ng-model="vm.documentTypes[firstIndex + '_' + secondIndex]" ng-options="documentType for documentType in value.documentTypeList" ng-init="vm.documentTypes[firstIndex + '_' + secondIndex] = value.documentTypeList[0]"></select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="verification-fileUpload{{firstIndex}}{{secondIndex}}" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">Choose Document to Upload:</label>
                                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                                        <label class="btn btn-small" for="verification-fileUpload{{firstIndex}}{{secondIndex}}">
                                                                            <input id="verification-fileUpload{{firstIndex}}{{secondIndex}}" name="file" type="file" class="hide" max-file-size="10485760" file-model-binding="vm.files[firstIndex + '_' + secondIndex]" ng-model="vm.files[firstIndex + '_' + secondIndex]" required>
                                                                            Choose File
                                                                        </label>
                                                                        <span>{{vm.files[firstIndex + '_' + secondIndex].name}}</span>
                                                                        <div class="help-line margin10-t">
                                                                            <span class="error-message" ng-if="actionResultDefaultForm.file.$error.maxFileSize && actionResultDefaultForm.file.$dirty">
                                                                                Please upload file that is less than 10MB in size.
                                                                            </span>
                                                                        </div>
                                                                    </div>
                                                                    <button class="btn btn-small btn-primary pull-right" ng-click="vm.submitDoc(applicant.applicantID, value, firstIndex, secondIndex)" ng-disabled="actionResultDefaultForm.$invalid">Submit</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div ng-if="vm.stateCode !== 'MN'" class="tab-pane" id="otherDocumentsTab">
                        <div class="tabbable tabs-left">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#otherDocuments" data-toggle="tab">
                                    Authorized Representative Authorization
                                    <i class="icon-ok-sign" ng-if="vm.webJson.consumerDocuments.length > 0"></i>
                                    <i class="icon-exclamation-sign" ng-if="vm.webJson.consumerDocuments.length === 0"></i>
                                    </a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="otherDocuments">
                                    <form name="authorizedRepresentativeAuthorizationForm" class='form-horizontal' ng-if="vm.webJson.consumerDocuments.length === 0">
                                        <table class="table margin40-t">
                                            <thead>
                                                <tr class="header">
                                                    <td style="width: 270px;">Document Type</td>
                                                    <td>Upload</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Authorized Representative Authorization</td>
                                                    <td>
                                                        <div class="enter-details">
                                                            <label class="btn btn-small">
                                                            <input type="file" class="hide" name="file" max-file-size="10485760" file-model-binding="vm.authorizedRepresentativeAuthorizationFile" ng-model="vm.authorizedRepresentativeAuthorizationFile" required>
                                                            Choose File
                                                            </label>
                                                            <span>{{vm.authorizedRepresentativeAuthorizationFile.name}}</span>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <span class="error-message" ng-if="authorizedRepresentativeAuthorizationForm.file.$error.maxFileSize && authorizedRepresentativeAuthorizationForm.file.$dirty">
                                        Please upload file that is less than 10MB in size.
                                        </span>
                                        <button class='btn btn-primary pull-right' ng-click="vm.submitDoc('Authorized Representative Authorization')" ng-disabled="authorizedRepresentativeAuthorizationForm.$invalid">Submit</button>
                                    </form>
                                    <div ng-if="vm.webJson.consumerDocuments.length > 0">
                                        <table class="table table-condensed margin40-t">
                                            <thead>
                                                <tr>
                                                    <td>Document Name</td>
                                                    <td>document Type</td>
                                                    <td>Status</td>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr ng-repeat="doc in vm.webJson.consumerDocuments">
                                                    <td>{{doc.documentName}}</td>
                                                    <td>{{doc.documentType}}</td>
                                                    <td>{{doc.accepted}}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .gutter10 -->
        </div>
        <!-- .row-fluid -->
    </div>
    <!-- verificationResults -->
    <div id="adminOverrideModal" class="modal hide fade overrideCommentModal">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Override reason</h3>
        </div>
        <div class="modal-body">
            <form name="vm.overrideCommentForm" novalidate>
                <textarea rows="5" ng-model="vm.overrideComment" required></textarea>
            </form>
        </div>
        <div class="modal-footer">
            <a href="javascript:viod(0)" data-dismiss="modal" class="btn">Close</a>
            <a ng-if="vm.qleVerificationModal === false" href="javascript:viod(0)" data-dismiss="modal" ng-click="vm.overrideCommentForm.$valid && vm.verificationDoc()" class="btn btn-primary" ng-disabled='vm.overrideCommentForm.$invalid'>Submit</a>
            <a ng-if="vm.qleVerificationModal === true" href="javascript:viod(0)" data-dismiss="modal" ng-click="vm.overrideCommentForm.$valid && vm.overrideQleVerification()" class="btn btn-primary" ng-disabled='vm.overrideCommentForm.$invalid'>Submit</a>
        </div>
    </div>
    <div id="ajaxErrorModal" class="modal hide fade">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>File Type Not Accepted</h3>
        </div>
        <div class="modal-body">
            <p class="alert alert-error">{{vm.ajaxError}}</p>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0)" data-dismiss="modal" class="btn btn-primary">Close</a>
        </div>
    </div>
    <div id="ajaxSuccessModal" class="modal hide fade">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3>Submitting Data</h3>
        </div>
        <div class="modal-body">
            <p class="alert alert-info">Your information has been successfully submitted. We are currently processing your information. Please check back later.</p>
        </div>
        <div class="modal-footer">
            <a href="javascript:void(0)" data-dismiss="modal" class="btn btn-primary">Close</a>
        </div>
    </div>
</div>

<!-- .gutter10 -->
<script type="text/javascript" src="
<c:url value="/resources/js/spring-security-csrf-token-interceptor.js "/>
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/components/datepicker.directive.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/components/popover.directive.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/ssap/verificationResultDashboard/verificationResultDashboard.app.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/ssap/verificationResultDashboard/verificationResultDashboard.controller.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/ssap/verificationResultDashboard/verificationResultDashboard.directive.js" />
"></script>
<script type="text/javascript" src="
<c:url value="/resources/js/ssap/verificationResultDashboard/verificationResultDashboard.service.js" />
"></script>
