package com.getinsured.ahbx.platform.ecm.mapper;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.ahbx.platform.ecm.Document;
import com.getinsured.ahbx.platform.ecm.Folder;
import com.getinsured.ahbx.platform.ecm.GetFile;
import com.getinsured.ahbx.platform.ecm.Image;
import com.getinsured.ahbx.platform.ecm.ObjectFactory;
import com.getinsured.hix.dto.platform.ecm.Content;
import com.getinsured.hix.dto.platform.ecm.Datasource;
import com.getinsured.hix.model.PlatformRequest;


/**
 * GetContentMapper class to transform
 * 		1. GHIX-Request to AHBX Request
 * 		2. AHBX Response to GHIX-Response
 *
 * @author Ekram Ali Kazi
 *
 */
public final class GetContentMapper {

	private static final Logger LOGGER = Logger.getLogger(GetContentMapper.class);

	private GetContentMapper(){}

	/**
	 * Map GHIX-Request to AHBX Request
	 *
	 * @param ecmRequest
	 * @return AHBX Request object - JAXBElement<GetFile>
	 */
	public static JAXBElement<GetFile> map(PlatformRequest.ECMRequest ecmRequest){

		ObjectFactory of = new ObjectFactory();
		GetFile getFile = of.createGetFile();
		getFile.setDocId(ecmRequest.getDocId());

		return of.createGetFile(getFile);
	}

	/**
	 * Map AHBX Response to GHIX-Response
	 *
	 * @param abhxContentObject
	 * @return Content
	 */
	public static Content map(com.getinsured.ahbx.platform.ecm.Content abhxContentObject) {

		Content giContentObject = null;

		if (abhxContentObject instanceof Image){
			Image ahbxImageContentObject = (Image) abhxContentObject;

			com.getinsured.hix.dto.platform.ecm.Image giImageContentObject = new com.getinsured.hix.dto.platform.ecm.Image();
			giImageContentObject.setBitDepth(ahbxImageContentObject.getBitDepth());
			giImageContentObject.setImageStream(ahbxImageContentObject.getImageStream());
			giImageContentObject.setHeight(ahbxImageContentObject.getHeight());
			giImageContentObject.setResolution(ahbxImageContentObject.getResolution());
			giImageContentObject.setWidth(ahbxImageContentObject.getWidth());

			giContentObject = giImageContentObject;
		} else if (abhxContentObject instanceof Document){
			Document ahbxDocumentContentObject = (Document) abhxContentObject;

			com.getinsured.hix.dto.platform.ecm.Document giDocumentContentObject = new com.getinsured.hix.dto.platform.ecm.Document();
			giDocumentContentObject.setAuthor(ahbxDocumentContentObject.getAuthor());
			giDocumentContentObject.setLanguage(ahbxDocumentContentObject.getLanguage());
			giDocumentContentObject.setPages(ahbxDocumentContentObject.getPages());
			giDocumentContentObject.setSnippet(ahbxDocumentContentObject.getSnippet());
			giDocumentContentObject.setSize(ahbxDocumentContentObject.getSize());

			com.getinsured.hix.dto.platform.ecm.DocumentStream documentStream = new com.getinsured.hix.dto.platform.ecm.DocumentStream();
			documentStream.setContent(ahbxDocumentContentObject.getDocumentStream() == null ? new byte[0] : ahbxDocumentContentObject.getDocumentStream().getContent());
			documentStream.setEncoding(ahbxDocumentContentObject.getDocumentStream() == null ? StringUtils.EMPTY : ahbxDocumentContentObject.getDocumentStream().getEncoding());
			documentStream.setIsoCode(ahbxDocumentContentObject.getDocumentStream() == null ? StringUtils.EMPTY : ahbxDocumentContentObject.getDocumentStream().getIsoCode());
			giDocumentContentObject.setDocumentStream(documentStream);

			giContentObject = giDocumentContentObject;

		} else if (abhxContentObject instanceof Folder){
			Folder ahbxFolderContentObject = (Folder) abhxContentObject;
			com.getinsured.hix.dto.platform.ecm.Folder giFolderContentObject = new com.getinsured.hix.dto.platform.ecm.Folder();
			giFolderContentObject.setParentId(ahbxFolderContentObject.getParentId());

			giContentObject = giFolderContentObject;
		} else {
			LOGGER.error("AHBX GetFile returned unknown object. Object type should be Image, Document or Folder");
			return giContentObject;
		}

		giContentObject.setContentId(abhxContentObject.getContentId());

		giContentObject.setCreationDate(abhxContentObject.getCreationDate() == null ? null : abhxContentObject.getCreationDate().toGregorianCalendar().getTime());
		giContentObject.setModifiedDate(abhxContentObject.getModifiedDate() == null ? null : abhxContentObject.getModifiedDate().toGregorianCalendar().getTime());

		giContentObject.setDescription(abhxContentObject.getDescription());
		giContentObject.setDocType(abhxContentObject.getDocType());
		giContentObject.setMimeType(abhxContentObject.getMimeType());
		giContentObject.setSecurityGroup(abhxContentObject.getSecurityGroup());
		giContentObject.setTitle(abhxContentObject.getTitle());
		giContentObject.setUrl(abhxContentObject.getUrl());
		giContentObject.setOriginalFileName(abhxContentObject.getOriginalFileName());

		Datasource giDatasource = new Datasource();
		giDatasource.setDatasourceId(abhxContentObject.getDataSource().getDatasourceId());
		giDatasource.setDescription(abhxContentObject.getDataSource().getDescription());
		giDatasource.setName(abhxContentObject.getDataSource().getName());
		giContentObject.setDataSource(giDatasource);

		// set customAttributeList
		/*MapWrapper mapWrapper = abhxContentObject.getCustomAttributes();
		List<KeyValue> keyValueList = mapWrapper.getEntries();

		Map<String,String>  customAttributeList = new HashMap<String,String>();
		for (KeyValue keyValue : keyValueList) {
			customAttributeList.put(keyValue.getKey().toString(),keyValue.getValue().toString());
		}*/

		return giContentObject;
	}
}

