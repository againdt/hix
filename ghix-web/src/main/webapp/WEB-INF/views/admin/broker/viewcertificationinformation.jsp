<%@page import="java.net.URLDecoder"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<script type="text/javascript" src="<c:url value="/resources/js/phoneUtils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/utility/dateUtils.js" />"></script>
<%
String myInboxUrl;
if ( GhixConstants.MY_INBOX_URL.contains("?")){
	myInboxUrl = GhixConstants.MY_INBOX_URL + "&languageCode=" + session.getAttribute("lang") ;
}else{
	myInboxUrl = GhixConstants.MY_INBOX_URL + "?languageCode=" + session.getAttribute("lang") ;
}
 String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
%>	
	<script type="text/javascript">
	function highlightThis(val){
		
		var currUrl = window.location.href;
		var newUrl="";
		/* Check if Query String parameter is set or not */
		if(currUrl.indexOf("?", 0) > 0) { /* If Yes */
			if(currUrl.indexOf("?lang=", 0) > 0) {/* Check if locale is already set without querystring param  */
				 newUrl = "?lang="+val;
			} else if(currUrl.indexOf("&lang=", 0) > 0) { /*  Check if locale is already set with querystring param  */
				newUrl = currUrl.substring(0, currUrl.length-2)+val; 
			} else { 
				newUrl = currUrl + "&lang="+val;
			}
		} else { /* If No  */
			newUrl = currUrl + "?lang="+val;
		}
		window.location = newUrl;
	}
	
	$(document).ready(function() {
		var brkId = ${broker.id};
		if(brkId != 0) {
			$('#certificationInfo').attr('class', 'done');
			$('#buildProfile').attr('class', 'done');
		} else {
			$('#certificationInfo').attr('class', 'visited');
			$('#buildProfile').attr('class', 'visited');
		};
	});	
	
	$(document).ready(function(){
		formatAllPhoneNumbers(["sPrimaryPhoneNumber", "sBusinessContactPhoneNumber", "sAlternatePhoneNumber", "sFaxNumber"]);
		formatDates(["sLicenseRenewalDate"]);
	});
</script>
<div class="gutter10">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
	<div class="l-page-breadcrumb" style="margin-top:10px;">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/admin/broker/viewprofile/${encBrokerId}"/>"><spring:message
							code="label.account" /></a></li>
				<li>${broker.user.firstName}&nbsp;${broker.user.firstName}</li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
		<h1><a name="skip"></a>
			<c:choose>
				<c:when test="${broker.user.firstName != null && broker.user.lastName != null}">
					${broker.user.firstName}&nbsp;${broker.user.lastName}
				</c:when>
				<c:otherwise>
					${broker.firstName}&nbsp;${broker.lastName}
				</c:otherwise>
			</c:choose>
		</h1>
	</div>
	
	<div class="row-fluid">
		<div id="sidebar" class="span3">							
			<div class="header"></div>
			<ul class="nav nav-list">
                 <li class="active"><a href="/hix/admin/broker/viewcertificationinformation/${encBrokerId}"><spring:message code="label.brkBrokerInformation"/></a></li>		                  
                 <li><a href="/hix/admin/broker/viewprofile/${encBrokerId}"><spring:message code="label.profile"/></a></li>
                 <li><a href="/hix/admin/broker/certificationstatus/${encBrokerId}"><spring:message code="label.brkCertificationStatus"/></a></li>				
 				<c:if test="${CA_STATE_CODE}">
					<li id="brkActivityStatus"><a href="/hix/admin/broker/activitystatus/${encBrokerId}"><spring:message code="label.agentactivitystatus"/></a></li>
				</c:if>
                 <li><a href="/hix/admin/broker/comments/${encBrokerId}"><spring:message code="label.brkComments"/></a></li>
                 <c:if test="${CA_STATE_CODE}">
					<li><a href="#"  onclick="window.open('<%=myInboxUrl%>&userId=${encryptedUserName}');"><spring:message code="label.brkSecureInbox"/></a></li>
				</c:if>	
                 <c:if test="${!CA_STATE_CODE && broker.user != null}">
					 <li><a href="/hix/admin/broker/tickethistory/${encBrokerId}"><spring:message code="label.brkTicketHistory"/></a></li>	
					 <%-- <li><a href="/hix/admin/broker/securityquestions/${encBrokerId}"><spring:message code="label.brkSecurityQuestions"/></a></li> --%>
				 </c:if>
                 
               </ul>
               <br>
            <c:if test="${!CA_STATE_CODE}">
				<div class="header">
					<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
				</div>
				<ul class="nav nav-list">
				<c:if test="${checkDilog == null}">  
					<li class="navmenu"><a href="#markCompleteDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
				<c:if test="${checkDilog != null}">  
					<li class="navmenu"><a href="/hix/admin/broker/dashboard?switchToModuleName=<encryptor:enc value="broker"/>&switchToModuleId=<encryptor:enc value="${broker.id}"/>&switchToResourceName=${encSwitchToResourceFullName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.ViewAgentAccount"/></a></li>
				</c:if>
	            </ul>   
            </c:if>
		</div><!-- end of span3 -->
	
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message code="label.brkBrokerInformation"/></h4>
				<a class="btn btn-small pull-right" title="<spring:message code="label.brkEdit"/>" href="<c:url value="/admin/broker/certificationapplication/${encBrokerId}"/>"><spring:message code="label.brkEdit"/></a>
			</div>
			 <form class="form-horizontal" id="frmviewCertification" name="frmviewCertification" action="viewcertificationinformation" method="POST">
			
				<!-- <p class="gutter10-tb">Apply for certification on Covered California by providing the following information. After a quick review, we'll send you an email letting you know if your application has been approved.
				</p> -->
				<div class="gutter10">
					<table class="table table-border-none table-condensed verticalThead">
						<tbody>
							<tr>
								<td class="span4 txt-right"><spring:message code="label.brkFirstName"/></td>
								<td class="span8"><strong>
									<c:choose>
										<c:when test="${broker.user.firstName!=null}">
											${broker.user.firstName}
										</c:when>
										<c:otherwise>
											${broker.firstName}
										</c:otherwise>
									</c:choose>
								</strong></td>
							</tr>
							
							<tr>
								<td class="span4 txt-right"><spring:message code="label.brkLastName"/></td>
								<td><strong>
									<c:choose>
										<c:when test="${broker.user.lastName!=null}">
											${broker.user.lastName}
										</c:when>
										<c:otherwise>
											${broker.lastName}
										</c:otherwise>
									</c:choose>
								
								</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code="label.brkLicenseNumber"/></td>
								<td><strong>${broker.licenseNumber}</strong></td>
							</tr>
							
							<c:if test="${stateCode=='NV' &&  broker.npn != '' && broker.npn != null}">
								<tr>
									<td class="txt-right"><spring:message code="label.brkNpn"/></td>
									<td><strong>${broker.npn}</strong></td>
								</tr>
							</c:if>
							
							<tr>
								<td class="txt-right"><spring:message code="label.brkLicenseRenewalDate"/></td>
								<td><strong id="sLicenseRenewalDate"><fmt:formatDate value="${broker.licenseRenewalDate}" pattern="MM/dd/yyyy"/></strong></td>
							</tr>
							<c:if test="${broker.personalEmailAddress != '' && broker.personalEmailAddress != null}">
								<tr>
									<td class="txt-right"><spring:message code="label.brkPersonalEmail"/></td>
									<td><strong>${broker.personalEmailAddress}</strong></td>
								</tr>
							</c:if>
							<tr>
								<td class="txt-right"><spring:message code="label.brkPrimaryPhoneNumber"/></td>
								<td><strong id="sPrimaryPhoneNumber">(${phone1}) ${phone2}-${phone3}</strong></td>
							</tr>							
							
							<c:if test="${broker.businessContactPhoneNumber != '' && broker.businessContactPhoneNumber != null  && fn:length(broker.businessContactPhoneNumber)==12 }">
							<tr>
								<td class="txt-right"><spring:message code="label.brkBusinessContactPhoneNumber"/></td>
								<td><strong id="sBusinessContactPhoneNumber">${broker.businessContactPhoneNumber}</strong></td>
							</tr>
							</c:if>
							
							<c:if test="${broker.alternatePhoneNumber != '' && broker.alternatePhoneNumber != null && fn:length(broker.alternatePhoneNumber)==12 }">
							<tr>
								<td class="txt-right"><spring:message code="label.brkAlternatePhoneNumber"/></td>
								<td><strong id="sAlternatePhoneNumber">${broker.alternatePhoneNumber}</strong></td>
							</tr>
							</c:if>
							
							<c:if test="${broker.faxNumber != '' && broker.faxNumber != null  && fn:length(broker.faxNumber)==12 }">
							<tr>
								<td class="txt-right"><spring:message code="label.brkFaxNumber"/></td>
								<td><strong id="sFaxNumber">${broker.faxNumber}</strong></td>
							</tr>
							</c:if>								
							<tr>
								<td class="txt-right"><spring:message code="label.brkPreferredMethodofCommunication"/> </td>
								<td><strong>${broker.communicationPreference}</strong></td>
							</tr>	
							<c:if test="${allowMailNotice=='Y'}">
							<tr>
								<td class="span4 txt-right vertical-align-top"><spring:message code="label.brk.postalMail"/></td>
									<c:choose>
										<c:when test="${broker.postalMailEnabled == 'Y'}">
											<td><strong> <spring:message code="label.brk.yes"/> </strong> </td>											
										</c:when>
										<c:otherwise>
											<td><strong> <spring:message code="label.brk.no"/> </strong></td>
										</c:otherwise>
									</c:choose>
									
								</tr>
							</c:if>
								<tr>					
							<tr>
								<td class="txt-right"><spring:message code="label.brkCompanyName"/> </td>
								<td><strong>${broker.companyName}</strong></td>
							</tr>	
							<tr>
								<td class="txt-right"><spring:message code="label.brkfederalEIN"/></td>
								<td><strong>${broker.federalEIN}</strong></td>
							</tr>
							
							<c:if test="${ ROLECHANGEALLOWED == 'TRUE' }">
								<tr>
									<td class="txt-right"><spring:message code="label.brkRole"/></td>
									<td>
										<c:choose>
											<c:when test="${CURRENTROLE == 'AGENCYMANAGER'}">
												<strong><spring:message code="label.brkAgencyManager"/></strong>										
											</c:when>
											<c:otherwise>
												<strong><spring:message code="label.brkAgent"/></strong>
											</c:otherwise>
										</c:choose>
									</td>
								</tr>
							 </c:if > 
															
							</tbody>
					</table>
				</div><!-- end of .gutter10 -->
				
					<div class="gutter10">
						<h4 class="lightgray"><spring:message code="label.brkBusinessAddress"/></h4>
						<table class="table table-border-none table-condensed verticalThead">
							<tbody>
								<tr>
									<td class="span4 txt-right"><spring:message code="label.brkBusinessAddressCheck"/></td>
									<td class="span8"><strong>${locationObj.address1} <br/>
									${locationObj.address2}</strong></td>
								</tr>
								<tr>
									<td class="span4 txt-right"></td>
									<td><strong>${locationObj.city}, ${locationObj.state} ${locationObj.zip}</strong></td>
								</tr>							
							</tbody>
						</table>
					</div><!-- end of .gutter10 -->
				
					<div class="gutter10">
						<h4 class="lightgray"> <spring:message code="label.brkMailingAddress"/> </h4>
						<table class="table table-border-none table-condensed verticalThead">
							<tbody>
								<tr>
									<td class="span4 txt-right"><spring:message code="label.brkSameAsBusinessAddress"/></td>
									<c:choose>
										<c:when test="${locationMatching == 'Y'}">
											<td class="span8"><i class="icon-ok"></i></td>
										</c:when>
										<c:otherwise>
											<td class="span8"><i class="icon-minus-sign"></i></td>
										</c:otherwise>
									</c:choose>
								</tr>
								<tr>
									<td class="span4 txt-right"><spring:message code="label.brkMailingAddressCheck"/></td>
									<td><strong>${mailingLocationObj.address1} <br/>
									${mailingLocationObj.address2}</strong></td>
								</tr>
								<tr>
									<td class="txt-right"></td>
									<td><strong>${mailingLocationObj.city}, ${mailingLocationObj.state} ${mailingLocationObj.zip}</strong></td>
								</tr>
							</tbody>
						</table>
					</div><!-- end of .gutter10 -->

				<div class="form-actions">
					<a class="btn btn-primary pull-right" type="button"  href="<c:url value="/admin/broker/certificationapplication/${encBrokerId}"/>"><spring:message code="label.brkEdit"/>  </a>
				</div>
			</form>
		</div>
	
	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p><spring:message code="label.brkBrokerEligibilityInfo"/></p>
			</div>
		</div>
	</div>
	
	</div>
</div>
<%-- Modal for switch user role --%>
  <c:set scope="request" var="switchToModuleId" value="${broker.id}"></c:set>
  <c:set scope="request" var="switchToResourceName" value="${encSwitchToResourceFullName}"></c:set>
  <c:set scope="request" var="currSwitchToResourceFullName" value="${unEncSwitchToResourceFullName}"></c:set>
 
  <jsp:include page="admintobrokerswitch.jsp"></jsp:include>
<%--Modal ends --%>