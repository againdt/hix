package com.getinsured.hix.broker.service;

import java.util.Date;
import java.util.List;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.DesignateBroker.Status;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Designate Broker Service to designate / de-designate employers / individuals
 * 
 * @author kanthi_v
 * 
 */
public interface DesignateService {

	/**
	 * De-designates broker for external employer id.
	 * 
	 * @param brokerId
	 *            Identification number for broker
	 * @param extEmployerId
	 *            AHBX employer id
	 * @return {@link DesignateBroker}
	 * @throws Exception 
	 */
	DesignateBroker deDesignateEmployerForBroker(int brokerId, int extEmployerId) throws GIException;

	/**
	 * De-designates broker for external individual id.
	 * 
	 * @param brokerId
	 *            Identification number for broker
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateBroker}
	 * @throws Exception 
	 */
	DesignateBroker deDesignateIndividualForBroker(int brokerId, int extIndividualId) throws GIException;

	/**
	 * Finds De-designate broker record for external individual id
	 * 
	 * @param brokerId
	 *            Identification number for broker
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findDeDesignateRecordForBrokerAndIndividualId(int brokerId, int extIndividualId);

	/**
	 * Finds De-designate broker record for external employer id.
	 * 
	 * @param brokerId
	 *            Identification number for broker
	 * @param extEmployerId
	 *            AHBX employer id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findDeDesignateRecordForBrokerAndEmployerId(int brokerId, int extEmployerId);

	/**
	 * Finds De-designate broker record for external employer id, agent id and
	 * status.
	 * 
	 * @param agentId
	 *            Identification number for agent
	 * @param extIndividualId
	 *            AHBX individual id
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findByIndividualAndAgentIdAndStatus(int agentId, int extIndividualId, Status status);

	/**
	 * Finds De-designate broker record for external employer id, agent id and
	 * status.
	 * 
	 * @param agentId
	 *            Identification number for agent
	 * @param externalEmployerId
	 *            AHBX employer id
	 * @param status
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findByEmployerAndAgentIdAndStatus(int agentId, int externalEmployerId, Status status);

	/**
	 * Designates broker for external individual id
	 * 
	 * @param brokerId
	 *            Identification number for agent
	 * @param extIndividualId
	 *            AHBX individual id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker designateIndividualForBroker(int brokerId,	int extIndividualId) throws GIException;

	/**
	 * Designates broker for external employer id
	 * 
	 * @param brokerId
	 *            Identification number for agent
	 * @param externalEmployerId
	 *            AHBX employer id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker designateEmployerForBroker(int brokerId, int extEmployerId) throws GIException;
	
	DesignateBroker designateEmployerByBroker(int brokerId, int employerId) throws GIException;

	/**
	 * Finds {@link DesignateBroker} by employer id
	 *
	 * @param employerId
	 *            Employer id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findBrokerByEmployerId(int employerId);
	

	/**
	 * Finds {@link DesignateBroker} by employer id and status
	 *
	 * @param employerId
	 *            Employer id
	 * @return {@link DesignateBroker}
	 */
	DesignateBroker findBrokerByEmployerIdAndStatus(int employerId,Status status);

	DesignateBroker findBrokerByIndividualId(Integer individualId);
	
	/**
	 * Saves designated broker with Pending status when employer/individual
	 * sends designate request.
	 *
	 * @param designateBroker
	 * @return 
	 */
	DesignateBroker saveDesignateBroker(DesignateBroker designateBroker);

	void sendAgentTransferToEnrollment(int brokerId, int targetBrokerId, String userName);

	void updateEnrollmentDetails(Broker brokerObj, String marketType, int householdId, String roleType,
			String actionFlag, String userName);

	void updateByBrokerId(int brokerId, int targetBrokerId, AccountUser user);

	void updateByBrokerIdAndIndividualId(int brokerId, int externalIndividualId, int targetBrokerId, AccountUser user)
			throws GIException;

	List<DesignateBroker> findByBrokerIdAndUpdatedTime(int brokerId, Date updatedDate);
	
}
