package com.getinsured.hix.batch.agent.service;

import java.text.SimpleDateFormat;
import java.time.Clock;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.batch.agent.AgentDailySummaryNoticesDTO;
import com.getinsured.hix.batch.agent.Broker;
import com.getinsured.hix.batch.agent.Individual;
import com.getinsured.hix.batch.agent.Notices;
import com.getinsured.hix.batch.repository.BrokerConnectRepository;
import com.getinsured.hix.model.BrokerConnect;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.timeshift.sql.TSTimestamp;


@Service("agentBatchService")
@Transactional
public class AgentBatchServiceImpl implements AgentBatchService{

	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private BrokerConnectRepository brokerConnectRepository;

	private static final String HEADER_CONTENT = "headerContent";
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentBatchServiceImpl.class);
	private static final String AGENT_NOTIFICATION_TEMPLATE_NAME = "Agent Daily Book of Business Notice Summary";
	private static final String AGENT_BOOK_OF_BUSINESS_NOTICE_SUMMARY = "agentBookOfBusinessNoticeSummary_";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";	
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String BROKER = "broker";
	private static final String PDF = ".pdf";
	private static final String ECMRELATIVEPATH = "agentnoticesummary";
	private static final String AGENTFIRSTNAME = "AgentFirstName";
	private static final String AGENTLASTNAME = "AgentLastName";
	private static final String DAYONWHICHINDIVIDUALNOTICESWEREDELIVERED = "DayOnWhichIndividualNoticesWereDelivered";
	private static final String INDIVIDUALLIST = "IndividualsWithActionableNotices";
	private static final String TOTALACTIVEINDWITHACTIONABLENOTICES = "TotalActiveIndividualsWithActionableNotices";
	private static final String LOS_ANGELES_ZONE_ID = "America/Los_Angeles";
	private static final String BROKER_TIME_FORMAT = "HH:mm";

	@Override
	public void sendNotices() throws GIException {


		// Get the notice_types names from global configuration list
		String actionalNotices = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.
				AEEConfigurationEnum.AGENT_ACTIONABLENOTICESTOINDIVIDUALS_CONFIGURED);
		String[] actionalNoticesArr = actionalNotices.split(",");
		String actionalNoticesStr = "";
		for (String actionalNotice : actionalNoticesArr) {
			actionalNoticesStr += actionalNoticesStr.length() == 0 ? "'" + actionalNotice + "'" : "," + "'" + actionalNotice + "'";
		}

		List<AgentDailySummaryNoticesDTO> dtoListOfSummaryNotices = setAgentNoticeSummaryDetails(actionalNoticesStr);
		sendDailySummaryNotice(dtoListOfSummaryNotices);



	}

	private void sendDailySummaryNotice(List<AgentDailySummaryNoticesDTO> listDTO) {

		String noticeTemplateName = AGENT_NOTIFICATION_TEMPLATE_NAME;
		List<String> sendToEmailList = null;

		for(AgentDailySummaryNoticesDTO agentDailySummaryNoticesDTO : listDTO) {


			Map<String, Object> brokerTemplateData = new HashMap<String, Object>();
			List<Individual> individualDetailsList  = agentDailySummaryNoticesDTO.getIndividualDetailsList();
			String agentFullName = agentDailySummaryNoticesDTO.getAgentfirstName() + " " + agentDailySummaryNoticesDTO.getAgentlastName();
			brokerTemplateData.put(AGENTFIRSTNAME,agentDailySummaryNoticesDTO.getAgentfirstName());
			brokerTemplateData.put(AGENTLASTNAME,agentDailySummaryNoticesDTO.getAgentlastName());
			brokerTemplateData.put(INDIVIDUALLIST, individualDetailsList);
			brokerTemplateData.put(TOTALACTIVEINDWITHACTIONABLENOTICES, agentDailySummaryNoticesDTO.getTotalActiveIndividualsWithActionableNotices());
			
			

			SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.DATE, -1);
			brokerTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new Date()));			
			String fileName = AGENT_BOOK_OF_BUSINESS_NOTICE_SUMMARY + System.currentTimeMillis() + PDF;
			brokerTemplateData.put(DAYONWHICHINDIVIDUALNOTICESWEREDELIVERED, sdf.format(cal.getTime()));
			// tokens required for the notification template
			brokerTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			brokerTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			brokerTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
			brokerTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
			brokerTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
			brokerTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
			brokerTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			brokerTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

			brokerTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
			brokerTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
			brokerTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
			brokerTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);


			Map<String, String> templateDataModified = getTemplateDataMap(brokerTemplateData);
			sendToEmailList = new LinkedList<String>();
			sendToEmailList.add(agentDailySummaryNoticesDTO.getEmailAddress());
			
			try {
				noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN,
						brokerTemplateData, ECMRELATIVEPATH, fileName, BROKER, agentDailySummaryNoticesDTO.getID(),
						sendToEmailList, "Exchange Admin", agentFullName);

			}catch(GIRuntimeException giexception) {
				LOGGER.error("Cannot update Broker's Certification Status : ", giexception);
				throw giexception;
			} catch(Exception exception) {
				LOGGER.error("Cannot update Broker's Certification Status: ", exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}



		}



	}

	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
		Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			if(pairs.getValue() != null) {
				templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
			}
		}
		return templateDataModified;
	}

	private List<AgentDailySummaryNoticesDTO> setAgentNoticeSummaryDetails(String actionalNoticesStr)
	{

		Query query = null;
		String queryStr = null;
		List<Broker> agentList = getAgentObjList(actionalNoticesStr);
		AgentDailySummaryNoticesDTO agentDailySummaryNoticesDTO = null;
		List<AgentDailySummaryNoticesDTO> dtoList = null;

		dtoList = new ArrayList();
		for(Broker agent : agentList ) {
			
			if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
				queryStr = "select distinct cmr.id, cmr.first_name , cmr.last_name , loc.address1 ,"
						+ " loc.address2  "
						+ ", cmr.phone_number , cmr.email_address , loc.city ,loc.state ,loc.zip  "
						+ "from notice_types nt "
						+ "inner join notices n on n.notice_type_id = nt.id and nt.NAME IN "
						+ "(" + actionalNoticesStr + ") and "
						+ " n.creation_timestamp BETWEEN date_trunc('day',now()) - interval '1 day' AND date_trunc('day',now())- interval '1 second' "
						+ "inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL'  "
						+ "inner join designate_broker db on db.individualid = cmr.id and db.status='Active' "
						+ "inner join brokers b on b.id = db.brokerid and b.id = "+ agent.getId()
						+ " inner join users u on u.id = b.userid "
						+ "left outer join locations loc on loc.id=cmr.contact_location_id";
			}else{
				queryStr = "select distinct cmr.id, cmr.first_name , cmr.last_name , loc.address1 ,"
						+ " loc.address2  "
						+ ", cmr.phone_number , cmr.email_address , loc.city ,loc.state ,loc.zip  "
						+ "from notice_types nt "
						+ "inner join notices n on n.notice_type_id = nt.id and nt.NAME IN "
						+ "(" + actionalNoticesStr + ") and "
						+ " n.creation_timestamp BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400 "
						+ "inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL'  "
						+ "inner join designate_broker db on db.individualid = cmr.id and db.status='Active' "
						+ "inner join brokers b on b.id = db.brokerid and b.id = "+ agent.getId()
						+ " inner join users u on u.id = b.userid "
						+ "left outer join locations loc on loc.id=cmr.contact_location_id";
			}
			List rsList =  getListFromQuery(queryStr);
			Iterator rsIterator = rsList.iterator();

			List<Individual> individualDetailsList = null;
			agentDailySummaryNoticesDTO = new AgentDailySummaryNoticesDTO();
			agentDailySummaryNoticesDTO.setID(agent.getId());
			agentDailySummaryNoticesDTO.setAgentfirstName(agent.getFirstName());       
			agentDailySummaryNoticesDTO.setAgentlastName(agent.getLastName());
			agentDailySummaryNoticesDTO.setEmailAddress(agent.getEmailAddress());
			int totalActiveIndividualsWithActionableNotices = 0;
			int indSerialNo = 1;
			
			individualDetailsList = new ArrayList<Individual>();
			String queryToFetchNotices = null;
			
			while(rsIterator.hasNext()) {

				Individual individual = new Individual();
				Object[] objArray = (Object[]) rsIterator.next();
				
				individual.setFirstName(objArray[1]==null?"":objArray[1].toString());
				individual.setLastName(objArray[2]==null?"":objArray[2].toString());
				individual.setAddress1(objArray[3]==null?"":objArray[3].toString());
				individual.setAddress2(objArray[4]==null?"":objArray[4].toString());
				if(objArray[5]==null)
				{
					individual.setPhoneNumber("");
				}
				else
				{
					individual.setPhoneNumber(objArray[5].toString().substring(0, 3) + "-" + objArray[5].toString().substring(3, 6) + "-" + objArray[5].toString().substring(6, 10));
				}
				individual.setEmailAddress(objArray[6]==null?"":objArray[6].toString());
				individual.setCity(objArray[7]==null?"":objArray[7].toString());        		
				individual.setState(objArray[8]==null?"":objArray[8].toString());        		
				individual.setZip(objArray[9]==null?"":objArray[9].toString());
				individual.setSerialNo(indSerialNo);
				
				indSerialNo++;
				totalActiveIndividualsWithActionableNotices++;
				if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
					queryToFetchNotices = "select n.id as NoticeSerialNumber , nt.name as NoticeName , n.subject as NoticeEmailSubject from notice_types nt "
							+ "inner join notices n on n.notice_type_id = nt.id and nt.NAME IN (" + actionalNoticesStr + ")  "
							+ " and n.creation_timestamp BETWEEN date_trunc('day',now()) - interval '1 day' AND date_trunc('day',now())- interval '1 second' "
							+ " inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL' and cmr.id = "+ objArray[0].toString()
							+ " inner join designate_broker db on db.individualid = cmr.id and db.status='Active' "
							+ " inner join brokers b on b.id = db.brokerid";
				}else{
					queryToFetchNotices = "select n.id as NoticeSerialNumber , nt.name as NoticeName , n.subject as NoticeEmailSubject from notice_types nt "
							+ "inner join notices n on n.notice_type_id = nt.id and nt.NAME IN (" + actionalNoticesStr + ")  "
							+ " and n.creation_timestamp BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400 "
							+ " inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL' and cmr.id = "+ objArray[0].toString()
							+ " inner join designate_broker db on db.individualid = cmr.id and db.status='Active' "
							+ " inner join brokers b on b.id = db.brokerid";
				}
				
				List rsListOfNotices = getListFromQuery(queryToFetchNotices);
				Iterator rsIteratornoti = rsListOfNotices.iterator();
				List<Notices> noticesList = new ArrayList<Notices>();
				int noticeSerialNo = 1;
				while(rsIteratornoti.hasNext()) {
					
					Object[] obj = (Object[]) rsIteratornoti.next();
					Notices notice = new Notices();
					notice.setNoticeSerialNumber(noticeSerialNo);
					notice.setNoticeName(obj[1]==null?"":obj[1].toString());
					notice.setNoticeEmailSubject(obj[2]==null?"":obj[2].toString());
					noticesList.add(notice);
					noticeSerialNo++;
				}
				individual.setNoticeList(noticesList);
				individualDetailsList.add(individual); 
				 
			}  
			
			agentDailySummaryNoticesDTO.setTotalActiveIndividualsWithActionableNotices(totalActiveIndividualsWithActionableNotices);
			agentDailySummaryNoticesDTO.setIndividualDetailsList(individualDetailsList);
			dtoList.add(agentDailySummaryNoticesDTO);

		}

		return dtoList;
	}

	private List<Broker> getAgentObjList(String actionalNoticesStr)
	{
		Query query = null;
		String queryStr = null;
		if("POSTGRESQL".equalsIgnoreCase(GhixConstants.DATABASE_TYPE)){
			queryStr = "select distinct(b.id),u.first_name,u.last_name,u.email from notice_types nt "
					+ "inner join notices n on n.notice_type_id=nt.id "
					+ "and nt.NAME IN (" + actionalNoticesStr + ")  and n.creation_timestamp BETWEEN date_trunc('day',now()) - interval '1 day' AND date_trunc('day',now())- interval '1 second' "
					+ "inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL' "
					+ "inner join designate_broker db on db.individualid = cmr.id and db.status='Active'  "
					+ "inner join brokers b on b.id = db.brokerid inner join users u on u.id = b.userid  ";
		}else{
			queryStr = "select distinct(b.id),u.first_name,u.last_name,u.email from notice_types nt "
					+ "inner join notices n on n.notice_type_id=nt.id "
					+ "and nt.NAME IN (" + actionalNoticesStr + ")  and n.creation_timestamp BETWEEN TRUNC(SYSDATE - 1) AND TRUNC(SYSDATE) - 1/86400 "
					+ "inner join cmr_household cmr on cmr.id = n.key_id and n.key_name = 'INDIVIDUAL' "
					+ "inner join designate_broker db on db.individualid = cmr.id and db.status='Active'  "
					+ "inner join brokers b on b.id = db.brokerid inner join users u on u.id = b.userid  ";
		}
		

		List<Object[]> rList = getListFromQuery(queryStr);
		List<Broker> agentObjList = new  ArrayList<Broker>();
		Iterator iterator = rList.iterator();
		Broker broker = null;

		while (iterator.hasNext()) {			
			Object[] objArray = (Object[]) iterator.next();
			broker = new Broker();
			broker.setId((Integer) (objArray[0]==null? "" : Integer.valueOf(objArray[0].toString())));
			broker.setFirstName(objArray[1]==null?"":objArray[1].toString());
			broker.setLastName(objArray[2]==null?"":objArray[2].toString());   
			broker.setEmailAddress(objArray[3]==null?"":objArray[3].toString());
			agentObjList.add(broker);			
		}
		return agentObjList;
	}

	private List<Object[]> getListFromQuery(String queryStr) {
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			Query query = em.createNativeQuery(queryStr);	
			List<Object[]> rList = query.getResultList();	
			return rList;
		} catch (Exception ex) {
			LOGGER.error("Exception occurred while fetching notice summary details", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
	}

	@Override
	public void setAgentBrokerAvailability() throws GIRuntimeException {
		try {
			List<BrokerConnect> availableBrokers = brokerConnectRepository.findActiveBrokers();
			if(availableBrokers != null) {
				LOGGER.debug("setAgentBrokerAvailability::number of available brokers:" + availableBrokers.size());
				List<BrokerConnect> availableBrokersList = new ArrayList<>();
				List<BrokerConnect> unavailableBrokersList = new ArrayList<>();
				availableBrokers.forEach(brokerConnect -> setBrokerAvailabilityOnCurrentTime(brokerConnect, availableBrokersList, unavailableBrokersList));
				brokerConnectRepository.save(availableBrokers);
				brokerConnectRepository.save(unavailableBrokersList);
			} else {
				throw new Exception("setAgentBrokerAvailability::availableBrokers object was null");
			}
		} catch (Exception ex) {
			LOGGER.error("setAgentBrokerAvailability::Exception occurred setting broker availability", ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
	}

	private void setBrokerAvailabilityOnCurrentTime(BrokerConnect brokerConnect, List<BrokerConnect> availableBrokers, List<BrokerConnect> unavailableBrokers) {
		try {
			if(brokerConnect != null) {
				TSTimestamp timestamp = new TSTimestamp();
				DateTimeFormatter dtf = DateTimeFormatter.ofPattern(BROKER_TIME_FORMAT);
				ZoneId losAngelesZoneId = ZoneId.of(LOS_ANGELES_ZONE_ID);
				ZoneId serverZoneId = ZonedDateTime.now().getZone();
				ZonedDateTime nowOnServer = timestamp.toLocalDateTime().atZone(serverZoneId);
				ZonedDateTime nowInLosAngeles = nowOnServer.withZoneSameInstant(losAngelesZoneId);
				LocalTime currentDateTime = nowInLosAngeles.toLocalTime().withSecond(0).withNano(0);
				LocalDate date = nowInLosAngeles.toLocalDate();

				LOGGER.debug("setBrokerAvailabilityOnCurrentTime::current date time = " + currentDateTime);
				LOGGER.debug("setBrokerAvailabilityOnCurrentTime::date = " + date.toString());
				int currentWeekday = date.getDayOfWeek().getValue() - 1;
				LOGGER.debug("setBrokerAvailabilityOnCurrentTime::current weekday = " + currentWeekday);
				
				StringBuilder activeBrokerHoursQuery = new StringBuilder();
				activeBrokerHoursQuery.append("SELECT broker_connect_hours.id, broker_connect_hours.broker_connect_id, broker_connect_hours.day, broker_connect_hours.from_time, broker_connect_hours.to_time ");
				activeBrokerHoursQuery.append("FROM broker_connect_hours ");
				activeBrokerHoursQuery.append("WHERE broker_connect_hours.broker_connect_id = :brokerConnectId AND broker_connect_hours.day = (:currentWeekday);");

				Map<String, Object> parameterMap = new HashMap<>();
				parameterMap.put("brokerConnectId", brokerConnect.getId());
				parameterMap.put("currentWeekday", String.valueOf(currentWeekday));

				List resultList = buildAndExecuteQuery(activeBrokerHoursQuery.toString(), parameterMap);

				if (CollectionUtils.isEmpty(resultList)) {
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("getBrokerHoursForDay::no active brokers found");
					}
				} else {
					Iterator rsIterator = resultList.iterator();
					Object[] objArray = null;

					while (rsIterator.hasNext()) {
						objArray = (Object[]) rsIterator.next();
						//Integer brokerConnectHoursId = objArray[0] != null ? Integer.parseInt(objArray[0].toString()) : null;
						Integer brokerConnectId = objArray[1] != null ? Integer.parseInt(objArray[1].toString()) : null;
						String day = objArray[2] != null ? objArray[2].toString() : null;
						String fromTime = objArray[3] != null ? objArray[3].toString() : null;
						String toTime = objArray[4] != null ? objArray[4].toString() : null;
						if(day != null && day.equals(String.valueOf(currentWeekday))) {
							LOGGER.debug("getBrokerHoursForDay::info for broker id {}: day {} : fromTime {} : toTime {}", brokerConnectId, day, fromTime, toTime);
							if(fromTime != null && toTime != null) {
								if(fromTime.equals("closed") && toTime.equals("closed")) {
									LOGGER.debug("getBrokerHoursForDay::from and to time closed set to unavailable");
									brokerConnect.setAvailable(false);
									unavailableBrokers.add(brokerConnect);
								}else{
									LocalTime formattedFromTime = LocalTime.parse(fromTime, dtf);
									LocalTime formattedToTime = LocalTime.parse(toTime, dtf);
									if(currentDateTime.equals(formattedFromTime)) {
										LOGGER.debug("getBrokerHoursForDay::from time is now set is available to true");
										brokerConnect.setAvailable(true);
										availableBrokers.add(brokerConnect);
									} else if(currentDateTime.equals(formattedToTime)) {
										LOGGER.debug("getBrokerHoursForDay::from time is now set is available to true");
										brokerConnect.setAvailable(false);
										unavailableBrokers.add(brokerConnect);
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("getBrokerHoursForDay::exception occurred getting active brokers", ex);
		}
	}

	private List buildAndExecuteQuery(String queryString, Map<String, Object> parameterMap) {
		EntityManager entityManager = null;
		List resultList = null;

		try {
			entityManager = emf.createEntityManager();
			Query query = entityManager.createNativeQuery(queryString);
			if(parameterMap != null) {
				parameterMap.entrySet().forEach(entry -> {
					query.setParameter(entry.getKey(), entry.getValue());
				});
			}
			LOGGER.debug("buildAndExecuteQuery::queryString = " + query.toString());

			resultList = query.getResultList();
		} catch(Exception ex) {
			LOGGER.error("Exception occurred in buildAndExecuteQuery", ex);
		} finally {
			// close the entity manager
			if(entityManager !=null && entityManager.isOpen()){
				entityManager.clear();
				entityManager.close();
			}
		}

		return resultList;
	}

}
