import { spinnerActionTypes } from '../constants/actionTypes';

const initialState = {
  isSpinnerActive: false,
  message: null
};

export default (state = initialState, action) => {
  switch (action.type) {
    case spinnerActionTypes.START_SPINNER:
      return {
        isSpinnerActive: true,
        message: action.message
      };
    case spinnerActionTypes.STOP_SPINNER:
      return initialState;
    case spinnerActionTypes.CLEAR_STATE:
      return initialState;
    default:
      return state;
  }
};
