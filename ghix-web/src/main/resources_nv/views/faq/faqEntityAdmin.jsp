<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a>Enrollment Entity Administrator FAQs</h1>
		<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
	             
					<df:csrfToken/>

		</div>
		<div class="span9 " id="rightpanel">
			<div class="header">
				<h4>Enrollment Entity Administrator FAQs</h4>
			</div>
			<div class="gutter10">
				<p>Page is coming soon</p>
			</div>
		</div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Help for NMHIX";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		