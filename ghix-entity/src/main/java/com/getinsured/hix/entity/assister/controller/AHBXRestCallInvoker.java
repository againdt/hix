package com.getinsured.hix.entity.assister.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.util.GhixEndPoints.AHBXEndPoints;


/**
 * Invokes the REST calls for EE to ghix-entity
 */
@Component
public class AHBXRestCallInvoker {

	private static final Logger LOGGER = LoggerFactory.getLogger(AHBXRestCallInvoker.class);

	@Autowired
	private RestTemplate restTemplate;

	/**
	 * Sends {@link Assister} details to AHBX (IND54)
	 * 
	 * @param assister
	 *            the assister object
	 */
	public String sendAssisterInfoForDelegationCode(Assister assister, Site primarySite) {

		String wsResponse = null;
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Assister. Assister Number: ").append(assister.getAssisterNumber());
		strResponse.append(" Assister Name: ");
		strResponse.append(assister.getFirstName()).append(" ");
		strResponse.append(assister.getLastName()).append(" ");

		try {

			DelegationRequest delegationRequest = populateDelegationRequestDTO(assister, primarySite);

			LOGGER.info("IND54: Sending Assister Information to Ghix-AHBX : Assister Number : "+assister.getAssisterNumber()+" : Assister ID : "+assister.getId());

			wsResponse = restTemplate.postForObject(AHBXEndPoints.DELEGATIONCODE_RESPONSE, delegationRequest,
					String.class);

			strResponse.append("\nResponse is: ").append(wsResponse);

			/*LOGGER.info("IND54: Successfully sent Assister Information over Rest call to Ghix-Ahbx. Received following response:"
					+ strResponse.toString());*/
			LOGGER.info("IND54: Successfully sent Assister Information over Rest call to Ghix-Ahbx. ");

		} catch (Exception exception) {

			LOGGER.error("IND54: Send Assister Information Web Service failed. Exception is :" , exception);
		}
		return wsResponse;
	}

	/**
	 * Populates the DelegationRequest parameter to be sent over rest call to
	 * ghix-ahbx
	 * 
	 * @param assister
	 *            {@ link Assister}
	 * @return the @ link DelegationRequest}
	 */
	private DelegationRequest populateDelegationRequestDTO(Assister assister, Site primarySite) {

		DelegationRequest delegationRequest = new DelegationRequest();

		delegationRequest.setRecordId(assister.getId());
		delegationRequest.setContactFirstName(assister.getEntity().getUser().getFirstName());
		delegationRequest.setContactLastName(assister.getEntity().getUser().getLastName());
		delegationRequest.setBusinessLegalName(assister.getEntity().getBusinessLegalName());
		
		// Assuming Entity Mailing Location to be Primary Site Location
		delegationRequest.setAddressLineOne(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation()
				.getAddress1() : null);
		delegationRequest.setAddressLineTwo(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation()
				.getAddress2() : null);
		delegationRequest.setCity(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation().getCity()
				: null);
		delegationRequest.setState(primarySite.getMailingLocation() != null ? primarySite.getMailingLocation()
				.getState() : null);
		delegationRequest.setZipCode(primarySite.getMailingLocation() != null ? String.valueOf(primarySite
				.getMailingLocation().getZip()) : null);

		delegationRequest.setAssisterFirstName(assister.getFirstName());
		delegationRequest.setAssisterLastName(assister.getLastName());
		delegationRequest.setFunctionalId(assister.getAssisterNumber());
		delegationRequest.setBusinessLegalName(assister.getEntity().getBusinessLegalName());

		delegationRequest.setCertificationStausCode(assister.getCertificationStatus());
		delegationRequest.setCertificationDate(assister.getStatusChangeDate() != null ? EntityUtils.formatDate(
				EntityConstants.DATE_FORMAT_PLAIN, assister.getStatusChangeDate()) : null);
		
		if(assister.getPrimaryPhoneNumber() != null) {
			delegationRequest.setPhoneNumber(Long.parseLong(assister.getPrimaryPhoneNumber()));
		}

		//delegationRequest.setPhoneNumber(assister.getPrimaryPhoneNumber() != null ? Long.parseLong(assister.getPrimaryPhoneNumber()) : null);
		delegationRequest.setEmailId(assister.getEmailAddress());

		delegationRequest.setCertificationId(assister.getCertificationNumber());
		delegationRequest.setEntityNumber(assister.getEntity().getEntityNumber());

		//Setting record type to Assister
		delegationRequest.setRecordType(EntityConstants.ASSISTER);
		return delegationRequest;
	}

}
