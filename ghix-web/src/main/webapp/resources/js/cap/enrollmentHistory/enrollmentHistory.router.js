(function(){
	'use strict';
	
	angular.module('enrollmentHistoryApp').config(routerConfig);

	angular.module('enrollmentHistoryApp').run(routerRun);

	
	
	routerConfig.$inject = ['$stateProvider', '$urlRouterProvider'];	
	function routerConfig($stateProvider, $urlRouterProvider){
		$urlRouterProvider.otherwise("/");
		
		$stateProvider.state('enrollmentHistory', {
			url: "/",
			templateUrl: "/hix/resources/html/cap/partials/enrollmentHistoryPartial.jsp",
			controller: 'EnrollmentHistoryCtrl'
		}).state('addOrChangeEnrollment', {
			url: "/addOrChangeEnrollment",
			templateUrl: "/hix/resources/html/cap/partials/addOrChangeEnrollmentTxn.jsp",
			controller: 'AddOrChangeEnrollmentTxnCtrl',
			controllerAs: 'vm',
			params: {
				'newEnrollment':undefined,
				'oldEnrollment':undefined,
				'isEditMode': undefined
			},
			resolve: {
            	checkEnrollment: function($stateParams, $state, $timeout){
            		if($stateParams.newEnrollment === undefined){
            			$timeout(function() {
                            $state.go('enrollmentHistory');
        				}, 0);
            		}
            	}
            }
		}).state('premiumHistory', {
			url: "/premiumHistory",
			templateUrl: "/hix/resources/html/cap/partials/premiumHistoryPartial.jsp",
			controller: 'PremiumHistoryCtrl',
			controllerAs: 'vm',
			params: {
				'newEnrollment':undefined,
				'oldEnrollment':undefined,
				'isEditMode': undefined,
				'fromPage': undefined,
				'isEnrollmentSaved': undefined
			},
			resolve: {
            	checkEnrollment: function($stateParams, $state, $timeout){
            		if($stateParams.newEnrollment === undefined){
            			$timeout(function() {
                            $state.go('enrollmentHistory');
        				}, 0);
            		}else{
            			angular.forEach($stateParams.newEnrollment.enrollmentPremiumDtoList, function(value, key){
            				if(value.isFinancial === false){
            					value.aptcAmount = 0;
            				}
            			});
            		}
            		
            	}
            }
		});
	}
	
	
	routerRun.$inject = ['$rootScope', '$anchorScroll'];
	
	function routerRun($rootScope, $anchorScroll){
		$rootScope.$on("$locationChangeSuccess", function() {
            $anchorScroll();
		});
	};
	
})();
