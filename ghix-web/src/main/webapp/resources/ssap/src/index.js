
import 'react-app-polyfill/stable';
import 'react-app-polyfill/ie9';

import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
// import { Router, Switch, Route,  } from 'react-router';


// Components

import App from './components/App';

// Redux Store

import appStore from './appStore';

// Browser history

// import history from './appHistory';

// Styles

import '../assets/sass/index.sass';

// Prototype extensions

import './utils/prototypeExtensions.js';

// Constants

// import CONFIG from './.env';

const root = document.getElementById('root');

ReactDom.render(
    <Provider store={appStore}>
        <App/>
    </Provider>,
    root
);
