package com.getinsured.iex.ssap.enums;

/**
 * 
 * @author raguram_p
 *
 *
 */

public enum GenderEnum {

	MALE("Male"), FEMALE("Female");

	private String s;

	GenderEnum(String str) {
		this.s = str;
	}

	public String value() {
		return s;
	}

	public static String value(String v) {
		if (v != null) {
			final GenderEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static GenderEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static GenderEnum readValue(String v) {
		GenderEnum data = null;
		for (GenderEnum c : GenderEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

}
