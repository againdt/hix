package com.getinsured.hix.batch.agency.service;

public interface AgencyRecordProcessor {
	
	public void process(String[] columns,int agencyColumnIndex,Long rowNo) throws Exception;
		
}
