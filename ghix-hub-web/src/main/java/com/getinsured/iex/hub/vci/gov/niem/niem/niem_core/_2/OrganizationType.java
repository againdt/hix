//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.29 at 08:34:31 AM IST 
//


package com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.vci.gov.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for a body of people organized for a particular purpose.
 * 
 * <p>Java class for OrganizationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="OrganizationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}OrganizationIdentification"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}OrganizationName"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "OrganizationType", propOrder = {
    "organizationIdentification",
    "organizationName"
})
public class OrganizationType
    extends ComplexObjectType
{

    @XmlElement(name = "OrganizationIdentification", required = true)
    protected IdentificationType organizationIdentification;
    @XmlElement(name = "OrganizationName", required = true)
    protected TextType organizationName;

    /**
     * Gets the value of the organizationIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getOrganizationIdentification() {
        return organizationIdentification;
    }

    /**
     * Sets the value of the organizationIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setOrganizationIdentification(IdentificationType value) {
        this.organizationIdentification = value;
    }

    /**
     * Gets the value of the organizationName property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getOrganizationName() {
        return organizationName;
    }

    /**
     * Sets the value of the organizationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setOrganizationName(TextType value) {
        this.organizationName = value;
    }

}
