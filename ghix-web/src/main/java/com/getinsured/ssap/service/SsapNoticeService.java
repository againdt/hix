package com.getinsured.ssap.service;

/**
 * Defines Notice service.
 *
 * @author Yevgen Golubenko
 * @since 2019-07-08
 */
public interface SsapNoticeService {
  /**
   * Sends notice to the primary applicant that additional information is required
   * for one of the applicants on this SSAP after unsuccessful VLP verification.
   *
   * @param caseNumber SSAP case number.
   */
  void sendAdditionalInformationNeeded(final String caseNumber);

  /**
   * Sends eligibility notification.
   * This is invoked after eligibility engine call.
   *
   * @param caseNumber SSAP case number.
   */
  void sendEligibilityNotification(final String caseNumber);

  /**
   * Sends eligibility notification if Notice Queuing is not turned on.
   * Otherwise queue the Eligibility Notice for future processing.
   * This is invoked after eligibility engine call.
   *
   * @param applicationID SSAP application ID
   * @param caseNumber SSAP case number.
   */
  void sendEligibilityNotification(final Long applicationID, final String caseNumber);
}
