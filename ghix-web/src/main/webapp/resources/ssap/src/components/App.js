import React, { Fragment, useEffect, useRef } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import TagManager from 'react-gtm-module';

// Components

import CommonHeaderContainer from '../containers/CommonHeaderContainer';
import ConnectedIntlProvider from '../utils/ConnectedIntlProvider';
import ErrorBoundary from './errorBoundary/ErrorBoundary';
import FooterContainer from '../components/layout/FooterContainer';
import HeaderContainer from '../components/layout/HeaderContainer';
import ModalContainer from '../containers/ModalContainer';
import NavBarContainer from '../containers/NavBarContainer';
import PagesContainer from '../containers/PagesContainer';
import Paginator from '../utils/Paginator';
import Spinner from './spinner/Spinner';
import SystemErrorMessage from './notifications/SystemErrorMessage';
import Webpack from '../utils/Webpack';
import DocumentTitle from '../components/DocumentTitle';

// Font awesome react setup

import { library } from '@fortawesome/fontawesome-svg-core';
import { faAsterisk, faHome, faEnvelope, faExclamation, faLock, faPhone, faTimes } from '@fortawesome/free-solid-svg-icons';
library.add(faAsterisk, faHome, faEnvelope, faExclamation, faLock, faPhone, faTimes);

// ActionCreators

import {
    adjustFlow,
    changeLanguage,
    fetchBloodRelationships,
    fetchInitialData,
    fetchFlow,
    fetchHeaderFooterData,
    hideModal,
    restoreSession,
    setFlow,
    setInstructions,
    setMessages,
    showModal
} from '../actions/actionCreators';

import { useGodMode } from '../hooks/useGodMode';

// Utils

import { getCurrentHouseholdMember } from '../utils/getCurrentHouseholdMember';
import { familyAnalyzer } from '../utils/familyAnalizer';
import { navigationBarEnhancer } from '../utils/navigationBarEnhancer';

// Constants

import CONFIG from '../.env';
import ENDPOINTS from '../constants/endPointsList';
import MASKS from '../constants/masksConstants';
import MODAL_TYPES from '../constants/modalTypes';
import { PAGES } from '../constants/pagesList';
import USER_ROLES from '../constants/userRoleTypes';
import { getCachedFlows } from '../constants/pagesList';
import FLOW_TYPES from '../constants/flowTypes';







const App = props => {

    const {
        adjustedFlow,
        data,
        data: {
            singleStreamlinedApplication: {
                applyingForFinancialAssistanceIndicator,
                currentPage,
                ssapApplicationId,
                taxHousehold
            }
        },
        environment: {
            stateCode,
            impersonationDetails,
            user,
            links: {
                PHONE
            }
        },
        error,
        error: {
            errorStatus
        },
        flow: {
            flowType,
            isFlowLoaded
        },
        householdMembers,
        householdMembers: {
            length: hhmsQty
        },
        headerLinks: {
            links: {
                DASHBOARD,
                LOGOUT
            },
            properties: {
                GOOGLE_ANALYTICS_CONTAINER_ID
            }
        },
        language,
        relations: {
            loadedRelations
        },
        modals,
        page,
        session: {
            inactiveIntervalMin,
            lastAccessedTime
        },
        spinner: {
            activeSpinnersQty,
            message
        }
    } = props;

    const {
        bloodRelationship
    } = taxHousehold[0].householdMember[0];

    const {
        primaryTaxFilerPersonId
    } = taxHousehold[0];

    const {
        hhmIndex,
        currentHhm
    } = getCurrentHouseholdMember(adjustedFlow, page, householdMembers);

    const familyConfig = {
        loadedRelations,
        bloodRelationships: bloodRelationship,
        currentMemberIndex: hhmIndex,
        householdMembers,
        primaryTaxFilerPersonId,
        MASKS
    };

    const familyStatus = hhmIndex !== null ? familyAnalyzer(familyConfig) : { currentHhm: {} };

    const btnRef = useRef();    // 'get-assistance_01' or 'get-assistance_02'
    const btn2Ref = useRef();   // 'my-account'
    const navRef = useRef();
    const ulRef = useRef();
    const ul2Ref = useRef();

    const refs = { btnRef, btn2Ref, navRef, ulRef, ul2Ref };

    useEffect(() => {
        if(stateCode === 'NV') {
            const handler = e => navigationBarEnhancer(e, btnRef, btn2Ref, navRef, ulRef, ul2Ref);
            window.addEventListener('click', handler);
            return () => {
                window.removeEventListener('click', handler);
            };
        }
    }, [ stateCode ]);


    /**
     * Formats language to 'es' or 'en' instead of 'English'/'Spanish'
     * Currently there is no API to provide a locale, and the active language is stored in session storage.
     * @returns {string}
     */
    const getFormattedLanguage = () => {
        const languageSessionProp = sessionStorage.getItem('language');
        let language = 'en';

        switch (languageSessionProp) {
        case 'English':
            language = 'en';
            break;
        case 'Spanish':
            language = 'es';
            break;
        default:
            language = 'en';
            break;
        }
        return language;
    };

    useEffect(() => {
        if (GOOGLE_ANALYTICS_CONTAINER_ID) {
            TagManager.initialize({gtmId: GOOGLE_ANALYTICS_CONTAINER_ID});
        }
    }, [GOOGLE_ANALYTICS_CONTAINER_ID]);

    useEffect(() => {
        if (CONFIG.ENV.PRODUCTION) {
            const language = getFormattedLanguage();
            props.actions.changeLanguage(language);
        }
    }, []);

    /**
     * Once a language has been changed - loads corresponding messages.
     */
    useEffect(() => {
        if (stateCode) {
            const stateLocale = stateCode.toLowerCase();
            import(/* webpackChunkName: "test" */ /* webpackMode: "eager" */
                `../../public/i18n/${stateLocale}/messages_${language}.json`)
                .then((messages) => {
                    props.actions.setMessages(messages, language);
                });
            import(`../../public/i18n/${stateLocale}/instructions.json`)
                .then(instructions => {
                    props.actions.setInstructions(instructions);
                });
        }
    }, [language, stateCode]);

    useEffect(() => {
        if(applyingForFinancialAssistanceIndicator) {
            flowType !== FLOW_TYPES.FINANCIAL_FLOW && props.actions.setFlow(FLOW_TYPES.FINANCIAL_FLOW);
        } else {
            flowType !== FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE && props.actions.setFlow(FLOW_TYPES.NON_FINANCIAL_FLOW_SWITCHABLE);
        }
    }, [ applyingForFinancialAssistanceIndicator ]);

    useEffect(() => {
        props.actions.adjustFlow(getCachedFlows(flowType, hhmsQty));
    }, [ flowType, hhmsQty ]);

    useEffect(() => {
        const timeout = setTimeout(() => {
            lastAccessedTime && props.actions.showModal(
                MODAL_TYPES.SESSION_EXPIRING_MODAL,
                MODAL_TYPES.SESSION_EXPIRING_MODAL,
                LOGOUT,
                [() => props.actions.restoreSession(`${window.location.origin}${ENDPOINTS.SESSION_PING}`)],
            );
        }, Math.round((inactiveIntervalMin * 0.65) * 60 * 1000));
        const sessionModal = modals.find(m => m.modalOwner === MODAL_TYPES.SESSION_EXPIRING_MODAL);
        !!sessionModal && props.actions.hideModal(sessionModal.modalOwner);
        return () => {
            clearTimeout(timeout);
        };
    }, [ lastAccessedTime ]);

    useEffect(() => {
        if (CONFIG.ENV.PRODUCTION) {
            stateCode && isFlowLoaded && props.actions.fetchInitialData(
                `${window.location.origin}${ENDPOINTS.INITIAL_DATA_ENDPOINT}${window.ssapApplicationObj.ssapId}`,
                [ impersonationDetails, user, USER_ROLES ],
                stateCode,
                flowType,
                getCachedFlows,
                PAGES
            );
        }
    }, [stateCode, isFlowLoaded]);

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.actions.restoreSession(`${window.location.origin}${ENDPOINTS.SESSION_PING}`);
        }
    }, []);

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.actions.fetchFlow(`${window.location.origin}${ENDPOINTS.FLOW_ENDPOINT}`);
        }
    }, []);

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            props.actions.fetchBloodRelationships(`${window.location.origin}${ENDPOINTS.BLOOD_RELATIONSHIPS_ENDPOINT}`, false);
        }
    }, []);

    useEffect(() => {

        if (CONFIG.ENV.PRODUCTION) {
            props.actions.fetchHeaderFooterData(`${window.location.origin}${ENDPOINTS.ENVIRONMENT_VARIABLES_FOR_HEADER}`);
        } else {
            props.actions.fetchHeaderFooterData(`${ENDPOINTS.ENVIRONMENT_VARIABLES_FOR_8888_PORT}`);
        }

    }, []);

    const getModals = modals => {
        return modals.map(modal => {
            return (
                <ModalContainer
                    key={modal.modalOwner}
                    { ...{ currentHhm, hhmIndex, modal } }
                />
            );
        });
    };

    const isHyperNavigationShown = useGodMode();

    return (
        <Fragment>
            <ConnectedIntlProvider>
                <>
                    {
                        errorStatus && !ssapApplicationId && <SystemErrorMessage {...{ data, DASHBOARD, error, PHONE }}/>
                    }
                    <DocumentTitle />
                    <HeaderContainer {...{refs}}/>
                    <ErrorBoundary>
                        {
                            modals.length !== 0 && getModals(modals)
                        }
                        {
                            activeSpinnersQty > 0 && <Spinner message={message}/>
                        }
                        {
                            ssapApplicationId ?

                                <main className="usa-grid usa-section usa-content usa-layout-docs main__section" id="main-content">
                                    {
                                        isHyperNavigationShown &&

                                        <Fragment>
                                            <div className="usa-width-one-whole">
                                                <Paginator/>
                                                <Webpack />
                                            </div>
                                        </Fragment>
                                    }

                                    {
                                        errorStatus ?

                                            <SystemErrorMessage {...{ data, DASHBOARD, error, PHONE }}/>

                                            :

                                            <Fragment>
                                                <aside className="usa-width-one-fourth usa-layout-docs-sidenav main__section__sidenav">
                                                    <NavBarContainer {...props}/>
                                                </aside>
                                                <div className="usa-width-three-fourths usa-layout-docs-main_content">
                                                    {
                                                        currentPage !== null &&

                                                        <Fragment>
                                                            <CommonHeaderContainer/>
                                                            <PagesContainer { ...{ hhmIndex, currentHhm, familyStatus } }/>
                                                        </Fragment>
                                                    }
                                                </div>
                                            </Fragment>
                                    }
                                </main> :
                                <div className='pre_load_placeholder'/>
                        }
                    </ErrorBoundary>
                    <FooterContainer />
                </>
            </ConnectedIntlProvider>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    adjustedFlow: state.adjustedFlow,
    data: state.data,
    environment: state.headerLinks,
    error: state.error,
    flow: state.flow,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    headerLinks: state.headerLinks,
    language: state.language,
    relations: state.relations,
    modals: state.modals,
    page: state.page,
    session: state.session,
    spinner: state.spinner,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        adjustFlow,
        changeLanguage,
        fetchBloodRelationships,
        fetchInitialData,
        fetchFlow,
        fetchHeaderFooterData,
        hideModal,
        restoreSession,
        setInstructions,
        setFlow,
        setMessages,
        showModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
