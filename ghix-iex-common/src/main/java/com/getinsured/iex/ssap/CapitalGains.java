
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class CapitalGains {

    @Expose
    private Double annualNetCapitalGains;

    public Double getAnnualNetCapitalGains() {
        return annualNetCapitalGains;
    }

    public void setAnnualNetCapitalGains(Double annualNetCapitalGains) {
        this.annualNetCapitalGains = annualNetCapitalGains;
    }

}
