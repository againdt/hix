import {headerFooterLinkTypes} from '../constants/actionTypes';

export const initialState = {
    tenantId: 0,
    devEnvironment: null,
    anonymous: null,
    user: {
        userId: null,
        firstName: '',
        lastName: '',
        role: '',
        landingPage: '',
        userName: '',
        activeModuleId: null,
        activeModuleName: ''
    },
    'stateCode': null,
    'exchangeName': '',
    'language': '',
    'links': {
        'CONTACT_US': '',
        'PHONE': '',
        'LOGIN': '',
        'CHAT': null,
        'UNREAD_MESSAGES': '',
        'SETTINGS': '',
        'AGENT_BROKER_SIGNUP': '',
        'LOGOUT': '',
        'HELP': '',
        'HOME': '',
        'ASSISTANCE': '#',
        'ENTITY_SIGNUP': '',
        'DASHBOARD': ''
    },
    'assets': {
        'FAV_ICON': '',
        'LOGO': ''
    },
    'properties': {
        'GOOGLE_ANALYTICS_CONTAINER_ID':'',
        'GOOGLE_ANALYTICS_CODE': ''
    },
    'impersonation': null,
    'impersonationDetails': {
        'role': '',
        'name': '',
        'switchBackLink': '',
        'moduleId': '',
    }
};

export default (state = initialState, action) => {
    switch (action.type) {
    case headerFooterLinkTypes.SET_ENVIRONMENTS:
        return action.headerLinks;
    case headerFooterLinkTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};