package com.getinsured.hix.tkm.service.jpa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.tkm.repository.ITkmQueuesRepository;
import com.getinsured.hix.tkm.service.TkmQueuesService;
import com.getinsured.hix.tkm.tkmException.TKMException;

@Service("tkmQueuesService")
@Repository
public class TkmQueuesServiceImpl implements TkmQueuesService {

	@Autowired private ITkmQueuesRepository iTkmQueuesRepository;
	
	@Override
	public List<TkmQueues> getQueuesList() {
		List<TkmQueues> queueList =iTkmQueuesRepository.findAll();
		
		return queueList;
	}

	@Override
	public TkmQueues findById(Integer id) {
		return iTkmQueuesRepository.findOne(id);
	}

	@Override
	public TkmQueues findByName(String name) {
		// TODO Auto-generated method stub
		return iTkmQueuesRepository.findByName(name);
	}
	
	@Override
	public List<Integer> getQueueIdlist() {
		List<Integer> queueIdList =iTkmQueuesRepository.findTkmQueueIds();
		
		return queueIdList;
	}
	
	@Override
	public List<Integer> getDefaultQueueIdlist() {
		List<Integer> queueIdList =iTkmQueuesRepository.findDefaultTkmQueueIds();
		return queueIdList;
	}
	
	@Override
	public void updateDefaultQueues(String defaultQueueIds) {
		String [] queueIdArray = defaultQueueIds.split("\\|");
		List<String> queueIdList = new ArrayList<String>(Arrays.asList(queueIdArray));
		List<TkmQueues> queuesList = getQueuesList();
		for(TkmQueues tkmQueues : queuesList) {
			if(queueIdList.contains(tkmQueues.getId().toString())) {
				tkmQueues.setIsDefault("Y");
			} else {
				tkmQueues.setIsDefault("N");
			}
			iTkmQueuesRepository.save(tkmQueues);
		}
	}
}
