package com.getinsured.hix.entity.enrollmententity.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.ISiteLocationHoursRepository;
import com.getinsured.hix.entity.enrollmententity.service.SiteLocationHoursService;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLocationHours;
/**
 * Implements {@link SiteService} to perform  SubSite associated SiteLocationHours
 * related operations like find,save,update etc.
 */
@Service("siteLocationHoursService")
@Transactional
public class SiteLocationHoursServiceImpl implements SiteLocationHoursService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteLocationHoursServiceImpl.class);
	
	@Autowired
	private  ISiteLocationHoursRepository  iSiteLocationHoursRepository;
	
	/**
	 * @see SiteLocationHoursService#findBySiteLocationHoursId(long)
	 */
	@Override
	public SiteLocationHours findBySiteLocationHoursId(long siteLocationHoursId) {
		
		LOGGER.info("findBySiteLocationHoursId: START");
		LOGGER.info("findBySiteLocationHoursId: END");
		
		
		return iSiteLocationHoursRepository.findBySiteLocationHoursId(siteLocationHoursId);
	}

	/**
	 * @see SiteLocationHoursService#findSiteLocationHoursBySite(site)
	 */
	@Override
	public List<SiteLocationHours> findSiteLocationHoursBySite(Site site) {
		
		LOGGER.info("findSiteLocationHoursBySite: START");
		LOGGER.info("findSiteLocationHoursBySite: END");
		
		
		return iSiteLocationHoursRepository.findSiteLocationHoursBySite(site);
	}
	
	/**
	 * @see SiteLocationHoursService#saveSiteLocationHours(site)
	 */
	@Override
	public void saveSiteLocationHours(Site site,
			List<SiteLocationHours> siteLocationHours) {
		
		LOGGER.info("saveSiteLocationHours: START");

		iSiteLocationHoursRepository.deleteSiteLocationHours(site.getId());		
		if (siteLocationHours != null && !siteLocationHours.isEmpty()) {
			for (SiteLocationHours hrs : siteLocationHours) {
				  hrs.setSite(site);
				  iSiteLocationHoursRepository.save(hrs);
			}
		}
		
		LOGGER.info("saveSiteLocationHours: END");
	}
}
