package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifIndividualResponseType;

/**
 * Wrapper Class for Agency3InitVerifIndividualResponseType
 * 
 * @author Nikhil Talreja
 * @since 14-Feb-2014
 * 
 */
public class Agency3InitVerifIndividualResponseTypeWrapper implements JSONAware{

	private Agency3InitVerifIndividualResponseType agency3InitVerifIndividualResponseType;
	
	private ResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private Agency3InitVerifIndividualResponseSetTypeWrapper agency3InitVerifIndividualResponseSet;
	
	public Agency3InitVerifIndividualResponseTypeWrapper(Agency3InitVerifIndividualResponseType agency3InitVerifIndividualResponseType){
		
		if(agency3InitVerifIndividualResponseType == null){
			return;
		}
		
		ResponseMetadataWrapper responseMetadataWrapper = new ResponseMetadataWrapper(agency3InitVerifIndividualResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
		this.lawfulPresenceVerifiedCode = agency3InitVerifIndividualResponseType.getLawfulPresenceVerifiedCode();
		
		Agency3InitVerifIndividualResponseSetTypeWrapper agency3InitVerifIndividualResponseSetTypeWrapper = 
				new Agency3InitVerifIndividualResponseSetTypeWrapper(agency3InitVerifIndividualResponseType.getAgency3InitVerifIndividualResponseSet());
		
		this.agency3InitVerifIndividualResponseSet = agency3InitVerifIndividualResponseSetTypeWrapper;
	}

	public Agency3InitVerifIndividualResponseType getAgency3InitVerifIndividualResponseType() {
		return agency3InitVerifIndividualResponseType;
	}

	public void setAgency3InitVerifIndividualResponseType(
			Agency3InitVerifIndividualResponseType agency3InitVerifIndividualResponseType) {
		this.agency3InitVerifIndividualResponseType = agency3InitVerifIndividualResponseType;
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public Agency3InitVerifIndividualResponseSetTypeWrapper getAgency3InitVerifIndividualResponseSet() {
		return agency3InitVerifIndividualResponseSet;
	}

	public void setAgency3InitVerifIndividualResponseSet(
			Agency3InitVerifIndividualResponseSetTypeWrapper agency3InitVerifIndividualResponseSet) {
		this.agency3InitVerifIndividualResponseSet = agency3InitVerifIndividualResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata);
		obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		obj.put("Agency3InitVerifIndividualResponseSet",this.agency3InitVerifIndividualResponseSet);
		return obj.toJSONString();
	}

}
