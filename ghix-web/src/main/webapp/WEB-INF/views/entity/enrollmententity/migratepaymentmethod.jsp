<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<div>
        <form action="<c:url value="/entity/enrollmententity/migratepaymentmethod"></c:url>" method="POST">
                <df:csrfToken/>
                <h4>Migration of  entity payment method</h4>
                <div>
                        <label for="pageSize" class="">Select No of Records to migrate</label>
                        <select size="1" id="noOfRecordsToMigrate" name="pageSize" class="input-medium">
                                <option value="<%=(GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT)%>"><%=(GhixConstants.PAYMENT_METHOD_MIGRATION_LIMIT)%></option>
                        </select>
                        <input id="submit" type="submit" value="Submit"/>
                </div>
        </form>
</div>