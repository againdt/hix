function unknown55ShowHide(){
	
	if($('input[name=appscr55paragraph]:radio:checked').val()== 'yes'){
		$('#contant_message_ui11').show();
	} /*else {
		
		if ($('input[name=appscr55paragraph]:radio:checked').val() == 'no' && $('input[name=checkTypeOfHelp4HealthInsurance]:radio:checked').val() == 'no') {
			//checkOut();
		}
	}*/
}

function checkOut() {
	alert(' Please contact your state health insurance exchange, to enroll in QHP');

	$("#appscr55").html($("#appscr55").html());
	window.location.reload();
}

/**
 * function for setting different content on for the div when different check
 * boxes selected
 */

//keys
function setDiffertContent() {
	var yesContent = '<b> '+jQuery.i18n.prop('ssap.page5.weEncourageText1')+'</b> '+jQuery.i18n.prop('ssap.page5.weEncourageText2')+ jQuery.i18n.prop('ssap.page5.weEncourageText3');
	var noContent = '<b>'+jQuery.i18n.prop('ssap.page5.BasedOnText1')+'</b>' + jQuery.i18n.prop('ssap.page5.BasedOnText2');
	var iDontKnowContent = '<b>'+jQuery.i18n.prop('ssap.page5.weEncourageText1')+'</b>'+jQuery.i18n.prop('ssap.page5.WeEncourageDontNoText2')+ jQuery.i18n.prop('ssap.page5.WeEncourageDontNoText3');
	if ($("#page55radio6").is(":checked")) {
		$("#contant_message_ui11").html(yesContent);
	} else if ($("#page55radio7").is(":checked")) {
		if ($("#page55radio10").is(":checked"))
			unknown55ShowHide();
		else
			$("#contant_message_ui11").html(noContent);
	} else if ($("#page55radio8").is(":checked")) {
		$("#contant_message_ui11").html(iDontKnowContent);
	}
}