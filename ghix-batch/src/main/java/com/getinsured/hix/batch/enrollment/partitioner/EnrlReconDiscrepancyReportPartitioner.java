package com.getinsured.hix.batch.enrollment.partitioner;

import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrlReconDiscrepancyReportService;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;

@Component("enrlReconDiscrepancyReportPartitioner")
@Scope("step")
public class EnrlReconDiscrepancyReportPartitioner implements Partitioner {
	@Value("#{stepExecution}")
	private StepExecution stepExecution;
	private JobService jobService;
	private String hiosIssuerIds;
	private String strYear;
	private int applicableYear;
	private String fileId;
	private EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportPartitioner.class);
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {
		Map<String, ExecutionContext> partitionMap = new HashMap<String, ExecutionContext>();
		ExecutionContext executionContext = null;
		try{
			setApplicableYear();
			if(stepExecution!=null){
				executionContext=stepExecution.getJobExecution().getExecutionContext();
				if(executionContext!=null){
					executionContext.put("year", applicableYear);
				}
			}
			if(null != fileId && NumberUtils.isNumber(fileId)) {
				LOGGER.info("EnrlReconDiscrepancyReportPartitioner : partition():: File Id received :: "+fileId);
				ExecutionContext value = new ExecutionContext();
				value.putInt("year", applicableYear);
				value.putString("fileId", fileId);
				partitionMap.put("partition - " + fileId, value);
				LOGGER.debug("EnrlReconDiscrepancyReportPartitioner : partition():: partition map entry end for partition :: "+fileId);
			}else {
				List<String> hiosIdList =  getHiosIdList();
				LOGGER.info("Year ::"+applicableYear+ " HiosIssuerId received ::" + hiosIdList);
				for(String hiosIssuerId : hiosIdList){
					putToBatchPartitionMap(partitionMap, hiosIssuerId);
				}
			}
		}catch(Exception e){
			LOGGER.error("EnrlReconDiscrepancyReportPartitioner failed to execute : "+ e.getMessage(), e);
			throw new RuntimeException("EnrlReconDiscrepancyReportPartitioner failed to execute : " + e.getMessage()) ;
		}
		return partitionMap;
	}
	
	private void putToBatchPartitionMap(Map<String, ExecutionContext> partitionMap, String hiosIssuerId) {
		ExecutionContext value = new ExecutionContext();
		value.putInt("year", applicableYear);
		value.putString("hiosIssuerId", hiosIssuerId);
		partitionMap.put("partition - " + hiosIssuerId, value);
		LOGGER.debug("EnrlReconDiscrepancyReportPartitioner : partition():: partition map entry end for partition :: "+hiosIssuerId);
	}

	
	private List<String> getHiosIdList() {
		List<String> hiosIdList = null;
		if(EnrollmentUtils.isNotNullAndEmpty(hiosIssuerIds)){
			String[] hiosIdArr =hiosIssuerIds.split("[|]");
			hiosIdList = Arrays.asList(hiosIdArr);
		}else{
			//Get list from summary table
			hiosIdList = enrlReconDiscrepancyReportService.getHiosIdListForReportGeneration(applicableYear);
		}
		return hiosIdList;
	}
	
	/**
	 * @return the jobService
	 */
	public JobService getJobService() {
		return jobService;
	}

	/**
	 * @param jobService the jobService to set
	 */
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	/**
	 * @return the hiosIssuerIds
	 */
	public String getHiosIssuerIds() {
		return hiosIssuerIds;
	}

	/**
	 * @param hiosIssuerIds the hiosIssuerIds to set
	 */
	public void setHiosIssuerIds(String hiosIssuerIds) {
		this.hiosIssuerIds = hiosIssuerIds;
	}

	/**
	 * @return the strYear
	 */
	public String getStrYear() {
		return strYear;
	}

	/**
	 * @param strYear the strYear to set
	 */
	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	
	private void setApplicableYear() throws GIException{
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if(NumberUtils.isNumber(strYear)){
			intYear = Integer.parseInt(strYear);
			if(intYear <= currentYear){
				applicableYear = intYear;
			}else{
				throw new GIException("Please provide valid month(mm) in the range [1,12] (Jan - Dec) and year <= current year");
			}
		}else if(null == strYear){
			applicableYear = currentYear;
		}

	}

	public String getFileId() {
		return fileId;
	}

	public void setFileId(String fileId) {
		this.fileId = fileId;
	}

	public EnrlReconDiscrepancyReportService getEnrlReconDiscrepancyReportService() {
		return enrlReconDiscrepancyReportService;
	}

	public void setEnrlReconDiscrepancyReportService(EnrlReconDiscrepancyReportService enrlReconDiscrepancyReportService) {
		this.enrlReconDiscrepancyReportService = enrlReconDiscrepancyReportService;
	}
}
