﻿define(function () {
    var grid = new baseComponent();
    grid.type = "grid";
    grid._addToScope = [];
    grid.preInit = function () {
        var vm = this;
        vm.context.on(vm.context.events.BEFORE_CONTROLLER_CALLED, {}, function (e) {
            var $scope = e.evData;          
            for (var k = 0; k < vm._addToScope.length; k++) {
                var sObj = vm._addToScope[k];
                $scope[sObj.key] = sObj.value;
            };
        });
    };
    grid.getHTML = function (cgfObj) {
        var defCfg = {
            scrollable: false,
            sortable: true,
            pageable: true,
            dataSource: {
                    type: "json",
                    serverPaging: true,
                    serverSorting: true,
                    serverFiltering: true,
                    allowUnsort: true,
                    pageSize: 15,
                    transport: {
                        read: {
                            url: "",
                            type: "POST",
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            data: {}

                        },
                        parameterMap: function (options) {
                            return JSON.stringify(options);
                        }
                    },
                    schema: {
                        model: {
                            fields: {
                                
                            }
                        },
                        data: "",
                        total: ""
                    }
            },
            filterable: {
                extra: false,
                operators: {
                    string: {
                        startswith: "Starts with",
                        eq: "Is equal to",
                        neq: "Is not equal to"
                    }
                }
            },
            columns: [
                   
                ]
        };
        cgfObj = $.extend(true, defCfg, cgfObj);
        log(cgfObj);
        cgfObj.dataSource = new kendo.data.DataSource(cgfObj.dataSource);
        this._addToScope.push(            {
            key: cgfObj.name + "Options",
                value: cgfObj
            });

        var $retObj = $("<div id=\"" + cgfObj.name + "\"></div>");
        $retObj.append('<kendo-grid id="kGrid" options="' + cgfObj.name + 'Options"></kendo-grid>');
        return $retObj[0].outerHTML;
    }
    return grid;
});