
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class IncarcerationStatus {

    @Expose
    private Boolean incarcerationManualVerificationIndicator=false;
    @Expose
    private Boolean incarcerationStatusIndicator=false;
    @Expose
    private Boolean useSelfAttestedIncarceration=false;
    @Expose
    private Boolean incarcerationPendingDispositionIndicator = null; // UI doesn't want anything preselected until user picks.
    @Expose
    private Boolean incarcerationAsAttestedIndicator=false;

    public Boolean getIncarcerationManualVerificationIndicator() {
        return incarcerationManualVerificationIndicator;
    }

    public void setIncarcerationManualVerificationIndicator(Boolean incarcerationManualVerificationIndicator) {
        this.incarcerationManualVerificationIndicator = incarcerationManualVerificationIndicator;
    }

    public Boolean getIncarcerationStatusIndicator() {
        return incarcerationStatusIndicator;
    }

    public void setIncarcerationStatusIndicator(Boolean incarcerationStatusIndicator) {
        this.incarcerationStatusIndicator = incarcerationStatusIndicator;
    }

    public Boolean getUseSelfAttestedIncarceration() {
        return useSelfAttestedIncarceration;
    }

    public void setUseSelfAttestedIncarceration(Boolean useSelfAttestedIncarceration) {
        this.useSelfAttestedIncarceration = useSelfAttestedIncarceration;
    }

    public Boolean getIncarcerationPendingDispositionIndicator() {
        return incarcerationPendingDispositionIndicator;
    }

    public void setIncarcerationPendingDispositionIndicator(Boolean incarcerationPendingDispositionIndicator) {
        this.incarcerationPendingDispositionIndicator = incarcerationPendingDispositionIndicator;
    }

    public Boolean getIncarcerationAsAttestedIndicator() {
        return incarcerationAsAttestedIndicator;
    }

    public void setIncarcerationAsAttestedIndicator(Boolean incarcerationAsAttestedIndicator) {
		this.incarcerationAsAttestedIndicator = incarcerationAsAttestedIndicator;
    }

}
