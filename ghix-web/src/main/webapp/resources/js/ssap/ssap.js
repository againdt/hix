/*var ssapHomeAddressValidationFlag = true;
var ssapMailingAddressValidationFlag = true;*/

/*
****************************************************************************************************************
* incomeCalculation.js                                                                                           *
****************************************************************************************************************
*/
function getIncomeData(name){
	var divName = "incomeCalculation_"+name;
	var strFirst = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.incomeCalculationFor')+" #NAME</b><hr/><table width='270px'><tr align='left'><th>"+jQuery.i18n.prop('ssap.incomeType')+"</th><th></th><th>"+jQuery.i18n.prop('ssap.amount')+"</th></tr>";
	var strFirstZero = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.incomeCalculationFor')+" #NAME</b><hr/><table>";
	var strTotal = " <tr><td></td><td colspan='3'><hr></td></tr><tr><th></th><th>"+jQuery.i18n.prop('ssap.total')+" (- 5%) </th><th class='alignRight'>#AMOUNT</th></tr></table></div>";
	var strAmt = " <tr><td class='italic'>#INCOMETYPE</td><td class='alignRight'>#MATH</td><td class='alignRight'>#AMOUNT</td></tr>";
	var zeroIncomeTr = " <tr><td colspan='4'><b>"+jQuery.i18n.prop('ssap.noIncome')+"</b></td></tr><td colspan='4'><hr></td></tr></table></div>";
	var div = "";

	div += strFirst.replace("#NAME", replaceall(name, '_', ' '));

	var zeroIncomeFlag = false;

	$.ajax({
		type : 'GET',
		url : 'SSAController/INCOME',
		dataType : 'json',
		data : {
			targetName : name
		},
		async : true,
		success : function(response) {

			for(var prop in response){
				var strToAppend = "";
				if(response[prop]['title']== 'MAGIFinalAmount'){
					strToAppend = strTotal.replace('#AMOUNT', response[prop]['amount']);
					div += strToAppend;

					if(parseInt(response[prop]['amount']) <= 0){
						zeroIncomeFlag = true;
						break;
					}

					break;
				}
				else{

					if(parseInt(response[prop]['amount'])<=0){
						continue;
					}

					strToAppend = strAmt.replace('#INCOMETYPE', response[prop]['title']);
					strToAppend = strToAppend.replace('#AMOUNT', response[prop]['amount']);
					strToAppend = strToAppend.replace('#MATH', response[prop]['type']);
					div += strToAppend;
				}

			}

			if(zeroIncomeFlag){
				div = strFirstZero.replace("#NAME", replaceall(name, '_', ' '));
				div += zeroIncomeTr;
			}

			var targetId = "table_incomeCalculation_"+name;

			setToolTipData(targetId , div);


		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			alert('error');
			$('#waiting').hide();
		}
	});


}

//Keys
function getIncomeDataAPTC(name){
	var divName = "incomeCalculationAPTC_"+name;

	var strFirst = "<div class='color333' id='"+divName+"'><b>"+jQuery.i18n.prop('ssap.annualIncomeCalculationFor')+" #NAME</b><hr/><table><tr><th>"+jQuery.i18n.prop('ssap.incomeType')+"</th><th></th><th>"+jQuery.i18n.prop('ssap.amount')+"</th></tr>";

	var strFirstZero = "<div class='color333' id='"+divName+"'><b>Annual Income Calculation for: #NAME</b><hr/><table>";

	var strTotal = " <tr><td></td><td colspan='3'><hr></td></tr><tr><th></th><th>Total </th><th class='alignRight'>#AMOUNT</th></tr></table></div>";

	var strAmt = " <tr><td class='italic'>#INCOMETYPE</td><td class='alignRight'>#MATH</td><td class='alignRight'>#AMOUNT</td></tr>";

	var zeroIncomeTr = " <tr><td colspan='4'><b>"+jQuery.i18n.prop('ssap.noIncome')+"</b></td></tr><td colspan='4'><hr></td></tr></table></div>";

	var div = "";

	div += strFirst.replace("#NAME", replaceall(name, '_', ' '));

	var zeroIncomeFlag = false;

	$.ajax({
		type : 'GET',
		url : 'SSAController/INCOME',
		dataType : 'json',
		data : {
			targetName : name
		},
		async : true,
		success : function(response) {

			for(var prop in response){
				var strToAppend = "";
				if(response[prop]['title']== 'MAGIFinalAmount'){
					strToAppend = strTotal.replace('#AMOUNT', ((parseInt(response[prop]['amount'])*100*12)/95));
					div += strToAppend;

					if(parseInt(response[prop]['amount']) <= 0){
						zeroIncomeFlag = true;
						break;
					}

					break;
				}
				else{

					if(parseInt(response[prop]['amount'])<=0){
						continue;
					}

					strToAppend = strAmt.replace('#INCOMETYPE', response[prop]['title']);
					strToAppend = strToAppend.replace('#AMOUNT', (parseInt(response[prop]['amount'])*12));
					strToAppend = strToAppend.replace('#MATH', response[prop]['type']);
					div += strToAppend;
				}

			}

			if(zeroIncomeFlag){
				div = strFirstZero.replace("#NAME", replaceall(name, '_', ' '));
				div += zeroIncomeTr;
			}

			var targetId = "table_incomeCalculationAPTC_"+name;

			setToolTipData(targetId , div);


		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			alert('error');
			$('#waiting').hide();
		}
	});
}

/*
****************************************************************************************************************
* continueFunctionality.js                                                                                         *
****************************************************************************************************************
*/
function UpdateAddressDetailsNonfinancial(i)
{
	if(validation(currentPage)==false)
		return false;
	var home_addressLine1=$('#home_addressLine1').val();
	var home_addressLine2=$('#home_addressLine2').val();
	var home_primary_city=$('#home_primary_city').val();
	var home_primary_zip=$('#home_primary_zip').val();
	var home_primary_state=$('#home_primary_state').val();
	var liveAtOtherAddressIndicator=false;
	var mailing_addressLine1=$('#mailing_addressLine1').val();
	var mailing_addressLine2=$('#mailing_addressLine2').val();
	var mailing_primary_city=$('#mailing_primary_city').val();
	var mailing_primary_zip=$('#mailing_primary_zip').val();
	var mailing_primary_state=$('#mailing_primary_state').val();
	var mailing_primary_county=$('#mailing_primary_county').val();
	var houseHoldPhone1=$('#first_phoneNo').val();
	var houseHoldPhoneExt1=$('#first_ext').val();
	var houseHoldPhoneType1=$('#first_phoneType').val();
	var houseHoldPhone2=$('#second_phoneNo').val();
	var houseHoldPhoneExt2=$('#second_ext').val();
	var houseHoldPhoneType2=$('#second_phoneType').val();
	var houseHoldSpokenLanguag=$('#preffegred_spoken_language').val();
	var houseHoldWrittenLanguag=$('#preffered_written_language').val();
	var isTemporarilyLivingOutsideIndicator=true;
	var otherAddressOfHouseHold="";
	if(i==0)
	{
		$("#appscr57HouseHoldhome_addressLine1TD"+(i+1)).html(home_addressLine1);
		$("#appscr57HouseHoldhome_addressLine2TD"+(i+1)).html(home_addressLine2);
		$("#appscr57HouseHoldhome_primary_cityTD"+(i+1)).html(home_primary_city);
		$("#appscr57HouseHoldhome_primary_zipTD"+(i+1)).html(home_primary_zip);
		$("#appscr57HouseHoldhome_primary_stateTD"+(i+1)).html(home_primary_state);
		$("#appscr57HouseHoldmailing_addressLine1TD"+(i+1)).html(mailing_addressLine1);
		$("#appscr57HouseHoldmailing_addressLine2TD"+(i+1)).html(mailing_addressLine2);
		$("#appscr57HouseHoldmailing_primary_cityTD"+(i+1)).html(mailing_primary_city);
		$("#appscr57HouseHoldmailing_primary_zipTD"+(i+1)).html(mailing_primary_zip);
		$("#appscr57mailing_primary_stateTD"+(i+1)).html(mailing_primary_state);
		$("#appscr57HouseHoldmailing_primary_countyTD"+(i+1)).html(mailing_primary_county);
		$("#appscr57houseHoldPhoneFirstTD"+(i+1)).html(houseHoldPhone1);
		$("#appscr57houseHoldPhoneExtFirstTD"+(i+1)).html(houseHoldPhoneExt1);
		$("#appscr57houseHoldPhoneTypeFirstTD"+(i+1)).html(houseHoldPhoneType1);
		$("#appscr57houseHoldPhoneSecondTD"+(i+1)).html(houseHoldPhone2);
		$("#appscr57houseHoldPhoneExtSecondTD"+(i+1)).html(houseHoldPhoneExt2);
		$("#appscr57houseHoldPhoneTypeSecondTD"+(i+1)).html(houseHoldPhoneType2);
		$("#appscr57houseHoldSpokenLanguageTD"+(i+1)).html(houseHoldSpokenLanguag);
		$("#appscr57houseHoldWrittenLanguageTD"+(i+1)).html(houseHoldWrittenLanguag);
		$("#appscr57liveAtOtherAddressIndicatorTD"+(i+1)).html(liveAtOtherAddressIndicator);
		$("#appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)).html(isTemporarilyLivingOutsideIndicator);
		$("#appscr57otherAddressState"+(i+1)).html(home_primary_state);
	}
	else
	{
		var actualName=getNameByTRIndex(i);
		home_addressLine1=$('#applicant_or_non-applican_address_1'+actualName).val();
		home_addressLine2=$('#applicant_or_non-applican_address_2'+actualName).val();
		home_primary_city=$('#city'+actualName).val();
		home_primary_zip=$('#zip'+actualName).val();
		otherAddressOfHouseHold=$('#applicant_or_non-applican_state'+actualName).val();

		  if ($('#householdContactAddress'+actualName).is(':checked')) {
			  liveAtOtherAddressIndicator=true;
			  home_primary_state="MI";
		  }
		  else
		  {
		  liveAtOtherAddressIndicator=false;
		  home_addressLine1=$('#home_addressLine1').val();
		  home_addressLine2=$('#home_addressLine2').val();
		  home_primary_city=$('#home_primary_city').val();
		  home_primary_zip=$('#home_primary_zip').val();
		  home_primary_state=$('#home_primary_state').val();
		  otherAddressOfHouseHold=home_primary_state;
		  }

  mailing_primary_state=home_primary_state;

  isTemporarilyLivingOutsideIndicator = $('input[name=isTemporarilyLivingOutsideIndicator'+actualName+']:radio:checked').val();
  	$("#appscr57HouseHoldhome_addressLine1TD"+(i+1)).html(home_addressLine1);
	$("#appscr57HouseHoldhome_addressLine2TD"+(i+1)).html(home_addressLine2);
	$("#appscr57HouseHoldhome_primary_cityTD"+(i+1)).html(home_primary_city);
	$("#appscr57HouseHoldhome_primary_zipTD"+(i+1)).html(home_primary_zip);
	$("#appscr57HouseHoldhome_primary_stateTD"+(i+1)).html(home_primary_state);

	$("#appscr57HouseHoldmailing_addressLine1TD"+(i+1)).html(mailing_addressLine1);
	$("#appscr57HouseHoldmailing_addressLine2TD"+(i+1)).html(mailing_addressLine2);
	$("#appscr57HouseHoldmailing_primary_cityTD"+(i+1)).html(mailing_primary_city);
	$("#appscr57HouseHoldmailing_primary_zipTD"+(i+1)).html(mailing_primary_zip);
	$("#appscr57mailing_primary_stateTD"+(i+1)).html(mailing_primary_state);
	$("#appscr57HouseHoldmailing_primary_countyTD"+(i+1)).html(mailing_primary_county);

	$("#appscr57houseHoldPhoneFirstTD"+(i+1)).html(houseHoldPhone1);
	$("#appscr57houseHoldPhoneExtFirstTD"+(i+1)).html(houseHoldPhoneExt1);
	$("#appscr57houseHoldPhoneTypeFirstTD"+(i+1)).html(houseHoldPhoneType1);
	$("#appscr57houseHoldPhoneSecondTD"+(i+1)).html(houseHoldPhone2);
	$("#appscr57houseHoldPhoneExtSecondTD"+(i+1)).html(houseHoldPhoneExt2);
	$("#appscr57houseHoldPhoneTypeSecondTD"+(i+1)).html(houseHoldPhoneType2);
	$("#appscr57houseHoldSpokenLanguageTD"+(i+1)).html(houseHoldSpokenLanguag);
	$("#appscr57houseHoldWrittenLanguageTD"+(i+1)).html(houseHoldWrittenLanguag);

	$("#appscr57liveAtOtherAddressIndicatorTD"+(i+1)).html(liveAtOtherAddressIndicator);
	$("#appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)).html(isTemporarilyLivingOutsideIndicator);
	$("#appscr57otherAddressState"+(i+1)).html(otherAddressOfHouseHold+'');
	}
}

function updateSpecialCircumstances(i)
{

	if(validation(currentPage)==false)
		return false;
		noOfHouseHold = $('#numberOfHouseHold').text();
		if(document.getElementById("disability"+i).checked == true)
			$("#appscr57HouseHoldDisability"+(i)).html("yes");
		else
			$("#appscr57HouseHoldDisability"+i).html("no");

		if(document.getElementById("assistanceServices"+i).checked == true)
			$("#appscr57HouseHoldAssistanceServices"+i).html("yes");
		else
			$("#appscr57HouseHoldAssistanceServices"+i).html("no");

		if(document.getElementById("native"+i).checked == true)
			$("#appscr57HouseHoldNative"+i).html("yes");
		else
			$("#appscr57HouseHoldNative"+i).html("no");

		if(document.getElementById("personfosterCare"+i).checked == true)
			$("#appscr57HouseHoldPersonfosterCare"+i).html("yes");
		else
			$("#appscr57HouseHoldPersonfosterCare"+i).html("no");

		$("#appscr57HouseHoldIsPersonGettingHealthCare"+i).html($('input[name=isPersonGettingHealthCare'+i+']:radio:checked').val());
		if(document.getElementById("deceased"+i).checked == true)
			$("#appscr57HouseHoldDeceased"+i).html("yes");
		else
			$("#appscr57HouseHoldDeceased"+i).html("no");

		 if($('#appscrHouseHoldgender57TD'+i).text() == 'female')
			 {

			 if(	document.getElementById("pregnancy"+i)!=null && document.getElementById("pregnancy"+i).checked == true)
				 {
				 $(".i_pregnant").html("yes");
				 $("#appscr57HouseHoldFetusCount"+i).text($('#number-of-babies option:selected').val());
				 }
			 else
				 {

				 	 $(".i_pregnant").html("no");
					 $("#appscr57HouseHoldFetusCount"+i).text("0");
				 }
			 }
		 else
			 {

			 $(".i_pregnant").html("no");
			 $("#appscr57HouseHoldFetusCount"+i).html("0");
			 }
}

function updateIncomeDiscrepancies()
{
	//var i =($('#houseHoldTRNo').val()).substring(9);
	var i = getCurrentApplicantID();
	$("#appappscr57HouseHoldExplanationTextAreaTD"+i).html($('#explanationTextArea').val());

	$("#appscrStopWorking57TD"+i).html($('input[name=stopWorking]:radio:checked').val());
	$("#appscrWorkAt57TD"+i).html($('input[name=workAt]:radio:checked').val());
	$("#appscrDecresedAt57TD"+i).html($('input[name=wecresedAt]:radio:checked').val());
	$("#appscrSalaryAt57TD"+i).html($('input[name=salaryAt]:radio:checked').val());
	$("#appscrSesWorker57TD"+i).html($('input[name=sesWorker]:radio:checked').val());
	$("#appscrExplainText57TD"+i).html($('#appscr73ExplanationTaxtArea').val());
	$("#appscrOtherJointIncome57TD"+i).html($('input[name=OtherJointIncome]:radio:checked').val());

}

function updateIncarceratedDetail(i)
{
		if(document.getElementById("incarcerationCB"+i).checked == true)
			$("#appscr85incarcerationTD"+i).html("yes");
		else
			$("#appscr85incarcerationTD"+i).html("no");

		$("#appscr85disposition"+i).html($('input[name=disposition'+i+']:radio:checked').val());

}
function updateFinancialIncomeDetails()
{

		var dataIndex=0;
		$("#incomesStatus"+financialHouseHoldDone).html($('input[name=appscr70radios4Income]:radio:checked').val());
		for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {


		if(dataIndex==0)
		{
			var irsIncome= $('#irsIncome').val();
				irsIncome = (irsIncome == '')?0:irsIncome;
				$("#appscrirsIncome57TD"+financialHouseHoldDone).html(irsIncome);
				$('#irsIncome').val('');

		continue;
		}
		var data = financial_DetailsArray[dataIndex];
		var fieldData = $('#'+data+'_id').val();
		if(fieldData == '' || fieldData == undefined)
			{
				fieldData = 0;
			}

		incomeAmount[data]=fieldData;


		var fieldFrequencyData= $('#'+data+'_select_id').val();

		incomeFrequency[data]=fieldFrequencyData;


			$("#appscr"+data+"57TD"+financialHouseHoldDone).html(fieldData);
			if(dataIndex!=2 && dataIndex!=6)
			$("#appscr"+data+"Frequency57TD"+financialHouseHoldDone).html(fieldFrequencyData);
		}
		/*var expectedIncomeByHouseHoldMember= $('#expectedIncomeByHouseHoldMember').val();
		if(expectedIncomeByHouseHoldMember=='')
			expectedIncomeByHouseHoldMember="N/A";

		$("#appscrexpectedIncome57TD"+financialHouseHoldDone).html(expectedIncomeByHouseHoldMember);*/


		$('#appscrFinancial_JobEmployerName57TD'+financialHouseHoldDone).text($('#Financial_Job_EmployerName_id').val());
		$('#appscrFinancial_JobHoursOrDays57TD'+financialHouseHoldDone).text($('#Financial_Job_HoursOrDays_id').val());
		$('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+financialHouseHoldDone).text($('#Financial_Self_Employment_TypeOfWork').val());
		$('#appscrFinancial_UnemploymentStateGovernment57TD'+financialHouseHoldDone).text($('#Financial_Unemployment_StateGovernment').val());
		$('#appscrFinancial_UnemploymentDate57TD'+financialHouseHoldDone).text($('#unemploymentIncomeDate').val());
		$('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+financialHouseHoldDone).text($('#Financial_OtherTypeIncome').val());


		$('#appscrStopWorking57TD'+financialHouseHoldDone).text($('input[name=stopWorking]:radio:checked').val());
		$('#appscrWorkAt57TD1'+financialHouseHoldDone).text($('input[name=workAt]:radio:checked').val());
		$('#appscrDecresedAt57TD'+financialHouseHoldDone).text($('input[name=decresedAt]:radio:checked').val());
		$('#appscrSalaryAt57TD'+financialHouseHoldDone).text($('input[name=salaryAt]:radio:checked').val());
		$('#appscr57HouseHoldExplanationTextAreaTD'+financialHouseHoldDone).text($('#explanationTextArea').val());
		$('#appscrSesWorker57TD'+financialHouseHoldDone).text($('input[name=sesWorker]:radio:checked').val());
		$('#appscrExplainText57TD'+financialHouseHoldDone).text($('#appscr73ExplanationTaxtArea').val());
		$('#appscrOtherJointIncome57TD'+financialHouseHoldDone).text($('input[name=OtherJointIncome]:radio:checked').val());
}

function updateNonFinancialDetailsToTable()
{


	 HouseHoldgender = $('input[name=appscr61_gender]:radio:checked').val();
	 HouseHoldHavingSSN =$('input[name=socialSecurityCardHolderIndicator]:radio:checked').val();
	 /*HouseHoldssn1=  $('#ssn1').val();
	 HouseHoldssn2=  $('#ssn2').val();
	 HouseHoldssn3=  $('#ssn3').val();
	 HouseHoldSSN = HouseHoldssn1 +'-'+HouseHoldssn2+'-'+HouseHoldssn3;*/
	 HouseHoldSSN = $('#ssn').val();
	 notAvailableSSNReason = $('#reasonableExplanationForNoSSN').val();
	 UScitizen= $('input[name=UScitizen]:radio:checked').val();
	 var UScitizenManualVerification= $('input[name=UScitizenManualVerification]:radio:checked').val();
	 naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();
	 livedIntheUSSince1996Indicator= $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();


	 certificateType = $('input[name=naturalizedCitizen]:radio:checked').val();
	 certificateAlignNumber = "";
	 certificateNumber = "";

	 documentSameNameIndicator = $('input[name=UScitizen62]:radio:checked').val();
	 documentFirstName = $('#documentFirstName').val();
	 documentLastName = $('#documentLastName').val();
	 documentMiddleName = $('#documentMiddleName').val();
	 documentSuffix = $('#documentSuffix').val();


	 var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').prop('checked');
	 if(naturalCitizen_eligibleImmigrationStatus=='')
		 {
		 naturalCitizen_eligibleImmigrationStatus='false';
		 }

	 if(getInfantData(nonFinancialHouseHoldDone)=='yes')
	 {

			 $('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html("male");
			 $('#appscr57HouseHoldHavingSSNTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57HouseHoldSSNTD'+nonFinancialHouseHoldDone).html("--");
			 $('#appscr57notAvailableSSNReasonTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57UScitizenTD'+nonFinancialHouseHoldDone).html(UScitizen);
			 $('#appscr57UScitizenManualVerificationTD'+nonFinancialHouseHoldDone).html(UScitizenManualVerification);
			 $('#appscr57livedIntheUSSince1996IndicatorTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57naturalizedCitizenshipIndicatorTD'+nonFinancialHouseHoldDone).html("no");
			 $('#appscr57ImmigrationStatusTD'+nonFinancialHouseHoldDone).html(naturalCitizen_eligibleImmigrationStatus);

			 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html('None');
			 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentSameNameIndicatorTD'+nonFinancialHouseHoldDone).html('yes');
			 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html('');
			 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html('');
			 $('appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html('Suffix');
	 }
 else{

	 $('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html(HouseHoldgender);
	 $('#appscr57HouseHoldHavingSSNTD'+nonFinancialHouseHoldDone).html(HouseHoldHavingSSN);
	 $('#appscr57HouseHoldSSNTD'+nonFinancialHouseHoldDone).html(HouseHoldSSN);
	 $('#appscr57notAvailableSSNReasonTD'+nonFinancialHouseHoldDone).html(notAvailableSSNReason);
	 $('#appscr57UScitizenTD'+nonFinancialHouseHoldDone).html(UScitizen);
	 $('#appscr57UScitizenManualVerificationTD'+nonFinancialHouseHoldDone).html(UScitizenManualVerification);
	 $('#appscr57livedIntheUSSince1996IndicatorTD'+nonFinancialHouseHoldDone).html(livedIntheUSSince1996Indicator);
	 $('#appscr57naturalizedCitizenshipIndicatorTD'+nonFinancialHouseHoldDone).html(naturalizedCitizenshipIndicator);
	 $('#appscr57ImmigrationStatusTD'+nonFinancialHouseHoldDone).html(naturalCitizen_eligibleImmigrationStatus);

	 if(certificateType == "CitizenshipCertificate")
	 {
		 certificateAlignNumber = $('#citizenshipAlignNumber').val();
		 certificateNumber = $('#citizenshipCertificateNumber').val();

		 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html(certificateType);
		 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html(certificateAlignNumber);
		 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html(certificateNumber);
	 }
	 else
	 {
		 certificateAlignNumber = $('#naturalizationAlignNumber').val();
		 certificateNumber = $('#naturalizationCertificateNumber').val();

		 $('#appscr57certificateTypeIndicatorTD'+nonFinancialHouseHoldDone).html(documentSameNameIndicator);
		 $('#appscr57naturalizedCitizenshipAlignNumberTD'+nonFinancialHouseHoldDone).html(certificateAlignNumber);
		 $('#appscr57certificateNumberTD'+nonFinancialHouseHoldDone).html(certificateNumber);
	 }
	 $('#appscr57documentSameNameIndicatorTD'+nonFinancialHouseHoldDone).html(documentSameNameIndicator);
	 if(documentSameNameIndicator == "no")
	 {
		 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html(documentFirstName);
		 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html(documentMiddleName);
		 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html(documentLastName);
		 $('appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html(documentSuffix);
	 }
	 else
	 {
		 $('#appscr57documentFirstNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentMiddleNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentLastNameTD'+nonFinancialHouseHoldDone).html('');
		 $('#appscr57documentSuffixTD'+nonFinancialHouseHoldDone).html('Suffix');
	 }


	var ethnicityIndicator = $('input[name=checkPersonNameLanguage]:radio:checked').val();
	$('#appscr57EthnicityIndicatorTD'+nonFinancialHouseHoldDone).text(ethnicityIndicator);
	if(ethnicityIndicator == "yes" || ethnicityIndicator == "Yes")
	{
		 $('#appscr57EthnicityCubanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_cuban').prop("checked"));
		 $('#appscr57EthnicityMexicanMexicanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_maxican').prop("checked"));
		 $('#appscr57EthnicityPuertoRicanTD'+nonFinancialHouseHoldDone).text($('#ethnicity_puertoRican').prop("checked"));
		 /*$('#appscr57EthnicityOtherTD'+nonFinancialHouseHoldDone).text($('#ethnicity_other').prop("checked"));
		 $('#appscr57EthnicityTextTD'+nonFinancialHouseHoldDone).text($('#other_ethnicity ').val());*/
	}
	else
	{
		$('#appscr57EthnicityCubanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityMexicanMexicanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityPuertoRicanTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityOtherTD'+nonFinancialHouseHoldDone).text("false");
		 $('#appscr57EthnicityTextTD'+nonFinancialHouseHoldDone).text("");
	}

	 $('#appscr57RaceAmericanIndianTD'+nonFinancialHouseHoldDone).text($('#americanIndianOrAlaskaNative').prop("checked"));
	 $('#appscr57RaceAsianIndianTD'+nonFinancialHouseHoldDone).text($('#asianIndian').prop("checked"));
	 $('#appscr57RaceBlackOrAfricanAmericanTD'+nonFinancialHouseHoldDone).text($('#BlackOrAfricanAmerican').prop("checked"));
	 $('#appscr57RaceChineseTD'+nonFinancialHouseHoldDone).text($('#chinese').prop("checked"));
	 $('#appscr57RaceFilipinoTD'+nonFinancialHouseHoldDone).text($('#filipino').prop("checked"));
	 $('#appscr57RaceGuamanianOrChamorroTD'+nonFinancialHouseHoldDone).text($('#guamanianOrChamorro').prop("checked"));
	 $('#appscr57RaceJapaneseTD'+nonFinancialHouseHoldDone).text($('#japanese').prop("checked"));
	 $('#appscr57RaceKoreanTD'+nonFinancialHouseHoldDone).text($('#korean').prop("checked"));
	 $('#appscr57RaceNativeHawaiianTD'+nonFinancialHouseHoldDone).text($('#nativeHawaiian').prop("checked"));
	 $('#appscr57RaceOtherAsianTD'+nonFinancialHouseHoldDone).text($('#otherAsian').prop("checked"));
	 $('#appscr57RaceOtherPacificIslanderTD'+nonFinancialHouseHoldDone).text($('#otherPacificIslander').prop("checked"));
	 $('#appscr57RaceSamoanTD'+nonFinancialHouseHoldDone).text($('#samoan').prop("checked"));
	 $('#appscr57RaceVietnameseTD'+nonFinancialHouseHoldDone).text($('#vietnamese').prop("checked"));
	 $('#appscr57RaceWhiteOrCaucasianTD'+nonFinancialHouseHoldDone).text($('#whiteOrCaucasian').prop("checked"));
	 /*$('#appscr57RaceOtherTD'+nonFinancialHouseHoldDone).text($('#raceOther').prop("checked"));
	 $('#appscr57RaceOtherTextTD'+nonFinancialHouseHoldDone).text($('#other_race').val());*/


 	}
}

function updateTaxRelatedData()
{

	var isMarried=false;
	var spouseId=false;
	var payJointly = false;
	var isTaxFiler = false;
	var isInfant = $('#appscr57IsInfantTD'+(HouseHoldRepeat)).text();
	if(isInfant =='no')
	{
	if($('input[name=planToFileFTRIndicator]:radio:checked').val()== 'yes'){
		isTaxFiler = true;
	}

	if($('input[name=marriedIndicator]:radio:checked').val()== 'yes'){
		isMarried = true;
		spouseId=$('input[name=householdContactSpouse]:radio:checked').val();
		if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val()== 'yes'){
			payJointly = true;

		}
		else
		{
			payJointly = false;


		}

	}
	}



	$("#appscr57isMarriedTD"+HouseHoldRepeat).html(""+isMarried);
		$("#appscr57spouseIdTD"+HouseHoldRepeat).html(""+spouseId);
		$("#appscr57payJointlyTD"+HouseHoldRepeat).html(""+payJointly);
		$("#appscr57isTaxFilerTD"+HouseHoldRepeat).html(""+isTaxFiler);

	}

function updateCurrentIncomeDetail()
{
	//var i =($('#houseHoldTRNo').val()).substring(9);
	var i = getCurrentApplicantID();


	if( $('#alimonyAmountInput').val() == '')

		$("#appscr57HouseHoldalimonyAmountTD"+i).html('0');
	else

	$("#appscr57HouseHoldalimonyAmountTD"+i).html($('#alimonyAmountInput').val());


		$("#appscr57HouseHoldalimonyFrequencyTD"+i).html($("#alimonyFrequencySelect option:selected").val());


	if($('#studentLoanInterestAmountInput').val() == '')

	$("#appscr57HouseHoldstudentLoanAmountTD"+i).html('0');
	else

	$("#appscr57HouseHoldstudentLoanAmountTD"+i).html($('#studentLoanInterestAmountInput').val());


		$("#appscr57HouseHoldstudentLoanFrequencyTD"+i).html($("#studentLoanInterestFrequencySelect option:selected").val());

		$("#appscr57HouseHolddeductionsTypeTD"+i).html($('#deductionTypeInput').val());

	if($('#deductionAmountInput').val() == '')

	$("#appscr57HouseHolddeductionsAmountTD"+i).html('0');
	else

	$("#appscr57HouseHolddeductionsAmountTD"+i).html($('#deductionAmountInput').val());

		$("#appscr57HouseHolddeductionsFrequencyTD"+i).html($("#deductionFrequencySelect option:selected").val());

		$("#appscr57HouseHoldbasedOnInputTD"+i).html($('#basedOnAmountInput').val());

}
/*
****************************************************************************************************************
* additionalQuestions.js                                                                                         *
****************************************************************************************************************
*/

var addQuetionsHouseHoldDone = 1;
function addAdditionalQuetionDataToTable()
{
	var willHealthCoverageThroughJob;
	var houseHoldHealthCoverage;
	var otherHealthCoverage;
	var appscr76P2PhysicianIndicator;

	var appendColumn;

	for(var i=1; i<=noOfHouseHold; i++)
	{
		if($('#appscr76P1UninsuredLast6MonthTD'+i).html() != undefined)
		{

			updateAdditionalQuetionDataToTable(i);
			continue;
		}

		willHealthCoverageThroughJob = $('input[name=willHealthCoverageThroughJob]:radio:checked').val();
		houseHoldHealthCoverage = $('#appscr76P1CheckBoxForEmployeeName').prop("checked");
		otherHealthCoverage = $('#appscr76P1CheckBoxForOtherEmployee').prop("checked");

		appendColumn = "";

		appendColumn+="<td class='76P1_uninsuredLast6Month' id='appscr76P1UninsuredLast6MonthTD"+i+"'>"+$('input[name=wasUninsuredFromLast6Month]:radio:checked').val()+"</td>";
		appendColumn+="<td class='76P1_isHealthCoverage' id='appscr76P1HealthCoverageTD"+i+"'>"+$('input[name=isHealthCoverageThroughJob]:radio:checked').val()+"</td>";
		appendColumn+="<td class='76P1_willHealthCoverage' id='appscr76P1WillHealthCoverageTD"+i+"'>"+willHealthCoverageThroughJob+"</td>";

		if(willHealthCoverageThroughJob == "yes" || willHealthCoverageThroughJob == "Yes" || willHealthCoverageThroughJob == "YES")
			appendColumn+="<td class='76P1_healthCoverageDate' id='appscr76P1HealthCoverageDateTD"+i+"'>"+$('#appscr76P1HealthCoverageDate').val()+"</td>";
		else
			appendColumn+="<td class='76P1_healthCoverageDate' id='appscr76P1HealthCoverageDateTD"+i+"'></td>";

		if(houseHoldHealthCoverage == true || houseHoldHealthCoverage == "true")
		{
			appendColumn+="<td class='76P1_houseHoldEmployer' id='appscr76P1HouseHoldEmployerTD"+i+"'>"+houseHoldHealthCoverage+"</td>";

			appendColumn+="<td class='76P1_employerContactName' id='appscr76P1EmployerContactNameTD"+i+"'>"+$('#appscr76P1EmployerContactName').val()+"</td>";
			appendColumn+="<td class='76P1_firstPhoneNumber' id='appscr76P1FirstPhoneNumberTD"+i+"'>"+$('#appscr76P1_firstPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_firstExt' id='appscr76P1FirstExtTD"+i+"'>"+$('#appscr76P1_firstExt').val()+"</td>";
			appendColumn+="<td class='76P1_firstPhoneType' id='appscr76P1FirstPhoneTypeTD"+i+"'>"+$('#appscr76P1_firstPhoneType').val()+"</td>";
			appendColumn+="<td class='76P1_emailAddress' id='appscr76P1EmailAddressTD"+i+"'>"+$('#appscr76P1_emailAddress').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='76P1_houseHoldEmployer' id='appscr76P1HouseHoldEmployerTD"+i+"'>false</td>";

			appendColumn+="<td class='76P1_employerContactName' id='appscr76P1EmployerContactNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstPhoneNumber' id='appscr76P1FirstPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstExt' id='appscr76P1FirstExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_firstPhoneType' id='appscr76P1FirstPhoneTypeTD"+i+"'>0</td>";
			appendColumn+="<td class='76P1_emailAddress' id='appscr76P1EmailAddressTD"+i+"'></td>";
		}

		if(otherHealthCoverage == true || otherHealthCoverage == "true")
		{
			appendColumn+="<td class='76P1_checkBoxForOtherEmployee' id='appscr76P1CheckBoxForOtherEmployeeTD"+i+"'>"+otherHealthCoverage+"</td>";

			appendColumn+="<td class='76P1_otherEmployerName' id='appscr76P1OtherEmployerNameTD"+i+"'>"+$('#appscr76P1_otherEmployerName').val()+"</td>";
			appendColumn+="<td class='76P1_employerEIN' id='appscr76P1EmployerEINTD"+i+"'>"+$('#appscr76P1_employerEIN').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerAddress1' id='appscr76P1OtherEmployerAddress1TD"+i+"'>"+$('#appscr76P1_otherEmployerAddress1').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerAddress2' id='appscr76P1OtherEmployerAddress2TD"+i+"'>"+$('#appscr76P1_otherEmployerAddress2').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerCity' id='appscr76P1OtherEmployerCityTD"+i+"'>"+$('#appscr76P1_otherEmployerCity').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerZip' id='appscr76P1OtherEmployerZipTD"+i+"'>"+$('#appscr76P1_otherEmployerZip').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerState' id='appscr76P1OtherEmployerStateTD"+i+"'>"+$('#appscr76P1_otherEmployerState').val()+"</td>";
			appendColumn+="<td class='76P1_secondPhoneNumber' id='appscr76P1SecondPhoneNumberTD"+i+"'>"+$('#appscr76P1_secondPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_secondExt' id='appscr76P1SecondExtTD"+i+"'>"+$('#appscr76P1_secondExt').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerContactName' id='appscr76P1OtherEmployerContactNameTD"+i+"'>"+$('#appscr76P1_otherEmployerContactName').val()+"</td>";
			appendColumn+="<td class='76P1_thirdPhoneNumber' id='appscr76P1ThirdPhoneNumberTD"+i+"'>"+$('#appscr76P1_thirdPhoneNumber').val()+"</td>";
			appendColumn+="<td class='76P1_thirdExt' id='appscr76P1ThirdExtTD"+i+"'>"+$('#appscr76P1_thirdExt').val()+"</td>";
			appendColumn+="<td class='76P1_thirdPhoneType' id='appscr76P1ThirdPhoneTypeTD"+i+"'>"+$('#appscr76P1_thirdPhoneType').val()+"</td>";
			appendColumn+="<td class='76P1_otherEmployerEmailAddress' id='appscr76P1OtherEmployerEmailAddressTD"+i+"'>"+$('#appscr76P1_otherEmployer_emailAddress').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='76P1_checkBoxForOtherEmployee' id='appscr76P1CheckBoxForOtherEmployeeTD"+i+"'>false</td>";

			appendColumn+="<td class='76P1_otherEmployerName' id='appscr76P1OtherEmployerNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_employerEIN' id='appscr76P1EmployerEINTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerAddress1' id='appscr76P1OtherEmployerAddress1TD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerAddress2' id='appscr76P1OtherEmployerAddress2TD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerCity' id='appscr76P1OtherEmployerCityTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerZip' id='appscr76P1OtherEmployerZipTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerState' id='appscr76P1OtherEmployerStateTD"+i+"'></td>";
			appendColumn+="<td class='76P1_secondPhoneNumber' id='appscr76P1SecondPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_secondExt' id='appscr76P1SecondExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerContactName' id='appscr76P1OtherEmployerContactNameTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdPhoneNumber' id='appscr76P1ThirdPhoneNumberTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdExt' id='appscr76P1ThirdExtTD"+i+"'></td>";
			appendColumn+="<td class='76P1_thirdPhoneType' id='appscr76P1ThirdPhoneTypeTD"+i+"'></td>";
			appendColumn+="<td class='76P1_otherEmployerEmailAddress' id='appscr76P1OtherEmployerEmailAddressTD"+i+"'></td>";
		}

		/*	PageId 76Part1 ended	 */

		/*	PageId 76Part2 started	 */

		appscr76P2PhysicianIndicator = $('input[name=haveApPrimaryCarePhysician]:radio:checked').val();

		appendColumn+="<td class='76P2_enrolledFollowing' id='appscr76P2EnrolledFollowingTD"+i+"'>"+$('input[name=appscr76p2radiosgroup]:radio:checked').val()+"</td>";
		/*appendColumn+="<td class='76P2_physicianIndicator' id='appscr76P2PhysicianIndicatorTD"+i+"'>"+appscr76P2PhysicianIndicator+"</td>";*/
		if(appscr76P2PhysicianIndicator == 'yes' || appscr76P2PhysicianIndicator == 'Yes')
			appendColumn+="<td class='76P2_physicianName' id='appscr76P2PhysicianNameTD"+i+"'>"+$('#appscr76P2_physicianName').val()+"</td>";
		else
			appendColumn+="<td class='76P2_physicianName' id='appscr76P2PhysicianNameTD"+i+"'></td>";
		appendColumn+="<td class='76P2_enrolledCoverageForCoverage' id='appscr76P2EnrollEdCoverageForCoverageTD"+i+"'>"+$('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val()+"</td>";

		/*	PageId 76Part2 ended	 */


		/*	PageId 77Part1 Started   */


		appendColumn+="<td class='77P1CurrentlyEnrolledInHealthPlan' id='appscr77P1CurrentlyEnrolledInHealthPlanTD"+i+"'>"+$('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val()+"</td>";

		var temp = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val();
		appendColumn+="<td class='77P1WillBeEnrolledInHealthPlan' id='appscr77P1WillBeEnrolledInHealthPlanTD"+i+"'>"+temp+"</td>";
		if(temp == "yes")
			appendColumn+="<td class='77P1WillBeEnrolledInHealthPlanDate' id='appscr77P1WillBeEnrolledInHealthPlanDateTD"+i+"'>"+$('#77P1WillBeEnrolledInHealthPlanDate').val()+"</td>";
		else
			appendColumn+="<td class='77P1WillBeEnrolledInHealthPlanDate' id='appscr77P1WillBeEnrolledInHealthPlanDateTD"+i+"'></td>";

		temp = $('input[name=77P1ExpectChangesToHealthCoverage]:radio:checked').val();
		appendColumn+="<td class='77P1ExpectChangesToHealthCoverage' id='appscr77P1ExpectChangesToHealthCoverageTD"+i+"'>"+temp+"</td>";
		if(temp == "no")
		{
			appendColumn+="<td class='77P1WillNoLongerHealthCoverage' id='appscr77P1WillNoLongerHealthCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'></td>";

			appendColumn+="<td class='77P1PlanToDropHealthCoverage' id='appscr77P1PlanToDropHealthCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'></td>";

			appendColumn+="<td class='77P1WillOfferCoverage' id='appscr77P1WillOfferCoverageTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'></td>";

			appendColumn+="<td class='77P1PlaningToEnrollInHC' id='appscr77P1PlaningToEnrollInHCTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'></td>";

			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChange' id='appscr77P1HealthPlanOptionsGoingToChangeTD"+i+"'>false</td>";
			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'></td>";
		}
		else
		{
			temp = $('#77P1WillNoLongerHealthCoverage').prop("checked");
			appendColumn+="<td class='77P1WillNoLongerHealthCoverage' id='appscr77P1WillNoLongerHealthCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'>"+$('#77P1NoLongerHealthCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1NoLongerHealthCoverageDate' id='appscr77P1NoLongerHealthCoverageDateTD"+i+"'></td>";

			temp = $('#77P1PlanToDropHealthCoverage').prop("checked");
			appendColumn+="<td class='77P1PlanToDropHealthCoverage' id='appscr77P1PlanToDropHealthCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'>"+$('#77P1PlanToDropHealthCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1PlanToDropHealthCoverageDate' id='appscr77P1PlanToDropHealthCoverageDateTD"+i+"'></td>";

			temp = $('#77P1WillOfferCoverage').prop("checked");
			appendColumn+="<td class='77P1WillOfferCoverage' id='appscr77P1WillOfferCoverageTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'>"+$('#77P1WillOfferCoverageDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1WillOfferCoverageDate' id='appscr77P1WillOfferCoverageDateTD"+i+"'></td>";

			temp = $('#77P1PlaningToEnrollInHC').prop("checked");
			appendColumn+="<td class='77P1PlaningToEnrollInHC' id='appscr77P1PlaningToEnrollInHCTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'>"+$('#77P1PlaningToEnrollInHCDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1PlaningToEnrollInHCDate' id='appscr77P1PlaningToEnrollInHCDateTD"+i+"'></td>";

			temp = $('#77P1HealthPlanOptionsGoingToChange').prop("checked");
			appendColumn+="<td class='77P1HealthPlanOptionsGoingToChange' id='appscr77P1HealthPlanOptionsGoingToChangeTD"+i+"'>"+temp+"</td>";
			if(temp || temp == "true")
				appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'>"+$('#77P1HealthPlanOptionsGoingToChangeDate').val()+"</td>";
			else
				appendColumn+="<td class='77P1HealthPlanOptionsGoingToChangeDate' id='appscr77P1HealthPlanOptionsGoingToChangeDateTD"+i+"'></td>";
		}


		/* PageId 77Part1 ended		 */

		/* PageId 77Part2 Started 	 */


		appendColumn+="<td class='77P2CurrentLowestCostSelfOnlyPlanName' id='appscr77P2CurrentLowestCostSelfOnlyPlanNameTD"+i+"'>"+$('#currentLowestCostSelfOnlyPlanName').val()+"</td>";
		appendColumn+="<td class='77P2CurrentPlanMeetsMinimumStandard' id='appscr77P2CurrentPlanMeetsMinimumStandardTD"+i+"'>"+$('#currentPlanMeetsMinimumStandard').prop("checked")+"</td>";

		appendColumn+="<td class='77P2ComingLowestCostSelfOnlyPlanName' id='appscr77P2ComingLowestCostSelfOnlyPlanNameTD"+i+"'>"+$('#comingLowestCostSelfOnlyPlanName').val()+"</td>";
		appendColumn+="<td class='77P2ComingPlanMeetsMinimumStandard' id='appscr77P2ComingPlanMeetsMinimumStandardTD"+i+"'>"+$('#comingPlanMeetsMinimumStandard').prop("checked")+"</td>";

		appendColumn+="<td class='77P2LowestCostSelfOnlyPremiumAmount' id='appscr77P2LowestCostSelfOnlyPremiumAmountTD"+i+"'>"+$('#lowestCostSelfOnlyPremiumAmount').val()+"</td>";
		appendColumn+="<td class='77P2LowestCostSelfOnlyPremiumFrequency' id='appscr77P2LowestCostSelfOnlyPremiumFrequencyTD"+i+"'>"+$('#lowestCostSelfOnlyPremiumFrequency').val()+"</td>";


		/* PageId 77Part2 ended		 */


		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);

	}

	//next();
}

function updateAdditionalQuetionDataToTable(i)
{
		/*	PageId 76Part1 started	 */

		$('#appscr76P1UninsuredLast6MonthTD'+i).text($('input[name=wasUninsuredFromLast6Month]:radio:checked').val());
		$('#appscr76P1HealthCoverageTD'+i).text($('input[name=isHealthCoverageThroughJob]:radio:checked').val());
		$('#appscr76P1WillHealthCoverageTD'+i).text($('input[name=willHealthCoverageThroughJob]:radio:checked').val());
		$('#appscr76P1HealthCoverageDateTD'+i).text($('#appscr76P1HealthCoverageDate').val());
		$('#appscr76P1HouseHoldEmployerTD'+i).text($('#appscr76P1CheckBoxForEmployeeName').prop("checked"));
		$('#appscr76P1EmployerContactNameTD'+i).text($('#appscr76P1EmployerContactName').val());
		$('#appscr76P1FirstPhoneNumberTD'+i).text($('#appscr76P1_firstPhoneNumber').val());
		$('#appscr76P1FirstExtTD'+i).text($('#appscr76P1_firstExt').val());
		$('#appscr76P1FirstPhoneTypeTD'+i).text($('#appscr76P1_firstPhoneType').val());
		$('#appscr76P1EmailAddressTD'+i).text($('#appscr76P1_emailAddress').val());
		$('#appscr76P1CheckBoxForOtherEmployeeTD'+i).text($('#appscr76P1CheckBoxForOtherEmployee').prop("checked"));
		$('#appscr76P1OtherEmployerNameTD'+i).text($('#appscr76P1_otherEmployerName').val());
		$('#appscr76P1EmployerEINTD'+i).text($('#appscr76P1_employerEIN').val());
		$('#appscr76P1OtherEmployerAddress1TD'+i).text($('#appscr76P1_otherEmployerAddress1').val());
		$('#appscr76P1OtherEmployerAddress2TD'+i).text($('#appscr76P1_otherEmployerAddress2').val());
		$('#appscr76P1OtherEmployerCityTD'+i).text($('#appscr76P1_otherEmployerCity').val());
		$('#appscr76P1OtherEmployerZipTD'+i).text($('#appscr76P1_otherEmployerZip').val());
		$('#appscr76P1OtherEmployerStateTD'+i).text($('#appscr76P1_otherEmployerState').val());
		$('#appscr76P1SecondPhoneNumberTD'+i).text($('#appscr76P1_secondPhoneNumber').val());
		$('#appscr76P1SecondExtTD'+i).text($('#appscr76P1_secondExt').val());
		$('#appscr76P1OtherEmployerContactNameTD'+i).text($('#appscr76P1_otherEmployerContactName').val());
		$('#appscr76P1ThirdPhoneNumberTD'+i).text($('#appscr76P1_thirdPhoneNumber').val());
		$('#appscr76P1ThirdExtTD'+i).text($('#appscr76P1_thirdExt').val());
		$('#appscr76P1ThirdPhoneTypeTD'+i).text($('#appscr76P1_thirdPhoneType').val());
		$('#appscr76P1OtherEmployerEmailAddressTD'+i).text($('#appscr76P1_otherEmployer_emailAddress').val());

		/*	PageId 76Part1 ended	 */

		/*	PageId 76Part2 started	 */

		$('#appscr76P2EnrolledFollowingTD'+i).text($('input[name=appscr76p2radiosgroup]:radio:checked').val());
		/*$('#appscr76P2PhysicianIndicatorTD'+i).text($('input[name=haveApPrimaryCarePhysician]:radio:checked').val());*/
		$('#appscr76P2PhysicianNameTD'+i).text($('#appscr76P2_physicianName').val());
		$('#appscr76P2EnrollEdCoverageForCoverageTD'+i).text($('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val());

		/*	PageId 76Part2 ended	 */

}

function appscr76FromMongodb()
{
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		appscr76FromMongodbJSON(currentID);
		return;
	}

	/*	PageId 76Part1 started
	var temp = $('#appscr76P1UninsuredLast6MonthTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#wasUninsuredFromLast6MonthYes').prop("checked", true);
	else
		$('#wasUninsuredFromLast6MonthNo').prop("checked", true);


	temp = $('#appscr76P1HealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#isHealthCoverageThroughJobYes').prop("checked", true);
	else
		$('#isHealthCoverageThroughJobNo').prop("checked", true);


	temp = $('#appscr76P1WillHealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#willHealthCoverageThroughJobYes').prop("checked", true);
	else
		$('#willHealthCoverageThroughJobNo').prop("checked", true);


	$('#appscr76P1HealthCoverageDate').val($('#appscr76P1HealthCoverageDateTD1').text());


	temp = $('#appscr76P1HouseHoldEmployerTD1').text();
	if(temp == "true")
	{
		document.getElementById('appscr76P1CheckBoxForEmployeeName').checked = true;
		$("#memberNameDataDivPart1").show();
	}
	else
	{
		document.getElementById('appscr76P1CheckBoxForEmployeeName').checked = false;
		$("#memberNameDataDivPart1").hide();
	}


	$('#appscr76P1EmployerContactName').val($('#appscr76P1EmployerContactNameTD1').text());
	$('#appscr76P1_firstPhoneNumber').val($('#appscr76P1FirstPhoneNumberTD1').text());
	$('#appscr76P1_firstExt').val($('#appscr76P1FirstExtTD1').text());
	$('#appscr76P1_firstPhoneType').val($('#appscr76P1FirstPhoneTypeTD1').text());
	$('#appscr76P1_emailAddress').val($('#appscr76P1EmailAddressTD1').text());

	temp = $('#appscr76P1CheckBoxForOtherEmployeeTD1').text();
	if(temp == "true")
	{
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = true;
		$("#memberNameDataDivPart2").show();
	}
	else
	{
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = false;
		$("#memberNameDataDivPart2").hide();
	}

	$('#appscr76P1_otherEmployerName').val($('#appscr76P1OtherEmployerNameTD1').text());
	$('#appscr76P1_employerEIN').val($('#appscr76P1EmployerEINTD1').text());
	$('#appscr76P1_otherEmployerAddress1').val($('#appscr76P1OtherEmployerAddress1TD1').text());
	$('#appscr76P1_otherEmployerAddress2').val($('#appscr76P1OtherEmployerAddress2TD1').text());
	$('#appscr76P1_otherEmployerCity').val($('#appscr76P1OtherEmployerCityTD1').text());
	$('#appscr76P1_otherEmployerZip').val($('#appscr76P1OtherEmployerZipTD1').text());
	$('#appscr76P1_otherEmployerState').val($('#appscr76P1OtherEmployerStateTD1').text());
	$('#appscr76P1_secondPhoneNumber').val($('#appscr76P1SecondPhoneNumberTD1').text());
	$('#appscr76P1_secondExt').val($('#appscr76P1SecondExtTD1').text());
	$('#appscr76P1_otherEmployerContactName').val($('#appscr76P1OtherEmployerContactNameTD1').text());
	$('#appscr76P1_thirdPhoneNumber').val($('#appscr76P1ThirdPhoneNumberTD1').text());
	$('#appscr76P1_thirdExt').val($('#appscr76P1ThirdExtTD1').text());
	$('#appscr76P1_thirdPhoneType').val($('#appscr76P1ThirdPhoneTypeTD1').text());
	$('#appscr76P1_otherEmployer_emailAddress').val($('#appscr76P1OtherEmployerEmailAddressTD1').text());

		PageId 76Part1 ended

		PageId 76Part2 started

	temp = $('#appscr76P2EnrolledFollowingTD1').text();
	if(temp == "corba")
		$('#appscr76P2_cobra').prop("checked", true);
	if(temp == "retire health plan")
		$('#appscr76P2_retireHealthPlan').prop("checked", true);
	if(temp == "veterans health program")
		$('#appscr76P2_veteransProgram').prop("checked", true);
	else if(temp == "none")
		$('#appscr76P2_noneOfAbove').prop("checked", true);


	temp = $('#appscr76P2PhysicianIndicatorTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#appscr76P2_physicianCareIndicatorYes').prop("checked", true);
	else
		$('#appscr76P2_physicianCareIndicatorNo').prop("checked", true);

	$('#appscr76P2_physicianName').val($('#appscr76P2PhysicianNameTD1').text());


	temp = $('#appscr76P2EnrollEdCoverageForCoverageTD1').text();
	if(temp == "corba")
		$('#appscr76P2_cobraForCoverage').prop("checked", true);
	if(temp == "retire health plan")
		$('#appscr76P2_RHPForCoverage').prop("checked", true);
	if(temp == "veterans health program")
		$('#appscr76P2_VHPForCoverage').prop("checked", true);
	else if(temp == "none")
		$('#appscr76P2_NOTAForCoverage').prop("checked", true);

		PageId 76Part2 ended

		PageId 77Part1 started

	temp = $('#appscr77P1CurrentlyEnrolledInHealthPlanTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1CurrentlyEnrolledInHealthPlanYes').prop("checked", true);
	else
		$('#77P1CurrentlyEnrolledInHealthPlanNo').prop("checked", true);


	temp = $('#appscr77P1WillBeEnrolledInHealthPlanTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1WillBeEnrolledInHealthPlanYes').prop("checked", true);
	else
		$('#77P1WillBeEnrolledInHealthPlanNo').prop("checked", true);

	temp = $('#appscr77P1WillBeEnrolledInHealthPlanDateTD1').text();
	$('#77P1WillBeEnrolledInHealthPlanDate').val(temp);


	temp = $('#appscr77P1ExpectChangesToHealthCoverageTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#77P1ExpectChangesToHealthCoverageYes').prop("checked", true);
	else
		$('#77P1ExpectChangesToHealthCoverageNo').prop("checked", true);


	temp = $('#appscr77P1WillNoLongerHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = true;
	else
		document.getElementById('77P1WillNoLongerHealthCoverage').checked = false;

	temp = $('#appscr77P1NoLongerHealthCoverageDateTD1').text();
	$('#77P1NoLongerHealthCoverageDate').val(temp);


	temp = $('#appscr77P1PlanToDropHealthCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlanToDropHealthCoverage').checked = true;
	else
		document.getElementById('77P1PlanToDropHealthCoverage').checked = false;

	temp = $('#appscr77P1PlanToDropHealthCoverageDateTD1').text();
	$('#77P1PlanToDropHealthCoverageDate').val(temp);


	temp = $('#appscr77P1WillOfferCoverageTD1').text();
	if(temp == "true")
		document.getElementById('77P1WillOfferCoverage').checked = true;
	else
		document.getElementById('77P1WillOfferCoverage').checked = false;

	temp = $('#appscr77P1WillOfferCoverageDateTD1').text();
	$('#77P1WillOfferCoverageDate').val(temp);


	temp = $('#appscr77P1PlaningToEnrollInHCTD1').text();
	if(temp == "true")
		document.getElementById('77P1PlaningToEnrollInHC').checked = true;
	else
		document.getElementById('77P1PlaningToEnrollInHC').checked = false;

	temp = $('#appscr77P1PlaningToEnrollInHCDateTD1').text();
	$('#77P1PlaningToEnrollInHCDate').val(temp);


	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeTD1').text();
	if(temp == "true")
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = true;
	else
		document.getElementById('77P1HealthPlanOptionsGoingToChange').checked = false;

	temp = $('#appscr77P1HealthPlanOptionsGoingToChangeDateTD1').text();
	$('#77P1HealthPlanOptionsGoingToChangeDate').val(temp);*/


	/*	PageId 77Part1 ended	 */

	/*	PageId 77Part2 started	 */


	/*temp = $('#appscr77P2CurrentLowestCostSelfOnlyPlanNameTD1').text();
	//$('#currentLowestCostSelfOnlyPlanName').val(temp);

	temp = $('#appscr77P2CurrentPlanMeetsMinimumStandardTD1').text();
	if(temp == "true")
		document.getElementById('currentPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('currentPlanMeetsMinimumStandard').checked = false;


	temp = $('#appscr77P2ComingLowestCostSelfOnlyPlanNameTD1').text();
	//$('#comingLowestCostSelfOnlyPlanName').val(temp);

	temp = $('#appscr77P2ComingPlanMeetsMinimumStandardTD1').text();
	if(temp == "tr" +
			"ue")
		document.getElementById('comingPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('comingPlanMeetsMinimumStandard').checked = false;


	temp = $('#appscr77P2LowestCostSelfOnlyPremiumAmountTD1').text();
	$('#lowestCostSelfOnlyPremiumAmount').val(temp);


	temp = $('#appscr77P2LowestCostSelfOnlyPremiumFrequencyTD1').text();
	$('#lowestCostSelfOnlyPremiumFrequency').val(temp);*/

	/*	PageId 77Part2 ended	 */
}


function addDataAppscr78ToTable()
{
	var appendColumn;

	for(var i=1; i<=noOfHouseHold; i++)
	{
		if($('#appscr78MedicareTD'+i).html() != undefined)
		{
			updateDataAppscr78ToTable(i);
			continue;
		}

		appendColumn = "";

		if(document.getElementById("appscr78Medicare").checked == true)
			appendColumn+="<td class='P78Medicare' id='appscr78MedicareTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78Medicare' id='appscr78MedicareTD"+i+"'>"+"no"+"</td>";


		if(document.getElementById("appscr78Tricare").checked == true)
		appendColumn+="<td class='P78Tricare' id='appscr78TricareTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78Tricare' id='appscr78TricareTD"+i+"'>"+"no"+"</td>";

		if(document.getElementById("appscr78PeaceCorps").checked == true)
			appendColumn+="<td class='P78PeaceCorps' id='appscr78PeaceCorpsTD"+i+"'>"+"yes"+"</td>";
		else
			appendColumn+="<td class='P78PeaceCorps' id='appscr78PeaceCorpsTD"+i+"'>"+"no"+"</td>";


		if(document.getElementById("appscr78OtherStateFHBP").checked == true)
		{
		appendColumn+="<td class='P78OtherStateFHBP' id='appscr78OtherStateFHBPTD"+i+"'>"+"yes"+"</td>";
		appendColumn+="<td class='P78TypeOfProgram' id='appscr78TypeOfProgramTD"+i+"'>"+$('#appscr78TypeOfProgram').val()+"</td>";
		appendColumn+="<td class='P78NameOfProgram' id='appscr78NameOfProgramTD"+i+"'>"+$('#appscr78NameOfProgram').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='P78OtherStateFHBP' id='appscr78OtherStateFHBPTD"+i+"'>"+"no"+"</td>";
			appendColumn+="<td class='P78TypeOfProgram' id='appscr78TypeOfProgramTD"+i+"'>"+"N/A"+"</td>";
			appendColumn+="<td class='P78NameOfProgram' id='appscr78NameOfProgramTD"+i+"'>"+"N/A"+"</td>";
		}

		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}

	next();
}

function updateDataAppscr78ToTable(i)
{
		if(document.getElementById("appscr78Medicare").checked == true)
			$('#HouseHold'+i).find(".P78Medicare").html("yes");
		else
			$('#HouseHold'+i).find(".P78Medicare").html("no");


		if(document.getElementById("appscr78Tricare").checked == true)
			$('#HouseHold'+i).find(".P78Tricare").html("yes");
		else
			$('#HouseHold'+i).find(".P78Tricare").html("no");

		if(document.getElementById("appscr78PeaceCorps").checked == true)
			$('#HouseHold'+i).find(".P78PeaceCorps").html("yes");
		else
			$('#HouseHold'+i).find(".P78PeaceCorps").html("no");


		if(document.getElementById("appscr78OtherStateFHBP").checked == true)
		{
			$('#HouseHold'+i).find(".P78OtherStateFHBP").html("yes");

			$('#HouseHold'+i).find(".P78TypeOfProgram").html($('#appscr78TypeOfProgram').val());

			$('#HouseHold'+i).find(".P78NameOfProgram").html($('#appscr78NameOfProgram').val());

		}
		else
		{
			$('#HouseHold'+i).find(".P78OtherStateFHBP").html("no");

			$('#HouseHold'+i).find(".P78TypeOfProgram").html("N/A");

			$('#HouseHold'+i).find(".P78NameOfProgram").html("N/A");

		}
}

function fillDataAppscr78ToTable()
{
	var temp = "";

	temp = $('#appscr78MedicareTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78Medicare').checked = true;
	else
		document.getElementById('appscr78Medicare').checked = false;


	temp = $('#appscr78TricareTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78Tricare').checked = true;
	else
		document.getElementById('appscr78Tricare').checked = false;


	temp = $('#appscr78PeaceCorpsTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78PeaceCorps').checked = true;
	else
		document.getElementById('appscr78PeaceCorps').checked = false;


	temp = $('#appscr78OtherStateFHBPTD1').text();
	if(temp == "yes")
		document.getElementById('appscr78OtherStateFHBP').checked = true;
	else
		document.getElementById('appscr78OtherStateFHBP').checked = false;


	temp = $('#appscr78TypeOfProgramTD1').text();
	$('#appscr78TypeOfProgram').val(temp);


	temp = $('#appscr78NameOfProgramTD1').text();
	$('#appscr78NameOfProgram').val(temp);
}

function addDataAppscr79ToTable()
{
	var appendColumn;
	var isMemberFRTribeIndicator = "";

	for(var i=1; i<=noOfHouseHold; i++)
	{
		if($('#appscr79IsMemberFRTribeTD'+i).html() != undefined)
		{
			updateDataAppscr79ToTable(i);
			continue;
		}

		appendColumn = "";

		isMemberFRTribeIndicator = $('input[name=AmericonIndianQuestionRadio]:radio:checked').val();
		appendColumn+="<td class='P79IsMemberFRTribe' id='appscr79IsMemberFRTribeTD"+i+"'>"+isMemberFRTribeIndicator+"</td>";

		if(isMemberFRTribeIndicator == "yes")
		{
			appendColumn+="<td class='P79State' id='appscr79StateTD"+i+"'>"+$('#appscr79State').val()+"</td>";
			appendColumn+="<td class='P79TribeName' id='appscr79TribeNameTD"+i+"'>"+$('#appscr79TribeName').val()+"</td>";
		}
		else
		{
			appendColumn+="<td class='P79State' id='appscr79StateTD"+i+"'></td>";
			appendColumn+="<td class='P79TribeName' id='appscr79TribeNameTD"+i+"'>TribeName</td>";
		}

		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}

	skipOrKeepPage80();
	next();
}

function updateDataAppscr79ToTable(i)
{
	var isMemberFRTribeIndicator = "";

	isMemberFRTribeIndicator = $('input[name=AmericonIndianQuestionRadio]:radio:checked').val();

		$('#appscr79IsMemberFRTribeTD'+i).html(isMemberFRTribeIndicator);
		$('#appscr79StateTD'+i).html($('#appscr79State').val());
		$('#appscr79TribeNameTD'+i).html($('#appscr79TribeName').val());

}

function fillValuesAppscr79FromMongodb()
{
	var temp = $('#appscr79IsMemberFRTribeTD1').text();
	if(temp == "yes")
		$('#AmericonIndianQuestionRadioYes').prop("checked", true);
	else
		$('#AmericonIndianQuestionRadioNo').prop("checked", true);

	checkAmericonIndianQuestion();

	temp = $('#appscr79StateTD1').text();
	$('#appscr79State').val(temp);

	temp = $('#appscr79TribeNameTD1').text();
	$('#appscr79TribeName').val(temp);
}

function addDataAppscr81ToTable()
{
	var appendColumn;

	var doesWantHelpForPayingBIll = "";
	var doesAnyHaveHealthInsure = "";
	var noneAnyHaveHealthInsure = "";

	var didHaveAHealthInsuranceFromLastJob = "";
	var whyDidThatInsuranceEnd = "";
	var isHealthPlanFromSelfJob = "";

	for(var i=1; i<=noOfHouseHold; i++)
	{
		if($('#P81BdoesWantHelpForPayingBIllTD'+i).html() != undefined)
		{
			updateDataAppscr81ToTable(i);
			continue;
		}

		appendColumn = "";

		/*	appscrPage81B started	*/
		doesWantHelpForPayingBIll = $('input[name=doesWantHelpForPayingBIllP81B]:radio:checked').val();
		doesAnyHaveHealthInsure = $('#peopleHealthInsurDependChild').prop("checked");
		noneAnyHaveHealthInsure = $('#peopleHealthInsurNone').prop("checked");

		appendColumn+= "<td class='P81BdoesWantHelpForPayingBIll' id='P81BdoesWantHelpForPayingBIllTD"+i+"'>"+doesWantHelpForPayingBIll+"</td>";

		if(noneAnyHaveHealthInsure)
		{
			appendColumn+= "<td class='P81BdoesAnyHaveHealthInsure' id='P81BdoesAnyHaveHealthInsureTD"+i+"'>false</td>";
			appendColumn+= "<td class='P81BnoneAnyHaveHealthInsure' id='P81BnoneAnyHaveHealthInsureTD"+i+"'>"+noneAnyHaveHealthInsure+"</td>";
		}
		else
		{
			appendColumn+= "<td class='P81BdoesAnyHaveHealthInsure' id='P81BdoesAnyHaveHealthInsureTD"+i+"'>"+doesAnyHaveHealthInsure+"</td>";
			appendColumn+= "<td class='P81BnoneAnyHaveHealthInsure' id='P81BnoneAnyHaveHealthInsureTD"+i+"'>false</td>";
		}
		/*	appscrPage81B ended	*/

		/*	appscrPage81C started	*/

		didHaveAHealthInsuranceFromLastJob = $('input[name=didHaveAHealthInsuranceFromLastJob]:radio:checked').val();
		whyDidThatInsuranceEnd = $('input[name=whyDidThatInsuranceEnd]:radio:checked').val();
		isHealthPlanFromSelfJob = $('input[name=isHealthPlanFromSelfJob]:radio:checked').val();

		appendColumn+= "<td class='P81CdidHaveAHealthInsuranceFromLastJob' id='P81CdidHaveAHealthInsuranceFromLastJobTD"+i+"'>"+didHaveAHealthInsuranceFromLastJob+"</td>";
		appendColumn+= "<td class='P81CwhyDidThatInsuranceEnd' id='P81CwhyDidThatInsuranceEndTD"+i+"'>"+whyDidThatInsuranceEnd+"</td>";
		if(whyDidThatInsuranceEnd == "OTHER")
			appendColumn+= "<td class='P81CExplanation' id='P81CExplanationTD"+i+"'>"+$('#descriptionTextArea').val()+"</td>";
		else
			appendColumn+= "<td class='P81CExplanation' id='P81CExplanationTD"+i+"'></td>";
		appendColumn+= "<td class='P81CisHealthPlanFromSelfJob' id='P81CisHealthPlanFromSelfJobTD"+i+"'>"+isHealthPlanFromSelfJob+"</td>";

		/*	appscrPage81C ended	*/

		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendColumn);
	}

}

function updateDataAppscr81ToTable(i)
{
	var doesWantHelpForPayingBIll = "";
	var doesAnyHaveHealthInsure = "";
	var noneAnyHaveHealthInsure = "";

	var didHaveAHealthInsuranceFromLastJob = "";
	var whyDidThatInsuranceEnd = "";
	var isHealthPlanFromSelfJob = "";

	doesWantHelpForPayingBIll = $('input[name=doesWantHelpForPayingBIllP81B]:radio:checked').val();
	doesAnyHaveHealthInsure = $('#peopleHealthInsurDependChild').prop("checked");
	noneAnyHaveHealthInsure = $('#peopleHealthInsurNone').prop("checked");

	didHaveAHealthInsuranceFromLastJob = $('input[name=didHaveAHealthInsuranceFromLastJob]:radio:checked').val();
	whyDidThatInsuranceEnd = $('input[name=whyDidThatInsuranceEnd]:radio:checked').val();
	isHealthPlanFromSelfJob = $('input[name=isHealthPlanFromSelfJob]:radio:checked').val();


		$('#P81BdoesWantHelpForPayingBIllTD'+i).html(doesWantHelpForPayingBIll);
		$('#P81BdoesAnyHaveHealthInsureTD'+i).html(doesAnyHaveHealthInsure);
		$('#P81BnoneAnyHaveHealthInsureTD'+i).html(noneAnyHaveHealthInsure);

		$('#P81BdidHaveAHealthInsuranceFromLastJobTD'+i).html(didHaveAHealthInsuranceFromLastJob);
		$('#P81BwhyDidThatInsuranceEndTD'+i).html(whyDidThatInsuranceEnd);
		$('#P81CExplanationTD'+i).html($('#descriptionTextArea').val());
		$('#P81BisHealthPlanFromSelfJobTD'+i).html(isHealthPlanFromSelfJob);



}

function fillValuesAppscr81FromMongodb()
{
	var temp = $('#P81BdoesWantHelpForPayingBIllTD1').text();
	if(temp == "yes")
		$('#doesWantHelpForPayingBIllP81BYes').prop("checked", true);
	else
		$('#doesWantHelpForPayingBIllP81BNo').prop("checked", true);


	temp = $('#P81BdoesAnyHaveHealthInsureTD1').text();
	if(temp == "true")
		document.getElementById('peopleHealthInsurDependChild').checked = true;
	else
		document.getElementById('peopleHealthInsurDependChild').checked = false;

	temp = $('#P81BnoneAnyHaveHealthInsureTD1').text();
	if(temp == "true")
		document.getElementById('peopleHealthInsurNone').checked = true;
	else
		document.getElementById('peopleHealthInsurNone').checked = false;

	showOrHide81BInfo();




	temp = $('#P81CdidHaveAHealthInsuranceFromLastJobTD1').text();
	if(temp == "yes")
		$('#didHaveAHealthInsuranceFromLastJobYes').prop("checked", true);
	else
		$('#didHaveAHealthInsuranceFromLastJobNo').prop("checked", true);


	temp = $('#P81CwhyDidThatInsuranceEndTD1').text();
	if(temp == "PARENT_NO_LONGER_EMPLOYED")
		$('#P81CParentNoLongerEmployed').prop("checked", true);
	else if(temp == "EMPLOYER_STOPPED_COVERAGE")
		$('#P81CEmployerStoppedCoverage').prop("checked", true);
	else if(temp == "EMPLOYER_STOPPED_DEPENDANT_COVERAGE")
		$('#P81CEmployerStoppedDependant').prop("checked", true);
	else if(temp == "INSURANCE_TOO_EXPENSIVE")
		$('#P81CInsuranceTooExpensive').prop("checked", true);
	else if(temp == "UNINSURED_MEDICAL_NEEDS")
		$('#P81CUninsuredMedicalNeeds').prop("checked", true);
	else
		$('#P81COther').prop("checked", true);


	temp = $('#P81CExplanationTD1').text();
	$('#descriptionTextArea').val(temp);


	temp = $('#P81CisHealthPlanFromSelfJobTD1').text();
	if(temp == "yes")
		$('#isHealthPlanFromSelfJobYes').prop("checked", true);
	else
		$('#isHealthPlanFromSelfJobNo').prop("checked", true);
}

function appscr76FromMongodbJSON(currentMember){

	//cleanappscr76FromMongodb();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentMember-1];

	/*if(householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes == null || householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes == ""){
		goToPageById("appscr81B"); //appscr81B
		return;
	}*/

	/*	PageId 76Part1 started	 */

	var job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	$('.EmployerNameHere').html(job_income_name);

	if(householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == true){
		$('#wasUninsuredFromLast6MonthYes').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").show();

	} else if (householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == false) {
		$('#wasUninsuredFromLast6MonthNo').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").hide();
	} else if (householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator == null){
		$('#wasUninsuredFromLast6MonthYes').prop("checked", false);
		$('#wasUninsuredFromLast6MonthNo').prop("checked", false);
	}
	$("input[name=isHealthCoverageThroughJob]").prop("checked",false);
	if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == true){
		$('#isHealthCoverageThroughJobYes').prop("checked", true);
		displayWillBeOfferedFromJob();
	}
	if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == false){
		$('#isHealthCoverageThroughJobNo').prop("checked", true);
		displayWillBeOfferedFromJob();
	} else if(householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator == null){
		$('#isHealthCoverageThroughJobYes').prop("checked", false);
		$('#isHealthCoverageThroughJobNo').prop("checked", false);
	}
	$("input[name=willHealthCoverageThroughJob]").prop("checked",false);
	$("#appscr76P1FirstHideAndShowDiv").hide();
	if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == true){
		$('#willHealthCoverageThroughJobYes').prop("checked", true);
		$("#appscr76P1FirstHideAndShowDiv").show();
	} else if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == false) {
		$('#willHealthCoverageThroughJobNo').prop("checked", true);
	} else if(householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator == null){
		$('#willHealthCoverageThroughJobYes').prop("checked", false);
		$('#willHealthCoverageThroughJobNo').prop("checked", false);
	}

	$('#appscr76P1HealthCoverageDate').val(UIDateformat(householdMemberObj.healthCoverage.employerWillOfferInsuranceStartDate));

	$("#memberNameDataDivPart1").hide();
	$('#appscr76P1CheckBoxForEmployeeName').prop("checked", false);
	if(trimSpaces(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName) != ""){
		$('#appscr76P1CheckBoxForEmployeeName').prop("checked", true);
		$("#memberNameDataDivPart1").show();
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName ===""){
		$('#appscr76P1EmployerContactName').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName);
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber!=" "){
		$('#appscr76P1_firstPhoneNumber').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber);
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension===""){
		$('#appscr76P1_firstExt').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension);
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType ===""){
		$('#appscr76P1_firstPhoneType').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType);
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress !=""){
		$('#appscr76P1_emailAddress').val(householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress);
	}

	$('#appscr76P1CheckBoxForOtherEmployee').prop("checked", false);
	$("#memberNameDataDivPart2").hide();

	if(trimSpaces(householdMemberObj.healthCoverage.formerEmployer[0].employerName) != "") {
		document.getElementById('appscr76P1CheckBoxForOtherEmployee').checked = true;
		$("#memberNameDataDivPart2").show();
	}

	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerName ===""){
		$('#appscr76P1_otherEmployerName').val(householdMemberObj.healthCoverage.formerEmployer[0].employerName);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber ===""){
		$('#appscr76P1_employerEIN').val(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber ===""){
		$('#appscr76P1_otherEmployerAddress1').val(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress1);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2 ===""){
		$('#appscr76P1_otherEmployerAddress2').val(householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.city ===""){
		$('#appscr76P1_otherEmployerCity').val(householdMemberObj.healthCoverage.formerEmployer[0].address.city);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode ===" "){
		$('#appscr76P1_otherEmployerZip').val(householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].address.state ===" "){
		$('#appscr76P1_otherEmployerState').val(householdMemberObj.healthCoverage.formerEmployer[0].address.state);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber !=" "){
		$('#appscr76P1_secondPhoneNumber').val(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension ===""){
		$('#appscr76P1_secondExt').val(householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName ===""){
		$('#appscr76P1_otherEmployerContactName').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber ===" "){
		$('#appscr76P1_thirdPhoneNumber').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension ===""){
		$('#appscr76P1_thirdExt').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType ===" "){
		$('#appscr76P1_thirdPhoneType').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType);
	//}
	//if(householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress ===""){
		$('#appscr76P1_otherEmployer_emailAddress').val(householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress);
	//}
	/*	PageId 76Part1 ended	 */

	/*	PageId 76Part2 started	 */

	$("input[name=appscr76p2radiosgroup]").prop("checked",false);
	if(householdMemberObj.healthCoverage.currentlyEnrolledInCobraIndicator == true){
		$('#appscr76P2_cobra').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInRetireePlanIndicator == true){
		$('#appscr76P2_retireHealthPlan').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInVeteransProgramIndicator == true){
		$('#appscr76P2_veteransProgram').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.currentlyEnrolledInNoneIndicator == true) {
		$('#appscr76P2_noneOfAbove').prop("checked", true);
	}


	/*temp = $('#appscr76P2PhysicianIndicatorTD1').text();
	if(temp == "yes" || temp == "Yes")
		$('#appscr76P2_physicianCareIndicatorYes').prop("checked", true);
	else
		$('#appscr76P2_physicianCareIndicatorNo').prop("checked", true);

	$('#appscr76P2_physicianName').val($('#appscr76P2PhysicianNameTD1').text());*/


	$("input[name=willBeEnrolledInHealthCoverageInFollowingYear]").prop("checked",false);
	if(householdMemberObj.healthCoverage.willBeEnrolledInCobraIndicator == true){
		$('#appscr76P2_cobraForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInRetireePlanIndicator == true){
		$('#appscr76P2_RHPForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInVeteransProgramIndicator == true){
		$('#appscr76P2_VHPForCoverage').prop("checked", true);
	}
	else if(householdMemberObj.healthCoverage.willBeEnrolledInNoneIndicator == true){
		$('#appscr76P2_NOTAForCoverage').prop("checked", true);
	}

	/*	PageId 76Part2 ended	 */

	/*	PageId 77Part1 started	 */

	$("input[name=77P1CurrentlyEnrolledInHealthPlan]").prop("checked",false);
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator == true){
		$('#77P1CurrentlyEnrolledInHealthPlanYes').prop("checked", true);
	}else if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator == false){
		$('#77P1CurrentlyEnrolledInHealthPlanNo').prop("checked", true);
	}

	$("input[name=77P1WillBeEnrolledInHealthPlan]").prop("checked",false);
	$('#DateToCovered').hide();
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator == true){
		$('#77P1WillBeEnrolledInHealthPlanYes').prop("checked", true);
		$('#DateToCovered').show();
	}else if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator == false){
		$('#77P1WillBeEnrolledInHealthPlanNo').prop("checked", true);
	}



	/*	PageId 77Part1 ended	 */

	/*	PageId 77Part2 started	 */
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName === ""){
		$('#currentLowestCostSelfOnlyPlanName').val(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName);
	}

	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentPlanMeetsMinimumStandardIndicator == true)
		document.getElementById('currentPlanMeetsMinimumStandard').checked = false;
	else
		document.getElementById('currentPlanMeetsMinimumStandard').checked = true;

	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName === ""){
		$('#comingLowestCostSelfOnlyPlanName').val(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName);
	}

	temp = $('#appscr77P2ComingPlanMeetsMinimumStandardTD1').text();
	if(householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingPlanMeetsMinimumStandardIndicator == true)
		document.getElementById('comingPlanMeetsMinimumStandard').checked = true;
	else
		document.getElementById('comingPlanMeetsMinimumStandard').checked = false;

	/*
	 * Following data are not available in json
	temp = $('#appscr77P2LowestCostSelfOnlyPremiumAmountTD1').text();
	$('#lowestCostSelfOnlyPremiumAmount').val(temp);


	temp = $('#appscr77P2LowestCostSelfOnlyPremiumFrequencyTD1').text();
	$('#lowestCostSelfOnlyPremiumFrequency').val(temp);
	*/

	/*	PageId 77Part2 ended	 */


}

/*$(document).ready(function(){
	$("input[name=isHealthCoverageThroughJob],input[name=willHealthCoverageThroughJob]").bind("click", function(){
		if($(this).attr("value") == "yes"){
			$("#memberNameDataDivHide, #memberNameDataDiv").show();
		}else if($("#isHealthCoverageThroughJobNo").is(":checked") && $("#willHealthCoverageThroughJobNo").is(":checked")){
			$("#memberNameDataDivHide, #memberNameDataDiv").hide();
		}
	});
});*/

/*
****************************************************************************************************************
* fillValuesFromMongodb.js                                                                                        *
****************************************************************************************************************
*/

var houseHoldNumberValue;
var haveDataFlag = false;
var disabilityFlag = true;
var i_cover_adoptionFlag = true;
var i_nativeAmericonFlag = true;
var i_cover_fosterFlag = true;
var i_deceasedFlag = true;
var isPregnantFlag = true;
var tribeCode = "";

function fillValuesToFields(currentPage1)
{
	if(currentPage1 == "appscr52")
	{
		setValuesToappscr53Mongodb();
	}
	else if(currentPage1 == "appscr53")
	{
		setValuesToappscr54Mongodb();
	}
	else if(currentPage1 == "appscr54")
	{
		setData55();
	}
	else if(currentPage1 == "appscr55")
	{
		setValuesToappscr57Mongodb();
	}
	else if(currentPage1 == "appscr57")
	{
		Appscr58DetailJson();
	}

	else if(currentPage1 == "appscr59A")
	{

		//if(haveData && HouseHoldRepeat<=houseHoldNumberValue){
			updateAppscr60FromJSON(HouseHoldRepeat);

		//}
	}
	else if(currentPage1 == "appscr59B")
	{

		//if(haveData && HouseHoldRepeat<=houseHoldNumberValue){
			updateAppscr60FromMongodb(HouseHoldRepeat);

		//}
	}

	else if(currentPage1 == "appscr60")
	{
		haveDataFlag = true;

		if(useJSON == true){
			appscr67EditController(nonFinancialHouseHoldDone);
		}else {
			if(nonFinancialHouseHoldDone<=houseHoldNumberValue)
				appscr67EditController(nonFinancialHouseHoldDone);
		}

		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr61"){
		//var rowId =($('#houseHoldTRNo').val()).substring(9);
		var rowId = getCurrentApplicantID();
		  appscr61FromJSON(rowId);
	}
	else if(currentPage1 == "appscr62Part1"){
		if(useJSON == true){
		  appscr63FromJSON(nonFinancialHouseHoldDone);
		}
	}
	else if(currentPage1 == "appscr64")
	{
		if(haveData == true)
			appscr65FromMongodb();
	}
	else if(currentPage1 == "appscr65")
	{
		//setAddressDetailsNonfinancial();
		addSpecialCircumstances();
		appscr66FromMongodbJson();
		//appscr66NoneOfThese();
	}
	else if(currentPage1 == "appscr66")
	{
		populateRelationship();
	}

	/*else if(currentPage1 == "appscrBloodRel")
	{
		showHouseHoldContactSummary();
	}*/

	else if(currentPage1 == "appscr67")
	{
		//appscrSNAPfromMongodb();
	}
	else if(currentPage1 == "appscrSNAP")
	{
		appscr68FromMongodb();
	}
	else if(currentPage1 == "appscr68")
	{
		haveDataFlag = true;
		if(useJSON == true){
			appscr74EditController(financialHouseHoldDone);
		}else {
			if(financialHouseHoldDone<=houseHoldNumberValue)
				appscr74EditController(financialHouseHoldDone);
		}
		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr70")
	{
		haveDataFlag = true;
		if(useJSON == true){
			appscr74EditController(financialHouseHoldDone);
		}else {
			if(financialHouseHoldDone<=houseHoldNumberValue)
				appscr74EditController(financialHouseHoldDone);
		}
		haveDataFlag = false;
	}
	else if(currentPage1 == "appscr69")
	{

		if(useJSON == true){
			appscr69FromMongodbJSON(financialHouseHoldDone);
		}

	}
	else if(currentPage1 == "appscr75B")
	{
		appscr76FromMongodb();
	}
	else if(currentPage1 == "appscr75A"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr76P1"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr76P2"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr77Part1"){
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr77Part2")
	{
		fillDataAppscr78ToTable();
		if(useJSON == true){
			appscr76FromMongodb();
		}
	}
	else if(currentPage1 == "appscr78")
	{
		fillValuesAppscr79FromMongodb();
	}
	else if(currentPage1 == "appscr80")
	{
		fillValuesAppscr81FromMongodb();
	}

	else if(currentPage1 == "appscr83")
	{
		generateReviewSummaryReport();
	}

}

function setValuesToappscr53Mongodb()
{

	if(useJSON == true){
		setValuesToappscr53JSON();
		return;
	}
}

function setValuesToappscr54Mongodb()
{

	if(useJSON == true){
		setValuesToappscr54JSON();
		return;
	}
}

function setValuesToappscr57Mongodb()
{
	if(useJSON == true){
		setValuesToappscr57JSON();
		return;
	}
}

function updateAppscr60FromMongodb(i)
{

	if(useJSON == true){
		updateAppscr60FromJSON(i);
		return;
	}


}


function appscr65FromMongodb()
{

	if(useJSON == true){
		appscr65FromMongodbJSON();
		return;
	}
}


function appscr66NoneOfThese(){
	$('#disabilityNoneOfThese').prop('checked',disabilityFlag);
	$('#assistanceServicesNoneOfThese').prop('checked',i_cover_adoptionFlag);
	$('#nativeNoneOfThese').prop('checked',i_nativeAmericonFlag);
	$('#personfosterCareNoneOfThese').prop('checked',i_cover_fosterFlag);
	$('#deceasedPersonNoneOfThese').prop('checked',i_deceasedFlag);
	if($('#pregnancyNoneOfThese').html() != undefined){
		$('#pregnancyNoneOfThese').prop('checked',isPregnantFlag);
		//document.getElementById('pregnancyNoneOfThese').checked = isPregnantFlag;
	}
}


function appscr66FromMongodb()
{
	if(useJSON == true){
		appscr66FromMongodbJson();
		return;
	}
}


function appscrSNAPfromMongodb()
{
	var temp = $('#appscr57personCashBenefits1').text();

	if(temp == "yes" || temp == "Yes")
		$('#cashBenefitsYes').prop("checked", true);
	else
		$('#cashBenefitsNo').prop("checked", true);


	for (var i = 1; i <= houseHoldNumberValue; i++)
	{
		temp = $('#appscr57Identification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('identification'+i).checked = true;
		else
			document.getElementById('identification'+i).checked = false;


		temp = $('#appscr57IdentificationVerification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('identificationVerification'+i).checked = true;
		else
			document.getElementById('identificationVerification'+i).checked = false;


		temp = $('#appscr57Food'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('food'+i).checked = true;
		else
			document.getElementById('food'+i).checked = false;


		temp = $('#appscr57FoodVerification'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('foodVerification'+i).checked = true;
		else
			document.getElementById('foodVerification'+i).checked = false;


		temp = $('#appscr57IsStriker'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('isStriker'+i).checked = true;
		else
			document.getElementById('isStriker'+i).checked = false;


		temp = $('#appscr57Having_IPV'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('having_IPV'+i).checked = true;
		else
			document.getElementById('having_IPV'+i).checked = false;


		temp = $('#appscr57ResidencyVerified'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('residencyVerified'+i).checked = true;
		else
			document.getElementById('residencyVerified'+i).checked = false;


		temp = $('#appscr57DataSSI'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('dataSSI'+i).checked = true;
		else
			document.getElementById('dataSSI'+i).checked = false;


		temp = $('#appscr57DrugFellow'+i).text();
		if(temp == "yes" || temp == "Yes")
			document.getElementById('drugFellow'+i).checked = true;
		else
			document.getElementById('drugFellow'+i).checked = false;


		temp = $('#appscr57DependentCareExpenseCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#dependentCareExpenseCost').val(temp);

			temp = $('#appscr57DependentCareExpenseFrequency1').text();
			$('#dependentCareExpenseFrequency').val(temp);
		}

		temp = $('#appscr57ChildSupportCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#childSupportCost').val(temp);

			temp = $('#appscr57ChildSupportFrequency1').text();
			$('#childSupportFrequency').val(temp);
		}

		temp = $('#appscr57MedicalCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#medicalCost').val(temp);

			temp = $('#appscr57MedicalFrequency1').text();
			$('#medicalFrequency').val(temp);
		}

		temp = $('#appscr57ShelterAndUtilityExpenseCost1').text();
		if(temp != "0.0" || temp != "0")
		{
			$('#shelterAndUtilityExpenseCost').val(temp);

			temp = $('#appscr57ShelterAndUtilityExpenseFrequency1').text();
			$('#shelterAndUtilityExpenseFrequency').val(temp);
		}

		temp = $('#appscr57HeatingAndCoolingIndicator1').text();
		if(temp == "yes" || temp == "Yes")
			$('#hAndCYes').prop("checked", true);
		else
			$('#hAndCNo').prop("checked", true);
	}
}


function appscr68FromMongodb()
{

}

function setValuesToappscr53JSON()
{

	var addressSameFlag = true;
	var householdAccountHolder = true;

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	$('#firstName').val(trimSpaces(householdMemberObj.name.firstName));
	$('#middleName').val(trimSpaces(householdMemberObj.name.middleName));
	$('#lastName').val(trimSpaces(householdMemberObj.name.lastName));

	var temp = householdMemberObj.name.suffix;

	if(temp == "Suffix")
		$('#appscr53Suffix').val('0');
	else
		$('#appscr53Suffix').val(temp);

	//$('#appscr53Suffix').val(householdMemberObj.name.suffix);



	$('#dateOfBirth').val(UIDateformat(householdMemberObj.dateOfBirth));
	$('#dobVerificationStatus').val($('#appscr57DOBVerificationTD1').text());
	$('#emailAddress').val(householdMemberObj.householdContact.contactPreferences.emailAddress);


	$('#home_addressLine1').val(trimSpaces(householdMemberObj.householdContact.homeAddress.streetAddress1));
	$('#home_addressLine2').val(trimSpaces(householdMemberObj.householdContact.homeAddress.streetAddress2));
	$('#home_primary_city').val(trimSpaces(householdMemberObj.householdContact.homeAddress.city));
	$('#home_primary_zip').val(trimSpaces(householdMemberObj.householdContact.homeAddress.postalCode));
	$('#home_primary_state').val(householdMemberObj.householdContact.homeAddress.state);

	//$('#home_primary_county').val(householdMemberObj.householdContact.homeAddress.county);
	var homeAddressCounty = householdMemberObj.householdContact.homeAddress.county;
	var homeAddressCountyCode = householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode;

	if(homeAddressCounty != null && homeAddressCounty != "") {
		$("#home_primary_county").html('');
		$("#home_primary_county").append("<option value='" + homeAddressCountyCode + "' selected>" + homeAddressCounty+ "</option>");
	}


	$('#mailing_addressLine1').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.streetAddress1));
	$('#mailing_addressLine2').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.streetAddress2));
	$('#mailing_primary_city').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.city));
	$('#mailing_primary_zip').val(trimSpaces(householdMemberObj.householdContact.mailingAddress.postalCode));
	$('#mailing_primary_state').val(householdMemberObj.householdContact.mailingAddress.state);

	//$('#mailing_primary_county').val(householdMemberObj.householdContact.mailingAddress.county);

	var mailingAddressCounty = householdMemberObj.householdContact.mailingAddress.county;

	if(mailingAddressCounty != null && mailingAddressCounty != "") {
		$("#mailing_primary_county").html('');
		$("#mailing_primary_county").append("<option value='" + mailingAddressCounty + "' selected>" + mailingAddressCounty + "</option>");
	}


	$('#first_phoneNo').val(householdMemberObj.householdContact.phone.phoneNumber);
	$('#first_ext').val(householdMemberObj.householdContact.phone.phoneExtension);
	$('#first_homePhoneNo').val(householdMemberObj.householdContact.otherPhone.phoneNumber);
	$('#first_homeExt').val(householdMemberObj.householdContact.otherPhone.phoneExtension);

	$('#first_phoneType').val($('#appscr57houseHoldPhoneTypeFirstTD1').text());
	$('#second_phoneNo').val(householdMemberObj.householdContact.otherPhone.phoneNumber);
	$('#second_ext').val(householdMemberObj.householdContact.otherPhone.phoneExtension);
	$('#second_phoneType').val($('#appscr57houseHoldPhoneTypeSecondTD1').text());
	$('#preffegred_spoken_language').val(householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage);
	$('#preffered_written_language').val(householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage);

	//mask the phone number after retrieve from JSON
	$('.phoneNumber').mask('(000) 000-0000');

	if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "EMAIL"){
		$('#email').prop("checked", true);
	}else if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "POSTAL MAIL"){
		$('#inTheEmail').prop("checked", true);
	}else if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "EmailAndMail"){
		$('#emailAndMail').prop("checked", true);
	}else if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "None"){
		$('#noEmailMail').prop("checked", true);
	}

//	if(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == "EMAIL | POSTAL MAIL"){
//		$('#inTheEmail').prop("checked", true);
//		$('#email').prop("checked", true);
//	}

	if(!($('#appscr57FirstNameTD1').text() == firstNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57MiddleNameTD1').text() == middleNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57LastNameTD1').text() == lastNameDB))
		householdAccountHolder = false;
	if(!($('#appscr57emailAddressTD1').text() == emailDB))
		householdAccountHolder = false;
	if(!($('#appscr57houseHoldPhoneFirstTD1').text() == phoneDB))
		householdAccountHolder = false;

	//document.getElementById('checkIsAccountHolder').checked = householdAccountHolder;


	if(!($('#home_addressLine1').val() == $('#mailing_addressLine1').val()))
		addressSameFlag = false;
	if(!($('#home_addressLine2').val() == $('#mailing_addressLine2').val()))
		addressSameFlag = false;
	if(!($('#home_primary_city').val() == $('#mailing_primary_city').val()))
		addressSameFlag = false;
	if(!($('#home_primary_zip').val() == $('#mailing_primary_zip').val()))
		addressSameFlag = false;
	if(!($('#home_primary_state').val() == $('#mailing_primary_state').val()))
		addressSameFlag = false;
	if(!($("#home_primary_county option:selected").text() == $('#mailing_primary_county').val()))
		addressSameFlag = false;

		document.getElementById('mailingAddressIndicator').checked = addressSameFlag;

		if (addressSameFlag == true) {
			$('#mailing_primary_state').removeClass("validateColor");
			$('#mailingAddressIndicator').attr('value', true);
			$('.physicalmailingAddress').hide();
			$('#mailing_addressLine1').val($('#home_addressLine1').val());
			$('#mailing_addressLine2').val($('#home_addressLine2').val());
			$('#mailing_primary_city').val($('#home_primary_city').val());
			$('#mailing_primary_zip').val($('#home_primary_zip').val());
			$('#mailing_primary_state').val($('#home_primary_state').val());

			var home_primary_address = $('#home_primary_state').val();/*.split("#")[0];*/
			$('#mailing_primary_county option[value!=""]').remove();
			$('#mailing_primary_county').html('');
			$('#mailing_primary_county').append('<option value="'+home_primary_address+'" selected="selected">'+home_primary_address+'</option>');
		}

		if(houseHoldNumberValue>1){
			$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
		}else if(ssapApplicationType != "NON_FINANCIAL"){
			$('#wantToGetHelpPayingHealthInsurance_id1').prop("checked", true);
		}

	showOrHideOptionDiv();
}

function setValuesToappscr54JSON()
{

	var helpType = webJson.singleStreamlinedApplication.helpType;
	var temp = "";
	$("input[name=authorizedRepresentativeIndicator]").prop("checked",false);
	if(helpType == "BROKER"){
		$("#helpQuestionDiv").hide();
		$("#brokerDiv, #authorizedRepresentativeDiv").show();

		brokerObj=webJson.singleStreamlinedApplication.broker;

		$('#brokerName54').val(brokerObj.brokerName);
		$('#brokerID54').val(brokerObj.brokerFederalTaxIdNumber);




	}else if(helpType == "ASSISTER"){
		$("#helpQuestionDiv").hide();
		$("#guideDiv, #authorizedRepresentativeDiv").show();

		assisterObj=webJson.singleStreamlinedApplication.assister;

		$('#guideID54').val(assisterObj.assisterID);
		$('#guideName54').val(assisterObj.assisterName);

	}else{
		$("#helpQuestionDiv").show();
		$("#brokerDiv, #authorizedRepresentativeDiv,#guideDiv, #authorizedRepresentativeDiv").hide();
		$("input[name=authorizedRepresentativeHelp]").prop("checked",false);
		if(webJson.singleStreamlinedApplication.getHelpIndicator == true){
			$("#authorizedRepresentativeHelpYes").prop("checked",true);
			$("#authorizedRepresentativeDiv").show();

		}else if(webJson.singleStreamlinedApplication.getHelpIndicator == false){
			$("#authorizedRepresentativeHelpNo").prop("checked",true);
		}
	}

	if(webJson.singleStreamlinedApplication.getHelpIndicator == true){
		temp = webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator;

		if(temp === true){
			$('#displayAuthRepresentativeInfo').prop("checked", true);
			displayAuthInfo();

			AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;

			$('#authorizedFirstName').val(AuthorizedRepresentativeObj.name.firstName);
			$('#authorizedMiddleName').val(AuthorizedRepresentativeObj.name.middleName);
			$('#authorizedLastName').val(AuthorizedRepresentativeObj.name.lastName);

			$('#authorizedEmailAddress').val(AuthorizedRepresentativeObj.emailAddress);

			$('#authorizedAddress1').val(AuthorizedRepresentativeObj.address.streetAddress1);
			$('#authorizedAddress2').val(AuthorizedRepresentativeObj.address.streetAddress2);
			$('#authorizedCity').val(AuthorizedRepresentativeObj.address.city);

			$('#authorizedState').val(AuthorizedRepresentativeObj.address.state);
			$('#authorizedZip').val(AuthorizedRepresentativeObj.address.postalCode);


			$('#appscr54_phoneNumber').val(AuthorizedRepresentativeObj.phone[0].phoneNumber);
			$('#appscr54_ext').val(AuthorizedRepresentativeObj.phone[0].phoneExtension);

			$('#appscr54_homePhoneNumber').val(AuthorizedRepresentativeObj.phone[1].phoneNumber);
			$('#appscr54_homeExt').val(AuthorizedRepresentativeObj.phone[1].phoneExtension);

			$('#appscr54_workPhoneNumber').val(AuthorizedRepresentativeObj.phone[2].phoneNumber);
			$('#appscr54_workExt').val(AuthorizedRepresentativeObj.phone[2].phoneExtension);

			//mask the phone number after retrieve from JSON
			$('.phoneNumber').mask('(000) 000-0000');

			temp = AuthorizedRepresentativeObj.partOfOrganizationIndicator;
			if(temp === true){
				$("#cmpnyNameAndOrgRadio54_1").prop("checked",true);
				nameOfCompanyAndOrg54();

				$('#authorizeCompanyName').val(AuthorizedRepresentativeObj.companyName);
				$('#authorizeOrganizationId').val(AuthorizedRepresentativeObj.organizationId);
			}else if(temp === false){
				$("#cmpnyNameAndOrgRadio54_2").prop("checked",true);
				nameOfCompanyAndOrg54();
			}

			temp = AuthorizedRepresentativeObj.signatureisProofIndicator;
			if(temp === true){
				$("#makeOtherizedRepresentativeSignature").prop("checked",true);
				$("#authorizeSignature").val(AuthorizedRepresentativeObj.signature);
			}else if(temp === false){
				$("#makeOtherizedRepresentativeLater").prop("checked",true);
			}


			if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
				AuthorizedRepresentativeObj.signatureisProofIndicator = true;
			}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
				AuthorizedRepresentativeObj.signatureisProofIndicator = false;
			}

		}else if(temp === false){
			$('#hideAuthRepresentativeInfo').prop("checked", true);
			displayAuthInfo();
		}
	}



}


function setValuesToappscr55JSON(){
	//page5
	householdMemberObj=webJson.singleStreamlinedApplication;
	$("input[name=ApplyingForhouseHold]").prop("checked",false);
	if(householdMemberObj.applyingForhouseHold == 'houseHoldContactOnly')
		$('#ApplyingForhouseHoldOnly').prop("checked", true);
	else if(householdMemberObj.applyingForhouseHold == 'otherFamilyMember')
		$('#ApplyingForhouseHoldMember').prop("checked", true);
	else if(householdMemberObj.applyingForhouseHold == 'otherExcludingHousehold')
		$('#ApplyingForhouseHoldOther').prop("checked", true);

	if(ssapApplicationType == "NON_FINANCIAL"){
		householdMemberObj.applyingForFinancialAssistanceIndicator = false;
	}
	$("input[name=wantToGetHelpPayingHealthInsurance]").prop("checked",false);
	if(householdMemberObj.applyingForFinancialAssistanceIndicator == true)
		$('#wantToGetHelpPayingHealthInsurance_id1').prop("checked", true);
	else if(householdMemberObj.applyingForFinancialAssistanceIndicator == false)
		$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);

	showOrHideOptionDiv();
}



function setValuesToappscr57JSON()
{
	//var household = $('#noOfPeople53').val();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	//addAnotherIndividual
	//noOfHouseHold
	$('#appscr57HouseholdForm').html('');
	for(var i=1; i<=noOfHouseHoldmember; i++){
		noOfHouseHold = i;
		addAnotherIndividual(i);
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		$('#appscr57FirstName'+i).val(householdMemberObj.name.firstName);
		$('#appscr57MiddleName'+i).val(householdMemberObj.name.middleName);
		$('#appscr57LastName'+i).val(householdMemberObj.name.lastName);
		$('#appscr57Suffix'+i).val(householdMemberObj.name.suffix);
		$('#appscr57DOB'+i).val(UIDateformat(householdMemberObj.dateOfBirth));
		$('#applicantName57' + i).text(getNameByPersonId(i));
		$('#applicantTitle57' + i).text(getNameByPersonId(i));
		temp = householdMemberObj.tobaccoUserIndicator;
		if(temp == true)
			$('#appscr57SexYes'+i).prop("checked", true);
		else
			$('#appscr57SexNo'+i).prop("checked", true);

		if(householdMemberObj.applyingForCoverageIndicator == true)
			$('#appscr57checkseekingcoverage'+i).prop("checked", true);
		else
			$('#appscr57checkseekingcoverage'+i).prop("checked", false);
	}

}

function updateAppscr60FromJSON(i)
{

	if(useJSON == true){
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		if(householdMemberObj.planToFileFTRIndicator === true){
			$("#planToFileFTRIndicatorYes").prop("checked",true);
			planToFileYes();
		}else if(householdMemberObj.planToFileFTRIndicator === false){
			$("#planToFileFTRIndicatorNo").prop("checked",true);
			planToFileNo();
		}else if(householdMemberObj.planToFileFTRIndicator === null){
			$("input[name=planToFileFTRIndicator]").prop("checked",false);

			//hide all sequence questions
			$("#marriedIndicatorDiv, #householdSpouseMainDiv, #willClaimDependents, #householdDependentMainDiv, #dependentDetailDiv_inputFields, #willBeClaimed, #appscr60ClaimedFilerDiv").hide();
		}

		//update married
		if(householdMemberObj.marriedIndicator === true){
			$("#marriedIndicatorYes").prop("checked",true);
			isMarried();


			//update joint tax file
			if(householdMemberObj.planToFileFTRJontlyIndicator === true){
				$("#planOnFilingJointFTRIndicatorYes").prop("checked",true);
				householdPayJointly();
			}else if(householdMemberObj.planToFileFTRJontlyIndicator === false){
				$("#planOnFilingJointFTRIndicatorNo").prop("checked",true);
				householdNotPayJointly();
			}else if(householdMemberObj.planToFileFTRJontlyIndicator === null){
				$("input[name=planOnFilingJointFTRIndicator]").prop("checked",false);
				householdNotPayJointly();
			}

			//update spouse
			getSpouse(i);
			if(householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
				$("#householdContactSpouse" + householdMemberObj.taxFiler.spouseHouseholdMemberId).prop("checked",true);

			}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId == 0){
				$("#householdSpouseDiv input[name=householdContactSpouse]").prop("checked",false);
			}
			setSpouse();
			changeSpouseDivView60A();


			//update living with spouse
			if(householdMemberObj.taxFiler.liveWithSpouseIndicator === true){
				$("#liveWithSpouseIndicatorYes").prop("checked",true);
			}else if(householdMemberObj.taxFiler.liveWithSpouseIndicator === false){
				$("#liveWithSpouseIndicatorNo").prop("checked",true);
			}else if(householdMemberObj.taxFiler.liveWithSpouseIndicator === null){
				$("input[name=liveWithSpouseIndicator]").prop("checked",false);
			}


		}else if(householdMemberObj.marriedIndicator === false){
			$("#marriedIndicatorNo").prop("checked",true);
			isNotMarried();

			$("#householdSpouseMainDiv input").prop("checked",false);
		}else if(householdMemberObj.marriedIndicator === null){
			$("input[name=marriedIndicator]").prop("checked",false);
			isNotMarried();

			$("#householdSpouseMainDiv input").prop("checked",false);
		}


		//update "claim dependents"
		if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === true){
			$("#householdHasDependantYes").prop("checked",true);

			//update "claim dependents" check box
			for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
				$("#householdContactDependant" + webJson.singleStreamlinedApplication.taxFilerDependants[j]).prop("checked",true);
			}

		}else if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === false){
			$("#householdHasDependantNo").prop("checked",true);
		}else if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === null){
			$("input[name=householdHasDependant]").prop("checked",false);
		}
		changeDependentDivView60A();

		//update "be claimed"
		if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === true){
			$("#doesApplicantHasTaxFillerYes").prop("checked",true);

			$("#householdContactFiler" + webJson.singleStreamlinedApplication.primaryTaxFilerPersonId).prop("checked",true);

		}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false){
			$("#doesApplicantHasTaxFillerNo").prop("checked",true);

			$("#householdSpouseDiv input[name=householdContactFiler]").prop("checked",false);
		}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === null){
			$("input[name=doesApplicantHasTaxFiller]").prop("checked",false);
			$("#householdSpouseDiv input[name=householdContactFiler]").prop("checked",false);
		}
		doesFilerHasTaxFiller();
	}
}

function appscr65FromMongodbJSON()
{
	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	var none = true;
	var nonenull = false;

	 updatePageOtherAddress();
	// populateDataToRelations();
	 setAddressDetailsToSpan();
	 populateRelationship();

	 var houseHoldotherAddressNoneOfTheAbove = true;


	 for(var i=2;i<=noOfHouseHoldmember;i++){
		 householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		 if (householdMemberObj.livesAtOtherAddressIndicator == true){

			//if one of them is checked, set houseHoldotherAddressNoneOfTheAbove to false
			houseHoldotherAddressNoneOfTheAbove = false;

			$('#householdContactAddress' + i).prop('checked',true);
			checkUncheck(i);
		 	$('#applicant_or_non-applican_address_1'+i).val(householdMemberObj.otherAddress.address.streetAddress1);
			$('#applicant_or_non-applican_address_2'+i).val(householdMemberObj.otherAddress.address.streetAddress2);
			$('#city'+i).val(householdMemberObj.otherAddress.address.city);
			$('#applicant_or_non-applican_state'+i).val(householdMemberObj.otherAddress.address.state);
			$('#zip'+i).val(householdMemberObj.otherAddress.address.postalCode);
			getCountyListByZipCode(i, householdMemberObj.otherAddress.address.county);
			if(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator == true){
				$('#TemporarilyLivingOutsideIndicatorYes'+i).prop("checked",true);
				$('#inNMDiv'+i).show();

				$('#applicant2city'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city);
				$('#applicant_or_non-applican_stateTemp'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state);
				$('#applicant-2-zip'+i).val(householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode);

			}else{
				$('#TemporarilyLivingOutsideIndicatorNo'+i).prop("checked",true);
				$('#inNMDiv'+i).hide();
			}
		}else{
			 $('#householdContactAddress' + i).prop('checked',false);
			 checkUncheck(i);
		}
	 }

	 if(houseHoldotherAddressNoneOfTheAbove){
		 $('#HouseHoldotherAddressNoneOfTheAbove').prop("checked", true);
	 }
}

function appscr69FromMongodbJSON(i){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

	var haveAnyIcome = false;
	$("#Financial_Job").prop('checked', false);
	if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){
		$("#Financial_Job").prop('checked', true);
		haveAnyIcome = true;
	}
	$("#Financial_Self_Employment").prop('checked', false);
	if(householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator == true){
		$("#Financial_Self_Employment").prop('checked', true);
		haveAnyIcome = true;
	}
	$("#Financial_SocialSecuritybenefits").prop('checked', false);
	if(householdMemberObj.detailedIncome.socialSecurityBenefitIndicator == true){
		$("#Financial_SocialSecuritybenefits").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Unemployment").prop('checked', false);
	if(householdMemberObj.detailedIncome.unemploymentBenefitIndicator == true){
		$("#Financial_Unemployment").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Retirement_pension").prop('checked', false);
	if(householdMemberObj.detailedIncome.retirementOrPensionIndicator == true){
		$("#Financial_Retirement_pension").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_Capitalgains").prop('checked', false);
	if(householdMemberObj.detailedIncome.capitalGainsIndicator == true){
		$("#Financial_Capitalgains").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_RentalOrRoyaltyIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator == true){
		$("#Financial_RentalOrRoyaltyIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_FarmingOrFishingIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.farmFishingIncomeIndictor == true){
		$("#Financial_FarmingOrFishingIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_AlimonyReceived").prop('checked', false);
	if(householdMemberObj.detailedIncome.alimonyReceivedIndicator == true){
		$("#Financial_AlimonyReceived").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_InvestmentIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.investmentIncomeIndicator == true){
		$("#Financial_InvestmentIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	$("#Financial_OtherIncome").prop('checked', false);
	if(householdMemberObj.detailedIncome.otherIncomeIndicator == true){
		$("#Financial_OtherIncome").prop('checked', true);
		haveAnyIcome = true;
	}

	if(haveAnyIcome == true){
		$("#Financial_None").prop('checked', false);
		$("#appscr70radios4IncomeYes").prop('checked', true);
		$('#checkBoxes_div').show();
		$('#checkBoxes_divlbl').show();

		showDiductionsFields();

		$("#incomeSourceDiv").show();

		$('#incomesStatus'+i).text("Yes");

	}else{
		$("#Financial_NoneFinancial_None").prop('checked', true);
		$('#incomesStatus'+i).text("No");
	}

	//$("#Financial_OtherIncome").prop('checked', false);

	//financial_DetailsArray[0] = 'Financial_NoneFinancial_None';
	//financial_DetailsArray[11] = 'Financial_OtherIncome';
	if(haveAnyIcome == false){
	$.each(incomesecion, function(key, value) {
	    if(value == true)
		{
	    	$("#"+key).prop('checked', true);
		}
	});
	}

}

function appscr61FromJSON(i){
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	var houseHoldname = getNameByPersonId(i);
	$('.nameOfHouseHold').html(""+houseHoldname);
	$('input[name=docType]:radio:checked').prop("checked", false);
	$('input[name=documentType]:radio:checked').prop("checked", false);
	$('input[name=naturalizedCitizenshipIndicator]:radio:checked').prop("checked", false);
	$('input[name=livesInUS]:radio:checked').prop("checked", false);
	$('input[name=honourably-veteran]:radio:checked').prop("checked", false);
	$(".immigrationDocType").hide();
	var temp = householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator;
	if(temp == false){
		  //$('input[name=UScitizen]:radio').prop('checked', false);
		  $("#UScitizenIndicatorNo").prop("checked", true);
		  isUSCitizen();
	}else if(temp == true){
		  //$('input[name=UScitizen]:radio').prop('checked', true); suneel 05/09/2014
		  $("#UScitizenIndicatorYes").prop("checked", true);
		  isUSCitizen();

	}else if(temp == null){
			$("#UScitizenIndicatorYes").prop("checked", true);
			isUSCitizen();
			$('input[name=UScitizen]:radio:checked').prop("checked", false);
	}

	if(householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == false){
			//$('input[name=naturalizedCitizenshipIndicator]:radio').prop('checked', false);
		$("#naturalizedCitizenshipIndicatorNo").prop("checked", true);
	}else if(householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator == true){
				// $('input[name=naturalizedCitizenshipIndicator]:radio').prop('checked', true); // suneel 05/09/2014
		$("#naturalizedCitizenshipIndicatorYes").prop("checked", true);
	}
	isNaturalizedCitizen();


	var exchangeName = $('#exchangeName').val();

	if(exchangeName === "NMHIX"){
		householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = true;
		$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',true).prop('disabled',true);
	}else if(exchangeName === "Your Health Idaho"){
		if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator == true){
			$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',true);
		}else{
			$('#naturalCitizen_eligibleImmigrationStatus').prop('checked',false);
		}

	}
	showOrHideNaturalCitizenImmigrationStatus();
			/*Retreving Data for immigration depending upon Document Type*/

			 if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator =="true"))
			 	{
				 	$("#permcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#permcarddetailsdiv").show(); // Suneel 05/09/2014

				 	$('#permcarddetailsAlignNumber').val("");
				 	$('#permcarddetailsppcardno').val("");
				 	$('#permcarddetailsdateOfExp').val("");

					$('#permcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#permcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.cardNumber);
					$('#permcarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));

			 	}
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator =="true"))
			 	{
				 	$("#tempcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#tempcarddetailsdiv").show(); // Suneel 05/09/2014

				 	$('#tempcarddetailsAlignNumber').val("");
				 	$('#tempcarddetailsppcardno').val("");
				 	$('#tempcarddetailsPassportexpdate').val("");

					$('#tempcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#tempcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);

					$('#tempcarddetailsCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);

					$('#tempcarddetailsPassportexpdate').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
			 	}
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator =="true"))
			 	{
				 	$("#machinecarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#machinecarddetailsdiv").show(); // Suneel 05/09/2014

				 	$('#machinecarddetailsAlignNumber').val("");
				 	$('#machinecarddetailsppcardno').val("");
				 	$('#machinecarddetailsppvisano').val("");

					$('#machinecarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#machinecarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
					$('#machinecarddetailsppvisano').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber);
					$('#machinecarddetailsCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
					$('#machinecarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				}

			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator =="true"))
			 	{
				 	$("#empauthcarddetails").prop("checked", true); // suneel 05/09/2014
				 	$("#empauthcarddetailsdiv").show(); // Suneel 05/09/2014

					$('#empauthcarddetailsAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
					$('#empauthcarddetailsppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.cardNumber);
					$('#empauthcarddetailsdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator =="true"))
			 	{
				 	$("#arrivaldeparturerecord").prop("checked", true); // suneel 05/09/2014
				 	$("#arrivaldeparturerecorddiv").show(); // Suneel 05/09/2014

				  $('#arrivaldeparturerecordI94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#arrivaldeparturerecordSevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#arrivaldeparturerecorddateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator =="true"))
			 	{
				 	$("#arrivaldeprecordForeign").prop("checked", true); // suneel 05/09/2014
				 	$("#arrivaldeprecordForeigndiv").show(); // Suneel 05/09/2014

				  $('#arrivaldeprecordForeignI94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#arrivaldeprecordForeignppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#arrivaldeprecordForeignCounty').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				  $('#arrivaldeprecordForeigndateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#arrivaldeprecordForeignppvisano').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber);
				  $('#arrivaldeprecordForeignSevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator =="true"))
			 	{
				 	$('#NoticeOfAction').prop("checked", true); // suneel 05/09/2014
				 	$('#NoticeOfActiondiv').show(); // Suneel 05/09/2014

				 	$('#NoticeOfActionAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#NoticeOfActionI94').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator =="true"))
			 	{

				 	$('#ForeignppI94').prop("checked", true); // suneel 05/09/2014
				 	$('#ForeignppI94div').show(); // Suneel 05/09/2014

				  $('#ForeignppI94AlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#ForeignppI94ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#ForeignppI94County').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				  $('#ForeignppI94dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#ForeignppI94SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator =="true"))
			 	{
				 	$('#rentrypermit').prop("checked", true); // suneel 05/09/2014
				 	$('#rentrypermitdiv').show(); // Suneel 05/09/2014

				  $('#rentrypermitAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#rentrypermitdateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator =="true"))
			 	{
				 	$('#refugeetraveldocI-571').prop("checked", true); // suneel 05/09/2014
				 	$('#refugeetraveldocI-571div').show(); // Suneel 05/09/2014

				  $('#refugeetraveldocI-571AlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#refugeetraveldocI-571dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator =="true"))
			 	{
				 	$('#certificatenonimmigrantF1-Student-I20').prop("checked", true); // suneel 05/09/2014
				 	$('#certificatenonimmigrantF1-Student-I20div').show(); // Suneel 05/09/2014

				  $('#certificatenonimmigrantF1-Student-I20SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#certificatenonimmigrantF1-Student-I20I94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#certificatenonimmigrantF1-Student-I20ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#certificatenonimmigrantF1-Student-I20dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#certificatenonimmigrantF1-Student-I20County').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance);
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator =="true"))
			 	{
				 	$('#certificatenonimmigrantJ1-Student-DS2019').prop("checked", true); // suneel 05/09/2014
				 	$('#certificatenonimmigrantJ1-Student-DS2019div').show(); // Suneel 05/09/2014

				  $('#certificatenonimmigrantJ1-Student-DS2019SevisID').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#certificatenonimmigrantJ1-Student-DS2019I94no').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#certificatenonimmigrantJ1-Student-DS2019ppcardno').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				 }
			 else if((householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType.length > 0) && (householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator == true || householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator =="true"))
			 	{
				 $('#docType_other').prop("checked", false); // suneel 05/09/2014
				 	$('#otherDocStatusOnPage62P1').hide();

				 	$('#docType_other').prop("checked", true); // suneel 05/09/2014
				 	$('#otherDocStatusOnPage62P1').show(); // Suneel 05/09/2014

				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");

				  $('#DocAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				  $('#DocI-94Number').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number);
				  $('#passportDocNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber);
				  $('#passportExpDate').val(UIDateformat(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate));
				  $('#sevisIDNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);
				  $('#documentDescription').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentDescription);
				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected == "NaturalizationCertificate")
			 	{
				 	$('#naturalizedCitizenNaturalizedIndicator').prop("checked", true);
				 	$('.appscr62Part1HideandShowFillingDetails').hide();


				 	$('#divNaturalizationCertificate').show();

				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");
				 	$('#naturalizationAlignNumber').val("");
				 	$('#naturalizationCertificateNumber').val("");

				 	$('#naturalizationAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#naturalizationCertificateNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);


				 }
			 else if(householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected == "CitizenshipCertificate")
			 	{
				 	$('#naturalizedCitizenNaturalizedIndicator2').prop("checked", true);
				 	$('.appscr62Part1HideandShowFillingDetails').hide();

				 	$('#divCitizenshipCertificate').show();

				 	$('#DocAlignNumber').val("");
				 	$('#DocI-94Number').val("");
				 	$('#passportDocNumber').val("");
				 	$('#passportExpDate').val("");
				 	$('#sevisIDNumber').val("");
				 	$('#documentDescription').val("");
				 	$('#citizenshipAlignNumber').val("");
				 	$('#appscr62p1citizenshipCertificateNumber').val("");

				 	$('#citizenshipAlignNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber);
				 	$('#appscr62p1citizenshipCertificateNumber').val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId);


				 }
			 else {
				 $('#otherDocStatusOnPage62P1').hide(); // Suneel 05/09/2014
			 }


			 // living in US since 1962
			 var livedInUs = householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator;
			 if(livedInUs == true) {
				 $('#62Part_livesInUS').prop("checked", true); // suneel 05/09/2014

			 }
			 else if(livedInUs == false) {
				 $('#62Part_livesInUSNo').prop("checked", true); // suneel 05/09/2014
			 }

			 var honourableVeteran = householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator;
			if(honourableVeteran == true || honourableVeteran == 'true') {
				$('#62Part_honourably-veteran').prop("checked", true); // suneel 05/09/2014
			} else if(honourableVeteran == false || honourableVeteran == 'false') {
				$('#62Part_honourably-veteranNo').prop("checked", true); // suneel 05/09/2014
			}

			// Other documents
			$('#haveTheseDocuments').show();
			$('#otherDocORR').prop('checked', false);
			$('#otherDocORR-under18').prop('checked', false);
			$('#Cuban-or-HaitianEntrant').prop('checked', false);
			$('#docRemoval').prop('checked', false);
			$('#adminOrderbyDeptofHomelanSecurity').prop('checked', false);
			$('#americanSamaoresident').prop('checked', false);

			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORRCertificationIndicator){
				$('#otherDocORR').prop('checked', true);
			}

			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORREligibilityLetterIndicator){
				$('#otherDocORR-under18').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.CubanHaitianEntrantIndicator){
				$('#Cuban-or-HaitianEntrant').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.WithholdingOfRemovalIndicator){
				$('#docRemoval').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.AmericanSamoanIndicator){
				$('#americanSamaoresident').prop('checked', true);
			}
			if(householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.StayOfRemovalIndicator){
				$('#adminOrderbyDeptofHomelanSecurity').prop('checked', true);
			}


		// Name different on document
			var nameSameonDocument = householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator;
			$('#sameNameInfoPage62P2').show(); // Suneel 05/09/2014

			//$('#62Part2_UScitizenYes').prop("checked", true); // suneel 05/09/2014
			if(nameSameonDocument == true) {
				$('#62Part2_UScitizenYes').prop("checked", true); // suneel 05/09/2014

				$("#documentFirstName").val("");
				$("#documentMiddleName").val("");
				$("#documentLastName").val("");
				$("#documentSuffix").val("");

			}
			else if(nameSameonDocument == false){
				$('#62Part2_UScitizen').prop("checked", true); // suneel 05/09/2014

				$("#documentFirstName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName);
				$("#documentMiddleName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName);
				$("#documentLastName").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName);
				$("#documentSuffix").val(householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix);

			}else{
				$('input[name=UScitizen62]').prop("checked", false);
			}

			displayOrHideSameNameInfoPage62();



}

function appscr63FromJSON(i){


	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	$("input[name=appscr63AtLeastOneChildUnder19Indicator]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator == true){
		$("#appscr63AtLeastOneChildUnder19Yes").prop("checked", true);
	 	parentCareTakerHideShow();
	} else if(householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator == false) {
		$("#appscr63AtLeastOneChildUnder19No").prop("checked", true);
	 	parentCareTakerHideShow();
	}


	//householdMemberObj.parentCaretakerRelatives.personId[0].personid =1 ;
	$("input[name=takeCareOf]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.anotherChildIndicator == true){
		$("#takeCareOf_anotherChild").prop("checked", true);
		method4AnotherChildCheckBox();

	}

	$('#parent-or-caretaker-firstName').val(householdMemberObj.parentCaretakerRelatives.name.firstName);
	$('#parent-or-caretaker-middleName').val(householdMemberObj.parentCaretakerRelatives.name.middleName);
	$('#parent-or-caretaker-lastName').val(householdMemberObj.parentCaretakerRelatives.name.lastName);
	$('#parent-or-caretaker-suffix').val(householdMemberObj.parentCaretakerRelatives.name.suffix);
	$('#parent-or-caretaker-dob').val(UIDateformat(householdMemberObj.parentCaretakerRelatives.dateOfBirth));
	$('#applicant-relationship').val(householdMemberObj.parentCaretakerRelatives.relationship);
	updateFullNameOfOtherChild();
	$("input[name=birthOrAdoptiveParents]").prop("checked",false);
	if(householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator == true){
		$("#appscr63BirthOrAdoptiveParentsYes").prop("checked", true);
	} else if (householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator == false){
		$("#appscr63BirthOrAdoptiveParentsNo").prop("checked", true);
	}

}

function appscr66FromMongodbJson(){
	var disability;
	var i_cover_adoption;
	var i_nativeAmericon;
	var i_cover_foster;
	var i_deceased;
	var isPregnant;
	var fatusCount;
	var isPersonGettingHealthCare;
	var noneOftheseDisabilityIndicator;

	var noAmericanIndianAlaskaNative = true;

	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

	for ( var i = 1; i <= noOfHouseHoldmember; i++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		disability = householdMemberObj.disabilityIndicator;
		i_nativeAmericon = householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator;
		i_cover_foster = householdMemberObj.specialCircumstances.fosterChild;
		isPregnant = householdMemberObj.specialCircumstances.pregnantIndicator;

		if(i_nativeAmericon && noAmericanIndianAlaskaNative){
			noAmericanIndianAlaskaNative = false;
		}

		try{

			if(disability == true)
				document.getElementById('disability'+i).checked = true;
			else
				document.getElementById('disability'+i).checked = false;

			if(disability == true && disabilityFlag)
				disabilityFlag = false;

			if(i_cover_adoption == true)
				document.getElementById('assistanceServices'+i).checked = true;
			else
				document.getElementById('assistanceServices'+i).checked = false;

			if(i_cover_adoption == true && i_cover_adoptionFlag)
				i_cover_adoptionFlag = false;

			if(i_nativeAmericon == true){
				document.getElementById('native'+i).checked = true;
				nativeOnChange();
				if(householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator == true){
					$('#AmericonIndianQuestionRadioYes'+i).prop("checked", true);
					checkAmericonIndianQuestion(i);
					$('#americonIndianQuestionSelect'+i).val(householdMemberObj.americanIndianAlaskaNative.state);
					objTribdNameDropdown = $('#tribeName'+i);
					tribeCode = householdMemberObj.americanIndianAlaskaNative.tribeName;
					getAmericanAlaskaTribeList(householdMemberObj.americanIndianAlaskaNative.state,objTribdNameDropdown);
				}else{
					$('#AmericonIndianQuestionRadioNo'+i).prop("checked", true);
				}
			}
			else
				document.getElementById('native'+i).checked = false;

			if(i_nativeAmericon == true && i_nativeAmericonFlag)
				i_nativeAmericonFlag = false;
			else
				i_nativeAmericonFlag = true;

			if(i_cover_foster == true)
				document.getElementById('personfosterCare'+i).checked = true;
			else
				document.getElementById('personfosterCare'+i).checked = false;

			if(i_cover_foster == true && i_cover_fosterFlag)
				i_cover_fosterFlag = false;

			if(i_deceased == true)
				document.getElementById('deceased'+i).checked = true;
			else
				document.getElementById('deceased'+i).checked = false;

			if(i_deceased == true && i_deceasedFlag)
				i_deceasedFlag = false;


			var strGender = householdMemberObj.gender;
			if(strGender.toLowerCase() == "female")
			{
				if(isPregnant == true)
				{
					document.getElementById('pregnancy'+i).checked = true;
					//getExpectedInPregnancy();
					//$('#number-of-babies'+i).val(householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy);
				}
				else
				{
					if(document.getElementById('pregnancy'+i)!=null){
						document.getElementById('pregnancy'+i).checked = false;
					}

				}
			}


			if(isPregnant == true && isPregnantFlag)
				isPregnantFlag = false;

			if(isPersonGettingHealthCare == true)
				$('#isPersonGettingHealthCareYes'+i).prop("checked", true);
			else
				$('#isPersonGettingHealthCareNo'+i).prop("checked", true);

		}catch(e){
			//console.log(e);
		}




	}

	$('#disabilityNoneOfThese').prop('checked',false);
	$('#assistanceServicesNoneOfThese').prop('checked',false);
	$('#nativeNoneOfThese').prop('checked',noAmericanIndianAlaskaNative);
	$('#personfosterCareNoneOfThese').prop('checked',false);
	$('#deceasedPersonNoneOfThese').prop('checked',false);




	//Is XXX eligible for health coverage from any of the following? Select even if bb bbb isnt currently enrolled:
	var noOfHouseHold = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

	for(var i=1;i<=noOfHouseHold;i++){
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		if(householdMemberObj.healthCoverage.otherInsuranceIndicator === false){
			$("#appscr78None"+i).prop("checked",true);
		}else if(householdMemberObj.healthCoverage.otherInsuranceIndicator === true){
			$("input:radio[name=fedHealth"+ i +"]").each(function(){
				if($(this).val() == householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected){
					$(this).prop("checked",true);
				}

				if(householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected == "Other"){
					otherInsuranceCheckbox(i);
					$("#appscr78TypeOfProgram" + i).val(householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType);
					$("#appscr78NameOfProgram" + i).val(householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName);
				}
			});
		}

	}
	/*if (haveData == true) {
		appscr66NoneOfThese();
	}*/
}

function appscr85FromMongodbJSON(){
	//agreement
	var parimaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	if(webJson.singleStreamlinedApplication.consentAgreement === true){
		$('#useIncomeDataIndicatorYes').prop('checked',true);
		updateTaxReturnPeriod();
	}else if(webJson.singleStreamlinedApplication.consentAgreement === false){
		$('#useIncomeDataIndicatorNo').prop('checked',true);
		updateTaxReturnPeriod();
		$('#taxReturnPeriod' + webJson.singleStreamlinedApplication.numberOfYearsAgreed).prop('checked',true);

	}
}

	function UIDateformat(dt){
		if(trimSpaces(dt)=="") return dt;
		if(dt==undefined) return "";
		var formattedDate = new Date(dt);
		var dd = formattedDate.getDate();
		var mm =  formattedDate.getMonth();
		mm += 1;  // JavaScript months are 0-11
		var yyyy = formattedDate.getFullYear();
		if(dd<10){dd='0'+dd} if(mm<10){mm='0'+mm}
		formatteddate = mm + "/" + dd + "/" + yyyy;
		return formatteddate;

	}

	function DBDateformat(dt){
		if(trimSpaces(dt)=="") return null;
		if(dt==undefined) return null;

		var monthNames = new Array("January", "February", "March",
				"April", "May", "June", "July", "August", "September",
				"October", "November", "December");

		var MonthNameShortLength = 3; //0 for full month name

		var arrDate = dt.split('/');
		var monthName = "";
		var monthNumber = arrDate[0].replace(/^0+/, '');
		if (MonthNameShortLength == 0){
			month = monthNames[monthNumber-1];
		} else {
			month = monthNames[monthNumber-1].substring(0, MonthNameShortLength);
		}
		var formattedDate = month + " " +  arrDate[1] + ", " + arrDate[2] + " 12:00:00 AM" ;
		return formattedDate;

	}

	function getDecimalNumber(num){
		if(num == null || trimSpaces(num)=="" ) return null;

		if(num == 0 ) return 0;

		return parseFloat(Number(num).toFixed(2));

	}

	function getIntegerNumber(num){
		if(num == null || trimSpaces(num)=="" ) return null;

		if(num == 0 ) return 0;

		if(num == parseInt(num)){
			return parseInt(num) ;
		}else {
			return null;
		}



	}

	function Moneyformat(num) {
		if(num == null || trimSpaces(num)=="" ) return num;
		if(num == 0 ) return 0;
	    var p = Number(num).toFixed(2).split(".");
	    return p[0].split("").reduceRight(function(acc, num, i, orig) {
	        if ("-" === num && 0 === i) {
	            return num + acc;
	        }
	        var pos = orig.length - i - 1
	        return  num + (pos && !(pos % 3) ? "," : "") + acc;
	    }, "") + (p[1] ? "." + p[1] : "");
	}


	function checkHelpType(){
		var helpType = webJson.singleStreamlinedApplication.helpType;
		if(helpType == "BROKER"){
			$("#brokerDiv, #authorizedRepresentativeDiv").show();
		}else if(helpType == "ASSISTER"){
			$("#guideDiv, #authorizedRepresentativeDiv").show();
		}else{
			$("#helpQuestionDiv").show();
		}
	}


/*
****************************************************************************************************************
* navigation.js                                                                                       *
****************************************************************************************************************
*/

	var haveData = false; /* change to false when the user is logged in */
	var firstNameDB = "";
	var middleNameDB = "";
	var lastNameDB = "";
	var emailDB = "";
	var dobDB = "";
	var phoneDB = "";
	var validationCheck = true;
	var useJSON = true;
	var webJson = "";
	var submitForAutoContinue = false;
	var mode = "";
	var totalApplicants = 0;

	var ssapApplicationJson = $('#ssapApplicationJson').val();
	var ssapCurrentPageId = $('#ssapCurrentPageId').val();
	var ssapApplicationStatus = $('#ssapApplicationStatus').val();//$('#ssapApplicationType').val()
	var applicationType = $('#applicationType').val();
	var ssapApplicationType = '';

	var dateValidation = true;
	var signatureValidation = true;

	var applicationStatus = "";


	var currentPage = 'appscr51';

	var currentPersonIndex = -1;
	var brokerCertificationNumber = "";
	var brokerName="";
	var brokerFirstName = "";
	var brokerLastName = "";

	var noneOftheseDisabilityIndicator = false;
	var noneOftheseAssistanceServices = false;
	var noneOftheseAmericanIndianAlaskaNativeIndicator=false;
	var noneOftheseEverInFosterCareIndicator=false;
	var noneoftheseDeceased = false;
	var nonoftheseIsAvailableForHealthCoverage= false;
	var noneofthesePregnancy = false;
	var applyingForCoverageIndicatorArrCopy = [];
	var exchangeName = $('#exchangeName').val();

	var pageArr = new Array('appscr51', 'appscr52', 'appscr53', 'appscr54',
			'appscr55', 'appscr57', 'appscr58', 'appscr59A', 'appscr60',
			'appscr61', 'appscr62Part1',  'appscr63', 'appscr64',
			'appscr65', 'appscr66', 'appscrBloodRel','appscr67', 'appscr68', 'appscr69', 'appscr70',
			'appscr71', 'appscr72', 'appscr73', 'appscr74', 'appscr75A',
			'appscr76P1', 'appscr77Part1', 'appscr76P2', 'appscr81B', 'appscr83',
			'appscr84','appscr85', 'appscr87');

	var f = new Object();
	f.appscr51 = 'Before We Begin';
	f.appscr52 = 'Get Ready to Start Your Application';
	f.appscr53 = 'Primary Contact Information';
	f.appscr54 = 'Application Assistance';
	f.appscr55 = 'Applicants';
	f.appscr56 = 'How Many are Applying for Health Insurance';
	f.appscr57 = 'About Your Household';
	f.appscr58 = 'Household Summary';
	f.appscr59A = 'Get Ready for Family and Household';
	/* f.appscr59B = ' Family & Household'; */

	f.appscr60 = 'Tell Us About your Household';

	var householdFName = $('#firstName').val();
	var householdLName= $('#lastName').val();
	var householdName = householdFName + " " + householdLName;

	f.appscr61 = 'Personal Information';
	f.appscr62Part1 = 'Citizenship/Immigration Status';
	f.appscr62Part2 = 'Citizenship/Immigration Status';
	f.appscr63 = 'Parent/Caretaker Relatives';
	f.appscr64 = 'Ethnicity and Race';
	f.appscr65 = 'Other Addresses';
	f.appscr66 = 'Additional Information';
	f.appscr67 = 'Family and Household Summary';
	f.appscr68 = 'Income';
	f.appscr69 = 'Expected Annual Income';
	f.appscr70 = 'Current Income';
	f.appscr71 = 'Current Income Details';
	f.appscr72 = 'Current Income Details';
	f.appscr73 = 'Income Discrepancies';
	f.appscr74 = 'Income Summary';

	f.appscr75A = 'Additional Question';

	f.appscr76P1 = 'Health Insurance Information';
	f.appscr76P2 = 'Health Insurance Information';
	f.appscr77Part1 = 'Employer Health Coverage Information';
	f.appscr77Part2 = 'Employer Health Coverage Information';
	f.appscr81B = 'Medicaid & CHP + Specific Questions';
	f.appscr82 = 'Additional Questions Summary';
	f.appscr83 = 'Review and Sign';
	f.appscr84A = 'Part1 - Review Application';
	f.appscr84B = 'Part2 - Review Application';
	f.appscr84 = 'Final Application Review';
	f.appscr85 = 'Sign and Submit';
	f.appscr86 = ' Required Documents';
	f.appscrBloodRel = 'Relationships';
	f.appscr87 = 'Application Complete';

	var tabClass = new Object();
	tabClass.appscr51 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr52 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr53 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr54 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr55 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr56 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr57 = "active#NONE#NONE#NONE#NONE";
	tabClass.appscr58 = "active#NONE#NONE#NONE#NONE";

	tabClass.appscr59A = "visited#active#NONE#NONE#NONE";
	tabClass.appscr60 = "visited#active#NONE#NONE#NONE";

	tabClass.appscr61 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr62Part1 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr62Part2 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr63 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr64 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr65 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr66 = "visited#active#NONE#NONE#NONE";
	tabClass.appscr67 = "visited#active#NONE#NONE#NONE";
	tabClass.appscrBloodRel = "visited#active#NONE#NONE#NONE";

	tabClass.appscr68 = "visited#visited#active#NONE#NONE";
	tabClass.appscr69 = "visited#visited#active#NONE#NONE";
	tabClass.appscr70 = "visited#visited#active#NONE#NONE";
	tabClass.appscr71 = "visited#visited#active#NONE#NONE";

	tabClass.appscr72 = "visited#visited#active#NONE#NONE";
	tabClass.appscr73 = "visited#visited#active#NONE#NONE";
	tabClass.appscr74 = "visited#visited#active#NONE#NONE";

	tabClass.appscr75A = "visited#visited#visited#active#NONE";
	tabClass.appscr76P1 = "visited#visited#visited#active#NONE";
	tabClass.appscr76P2 = "visited#visited#visited#active#NONE";
	tabClass.appscr77Part1 = "visited#visited#visited#active#NONE";
	tabClass.appscr77Part2 = "visited#visited#visited#active#NONE";
	tabClass.appscr81B = "visited#visited#visited#active#NONE";
	tabClass.appscr82 = "visited#visited#visited#active#NONE";
	tabClass.appscr83 = "visited#visited#visited#visited#active";
	tabClass.appscr84A = "visited#visited#visited#visited#active";
	tabClass.appscr84B = "visited#visited#visited#visited#active";
	tabClass.appscr84 = "visited#visited#visited#visited#active";
	tabClass.appscr85 = "visited#visited#visited#visited#active";
	tabClass.appscr86 = "visited#visited#visited#visited#active";
	tabClass.appscr87 = "visited#visited#visited#visited#active";

	var pageMethod = new Object();
	pageMethod.appscr51 = "next();";
	pageMethod.appscr52 = "next();";
	pageMethod.appscr53 = "triggerMailingAddressValidator();";
	pageMethod.appscr54 = "saveData54();loadData55();";
	pageMethod.appscr55 = "saveData55();createHousehold();";
	pageMethod.appscr56 = "createHousehold()";
	pageMethod.appscr57 = "saveData57();";
	pageMethod.appscr58 = "next();";
	pageMethod.appscr59A = "getSpouse(1);setNonFinancialPageProperty(1);next();";

	pageMethod.appscr60 = "continueData60();addHouseHold()";

	pageMethod.appscr61 = "continueData61();addNonFinancialHouseHold()";
	pageMethod.appscr62Part1 = "continueData62Part1();addNonFinancialHouseHold()";
	pageMethod.appscr62Part2 = "continueData62Part2();addNonFinancialHouseHold()";
	pageMethod.appscr63 = "continueData63();addNonFinancialHouseHold()";
	pageMethod.appscr64 = "continueData64();addNonFinancialHouseHold()";

	pageMethod.appscr65 = "continueData65();addSpecialCircumstances();next();";
	pageMethod.appscr66 = "continueData66();saveSpecialCircumstances();";
	pageMethod.appscr67 = "next();";
	pageMethod.appscr68 = "setupPage69();";

	pageMethod.appscr69 = "continueData69();addFinancialHouseHold()";
	pageMethod.appscr70 = "continueData70();addFinancialHouseHold()";
	pageMethod.appscr71 = "continueData71();addFinancialHouseHold()";
	pageMethod.appscr72 = "continueData72();addFinancialHouseHold()";
	pageMethod.appscr73 = "continueData73();addFinancialHouseHold()";
	pageMethod.appscr74 = "next()";
	pageMethod.appscr75A = "next(); setNonFinancialPageProperty(1); setHouseHoldData()";
	pageMethod.appscr76P1 = "continueData76P1();next()";

	pageMethod.appscr77Part1="next()";
	pageMethod.appscr76P2 = "continueData76P2();next()";
	pageMethod.appscr82 = " next();";
	pageMethod.appscr81B = "continueData77Part2();addAdditionalQuetionDataToTable();continueData78(); continueData79();continueData81A();addDataAppscr81ToTable();addQuestionSummery(); ";

	pageMethod.appscr83 = "addReviewSummary();";
	//pageMethod.appscr84 = "addIncarceratedContent(); caller();setNonFinancialPageProperty(1);";
	pageMethod.appscr84 = "caller();";
	pageMethod.appscr85 = "saveIncarceratedDetail();";
	pageMethod.appscr86 = "setButtons();";
	pageMethod.appscrBloodRel = "saveBloodRelationData();";

	pageMethod.appscr87 = "goToDashboard();";

	function goToDashboard() {
		location.href="indportal";
	}

	function changeTabCss(strCol) {
		var classArr = strCol.split('#');
		$('#tab1').removeClass('visited').removeClass('active').removeClass(
				'active').removeClass('active').removeClass('NONE').addClass(
				classArr[0]);
		$('#tab2').removeClass('visited').removeClass('active').removeClass(
				'active').removeClass('active').removeClass('NONE').addClass(
				classArr[1]);
		$('#tab3').removeClass('visited').removeClass('active').removeClass(
				'active').removeClass('active').removeClass('NONE').addClass(
				classArr[2]);
		$('#tab4').removeClass('visited').removeClass('active').removeClass(
				'active').removeClass('active').removeClass('NONE').addClass(
				classArr[3]);
		$('#tab5').removeClass('visited').removeClass('active').removeClass(
				'active').removeClass('active').removeClass('NONE').addClass(
				classArr[4]);
	}

	function goToPageById(pageId) {

		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;

		updateProgressBar(currentPage);
		backButton(currentPage);
		saveButton(currentPage);
		//focus on first element for accessibility
		document.activeElement.blur();

		getCurrentPageMethod(currentPage);

		changeTitle(currentPage);
		if ((currentPage == 'appscr73' && ($(
				'input[name=aboutIncomeSpped]:radio:checked').val() == 'yes' || ($(
				'input[name=aboutIncomeSpped]:radio:checked').val() == 'no' && $(
				'input[name=incomeHighOrLow]:radio:checked').val() == 'higher')))) {
			$('#prevBtn').click();
		}

		//scroll to top if error message shows
		$("html, body").animate({ scrollTop: 0 }, "fast");
	}

	function getCurrentPageMethod(page) {

		var v = 0;
		for (; v < pageArr.length; v++) {
			if (page == pageArr[v]) {
				changeContinue(pageMethod[pageArr[v]]);
				return;
			}
		}
		changeContinue('next();');
	}

	function getCurrentPageTabCss(page) {
		var v = 0;
		for (; v < pageArr.length; v++) {
			if (page == pageArr[v]) {
				changeTabCss(tabClass[pageArr[v]]);
				return;
			}
		}
	}

	function getMethod(page) {

		var v = 0;
		for (; v < pageArr.length; v++) {
			if (page == pageArr[v]) {

				changeContinue(pageMethod[pageArr[v]]);
				return;
			}
		}

		changeContinue('next();');
	}

	function changeContinue(methodName) {
		$('#contBtn').attr("onClick", methodName);

	}

	function getNextPage() {

		var v = 0;
		for (; v < pageArr.length; v++) {
			if (currentPage == pageArr[v]) {
				return pageArr[v + 1];
			}
		}

		return false;
	}

	function getPrevPage() {

		var v = 0;
		for (; v < pageArr.length; v++) {
			if (currentPage == pageArr[v]) {
				return pageArr[v - 1];
			}
		}

		return false;
	}

	function next() {
		pageValidation();

	/*	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}*/

		// At last page if user enters wrong name at esignature testbox, the signatureValidation variable will be false. for all other pages it will be true.
		if(!signatureValidation){
			return;
		}

		if(!dateValidation){

			$('html, body').animate({
				scrollTop : $("#"+currentPage).find('.dobCustomError span').first().offset().top - 250
			}, 'fast');
			return;
		}

		//Atleast 1 person should be seeking coverage to continue the application
		if(currentPage == 'appscr58'){
			if(coverageMinimumCheck() == false){
				$('#coverageMinimumAlert').modal();
				return;
			}
		}

		/*if(currentPage == 'appscr67' ){
			assignjointTaxFilerSpousePersonId();
		}
		if(currentPage == 'appscr74' ){
			derivehouseHoldIncome();
		}*/

		if (currentPage == 'appscr67' || currentPage == 'appscr74' || currentPage == 'appscr82') {
			editNonFinacial = false;
			saveData();
		}

		if (currentPage == 'appscr84') {
			$("#waiting").hide();
		}

		/* added by shovan */
		if (currentPage == "appscr51") {
			termsCheckboxIsCheck();
		}
		/* code ends here by @shovan */

		if (currentPage == "appscr51" && validation(currentPage) == false) {
			return false;
		}

		if (currentPage == "appscr55" && haveData == false) {
			setValuesToappscr57JSON();
		}

		if(currentPage == "appscr65" && haveData == false )
		{
			addSpecialCircumstances();
			appscr66FromMongodbJson();
			//appscr66FromMongodb();
			//appscr66NoneOfThese();
		}

		if (currentPage == "appscr59B") {
			validationCheck = true;
		}

		if (validationCheck == false) {
			return false;
		}

		if ((currentPage == "appscr68" || currentPage == "appscr69" || currentPage == "appscr70")  && haveData == false) {
			fillValuesToFields(currentPage);
		}

		if (haveData == true) {
			fillValuesToFields(currentPage);
		}

		var pageId;
		var ssapApplicationObj = webJson.singleStreamlinedApplication;
		if(currentPage == 'appscr67' && ssapApplicationObj.applyingForFinancialAssistanceIndicator==false){
			addReviewSummary();// suneel 05092014
			pageId = 'appscr83';
			//skip ParentOrCaretaker page if under 19 or NFA
		}else if(currentPage == 'appscr62Part1' && (hideParentOrCaretaker() || ssapApplicationType == "NON_FINANCIAL")){
			pageId = 'appscr64';
		}else if(currentPage == 'appscr59A' && ssapApplicationType == "NON_FINANCIAL"){
			pageId = 'appscr61';
			appscr67EditController(nonFinancialHouseHoldDone);
		}else if(currentPage == 'appscr76P1' && !ssapApplicationObj.taxHousehold[0].householdMember[addQuetionsHouseHoldDone-1].healthCoverage.employerWillOfferInsuranceIndicator){
			pageId = 'appscr76P2';
		}else if(currentPage == 'appscr64' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
			//skip other address page if there is only 1 person
			addSpecialCircumstances();
			appscr66FromMongodbJson();
			pageId = 'appscr66';
		}else if(currentPage == 'appscr66' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
			//skip relationship page if there is only 1 person
			updateSingleMemRel();
			showHouseHoldContactSummary();
			pageId = 'appscr67'; //summary page
		}else{
			pageId = getNextPage();
		}

		if (!pageId) {
			return false;
		}



		$('#' + currentPage).hide();
		$('#' + pageId).show();

		getMethod(pageId);
		currentPage = pageId;

		updateProgressBar(currentPage);

		//focus on first element for accessibility
		document.activeElement.blur();

		if (haveData == false && currentPage == "appscr75A" && useJSON == true) {
			setNonFinancialPageProperty(1);
			appscr76FromMongodb();
		}
		backButton(currentPage);
		saveButton(currentPage);
		changeTitle(currentPage);
		getCurrentPageTabCss(currentPage);

		if (currentPage == "appscr54") {
			setFederalAmount400();
		}

		if (currentPage == "appscrBloodRel") {
			populateDataToRelations();
			//populateRelationship();
		}

		if (currentPage == "appscr60" && haveData == false) {
			updateAppscr60FromJSON(HouseHoldRepeat);
		}

		if (currentPage == "appscr62Part1") {
			appscr61FromJSON(nonFinancialHouseHoldDone);
		}

		if (currentPage == "appscr63" && haveData == false) {
			appscr63FromJSON(nonFinancialHouseHoldDone);
		}

		if (currentPage == "appscr64") {
			fillAppscr64(nonFinancialHouseHoldDone);
		}

		if (currentPage == "appscr65" && haveData == false) {
			appscr65FromMongodbJSON();
		}

		if (currentPage == "appscr67") {
			nonFinancialHouseHoldDone = getNoOfHouseHolds();
		}

		if (currentPage == 'appscr68') {
			setNonFinancialPageProperty(1);
			$('#prevBtn').attr('onclick', 'prev();');
		}

		if (currentPage == "appscr85"){
			appscr85FromMongodbJSON();
		}

		if (currentPage == 'appscr59A') {
			$('.button_ui_design_rote').css('margin-top', '-100px');
		} else {
			$('.button_ui_design_rote').css('margin-top', '');
		}

		//scroll to top if error message shows
		$("html, body").animate({ scrollTop: 0 }, "fast");


		if (currentPage == "appscr73"
				&& ($('input[name=aboutIncomeSpped]:radio:checked').val() == 'yes' || ($(
						'input[name=aboutIncomeSpped]:radio:checked').val() == 'no' && $(
						'input[name=incomeHighOrLow]:radio:checked').val() == 'higher'))) {
			$('#contBtn').click();
		}

		if (haveData == false && currentPage == "appscr64" && useJSON == true) {
			appscr65FromMongodbJSON();
		}


		if (currentPage == "appscr85"){
			$("#contBtn").text("Submit Application");
			$("#waiting").hide();
		}else if(currentPage == "appscr87"){
			$("#contBtn").text("Go to Dashboard");
			//$("#sidebar").hide();
			$( "#sidebar" ).children(".header, .nav-list").hide();

			if(applicationType == "OE"){
				$("#OEPart").show();
			}else if(applicationType == "QEP"){
				$("#specialEnrollmentDenied").show();
			}


		}else{
			$("#contBtn").text("Continue");
		}

	}
	
	function isSepEventDateValid(){
		var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
		var today = new Date();
		var currentDate =new Date(parseInt(today.getMonth())+1 + "/" + today.getDate() + "/" + today.getFullYear());
		var eventDate = new Date(sepEventDate);
		//var diffDays = Math.round(Math.abs(currentDate.getTime() - (eventDate.getTime())/(oneDay)));
		var dateDiff =parseInt(currentDate.getTime() - (eventDate.getTime()));
		var diffDays = 0;
		if(dateDiff != 0){
			diffDays = Math.round(dateDiff/(oneDay));
		}
		
		return parseInt(diffDays) <= parseInt($('#qepAllowedInterval').val())
		
	}
	function getQEPEnrollmentDate(qepEventDate){
		var qepEnrollmentDate = new Date(qepEventDate);
		qepEnrollmentDate.setDate(qepEnrollmentDate.getDate() + parseInt($('#qepAllowedInterval').val()));
		return qepEnrollmentDate;

	}
	function changeTitle(page) {

		document.title = f[page];

		/*Added by Kunal for adding Dynamic page title*/

		if (currentPage == "appscr60" || currentPage == "appscr61" || currentPage == "appscr62Part1" || currentPage == "appscr62Part2" ||
				currentPage == "appscr63" || currentPage == "appscr64" || currentPage == "appscr69" ||
				currentPage == "appscr70" || currentPage == "appscr71" || currentPage == "appscr72" || currentPage == "appscr86" )
		{
			$('#headerHouseHoldName').show();
			}
		else{
			$('#headerHouseHoldName').hide();
		}

		$('.SSAPpgtitle').text('');
		$('.SSAPpgtitle').append("&nbsp;" + f[page]);
		/*Added by Kunal for adding Dynamic page title ends here*/
	}

	function prev() {

		validationCheck = true;
		dateValidation = true;
		signatureValidation = true;
		$('#'+$("#"+currentPage).find("form").attr("id")).find("div.errorsWrapper").hide();
		if (currentPage == "appscr52" || currentPage == "appscr53")
			validationCheck = true;

		if (currentPage == "appscr54")
			backData53();

		if (currentPage == "appscr55"){
			backData54();
			checkFinacialAssistance();
		}

		if (currentPage == "appscr57") {
			backData55();
			deleteHouseholdForm();
		}

		if (currentPage == "appscr58"){
			backData57();
			setValuesToappscr57JSON();
		}

		if (currentPage == "appscr59A"){
			Appscr58DetailJson();
		}

		if (currentPage == "appscr60")
			backData60();

		if(currentPage == "appscr61")	{
			updateAppscr60FromJSON(HouseHoldRepeat);
		}

		if (currentPage == "appscr62Part1")
			backData61();

		if (currentPage == "appscr62Part2")
			backData62Part1();

		if (currentPage == "appscr63") {
			backData62Part1();
		}

		if (currentPage == "appscr64")
			backData63();

		if (currentPage == "appscr66"){
			continueData66();
			backData65();
		}
		if (currentPage == "appscrBloodRel"){
			addSpecialCircumstances();
			appscr66FromMongodbJson();
		//appscr66FromMongodb();
		//appscr66NoneOfThese();
		}
		if (currentPage == "appscr67")
			backData66();

		if (currentPage == "appscr70")
			backData69();

		if (currentPage == "appscr71")
			backData70();

		if (currentPage == "appscr72")
			backData71();

		if (currentPage == "appscr73")
			backData72();

		if (currentPage == "appscr76P2")
			backData76P2();

		if (currentPage == "appscr77Part1")
			backData76P2();

		if (currentPage == "appscr77Part2")
			backData77Part1();

		if (currentPage == "appscr78")
			backData77Part2();

		if (currentPage == "appscr79")
			backData78();

		if (currentPage == "appscr81A")
			backData79();

		if (currentPage == "appscr81B")
			backData81A();

		if (currentPage == "appscr83"){
			showHouseHoldContactSummary();
		}

		if (currentPage == "appscr85")
			$('#saveBtn').show();

		if (baseFunctionForBackButton() == false) {
			return false;
		}

		removeValidationClass(currentPage);

		if (currentPage == "appscr75A") {
			totalIncomeShowMethod();
		}

		var pageId;
		var ssapApplicationObj = webJson.singleStreamlinedApplication;
		if(currentPage == 'appscr64' && (hideParentOrCaretaker() || ssapApplicationType == "NON_FINANCIAL")){
			backData62Part1();
			pageId = 'appscr62Part1';
		}else if(currentPage == 'appscr83' && ssapApplicationObj.applyingForFinancialAssistanceIndicator == false){
			pageId = 'appscr67';
		}else if(currentPage == 'appscr76P2' && !ssapApplicationObj.taxHousehold[0].householdMember[addQuetionsHouseHoldDone-1].healthCoverage.employerWillOfferInsuranceIndicator){
			pageId = 'appscr76P1';
		}else if(currentPage == 'appscr66' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
			//skip address other page if there is only 1 person
			pageId = 'appscr64';
			fillAppscr64(nonFinancialHouseHoldDone);
		}else if(currentPage == 'appscr67' && ssapApplicationObj.taxHousehold[0].householdMember.length == 1){
			//skip relationship page if there is only 1 person
			pageId = 'appscr66';
			addSpecialCircumstances();
			appscr66FromMongodbJson();
			//appscr66NoneOfThese();
		}else{
			pageId = getPrevPage();
		}

		if (!pageId) {
			alert('No Previous page!');
			return false;
		}

		$('#' + currentPage).hide();
		$('#' + pageId).show();

		currentPage = pageId;

		updateProgressBar(currentPage);
		//focus on first element for accessibility
		document.activeElement.blur();


		if (currentPage == 'appscr59A') {
			$('.button_ui_design_rote').css('margin-top', '-100px');
		} else {
			$('.button_ui_design_rote').css('margin-top', '');
		}

		backButton(currentPage);

		saveButton(currentPage);
		changeTitle(currentPage);
		getCurrentPageMethod(currentPage);
		getCurrentPageTabCss(currentPage);

		//scroll to top if error message shows
		$("html, body").animate({ scrollTop: 0 }, "fast");

		if (currentPage == "appscr56") {
			goToPageById("appscr55");
		}

	}

	function deleteHouseholdForm() {
		var strouter = "";
		$('#appscr57HouseholdForm').html(strouter);
	}

	function backButton(pageID) {
		//if (pageID == "appscr51") { // suneel 05092014
		if (pageID == "appscr51" || pageID == "appscr87") {
			$('#prevBtn').hide();
		} else {
			$('#prevBtn').show();
		}
		if (pageID == "appscr57") {
			$('#cancelButtonCancel').hide();
			$('#back_button_div').show();
		}
	}

	function saveButton(pageID) {
		if (pageID == "appscr51" || pageID == "appscr52" || pageID == "appscr59A" || pageID == "appscr83" || pageID == "appscr87") {
			$('#save_button_div').hide();
		} else {
			$('#save_button_div').show();
		}
	}

	function setCurrentFinancialYear() {
		var today = new Date();
		var month = today.getMonth();
		var year = 1900 + today.getYear();
		var appYear = year + " - ";
		if (month < 3) {
			year--;
		} else {
			year++;
		}
		appYear += year;
		return appYear;
	}

	function setFederalAmount400() {

		var noOfPeople = $('#noOfPeople53').val();
		var amount = 0;
		if (noOfPeople <= 0) {
			$('#noOfPeople53').val('');
			$('#noOfPeople53').addClass('validateColor');
			setTimeout("$('#noOfPeople53').removeClass('validateColor');", 2000);
			$('#amount400').html(0);
			return;
		}
		amount = (11490 + (4020 * (noOfPeople - 1)));
		var amount400 = amount * 4;
		$('#amount400').html(amount400);
	}

	function noIrsData() {
		$('#irsIncome2013').val(0);
		$('#irsIncome').val(0);

		if ($('#noIrsData').is(":checked") == true) {
			$('#irsTable').hide();
			$('.irsIncome2013').text('[ Not Provided ]');
		} else {
			$('#irsTable').show();
			$('.irsIncome2013').text('$0');
		}

	}

	function irsIncomePrevYear() {
		var amount = $('#irsIncome2013').val();
		if (amount != undefined)
			amount = (amount.length == 0) ? 0 : amount;
		else
			amount = 0;
		$('#irsIncome').val(amount);
		$('.irsIncome2013').text('$' + amount);
	}

	// Keys
	function irsIncomeResetValues() {

		$('#irsIncomeLabel').text(jQuery.i18n.prop('ssap.amountFromIRS'));
		$('#irsIncome2013').val('');
		/* document.getElementById('noIrsData').checked = false; */
		$('#irsTable').show();
	}

	function irsIncome2014(obj) {
		var val = obj.value;
		/* $('#expectedIncomeByHouseHoldMember').val(val); */
	}

	function addRemoveClass(id, bool) {

		if (bool)
			$('#' + id).addClass('validateColor');
		else
			$('#' + id).removeClass('validateColor');

	}

	function editFinalPage() {

		var editType = $('input[name=editRadioGroup]:radio:checked').val();

		$('#appscr57HouseHoldPersonalInformation tr').each(function(i) {
			$('#' + editType + (i + 1)).attr("checked", false);
		});

		$('.show_divtext').hide();
		$('.ad_alin_ui1').removeClass('visited_list');
		$('#' + editType + "_head").addClass('visited_list');
		$('#' + editType + "_div").slideDown('slow');

	}

	function populateChechBox() {

		var strToApp = '';
		$('#appscr57HouseHoldPersonalInformation tr').each(
				function(i) {
					var pureNameOfUser = '';
					$("td", this).each(function(j) {
						if (j < 3)
							pureNameOfUser += $(this).text();
						if (j < 2)
							pureNameOfUser += ' ';
					});

					strToApp += "<div class='checkbox_alinment'>"
							+ "<input id='_TYPE" + (i + 1)
							+ "' type='checkbox' name='CheckboxGroup" + (i + 1)
							+ "' value='" + pureNameOfUser
							+ "' class='list_selected_check'>"
							+ "<div class='text_style_change'>" + pureNameOfUser
							+ "</div>" + "</div>";

				});

		$('#HouseHoldDetailsUpdate_div').html(
				replaceAllCustom(strToApp, '_TYPE', 'HouseHoldDetailsUpdate'));
		$('#nonFinancialUpdate_div').html(
				replaceAllCustom(strToApp, '_TYPE', 'nonFinancialUpdate'));
		$('#financialDetailsUpdate_div').html(
				replaceAllCustom(strToApp, '_TYPE', 'financialDetailsUpdate'));

	}

	function replaceAllCustom(target, replace, replacement) {

		while (target.indexOf(replace) != -1) {
			target = target.replace(replace, replacement);
		}
		return target;
	}

	var irsIncomeLabelToolTip;
	function setupPage69() {
		//$('#houseHoldTRNo').val("HouseHold1");
		saveCurrentApplicantID(1);
		next();
	}

	function setKeyManagerForInput() {
		//$('#currentDate').val('18-Apr-2013'); // this format is not working in validating birthdate days count function checkBirthDate(fDob)
		$('#currentDate').val('05/05/2014');

		jQuery.fn.ForceCharacterOnly = function() {
			return this.each(function() {
				$(this).keydown(function(e) {
					var key = e.charCode || e.keyCode || 0;
					// allow backspace, tab, delete, arrows,
					// Alfabetics
					// home, end, and Esc.
					return (key == 27 || key == 46 || key == 8 || key == 9 || (key >= 35 && key <= 40) || (key >= 65 && key <= 90));
				});
			});
		};
		jQuery.fn.ForceNumericOnly = function() {
			return this.each(function() {
				$(this).keydown(function(e) {
					var key = e.charCode || e.keyCode || 0;

					return (key == 8 || key == 9 || key == 46 || key == 110 || key == 190 || (key >= 35 && key <= 40) || (key >= 48 && key <= 57) || (key >= 96 && key <= 105));
				});
			});
		};

		$('#Financial_Job_HoursOrDays_id, #noOfPeople53,'
		+ '#irsIncome, #irsIncome2013,'
		+ '#Financial_Job_id, #Financial_Self_Employment_id, #Financial_SocialSecuritybenefits_id, #Financial_Unemployment_id'
		+ '#Financial_Retirement_pension_id, #Financial_Capitalgains_id, #Financial_InvestmentIncome_id, #Financial_RentalOrRoyaltyIncome_id'
		+ '#Financial_FarmingOrFishingIncome_id, #Financial_AlimonyReceived_id, #Financial_OtherIncome_id, .numberStroke').ForceNumericOnly();

		$('#accordingToFederal_incomeTax').ForceNumericOnly();
		$('#basedOnAmountInput').ForceNumericOnly();

		$('#dateOfBirth, .dob-input').mask("00/00/0000");
		//$('#ssn').mask("999-99-9999");
		$('#passportExpDate').mask("00/00/0000");

		$('#unemploymentIncomeDate').mask("00/00/0000");

		$('#77P1WillBeEnrolledInHealthPlanDate').mask("12/31/9999");

		$('#ssn1').mask("999");
		$('#ssn2').mask("99");
		$('#ssn3').mask("9999");

		$('#spouseDOB').mask("00/00/0000");
		$('#taxFilerDob').mask("00/00/0000");

		$('#ssn1_80').mask("999");
		$('#ssn2_80').mask("99");
		$('#ssn3_80').mask("9999");

		$('#ssn1_81b').mask("999");
		$('#ssn2_81b').mask("99");
		$('#ssn3_81b').mask("9999");

		/*masking for amount*/

		//$(".amountMask").mask("00000000000000000");
		setControls();

		seleniumFlag = true;
	}

	function setFunctions() {

		$("input[name='docType']:radio").on('click', function() {
			if ($("#docType_other").is(":checked")) {
				$("#otherDocStatusOnPage62P1").show();

				$('#DocAlignNumber').removeClass("validateColor");
				$('#DocI-94Number').removeClass("validateColor");
				$('#passportDocNumber').removeClass("validateColor");
				$('#appscr62P1County').removeClass("validateColor");
				$('#passportExpDate').removeClass("validateColor");
				$('#sevisIDNumber').removeClass("validateColor");
			} else {
				$("#otherDocStatusOnPage62P1").hide();
			}
		});

		$(":input[name='whatHealthInsuranceDoesHaveNow']").on('click', function() {
			if ($("#otherHealthPlanRadioOnPage81P1").is(":checked")) {
				$("#otherHealthPlanOnPage80P1").show();
			} else
				$("#otherHealthPlanOnPage80P1").hide();
		});

		$(":input[name='doesHaveAPrimaryCarePhysicianP81A']").on(
				'click',
				function() {

					if ($(":input[name='doesHaveAPrimaryCarePhysicianP81A']").eq(1)
							.is(":checked")) {
						$("#primaryPhysicianInfoOnPage81P1").hide();
					} else
						$("#primaryPhysicianInfoOnPage81P1").show();
				});

		$(":input[name='whyDidThatInsuranceEnd']").on('click', function() {
			if ($(":input[name='whyDidThatInsuranceEnd']").last().is(":checked")) {
				$("#descriptionTextArea").show();
			} else
				$("#descriptionTextArea").hide();

		});

		var $appscr77Part1RG2 = $(":input[name='appscr77Part1RG2']");
		var $appscr77Part1RG3 = $(":input[name='appscr77Part1RG3']");
		var $hideandDisplayDiv = $(".appscr77P1HideOrDisplay");
		$appscr77Part1RG2.on('click', function() {
			if ($appscr77Part1RG2.first().is(":checked")) {
				$hideandDisplayDiv.first().show();
			} else {
				$hideandDisplayDiv.first().hide();
			}
		});
		$appscr77Part1RG3.on('click', function() {
			if ($appscr77Part1RG3.first().is(":checked")) {
				$hideandDisplayDiv.eq(1).show();
			} else {
				$hideandDisplayDiv.eq(1).hide();
			}
		});

		var $UScitizen77pp1RadioButton = $(":input[name='77P1ExpectChangesToHealthCoverage']");
		$UScitizen77pp1RadioButton.click(function() {
			if ($UScitizen77pp1RadioButton.first().is(":checked")) {
				$(".doesExceptAnyChangeToHealthCoverageInappscr77Part1").show();
			} else
				$(".doesExceptAnyChangeToHealthCoverageInappscr77Part1").hide();
		});

		var $UScitizen77Radios = $(":input[name='77P1WillBeEnrolledInHealthPlan']");
		$UScitizen77Radios.click(function() {
			if ($UScitizen77Radios.first().is(":checked")) {
				$("#DateToCovered").show();
			} else
				$("#DateToCovered").hide();
		});

		var $willHealthCoverageThroughJob = $(":input[name='willHealthCoverageThroughJob']");
		$willHealthCoverageThroughJob.click(function() {
			if ($willHealthCoverageThroughJob.first().is(":checked")) {
				$("#appscr76P1FirstHideAndShowDiv").show();
			} else
				$("#appscr76P1FirstHideAndShowDiv").hide();
		});

		$("#appscr76P1CheckBoxForEmployeeName").click(function() {
			if ($(this).is(":checked")) {
				$("#memberNameDataDivPart1").show();
			} else
				$("#memberNameDataDivPart1").hide();
		});

		$("#appscr76P1CheckBoxForOtherEmployee").click(function() {
			if ($(this).is(":checked")) {
				$("#memberNameDataDivPart2").show();
			} else
				$("#memberNameDataDivPart2").hide();
		});

		var $haveApPrimaryCarePhysician = $(":input[name='haveApPrimaryCarePhysician']");
		$haveApPrimaryCarePhysician.click(function() {
			if ($haveApPrimaryCarePhysician.first().is(":checked")) {
				$("#haveApPrimaryCarePhysicianOnPageAppscr76Part2").show();
			} else
				$("#haveApPrimaryCarePhysicianOnPageAppscr76Part2").hide();
		});

		$("#nameOfHouseholdOnPage76P2").click(function() {
			if ($(this).is("checked")) {
				$("#tellUsaboutUrFormerEmployerAapscr76P2").show();
			} else {
				$("#tellUsaboutUrFormerEmployerAapscr76P2").hide();
			}
		});

		$("#otherEmployeeOnPage76P2").click(function() {
			if ($(this).is(":checked")) {
				$("#secondDivToBeHideAndShowOnAppsce76P2").show();

			} else {
				$("#secondDivToBeHideAndShowOnAppsce76P2").hide();
			}
		});

		$("#naturalizedCitizenNaturalizedIndicator").click(function() {
			if ($(this).is(":checked")) {
				$(".appscr62Part1HideandShowFillingDetails").eq(0).show();
				$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();

			} else {
				$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
			}

		});

		$("#naturalizedCitizenNaturalizedIndicator2").click(function() {
			if ($(this).is(":checked")) {
				$(".appscr62Part1HideandShowFillingDetails").eq(1).show();
				$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();

			} else {
				$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();
			}
		});

		$('input[name="docType"]').bind('click', function() {
			$(".immigrationDocType").hide();
			var immigrationDocType = $(this).attr('id');
			$('#' + immigrationDocType + 'div').show();
			$("#otherDocStatusOnPage62P1").hide();
			if(immigrationDocType == 'docType_other'){
				$("#otherDocStatusOnPage62P1").show();
			}
		});
	}

	function isPrimaryAddressEmpty() {
		var addLine1 = document.getElementById("home_addressLine1").value;
		var addLine2 = document.getElementById("home_addressLine2").value;
		var city = document.getElementById("home_primary_city").value;
		var zip = document.getElementById("home_primary_zip").value;
		var state = document.getElementById("home_primary_state").value;
		var county = document.getElementById("home_primary_county").value;
		if (addLine1 == "" || city == "" || zip == "" || state == ""
				|| county == "") {
			return true;
		} else
			return false;

	}

	/*
	 * This function is used for filling the adress as the home address and mailing
	 * address is same
	 *
	 */

	function addressIsSame() {
		var contactHomeAddress = document.getElementById("mailingAddressIndicator");
		if (isPrimaryAddressEmpty() && contactHomeAddress.checked) {
			/*
			 * alert("Primary Address is not filled correctly");
			 */
			$('#primary-contact-information-form').parsley('validate');

			/*if ($('#dateOfBirth').next().hasClass('parsley-error-list')) {
				$('#dateSpan').addClass('dateSpanWithError');
			} else {
				$('#dateSpan').removeClass('dateSpanWithError');
			}*/
			contactHomeAddress.checked = false;

		}
		if (!$("#mailingAddressIndicator").is(":checked")) {
			$('#mailingAddressIndicator').attr('value', false);

		}
		if (!isPrimaryAddressEmpty()) {
			if (contactHomeAddress.checked == true) {
				$('#mailing_primary_state').removeClass("validateColor");
				$('#mailingAddressIndicator').attr('value', true);
				$('.physicalmailingAddress').hide();
				$('#mailing_addressLine1').val($('#home_addressLine1').val());
				$('#mailing_addressLine2').val($('#home_addressLine2').val());
				$('#mailing_primary_city').val($('#home_primary_city').val());
				$('#mailing_primary_zip').val($('#home_primary_zip').val());
				$('#mailing_primary_state').val($('#home_primary_state').val());

				var home_primary_address = $('#home_primary_state').val();/*.split("#")[0];*/
				$('#mailing_primary_county option[value!=""]').remove();
				$('#mailing_primary_county').append('<option value="'+home_primary_address+'" selected="selected">'+home_primary_address+'</option>');
				
				//ssapMailingAddressValidationFlag = true;
				
			} else if (contactHomeAddress.checked == false) {
				$('.physicalmailingAddress').show();
				$("#mailing_addressLine1, #mailing_addressLine2, #mailing_primary_city, #mailing_primary_zip").val('');
				$('#mailing_primary_county option[value!=""]').remove();
				setStateValue();
			} 
		}
	}

	$(document).ready(function() {
		setKeyManagerForInput();

		/*var url = window.location.href;
		var lang = url.split("=")[1];*/

		/*HIX-46231*/
		var lang = 'en';

		loadBundles(lang);
		if ($("#mailingAddressIndicator").is(":checked")) {
			$('.physicalmailingAddress').hide();
		}
		getCountyList(' ');
		setStateValue();
		/*Added by Kunal*/
		$('.SSAPpgtitle').append('Before We Begin'); //need to put to properties file

		$('.dob').on('blur',function(){
			var strDate = $(this).val();
			if(!strDate){
				return;
			}
			var mon = parseInt(strDate.substring(0, 2), 10);
			var dt = parseInt(strDate.substring(3, 5), 10);
			var yr = parseInt(strDate.substring(6, 10), 10);

			var validDate =/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/;

			var Joindate = new Date(yr, mon-1, dt);
			var today = new Date();

			var today_mon = today.getMonth() + 1;
			var today_dt = today.getDate();
			var today_yr = today.getFullYear();


			if($(this).closest('div.controls').find('.dobCustomError').length == 0){
				$(this).closest('div.controls').append('<div class ="dobCustomError"></div>');
			};

			var dobCustomError = $(this).closest('div.controls').find('.dobCustomError');
			if(validDate.test(strDate)){
			if (yr > today_yr) {

				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
				dateValidation = false;
			} else if ((yr == today_yr) && mon > today_mon) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
				dateValidation = false;
			} else if ((yr == today_yr) && (mon == today_mon) && (dt > today_dt)) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
				dateValidation = false;
			} else if ((today_yr - yr) >= 104) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date, Age Is More Than 104.</span>');
				dateValidation = false;
			}else{
				dobCustomError.html('');
				dateValidation = true;
			}
			} else{
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter A Valid Date.</span>');
				dateValidation = false;

			}

		});

		$('.addressValidator').on('validateAddress',addressValidatorEvent);
	});

	function loadBundles(lang) {
		jQuery.i18n.properties({
			name : 'ssap',
			path : 'resources/js/ssap/',
			mode : 'both',
			language : lang,
			callback : function() {
				readPropertyFile();
			}
		});
	}

	function readPropertyFile() {

	}

	/*$(function() {
		$(".validateDedesignate")
				.click(
						function(e) {
							e.preventDefault();
							var href = $(this).attr('href');
							if (href.indexOf('#') != 0) {
								$(
										'<div id="modal" class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" class="modalClose close" aria-hidden="true">×</button></div><div class="modal-body"><iframe id="dedesignatePopup" src="'
												+ href
												+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:140px;"></iframe></div></div>')
										.modal({
											backdrop : "static",
											keyboard : "false"
										});
							}
						});
	});*/

	function closeLightbox() {
		$("#modal, .modal-backdrop").remove();
		window.location.reload();
	}

	function closeLightboxOnCancel() {
		$("#modal, .modal-backdrop").remove();
	}

	var registrationUrls = new Array("/securityquestions",
			"/account/phoneverification/sendcode", "/user/activation");
	var currDocUrl = document.URL;
	var urlIdx = 0;
	var noOfRegUrls = registrationUrls.length;

	while (urlIdx < noOfRegUrls) {
		if (currDocUrl.indexOf(registrationUrls[urlIdx]) > 0) {
			$('#menu').hide();
		}
		if (currDocUrl.indexOf('admin/broker/securityquestions') > 0) {
			$('#menu').show();
		}
		if (currDocUrl.indexOf('crm/consumer/securityquestions') > 0) {
			$('#menu').show();
		}
		urlIdx++;
	}

	$(document).ready(
			function() {
				$('#ssn').mask('999-99-9999');
				$('#date, .date-picker').each(function() {
					$(this).datepicker({
						autoclose : true,
						dateFormat : 'MM/DD/YYYY'
					}).on('changeDate', function(){
						$('.dob').trigger('blur');
					});
				});

				//Event date validation
				$('.event-date-picker').each(function() {
					$(this).datepicker({
						autoclose : true,
						dateFormat : 'MM/DD/YYYY'
					}).on('changeDate', function(){
						$('input[name=sepEventDate]').trigger('blur');
					});
				});

				/*Setting z-index for date-picker when open in modal dialog */
				$('.js-dateTrigger').on('click', function(){
					setTimeout(function(){
						$('.datepicker').css({zIndex: 1070});
					},80);
				});

				getDetailsFromJSONFile();

				// add "Any" option into plan level
				$("#planLevel").prepend("<option value=''>Any</option>").val('');
				var selectedPlanLevel = '';

				if (selectedPlanLevel == '') {
					$("#planLevel option[value='']").attr('selected', 'selected');
				} else {
					$("#planLevel option[value='" + selectedPlanLevel + "']").attr(
							'selected', 'selected');
				}
				populateCountriesOfIssuance();


				if($("#helpingEntity").val() == "NONE"){

					$("#authorizedRepresentativeHelpYes").prop("checked",false);
					$("#authorizedRepresentativeHelpNo").prop("checked",true);
				}else{
					$("#authorizedRepresentativeHelpYes").prop("checked",true);
					$("#authorizedRepresentativeHelpNo").prop("checked",false);
					displayAuthorizedRepresenatitive();
				}
			});

	// /// DATA READ FROM JSON FILE /////

	function getDetailsFromJSONFile() {
			applicationStatus = $("#ssapApplicationStatus").val();
			mode = $("#applicationMode").val();
			haveData = true;
			webJsonString = $("#ssapApplicationJson").val();
			webJson = $.parseJSON(webJsonString);
			var orgJson = $.parseJSON(webJsonString);
			applicationType = $('#applicationType').val();
			$("#ssapApplicationJson").val('');

			noOfHouseHold = getNoOfHouseHolds();
			var householdMemberObj = "";
			for ( var outerCnt = 1; outerCnt <= noOfHouseHold; outerCnt++) {
				for ( var innerCnt = 1; innerCnt <= noOfHouseHold; innerCnt++) {
					householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[innerCnt-1];
					if(householdMemberObj.personId == outerCnt){
						break;
					}
				}
				orgJson.singleStreamlinedApplication.taxHousehold[0].householdMember[outerCnt-1] = householdMemberObj;
			}
			webJson = orgJson;

			//var arrlastSavedPage = $("#ssapCurrentPageId").val();
			//arrlastSavedPage = arrlastSavedPage.split('#');

			var lastSavedPage = $("#ssapCurrentPageId").val();
			var CurrentApplicantID = 1;
			var helpType = $("#helpingEntity").val();
			var brokerCertificationNumber = $("#designatedBrokerLicenseNumber").val();
			var brokerName = $("#designatedBrokerName").val();
			var brokerFirstName =  $("#brokerFirstName").val();
			var brokerLastName =   $("#brokerLastName").val();
			var internalBrokerId =  parseInt($("#internalBrokerId").val());
			var internalAssisterId = parseInt($("#internalAssisterId").val());
			var assisterLicenseNumber = $("#designatedAssisterLicenseNumber").val();
			var assisterName = $("#designatedAssisterName").val();
			var assisterFirstName =  $("#assisterFirstName").val();
			var assisterLastName =   $("#assisterLastName").val();

			if(webJson == null || webJson == "") {
				$('#appscr51').show();
				webJson = getDefatulJson();

				webJson.singleStreamlinedApplication.helpType = helpType;
				var cmrHouseHoldInfo = $.parseJSON($("#cmrHouseHold").val());



				if(helpType == "BROKER"){
					webJson.singleStreamlinedApplication.broker.brokerName = brokerName;
					webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = brokerCertificationNumber;
					webJson.singleStreamlinedApplication.broker.brokerFirstName = brokerFirstName;
					webJson.singleStreamlinedApplication.broker.brokerLastName = brokerLastName;
					webJson.singleStreamlinedApplication.broker.internalBrokerId = internalBrokerId;
				}else if(helpType == "ASSISTER"){
					if(webJson.singleStreamlinedApplication.assister == undefined){
						var defaultJson = getDefatulJson();
						var assister = defaultJson.singleStreamlinedApplication.assister;
						webJson.singleStreamlinedApplication.assister = assister;
					}
					if(webJson.singleStreamlinedApplication.assister != undefined){
						webJson.singleStreamlinedApplication.assister.assisterName = assisterName;
						webJson.singleStreamlinedApplication.assister.assisterID = assisterLicenseNumber;
						webJson.singleStreamlinedApplication.assister.internalAssisterId = internalAssisterId;
						webJson.singleStreamlinedApplication.assister.assisterFirstName = assisterFirstName;
						webJson.singleStreamlinedApplication.assister.assisterLastName = assisterLastName;
					}

				}else{
					webJson.singleStreamlinedApplication.broker.brokerName = "";
					webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = "";
					webJson.singleStreamlinedApplication.broker.brokerFirstName = "";
					webJson.singleStreamlinedApplication.broker.brokerLastName = "";
					webJson.singleStreamlinedApplication.broker.internalBrokerId = 0;
					if(webJson.singleStreamlinedApplication.assister != undefined){
						webJson.singleStreamlinedApplication.assister.assisterName = "";
						webJson.singleStreamlinedApplication.assister.assisterID = null;
						webJson.singleStreamlinedApplication.assister.assisterFirstName = "";
						webJson.singleStreamlinedApplication.assister.assisterLastName = "";
						webJson.singleStreamlinedApplication.assister.internalAssisterId = 0;
					}
				}

				haveData = false;
				mode="";

				$('#firstName').val(cmrHouseHoldInfo.firstName);
				$('#middleName').val(cmrHouseHoldInfo.middleName);
				$('#lastName').val(cmrHouseHoldInfo.lastName);
				$('#emailAddress').val(cmrHouseHoldInfo.emailAddress);
				$('#dateOfBirth').val(UIDateformat(cmrHouseHoldInfo.dateOfBirth));
				$('#first_homePhoneNo').val(cmrHouseHoldInfo.phoneNumber);
				$('#appscr53Suffix').val(cmrHouseHoldInfo.nameSuffix).attr("selected", "selected");;
				//set default address
				$('#mailing_addressLine1').val($('#mailingAddressLine1').val());
				$('#mailing_addressLine2').val($('#mailingAddressLine2').val());
				$('#mailing_primary_city').val($('#mailingAddressCity').val());
				$('#mailing_primary_zip').val($('#mailingAddressZip').val());
				$('#mailing_primary_state').val($('#mailingAddressState').val());
				var county = $('#mailingAddressCountyCode').val();
				getCountyList($('#mailingAddressZip').val(), county, 'mailing_primary_zip');
				
				$('#emailAddress').val($('#userEmailAddress').val());
				$("#preffegred_spoken_language").val($("#spokenLang").val());
				$("#preffered_written_language").val($("#writtenLang").val());
				
				var contactMethod = $("#prefCommunication").val();
				if(contactMethod == "Mail"){
					$("#inTheEmail").attr("checked",true);
				}else if(contactMethod == "EmailAndMail"){
					$("#emailAndMail").attr("checked",true);
				}else if(contactMethod == "None"){
					$("#noEmailMail").attr("checked",true);
				}else{
					$("#email").attr("checked",true);
				}
				
				$('#acceptanceCB').prop("checked", false);
				return;
			}
			else {

				webJson.singleStreamlinedApplication.helpType = helpType;

				if(helpType == "BROKER"){
					webJson.singleStreamlinedApplication.broker.brokerName = brokerName;
					webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = brokerCertificationNumber;
					webJson.singleStreamlinedApplication.broker.brokerFirstName = brokerFirstName;
					webJson.singleStreamlinedApplication.broker.brokerLastName = brokerLastName;
					webJson.singleStreamlinedApplication.broker.internalBrokerId = internalBrokerId;
				}else if(helpType == "ASSISTER"){
					webJson.singleStreamlinedApplication.assister.assisterName = assisterName;
					webJson.singleStreamlinedApplication.assister.internalAssisterId = internalAssisterId;
					webJson.singleStreamlinedApplication.assister.assisterID = assisterLicenseNumber ;
					webJson.singleStreamlinedApplication.assister.assisterFirstName = assisterFirstName;
					webJson.singleStreamlinedApplication.assister.assisterLastName = assisterLastName;
				}else{
					webJson.singleStreamlinedApplication.broker.brokerName = "";
					webJson.singleStreamlinedApplication.broker.brokerFederalTaxIdNumber = "";
					webJson.singleStreamlinedApplication.broker.brokerFirstName = "";
					webJson.singleStreamlinedApplication.broker.brokerLastName = "";
					webJson.singleStreamlinedApplication.broker.internalBrokerId = 0 ;
					if(webJson.singleStreamlinedApplication.assister != undefined){

						webJson.singleStreamlinedApplication.assister.assisterName = "";
						webJson.singleStreamlinedApplication.assister.assisterID = null;
						webJson.singleStreamlinedApplication.assister.assisterFirstName = "";
						webJson.singleStreamlinedApplication.assister.assisterLastName = "";
						webJson.singleStreamlinedApplication.assister.internalAssisterId = 0;
					}


				}

				if (webJson.singleStreamlinedApplication.applicationSignatureDate !=""){
					//$('.SSAPpgtitle').append('View Your Application');
					f.appscr51 = 'Before We Begin';
					f.appscr52 = 'Get Ready to Start Your Application';
				}
				$('#acceptanceCB').prop("checked", true);
				if (typeof webJson.singleStreamlinedApplication.currentApplicantId !== 'undefined' && webJson.singleStreamlinedApplication.currentApplicantId !== null){
					 if(lastSavedPage == "appscr67"){
						webJson.singleStreamlinedApplication.currentApplicantId =  webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
					 }
					 CurrentApplicantID = webJson.singleStreamlinedApplication.currentApplicantId;
				}
				if(lastSavedPage != ""){
				if ("Y" != $("#csroverride").val()) {
					currentPage = lastSavedPage;
					currentPage = getPrevPage();
					$('#appscr51').hide();
					noOfHouseHoldmember=getNoOfHouseHolds();
					$('#numberOfHouseHold').text(noOfHouseHoldmember);
					$('#noOfPeople53').val(noOfHouseHoldmember);
					saveCurrentApplicantID(CurrentApplicantID);
					nonFinancialHouseHoldDone = CurrentApplicantID;
					saveCurrentApplicantID(CurrentApplicantID);
					next();
					}
				}else{
					$('#appscr51').show();
				}

				if ("Y"== $("#csroverride").val()) {
					$('#contBtn').show();
					$('#prevBtn').show();
					$('#saveBtn').show();

					var totalApplicants = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
					$('#appscr57HouseHoldPersonalInformation').html('');
					houseHoldNumberValue = 0;
					mode=""; // reset so that the data cna be saved

					$('#contBtn').click();
					$('#contBtn').click();

					return;

				}
				if (applicationStatus == "CC" || applicationStatus == "CL" || applicationStatus == "SG" || applicationStatus == "SU" || applicationStatus == "ER" || applicationStatus == "EN") {
					$( "#sidebar" ).children(".header, .nav-list").hide();
					$('#appscr51').hide();
					$('#' + currentPage).hide();
					$('.SSAPpgtitle').text("Household and Contact Information");
					$('#tab1').removeClass('active');
					currentPage = "appscr84";

					$('#progressDiv').hide();

					$('#appscr84').show();
					$('#contBtn').hide();
					$('#prevBtn').hide();
					$('#saveBtn').hide();

					//update26Indicator();
					addReviewSummaryJson();
					$('#appscr84addReviewSummary .applicationSummary').text("Application Summary");
					hideNonFinacial();

					submitForAutoContinue = true;

					return;
				}

				$('#contBtn').show();
				$('#prevBtn').show();
				$('#saveBtn').show();

				var totalApplicants = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
				$('#appscr57HouseHoldPersonalInformation').html('');
				houseHoldNumberValue = 0;
				mode="";

				if("appscr74" == lastSavedPage || "appscr75A" == lastSavedPage || "appscr76P1" == lastSavedPage
						|| "appscr77Part1" == lastSavedPage || "appscr76P2" == lastSavedPage || "appscr81B" == lastSavedPage  ) {

					$('#appscr53' ).hide();
					addReviewSummary();
					addIncarceratedContent();
					//caller();
					//setNonFinancialPageProperty(2);
					setNonFinancialPageProperty(1);
					currentPage = "appscr85";
					getMethod("appscr85");
					currentPage = "appscr85";
					$('#appscr53' ).hide();
					$('#appscr85').show();
					$('#contBtn').text('Submit');

				}

				else if("appscr67"==lastSavedPage){
					showHouseHoldContactSummary();
				}
		}
	}

	function setButtons() {
		$('#contBtn').text('Submit');
		next();
	}
	function caller() {
		$('#waiting').fadeIn();
		$.ajax({
			type : "GET",
			url : theUrlForisRidpVerified,
			dataType : 'json',
			success : function(response) {

				if(isInvalidCSRFToken(response))
					return;

				if(response.toString()=='true'){
					webJson.singleStreamlinedApplication.isRidpVerified = true;
					setNonFinancialPageProperty(1);
					addIncarceratedContent();
					//next();
				}else{
					//currentPage = 'appscr85';
					//saveDataSubmit();
					window.location.assign("http://"+window.location.host + "/hix/ridp/verification?caseNumber=" + webJson.singleStreamlinedApplication.applicationGuid);
				}
			},
			error : function(e) {
				alert("Failed to Check RIDP Response");
			}
		});
		$('#waiting').hide();
		$('#saveBtn').hide();
	}

	function getDefatulJson() {
		var jsonData = {
			"singleStreamlinedApplication" : {
				"ssapVersion":1.0,
				"ssapApplicationId" : "",
				"applicationDate" : "Mar 13, 2014 1:25:47 PM",
				"applicationStartDate" : getCurrentDateTime(),
				"applicationSignatureDate" : null,
				"authorizedRepresentativeIndicator" : null,
				"acceptPrivacyIndicator" : true,
				"isRidpVerified":false,
				"ApplicationType" : applicationType,
				"clientIp" : "",
				"IP" : null,
				"applicationGuid" : "",
				"currentApplicantId" : null,
				"applyingForFinancialAssistanceIndicator" : null,
				"mecCheckStatus" : "N",
				"getHelpIndicator": null,
				"helpType" : "",
				"applyingForhouseHold" : null,
				"consentAgreement" : null,
				"numberOfYearsAgreed" : "",
				"incarcerationAsAttestedIndicator":null,
				"authorizedRepresentative" : {
					"name" : {
						"firstName" : "",
						"middleName" : "",
						"lastName" : "",
						"suffix" : ""
					},
					"address" : {
						"streetAddress1" : "",
						"streetAddress2" : "",
						"city" : "",
						"state" : "",
						"postalCode" : ""
					},
					"phone" : [
					    {
							"phoneNumber" : "",
							"phoneExtension" : "",
							"phoneType":"cell"
						},
						{
							"phoneNumber" : "",
							"phoneExtension" : "",
							"phoneType":"home"
						},
						{
							"phoneNumber" : "",
							"phoneExtension" : "",
							"phoneType":"work"
						}
					],
					"emailAddress" : "",
					"partOfOrganizationIndicator" : null,
					"companyName" : "",
					"organizationId" : null,
					"signatureisProofIndicator" : null,
					"signature":""
				},
				"broker" : {
					"brokerName" : "",
					"brokerFirstName" : "",
					"brokerLastName" : "",
					"brokerFederalTaxIdNumber" : "",
					"internalBrokerId" : 0
				},
				"assister" : {
					"assisterName" : "",
					"assisterFirstName" : "",
					"assisterLastName" : "",
					"assisterID" : null,
					"internalAssisterId" : 0
				},
				"primaryTaxFilerPersonId" : 0,
				"taxFilerDependants" : [],
				"taxHousehold" : [ {
					"houseHoldIncome": 0,
					"householdMember" : [
							{
								"personId" : 1,
								"applicantGuid" : "",
								"name" : {
									"firstName" : "",
									"middleName" : "",
									"lastName" : "",
									"suffix" : ""
								},
								"dateOfBirth" : null,
								"householdContactIndicator" : true,
								"applyingForCoverageIndicator" : false,
								"under26Indicator" : true,
								"householdContact" : {
									"homeAddressIndicator" : false,
									"homeAddress" : {
										"streetAddress1" : "",
										"streetAddress2" : "",
										"city" : "",
										"state" : "",
										"postalCode" : "",
										"county" : "",
										"primaryAddressCountyFipsCode" : ""
									},
									"mailingAddressSameAsHomeAddressIndicator" : true,
									"mailingAddress" : {
										"streetAddress1" : "",
										"streetAddress2" : "",
										"city" : "",
										"state" : "",
										"postalCode" : "",
										"county" : ""
									},
									"phone" : {
										"phoneNumber" : "",
										"phoneExtension" : "",
										"phoneType" : "CELL"
									},
									"otherPhone" : {
										"phoneNumber" : "",
										"phoneExtension" : "",
										"phoneType" : "HOME"
									},
									"contactPreferences" : {
										"preferredSpokenLanguage" : "",
										"preferredWrittenLanguage" : "",
										"emailAddress" : "",
										"preferredContactMethod":""
									},
									"stateResidencyProof" : ""
								},
								"livesWithHouseholdContactIndicator" : true,
								"taxFiler" : {
									"liveWithSpouseIndicator" : null,
									"spouseHouseholdMemberId" : 0,
									"claimingDependantsOnFTRIndicator" : null,
									"taxFilerDependants": [
							                                {
							                                    "dependantHouseholdMemeberId": 0
							                                }
							                            ]
								},
								"taxFilerDependant" : {
									"taxFilerDependantIndicator" : null
								},
								"planToFileFTRIndicator" : null,
								"planToFileFTRJontlyIndicator" : null,
								"marriedIndicator" : null,
								"gender" : "",
								"tobaccoUserIndicator" : false,
								"livesAtOtherAddressIndicator" : null,
								"otherAddress" : {
									"address" : {
										"streetAddress1" : "",
										"streetAddress2" : "",
										"city" : "",
										"state" : "",
										"postalCode" : "",
										"county" : ""
									},
									"livingOutsideofStateTemporarilyIndicator" : false,
									"livingOutsideofStateTemporarilyAddress" :{
										"streetAddress1" : "",
										"streetAddress2" : "",
										"city" : "",
										"state" : "",
										"postalCode" : "",
										"county" : ""
									}
								},
								"specialEnrollmentPeriod" : {
									"birthOrAdoptionEventIndicator" : false
								},
								"socialSecurityCard" : {
									"socialSecurityCardHolderIndicator" : null,
									"socialSecurityNumber" : "",
									"ssnManualVerificationIndicator" : true,
									"nameSameOnSSNCardIndicator" : null,
									"firstNameOnSSNCard" : "",
									"middleNameOnSSNCard" : "",
									"lastNameOnSSNCard" : "",
									"suffixOnSSNCard": "",
									"deathIndicatorAsAttested" : false,
									"deathIndicator" : false,
									"deathManualVerificationIndicator" : false,
									"useSelfAttestedDeathIndicator" : true,
									"individualMandateExceptionIndicator": null,
		                            "individualManadateExceptionID": null
								},
								"specialCircumstances" : {
									"americanIndianAlaskaNativeIndicator" : false,
									"tribalManualVerificationIndicator" : true,
									"pregnantIndicator" : false,
									"numberBabiesExpectedInPregnancy" : 0,
									"fosterChild" : false,
									"everInFosterCareIndicator" : false,
									"fosterCareState": "",
		                            "ageWhenLeftFosterCare": 0,
		                            "gettingHealthCareThroughStateMedicaidIndicator": true
								},
								"americanIndianAlaskaNative" : {
									"memberOfFederallyRecognizedTribeIndicator" : false,
									"state": "",
		                            "tribeName": "",
		                            "tribeFullName":"",
		                            "stateFullName":""
								},

								"citizenshipImmigrationStatus" : {
									"citizenshipAsAttestedIndicator" : true,
									"citizenshipStatusIndicator" : null,
									"citizenshipManualVerificationStatus" : null,
									"naturalizedCitizenshipIndicator" : null,
									"naturalizationCertificateIndicator" : null,
									"naturalizationCertificateAlienNumber" : "",
									"naturalizationCertificateNaturalizationNumber" : "",
									"eligibleImmigrationStatusIndicator" : null,
									"livedIntheUSSince1996Indicator" : null,
									"lawfulPresenceIndicator" : true,
									"honorablyDischargedOrActiveDutyMilitaryMemberIndicator" : null,
									"eligibleImmigrationDocumentSelected":"",
									"eligibleImmigrationDocumentType" : [ {
										"I551Indicator" : false,
										"TemporaryI551StampIndicator" : false,
										"MachineReadableVisaIndicator" : false,
										"I766Indicator" : false,
										"I94Indicator" : false,
										"I94InPassportIndicator" : false,
										"I797Indicator" : false,
										"UnexpiredForeignPassportIndicator" : false,
										"I327Indicator" : false,
										"I571Indicator" : false,
										"I20Indicator" : false,
										"DS2019Indicator" : false,
										"OtherDocumentTypeIndicator" : false,
									} ],
									"otherImmigrationDocumentType" : {
										"ORRCertificationIndicator" : false,
										"ORREligibilityLetterIndicator" : false,
										"CubanHaitianEntrantIndicator" : false,
										"WithholdingOfRemovalIndicator" : false,
										"AmericanSamoanIndicator" : false,
										"StayOfRemovalIndicator" : false
									},
									"citizenshipDocument" : {
										"alienNumber" : "",
										"i94Number" : "",
										"cardNumber" : "",
										"documentExpirationDate" : null,
										"visaNumber" : "",
										"SEVISId" : "",
										"foreignPassportOrDocumentNumber" : "",
										"foreignPassportCountryOfIssuance" : "0",
										"sevisId" : "",
										"documentDescription" : "",
										"nameSameOnDocumentIndicator" : null,
										"nameOnDocument" : {
											"firstName" : "",
											"middleName" : "",
											"lastName" : "",
											"suffix" : ""
										}
									},
								},
								"parentCaretakerRelatives": {
		                            "mainCaretakerOfChildIndicator": null,
		                            "personId": [],
		                            "anotherChildIndicator": false,
		                            "name": {
		                                "firstName": "",
		                                "middleName": "",
		                                "lastName": "",
		                                "suffix": ""
		                            },
		                            "dateOfBirth": null,
		                            "relationship": "",
		                            "liveWithAdoptiveParentsIndicator": true
		                        },
								"ethnicityAndRace" : {
									"hispanicLatinoSpanishOriginIndicator" : null,
									"ethnicity" : [],
									"race" : []
								},
								"expeditedIncome" : {
									"irsReportedAnnualIncome" : null,
									"irsIncomeManualVerificationIndicator" : false,
									"expectedAnnualIncome" : null,
									"expectedAnnualIncomeUnknownIndicator" : null,
									"expectedIncomeVaration" : null
								},
								"healthCoverage" : {
									"haveBeenUninsuredInLast6MonthsIndicator" : null,
									"isOfferedHealthCoverageThroughJobIndicator" : null,
									"employerWillOfferInsuranceIndicator" : null,
									"employerWillOfferInsuranceCoverageStartDate": null,
									"currentEmployer" : [ {
										"employer" : {
											"phone" : {
												"phoneNumber" : "",
												"phoneExtension" : "",
												"phoneType" : "HOME"
											},
											"employerContactPersonName" : "",
											"employerContactEmailAddress" : ""
										},
										"currentEmployerInsurance" : {
											"isCurrentlyEnrolledInEmployerPlanIndicator" : false,
											"willBeEnrolledInEmployerPlanIndicator" : false,
											"willBeEnrolledInEmployerPlanDate": null,
											"expectedChangesToEmployerCoverageIndicator": true,
	                                        "employerWillNotOfferCoverageIndicator": false,
	                                        "employerCoverageEndingDate": null,
	                                        "memberPlansToDropEmployerPlanIndicator": true,
	                                        "memberEmployerPlanEndingDate": null,
	                                        "employerWillOfferPlanIndicator": true,
	                                        "employerPlanStartDate": null,
	                                        "memberPlansToEnrollInEmployerPlanIndicator": true,
	                                        "memberEmployerPlanStartDate": null,
											"currentLowestCostSelfOnlyPlanName" : "",
											"currentPlanMeetsMinimumStandardIndicator" : false,
											"comingLowestCostSelfOnlyPlanName" : "",
											"comingPlanMeetsMinimumStandardIndicator" : false
										}
									} ],
									"currentlyEnrolledInCobraIndicator" : null,
									"currentlyEnrolledInRetireePlanIndicator" : null,
									"currentlyEnrolledInVeteransProgramIndicator" : null,
									"currentlyEnrolledInNoneIndicator" : null,
									"willBeEnrolledInCobraIndicator" : null,
									"willBeEnrolledInRetireePlanIndicator" : null,
									"willBeEnrolledInVeteransProgramIndicator" : null,
									"willBeEnrolledInNoneIndicator" : null,
									"hasPrimaryCarePhysicianIndicator" : false,
									"primaryCarePhysicianName" : "",
									"formerEmployer" : [ {
										"employerName" : "",
										"address" : {
											"streetAddress1" : "",
											"streetAddress2" : "",
											"city" : "",
											"state" : "",
											"postalCode": ""
										},
										"phone" : {
											"phoneNumber" : "",
											"phoneExtension" : "",
											"phoneType" : "CELL"
										},
										"employerIdentificationNumber" : "",
										"employerContactPersonName" : "",
										"employerContactPhone" : {
											"phoneNumber" : "",
											"phoneExtension" : "",
											"phoneType" : "HOME"
										},
										"employerContactEmailAddress" : ""
									} ],
									"currentOtherInsurance" : {
										"currentOtherInsuranceSelected":"",
										"otherStateOrFederalProgramType" : "",
										"otherStateOrFederalProgramName" : "",
									},
									"currentlyHasHealthInsuranceIndicator" : false,
									"otherInsuranceIndicator" : null,
									"medicaidInsurance" : {
										"requestHelpPayingMedicalBillsLast3MonthsIndicator" : false
									},
									"chpInsurance" : {
										"insuranceEndedDuringWaitingPeriodIndicator" : false,
										"reasonInsuranceEndedOther" : ""
									}
								},
								"incarcerationStatus" : {
									"incarcerationAsAttestedIndicator" : false,
									"incarcerationStatusIndicator" : false,
									"incarcerationPendingDispositionIndicator" : false,
									"incarcerationManualVerificationIndicator" : false,
									"useSelfAttestedIncarceration" : false
								},
								"detailedIncome" : {
									"jobIncomeIndicator":null,
									"jobIncome" : [ {
										"employerName" : "",
										"incomeAmountBeforeTaxes" : null,
										"incomeFrequency" : "",
										"workHours" : null
									} ],
									"otherIncomeIndicator":null,
									"otherIncome" : [{
										"otherIncomeTypeDescription" : "Canceled debts",
										"incomeAmount" : null,
										"incomeFrequency" : ""
									},
									{
										"otherIncomeTypeDescription" : "Court Awards",
										"incomeAmount" : null,
										"incomeFrequency" : ""
									},
									{
										"otherIncomeTypeDescription" : "Jury duty pay",
										"incomeAmount" : null,
										"incomeFrequency" : ""
									}],
									"capitalGainsIndicator":null,
									"capitalGains" : {
										"annualNetCapitalGains" : null
									},
									"rentalRoyaltyIncomeIndicator":null,
									"rentalRoyaltyIncome" : {
										"netIncomeAmount" : null,
										"incomeFrequency" : ""
									},
									"farmFishingIncomeIndictor":null,
									"farmFishingIncome" : {
										"netIncomeAmount" : null,
										"incomeFrequency" : ""
									},
									"alimonyReceivedIndicator":null,
									"alimonyReceived" : {
										"amountReceived" : null,
										"incomeFrequency" : ""
									},
									"selfEmploymentIncomeIndicator":null,
									"selfEmploymentIncome" : {
										"typeOfWork" : "",
										"monthlyNetIncome" : null
									},
									"deductions" : {
										"deductionType" : [ "ALIMONY",
												"STUDENT_LOAN_INTEREST", "OTHER" ],
										"deductionAmount" : null,
										"deductionFrequency" : "MONTHLY",
										"alimonyDeductionAmount" : null,
										"alimonyDeductionFrequency" : "",
										"studentLoanDeductionAmount" : null,
										"studentLoanDeductionFrequency" : "",
										"otherDeductionAmount" : null,
										"otherDeductionFrequency" : ""
									},
									"unemploymentBenefitIndicator":null,
									"unemploymentBenefit" : {
										"stateGovernmentName" : "",
										"benefitAmount" : null,
										"incomeFrequency" : "",
										"unemploymentDate" : null
									},
									"retirementOrPensionIndicator":null,
									"retirementOrPension" : {
										"taxableAmount" : null,
										"incomeFrequency" : ""
									},
									"socialSecurityBenefitIndicator":null,
									"socialSecurityBenefit" : {
										"benefitAmount" : null,
										"incomeFrequency" : ""
									},
									"investmentIncomeIndicator":null,
									"investmentIncome" : {
										"incomeAmount" : null,
										"incomeFrequency" : ""
									},
									"discrepancies" : {
										"hasChangedJobsIndicator" : false,
										"stopWorkingAtEmployerIndicator" : null,
										"didPersonEverWorkForEmployerIndicator" : null,
										"hasHoursDecreasedWithEmployerIndicator" : null,
										"hasWageOrSalaryBeenCutIndicator" : null,
										"explanationForJobIncomeDiscrepancy" : "",
										"seasonalWorkerIndicator" : null,
										"explanationForDependantDiscrepancy" : "",
										"otherAboveIncomeIncludingJointIncomeIndicator" : null
									},
									"wageIncomeProof" : "",
									"scholarshipIncomeProof" : "",
									"dividendsIncomeProof" : "",
									"taxableInterestIncomeProof" : "",
									"annuityIncomeProof" : "",
									"pensionIncomeProof" : "",
									"royaltiesIncomeProof" : "",
									"unemploymentCompensationIncomeProof" : "",
									"foreignEarnedIncomeProof" : "",
									"rentalRealEstateIncomeProof" : "",
									"sellingBusinessPropertyIncomeProof" : "",
									"farmIncomeProof" : "",
									"partnershipAndSwarpIncomeIncomeProof" : "",
									"businessIncomeProof" : "",
									"childNaTribe" : "",
									"taxExemptedIncomeProof" : "",
									"socialSecurityBenefitProof" : "",
									"selfEmploymentTaxProof" : "",
									"studentLoanInterestProof" : "",
									"receiveMAthrough1619" : false,
									"receiveMAthroughSSI" : false,
									"foreignEarnedIncome" : {
										"incomeAmount" : null
									},
									"rentalRealEstateIncome" : {
										"incomeAmount" : null
									},
									"sellingBusinessProperty" : {
										"incomeAmount" : null
									},
									"farmIncom" : {
										"incomeAmount" : null
									},
									"partnershipsCorporationsIncome" : {
										"incomeAmount" : null
									},
									"businessIncome" : {
										"incomeAmount" : null
									},
									"selfEmploymentTax" : {
										"incomeAmount" : null
									},
									"studentLoanInterest" : {
										"incomeAmount" : null
									}
								},
								"bloodRelationship" : [{
										"individualPersonId" : null,
										"relatedPersonId" : null,
										"relation" : "",
										"textDependency" : null
										}],
								"migrantFarmWorker" : true,
								"birthCertificateType" : "NO_CRETIFICATE",
								"infantIndicator" : false,
								"PIDIndicator" : false,
								"PIDVerificationIndicator" : false,
								"livingArrangementIndicator" : false,
								"IPVIndicator" : false,
								"strikerIndicator" : false,
								"drugFellowIndicator" : false,
								"SSIIndicator" : false,
								"residencyVerificationIndicator" : false,
								"disabilityIndicator" : false,
								"shelterAndUtilityExpense" : null,
								"dependentCareExpense" : null,
								"childSupportExpense" : null,
								"medicalExpense" : null,
								"heatingCoolingindicator" : false


							},

					],
					"applyingForCashBenefitsIndicator" : false
				} ]
			}
		};
		return jsonData;

	}


	function callLastPage() {

	}

	function testRidp() {
		location.href = "ssap/ssapverification";
	}

	/*
	 * Populate Counties Based on the State i.e the exchange Type
	 */

	function getCountyList(stateCode) {

		$.ajax({
			type : "POST",
			url : theUrlForAddCounties,
			data : {
				stateCode : stateCode
			},
			dataType : 'json',
			success : function(response) {

				// populateCounties(response,''); FIX ME
			},
			error : function(e) {
				alert("Failed to Populate County Details");
			}
		});
	}

	function populateCounties(response, county) {
		// console.log(response, eIndex,county,"populate counties")
		$('#mailing_primary_county').html('');
		$('#home_primary_county').html('');

		var optionsstring = '<option value="">County</option>';
		var i = 0;
		$.each(response, function(key, value) {
			var selected = (county == key) ? 'selected' : '';
			var options = '<option value="' + key + '" ' + selected + '>' + key
					+ '</option>';
			optionsstring = optionsstring + options;
			i++;
		});

		$('#mailing_primary_county').html(optionsstring);
		$('#home_primary_county').html(optionsstring);
	}

	/*
	 * Populating Counties Ends here
	 *
	 */

	function guidGenerator() {
		var IEBrowser = isIEBrowser();
		if(IEBrowser && IEBrowser < 11){
			var buf = new Array(8);
			for(var i=0; i < buf.length; i++){
				buf[i] = Math.floor((Math.random() * 9999) + 1000).toString();
			}

			return (buf[0] + buf[1] + "-" + buf[2] + "-4"
					+ buf[3].substring(1) + "-y" + buf[4].substring(1) + "-"
					+ buf[5] + buf[6] + buf[7]);
		}

		var buf = new Uint16Array(8);
		window.crypto.getRandomValues(buf);
		var S4 = function(num) {
			var ret = num.toString(16);
			while (ret.length < 4) {
				ret = "0" + ret;
			}
			;
			return ret;
		};
		return (S4(buf[0]) + S4(buf[1]) + "-" + S4(buf[2]) + "-4"
				+ S4(buf[3]).substring(1) + "-y" + S4(buf[4]).substring(1) + "-"
				+ S4(buf[5]) + S4(buf[6]) + S4(buf[7]));
	}

	function isIEBrowser() { //return false or browser version
	  var BrowserDetail = navigator.userAgent.toLowerCase();
	  return (BrowserDetail.indexOf('msie') != -1) ? parseInt(BrowserDetail.split('msie')[1]) : false;
	}


	function setStateValue(){
		var exchangeName = $('#exchangeName').val();

		if(exchangeName === "NMHIX"){
			$('#home_primary_state option[value="NM"], #mailing_primary_state option[value="NM"]').attr("selected","selected");
		}else if(exchangeName === "One, Mississippi"){
			$('#home_primary_state option[value="MS"], #mailing_primary_state option[value="MS"]').attr("selected","selected");
		}else if(exchangeName === "Your Health Idaho"){
			$('#home_primary_state option[value="ID"], #mailing_primary_state option[value="ID"]').attr("selected","selected");
		}
	}

	function startFinancialSectionPages() {



		if (currentPage == "appscr68") {
			if(currentPersonIndex == -1) {
				currentPersonIndex ++;
			}
			var pageId = getNextPage();

			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);

			$('#' + currentPage).hide();
			$('#' + pageId).show();
			currentPage = pageId;

			$('#prevBtn').show();
		}

		else if (currentPage == "appscr69") {
			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = getNextPage();
			$('#' + currentPage).hide();
			$('#' + pageId).show();

			currentPage = pageId;

			appscr69FromMongodbJSON(currentPersonIndex + 1);
			selectIncomesCheckBoxes();
			appscr69FromMongodbJSON(currentPersonIndex + 1);
			$('#prevBtn').show();
		}

		else if (currentPage == "appscr70") {
			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = getNextPage();
			$('#' + currentPage).hide();
			$('#' + pageId).show();

			currentPage = pageId;

			var otherIncomeIndicator = personObject.detailedIncome.otherIncomeIndicator;
			if(otherIncomeIndicator != null && otherIncomeIndicator == true) {
				$('#Financial_OtherIncome').prop("checked", true);
			}
			else {
				$('#Financial_OtherIncome').prop("checked", false);
			}

			var financialAlimonyReceivedIndicator = personObject.detailedIncome.alimonyReceivedIndicator;
			if(financialAlimonyReceivedIndicator != null && financialAlimonyReceivedIndicator == true) {
				$('#Financial_AlimonyReceived').prop("checked", true);
			}
			else {
				$('#Financial_AlimonyReceived').prop("checked", false);
			}

			var farmFishingIncomeIndictor = personObject.detailedIncome.farmFishingIncomeIndictor;
			if(farmFishingIncomeIndictor != null && farmFishingIncomeIndictor == true) {
				$('#Financial_FarmingOrFishingIncome').prop("checked", true);
			}
			else {
				$('#Financial_FarmingOrFishingIncome').prop("checked", false);
			}

			var rentalRoyaltyIncomeIndicator = personObject.detailedIncome.rentalRoyaltyIncomeIndicator;
			if(rentalRoyaltyIncomeIndicator != null && rentalRoyaltyIncomeIndicator == true) {
				$('#Financial_RentalOrRoyaltyIncome').prop("checked", true);
			}
			else {
				$('#Financial_RentalOrRoyaltyIncome').prop("checked", false);
			}

			var investmentIncomeIndicator = personObject.detailedIncome.investmentIncomeIndicator;
			if(investmentIncomeIndicator != null && investmentIncomeIndicator == true) {
				$('#Financial_InvestmentIncome').prop("checked", true);
			}
			else {
				$('#Financial_InvestmentIncome').prop("checked", false);
			}

			var capitalGainsIndicator = personObject.detailedIncome.capitalGainsIndicator;
			if(capitalGainsIndicator != null && capitalGainsIndicator == true) {
				$('#Financial_Capitalgains').prop("checked", true);
			}
			else {
				$('#Financial_Capitalgains').prop("checked", false);
			}

			var retirementOrPensionIndicator = personObject.detailedIncome.retirementOrPensionIndicator;
			if(retirementOrPensionIndicator != null && retirementOrPensionIndicator == true) {
				$('#Financial_Retirement_pension').prop("checked", true);
			}
			else {
				$('#Financial_Retirement_pension').prop("checked", false);
			}

			var unemploymentBenefitIndicator = personObject.detailedIncome.unemploymentBenefitIndicator;
			if(unemploymentBenefitIndicator != null && unemploymentBenefitIndicator == true) {
				$('#Financial_Unemployment').prop("checked", true);
			}
			else {
				$('#Financial_Unemployment').prop("checked", false);
			}

			var socialSecurityBenefitIndicator = personObject.detailedIncome.socialSecurityBenefitIndicator;
			if(socialSecurityBenefitIndicator != null && socialSecurityBenefitIndicator == true) {
				$('#Financial_SocialSecuritybenefits').prop("checked", true);
			}
			else {
				$('#Financial_SocialSecuritybenefits').prop("checked", false);
			}

			var selfEmploymentIncomeIndicator = personObject.detailedIncome.selfEmploymentIncomeIndicator;
			if(selfEmploymentIncomeIndicator != null && selfEmploymentIncomeIndicator == true) {
				$('#Financial_Self_Employment').prop("checked", true);
			}
			else {
				$('#Financial_Self_Employment').prop("checked", false);
			}


			var selfEmploymentIncomeIndicator = personObject.detailedIncome.selfEmploymentIncomeIndicator;
			if(selfEmploymentIncomeIndicator != null && selfEmploymentIncomeIndicator == true) {
				$('#Financial_Job').prop("checked", true);
			}
			else {
				$('#Financial_Job').prop("checked", false);
			}

			if ($('#Financial_Job').prop("checked")== true) {
				$('#Financial_Job_Div').show();
				$('#Financial_Job_EmployerName_id').val(personObject.detailedIncome.jobIncome[0].employerName);
				$('#Financial_Job_id').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes)));
				$('#Financial_Job_select_id').val(personObject.detailedIncome.jobIncome[0].incomeFrequency);
				$('#Financial_Job_HoursOrDays_id').val(personObject.detailedIncome.jobIncome[0].workHours);

				showDiductionsFields();
			}
			else {
				$('#Financial_Job_Div').hide();
				$('#Financial_Job_EmployerName_id').val("");
				$('#Financial_Job_HoursOrDays_id').val("");
				$('#Financial_Job_id').val("");
				$('#Financial_Job_select_id').val("");

			}


			if ($('#Financial_Self_Employment').prop("checked")== true) {
				$('#Financial_Self_Employment_Div').show();
				$('#Financial_Self_Employment_TypeOfWork').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.selfEmploymentIncome)));

				//showDiductionsFields();
			}
			else {
				$('#Financial_Self_Employment_Div').hide();
				$('#Financial_Self_Employment_TypeOfWork').val("");

			}
		}

		else if (currentPage == "appscr71") {
			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = getNextPage();
			$('#' + currentPage).hide();
			$('#' + pageId).show();

			currentPage = pageId;

			personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes = $('#Financial_Job_id').val();
			personObject.detailedIncome.jobIncome[0].incomeFrequency = $('#Financial_Job_select_id').val();

			continueData71Json(currentPersonIndex+1)

			totalIncomeAt71(currentPersonIndex+1);

			var alimonyIncome = personObject.detailedIncome.deductions.alimonyDeductionAmount;
			if(alimonyIncome != null && alimonyIncome != "") {
				$('#appscr72alimonyPaid').prop("checked", true);
				appscr72CheckBoxAction($('#appscr72alimonyPaid'));
				$('#alimonyAmountInput').val(alimonyIncome);
				$('#alimonyFrequencySelect').val(householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency);

			}
			else {
				$('#appscr72alimonyPaid').prop("checked", false);
				appscr72CheckBoxAction($('#appscr72alimonyPaid'));
				$('#alimonyAmountInput').val(null);
				$('#alimonyFrequencySelect').val("SelectFrequency");
			}


			var studentLoanDeductionAmount = personObject.detailedIncome.deductions.studentLoanDeductionAmount;
			if(studentLoanDeductionAmount != null && studentLoanDeductionAmount != "") {
				$('#appscr72studentLoanInterest').prop("checked", true);
				appscr72CheckBoxAction($('#appscr72studentLoanInterest'));
				$('#studentLoanInterestAmountInput').val(studentLoanDeductionAmount);
				$('#studentLoanInterestFrequencySelect').val(householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency);
			}
			else {
				$('#appscr72studentLoanInterest').prop("checked", false);
				appscr72CheckBoxAction($('#appscr72alimonyPaid'));
				$('#studentLoanInterestAmountInput').val(null);
				$('#studentLoanInterestFrequencySelect').val("SelectFrequency");
			}

			var otherDeductionAmount = personObject.detailedIncome.deductions.otherDeductionAmoun;
			if(otherDeductionAmount != null && otherDeductionAmount != "") {
				$('#appscr72otherDeduction').prop("checked", true);
				appscr72CheckBoxAction($('#appscr72otherDeduction'));
				$('#deductionAmountInput').val(otherDeductionAmount);
				$('#deductionFrequencySelect').val(householdMemberObj.detailedIncome.deductions.otherDeductionFrequency);

				$('#deductionTypeInput').val("");
			}
			else {
				$('#appscr72otherDeduction').prop("checked", false);
				appscr72CheckBoxAction($('#appscr72otherDeduction'));
				$('#deductionAmountInput').val(null);
				$('#deductionFrequencySelect').val("SelectFrequency");
				$('#deductionTypeInput').val("");
			}
		}

		else if (currentPage == "appscr72") {
			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = getNextPage();
			$('#' + currentPage).hide();
			$('#' + pageId).show();

			currentPage = pageId;
		}

		else if (currentPage == "appscr73") {
			var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

			if(editData == true) { // code repeated. TBD
				$('#' + currentPage).hide();
				editData = false;
				$('#prevBtn').show();
				currentPage = "appscr74";
				$('#' + currentPage).show();
				showFinancialDetailsSummaryJSON();
			}

			else if(currentPersonIndex < householdSize -1  ) {
				currentPersonIndex ++;
				$('#' + currentPage).hide();
				currentPage = "appscr69";

				var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
				$('.nameOfHouseHold').text(lastName);

				$('#' + currentPage).show();

				var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
				$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);
			}


			else {
				$('#' + currentPage).hide();
				currentPage = "appscr74";
				$('#' + currentPage).show();
				showFinancialDetailsSummaryJSON();
			}

			editData = false; // resert this to show the back button
			$('#prevBtn').show();

		}


	}


	/********************/

	function reverseFinancialSectionPages() {



		if(currentPage == "appscr74") {

			var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
			currentPersonIndex = householdSize -1;

			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var pageId = getPrevPage();
			$('#' + currentPage).hide();
			currentPage = pageId;
			var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
			$('.nameOfHouseHold').text(lastName);
			$('#' + currentPage).show();
		}
		else if(currentPage == "appscr73") {
			var pageId = getPrevPage();
			$('#' + currentPage).hide();
			currentPage = pageId;
			$('#' + currentPage).show();
			totalIncomeAt71(currentPersonIndex+1);
		}

		else if(currentPage == "appscr72") {
			var pageId = getPrevPage();
			$('#' + currentPage).hide();
			currentPage = pageId;
			$('#' + currentPage).show();

			var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
			var employerName = personObject.detailedIncome.jobIncome[0].employerName;
			if(employerName != null && employerName != null) {
				$('#Financial_Job').prop("checked", true);
				$('#Financial_Job_Div').show();

				$('#Financial_Job_EmployerName_id').val(personObject.detailedIncome.jobIncome[0].employerName);
				$('#Financial_Job_id').val(getDecimalNumber(cleanMoneyFormat(personObject.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes)));
				$('#Financial_Job_select_id').val(personObject.detailedIncome.jobIncome[0].incomeFrequency);
				$('#Financial_Job_HoursOrDays_id').val(personObject.detailedIncome.jobIncome[0].workHours);
			}

			else {
				$('#Financial_Job').prop("checked", false);
				$('#Financial_Job_Div').show();
			}

		}

		else if(currentPage == "appscr71") {
			var pageId = getPrevPage();
			$('#' + currentPage).hide();
			currentPage = pageId;
			$('#' + currentPage).show();
		}

		else if(currentPage == "appscr70") {
			var pageId = getPrevPage();
			$('#' + currentPage).hide();
			currentPage = pageId;
			$('#' + currentPage).show();

			if(editData ==  true) {
				$('#prevBtn').hide();
			}
			else {
				$('#prevBtn').show();
			}
		}

		else if(currentPage == "appscr69") {
			var householdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

			if(householdSize == 1 || currentPersonIndex == 0) {
				$('#' + currentPage).hide();
				currentPage = "appscr68";
				$('#' + currentPage).show();
			}

			else if(currentPersonIndex != 0) {
				currentPersonIndex --;

				var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex];
				var pageId = "appscr73";
				$('#' + currentPage).hide();
				currentPage = pageId;
				var lastName = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentPersonIndex].name.lastName;
				$('.nameOfHouseHold').text(lastName);
				$('#' + currentPage).show();

			}

		}

	}



	function checkFinacialAssistance(){
		if($("#wantToGetHelpPayingHealthInsurance_id1").is(":checked") || $('#page55radio9').is(":checked")){
			webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = true;
				ssapApplicationType = "FINANCIAL";
				$(".non_financial_hide").css("display","");
				$(".financial_hide").css("display","none");
			}else{
				webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = false;
				ssapApplicationType = "NON_FINANCIAL";
				$(".non_financial_hide").css("display","none");
				$(".financial_hide").css("display","");

			}
	}

	$(document).ready(function(){
		hideNonFinacial();

		$("#sidebar ul").css("visibility","visible");

		$("input, select").attr("data-parsley-trigger","change");
		//removeRequiredValidation();
		$('.currency').mask('000,000,000,000.00', {reverse: true});
		$('.phoneNumber').mask('(000) 000-0000');
		$('.phoneNumberExt').mask('0000');

		updateCoverageYear();

		$('.removeComma').blur(function() {
			 $(this).val($(this).val().replace(/,/g,""));
		});

		$('.ssapTooltip').tooltip();
		/*$('#progressDiv').waypoint('sticky');*/
		calculateProgressBarHeight();
		if(applicationStatus == 'OP'){
			updateProgressBar();
		}
	});


	function removeRequiredValidation(){
		if(haveData == true || true){
			$('input, select').each(function(){
				$(this).removeAttr('required');
				$(this).removeAttr('parsley-required');
			});

			$('input[type=checkbox]').each(function(){
				$(this).removeAttr('parsley-mincheck');
			});

			$('input[name=householdContactAddress]').removeAttr('parsley-mincheck');
		}
	}

	function getNameByPersonId(personId){
		if(personId == null || personId == "") {
			return "";
		}
		var houseHoldname = "";
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[personId-1];
		if(householdMemberObj != undefined){
			houseHoldname = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
			if(householdMemberObj.name.suffix != "" && currentPage != "appscr54" && currentPage != "appscr57"){
				houseHoldname += " "+getFormattedname(householdMemberObj.name.suffix);
			}
			
		}
		return houseHoldname;
	}

	function getCurrentDateTime(){
		return moment().format('MMMM D, YYYY h:mm:ss A');

	}

	function hideNonFinacial(){
		ssapApplicationType = $('#ssapApplicationType').val();
		if(ssapApplicationType == "NON_FINANCIAL" || webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator === false){

			//check no need for financial assistance
			$('#wantToGetHelpPayingHealthInsurance_id2').prop("checked", true);
			$('#page55radio10').prop("checked", true);

			$(".non_financial_hide").css("display","none");

			if($("#exchangeName ").val() != "NMHIX"){
				//hide in id only
				$(".non_financial_hide_id").css("display","none");
				/*$('#progressDiv').css('top','78px');*/
			}

		}else{
			$(".financial_hide").css("display","none");
		}

		//only hide for nm
		if($("#exchangeName ").val() == "NMHIX"){
			$(".hide_nm").css("display","none");
		}else{
			$(".hide_nm").css("display","block");
		}
	}

	function getNoOfHouseHolds(){
		if(webJson == null || webJson == undefined || webJson == 'undefined'){
			return 0;
		}
		var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		return noOfHouseHoldmember;
	}

	function updatePrimaryContactName(){

		var primaryContactObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
		$('.primaryContactName').text(primaryContactObject.name.firstName+" "+primaryContactObject.name.middleName+" "+primaryContactObject.name.lastName);
	}

	window.ParsleyConfig = {
	  errorsWrapper: '<div class="errorsWrapper"></div>',
	  errorTemplate: '<span class="validation-error-text-container"></span>'
	};

	function pageValidation(){
		var formValidationID = $("#"+currentPage).find("form").attr("id");
		if(formValidationID !== undefined && parsleyPageValidation(formValidationID) == false){
				return;
		}
	}

	function parsleyPageValidation(parsleyPageID){

		validationCheck = $('#'+parsleyPageID).parsley().validate();

		if($('#'+parsleyPageID).find('.dob').length > 0){
			$('.dob').trigger('blur');
		}

		if($('#'+parsleyPageID).find('.dobCustomError').length > 0){
			validationCheck = validationCheck  && dateValidation == true && $('#'+parsleyPageID).find('.dobCustomError').find('span').length == 0;
		}

		var errorsWrapper = $('#'+parsleyPageID).find("div.errorsWrapper");
		if(validationCheck == true){
			errorsWrapper.hide();
		}else{
			$('#'+parsleyPageID).find("div.errorsWrapper").each(function(){
				if($(this).closest("div.controls").is(":visible") || $(this).closest("div.control-group").is(":visible")){
					$(this).show();
				}else{
					$(this).hide();
				}
			});
		}
		return validationCheck;


	};


	function trimSpaces(paraString){
		return $.trim(paraString);
	}


	/*function resetError(){
		if($("#"+currentPage).find("form").attr("id") !== undefined){
			$('#' + $("#" + currentPage).find("form").attr("id")).parsley().reset();
		}
	}*/
	function updateProgressBar(){
		if(currentPage == 'appscr51'){
			$('#progressDiv').hide();
			$('.triangle-isosceles').css('left','0px');
			return;
		}else{
			$('#progressDiv').show();
		}
		$('.triangle-isosceles').css('visibility','hidden');

		$('.triangle-isosceles').animate({opacity: "0"},'fast');


		var progressPercentage = calculatePagePercentage();
		$('.ssapProgressBar .bar').css('width', progressPercentage*100 + '%');


		$('.triangle-isosceles').animate({left:$('.ssapProgressBar').width()*progressPercentage - 23 + 'px'},300,function(){
			$('.triangle-isosceles').css('visibility','visible');
			$('.triangle-isosceles').text(Math.ceil(progressPercentage*100) + '%').animate({opacity: "1"},'fast');
		});
	}


	function calculateProgressBarHeight(){
		//height of masthead of nm and id are different.
		if(exchangeName === "NMHIX"){
			//new mexico
			if($('#navtopview').length > 0){
				$('#progressDiv').css("padding-top","62px");
			}else{
				$('#progressDiv').css("padding-top","32px");
			}
		}else{
			//idaho
			if($('#navtopview').length > 0){
				$('#progressDiv').css("padding-top","77px");
			}else{
				$('#progressDiv').css("padding-top","15px");
			}
		}
	}

	function calculatePagePercentage(){
		var houseHoldMembercount = getNoOfHouseHolds();
		//var HouseHoldRepeat = webJson.singleStreamlinedApplication

		//4 repeated pages in household, 5 in income, 4 in additional
		var totalPages = pageArr.length + (houseHoldMembercount - 1) * 13;

		var currentPageIndex = pageArr.indexOf(currentPage) + 1;

		var familyRepeatCount = (nonFinancialHouseHoldDone - 1) * 4;
		var incomeRepeatCount = (financialHouseHoldDone - 1) * 5;
		var additionalRepeatCount = (addQuetionsHouseHoldDone - 1) * 4;

		var familyAdditionCount = (houseHoldMembercount - 1) * 4;
		var incomeAdditionCount = (houseHoldMembercount - 1) * 5;
		var additionalAdditionCount = (houseHoldMembercount - 1) * 4;

		//start application:'appscr51', 'appscr52', 'appscr53', 'appscr54', 'appscr55', 'appscr57', 'appscr58', 'appscr59A', 'appscr60'
		if(currentPageIndex < 10){
			return currentPageIndex/totalPages;
		//family and household repeat pages: 'appscr61', 'appscr62Part1',  'appscr63', 'appscr64'
		}else if(10 <= currentPageIndex && currentPageIndex < 14){
			return (currentPageIndex + familyRepeatCount) / totalPages;
		//non-repeat: 'appscr65', 'appscr66', 'appscrBloodRel','appscr67', 'appscr68'
		}else if(14 <= currentPageIndex && currentPageIndex < 19){
			return (currentPageIndex + familyAdditionCount) / totalPages;
		//income repeat pages: 'appscr69', 'appscr70', 'appscr71', 'appscr72', 'appscr73'
		}else if(19 <= currentPageIndex && currentPageIndex < 24){
			return (currentPageIndex + incomeRepeatCount + familyAdditionCount) / totalPages;
		//non-repeat: 'appscr74', 'appscr75A'
		}else if(24 <= currentPageIndex && currentPageIndex < 26){
			return (currentPageIndex + familyAdditionCount + incomeAdditionCount) / totalPages;
		//additional repeat pages: 'appscr76P1', 'appscr77Part1', 'appscr76P2', 'appscr81B'
		}else if(26 <= currentPageIndex && currentPageIndex < 30){
			return (currentPageIndex + additionalRepeatCount + familyAdditionCount + incomeAdditionCount) / totalPages;
		//'appscr83', 'appscr84','appscr85', 'appscr87'
		}else{
			return (currentPageIndex + familyAdditionCount + incomeAdditionCount + additionalAdditionCount) / totalPages;
		}


	}


	function getAmericanAlaskaTribeList(stateCode,dropdownFocused) {
		$.ajax({
			type : "POST",
			url : theUrlForAmericanAlaskaTribe,
			data : {stateCode : stateCode,
				csrftoken : csrftoken },
			dataType:'json',
			success : function(response) {
				populateTribes(response, dropdownFocused);
			},error : function(e) {
				 var optionsstring = '<option value="">Tribe Name</option>';
					var i =0;
					$(dropdownFocused).html('');
					$(dropdownFocused).append(optionsstring);
			}
		});
	}

	function populateTribes(response, dropdownFocused) {
	    var optionsstring = '<option value="">Tribe Name</option>';
			var i =0;
			$.each(response, function(key, value) {
				var optionVal = key;/*+'#'+value;*/
				var selectedoption = (tribeCode == optionVal)? "selected" : "";
				var options = '<option value="'+optionVal+'" '+selectedoption+'>'+ value +'</option>';
				optionsstring = optionsstring + options;
				i++;
			});
			$(dropdownFocused).html('');
			$(dropdownFocused).append(optionsstring);
			noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
			for ( var i = 1; i <= noOfHouseHoldmember; i++) {
				householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				if(householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator == true){
					var tribeCode = householdMemberObj.americanIndianAlaskaNative.tribeName;
					$('#tribeName'+i+ ' option[value='+tribeCode+']').attr('selected','selected');
				}
			}

	}


	function populateCountriesOfIssuance(){
		var response = $("#alCountryOfIssuances").val();
		response = $.parseJSON(response)
		var optionsstring = '<option value="">Country Name</option>';
		$.each(response , function(key, value) {
			var optionVal = key;/*+'#'+value;*/
			var options = '<option value="'+optionVal+'">'+ value +'</option>';
			optionsstring = optionsstring + options;

		});

		$('#tempcarddetailsCounty').html('');
		$('#tempcarddetailsCounty').append(optionsstring);

		$('#machinecarddetailsCounty').html('');
		$('#machinecarddetailsCounty').append(optionsstring);

		$('#arrivaldeprecordForeignCounty').html('');
		$('#arrivaldeprecordForeignCounty').append(optionsstring);

		$('#ForeignppI94County').html('');
		$('#ForeignppI94County').append(optionsstring);

		$('#certificatenonimmigrantF1-Student-I20County').html('');
		$('#certificatenonimmigrantF1-Student-I20County').append(optionsstring);

	}

	function saveCurrentApplicantID(i){
		$('#houseHoldTRNo').val(parseInt(i));
	}

	function getCurrentApplicantID(){
		return parseInt($('#houseHoldTRNo').val());
	}


	function updateCoverageYear(){
		$('.coverageYear').text(getCoverageYear());
	}

	function getCoverageYear(){
		var coverageDate = new Date();
		var coverageMonth = coverageDate.getMonth()+1;

		var coverageYear = coverageDate.getFullYear();

		if(coverageMonth >= 11 || coverageYear == 2014){
			coverageYear++;
		}

		return coverageYear;
	}


	function validateSignature(){

		var esignature = $("#appscr85ESignature").val();
		var name = "";

		if($("#appscr85ESignature").closest('div.control-group').find('#esignErrorDiv').length == 0){
			$("#appscr85ESignature").closest('div.control-group').append('<div id="esignErrorDiv"></div>');
		};
		var esignCustomError = $("#appscr85ESignature").closest('div.control-group').find('#esignErrorDiv');

		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

		if(householdMemberObj != undefined){
			if(getFormattedname(householdMemberObj.name.middleName)!= ""){
				name = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
			}
			else{
				name = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.lastName);
			}
		}

		if(trimSpaces(esignature.toLowerCase()) == trimSpaces(name.toLowerCase())){
			signatureValidation = true;
			esignCustomError.html('');
		}
		else{
			esignCustomError.html('');
			esignCustomError.html('<span class="validation-error-text-container">Please Enter  valid Signature</span>');
			signatureValidation = false;
		}
	}

	function update26Indicator(){
		if($("#exchangeName").val().trim() == "Your Health Idaho"){

			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;

			var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
			for(var j=0;j<parimaryContactRelationArr.length;j++){

				var relatedMemberObj = householdMemberObj[parimaryContactRelationArr[j].relatedPersonId - 1];

				//only care relationship to PC
				if(parimaryContactRelationArr[j].individualPersonId == '1'){

					//No age check for PC, spouse and ward, so under26Indicator should be always true for them
					if(parimaryContactRelationArr[j].relation == '18' || parimaryContactRelationArr[j].relation == '01' || parimaryContactRelationArr[j].relation == '31' ){
						relatedMemberObj.under26Indicator = true;
					}else{
						var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);
						// if they are 26 years or over, mark under26Indicator as false
						if(ageGreaterThan26(dateOfBirth)){

							relatedMemberObj.under26Indicator = false;
						}
						else{
							relatedMemberObj.under26Indicator = true;
						}
					}
				}
			}

		}
	}


/*
****************************************************************************************************************
* NoOfHouseHold.js                                                                                        *
****************************************************************************************************************
*/
	var incomeFalg = false;
	var incomeLabel = new Object();

		incomeLabel.Financial_Job = 'Job Income';
		incomeLabel.Financial_Self_Employment = 'Self Employment Income';
		incomeLabel.Financial_SocialSecuritybenefits = 'Social Security Benefits Income';
		incomeLabel.Financial_Unemployment = 'Unemployment Income';
		incomeLabel.Financial_Retirement_pension = 'Retirement and Pension Income';
		incomeLabel.Financial_Capitalgains = 'Capital Gain Income';
		incomeLabel.Financial_InvestmentIncome = 'Investment Income';
		incomeLabel.Financial_RentalOrRoyaltyIncome = 'Rental & Royality Income';
		incomeLabel.Financial_FarmingOrFishingIncome = 'Farming or Fishing income';
		incomeLabel.Financial_AlimonyReceived = 'Alimony Received Income';
		incomeLabel.Financial_OtherIncome = 'Other Income';


	var incomeMultiply = new Object();
	incomeMultiply.Hourly="8640";
	incomeMultiply.Daily="360";
	incomeMultiply.Weekly="480";
	incomeMultiply.EveryTwoWeeks="24";
	incomeMultiply.Monthly="12";
	incomeMultiply.Yearly="1";
	incomeMultiply.OneTimeOnly="1";
	incomeMultiply.SelectFrequency="1";

	var noOfHouseHold = 1;
	var count = 1;

	function createHousehold()
	{

		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}


		noOfHouseHold = $('#noOfPeople53').val();
		$('#numberOfHouseHold').text(noOfHouseHold);
		var i;
		var strOuter = "";
		/*$('.parsley-error-list').hide();*/
		for (i = 1; i <= noOfHouseHold; i++) {

			if($('#appscr57FirstName'+i).html() != undefined)
			{
				continue;
			}

			var temp = jQuery.i18n.prop('ssap.primaryContact');
			if(i!=1){
				temp="";
			}

			var str='<div class="header margin10-b"><h4><strong id="applicantTitle57'+i+'" class = "camelCaseName"></strong> '+temp +'</h4></div>'
			/*+'<h4>heading'+jQuery.i18n.prop("ssap.applicant")+'&nbsp;'+i+'&nbsp;'+temp +'</h4>'*/
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop('ssap.firstName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
			+'<span aria-label="Required!"></span></label><div class="controls">'
			+'<input id="appscr57FirstName'+i+'"type="text" onkeyup="removeValidationFirstName('+i+'); setName('+i+')"'
			+ 'onclick="removeValidationFirstName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.firstName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.firstNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.firstNameRequired")+'" />'
			+'</div></div>'
			+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.middleName")+'</label><div class="controls">'
			+'<input id="appscr57MiddleName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.middleNameValid")+'"'
			+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.middleName")+'" class="input-medium">'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.lastName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
			+'<span aria-label="Required!"></span></label><div class="controls">'
			+'<input id="appscr57LastName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')"'
			+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.lastName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z]))+((\\s|-)?([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z])))*)$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" />'
			+'</div></div>'
			+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.suffix")+'</label><div class="controls">'
			+'<select id="appscr57Suffix'+i+'" class="input-medium">'
			+'<option value="">'+jQuery.i18n.prop("ssap.suffix")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.sr")+'">'+jQuery.i18n.prop("ssap.sr")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.jr")+'">'+jQuery.i18n.prop("ssap.jr")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.III")+'">'+jQuery.i18n.prop("ssap.III")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.IV")+'">'+jQuery.i18n.prop("ssap.IV")+'</option>'
			+'</select></div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.dateOfBirth")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
			+'<span aria-label="Required!"></span></label><div class="controls">'
			+'<div class="input-append date date-picker" id="calDiv'+i+'" data-date="">'
			+'<input type="text" onkeyup="removeValidationDob('+i+')" onclick="removeValidationDob('+i+')" id="appscr57DOB'+i+'" placeholder="'+jQuery.i18n.prop("ssap.mmDDYYYY")+'"'
			+'class="input-medium dob" '
			+'onfocusout = "validateBirthDate($(this))" '
			+'onblur = "validateBirthDate($(this))" '
			+'data-parsley-trigger="change" data-parsley-errors-container="#dateOfBirthErrorDiv'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.dobRequired")+'" '
			+'/><span class="add-on"><i class="icon-calendar"></i></span></div>'
			+'<div id="dateOfBirthErrorDiv'+i+'"></div>'
			+'</div></div>'
			/*hix-33010*/
			/*+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.dateOfBirthVerified")+'</label><div class="controls">'
			+'<select id="appscr57DOBVerificationStatus'+i+'" class="input-medium">'
			+'<option value ="yes">'+jQuery.i18n.prop("ssap.yes")+'</option>'
			+'<option value ="no">'+jQuery.i18n.prop("ssap.no")+'</option>'
			+'</select></div></div>'*/
			+'<div class="control-group hide">'
			+'<p>'+jQuery.i18n.prop("ssap.has")+'&nbsp;<strong id="applicantName57'+i+'"></strong>&nbsp;'+jQuery.i18n.prop("ssap.usedTobaccoProducts")+'<img width="10" height="10"  src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>'
			+'<div class="controls">'
			+'<label><input type="radio" name="appscr57Tobacco'+ i+ '" value="yes" id="appscr57SexYes'+ i+ '">'+jQuery.i18n.prop("ssap.yes")+'</label>'
			+'<label><input type="radio" name="appscr57Tobacco'+ i+ '" value="no" id="appscr57SexNo'+ i+ '">'+jQuery.i18n.prop("ssap.no")+'</label>'
			+'</div>'
			+'</div>'

			/*+'<div class="control-group hide"><label class="control-label">dobver'+jQuery.i18n.prop('ssap.sex')+'</label><div class="controls">'
			+'<div class="isSex"><label><input type="radio" name="appscr57Sex'+i+'" value="male" id="appscr57SexMale'+i+'" required class="radioValidate">'+jQuery.i18n.prop("ssap.male")+'</label>'
			+'<label><input type="radio" name="appscr57Sex'+i+'"value="female" id="appscr57SexFemale'+i+'">'+jQuery.i18n.prop("ssap.female")+'</label>'
			+'</div></div><span class="errorMessage"></span></div>'*/

	/*		+'<div class="control-group">'
			+'<label class="control-label">Relationship to <strong id="appscr57relationshiplbl'+i+'"></strong> (Primary Contact)</label>'
			+'<div class="controls">'
			+'<select id="appscr57relationship'+i+'" name="appscr57relationship'+i+'" class="input-large">'
			+'<option value="0">Select</option>'
			+'<option value="Wife">Wife</option>'
			+'<option value="Child">Child</option>'
			+'<option value="Parent">Parent</option>'
			+'<option value="Caretaker">Caretaker</option>'
			+'</select>'
			+'</div></div>'*/

			+'<div class="control-group"><label class="controls capitalize-none"><input type="checkbox" name="appscr57checkseekingcoverage'+i+'" id="appscr57checkseekingcoverage'+i+'">Are you seeking coverage?</label></div>';

			if (i != noOfHouseHold)
				str += "<hr>";
			strOuter += str;

		}

		$('#appscr57HouseholdForm').html(strOuter);

		/*retrieve data once user go back after adding a new applicant Refer HIX-32515 --- (code done by shovan)*/
		var firstName,middleName, lastName, identifire, suffix, dob, dobVerified, emailAddress, tobacco, gender, isChild, isInfant;
		$('#appscr57HouseHoldPersonalInformation tr').each(function(index) {
		        $this = $(this);
		        firstName = $this.find(".firstName").text(),
		        middleName= $this.find(".middleName").text(),
		        lastName= $this.find(".lastName").text(),
		        identifire= $this.find(".identifire").text(),
		        suffix= $this.find(".suffix").text(),
		        dob= $this.find(".dob").text(),
		        dobVerified= $this.find(".dobVerified").text(),
		        emailAddress= $this.find(".emailAddress").text(),
		        tobacco= $this.find(".tobacco").text(),
		        gender= $this.find(".gender").text(),
		        isChild= $this.find(".isChild").text(),
		        isInfant= $this.find(".isInfant").text();
		        $('#appscr57FirstName'+(index+1)).val(firstName);
		        $('#appscr57FirstName'+(index+1)).attr("value",firstName);
		        $('#appscr57MiddleName'+(index+1)).val(middleName);
		        $('#appscr57MiddleName'+(index+1)).attr("value",middleName);
		        $('#appscr57LastName'+(index+1)).val(lastName);
		        $('#appscr57LastName'+(index+1)).attr("value",lastName);
		        var applicantName = trimSpaces(firstName)+" "+trimSpaces(middleName)+" "+trimSpaces(lastName);
		        $('#applicantName57'+(index+1)).text(applicantName);
		        $('#appscr57DOB'+(index+1)).val(dob);
		        $('#appscr57DOB'+(index+1)).attr("value",dob);
		        $('#appscr57DOBVerificationStatus'+(index+1)).val(dobVerified);
		        $('#appscr57DOBVerificationStatus'+(index+1)).attr("value",dobVerified);
		        $('#appscr57Suffix'+(index+1)).val(suffix);
		        $('#appscr57Suffix'+(index+1)).attr("value",suffix);
		        $('#applicantTitle57'+(index+1)).text(applicantName);

		        if(tobacco === "yes"){
		        	$('input[id=appscr57SexYes'+(index+1)+']:radio').prop('checked', true);
		        }else{
		        	$('input[id=appscr57SexNo'+(index+1)+']:radio').prop('checked', true);
		        }

		});
		/*retrieve data once user go back after adding a new applicant--- (code done by shovan)*/

		$('#appscr57FirstName1').val($('#firstName').val());
		$('#appscr57FirstName1').attr("value",$('#firstName').val());
		$('#appscr57MiddleName1').val($('#middleName').val());
		$('#appscr57MiddleName1').attr("value",$('#middleName').val());
		$('#appscr57LastName1').val($('#lastName').val());
		$('#appscr57LastName1').attr("value",$('#lastName').val());
		var applicantName = trimSpaces($('#firstName').val())+" "+trimSpaces($('#middleName').val())+" "+trimSpaces($('#lastName').val());
		$('#applicantName571').text(applicantName);
		$('#appscr57DOB1').val($('#dateOfBirth').val());
		$('#appscr57DOB1').attr("value",$('#dateOfBirth').val());
		$('#appscr57DOBVerificationStatus1').val($('#dobVerificationStatus').val());
		$('#appscr57DOBVerificationStatus1').attr("value",$('#dobVerificationStatus').val());
		$('#appscr57Suffix1').val($('#appscr53Suffix').val());
		$('#appscr57Suffix1').attr("value",$('#appscr53Suffix').val());

		//var applicantName = $('#appscr57FirstName' + i).val()+" "+ $('#appscr57MiddleName' + i).val()+" "+$('#appscr57LastName' + i).val();
		var applicantName = trimSpaces($('#firstName').val())+" "+trimSpaces($('#middleName').val())+" "+trimSpaces($('#lastName').val());;
		$('#applicantTitle571').text(applicantName);

		addMaskingOnAppscr57();

		for(var j=1;j<=i;j++){
			createCalendar(j);
			/*$('.characterStroke').ForceCharacterOnly();*/
		}

		var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
			if(householdMemberObj.applyingForCoverageIndicator == true)
				$('#appscr57checkseekingcoverage'+cnt).prop("checked", true);
			else
				$('#appscr57checkseekingcoverage'+cnt).prop("checked", false);
		}

		next();
	}

	function addHouseHoldDetail(){
		if(editFlag == true && currentPage == "appscr57")
		{
			appscr58EditUdate(editMemberNumber,false);
			return false;
		}

		if(currentPage != 'appscr58'){ //  HIX-50570
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}
		}
		$('#cancelButtonCancel').hide();
		cancleFlag = true;

		var houseHoldNames=new Array();

		noOfHouseHold = $('#numberOfHouseHold').text();
		//noOfHouseHold = getNoOfHouseHolds()+1;
		//empty array
		applyingForCoverageIndicatorArrCopy = [];

		//noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		if(useJSON == true){
		for ( var cnt = 1; cnt <= noOfHouseHold; cnt++) {
			noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

			if(cnt > noOfHouseHoldmember){
				defaultJSON = getDefatulJson();
				householdMemberObj=defaultJSON.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
				//householdMemberObj.personId = cnt;
				//var lastPersonid = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[noOfHouseHoldmember-1].personId;
				householdMemberObj.personId = getNextApplicantId();
			}else{
				householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
			}
			if(trimSpaces($('#appscr57FirstName'+cnt).val())!=""){
				householdMemberObj.name.firstName = $('#appscr57FirstName'+cnt).val();
				householdMemberObj.name.middleName = $('#appscr57MiddleName'+cnt).val();
				householdMemberObj.name.lastName = $('#appscr57LastName'+cnt).val();
				householdMemberObj.name.suffix = $('#appscr57Suffix'+cnt).val();
				householdMemberObj.dateOfBirth = DBDateformat($('#appscr57DOB'+cnt).val());
				householdMemberObj.tobaccoUserIndicator = false;

				if($('input[name=liveWithSpouseIndicator'+ cnt +']:radio:checked').val()=='yes'){
					householdMemberObj.tobaccoUserIndicator = true;
				}

				householdMemberObj.applyingForCoverageIndicator = $('input[name=appscr57checkseekingcoverage'+cnt+']').is(':checked') == true  ? true : false;

				//for updating seeking coverage indicator, saveJsonData line 250
				applyingForCoverageIndicatorArrCopy.push(householdMemberObj.applyingForCoverageIndicator);
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
			}
		}
		}

		for ( var i = 1; i <= noOfHouseHold; i++) {
			if((haveData && i<=houseHoldNumberValue) || $('#HouseHold'+i).html() != undefined){
				appscr58EditUdate(i,false);
				continue;
			}

			if($('#appscr57MiddleName' + i).val()=='Middle Name'){
				$('#appscr57MiddleName' + i).val('');
			}

			if($('#appscr57LastName' + i).val()=='Last Name'){
				$('#appscr57LastName' + i).val('');
			}
			//var HouseHoldName = "" + $('#appscr57FirstName' + i).val() + " " + $('#appscr57MiddleName' + i).val() + " " + $('#appscr57LastName' + i).val();
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName + "" + householdMemberObj.name.suffix;
			houseHoldNames[i-1]=HouseHoldName;
			if(i>1){

				$('#appscr57FirstName' + i).attr("value",$('#appscr57FirstName'+i).val());
				$('#appscr57MiddleName'+i).attr("value",$('#appscr57MiddleName'+i).val());
				$('#appscr57LastName'+i).attr("value",$('#appscr57LastName'+i).val());
				$('#appscr57Suffix'+i).attr("value",$('#appscr57Suffix'+i).val());
				$('#appscr57DOBVerificationStatus'+i).attr("value",$('#appscr57DOBVerificationStatus'+i).val());
				$('#appscr57DOB'+i).attr("value",$('#appscr57DOB'+i).val());
				/*HIX-45293 - remove don't allow same name*/
				//don't allow same name(first + middle + last + suffix)
				/*for(var j=0; j<(houseHoldNames.length-1); j++){
					if(houseHoldNames[j] == HouseHoldName ){
						$('#duplicatedNameErrorDiv' + i).html('<div class="errorsWrapper filled"><span class="validation-error-text-container parsley-required">Two  applicant cannot have the same name.</span></div>');
						//addLabels('Two  applicant cannot have the same name',"appscr57FirstName"+i);
						$('#appscr57FirstName' + i).focus();
						return false;
					}else{
						$('#duplicatedNameErrorDiv' + i).html('');
					}
				}*/
			}

		}
		if(SavingData == false){
		next();
		addAppscr58Detail();
		$('#addAnotherIndividualButton').show();
		$('#addAnotherIndividual').hide();
		}
	}

	function resetHouseHoldDetail(i)
	{


	}


	function repeatPage() {
		noOfHouseHold = getNoOfHouseHolds();
		if (count <= noOfHouseHold) {
			setNonFinancialPageProperty(count);
			count++;
		} else {
			count = 1;
			next();
		}
	}

	function goToAppscr61()
	{

		setNonFinancialPageProperty(1);
		count++;
		next();
	}


	function setNonFinancialPageProperty(i)
	{
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
		if(useJSON == true){
			$('.nameOfHouseHold').html(""+houseHoldname);
		}
		//$('#houseHoldTRNo').val('HouseHold'+i);
		saveCurrentApplicantID(i);
		//appscr67EditControllerJSON(i);
		setHisOrHerAfterContinue();
		updateLiveWithChildUnderAgeView();
		return;
	}

	//keys

	function Appscr58DetailJson(){
		document.getElementById('appscr58HouseholdForm').innerHTML = '';
		var strOuter = "";
		noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
			var HouseHoldName="" + householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;
			var dateOfBirth = UIDateformat(householdMemberObj.dateOfBirth);
			var str="";
			if(cnt==1)
			{
				str+="<div class='hixApplicantInfo camelCaseName'><h5>"+HouseHoldName+" "+jQuery.i18n.prop('ssap.primaryContact')+"<span class='pull-right'><input type='button' value='Edit' class='btn btn-primary' onclick=appscr58EditController("+cnt+") /></span></h5>";
			    str+="<p style='text-transform: none;'>"+jQuery.i18n.prop('ssap.dateOfBirth1')+" "+dateOfBirth+"</p></div>";
			}
			else if(cnt<=noOfHouseHoldmember && cnt!=1)
			{
				str+="<div class='hixApplicantInfo camelCaseName'><h5>"+HouseHoldName+" <span class='pull-right'><input type='button' value='Delete' class='btn' style='background-image: none; padding: 2px 8px; margin: 2px 0 0 9px;' onclick=removeApplicant("+householdMemberObj.personId+") /></span><span class='pull-right'><input type='button' value='Edit' class='btn btn-primary' onclick=appscr58EditController("+cnt+") /></span></h5>";
			    str+="<p style='text-transform: none;'>"+jQuery.i18n.prop('ssap.dateOfBirth1')+" "+dateOfBirth+" </p></div>";
			}
			strOuter += str;
		}
		$("#appscr58HouseholdForm").html(strOuter);
	}
	function addAppscr58Detail(){
		if(useJSON == true){
			Appscr58DetailJson();
			return;
		}

	}


	//Keys
	function addSpecialCircumstances()
	{
		noOfHouseHold = getNoOfHouseHolds();

		var i;
		var strOuter = "";
		var femaleFlag=0;

		//strOuter+= '<h5>'+jQuery.i18n.prop("ssap.answerTheQuestionsBelow")+'</h5>';
		strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.doAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

		for (i = 1; i <= noOfHouseHold; i++){
			//var HouseHoldName=''+$("#appscr57FirstNameTD"+i).text()+' '+$("#appscr57MiddleNameTD"+i).text()+' '+$("#appscr57LastNameTD"+i).text();
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			strOuter+='<label><input type="checkbox" id="disability'+i+'" onchange="disabilityOnChange()" name="personDisability" value="disability'+i+'" data-parsley-errors-container="#disabilityErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">'+HouseHoldName+'</label>';
		}

		strOuter+='<label class="capitalize-none"><input type="checkbox"  name="personDisability" onchange="disabilityNoneOfTheseChecked()" id="disabilityNoneOfThese" value="disabilityNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
		strOuter+='<div id="disabilityErrorDiv"></div>';
		strOuter+='</div></div><div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.doAnyOfThePeopleBelowNeedHelp")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l"  data-rel="multipleInputs">';

		for (i = 1; i <= noOfHouseHold; i++){
			//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="assistanceServicesOnChange()" name="personPersonalAssistance" id="assistanceServices'+i+'" value="assistanceServices'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#assistanceServicesErrorDiv">'+HouseHoldName+'</label>';
		}

			strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="assistanceServicesNoneOfTheseChecked()" name="personPersonalAssistance" id="assistanceServicesNoneOfThese" value="assistanceServicesNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
			strOuter+='<div id="assistanceServicesErrorDiv"></div>';
			strOuter+='</div></div>';
			strOuter+='<div class="control-group"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

		for (i = 1; i <= noOfHouseHold; i++){

			//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			HouseHoldName=getNameByPersonId(i);
			var HouseHoldgender = householdMemberObj.gender;
			if(HouseHoldgender != "" && HouseHoldgender != undefined){
				if(HouseHoldgender.toLowerCase() == 'female') {
					femaleFlag=1;
				}
			}
			strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="nativeOnChange()" name="peopleBelowTo" id="native'+i+'" value="native'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#nativeErrorDiv"><span class="camelCaseName">'+HouseHoldName+'</span></label>';
			strOuter+='<div class="control-group hide" id="nativeTribe'+i+'"><p>';
			strOuter+=jQuery.i18n.prop("ssap.is")+'&nbsp;<strong><span class="camelCaseName">'+HouseHoldName+'</span></strong>&nbsp';
			strOuter+=jQuery.i18n.prop("ssap.page29.aMemberOfAFederally")+'?<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p>';
			strOuter+='<div class="margin5-l controls">';
			strOuter+='<label><input type="radio" name="AmericonIndianQuestionRadio' + i + '" id="AmericonIndianQuestionRadioYes' + i + '" value="yes" onchange="checkAmericonIndianQuestion(' + i + ');"  data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#americonIndianQuestionRadioYesErrorDiv' + i + '"/>';
			strOuter+=jQuery.i18n.prop("ssap.yes")+'</label>';
			strOuter+='<label><input type="radio" name="AmericonIndianQuestionRadio' + i + '" id="AmericonIndianQuestionRadioNo' + i + '" value="no"  onchange="checkAmericonIndianQuestion(' + i + ');">';
			strOuter+=jQuery.i18n.prop("ssap.no")+'</label>';
			strOuter+='<div id="americonIndianQuestionRadioYesErrorDiv' + i + '"></div>';
			strOuter+='</div>';

			strOuter+='<div class="hide clearfix" id="americonIndianQuestionDiv' + i + '">';
			strOuter+='<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
			+'<span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<select id="americonIndianQuestionSelect'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" onChange="getAmericanAlaskaTribeList(this.value,tribeName'+i+')" >'
			+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
			+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
			+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
			+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
			+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
			+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
			+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
			+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
			+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
			+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
			+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
			+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
			+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
			+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
			+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
			+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
			+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'
			+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
			+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
			+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
			+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
			+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
			+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
			+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
			+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
			+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
			+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
			+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
			+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
			+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
			+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
			+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
			+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
			+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
			+'</select>';
			strOuter+='</div></div>';


			strOuter+='<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.page29.tribeName")+''
			+'<span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<select id="tribeName'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">'
			+'<option value="">'+jQuery.i18n.prop("ssap.page29.tribeName")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.JApacheNation")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.JApacheNation")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.MApacheTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.MApacheTribe")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.NNation")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.NNation")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.OOwingeh")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.OOwingeh")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PAcoma")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PAcoma")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PCochiti")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PCochiti")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PJemez")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PJemez")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PIsleta")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PIsleta")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PLaguna")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PLaguna")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PNambe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PNambe")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPicuris")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPicuris")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPojoaque")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPojoaque")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanFelipe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanFelipe")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanIldefonso")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanIldefonso")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSandia")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSandia")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaAna")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaAna")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaClara")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaClara")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantoDomingo")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantoDomingo")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTaos")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTaos")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTesuque")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTesuque")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PZia")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PZia")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.UMountainTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.UMountainTribe")+'</option>'
			+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.ZTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.ZTribe")+'</option>'
			+'</select>';
			strOuter+='</div></div>';
			strOuter+='</div>';
			strOuter+='</div>';
		}

		strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="nativeNoneOfTheseChecked()" name="peopleBelowTo" id="nativeNoneOfThese" value="nativeNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
		strOuter+='<div id="nativeErrorDiv"></div>';
		strOuter+="</div></div>";

		if(femaleFlag == 1){
			//check num of female that are 13 or above
			var pregnantAgeCount = checkPregnantCount();
		}
		var flag=0;
		var cnt = 0;
		for (i = 1; i <= noOfHouseHold; i++){
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var fDob = householdMemberObj.dateOfBirth;
			var HouseHoldgender = householdMemberObj.gender;
			if(HouseHoldgender.toLowerCase() == 'female' && checkBirthDate(fDob)>(365*13)){
				//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
				if(cnt == 0){
					strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelowPregnant")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';
					cnt++;
				}
				var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
				strOuter+='<label><input type="checkbox" id="pregnancy'+i+'" onchange="pregnancyOnChange()" onclick="getExpectedInPregnancy()" name="personPregnant" value="pregnancy'+HouseHoldName+''+i+'"'
						+' data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#pregnancyErrorDiv"'
						+'>'+HouseHoldName+'</label>';
				flag=1;
			}
		}

		if(flag==1){
			strOuter+='<label class="capitalize-none"><input type="checkbox" id="pregnancyNoneOfThese" onchange="pregnancyNoneOfTheseChecked()" name="personPregnant" value="pregnancyNoneOfThese" value="None of these people">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>'
					+'<div id="pregnancyErrorDiv"></div>'
					+'</div></div>';
		}

		strOuter+='<div id="appscr66PregnancyDetail" class="margin30-l margin10-b"> </div>';
		strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.wereAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

	    for (i = 1; i <= noOfHouseHold; i++)
		{
	    	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	    	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	    	var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	    	strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="personfosterCareOnChange()" name="personfosterCare" id="personfosterCare'+i+'" value="personfosterCare'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#personfosterCareErrorDiv">'+HouseHoldName+'</label>';
		}
	    strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="personfosterCareNoneOfTheseChecked()" name="personfosterCare" id="personfosterCareNoneOfThese" value="personfosterCareNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
	    strOuter+='<div id="personfosterCareErrorDiv"></div>';
	    strOuter+='</div></div>';
	    strOuter+='<div id="appscr66DeceasedDetail"></div>';
	    strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelowDeceased")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

	    for (i = 1; i <= noOfHouseHold; i++)
		{
	    	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	    	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

	    	strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="deceasedOnChange()" name="deceasedPerson" id="deceased'+i+'" value="deceased'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#deceasedErrorDiv">'+HouseHoldName+'</label>';
		}
	    strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="deceasedNoneOfTheseChecked()" name="deceasedPerson" id="deceasedPersonNoneOfThese" value="deceasedPersonNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
	    strOuter+='<div id="deceasedErrorDiv"></div>';
	    strOuter+='</div></div>';

		for (i = 1; i <= noOfHouseHold; i++){
			//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			strOuter+='<div class="control-group eligibleCoverage non_financial_hide"><p>';
			strOuter+=jQuery.i18n.prop("ssap.is")+'&nbsp;<strong>'+HouseHoldName+'</strong>&nbsp';
			strOuter+=jQuery.i18n.prop("ssap.page28.eligibleForHealthCovergae")+'&nbsp;'+HouseHoldName+'&nbsp;'+jQuery.i18n.prop("ssap.page28.isntCurrentlyEnrolled")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></p>';
			strOuter+='<div class="margin5-l">';
			strOuter+='<label class="capitalize-none"><input type="radio" id="appscr78Medicare'+i+'" name="fedHealth'+i+'" value="Medicare" onchange="otherInsuranceCheckbox('+ i +')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#appscr78MedicareErrorDiv'+i+'">';
			strOuter+=jQuery.i18n.prop("ssap.page28.medicare")+'</label>';

			strOuter+='<label ><input type="radio" id="appscr78Tricare'+i+'" name="fedHealth'+i+'" value="TRICARE" onchange="otherInsuranceCheckbox('+ i +')">';
			strOuter+=jQuery.i18n.prop("ssap.page28.tricare")+'</label>';

			strOuter+='<label ><input type="radio" id="appscr78PeaceCorps'+i+'" name="fedHealth'+i+'" value="PeaceCorps" onchange="otherInsuranceCheckbox('+ i +')">';
			strOuter+=jQuery.i18n.prop("ssap.page28.peaceCorps")+'</label>';

			strOuter+='<label class="capitalize-none"><input type="radio" name="fedHealth'+i+'" id="appscr78OtherStateFHBP'+i+'" value="Other" onchange="otherInsuranceCheckbox('+ i +')">';
			strOuter+=jQuery.i18n.prop("ssap.page28.otherStateOrFederal")+'</label>';

			strOuter+='<div id="appscr78OtherStateOrFederalHealthPlan'+i+'" class="appscr78OtherStateOrFederalHealthPlan hide"><div class="control-group">';
			strOuter+='<label class="control-label">'+jQuery.i18n.prop("ssap.page28.programType")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></label>';
			/*strOuter+='<div class="controls"><input type="text"  id="appscr78TypeOfProgram'+i+'" name="typeTxt" placeholder="'+jQuery.i18n.prop("ssap.page28.type")+'" class="input-medium"/>';*/
			strOuter+='<div class="controls"><select id="appscr78TypeOfProgram'+i+'" class="input-medium" data-parsley-trigger="change" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.programType")+'"><option value>'+jQuery.i18n.prop("ssap.page28.selectType")+'</option><option value="State">'+jQuery.i18n.prop("ssap.page28.state")+'</option><option value="Federal">'+jQuery.i18n.prop("ssap.page28.federal")+'</option></select>'
			strOuter+='</div></div>';
			strOuter+='<div class="control-group"><label class="control-label">';
			strOuter+=jQuery.i18n.prop("ssap.page28.programName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></label>';
			strOuter+='<div class="controls"><input type="text" id="appscr78NameOfProgram'+i+'" name="programTxt" placeholder="'+jQuery.i18n.prop("ssap.page28.programName")+'" class="input-medium" data-parsley-trigger="change" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.programName")+'"/>';
			strOuter+='</div></div></div><!-- appscr78OtherStateOrFederalHealthPlan ends -->';

			strOuter+='<label class="capitalize-none"><input type="radio" id="appscr78None'+i+'" name="fedHealth'+i+'" value="None" onchange="otherInsuranceCheckbox('+ i +')">';
			strOuter+=jQuery.i18n.prop("ssap.noneOfTheAbove")+'</label>';
			strOuter+='<div id="appscr78MedicareErrorDiv'+i+'"></div>';
			strOuter+='</div></div>';
		}

		$("#appscr66SpecialCircumstances").html(strOuter);

		if(ssapApplicationType == "NON_FINANCIAL"){
			$(".non_financial_hide").css("display","none");
			$(".non_financial_hide input").removeAttr("parsley-mincheck").removeClass("parsley-validated parsley-error");

		}
		appscr66FromMongodbJson();
		//next();
	}


	//Keys
	function getExpectedInPregnancy()
	{
		$('#appscr66PregnancyDetail').html("");
		var strOut="";
		noOfHouseHold = getNoOfHouseHolds();
		 for (var i = 1; i <= noOfHouseHold; i++)
			{
			 householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			 var HouseHoldgender = householdMemberObj.gender;

			 if(HouseHoldgender.toLowerCase() == 'female')
				 {
				 //if(document.getElementById("pregnancy"+i).checked == true)
				 if($('#pregnancy' + i).is(":checked"))
				 	{
					 	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

						var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
					 	strOut+='<div>'+jQuery.i18n.prop("ssap.howManyBabiesIs")+' <strong>'+HouseHoldName+'</strong> '+jQuery.i18n.prop("ssap.expectingDuringThisPregnancy")+'</div>';
					 	strOut+='<div  class="form_row controls margin5-l"><select name="numberOfBabies" id="number-of-babies'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">';
					 	strOut+='<option value=""> Please Select </option>	<option value="1">1</option>	<option value="2">2</option>	  <option value="3">3</option>';
					 	strOut+='<option value="4">4</option>	 <option value="5">5</option>	 <option value="6">6</option>	 <option value="7">7</option>';
					 	strOut+='<option value="8">8</option>	 <option value="9">9</option>	</select><span class="errorMessage"></span></div>  ';
				 	}
				 }
			}

		 $("#appscr66PregnancyDetail").html(strOut);
		 for (var i = 1; i <= noOfHouseHold; i++){
			 householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			 var HouseHoldgender = householdMemberObj.gender;

			 if(HouseHoldgender.toLowerCase() == 'female' && $('#pregnancy' + i).is(":checked") && householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy>0) {
				 $('#number-of-babies'+i).val(householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy);
			 }
		 }
	}

	function saveSpecialCircumstances()
	{
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}
		//showHouseHoldContactSummary();
		next();

	}

	function appscr72CheckBoxAction(idOfCb)
	{
		document.getElementById("appscr72incomeNone").checked = false;
		var currentId = $(idOfCb).attr('id');
		if(document.getElementById(currentId).checked == true)
			{

			$('#'+currentId+"TR").show();
			}
		else
			{
			$('#'+currentId+"TR").hide();

			appscr72CheckBoxesValuesSet(idOfCb);
			}

	}

	function appscr72CheckBoxesValuesSet(idOfCb)
	{
		idOfCb = $(idOfCb).attr('id');

		if(idOfCb == "appscr72alimonyPaid")
		{
			$('#alimonyAmountInput').val('');
			$('#alimonyFrequencySelect').val('SelectFrequency');
		}
		else if(idOfCb == "appscr72studentLoanInterest")
		{
			$('#studentLoanInterestAmountInput').val('');
			$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
		}
		else if(idOfCb == "appscr72otherDeduction")
		{
			$('#deductionTypeInput').val('');
			$('#deductionAmountInput').val('');
			$('#deductionsFrequencySelect').val('SelectFrequency');
		}
	}

	function appscr72DisableDeductions(idOfCb)
	{
		if(document.getElementById("appscr72incomeNone").checked == true)
			{
			document.getElementById("appscr72alimonyPaid").checked = false;
			document.getElementById("appscr72studentLoanInterest").checked = false;
			document.getElementById("appscr72otherDeduction").checked = false;

			$('#alimonyAmountInput').val('');
			$('#studentLoanInterestAmountInput').val('');
			$('#deductionTypeInput').val('');
			$('#deductionAmountInput').val('');
			$('#alimonyFrequencySelect').val('SelectFrequency');
			$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
			$('#deductionsFrequencySelect').val('SelectFrequency');

			document.getElementById("appscr72alimonyPaidTR").style.display = 'none';
			document.getElementById("appscr72studentLoanInterestTR").style.display = 'none';
			document.getElementById("appscr72otherDeductionTR").style.display = 'none';
			}

	}

	function appscr72basedOnAmountInput()
	{
		if((document.getElementById("basedOnAmountInput").value.length) >= 1)
			{
			document.getElementById("basedOnAmountCB").checked = false;
			}
	}


	function appscr72BasedOnAmountCB()
	{
		if(document.getElementById("basedOnAmountCB").checked == true)
			document.getElementById("basedOnAmountInput").value = "0";

	}

	function appscr72incomeHighOrLow(){
		if($('input[name=aboutIncomeSpped]:radio:checked').val() =="no"){
			$('#incomeHighOrLow').show();
		}else{
			$('#incomeHighOrLow').hide();
			$('#incomeVariationHigh').prop("checked",false);
			$('#incomeVariationLow').prop("checked",false);
		}
	}

	function addHouseHold(){

		/*if(validation(currentPage)==false)
			return false;*/
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}

		var noOfHouseHold = parseInt($('#numberOfHouseHold').text());

		createTaxRelation();
		setTaxRelatedData();
		if(flagToUpdateFromResult== true){
			HouseHoldRepeat=editMemberNumber;
			flagToUpdateFromResult=false;
			if(checkStillUpdateRequired())
				{

				return true;
				}
			else
				{
				$('#finalEditDiv').show();
				goToPageById('appscrBloodRel');
				return false;
				}


		}
		var primaryTaxFilerPersonId = webJson.singleStreamlinedApplication.primaryTaxFilerPersonId;
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;

		//go to sex page if
		//1.Primary Contact is a tax filer(primaryTaxFilerPersonId == 1)
		//2.Primary Contact is not a tax filer and not being claimed as a dependent of anyone else(primaryTaxFilerPersonId == 0)
		//3.Tax file page is not Primary Contact
		if(primaryTaxFilerPersonId == 1 || primaryTaxFilerPersonId == 0 || HouseHoldRepeat != 1){


			var notSeekingCoverageCount = [];

			$("#notSeekingCoverageList").html("");

			//PC never show in the list
			for(var j=1; j<householdMemberObj.length; j++){
				 if(householdMemberObj[j].applyingForCoverageIndicator == false){
					$("#notSeekingCoverageList").append("<li><strong>" + householdMemberObj[j].name.firstName + " " + householdMemberObj[j].name.middleName + " " + householdMemberObj[j].name.lastName + "</strong></li>");
					notSeekingCoverageCount++;
				}
			}

			if(primaryTaxFilerPersonId == 0){
				$(".PCName").html(householdMemberObj[0].name.firstName + " " + householdMemberObj[0].name.middleName + " " + householdMemberObj[0].name.lastName);
				$("#notSeekingCoverageAlert .PCNotEligible").show();
			}else{
				$("#notSeekingCoverageAlert .PCNotEligible").hide();
			}


			if(notSeekingCoverageCount == 1){
				$("#notSeekingCoverageAlert .dependentNotEligible").show();
				$("#notSeekingCoverageAlert .plural").hide();
				$("#notSeekingCoverageAlert .singular").show();
			}else if(notSeekingCoverageCount > 1){
				$("#notSeekingCoverageAlert .dependentNotEligible").show();
				$("#notSeekingCoverageAlert .singular").hide();
				$("#notSeekingCoverageAlert .plural").show();
			}
			else{
				$("#notSeekingCoverageAlert .dependentNotEligible").hide();
			}

			if(($("#notSeekingCoverageList").html() != "" || primaryTaxFilerPersonId == 0)  && ssapApplicationType == "FINANCIAL"){
				$('#notSeekingCoverageAlert').modal();

				$('#notSeekingCoverageAlert').on('hide', function () {
					setNonFinancialPageProperty(1);
				});

			}else{
				setNonFinancialPageProperty(1);
				next();
			}

		}else{
			HouseHoldRepeat=primaryTaxFilerPersonId;

			goToPageById('appscr60');

			getSpouse(HouseHoldRepeat);
			setNonFinancialPageProperty(HouseHoldRepeat);
			updateAppscr60FromJSON(HouseHoldRepeat);

			//tax page: TF is not PC
			$("#planToFileFTRIndicatorYes").prop("checked",true);

			planToFileYes();
			//hide will Be Claimed
			$("#willBeClaimed, #appscr60ClaimedFilerDiv").hide();


			$("input[name=planToFileFTRIndicator]").prop("disabled",true);

			$("#householdHasDependantYes").prop("checked",true);
			$("input[name=householdHasDependant]").prop("disabled",true);

			$("#householdDependentMainDiv").show();
			//getSpouse();
			$("#householdContactDependant1").prop("checked",true).prop("disabled",true);
		}
		$('html, body').animate({scrollTop:$('#menu').position().top}, 'fast');

	}

	function resetPage69Values()
	{
		$('#appscr69Form input:text').val('');
		$('#appscr69Form select').val('0');
		irsIncomeResetValues();
	}

	function resetPage71Values()
	{
		$('#appscr71Form input:text').val('');
		$('#appscr71Form select').val('0');
	}


	function resetPage72DeductionValues()
	{
		$('#appscr72Form input:text').val('');
		$('#appscr72Form select').val('SelectFrequency');

		$('#appscr72alimonyPaidTR').hide();
		$('#appscr72studentLoanInterestTR').hide();
		$('#appscr72otherDeductionTR').hide();

		document.getElementById('appscr72alimonyPaid').checked = false;
		document.getElementById('appscr72studentLoanInterest').checked = false;
		document.getElementById('appscr72otherDeduction').checked = false;
	}


	function addCurrentIncomeDetail()
	{
		if(editFlag == true)
			return true;
	}

	function incomeDidcrepancies(){
		//
	}


	//Keys
	function addIncarceratedContent()
	{

		//noOfHouseHold = $('#numberOfHouseHold').text();
		noOfHouseHold = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		var i;
		var strOuter = "";

		var str = '<div class="control-group margin0"><p>'+jQuery.i18n.prop('ssap.whoIsIncarcerated')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';

		for (i = 1; i <= noOfHouseHold; i++) {
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var HouseHoldName = householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

			str += '<div class="margin5-l"><label>'
			+'<input type="checkbox" id="incarcerationCB'+i+'" onchange="incarcerationCBChange('+i+')"  name="FNLNS" value="yes" data-parsley-required data-parsley-required-message="This Value Is Required"><span class="camelCaseName">'+HouseHoldName+' <span style="text-transform: none">is incarcerated</span></span></label></div>'
			+'<div id="incarcerationCBOption'+i+'" class="control-group margin0 hide"><p>'+jQuery.i18n.prop('ssap.isThisPersonPendingDisposition')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p><div class="margin5-l"><label>'
			+'<input type="radio" name="disposition'+i+'" value="yes" radio-name = "deposition" data-parsley-required data-parsley-required-message="This Value Is Required" />'+jQuery.i18n.prop('ssap.yes')+'</label>'
			+'<label><input type="radio" name="disposition'+i+'" value="no" radio-name = "deposition" data-parsley-required data-parsley-required-message="This Value Is Required" />'+jQuery.i18n.prop('ssap.no')+'</label></div></div>';


			if(i != noOfHouseHold)
			str +="<hr />";



		}
		str +='</div>';
		strOuter += str;
		document.getElementById('incarceratedContent85').innerHTML = strOuter;
		next();
	}
	function enableSubmitCSRApplicationType(){
		$('#btnSubmitCSRApplicationType').removeAttr('disabled');
	}
	function cancelCSRApplicationType(){
		$('#modalCSRApplicationType').modal('hide');
	}

	function saveIncarceratedDetail()
	{
		/*pageValidation();*/
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}
		//var userActiveRoleName = $('#userActiveRoleName').val();
		/*
		if($("#applicationType").val()=='OE'){
			saveDataSubmit();
			next();
			return;
		}
		*/
		if("Y" == $("#trackApplicationSource").val()){
			$('#modalCSRApplicationType').modal();
				if($("#applicationType").val()== "OE") {
					$('#csrQepEventNameDiv').hide();
					$('#csrQepEventDateDiv').hide();
				}
		}else {
			saveIncarceratedDetailDB();
		}
		return;
	}

	function isMailingAddressNotSameAsHousehold(){
		if(	householdMemberObj.householdContact.mailingAddress.streetAddress1 != $('#mailingAddressLine1').val() ||
			householdMemberObj.householdContact.mailingAddress.streetAddress2 != $('#mailingAddressLine2').val() ||
			householdMemberObj.householdContact.mailingAddress.city != $('#mailingAddressCity').val() ||
			householdMemberObj.householdContact.mailingAddress.state != $('#mailingAddressState').val() ||
			householdMemberObj.householdContact.mailingAddress.county != $('#mailingAddressCountyCode').val() ||
			householdMemberObj.householdContact.mailingAddress.postalCode != $('#mailingAddressZip').val()
		 ){
			$('#isMailingAddressUpdated').val("yes");
			return true;
		}
		$('#isMailingAddressUpdated').val("no");
		return false;
	}
	
	function saveIncarceratedDetailDB()
	{
		/*pageValidation();*/
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}
		if("Y" == $("#trackApplicationSource").val()){
			if($("#applicationType").val() == "QEP" ){
				if(!validateEvent($('#sepEventCSR'))) return false;
				if(!validateEventDate($('#sepEventDateCSR'))) return false;
			}

			if(!($('#optCSRApplicationTypeMail').is(':checked') || $('#optCSRApplicationTypePhone').is(':checked'))){
				return false;
			}

			sepEvent = $('#sepEventCSR').val();
			sepEventDate = $('#sepEventDateCSR').val();
		}
		var appendRowData='';
		noOfHouseHold = getNoOfHouseHolds();
		webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = false;
		for (i = 1; i <= noOfHouseHold; i++) {
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			householdMemberObj.incarcerationStatus.incarcerationStatusIndicator = false;
			householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator = false;

			if(document.getElementById("ononeIncarceratedStatus").checked == false){
				if(document.getElementById("incarcerationCB"+i).checked == true){
					householdMemberObj.incarcerationStatus.incarcerationStatusIndicator = true;
					//householdMemberObj.applyingForCoverageIndicator = false;
					webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = true;

					if($('input[name=disposition'+i+']:radio:checked').val() == 'yes'){
						householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator = true;
					}else{
						householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator =false;
					}
				}
			}

			householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].householdContact.homeAddress.primaryAddressCountyFipsCode;
		}
		//agreement
		var parimaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
		if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'yes'){
			webJson.singleStreamlinedApplication.consentAgreement = true;
			webJson.singleStreamlinedApplication.numberOfYearsAgreed = "";
		}else if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'no'){
			webJson.singleStreamlinedApplication.consentAgreement = false;
			webJson.singleStreamlinedApplication.numberOfYearsAgreed = $('input[name=taxReturnPeriod]:radio:checked').val();
		}
		for(var i=1; i<=noOfHouseHold; i++)	{
			if($("#appscr85incarcerationTD"+i).html()!= undefined)
			{
				updateIncarceratedDetail(i);
				continue;
			}

			if(document.getElementById("incarcerationCB"+i) != null && document.getElementById("incarcerationCB"+i).checked == true)
				appendRowData="<td  class='incarcerationIndicator' id='appscr85incarcerationTD"+i+"'>yes</td>";
			else
				appendRowData="<td class='incarcerationIndicator' id='appscr85incarcerationTD"+i+"'>no</td>";

			appendRowData+="<td class='incarcerationPendingDispositionIndicator' id='appscr85disposition"+i+"'>"+$('input[name=disposition'+i+']:radio:checked').val()+"</td>";
			 $('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendRowData);
			 appendRowData='';
		}

		 webJson.singleStreamlinedApplication.applicationSignatureDate = getCurrentDateTime();

		//Atleast 1 person should be seeking coverage to continue the application
		 if(coverageMinimumCheckWithUnder26Indicator() == false){
			$('#coverageMinimumAlert').modal();
			return;
		 }else{
	 		var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	 		for(var i = 0; i < householdMemberArr.length; i++){

	 			if(householdMemberArr[i].applyingForCoverageIndicator == true && householdMemberArr[i].under26Indicator == true){

	 				householdMemberArr[i].applyingForCoverageIndicator = true;
	 			}
	 			else{

	 				householdMemberArr[i].applyingForCoverageIndicator = false;
	 			}

	 		}
		}
		updateMailingAddressSameAsHomeAddressIndicator();
		 if("Y" != $("#trackApplicationSource").val()){
			 //$('#eventModal').modal();
			 if($("#applicationType").val()== "QEP") {
		          $('#eventModal').modal();
		      } else {
		    	  showMailingUpdateConfirmation(); //suneel 05092014
				     //next();
				 }
		 } else {
			 showMailingUpdateConfirmation(); //suneel 05092014
		     //next();
		 }
	}
	var sepEvent="";
	var sepEventDate="";
	function saveSSAPEvent(){
		/*
		if(parsleyPageValidation($("#pageSSSAPEvent"+).find("form").attr("id")) == false){
			return;
		}*/
		if(!validateEvent($('#sepEvent'))) return false;
		if(!validateEventDate($('#sepEventDate'))) return false;

		sepEvent = $('#sepEvent').val();
		sepEventDate = $('#sepEventDate').val();
		$('#eventModal').modal('hide');
		showMailingUpdateConfirmation(); //suneel 05092014
	    //next(); // call after successfully saved.
	}

	function showMailingUpdateConfirmation(){
		//show popup for address change 
		if(isMailingAddressNotSameAsHousehold()){
			//show popup
			$('#modalCSRApplicationType').modal('hide');
			$('#modalMailingAddressNotSameAsHousehold').modal();
			return; 
		}
		saveDataSubmit();
		
	}
	
	function addQuestionSummery()
	{
		noOfHouseHold = parseInt($('#numberOfHouseHold').text());
		if (currentPage == 'appscr81B' && addQuetionsHouseHoldDone < noOfHouseHold) {
			addQuetionsHouseHoldDone++;
			appscr75AEditController(addQuetionsHouseHoldDone);
		}else{
			next();
		}
		document.getElementById('appscr82addQuestionSummery').innerHTML = '';
		var i;
		var strOuter = "";

		for (i = 1; i <= noOfHouseHold; i++) {
			//var houseHoldName =$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var houseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

			var str ='<div class="control-group"><label class="pull-left"><strong>'+houseHoldName+'</strong>&nbsp;</label>';
			if(i==1){
				str+= jQuery.i18n.prop("ssap.primaryContact")+'&nbsp;';
			}

			str+='<input type="button" value="Edit" class="btn btn-primary" onclick="appscr75AEditController('+i+')"/></div>';
			/*
			+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.currentlyEnrolled")+'</label><div class="controls margin5-t">'
			+jQuery.i18n.prop("ssap.naYesNot")+'</div></div>'
			+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.hasOptionToEnroll")+'</label><div class="controls margin5-t">'
			+jQuery.i18n.prop("ssap.naEnrollmentOptions")+'</div></div>'; */

			if(i != noOfHouseHold)
				str+="<hr />";
				strOuter+= str;
		}
		document.getElementById('appscr82addQuestionSummery').innerHTML = strOuter;
		//next();
	}

	function getTotalDeductions(houseHoldID)
	{
		totalDeduction= 0;
		return totalDeduction;
	}

	function showIndividualDiv()
	{
		editNonFinacial = false;
		editFlag = false;
		//$('#contBtn').text('Update');
		$('#countinue_button_div').removeClass("Continue_button");
		$('#countinue_button_div').addClass("ContinueForUpdate_button");
		$('#cancelButtonCancel').show();
		$('#back_button_div').hide();

		prev();
		var i= getNoOfHouseHolds();
		noOfHouseHold++;
		$('#numberOfHouseHold').text(noOfHouseHold);
		$('#noOfPeople53').val(noOfHouseHold);
		addAnotherIndividual((i+1));
		addMaskingOnAppscr57();
	}

	//Keys
	function addAnotherIndividual(i)
	{
		$('#cancelButtonCancel').show();
		$('#back_button_div').hide();
		cancleFlag = false;
		if($('#appscr57FirstName'+i).html() != undefined)
		{
			$('#addAnotherHouseHold'+noOfHouseHold).show();
			return false;
		}


		var str='<div id="addAnotherHouseHold'+i+'">'
		+'<h4><strong id="applicantTitle57'+i+'" class = "camelCaseName">'+jQuery.i18n.prop("ssap.applicant")+'&nbsp;'+i+'</strong></h4>'
		+'<div class="margin5-b" id="duplicatedNameErrorDiv'+i+'"></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop('ssap.firstName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<input id="appscr57FirstName'+i+'"type="text" onkeyup="removeValidationFirstName('+i+'); setName('+i+')"'
		+ 'onclick="removeValidationFirstName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.firstName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.firstNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.firstNameRequired")+'" />'
		+'</div></div>'
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.middleName")+'</label><div class="controls">'
		+'<input id="appscr57MiddleName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.middleNameValid")+'"'
		+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.middleName")+'" class="input-medium characterStroke">'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.lastName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<input id="appscr57LastName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')"'
		+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.lastName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z]))+((\\s|-)?([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z])))*)$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" />'
		+'</div></div>'
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.suffix")+'</label><div class="controls">'
		+'<select id="appscr57Suffix'+i+'" class="input-medium">'
		+'<option value="">'+jQuery.i18n.prop("ssap.suffix")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.sr")+'">'+jQuery.i18n.prop("ssap.sr")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.jr")+'">'+jQuery.i18n.prop("ssap.jr")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.III")+'">'+jQuery.i18n.prop("ssap.III")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.IV")+'">'+jQuery.i18n.prop("ssap.IV")+'</option>'
		+'</select></div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.dateOfBirth")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<div class="input-append date date-picker" id="calDiv'+i+'" data-date="">'
		+'<input type="text" onkeyup="removeValidationDob('+i+')" onclick="removeValidationDob('+i+')" id="appscr57DOB'+i+'" placeholder="'+jQuery.i18n.prop("ssap.mmDDYYYY")+'"'
		+'class="input-medium dob" '
		+'onfocusout = "validateBirthDate($(this))" '
		+'onblur = "validateBirthDate($(this))" '
		+'data-parsley-trigger="change" data-parsley-errors-container="#dateOfBirthErrorDiv'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.dobRequired")+'" '
		+'/><span class="add-on"><i class="icon-calendar"></i></span></div>'
		+'<div id="dateOfBirthErrorDiv'+i+'"></div>'
		+'</div></div>'
		/*hix-33010*/
		/*+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.dateOfBirthVerified")+'</label><div class="controls">'
		+'<select id="appscr57DOBVerificationStatus'+i+'" class="input-medium">'
		+'<option value ="yes">'+jQuery.i18n.prop("ssap.yes")+'</option>'
		+'<option value ="no">'+jQuery.i18n.prop("ssap.no")+'</option>'
		+'</select></div></div>'*/
		+'<div class="control-group hide">'
		+'<p>'+jQuery.i18n.prop("ssap.has")+'&nbsp;<strong id="applicantName57'+i+'"></strong>&nbsp;'+jQuery.i18n.prop("ssap.usedTobaccoProducts")+'<img width="10" height="10"  src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>'
		+'<div class="controls">'
		+'<label>'
		+'<input type="radio" name="appscr57Tobacco'+ i+ '" value="yes" id="appscr57SexYes'+ i+ '" >'+jQuery.i18n.prop("ssap.yes")+'</label>'
		+'<label>'
		+'<input type="radio" name="appscr57Tobacco'+ i+ '" value="no" id="appscr57SexNo'+ i+ '">'+jQuery.i18n.prop("ssap.no")+'</label>'
		+'<span class="errorMessage"></span>'
		+'</div>'
		+'</div>'

		/*+'<div class="control-group hide"><label class="control-label">'+jQuery.i18n.prop('ssap.sex')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></label><div class="controls">'
		+'<div  class="isSex"><label><input type="radio" name="appscr57Sex'+i+'" value="male" id="appscr57SexMale'+i+'">'+jQuery.i18n.prop("ssap.male")+'</label>'
		+'<label><input type="radio" name="appscr57Sex'+i+'"value="female" id="appscr57SexFemale'+i+'">'+jQuery.i18n.prop("ssap.female")+'</label>'
		+'</div></div></div>'*/
		/*
		+'<div class="control-group margin20-t"><label class="control-label">Relationship to <strong id="appscr57relationshiplbl'+i+'"></strong> (Primary Contact)</label>'
		+'<div class="controls">'
		+'<select id="appscr57relationship'+i+'" name="appscr57relationship'+i+'" class="input-large">'
		+'<option value="0">Select</option>'
		+'<option value="Wife">Wife</option>'
		+'<option value="Child">Child</option>'
		+'<option value="Parent">Parent</option>'
		+'<option value="Caretaker">Caretaker</option>'
		+'</select>'
		+'<div class="help-inline" id="appscr57relationship_error"></div></div></div>'
		*/
		+'<div class="control-group margin20-t"><label class="controls capitalize-none"><input type="checkbox" name="appscr57checkseekingcoverage'+i+'" id="appscr57checkseekingcoverage'+i+'">Are you seeking coverage?</label></div>';


	var existing = $('#appscr57HouseholdForm').html();
	/*var strComplete = existing + str;*/
	$('#appscr57HouseholdForm').append(str);
	document.getElementById('numberOfHouseHold').innerHTML = i;

	for(var j=1;j<=i;j++){
		 createCalendar(j);
		 /*$('.characterStroke').ForceCharacterOnly();*/
	}

	//removeRequiredValidation();

	}

	function setName(i)
	{
		var applicantName = getNameByPersonId(i);
		if( applicantName == ""){
			if(($('#appscr57FirstName' + i).val() !="") || ($('#appscr57MiddleName' + i).val() !="") || ($('#appscr57LastName' + i).val() )!=""){
				applicantName = $('#appscr57FirstName' + i).val()+" "+ $('#appscr57MiddleName' + i).val()+" "+ $('#appscr57LastName' + i).val();
			}
		}
		$('#applicantName57'+i).text(applicantName);
		$('#applicantTitle57'+i).text(applicantName);
		$('#appscr57relationshiplbl'+i).text(applicantName);
	}

	function setHisOrHerAfterContinue(){


		$('#appscr61SSNHisHer').text('');
		$('#appscr62Part2SSNHisHer').text('');
		$('#appscr63ChildHeShe').text('');
		if($('input[name=appscr61_gender]:radio:checked').val()== 'male' || $('input[name=gender]:radio:checked').val()== 'male'){
			$('#appscr61SSNHisHer').text('his');
			$('#appscr62Part2SSNHisHer').text('his');
			$('#appscr63ChildHeShe').text('he');
			$('#appscr61HimOrHer').text('him');
		}
		else{
			$('#appscr61SSNHisHer').text('her');
			$('#appscr62Part2SSNHisHer').text('her');
			$('#appscr63ChildHeShe').text('she');
			$('#appscr61HimOrHer').text('her');

		}
	}

	function addChildRelatedDetails(i,dateOfBirth)
	{
		var childData="";

		childData+="<td id='appscr57IsChildTD" + i + "' class='isChild'>";
		if(checkBirthDate(dateOfBirth)<(365*18))
			childData+="yes";
		else
			childData+="no";

		childData+= "</td>";


		childData+="<td id='appscr57IsInfantTD" + i + "' class='isInfant'>";
		if(checkBirthDate(dateOfBirth)<365)
			childData+="yes";
		else
			childData+="no";

		childData+="</td>";


		return childData;
	}

	function updateInfantRelatedDetails(i,dateOfBirth)
	{
		return "no"; // we do no consider infant, HIX-42563
		if(checkBirthDate(dateOfBirth)<365)
			return "yes";
		else
			return "no";
	}

	function updateChildRelatedDetails(i,dateOfBirth)
	{
		if(checkBirthDate(dateOfBirth)<(365*18))
			return "yes";
		else
			return "no";
	}



	function getInfantData(tdIndex)
	{
		return "no"; // we do no consider infant, HIX-42563
		var isInfant = $('#appscr57IsInfantTD'+(tdIndex)).html();
		if(isInfant!='undefined')
			return isInfant;
		else
			return no;
	}

	var HoldContactDetailsArray = new Array();
	HoldContactDetailsArray[0] = 'firstName';
	HoldContactDetailsArray[1]= 'middleName';
	HoldContactDetailsArray[2] = 'lastName';
	HoldContactDetailsArray[3] = 'suffix';
	HoldContactDetailsArray[4] = 'dateOfBirth';
	HoldContactDetailsArray[5] = 'emailAddress';
	HoldContactDetailsArray[6] = 'home_addressLine1';
	HoldContactDetailsArray[7] = 'home_addressLine2';
	HoldContactDetailsArray[8] = 'home_primary_city';
	HoldContactDetailsArray[9] = 'home_primary_zip';
	HoldContactDetailsArray[10] = 'home_primary_state';
	HoldContactDetailsArray[11] = 'home_primary_county';
	HoldContactDetailsArray[12] = 'mailing_addressLine1';
	HoldContactDetailsArray[13] = 'mailing_addressLine2';
	HoldContactDetailsArray[14] = 'mailing_primary_city';
	HoldContactDetailsArray[15] = 'mailing_primary_zip';
	HoldContactDetailsArray[16] = 'mailing_primary_county';
	HoldContactDetailsArray[17] = 'first_phoneNo';
	HoldContactDetailsArray[18] = 'first_ext';
	HoldContactDetailsArray[19] = 'first_phoneType';
	HoldContactDetailsArray[20] = 'second_phoneNo';
	HoldContactDetailsArray[21] = 'second_ext';
	HoldContactDetailsArray[22] = 'second_phoneType';
	HoldContactDetailsArray[23] = 'preffegred_spoken_language';
	HoldContactDetailsArray[24] = 'preffered_written_language';

	function createCalendar(i){
		$('#calDiv'+i).on('click',function() {
			$('#calDiv'+i).each(function() {
				$(this).datepicker({
				    autoclose: true,
				    dateFormat: 'MM/DD/YYYY'
				}).on('changeDate', function(){
					$('.dob').trigger('blur');
				});

			});
		});
		$('#calDiv'+i).each(function() {
			$(this).datepicker({
			    autoclose: true,
			    dateFormat: 'MM/DD/YYYY'
			}).on('changeDate', function(){
				$('.dob').trigger('blur');
			});
		});
	}

	function validateBirthDate(memberDob){
		var strDate = memberDob.val();
		var mon = parseInt(strDate.substring(0, 2), 10);
		var dt = parseInt(strDate.substring(3, 5), 10);
		var yr = parseInt(strDate.substring(6, 10), 10);
		var validDate =/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/;
		var today = new Date();
		var today_mon = today.getMonth()+1;
		var today_dt = today.getDate();
		var today_yr = today.getFullYear();
		if(memberDob.closest('div.controls').find('.dobCustomError').length == 0){
			memberDob.closest('div.controls').append('<div class ="dobCustomError"></div>');
		};

		var dobCustomError = memberDob.closest('div.controls').find('.dobCustomError');
		if(validDate.test(strDate)){
		if (yr > today_yr) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
			dateValidation = false;
		} else if ((yr == today_yr) && mon > today_mon) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
			dateValidation = false;
		} else if ((yr == today_yr) && (mon == today_mon) && (dt > today_dt)) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
			dateValidation = false;
		} else if ((today_yr - yr) >= 104) {
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date ,Age Is More Than 104.</span>');
			dateValidation = false;
		}else{
			dobCustomError.html('');
			dateValidation = true;
		}
		} else{
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
			dateValidation = false;

		}
		//alert("I am here");
	}

	function validateEventDate(eventDate){
		var strDate = eventDate.val();
		var mon = parseInt(strDate.substring(0, 2), 10);
		var dt = parseInt(strDate.substring(3, 5), 10);
		var yr = parseInt(strDate.substring(6, 10), 10);
		var validDate =/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/;
		var today = new Date();
		var today_mon = today.getMonth()+1;
		var today_dt = today.getDate();
		var today_yr = today.getFullYear();
		if(eventDate.closest('div.controls').find('.dobCustomError').length == 0){
			eventDate.closest('div.controls').append('<div class ="dobCustomError"></div>');
		};

		var dobCustomError = eventDate.closest('div.controls').find('.dobCustomError');
		var sepEvent = ''
		if("Y" == $("#trackApplicationSource").val()){
			sepEvent = $('#sepEventCSR').val();
		}else{
			sepEvent = $('#sepEvent').val();
		}

		if(validDate.test(strDate)){
			var d = new Date(yr,mon-1,dt);
			if(d && (d.getMonth() + 1) != mon){
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
				dateValidation = false;
			}else if (yr > today_yr) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">'+jQuery.i18n.prop("ssap.page35.qep.futureDateInvalidMsg")+'</span>');
				dateValidation = false;
			} else if ((yr == today_yr) && mon > today_mon) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">'+jQuery.i18n.prop("ssap.page35.qep.futureDateInvalidMsg")+'</span>');
				dateValidation = false;
			} else if ((yr == today_yr) && (mon == today_mon) && (dt > today_dt)) {
				dobCustomError.html('');
				dobCustomError.html('<span class="validation-error-text-container">'+jQuery.i18n.prop("ssap.page35.qep.futureDateInvalidMsg")+'</span>');
				dateValidation = false;
			} else{
				dobCustomError.html('');
				dateValidation = true;
			}
		}
		else{
			dobCustomError.html('');
			dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
			dateValidation = false;

		}


		return dateValidation;
	}

	function validateEvent(qepEvent){
		if(qepEvent.closest('div.controls').find('.dobCustomError').length == 0){
			qepEvent.closest('div.controls').append('<div class ="dobCustomError"></div>');

		};

		var dobCustomError = qepEvent.closest('div.controls').find('.dobCustomError');
		dobCustomError.html('');

		if( $.trim(qepEvent.val()) == ""){
			dobCustomError.html('<span class="validation-error-text-container">Please select event</span>');
			return false;
		}

		return true;
	}

/*
****************************************************************************************************************
* bloodRelationship.js                                                                                        *
****************************************************************************************************************
*/


	var noOfRelation = 0;



	function indivOnChange(){

		$('#appscrBloodRelIndivRelatedTolabel').html($('#appscrBloodRelIndivRelatedTo option:selected').text());
		$('#appscrBloodRelIndivLabel').html($('#appscrBloodRelIndiv option:selected').text());

	}

	function clear(){

		$("#relat").val(-1);
		$("#appscrBloodRelIndivRelatedTo").val(-1);
		$("#appscrBloodRelIndiv").val(-1);
		indivOnChange();
	}


	function  addButtonError(){

		$('#addButton').addClass('colorRed');
		var t = setTimeout("$('#addButton').removeClass('colorRed');", 1000);


	}



	function addRelationship(){

		var indiv = $('#appscrBloodRelIndiv option:selected').val().split("_")[0];
		var individ = $('#appscrBloodRelIndiv option:selected').val().split("_")[1];
		var indivTagrget  = $('#appscrBloodRelIndivRelatedTo option:selected').val().split("_")[0];
		var indivTagrgetid  = $('#appscrBloodRelIndivRelatedTo option:selected').val().split("_")[1];
		var relation  = $('#relat option:selected').text();
		var relationCode  = $('#relat').val();
		/*var isTaxDependent = $('input[name=taxDependent]:radio:checked').val();*/


		if($("#relat").val() == -1){
			document.getElementById("relat").focus();
			addButtonError();
			return;
		}
		if($("#appscrBloodRelIndivRelatedTo").val() == -1){
			document.getElementById("appscrBloodRelIndivRelatedTo").focus();
			addButtonError();
			return;
		}
		if($("#appscrBloodRelIndiv").val() == -1){
			document.getElementById("appscrBloodRelIndiv").focus();
			addButtonError();
			return;
		}


		var str = 	'<tr>' +
					'<td class="Indiv doc">'+indiv+'</td>' +
					'<td class="Individ hide">'+individ+'</td>' +
					'<td class="rel doc">'+indivTagrget+'</td>'+
					'<td class="relid hide">'+indivTagrgetid+'</td>'+
					'<td class="relt hide">'+relationCode+'</td>'+
					'<td class="relt1Text doc">' +relation+'</td>'+
					'<td class="relat hide">No</td>'+
					'<td class="pct hide">No</td>'+
					/*'<td class="tax doc">'+isTaxDependent+'</td>'+*/
					'<td class="doc" onclick="removeTR($(this));"><i class="icon-remove"></i></td>'+
					'</tr>';

		$('#appscrBloodRelTableBody').append(str);
		noOfRelation++;
		clear();
		check();
	}

	function removeTR(id){
		id.parent().remove();
		noOfRelation--;
		check();
	}

	//Keys
	function check(){
		if(noOfRelation <= 0 ){
			$('#bloodRelationshipslabel').text(jQuery.i18n.prop('ssap.noRelationshipAdded'));
			$('#appscrBloodRelTableContainer').hide();
		}
		else{
			$('#bloodRelationshipslabel').text(jQuery.i18n.prop('ssap.bloodRelationship'));
			$('#appscrBloodRelTableContainer').show();
		}
	}

	function populateDataToRelations()
	{
	/* $('#appscrBloodRelIndiv').html('');

	 $('#appscrBloodRelIndivRelatedTo').html('');


	 $('#appscrBloodRelIndiv').append(new Option('Select','-1') );

	 $('#appscrBloodRelIndivRelatedTo').append(new Option('Select','-1'));


	 	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
			var pureNameOfUser=householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			$('#appscrBloodRelIndiv').append(new Option(pureNameOfUser,pureNameOfUser+"_"+cnt) );
		    $('#appscrBloodRelIndivRelatedTo').append(new Option(pureNameOfUser,pureNameOfUser+"_"+cnt));

		}

	 populateChechBox();*/


		var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;

		for(var i=0;i<parimaryContactRelationArr.length;i++){
			var individualPersonId = parimaryContactRelationArr[i].individualPersonId;
			var relatedPersonId = parimaryContactRelationArr[i].relatedPersonId;
			var relation = parimaryContactRelationArr[i].relation;

			var reversedRelation = '';

			if(parseInt(individualPersonId) < parseInt(relatedPersonId)){
				if(relation == '03' || relation == '15'){	//multiple relationship

					//get reversed relation
					for(var j=0; j<parimaryContactRelationArr.length; j++){
						if(parimaryContactRelationArr[j].relatedPersonId == individualPersonId && parimaryContactRelationArr[j].individualPersonId == relatedPersonId){
							reversedRelation = parimaryContactRelationArr[j].relation;
						}
					}

					//get relation based on its reversed relation
					if(reversedRelation == '09'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('03a');
					}else if(reversedRelation == '10'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('03f');
					}else if(reversedRelation == '19'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('03');
					}else if(reversedRelation == '26'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('15');
					}else if(reversedRelation == '31'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('15c');
					}else if(reversedRelation == '15'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('03w');
					}else if(reversedRelation == '03'){
						$('#'+ individualPersonId + 'to' + relatedPersonId).val('15p');
					}



				}else{	//one-one relationship
					$('#'+ individualPersonId + 'to' + relatedPersonId).val(parimaryContactRelationArr[i].relation);
				}

				//show this and next section
				var nextRelationshipSelection = $('#relationshipSelection' + (parseInt(individualPersonId)+1));
				if(nextRelationshipSelection.find('select').length > 0){
					nextRelationshipSelection.show();
				}

			}
		}



		/*var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			var householdMemRelArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1].bloodRelationship;
			var showRelationshipSelection = false;

			for(var j=0;j<householdMemRelArr.length;j++){
				if(householdMemRelArr[j].individualPersonId < householdMemRelArr[j].relatedPersonId){

					var relation = '';
					householdMemMulRelArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemRelArr[j].relatedPersonId-1].bloodRelationship;
					for(var k=0; k<householdMemMulRelArr.length; k++){
						if(householdMemMulRelArr[k].relatedPersonId == i){
							relation = householdMemMulRelArr[k].relation;
						}
					}

					if(householdMemRelArr[j].relation == '03'){
						if(relation == '09'){
							$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03a');
						}else if(relation == '10'){
							$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03f');
						}else if(relation == '19'){
							$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('03');
						}

					}else if(householdMemRelArr[j].relation == '15'){
						if(relation == '26'){
							$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('15');
						}else if(relation == '31'){
							$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val('15c');
						}
					}else{
						$('#'+ householdMemRelArr[j].individualPersonId + 'to' + householdMemRelArr[j].relatedPersonId).val(householdMemRelArr[j].relation);
					}


					showRelationshipSelection = true;
				}
			}

			//show next relationship section
			var nextSelection = $('#relationshipSelection' + (i+1));
			if(showRelationshipSelection && nextSelection.find('select').length > 0 ){
				nextSelection.show();
			}

			for ( var j = i + 1; j <= noOfHouseHoldmember; j++ ) {
				$('#'+ i + 'to' + j).val(householdMemberObj.bloodRelationship[i-1].relation);
			}

			for ( var j = 0; j < noOfHouseHoldmember; j++ ) {
				$('#'+ i + 'to' + j).val(householdMemberObj.bloodRelationship[j].relation);
			}
		}*/






		//hide and show relationship
		/*for ( var i = 1; i <= noOfHouseHoldmember - 1; i++) {
			for ( var j = i + 1; j <= noOfHouseHoldmember; j++) {
				if($('#'+ i + 'to' + j).val() !== undefined){
					$('#relationshipSelection' + i).show();
				}
			}
		}*/




	}

	function populateRelationship() {
		/*$('#appscrBloodRelTableBody').html('');
		var objBloodRelation = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
		noOfRelation = objBloodRelation.length;
		for ( var cnt = 1; cnt <= objBloodRelation.length; cnt++) {
			var relationships = objBloodRelation[cnt-1];


			var indiv = getNameByPersonId(relationships.individualPersonId);
			var individ = relationships.individualPersonId;
			var indivTagrget  = getNameByPersonId(relationships.relatedPersonId);
			var indivTagrgetid  = relationships.relatedPersonId;
			var relation  = getBloodRelation(relationships.relation);
			var relationCode  = relationships.relation;
			var isTaxDependent = relationships.textDependency == true ? 'Yes' : 'No';


			if((individ != "" && individ != null) && (indivTagrgetid != "" && indivTagrgetid != null)){

			var str = 	'<tr>' +
						'<td class="Indiv doc">'+indiv+'</td>' +
						'<td class="Individ hide">'+individ+'</td>' +
						'<td class="rel doc">'+indivTagrget+'</td>'+
						'<td class="relid hide">'+indivTagrgetid+'</td>'+
						'<td class="relt hide">'+relationCode+'</td>'+
						'<td class="relt1Text doc">' +relation+'</td>'+
						'<td class="relat hide">No</td>'+
						'<td class="pct hide">No</td>'+
						'<td class="tax doc">'+isTaxDependent+'</td>'+
						'<td class="doc" onclick="removeTR($(this));"><i class="icon-remove"></i></td>'+
						'</tr>';

			$('#appscrBloodRelTableBody').append(str);
			$('#appscrBloodRelTableContainer').show();

			}

		}*/
		var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		//Update number in list
		$('.householdMemberCount').text(noOfHouseHoldmember);

		//update list
		$('#relationshipMembersList').html('');
		var relationshipMembersList = '';
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var householdMemberObjFullName = householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;

			relationshipMembersList += '<li> <span class="camelCaseName">' + householdMemberObjFullName + '</span></li>';
		}

		$('#relationshipMembersList').append(relationshipMembersList);

		var relationshipOptions = "";
		//update relationship
		if($("#exchangeName").val() != "NMHIX"){
			relationshipOptions =
			'<option value="">'+jQuery.i18n.prop("ssap.page13.selectRelationship")+'</option>'+
			'<option value="01">'+jQuery.i18n.prop("ssap.page13.spouse")+'</option>'+
			'<option value="03">'+jQuery.i18n.prop("ssap.page13.parentOfChild")+'</option>'+
			'<option value="03a">'+jQuery.i18n.prop("ssap.page13.parentOfAdoptedChild")+'</option>'+
			'<option value="03w">'+jQuery.i18n.prop("ssap.page13.parentCaretakerOfWard")+'</option>'+
			'<option value="09">'+jQuery.i18n.prop("ssap.page13.adoptedChild")+'</option>'+
			'<option value="15c">'+jQuery.i18n.prop("ssap.page13.wardOfCGuardian")+'</option>'+
			'<option value="15p">'+jQuery.i18n.prop("ssap.page13.wardOfParentCaretaker")+'</option>'+
			'<option value="16">'+jQuery.i18n.prop("ssap.page13.stepParent")+'</option>'+
			'<option value="17">'+jQuery.i18n.prop("ssap.page13.stepChild")+'</option>'+
			'<option value="19">'+jQuery.i18n.prop("ssap.page13.childSonDaughter")+'</option>'+
			'<option value="31">'+jQuery.i18n.prop("ssap.page13.courtAppointedGuardian")+'</option>';
		}else{
			relationshipOptions =
			'<option value="">'+jQuery.i18n.prop("ssap.page13.selectRelationship")+'</option>'+
			'<option value="01">'+jQuery.i18n.prop("ssap.page13.spouse")+'</option>'+
			'<option value="03">'+jQuery.i18n.prop("ssap.page13.parentOfChild")+'</option>'+
			'<option value="03a">'+jQuery.i18n.prop("ssap.page13.parentOfAdoptedChild")+'</option>'+
			'<option value="03f">'+jQuery.i18n.prop("ssap.page13.parentOfFosterChild")+'</option>'+
			'<option value="04">'+jQuery.i18n.prop("ssap.page13.grandParent")+'</option>'+
			'<option value="05">'+jQuery.i18n.prop("ssap.page13.grandChild")+'</option>'+
			'<option value="06">'+jQuery.i18n.prop("ssap.page13.uncleAunt")+'</option>'+
			'<option value="07">'+jQuery.i18n.prop("ssap.page13.nephewNiece")+'</option>'+
			'<option value="08">'+jQuery.i18n.prop("ssap.page13.firstCousin")+'</option>'+
			'<option value="09">'+jQuery.i18n.prop("ssap.page13.adoptedChild")+'</option>'+
			'<option value="10">'+jQuery.i18n.prop("ssap.page13.fosterChild")+'</option>'+
			'<option value="11">'+jQuery.i18n.prop("ssap.page13.sonDaughterInLaw")+'</option>'+
			'<option value="12">'+jQuery.i18n.prop("ssap.page13.brotherSisterInLaw")+'</option>'+
			'<option value="13">'+jQuery.i18n.prop("ssap.page13.motherFatherInLaw")+'</option>'+
			'<option value="14">'+jQuery.i18n.prop("ssap.page13.sibling")+'</option>'+
			'<option value="15">'+jQuery.i18n.prop("ssap.page13.wardOfGuardian")+'</option>'+
			'<option value="15c">'+jQuery.i18n.prop("ssap.page13.wardOfCGuardian")+'</option>'+
			'<option value="16">'+jQuery.i18n.prop("ssap.page13.stepParent")+'</option>'+
			'<option value="17">'+jQuery.i18n.prop("ssap.page13.stepChild")+'</option>'+
			'<option value="19">'+jQuery.i18n.prop("ssap.page13.childSonDaughter")+'</option>'+
			'<option value="25">'+jQuery.i18n.prop("ssap.page13.formerSpouse")+'</option>'+
			'<option value="26">'+jQuery.i18n.prop("ssap.page13.guardian")+'</option>'+
			'<option value="31">'+jQuery.i18n.prop("ssap.page13.courtAppointedGuardian")+'</option>'+
			'<option value="53">'+jQuery.i18n.prop("ssap.page13.domesticPartner")+'</option>'+
			'<option value="G8">'+jQuery.i18n.prop("ssap.page13.unspecifiedRelationship")+'</option>'+
			'<option value="G9">'+jQuery.i18n.prop("ssap.page13.unspecifiedRelative")+'</option>'+
			'<option value="03-53">'+jQuery.i18n.prop("ssap.page13.parentDomesticPartner")+'</option>'+
			'<option value="53-19">'+jQuery.i18n.prop("ssap.page13.childOfDomesticPartner")+'</option>';
		}


		$('#relationshipSelection').html('');
		var relationshipSelection = '';
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var householdMemberObjFullName = householdMemberObj.name.firstName + ' ' + householdMemberObj.name.middleName + ' ' + householdMemberObj.name.lastName;

			if(i == 1){
				relationshipSelection += '<li id=relationshipSelection' + i + '>How is <strong><span class="camelCaseName">' + householdMemberObjFullName + '</span></strong> related to the other household members?';
			}else{
				relationshipSelection += '<li id=relationshipSelection' + i + ' class="hide">How is <strong><span class="camelCaseName">' + householdMemberObjFullName + '</span></strong> related to the other household members?';
			}

			relationshipSelection += '<div class="gutter10">';

			for ( var j = i + 1; j <= noOfHouseHoldmember; j++) {
				var householdMemberRelatetionshipObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j-1];
				var householdMemberRelatetionshipObjFullName = householdMemberRelatetionshipObj.name.firstName + " " + householdMemberRelatetionshipObj.name.middleName + " " + householdMemberRelatetionshipObj.name.lastName;

				relationshipSelection += '<div class="control-group">';
				relationshipSelection += '<label class="control-label capitalize-none"><span class="camelCaseName">'+ householdMemberObjFullName +'</span> is <span class="camelCaseName">' + householdMemberRelatetionshipObjFullName + '</span>\'s </label>';
				relationshipSelection += '<div class="controls">';
				var iPersonId = householdMemberObj.personId;
				var jPersonId = householdMemberRelatetionshipObj.personId;
				if(i != 1 && $("#exchangeName").val() != "NMHIX"){
					relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + iPersonId + 'to' + jPersonId + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '<option value="14">'+ jQuery.i18n.prop('ssap.page13.sibling') +'</option></select>';
				}else{
					//relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + i + 'to' + j + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '</select>';
					relationshipSelection += '<select onchange="showNextSelection($(this));" id="' + iPersonId + 'to' + jPersonId + '" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.relationshipRequired")+'">' + relationshipOptions + '</select>';
				}
				relationshipSelection += '</div></div>';
			}
			//checkMultipleMapping($(this),\''+i+'\',\''+j+'\',\''+householdMemberObjFullName+'\',\''+householdMemberRelatetionshipObjFullName+'\')
			//relationshipSelection += '<div class="multipleParent"></div><div class="multipleWard"></div>';

			relationshipSelection += '</div></li>';
		}

		$('#relationshipSelection').append(relationshipSelection);

	}



	function saveBloodRelation(){
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}

		var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;

		//before save, clear all relationship
		parimaryContactRelationArr = [];


		//save relationship data
		$('#relationshipSelection select').each(function(){
			//get head number as individual person ID
			var individualPersonId = $(this).attr("id").match(/^\d+/)[0];
			//get tail munber as related person ID
			var relatedPersonId = $(this).attr("id").match(/\d+$/)[0];
			//get relationship number
			var relationshipNum = $(this).val();

			//update relationship

			//handle 1 to many relationship, only for nm
			var relationshipNumJSON;
			if(relationshipNum == '03a' || relationshipNum == '03f' || relationshipNum == '03w'){
				relationshipNumJSON = '03';
			}else if(relationshipNum == '15c' || relationshipNum == '15p'){
				relationshipNumJSON = '15';
			}else{
				relationshipNumJSON = relationshipNum;
			}
			var relationshipObj = {
					"individualPersonId" : individualPersonId,
					"relatedPersonId" : relatedPersonId,
					"relation" : relationshipNumJSON
			};
			parimaryContactRelationArr.push(relationshipObj);

			//update related relationshiop
			var relatedRelationshipObj = {
					"individualPersonId" : relatedPersonId,
					"relatedPersonId" : individualPersonId,
					"relation" : getReveseredRel(relationshipNum)
			};
			parimaryContactRelationArr.push(relatedRelationshipObj);

		});


		//add 'self' to blood relationships
		for(var i=1; i <= webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length; i++){
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var selfRelationshipObj = {
					"individualPersonId" : householdMemberObj.personId.toString(),
					"relatedPersonId" : householdMemberObj.personId.toString(),
					"relation" : '18'
			};

			parimaryContactRelationArr.push(selfRelationshipObj);
		}

		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = parimaryContactRelationArr;


		//only for idaho
		if($("#exchangeName").val().trim() == "Your Health Idaho"){
			//var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
			var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;

			$('#over26DependentList').html('');

			for(var j=0;j<parimaryContactRelationArr.length;j++){

				var relatedMemberObj =findApplicantByPersonId(parimaryContactRelationArr[j].relatedPersonId); //householdMemberObj[parimaryContactRelationArr[j].relatedPersonId - 1];

				//only care relationship to PC
				if(parimaryContactRelationArr[j].individualPersonId == '1'){

					//No age check for PC, spouse and ward, so under26Indicator should be always true for them
					if(parimaryContactRelationArr[j].relation == '18' || parimaryContactRelationArr[j].relation == '01' || parimaryContactRelationArr[j].relation == '31'){
						relatedMemberObj.under26Indicator = true;
					//check parent/caretaker of ward
					}else if(parimaryContactRelationArr[j].relation == '03'){

						var eligibleForCoverage = false;
						for(var k=0;k<parimaryContactRelationArr.length;k++){
							var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);

							if(!ageGreaterThan26(dateOfBirth) || (parimaryContactRelationArr[k].relatedPersonId == '1' && parimaryContactRelationArr[k].individualPersonId == parimaryContactRelationArr[j].relatedPersonId && parimaryContactRelationArr[k].relation == '15')){
								eligibleForCoverage = true;
							}
						}


						if(eligibleForCoverage == false){
							$('#over26DependentList').append('<p>'+relatedMemberObj.name.firstName + ' ' +relatedMemberObj.name.middleName + ' ' +relatedMemberObj.name.lastName + '</p>');
						}

						relatedMemberObj.under26Indicator = eligibleForCoverage;

					}else{
						var dateOfBirth = UIDateformat(relatedMemberObj.dateOfBirth);
						// if they are 26 years or over, mark under26Indicator as false
						if(ageGreaterThan26(dateOfBirth)){

							$('#over26DependentList').append('<p>'+relatedMemberObj.name.firstName + ' ' +relatedMemberObj.name.middleName + ' ' +relatedMemberObj.name.lastName + '</p>');

							relatedMemberObj.under26Indicator = false;
						}
						else{
							relatedMemberObj.under26Indicator = true;
						}
					}
				}
			}
		}

	}



	function saveBloodRelationData(){
		$('#multiSpouse_error').hide();
		saveBloodRelation();

		var householdMemberCount = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		var spouseCountObj = {};

		for(var i = 1; i <= householdMemberCount; i++){
			spouseCountObj[i] = 0;
		}

		var bloodRelationshipArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
		for(var j = 0; j < bloodRelationshipArr.length; j++){
			if(bloodRelationshipArr[j].relation === '01'){
				spouseCountObj[bloodRelationshipArr[j].individualPersonId]++;
				spouseCountObj[bloodRelationshipArr[j].relatedPersonId]++;

				if(spouseCountObj[bloodRelationshipArr[j].individualPersonId] > 2 || spouseCountObj[bloodRelationshipArr[j].relatedPersonId] > 2){
					$('#multiSpouse_error').show();
					$("html, body").animate({ scrollTop: 0 }, "fast");
					return;
				}
			}
		}

		//only for idaho
		if($("#exchangeName").val().trim() == "Your Health Idaho"){
			if($('#over26DependentList p').length > 0){
				updatePrimaryContactName();
				$('#over26DependentAlert').modal();
				showHouseHoldContactSummary();
			}else{
				showHouseHoldContactSummary();
				next();
			}

		}else{
			showHouseHoldContactSummary();
			next();
		}
	}

	function getBloodRelation(relationShipCode){
		return $("#relat option[value="+relationShipCode+"]").text();
	}

	function updateSingleMemRel(){
		var parimaryContactRelationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
		//before save, clear all relationship
		parimaryContactRelationArr = [];

		//add 'self' to blood relationships
		var selfRelationshipObj = {
				"individualPersonId" : "1",
				"relatedPersonId" : "1",
				"relation" : '18'
		};

		parimaryContactRelationArr.push(selfRelationshipObj);


		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = parimaryContactRelationArr;

	}

	function getReveseredRel(rel){
		var reversedRel = '';

		switch(rel) {
			case '01':
		    	reversedRel = '01';
		        break;
			case '03':
		    	reversedRel = '19';
		        break;
			case '03a':
		    	reversedRel = '09';
		        break;
			case '03f':
		    	reversedRel = '10';
		        break;
			case '03w':
		    	reversedRel = '15';
		        break;
			case '04':
		    	reversedRel = '05';
		        break;
		    case '05':
		    	reversedRel = '04';
		        break;
		    case '06':
		    	reversedRel = '07';
		        break;
		    case '07':
		    	reversedRel = '06';
		        break;
		    case '08':
		    	reversedRel = '08';
		        break;
		    case '09':
		    	reversedRel = '03';
		        break;
		    case '10':
		    	reversedRel = '03';
		        break;
		    case '11':
		    	reversedRel = '13';
		        break;
		    case '12':
		    	reversedRel = '12';
		        break;
		    case '13':
		    	reversedRel = '11';
		        break;
		    case '14':
		    	reversedRel = '14';
		        break;
		    case '15':
		    	reversedRel = '26';
		        break;
		    case '15c':
		    	reversedRel = '31';
		        break;
		    case '15p':
		    	reversedRel = '03';
		        break;
		    case '16':
		    	reversedRel = '17';
		        break;
		    case '17':
		    	reversedRel = '16';
		        break;
		    case '18':
		    	reversedRel = '18';
		        break;
		    case '19':
		    	reversedRel = '03';
		        break;
		    case '25':
		    	reversedRel = '25';
		        break;
		    case '26':
		    	reversedRel = '15';
		        break;
		    case '31':
		    	reversedRel = '15';
		        break;
		    case '53':
		    	reversedRel = '53';
		        break;
		    case 'G8':
		    	reversedRel = 'G8';
		        break;
		    case 'G9':
		    	reversedRel = 'G9';
		        break;
		    case '03-53':
		    	reversedRel = '53-19';
		        break;
		    case '53-19':
		    	reversedRel = '03-53';
		    	break;
		    //following relationship does not have reversed mapping, need update from Abhinav
		    case '23':
		    	reversedRel = '23';
		    	break;
		    case '24':
		    	reversedRel = '24';
		    	break;
		    case '38':
		    	reversedRel = '38';
		    	break;
		    case '60':
		    	reversedRel = '60';
		    	break;
		    case 'D2':
		    	reversedRel = 'D2';
		        break;
	}

		return reversedRel;
	}


	function showNextSelection(e){
		var showNextFlag = true;
		e.closest('li[id^=relationshipSelection]').find('select').each(function(){
			if($(this).val() == ''){
				showNextFlag = false;
			}
		});

		var  nextSelection = e.closest('li').next();
		if(showNextFlag && nextSelection.find('select').length > 0){
			nextSelection.fadeIn("slow");
		}
	}

/*
****************************************************************************************************************
* addNonFinancialDetails.js                                                                                        *
****************************************************************************************************************
*/
	var nonFinancialHouseHoldDone = 1 ;
	var HouseHoldRepeat=1;

	var HouseHoldgender ;
	var HouseHoldHavingSSN ;
	var  HouseHoldssn1 ;
	var HouseHoldssn2 ;
	var  HouseHoldssn3 ;
	var  HouseHoldSSN ;
	var  notAvailableSSNReason ;
	var UScitizen ;
	var naturalizedCitizenshipIndicator  ;
	var livedIntheUSSince1996Indicator ;


	var certificateType ;
	var certificateAlignNumber ;
	var certificateNumber ;

	var documentSameNameIndicator ;
	var documentFirstName ;
	var documentLastName ;
	var documentMiddleName ;
	var documentSuffix ;

	var atLeastOneChildUnder19 ;
	var takeCareOfSelf ;
	var takeCareOfAnotherChild ;
	var takeCareOfAnotherChildFirstName ;
	var takeCareOfAnotherChildMiddleName ;
	var takeCareOfAnotherChildLastName ;
	var takeCareOfAnotherChildSuffix ;
	var takeCareOfAnotherChildDob ;
	var relationShipWithAnotherChild ;
	var doesLiveWithAdoptivePerent ;


	function resetNonFinancialGlobalData()
	{
		$("#askAboutSSNOnPage61").show();
		/*$("#sameSSNPage61Info").show();*/
		$("#mandateExemptionP61").show();
		$("#sameNameInfoPage62P2").show();
		//$(".parentOrCaretakerBasicDetailsContainer").show(); --Commented by Yogesh to avoid auto display of parentcaretaker details for second applicant
		 HouseHoldgender ='';
		 HouseHoldHavingSSN ='';
		 HouseHoldssn1=  '';
		 HouseHoldssn2= '';
		 HouseHoldssn3=  '';
		 HouseHoldSSN = '';
		 notAvailableSSNReason = '';
		 UScitizen='';
		 naturalizedCitizenshipIndicator = '';
		 livedIntheUSSince1996Indicator='';


		 certificateType = '';
		 certificateAlignNumber = "";
		 certificateNumber = "";

		 documentSameNameIndicator = '';
		 documentFirstName = '';
		 documentLastName = '';
		 documentMiddleName = '';
		 documentSuffix = '';

		 atLeastOneChildUnder19 = '';
		 takeCareOfSelf = '';
		 takeCareOfAnotherChild = '';
		 takeCareOfAnotherChildFirstName = '';
		 takeCareOfAnotherChildMiddleName = '';
		 takeCareOfAnotherChildLastName = '';
		 takeCareOfAnotherChildSuffix = '';
		 takeCareOfAnotherChildDob = '';
		 relationShipWithAnotherChild = '';
		 doesLiveWithAdoptivePerent = '';
		 cleanAppscr67();
		 cleanAppscr61();
		 cleanAppscr63();

	}




	function addNonFinancialHouseHold()
	{
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}

		var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
		if(currentPage == 'appscr64' && nonFinancialHouseHoldDone == noOfHouseHold)
		{
		next();
		return;
		}

		if(currentPage == 'appscr64' && editNonFinacial == true)
		{
		next();
		editNonFinacial == false;
		return;
		}

		if(currentPage == 'appscr64' && (editFlag == true || flagToUpdateFromResult== true)	)
		{


			appscr67EditControllerUpdateValues(editMemberNumber);
			updateNonFinancialDetailsToTable();
			//showHouseHoldContactSummary();

			if(flagToUpdateFromResult == true)
			{
			flagToUpdateFromResult=false;
			if(checkStillUpdateRequired())
				{

				return false;
				}
			else
				{
				goToPageById('appscrBloodRel');
				$('#finalEditDiv').show();
				return false;
				}

			}
			else
			goToPageById('appscr67');
			editFlag = false;
			return false;
		}
		else if(editFlag == true || flagToUpdateFromResult== true)
		 {
			  $('#back_button_div').show();
			  var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();

			  $('.nameOfHouseHold').text(houseHoldname);
		 }


		$('#goBack').show();
		$('#prevBtn').attr('onclick','prev();');
		var noOfHouseHold = parseInt($('#numberOfHouseHold').text());



		 if(currentPage=='appscr64' && nonFinancialHouseHoldDone == noOfHouseHold)
		 {
			 updatePageOtherAddress();
			 populateDataToRelations();
			 setAddressDetailsToSpan();
		 }

		if(currentPage=='appscr64')
			{

			if($('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html() == undefined)
				addNonFinancialDetailsToTable();
			else
				updateNonFinancialDetailsToTable();
			resetNonFinancialGlobalData();
			resetAddNonFinancialDetails();
			}
		else
			if(currentPage=='appscr62Part1'){

				checkUSCitizenForNextPage();

			}




		if(currentPage=='appscr64' && nonFinancialHouseHoldDone < noOfHouseHold)
				{

					nonFinancialHouseHoldDone++;
					$('#goBack').hide();
					while(getInfantData(nonFinancialHouseHoldDone)=='yes')
					{
						setNonFinancialPageProperty(nonFinancialHouseHoldDone);
						if($('#appscrHouseHoldgender57TD'+nonFinancialHouseHoldDone).html() == undefined)
							addNonFinancialDetailsToTable();
						else
							updateNonFinancialDetailsToTable();
						resetNonFinancialGlobalData();
						if(nonFinancialHouseHoldDone == noOfHouseHold)
							break;
							nonFinancialHouseHoldDone++;
					}
					if(currentPage=='appscr64' && nonFinancialHouseHoldDone == noOfHouseHold && getInfantData(nonFinancialHouseHoldDone)=='yes')
					 {
						 updatePageOtherAddress();
						 populateDataToRelations();
						 setAddressDetailsToSpan();
						 setNonFinancialPageProperty(1);
						 next();
					 }
					else
						if(nonFinancialHouseHoldDone <= noOfHouseHold)
						{
						$('#DontaskAboutSSNOnPage61').hide();
						goToPageById('appscr61');
						cleanAppscr67();
						resetNonFinancialGlobalData();
						resetAddNonFinancialDetails();
						setNonFinancialPageProperty(nonFinancialHouseHoldDone);
						}

					else
						{
						setNonFinancialPageProperty(1);

						next();
						}

					if(haveData == true)
					{
						haveDataFlag = true;
						if(useJSON == true){
							appscr67EditController(nonFinancialHouseHoldDone);
						}else {
							if(nonFinancialHouseHoldDone<=houseHoldNumberValue)
								appscr67EditController(nonFinancialHouseHoldDone);
						}
						haveDataFlag = false;
					}
				}
		else
			if(nonFinancialHouseHoldDone == noOfHouseHold)
				{
				if(currentPage == "appscr64")
					fillValuesToFields("appscr64");
				next();
				$('#goBack').show();

				}
			else

				next();



	}


	function addNonFinancialDetailsToTable()
	{


		 HouseHoldgender = $('input[name=appscr61_gender]:radio:checked').val();
		 HouseHoldHavingSSN =$('input[name=socialSecurityCardHolderIndicator]:radio:checked').val();
		 /*HouseHoldssn1=  $('#ssn1').val();
		 HouseHoldssn2=  $('#ssn2').val();
		 HouseHoldssn3=  $('#ssn3').val();
		 HouseHoldSSN = HouseHoldssn1 +'-'+HouseHoldssn2+'-'+HouseHoldssn3;*/

		 HouseHoldSSN = $('#ssn').val();
		 notAvailableSSNReason = $('#reasonableExplanationForNoSSN').val();

		 UScitizen=$('input[name=UScitizen]:radio:checked').val();

		 var UScitizenManualVerification= $('input[name=UScitizenManualVerification]:radio:checked').val();

		 naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();
		 livedIntheUSSince1996Indicator= $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();

		 var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').prop('checked');

		 certificateType = $('input[name=naturalizedCitizen]:radio:checked').val();
		 certificateAlignNumber = "";
		 certificateNumber = "";

		 documentSameNameIndicator = $('input[name=UScitizen62]:radio:checked').val();
		 documentFirstName = $('#documentFirstName').val();
		 documentLastName = $('#documentLastName').val();
		 documentMiddleName = $('#documentMiddleName').val();
		 documentSuffix = $('#documentSuffix').val();

		 atLeastOneChildUnder19 = $('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val();
		 //takeCareOfSelf = $('#takeCareOf_applicantOrNonapplicant').prop('checked');
		 takeCareOfAnotherChild = $('#takeCareOf_anotherChild').prop('checked');
		 takeCareOfAnotherChildFirstName = $('#parent-or-caretaker-firstName').val();
		 takeCareOfAnotherChildMiddleName = $('#parent-or-caretaker-middleName').val();
		 takeCareOfAnotherChildLastName = $('#parent-or-caretaker-lastName').val();
		 takeCareOfAnotherChildSuffix = $('#parent-or-caretaker-suffix').val();
		 takeCareOfAnotherChildDob = $('#parent-or-caretaker-dob').val();
		 relationShipWithAnotherChild = $('#applicant-relationship').val();
		 doesLiveWithAdoptivePerent = $('input[name=birthOrAdoptiveParents]:radio:checked').val();

		 var appendRowData='';

		 if(getInfantData(nonFinancialHouseHoldDone)=='yes')
		 {

			 	appendRowData+="<td class='sex' id='appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone+"'>"+"male"+"</td>";
		 		appendRowData+="<td class='i_ssn_verified' id='appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";
				appendRowData+="<td class='i_ssn' id='appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone+"'>"+"--"+"</td>";
				appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";
				appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
				appendRowData+="<td class='notAvailableSSNReason' id='appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";

				appendRowData+="<td class='i_citizenship' id='appscr57UScitizenTD"+nonFinancialHouseHoldDone+"'>"+UScitizen+"</td>";
				appendRowData+="<td class='i_citizenshipManualVerified' id='appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone+"'>"+UScitizenManualVerification+"</td>";

				appendRowData+="<td class='i_naturalized' id='appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone+"'>"+"no"+"</td>";
				appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
				appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='i_migrant' id='appscr57MigrantStatusIndicatorTD"+nonFinancialHouseHoldDone+"'>"+naturalCitizen_eligibleImmigrationStatus+"</td>";

				appendRowData+="<td class='documentSameNameIndicator' id='appscr57documentSameNameIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
				appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";

				appendRowData+="<td class='citizenshipAsVerifiedIndicator' id='appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone+"'>"+"false"+"</td>";

				appendRowData+="<td class='atLeastOneChildUnder19' id='appscr57AtLeastOneChildUnder19TD"+nonFinancialHouseHoldDone+"'>no</td>";
				appendRowData+="<td class='takeCareOfSelf' id='appscr57TakeCareOfSelfTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='takeCareOfAnotherChild' id='appscr57TakeCareOfAnotherChildTD"+nonFinancialHouseHoldDone+"'>false</td>";

				appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
				appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'></td>";

				appendRowData+="<td class='doesLiveWithAdoptivePerent' id='appscr57DoesLiveWithAdoptivePerentTD"+nonFinancialHouseHoldDone+"'>no</td>";


				appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";

				appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>true</td>";
				appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'></td>";

				appendRowData+="<td class='raceAmericanIndian' id='appscr57RaceAmericanIndianTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceAsianIndian' id='appscr57RaceAsianIndianTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceBlackOrAfricanAmerican' id='appscr57RaceBlackOrAfricanAmericanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceChinese' id='appscr57RaceChineseTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceFilipino' id='appscr57RaceFilipinoTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceGuamanianOrChamorro' id='appscr57RaceGuamanianOrChamorroTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceJapanese' id='appscr57RaceJapaneseTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceKorean' id='appscr57RaceKoreanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceNativeHawaiian' id='appscr57RaceNativeHawaiianTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceOtherAsian' id='appscr57RaceOtherAsianTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceOtherPacificIslander' id='appscr57RaceOtherPacificIslanderTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceSamoan' id='appscr57RaceSamoanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceVietnamese' id='appscr57RaceVietnameseTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceWhiteOrCaucasian' id='appscr57RaceWhiteOrCaucasianTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='raceOther' id='appscr57RaceOtherTD"+nonFinancialHouseHoldDone+"'>true</td>";
				appendRowData+="<td class='raceOtherText' id='appscr57RaceOtherTextTD"+nonFinancialHouseHoldDone+"'></td>";

		 }
	 else{
		 	appendRowData+="<td class='sex' id='appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone+"'>"+HouseHoldgender+"</td>";
			appendRowData+="<td class='i_ssn_verified' id='appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone+"'>"+HouseHoldHavingSSN+"</td>";
			appendRowData+="<td class='i_ssn' id='appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone+"'>"+HouseHoldSSN+"</td>";
			if($('input[name=fnlnsSame]:radio:checked').val()=="yes")
			{
				appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";
				appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			}
			else
			{
				appendRowData+="<td class='sameNameInSSNCardIndicator' id='appscr57SameNameInSSNCardIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";
				appendRowData+="<td class='firstNameOnSSNCard' id='appscr57FirstNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#firstNameOnSSNCard').val()+"</td>";
				appendRowData+="<td class='middleNameOnSSNCard' id='appscr57MiddleNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#middleNameOnSSNCard').val()+"</td>";
				appendRowData+="<td class='lastNameOnSSNCard' id='appscr57LastNameOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#lastNameOnSSNCard').val()+"</td>";
				appendRowData+="<td class='suffixOnSSNCard' id='appscr57SuffixOnSSNCardTD"+nonFinancialHouseHoldDone+"'>"+$('#suffixOnSSNCard').val()+"</td>";
			}
			appendRowData+="<td class='notAvailableSSNReason' id='appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone+"'>"+notAvailableSSNReason+"</td>";

			appendRowData+="<td class='i_citizenship' id='appscr57UScitizenTD"+nonFinancialHouseHoldDone+"'>"+UScitizen+"</td>";
			appendRowData+="<td class='i_citizenshipManualVerified' id='appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone+"'>"+UScitizenManualVerification+"</td>";
			appendRowData+="<td class='i_naturalized' id='appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone+"'>"+naturalizedCitizenshipIndicator+"</td>";

			if(certificateType == "CitizenshipCertificate")
			{
				certificateAlignNumber = $('#citizenshipAlignNumber').val();
				certificateNumber = $('#appscr62p1citizenshipCertificateNumber').val();
				appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
				appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateAlignNumber+"</td>";
				appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateNumber+"</td>";
			}
			else
			{
				certificateAlignNumber = $('#naturalizationAlignNumber').val();
				certificateNumber = $('#naturalizationCertificateNumber').val();

				appendRowData+="<td class='i_certificateType' id='appscr57certificateTypeIndicatorTD"+nonFinancialHouseHoldDone+"'>"+certificateType+"</td>";
				appendRowData+="<td class='i_certificateAlignNumber' id='appscr57naturalizedCitizenshipAlignNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateAlignNumber+"</td>";
				appendRowData+="<td class='i_certificateNumber' id='appscr57certificateNumberTD"+nonFinancialHouseHoldDone+"'>"+certificateNumber+"</td>";
			}
			appendRowData+="<td class='i_migrant' id='appscr57ImmigrationStatusTD"+nonFinancialHouseHoldDone+"'>"+naturalCitizen_eligibleImmigrationStatus+"</td>";

			appendRowData+="<td class='documentSameNameIndicator' id='appscr57documentSameNameIndicatorTD"+nonFinancialHouseHoldDone+"'>"+documentSameNameIndicator+"</td>";
			if(documentSameNameIndicator == "no")
			{
				appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'>"+documentFirstName+"</td>";
				appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'>"+documentMiddleName+"</td>";
				appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'>"+documentLastName+"</td>";
				appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>"+documentSuffix+"</td>";
			}
			else
			{
				appendRowData+="<td class='documentFirstName' id='appscr57documentFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentMiddleName' id='appscr57documentMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentLastName' id='appscr57documentLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='documentSuffix' id='appscr57documentSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
			}
			appendRowData+="<td class='citizenshipAsVerifiedIndicator' id='appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone+"'>"+livedIntheUSSince1996Indicator+"</td>";


			appendRowData+="<td class='atLeastOneChildUnder19' id='appscr57AtLeastOneChildUnder19TD"+nonFinancialHouseHoldDone+"'>"+atLeastOneChildUnder19+"</td>";
			appendRowData+="<td class='takeCareOfSelf' id='appscr57TakeCareOfSelfTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfSelf+"</td>";
			appendRowData+="<td class='takeCareOfAnotherChild' id='appscr57TakeCareOfAnotherChildTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChild+"</td>";
			if(takeCareOfAnotherChild == true || takeCareOfAnotherChild == "true")
			{
				appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildFirstName+"</td>";
				appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildMiddleName+"</td>";
				appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildLastName+"</td>";
				appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildSuffix+"</td>";
				appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'>"+takeCareOfAnotherChildDob+"</td>";
				appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'>"+relationShipWithAnotherChild+"</td>";
			}
			else
			{
				appendRowData+="<td class='takeCareOfAnotherChildFirstName' id='appscr57TakeCareOfAnotherChildFirstNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildMiddleName' id='appscr57TakeCareOfAnotherChildMiddleNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildLastName' id='appscr57TakeCareOfAnotherChildLastNameTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='takeCareOfAnotherChildSuffix' id='appscr57TakeCareOfAnotherChildSuffixTD"+nonFinancialHouseHoldDone+"'>Suffix</td>";
				appendRowData+="<td class='takeCareOfAnotherChildDob' id='appscr57TakeCareOfAnotherChildDobTD"+nonFinancialHouseHoldDone+"'></td>";
				appendRowData+="<td class='relationShipWithAnotherChild' id='appscr57RelationShipWithAnotherChildTD"+nonFinancialHouseHoldDone+"'></td>";
			}

			appendRowData+="<td class='doesLiveWithAdoptivePerent' id='appscr57DoesLiveWithAdoptivePerentTD"+nonFinancialHouseHoldDone+"'>"+doesLiveWithAdoptivePerent+"</td>";

			var ethnicityIndicator = $('input[name=checkPersonNameLanguage]:radio:checked').val();
			if(ethnicityIndicator == "yes" || ethnicityIndicator == "Yes")
			{
				appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>yes</td>";

				appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_cuban').prop("checked")+"</td>";
				appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_maxican').prop("checked")+"</td>";
				appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_puertoRican').prop("checked")+"</td>";
				/*appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>"+$('#ethnicity_other').prop("")+"</td>";
				appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'>"+$('#other_ethnicity ').val()+"</td>";*/
			}
			else
			{
				appendRowData+="<td class='ethnicityIndicator' id='appscr57EthnicityIndicatorTD"+nonFinancialHouseHoldDone+"'>no</td>";

				appendRowData+="<td class='ethnicityCuban' id='appscr57EthnicityCubanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityMexicanMexican' id='appscr57EthnicityMexicanMexicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityPuertoRican' id='appscr57EthnicityPuertoRicanTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityOther' id='appscr57EthnicityOtherTD"+nonFinancialHouseHoldDone+"'>false</td>";
				appendRowData+="<td class='ethnicityText' id='appscr57EthnicityTextTD"+nonFinancialHouseHoldDone+"'></td>";
			}
			appendRowData+="<td class='raceAmericanIndian' id='appscr57RaceAmericanIndianTD"+nonFinancialHouseHoldDone+"'>"+$('#americanIndianOrAlaskaNative').prop("checked")+"</td>";
			appendRowData+="<td class='raceAsianIndian' id='appscr57RaceAsianIndianTD"+nonFinancialHouseHoldDone+"'>"+$('#asianIndian').prop("checked")+"</td>";
			appendRowData+="<td class='raceBlackOrAfricanAmerican' id='appscr57RaceBlackOrAfricanAmericanTD"+nonFinancialHouseHoldDone+"'>"+$('#BlackOrAfricanAmerican').prop("checked")+"</td>";
			appendRowData+="<td class='raceChinese' id='appscr57RaceChineseTD"+nonFinancialHouseHoldDone+"'>"+$('#chinese').prop("checked")+"</td>";
			appendRowData+="<td class='raceFilipino' id='appscr57RaceFilipinoTD"+nonFinancialHouseHoldDone+"'>"+$('#filipino').prop("checked")+"</td>";
			appendRowData+="<td class='raceGuamanianOrChamorro' id='appscr57RaceGuamanianOrChamorroTD"+nonFinancialHouseHoldDone+"'>"+$('#guamanianOrChamorro').prop("checked")+"</td>";
			appendRowData+="<td class='raceJapanese' id='appscr57RaceJapaneseTD"+nonFinancialHouseHoldDone+"'>"+$('#japanese').prop("checked")+"</td>";
			appendRowData+="<td class='raceKorean' id='appscr57RaceKoreanTD"+nonFinancialHouseHoldDone+"'>"+$('#korean').prop("checked")+"</td>";
			appendRowData+="<td class='raceNativeHawaiian' id='appscr57RaceNativeHawaiianTD"+nonFinancialHouseHoldDone+"'>"+$('#nativeHawaiian').prop("checked")+"</td>";
			appendRowData+="<td class='raceOtherAsian' id='appscr57RaceOtherAsianTD"+nonFinancialHouseHoldDone+"'>"+$('#otherAsian').prop("checked")+"</td>";
			appendRowData+="<td class='raceOtherPacificIslander' id='appscr57RaceOtherPacificIslanderTD"+nonFinancialHouseHoldDone+"'>"+$('#otherPacificIslander').prop("checked")+"</td>";
			appendRowData+="<td class='raceSamoan' id='appscr57RaceSamoanTD"+nonFinancialHouseHoldDone+"'>"+$('#samoan').prop("checked")+"</td>";
			appendRowData+="<td class='raceVietnamese' id='appscr57RaceVietnameseTD"+nonFinancialHouseHoldDone+"'>"+$('#vietnamese').prop("checked")+"</td>";
			appendRowData+="<td class='raceWhiteOrCaucasian' id='appscr57RaceWhiteOrCaucasianTD"+nonFinancialHouseHoldDone+"'>"+$('#whiteOrCaucasian').prop("checked")+"</td>";
			/*appendRowData+="<td class='raceOther' id='appscr57RaceOtherTD"+nonFinancialHouseHoldDone+"'>"+$('#raceOther').prop("")+"</td>";
			appendRowData+="<td class='raceOtherText' id='appscr57RaceOtherTextTD"+nonFinancialHouseHoldDone+"'>"+$('#other_race').val()+"</td>";*/

		 }
			//$('#appscr57HouseHoldPersonalInformation').find('#'+$('#houseHoldTRNo').val()).append(appendRowData);
		 $('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+getCurrentApplicantID()).append(appendRowData);

	}

	function resetAddNonFinancialDetails()
	{
		/// PageId 61
		$('#appscr61Form input:text').val('');
		$('#appscr61Form select').val('0');

		/*$("#appscr61GenderMaleID").prop("checked", true);
		$("#socialSecurityCardHolderIndicatorYes").prop("checked", true);
		$("#fnlnsSameIndicatorNo").prop("checked", true);
		$("#fnlnsExemptionIndicatorYes").prop("checked", true);	*/

		/*$('#sameSSNPage61Info').show();*/

		/// PageId 62Part1
		$('#appscr62Part1Form input:text').val('');
		$('#appscr62Part1Form select').val('0');
		//$("#UScitizenIndicatorYes").prop("checked", true);
		//$("#naturalizedCitizenshipIndicatorYes").prop("checked", true);
		$("#naturalizedCitizenNaturalizedIndicator").prop("checked", false);
		//document.getElementById('naturalizedDonotHave').checked = false;
		//document.getElementById('citizenDonotHave').checked = false;
		//$("#naturalCitizen_eligibleImmigrationStatus").prop("checked", true);
		//$("#docType_other").prop("checked", true);
		showOrHideNaturalCitizenImmigrationStatus();
		/// PageId 62Part2
		$('#appscr62Part2Form input:text').val('');
		$('#appscr62Part2Form select').val('0');
		$("#docType_otherDocs").prop("checked", true);
		showOrHideAt62Part2();
		$('input[name=UScitizen62]').prop("checked", false);
		displayOrHideSameNameInfoPage62();
//		/$("#62Part_livesInUS").prop("checked", true);
		$("#62Part_livedIntheUSSince1996Indicator").prop("checked", true);

		$("#otherDocStatusOnPage62P1").show();
		isNaturalizedCitizen();
		/// PageId 63
		$('#appscr63Form input:text').val('');
		$('#appscr63Form select').val('0');
		$("#liveOneChildAge19").prop("checked", true);
		//document.getElementById('takeCareOf_applicantOrNonapplicant').checked = false;
		//document.getElementById('takeCareOf_anotherChild').checked = true;
		//$('#appscr63BirthOrAdoptiveParentsNo').prop("checked", true);

		/// PageId 64
		//$("#checkPersonNameLanguageYes").prop("checked", true);
		$('#appscr64Form input:checkbox').attr('checked',false);
		$('#appscr64Form input:text').val('');
		/*document.getElementById('ethnicity_other').checked = false;
		document.getElementById('raceOther').checked = false;*/



		$('#ethnicityDiv').hide();
		$("#documentTypeDiv").show();

		$("#naturalizedCitizenDiv").hide();
		$("#documentTypeDiv").hide();
		$("#eligibleImmigrationStatusDiv").hide();

		cleanAppscr61();
		fillAppscr61(nonFinancialHouseHoldDone);
	}


	function updatePageOtherAddress()
	{

		var houseHoldName='';
		$('#noOfOtherApplicant').html('');

		$('#HouseHoldAddressDiv').html('');

		var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		for(var i=1;i<=noOfHouseHoldmember;i++){
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var nameWithSpace=""+trimSpaces(householdMemberObj.name.firstName) +" "+trimSpaces(householdMemberObj.name.middleName)+" "+trimSpaces(householdMemberObj.name.lastName);
			var pureNameOfUser = trimSpaces(householdMemberObj.name.firstName) + "_" + trimSpaces(householdMemberObj.name.middleName) + "_" + trimSpaces(householdMemberObj.name.lastName);

			if(i>1){

				var chDATA="<label  class='capitalize-none' ><input type='checkbox'   id='householdContactAddress"+i+"' onclick=\"checkUncheck('"+i+"')\""+" name='householdContactAddress' value='"+pureNameOfUser+"' data-parsley-errors-container='#householdContactAddressErrorDiv' data-parsley-required data-parsley-required-message='"+ jQuery.i18n.prop("ssap.required.field") +"'><span class='camelCaseName'>"+nameWithSpace+"</span></label>";


				namesOfHouseHold.push(pureNameOfUser);


				$('#noOfOtherApplicant').append(chDATA);

				$('#HouseHoldAddressDiv').append( getAddressData(pureNameOfUser, i, houseHoldName));

				/*HIX-39915*/
				if(exchangeName == "NMHIX"){
					$(".stateName").text("New Mexico");
				}else{
					$(".stateName").text("Idaho");
				}


				$('input[name=isTemporarilyLivingOutsideIndicator'+i+']').change(function(){

					if($(this).val()=="yes")
					{
						$(this).closest('.formContainerSecound').find('div[id^=inNMDiv]').show();
						//$('#inNMDiv'+pureNameOfUser).show();
					}
					else
					{	$(this).closest('.formContainerSecound').find('div[id^=inNMDiv]').hide();
						//$('#inNMDiv'+pureNameOfUser).hide();
					}

				});

			}
		}

		var otherDATA='<label  class="capitalize-none">'
			+'<input type="checkbox" id="HouseHoldotherAddressNoneOfTheAbove" onchange="otherAddressNoneOfTheAbove()" name="householdContactAddress" value="others"'
			+' data-parsley-required data-parsley-required-message="' + jQuery.i18n.prop("ssap.required.field") + '" data-parsley-errors-container="#householdContactAddressErrorDiv"'
			+' />'
			+'None of the Above</div>'
			+'</label><div id="householdContactAddressErrorDiv"></div>';
		$('#noOfOtherApplicant').append(otherDATA);

		//removeRequiredValidation();
	}


	//KEY FOR STATES

	function getAddressData(applicantActualName, i, houseHoldName)
	{
		houseHoldName = getNameRemovedBySpace(houseHoldName);
		var nameOfSpacedName = getNameRemovedBySpace(applicantActualName);

		var data ='<div id="other_Address_'+i+'" class="formContainerSecound hide">'
		+'<div class="control-group"><p>'
		+'Where does <strong><span class = "camelCaseName">'+nameOfSpacedName+'</span></strong> live? </span>'
		+'</p></div>';

		if(i<2){
			data+='<div class="hide">';
		}else{
			data+='<div>';
		}

		data+='</div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.address1")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="Address 1" value="" name="applicantOrNon-applicantAddress1" id="applicant_or_non-applican_address_1'+i+'" class="input-medium" data-parsley-pattern="^[A-Za-z0-9\\\'\\.\\-\\s\\,]{5,75}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.addressValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.addressRequired")+'"/>'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.address2")+'</label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="Address 2" value="" name="applicantOrapplicantAddress2" id="applicant_or_non-applican_address_2'+i+'" class="input-medium" data-parsley-pattern="^[A-Za-z0-9\\\'\\.\\-\\s\\,]{5,75}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.addressValid")+'"/>'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.city")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="City" value="" name="city" id="city'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.cityRequired")+'"  data-parsley-pattern="^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" data-parsley-length="[1, 100]" data-parsley-length-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" />'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.zip")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="Zip" value="" name="zip" id="zip'+i+'" class="input-medium" onblur="getCountyListByZipCode('+i+')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.zipRequired")+'" data-parsley-pattern="^[0-9]{5}(\\-[0-9]{4})?$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.zipValid")+'"/>'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<select name="applicantOrNon-applicantState" id="applicant_or_non-applican_state'+i+'" class="input-medium" onChange="getCountyListByZipCode('+i+')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.stateRequired")+'">'
			+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
			+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
			+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
			+'<option value="AS">'+jQuery.i18n.prop("ssap.state.AmericanSamoa")+'</option>'
			+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
			+'<option value="AR">'+jQuery.i18n.prop("ssap.state.Arkansas")+'</option>'
			+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
			+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
			+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
			+'<option value="DE">'+jQuery.i18n.prop("ssap.state.Delaware")+'</option>'
			+'<option value="DC">'+jQuery.i18n.prop("ssap.state.DistrictOfColumbia")+'</option>'
			+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
			+'<option value="GA">'+jQuery.i18n.prop("ssap.state.Georgia")+'</option>'
			+'<option value="GU">'+jQuery.i18n.prop("ssap.state.Guam")+'</option>'
			+'<option value="HI">'+jQuery.i18n.prop("ssap.state.Hawaii")+'</option>'
			+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
			+'<option value="IL">'+jQuery.i18n.prop("ssap.state.Illinois")+'</option>'
			+'<option value="IN">'+jQuery.i18n.prop("ssap.state.Indiana")+'</option>'
			+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
			+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
			+'<option value="KY">'+jQuery.i18n.prop("ssap.state.Kentucky")+'</option>'
			+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
			+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
			+'<option value="MD">'+jQuery.i18n.prop("ssap.state.Maryland")+'</option>'
			+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
			+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
			+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
			+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'
			+'<option value="MO">'+jQuery.i18n.prop("ssap.state.Missouri")+'</option>'
			+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
			+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
			+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
			+'<option value="NH">'+jQuery.i18n.prop("ssap.state.NewHampshire")+'</option>'
			+'<option value="NJ">'+jQuery.i18n.prop("ssap.state.NewJersey")+'</option>'
			+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
			+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
			+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
			+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
			+'<option value="MP">'+jQuery.i18n.prop("ssap.state.NorthernMarianasIslands")+'</option>'
			+'<option value="OH">'+jQuery.i18n.prop("ssap.state.Ohio")+'</option>'
			+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
			+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
			+'<option value="PA">'+jQuery.i18n.prop("ssap.state.Pennsylvania")+'</option>'
			+'<option value="PR">'+jQuery.i18n.prop("ssap.state.PuertoRico")+'</option>'
			+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
			+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
			+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
			+'<option value="TN">'+jQuery.i18n.prop("ssap.state.Tennessee")+'</option>'
			+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
			+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
			+'<option value="VT">'+jQuery.i18n.prop("ssap.state.Vermont")+'</option>'
			+'<option value="VA">'+jQuery.i18n.prop("ssap.state.Virginia")+'</option>'
			+'<option value="VI">'+jQuery.i18n.prop("ssap.state.VirginIslands")+'</option>'
			+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
			+'<option value="WV">'+jQuery.i18n.prop("ssap.state.WestVirginia")+'</option>'
			+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
			+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
			+'</select>'
			+'</div></div>'
			+'<div class="control-group">'
			+'<label class="control-label" for="home_primary_county">'+jQuery.i18n.prop("ssap.county")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
			+'<span aria-label="Required!"></span></label>'
			+ '<div class="controls">'
			+'<select data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.county")+'" id="applicant_or_non-applican_county'+i+'" name="home_primary_county" class="input-large" data-rel="countyPopulate">'
			+'<option value=""><spring:message code="ssap.County" text="Select County" /></option></select>'
			+'</div></div>'
			+'<p>Is&nbsp;<strong>'+nameOfSpacedName+'</strong>&nbsp;living outside <span class="stateName"></span> temporarily?<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" />'
			+'<div class="margin5-l controls"><label>'
			+'<input type="radio" value="yes" id="TemporarilyLivingOutsideIndicatorYes'+i+'" name="isTemporarilyLivingOutsideIndicator'+i+'" required parsley-error-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>Yes'
			+'</label><label>'
			+'<input type="radio" id="TemporarilyLivingOutsideIndicatorNo" value="no" checked  name="isTemporarilyLivingOutsideIndicator'+i+'" />No'
			+'</label></div>'
			+'<div class="hide" id="inNMDiv'+i+'">'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.city")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="City" value="" name="applicant2City" id="applicant2city'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.cityRequired")+'"  data-parsley-pattern="^[a-zA-Z]+(?:[\\s-][a-zA-Z]+)*$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'" data-parsley-length="[1, 100]" data-parsley-length-message="'+jQuery.i18n.prop("ssap.error.cityValid")+'"/>'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.zip")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<input type="text" placeholder="Zip" value="" name="applicant2Zip" id="applicant-2-zip'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.zipRequired")+'" data-parsley-pattern="^[0-9]{5}(\\-[0-9]{4})?$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.zipValid")+'"/>'
			+'</div></div>'
			+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
			+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"><span aria-label="Required!"></span></label>'
			+'<div class="controls">'
			+'<select name="applicantOrNon-applicantState" id="applicant_or_non-applican_stateTemp'+i+'" class="input-medium" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.stateRequired")+'">'
			+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
			+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
			+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
			+'<option value="AS">'+jQuery.i18n.prop("ssap.state.AmericanSamoa")+'</option>'
			+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
			+'<option value="AR">'+jQuery.i18n.prop("ssap.state.Arkansas")+'</option>'
			+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
			+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
			+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
			+'<option value="DE">'+jQuery.i18n.prop("ssap.state.Delaware")+'</option>'
			+'<option value="DC">'+jQuery.i18n.prop("ssap.state.DistrictOfColumbia")+'</option>'
			+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
			+'<option value="GA">'+jQuery.i18n.prop("ssap.state.Georgia")+'</option>'
			+'<option value="GU">'+jQuery.i18n.prop("ssap.state.Guam")+'</option>'
			+'<option value="HI">'+jQuery.i18n.prop("ssap.state.Hawaii")+'</option>'
			+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
			+'<option value="IL">'+jQuery.i18n.prop("ssap.state.Illinois")+'</option>'
			+'<option value="IN">'+jQuery.i18n.prop("ssap.state.Indiana")+'</option>'
			+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
			+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
			+'<option value="KY">'+jQuery.i18n.prop("ssap.state.Kentucky")+'</option>'
			+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
			+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
			+'<option value="MD">'+jQuery.i18n.prop("ssap.state.Maryland")+'</option>'
			+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
			+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
			+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
			+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'
			+'<option value="MO">'+jQuery.i18n.prop("ssap.state.Missouri")+'</option>'
			+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
			+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
			+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
			+'<option value="NH">'+jQuery.i18n.prop("ssap.state.NewHampshire")+'</option>'
			+'<option value="NJ">'+jQuery.i18n.prop("ssap.state.NewJersey")+'</option>'
			+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
			+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
			+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
			+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
			+'<option value="MP">'+jQuery.i18n.prop("ssap.state.NorthernMarianasIslands")+'</option>'
			+'<option value="OH">'+jQuery.i18n.prop("ssap.state.Ohio")+'</option>'
			+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
			+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
			+'<option value="PA">'+jQuery.i18n.prop("ssap.state.Pennsylvania")+'</option>'
			+'<option value="PR">'+jQuery.i18n.prop("ssap.state.PuertoRico")+'</option>'
			+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
			+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
			+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
			+'<option value="TN">'+jQuery.i18n.prop("ssap.state.Tennessee")+'</option>'
			+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
			+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
			+'<option value="VT">'+jQuery.i18n.prop("ssap.state.Vermont")+'</option>'
			+'<option value="VA">'+jQuery.i18n.prop("ssap.state.Virginia")+'</option>'
			+'<option value="VI">'+jQuery.i18n.prop("ssap.state.VirginIslands")+'</option>'
			+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
			+'<option value="WV">'+jQuery.i18n.prop("ssap.state.WestVirginia")+'</option>'
			+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
			+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
			+'</select>'
			+'</div></div>'
			+'</div>';

		/*my code above*/




	return data;
	}


	function checkUncheck(addressID) {



		if ($('#householdContactAddress'+addressID).is(':checked')) {


	            $("#other_Address_"+addressID).show();
	            $('#HouseHoldotherAddressNoneOfTheAbove').attr("checked",false);
		}
	     else {

	            $("#other_Address_"+addressID).hide();
	        }


	}


	function setAddressDetailsNonfinancial()
	{
		if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
			return;
		}
		var home_addressLine1=$('#home_addressLine1').val();
		var home_addressLine2=$('#home_addressLine2').val();
		var home_primary_city=$('#home_primary_city').val();
		var home_primary_zip=$('#home_primary_zip').val();
		var home_primary_state=$('#home_primary_state').val();
		var home_primary_county=$('#home_primary_county').val();
		var liveAtOtherAddressIndicator=false;
		var mailing_addressLine1=$('#mailing_addressLine1').val();
		var mailing_addressLine2=$('#mailing_addressLine2').val();
		var mailing_primary_city=$('#mailing_primary_city').val();
		var mailing_primary_zip=$('#mailing_primary_zip').val();
		var mailing_primary_state=$('#mailing_primary_state').val();
		var mailing_primary_county=$('#mailing_primary_county').val();

		var houseHoldPhone1=$('#first_phoneNo').val();
		var houseHoldPhoneExt1=$('#first_ext').val();
		var houseHoldPhoneType1=$('#first_phoneType').val();
		var houseHoldPhone2=$('#second_phoneNo').val();
		var houseHoldPhoneExt2=$('#second_ext').val();
		var houseHoldPhoneType2=$('#second_phoneType').val();
		var houseHoldSpokenLanguag=$('#preffegred_spoken_language').val();
		var houseHoldWrittenLanguag=$('#preffered_written_language').val();

		var isTemporarilyLivingOutsideIndicator=true;
		 var otherAddressOfHouseHold="";




			$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {

				if($('#appscr57HouseHoldhome_addressLine1TD'+(i+1)).html() != undefined)
				{
				UpdateAddressDetailsNonfinancial(i);

				return;
				}

				var appendRowData='';
			if(i==0)
				{

				appendRowData+="<td id='appscr57HouseHoldhome_addressLine1TD"+(i+1)+"' class='residensy_addressLine1'>"+home_addressLine1+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_addressLine2TD"+(i+1)+"' class='residensy_addressLine2'>"+home_addressLine2+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_primary_cityTD"+(i+1)+"' class='city'>"+home_primary_city+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_primary_zipTD"+(i+1)+"' class='zip'>"+home_primary_zip+"</td>";
				appendRowData+="<td class='homeState'  id='appscr57HouseHoldhome_primary_stateTD"+(i+1)+"'>"+home_primary_state+"</td>";
				appendRowData+="<td class='homeCounty'  id='appscr57HouseHoldhome_primary_countyTD"+(i+1)+"'>"+home_primary_county+"</td>";


				appendRowData+="<td id='appscr57HouseHoldmailing_addressLine1TD"+(i+1)+"' class='mailing_addressLine1'>"+mailing_addressLine1+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_addressLine2TD"+(i+1)+"' class='mailing_addressLine2'>"+mailing_addressLine2+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_primary_cityTD"+(i+1)+"' class='mailingCity'>"+mailing_primary_city+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_primary_zipTD"+(i+1)+"' class='mailingZip'>"+mailing_primary_zip+"</td>";
				appendRowData+="<td class='mailingState' id='appscr57mailing_primary_stateTD"+(i+1)+"'>"+mailing_primary_state+"</td>";
				appendRowData+="<td class='mailingCounty'  id='appscr57HouseHoldmailing_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>";

				appendRowData+="<td class='houseHoldPhoneFirst'  id='appscr57houseHoldPhoneFirstTD"+(i+1)+"'>"+houseHoldPhone1+"</td>";
				appendRowData+="<td class='houseHoldPhoneExtFirst'  id='appscr57houseHoldPhoneExtFirstTD"+(i+1)+"'>"+houseHoldPhoneExt1+"</td>";
				appendRowData+="<td class='houseHoldPhoneTypeFirst'  id='appscr57houseHoldPhoneTypeFirstTD"+(i+1)+"'>"+houseHoldPhoneType1+"</td>";
				appendRowData+="<td class='houseHoldPhoneSecond'  id='appscr57houseHoldPhoneSecondTD"+(i+1)+"'>"+houseHoldPhone2+"</td>";
				appendRowData+="<td class='houseHoldPhoneExtSecond'  id='appscr57houseHoldPhoneExtSecondTD"+(i+1)+"'>"+houseHoldPhoneExt2+"</td>";
				appendRowData+="<td class='houseHoldPhoneTypeSecond'  id='appscr57houseHoldPhoneTypeSecondTD"+(i+1)+"'>"+houseHoldPhoneType2+"</td>";
				appendRowData+="<td class='houseHoldSpokenLanguage'  id='appscr57houseHoldSpokenLanguageTD"+(i+1)+"'>"+houseHoldSpokenLanguag+"</td>";
				appendRowData+="<td class='houseHoldWrittenLanguage'  id='appscr57houseHoldWrittenLanguageTD"+(i+1)+"'>"+houseHoldWrittenLanguag+"</td>";

				appendRowData+="<td class='liveAtOtherAddressIndicatorClass' id='appscr57liveAtOtherAddressIndicatorTD"+(i+1)+"'>"+liveAtOtherAddressIndicator+"</td>";
				appendRowData+="<td class='isTemporarilyLivingOutsideIndicatorClass' id='appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)+"'>"+isTemporarilyLivingOutsideIndicator+"</td>";
				appendRowData+="<td id='appscr57otherAddressState"+(i+1)+"' class='otherAddressState'>"+home_primary_state+"</td>";


				}
			else
				{

				 var actualName=getNameByTRIndex(i);

				  home_addressLine1=$('#applicant_or_non-applican_address_1'+actualName).val();
				  home_addressLine2=$('#applicant_or_non-applican_address_2'+actualName).val();
				  home_primary_city=$('#city'+actualName).val();
				  home_primary_zip=$('#zip'+actualName).val();
				  otherAddressOfHouseHold=$('#applicant_or_non-applican_state'+actualName).val();

				  if ($('#householdContactAddress'+actualName).is(':checked')) {
					  liveAtOtherAddressIndicator=true;
					  home_addressLine1 = $('#applicant_or_non-applican_address_1'+actualName).val();
					  home_addressLine2 = $('#applicant_or_non-applican_address_2'+actualName).val();
					  home_primary_city = $('#city'+actualName).val();
					  home_primary_zip = $('#zip'+actualName).val();
					  home_primary_state= $('#applicant_or_non-applican_state'+actualName).val();
					  otherAddressOfHouseHold=home_primary_state=$('#applicant_or_non-applican_state'+actualName).val();
				  }
				  else
					  {
					  liveAtOtherAddressIndicator=false;

					  home_addressLine1=$('#home_addressLine1').val();
					  home_addressLine2=$('#home_addressLine2').val();
					  home_primary_city=$('#home_primary_city').val();
					  home_primary_zip=$('#home_primary_zip').val();
					  home_primary_state=$('#home_primary_state').val();
					  otherAddressOfHouseHold=home_primary_state=$('#home_primary_state').val();
					  }


				mailing_primary_state=home_primary_state;

				isTemporarilyLivingOutsideIndicator = $('input[name=isTemporarilyLivingOutsideIndicator'+actualName+']:radio:checked').val();

				appendRowData+="<td id='appscr57HouseHoldhome_addressLine1TD"+(i+1)+"' class='residensy_addressLine1'>"+home_addressLine1+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_addressLine2TD"+(i+1)+"' class='residensy_addressLine2'>"+home_addressLine2+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_primary_cityTD"+(i+1)+"' class='city'>"+home_primary_city+"</td>";
				appendRowData+="<td id='appscr57HouseHoldhome_primary_zipTD"+(i+1)+"' class='zip'>"+home_primary_zip+"</td>";
				appendRowData+="<td class='homeState' id='appscr57HouseHoldhome_primary_stateTD"+(i+1)+"'>"+home_primary_state+"</td>";
				appendRowData+="<td class='homeCounty'  id='appscr57HouseHoldhome_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>";


				appendRowData+="<td id='appscr57HouseHoldmailing_addressLine1TD"+(i+1)+"' class='mailing_addressLine1'>"+mailing_addressLine1+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_addressLine2TD"+(i+1)+"' class='mailing_addressLine2'>"+mailing_addressLine2+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_primary_cityTD"+(i+1)+"' class='mailingCity'>"+mailing_primary_city+"</td>";
				appendRowData+="<td id='appscr57HouseHoldmailing_primary_zipTD"+(i+1)+"' class='mailingZip'>"+mailing_primary_zip+"</td>";
				appendRowData+="<td class='mailingState' id='appscr57mailing_primary_stateTD"+(i+1)+"'>"+mailing_primary_state+"</td>";
				appendRowData+="<td class='mailingCounty'  id='appscr57HouseHoldmailing_primary_countyTD"+(i+1)+"'>"+mailing_primary_county+"</td>";

				appendRowData+="<td class='houseHoldPhoneFirst'  id='appscr57houseHoldPhoneFirstTD"+(i+1)+"'>"+houseHoldPhone1+"</td>";
				appendRowData+="<td class='houseHoldPhoneExtFirst'  id='appscr57houseHoldPhoneExtFirstTD"+(i+1)+"'>"+houseHoldPhoneExt1+"</td>";
				appendRowData+="<td class='houseHoldPhoneTypeFirst'  id='appscr57houseHoldPhoneTypeFirstTD"+(i+1)+"'>"+houseHoldPhoneType1+"</td>";
				appendRowData+="<td class='houseHoldPhoneSecond'  id='appscr57houseHoldPhoneSecondTD"+(i+1)+"'>"+houseHoldPhone2+"</td>";
				appendRowData+="<td class='houseHoldPhoneExtSecond'  id='appscr57houseHoldPhoneExtSecondTD"+(i+1)+"'>"+houseHoldPhoneExt2+"</td>";
				appendRowData+="<td class='houseHoldPhoneTypeSecond'  id='appscr57houseHoldPhoneTypeSecondTD"+(i+1)+"'>"+houseHoldPhoneType2+"</td>";
				appendRowData+="<td class='houseHoldSpokenLanguage'  id='appscr57houseHoldSpokenLanguageTD"+(i+1)+"'>"+houseHoldSpokenLanguag+"</td>";
				appendRowData+="<td class='houseHoldWrittenLanguage'  id='appscr57houseHoldWrittenLanguageTD"+(i+1)+"'>"+houseHoldWrittenLanguag+"</td>";

				appendRowData+="<td class='liveAtOtherAddressIndicatorClass' id='appscr57liveAtOtherAddressIndicatorTD"+(i+1)+"'>"+liveAtOtherAddressIndicator+"</td>";
				appendRowData+="<td class='isTemporarilyLivingOutsideIndicatorClass' id='appscr57isTemporarilyLivingOutsideIndicatorTD"+(i+1)+"'>"+isTemporarilyLivingOutsideIndicator+"</td>";
				appendRowData+="<td id='appscr57otherAddressState"+(i+1)+"' class='otherAddressState'>"+otherAddressOfHouseHold+"</td>";
				}

			$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+(i+1)).append(appendRowData);

			});
			addSpecialCircumstances();


	}




	function getNameByTRIndex(index)
	{


		return getNameByPersonId(index);

		/*var houseHoldName='';

	$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {

		var pureNameOfUser='';


		 if(i==index)
			 {
				 $("td", this).each(function( j ) {
					if(j<3)
						pureNameOfUser+= $(this).text();
					 if(j<2)
						 pureNameOfUser+='_';
					  });


				houseHoldName=pureNameOfUser;
			 }



		});

		return houseHoldName;*/
	}

	function getNameRemovedBySpace(userNameId)
	{

		var removedScoreId = userNameId.replace(/_/g," ");

		return removedScoreId;
	}

	function setAddressDetailsToSpan()
	{
		$('#houseHoldAddressDiv').html('');

		var homeAddress = "";
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
		var homeAddress = householdMemberObj.householdContact.homeAddress.streetAddress1;
		if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
			homeAddress += " " + householdMemberObj.householdContact.homeAddress.streetAddress2;
		if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
			homeAddress += " " + householdMemberObj.householdContact.homeAddress.city;
		if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
			homeAddress += " " + householdMemberObj.householdContact.homeAddress.state;

		/*
		var home_addressLine1=$('#home_addressLine1').val();
		var home_addressLine2=$('#home_addressLine2').val();
		var home_primary_city=$('#home_primary_city').val();
		var home_primary_state=$('#home_primary_state').val();
		*/

		$('#houseHoldAddressDiv').append(homeAddress);
	}


	function setHouseHoldContact()
	{

		$('#houseHold_Contact_span1').html('');
		$('#houseHold_Contact_span1').html('');
		$('#houseHold_Contact_span1').html('');


		if($('#middleName').val() == "Middle Name")
			$('#middleName').val('');

		if($('#lastName').val() == "Last Name")
			$('#lastName').val('');

		var name= $('#firstName').val() + " " + $('#middleName').val() + " " + $('#lastName').val();

		$('#houseHold_Contact_span1').html(name);
		$('#houseHold_Contact_span2').html(name);
		$('#houseHold_Contact_span3').html(name);

		/*validation of appscr54*/
		if(validation(currentPage)==false){
			return false;
		}

		next();

	}


	function apppscr60SetGenderInSSAP(){

		setHisOrHerAfterContinue();
	}



	function checkUSCitizenForNextPage()
	{
		if($('input[name=UScitizen]:radio:checked').val()=='yes'){

			next();
		}
	}

	function cleanAppscr61() {
		$('#UScitizenIndicatorYes').prop("checked", false);
		$('#UScitizenIndicatorNo').prop("checked", false);
		$("#naturalizedCitizenDiv").hide();
		$("#naturalizedCitizenDiv").hide();
		$("#documentTypeDiv").hide();
		$("#eligibleImmigrationStatusDiv").hide();
		//fillAppscr61(nonFinancialHouseHoldDone);

	}

	function  fillAppscr61(i) {

		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		var editHouseHoldgender 				= householdMemberObj.gender; //$('#appscrHouseHoldgender57TD'+i).text();
		var editHouseHoldHavingSSN  			= householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator; //$('#appscr57HouseHoldHavingSSNTD'+i).text();
		var editHouseHoldSSN 					= householdMemberObj.socialSecurityCard.socialSecurityNumber; //$('#appscr57HouseHoldSSNTD'+i).text();
		var editsameNameInSSNCardIndicator		= householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator; //$('#appscr57SameNameInSSNCardIndicatorTD'+i).text();
		var editfirstNameOnSSNCard				= householdMemberObj.socialSecurityCard.firstNameOnSSNCard; //$('#appscr57FirstNameOnSSNCardTD'+i).text();
		var editmiddleNameOnSSNCard				= householdMemberObj.socialSecurityCard.middleNameOnSSNCard; //$('#appscr57MiddleNameOnSSNCardTD'+i).text();
		var editlastNameOnSSNCard				= householdMemberObj.socialSecurityCard.lastNameOnSSNCard; //$('#appscr57LastNameOnSSNCardTD'+i).text();
		var editsuffixOnSSNCard					= householdMemberObj.socialSecurityCard.suffixOnSSNCard; //$('#appscr57SuffixOnSSNCardTD'+i).text(); //WE DON'T HAVE SUFFIX IN JSON
		var editnotAvailableSSNReason			= householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN; //$('#appscr57notAvailableSSNReasonTD'+i).text();


		/*var ssn1 = editHouseHoldSSN.split("-")[0];
		var ssn2 = editHouseHoldSSN.split("-")[1];
		var ssn3 = editHouseHoldSSN.split("-")[2];*/

		var houseHoldname = householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName; //$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

		/*This causes an infinity loop*/
		/*if(editHouseHoldSSN == "" || editHouseHoldSSN == null){
			resetAddNonFinancialDetails();
		}*/

		$('.nameOfHouseHold').text(houseHoldname);

		if(editHouseHoldgender != "" && editHouseHoldgender != undefined){
			if(editHouseHoldgender.toLowerCase() == 'male')
			{
				$('#appscr61GenderMaleID').prop('checked',true);
				apppscr60SetGenderInSSAP();

			}
			else if(editHouseHoldgender.toLowerCase() == 'female')
			{
				$('#appscr61GenderFemaleID').prop('checked',true);
				apppscr60SetGenderInSSAP();

			}
		}
		if(editHouseHoldHavingSSN == 'yes' || editHouseHoldHavingSSN == 'true' || editHouseHoldHavingSSN == true )
		{
			$('#socialSecurityCardHolderIndicatorYes').prop('checked',true);
			askForSSNNoPage61();
		}
		else if(editHouseHoldHavingSSN == 'no' || editHouseHoldHavingSSN == 'false' || editHouseHoldHavingSSN === false )
		{
			$('#socialSecurityCardHolderIndicatorNo').prop('checked',true);

			validation('appscr61');
			askForSSNNoPage61();
		}

		/*$('#ssn1').val(ssn1);
		$('#ssn2').val(ssn2);
		$('#ssn3').val(ssn3);*/

		if(editHouseHoldSSN){
			//$("#ssn").attr('data-ssn',editHouseHoldSSN).val("***-**-" + editHouseHoldSSN.substring(5));
			$("#ssn").val("***-**-" + editHouseHoldSSN.substring(5));
		}

		if(editsameNameInSSNCardIndicator == "yes" || editsameNameInSSNCardIndicator == 'true' || editsameNameInSSNCardIndicator == true )
			$('#fnlnsSameIndicatorYes').prop("checked", true);
		else if(editsameNameInSSNCardIndicator == "no" || editsameNameInSSNCardIndicator == 'false' || editsameNameInSSNCardIndicator === false )
		{

			$('#fnlnsSameIndicatorNo').prop("checked", true);
			$('#firstNameOnSSNCard').val(editfirstNameOnSSNCard);
			$('#middleNameOnSSNCard').val(editmiddleNameOnSSNCard);
			$('#lastNameOnSSNCard').val(editlastNameOnSSNCard);
			$('#suffixOnSSNCard').val(editsuffixOnSSNCard);
		}

		hasSameSSNNumberP61();
		if(editnotAvailableSSNReason == 0)
			$('#reasonableExplanationForNoSSN').val('');
		else
			$('#reasonableExplanationForNoSSN').val(editnotAvailableSSNReason);
	}

	function cleanAppscr63() {
		$('input').prop("checked", false);
		$(".parentCareTaker, .parentOrCaretakerBasicDetailsContainer").hide();
	}

	function cleanAppscr67() {
		$('#appscr61GenderMaleID').prop("checked", false);
		$('#appscr61GenderFemaleID').prop("checked", false);
		$('#socialSecurityCardHolderIndicatorYes').prop("checked", false);
		$('#socialSecurityCardHolderIndicatorNo').prop("checked", false);
		$("#DontaskAboutSSNOnPage61").hide();
		$("#askAboutSSNOnPage61").hide();
		/*$("#ssn1").val("");
		$("#ssn2").val("");
		$("#ssn3").val("");*/
		$("#ssn").val("");
		$('#fnlnsSameIndicatorYes').prop("checked", false);
		$('#fnlnsSameIndicatorNo').prop("checked", false);
		$("#sameSSNPage61Info").hide();
	}

	function  fillAppscr64(i) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var editethnicityIndicator =  householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator;
		var houseHoldname = getNameByPersonId(i);
		$('.nameOfHouseHold').html(""+houseHoldname);
		if(editethnicityIndicator == true){
			$('#checkPersonNameLanguageYes').prop("checked", true);

			for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.ethnicity.length; cnt++){
				$('input:checkbox[name="ethnicity"]').each(function(){
					if($(this).val() == householdMemberObj.ethnicityAndRace.ethnicity[cnt].code){
						$(this).prop('checked',true);

						//for other ethnicity
						if($(this).val() == '0000-0'){

							$('#otherEthnicity').val(householdMemberObj.ethnicityAndRace.ethnicity[cnt].label);
						}
					}
				});
			}



		}else if(editethnicityIndicator == false){
			$('#checkPersonNameLanguageNo').prop("checked", true);
		}

		displayOtherEthnicity();
		checkLanguage();


		for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.race.length; cnt++){
			$('input:checkbox[name="race"]').each(function(){
				if($(this).val() == householdMemberObj.ethnicityAndRace.race[cnt].code){

					$(this).prop('checked',true);
					//for other race
					if($(this).val() == '2131-1'){
						$('#otherRace').val(householdMemberObj.ethnicityAndRace.race[cnt].label);
					}
				}
			});
		}

		displayOtherRace();

	}

	function getCountyListByZipCode(applicantindex,countyCode) {
		var zipCode = $('#zip'+applicantindex).val();
		var stateCode = $('#applicant_or_non-applican_state'+ applicantindex +" option:selected").val();

		if(zipCode == "" || stateCode==""){
			$('#applicant_or_non-applican_county'+applicantindex).html('');
			return false;
		}
		$.ajax({
			type : "POST",
			url : theUrlForAddCountiesForStates,
			data : {
				zipCode : zipCode,
				stateCode: stateCode,
				csrftoken : csrftoken 
			},
			dataType : 'json',
			success : function(response) {

			populateOtherAddressCounties(response,applicantindex,countyCode);
			},
			error : function(e) {
				alert("Failed to Populate County Details");
			}
		});
	}

	function populateOtherAddressCounties(response, applicantindex, countyCode) {
		// console.log(response, eIndex,county,"populate counties")
		$('#applicant_or_non-applican_county'+applicantindex).html('');
		countyCode = typeof countyCode !== 'undefined' ? countyCode : "";
		var optionsstring = '<option value="">County</option>';
		var i = 0;
		$.each(response, function(key, value) {
			var selected = (countyCode == value) ? 'selected' : '';
			var options = '<option value="' + value + '" '+selected+'>' + key + '</option>';
			optionsstring = optionsstring + options;
			i++;
		});

		$('#applicant_or_non-applican_county'+applicantindex).html(optionsstring);
	}



/*
****************************************************************************************************************
* appscr60A.js                                                                                        *
****************************************************************************************************************
*/

	function setCoverageYear()
	{
		 var currentTime = new Date();
		 var year = currentTime.getFullYear();
		 $('.nameOfHouseHold').html(year);
	}

	var bari="";
	function getSpouse(hNumber){

		//resetHouseHoldDetail();


		//spouse: remember what is selected before reset div to html('')
		/*var spouseCheckedId = 0;;
		if(HouseHoldRepeat == 1 && $('#householdSpouseDiv').find("input:checked").length == 1){
			spouseCheckedId = $('#householdSpouseDiv').find("input:checked").attr("id").substring(22);
		}*/

		$('#householdSpouseDiv').html('');
		$('#householdDependantDiv').html('');
		$('#householdFilerDiv').html('');
		$('#claimingTaxFilerDiv').html('');

			//var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
			var noOfHouseHold=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

			/*if(noOfHouseHold<=1)
			{
				$('#appscrIsChildDiv').hide();
				$('#willClainDependents').hide();
				$('#dinplayNoneBlock').hide();
			}
			else
			{*/
				/*$('#appscrIsChildDiv').show();
				$('#willClainDependents').show();
				$('#dinplayNoneBlock').show();*/
			/*}*/


			/*var currenthouseholdMember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[HouseHoldRepeat-1];*/

			//var spouseID = 0;

			/*if(!(currenthouseholdMember.taxFiler.spouseHouseholdMemberId =="" || currenthouseholdMember.taxFiler.spouseHouseholdMemberId ==null )){
				 spouseID = currenthouseholdMember.taxFiler.spouseHouseholdMemberId.substring(9);
				}*/
			/*var spouseID = currenthouseholdMember.taxFiler.spouseHouseholdMemberId;*/
			for(var i=1; i<=noOfHouseHold ; i++){
				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

				//don't add applicant in dependant and spouse list
				if(i == HouseHoldRepeat){
					continue;
				}


				/*if(HouseHoldRepeat != 1 && (householdMemberObj.taxFiler.spouseHouseholdMemberId != 0 && householdMemberObj.taxFiler.spouseHouseholdMemberId != spouseID)){
					continue;
				}*/

				var HouseholdName =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
				var str = "";

				//OR only for TF is not PC ------->  don't add PC if he/she is married and his/her spouse is not TF, also for his/her spouse
				/*if(HouseHoldRepeat != 1 && (householdMemberObj.taxFiler.spouseHouseholdMemberId != 0 && householdMemberObj.personId != spouseID)){

					if(i != spouseID ){ //do not display spouse as dependent
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);
					}
				}else{*/



				/*if(spouseID == i){
					str ='<label class="margin5-l"><input type="radio" id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" required checked="true" parsley-error-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
				} else {*/


					//PC never show in spouse list

					//For PC, show all member in spouse list except himself
					if(HouseHoldRepeat == 1 && i != 1){
						/*if(i == spouseCheckedId){
							str ='<label class="margin5-l"><input type="radio" checked id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" data-parsley-errors-container="#householdContactSpouseSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						}else{*/
							str ='<label class="margin5-l"><input type="radio" id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" data-parsley-errors-container="#householdContactSpouseSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						/*}*/
						$('#householdSpouseDiv').append(str);
					}else if(HouseHoldRepeat != 1 && i != 1 && (householdMemberObj.taxFiler.spouseHouseholdMemberId == 0 || householdMemberObj.taxFiler.spouseHouseholdMemberId == HouseHoldRepeat) ){
						//For TF who is not PC, don't show PC's spouse
						/*if(spouseID == i){
							str ='<label class="margin5-l"><input type="radio" checked id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" required parsley-error-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						}else{*/
							str ='<label class="margin5-l"><input type="radio" id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" data-parsley-errors-container="#householdContactSpouseSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						/*}*/
					/*}*/

						$('#householdSpouseDiv').append(str);
					}
				/*if(i>1 ){ // do not display applicant as dependent
					if(i == 2 && $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'no' ){ //do not display spouse as depedent
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);
					} else if(i>2) {
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);
					}
				}*/

				//	if(i != spouseID ){ //do not display spouse as dependent
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" name="householdContactDependant" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);
			//		}
				/*}*/


				str = '<label class="margin5-l"><input type="radio" id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" data-parsley-errors-container="#householdContactFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
				$('#householdFilerDiv').append(str);
				str = '<label class="margin5-l"><input type="radio" id="claimingTaxFilerSpouse'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeClaimingTaxFilerDivView60A()" name="claimingTaxFilerSpouse" data-parsley-errors-container="#claimingTaxFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
				$('#claimingTaxFilerDiv').append(str);

				/*
					$('#appscr57HouseHoldPersonalInformation tr td').each(function(){
						if($(this).attr("id") == "appscr57FirstNameTD"+i){
							var HouseholdName = $('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
							var str ='<label class="margin5-l"><input type="radio" id="householdContactSpouse'+i+'" onclick="setSpouse()" temp="'+HouseholdName+'" onchange="changeSpouseDivView60A()" value="HouseHold'+i+'" name="householdContactSpouse" required />'+ HouseholdName +'</label>';
							//$('#householdSpouseDiv').append(str);
							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
							$('#householdDependantDiv').append(str);
							str = '<label class="margin5-l"><input type="radio" id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" required />'+ HouseholdName +'</label>';
							$('#householdFilerDiv').append(str);
							str = '<label class="margin5-l"><input type="radio" id="claimingTaxFilerSpouse'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeClaimingTaxFilerDivView60A()" name="claimingTaxFilerSpouse" />'+ HouseholdName +'</label>';
							$('#claimingTaxFilerDiv').append(str);


						}
					});*/
					/*$('#householdSpouseDiv input:first').attr('checked',true);
					$('#householdFilerDiv input:first').attr('checked',true);*/

			}

			var isChild = $('#appscr57IsChildTD'+HouseHoldRepeat).text();
			if(isChild == 'yes'){
				$('#appscrIsChildDiv').hide();
				$('#marriedIndicatorYes').prop('checked',false);

			}
			else{
				$('#appscrIsChildDiv').show();
				/*$('#marriedIndicatorYes').prop('checked',true);*/
			}

			/*if(noOfHouseHold<=1)
			{
				$('#appscrIsChildDiv').hide();
			}*/

			$('#householdSpouseDiv').append('<label class="margin5-l"><input type="radio" id="householdContactSpouseSomeOneChecked" name="householdContactSpouse" onclick="setSpouse()" onchange="changeSpouseDivView60A()" value="someOne" data-parsley-errors-container="#householdContactSpouseSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+'</label><div id="householdContactSpouseSomeOneCheckedErrorDiv"></div>');
			$('#householdDependantDiv').append('<label class="margin5-l capitalize-none"><input type="checkbox" id="householdContactDependantSomeOneChecked" name="householdContactDependant" onchange="changeDependentDivView60A()" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+' </label><div id="householdContactDependantSomeOneCheckedErrorDiv"></div>');
			$('#householdFilerDiv').append('<label class="margin5-l"><input type="radio" name="householdContactFiler" id="householdContactFilerSomeOneChecked" onchange="changeTaxFilerDivView60A()" value="someOne" data-parsley-errors-container="#householdContactFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+' </label><div id="householdContactFilerSomeOneCheckedErrorDiv"></div>');
			$('#claimingTaxFilerDiv').append('<label class="margin5-l"><input type="radio" name="claimingTaxFilerSpouse" id="claimingTaxFilerSomeOneChecked" onchange="changeClaimingTaxFilerDivView60A()" value="someOne" data-parsley-errors-container="#claimingTaxFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+' </label><div id="claimingTaxFilerSomeOneCheckedErrorDiv"></div>');



		if(currentPage == 'appscr59B')
		{
			next();
			setNonFinancialPageProperty(1);
		}


		bari = hNumber;


		setHisOrHer(bari);
		//removeRequiredValidation();
	}


	function setHisOrHer(bari){
		$('#appscr61SSNHisHer').text('');

		if($('input[name="appscr57Sex'+bari+'"]:radio:checked').val()== 'male'){

			$('#appscr61SSNHisHer').text('his');
			$('.onDependantHisHer').text('his');
		}
		else{
			$('#appscr61SSNHisHer').text('her');
			$('#appscr60JointlyHisHer').text('her');
		}
	}



	function householdPayJointly(){

		var count = 0;
		/*
		$('#appscr57HouseHoldPersonalInformation tr').each(function(){

			count++;
		});
		*/
		count=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
		var jointSpouseName = $('input[name=householdContactSpouse]:radio:checked').attr("temp");

		if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val()== 'yes' && jointSpouseName){
			//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			$(".householdContactSpouseName").text("and "+jointSpouseName);
			/*var i=1;

			$('.onDependantHisHer').text('their');

					$('#householdDependantDiv').html('');

					for(var i=1; i<=count ; i++){
						var householdContact = "HouseHold" + i;
						if(i>2  && householdContact != $('input[name=householdContactSpouse]:radio:checked').val()){
							householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
							var HouseholdName =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
							$('#householdDependantDiv').append(str);

						}

					}


					$('#householdDependantDiv').append('<label class="margin5-l"><input type="checkbox" name="householdContactDependant'+i+'" />'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+'</label>');*/
		}
		else{
			householdNotPayJointly();
			setHisOrHer(bari);
		}



	}

	function householdNotPayJointly(){

		$(".householdContactSpouseName").text('');
		/*var i=1;


				$('#householdDependantDiv').html('');

				count=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

				for(var i=1; i<=count ; i++){
					var householdContact = "HouseHold" + i;
					if(i != HouseHoldRepeat){
						householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
						var HouseholdName =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);
					}
				}

				$('#householdDependantDiv').append('<label class="margin5-l"><input type="checkbox" name="householdContactDependant'+i+'" />'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+'</label>');
			*/


	}
	//Keys
	function isMarried(){

		var count = 0;
		count=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		$('#householdSpouseMainDiv').show();
		setSpouse();
		if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val()== 'yes' && count>1 ){
			$(".householdContactSpouseName").text("and "+$('input[name=householdContactSpouse]:radio:checked').attr("temp"));
		/*	var i=1;
			$('.onDependantHisHer').text('their');

				$('#householdDependantDiv').html('');

				for(var i=1; i<=count ; i++){
					var householdContact = "HouseHold" + i;
					if(i>2  && householdContact != $('input[name=householdContactSpouse]:radio:checked').val()){
						householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
						var HouseholdName =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						$('#householdDependantDiv').append(str);

					}

				}


				$('#householdDependantDiv').append('<label class="margin5-l"><input type="checkbox" name="householdContactDependant'+i+'" />'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+' </label>');*/
		}
		else{
			householdNotPayJointly();
			setHisOrHer(bari);
		}
	}

	function isNotMarried(){
		$('#householdSpouseMainDiv').hide();

		//$("#householdSpouseMainDiv input").prop("checked",false);

		setSpouse();

		householdNotPayJointly();
	}
	//Keys
	function setSpouse(){

		householdPayJointly();

			var count = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

			//remember what are selected before reset div to html('')
			var claimDependentsIdArr = [];
			$("#householdDependantDiv").find("input:checked").each(function(){
				claimDependentsIdArr.push($(this).attr("id").substring(25));
			});


			var claimDependentDisabledId = 0;
			//TF is not PC, and will claim PC. We need tto disable TC
			if(HouseHoldRepeat != 1 && $('#householdDependantDiv').find("input:disabled").length == 1){
				claimDependentDisabledId = $('#householdDependantDiv').find("input:disabled").attr("id").substring(25);
			}

			var claimedDependentId = 0;
			$("#householdFilerDiv").find("input:checked").each(function(){
				claimedDependentId = $(this).attr("id").substring(21);
			});

			$('#householdDependantDiv').html('');
			$('#householdFilerDiv').html('');

			//remove all spouse id if in PC tax page, will build it again when click continue
			/*if(HouseHoldRepeat == 1){
				for(var i=1; i<=count ; i++){
					webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1].taxFiler.spouseHouseholdMemberId = 0;
				}
			}
				*/
			for(var i=1; i<=count ; i++){
				//don't add applicant to dependent list
				if(i == HouseHoldRepeat){
					continue;
				}
				var householdContact = "HouseHold" + i;

				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				var HouseholdName =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

				//if married, and equal to spouse checkbox which is checked, skip
				if(!($("#marriedIndicatorYes").is(":checked") && householdContact == $('input[name=householdContactSpouse]:radio:checked').val())){
					//pre-select checkbox
					if(claimDependentsIdArr.indexOf(i+"") != -1){
						//pre-disable checkbox
						if(claimDependentDisabledId == i){
							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" disabled checked name="householdContactDependant" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						}else{
							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" checked name="householdContactDependant" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
						}
					}else{
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" name="householdContactDependant" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+ HouseholdName +'</label>';
					}

					$('#householdDependantDiv').append(str);

					if(claimedDependentId == i){
						str = '<label class="margin5-l"><input type="radio" checked id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" data-parsley-errors-container="#householdContactFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" />'+ HouseholdName +'</label>';
					}else{
						str = '<label class="margin5-l"><input type="radio" id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" data-parsley-errors-container="#householdContactFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" />'+ HouseholdName +'</label>';
					}
					$('#householdFilerDiv').append(str);

					//if not married: add all member to dependent list
				}/*else if($("#marriedIndicatorNo").is(":checked")){
					//pre-select checkbox
					if(claimDependentsIdArr.indexOf(i+"") != -1){
						//pre-disable checkbox
						if(claimDependentDisabledId == i){
							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" disabled checked name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						}else{
							str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" checked name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
						}
					}else{
						str = '<label class="margin5-l"><input type="checkbox" id="householdContactDependant'+i+'" temp='+HouseholdName+' value="HouseHold'+i+'" name="householdContactDependant'+i+'"/>'+ HouseholdName +'</label>';
					}

					$('#householdDependantDiv').append(str);

					if(claimedDependentId == i){
						str = '<label class="margin5-l"><input type="radio" checked id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" required />'+ HouseholdName +'</label>';
					}else{
						str = '<label class="margin5-l"><input type="radio" id="householdContactFiler'+i+'" temp="'+HouseholdName+'" value="HouseHold'+i+'" onchange="changeTaxFilerDivView60A()" name="householdContactFiler" required />'+ HouseholdName +'</label>';
					}
					$('#householdFilerDiv').append(str);
				}*/

			}


			$('#householdDependantDiv').append('<label class="margin5-l capitalize-none"><input type="checkbox" id="householdContactDependantSomeOneChecked" onchange="changeDependentDivView60A()" name="householdContactDependant" data-parsley-errors-container="#householdContactDependantSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+'</label><div id="householdContactDependantSomeOneCheckedErrorDiv"></div>');
			$('#householdFilerDiv').append('<label class="margin5-l"><input type="radio" name="householdContactFiler" id="householdContactFilerSomeOneChecked" onchange="changeTaxFilerDivView60A()" value="someOne" data-parsley-errors-container="#householdContactFilerSomeOneCheckedErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'"/>'+jQuery.i18n.prop("ssap.someoneElseNotSeeking")+' </label><div id="householdContactFilerSomeOneCheckedErrorDiv"></div>');


	}

	function createTaxRelation(){

		while($('#claimDependantsRow'+HouseHoldRepeat).text().length != 0)
		if($('#claimDependantsRow'+HouseHoldRepeat).text() != undefined)
			$('#claimDependantsRow'+HouseHoldRepeat).remove();

		if($('input[name=householdHasDependant]:radio:checked').val()== 'yes'){

			$("#householdDependantDiv input:checkbox:checked").each(function(){

				var appendRow="<tr id='claimDependantsRow"+HouseHoldRepeat+"'>";
				appendRow+="<td >HouseHold"+HouseHoldRepeat+"</td>";
				appendRow+="<td >"+$(this).val()+"</td>";
				appendRow+="</tr>";
				$('#appscr60TaxRelation').append(appendRow);

			});

	}

	}


	function setTaxRelatedData()
	{

		if($('#appscr57isMarriedTD'+HouseHoldRepeat).html() !=undefined)
			{
			updateTaxRelatedData();
			return false;
			}

		var isMarried=false;
		var spouseId=false;
		var payJointly = false;
		var isTaxFiler = false;
		var isLiveWithSpouse = false;
		var isClaimDependants = false;

		var isInfant = $('#appscr57IsInfantTD'+(HouseHoldRepeat)).html();
		if(isInfant =='no')
		{
		if($('input[name=planToFileFTRIndicator]:radio:checked').val()== 'yes'){
			isTaxFiler = true;
		}

		if($('input[name=marriedIndicator]:radio:checked').val()== 'yes'){
			isMarried = true;
			spouseId=$('input[name=householdContactSpouse]:radio:checked').val();
			if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val()== 'yes'){
				payJointly = true;
			}
			if($('input[name=liveWithSpouseIndicator]:radio:checked').val()== 'yes'){
				isLiveWithSpouse = true;
			}
		}

		if($('input[name=householdHasDependant]:radio:checked').val()== 'yes')
			isClaimDependants = true;

		}

		var appendRowData='';
		appendRowData+="<td id='appscr57isMarriedTD"+(HouseHoldRepeat)+"' class='i_married'>"+isMarried+"</td>";
		appendRowData+="<td id='appscr57spouseIdTD"+(HouseHoldRepeat)+"' class='i_spouseId'>"+spouseId+"</td>";
		appendRowData+="<td id='appscr57payJointlyTD"+(HouseHoldRepeat)+"' class='i_payJointly'>"+payJointly+"</td>";
		appendRowData+="<td id='appscr57isTaxFilerTD"+(HouseHoldRepeat)+"' class='payer'>"+isTaxFiler+"</td>";
		appendRowData+="<td id='appscr57isLiveWithSpouseTD"+(HouseHoldRepeat)+"' class='liveWithSpouse'>"+isLiveWithSpouse+"</td>";
		appendRowData+="<td id='appscr57isClaimDependantsTD"+(HouseHoldRepeat)+"' class='isClaimDependants'>"+isClaimDependants+"</td>";

			$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+(HouseHoldRepeat)).append(appendRowData);



	}


	function setSpouseShowHide(){
		if($('input[name=isClaimingTaxFillerMarried]:radio:checked').val()== 'yes'){
			$('#claimingFilerSpouseDiv').show();
		}
		else{
			$('#claimingFilerSpouseDiv').hide();
		}

	}

	function showhideParagraph(){
		if($('input[name=doesPlanForTaxReturn]:radio:checked').val()== 'unkonwn'){
			$('#unknownParagraph').show();
		}
		else{
			$('#unknownParagraph').hide();
		}
	}

	function doesFilerHasTaxFiller(){
		if($('input[name=doesApplicantHasTaxFiller]:radio:checked').val()== 'yes'){
			$('#appscr60ClaimedFilerDiv').show();
		}
		else{
			$('#appscr60ClaimedFilerDiv').hide();
		}
	}


	function resetHouseHoldDetail()
	{
		$('.householdContactName').text('');
		$('#householdSpouseDiv').html('');
		$('#householdDependantDiv').html('');
		$('#householdFilerDiv').html('');
		$('#claimingTaxFilerDiv').html('');
		$('#appscr60Form input:text').val('');
		$('#appscr60JointlyHisHer').text('');
		$('.onDependantHisHer').text('');



		/*$('#planToFileFTRIndicatorYes').prop('checked',true);
		$('#marriedIndicatorYes').prop('checked',true);

		$('#planOnFilingJointFTRIndicatorNo').prop('checked',true);
		$('#liveWithSpouseIndicatorYes').prop('checked',true);
		$('#householdHasDependantNo').prop('checked',true);
		$('#doesApplicantHasTaxFillerNo').prop('checked',true);*/
		$('#appscr60ClaimedFilerDiv').hide();
		$('#householdSpouseMainDiv').show();
		$('#householdDependentMainDiv').hide();

		$('#appscr60Form input:radio').each(function(){
			if (!$(this).is(':checked')){
				$(this).prop('checked',false);
			}

		});

	}

	function setValueToRadioTextbox(){
		$('input[name=householdContactSpouse]').each(function(){
			var selectedValue = $('#resultingTableOfSaveDataJS tr .i_spouseId').text();
			if($(this).val() === selectedValue){
			    $(this).prop('checked',true);
			}
		});

		if($('#resultingTableOfSaveDataJS tr .doesApplicantHasTaxFiller').text() === "yes"){
			doesFilerHasTaxFiller();
		}
	}

	function planToFileYes(){
		$("#marriedIndicatorDiv, #willClaimDependents").show();

		if($("#householdHasDependantYes").is(":checked")){
			$("#householdDependentMainDiv").show();
			$("#willBeClaimed, #appscr60ClaimedFilerDiv").hide();

			if($("#householdContactDependantSomeOneChecked").is(":checked")){
				$("#dependentDetailDiv_inputFields").show();
			}
		}else{
			$("#willBeClaimed").show();
		}

		$("#planOnFilingJointFTRDiv").show();
		householdPayJointly();
	}

	function planToFileNo(){
		$("#marriedIndicatorDiv, #willBeClaimed").show();
		$("#willClaimDependents, #householdDependentMainDiv, #dependentDetailDiv_inputFields").hide();

		if($("#doesApplicantHasTaxFillerYes").is(":checked")){
			doesFilerHasTaxFiller();
		}

		$("#planOnFilingJointFTRDiv").hide();
		householdNotPayJointly();

	}






/*
****************************************************************************************************************
* addFinancialDetails.js                                                                                        *
****************************************************************************************************************
*/
	var incomeNumber = 0;
	var deductionNumber = 0;
	var totalIncomes = new Array();
	var totalIncomeDeductions = new Array();
	var job_income_name='';


	var financial_DetailsArray = new Array();
	financial_DetailsArray[0] = 'Financial_NoneFinancial_None';
	financial_DetailsArray[1]=  'Financial_Job';
	financial_DetailsArray[2] = 'Financial_Self_Employment';
	financial_DetailsArray[3] = 'Financial_SocialSecuritybenefits';
	financial_DetailsArray[4] = 'Financial_Unemployment';
	financial_DetailsArray[5] = 'Financial_Retirement_pension';
	financial_DetailsArray[6] = 'Financial_Capitalgains';
	financial_DetailsArray[7] = 'Financial_InvestmentIncome';
	financial_DetailsArray[8] = 'Financial_RentalOrRoyaltyIncome';
	financial_DetailsArray[9] = 'Financial_FarmingOrFishingIncome';
	financial_DetailsArray[10] = 'Financial_AlimonyReceived';
	financial_DetailsArray[11] = 'Financial_OtherIncome';

	var indicationIncomeObjectArray = new Array();
	var incomeAmount = new Object();
	var incomeFrequency = new Object();



var financialHouseHoldDone = 1;

function addFinancialHouseHold() {

	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	/*$('.parsley-error-list').hide();*/
	if((currentPage == "appscr73" || ($('input[name=appscr70radios4Income]:radio:checked').val() == 'No') && currentPage == "appscr70") && (editFlag == true || flagToUpdateFromResult== true) && useJSON == false)					// Edit functionality code
	{
			appscr74EditUpdateValues(editMemberNumber);
			showFinancialDetailsSummary();

			if(flagToUpdateFromResult == true)
				{
				flagToUpdateFromResult=false;
				if(checkStillUpdateRequired())
					{

					return false;
					}
				else
					{
						goToPageById('appscrBloodRel');
						$('#finalEditDiv').show();
						$('#contBtn').text('Submit');
						$('#prevBtn').hide();
					return false;
					}

				}
			else
			goToPageById('appscr74');
			editFlag = false;
			return false;
	}
	else if(editFlag == true || flagToUpdateFromResult== true)
	{
		$('#back_button_div').show();
		if(editMemberNumber == 1)
			job_income_name = $("#Financial_Job_EmployerName_id").val();
	}

	if(validation(currentPage)==false)
		return false;
	$('#prevBtn').attr('onclick','prev();');

	$('#goBack').show();
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());

	if (currentPage == 'appscr71') {


		if(editFlag == false)
		{
			if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined ){
				totalIncomeAt71(financialHouseHoldDone); }
			else
			{
				editFlag = true;
				totalIncomeAt71(financialHouseHoldDone-1);
				editFlag = false;
			}

		}
		else
		{
			totalIncomeAt71(financialHouseHoldDone);

		}
	}
	else if(currentPage == 'appscr70')
		{

			if($('#Financial_None').is(":checked") == true)
			{

				if(editFlag == true)
				{

					appscr74EditUpdateValues(editMemberNumber);
					showFinancialDetailsSummary();
					goToPageById('appscr74');
					editFlag = false;
					return false;
				}
				totalIncomeAt71(financialHouseHoldDone);

				continueData71();
				continueData72();
				continueData73();
				next();


				if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined )
				{
				addCurrentIncomeDetail();
				}
				else
					{
					updateCurrentIncomeDetail();
					}
				setJobIncomeExistDetails();
				next();

				if( $("#incomesStatus"+financialHouseHoldDone).html() == undefined)
				{
				setFinancialIncomeDetails();
				incomeDidcrepancies();
				}
				else
				{
				updateFinancialIncomeDetails();
				updateIncomeDiscrepancies();
				}

				if (currentPage == 'appscr73' && financialHouseHoldDone < noOfHouseHold) {
					financialHouseHoldDone++;
					resetAddCurrentIncomeDetail();
					resetPage71Values();
					resetPage69Values();

					if(editFlag == true)
					{
						showFinancialDetailsSummary();
						next();
						editFlag = false;
					}else{
						goToPageById('appscr69');
					}

					//if(havaData == true)
					//{
						haveDataFlag = true;
						if(financialHouseHoldDone<=houseHoldNumberValue)
							appscr74EditController(financialHouseHoldDone);
						haveDataFlag = false;
					//}
				}
				else
					{
						showFinancialDetailsSummary();
						if(totalIncomes.length == noOfHouseHold && currentPage == 'appscr73')
							totalIncomeShowMethod();
						setEmployerNameToAll();
						next();

					}

				setNonFinancialPageProperty(financialHouseHoldDone);
			}
			else
				showFinancialDiv();
		}
	else if(currentPage == 'appscr72')
		{
		if(editFlag ==  false)
		{
			if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined )
				{

					addCurrentIncomeDetail();
				}


			else
				{

				updateCurrentIncomeDetail();
				}
		}
			setJobIncomeExistDetails();
		}

	else if(currentPage == 'appscr73')
	{


		if( $("#incomesStatus"+financialHouseHoldDone).html() == undefined)
		{
		setFinancialIncomeDetails();
		incomeDidcrepancies();

		}
		else
		{
		updateFinancialIncomeDetails();
		updateIncomeDiscrepancies();
		}
	}

	if (currentPage == 'appscr73' && financialHouseHoldDone < noOfHouseHold) {


		financialHouseHoldDone++;

		resetAddCurrentIncomeDetail();
		 resetPage71Values();
		 resetPage69Values();
		 resetPage72DeductionValues();
		 if(editFlag == true)
			{
				showFinancialDetailsSummary();
				next();
				editFlag = false;
			}else{
				goToPageById('appscr69');
			}



		//if(haveData == true)
		//{
			haveDataFlag = true;

			if(useJSON == true){
					appscr74EditController(financialHouseHoldDone);
			}else {
				if(financialHouseHoldDone<=houseHoldNumberValue)
					appscr74EditController(financialHouseHoldDone);
			}


			haveDataFlag = false;
		//}
	}
	else
		{

			showFinancialDetailsSummary();

			if(totalIncomes.length == noOfHouseHold && currentPage == 'appscr73')
				totalIncomeShowMethod();


			setEmployerNameToAll();

		next();

		}

	setNonFinancialPageProperty(financialHouseHoldDone);

}

function showFinancialDiv() {
	var incomeCBStatus = new Object();
	var dataIndex=0;

	hideAllFinancial(financial_DetailsArray);

	for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		var data = financial_DetailsArray[dataIndex];



		if(dataIndex==0)
			{
			if ($('#'+data).is(':checked')) {
				hideAllFinancial(financial_DetailsArray);
				var finData=0;
				for ( ;finData < financial_DetailsArray.length ; finData++) {

					if(finData==0)
						continue;
					var myData = financial_DetailsArray[finData];


					$('#' + myData + '_id').val('');

					$('#' + myData + '_select_id').val(jQuery.i18n.prop('ssap.SelectFrequency')); //KEY

				}




				break;
			}
			else
				{
				continue;
				}
			}


		if ($('#' + data).is(':checked')) {
			incomeCBStatus[data] = "true";

			$('#' + data + '_Div').show();
			//$('#' + data + '_Div').css("margin-bottom","160px");
		}
		else
			{
			incomeCBStatus[data] = "false";
			incomeAmount[data]=0;
			incomeFrequency[data]='N/A';

			$('#' + data + '_id').val('');
			$('#' + data + '_select_id').val(jQuery.i18n.prop('ssap.SelectFrequency')); //KEY

			$('#' + data + '_Div').hide();
			$('#' + data + '_Div').css("margin-bottom","2%");
			}

	}

	//var RowId =($('#houseHoldTRNo').val()).substring(9);
	var RowId = getCurrentApplicantID();
	indicationIncomeObjectArray[parseInt(RowId)-1]=incomeCBStatus;

}

function hideAllFinancial(financial_DetailsArray){


	var dataI=0;
	for ( ;dataI < financial_DetailsArray.length ; dataI++) {

		if(dataI==0)
			continue;
		var data = financial_DetailsArray[dataI];
		$('#' + data + '_Div').hide();

	}

}
var financial_styleClass = new Array();

financial_styleClass[0] = "class='income'";
financial_styleClass[1] = "class='i_job'";
financial_styleClass[2] = "class='i_self'";
financial_styleClass[3] = "class='i_social'";
financial_styleClass[4] = "class='i_unemploy'";
financial_styleClass[5] = "class='i_pension'";
financial_styleClass[6] = "class='i_capital'";
financial_styleClass[7] = "class='i_investment'";
financial_styleClass[8] = "class='i_rental'";
financial_styleClass[9] = "class='i_farming'";
financial_styleClass[10] = "class='i_alimony'";
financial_styleClass[11] = "class='i_other'";


function setFinancialIncomeDetails()
{



		var dataIndex=0;
		var appendRowData='';

		appendRowData+="<td "+styleClass+" id='incomesStatus"+financialHouseHoldDone+"'>"+$('input[name=appscr70radios4Income]:radio:checked').val()+"</td>";

		for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {

		var styleClass=financial_styleClass[dataIndex];
		if(dataIndex==0)
		{
			var irsIncome= $('#irsIncome').val();
				irsIncome = (irsIncome == '')?0:irsIncome;
		appendRowData+="<td "+styleClass+" id='appscrirsIncome57TD"+financialHouseHoldDone+"'>"+irsIncome+"</td>";

		$('#irsIncome').val('');

		continue;
		}
		var data = financial_DetailsArray[dataIndex];
		var fieldData = $('#'+data+'_id').val();

		if(fieldData == '' || fieldData == undefined)
			{

			fieldData = 0;
			}

		incomeAmount[data]=fieldData;


		var fieldFrequencyData= $('#'+data+'_select_id').val();

		incomeFrequency[data]=fieldFrequencyData;

		if(dataIndex == 1)
		{
			appendRowData+="<td class='employerName' id='appscr"+data+"EmployerName57TD"+financialHouseHoldDone+"'>"+$('#Financial_Job_EmployerName_id').val()+"</td>";
			appendRowData+="<td class='hoursOrDays' id='appscr"+data+"HoursOrDays57TD"+financialHouseHoldDone+"'>"+$('#Financial_Job_HoursOrDays_id').val()+"</td>";
		}
		else if(dataIndex == 2)
			appendRowData+="<td class='typeOfWork' id='appscr"+data+"TypeOfWork57TD"+financialHouseHoldDone+"'>"+$('#Financial_Self_Employment_TypeOfWork').val()+"</td>";
		else if(dataIndex == 4)
		{
			appendRowData+="<td class='stateGovt' id='appscr"+data+"StateGovernment57TD"+financialHouseHoldDone+"'>"+$('#Financial_Unemployment_StateGovernment').val()+"</td>";
			appendRowData+="<td class='unemploymentIncomeDate' id='appscr"+data+"Date57TD"+financialHouseHoldDone+"'>"+$('#unemploymentIncomeDate').val()+"</td>";
		}
		else if(dataIndex == 11)
			appendRowData+="<td class='otherTypeIncome' id='appscr"+data+"OtherTypeIncome57TD"+financialHouseHoldDone+"'>"+$('#Financial_OtherTypeIncome').val()+"</td>";


			appendRowData+="<td "+styleClass+" id='appscr"+data+"57TD"+financialHouseHoldDone+"'>"+fieldData+"</td>";

			if(dataIndex!=2 && dataIndex!=6)
				appendRowData+="<td class='"+data+"Frequency' id='appscr"+data+"Frequency57TD"+financialHouseHoldDone+"'>"+fieldFrequencyData+"</td>";
		}
		/*var expectedIncomeByHouseHoldMember= $('#expectedIncomeByHouseHoldMember').val();
		if(expectedIncomeByHouseHoldMember=='')
			expectedIncomeByHouseHoldMember="N/A";
		appendRowData+="<td class='i_expectedIncome' id='appscrexpectedIncome57TD"+financialHouseHoldDone+"'>"+expectedIncomeByHouseHoldMember+"</td>";*/

		//$('#appscr57HouseHoldPersonalInformation').find('#'+$('#houseHoldTRNo').val()).append(appendRowData);
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+getCurrentApplicantID()).append(appendRowData);

}

function resetAddCurrentIncomeDetail()
{

}


function totalIncomeAt71(incomeArrayNumber)
{

	if(useJSON == true){
		totalIncomeAt71JSON(incomeArrayNumber);
		return;
	}

	var totalIncome71 = 0;

	for(var i=1; i<financial_DetailsArray.length; i++)
	{
		if($('#'+financial_DetailsArray[i]).is(":checked") == true)
		{
			var frquency = $('#'+financial_DetailsArray[i]+'_select_id').val();
			var income = $('#'+financial_DetailsArray[i]+'_id').val();

			if(income == "")
				income = 0;


			if(financial_DetailsArray[i] == 'Financial_Self_Employment')
			{
				totalIncome71 = totalIncome71 + ($('#'+financial_DetailsArray[i]+'_id').val())*12;


			}
			else if(financial_DetailsArray[i] == 'Financial_Capitalgains')
			{
				totalIncome71 = totalIncome71 + ($('#'+financial_DetailsArray[i]+'_id').val())*1;


			}
			else if(!(frquency == "SelectFrequency"))
			{

				totalIncome71 = totalIncome71 + frequencyIncome(frquency, income);

			}

		}
	}

	totalIncome71 = (totalIncome71/12).toFixed(2);

	$('#totalIncomeAt72LabelHead').html('<h3> $  '+totalIncome71+' /month </h3>');
	$('#totalIncomeAt72Label').text('$  '+totalIncome71+' /month');
	$('.amountAt72PerYear').text('$  '+(totalIncome71*12));


	checkBoxSameChecke(financial_DetailsArray);
}

function totalIncomeToArray(arrayIndex)
{
	var idIndex = 1;

	var totalIncome = 0.0;
	var frequencyF = "";
	var income = 0;
	var vId = "";

	for(; idIndex<financial_DetailsArray.length; idIndex++)
	{
		vId = financial_DetailsArray[idIndex];

		income = $('#appscr'+vId+'57TD'+arrayIndex).text();
		frequencyF = $('#appscr'+vId+'Frequency57TD'+arrayIndex).text();


		if(financial_DetailsArray[idIndex] == 'Financial_Self_Employment')
		{
			totalIncome+= income*12;
		}
		else if(financial_DetailsArray[idIndex] == 'Financial_Capitalgains')
		{
			totalIncome+= income*1;
		}
		else
		{
			totalIncome+= frequencyIncome(frequencyF, income);
		}


	}

	totalIncomes[(arrayIndex-1)] = (totalIncome/12).toFixed(2);
}



function totalDeductionInArray(arrayIndex)
{
	var totalDeduction = 0.0;
	var frequencyF = "";
	var income = 0;

	income = $('#appscr57HouseHoldalimonyAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHoldalimonyFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);

	income = $('#appscr57HouseHoldstudentLoanAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHoldstudentLoanFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);



	income = $('#appscr57HouseHolddeductionsAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHolddeductionsFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);
	totalIncomeDeductions[(arrayIndex-1)] = (totalDeduction/12).toFixed(2);
}


function frequencyIncome(frequency, income)
{
	if(frequency == 'Hourly')
	{
		return income*8640;
	}
	else if(frequency == 'Daily')
	{
		return income*365;
	}
	else if(frequency == 'Weekly')
	{
		return income*52;
	}
	else if(frequency == 'EveryTwoWeeks')
	{
		return income*26;
	}
	else if(frequency == 'Monthly')
	{
		return income*12;
	}
	else if(frequency == 'Yearly')
	{
		return income*1;
	}
	else if(frequency == 'OneTimeOnly')
	{
		return income*1;
	}
	else if(frequency == 'TwiceAMonth')
	{
		return income*24;
	}
	else if(frequency == 'Quarterly')
	{
		return income*3;
	}
	else
		return 0.0;
}


function checkBoxSameChecke(idOfChecks)
{
	for(var i=1; i<idOfChecks.length; i++)
	{
		if($('#'+idOfChecks[i]).is(":checked") == true)
		{
			document.getElementById(idOfChecks[i]+'1').checked = true;
		}
		else
		{
			document.getElementById(idOfChecks[i]+'1').checked = false;
		}
		$('#'+idOfChecks[i]+'1').attr("disabled",true);
	}
	if($('#Financial_None').is(":checked") == true)
		document.getElementById('Financial_None1').checked = true;
	else
		document.getElementById('Financial_None1').checked = false;

	$('#Financial_None1').attr("disabled",true);

}



function setJobIncomeExistDetails()
{
	//var index =($('#houseHoldTRNo').val()).substring(9);
	var index = getCurrentApplicantID();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[index-1];
	var job_income = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	//var job_income= $('#'+'Financial_Job'+'_id').val();
	//if(index==1)
	//	job_income_name= $("#Financial_Job_EmployerName_id").val();
	job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;

	if(job_income=='0' || job_income=='' || job_income== undefined){
		$('#appscr73EmployerNameDiv').hide();
	}
	else
		{
		$('.appscr73EmployerName').html(job_income_name);
		$('#appscr73EmployerNameDiv').show();
		}


}



function setHouseHoldData()
{

	//var index =($('#houseHoldTRNo').val()).substring(9),
	var index = getCurrentApplicantID(),
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[index-1],
		job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;

	$('#houseHoldNameHere').html('');
	$('#houseHoldNameHere').html(job_income_name);
	$('#houseHoldNameHere2').html(job_income_name);

	/*if(job_income_name=='')
		{
		$(".memberNameDataDiv").hide();
		$("#memberNameDataDivHide").hide();
		}
	else
		{
	//	$(".memberNameDataDiv").show();
	//	$("#memberNameDataDivHide").show();
		}*/

	if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){
		$("#employerOfJobDiv").show();
	}else{
		$("#employerOfJobDiv").hide();
	}
}

function setEmployerNameToAll()
{
	$('.EmployerNameHere').html(job_income_name);
}

function checkEmployerNameExist()
{

if(job_income_name=='' || job_income_name ==undefined)
	{

	next();
	}
if(currentPage == "appscr77Part1"){
	continueData77Part1();
}
next();
}

function totalIncomeAt71JSON(incomeArrayNumber)
{
	var totalIncome71 = getFamilyMemberIncome(incomeArrayNumber);

	totalIncome71 = (totalIncome71/12).toFixed(2); //income per month

	$('#totalIncomeAt72LabelHead').html('<h3> $  '+ Moneyformat(totalIncome71)+' /month </h3>');
	$('#totalIncomeAt72Label').text('$  '+Moneyformat(totalIncome71)+' /month');
	$('.amountAt72PerYear').text('$  '+Moneyformat(totalIncome71*12));


	checkBoxSameChecke(financial_DetailsArray);
}

function getFamilyMemberIncome(familyMemberNo){

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[familyMemberNo-1];

	if(typeof householdMemberObj == 'undefined'){
		return 0;
	}

	var editJobIncome 							  = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	var editJobFrequency 						  = householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency;
	var editSelfEmploymentIncome 				  = householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome;
	var editSocialSecurityBenefitsIncome 		  = householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount;
	var editSocialSecurityBenefitsIncomeFrequency = householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency;
	var editUnemploymentIncome 					  = householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount;
	var editUnemploymentIncomeFrequency 		  = householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency;
	var editRetirementPensionIncome 			  = householdMemberObj.detailedIncome.retirementOrPension.taxableAmount;
	var editRetirementPensionIncomeFrequency 	  = householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency;
	var editCapitalgainsIncome 					  = householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains;
	var editInvestmentIncome 					  = householdMemberObj.detailedIncome.investmentIncome.incomeAmount;
	var editInvestmentIncomeFrequency 			  = householdMemberObj.detailedIncome.investmentIncome.incomeFrequency;
	var editRentalOrRoyaltyIncome 				  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount;
	var editRentalOrRoyaltyIncomeFrequency 		  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency;
	var editFarmingOrFishingIncome 				  = householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount;
	var editFarmingOrFishingIncomeFrequency 	  = householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency;
	var editAlimonyReceivedIncome 				  = householdMemberObj.detailedIncome.alimonyReceived.amountReceived;
	var editAlimonyReceivedIncomeFrequency 		  = householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency;

	//We don't have other income in JSON
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();




	var totalIncome71 = 0;

	if(editJobIncome != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(editJobFrequency, editJobIncome);
	}

	if(editSelfEmploymentIncome!=""){
		totalIncome71 = totalIncome71 + (editSelfEmploymentIncome*12);
	}

	if(editSocialSecurityBenefitsIncome!=""){
		totalIncome71 = totalIncome71 + frequencyIncome(editSocialSecurityBenefitsIncomeFrequency, editSocialSecurityBenefitsIncome);
	}

	if(editUnemploymentIncome != "") {
		totalIncome71 = totalIncome71 + frequencyIncome(editUnemploymentIncomeFrequency, editUnemploymentIncome);
	}

	if(editRetirementPensionIncome != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(editRetirementPensionIncomeFrequency, editRetirementPensionIncome);
	}

	if(editCapitalgainsIncome != "") {
		totalIncome71 = totalIncome71 + editCapitalgainsIncome*1;
	}

	if(editInvestmentIncome != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(editInvestmentIncomeFrequency, editInvestmentIncome);
	}

	if(editRentalOrRoyaltyIncome!=""){
		totalIncome71 = totalIncome71 + frequencyIncome(editRentalOrRoyaltyIncomeFrequency, editRentalOrRoyaltyIncome);
	}

	if(editFarmingOrFishingIncome != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(editFarmingOrFishingIncomeFrequency, editFarmingOrFishingIncome);
	}

	if(editAlimonyReceivedIncome!=""){
		totalIncome71 = totalIncome71 + frequencyIncome(editAlimonyReceivedIncomeFrequency, editAlimonyReceivedIncome);
	}

	if(editOtherIncome != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(editOtherIncomeFrequency, editOtherIncome);
	}

	if(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[0].incomeAmount);
	}

	if(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[1].incomeAmount);
	}

	if(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[2].incomeAmount);
	}

	//totalIncome71 = (totalIncome71/12).toFixed(2);

	return getDecimalNumber(totalIncome71); //family member yearly income

}

function setTotalFamilyIncomeJSON() {

	totalFamilyIncome = 0;

	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

	for ( var cnt = 1; cnt <= noOfHouseHold; cnt++) {
		totalFamilyIncome = totalFamilyIncome + getFamilyMemberIncome(cnt);
	}

	webJson.singleStreamlinedApplication.taxHousehold[0].houseHoldIncome = getDecimalNumber(totalFamilyIncome);

}

/*
****************************************************************************************************************
* magiValidation.js                                                                                        *
****************************************************************************************************************
*/

var householdSSNIndicationValue = "";

function validation(currentPage1)
{

	var flag = true;
	var idFocus = "";


	emptyLabels();


	if(currentPage1 == 'appscr51' &&  document.getElementById('acceptanceCB').checked == false)
		{
		$('#acceptanceCB').addClass("validateColor");
		addLabels(" accept the agreement !", "acceptanceCB");
		idFocus = "acceptanceCB";


		flag = false;
		}
	else if(currentPage1 == "appscr53")															//appscr53 Page Validation
	{
		/*var alphaExp = /^[a-zA-Z]+$/;
		//First Name validation
		var name = $('#firstName').val();
		if($('#firstName').val()=="" || !(name.match(alphaExp)))
		{
			$('#firstName').addClass("validateColor");
			addLabels("Enter First Name", "firstName");
			idFocus = "firstName";
			flag = false;
		}

		$('#dateOfBirth').parsley('validate');
		if($('#dateOfBirth').parsley('isValid')==false){
			flag = false;
		}


		//Date of Birth Validation
		if($('#dateOfBirth').val()=="" || checkDate($('#dateOfBirth').val())==false)
		{
			$('#dateOfBirth').addClass("validateColor");
			addLabels("Enter Date of Birth in MM/DD/YYYY Format", "dateOfBirth");
			if(idFocus == "")
				idFocus = "dateOfBirth";
			flag = false;
		}

		//Email ID Validation
		if($('#emailAddress').val()=="" || checkEmail($('#emailAddress').val())==false)
		{
			$('#emailAddress').addClass("validateColor");
			addLabels("Enter Email address in \"example@gmail.com\" Format", "emailAddress");
			if(idFocus == "")
				idFocus = "emailAddress";
			flag = false;
		}

		//Home Address 1 Validation
		if(!($('#homeAddressIndicator').is(":checked")))
		{
			if($('#home_addressLine1').val()=="")
			{
				$('#home_addressLine1').addClass("validateColor");
				addLabels("Enter Home address line 1", "home_addressLine1");
				if(idFocus == "")
					idFocus = "home_addressLine1";
				flag = false;
			}
		}
		else
		{
			$('#home_addressLine1').removeClass("validateColor");
		}

		//Mailing Address Validation
		if($('#mailingAddressIndicator').is(":checked"))
		{

		}

		//Home Address City Validation
		if($('#home_primary_city').val()=="")
		{
			$('#home_primary_city').addClass("validateColor");
			addLabels("Enter City", "home_primary_city");
			if(idFocus == "")
				idFocus = "home_primary_city";
			flag = false;
		}

		//Home Address Zip Validation
		if($('#home_primary_zip').val()=="")
		{
			$('#home_primary_zip').addClass("validateColor");
			addLabels("Enter Zip Code", "home_primary_zip");
			if(idFocus == "")
				idFocus = "home_primary_zip";
			flag = false;
		}

		//Home Address State Validation
		if($('#home_primary_state').val()=="")
		{
			$('#home_primary_state').addClass("validateColor");
			addLabels("Enter State", "home_primary_state");
			if(idFocus == "")
				idFocus = "home_primary_state";
			flag = false;
		}

		//Home Address Country Validation
		if($('#home_primary_county').val()=="")
		{
			$('#home_primary_county').addClass("validateColor");
			addLabels("Enter County", "home_primary_county");
			if(idFocus == "")
				idFocus = "home_primary_county";
			flag = false;
		}

		//Home Address Mailing State Validation
		if($('#mailing_primary_state').val()=="")
		{
			$('#mailing_primary_state').addClass("validateColor");
			addLabels("Enter Mailing State", "mailing_primary_state");
			if(idFocus == "")
				idFocus = "mailing_primary_state";
			flag = false;
		}*/

	}
	else if(currentPage1 == "appscr54")                     								//appscr54 Page Validation
	{

		/*need to uncomment after demo on 23 May*/
		/*$('input[name=authorizedRepresentativeIndicator]:radio').parsley('validate');
		if($('input[name=authorizedRepresentativeIndicator]:radio').parsley('isValid')==false){
			flag = false;
		}*/

		if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == "yes")
		{
			if($('#authorizedFirstName').val()=="")
			{
				$('#authorizedFirstName').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedFirstName";
				flag = false;
			}

			if($('#authorizedLastName').val()=="")
			{
				$('#authorizedLastName').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedLastName";
				flag = false;
			}

			if($('#authorizedEmailAddress').val()=="" || checkEmail($('#authorizedEmailAddress').val())==false)
			{
				$('#authorizedEmailAddress').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedEmailAddress";
				flag = false;
			}

			if($('#authorizedAddress1').val()=="")
			{
				$('#authorizedAddress1').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedAddress1";
				flag = false;
			}

			if($('#authorizedCity').val()=="")
			{
				$('#authorizedCity').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedCity";
				flag = false;
			}

			if($('#authorizedZip').val()=="")
			{
				$('#authorizedZip').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedZip";
				flag = false;
			}

			if($('#authorizedState').val()=="0")
			{
				$('#authorizedState').addClass("validateColor");

				if(idFocus == "")
					idFocus = "authorizedState";
				flag = false;
			}

			if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == "Yes")
			{
				if($('#authorizeCompanyName').val()=="")
				{
					$('#authorizeCompanyName').addClass("validateColor");

					if(idFocus == "")
						idFocus = "authorizeCompanyName";
					flag = false;
				}

				if($('#authorizeOrganizationId').val()=="")
				{
					$('#authorizeOrganizationId').addClass("validateColor");

					if(idFocus == "")
						idFocus = "authorizeOrganizationId";
					flag = false;
				}
			}

			if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == "Signature")
			{
				if($('#authorizeSignature').val()=="")
				{
					$('#authorizeSignature').addClass("validateColor");

					if(idFocus == "")
						idFocus = "authorizeSignature";
					flag = false;
				}
			}

		}
		else
		{
			$('#authorizedFirstName').removeClass("validateColor");
			$('#authorizedLastName').removeClass("validateColor");
			$('#authorizedEmailAddress').removeClass("validateColor");
			$('#authorizedAddress1').removeClass("validateColor");
			$('#authorizedCity').removeClass("validateColor");
			$('#authorizedZip').removeClass("validateColor");
			$('#authorizedState').removeClass("validateColor");
			$('#authorizeCompanyName').removeClass("validateColor");
			$('#authorizeOrganizationId').removeClass("validateColor");
			$('#authorizeSignature').removeClass("validateColor");
		}

	}
	else if(currentPage1 == "appscr55")
	{
		$('#get-help-with-costs-form').parsley('validate');
		if($('#get-help-with-costs-form').parsley('isValid')==false){
			flag = false;
		}

		if($('#noOfPeople53').val()=="")
			{
				$('#noOfPeople53').addClass("validateColor");

				if(idFocus == "")
					idFocus = "noOfPeople53";
				flag = false;
			}


	}
	else if(currentPage1 == "appscr57")                    								//appscr57 Page Validation
	{


		//Applicants applying for Health Insurance Validation
		var noOfHouseHold = $('#numberOfHouseHold').text();
		for(var i = 1;i<=noOfHouseHold;i++)
		{

			$('#appscr57FirstName'+i).parsley('validate');
			if($('#appscr57FirstName'+i).parsley('isValid')==false){
				flag = false;
			}

			$('#appscr57LastName'+i).parsley('validate');
			if($('#appscr57LastName'+i).parsley('isValid')==false){
				flag = false;
			}

			$('#appscr57DOB'+i).parsley('validate');
			if($('#appscr57DOB'+i).parsley('isValid')==false){
				flag = false;
			}

			//if(!$('input[name=appscr57Tobacco'+i+']:radio').is(':checked')){
				$('input[name=appscr57Tobacco'+i+']:radio').parsley('validate');
				if($('input[name=appscr57Tobacco'+i+']:radio').parsley('isValid')==false){
					flag = false;
				}
			//}

		}
	}
	else if(currentPage1 == "appscr59A"){
		//console.log('valid for 59');
	}
	else if(currentPage1 == "appscr60")                    											//appscr60 Page Validation
	{

		$('#houseHoldQuestion input').parsley('validate');
		if($('#houseHoldQuestion input').parsley('isValid')==false){
			flag = false;
		}

		if($('#appscrIsChildDiv').is(':visible') && $('#willClaimDependents').is(':visible') && $('#willBeClaimed').is(':visible') && $('#householdSpouseMainDiv').is(':visible')){
			//$('#appscrIsChildDiv input').parsley('validate');
			$('#willClaimDependents input').parsley('validate');
			if($('#willClaimDependents input').parsley('isValid')==false){
				flag = false;
			}

			$('#willBeClaimed input').parsley('validate');
			if($('#willBeClaimed input').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=liveWithSpouseIndicator]').parsley('validate');
			if($('input[name=liveWithSpouseIndicator]').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=marriedIndicator]').parsley('validate');
			if($('input[name=marriedIndicator]').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=planOnFilingJointFTRIndicator]').parsley('validate');
			if($('input[name=planOnFilingJointFTRIndicator]').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=householdContactSpouse]').parsley('validate');
			if($('input[name=householdContactSpouse]').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#appscr60ClaimedFilerDiv').is(':visible')){
			$('input[name=householdContactFiler]').parsley('validate');
			if($('input[name=householdContactFiler]').parsley('isValid')==false){
				flag = false;
			}
		}


	///// Changes start

		//Tell Us About Your Household Spouse First Name
		if($('input[name=householdContactSpouse]:radio:checked').val() == "someOne" && $('input[name=marriedIndicator]:radio:checked').val() == "yes")
		{
			if($('#spouseFirstName').val()=="")
			{
				$('#spouseFirstName').addClass("validateColor");
				$('#parsley-appscr60-s-fn').show();
				if(idFocus == "")
					idFocus = "spouseFirstName";
				flag = false;
			}

			//Tell Us About Your Household Spouse Last Name
			if($('#spouseLastName').val()=="")
			{
				$('#spouseLastName').addClass("validateColor");
				$('#parsley-appscr60-s-ln').show();
				if(idFocus == "")
					idFocus = "spouseLastName";
				flag = false;
			}

			//Tell Us About Your Household Spouse Date Of Birth
			if($('#spouseDOB').val()=="" || checkDate($('#spouseDOB').val())==false)
			{
				$('#spouseDOB').addClass("validateColor");
				$('#parsley-appscr60-s-dob').show();
				if(idFocus == "")
					idFocus = "spouseDOB";
				flag = false;
			}
		}
		else
		{
			$('#spouseFirstName').removeClass("validateColor");
			$('#spouseMiddleName').removeClass("validateColor");
			$('#spouseLastName').removeClass("validateColor");
			$('#spouseDOB').removeClass("validateColor");
		}


		if($('input[name=householdContactFiler]:radio:checked').val() == "someOne" && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == "yes")
		{
			//Tell Us About Your Household Tax_Filer First Name
			if($('#taxFilerFirstName').val()=="")
			{
				$('#taxFilerFirstName').addClass("validateColor");
				$('#parsley-appscr60-tf-fn').show();
				//addLabels('Enter the tex filer First Name', "taxFilerName60A_firstName");
				if(idFocus == "")
					idFocus = "taxFilerFirstName";
				flag = false;
			}
			else if($('#taxFilerFirstName').val()!="")
				$('#parsley-appscr60-tf-fn').hide();

			//Tell Us About Your Household Tax_Filer Last Name

			if($('#taxFilerLastName').val()=="")
			{
				$('#taxFilerLastName').addClass("validateColor");
				$('#parsley-appscr60-tf-ln').show();

				if(idFocus == "")
					idFocus = "taxFilerLastName";
				flag = false;
			}
			else if($('#taxFilerLastName').val()!="")
				$('#parsley-appscr60-tf-ln').hide();

			//Tell Us About Your Household Tax_Filer  Date Of Birth
			if($('#taxFilerDob').val()=="" || checkDate($('#taxFilerDob').val())==false)
			{
				$('#taxFilerDob').addClass("validateColor");
				$('#parsley-appscr60-tf-dob').show();

				if(idFocus == "")
					idFocus = "taxFilerDob";
				flag = false;
			}
			else if($('#taxFilerDob').val()=="" && checkDate($('#taxFilerDob').val())==true)
				$('#parsley-appscr60-tf-dob').hide();
		}
		else
		{
			$('#taxFilerFirstName').removeClass("validateColor");
			$('#taxFilerMiddleName').removeClass("validateColor");
			$('#taxFilerLastName').removeClass("validateColor");
			$('#taxFilerDob').removeClass("validateColor");


		}



		if($('#householdContactDependantSomeOneChecked').is(":checked") && $('input[name=householdHasDependant]:radio:checked').val() == "yes")
		{
			//Tell Us About Your Household Tax_Filer First Name
			if($('#dependentFirstName').val()=="")
			{
				$('#dependentFirstName').addClass("validateColor");
				$('#parsley-appscr60-dd-fn').show();
				//addLabels('Enter the tex filer First Name', "taxFilerName60A_firstName");
				if(idFocus == "")
					idFocus = "dependentFirstName";
				flag = false;
			}
			else if($('#dependentFirstName').val()!="")
				$('#parsley-appscr60-dd-fn').hide();

			//Tell Us About Your Household Tax_Filer Last Name

			if($('#dependentLastName').val()=="")
			{
				$('#dependentLastName').addClass("validateColor");
				$('#parsley-appscr60-dd-ln').show();

				if(idFocus == "")
					idFocus = "dependentLastName";
				flag = false;
			}
			else if($('#dependentLastName').val()!="")
				$('#parsley-appscr60-dd-ln').hide();

			//Tell Us About Your Household Tax_Filer  Date Of Birth
			if($('#dependentDOB').val()=="" || checkDate($('#taxFilerDob').val())==false)
			{
				$('#dependentDOB').addClass("validateColor");
				$('#parsley-appscr60-dd-dob').show();

				if(idFocus == "")
					idFocus = "dependentDOB";
				flag = false;
			}
			else if($('#dependentDOB').val()=="" && checkDate($('#dependentDOB').val())==true)
				$('#parsley-appscr60-dd-dob').hide();
		}
		else
		{
			$('#dependentFirstName').removeClass("validateColor");

			$('#dependentLastName').removeClass("validateColor");
			$('#dependentDOB').removeClass("validateColor");


		}


	}

	////// Changes end, made on March 4, due to bad parsley functionality


	else if(currentPage1 == "appscr61")                    										//appscr61 Page Validation
	{
		$('input[name=appscr61_gender]').parsley('validate');
		if($('input[name=appscr61_gender]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=socialSecurityCardHolderIndicator]').parsley('validate');
		if($('input[name=socialSecurityCardHolderIndicator]').parsley('isValid')==false){
			flag = false;
		}

		if($('#DontaskAboutSSNOnPage61').is(':visible')){
			$('#reasonableExplanationForNoSSN').parsley('validate');
			if($('#reasonableExplanationForNoSSN').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('input:radio[name=socialSecurityCardHolderIndicator]:checked').val() == "yes")
		{
			/*if($('#ssn1').val()=="")
			{
				$('#ssn1').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn1");
				if(idFocus == "")
					idFocus = "ssn1";
				    flag = false;
			}
			if($('#ssn2').val()=="")
			{
				$('#ssn2').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn2");
				if(idFocus == "")
					idFocus = "ssn2";
				flag = false;
			}

			if($('#ssn3').val()=="")
			{
				$('#ssn3').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn3");
				if(idFocus == "")
					idFocus = "ssn3";
				flag = false;
			}*/

			$('#ssn1').parsley('validate');
			if($('#ssn1').parsley('isValid')==false){
				flag = false;
			}

			$('#ssn2').parsley('validate');
			if($('#ssn2').parsley('isValid')==false){
				flag = false;
			}

			$('#ssn3').parsley('validate');
			if($('#ssn3').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=fnlnsSame]').parsley('validate');
			if($('input[name=fnlnsSame]').parsley('isValid')==false){
				flag = false;
			}
		}
		/*else
		{
			$('#ssn1').removeClass("validateColor");
			$('#ssn2').removeClass("validateColor");
			$('#ssn3').removeClass("validateColor");
		}*/

		if($('input:radio[name=socialSecurityCardHolderIndicator]:checked').val() == "yes" && $('input[name=fnlnsSame]:radio:checked').val() == "no")
		{
			$('#firstNameOnSSNCard').parsley('addConstraint', {required: true });
			$('#firstNameOnSSNCard').parsley('validate');
			if($('#firstNameOnSSNCard').parsley('isValid')==false){
				flag = false;
			}

			$('#lastNameOnSSNCard').parsley('addConstraint', {required: true });
			$('#lastNameOnSSNCard').parsley('validate');
			if($('#lastNameOnSSNCard').parsley('isValid')==false){
				flag = false;
			}
		}else{
			$('#lastNameOnSSNCard').parsley('removeConstraint', 'required');
			$('#firstNameOnSSNCard').parsley('validate');
			$('#firstNameOnSSNCard').parsley('removeConstraint', 'required');
			$('#lastNameOnSSNCard').parsley('validate');
		}


		/*if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'no')
		{
			$('#exemptionId').parsley('addConstraint', {required: true });
			$('#exemptionId').parsley('validate');
			if($('#exemptionId').parsley('isValid')==false){
				flag = false;
			}


		}*/

		/*if($('#DontaskAboutSSNOnPage61').is(':visible')){
			$('#reasonableExplanationForNoSSN').parsley('addConstraint', {required: true });
			$('#reasonableExplanationForNoSSN').parsley('validate');
			if($('#reasonableExplanationForNoSSN').parsley('isValid')==false){
				flag = false;
			}
		}*/

	}
	else if(currentPage1 =="appscr62Part1")/*appscr62Part1 Page Validation*/{
		/*first question*/
		$('input[name=UScitizen]').parsley('validate');
		if($('input[name=UScitizen]').parsley('isValid')==false){
			flag = false;
		}
		/*naturalized citizen question*/
		if($('#naturalizedCitizenDiv').is(':visible')){
			$('#naturalizedCitizenDiv input').parsley('validate');
			if($('#naturalizedCitizenDiv input').parsley('isValid')==false){
				flag = false;
			}
		}

		/*document type*/
		if($('#documentTypeDiv').is(':visible')){
			$('input[name=documentType]').parsley('validate');
			if($('input[name=documentType]').parsley('isValid')==false){
				flag = false;
			}
		}

		/*naturalized certificate input textbox when visible*/
		if($('#divNaturalizationCertificate').is(':visible')){
			$('#naturalizationAlignNumber').parsley('validate');
			if($('#naturalizationAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#naturalizationCertificateNumber').parsley('validate');
			if($('#naturalizationCertificateNumber').parsley('isValid')==false){
				flag = false;
			}
		}

		/*citizenship certificate textbox when visible*/
		if($('#divCitizenshipCertificate').is(':visible')){
			$('#citizenshipAlignNumber').parsley('validate');
			if($('#citizenshipAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr62p1citizenshipCertificateNumber').parsley('validate');
			if($('#appscr62p1citizenshipCertificateNumber').parsley('isValid')==false){
				flag = false;
			}
		}


		/*eligibleImmigrationStatus when NO is selected from naturalized citizen*/
		if($('#eligibleImmigrationStatus').is(':visible')){
			$('input[name=docType]').parsley('validate');
			if($('input[name=docType]').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=livesInUS]').parsley('validate');
			if($('input[name=livesInUS]').parsley('isValid')==false){
				flag = false;
			}

			$('input[name=honourably-veteran]').parsley('validate');
			if($('input[name=honourably-veteran]').parsley('isValid')==false){
				flag = false;
			}

		}

		/*options of 1st radio button*/
		if($('#permcarddetailsdiv').is(':visible')){
			$('#permcarddetailsAlignNumber').parsley('validate');
			if($('#permcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#permcarddetailsppcardno').parsley('validate');
			if($('#permcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#permcarddetailsdateOfExp').parsley('validate');
			if($('#permcarddetailsdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 2nd radio button*/
		if($('#tempcarddetailsdiv').is(':visible')){
			$('#tempcarddetailsAlignNumber').parsley('validate');
			if($('#tempcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#tempcarddetailsppcardno').parsley('validate');
			if($('#tempcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			$('#tempcarddetailsCounty').parsley('validate');
			if($('#tempcarddetailsCounty').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#tempcarddetailsPassportexpdate').parsley('validate');
			if($('#tempcarddetailsPassportexpdate').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 3rd radio button*/
		if($('#machinecarddetailsdiv').is(':visible')){
			$('#machinecarddetailsAlignNumber').parsley('validate');
			if($('#machinecarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#machinecarddetailsppcardno').parsley('validate');
			if($('#machinecarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}

			$('#machinecarddetailsCounty').parsley('validate');
			if($('#machinecarddetailsCounty').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#machinecarddetailsppvisano').parsley('validate');
			if($('#machinecarddetailsppvisano').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 4th radio button*/
		if($('#empauthcarddetailsdiv').is(':visible')){
			$('#empauthcarddetailsAlignNumber').parsley('validate');
			if($('#empauthcarddetailsAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#empauthcarddetailsppcardno').parsley('validate');
			if($('#empauthcarddetailsppcardno').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#empauthcarddetailsdateOfExp').parsley('validate');
			if($('#empauthcarddetailsdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 5th radio button*/
		if($('#arrivaldeparturerecorddiv').is(':visible')){
			$('#arrivaldeparturerecordI94no').parsley('validate');
			if($('#arrivaldeparturerecordI94no').parsley('isValid')==false){
				flag = false;
			}

			/*
			$('#arrivaldeparturerecordSevisID').parsley('validate');
			if($('#arrivaldeparturerecordSevisID').parsley('isValid')==false){
				flag = false;
			}

			$('#arrivaldeparturerecorddateOfExp').parsley('validate');
			if($('#arrivaldeparturerecorddateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 6th radio button*/
		if($('#arrivaldeprecordForeigndiv').is(':visible')){
			$('#arrivaldeprecordForeignI94no').parsley('validate');
			if($('#arrivaldeprecordForeignI94no').parsley('isValid')==false){
				flag = false;
			}

			$('#arrivaldeprecordForeignppcardno').parsley('validate');
			if($('#arrivaldeprecordForeignppcardno').parsley('isValid')==false){
				flag = false;
			}

			$('#arrivaldeprecordForeignCounty').parsley('validate');
			if($('#arrivaldeprecordForeignCounty').parsley('isValid')==false){
				flag = false;
			}

			$('#arrivaldeprecordForeigndateOfExp').parsley('validate');
			if($('#arrivaldeprecordForeigndateOfExp').parsley('isValid')==false){
				flag = false;
			}
			/* Don't require validation for following fields #HIX-42693
			$('#arrivaldeprecordForeignppvisano').parsley('validate');
			if($('#arrivaldeprecordForeignppvisano').parsley('isValid')==false){
				flag = false;
			}

			$('#arrivaldeprecordForeignSevisID').parsley('validate');
			if($('#arrivaldeprecordForeignSevisID').parsley('isValid')==false){
				flag = false;
			}*/
		}


		/*options of 7th radio button*/
		if($('#NoticeOfActiondiv').is(':visible')){
			$('#NoticeOfActionAlignNumber').parsley('validate');
			if($('#NoticeOfActionAlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#NoticeOfActionI94').parsley('validate');
			if($('#NoticeOfActionI94').parsley('isValid')==false){
				flag = false;
			}
		}

		/*options of 8th radio button*/
		if($('#ForeignppI94div').is(':visible')){
			$('#ForeignppI94AlignNumber').parsley('validate');
			if($('#ForeignppI94AlignNumber').parsley('isValid')==false){
				flag = false;
			}

			$('#ForeignppI94ppcardno').parsley('validate');
			if($('#ForeignppI94ppcardno').parsley('isValid')==false){
				flag = false;
			}

			$('#ForeignppI94County').parsley('validate');
			if($('#ForeignppI94County').parsley('isValid')==false){
				flag = false;
			}

			$('#ForeignppI94dateOfExp').parsley('validate');
			if($('#ForeignppI94dateOfExp').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#ForeignppI94SevisID').parsley('validate');
			if($('#ForeignppI94SevisID').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*options of 9th radio button*/
		if($('#rentrypermitdiv').is(':visible')){

			$('#rentrypermitAlignNumber').parsley('validate');
			if($('#rentrypermitAlignNumber').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#rentrypermitdateOfExp').parsley('validate');
			if($('#rentrypermitdateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/*option of 10th radio button*/
		if ($('#refugeetraveldocI-571div').is(':visible')){

			$('#refugeetraveldocI-571AlignNumber').parsley('validate');
			if($('#refugeetraveldocI-571AlignNumber').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#refugeetraveldocI-571dateOfExp').parsley('validate');
			if($('#refugeetraveldocI-571dateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}


		/*options of 11th radio button*/
		if($('#certificatenonimmigrantF1-Student-I20div').is(':visible')){

			$('#certificatenonimmigrantF1-Student-I20SevisID').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20SevisID').parsley('isValid')==false){
				flag = false;
			}

			$('#certificatenonimmigrantF1-Student-I20County').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20County').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#certificatenonimmigrantF1-Student-I20I94no').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20I94no').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantF1-Student-I20ppcardno').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20ppcardno').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantF1-Student-I20I94no').parsley('validate');
			if($('#certificatenonimmigrantF1-Student-I20dateOfExp').parsley('isValid')==false){
				flag = false;
			}*/
		}

		/* option 12th Radio button*/
		if($('#certificatenonimmigrantJ1-Student-DS2019div').is(':visible')){

			$('#certificatenonimmigrantJ1-Student-DS2019SevisID').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019SevisID').parsley('isValid')==false){
				flag = false;
			}
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019I94no').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019I94no').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019ppcardno').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019ppcardno').parsley('isValid')==false){
				flag = false;
			}*/
			/*
			$('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').parsley('validate');
			if($('#certificatenonimmigrantJ1-Student-DS2019dateOfExp').parsley('isValid')==false){
				flag = false;
			} */
		}


		if($('#sameNameOnDocument').is(':visible')){

			$('#documentFirstName').parsley('validate');
			if($('#documentFirstName').parsley('isValid')==false){
				flag = false;
			}

			$('#documentLastName').parsley('validate');
			if($('#documentLastName').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#sameNameInfoPage62P2').is(':visible')){
			$('input[name=UScitizen62]').parsley('validate');
			if($('input[name=UScitizen62]').parsley('isValid')==false){
				flag = false;
			}
		}
		/*
		if($('#haveTheseDocuments').is(':visible')){
			$('input[name=checkboxValid]').parsley('validate');
			if($('input[name=checkboxValid]').parsley('isValid')==false){
				flag = false;
			}
		}*/


	}
	else if(currentPage1 =="appscr62Part2")                    										//appscr62Part2 Page Validation
	{

			if($('input[name=docType1]:radio:checked').val() == "OTHER_DOC")
			{
				if($('#appscr62OtherDocAlignNumber').val()=="")
				{
					$('#appscr62OtherDocAlignNumber').addClass("validateColor");

					if(idFocus == "")
						idFocus = "appscr62OtherDocAlignNumber";
					    flag = false;
				}

				if($('#appscr62P2I-94Number').val()=="")
				{
					$('#appscr62P2I-94Number').addClass("validateColor");

					if(idFocus == "")
						idFocus = "appscr62P2I-94Number";
					    flag = false;
				}
			}
			else
			{
				$('#appscr62P2I-94Number').removeClass("validateColor");
				$('#appscr62OtherDocAlignNumber').removeClass("validateColor");
			}

		}
		else if(currentPage1 =="appscr63")                    										//appscr63 Page Validation
		{
			/*console.log('ddddddddddddddddddddddddddddddd '+ checkBirthDate($('#parent-or-caretaker-dob').val()));*/

			// Commented by suneel. This has to be revisited as the data elements doesnt match with the IDs
			// HIX-HIX-39772


			/*
			$('input[name=appscr63AtLeastOneChildUnder19Indicator]').parsley('validate');
			if($('input[name=appscr63AtLeastOneChildUnder19Indicator]').parsley('isValid')==false){
				flag = false;
			}

			if($('.parentCareTaker').is(':visible')){
				$('input[name=takeCareOf]').parsley('validate');
				if($('input[name=takeCareOf]').parsley('isValid')==false){
					flag = false;
				}
			}

			if($('.parentOrCaretakerBasicDetailsContainer').is(':visible')){
				$('#parent-or-caretaker-firstName').parsley('validate');
				if($('#parent-or-caretaker-firstName').parsley('isValid')==false){
					flag = false;
				}

				$('#parent-or-caretaker-lastName').parsley('validate');
				if($('#parent-or-caretaker-lastName').parsley('isValid')==false){
					flag = false;
				}

				$('#applicant-relationship').parsley('validate');
				if($('#applicant-relationship').parsley('isValid')==false){
					flag = false;
				}

				$('input[name=birthOrAdoptiveParents]').parsley('validate');
				if($('input[name=birthOrAdoptiveParents]').parsley('isValid')==false){
					flag = false;
				}

				$('#parent-or-caretaker-dob').parsley('validate');
				if($('#parent-or-caretaker-dob').parsley('isValid')==false){
					flag = false;
				}
			}*/
		}
	else if(currentPage1 =="appscr64")                    										//appscr64 Page Validation
	{
		/*$('input[name=checkPersonNameLanguage]').parsley('validate');
		if($('input[name=checkPersonNameLanguage]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=race]').parsley('validate');
		if($('input[name=race]').parsley('isValid')==false){
			flag = false;
		}

		if($('#ethnicityDiv').is(':visible')){
			$('input[name=ethnicity]').parsley('validate');
			if($('input[name=ethnicity]').parsley('isValid')==false){
				flag = false;
			}
		}*/
	}
	else if(currentPage1 == "appscr65")												//Validation on appscr65
	{

		$('input[name=householdContactAddress]').parsley('validate');
		if($('input[name=householdContactAddress]').parsley('isValid')==false){
			flag = false;
		}

		for(var i=0;i<namesOfHouseHold.length;i++)
		{

			//$('input[name=householdContactAddress]').parsley('addConstraint', {required: true });
			$('input[name=householdContactAddress]').parsley('validate');
			if($('input[name=householdContactAddress]').parsley('isValid')==false){
				flag = false;
			}
			if($('#householdContactAddress'+namesOfHouseHold[i]).is(":checked")){

				$('#applicant_or_non-applican_address_1'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_address_1'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('#city'+namesOfHouseHold[i]).parsley('validate');
				if($('#city'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('#zip'+namesOfHouseHold[i]).parsley('validate');
				if($('#zip'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('#applicant_or_non-applican_state'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_state'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('input[name=isTemporarilyLivingOutsideIndicator'+namesOfHouseHold[i]+']').parsley('validate');
				if($('input[name=isTemporarilyLivingOutsideIndicator'+namesOfHouseHold[i]+']').parsley('isValid')==false){
					flag = false;
				}
			}

			if($('#inNMDiv'+namesOfHouseHold[i]).is(':visible')){

				$('#applicant2city'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant2city'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('#applicant-2-zip'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant-2-zip'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

				$('#applicant_or_non-applican_stateTemp'+namesOfHouseHold[i]).parsley('validate');
				if($('#applicant_or_non-applican_stateTemp'+namesOfHouseHold[i]).parsley('isValid')==false){
					flag = false;
				}

			}
		}
	}

	else if(currentPage1 == "appscr66"){
		$('input[name=personDisability]').parsley('validate');
		if($('input[name=personDisability]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=personPersonalAssistance]').parsley('validate');
		if($('input[name=personPersonalAssistance]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=peopleBelowTo]').parsley('validate');
		if($('input[name=peopleBelowTo]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=personPregnant]').parsley('validate');
		if($('input[name=personPregnant]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=personfosterCare]').parsley('validate');
		if($('input[name=personfosterCare]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=deceasedPerson]').parsley('validate');
		if($('input[name=deceasedPerson]').parsley('isValid')==false){
			flag = false;
		}

		for(var i=1;i<=$('.eligibleCoverage').length ;i++){
			$('input[name=fedHealth'+i+']').parsley('validate');
			if($('input[name=fedHealth'+i+']').parsley('isValid')==false){
				flag = false;
			}
		}

		var noOfHouseHold = $('#numberOfHouseHold').text();
		for (i = 1; i <= noOfHouseHold; i++){
			if($('#nativeTribe'+i).is(":visible")){
				$('input[name=AmericonIndianQuestionRadio'+i+']').parsley('addConstraint', {required: true });
				$('input[name=AmericonIndianQuestionRadio'+i+']').parsley('validate');
				if($('input[name=AmericonIndianQuestionRadio'+i+']').parsley('isValid')==false){
					flag = false;
				}
			}
			if($('#americonIndianQuestionDiv'+i).is(":visible")){
				/*$('input[name=americonIndianQuestionSelect'+i+']').parsley('addConstraint', {required: true });*/
				$('#americonIndianQuestionSelect'+i).parsley('validate');
				if($('#americonIndianQuestionSelect'+i).parsley('isValid')==false){
					flag = false;
				}

				$('#tribeName'+i).parsley('validate');
				if($('#tribeName'+i).parsley('isValid')==false){
					flag = false;
				}


			}
		}
	}
	else if(currentPage1 == "appscrSNAP")
	{
		if($('#shelterAndUtilityExpenseCost').val()=="")
		{
			$('#shelterAndUtilityExpenseCost').addClass("validateColor");
			if(idFocus == "")
				idFocus = "shelterAndUtilityExpenseCost";
			flag = false;
		}
		if($('#shelterAndUtilityExpenseFrequency').val()=="SelectFrequency")
		{
			$('#shelterAndUtilityExpenseFrequency').addClass("validateColor");
			if(idFocus == "")
				idFocus = "shelterAndUtilityExpenseFrequency";
			flag = false;
		}

	}
	else if(currentPage1 == "appscr69")												//Validation on Page appscr69
	{

		$('#expeditedIncome69').parsley('validate');
		if($('#expeditedIncome69').parsley('isValid')==false){
			flag = false;
			/*$('.errorMessage ul').show();*/
		}
	}
	else if(currentPage1 == "appscr70")												//Validation on Page appscr70
	{

		$('input[name=appscr70radios4Income]').parsley('validate');
		if($('input[name=appscr70radios4Income]').parsley('isValid')==false){
			flag = false;
		}

		if($('#incomeSourceDiv').is(":visible")){
			$('input[name=incomeSources]').parsley('validate');
			if($('input[name=incomeSources]').parsley('isValid')==false){
				flag = false;
			}
		}
	}
	else if(currentPage1 == "appscr71")														//validation on Page appscr71
	{
		allIncomeValidations();
		if($('.parsley-error-list').is(':visible')){
			flag = false;
			return false;
		}
	}
	else if(currentPage1 == "appscr72")														// Validation on Page appscr72
	{
		flag = true;
		//Temporary remove deduction validation
		/*
		if($('#incomeDeductions_div').is(':visible')){
			$('input[name=incomeDeduction]').parsley('validate');
			if($('input[name=incomeDeduction]').parsley('isValid')==false){
				flag = false;
			}
		}


		if($('#appscr72alimonyPaidTR').is(':visible')){

			$('#alimonyAmountInput').parsley('validate');
			if($('#alimonyAmountInput').parsley('isValid')==false){
				flag = false;
			}

			$('#alimonyFrequencySelect').parsley('validate');
			if($('#alimonyFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#appscr72studentLoanInterestTR').is(':visible')){

			$('#studentLoanInterestAmountInput').parsley('validate');
			if($('#studentLoanInterestAmountInput').parsley('isValid')==false){
				flag = false;
			}

			$('#studentLoanInterestFrequencySelect').parsley('validate');
			if($('#studentLoanInterestFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#appscr72otherDeductionTR').is(':visible')){

			$('#deductionTypeInput').parsley('validate');
			if($('#deductionTypeInput').parsley('isValid')==false){
				flag = false;
			}

			$('#deductionAmountInput').parsley('validate');
			if($('#deductionAmountInput').parsley('isValid')==false){
				flag = false;
			}

			$('#deductionFrequencySelect').parsley('validate');
			if($('#deductionFrequencySelect').parsley('isValid')==false){
				flag = false;
			}
		}

		$('input[name=aboutIncomeSpped]').parsley('validate');
		if($('input[name=aboutIncomeSpped]').parsley('isValid')==false){
			flag = false;
		}

		if($('#incomeHighOrLow').is(':visible')){
			$('input[name=incomeHighOrLow]').parsley('validate');
			if($('input[name=incomeHighOrLow]').parsley('isValid')==false){
				flag = false;
			}
		}
		*/
	}
	else if(currentPage1 == "appscr76P1")											// Validation on Page appscr76P1
	{
		$('input[name=wasUninsuredFromLast6Month]').parsley('validate');
		if($('input[name=wasUninsuredFromLast6Month]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=isHealthCoverageThroughJob]').parsley('validate');
		if($('input[name=isHealthCoverageThroughJob]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=willHealthCoverageThroughJob]').parsley('validate');
		if($('input[name=willHealthCoverageThroughJob]').parsley('isValid')==false){
			flag = false;
		}

		if($('#appscr76P1FirstHideAndShowDiv').is(':visible')){
			$('#appscr76P1HealthCoverageDate').parsley('validate');
			if($('#appscr76P1HealthCoverageDate').parsley('isValid')==false){
				flag = false;
			}


			if(!$('input[name=healthCoverageEmployee]').is(':checked')){
				$('input[name=healthCoverageEmployee]').parsley('validate');
				if($('input[name=healthCoverageEmployee]').parsley('isValid')==false){
					flag = false;
				}
			}


		}
		/*if(!$('input[name=healthCoverageEmployee]').is(':checked')){
			$('input[name=healthCoverageEmployee]').parsley('validate');
			if($('input[name=healthCoverageEmployee]').parsley('isValid')==false){
				flag = false;
			}
		}
		*/


		if($('#memberNameDataDivPart1').is(':visible')){
			$('#memberNameDataEinText').parsley('validate');
			if($('#memberNameDataEinText').parsley('isValid')==false){
				flag = false;
			}

			$('#memberNameDataAddress1').parsley('validate');
			if($('#memberNameDataAddress1').parsley('isValid')==false){
				flag = false;
			}

			$('#memberNameDataCity').parsley('validate');
			if($('#memberNameDataCity').parsley('isValid')==false){
				flag = false;
			}

			$('#memberNameDataZip').parsley('validate');
			if($('#memberNameDataZip').parsley('isValid')==false){
				flag = false;
			}

			$('#memberNameDataState').parsley('validate');
			if($('#memberNameDataState').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#memberNameDataDivPart2').is(':visible')){
			$('#appscr76P1_otherEmployerName').parsley('validate');
			if($('#appscr76P1_otherEmployerName').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr76P1_employerEIN').parsley('validate');
			if($('#appscr76P1_employerEIN').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr76P1_otherEmployerAddress1').parsley('validate');
			if($('#appscr76P1_otherEmployerAddress1').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr76P1_otherEmployerCity').parsley('validate');
			if($('#appscr76P1_otherEmployerCity').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr76P1_otherEmployerZip').parsley('validate');
			if($('#appscr76P1_otherEmployerZip').parsley('isValid')==false){
				flag = false;
			}

			$('#appscr76P1_otherEmployerState').parsley('validate');
			if($('#appscr76P1_otherEmployerState').parsley('isValid')==false){
				flag = false;
			}
		}




	}
	else if(currentPage1 == "appscr76P2")											// Validation on Page appscr76P2
	{
		$('input[name=appscr76p2radiosgroup]').parsley('validate');
		if($('input[name=appscr76p2radiosgroup]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=willBeEnrolledInHealthCoverageInFollowingYear]').parsley('validate');
		if($('input[name=willBeEnrolledInHealthCoverageInFollowingYear]').parsley('isValid')==false){
			flag = false;
		}
	}
	else if(currentPage1 =="appscr77Part1")                    //appscr77Part1 Page Validation
	{
		$('input[name=77P1CurrentlyEnrolledInHealthPlan]').parsley('validate');
		if($('input[name=77P1CurrentlyEnrolledInHealthPlan]').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=77P1WillBeEnrolledInHealthPlan]').parsley('validate');
		if($('input[name=77P1WillBeEnrolledInHealthPlan]').parsley('isValid')==false){
			flag = false;
		}

		if($('#DateToCovered').is(':visible')){
			$('#77P1WillBeEnrolledInHealthPlanDate').parsley('validate');
			if($('#77P1WillBeEnrolledInHealthPlanDate').parsley('isValid')==false){
				flag = false;
			}
		}

		$('#77P1NoLongerHealthCoverageDate').parsley('validate');
		if($('#77P1NoLongerHealthCoverageDate').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=77P1ExpectChangesToHealthCoverage]').parsley('validate');
		if($('input[name=77P1ExpectChangesToHealthCoverage]').parsley('isValid')==false){
			flag = false;
		}

		if($('.doesExceptAnyChangeToHealthCoverageInappscr77Part1').is(':visible')){

			$('#77P1PlanToDropHealthCoverageDate').parsley('validate');
			if($('#77P1PlanToDropHealthCoverageDate').parsley('isValid')==false){
				flag = false;
			}

			$('#77P1WillOfferCoverageDate').parsley('validate');
			if($('#77P1WillOfferCoverageDate').parsley('isValid')==false){
				flag = false;
			}

			$('#77P1PlaningToEnrollInHCDate').parsley('validate');
			if($('#77P1PlaningToEnrollInHCDate').parsley('isValid')==false){
				flag = false;
			}

			$('#77P1HealthPlanOptionsGoingToChangeDate').parsley('validate');
			if($('#77P1HealthPlanOptionsGoingToChangeDate').parsley('isValid')==false){
				flag = false;
			}
		}

		$('#currentLowestCostSelfOnlyPlanName').parsley('validate');
		if($('#currentLowestCostSelfOnlyPlanName').parsley('isValid')==false){
			flag = false;
		}

		$('#comingLowestCostSelfOnlyPlanName').parsley('validate');
		if($('#comingLowestCostSelfOnlyPlanName').parsley('isValid')==false){
			flag = false;
		}

		$('#lowestCostSelfOnlyPremiumAmount').parsley('validate');
		if($('#lowestCostSelfOnlyPremiumAmount').parsley('isValid')==false){
			flag = false;
		}

		$('#premiumPlansStatus').parsley('validate');
		if($('#premiumPlansStatus').parsley('isValid')==false){
			flag = false;
		}

		$('input[name=appscr77p2IsAffordable]').parsley('validate');
		if($('input[name=appscr77p2IsAffordable]').parsley('isValid')==false){
			flag = false;
		}

	}
	else if(currentPage1 =="appscr77Part2")                    //appscr77Part2 Page Validation
	{

	}
	else if(currentPage1 =="appscr78")                    //appscr78 Page Validation
	{

	}
	else if(currentPage1 =="appscr79")                    //appscr79 Page Validation
	{

	}
	else if(currentPage1 =="appscr80")                    //appscr80 Page Validation
	{
		if(householdSSNIndicationValue == "no")
		if($('input:radio[name=ssnIndicator80]:checked').val() == "yes")
		{
			if($('#ssn1_80').val()=="")
			{
				$('#ssn1_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn1_80");
				if(idFocus == "")
					idFocus ="ssn1_80";
				    flag = false;
			}
			if($('#ssn2_80').val()=="")
			{
				$('#ssn2_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn2_80");
				if(idFocus == "")
					idFocus ="ssn2_80";
				flag = false;
			}

			if($('#ssn3_80').val()=="")
			{
				$('#ssn3_80').addClass("validateColor");
				addLabels('Enter the SSN Number', "ssn3_80");
				if(idFocus == "")
					idFocus ="ssn3_80";
				flag = false;
			}
		}
		else
		{
			$('#ssn1_80').removeClass("validateColor");
			$('#ssn2_80').removeClass("validateColor");
			$('#ssn3_80').removeClass("validateColor");


		}


	}
	else if(currentPage1 =="appscr81B")                    //appscr81B Page Validation
	{


	}
	else if(currentPage1 =="appscr81C")                    //appscr81C Page Validation
	{


	}
	else
	{

		$('#prevBtn').show();
		flag = true;
	}

	$('#'+idFocus).focus();
	return flag;
}


//Check Date Format
function checkDate(date)
{

	var validformat = /^\d{2}\/\d{2}\/\d{4}$/;

	if (!validformat.test(date))
		return false;
	else
	{
		var monthfield = date.split("/")[0];
		var dayfield = date.split("/")[1];
		var yearfield = date.split("/")[2];


		var fdate=new Date();


	    var dayobj = new Date(yearfield, monthfield - 1, dayfield);

	    if(fdate.getTime()<=dayobj.getTime())
	        {
	    			return false;
	        }


		if ((dayobj.getMonth() + 1 != monthfield) || (dayobj.getDate() != dayfield)	|| (dayobj.getFullYear() != yearfield))
			return false;
		else
			return true;
	}
}


//Check Email ID Format
function checkEmail(email)
{
	var a="";
	var b="";
	var c="";

	try
	{
		a = email.split("@")[0];

		if(a!=undefined)
		{
		b = email.split("@")[1];
		}

		if(a!=undefined)
			c = b.split(".")[1];
	}
	catch(Ex)
	{}


	if(a==""|| c==""|| b==undefined || c==undefined)
	{
		return false;

	}
	else
		return true;
}




//check Number
function checkNumber(num)
{
	var reg=/[0-9 -()+]+$/;

	if (!reg.test(num))
		return false;

	else
		return true;
}

//check date
function checkDob(date)
{
	var validformat = /^\d{2}\/\d{2}\/\d{4}$/;

	if (!validformat.test(date))
		return false;
	else
		return true;
}

//String Checking
function checkString(str)
{

	var reg = new RegExp("^[a-zA-Z ]+$");
	alert(reg.test(str));
	if (!reg.test(str))
		return false;
	else
		return true;
}

// check zip code
function checkZip_code(zipcode)
{
	alert(zipcode);
var zip=/^[0-9]{5}$/;
alert(zip.test(zipcode));
if(!zip.test(zipcode))
	return false;
else
	return true;


}



function emptyLabels()
{
	$('#labelsAdd').html('');
}



// 		Remove Validation Class

function removeValidationClass(currentPage1)
{
	emptyLabels();		// Empty labels

	if(currentPage1 == "appscr53")
	{
		$('#firstName').removeClass("validateColor");
		$('#lastName').removeClass("validateColor");
		$('#dateOfBirth').removeClass("validateColor");
		$('#emailAddress').removeClass("validateColor");
		$('#home_addressLine1').removeClass("validateColor");
		$('#home_primary_city').removeClass("validateColor");
		$('#home_primary_zip').removeClass("validateColor");
		$('#home_primary_state').removeClass("validateColor");
		$('#home_primary_county').removeClass("validateColor");
	}
	else if(currentPage1 == "appscr54")
	{
		$('#healthInsurBroker_name').removeClass("validateColor");
		$('#healthInsurBroker_id').removeClass("validateColor");
		$('#healthCoverageGuide_name').removeClass("validateColor");
		$('#healthCoverageGuide_id').removeClass("validateColor");
	}
	else if(currentPage1 == "appscr57")
	{

	}
}


//Add labels

function addLabels(msg, idname)
{
	var divname = idname+"_div";

	var str = "<div  id="+divname+"><label class='validatelblColor'><br />Please "+ msg + "</label></div>";

	$('#labelsAdd').append(str);
}


function otherAddressNoneOfTheAbove()
{

	if($('#HouseHoldotherAddressNoneOfTheAbove').is(":checked") == true)
	{
		for(var i=2;i<=noOfHouseHold;i++)
		{
			//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			//var HouseHoldName = householdMemberObj.name.firstName + "_" + householdMemberObj.name.middleName + "_" + householdMemberObj.name.lastName;

			document.getElementById('householdContactAddress'+i).checked = false;

			$('#other_Address_'+i).hide();

		}

	}
}


/// All Income Validations
function allIncomeValidations(){
	//console.log(idFocus,'---idFocus')

	if($('#Financial_Job_Div').is(':visible')){
		$('#Financial_Job_EmployerName_id').parsley('validate');
		if($('#Financial_Job_EmployerName_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Job_id').parsley('validate');
		if($('#Financial_Job_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Job_select_id').parsley('validate');
		if($('#Financial_Job_select_id').parsley('isValid')==false){
			flag = false;
		}

		if($('#howMuchPerWeek').is(':visible')){
			$('#Financial_Job_HoursOrDays_id').parsley('validate');
			if($('#Financial_Job_HoursOrDays_id').parsley('isValid')==false){
				flag = false;
			}
		}
	}

	if($('#Financial_Self_Employment_Div').is(':visible')){
		$('#Financial_Self_Employment_TypeOfWork').parsley('validate');
		if($('#Financial_Self_Employment_TypeOfWork').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Self_Employment_id').parsley('validate');
		if($('#Financial_Self_Employment_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_SocialSecuritybenefits_Div').is(':visible')){

		$('#Financial_SocialSecuritybenefits_id').parsley('validate');
		if($('#Financial_SocialSecuritybenefits_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_SocialSecuritybenefits_select_id').parsley('validate');
		if($('#Financial_SocialSecuritybenefits_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_Unemployment_Div').is(':visible')){

		$('#Financial_Unemployment_StateGovernment').parsley('validate');
		if($('#Financial_Unemployment_StateGovernment').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Unemployment_id').parsley('validate');
		if($('#Financial_Unemployment_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Unemployment_select_id').parsley('validate');
		if($('#Financial_Unemployment_select_id').parsley('isValid')==false){
			flag = false;
		}

		if($('#isDateUnemploymentSet').is(':visible')){
			$('input[name=payPerWeekRadio]').parsley('validate');
			if($('input[name=payPerWeekRadio]').parsley('isValid')==false){
				flag = false;
			}
		}

		if($('#dateEmployementExpire').is(':visible')){
			$('#unemploymentIncomeDate').parsley('validate');
			if($('#unemploymentIncomeDate').parsley('isValid')==false){
				flag = false;
			}
		}
	}


	if($('#Financial_Retirement_pension_Div').is(':visible')){
		$('#Financial_Retirement_pension_id').parsley('validate');
		if($('#Financial_Retirement_pension_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_Retirement_pension_select_id').parsley('validate');
		if($('#Financial_Retirement_pension_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_Capitalgains_Div').is(':visible')){
		$('#Financial_Capitalgains_id').parsley('validate');
		if($('#Financial_Capitalgains_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_InvestmentIncome_Div').is(':visible')){
		$('#Financial_InvestmentIncome_id').parsley('validate');
		if($('#Financial_InvestmentIncome_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_InvestmentIncome_select_id').parsley('validate');
		if($('#Financial_InvestmentIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_RentalOrRoyaltyIncome_Div').is(':visible')){
		$('#Financial_RentalOrRoyaltyIncome_id').parsley('validate');
		if($('#Financial_RentalOrRoyaltyIncome_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_RentalOrRoyaltyIncome_select_id').parsley('validate');
		if($('#Financial_RentalOrRoyaltyIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_FarmingOrFishingIncome_Div').is(':visible')){
		$('#Financial_FarmingOrFishingIncome_id').parsley('validate');
		if($('#Financial_FarmingOrFishingIncome_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_FarmingOrFishingIncome_select_id').parsley('validate');
		if($('#Financial_FarmingOrFishingIncome_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_AlimonyReceived_Div').is(':visible')){
		$('#Financial_AlimonyReceived_id').parsley('validate');
		if($('#Financial_AlimonyReceived_id').parsley('isValid')==false){
			flag = false;
		}

		$('#Financial_AlimonyReceived_select_id').parsley('validate');
		if($('#Financial_AlimonyReceived_select_id').parsley('isValid')==false){
			flag = false;
		}
	}

	if($('#Financial_OtherIncome_Div').is(':visible')){
		$('input[name=otherIncome]').parsley('validate');
		if($('input[name=otherIncome]').parsley('isValid')==false){
			flag = false;
		}
	}

	if(($('#cancelled_Debts').is(':visible')) && ($('#cancelledDebtsFrequencyDiv').is(':visible'))){

		$('#cancelled_Debts_amount').parsley('validate');
		if($('#cancelled_Debts_amount').parsley('isValid')==false){
			flag = false;
		}

		$('#cancelledDebtsFrequency').parsley('validate');
		if($('#cancelledDebtsFrequency').parsley('isValid')==false){
			flag = false;
		}
	}

	if(($('#court_awrds').is(':visible')) && ($('#courtAwrdsFrequencyDiv').is(':visible'))){

		$('#court_awards_amount').parsley('validate');
		if($('#court_awards_amount').parsley('isValid')==false){
			flag = false;
		}

		$('#courtAwrdsFrequency').parsley('validate');
		if($('#courtAwrdsFrequency').parsley('isValid')==false){
			flag = false;
		}
	}

	if(($('#jury_duty_pay').is(':visible')) && ($('#juryDutyPayFrequencyDiv').is(':visible'))){

		$('#jury_duty_pay_amount').parsley('validate');
		if($('#jury_duty_pay_amount').parsley('isValid')==false){
			flag = false;
		}

		$('#juryDutyPayFrequency').parsley('validate');
		if($('#juryDutyPayFrequency').parsley('isValid')==false){
			flag = false;
		}
	}
}



// check is member age less than 365 days or not
// gets two dates in string and compare
function checkBirthDate(BirthDate)
{


	var serverOfDate = $('#currentDate').val();



var serverDate= new Date(serverOfDate);
var houseHoldDob= new Date(BirthDate);


var diffrence = parseInt((serverDate-houseHoldDob)/(1000*60*60*24));


return diffrence;


}

//check num of female that are 13 or above
function checkPregnantCount()
{
	var pregnantAgeCount=0;
	for (var i = 1; i <= noOfHouseHold; i++){
		var fDob = $('#appscr57DOBTD'+i).text();
		if(checkBirthDate(fDob)>(365*13)){
			pregnantAgeCount++;
		}
	}

	return pregnantAgeCount;
}



//Set authorized name and Household name
function setAuthorizedName()
{
var authorizedName = $('#authorizedFirstName').val() + " "+ $('#authorizedMiddleName').val() + " "+ $('#authorizedLastName').val()+" ";

if($('#authorizedFirstName').val()== "" && $('#authorizedMiddleName').val()== "" && $('#authorizedLastName').val()== "")
	$('.appscr54_authorizedName').text("Authorized Representative ");
else
	$('.appscr54_authorizedName').text(authorizedName);

}


// Masking at appscr57
function addMaskingOnAppscr57()
{
	for(var i=1; i<=noOfHouseHold; i++)
	{
		$('#appscr57DOB'+i).mask("00/00/0000");

	}

	$('#parent-or-caretaker-dob, #appscr76P1HealthCoverageDate, #77P1PlanToDropHealthCoverageDate, #77P1WillOfferCoverageDate, #77P1PlaningToEnrollInHCDate, #77P1HealthPlanOptionsGoingToChangeDate').mask("00/00/0000");

}

function checkOnlyDateFormate(date)
{
	var flag = true;

	var month = date.split("/")[0];
	var day = date.split("/")[1];
	var year = date.split("/")[2];

	if(month != undefined && day != undefined && year != undefined)
	{
		if(month.length == 2 && month<=12 && month>0)
		{
			if(day.length == 2 && day<=31 && day>0)
			{
				if(year.length == 4)
					flag = true;
				else
					flag = false;
			}
			else
				flag = false;
		}
		else
			flag = false;
	}
	else
		flag = false;

	return flag;
}

/*
****************************************************************************************************************
* magiValidationEvents.js                                                                                        *
****************************************************************************************************************
*/


var namesOfHouseHold = [];


function setControls(){

	//$('.coverageYear').text('2015');

																		//Events for Page_ID appscr53
	$('#firstName').click(function(){
		$('#firstName').removeClass('validateColor');
		$('#firstName_div').remove();
	});

	$('#firstName').keyup(function(){
		$('#firstName').removeClass('validateColor');
		$('#firstName_div').remove();
	});


	$('#dateOfBirth').click(function(){
		$('#dateOfBirth').removeClass('validateColor');
		$('#dateOfBirth_div').remove();
	});

	$('#dateOfBirth').keyup(function(){
		$('#dateOfBirth').removeClass('validateColor');
		$('#dateOfBirth_div').remove();
	});

	$('#sepEventDate').click(function(){
		$('#sepEventDate').removeClass('validateColor');
		$('.dobCustomError').remove();
	});

	$('#sepEventDate').keyup(function(){
		$('#sepEventDate').removeClass('validateColor');
		$('.dobCustomError').remove();
	});

	$('#emailAddress').click(function(){
		$('#emailAddress').removeClass('validateColor');
		$('#emailAddress_div').remove();
	});

	$('#emailAddress').keyup(function(){
		$('#emailAddress').removeClass('validateColor');
		$('#emailAddress_div').remove();
	});

	$('#home_addressLine1').click(function(){
		$('#home_addressLine1').removeClass('validateColor');
		$('#home_addressLine1_div').remove();
	});

	$('#home_addressLine1').keyup(function(){
		$('#home_addressLine1').removeClass('validateColor');
		$('#home_addressLine1_div').remove();
	});

	$('#home_primary_city').click(function(){
		$('#home_primary_city').removeClass('validateColor');
		$('#home_primary_city_div').remove();
	});

	$('#home_primary_city').keyup(function(){
		$('#home_primary_city').removeClass('validateColor');
		$('#home_primary_city_div').remove();
	});

	$('#home_primary_zip').click(function(){
		$('#home_primary_zip').removeClass('validateColor');
		$('#home_primary_zip_div').remove();
	});

	$('#home_primary_zip').keyup(function(){
		$('#home_primary_zip').removeClass('validateColor');
		$('#home_primary_zip_div').remove();
	});

	$('#home_primary_state').click(function(){
		$('#home_primary_state').removeClass('validateColor');
		$('#home_primary_state_div').remove();
	});

	$('#home_primary_state').keyup(function(){
		$('#home_primary_state').removeClass('validateColor');
		$('#home_primary_state_div').remove();
	});

	$('#home_primary_county').click(function(){
		$('#home_primary_county').removeClass('validateColor');
		$('#home_primary_county_div').remove();
	});

	$('#home_primary_county').keyup(function(){
		$('#home_primary_county').removeClass('validateColor');
		$('#home_primary_county_div').remove();
	});

	$('#mailing_primary_state').click(function(){
		$('#mailing_primary_state').removeClass('validateColor');
		$('#mailing_primary_state_div').remove();
	});

	$('#mailing_primary_state').keyup(function(){
		$('#mailing_primary_state').removeClass('validateColor');
		$('#mailing_primary_state_div').remove();
	});




																					//Events for Page_ID appscr54



	$('#authorizedFirstName').click(function(){
		$('#authorizedFirstName').removeClass('validateColor');
	});

	$('#authorizedFirstName').keyup(function(){
		$('#authorizedFirstName').removeClass('validateColor');
	});

	$('#authorizedLastName').click(function(){
		$('#authorizedLastName').removeClass('validateColor');
	});

	$('#authorizedLastName').keyup(function(){
		$('#authorizedLastName').removeClass('validateColor');
	});

	$('#authorizedEmailAddress').click(function(){
		$('#authorizedEmailAddress').removeClass('validateColor');
	});

	$('#authorizedEmailAddress').keyup(function(){
		$('#authorizedEmailAddress').removeClass('validateColor');
	});

	$('#authorizedAddress1').click(function(){
		$('#authorizedAddress1').removeClass('validateColor');
	});

	$('#authorizedAddress1').keyup(function(){
		$('#authorizedAddress1').removeClass('validateColor');
	});

	$('#authorizedCity').click(function(){
		$('#authorizedCity').removeClass('validateColor');
	});

	$('#authorizedCity').keyup(function(){
		$('#authorizedCity').removeClass('validateColor');
	});

	$('#authorizedZip').click(function(){
		$('#authorizedZip').removeClass('validateColor');
	});

	$('#authorizedZip').keyup(function(){
		$('#authorizedZip').removeClass('validateColor');
	});

	$('#authorizedState').click(function(){
		$('#authorizedState').removeClass('validateColor');
	});

	$('#authorizedState').keyup(function(){
		$('#authorizedState').removeClass('validateColor');
	});

	$('#authorizeCompanyName').click(function(){
		$('#authorizeCompanyName').removeClass('validateColor');
	});

	$('#authorizeCompanyName').keyup(function(){
		$('#authorizeCompanyName').removeClass('validateColor');
	});

	$('#authorizeOrganizationId').click(function(){
		$('#authorizeOrganizationId').removeClass('validateColor');
	});

	$('#authorizeOrganizationId').keyup(function(){
		$('#authorizeOrganizationId').removeClass('validateColor');
	});

	$('#authorizeSignature').click(function(){
		$('#authorizeSignature').removeClass('validateColor');
	});

	$('#authorizeSignature').keyup(function(){
		$('#authorizeSignature').removeClass('validateColor');
	});


	// PageID 55

	$('#noOfPeople53').click(function(){
		$('#noOfPeople53').removeClass('validateColor');
	});

	$('#noOfPeople53').keyup(function(){
		$('#noOfPeople53').removeClass('validateColor');
	});


																					//Events for Page_ID appscr60

	$('#spouseFirstName').click(function(){
		$('#spouseFirstName').removeClass('validateColor');
	});

	$('#spouseFirstName').keyup(function(){
		$('#spouseFirstName').removeClass('validateColor');
	});

	$('#spouseMiddleName').click(function(){
		$('#spouseMiddleName').removeClass('validateColor');
	});

	$('#spouseMiddleName').keyup(function(){
		$('#spouseMiddleName').removeClass('validateColor');
	});

	$('#spouseLastName').click(function(){
		$('#spouseLastName').removeClass('validateColor');
	});

	$('#spouseLastName').keyup(function(){
		$('#spouseLastName').removeClass('validateColor');
	});

	$('#spouseDOB').click(function(){
		$('#spouseDOB').removeClass('validateColor');
	});

	$('#spouseDOB').keyup(function(){
		$('#spouseDOB').removeClass('validateColor');
	});



	$('#taxFilerFirstName').click(function(){
		$('#taxFilerFirstName').removeClass('validateColor');
	});

	$('#taxFilerFirstName').keyup(function(){
		$('#taxFilerFirstName').removeClass('validateColor');
	});

	$('#taxFilerMiddleName').click(function(){
		$('#taxFilerMiddleName').removeClass('validateColor');
	});

	$('#taxFilerMiddleName').keyup(function(){
		$('#taxFilerMiddleName').removeClass('validateColor');
	});

	$('#taxFilerLastName').click(function(){
		$('#taxFilerLastName').removeClass('validateColor');
	});

	$('#taxFilerLastName').keyup(function(){
		$('#taxFilerLastName').removeClass('validateColor');
	});

	$('#taxFilerDob').click(function(){
		$('#taxFilerDob').removeClass('validateColor');
	});

	$('#taxFilerDob').keyup(function(){
		$('#taxFilerDob').removeClass('validateColor');
	});


																					//Events for Page_ID appscr61

	$('#ssn1').click(function(){
		$('#ssn1').removeClass('validateColor');
		$('#ssn1_div').remove();
	});

	$('#ssn1').keyup(function(){
		$('#ssn1').removeClass('validateColor');
		$('#ssn1_div').remove();

		if(($('#ssn1').val()).length == 3)
			$('#ssn2').focus();
	});

	$('#ssn2').click(function(){
		$('#ssn2').removeClass('validateColor');
		$('#ssn2_div').remove();
	});

	$('#ssn2').keyup(function(){
		$('#ssn2').removeClass('validateColor');
		$('#ssn2_div').remove();

		if(($('#ssn2').val()).length == 2)
			$('#ssn3').focus();
	});

	$('#ssn3').click(function(){
		$('#ssn3').removeClass('validateColor');
		$('#ssn3_div').remove();
	});

	$('#ssn3').keyup(function(){
		$('#ssn3').removeClass('validateColor');
		$('#ssn3_div').remove();
	});

	$('#firstNameOnSSNCard').click(function(){
		$('#firstNameOnSSNCard').removeClass('validateColor');
	});

	$('#firstNameOnSSNCard').keyup(function(){
		$('#firstNameOnSSNCard').removeClass('validateColor');
	});

	$('#middleNameOnSSNCard').click(function(){
		$('#middleNameOnSSNCard').removeClass('validateColor');
	});

	$('#middleNameOnSSNCard').keyup(function(){
		$('#middleNameOnSSNCard').removeClass('validateColor');
	});

	$('#lastNameOnSSNCard').click(function(){
		$('#lastNameOnSSNCard').removeClass('validateColor');
	});

	$('#lastNameOnSSNCard').keyup(function(){
		$('#lastNameOnSSNCard').removeClass('validateColor');
	});

	$('#suffixOnSSNCard').click(function(){
		$('#suffixOnSSNCard').removeClass('validateColor');
	});

	$('#suffixOnSSNCard').keyup(function(){
		$('#suffixOnSSNCard').removeClass('validateColor');
	});

	$('#reasonableExplanationForNoSSN').click(function(){
		$('#reasonableExplanationForNoSSN').removeClass('validateColor');
	});

	$('#reasonableExplanationForNoSSN').keyup(function(){
		$('#reasonableExplanationForNoSSN').removeClass('validateColor');
	});

	$('#exemptionId').click(function(){
		$('#exemptionId').removeClass('validateColor');
	});

	$('#exemptionId').keyup(function(){
		$('#exemptionId').removeClass('validateColor');
	});


																						//Events for Page_ID appscr62Part1



	$('#DocAlignNumber').click(function(){
		$('#DocAlignNumber').removeClass('validateColor');
	});

	$('#DocAlignNumber').keyup(function(){
		$('#DocAlignNumber').removeClass('validateColor');
	});

	$('#DocI-94Number').click(function(){
		$('#DocI-94Number').removeClass('validateColor');
	});

	$('#DocI-94Number').keyup(function(){
		$('#DocI-94Number').removeClass('validateColor');
	});

	$('#passportDocNumber').click(function(){
		$('#passportDocNumber').removeClass('validateColor');
	});

	$('#passportDocNumber').keyup(function(){
		$('#passportDocNumber').removeClass('validateColor');
	});

	$('#appscr62P1County').click(function(){
		$('#appscr62P1County').removeClass('validateColor');
	});

	$('#appscr62P1County').keyup(function(){
		$('#appscr62P1County').removeClass('validateColor');
	});

	$('#passportExpDate').click(function(){
		$('#passportExpDate').removeClass('validateColor');
	});

	$('#passportExpDate').keyup(function(){
		$('#passportExpDate').removeClass('validateColor');
	});

	$('#sevisIDNumber').click(function(){
		$('#sevisIDNumber').removeClass('validateColor');
	});

	$('#sevisIDNumber').keyup(function(){
		$('#sevisIDNumber').removeClass('validateColor');
	});


																					//Events for Page_ID appscr62Part2


																						//Events for Page_ID appscr63



																									//Events for Page_ID appscr64

	$('#other_ethnicity').click(function(){
		$('#other_ethnicity').removeClass('validateColor');
	});

	$('#other_ethnicity').keyup(function(){
		$('#other_ethnicity').removeClass('validateColor');
	});

	$('#other_race').click(function(){
		$('#other_race').removeClass('validateColor');
	});

	$('#other_race').keyup(function(){
		$('#other_race').removeClass('validateColor');
	});


																								//Events for Page_ID appscr69

	$('#irsIncome').click(function(){
		$('#irsIncome').removeClass('validateColor');
		$('#irsIncome_div').remove();
	});

	$('#irsIncome').keyup(function(){
		$('#irsIncome').removeClass('validateColor');
		$('#irsIncome_div').remove();
	});



																					//Events for Page_ID appscr71

	$('#Financial_Job_EmployerName_id').click(function(){
		$('#Financial_Job_EmployerName_id').removeClass('validateColor');
		$('#Financial_Job_EmployerName_id_div').remove();
	});

	$('#Financial_Job_EmployerName_id').keyup(function(){
		$('#Financial_Job_EmployerName_id').removeClass('validateColor');
		$('#Financial_Job_EmployerName_id_div').remove();
	});

	$('#Financial_Job_id').click(function(){
		$('#Financial_Job_id').removeClass('validateColor');
		$('#Financial_Job_id_div').remove();
	});

	$('#Financial_Job_id').keyup(function(){
		$('#Financial_Job_id').removeClass('validateColor');
		$('#Financial_Job_id_div').remove();
	});

	$('#Financial_Job_select_id').click(function(){
		$('#Financial_Job_select_id').removeClass('validateColor');
		$('#Financial_Job_select_id_div').remove();
	});

	$('#Financial_Job_select_id').keyup(function(){
		$('#Financial_Job_select_id').removeClass('validateColor');
		$('#Financial_Job_select_id_div').remove();
	});

	$('#Financial_Self_Employment_id').click(function(){
		$('#Financial_Self_Employment_id').removeClass('validateColor');
		$('#Financial_Self_Employment_id_div').remove();
	});

	$('#Financial_Self_Employment_id').keyup(function(){
		$('#Financial_Self_Employment_id').removeClass('validateColor');
		$('#Financial_Self_Employment_id_div').remove();
	});

	$('#Financial_SocialSecuritybenefits_id').click(function(){
		$('#Financial_SocialSecuritybenefits_id').removeClass('validateColor');
		$('#Financial_SocialSecuritybenefits_id_div').remove();
	});

	$('#Financial_SocialSecuritybenefits_id').keyup(function(){
		$('#Financial_SocialSecuritybenefits_id').removeClass('validateColor');
		$('#Financial_SocialSecuritybenefits_id_div').remove();
	});

	$('#Financial_SocialSecuritybenefits_select_id').click(function(){
		$('#Financial_SocialSecuritybenefits_select_id').removeClass('validateColor');
		$('#Financial_SocialSecuritybenefits_select_id_div').remove();
	});

	$('#Financial_SocialSecuritybenefits_select_id').keyup(function(){
		$('#Financial_SocialSecuritybenefits_select_id').removeClass('validateColor');
		$('#Financial_SocialSecuritybenefits_select_id_div').remove();
	});

	$('#Financial_Unemployment_id').click(function(){
		$('#Financial_Unemployment_id').removeClass('validateColor');
		$('#Financial_Unemployment_id_div').remove();
	});

	$('#Financial_Unemployment_id').keyup(function(){
		$('#Financial_Unemployment_id').removeClass('validateColor');
		$('#Financial_Unemployment_id_div').remove();
	});

	$('#Financial_Unemployment_select_id').click(function(){
		$('#Financial_Unemployment_select_id').removeClass('validateColor');
		$('#Financial_Unemployment_select_id_div').remove();
	});

	$('#Financial_Unemployment_select_id').keyup(function(){
		$('#Financial_Unemployment_select_id').removeClass('validateColor');
		$('#Financial_Unemployment_select_id_div').remove();
	});

	$('#Financial_Retirement_pension_id').click(function(){
		$('#Financial_Retirement_pension_id').removeClass('validateColor');
		$('#Financial_Retirement_pension_id_div').remove();
	});

	$('#Financial_Retirement_pension_id').keyup(function(){
		$('#Financial_Retirement_pension_id').removeClass('validateColor');
		$('#Financial_Retirement_pension_id_div').remove();
	});

	$('#Financial_Retirement_pension_select_id').click(function(){
		$('#Financial_Retirement_pension_select_id').removeClass('validateColor');
		$('#Financial_Retirement_pension_select_id_div').remove();
	});

	$('#Financial_Retirement_pension_select_id').keyup(function(){
		$('#Financial_Retirement_pension_select_id').removeClass('validateColor');
		$('#Financial_Retirement_pension_select_id_div').remove();
	});

	$('#Financial_Capitalgains_id').click(function(){
		$('#Financial_Capitalgains_id').removeClass('validateColor');
		$('#Financial_Capitalgains_id_div').remove();
	});

	$('#Financial_Capitalgains_id').keyup(function(){
		$('#Financial_Capitalgains_id').removeClass('validateColor');
		$('#Financial_Capitalgains_id_div').remove();
	});

	$('#Financial_InvestmentIncome_id').click(function(){
		$('#Financial_InvestmentIncome_id').removeClass('validateColor');
		$('#Financial_InvestmentIncome_id_div').remove();
	});

	$('#Financial_InvestmentIncome_id').keyup(function(){
		$('#Financial_InvestmentIncome_id').removeClass('validateColor');
		$('#Financial_InvestmentIncome_id_div').remove();
	});

	$('#Financial_InvestmentIncome_select_id').click(function(){
		$('#Financial_InvestmentIncome_select_id').removeClass('validateColor');
		$('#Financial_InvestmentIncome_select_id_div').remove();
	});

	$('#Financial_InvestmentIncome_select_id').keyup(function(){
		$('#Financial_InvestmentIncome_select_id').removeClass('validateColor');
		$('#Financial_InvestmentIncome_select_id_div').remove();
	});

	$('#Financial_RentalOrRoyaltyIncome_id').click(function(){
		$('#Financial_RentalOrRoyaltyIncome_id').removeClass('validateColor');
		$('#Financial_RentalOrRoyaltyIncome_id_div').remove();
	});

	$('#Financial_RentalOrRoyaltyIncome_id').keyup(function(){
		$('#Financial_RentalOrRoyaltyIncome_id').removeClass('validateColor');
		$('#Financial_RentalOrRoyaltyIncome_id_div').remove();
	});

	$('#Financial_RentalOrRoyaltyIncome_select_id').click(function(){
		$('#Financial_RentalOrRoyaltyIncome_select_id').removeClass('validateColor');
		$('#Financial_RentalOrRoyaltyIncome_select_id_div').remove();
	});

	$('#Financial_RentalOrRoyaltyIncome_select_id').keyup(function(){
		$('#Financial_RentalOrRoyaltyIncome_select_id').removeClass('validateColor');
		$('#Financial_RentalOrRoyaltyIncome_select_id_div').remove();
	});

	$('#Financial_FarmingOrFishingIncome_id').click(function(){
		$('#Financial_FarmingOrFishingIncome_id').removeClass('validateColor');
		$('#Financial_FarmingOrFishingIncome_id_div').remove();
	});

	$('#Financial_FarmingOrFishingIncome_id').keyup(function(){
		$('#Financial_FarmingOrFishingIncome_id').removeClass('validateColor');
		$('#Financial_FarmingOrFishingIncome_id_div').remove();
	});

	$('#Financial_FarmingOrFishingIncome_select_id').click(function(){
		$('#Financial_FarmingOrFishingIncome_select_id').removeClass('validateColor');
		$('#Financial_FarmingOrFishingIncome_select_id_div').remove();
	});

	$('#Financial_FarmingOrFishingIncome_select_id').keyup(function(){
		$('#Financial_FarmingOrFishingIncome_select_id').removeClass('validateColor');
		$('#Financial_FarmingOrFishingIncome_select_id_div').remove();
	});

	$('#Financial_AlimonyReceived_id').click(function(){
		$('#Financial_AlimonyReceived_id').removeClass('validateColor');
		$('#Financial_AlimonyReceived_id_div').remove();
	});

	$('#Financial_AlimonyReceived_id').keyup(function(){
		$('#Financial_AlimonyReceived_id').removeClass('validateColor');
		$('#Financial_AlimonyReceived_id_div').remove();
	});

	$('#Financial_AlimonyReceived_select_id').click(function(){
		$('#Financial_AlimonyReceived_select_id').removeClass('validateColor');
		$('#Financial_AlimonyReceived_select_id_div').remove();
	});

	$('#Financial_AlimonyReceived_select_id').keyup(function(){
		$('#Financial_AlimonyReceived_select_id').removeClass('validateColor');
		$('#Financial_AlimonyReceived_select_id_div').remove();
	});

	$('#Financial_OtherIncome_id').click(function(){
		$('#Financial_OtherIncome_id').removeClass('validateColor');
		$('#Financial_OtherIncome_id_div').remove();
	});

	$('#Financial_OtherIncome_id').keyup(function(){
		$('#Financial_OtherIncome_id').removeClass('validateColor');
		$('#Financial_OtherIncome_id_div').remove();
	});

	$('#Financial_OtherIncome_select_id').click(function(){
		$('#Financial_OtherIncome_select_id').removeClass('validateColor');
		$('#Financial_OtherIncome_select_id_div').remove();
	});

	$('#Financial_OtherIncome_select_id').keyup(function(){
		$('#Financial_OtherIncome_select_id').removeClass('validateColor');
		$('#Financial_OtherIncome_select_id_div').remove();
	});






	//// page ID appscr73
	$('#basedOnAmountInput').click(function(){
		$('#basedOnAmountInput').removeClass('validateColor');
		$('#basedOnAmountInput_div').remove();
	});

	$('#basedOnAmountInput').keyup(function(){
		$('#basedOnAmountInput').removeClass('validateColor');
		$('#basedOnAmountInput_div').remove();

	});



																			//Events for Page_ID appscr76P1



																								//Events for Page_ID appscr76P2

	$('#appscr76P2_physicianName').click(function(){
		$('#appscr76P2_physicianName').removeClass('validateColor');
	});

	$('#appscr76P2_physicianName').keyup(function(){
		$('#appscr76P2_physicianName').removeClass('validateColor');
	});


	// onClick or onKeyUp event functions PageId 77Part1

	$('#77P1WillBeEnrolledInHealthPlanDate').click(function(){
		$('#77P1WillBeEnrolledInHealthPlanDate').removeClass('validateColor');
	});

	$('#77P1WillBeEnrolledInHealthPlanDate').keyup(function(){
		$('#77P1WillBeEnrolledInHealthPlanDate').removeClass('validateColor');
	});


	// onClick or onKeyUp Event functions

	$('#ssn1_80').click(function(){
		$('#ssn1_80').removeClass('validateColor');
		$('#ssn1_80_div').remove();
	});

	$('#ssn1_80').keyup(function(){
		$('#ssn1_80').removeClass('validateColor');
		$('#ssn1_80_div').remove();
	});

	$('#ssn2_80').click(function(){
		$('#ssn2_80').removeClass('validateColor');
		$('#ssn2_80_div').remove();
	});

	$('#ssn2_80').keyup(function(){
		$('#ssn2_80').removeClass('validateColor');
		$('#ssn2_80_div').remove();
	});

	$('#ssn3_80').click(function(){
		$('#ssn3_80').removeClass('validateColor');
		$('#ssn3_80_div').remove();
	});

	$('#ssn3_80').keyup(function(){
		$('#ssn3_80').removeClass('validateColor');
		$('#ssn3_80_div').remove();
	});



	setFunctions();

}


// onClick or onKeyUp Event functions															PageId appscr80
function onClickAt80SSNIndicator()
{
	$('#ssn1_80').removeClass("validateColor");
	$('#ssn2_80').removeClass("validateColor");
	$('#ssn3_80').removeClass("validateColor");



}


//onClick or onKeyUp Event Functions



/// HouseHold Added Validation Remove First Name															Page_ID appscr57
function removeValidationFirstName(i)
{
		$('#appscr57FirstName'+i).removeClass('validateColor');
		$('#appscr57FirstName'+i+'_div').remove();
}

/// HouseHold Added Validation Remove Last Name
function removeValidationLastName(i)
{
		$('#appscr57LastName'+i).removeClass('validateColor');
		$('#appscr57LastName'+i+'_div').remove();
}

/// HouseHold Added Validation Remove DOB
function removeValidationDob(i)
{
		$('#appscr57DOB'+i).removeClass('validateColor');
		$('#appscr57DOB'+i+'_div').remove();
}






/// Page Other Addresses Validation Remove DOB																	Page_ID appscr65
function removeValidationAddress1(i)
{
		$('#applicant_or_non-applican_address_1'+i).removeClass('validateColor');
}

/// Page Other Addresses Validation Remove DOB
function removeValidationCity(i)
{
		$('#city'+i).removeClass('validateColor');
}

/// Page Other Addresses Validation Remove DOB
function removeValidationZip(i)
{
		$('#zip'+i).removeClass('validateColor');
}

/// Page Other Addresses Validation Remove State
function removeValidationState(i)
{
		$('#applicant_or_non-applican_state'+i).removeClass('validateColor');
}


function removeValidationColoradoCity()
{
		$('#applicant2city').removeClass('validateColor');
}

//Page Other Addresses Validation Remove colorado remporarily Zip
function removeValidationColoradoZip()
{
		$('#applicant-2-zip').removeClass('validateColor');
}

//Page Other Addresses Validation Remove colorado remporarily State
function removeValidationColoradoState()
{
		$('#applicant_2_state').removeClass('validateColor');
}



/////  working of appscr70.jsp

function selectIncomesCheckBoxes()
{
	if($('input:radio[name=appscr70radios4Income]:checked').val() == "Yes")
	{


		var dataI=0;

		for ( ;dataI < financial_DetailsArray.length ; dataI++) {


			var data = financial_DetailsArray[dataI];

			$('#' + data).prop("checked", false);

		}

		document.getElementById('Financial_None').checked = false;
		//document.getElementById('Financial_Job').checked = true;


		$('#checkBoxes_div').show();
		$('#checkBoxes_divlbl').show();

		showDiductionsFields();

		$("#incomeSourceDiv").show();
	}
	else
	{
		var dataI=0;

		for ( ;dataI < financial_DetailsArray.length ; dataI++) {


			var data = financial_DetailsArray[dataI];



			$('#' + data).prop("checked", false);

		}

		document.getElementById('Financial_None').checked = true;

		$('#checkBoxes_div').hide();
		$('#checkBoxes_divlbl').hide();

		hideDiductionsFields();

		$("#incomeSourceDiv").hide();
	}

}

function noneOfTheseAboveIncome()
{
	if($('#Financial_None').is(":checked") == true)
	{


		var dataI=0;

		for ( ;dataI < financial_DetailsArray.length ; dataI++) {

			if(dataI==0)
				continue;
			var data = financial_DetailsArray[dataI];


			$('#' + data).prop("checked", false);
		}


		hideDiductionsFields();
	}
	else
	{

		var dataI=0;

		for ( ;dataI < financial_DetailsArray.length ; dataI++) {


			var data = financial_DetailsArray[dataI];


			$('#' + data).prop("checked", false);

		}

		document.getElementById('Financial_Job').checked = true;


	}
}

function unckeckToNoneCheckBox(id)
{
	if($('#'+id).is(":checked") == true)
	{
		document.getElementById('Financial_None').checked = false;
		$("#appscr70radios4IncomeYes").prop("checked", true);

	}
	else
	{
		var idOfChecks = new Array('Financial_Job', 'Financial_Self_Employment', 'Financial_SocialSecuritybenefits',
									'Financial_Unemployment', 'Financial_Retirement_pension', 'Financial_Capitalgains',
									'Financial_InvestmentIncome', 'Financial_RentalOrRoyaltyIncome',
									'Financial_FarmingOrFishingIncome', 'Financial_AlimonyReceived',
									'Financial_OtherIncome');
		var flag = false;

		for(var i=0; i<idOfChecks.length;i++)
		{
			if($('#'+idOfChecks[i]).is(":checked") == true)
			{
				flag = false;
				break;
			}
			else
			{
				flag = true;
			}
		}
		document.getElementById('Financial_None').checked = flag;

		if(flag == true)
		{
			$("#appscr70radios4IncomeNo").prop("checked", true);

			$('#checkBoxes_div').hide();
			$('#checkBoxes_divlbl').hide();

			hideDiductionsFields();
		}
	}
}


function showDiductionsFields()
{
	$('#incomeDeductions_headlabel').show();
	$('#incomeDeductions_textlabel').show();
	$('#incomeDeductions_div').show();
	$('#incomeDeductions_downmsg').show();
	$('#deductionsHorzLine').show();
}
function hideDiductionsFields()
{
	$('#incomeDeductions_headlabel').hide();
	$('#incomeDeductions_textlabel').hide();
	$('#incomeDeductions_div').hide();
	$('#incomeDeductions_downmsg').hide();
	$('#deductionsHorzLine').hide();
}


// Spouse View changes
function changeSpouseDivView60A()
{
	householdPayJointly();
	if($('input:radio[name=householdContactSpouse]:checked').val() == 'someOne')
	{
		$('#appscr60_spouseDiv').show();
	}
	else
	{
		$('#appscr60_spouseDiv').hide();
	}
}

//Tax Filer View changes
function changeTaxFilerDivView60A()
{
	if($('input:radio[name=householdContactFiler]:checked').val() == 'someOne')
	{
		$('#appscr60_taxFilerDiv1').show();
		$('#appscr60_taxFilerDiv2').show();

		}
	else
	{
		$('#appscr60_taxFilerDiv1').hide();
		$('#appscr60_taxFilerDiv2').hide();

	}
}

//Claiming Tax Filer View changes
function changeClaimingTaxFilerDivView60A()
{
	if($('input:radio[name=claimingTaxFilerSpouse]:checked').val() == 'someOne')
	{
		$('#appscr60_claimingTaxFilerDiv1').show();
		$('#appscr60_claimingTaxFilerDiv2').show();
	}
	else
	{
		$('#appscr60_claimingTaxFilerDiv1').hide();
		$('#appscr60_claimingTaxFilerDiv2').hide();
	}
}

//Spouse View changes
function changeDependentDivView60A()
{

	if($('input:radio[name=householdHasDependant]:checked').val() == "yes")
	{

		$('#householdDependentMainDiv').show();
		$("#willBeClaimed, #appscr60ClaimedFilerDiv").hide();
	}
	else if($('input:radio[name=householdHasDependant]:checked').val() == "no")
	{
		$("#willBeClaimed").show();
		if($("#doesApplicantHasTaxFillerYes").is(":checked")){
			$("#appscr60ClaimedFilerDiv").show();
		}
		$('#householdDependentMainDiv, #dependentDetailDiv_inputFields').hide();

		for(var i=1; i<noOfHouseHold ; i++)
		{
			if(i == HouseHoldRepeat)
				continue;
			if(document.getElementById('householdContactDependant'+i)!=null){
				document.getElementById('householdContactDependant'+i).checked = false;
			}

		}

		if(document.getElementById('householdContactDependantSomeOneChecked')!=null){
			document.getElementById('householdContactDependantSomeOneChecked').checked = false;
		}

	}
	if($('#householdContactDependantSomeOneChecked').is(':checked') == true)
	{
		$('#dependentDetailDiv_inputFields').show();
	}
	else
	{
		$('#dependentDetailDiv_inputFields').hide();
	}
}


//	Remove validation from SNAP page
function removeClassNumber()
{
		$('#shelterAndUtilityExpenseCost').removeClass('validateColor');
}

function removeClassSelect()
{
		$('#shelterAndUtilityExpenseFrequency').removeClass('validateColor');
}


function deductionCheck(id){
	var checkboxId = id;

	if($('#'+checkboxId).is(":checked") == true)
	{
		$('#'+checkboxId).parents('.otherIncomeDiv').find('.control-group').show();
	}
	else
	{
		$('#'+checkboxId).parents('.otherIncomeDiv').find('.control-group').hide();
	}
}


/*
****************************************************************************************************************
* summaryController.js                                                                                        *
****************************************************************************************************************
*/

var houseHoldSummaryHtml = "";
var incomeSummaryHtml = "";

function showHouseHoldContactSummary()
{
	$('#houseHoldContactSummaryDiv').html('');
	createHouseHoldContactSummaryHtml();
	$('#houseHoldContactSummaryDiv').append(houseHoldSummaryHtml);

	if(ssapApplicationType == "NON_FINANCIAL"){
		$(".non_financial_hide").css("display","none");
	}else{
		$(".non_financial_hide").css("display","");
	}

	incomeNumber = 0;
	deductionNumber = 0;
}

function parseDateofBirth(dateofBirth) {
	if(dateofBirth == null || dateofBirth == "") {
		return "";
	}

	return dob = dateofBirth.substring(0, 12);

}

function getRelationshipWithprimary(personId) {
	var relationArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	for(var i=0; i<relationArr.length; i++) {
		var object = relationArr[i];
		//get relationship if relatedPersonId is PC
		if(object.individualPersonId == personId  && object.relatedPersonId==1) {
			return getRelationshipText(object.individualPersonId);
		}
	}
}
/*
function getrelationshiptext(relationshipId) {
	if(relationshipId == "18") {
		return "Self";
	}
	if(relationshipId == "01") {
		return "Spouse";
	}
	else {
		return "other";
	}
}*/

function getSSN(householdMemberObj) {
	var ssn = householdMemberObj.socialSecurityCard.socialSecurityNumber;
	if(ssn == null || ssn == "--") {
		return "";
	}
	else {
		return "***-**-" + ssn.substring(7);
	}

}

function getNameOnSsnCard(householdMemberObj) {
	var ssn =  getSSN(householdMemberObj);
	if(ssn == "") {
		return "";
	}

	if(householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator) {
		return getFormattedname(householdMemberObj.name.firstName) + " "
		+ getFormattedname(householdMemberObj.name.middleName) + " "
		+ getFormattedname(householdMemberObj.name.lastName);
	}
	else {
		return getFormattedname(householdMemberObj.socialSecurityCard.firstNameOnSSNCard) + " "
		+ getFormattedname(householdMemberObj.socialSecurityCard.middleNameonSSNCard) + " "
		+ getFormattedname(householdMemberObj.socialSecurityCard.lastNameOnSSNCard);
	}
}

function getFormattedname(name) {
	if(name == null) {
		return "";
	}
	else {
		return name;
	}

}

function getTaxDependendantsInfo() {
	var dependentName = "";
	var dependentInfo = "";
	for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
		var dependentId = webJson.singleStreamlinedApplication.taxFilerDependants[j];

		var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependentId-1];


		dependentName = getFormattedname(dependentHouseholdMemberObj.name.firstName)+" "+getFormattedname(dependentHouseholdMemberObj.name.middleName)+" "+getFormattedname(dependentHouseholdMemberObj.name.lastName) ;
		dependentInfo+=	" | " + dependentName;
	}
	return dependentInfo;
}


function getFormattedHomeAddress(householdMemberObj) {
	var homeAddress = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		homeAddress += ", " + householdMemberObj.householdContact.homeAddress.postalCode;

	return homeAddress;

}

function getFormattedMailingAddress(householdMemberObj) {
	var mailingAddress = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.homeAddress.postalCode;
    return mailingAddress;
}
function getMembersApplyingCoverage() {

	noOfHouseHold = getNoOfHouseHolds();
	var coverageInfo = "<p>-------------------------------------------------------------------------------------------------------------------------</p>";
	coverageInfo += "<p><h2><u>Applying for coverage</u></h2>.</p>";

	for ( var i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		coverageInfo+="<p><h3>" + nameOfCurrentMember + "</h3></p>";
		coverageInfo+="<p>Date of birth: " + parseDateofBirth(householdMemberObj.dateOfBirth); + "</p>";
		if(i == 1){
			coverageInfo+="<p>Reltionship to primary: Self</p>";
		}else{
			coverageInfo+="<p>Reltionship to primary: " + getRelationshipWithPrimary(i); + "</p>";
		}
		if($('#applicationSource').val() == 'RF'){
			coverageInfo+="<p>Applying for coverage: " + (householdMemberObj.applyingForCoverageIndicator? 'Yes' : 'No') + "</p>";
		}else{
			coverageInfo+="<p>Applying for coverage: " + ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + "</p>";
		}
		coverageInfo += "<p>  </p>";
	}
	//coverageInfo = "";
	//coverageInfo = "<div><table border=\"1\"><tbody><tr><td>Name</td><td>Relationship</td><td>Date of Birth</td><td>Applying for Health Coverage</td><td>&nbsp;</td></tr><tr><td>Mama  John</td><td>&nbsp;&nbsp;</td><td>Jan 01, 1987 12:00:00 PM</td><td>true</td><td>Edit</td></tr><tr><td>Papa  John</td><td>&nbsp;&nbsp;</td><td>Jan 10, 1988 12:00:00 PM</td><td>true</td><td>Edit</td></tr></tbody></table></div>";
	return coverageInfo;
}

function getSpouseInformation(householdMemberObj) {
	var spouseInformation = "";
	householdMemberSpouseObj = null;

	if(householdMemberObj.marriedIndicator){
		var spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		if(!isNaN(spouseId)){
			householdMemberSpouseObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1];
		}
		if(householdMemberSpouseObj != null) {
			spouseInformation = getFormattedname(householdMemberSpouseObj.name.firstName)
			+ " "+getFormattedname(householdMemberSpouseObj.name.middleName)
			+ " "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
		}

	}
	return spouseInformation;
}
function getHouseholdMemberDetails() {
	noOfHouseHold = getNoOfHouseHolds();
	var memberInfo = "";
	var spouseId = "";

	for ( var i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)
			+" "+getFormattedname(householdMemberObj.name.middleName)
			+" "+getFormattedname(householdMemberObj.name.lastName);
		memberInfo+="<p><h3>" + nameOfCurrentMember + "'s information</h3></p>";
		memberInfo+="<p><b>Gender:</b> " + householdMemberObj.gender + "</p>";
		var ssn = getSSN(householdMemberObj);
		memberInfo+="<p><b>SSN:</b> " + ssn + "</p>";

		if(ssn == "") {
			memberInfo+="<p><b>Name on SSN card:</b> </p>";
		}
		else {
			var nameOnSsnCard = getNameOnSsnCard(householdMemberObj);
			memberInfo+="<p><b>Name on SSN card:</b> "  + nameOnSsnCard + "</p>";

		}
		memberInfo+="<p><b>U.S. citizen or U.S. national:</b> " + householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator + "</p>";

		var homeAddress = getFormattedHomeAddress(householdMemberObj);
		memberInfo += '<p><b>Home address</b>: ' + homeAddress + '</p>';

		var mailingAddress = getFormattedMailingAddress(householdMemberObj);
		memberInfo += '<p><b>Mailing address</b>: ' + mailingAddress + '</p>';

		var taxFilingInfo="No";
		if(householdMemberObj.planToFileFTRIndicator){

			taxFilingInfo = "Yes";

			if(householdMemberObj.planToFileFTRJontlyIndicator){
				taxFilingInfo +=", Jointly with "+ getSpouseInformation(householdMemberObj) ;
			}
		}

		memberInfo += '<p><b>Will file 2014 tax return</b>: ' + taxFilingInfo + '</p>';

		if(householdMemberObj.marriedIndicator) {
			memberInfo += '<p><b>Spouse information</b>: ' + getSpouseInformation(householdMemberObj) + '</p>';
			spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		}

		if(householdMemberObj.planToFileFTRIndicator){
			memberInfo += '<p><b>Tax dependent Claim for 2014</b>: ' + webJson.singleStreamlinedApplication.taxFilerDependants.length + '</p>';
			memberInfo += "<p><b>Tax dependents: "  + getTaxDependendantsInfo() + "</b></p>";
		}

		else {
			memberInfo += '<p><b>Tax dependent Claim for 2014</b>: None</p>';
		}
	}
	return memberInfo;
}

function generatePdf() {

	var caseNumber = webJson.singleStreamlinedApplication.applicationGuid;
	 window.open(theUrlForReviewSummaryPDF+"?caseNumber="+caseNumber);
	 return;
}

	/*

	var margins = {
		      top: 40,
		      bottom: 40,
		      left: 20,
		     // width: 522
		    };
	var specialElementHandlers = {
			// element with id of "bypass" - jQuery style selector
			'#bypassme': function(element, renderer){
				// true = "handled elsewhere, bypass text extraction"
				return true
			}
		};
	var doc = new jsPDF();

	var source = "";
	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	source += "<h2>Household contact information</h2>";

	source += '<p><b>Name</b>: ' + getFormattedname(householdMemberObj.name.firstName) +' '+getFormattedname(householdMemberObj.name.middleName)+' '+getFormattedname(householdMemberObj.name.lastName) + '</p>';
	source += '<p><b>Date of birth</b>: ' + parseDateofBirth(householdMemberObj.dateOfBirth) + '</p>';
	source += '<p><b>Email</b>: ' + householdMemberObj.householdContact.contactPreferences.emailAddress + '</p>';

	var homeAdress = getFormattedHomeAddress(householdMemberObj);
	source += '<p><b>Home address</b>: ' + homeAdress + '</p>';

	var mailingAddress = getFormattedMailingAddress(householdMemberObj);
	source += '<p><b>Mailing address</b>: ' + mailingAddress + '</p>';


	var phoneNumber = 'Not provided';
	var otherPhoneNumber = 'Not provided';

	if(householdMemberObj.householdContact.phone.phoneNumber != ''){
		phoneNumber = householdMemberObj.householdContact.phone.phoneNumber;
	}
	if(householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		householdMemberObj.householdContact.otherPhone.phoneNumber;
	}

	source += '<p><b>Phone number</b>: ' + phoneNumber + '</p>';
	source += '<p><b>Second phone number</b>: ' + otherPhoneNumber + '</p>';

	source += '<p><b>Prefferred spoken language</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage + '</p>';
	source += '<p><b>Prefferred written language</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage + '</p>';

	source += '<p><b>Preferred method to read notices</b>: ' + householdMemberObj.householdContact.contactPreferences.preferredContactMethod + '</p>';



	source += getMembersApplyingCoverage();
	source += "<p>-------------------------------------------------------------------------------------------------------------------------</p>";
	source += "<h2>Household members</h2";

	source += getHouseholdMemberDetails();

	doc.text(20, 20, $('#exchamgeName').val() + " Application ID: " + webJson.singleStreamlinedApplication.applicationGuid);
	doc.line(20, 25, 200, 25); // horizontal line
	doc.setFontSize(12);


	doc.fromHTML(source
			,margins.left // x coord
	    	, margins.top // y coord

	    	, {
	    		'width': margins.width // max width of content on PDF
	    		,'elementHandlers': specialElementHandlers
	    	},

	    	function (dispose) {
	      	  // dispose: object with X, Y of the last line add to the PDF
	      	  //          this allow the insertion of new lines after html
	    		if (navigator.userAgent.match(/msie|trident/i) ) {
	    	    	doc.output('save', 'downloadApplication.pdf');
	    	    }
	    	    else {
	    	    	doc.output('dataurlnewwindow', 'downloadApplication.pdf');
	    	    }
	          },

	    	margins
	);

*/

function createHouseHoldContactSummaryHtml(){
	var noOfHouseHold = getNoOfHouseHolds();
	var primaryContactHomeAddress = "";
	var appendRowData='';

	noOfHouseHoldmember = getNoOfHouseHolds();

	try{
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
		//$('#appscr57HouseHoldPersonalInformation tr').each(function( i ) {

			var nameOfCurrentMember;

			//if(i == 0){
				householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember=getNameByPersonId(i);
			//}else{
				//nameOfCurrentMember= getNameByTRIndex(i);
			//}

			//nameOfCurrentMember=getNameRemovedBySpace(nameOfCurrentMember);
			//var ssnNo= $(this).find("#appscr57HouseHoldSSNTD"+(i+1)).html();
			var ssnNo = "";
			var nameOnSsnCard = "";
			var homeAddress = "";
			var mailingAddress = "";
			var taxFilingInfo = "";
			var householdMemberSpouseObj=null;
			var spouseInformation = "";

			if( householdMemberObj.socialSecurityCard.socialSecurityNumber !="" && householdMemberObj.socialSecurityCard.socialSecurityNumber !=null && householdMemberObj.socialSecurityCard.socialSecurityNumber != "--"){
				ssnNo = "***-**-"+householdMemberObj.socialSecurityCard.socialSecurityNumber.substring(5);
	 		}
			if(householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator){
				nameOnSsnCard = nameOfCurrentMember;
			}else{
				nameOnSsnCard = getFormattedname(householdMemberObj.socialSecurityCard.firstNameOnSSNCard)+" "+getFormattedname(householdMemberObj.socialSecurityCard.middleNameOnSSNCard)+" "+getFormattedname(householdMemberObj.socialSecurityCard.lastNameOnSSNCard);
			}

			if(householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator!=null && !householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator){
				var ReasonableExplanationForNoSSNCode = replaceNullWithEmpty(householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN);
				if(ReasonableExplanationForNoSSNCode == "" ){
					ReasonableExplanationForNoSSNText = "";
				}else {
					ReasonableExplanationForNoSSNText = $("#reasonableExplanationForNoSSN option[value="+ReasonableExplanationForNoSSNCode+"]").text();
				}
				ssnNo = "--NA-- (Reason : "+ReasonableExplanationForNoSSNText+")";
				nameOnSsnCard = "--NA--";
			}
			if(householdMemberObj.householdContact.homeAddressIndicator == true || householdMemberObj.householdContact.homeAddressIndicator == 'true'){
				primaryContactHomeAddress = householdMemberObj.householdContact.homeAddress.streetAddress1+addAddressSeperator(householdMemberObj.householdContact.homeAddress.streetAddress2)+addAddressSeperator(householdMemberObj.householdContact.homeAddress.city)+addAddressSeperator(householdMemberObj.householdContact.homeAddress.state)+addAddressSeperator(householdMemberObj.householdContact.homeAddress.postalCode)+addAddressSeperator(householdMemberObj.householdContact.homeAddress.county);
			}else if(i>1 && householdMemberObj.livesAtOtherAddressIndicator == true){
					primaryContactHomeAddress = householdMemberObj.otherAddress.address.streetAddress1+addAddressSeperator(householdMemberObj.otherAddress.address.streetAddress2)+addAddressSeperator(householdMemberObj.otherAddress.address.city)+addAddressSeperator(householdMemberObj.otherAddress.address.state)+addAddressSeperator(householdMemberObj.otherAddress.address.postalCode)+addAddressSeperator(householdMemberObj.otherAddress.address.county);
			}

			if(householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator == true || householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator == 'true'){
				mailingAddress = "Same as home address";
			}else{
				mailingAddress = householdMemberObj.householdContact.mailingAddress.streetAddress1+addAddressSeperator(householdMemberObj.householdContact.mailingAddress.streetAddress2)+addAddressSeperator(householdMemberObj.householdContact.mailingAddress.city)+addAddressSeperator(householdMemberObj.householdContact.mailingAddress.state)+addAddressSeperator(householdMemberObj.householdContact.mailingAddress.postalCode)+addAddressSeperator(householdMemberObj.householdContact.mailingAddress.county);
			}

			if(householdMemberObj.householdContactIndicator){

			}
			if(householdMemberObj.marriedIndicator ){
				var spouseId = householdMemberObj.taxFiler.spouseHouseholdMemberId;
				if(!isNaN(spouseId) && spouseId !== null){
					householdMemberSpouseObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1];
					spouseInformation = getFormattedname(householdMemberSpouseObj.name.firstName)+" "+getFormattedname(householdMemberSpouseObj.name.middleName)+" "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
				}

			}

			if(householdMemberObj.planToFileFTRIndicator){

				taxFilingInfo = "Yes";

				if(householdMemberObj.planToFileFTRJontlyIndicator && householdMemberSpouseObj!=null){
					taxFilingInfo +=", Jointly with "+ getFormattedname(householdMemberSpouseObj.name.firstName)+" "+getFormattedname(householdMemberSpouseObj.name.middleName)+" "+getFormattedname(householdMemberSpouseObj.name.lastName) ;
				}
			}

			var citizenshipStatusIndicator = "No";
			if(householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator!=null && householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator){
				citizenshipStatusIndicator = "Yes";
			}

			//No age check for PC, spouse and ward, so under26Indicator should be always true for them
			var applyingForCoverage = '';

			if($('#applicationSource').val() == 'RF' ){
				applyingForCoverage = ((householdMemberObj.applyingForCoverageIndicator == true)? 'Yes' : 'No');
			}else{
				applyingForCoverage = ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No');
			}

			appendRowData+="<div class='hixApplicantInfo'><h5 class='camelCaseName'>"+nameOfCurrentMember+'&nbsp;';

			if(i==1){
				appendRowData+=jQuery.i18n.prop('ssap.primaryContact')+'&nbsp;';
			}

			var isInfant = $('#appscr57IsInfantTD'+(i)).text();

			if(isInfant == "yes")
			{
				appendRowData+= '</h5><p>'+jQuery.i18n.prop("ssap.socialSecurityNumber")+'&nbsp;<strong>'+ssnNo+'</strong></p>'
				+'<p>'+jQuery.i18n.prop("ssap.applyingForCoverage")+'&nbsp;<strong>'+applyingForCoverage+"</strong></p>"
				+'';
			}
			else
			{
				if(applicationStatus=='OP'){
					appendRowData+='<span class="pull-right"><input type="button" value="Edit" class="btn btn-primary" onclick="appscr67Edit('+(i)+')"/></span>';
				}
				appendRowData+='</h5>'

				+'<table class="summary-table">'
				+'<tr><td>'+jQuery.i18n.prop("ssap.applyingForCoverage")+'</td> <td>'+applyingForCoverage+'</td></tr>'
				+'<tr><td> Gender</td> <td class='+'"camelCaseName"'+'>'+householdMemberObj.gender+'</td></tr>'
				+'<tr><td>'+jQuery.i18n.prop("ssap.socialSecurityNumber")+'</td> <td>'+ssnNo+'</td></tr>'
				/*+'<tr><td> Social Security No:</td> <td>'+ssnNo+'</td></tr>'*/
				+'<tr><td> Name on SSN Card</td> <td>'+nameOnSsnCard+'</td></tr>'
				+'<tr><td> US Citizen or US National</td> <td>'+citizenshipStatusIndicator+'</td></tr>'
				+'<tr><td> Home Address</td> <td>'+primaryContactHomeAddress+'</td></tr>';
				if(i==1){
					appendRowData+='<tr><td> Mailing Address</td> <td>'+mailingAddress+'</td></tr>';
				}
				appendRowData+='<tr class="non_financial_hide"><td> Will File 2014 Income Tax Return</td> <td>'+taxFilingInfo+'</td></tr>'
				+'<tr class="non_financial_hide"><td> Spouse Information</td> <td>'+spouseInformation+'</td></tr>'
				+'<tr class="non_financial_hide"><td> Tax Dependent Claim for 2014</td> <td>'+webJson.singleStreamlinedApplication.taxFilerDependants.length+'</td></tr>'

				+'</table>';
				appendRowData+= '<table class="summary-table margin20-b non_financial_hide"><tr><td>Tax Dependent:</td> <td>Claimed on tax return for</td></tr>';
				for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
					var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j];
					var dependentName = getFormattedname(dependentHouseholdMemberObj.name.firstName)+" "+getFormattedname(dependentHouseholdMemberObj.name.middleName)+" "+getFormattedname(dependentHouseholdMemberObj.name.lastName) ;
					appendRowData+=	'<tr><td>'+dependentName+'</td><td>2014</td></tr>';
				}
				appendRowData+= '</table>';
				/*appendRowData+= '<table><tr><td>Lives with and has responsibility for primary care of</td></tr></table>';

				appendRowData+= '<table><tr><td>Child Name</td><td>Date of Birth</td><td>Relationship</td></tr>';
				for(var j=0; j<webJson.singleStreamlinedApplication.taxFilerDependants.length; j++){
					var dependentHouseholdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j];
					var dependentName = dependentHouseholdMemberObj.name.firstName+" "+dependentHouseholdMemberObj.name.middleName+" "+dependentHouseholdMemberObj.name.lastName ;
					appendRowData+=	'<tr><td>'+dependentName+'</td><td>2014</td></tr>';
				}*/
			}


			appendRowData += '</div></div>';
	// if(i==noOfHouseHold-1)
//		appendRowData+=	'<hr class="hide" />';
	// else
//		appendRowData+=	'<hr />';
			if(i==noOfHouseHold)
				appendRowData+='<div class="control-group hide"><input type="button" value="Edit Household Composition" class="edit_household_button" /></div>';


			}

		appendRowData+='<h4>More About This Household</h4><div class="hixApplicantInfo">';

		appendRowData+='<h5 class="non_financial_hide">Has a disability</h5>';
		appendRowData+='<table class="summary-table margin40-b non_financial_hide">';


		for ( var i = 1; i <= noOfHouseHoldmember; i++) {
			var nameOfCurrentMember;

			//if(i == 0){
				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember=getNameByPersonId(i);
				//nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
				var disabilityIndicator = householdMemberObj.disabilityIndicator?'Yes':'No';
				if(disabilityIndicator == 'Yes'){
					disabilityIndicator += ' [<b>Tribe</b> : '+householdMemberObj.americanIndianAlaskaNative.tribeFullName+' / <b>State</b> : '+householdMemberObj.americanIndianAlaskaNative.stateFullName+']';
				}
				appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';
		}

		appendRowData+='</table>';

		appendRowData+='<h5 class="non_financial_hide">Need Help with Activities of daily living</h5>';
		appendRowData+='<table class="summary-table margin40-b non_financial_hide">';

		for ( var i = 1; i <= noOfHouseHoldmember; i++) {

	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		var nameOfCurrentMember;
		nameOfCurrentMember = getNameByPersonId(i);
		//nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		var disabilityIndicator = householdMemberObj.healthCoverage.medicaidInsurance.requestHelpPayingMedicalBillsLast3MonthsIndicator?'Yes':'No';
		appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';


}
		appendRowData+='</table>';


		appendRowData+='<h5>Members of a federally recognized tribe</h5>';
		appendRowData+='<table class="summary-table margin40-b camelCaseName">';

		for ( var i = 1; i <= noOfHouseHoldmember; i++) {

			var nameOfCurrentMember;

			//if(i == 0){
				var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
				nameOfCurrentMember = getNameByPersonId(i);
				//nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
				var disabilityIndicator = householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator?'Yes':'No';
				appendRowData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';
}
		appendRowData+='</table>';
		var appendPregnantData ='<h5 class="non_financial_hide">Is Pregnant</h5>';
		appendPregnantData+='<table class="summary-table margin20-b non_financial_hide">';
		var isFemaleApplicant = false;
		for ( var i = 1; i <= noOfHouseHoldmember; i++) {

	var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	if(householdMemberObj.gender=='female'){
		isFemaleApplicant = true;
		var nameOfCurrentMember;
		nameOfCurrentMember = getNameByPersonId(i);
		//nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
		var disabilityIndicator = householdMemberObj.specialCircumstances.pregnantIndicator?'Yes':'No';
		appendPregnantData+='<tr><td>'+nameOfCurrentMember+'</td><td>'+disabilityIndicator+'</td></tr>';

	}
}

		appendPregnantData+='</table>';
		if(isFemaleApplicant == true){
			appendRowData+=appendPregnantData;
		}


		appendRowData+='</div>';


	}catch(e){
		//e.message();
	}

	houseHoldSummaryHtml = appendRowData;
}

function showFinancialDetailsSummary(){

	$('#FinancialDetailsSummaryDiv form').html('');
	showFinancialDetailsSummaryJSON();
	$('#FinancialDetailsSummaryDiv form').append(incomeSummaryHtml);

}

function addAddressSeperator(value){
	if(value != null && value !=""){
		return ", "+value;
	}
	return "";
}

function showFinancialDetailsSummaryJSON(){

	var idNumber = 0;
	var noOfHouseHold = parseInt(webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length);
	var appendRowData='';

	for(var cnt=0; cnt<noOfHouseHold; cnt++){

		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt];
		var nameOfCurrentMember= getNameOfCurrentMember(cnt);
		nameOfCurrentMember=getNameRemovedBySpace(nameOfCurrentMember);
		appendRowData+='<div class="hixApplicantInfo camelCaseName"><h5>'+nameOfCurrentMember;
		if(cnt==0){
			appendRowData+='&nbsp;<small>'+jQuery.i18n.prop("ssap.primaryContact")+'</small>';
		}
		appendRowData+='<span class="pull-right"><input type="button" value="Edit" class="btn btn-primary" onclick="appscr74EditController('+(cnt+1)+')"/></span></h5>';

		for(key in householdMemberObj.detailedIncome) {
			var amount = 0;
			var incomeFrequency = "";
			  //if(householdMemberObj.detailedIncome(key)) {

				  switch(key)
				  {
				  case 'jobIncome':
					 data = householdMemberObj.detailedIncome[key];
					 amount = data[0].incomeAmountBeforeTaxes;
					 frequency = data[0].incomeFrequency;
					 incomeType = "Job";
				    break;
				  case "selfEmploymentIncome":
					  data = householdMemberObj.detailedIncome[key];
						 amount = data.monthlyNetIncome;
						 frequency = "Monthly";
						 incomeType = "Self Employment";
				    break;
				  case "socialSecurityBenefit":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.benefitAmount;
						frequency = data.incomeFrequency;
						incomeType = "Social Security Benefit";
				    break;
				  case "unemploymentBenefit":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.benefitAmount;
						frequency = data.incomeFrequency;
						incomeType = "Unemployment Benefit";
				    break;
				  case "retirementOrPension":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.taxableAmount;
						frequency = data.incomeFrequency;
						incomeType = "Retirement Or Pension";
				    break;
				  case "capitalGains":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.annualNetCapitalGains;
						frequency = "Yearly";
						incomeType = "Capital Gains";
				    break;
				  case "investmentIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.incomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Investment Income";
				    break;
				  case "rentalRoyaltyIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.netIncomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Rental Royalty Income";
				    break;
				  case "farmFishingIncome":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.netIncomeAmount;
						frequency = data.incomeFrequency;
						incomeType = "Farm Fishing Income";
				    break;
				  case "alimonyReceived":
					  	data = householdMemberObj.detailedIncome[key];
						amount = data.amountReceived;
						frequency = data.incomeFrequency;
						incomeType = "Alimony Received";
				    break;
				  case "otherIncome":
				    	data = householdMemberObj.detailedIncome[key];
				    	for(var i=0;i<data.length;i++){
					    	if(data[i].incomeAmount != "" || data[i].incomeAmount != null){
								amount = data[i].incomeAmount;
								frequency = data[i].incomeFrequency;
								incomeType = data[i].otherIncomeTypeDescription;
								if(Number(amount) > 0 ){
									appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.incomeType1")+'</td><td>'
									+incomeType+'</td></tr><tr><td>'+jQuery.i18n.prop("ssap.income")+'</td>'
									+'<td>$'+Moneyformat(amount);

									appendRowData+="/"+frequency+"</td></tr></table></div>" ;
								}
					    	}
				    	}
				    break;

				  default:
				    //code to be executed if n is different from case 1 and 2
				  }

				  if(key != 'otherIncome'){
				  if(Number(amount) > 0 ){
						appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.incomeType1")+'</td><td>'
						+incomeType+'</td></tr><tr><td>'+jQuery.i18n.prop("ssap.income")+'</td>'
						+'<td>$'+Moneyformat(amount);

						appendRowData+="/"+frequency+"</td></tr></table>" ;
					}
				  }
		}

				if(householdMemberObj.detailedIncome !== undefined 
					&& householdMemberObj.detailedIncome.deductions !== undefined){
						for(key in householdMemberObj.detailedIncome.deductions) {

							householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount
							amount = 0;
							switch(key)
							{
							case 'alimonyDeductionAmount':
							   amount = householdMemberObj.detailedIncome.deductions[key];
							   frequency = householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency;
							   incomeType = "Alimony";
							  break;
							case 'studentLoanDeductionAmount':
								   amount = householdMemberObj.detailedIncome.deductions[key];
								   frequency = householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency;
								   incomeType = "Student Loan Deduction";
								  break;
	  
							case 'otherDeductionAmount':
								   amount = householdMemberObj.detailedIncome.deductions[key];
								   frequency = householdMemberObj.detailedIncome.deductions.otherDeductionFrequency;
								   incomeType = householdMemberObj.detailedIncome.deductions.deductionType[2];
								  break;
							}
	  
	  
							if(Number(amount) > 0 ){
	  
								  appendRowData+='<table class="summary-table margin10-b"><tr><td>' +jQuery.i18n.prop("ssap.deductionsType")+'</td><td>'
								  +incomeType+'</td></tr><tr><td>Deduction</td>'
								  +'<td>$'+Moneyformat(amount);
	  
								  appendRowData+="/"+frequency+"</td></tr></table></div>" ;
								}
	  
						}

				}

				  

			}

		incomeSummaryHtml = appendRowData;
		totalIncomeShowMethod();
	}

function caluculateFrequencywiseIncome(salary, frequency) {
	var w = new String(frequency);
	var f = trimSpaces(w.toLowerCase());
	var salaryInfloat = parseFloat(new String(salary));
	var salaryReturned = 0.0;
	if (f == "hourly") {
		salaryReturned = salaryInfloat * 24 * 30;
	} else if (f == "daily") {
		salaryReturned = salaryInfloat * 30;
	} else if (f == "quarterly") {
		salaryReturned = salaryInfloat / 3;
	} else if (f = "monthly") {
		salaryReturned = salaryInfloat;
	} else if (f == "weekly") {
		salaryReturned = salaryInfloat * 4;
	} else if (f == "everytwoweeks") {
		salaryReturned = salaryInfloat * 2;
	} else if (f == "twiceamonth") {
		salaryReturned = salaryInfloat * 2;
	} else if (f == "yearly" || f == "onetimeonly") {
		salaryReturned = salaryInfloat / 12;
	}

	return salaryReturned;

}




function totalIncomeShowMethod()
{

	for(var i=0; i<noOfHouseHold; i++)
	{
			totalIncomeToArray(i+1);
			totalDeductionInArray(i+1);


		var totalIncomess = totalIncomes[i] - totalIncomeDeductions[i];

		$('#totalIncome72_'+i).text('  $  '+totalIncomess.toFixed(2)+"/"+jQuery.i18n.prop('ssap.monthly'));
		$('#deductionLabel_show_'+i).text('  $  '+totalIncomeDeductions[i]+"/"+jQuery.i18n.prop('ssap.monthly'));
	}



}

function getNameOfCurrentMember(i){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
	return getFormattedname(householdMemberObj.name.firstName) + " " + getFormattedname(householdMemberObj.name.middleName) + " " + getFormattedname(householdMemberObj.name.lastName);
}


//Keys
function addReviewSummary()
{

	if(useJSON == true){
		addReviewSummaryJson();
		return;
	}

}


function addReviewSummaryJson()
{

	generateReviewSummaryReport();
	if(currentPage == 'appscr67' && !webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator) { // suneel 05092014
		return;
	}

	else if (applicationStatus == "CC" || applicationStatus == "CL" || applicationStatus == "SG" || applicationStatus == "SU" || applicationStatus == "ER" || applicationStatus == "EN") {
		if( $("#csroverride").val()=="Y"){
			next();
		} else {
			return; // this is for preview application after esign and submit
		}
	}
	else {
		next();
	}

}

function generateReviewSummaryReport(){
	var strOuter = "";
	noOfHouseHold = getNoOfHouseHolds();
	document.getElementById('appscr84addReviewSummary').innerHTML = '';

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	var houseHoldName = "";

	houseHoldName = getFormattedname(householdMemberObj.name.firstName) +" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
	//Show adress of primary contact only
	if(householdMemberObj.name.suffix != ""){
		houseHoldName += " "+getFormattedname(householdMemberObj.name.suffix);
	}

	var address = householdMemberObj.householdContact.homeAddress.streetAddress1;
	if(householdMemberObj.householdContact.homeAddress.streetAddress2 !=null && householdMemberObj.householdContact.homeAddress.streetAddress2!="")
			address += "</br> " + householdMemberObj.householdContact.homeAddress.streetAddress2;
	if(householdMemberObj.householdContact.homeAddress.city !=null && householdMemberObj.householdContact.homeAddress.city!="")
			address += ", " + householdMemberObj.householdContact.homeAddress.city;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.state!="")
		address += ", " + householdMemberObj.householdContact.homeAddress.state;
	if(householdMemberObj.householdContact.homeAddress.state !=null && householdMemberObj.householdContact.homeAddress.postalCode!="")
		address += ", " + householdMemberObj.householdContact.homeAddress.postalCode;
	if(householdMemberObj.householdContact.homeAddress.county !=null && householdMemberObj.householdContact.homeAddress.county!="")
		address += ", " + householdMemberObj.householdContact.homeAddress.county;

	var primaryApplicantAddress =  address;


	var mailingAddress = "";
	if(householdMemberObj.householdContact.mailingAddress.streetAddress1 !=null && householdMemberObj.householdContact.mailingAddress.streetAddress1!="")
		mailingAddress += householdMemberObj.householdContact.mailingAddress.streetAddress1;
	if(householdMemberObj.householdContact.mailingAddress.streetAddress2 !=null && householdMemberObj.householdContact.mailingAddress.streetAddress2!="")
		mailingAddress += "</br> " + householdMemberObj.householdContact.mailingAddress.streetAddress2;
	if(householdMemberObj.householdContact.mailingAddress.city !=null && householdMemberObj.householdContact.mailingAddress.city!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.city;
	if(householdMemberObj.householdContact.mailingAddress.state !=null && householdMemberObj.householdContact.mailingAddress.state!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.state;
	if(householdMemberObj.householdContact.mailingAddress.postalCode !=null && householdMemberObj.householdContact.mailingAddress.postalCode!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.postalCode;
	if(householdMemberObj.householdContact.mailingAddress.county !=null && householdMemberObj.householdContact.mailingAddress.county!="")
		mailingAddress += ", " + householdMemberObj.householdContact.mailingAddress.county;


	if(householdMemberObj.householdContact.otherPhone.phoneNumber == null) {
		householdMemberObj.householdContact.otherPhone.phoneNumber = '';
	}

	if(householdMemberObj.householdContact.phone.phoneNumber == null) {
		householdMemberObj.householdContact.phone.phoneNumber = '';
	}

	var stateCode = $(stateExchangeCode).val();

	if(stateCode == 'CA'){
		strOuter ='<div class="header ssap-header"><h4 class="pull-left noBorder applicationSummary">Final Application Review and Confirmation</h4>'
		+'<a href="javascript:void(0)" class="btn btn-small pull-right" onclick="window.print()">'+jQuery.i18n.prop("ssap.print")+'</a>'
		+'</div>'
		+'<div class="gutter10"><div class="hixApplicantInfo"><h5 camelCaseName>'+houseHoldName+'</h5><table class="summary-table">'
		+'<tbody>'
		+'<tr><td>Email</td><td>'+getFormattedname(householdMemberObj.householdContact.contactPreferences.emailAddress)+'</td></tr>';
	}else{
	strOuter ='<div class="header ssap-header"><h4 class="pull-left noBorder applicationSummary">Final Application Review and Confirmation</h4>'
	+'<a href="javascript:void(0)" class="btn btn-small pull-right" onclick="window.print()">'+jQuery.i18n.prop("ssap.print")+'</a>'
	+'<a href="javascript:generatePdf();" class="btn btn-small btn-primary pull-right">'+jQuery.i18n.prop("ssap.download")+'</a>'
	+'</div>'
	+'<div class="gutter10"><div class="hixApplicantInfo"><h5 camelCaseName>'+houseHoldName+'</h5><table class="summary-table">'
	+'<tbody>'
	+'<tr><td>Email</td><td>'+getFormattedname(householdMemberObj.householdContact.contactPreferences.emailAddress)+'</td></tr>';
	}
	

	var ext = (householdMemberObj.householdContact.otherPhone.phoneExtension=="" || householdMemberObj.householdContact.otherPhone.phoneExtension==null)? "" : " Ext.: "+householdMemberObj.householdContact.otherPhone.phoneExtension;
	if(householdMemberObj.householdContact.phone.phoneNumber != '' && householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber)+' / '+formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber)+ ext +'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber != '' && householdMemberObj.householdContact.otherPhone.phoneNumber == '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber)+'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber == '' && householdMemberObj.householdContact.otherPhone.phoneNumber != '' ){
		strOuter+='<tr><td>Phone</td><td>'+formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber)+ext+'</td></tr>';
	}
	if(householdMemberObj.householdContact.phone.phoneNumber == '' && householdMemberObj.householdContact.otherPhone.phoneNumber == '' ){
		//strOuter+='<tr><td class="span4 txt-right">Phone</td><td><strong>'+jQuery.i18n.prop("ssap.notAvailable")+'</td></tr>';
	}
	strOuter+='<tr><td>Date of Birth</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td></tr>';
	strOuter+='<tr><td>Home Address</td><td>'+address+'</td></tr>';
	strOuter+='<tr><td>Mailing Address</td><td>'+(mailingAddress == null? '':mailingAddress)+'</td></tr>';
	strOuter+='<tr><td>Preferred Phone Number</td><td>'+(householdMemberObj.householdContact.phone.phoneNumber == '' ? '': formatPhoneNumbr(householdMemberObj.householdContact.phone.phoneNumber) +' (Cell)' )+'</td></tr>';
	strOuter+='<tr><td>Second Phone Number</td><td>'+(householdMemberObj.householdContact.otherPhone.phoneNumber == '' ? '': formatPhoneNumbr(householdMemberObj.householdContact.otherPhone.phoneNumber) + ' (Home)')+ext+'</td></tr>';
	strOuter+='<tr><td>Preferred Spoken Language</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage == null? '':householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage)+'</td></tr>';
	strOuter+='<tr><td>Preferred Written Language</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage == null? '':householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage)+'</td></tr>';
	strOuter+='<tr><td>Preferred Method of Communication</td><td>'+(householdMemberObj.householdContact.contactPreferences.preferredContactMethod == null? '' : householdMemberObj.householdContact.contactPreferences.preferredContactMethod)+'</td></tr>';


	strOuter+='</tbody></table>';

	strOuter+='<h5 class="margin40-t margin20-b">Applying for health coverage</h5>';
	strOuter+='<table class="table table-condensed">';
	strOuter+='<tr><td>Name</td><td>Relationship</td><td>Date of Birth</td><td>Seeking Coverage</td></tr>';
	for ( var i = 1; i <= noOfHouseHold; i++) {

			var householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
			var nameOfCurrentMember = getNameByPersonId(i);
			//var nameOfCurrentMember=getFormattedname(householdMemberObj.name.firstName)+" "+ getFormattedname(householdMemberObj.name.middleName) +" "+getFormattedname(householdMemberObj.name.lastName);

			//relationship
			if(i>1){
				var relationText = getRelationshipText(householdMemberObj.personId);
				if($('#applicationSource').val() == 'RF' || webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator){
					strOuter+='<tr><td style="word-break: break-all">'+nameOfCurrentMember+'</td><td>' + relationText + '</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator == true)? 'Yes' : 'No') + '</tr>';
				}else{
					strOuter+='<tr><td style="word-break: break-all">'+nameOfCurrentMember+'</td><td>' + relationText + '</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + '</td></tr>';
				}
			}else{

				if($('#applicationSource').val() == 'RF' || webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator ){
					strOuter+='<tr><td style="word-break: break-all">'+nameOfCurrentMember+'</td><td>Self</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator)? 'Yes' : 'No') + '</td></tr>';
				}else{
					strOuter+='<tr><td style="word-break: break-all">'+nameOfCurrentMember+'</td><td>Self</td><td>'+parseDateofBirth(householdMemberObj.dateOfBirth)+'</td><td>'+ ((householdMemberObj.applyingForCoverageIndicator && householdMemberObj.under26Indicator)? 'Yes' : 'No') + '</td></tr>';
				}
			}
	}
	strOuter+='</table></div></div>';
	strOuter+='<div class="header"><h4>Household Members</h4></div><div class="gutter10">';

	//for review application after logout
	if(houseHoldSummaryHtml == ''){
		createHouseHoldContactSummaryHtml();
	}
	strOuter+=houseHoldSummaryHtml;
	strOuter+='</div><div class="header non_financial_hide"><h4>Income</h4></div><div class="gutter10 non_financial_hide">';
	//for review application after logout
	 if(incomeSummaryHtml == '' && webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator == true){
		showFinancialDetailsSummaryJSON();
	}
	strOuter+=incomeSummaryHtml;
	strOuter+='</div>';

	document.getElementById('appscr84addReviewSummary').innerHTML = strOuter;

	if(ssapApplicationType == "NON_FINANCIAL"){
		$(".non_financial_hide").css("display","none");
	}else{
		$(".non_financial_hide").css("display","");
	}

}


function getRelationshipText(relatedPersonId){
	var bloodRelationshipArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	var relationShipCode = '';
	var reversedRelation = '';
	//relationship
	for(var i=0;i<bloodRelationshipArr.length;i++){
		if(bloodRelationshipArr[i].relatedPersonId == 1  && bloodRelationshipArr[i].individualPersonId == relatedPersonId ){

			//one to many relationship
			if(bloodRelationshipArr[i].relation == '03' || bloodRelationshipArr[i].relation == '15'){	//multiple relationship

				//get reversed relation
				for(var j=0; j<bloodRelationshipArr.length; j++){
					if(bloodRelationshipArr[j].relatedPersonId == relatedPersonId && bloodRelationshipArr[j].individualPersonId == 1){
						reversedRelation = bloodRelationshipArr[j].relation;
					}
				}

				//get relation code based on its reversed relation
				if(reversedRelation == '09'){
					relationShipCode = '03a';
				}else if(reversedRelation == '10'){
					relationShipCode = '03f';
				}else if(reversedRelation == '19'){
					relationShipCode = '03';
				}else if(reversedRelation == '26'){
					relationShipCode = '15';
				}else if(reversedRelation == '31'){
					relationShipCode = '15c';
				}else if(reversedRelation == '15'){
					relationShipCode = '03w';
				}else if(reversedRelation == '03'){
					relationShipCode = '15p';
				}else{
					relationShipCode = bloodRelationshipArr[i].relation;
				}

			}else{
				relationShipCode = bloodRelationshipArr[i].relation;
			}

			return  $("#relationshipOptions_all option[value="+relationShipCode+"]").text();

		}
	}
}



function CapitaliseFirstLetter(elementId) {
    var txt = $("#" + elementId).val().toLowerCase();
    $("#" + elementId).val(txt.replace(/^(.)|\s(.)/g, function($1) {
    return $1.toUpperCase(); }));
    }

function replaceNullWithEmpty(elementId){
	if(elementId!=null){
		return elementId.replace("null", "");
	}else{
		return "";
	}
}
function formatPhoneNumbr(strPhoneNo){
	if(strPhoneNo =="" || strPhoneNo == null || strPhoneNo == undefined){
		return "";
	}else{
		return strPhoneNo.replace(/(\d{3})(\d{3})(\d{4})/, "($1)-$2-$3");
	}

}


/*
****************************************************************************************************************
* displayAndHideInfo.js                                                                                       *
****************************************************************************************************************
*/


function displayAuthInfo() {
	/*$('#parsley-authorizedRepresentativeIndicator').hide();*/
	if ($("#displayAuthRepresentativeInfo").is(":checked")) {
		$("#authPerInfoPage54").show();


		//var householdName = $('#firstName').val() + " "+ $('#middleName').val() + " "+ $('#lastName').val();
		 $('.appscr54_houseHoldName').text(householdName);
		 updatePrimaryContactName();
	} else if ($("#hideAuthRepresentativeInfo").is(":checked")) {
		$("#authPerInfoPage54").hide();

	}
}
function nameOfCompanyAndOrg54()
{
	if ($("#cmpnyNameAndOrgRadio54_1").is(":checked")) {
		$("#cmpnyNameAndOrgDiv").show();
	} else if ($("#cmpnyNameAndOrgRadio54_2").is(":checked")) {
		$("#cmpnyNameAndOrgDiv").hide();

	}
}





function askForSSNNoPage61() {

	if ($("#socialSecurityCardHolderIndicatorYes").is(":checked")) {
		$("#askAboutSSNOnPage61").show();
		$("#DontaskAboutSSNOnPage61").hide();
	} else if ($("#socialSecurityCardHolderIndicatorNo").is(":checked")) {
		$("#askAboutSSNOnPage61").hide();
		$("#DontaskAboutSSNOnPage61").show();

	}

}

function hasSameSSNNumberP61() {
	if ($("#fnlnsSameIndicatorYes").is(":checked")) {
		$("#sameSSNPage61Info").hide();
	} else if ($("#fnlnsSameIndicatorNo").is(":checked")) {
		$("#sameSSNPage61Info").show();

	}

}
function hasAnIdividualMandateExemptionP61() {
	if ($("#fnlnsExemptionIndicatorYes").is(":checked")) {
		$("#mandateExemptionP61").show();
	} else if ($("#fnlnsExemptionIndicatorNo").is(":checked")) {
		$("#mandateExemptionP61").hide();

	}

}

function displayOrHideSameNameInfoPage62() {
	 if ($("#62Part2_UScitizen").is(":checked")) {
		$("#sameNameOnDocument").show();

	}else{
		$("#sameNameOnDocument").hide();
	}
}



function method4AnotherChildCheckBox() {
	if($("#takeCareOf_anotherChild").is(":checked"))
		{
			$(".parentOrCaretakerBasicDetailsContainer").show();
		}
	else{
		$(".parentOrCaretakerBasicDetailsContainer").hide();
	}
}


function money4OtherIncometypeCheckBox() {
	if($("#anotherIncomeTypeSource").is(":checked"))
		{
			$(".incomeTypeDetailcontainer").show();
		}
	else{
		$(".incomeTypeDetailcontainer").hide();
	}
}




function isUSCitizen() {

	if($('input[name=UScitizen]:radio:checked').val()=='true')
		{
			$("#naturalizedCitizenDiv").hide();
			$("#documentTypeDiv").hide();
			$("#eligibleImmigrationStatusDiv").hide();
		}
	else if($('input[name=UScitizen]:radio:checked').val()=='false'){
			$("#naturalizedCitizenDiv").show();

			isNaturalizedCitizen();
			/*$("#documentTypeDiv").hide();
			$("#eligibleImmigrationStatusDiv").hide();
			if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='yes')
			{
				$("#documentTypeDiv").show();
				$("#eligibleImmigrationStatusDiv").hide();
			}
			else{
				$("#documentTypeDiv").hide();
				$("#eligibleImmigrationStatusDiv").show();
			}*/
		}
}

$(document).ready(function(){
	$("#eligibleImmigrationStatus input:radio").click(function(){
		$("#sameNameInfoPage62P2, #haveTheseDocuments").show();
	});
});




function parentCareTakerHideShow() {

	if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val()=='yes')
		{
			$(".parentCareTaker").show();
			if($("#takeCareOf_anotherChild").is(":checked")){
				$(".parentOrCaretakerBasicDetailsContainer").show();
			}
		}
	else{
		$(".parentCareTaker, .parentOrCaretakerBasicDetailsContainer").hide();

	}
}

function isNaturalizedCitizen()
{

	if($('#UScitizenIndicatorNo').is(':checked')){
		if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='true')
			{
				$("#documentTypeDiv").show();
				if(!$("#naturalizedCitizenNaturalizedIndicator").is(":checked")) {
					$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
				}
				$('#eligibleImmigrationStatusDiv').hide();

			}
		else if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='false'){

			$("#documentTypeDiv").hide();
			$('#eligibleImmigrationStatusDiv').show();
		//	$('#naturalCitizen_eligibleImmigrationStatus_ID').hide();

		}
	}
	else{
		$('#eligibleImmigrationStatusDiv').hide();
		$("#documentTypeDiv").hide();
	}

}


function showOrHideAt62Part2()
{
	if($('input[name=docType1]:radio:checked').val() == '1')
	{
		$('#appscr62P1_otherAlignAndI-94_table').show();
		$('#appscr62P1_otherComment_label').show();
		$('#appscr62P1_otherComment').show();
	}
	else
	{
		$('#appscr62P1_otherAlignAndI-94_table').hide();
		$('#appscr62P1_otherComment_label').hide();
		$('#appscr62P1_otherComment').hide();
	}
}

function showOrHide81BInfo()
{
	if($('#peopleHealthInsurNone').is(":checked") == true)
	{
		$("#peopleHealthInsurDependChild").prop("checked",false);
		$('#showOrHide_divAt81B').show();
	}
	else
	{
		$('#showOrHide_divAt81B').hide();
	}
}

function showOrHide81BInfoPart2() {
	if($("#peopleHealthInsurDependChild").is(":checked")){
		$('#peopleHealthInsurNone').prop("checked",false);
		$('#showOrHide_divAt81B').hide();
	}
}

function checkLanguage()
{
	if($('input[name=checkPersonNameLanguage]:radio:checked').val()=='yes')
		{
		$('#ethnicityDiv').show();
		}
	else
		{
		$('#ethnicityDiv').hide();
		}

}

function checkLivingInColorado(applicantActualName)
{

	if($('input[name=isTemporarilyLivingOutsideIndicator'+applicantActualName+']:radio:checked').val()=='yes')
	{
		$('#inColoradoDiv'+applicantActualName).show();
	}
	else
	{
		$('#inColoradoDiv'+applicantActualName).hide();
	}

}


function checkAmericonIndianQuestion(i)
{
	if($('input[name=AmericonIndianQuestionRadio'+ i +']:radio:checked').val()=='yes')
		{
		$('#americonIndianQuestionDiv' + i).show();
		}
	else if($('input[name=AmericonIndianQuestionRadio'+ i +']:radio:checked').val()=='no')
		{
		$('#americonIndianQuestionDiv' + i).hide();
		}




}

function showOrHideOptionDiv() {
	if ($("#wantToGetHelpPayingHealthInsurance_id1").is(":checked")) {
		$("#contant_message_ui").hide();
		/*if ($('#noOfPeople53').val().length <= 0 ) {*/
		 	$('#noOfPeople53').val("1");
		 	$("#noOfPeople53").focus();
	    /*}*/
	} else if ($("#wantToGetHelpPayingHealthInsurance_id2").is(":checked") && $("#exchangeName ").val() == "NMHIX") {
		$("#contant_message_ui").show();
	}
}
function hideSSNOnPage80()
{
	var $isSSNDisplayable=$("input[name=ssnIndicator80]:radio:checked").val().toLowerCase();
	if($isSSNDisplayable=='yes')
	{
		$("#ssnOnPage80").show();
	}
	else if ($isSSNDisplayable=='no') {
		$('#ssn1_80').removeClass("validateColor");
		$('#ssn2_80').removeClass("validateColor");
		$('#ssn3_80').removeClass("validateColor");
		$("#ssnOnPage80").hide();
	}
}






function showOrHideNaturalCitizenImmigrationStatus()
{
	var $checkBox=$("#naturalCitizen_eligibleImmigrationStatus");
	if ($checkBox.is(":checked")) {
		$("#naturalCitizen_eligibleImmigrationStatus_ID").show();
	}
	else $("#naturalCitizen_eligibleImmigrationStatus_ID").hide();
}


function disabilityOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#disability'+i).prop("checked"))
		{
			flag = true;
		}
	}
	if(flag)
		$('#disabilityNoneOfThese').prop("checked", false);
}
function disabilityNoneOfTheseChecked()
{
	if($('#disabilityNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#disability'+i).prop("checked", false);
		}

	}
}

function assistanceServicesOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#assistanceServices'+i).prop("checked"))
		{
			flag = true;
		}
	}
	if(flag)
		$('#assistanceServicesNoneOfThese').prop("checked", false);
}
function assistanceServicesNoneOfTheseChecked()
{
	if($('#assistanceServicesNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#assistanceServices'+i).prop("checked", false);
		}

	}
}

function incarceratedOnChange(obj){

		if(obj.checked)
		{
			$('#incarceratedContent85').hide();
			$("input[name=FNLNS]").prop("checked",false);
			$("input[radio-name=deposition]").prop("checked",false);
		}else{
			$('#incarceratedContent85').show();
		}

}

function incarcerationCBChange(i){
	if($('#incarcerationCB'+i).prop("checked"))	{
		$('#incarcerationCBOption'+i).show();
	}else{
		$('#incarcerationCBOption'+i).hide();
	}

}

function nativeOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#native'+i).prop("checked"))
		{
			$('#nativeTribe'+i).show();
			checkAmericonIndianQuestion(i);
			flag = true;
		}else{
			$('#nativeTribe'+i).hide();
			checkAmericonIndianQuestion(i);
		}
	}
	if(flag){
		$('#nativeNoneOfThese').prop("checked", false);
	}
}
function nativeNoneOfTheseChecked()
{
	if($('#nativeNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#native'+i).prop("checked", false);
			$('#native'+i).parent().next().hide();
		}

	}
}

function personfosterCareOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#personfosterCare'+i).prop("checked"))
		{
			flag = true;
		}
	}
	if(flag)
		$('#personfosterCareNoneOfThese').prop("checked", false);
}
function personfosterCareNoneOfTheseChecked()
{
	if($('#personfosterCareNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#personfosterCare'+i).prop("checked", false);
		}

	}
}

function deceasedOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#deceased'+i).prop("checked"))
		{
			flag = true;
		}
	}
	if(flag)
		$('#deceasedPersonNoneOfThese').prop("checked", false);
}
function deceasedNoneOfTheseChecked()
{
	if($('#deceasedPersonNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#deceased'+i).prop("checked", false);
		}

	}
}

function pregnancyOnChange()
{
	var flag = false;

	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#pregnancy'+i).html() != undefined)
			if($('#pregnancy'+i).prop("checked"))
			{
				flag = true;
			}
	}
	if(flag)
		$('#pregnancyNoneOfThese').prop("checked", false);
}
function pregnancyNoneOfTheseChecked()
{
	if($('#pregnancyNoneOfThese').html() != undefined)
		if($('#pregnancyNoneOfThese').prop("checked"))
		{
			for(var i = 1; i<=noOfHouseHold; i++)
			{
				$('#pregnancy'+i).prop("checked", false);
			}
			document.getElementById('appscr66PregnancyDetail').innerHTML = "";
		}
}

/*code by @Shovan as per new requirement*/
function textboxFocusEventFire(){
	$('#dateOfBirth').keyup(function(){
		if($(this).val()!= ""){
			$('.dateSpan').removeClass('dateSpanWithError');
		}else{
			$('.dateSpan').addClass('dateSpanWithError');
		}
	});
}

function termsCheckboxIsCheck(){
	if(!$('#acceptanceCB').is(":checked")){
		$('#parsley-terms-checkbox').show();
	}
	$('#acceptanceCB').click(function(){
		if(!$(this).is(":checked")){
			$('#parsley-terms-checkbox').show();
		}else{
			$('#parsley-terms-checkbox').hide();
		}
	});

}

/*for hix-33700*/
function payPerWeekRadioCheck(){
	if($('input[name=payPerWeekRadio]:radio:checked').val()=='Yes'){
		$('#dateEmployementExpire').show();
	}else{
		$('#dateEmployementExpire').hide();
	}
}



function applyingHousehold(){
	$('#parsley-ApplyingForhouseHold').hide();
}

/*code by @Shovan ends*/

function displayDateUnemploymentExpire(sel){
	  var selectedValue = sel.value;
	  if(selectedValue == "Weekly" || selectedValue == "Monthly"){
		  $("#isDateUnemploymentSet").show();
		  if($('input[name=payPerWeekRadio]:radio:checked').val()=='Yes'){
			  $('#dateEmployementExpire').show();
		  }

	  }else{
		  $("#isDateUnemploymentSet, #dateEmployementExpire").hide();
	  }
}


function howOftenGetThisAmount(sel){
	  var selectedValue = sel.value;
	  if(selectedValue == "Hourly"){
		  $("#howMuchPerWeek").show();
		  $("#Financial_Job_HoursOrDays_id").attr("data-parsley-pattern","^([1-9][0-9]?|1[0-5][0-9]|16[0-8])$");

	  }else if(selectedValue == "Daily") {
		  $("#howMuchPerWeek").show();
		  $("#Financial_Job_HoursOrDays_id").attr("data-parsley-pattern","^[1-7]{1}$");

	  }else{
		  $("#howMuchPerWeek").hide();
	  }
}


function displayWillBeOfferedFromJob(){
	 $("#willBeOfferedFromJob").show();
}

function displayAuthorizedRepresenatitive(){
	if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'yes'){
		$('#authorizedRepresentativeDiv').show();
	}else{
		$('#authorizedRepresentativeDiv').hide();
	}
}


function displaySigniture(){
	if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
		$('#signitureLabel').show();
	}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
		$('#signitureLabel').hide();
	}
}


function updateTaxReturnPeriod(){
	if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'yes'){
		$('#taxReturnPeriodDiv').hide();
	}else if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'no'){
		$('#taxReturnPeriodDiv').show();
	}
}

function displayOtherEthnicity(){
	if($('#ethnicity_other').is(':checked')){
		$('#otherEthnicity').show();
	}else{
		$('#otherEthnicity').hide();
	}
}

function displayOtherRace(){
	if($('#race_other').is(':checked')){
		$('#otherRace').show();
	}else{
		$('#otherRace').hide();
	}
}

/*
****************************************************************************************************************
* appscr55.js                                                                                       *
****************************************************************************************************************
*/

function unknown55ShowHide(){

	if($('input[name=appscr55paragraph]:radio:checked').val()== 'yes'){
		$('#contant_message_ui11').show();
	} /*else {

		if ($('input[name=appscr55paragraph]:radio:checked').val() == 'no' && $('input[name=checkTypeOfHelp4HealthInsurance]:radio:checked').val() == 'no') {
			//checkOut();
		}
	}*/
}

function checkOut() {
	alert(' Please contact your state health insurance exchange, to enroll in QHP');

	$("#appscr55").html($("#appscr55").html());
	window.location.reload();
}

/**
 * function for setting different content on for the div when different check
 * boxes selected
 */

//keys
function setDiffertContent() {
	var yesContent = '<b> '+jQuery.i18n.prop('ssap.page5.weEncourageText1')+'</b> '+jQuery.i18n.prop('ssap.page5.weEncourageText2')+ jQuery.i18n.prop('ssap.page5.weEncourageText3');
	var noContent = '<b>'+jQuery.i18n.prop('ssap.page5.BasedOnText1')+'</b>' + jQuery.i18n.prop('ssap.page5.BasedOnText2');
	var iDontKnowContent = '<b>'+jQuery.i18n.prop('ssap.page5.weEncourageText1')+'</b>'+jQuery.i18n.prop('ssap.page5.WeEncourageDontNoText2')+ jQuery.i18n.prop('ssap.page5.WeEncourageDontNoText3');
	if ($("#page55radio6").is(":checked")) {
		$("#contant_message_ui11").html(yesContent);
	} else if ($("#page55radio7").is(":checked")) {
		if ($("#page55radio10").is(":checked"))
			unknown55ShowHide();
		else
			$("#contant_message_ui11").html(noContent);
	} else if ($("#page55radio8").is(":checked")) {
		$("#contant_message_ui11").html(iDontKnowContent);
	}
}

/*
****************************************************************************************************************
* appscr81A.js                                                                                      *
****************************************************************************************************************
*/

function setappscr81A(){


	if(currentPage == "appscr81A")
		hideSSNQuestions();

	next();

}

//Skip or Keep Page80
function skipOrKeepPage80()
{
	householdSSNIndicationValue =  $('#appscr57HouseHoldHavingSSNTD1').text();
	if(currentPage == 'appscr79')
		if(householdSSNIndicationValue == "yes")
		{
			next();
		}
}

function hideSSNQuestions()
{
	if(householdSSNIndicationValue == "yes" || $('input:radio[name=ssnIndicator80]:checked').val() == "yes" )
	{
		$('#wouldLikeToGiveSSN_div').hide();
		$('#wouldLikeToGiveSSNlbl_div').hide();
	}
	else
	{
		$('#wouldLikeToGiveSSN_div').show();
		$('#wouldLikeToGiveSSNlbl_div').show();
	}
}

/*
****************************************************************************************************************
* backFunctionality.js                                                                                       *
****************************************************************************************************************
*/

function baseFunctionForBackButton() {
	var appscrnpage = currentPage;

	if((flagToUpdateFromResult || editFlag) && ( appscrnpage == 'appscr61' || appscrnpage == 'appscr62Part1')){
		//$('#back_button_div').hide();
		$('#countinue_button_div').show();
		$('#contBtn').text('Continue');
		$('#contBtn').show();
	}
	else
		{
		$('#back_button_div').show();
		$('#countinue_button_div').show();
		$('#contBtn').text('Continue');
		$('#contBtn').show();
		}

	switch (appscrnpage) {


	case "appscr61":
		if (nonFinancialHouseHoldDone > 1) {


			nonFinancialHouseHoldDone--;

			if(getInfantData(nonFinancialHouseHoldDone)=='yes')
				while(getInfantData(nonFinancialHouseHoldDone)=='yes')
				{
					nonFinancialHouseHoldDone--;
				}

			setNonFinancialPageProperty(nonFinancialHouseHoldDone);

			appscr67EditController(nonFinancialHouseHoldDone);
			editFlag = false;
			$('#back_button_div').show();





			fetchDataNonFinancialHouseHold();
			backData64();
			goToPageById("appscr64");

			return false;
		}
		else
			{

			//HouseHoldRepeat = parseInt($('#numberOfHouseHold').text());

			//always go back to primary contact tax file page
			HouseHoldRepeat = 1;

			$("input[name=planToFileFTRIndicator], input[name=householdHasDependant], #householdContactDependant1").prop("disabled",false);

			if(getInfantData(HouseHoldRepeat)=='yes')
				while(getInfantData(HouseHoldRepeat)=='yes')
				{
					HouseHoldRepeat--;
				}

			setNonFinancialPageProperty(HouseHoldRepeat);
			fetchDataAddHouseHold();
			backData60(); // for save

			if(ssapApplicationType == "NON_FINANCIAL"){
				goToPageById("appscr59A");
			}else{
				//pre-populate data from JSON
				updateAppscr60FromJSON(HouseHoldRepeat);
				goToPageById("appscr60");
			}


			return false;

			}
		break;


	case "appscr69":

		if (financialHouseHoldDone > 1) {

			financialHouseHoldDone--;

			appscr74EditController(financialHouseHoldDone);
			$('#back_button_div').show();

			editFlag=false;

			if($('#incomesStatus'+financialHouseHoldDone).text() != 'No')
				{
				backData73();
				goToPageById("appscr73");
				}
			else
				{


				goToPageById("appscr70");
				}
			fetchDataAddFinancialHouseHold();
			setNonFinancialPageProperty(financialHouseHoldDone);
			backData73();
			backData72();
			backData71();
			backData70();
			return false;
		}
		else
			{
			setNonFinancialPageProperty(nonFinancialHouseHoldDone);
			}
		break;


	case "appscr76P1":
		if(addQuetionsHouseHoldDone != 1)
		{
			addQuetionsHouseHoldDone--;
			setNonFinancialPageProperty(addQuetionsHouseHoldDone);
			setHouseHoldData();

			goToPageById("appscr81B");
			return false;
		}
		break;

	/*case "appscr76P2":
		if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')
		{

		goToPageById("appscr75A");
		return false;

		}
		backData76P1();
		break;*/

	case "appscr74":

			appscr74EditController(financialHouseHoldDone);


			editFlag=false;

			if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')

				$("#appscr73EmployerNameDiv").hide();
			else

				$("#appscr73EmployerNameDiv").show();


			fetchDataAddFinancialHouseHold();

		if(anyIncome(financialHouseHoldDone)==false){
			    backData69();backData70();backData71();backData72();backData73();
				goToPageById("appscr70");

				}
				else{
				backData73();
				goToPageById("appscr73");
				}



			$('#back_button_div').show();

			setNonFinancialPageProperty(financialHouseHoldDone);
			backData73();
			return false;

			break;


	case "appscr60":

		if (HouseHoldRepeat > 1) {

			$("input[name=planToFileFTRIndicator], input[name=householdHasDependant], input[name=householdContactSpouse], #householdContactDependant1").prop("disabled",false);

			//HouseHoldRepeat--;
			HouseHoldRepeat = 1;

			if(getInfantData(HouseHoldRepeat)=='yes')
				while(getInfantData(HouseHoldRepeat)=='yes')
				{
					HouseHoldRepeat--;
				}



			setNonFinancialPageProperty(HouseHoldRepeat);

			fetchDataAddHouseHold();

			backData60(); // for save

			//pre-populate data from JSON
			updateAppscr60FromJSON(HouseHoldRepeat);
			goToPageById("appscr60");

			return false;

		}
		else
			{



			goToPageById("appscr59A");
			return false;

			}

	case "appscr65":

			if(getInfantData(nonFinancialHouseHoldDone)=='yes')
			while(getInfantData(nonFinancialHouseHoldDone)=='yes')
			{
				nonFinancialHouseHoldDone--;
			}


			setNonFinancialPageProperty(nonFinancialHouseHoldDone);

			appscr67EditController(nonFinancialHouseHoldDone);
			editFlag = false;
			$('#back_button_div').show();

			fetchDataAddHouseHold();
			backData64();
			goToPageById("appscr64");

			return false;



		break;

	case "appscr63":

		var usVariable = $("#appscr57UScitizenTD"+nonFinancialHouseHoldDone).text();
		var TempUScitizen=$('input[name=UScitizen]:radio:checked').val();

		goToPageById("appscr62Part1");
		backData62Part1();
		return false;



		/*if(usVariable == 'yes' || TempUScitizen == 'yes')
			{
			goToPageById("appscr62Part1");
			backData62Part1();
			return false;
			}
		else
			{
			goToPageById("appscr62Part2");
			backData62Part2();
			return false;
			}


		break;*/

	}
}


function anyIncome(applicantID){


		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[applicantID-1];

		var haveAnyIcome = false;

		if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.socialSecurityBenefitIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.unemploymentBenefitIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.retirementOrPensionIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.capitalGainsIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.farmFishingIncomeIndictor == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.alimonyReceivedIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.investmentIncomeIndicator == true){
			haveAnyIcome = true;
		}
		if(householdMemberObj.detailedIncome.otherIncomeIndicator == true){
			haveAnyIcome = true;
		}
		return haveAnyIcome;
}

/*********************************************************************************************/
function fetchDataAddHouseHold() {

	var houseHoldIndex = HouseHoldRepeat;

	getSpouse(houseHoldIndex);
	/*var martialStatus=$("#appscr57isMarriedTD"+houseHoldIndex).text();
	var spouseID=$("#appscr57spouseIdTD"+houseHoldIndex).text();
	var isPayingJointly=$("#appscr57payJointlyTD"+houseHoldIndex).text();
	var  isTaxFiller=$("#appscr57isTaxFilerTD"+houseHoldIndex).text();

	if($('#appscrIsChildDiv').is(':visible')){
		if(martialStatus.toLowerCase()=="yes"||martialStatus.toLowerCase()=="true"){
			$("#marriedIndicatorYes").prop('checked',true);
		}
		else if(martialStatus.toLowerCase()=="no"||martialStatus.toLowerCase()=="false") {
				$("#marriedIndicatorNo").prop('checked',true);
		}
	}


	if($('#householdSpouseMainDiv').is(':visible')){
		if(isPayingJointly.toLowerCase()=="yes"||isPayingJointly.toLowerCase()=="true"){
			$("#planOnFilingJointFTRIndicatorYes").prop('checked',true);
		}
		else if(isPayingJointly.toLowerCase()=="no"||isPayingJointly.toLowerCase()=="false") {
			$("#planOnFilingJointFTRIndicatorNo").prop('checked',true);
		}
	}



	if (isTaxFiller.toLowerCase()=="yes"||isTaxFiller.toLowerCase()=="true") {
		$("#planToFileFTRIndicatorYes").prop('checked',true);

	} else if (isTaxFiller.toLowerCase()=="no"||isTaxFiller.toLowerCase()=="false") { {
		$("#planToFileFTRIndicatorNo").prop('checked',true);
	}

	for ( var hNum = 1; hNum <= noOfHouseHold; hNum++) {
		if($("#HouseHold"+hNum).is(":checked")){
			if ($("#HouseHold"+hNum)==spouseID) {
				$("#HouseHold"+hNum).prop('checked',true);
				break;
			}
		}

		}
	}*/



}

/*********************************************************************************************/
function fetchDataNonFinancialHouseHold() {

	/*

	$('input[name=appscr61_gender][value='+$("#appscrHouseHoldgender57TD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

	$('input[name=socialSecurityCardHolderIndicator][value='+$("#appscr57HouseHoldHavingSSNTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);




	var HouseHoldSSN=$("#appscr57HouseHoldSSNTD"+nonFinancialHouseHoldDone).text();
	if(HouseHoldSSN=='')
	{

	 $('#ssn1').val("");
	 $('#ssn2').val("");
	 $('#ssn3').val("");
		$('#ssn').val("");
	}
	else
		{
			$('#ssn').val(HouseHoldSSN);
		 $('#ssn1').val(HouseHoldSSN.split("-")[0]);
		 $('#ssn2').val(HouseHoldSSN.split("-")[1]);
		 $('#ssn3').val(HouseHoldSSN.split("-")[2]);

		}


		$('#reasonableExplanationForNoSSN').val($("#appscr57notAvailableSSNReasonTD"+nonFinancialHouseHoldDone).text());



		$('input[name=UScitizen][value='+$("#appscr57UScitizenTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=UScitizenManualVerification][value='+$("#appscr57UScitizenManualVerificationTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=naturalizedCitizenshipIndicator][value='+$("#appscr57naturalizedCitizenshipIndicatorTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=livedIntheUSSince1996Indicator][value='+$("#appscr57livedIntheUSSince1996IndicatorTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

		$('input[name=naturalCitizen_eligibleImmigrationStatus][value='+$("#appscr57ImmigrationStatusTD"+nonFinancialHouseHoldDone).text()+']').prop('checked',true);

*/
}

/*********************************************************************************************/
function fetchDataAddFinancialHouseHold() {

		/*var availableIncome=false;
			var dataIndex=0;

				$('input[name=appscr70radios4Income][value='+$("#incomesStatus"+financialHouseHoldDone).text()+']').prop('checked',true);

			for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {


			if(dataIndex==0)
			{
					 $('#irsIncome').val($("#appscrirsIncome57TD"+financialHouseHoldDone).text());
					 $('#irsIncomeLabel').html("$"+$("#appscrirsIncome57TD"+financialHouseHoldDone).text());
					 continue;
			}
			var data = financial_DetailsArray[dataIndex];


			 $('#'+data+'_id').val($("#appscr"+data+"57TD"+financialHouseHoldDone).html());
				if(dataIndex!=2 && dataIndex!=6)
					$('#'+data+'_select_id').val($("#appscr"+data+"Frequency57TD"+financialHouseHoldDone).text());



				if( $("#appscr"+data+"57TD"+financialHouseHoldDone).text() == '0')
				{

					$('#' + data + '_Div').hide();
					$('#' + data ).prop('checked', false);


				}
			else
				{
				//$('#' + data + '_Div').show();
				//$('#' + data ).prop('checked', true);

				availableIncome=true;
				}
			}

			if($("#appscrexpectedIncome57TD"+financialHouseHoldDone).html() != 'N/A')
				$('#expectedIncomeByHouseHoldMember').val($("#appscrexpectedIncome57TD"+financialHouseHoldDone).text());
			else
				$('#expectedIncomeByHouseHoldMember').val('');

			if(availableIncome==true)
				{

				$('#checkBoxes_div').show();
				$('#checkBoxes_divlbl').show();

				}
			else
				{
				$('#Financial_None').prop('checked',true);
				$('#checkBoxes_div').hide();
				$('#checkBoxes_divlbl').hide();
				}


			if($("#appscrFinancial_Job57TD"+financialHouseHoldDone).text()=='0')

				$("#appscr73EmployerNameDiv").hide();
			else

				$("#appscr73EmployerNameDiv").show();
*/

}

/*
****************************************************************************************************************
* editController.js                                                                                       *
****************************************************************************************************************
*/


var editFlag = false;
var editMemberNumber = 0;
var cancleFlag = true;
var loginUserID = "";
var editData = false;
var editNonFinacial = false;
var applicantGuidToBeRemoved = "";
var personIdToBeRemoved = "";
//	Edit functionality of Household Information

function removeApplicant(personID)
{
    if(personID == 1) return; // personid = 1 can't be deleted

    var noOfHouseHoldmember= getNoOfHouseHolds();
    for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
    		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
         	if(householdMemberObj.personId > 1 &&  householdMemberObj.personId == personID){
         		personIdToBeRemoved = personID;
         		applicantGuidToBeRemoved = householdMemberObj.applicantGuid;
         		houseHoldname = getFormattedname(householdMemberObj.name.firstName)+" "+getFormattedname(householdMemberObj.name.middleName)+" "+getFormattedname(householdMemberObj.name.lastName);
         		$('#deleteApplicantName').html(houseHoldname);
         		$('#btnDeleteApplicantYes').prop("disabled",false);
         		$('#modalDeleteApplicant').modal();
         		/*if(householdMemberObj.applicantGuid==""){
         			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.splice(cnt-1,1);// remove applicant from array
         			afterDeleteApplicant();
         		}else{
         			applicantGuidToBeRemoved = householdMemberObj.applicantGuid;
         			saveDataSubmit();
         		}*/
         		//processApplicantDeletion(householdMemberObj.applicantGuid, personID);
         		break; //exit for loop
         	}
    }

    //nonFinancialHouseHoldDone = noOfHouseHoldmember; // last household done

    //saveData()// call existing method to save data into DB

}
function deleteApplicantConfirmed(){
	//$('#btnDeleteApplicantYes').prop("disabled",true); // disabled, restict user to click only once
	$('#modalDeleteApplicant').modal('hide');
	processApplicantDeletion(applicantGuidToBeRemoved, personIdToBeRemoved);
}
function cancelDeleteApplicant(){
	$('#modalDeleteApplicant').modal('hide');
	applicantGuidToBeRemoved ="";
	personIdToBeRemoved = "";
}
function removeRelationship(personID){

	var bloodRelationship=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship;
	var noOfRelations = bloodRelationship.length;
	var currentRelationInd = 0;
    for ( var ind = 0; ind < noOfRelations; ind++) {
    	if (bloodRelationship[currentRelationInd].individualPersonId == personID || bloodRelationship[currentRelationInd].relatedPersonId == personID){
    		bloodRelationship.splice(currentRelationInd,1);
    	}else{
    		currentRelationInd++;
    	}
    }
    webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].bloodRelationship = bloodRelationship;
}

function correctIncarcerationIndicator(){
	noOfHouseHold = getNoOfHouseHolds();
	webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = false;
	for (i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		if(householdMemberObj.incarcerationStatus.incarcerationStatusIndicator == true){
			webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = true;
			break;
		}
	}
}

function afterDeleteApplicant(personID){
	//removeRelationship(personID);// still have to work on this logic
	//correctIncarcerationIndicator();
	noOfHouseHoldmember=getNoOfHouseHolds();
    $('#numberOfHouseHold').text(noOfHouseHoldmember); //reset no. of household variable
    $('#noOfPeople53').val(noOfHouseHoldmember); //reset no. of household variable
    if(currentPage == "appscr58"){
		Appscr58DetailJson();
	}else if(currentPage == "appscr67"){
		showHouseHoldContactSummary();
		nonFinancialHouseHoldDone = getNoOfHouseHolds();
		saveCurrentApplicantID(nonFinancialHouseHoldDone);
	}
}

function findApplicantByPersonId(personID){
	var noOfHouseHoldmember= getNoOfHouseHolds();
    for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
    		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
         	if(householdMemberObj.personId == personID){
         		return householdMemberObj;
         		break;
         	}
    }
}

/*
function deleteApplicantFromDB(personId){
	webJsonstring = JSON.stringify(webJson);
	var isApplicantDeleted = false;
	$.ajax({
		type : 'POST',
		url : '"/ssap/deleteSsapApplicant',
		dataType : 'json',
		data : {
			personId : personId,
			currentPage : currentPage,
			webJson:webJsonstring

		},
		//async : true,
		success : function(msg,xhr) {
			if(msg!='' && msg != null) {
				webJson = msg;
			}

			if(webJson.singleStreamlinedApplication.mecCheckStatus!=null && webJson.singleStreamlinedApplication.mecCheckStatus!=undefined && "Y"==webJson.singleStreamlinedApplication.mecCheckStatus){
				$('#mecCheckAlert').modal();
			}
			$('#ssapSubmitModal').modal('hide');
			$('#waiting').hide();
			$('#contBtn').removeAttr('disabled');
			//$('#generateJSONForm').submit();
			isApplicantDeleted = true;

		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			$('#waiting').hide();
			$('#ssapSubmitModal').modal('hide');
			alert("Please try Again !");
			//$('#contBtn').removeAttr('disabled');
			isApplicantDeleted = false;
		}
	});
	return isApplicantDeleted;
}
*/
function appscr58EditController(i)
{
	editFlag = true;

	editMemberNumber = i;

	//$('#contBtn').text('Update');
	$('#countinue_button_div').removeClass("Continue_button");
	$('#countinue_button_div').addClass("ContinueForUpdate_button");
	$('#cancelButtonCancel').show();
	$('#back_button_div').hide();
	$('#contBtn').hide();


	if(i == 1)
	{
		setValuesToappscr53JSON();
		goToPageById("appscr53");
		$('#contBtn').attr('onclick', 'appscr58EditUdate(1),'+true+'');
	}
	else
	{
		$('#appscr57FirstName'+i).focus();
		setValuesToappscr57JSON();
		goToPageById("appscr57");
		$('#contBtn').attr('onclick', 'appscr58EditUdate('+i+'),'+true+'');

		blockOtherFields(i);
	}
}

function enableOtherFields()
{
	/*for(var j=1; j<=noOfHouseHold; j++)
	{
		$('#appscr57FirstName'+j).attr("disabled",false);
		$('#appscr57MiddleName'+j).attr("disabled",false);
		$('#appscr57LastName'+j).attr("disabled",false);
		$('#appscr57Suffix'+j).attr("disabled",false);
		$('#appscr57DOB'+j).attr("disabled",false);

		$('#appscr57DOBVerificationStatus'+j).attr("disabled",false);
		$('input[name=appscr57Sex'+j+']:radio').attr("disabled",false);
		$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",false);
	}*/
	$('#appscr57HouseholdForm input, #appscr57HouseholdForm select').prop('disabled',false);
}

function blockOtherFields(i)
{
	for(var j=1; j<=noOfHouseHold; j++)
	{
		if(j==i)
		{
			$('#appscr57FirstName'+j).focus();
			continue;
		}
		else
		{
			$('#appscr57FirstName'+j).attr("disabled",true);
			$('#appscr57MiddleName'+j).attr("disabled",true);
			$('#appscr57LastName'+j).attr("disabled",true);
			$('#appscr57Suffix'+j).attr("disabled",true);
			$('#appscr57DOB'+j).attr("disabled",true);

			$('#appscr57DOBVerificationStatus'+j).attr("disabled",true);
			$('input[name=appscr57Sex'+j+']:radio').attr("disabled",true);
			$('input[name=appscr57Tobacco'+j+']:radio').attr("disabled",true);

			$('#appscr57checkseekingcoverage'+j).prop("disabled",true);
		}
	}
}

function appscr58EditCancel()
{
	$('#contBtn').show();
	dateValidation = true;
	enableOtherFields();

	if(!cancleFlag)
	{	validationCheck = true;
		$('#addAnotherHouseHold'+noOfHouseHold).hide();
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();
		noOfHouseHold--;

		$('#numberOfHouseHold').text(noOfHouseHold);
		$('#noOfPeople53').val(noOfHouseHold);
		cancleFlag = true;
	}
	else
	{
		$('#back_button_div').show();
		$('#contBtn').text('Continue');
		$('#countinue_button_div').addClass("Continue_button");
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");
		$('#cancelButtonCancel').hide();
		enableOtherFields();
		editFlag = false;
	}
		goToPageById("appscr58");

}

function appscr58EditCancle()
{
	$('#countinue_button_div').show();
	$('#back_button_div').show();
	$('#editUdateOrCancle_Div').hide();

	goToPageById("appscr58");
	editFlag = false;
}

function appscr58EditUdate(i,updateFlag)
{
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	if(!dateValidation){

		$('html, body').animate({
			scrollTop : $("#"+currentPage).find('.dobCustomError span').first().offset().top - 250
		}, 'fast');
		return;
	}

	$('#contBtn').text('Continue');
	$('#countinue_button_div').removeClass("ContinueForUpdate_button");
	$('#countinue_button_div').addClass("Continue_button");
	$('#cancelButtonCancel').hide();
	$('#back_button_div').show();

	cancleFlag = true;
	if(i == 1)
	{
		if(updateFlag!=false)
		{
			saveData53();
		}

	}
	else
	{
		if(updateFlag!=false)
		{	backData57();
			generateArray57();
			saveData57();
		}

		if(useJSON == true){
			appscr58EditUdateJson(i);
		}

	}

	addAppscr58Detail();

	if(editFlag)
	{
		goToPageById("appscr58");
		enableOtherFields();
	}
	editFlag = false;
}


//	Edit functionality of NonFinancialDetails

function appscr67EditController(i)
{

	if(useJSON == true){
		appscr67EditControllerJSON(i);
		return;
	}
	$('#back_button_div').show();
}

function appscr74EditController(i)
{
	editMemberNumber = i;
	if(!haveDataFlag){
		editFlag = true;
		//$('#back_button_div').hide();
	}
	if(useJSON == true){
		if(!haveDataFlag){
			financialHouseHoldDone = i;
			goToPageById("appscr69");
		}
		appscr74EditControllerJSON(i);
		return;
	}
	var editIrsIncome 							  = $('#appscrirsIncome57TD'+i).text();
	var editIncomesStatus 						  = $('#incomesStatus'+i).text();
	var editJobIncome 							  = $('#appscrFinancial_Job57TD'+i).text();
	var editJobFrequency 						  = $('#appscrFinancial_JobFrequency57TD'+i).text();
	var editEmployerName						  = $('#appscrFinancial_JobEmployerName57TD'+i).text();
	var editHoursOrDays							  = $('#appscrFinancial_JobHoursOrDays57TD'+i).text();
	var editSelfEmploymentIncome 				  = $('#appscrFinancial_Self_Employment57TD'+i).text();
	var editEmploymentTypeOfWork				  = $('#appscrFinancial_Self_EmploymentTypeOfWork57TD'+i).text();
	var editSocialSecurityBenefitsIncome 		  = $('#appscrFinancial_SocialSecuritybenefits57TD'+i).text();
	var editSocialSecurityBenefitsIncomeFrequency = $('#appscrFinancial_SocialSecuritybenefitsFrequency57TD'+i).text();
	var editUnemploymentIncome 					  = $('#appscrFinancial_Unemployment57TD'+i).text();
	var editUnemploymentIncomeFrequency 		  = $('#appscrFinancial_UnemploymentFrequency57TD'+i).text();
	var editUnemploymentStateGovernment			  = $('#appscrFinancial_UnemploymentStateGovernment57TD'+i).text();
	var editunemploymentIncomeDate				  = $('#appscrFinancial_UnemploymentDate57TD'+i).text();
	var editRetirementPensionIncome 			  = $('#appscrFinancial_Retirement_pension57TD'+i).text();
	var editRetirementPensionIncomeFrequency 	  = $('#appscrFinancial_Retirement_pensionFrequency57TD'+i).text();
	var editCapitalgainsIncome 					  = $('#appscrFinancial_Capitalgains57TD'+i).text();
	var editInvestmentIncome 					  = $('#appscrFinancial_InvestmentIncome57TD'+i).text();
	var editInvestmentIncomeFrequency 			  = $('#appscrFinancial_InvestmentIncomeFrequency57TD'+i).text();
	var editRentalOrRoyaltyIncome 				  = $('#appscrFinancial_RentalOrRoyaltyIncome57TD'+i).text();
	var editRentalOrRoyaltyIncomeFrequency 		  = $('#appscrFinancial_RentalOrRoyaltyIncomeFrequency57TD'+i).text();
	var editFarmingOrFishingIncome 				  = $('#appscrFinancial_FarmingOrFishingIncome57TD'+i).text();
	var editFarmingOrFishingIncomeFrequency 	  = $('#appscrFinancial_FarmingOrFishingIncomeFrequency57TD'+i).text();
	var editAlimonyReceivedIncome 				  = $('#appscrFinancial_AlimonyReceived57TD'+i).text();
	var editAlimonyReceivedIncomeFrequency 		  = $('#appscrFinancial_AlimonyReceivedFrequency57TD'+i).text();
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	var editOtherTypeIncome						  = $('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text();

	var editExpectedIncome 						  = $('#appscrexpectedIncome57TD'+i).text();
	var editIrsHouseHoldExplanationTextArea 	  = $('#appscr57HouseHoldExplanationTextAreaTD'+i).text();

	var editHouseHoldalimonyAmount 				  = $('#appscr57HouseHoldalimonyAmountTD'+i).text();
	var editHouseHoldalimonyFrequency 			  = $('#appscr57HouseHoldalimonyFrequencyTD'+i).text();
	var editHouseHoldstudentLoanAmount 			  = $('#appscr57HouseHoldstudentLoanAmountTD'+i).text();
	var editHouseHoldstudentLoanFrequency 		  = $('#appscr57HouseHoldstudentLoanFrequencyTD'+i).text();
	var editHouseHolddeductionsType 			  = $('#appscr57HouseHolddeductionsTypeTD'+i).text();
	var editHouseHolddeductionsAmount 			  = $('#appscr57HouseHolddeductionsAmountTD'+i).text();
	var editHouseHolddeductionsFrequency 		  = $('#appscr57HouseHolddeductionsFrequencyTD'+i).text();

	var editHouseHoldbasedOnInput 				  = $('#appscr57HouseHoldbasedOnInputTD'+i).text();

	var editstopWorking							  = $("#appscrStopWorking57TD"+i).html();
	var editworkAt								  =	$("#appscrWorkAt57TD"+i).html();
	var editdecresedAt							  = $("#appscrDecresedAt57TD"+i).html();
	var editsalaryAt							  = $("#appscrSalaryAt57TD"+i).html();
	var editsesWorker							  = $("#appscrSesWorker57TD"+i).html();
	var editexplainText							  = $("#appscrExplainText57TD"+i).html();
	var editotherJointIncome					  = $("#appscrOtherJointIncome57TD"+i).html();

	var incomeStatusFlag;

	resetPage69Values();
	resetPage71Values();
	resetPage72DeductionValues();

	var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	//var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();

	  $('.nameOfHouseHold').text(houseHoldname);


	if(editIrsIncome != 0)
	{
		$('#irsIncome2013').val(editIrsIncome);

		irsIncomePrevYear();

		$('#irsIncomeLabel').text( $('#appscrirsIncome57TD'+i).text());
	}

	if(editIncomesStatus == "Yes" || editIncomesStatus == "yes")
	{
		$('#appscr70radios4IncomeYes').prop("checked",true);
		selectIncomesCheckBoxes();

		if(editJobFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;

			document.getElementById('Financial_Job').checked = true;
			$('#Financial_Job_EmployerName_id').val(editEmployerName);
			$('#Financial_Job_HoursOrDays_id').val(editHoursOrDays);
			$('#Financial_Job_id').val(editJobIncome);
			$('#Financial_Job_select_id').val(editJobFrequency);
			showDiductionsFields();
		}
		else
		{
			document.getElementById('Financial_Job').checked = false;
			$('#Financial_Job_EmployerName_id').val('');
			$('#Financial_Job_HoursOrDays_id').val('');
			$('#Financial_Job_Div').hide();
		}

		if(editSelfEmploymentIncome != "0" && editSelfEmploymentIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Self_Employment').checked = true;
			$('#Financial_Self_Employment_TypeOfWork').val(editEmploymentTypeOfWork);
			$('#Financial_Self_Employment_id').val(editJobIncome);
		}
		else
		{
			document.getElementById('Financial_Self_Employment').checked = false;
			$('#Financial_Self_Employment_TypeOfWork').val('');
			$('#Financial_Self_Employment_Div').hide();
		}

		if(editSocialSecurityBenefitsIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_SocialSecuritybenefits').checked = true;
			$('#Financial_SocialSecuritybenefits_id').val(editSocialSecurityBenefitsIncome);
			$('#Financial_SocialSecuritybenefits_select_id').val(editSocialSecurityBenefitsIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_SocialSecuritybenefits').checked = false;
			$('#Financial_SocialSecuritybenefits_Div').hide();
		}

		if(editUnemploymentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Unemployment').checked = true;
			$('#Financial_Unemployment_StateGovernment').val(editUnemploymentStateGovernment);
			$('#Financial_Unemployment_id').val(editUnemploymentIncome);
			$('#unemploymentIncomeDate').val(editunemploymentIncomeDate);
			$('#Financial_Unemployment_select_id').val(editUnemploymentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Unemployment').checked = false;
			$('#Financial_Unemployment_StateGovernment').val('');
			$('#unemploymentIncomeDate').val('');
			$('#Financial_Unemployment_Div').hide();
		}

		if(editRetirementPensionIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Retirement_pension').checked = true;
			$('#Financial_Retirement_pension_id').val(editRetirementPensionIncome);
			$('#Financial_Retirement_pension_select_id').val(editRetirementPensionIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_Retirement_pension').checked = false;
			$('#Financial_Retirement_pension_Div').hide();
		}

		if(editCapitalgainsIncome != "0" && editCapitalgainsIncome != "0.0")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_Capitalgains').checked = true;
			$('#Financial_Capitalgains_id').val(editCapitalgainsIncome);
		}
		else
		{
			document.getElementById('Financial_Capitalgains').checked = false;
			$('#Financial_Capitalgains_Div').hide();
		}

		if(editInvestmentIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_InvestmentIncome').checked = true;
			$('#Financial_InvestmentIncome_id').val(editInvestmentIncome);
			$('#Financial_InvestmentIncome_select_id').val(editInvestmentIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_InvestmentIncome').checked = false;
			$('#Financial_InvestmentIncome_Div').hide();
		}

		if(editRentalOrRoyaltyIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = true;
			$('#Financial_RentalOrRoyaltyIncome_id').val(editRentalOrRoyaltyIncome);
			$('#Financial_RentalOrRoyaltyIncome_select_id').val(editRentalOrRoyaltyIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_RentalOrRoyaltyIncome').checked = false;
			$('#Financial_RentalOrRoyaltyIncome_Div').hide();
		}

		if(editFarmingOrFishingIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_FarmingOrFishingIncome').checked = true;
			$('#Financial_FarmingOrFishingIncome_id').val(editFarmingOrFishingIncome);
			$('#Financial_FarmingOrFishingIncome_select_id').val(editFarmingOrFishingIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_FarmingOrFishingIncome').checked = false;
			$('#Financial_FarmingOrFishingIncome_Div').hide();
		}

		if(editAlimonyReceivedIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_AlimonyReceived').checked = true;
			$('#Financial_AlimonyReceived_id').val(editAlimonyReceivedIncome);
			$('#Financial_AlimonyReceived_select_id').val(editAlimonyReceivedIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_AlimonyReceived').checked = false;
			$('#Financial_AlimonyReceived_Div').hide();
		}

		if(editOtherIncomeFrequency != "SelectFrequency")
		{
			incomeStatusFlag = true;
			document.getElementById('Financial_OtherIncome').checked = true;
			$('#Financial_OtherTypeIncome').val(editOtherTypeIncome);
			$('#Financial_OtherIncome_id').val(editOtherIncome);
			$('#Financial_OtherIncome_select_id').val(editOtherIncomeFrequency);
		}
		else
		{
			document.getElementById('Financial_OtherIncome').checked = false;
			$('#Financial_OtherTypeIncome').val('');
			$('#Financial_OtherIncome_Div').hide();
		}
	}
	else
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}

	if(incomeStatusFlag == false)
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}

	if(editExpectedIncome != 0)
	{
		/*$('#expectedIncomeByHouseHoldMember').val(editExpectedIncome);*/
		$('#appscr69ExpectedIncome').val(editExpectedIncome);
	}

	if(editIrsHouseHoldExplanationTextArea != "")
	{
		$('#explanationTextArea').val(editIrsHouseHoldExplanationTextArea);
	}



	resetPage72DeductionValues();

	if(editHouseHoldalimonyFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72alimonyPaid').checked = true;
		$('#alimonyAmountInput').val(editHouseHoldalimonyAmount);
		$('#alimonyFrequencySelect').val(editHouseHoldalimonyFrequency);
		$('#appscr72alimonyPaidTR').show();
	}

	if(editHouseHoldstudentLoanFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72studentLoanInterest').checked = true;
		$('#studentLoanInterestAmountInput').val(editHouseHoldstudentLoanAmount);
		$('#studentLoanInterestFrequencySelect').val(editHouseHoldstudentLoanFrequency);
		$('#appscr72studentLoanInterestTR').show();
	}

	if(editHouseHolddeductionsFrequency != "SelectFrequency")
	{
		document.getElementById('appscr72otherDeduction').checked = true;
		$('#deductionTypeInput').val(editHouseHolddeductionsType);
		$('#deductionAmountInput').val(editHouseHolddeductionsAmount);
		$('#deductionsFrequencySelect').val(editHouseHolddeductionsFrequency);
		$('#appscr72otherDeductionTR').show();
	}

	if(editHouseHoldbasedOnInput !="0" && editHouseHoldbasedOnInput != "0.0")
	{
		$('#basedOnAmountInput').val(editHouseHoldbasedOnInput);
	}

	if(editworkAt == "Yes" || editworkAt == "yes"){
		$('#appscr73WorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73WorkingIndicatorNo').prop("checked",true);
	}

	if(editdecresedAt == "Yes" || editdecresedAt == "yes"){
		$('#appscr73DecresedAtYes').prop("checked",true);
	}
	else{
		$('#appscr73DecresedAtNo').prop("checked",true);
	}

	if(editstopWorking == "Yes" || editstopWorking == "yes"){
		$('#appscr73stopWorkingIndicatorYes').prop("checked",true);
	}
	else{
		$('#appscr73stopWorkingIndicatorNo').prop("checked",true);
	}

	if(editsalaryAt == "Yes" || editsalaryAt == "yes"){
		$('#appscr73SalaryAtYes').prop("checked",true);
	}else{
		$('#appscr73SalaryAtNo').prop("checked",true);
	}

	if(editsesWorker == "Yes" || editsesWorker == "yes"){
		$('#appscr73SesWorkerYes').prop("checked",true);
	}else{
		$('#appscr73SesWorkerNo').prop("checked",true);
	}
	if(editotherJointIncome == "Yes" || editotherJointIncome == "yes"){
		$('#appscr73OtherJointIncomeYes').prop("checked",true);
	}else{
		$('#appscr73OtherJointIncomeNo').prop("checked",true);
	}


	$('#appscr73ExplanationTaxtArea').val(editexplainText);

	if(!haveDataFlag)
		goToPageById("appscr69");

}

function appscr74EditUpdateValues(i) {
	//
}

function resetTotalInputsOrSelectboxesforEdit()
{
	$('#Financial_Job_EmployerName_id').val('');
	$('#Financial_Job_HoursOrDays_id').val(0);
	$('#Financial_Job_id').val(0);
	$('#Financial_Job_select_id').val('SelectFrequency');
	$('#Financial_Self_Employment_TypeOfWork').val('');
	$('#Financial_Self_Employment_id').val(0);
	$('#Financial_SocialSecuritybenefits_id').val(0);
	$('#Financial_SocialSecuritybenefits_select_id').val('SelectFrequency');
	$('#Financial_Unemployment_StateGovernment').val('');
	$('#Financial_Unemployment_id').val(0);
	$('#Financial_Unemployment_select_id').val('SelectFrequency');
	$('#Financial_Retirement_pension_id').val(0);
	$('#Financial_Retirement_pension_select_id').val('SelectFrequency');
	$('#Financial_Capitalgains_id').val(0);
	$('#Financial_InvestmentIncome_id').val(0);
	$('#Financial_InvestmentIncome_select_id').val('SelectFrequency');
	$('#Financial_RentalOrRoyaltyIncome_id').val(0);
	$('#Financial_RentalOrRoyaltyIncome_select_id').val('SelectFrequency');
	$('#Financial_FarmingOrFishingIncome_id').val(0);
	$('#Financial_FarmingOrFishingIncome_select_id').val('SelectFrequency');
	$('#Financial_AlimonyReceived_id').val(0);
	$('#Financial_AlimonyReceived_select_id').val('SelectFrequency');
	$('#Financial_OtherTypeIncome').val('');
	$('#Financial_OtherIncome_id').val(0);
	$('#Financial_OtherIncome_select_id').val('SelectFrequency');
	/*$('#expectedIncomeByHouseHoldMember').val(0);*/
	$('#explanationTextArea').val('');
	$('#alimonyAmountInput').val(0);
	$('#alimonyFrequencySelect').val('SelectFrequency');
	$('#studentLoanInterestAmountInput').val(0);
	$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
	$('#deductionTypeInput').val('');
	$('#deductionAmountInput').val(0);
	$('#deductionsFrequencySelect').val('SelectFrequency');
	$('#basedOnAmountInput').val(0);
}

// Additional Questions Editing

function appscr75AEditController(i)
{

	//$('#houseHoldTRNo').val('HouseHold' + i);
	setNonFinancialPageProperty(i);
	setHouseHoldData();
	goToPageById("appscr76P1");
	appscr76FromMongodb();
}


//	Edit functionality Conditions calling form navigation.js

function editFunctionalityConditions()
{

	if(currentPage == "appscr75A")
		totalIncomeShowMethod();

	if(editFlag == true && currentPage == "appscr62Part1")
	{
		$('#back_button_div').hide();
	}

	if(editFlag == true && currentPage == "appscr70")
	{
		$('#back_button_div').hide();
	}

}


var updateTo='';
var flagToUpdateFromResult=false;

/*
 * @returns no of Household
 */
function checkStillUpdateRequired()
{
	updateTo = $('input[name=editRadioGroup]:radio:checked').val();
	flagToUpdateFromResult=false;
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	for(var memberId=1; memberId <= noOfHouseHold; memberId++ )
		{
				var check = $('#'+updateTo+memberId).is(':checked');
				if(check)
				{
					$('#finalEditDiv').hide();
				switch(updateTo)
				{
				case "HouseHoldDetailsUpdate":
					flagToUpdateFromResult = true;
					fetchDataHouseholdUpdate(memberId);
					$('#'+updateTo+memberId).prop('checked', false);
					editMemberNumber=memberId;
					goToPageById("appscr60");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				case "nonFinancialUpdate":
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					appscr67EditController(memberId);
					goToPageById("appscr61");
					editMemberNumber=memberId;
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				case "financialDetailsUpdate":
					flagToUpdateFromResult = true;
					$('#'+updateTo+memberId).prop('checked', false);
					appscr74EditController(memberId);
					editMemberNumber=memberId;
					goToPageById("appscr69");
					$('#back_button_div').hide();
					$('#countinue_button_div').show();
					$('#contBtn').show();
					$('#contBtn').html('Next');
					setNonFinancialPageProperty(memberId);
					break;
				}
				break;
			}
		}
	return flagToUpdateFromResult;
}

function fetchDataHouseholdUpdate(targetIndex) {
	var houseHoldIndex = targetIndex;
	getSpouse(houseHoldIndex);
	var martialStatus=$("#appscr57isMarriedTD"+houseHoldIndex).text();
	var spouseID=$("#appscr57spouseIdTD"+houseHoldIndex).text();
	var isPayingJointly=$("#appscr57payJointlyTD"+houseHoldIndex).text();
	var  isTaxFiller=$("#appscr57isTaxFilerTD"+houseHoldIndex).text();
	if(martialStatus.toLowerCase()=="yes"||martialStatus.toLowerCase()=="true")
		{
			$("#marriedIndicatorYes").prop('checked',true);
		}
	else if(martialStatus.toLowerCase()=="no"||martialStatus.toLowerCase()=="false") {
			$("#marriedIndicatorNo").prop('checked',true);
	}
	if(isPayingJointly.toLowerCase()=="yes"||isPayingJointly.toLowerCase()=="true"){
		$("#planOnFilingJointFTRIndicatorYes").prop('checked',true);
	}
	else if(isPayingJointly.toLowerCase()=="no"||isPayingJointly.toLowerCase()=="false") {
		$("#planOnFilingJointFTRIndicatorNo").prop('checked',true);
	}
	if (isTaxFiller.toLowerCase()=="yes"||isTaxFiller.toLowerCase()=="true") {
		$("#planToFileFTRIndicatorYes").prop('checked',true);

	} else if (isTaxFiller.toLowerCase()=="no"||isTaxFiller.toLowerCase()=="false") { {
		$("#planToFileFTRIndicatorNo").prop('checked',true);
	}
	for ( var hNum = 1; hNum <= noOfHouseHold; hNum++) {
		if($("#HouseHold"+hNum).is(":checked")){
			if ($("#HouseHold"+hNum)==spouseID) {
				$("#HouseHold"+hNum).prop('checked',true);
				break;
			}
		}

		}
	}



}
function appscr67Edit(i){
	editNonFinacial = true;
	appscr67EditControllerJSON(i)
}
function appscr67EditControllerJSON(i){
	editMemberNumber = i;
	nonFinancialHouseHoldDone = i;
	//$('#houseHoldTRNo').val('HouseHold' + i);
	saveCurrentApplicantID(i);
	editFlag = false;
	if( i == noOfHouseHoldmember){
		 //editFlag = true;
		 populateDataToRelations();
	}
	if(!haveDataFlag)
	{
		//editFlag = true;
		//$('#back_button_div').hide();
	}

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	var editHouseHoldgender 				= householdMemberObj.gender; //$('#appscrHouseHoldgender57TD'+i).text();
	var editHouseHoldHavingSSN  			= householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator; //$('#appscr57HouseHoldHavingSSNTD'+i).text();
	var editHouseHoldSSN 					= householdMemberObj.socialSecurityCard.socialSecurityNumber; //$('#appscr57HouseHoldSSNTD'+i).text();
	var editsameNameInSSNCardIndicator		= householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator; //$('#appscr57SameNameInSSNCardIndicatorTD'+i).text();
	var editfirstNameOnSSNCard				= householdMemberObj.socialSecurityCard.firstNameOnSSNCard; //$('#appscr57FirstNameOnSSNCardTD'+i).text();
	var editmiddleNameOnSSNCard				= householdMemberObj.socialSecurityCard.middleNameOnSSNCard; //$('#appscr57MiddleNameOnSSNCardTD'+i).text();
	var editlastNameOnSSNCard				= householdMemberObj.socialSecurityCard.lastNameOnSSNCard; //$('#appscr57LastNameOnSSNCardTD'+i).text();
	var editsuffixOnSSNCard					= householdMemberObj.socialSecurityCard.suffixOnSSNCard; //$('#appscr57SuffixOnSSNCardTD'+i).text(); //WE DON'T HAVE SUFFIX IN JSON
	var editnotAvailableSSNReason			= householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN; //$('#appscr57notAvailableSSNReasonTD'+i).text();
	var editUScitizen 						= $('#appscr57UScitizenTD'+i).text(); //WE DON'T HAVE US CITIZEN IN JSON
	var editlivedIntheUSSince1996Indicator 	= householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator; //$('#appscr57livedIntheUSSince1996IndicatorTD'+i).text();
	var editnaturalizedCitizenshipIndicator	= householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator; //$('#appscr57naturalizedCitizenshipIndicatorTD'+i).text();
	var editcertificateType 				= $('#appscr57certificateTypeIndicatorTD'+i).text(); //NO DATA FOUND IN JSON
	var editcertificateAlignNumber 			= householdMemberObj.citizenshipImmigrationStatus.naturalizationCertificateAlienNumber; //$('#appscr57naturalizedCitizenshipAlignNumberTD'+i).text();
	var editcertificateNumber 				= householdMemberObj.citizenshipImmigrationStatus.naturalizationCertificateNaturalizationNumber; //$('#appscr57certificateNumberTD'+i).text();
	var editaliegnNumber 					= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber; //$('#appscr57alienNoTD'+i).text();
	var editi94Number 						= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number; //$('#appscr57i94NoTD'+i).text();
	var editForeignPassportNumber 			= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber; //$('#appscr57foreignPassNoTD'+i).text();
	var editForeignPassportCountry			= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance; //$('#appscr57foreignPassCountryTD'+i).text();
	var editForeignPassportExpiration		= $('#appscr57foreignPassExpirationTD'+i).text(); //NO DATA FOUND IN JSON
	var editSevisID 						= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.sevisId; //$('#appscr57sevisIDTD'+i).text();
	var editdocumentSameNameIndicator 		= $('#appscr57documentSameNameIndicatorTD'+i).text(); //NO DATA FOUND IN JSON
	var editdocumentFirstName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName; //$('#appscr57documentFirstNameTD'+i).text();
	var editdocumentLastName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName; //$('#appscr57documentMiddleNameTD'+i).text();
	var editdocumentMiddleName 				= householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName; //$('#appscr57documentLastNameTD'+i).text();
	var editdocumentSuffix 					= $('#appscr57documentSuffixTD'+i).text(); //NO DATA FOUND IN JSON
	var editethnicityIndicator				=  householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator;// $('#appscr57EthnicityIndicatorTD'+i).text();
	temp = householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator;
	if(temp == "true" || temp == true){
		$('#fnlnsExemptionIndicatorYes').prop("checked", true);
	} else if(temp == "false" || temp == false) {
		$('#fnlnsExemptionIndicatorNo').prop("checked", true);
	}
	$('#exemptionId').val(householdMemberObj.socialSecurityCard.individualManadateExceptionID);
	/*var ssn1 = editHouseHoldSSN.split("-")[0];
	var ssn2 = editHouseHoldSSN.split("-")[1];
	var ssn3 = editHouseHoldSSN.split("-")[2];*/
	var houseHoldname = householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName; //$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
	resetAddNonFinancialDetails();
	$('.nameOfHouseHold').text(houseHoldname);
	if(editHouseHoldgender != "" && editHouseHoldgender != undefined){
		if(editHouseHoldgender.toLowerCase() == 'male')
		{
			$('#appscr61GenderMaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
		else if(editHouseHoldgender.toLowerCase() == 'female')
		{
			$('#appscr61GenderFemaleID').prop('checked',true);
			apppscr60SetGenderInSSAP();

		}
	}
	if(editHouseHoldHavingSSN == 'yes' || editHouseHoldHavingSSN == 'true' || editHouseHoldHavingSSN == true )
	{
		$('#socialSecurityCardHolderIndicatorYes').prop('checked',true);
		askForSSNNoPage61();
	}
	else if(editHouseHoldHavingSSN == 'no' || editHouseHoldHavingSSN == 'false' || editHouseHoldHavingSSN === false )
	{
		$('#socialSecurityCardHolderIndicatorNo').prop('checked',true);

		validation('appscr61');
		askForSSNNoPage61();
	}

	/*$('#ssn1').val(ssn1);
	$('#ssn2').val(ssn2);
	$('#ssn3').val(ssn3);*/
	//if 2nd applicant changes ssn and click back, data-ssn is updated due to focusout, then user goes back to 1st applicant, and continue without edit ssn. 1st applicant's ssn will be updated to data-ssn.
	if(editHouseHoldSSN){
		//$("#ssn").attr('data-ssn',editHouseHoldSSN).val("***-**-" + editHouseHoldSSN.substring(5));
		$("#ssn").val("***-**-" + editHouseHoldSSN.substring(5));
	}

	if(editsameNameInSSNCardIndicator == "yes" || editsameNameInSSNCardIndicator == 'true' || editsameNameInSSNCardIndicator == true )
		$('#fnlnsSameIndicatorYes').prop("checked", true);
	else if(editsameNameInSSNCardIndicator == "no" || editsameNameInSSNCardIndicator == 'false' || editsameNameInSSNCardIndicator === false )
	{

		$('#fnlnsSameIndicatorNo').prop("checked", true);
		$('#firstNameOnSSNCard').val(editfirstNameOnSSNCard);
		$('#middleNameOnSSNCard').val(editmiddleNameOnSSNCard);
		$('#lastNameOnSSNCard').val(editlastNameOnSSNCard);
		$('#suffixOnSSNCard').val(editsuffixOnSSNCard);
	}

	hasSameSSNNumberP61();

	if(editnotAvailableSSNReason == 0)
		$('#reasonableExplanationForNoSSN').val('');
	else
		$('#reasonableExplanationForNoSSN').val(editnotAvailableSSNReason);

	if(editUScitizen == 'no')
	{
		$('#UScitizenIndicatorNo').prop('checked',true);
		isUSCitizen();
	}
	else if(editUScitizen == 'yes')
	{
		$('#UScitizenIndicatorYes').prop('checked',true);
		isUSCitizen();
	}

	if(editlivedIntheUSSince1996Indicator == 'yes')
		$('#62Part_livedIntheUSSince1996IndicatorYes').prop('checked',true);
	else
		$('#62Part_livedIntheUSSince1996Indicator').prop('checked',true);
	$('input[name=naturalizedCitizenshipIndicator]:radio:checked').prop("checked", false);
	if(editnaturalizedCitizenshipIndicator == true){
		$('#naturalizedCitizenshipIndicatorYes').prop('checked',true);
	}else if(editnaturalizedCitizenshipIndicator == false){
		$('#naturalizedCitizenshipIndicatorNo').prop('checked',true);
	}

	if(editcertificateType == 'CitizenshipCertificate')
	{
		$('#naturalizedCitizenNaturalizedIndicator2').prop('checked',true);
		$('#citizenshipAlignNumber').val(editcertificateAlignNumber);
		$('#appscr62p1citizenshipCertificateNumber').val(editcertificateNumber);

		$(".appscr62Part1HideandShowFillingDetails").eq(1).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(0).hide();
	}
	else
	{
		$('#naturalizedCitizenNaturalizedIndicator').prop('checked',true);
		$('#naturalizationAlignNumber').val(editcertificateAlignNumber);
		$('#naturalizationCertificateNumber').val(editcertificateNumber);

		$(".appscr62Part1HideandShowFillingDetails").eq(0).show();
		$(".appscr62Part1HideandShowFillingDetails").eq(1).hide();
	}

	$('#naturalCitizen_eligibleImmigrationStatus').prop('checked');


	if(editaliegnNumber!=" "){
		$('#DocAlignNumber').val(editaliegnNumber);
	}
	if(editi94Number!=" "){
		$('#DocI-94Number').val(editi94Number);
	}
	if(editForeignPassportNumber!=" "){
		$('#passportDocNumber').val(editForeignPassportNumber);
	}
	if(editForeignPassportCountry!="0"){
		$('#appscr62P1County').val(editForeignPassportCountry);
	}
	if(editForeignPassportExpiration!="0"){
		$('#passportExpDate').val(editForeignPassportExpiration);
	}
	if(editSevisID!="0"){
		$('#sevisIDNumber').val(editSevisID);
	}
	if(editdocumentSameNameIndicator == 'no')
	{
		$('#62Part2_UScitizen').prop('checked',true);
		$('#documentFirstName').val(editdocumentFirstName);
		$('#documentMiddleName').val(editdocumentMiddleName);
		$('#documentLastName').val(editdocumentLastName);
		$('#documentSuffix').val(editdocumentSuffix);

		displayOrHideSameNameInfoPage62();
	}
	else
	{
		$('#62Part2_UScitizenYes').prop('checked',true);
		$('#documentFirstName').val('');
		$('#documentMiddleName').val('');
		$('#documentLastName').val('');
		$('#documentSuffix').val('0');

		displayOrHideSameNameInfoPage62();
	}

	if(editethnicityIndicator == true){
		$('#checkPersonNameLanguageYes').prop("checked", true);

		for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.ethnicity.length; cnt++){
			$('input:checkbox[name="ethnicity"]').each(function(){
				if($(this).val() == householdMemberObj.ethnicityAndRace.ethnicity[cnt].code){

					$(this).prop('checked',true);
					//for other ethnicity
					if($(this).val() == '0000-0'){
						$('#otherEthnicity').val(householdMemberObj.ethnicityAndRace.ethnicity[cnt].label);
					}
				}
			});
		}

	}else if(editethnicityIndicator == false){
		$('#checkPersonNameLanguageNo').prop("checked", true);
	}
	displayOtherEthnicity();
	checkLanguage();

	for(var cnt=0; cnt<householdMemberObj.ethnicityAndRace.race.length; cnt++){
		$('input:checkbox[name="race"]').each(function(){
			if($(this).val() == householdMemberObj.ethnicityAndRace.race[cnt].code){

				$(this).prop('checked',true);
				//for other race
				if($(this).val() == '2131-1'){
					$('#otherRace').val(householdMemberObj.ethnicityAndRace.race[cnt].label);
				}
			}
		});
	}
	displayOtherRace();
	if(!haveDataFlag)
		goToPageById('appscr61');
}

function appscr74EditControllerJSON(i)
{
	editMemberNumber = i;
	if(editMemberNumber =="" || editMemberNumber < 0){
		return;
	}
	//$('#houseHoldTRNo').val("HouseHold"+i);
	saveCurrentApplicantID(i);
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[editMemberNumber-1];

	if(!haveDataFlag)
	{
		editFlag = true;
		//$('#back_button_div').hide();
	}

	//$('input[name=appscr70radios4Income]:radio:checked').val()

	var editIrsIncome 							  = $('#appscrirsIncome57TD'+i).text();
	var editIncomesStatus 						  = $('#incomesStatus'+i).text();
	var editJobIncome 							  = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	var editJobFrequency 						  = householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency;
	var editEmployerName						  = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	var editHoursOrDays							  = householdMemberObj.detailedIncome.jobIncome[0].workHours;
	var editSelfEmploymentIncome 				  = householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome;
	var editEmploymentTypeOfWork				  = householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork;
	var editSocialSecurityBenefitsIncome 		  = householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount;
	var editSocialSecurityBenefitsIncomeFrequency = householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency;
	var editUnemploymentIncome 					  = householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount;
	var editUnemploymentIncomeFrequency 		  = householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency;
	var editUnemploymentStateGovernment			  = householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName;
	var editunemploymentIncomeDate				  = UIDateformat(householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate);
	var editRetirementPensionIncome 			  = householdMemberObj.detailedIncome.retirementOrPension.taxableAmount;
	var editRetirementPensionIncomeFrequency 	  = householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency;
	var editCapitalgainsIncome 					  = householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains;
	var editInvestmentIncome 					  = householdMemberObj.detailedIncome.investmentIncome.incomeAmount;
	var editInvestmentIncomeFrequency 			  = householdMemberObj.detailedIncome.investmentIncome.incomeFrequency;
	var editRentalOrRoyaltyIncome 				  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount;
	var editRentalOrRoyaltyIncomeFrequency 		  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency;
	var editFarmingOrFishingIncome 				  = householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount;
	var editFarmingOrFishingIncomeFrequency 	  = householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency;
	var editAlimonyReceivedIncome 				  = householdMemberObj.detailedIncome.alimonyReceived.amountReceived;
	var editAlimonyReceivedIncomeFrequency 		  = householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	var editOtherTypeIncome						  = $('#appscrFinancial_OtherIncomeOtherTypeIncome57TD'+i).text();

	//var editExpectedIncome 						  = $('#appscrexpectedIncome57TD'+i).text();
	//var editIrsHouseHoldExplanationTextArea 	  = $('#appscr57HouseHoldExplanationTextAreaTD'+i).text();

	var editHouseHoldalimonyAmount 				  = householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount;
	var editHouseHoldalimonyFrequency 			  = householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency;
	var editHouseHoldstudentLoanAmount 			  = householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount;
	var editHouseHoldstudentLoanFrequency 		  = householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency;
	var editHouseHolddeductionsType 			  = householdMemberObj.detailedIncome.deductions.deductionType[2];
	var editHouseHolddeductionsAmount 			  = householdMemberObj.detailedIncome.deductions.otherDeductionAmount;
	var editHouseHolddeductionsFrequency 		  = householdMemberObj.detailedIncome.deductions.otherDeductionFrequency;
	var editHouseHoldbasedOnInput 				  = $('#appscr57HouseHoldbasedOnInputTD'+i).text();
	var editstopWorking							  = householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator;;
	var editworkAt								  =	householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator;
	var editdecresedAt							  = householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator;
	var editsalaryAt							  = householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator;
	var editsesWorker							  = householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator;
	var editotherJointIncome					  = householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator;
	var incomeStatusFlag;

	resetPage69Values();
	resetPage71Values();
	resetPage72DeductionValues();

	var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	//var houseHoldname = $('#appscr57FirstNameTD'+editMemberNumber).text()+" "+$('#appscr57MiddleNameTD'+editMemberNumber).text()+" "+$('#appscr57LastNameTD'+editMemberNumber).text();
	  $('.nameOfHouseHold').text(houseHoldname);
	  job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	  $('.appscr73EmployerName').html(job_income_name);

	  if(currentPage == "appscr68" || currentPage == "appscr69"){
		  appscr69JSON(editMemberNumber);
		  //$('#expeditedIncome69').val(householdMemberObj.expeditedIncome.irsReportedAnnualIncome);
	  }
	  editIncomesStatus = "Yes";
	//if(editIncomesStatus == "Yes" || editIncomesStatus == "yes")
	if(currentPage == "appscr70" || currentPage == "appscr72")
	{

		$('#appscr70radios4IncomeYes').prop("checked",true);
		//selectIncomesCheckBoxes();

		//if(editJobFrequency != "SelectFrequency")
		if(editJobIncome == null || editJobIncome == "")
		{
			$('#Financial_Job_EmployerName_id').val('');
			$('#Financial_Job_HoursOrDays_id').val('');
			if( $('#Financial_Job').is(':checked') == false)
				$('#Financial_Job_Div').hide();
			else
				$('#Financial_Job_Div').show();

		}
		else
		{
			incomeStatusFlag = true;

			//document.getElementById('Financial_Job').checked = true;
			$('#Financial_Job_EmployerName_id').val(editEmployerName);
			$('#Financial_Job_HoursOrDays_id').val(editHoursOrDays);
			$('#Financial_Job_id').val(Moneyformat(editJobIncome));
			$('#Financial_Job_select_id').val(editJobFrequency);
			showDiductionsFields();

		}
		//if(editSelfEmploymentIncome != "0" && editSelfEmploymentIncome != "0.0")
		if(editSelfEmploymentIncome == null || editSelfEmploymentIncome == "" )
		{
			//document.getElementById('Financial_Self_Employment').checked = false;
			$('#Financial_Self_Employment_TypeOfWork').val('');
			$('#Financial_Self_Employment_Div').hide();
			if( $('#Financial_Self_Employment').is(':checked') == false)
				$('#Financial_Self_Employment_Div').hide();
			else
				$('#Financial_Self_Employment_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Self_Employment').checked = true;
			$('#Financial_Self_Employment_TypeOfWork').val(editEmploymentTypeOfWork);
			$('#Financial_Self_Employment_id').val(Moneyformat(editSelfEmploymentIncome));

		}
		//if(editSocialSecurityBenefitsIncomeFrequency != "SelectFrequency")
		if(editSocialSecurityBenefitsIncome == null || editSocialSecurityBenefitsIncome == "")
		{
			//document.getElementById('Financial_SocialSecuritybenefits').checked = false;
			$('#Financial_SocialSecuritybenefits_Div').hide();
			if( $('#Financial_SocialSecuritybenefits').is(':checked') == false)
				$('#Financial_SocialSecuritybenefits_Div').hide();
			else
				$('#Financial_SocialSecuritybenefits_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_SocialSecuritybenefits').checked = true;
			$('#Financial_SocialSecuritybenefits_id').val(Moneyformat(editSocialSecurityBenefitsIncome));
			$('#Financial_SocialSecuritybenefits_select_id').val(editSocialSecurityBenefitsIncomeFrequency);
		}

		//if(editUnemploymentIncomeFrequency != "SelectFrequency")
		if(editUnemploymentIncome == null || editUnemploymentIncome == "")
		{

			//document.getElementById('Financial_Unemployment').checked = false;
			$('#Financial_Unemployment_StateGovernment').val('');
			$('#unemploymentIncomeDate').val('');
			if( $('#Financial_Unemployment').is(':checked') == false)
				$('#Financial_Unemployment_Div').hide();
			else
				$('#Financial_Unemployment_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Unemployment').checked = true;
			$('#Financial_Unemployment_StateGovernment').val(editUnemploymentStateGovernment);
			$('#Financial_Unemployment_id').val(Moneyformat(editUnemploymentIncome));
			$('#unemploymentIncomeDate').val(editunemploymentIncomeDate);
			$('#Financial_Unemployment_select_id').val(editUnemploymentIncomeFrequency);
		}

		//if(editRetirementPensionIncomeFrequency != "SelectFrequency")
		if(editRetirementPensionIncome == null || editRetirementPensionIncome == "")
		{

			//document.getElementById('Financial_Retirement_pension').checked = false;
			if( $('#Financial_Retirement_pension').is(':checked') == false)
				$('#Financial_Retirement_pension_Div').hide();
			else
				$('#Financial_Retirement_pension_Div').show();

		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Retirement_pension').checked = true;
			$('#Financial_Retirement_pension_id').val(Moneyformat(editRetirementPensionIncome));
			$('#Financial_Retirement_pension_select_id').val(editRetirementPensionIncomeFrequency);
		}

		//if(editCapitalgainsIncome != "0" && editCapitalgainsIncome != "0.0")
		if(editCapitalgainsIncome == null || editCapitalgainsIncome == "")
		{
			//document.getElementById('Financial_Capitalgains').checked = false;
			if( $('#Financial_Capitalgains').is(':checked') == false)
				$('#Financial_Capitalgains_Div').hide();
			else
				$('#Financial_Capitalgains_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_Capitalgains').checked = true;
			$('#Financial_Capitalgains_id').val(Moneyformat(editCapitalgainsIncome));
		}

		//if(editInvestmentIncomeFrequency != "SelectFrequency")
		if(editInvestmentIncome == null || editInvestmentIncome == "")
		{
			//document.getElementById('Financial_InvestmentIncome').checked = false;
			if( $('#Financial_InvestmentIncome').is(':checked') == false)
				$('#Financial_InvestmentIncome_Div').hide();
			else
				$('#Financial_InvestmentIncome_Div').show();

		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_InvestmentIncome').checked = true;
			$('#Financial_InvestmentIncome_id').val(Moneyformat(editInvestmentIncome));
			$('#Financial_InvestmentIncome_select_id').val(editInvestmentIncomeFrequency);
		}

		//if(editRentalOrRoyaltyIncomeFrequency != "SelectFrequency")
		if(editRentalOrRoyaltyIncome == null || editRentalOrRoyaltyIncome == "")
		{
			//document.getElementById('Financial_RentalOrRoyaltyIncome').checked = false;
			if( $('#Financial_RentalOrRoyaltyIncome').is(':checked') == false)
				$('#Financial_RentalOrRoyaltyIncome_Div').hide();
			else
				$('#Financial_RentalOrRoyaltyIncome_Div').show();
		}
		else
		{

			incomeStatusFlag = true;
			//document.getElementById('Financial_RentalOrRoyaltyIncome').checked = true;
			$('#Financial_RentalOrRoyaltyIncome_id').val(Moneyformat(editRentalOrRoyaltyIncome));
			$('#Financial_RentalOrRoyaltyIncome_select_id').val(editRentalOrRoyaltyIncomeFrequency);
		}

		//if(editFarmingOrFishingIncomeFrequency != "SelectFrequency")
		if(editFarmingOrFishingIncome == null || editFarmingOrFishingIncome == "")
		{
			//document.getElementById('Financial_FarmingOrFishingIncome').checked = false;
			$('#Financial_FarmingOrFishingIncome_Div').hide();
			if( $('#Financial_FarmingOrFishingIncome').is(':checked') == false)
				$('#Financial_FarmingOrFishingIncome_Div').hide();
			else
				$('#Financial_FarmingOrFishingIncome_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_FarmingOrFishingIncome').checked = true;
			$('#Financial_FarmingOrFishingIncome_id').val(Moneyformat(editFarmingOrFishingIncome));
			$('#Financial_FarmingOrFishingIncome_select_id').val(editFarmingOrFishingIncomeFrequency);
		}

		//if(editAlimonyReceivedIncomeFrequency != "SelectFrequency")
		if(editAlimonyReceivedIncome == null || editAlimonyReceivedIncome == "")
		{
			//document.getElementById('Financial_AlimonyReceived').checked = false;
			if( $('#Financial_AlimonyReceived').is(':checked') == false)
				$('#Financial_AlimonyReceived_Div').hide();
			else
				$('#Financial_AlimonyReceived_Div').show();
		}
		else
		{
			incomeStatusFlag = true;
			//document.getElementById('Financial_AlimonyReceived').checked = true;
			$('#Financial_AlimonyReceived_id').val(Moneyformat(editAlimonyReceivedIncome));
			$('#Financial_AlimonyReceived_select_id').val(editAlimonyReceivedIncomeFrequency);
		}

		//var otherincome = false;
		$('#Financial_OtherIncome_Div').hide();
		if (householdMemberObj.detailedIncome.otherIncomeIndicator == true){
			$('#Financial_OtherIncome_Div').show();
		}
		$('#appscr71CanceleddebtsCB').prop("checked", false);
		deductionCheck('appscr71CanceleddebtsCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[0].incomeAmount == "")){
			$('#appscr71CanceleddebtsCB').prop("checked", true);
			$('#cancelled_Debts_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount));
			$('#cancelledDebtsFrequency').val(householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71CanceleddebtsCB');
		}
		$('#appscr71CourtAwardsCB').prop("checked", false);
		deductionCheck('appscr71CourtAwardsCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[1].incomeAmount == "")){
			$('#appscr71CourtAwardsCB').prop("checked", true);
			$('#court_awards_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount));
			$('#courtAwrdsFrequency').val(householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71CourtAwardsCB');
		}
		$('#appscr71JuryDutyPayCB').prop("checked", false);
		deductionCheck('appscr71JuryDutyPayCB');
		if(!(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount == null || householdMemberObj.detailedIncome.otherIncome[2].incomeAmount == "")){
			$('#appscr71JuryDutyPayCB').prop("checked", true);
			$('#jury_duty_pay_amount').val(Moneyformat(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount));
			$('#juryDutyPayFrequency').val(householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency);
			$('#Financial_OtherIncome_Div').show();
			deductionCheck('appscr71JuryDutyPayCB');
		}

	}
	else
	{
		//$('#appscr70radios4IncomeNo').prop("checked",true);
		//selectIncomesCheckBoxes();

		//hideDiductionsFields();
	}

	if(incomeStatusFlag == false)
	{
		$('#appscr70radios4IncomeNo').prop("checked",true);
		selectIncomesCheckBoxes();

		hideDiductionsFields();
	}

	resetPage72DeductionValues();
	$('#appscr72alimonyPaid').prop("checked",false);
	$('#appscr72alimonyPaidTR').hide();
	if(editHouseHoldalimonyAmount != "" && editHouseHoldalimonyAmount != null)
	{
		$('#appscr72alimonyPaid').prop("checked",true);
		//document.getElementById('appscr72alimonyPaid').checked = true;
		$('#alimonyAmountInput').val(Moneyformat(editHouseHoldalimonyAmount));
		$('#alimonyFrequencySelect').val(editHouseHoldalimonyFrequency);
		$('#appscr72alimonyPaidTR').show();
	}
	$('#appscr72studentLoanInterest').prop("checked",false);
	$('#appscr72studentLoanInterestTR').hide();
	if(editHouseHoldstudentLoanAmount != "" && editHouseHoldstudentLoanAmount != null)
	{
		$('#appscr72studentLoanInterest').prop("checked",true);
		//document.getElementById('appscr72studentLoanInterest').checked = true;
		$('#studentLoanInterestAmountInput').val(Moneyformat(editHouseHoldstudentLoanAmount));
		$('#studentLoanInterestFrequencySelect').val(editHouseHoldstudentLoanFrequency);
		$('#appscr72studentLoanInterestTR').show();
	}
	$('#appscr72otherDeduction').prop("checked",false);
	$('#appscr72otherDeductionTR').hide();
	if(editHouseHolddeductionsAmount != "" && editHouseHolddeductionsAmount != null)
	{
		$('#appscr72otherDeduction').prop("checked",true);
		//document.getElementById('appscr72otherDeduction').checked = true;
		$('#deductionTypeInput').val(editHouseHolddeductionsType);
		$('#deductionAmountInput').val(Moneyformat(editHouseHolddeductionsAmount));
		$('#deductionsFrequencySelect').val(editHouseHolddeductionsFrequency);
		$('#appscr72otherDeductionTR').show();
	}

	if(householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator == true){
		$('#aboutIncomeSppedYes').prop("checked",true);
		appscr72incomeHighOrLow();
	} else if(householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator == false){
		$('#aboutIncomeSppedNo').prop("checked",true);
		appscr72incomeHighOrLow();
	}

	if(householdMemberObj.expeditedIncome.expectedIncomeVaration == 'higher'){
		$('#incomeVariationHigh').prop("checked",true);
	} else if(householdMemberObj.expeditedIncome.expectedIncomeVaration == 'lower'){
		$('#incomeVariationLow').prop("checked",true);
	}


	if(editHouseHoldbasedOnInput !="0" && editHouseHoldbasedOnInput != "0.0")
	{
		$('#basedOnAmountInput').val(editHouseHoldbasedOnInput);
	}
	if(editdecresedAt == "Yes" || editdecresedAt == "yes"){
		$('#appscr73DecresedAtYes').prop("checked",true);
	}
	else{
		$('#appscr73DecresedAtNo').prop("checked",true);
	}

	if(editstopWorking == true){
		$('#appscr73stopWorkingIndicatorYes').prop("checked",true);
	}
	else if(editstopWorking == false) {
		$('#appscr73stopWorkingIndicatorNo').prop("checked",true);
	}

	if(editsalaryAt == true){
		$('#appscr73SalaryAtYes').prop("checked",true);
	}else if(editsalaryAt == false){
		$('#appscr73SalaryAtNo').prop("checked",true);
	}
	if(editsesWorker == true){
		$('#appscr73SesWorkerYes').prop("checked",true);
	}else if(editsesWorker == false){
		$('#appscr73SesWorkerNo').prop("checked",true);
	}
	if(editotherJointIncome == true){
		$('#appscr73OtherJointIncomeYes').prop("checked",true);
	}else if(editotherJointIncome == false){
		$('#appscr73OtherJointIncomeNo').prop("checked",true);
	}
	$('#explanationTextArea').val(householdMemberObj.detailedIncome.discrepancies.explanationForJobIncomeDiscrepancy);
	$('#appscr73ExplanationTaxtArea').val(householdMemberObj.detailedIncome.discrepancies.explanationForDependantDiscrepancy);


}

function appscr69JSON(i){
	editMemberNumber = i;
	if(i =="" || i < 0){
		return;
	}
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	$('#expeditedIncome69').val(Moneyformat(householdMemberObj.expeditedIncome.irsReportedAnnualIncome));
 }

function updatePrimaryContact(){
	$('#firstName').val($('#appscr57FirstName1').val());
	$('#middleName').val($('#appscr57MiddleName1').val());
	$('#lastName').val($('#appscr57LastName1').val());
	$('#appscr53Suffix').val($('#appscr57Suffix1').val());
	$('#dateOfBirth').val($('#appscr57DOB1').val());

	$('#appscr57FirstNameTD1').text($('#firstName').val());
	$('#appscr57MiddleNameTD1').text($('#middleName').val());
	$('#appscr57LastNameTD1').text($('#lastName').val());
	$('#appscr57SuffixTD1').text($('#appscr53Suffix').val());
	$('#appscr57DOBTD1').text($('#dateOfBirth').val());

	var name= $('#firstName').val() + " " + $('#middleName').val() + " " + $('#lastName').val();

	$('#houseHold_Contact_span1').html(name);
	$('#houseHold_Contact_span2').html(name);
	$('#houseHold_Contact_span3').html(name);
}

function editFinancialSectionPerson(personId) {
	editData = true;
	if(editData == true) {
		$('#prevBtn').hide();
	}


	var hosueholdSize = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	var personObject = null;
	for(var i=0; i<hosueholdSize; i++) {
		var person = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
		if(person.personId == personId) {
			currentPersonIndex = i;
			personObject = person;
			//break;
		}
	}

	if(personObject == null) {
		return false;
	}
	//var personObject = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[1];
	$('#expeditedIncome69').val(personObject.expeditedIncome.irsReportedAnnualIncome);
	var lastName = personObject.name.lastName;
	$('.nameOfHouseHold').text(lastName);

	$('#' + currentPage).hide();
	pageId = "appscr69";

	$('#' + pageId).show();

	currentPage = pageId;
}

function updateFullNameOfOtherChild(){
	$(".fullNameOfOtherChild").text($("#parent-or-caretaker-firstName").val() + " " + $("#parent-or-caretaker-middleName").val() + " " + $("#parent-or-caretaker-lastName").val());
}

function updateLiveWithChildUnderAgeView(){
	$("#liveWithChildUnderAgeView").html("");

	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		var dobArr =UIDateformat(householdMemberObj.dateOfBirth);
		if(ageUnder19(dobArr)){
			fullName = householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;

			$("#liveWithChildUnderAgeView").append('<label><input type="checkbox" name="takeCareOf" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#takeCareOf_anotherChildErrorDiv" />'+ fullName +'</label>');
		}
	}

	$("#liveWithChildUnderAgeView").append('<label><input type="checkbox" id="takeCareOf_anotherChild" name="takeCareOf" value="Another child" onclick="method4AnotherChildCheckBox();" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#takeCareOf_anotherChildErrorDiv" />'+jQuery.i18n.prop("ssap.page13.anotherChild")+'</label><div id="takeCareOf_anotherChildErrorDiv"></div>');

}

function hideParentOrCaretaker(){
	//var currentApplicantID = ($('#houseHoldTRNo').val()).substring(9);
	var currentApplicantID = getCurrentApplicantID();
	//var currentApplicantDOB = $("#HouseHold"+currentApplicantID).find(".dob").text();
	var currentApplicantDOB = UIDateformat(webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentApplicantID-1].dateOfBirth);
	if(ageUnder19(currentApplicantDOB)){
		return true;
	}else{
		return false;
	}


}

function ageUnder19(dateOfBirth){
    var birthDate = new Date(dateOfBirth);
    var effectiveDate = getEffectiveDate();;

    var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

    if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
        years--;
    }

    if(years >=19){
    	return false;
    }else{
    	return true;
    }
 }

function editFinancialSectionPerson(personId) {
	editData = true;
	if(editData == true) {
		$('#prevBtn').hide();
	}
}

function maskSSN(){
	var pattern = /^([1-57-8][0-9]{2}|0([1-9][0-9]|[0-9][1-9])|6([0-57-9][0-9]|[0-9][0-57-9]))([1-9][0-9]|[0-9][1-9])([1-9]\d{3}|\d[1-9]\d{2}|\d{2}[1-9]\d|\d{3}[1-9])$/;
	var ssnValue = $('#ssn').val().replace(/-/g,'');
	if(pattern.test(ssnValue)){
		$('#ssn').closest('div.controls').find('.errorsWrapper').hide();
		var currentID = getCurrentApplicantID();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
		householdMemberObj.socialSecurityCard.socialSecurityNumber = ssnValue;
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;
		//$('#ssn').attr('data-ssn',ssnValue).val("***-**-" + ssnValue.substring(5));
		$('#ssn').val("***-**-" + ssnValue.substring(5));
	}
}

function ageGreaterThan26(dateOfBirth){
    var birthDate = new Date(dateOfBirth);

    var effectiveDate = getEffectiveDate();

    var years = (effectiveDate.getFullYear() - birthDate.getFullYear());

    if (effectiveDate.getMonth() < birthDate.getMonth() || effectiveDate.getMonth() == birthDate.getMonth() && effectiveDate.getDate() < birthDate.getDate()) {
        years--;
    }

    if(years >=26){
    	return true;
    }else{
    	return false;
    }
 }

function coverageMinimumCheckWithUnder26Indicator(){
	var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	for(var i = 0; i < householdMemberArr.length; i++){
		if(householdMemberArr[i].applyingForCoverageIndicator == true && householdMemberArr[i].under26Indicator == true){
			return true;
		};
	};

	return false;

}

function coverageMinimumCheck(){
	var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
	for(var i = 0; i < householdMemberArr.length; i++){
		if(householdMemberArr[i].applyingForCoverageIndicator == true){
			return true;
		};
	};

	return false;

}


function addJob(){
	var newJob = '';

	//var jobArrLength = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[financialHouseHoldDone-1].detailedIncome.jobIncome.length;

	newJob += '<div class="jobType"><hr><div class="gutter10">';
		newJob += '<strong class="pull-right margin5-r"><a id="removeJob" class="btn" onclick="removeJob($(this));">'+jQuery.i18n.prop('ssap.page21.removeJob')+'</a></strong>';

		newJob += '<div class="control-group">';
			newJob += '<label class="control-label">'+jQuery.i18n.prop('ssap.page21.employerName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></label>';
			newJob += '<div class="controls">';
				newJob += '<input type="text" placeholder="Employer Name" class="input-medium income_employerName"  data-parsley-length="[8, 256]" data-parsley-length-message="'+jQuery.i18n.prop('ssap.page21.employerNameValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.employerNameRequired')+'"/>';
			newJob += '</div>';
		newJob += '</div>';

		newJob += '<div class="control-group">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howMuch')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.paid')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<label class="control-label">'+jQuery.i18n.prop('ssap.page21.amount')+' <strong>$</strong></label>';
			newJob += '<div class="controls">';
				newJob += '<input name="" type="text" placeholder="Dollar Amount" class="input-medium currency income_amount" data-parsley-pattern="^[0-9\\,\\.]{1,18}$" data-parsley-pattern-message="'+jQuery.i18n.prop('ssap.page21.amountValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.amountRequired')+'" />';
				newJob += '</div>';
		newJob += '</div>';

		newJob += '<div class="control-group">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howOften')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.paidAmount')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<div class="controls">';
				newJob += '<select class="input-large income_frequency" onchange="howOftenGetThisAmount($(this))" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.required')+'">';
					newJob += '<option value="">'+jQuery.i18n.prop('ssap.page21.selectFrequency')+'</option>';
					newJob += '<option value="Hourly">'+jQuery.i18n.prop('ssap.page21.hourly')+'</option>';
					newJob += '<option value="Daily">'+jQuery.i18n.prop('ssap.page21.daily')+'</option>';
					newJob += '<option value="Weekly">'+jQuery.i18n.prop('ssap.page21.weekly')+'</option>';
					newJob += '<option value="EveryTwoWeeks">'+jQuery.i18n.prop('ssap.page21.biWeekly')+'</option>';
					newJob += '<option value="TwiceAMonth">'+jQuery.i18n.prop('ssap.page21.twiceMonth')+'</option>';
					newJob += '<option value="Monthly">'+jQuery.i18n.prop('ssap.page21.monthly')+'</option>';
					newJob += '<option value="Yearly">'+jQuery.i18n.prop('ssap.page21.yearly')+'</option>';
					newJob += '<option value="OneTimeOnly">'+jQuery.i18n.prop('ssap.page21.oneTime')+'</option>';
				newJob += '</select>';
			newJob += '</div>';
		newJob += '</div>';

		newJob += '<div class="control-group hide income_frequencyDiv">';
			newJob += '<p>'+jQuery.i18n.prop('ssap.page21.howMuch')+' <strong class="nameOfHouseHold"></strong> '+jQuery.i18n.prop('ssap.page21.workPerWeek')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';
			newJob += '<div class="controls">';
				newJob += '<input type="text" placeholder="Hours Or Days per week" class="input-medium income_hoursOrDays" data-parsley-pattern-message="'+jQuery.i18n.prop('ssap.page21.workPerWeekValid')+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop('ssap.page21.required')+'"/>';
			newJob += '</div>';
		newJob += '</div>';

	newJob += '</div></div>';

	$('#moreJobDiv').append(newJob);

	$('.currency').mask('000,000,000,000.00', {reverse: true});
}




function removeJob(jobDiv){
	jobDiv.closest('div.jobType').remove();
}

function getEffectiveDate(){
	//application full date
	var applicationDate = new Date();
	//application day
	var applicationDay = applicationDate.getDate();

	//effective month, this will only be used in 15 day logic
	var effectiveMonth = applicationDate.getMonth();
	//effective year from server side
	var effectiveYear = $("#coverageYear").val();

	var applicationType = $("#applicationType").val();

	/*if(applicationType === 'OE'){ //OEP
		if(applicationDate.getFullYear() < effectiveYear){
			effectiveDate =  new Date('01/01/' + effectiveYear);
		}else{

		    effectiveDate = get15EffectiveDate(applicationDay, effectiveMonth, effectiveYear);
		}

	}else{//QEP

	    effectiveDate = get15EffectiveDate(applicationDay, effectiveMonth, effectiveYear);
	}*/


	if(applicationType === 'OE' && applicationDate.getFullYear() < effectiveYear){
		effectiveDate =  new Date('01/01/' + effectiveYear);
	}else{
		effectiveDate = get15EffectiveDate(applicationDay, effectiveMonth, effectiveYear);
	}

	return effectiveDate;
}


function get15EffectiveDate(applicationDay, effectiveMonth, effectiveYear){
	if(applicationDay <= 15){
        //getMonth starts from 0
        effectiveMonth += 2;
    }else{
        effectiveMonth += 3;
    }

    if(effectiveMonth > 12){
        effectiveMonth -= 12;
        effectiveYear++;
    }

    return new Date(effectiveMonth + "/01/" + effectiveYear);
}

function processApplicantDeletion(applicantGuidToBeRemoved, personId) {
	$('#ssapSubmitModal').modal();
	$.ajax({
		type : 'POST',
		url : theUrlFordeleteApplicant,
		dataType : 'json',
		data : {
			currentPage : currentPage,
			webJson:JSON.stringify(webJson),
			applicantGuidToBeRemoved:applicantGuidToBeRemoved,
			personId:personId,
			csrftoken: csrftoken
		},
		success : function(msg,xhr) {
			if(isInvalidCSRFToken(msg))	{
				applicantGuidToBeRemoved = "";
				personIdToBeRemoved = "";
				return;
			}
			webJson = msg;
			afterDeleteApplicant();
			$('#ssapSubmitModal').modal('hide');
		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			$('#ssapSubmitModal').modal('hide');
			applicantGuidToBeRemoved ="";
			personIdToBeRemoved = "";
			alert("Please try Again !");
		}
	});
}

/*
****************************************************************************************************************
* saveData.js                                                                                       *
****************************************************************************************************************
*/

var row ='<tr id="person1">';
var SavingData = false;
function saveData53(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData53Json();
	var state = $('#home_primary_state').val();
	var county =  $('#home_primary_county').val();
	/*
    $.ajax({
            type : "POST",
            url : theUrlForCountyCodeDetails,
            data: { state: state, county: county},
            success : function(response) {
            	    webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].householdContact.homeAddress.primaryAddressCountyFipsCode = response.toString();

            },
            error : function(e) {
                    alert("Failed to get countycodedetails Response  ------->"+e);
            }
    });*/
}

function backData53(){
	setValuesToappscr53JSON();
}

function loadData54()
{
	setValuesToappscr54JSON();
	next();
}

function saveData54(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData54Json();
}

function backData54(){
	setValuesToappscr54JSON();
}

function loadData55()
{
	setData55();
	next();
}
function setData55(){
	setHouseHoldContact();
	setValuesToappscr55JSON();
}

function setHouseHoldContact()
{
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	if($('#middleName').val() == "Middle Name")
		$('#middleName').val('');

	if($('#lastName').val() == "Last Name")
		$('#lastName').val('');

	var name = getNameByPersonId(1);
	$('#houseHold_Contact_span1').html(name);
	$('#houseHold_Contact_span2').html(name);
	$('#houseHold_Contact_span3').html(name);
}

function saveData55(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData55Json();
}

function backData55(){
	setHouseHoldContact();
	setValuesToappscr55JSON();
	var firstIndex;
	var secondIndex;
	if($('#1ApplyingForhouseHoldOther').html() != undefined)
		firstIndex = $('#1ApplyingForhouseHoldOther').index();
	else if($('#1ApplyingForhouseHoldMember').html() != undefined)
		firstIndex = $('#1ApplyingForhouseHoldMember').index();
	else
		firstIndex = $('#1ApplyingForhouseHoldOnly').index();

	if($('#1page55radio9').html() != undefined)
		secondIndex = $('#1page55radio9').index();
	else
		secondIndex = $('#1page55radio10').index();
}


function saveData57(){
	//Validation Checking
	if(currentPage == 'appscr57'){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	}

	$('#back_button_div').show();
	addHouseHoldDetail();
}

function backData57(){

	$('#back_button_div').hide();
	var firstIndex = $('#1appscr57FirstName').index();
	var secondIndex;
	if($('#1appscr57SexNo').html() != undefined)
		secondIndex = $('#1appscr57SexNo').index();
	else
		secondIndex = $('#1appscr57SexYes').index();
	var personCount = 1;
	//setValuesToappscr57JSON();
	return;
}

function continueData60(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData60Json(currentID);
	return;
}

function backData60(){
	//
}

function continueData61(){

	//HIX-53221 SSN is not updated: need to call parsleyPageValidation() twice
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false && parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	var currentID = getCurrentApplicantID();
	continueData61Json(currentID);
	return;
}

function backData61(){
	var ApplicantID = getCurrentApplicantID();
	fillAppscr61(ApplicantID);
}

function continueData62Part1(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	//continueData62Part1Json();// Suneel 05/09/2014
	continueData62Part1Json(currentID);
	return;
}

function backData62Part1(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr61FromJSON(currentID);
	return;
}

function continueData62Part2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData62Part2Json();
}

function backData62Part2(){
	//
}

function continueData63(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData63Json(currentID);
	return;
}

function backData63(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr63FromJSON(currentID);
	return;
}

function continueData64(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData64Json(currentID);
	return;
}

function backData64(){
	//
}

function continueData65(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	continueData65Json();
	return;
}

function backData65(){
	appscr65FromMongodb();
}

function continueData66(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	continueData66Json(); return; //below code not needed
}

function backData66(){
	populateRelationship();
	populateDataToRelations();
}

function continueData69(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData69Json(currentID);
	return;
}

function backData69(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		appscr69JSON(currentID);
		//appscr74EditControllerJSON(currentID);
	}
	return;
}

function continueData70(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData70Json(currentID);
}

function backData70(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		appscr69FromMongodbJSON(currentID);
		//appscr74EditControllerJSON(currentID);
	}
}

function continueData71(){

	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData71Json(currentID);
	return;
}

function backData71(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		//appscr69JSON(currentID);
		appscr74EditControllerJSON(currentID);
	}
}

function continueData72(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData72Json(currentID);
	return;
}

function backData72(){
	//
}

function continueData73(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData73Json(currentID);
	return; //don't need following code
}

function backData73(){
	//
}

function continueData76P1(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData76P1Json(currentID);
	return;
}

function backData76P1(){
	//
}

function continueData76P2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData76P2Json(currentID);
}

function backData76P2(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr76FromMongodbJSON(currentID);
	return;
}

function continueData77Part1(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData77Part1Json(currentID);
}

function backData77Part1(){
	//
}

function continueData77Part2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData77Part2Json(currentID);
}

function backData77Part2(){
	//
}

function continueData78(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	//var currentID = getCurrentApplicantID();
}

function backData78(){
	//
}

function continueData79(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function backData79(){
	//
}

function continueData81A(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function backData81A(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr76FromMongodbJSON(currentID);
	return;
}

function saveData(){
	SavingData = true;
	if(currentPage == "appscr53"){
		/*if(ssapHomeAddressValidationFlag === false || ssapMailingAddressValidationFlag === false){
			SavingData = false;
			$('#addressNotFoundModal').modal({
				keyboard: false,
				backdrop: 'static'
			});
			
			return;
		}*/
		
		saveData53();
	}
	if(currentPage == "appscr54"){
		saveData54();
	}
	if(currentPage == "appscr55"){
		saveData55();
	}
	if(currentPage == "appscr57"){
		$('#contBtn').show();
		$('#contBtn').text('Continue');
		$('#countinue_button_div').addClass("Continue_button");
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");

		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr58"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr59A"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr59B"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr60"){
		continueData60();
	}
	if(currentPage == "appscr61"){
		continueData61();
	}
	if(currentPage == "appscr62Part1"){
		continueData62Part1();
	}
	if(currentPage == "appscr62Part2"){
		continueData62Part2();
	}
	if(currentPage == "appscr63"){
		continueData63();
	}
	if(currentPage == "appscr64"){
		continueData64();
	}
	if(currentPage == "appscr65"){
		continueData65();
	}
	if(currentPage == "appscr66"){
		continueData66();
	}
	if(currentPage == "appscr69"){
		continueData69();
	}
	if(currentPage == "appscr70"){
		continueData70();
	}
	if(currentPage == "appscr71"){
		continueData71();
	}
	if(currentPage == "appscr72"){
		continueData72();
	}
	if(currentPage == "appscr73"){
		continueData73();
	}
	if(currentPage == "appscr76P1"){
		continueData76P1();
	}
	if(currentPage == "appscr76P2"){
		continueData76P2();
	}
	if(currentPage == "appscr77Part1"){
		continueData77Part1();
	}
	if(currentPage == "appscr77Part2"){
		continueData77Part2();
	}
	if(currentPage == "appscr78"){
		continueData78();
	}
	if(currentPage == "appscr79"){
		continueData79();
	}
	if(currentPage == "appscr81A"){
		continueData81A();
	}
	if(currentPage == "appscr53" && editFlag){
		addAppscr58Detail(); //reload summary data
		goToPageById("appscr58");
		enableOtherFields();
		editFlag = false;
		$('#contBtn').text('Continue');
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");
		$('#countinue_button_div').addClass("Continue_button");
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();
		$('#contBtn').show();
	}
	if(currentPage == "appscrBloodRel"){
		saveBloodRelation();
	}
	//saveBloodRelationData();
	/*if(submitForAutoContinue==true) {
		saveDataSubmit();
	}*/
	if(mode=="" && validationCheck==true) {
		saveDataSubmit();

		/*if(currentPage=="appscr82") {
			testRidp();
		}*/
	}
	SavingData = false;
}
var array53 = new Array();
function createArray53(){

	array53[0] = 'firstName';
	array53[1]= 'middleName';
	array53[2] = 'lastName';
	array53[3] = 'appscr53Suffix';
	array53[4] = 'dateOfBirth';
	array53[5] = 'emailAddress';
	array53[6] = 'home_addressLine1';
	array53[7] = 'home_addressLine2';
	array53[8] = 'home_primary_city';
	array53[9] = 'home_primary_zip';
	array53[10] = 'home_primary_state';
	array53[11] = 'home_primary_county';
	array53[12] = 'mailingAddressIndicator';
	array53[13] = 'mailing_addressLine1';
	array53[14] = 'mailing_addressLine2';
	array53[15] = 'mailing_primary_city';
	array53[16] = 'mailing_primary_zip';
	array53[17] = 'mailing_primary_state';
	array53[18] = 'mailing_primary_county';
	array53[19] = 'first_phoneNo';
	array53[20] = 'first_ext';
	array53[21] = 'first_phoneType';
	array53[22] = 'second_phoneNo';
	array53[23] = 'second_ext';
	array53[24] = 'second_phoneType';
	array53[25] = 'preffegred_spoken_language';
	array53[26] = 'preffered_written_language';
}

var array54 = new Array();
function createArray54(){

	array54[0] = 'brokerName54';
	array54[1]= 'brokerID54';
	array54[2] = 'guideName54';
	array54[3] = 'guideID54';
	if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val()== 'yes'){
		array54[4] = 'displayAuthRepresentativeInfo';
	}
	else{
		array54[4] = 'hideAuthRepresentativeInfo';
	}
	array54[5] = 'authorizedFirstName';
	array54[6] = 'authorizedMiddleName';
	array54[7] = 'authorizedLastName';
	array54[8] = 'authorizedSuffix';
	array54[9] = 'authorizedEmailAddress';
	array54[10] = 'authorizedAddress1';
	array54[11] = 'authorizedAddress2';
	array54[12] = 'authorizedCity';
	array54[13] = 'authorizedState';
	array54[14] = 'authorizedZip';
	array54[15] = 'appscr54_phoneNumber';
	array54[16] = 'appscr54_ext';
	array54[17] = 'authorizedPhoneType';
	if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val()== 'Yes'){
		array54[18] = 'cmpnyNameAndOrgRadio54_1';
	}
	else{
		array54[18] = 'cmpnyNameAndOrgRadio54_2';
	}
	array54[19] = 'authorizeCompanyName';
	array54[20] = 'authorizeOrganizationId';
	if($('input[name=makeOtherizedRepresentative]:radio:checked').val()== 'Signature'){
		array54[21] = 'makeOtherizedRepresentativeSignature';
	}
	else{
		array54[21] = 'makeOtherizedRepresentativeLater';
	}
}

var array55 = new Array();
function createArray55(){

	if($('input[name=ApplyingForhouseHold]:radio:checked').val()== 'houseHoldContactOnly'){
		array55[0] = 'ApplyingForhouseHoldOnly';
	}
	else if($('input[name=ApplyingForhouseHold]:radio:checked').val()== 'otherFamilyMember'){
		array55[0] = 'ApplyingForhouseHoldMember';
	}
	else{
		array55[0] = 'ApplyingForhouseHoldOther';
	}
	if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val()== 'Yes'){
		array55[1]= 'wantToGetHelpPayingHealthInsurance_id1';
	}
	else{
		array55[1]= 'wantToGetHelpPayingHealthInsurance_id2';
	}
	if($('input[name=appscr55paragraph]:radio:checked').val()== 'yes'){
		array55[2] = 'page55radio6';
	}
	else if($('input[name=appscr55paragraph]:radio:checked').val()== 'no'){
		array55[2] = 'page55radio7';
	}
	else{
		array55[2] = 'page55radio8';
	}

	array55[3] = 'noOfPeople53';

	if($('input[name=checkTypeOfHelp4HealthInsurance]:radio:checked').val()== 'yes'){
		array55[4]= 'page55radio9';
	}
	else{
		array55[4]= 'page55radio10';
	}

}

var array57 = new Array();
function generateArray57(){
		var noOfPeople=getNoOfHouseHolds (); //$('#noOfPeople53').val();
		var count = 0;
		for(var index=1; index<=noOfPeople;index++){
			array57[count++] = 'appscr57FirstName';
			array57[count++]= 'appscr57MiddleName';
			array57[count++] = 'appscr57LastName';
			array57[count++] = 'identifire';
			array57[count++] = 'appscr57Suffix';
			array57[count++] = 'appscr57DOB';
			array57[count++] = 'appscr57DOBVerificationStatus';
			if($('input[name=appscr57Tobacco'+(index)+']:radio:checked').val()== 'yes'){
				array57[count++]= 'appscr57SexYes';
			}
			else{
				array57[count++]= 'appscr57SexNo';
			}

		}
}
////////////////////////// Method generating Ajax Request start////////

function saveDataSubmit(){
	$('#modalCSRApplicationType').modal('hide');
	$('#ssapSubmitModal').modal();
	$('#waiting').show();
	$('#contBtn').attr('disabled','disabled');
	if(currentPage == "appscr87"){
		//$('#generateXMLFormLast').submit();
		//$('#waiting').hide();
		//return;
	}
	var idname="";
	var caseindividual = [];
	var count = 0;

	var result = "";
	for ( var i = 0; i < caseindividual.length; i++) {
		result += caseindividual[i];
	}
	var dataFD = getFDData();
	var casei;
	var CaseIdentifier = 1;//$('#cohbeApplicationId').val();
	var HeadOFHousehold = 1;//$('#cbmsApplicationId').val();

	casei = 'CaseIdentifier=' + CaseIdentifier + '&HeadOFHousehold='
			+ HeadOFHousehold;

	var relationship = [];
	$('#appscrBloodRelTableBody tr').each(
			function() {

					var Indiv = $(this).find(".Indiv").html();
					var rel = $(this).find(".rel").html();
					var relt = $(this).find(".relt").html();
					var relat = $(this).find(".relat").html();
					var pct = $(this).find(".pct").html();
					var tax = $(this).find(".tax").html();
					relationship.push(Indiv + '&' + rel + '&' + relt + '&'
							+ relat + '&' + pct + '&' + tax + '&');

			});


	var result1 = "";
	for ( var i = 0; i < relationship.length; i++) {
		result1 += relationship[i];
	}

	useJSONstring="No";
	if(useJSON == true){
		useJSONstring="Yes";
	}
	webJson.singleStreamlinedApplication.currentApplicantId = getCurrentApplicantID();
	if(sepEventDate == ""){  //OE application defatul date.
		var today = new Date();
		sepEventDate =parseInt(today.getMonth())+1 + "/" + (parseInt(today.getDate())-1) + "/" + today.getFullYear();
	}
	if(sepEvent == "") sepEvent = null;
	webJsonstring = JSON.stringify(webJson);
	$.ajax({
		type : 'POST',
		url : theUrlForSaveJsonData,
		dataType : 'json',
		data : {
			case1 : casei,
			case2 : result,
			case3 : result1,
			case4 : idname,
			data : dataFD,
			currentPage : currentPage,
			webJson:webJsonstring,
			useJSON:useJSONstring,
			applicationSource:$('input[name=optCSRApplicationType]:radio:checked').val()==undefined?"ON":$('input[name=optCSRApplicationType]:radio:checked').val(),
			sepEvent:sepEvent,
			sepEventDate:sepEventDate,
			applicationType:applicationType,
			isMailingAddressUpdated:$('#isMailingAddressUpdated').val(),
			csrftoken: csrftoken
		},
		//async : true,
		success : function(msg,xhr) {

			if(isInvalidCSRFToken(msg))	{
				applicantGuidToBeRemoved = "";
				return;
			}
			/*if(msg!='' && msg.status == 'SSAC_SUCCESS' ){

				if(msg.object == 'N'){
					alert('Cannot Proceed ');
				}
			}*/

			if(msg!='' && msg != null) {
				webJson = msg;
			}

			if(webJson.singleStreamlinedApplication.mecCheckStatus!=null && webJson.singleStreamlinedApplication.mecCheckStatus!=undefined && "Y"==webJson.singleStreamlinedApplication.mecCheckStatus){
				$('#mecCheckAlert').modal();
			}
			
			if(currentPage === "appscr85" && applicationType === "QEP" && isSepEventDateValid()){
				goToDashboard();
			}else{
				$('#ssapSubmitModal').modal('hide');
				$('#waiting').hide();
				$('#contBtn').removeAttr('disabled');
				onSucessfullSaveDataSubmit();
				//$('#generateJSONForm').submit();
			}

		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			if("500:ESIGNED_APPLICATION" == XMLHttpRequest.responseText) {
				location.href="indportal";
			}else if (XMLHttpRequest.responseText.indexOf("application-error") != -1 || XMLHttpRequest.responseText.indexOf("Application Error") != -1){
               ////Added this if condition to show serverside validation error page for HIX-67907
				document.write(XMLHttpRequest.responseText);
     		   document.close();
     	   }else{
				applicantGuidToBeRemoved = "";
				$('#waiting').hide();
				$('#ssapSubmitModal').modal('hide');
				//alert("Please try Again !");HIX-73052
				location.href="indportal";
				//$('#contBtn').removeAttr('disabled');
			}
		}
	});
}


function updateMailingAddressSameAsHomeAddressIndicator(){
    var arr = ["postalCode","county","streetAddress1","streetAddress2","state","city"];
	for(var i=0;i<webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;i++){
		    var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i];
		    var comparisonResult = true;
		    for(var j=0;j<arr.length;j++){
				comparisonResult = (eval('householdMemberObj.householdContact.homeAddress.'+arr[j]) == eval('householdMemberObj.householdContact.mailingAddress.'+arr[j]));
				if(!comparisonResult){
					break;
				}
			}
			if(!comparisonResult){
				householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = false;
			}

	  }
}

function onSucessfullSaveDataSubmit(){
	if(currentPage == "appscr85"){
		next();
	}

}
function saveFDRelation(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function getFDData(){
//
}

function getFrequencyData(HouseHoldRow)
{
	var dataIndex = 0;
	var frequencyData='';
	for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		var data = financial_DetailsArray[dataIndex];
		if(dataIndex!=0 && dataIndex!=2 && dataIndex!=6){
			frequencyData+=HouseHoldRow.find("."+data+"Frequency").html();
			frequencyData+='#';
		}
	}
	return frequencyData;
}

function otherInsuranceCheckbox(i){
	if($("#appscr78OtherStateFHBP"+i).is(":checked")){
		$("#appscr78OtherStateOrFederalHealthPlan"+i).show();
	}else{
		$("#appscr78OtherStateOrFederalHealthPlan"+i).hide();
	}
}

/*
****************************************************************************************************************
* saveJsonData.js                                                                                     *
****************************************************************************************************************
*/

var incomesecion = {'Financial_Job':false, 'Financial_Self_Employment':false, 'Financial_SocialSecuritybenefits':false,
		'Financial_Unemployment':false, 'Financial_Retirement_pension':false, 'Financial_Capitalgains':false,
		'Financial_InvestmentIncome':false, 'Financial_RentalOrRoyaltyIncome':false,
		'Financial_FarmingOrFishingIncome':false, 'Financial_AlimonyReceived':false,
		'Financial_OtherIncome':false};

/*
* Save Data To JSON from Primary Contact Information Page
*/
function saveData53Json(){
if(useJSON == true){
resetData53Json();
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
householdMemberObj.name.firstName = trimSpaces($('#firstName').val());
householdMemberObj.name.middleName = trimSpaces($('#middleName').val());
householdMemberObj.name.lastName = trimSpaces($('#lastName').val());
householdMemberObj.name.suffix = $('#appscr53Suffix').val();
householdMemberObj.dateOfBirth = DBDateformat($('#dateOfBirth').val());

householdMemberObj.householdContact.contactPreferences.emailAddress = $('#emailAddress').val();
householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage = $('#preffegred_spoken_language').val();
householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage = $('#preffered_written_language').val();

var preferredContactBy = "";

if($("#email").is(":checked")) {
	preferredContactBy = "EMAIL";
}else if($("#inTheEmail").is(":checked")) {
		preferredContactBy = "POSTAL MAIL";
}else if($("#emailAndMail").is(":checked")){
	preferredContactBy = "EmailAndMail"
}else{
	preferredContactBy = "None"
}

householdMemberObj.householdContact.contactPreferences.preferredContactMethod = preferredContactBy;

if((householdMemberObj.householdContact.contactPreferences.preferredContactMethod == null) || (householdMemberObj.householdContact.contactPreferences.preferredContactMethod=="")) {
	householdMemberObj.householdContact.contactPreferences.preferredContactMethod = "EMAIL";
}

householdMemberObj.householdContact.homeAddressIndicator = true;
householdMemberObj.householdContact.homeAddress.city = trimSpaces($('#home_primary_city').val());
householdMemberObj.householdContact.homeAddress.county = $("#home_primary_county option:selected").text();
householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode = $('#home_primary_county').val();
householdMemberObj.householdContact.homeAddress.postalCode =  trimSpaces($('#home_primary_zip').val());
householdMemberObj.householdContact.homeAddress.state = $('#home_primary_state').val();
householdMemberObj.householdContact.homeAddress.streetAddress1 = trimSpaces($('#home_addressLine1').val());
householdMemberObj.householdContact.homeAddress.streetAddress2 = trimSpaces($('#home_addressLine2').val());

householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = $('#mailingAddressIndicator').val();

if($('#mailingAddressIndicator').is(':checked')){
	householdMemberObj.householdContact.mailingAddress.city = householdMemberObj.householdContact.homeAddress.city;
	householdMemberObj.householdContact.mailingAddress.county = householdMemberObj.householdContact.homeAddress.county;
	householdMemberObj.householdContact.mailingAddress.postalCode = householdMemberObj.householdContact.homeAddress.postalCode;
	householdMemberObj.householdContact.mailingAddress.state = householdMemberObj.householdContact.homeAddress.state;
	householdMemberObj.householdContact.mailingAddress.streetAddress1 = householdMemberObj.householdContact.homeAddress.streetAddress1;
	householdMemberObj.householdContact.mailingAddress.streetAddress2 = householdMemberObj.householdContact.homeAddress.streetAddress2;
	//householdMemberObj.householdContact.mailingAddress.primaryAddressCountyFipsCode = householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode;
}else{
	householdMemberObj.householdContact.mailingAddress.city = trimSpaces($('#mailing_primary_city').val());
	householdMemberObj.householdContact.mailingAddress.county = $('#mailing_primary_county option:selected').text();
	//householdMemberObj.householdContact.mailingAddress.primaryAddressCountyFipsCode = $('#mailing_primary_county').val();
	householdMemberObj.householdContact.mailingAddress.postalCode = trimSpaces($('#mailing_primary_zip').val());
	householdMemberObj.householdContact.mailingAddress.state = $('#mailing_primary_state').val();
	householdMemberObj.householdContact.mailingAddress.streetAddress1 = trimSpaces($('#mailing_addressLine1').val());
	householdMemberObj.householdContact.mailingAddress.streetAddress2 = trimSpaces($('#mailing_addressLine2').val());
}


householdMemberObj.householdContact.phone.phoneNumber = $('#first_phoneNo').val().replace(/\D+/g, "");
householdMemberObj.householdContact.phone.phoneExtension = $('#first_ext').val();
//householdMemberObj.householdContact.phone.phoneType = $('#first_phoneType').val();
householdMemberObj.householdContact.phone.phoneType = "CELL";

householdMemberObj.householdContact.otherPhone.phoneNumber = $('#first_homePhoneNo').val().replace(/\D+/g, "");
householdMemberObj.householdContact.otherPhone.phoneExtension = $('#first_homeExt').val();
//householdMemberObj.householdContact.otherPhone.phoneType = $('#second_phoneType').val();
householdMemberObj.householdContact.otherPhone.phoneType = "HOME";



webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;
}


}

function saveData54Json(){
if(useJSON == true){
resetData54Json();

var helpType = webJson.singleStreamlinedApplication.helpType;


//get help from broker
if(helpType == "BROKER"){
	BrokerObj=webJson.singleStreamlinedApplication.broker;

	BrokerObj.brokerFederalTaxIdNumber = $('#brokerID54').val();
	BrokerObj.brokerName = $('#brokerName54').val();
	BrokerObj.internalBrokerId = parseInt($('#internalBrokerId').val());
	webJson.singleStreamlinedApplication.broker = BrokerObj;

	webJson.singleStreamlinedApplication.getHelpIndicator = true;
//get help from assister
}else if(helpType == "ASSISTER"){
	assisterObj=webJson.singleStreamlinedApplication.assister;

	assisterObj.assisterID = $('#guideID54').val();
	assisterObj.assisterName = $('#guideName54').val();
	webJson.singleStreamlinedApplication.broker = assisterObj;

	webJson.singleStreamlinedApplication.getHelpIndicator = true;
}else{
	if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'yes'){
		webJson.singleStreamlinedApplication.getHelpIndicator = true;
	}else if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'no'){
		webJson.singleStreamlinedApplication.getHelpIndicator = false;
		webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = false;
	}
}

if(webJson.singleStreamlinedApplication.getHelpIndicator === true){
	if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == 'yes'){
		webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = true;
	}else if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val() == 'no'){
		webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = false;
	}
}

if(webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator === true){
	AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;

	AuthorizedRepresentativeObj.name.firstName = $('#authorizedFirstName').val();
	AuthorizedRepresentativeObj.name.middleName = $('#authorizedMiddleName').val();
	AuthorizedRepresentativeObj.name.lastName = $('#authorizedLastName').val();
	AuthorizedRepresentativeObj.name.suffix = $('#authorizedSuffix').val();

	AuthorizedRepresentativeObj.emailAddress = $('#authorizedEmailAddress').val();

	AuthorizedRepresentativeObj.address.streetAddress1 = $('#authorizedAddress1').val();
	AuthorizedRepresentativeObj.address.streetAddress2 = $('#authorizedAddress2').val();
	AuthorizedRepresentativeObj.address.city = $('#authorizedCity').val();
	AuthorizedRepresentativeObj.address.state = $('#authorizedState').val();
	AuthorizedRepresentativeObj.address.postalCode = $('#authorizedZip').val();

	//cell
	AuthorizedRepresentativeObj.phone[0].phoneNumber = $('#appscr54_phoneNumber').val().replace(/\D+/g, "");
	AuthorizedRepresentativeObj.phone[0].phoneExtension = $('#appscr54_ext').val();

	//home
	AuthorizedRepresentativeObj.phone[1].phoneNumber = $('#appscr54_homePhoneNumber').val().replace(/\D+/g, "");
	AuthorizedRepresentativeObj.phone[1].phoneExtension = $('#appscr54_homeExt').val();


	//work
	AuthorizedRepresentativeObj.phone[2].phoneNumber = $('#appscr54_workPhoneNumber').val().replace(/\D+/g, "");
	AuthorizedRepresentativeObj.phone[2].phoneExtension = $('#appscr54_workExt').val();


	if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == 'yes'){
		AuthorizedRepresentativeObj.partOfOrganizationIndicator = true;

		AuthorizedRepresentativeObj.companyName = $('#authorizeCompanyName').val();
		AuthorizedRepresentativeObj.organizationId = $('#authorizeOrganizationId').val();

	}else if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val() == 'no'){
		AuthorizedRepresentativeObj.partOfOrganizationIndicator = false;
	}



	if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
		AuthorizedRepresentativeObj.signatureisProofIndicator = true;
		AuthorizedRepresentativeObj.signature = $("#authorizeSignature").val();
	}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
		AuthorizedRepresentativeObj.signatureisProofIndicator = false;
	}

	webJson.singleStreamlinedApplication.authorizedRepresentative=AuthorizedRepresentativeObj;
}

}

}

function saveData55Json(){
if(useJSON == true){
	resetData55Json();
	webJson.singleStreamlinedApplication.applyingForhouseHold = $('input[name=ApplyingForhouseHold]:radio:checked').val();
	if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val() == 'Yes')
		householdMemberObj.applyingForFinancialAssistanceIndicator = true;
	else if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val() == 'No')
		householdMemberObj.applyingForFinancialAssistanceIndicator = false;

	checkFinacialAssistance();

	//webJson.singleStreamlinedApplication = householdMemberObj;
}

}

function continueData60Json(i){
if(useJSON == true){
resetData60Json(i);
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

if($('input[name=planToFileFTRIndicator]:radio:checked').val() == 'yes'){
	householdMemberObj.planToFileFTRIndicator = true;
}else if($('input[name=planToFileFTRIndicator]:radio:checked').val() == 'no'){
	householdMemberObj.planToFileFTRIndicator = false;
}else{
	householdMemberObj.planToFileFTRIndicator = null;
}


if($('input[name=marriedIndicator]:radio:checked').val() == 'yes'){
	householdMemberObj.marriedIndicator =  true;
}else if($('input[name=marriedIndicator]:radio:checked').val() == 'no'){
	householdMemberObj.marriedIndicator = false;
}else{
	householdMemberObj.marriedIndicator = null;
}



if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.marriedIndicator ===  true && $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes' ){
	householdMemberObj.planToFileFTRJontlyIndicator = true;
}else if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.marriedIndicator ===  true && $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'no' ){
	householdMemberObj.planToFileFTRJontlyIndicator = false;
}else{
	householdMemberObj.planToFileFTRJontlyIndicator = null;
}



if(householdMemberObj.marriedIndicator ===  true && $('input[name=liveWithSpouseIndicator]:radio:checked').val() == 'yes'){
	householdMemberObj.taxFiler.liveWithSpouseIndicator = true;
}else if(householdMemberObj.marriedIndicator ===  true && $('input[name=liveWithSpouseIndicator]:radio:checked').val() == 'no'){
	householdMemberObj.taxFiler.liveWithSpouseIndicator = false;
}else{
	householdMemberObj.taxFiler.liveWithSpouseIndicator = null;
}

//householdMemberObj.taxFiler.spouseHouseholdMemberId = $('input[name=householdContactSpouse]:radio:checked').val();

if(householdMemberObj.planToFileFTRIndicator === true && $('input[name=householdHasDependant]:radio:checked').val() == 'yes'){
	householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = true;
}else if(householdMemberObj.planToFileFTRIndicator === true && $('input[name=householdHasDependant]:radio:checked').val() == 'no'){
	householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = false;
}else{
	householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = null;
}


if(!householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == 'yes'){
	householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = true;
}else if(!householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator && $('input[name=doesApplicantHasTaxFiller]:radio:checked').val() == 'no'){
	householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = false;
}else{
	householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = null;
}


var count = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;


/*****************************************************************************************************************************************/
/********************************************************Update primary tax filer Id*****************************************************/
/***************************************************************************************************************************************/
//TF is decided in PC page
if(HouseHoldRepeat == 1){
	//primary contact is tax filer
	if(householdMemberObj.planToFileFTRIndicator === true && !householdMemberObj.taxFilerDependant.taxFilerDependantIndicator){
		webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = parseInt(i);
	}else if(householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === true){
		//Primary Contact is not a tax filer and being claimed as a dependent of someone who is part of the household
		//add one more condition is because "Will [] be claimed" will be asked even PC is TF
		var taxFilerDependantId = parseInt($('input[name=householdContactFiler]:radio:checked').attr('id').substring(21));
		//if NaN, that means someone else not in family household, we set it to -1
		if(!isNaN(taxFilerDependantId)){
			webJson.singleStreamlinedApplication.primaryTaxFilerPersonId =  taxFilerDependantId;
		}else{
			webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = -1;
		}
	}else{
		webJson.singleStreamlinedApplication.primaryTaxFilerPersonId = 0;
	}

}



/*****************************************************************************************************************************************/
/********************************************************Update primary tax filer Id End*************************************************/
/***************************************************************************************************************************************/


/*****************************************************************************************************************************************/
/***********************************************************Update spouse Id*************************************************************/
/***************************************************************************************************************************************/
//get spouse
if(householdMemberObj.marriedIndicator ===  true){
	//get spouse ID
	var spouseId = parseInt($('input[name=householdContactSpouse]:radio:checked').attr('id').substring(22));

	//if spouse is changed on PC page, we need to reset the spouse for all applicants because if PC is not TF then TF's spouse will be changed base on PC's spouse
	if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.spouseHouseholdMemberId != spouseId){
		for(var n=1; n<=count ; n++){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[n-1].taxFiler.spouseHouseholdMemberId = 0;
		}
	}

	//if spouse is part of family
	if(!isNaN(spouseId)){
		householdMemberObj.taxFiler.spouseHouseholdMemberId = spouseId;
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[spouseId-1].taxFiler.spouseHouseholdMemberId = householdMemberObj.personId;
	}else{
		//if spouse is NOT part of family
		//set PC's spouse Id to -1
		householdMemberObj.taxFiler.spouseHouseholdMemberId = -1;

		//if PC already has a spouse who is part of family before, set spouse IDs to 0 for that spouse
		if(householdMemberObj.taxFiler.spouseHouseholdMemberId != 0 && householdMemberObj.taxFiler.spouseHouseholdMemberId != -1){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId-1].taxFiler.spouseHouseholdMemberId = 0;
		}
	}
}else{

	//if spouse is changed on PC page, we need to reset the spouse for all applicants because if PC is not TF then TF's spouse will be changed base on PC's spouse
	if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.spouseHouseholdMemberId != 0){
		for(var n=1; n<=count ; n++){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[n-1].taxFiler.spouseHouseholdMemberId = 0;
		}
	}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId !=0){
		//if PC is not TF, only remove TF's spouse id and keep PC's spouse
		var temp = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		householdMemberObj.taxFiler.spouseHouseholdMemberId = 0;
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[temp-1].taxFiler.spouseHouseholdMemberId = 0;
	};

	//if PC is TF(for back situation), remove all spouse id
	/*if(HouseHoldRepeat == 1 && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false){
		for(var j=1; j<=count; j++){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[j-1].taxFiler.spouseHouseholdMemberId = 0;
		}
	}else if(householdMemberObj.taxFiler.spouseHouseholdMemberId !=0){
		//if PC is not TF, only remove TF's spouse id
		var temp = householdMemberObj.taxFiler.spouseHouseholdMemberId;
		householdMemberObj.taxFiler.spouseHouseholdMemberId = 0;
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[temp-1].taxFiler.spouseHouseholdMemberId = 0;
	};
*/
}


/*****************************************************************************************************************************************/
/***********************************************************Update spouse Id End*********************************************************/
/***************************************************************************************************************************************/


/*****************************************************************************************************************************************/
/********************************************Update dependents Id and seeking coverage indicator*****************************************/
/***************************************************************************************************************************************/

//Update array when TF and claim dependents
if(householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
	//remove previous dependents
	webJson.singleStreamlinedApplication.taxFilerDependants = [];

	//update dependents and their coverage indicator
	$("#householdDependantDiv").find("input").each(function(){
		var dependantId = parseInt($(this).attr("id").substring(25));
		if($(this).is(":checked")){
			if(!isNaN(dependantId) && webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId) == -1){
				webJson.singleStreamlinedApplication.taxFilerDependants.push(dependantId);

				//check applyingForCoverageIndicatorArrCopy
				if(applyingForCoverageIndicatorArrCopy[i-1] === true){
					webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = true;
				}
			}
		}else if(!isNaN(dependantId)){
			//remove unchecked dependent from array
			var dependantIndex = webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId);
			if(dependantIndex != -1) {
				webJson.singleStreamlinedApplication.taxFilerDependants.splice(dependantIndex, 1);
			}
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = false;
		}
	});

	// if TF is not PC, that means he/she is not in dependent list, so we need to update its applyingForCoverageIndicator manually
	if(HouseHoldRepeat != 1){
		if(applyingForCoverageIndicatorArrCopy[HouseHoldRepeat-1] === true){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[HouseHoldRepeat-1].applyingForCoverageIndicator = true;
		}
	}

//empty array when not TF and not be claimed
//empty array when TF and not to claim other people
}else if((householdMemberObj.planToFileFTRIndicator === true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator === false && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false) || (householdMemberObj.planToFileFTRIndicator === false && householdMemberObj.taxFilerDependant.taxFilerDependantIndicator === false)){
	//empty dependents
	webJson.singleStreamlinedApplication.taxFilerDependants = [];

	//NEVER CHANGE PC'S SEEKING COVERAGE INDICATOR

	//update seeking coverage indicator to false for all people except PC
	//this situation only happen on HouseHoldRepeat = 1
	/*if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){*/
		for(var k=2;k<=count;k++){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;

		}
	/*}*/

//otherwise don't change array
}else{

}



/*
//collect dependents ID only on TF tax page, and update seeking coverage indicator
if(householdMemberObj.planToFileFTRIndicator == true && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
	//update dependents and their coverage indicator
	if(householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == true){
		$("#householdDependantDiv").find("input").each(function(){
			var dependantId = parseInt($(this).attr("id").substring(25));
			if($(this).is(":checked")){
				if(!isNaN(dependantId) && webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId) == -1){
					webJson.singleStreamlinedApplication.taxFilerDependants.push(dependantId);

					//check applyingForCoverageIndicatorArrCopy
					if(applyingForCoverageIndicatorArrCopy[i-1] == true){
						webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = true;
					}
				}
			}else if(!isNaN(dependantId)){
				//remove unchecked dependent from array
				var dependantIndex = webJson.singleStreamlinedApplication.taxFilerDependants.indexOf(dependantId);
				if(dependantIndex != -1) {
					webJson.singleStreamlinedApplication.taxFilerDependants.splice(dependantIndex, 1);
				}
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[dependantId-1].applyingForCoverageIndicator = false;
			}
		});

		//need to update applyingForCoverageIndicator flag if TF is not PC
		if(HouseHoldRepeat != 1){
			if(applyingForCoverageIndicatorArrCopy[HouseHoldRepeat-1] == true){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[HouseHoldRepeat-1].applyingForCoverageIndicator = true;
			}
		}


	}else{
		//dependent indicator is false, that means no dependent
		webJson.singleStreamlinedApplication.taxFilerDependants = [];

		//if this is PC, planToFileFTRIndicator = false, and will not be claimed
		//update seeking coverage indicator to false for all people except PC
		if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){
			for(var k=2;k<=count;k++){
				webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;

			}
		}
	}

	//update spouses and their coverage indicator
	if(householdMemberObj.planToFileFTRIndicator != true && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId-1].applyingForCoverageIndicator = false;
	}

}else{
	//remove dependents
	webJson.singleStreamlinedApplication.taxFilerDependants = [];

	//if this is PC, planToFileFTRIndicator = false, and will not be claimed
	//update seeking coverage indicator to false for all people except PC
	if(HouseHoldRepeat == 1 && householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator == false){
		for(var k=2;k<=count;k++){
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[k-1].applyingForCoverageIndicator = false;

		}
	}
}*/

//if TF file a joint tax, and spouse is part of household, and spouse will seeking coverage, then update spouse's indicator to true
if(householdMemberObj.planToFileFTRJontlyIndicator === true && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0 && applyingForCoverageIndicatorArrCopy[householdMemberObj.taxFiler.spouseHouseholdMemberId-1] === true){
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId - 1].applyingForCoverageIndicator = true;
}else if(householdMemberObj.planToFileFTRJontlyIndicator === false && householdMemberObj.taxFiler.spouseHouseholdMemberId > 0){
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[householdMemberObj.taxFiler.spouseHouseholdMemberId - 1].applyingForCoverageIndicator = false;
}

/*****************************************************************************************************************************************/
/********************************************Update dependents Id and seeking coverage indicator End*************************************/
/***************************************************************************************************************************************/

/*
householdMemberObj.livesWithHouseholdContactIndicator = false;
if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes'){
	householdMemberObj.livesWithHouseholdContactIndicator = true;
}
*/

//householdMemberObj.taxFiler.spouseHouseholdMemberId = false;


webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1] = householdMemberObj;

//var spouseid = householdMemberObj.taxFiler.spouseHouseholdMemberId.charAt(9);
spouseObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
spousename = spouseObj.name.firstName + " " +  spouseObj.name.middleName + " " + spouseObj.name.lastName;
if($('input[name=planOnFilingJointFTRIndicator]:radio:checked').val() == 'yes' && $('input[name=marriedIndicator]:radio:checked').val() == 'yes'){
	$(".householdContactSpouseName").text("and "+ spousename);
}


//var planOnFilingJointFTRIndicator = $('input[name=planOnFilingJointFTRIndicator]:radio:checked').val();
//var householdContactSpouse = $('input[name=householdContactSpouse]:radio:checked').val();

//var householdHasDependant = $('input[name=householdHasDependant]:radio:checked').val();
//var doesApplicantHasTaxFiller = $('input[name=doesApplicantHasTaxFiller]:radio:checked').val();

}

}

function continueData61Json(currentID){
if(useJSON == true){
resetData61Json(currentID);

householdMemberObj.gender = $('input[name=appscr61_gender]:radio:checked').val();

if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'yes'){
	householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = true;
	//householdMemberObj.socialSecurityCard.socialSecurityNumber = $('#ssn').attr('data-ssn');
	if($('input[name=fnlnsSame]:radio:checked').val() == 'yes'){
		householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = true;
	}else if($('input[name=fnlnsSame]:radio:checked').val() == 'no'){
		householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = false;

		householdMemberObj.socialSecurityCard.firstNameOnSSNCard = $('#firstNameOnSSNCard').val();
		householdMemberObj.socialSecurityCard.middleNameOnSSNCard = $('#middleNameOnSSNCard').val();
		householdMemberObj.socialSecurityCard.lastNameOnSSNCard = $('#lastNameOnSSNCard').val();
		householdMemberObj.socialSecurityCard.suffixOnSSNCard = $('#suffixOnSSNCard').val();
	}
}else if($('input[name=socialSecurityCardHolderIndicator]:radio:checked').val() == 'no'){
	householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = false;
	householdMemberObj.socialSecurityCard.socialSecurityNumber = '';
	householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN = $('#reasonableExplanationForNoSSN').val();
}
if($('input[name=fnlnsExemption]:radio:checked').val() == 'yes'){
	householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = true;
} else if($('input[name=fnlnsExemption]:radio:checked').val() == 'no'){
	householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = false;
}
householdMemberObj.socialSecurityCard.individualManadateExceptionID = $('#exemptionId').val();

webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;

//var socialSecurityNameSuffix = $('#suffixOnSSNCard').val();
//var fnlnsExemption = $('input[name=fnlnsExemption]:radio:checked').val();
//var exemptionId = $('#exemptionId').val();
}

}

function continueData62Part1Json(currentID){
if(useJSON == true){

	//before set new value and saving, assigan default value
	var defaultJson = getDefatulJson(); // get default json
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	//Assign default value
	householdMemberObj.citizenshipImmigrationStatus = defaultJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].citizenshipImmigrationStatus;

/*	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
householdMemberObj.citizenshipImmigrationStatus.citizenshipManualVerificationStatus = $('input[name=UScitizenManualVerification]:radio:checked').val();
householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();
householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val();	*/


/*25 April*/
//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

 householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator = $('input[name=UScitizen]:radio:checked').val() == "true" ? true : false ;

 householdMemberObj.citizenshipImmigrationStatus.citizenshipAsAttestedIndicator = $('input[name=UScitizen]:radio:checked').val() == "true" ? true : false ;

 if( householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator == true) return;
// if(householdMemberObj.citizenshipImmigrationStatus.citizenshipStatusIndicator)

 //suneel 05/09/2014
 //householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]').val()== "true" ? true : false ;
 householdMemberObj.citizenshipImmigrationStatus.naturalizedCitizenshipIndicator = $('input[name=naturalizedCitizenshipIndicator]:radio:checked').val() == "true" ? true : false ;

 householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator = $('input[name=livesInUS]:radio:checked').val() == "true" ? true : false ;

 //suneel:05/09/2014
 //householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = $('input[name=honourably-veteran]').val()== "true" ? true : false ;
 householdMemberObj.citizenshipImmigrationStatus.honorablyDischargedOrActiveDutyMilitaryMemberIndicator = $('input[name=honourably-veteran]:radio:checked').val()== "true" ? true : false ;

 //suneel:05/09/2014
 //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = $('input[name=EligibleImmigrationStatuscheckbox]').is(':checked') == true  ? true : false;
 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationStatusIndicator = $('input[name=EligibleImmigrationStatuscheckbox]').is(':checked') == true  ? true : false;

 //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator = $('input[name=EligibleImmigrationStatus]:radio:checked').val() == true  ? true : false;
 /*	input-permcarddetails
	arrivaldeprecordForeignPassportexpdate
	arrivaldeprecordForeignppcardno
	arrivaldeprecordForeignSevisID
	arrivaldeprecordForeignCounty
	arrivaldeprecordForeignI94no
	arrivaldeprecordForeignppvisano*/

 var docType =  $('input[name=docType]:radio:checked').attr('id');

 if(docType == undefined) docType =  $('input[name=documentType]:radio:checked').attr('id');

 if(docType == "permcarddetails"){
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I551";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I551Indicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.cardNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());

 }
 else if(docType == "tempcarddetails"){
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "TempI551";

	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"Passportexpdate").val());
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
 }
 else if(docType == "machinecarddetails"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "MacReadI551";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].MachineReadableVisaIndicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = $("#"+docType+"ppvisano").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat( $("#"+docType+"dateOfExp").val());

	  // add passport expiration date later
	 }

 else if(docType == "empauthcarddetails"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I766";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I766Indicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.cardNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	 }
 else if(docType == "arrivaldeparturerecord"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I94";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94Indicator = true;  // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"I94no").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	 }
 else if(docType == "arrivaldeprecordForeign"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I94UnexpForeignPassport";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I94InPassportIndicator = true;
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = $("#"+docType+"ppvisano").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();
	 }
 else if(docType == "NoticeOfAction"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I797";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I797Indicator = true; // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94").val();
	 }
 else if(docType == "ForeignppI94"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "UnexpForeignPassport";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].UnexpiredForeignPassportIndicator = true;    // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();
	 }
 else if(docType == "rentrypermit"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I327";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I327Indicator = true;  // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	 }
 else if(docType == "refugeetraveldocI-571"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I571";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I571Indicator = true;   // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#"+docType+"AlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	 }
 else if(docType == "certificatenonimmigrantF1-Student-I20"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "I20";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].I20Indicator = true;   // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = $("#"+docType+"County").val();
	 }
 else if(docType == "certificatenonimmigrantJ1-Student-DS2019"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "DS2019";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].DS2019Indicator = true;  // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#"+docType+"SevisID").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#"+docType+"I94no").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#"+docType+"ppcardno").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#"+docType+"dateOfExp").val());
}
 else if(docType == "docType_other"){
	 householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "OtherCase1";
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].OtherDocumentTypeIndicator = true;  // remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#DocAlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = $("#DocI-94Number").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = $("#passportDocNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = DBDateformat($("#passportExpDate").val());
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId = $("#sevisIDNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentDescription = $("#documentDescription").val();
}
 else if(docType == "naturalizedCitizenNaturalizedIndicator"){
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "NaturalizationCertificate";

	  //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#naturalizationAlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId  = $("#naturalizationCertificateNumber").val();

 }
 else if(docType == "naturalizedCitizenNaturalizedIndicator2"){
	  householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentSelected= "CitizenshipCertificate";

	  //householdMemberObj.citizenshipImmigrationStatus.eligibleImmigrationDocumentType[0].TemporaryI551StampIndicator = true;// remove
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = $("#citizenshipAlignNumber").val();
	  householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.SEVISId  = $("#appscr62p1citizenshipCertificateNumber").val();

 }

// other documents
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORRCertificationIndicator =  $('#otherDocORR').is(':checked');
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.ORREligibilityLetterIndicator =  $('#otherDocORR-under18').is(':checked');
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.CubanHaitianEntrantIndicator =  $('#Cuban-or-HaitianEntrant').is(':checked');
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.WithholdingOfRemovalIndicator =  $('#docRemoval').is(':checked');
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.AmericanSamoanIndicator =  $('#americanSamaoresident').is(':checked');
 householdMemberObj.citizenshipImmigrationStatus.otherImmigrationDocumentType.StayOfRemovalIndicator =  $('#adminOrderbyDeptofHomelanSecurity').is(':checked');

// name same on the documents
 var isNameSameOnDocument;
 if($('#62Part2_UScitizenYes').is(':checked')){
	 isNameSameOnDocument = true;
 }else if($('#62Part2_UScitizen').is(':checked')){
	 isNameSameOnDocument = false;
 }else{
	 isNameSameOnDocument = null;
 }


 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameSameOnDocumentIndicator = isNameSameOnDocument;
 if(isNameSameOnDocument == false) {
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = $("#documentFirstName").val();
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = $("#documentMiddleName").val();
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = $("#documentLastName").val();
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix = $("#documentSuffix").val()=="0"?"":$("#documentSuffix").val();

 }
 else {
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = "";
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = "";
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = "";
	 householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.suffix = "";
 }




/*	input-permcarddetails
arrivaldeprecordForeignPassportexpdate
arrivaldeprecordForeignppcardno
arrivaldeprecordForeignSevisID
arrivaldeprecordForeignCounty
arrivaldeprecordForeignI94no
arrivaldeprecordForeignppvisano*/

//var checkboxid = $('input[name=UScitizenManualVerification]:radio:checked').attr('id');
var checkboxid = $('input[name=docType]:radio:checked').attr('id');





/*
var AlignNumber = $('#'+checkboxid+"AlignNumber").val();
var I94no = $('#'+checkboxid+"I94no").val();
var ppcardno = $('#'+checkboxid+"ppcardno").val();
var County = $('#'+checkboxid+"County").val();
var SevisID = $('#'+checkboxid+"SevisID").val();
var ppvisano = $('#'+checkboxid+"ppvisano").val();
var Passportexpdate = $('#'+checkboxid+"Passportexpdate").val();

householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber = AlignNumber;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number = I94no;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportOrDocumentNumber = ppcardno;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.foreignPassportCountryOfIssuance = County;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.sevisId = SevisID;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.visaNumber = ppvisano;
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.documentExpirationDate = Passportexpdate;*/








//webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;	 //suneel 05/09/2014
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;

/*	var inputvalues=[];
$('input[name=docType]:radio:checked').each(function(){
	  var type = $(this).attr("type");
        if ((type == "checkbox" || type == "radio") && $(this).is(":checked")) {
            inputValues.push($(this).val());
        }
});


$('input[name=docType]:radio:checked').parent().next('div').find('.controls').each(function(){
	$(this).child('input[type=text]').val();
});*/


//FOLLOWING DATA PROPERTIES NOT FOUND IN JSON

//var UScitizen = $('input[name=UScitizen]:radio:checked').val();
//var naturalizedCitizen = $('input[name=naturalizedCitizen]:radio:checked').val();
//var naturalizationAlignNumber = $('#naturalizationAlignNumber').val();
//var naturalizationCertificateNumber = $('#naturalizationCertificateNumber').val();
//var naturalizedDonotHave = $('#naturalizedDonotHave').is(':checked');
//var citizenshipAlignNumber = $('#citizenshipAlignNumber').val();
//var appscr62p1citizenshipCertificateNumber = $('#appscr62p1citizenshipCertificateNumber').val();
//var citizenDonotHave = $('#citizenDonotHave').is(':checked');
//var naturalCitizen_eligibleImmigrationStatus = $('#naturalCitizen_eligibleImmigrationStatus').is(':checked');
//var docType = $('input[name=docType]:radio:checked').val();
//var passportExpDate = $('#passportExpDate').val();
//var documentDescription = $('#documentDescription').text();
}

}

function continueData62Part2Json(){
if(useJSON == true){
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.alienNumber =$('#appscr62OtherDocAlignNumber').val();
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.i94Number =$('#appscr62P2I-94Number').val();
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.firstName = $('#documentFirstName').val();
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.middleName = $('#documentMiddleName').val();
householdMemberObj.citizenshipImmigrationStatus.citizenshipDocument.nameOnDocument.lastName = $('#documentLastName').val();
householdMemberObj.citizenshipImmigrationStatus.livedIntheUSSince1996Indicator = $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0]=householdMemberObj;

//FOLLOWING DATA PROPERTIES NOT FOUND IN JSON

//var docType1 = $('input[name=docType1]:radio:checked').val();

//var appscr62OtherComment = $('#appscr62OtherComment').text();
//var UScitizen62 = $('input[name=UScitizen62]:radio:checked').val();

//var documentSuffix = $('#documentSuffix').val();
//var livesInUS = $('input[name=livesInUS]:radio:checked').val();
//var livedIntheUSSince1996Indicator = $('input[name=livedIntheUSSince1996Indicator]:radio:checked').val();
}

}

function continueData63Json(currentMemember){

if(useJSON == true){

	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];

	if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val() == 'yes'){
		householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator = true;
	} else if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val() == 'no'){
		householdMemberObj.parentCaretakerRelatives.mainCaretakerOfChildIndicator = false;
	}

	//householdMemberObj.parentCaretakerRelatives.personId[0].personid =1 ;

	householdMemberObj.parentCaretakerRelatives.anotherChildIndicator = false;
	if($('#takeCareOf_anotherChild').is(':checked')){
		householdMemberObj.parentCaretakerRelatives.anotherChildIndicator = true;
	}

	householdMemberObj.parentCaretakerRelatives.name.firstName = $('#parent-or-caretaker-firstName').val();
	householdMemberObj.parentCaretakerRelatives.name.middleName = $('#parent-or-caretaker-middleName').val();
	householdMemberObj.parentCaretakerRelatives.name.lastName = $('#parent-or-caretaker-lastName').val();
	householdMemberObj.parentCaretakerRelatives.name.suffix = $('#parent-or-caretaker-suffix').val();
	householdMemberObj.parentCaretakerRelatives.dateOfBirth = DBDateformat($('#parent-or-caretaker-dob').val());
	householdMemberObj.parentCaretakerRelatives.relationship = $('#applicant-relationship').val();

	householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator = false;
	if($('input[name=birthOrAdoptiveParents]:radio:checked').val() == 'yes'){
		householdMemberObj.parentCaretakerRelatives.liveWithAdoptiveParentsIndicator = true;
	}

	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
}

}

function continueData64Json(currentID) {

if(useJSON == true){

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

	if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'yes'){
		householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = true;

		householdMemberObj.ethnicityAndRace.ethnicity = [];

		$('input:checkbox[name="ethnicity"]').each(function(){
			if($(this).is(':checked') && $(this).attr('id') != 'ethnicity_other'){

				householdMemberObj.ethnicityAndRace.ethnicity.push({
					'label' : $(this).closest('label').text().trim(),
					'code' : $(this).val()
				});
			}
		});

		//for other ethnicity
		if($('#ethnicity_other').is(':checked')){
			householdMemberObj.ethnicityAndRace.ethnicity.push({
				'label' : $('#otherEthnicity').val(),
				'code' : $('#ethnicity_other').val()
			});
		}



	}else if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'no'){
		householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = false;
		householdMemberObj.ethnicityAndRace.ethnicity = [];
	}


	householdMemberObj.ethnicityAndRace.race = [];
	$('input:checkbox[name="race"]').each(function(){
		if($(this).is(':checked') && $(this).attr('id') != 'race_other'){

			householdMemberObj.ethnicityAndRace.race.push({
				'label' : $(this).closest('label').text().trim(),
				'code' : $(this).val()
			});
		}
	});

	//for other ethnicity
	if($('#race_other').is(':checked')){
		householdMemberObj.ethnicityAndRace.race.push({
			'label' : $('#otherRace').val(),
			'code' : $('#race_other').val()
		});
	}

	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;


	}


/*if(useJSON == true){
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

var defaultJson = getDefatulJson(); // get default json
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
//Assign default value
householdMemberObj.ethnicityAndRace = defaultJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].ethnicityAndRace;


householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'yes')
	householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = true;
else if($('input[name=checkPersonNameLanguage]:radio:checked').val() == 'no')
	householdMemberObj.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator = false;

ethnicityCount=0;

for(var icount=0;icount<householdMemberObj.ethnicityAndRace.ethnicity.length;icount++){
	householdMemberObj.ethnicityAndRace.ethnicity.splice(icount);
}

if($('#ethnicity_cuban').is(':checked')){
	var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
	ethinicity.code=$('#ethnicity_cuban').val();
	ethinicity.label="cuban";
	householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
	ethnicityCount++;
}

if($('#ethnicity_maxican').is(':checked')){
	var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
	ethinicity.code=$('#ethnicity_maxican').val();
	ethinicity.label="mexican";
	householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
	ethnicityCount++;
}
if($('#ethnicity_puertoRican').is(':checked')){
	var ethinicity =  {"label" : "","code" : "", "otherLabel": ""};
	ethinicity.code=$('#ethnicity_puertoRican').val();
	ethinicity.label="puerto rican";
	householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
	ethnicityCount++;
}
if($('#ethnicity_other').is(':checked')){
	ethinicity.code=$('#other_ethnicity').val();
	ethinicity.label="other";
	householdMemberObj.ethnicityAndRace.ethnicity.push(ethinicity);
	ethnicityCount++;
}


raceCount=0;

for(var icount=0;icount<householdMemberObj.ethnicityAndRace.race.length;icount++){
	householdMemberObj.ethnicityAndRace.race.splice(icount);
}

if($('#americanIndianOrAlaskaNative').is(':checked')){

	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#americanIndianOrAlaskaNative').val();
	race.label="american indian or alaska native";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

if($('#asianIndian').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#asianIndian').val();
	race.label="asian indian";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

if($('#BlackOrAfricanAmerican').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#BlackOrAfricanAmerican').val();
	race.label="black or african american";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#chinese').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#chinese').val();
	race.label="chinese";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#filipino').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#filipino').val();
	race.label="filipino";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#guamanianOrChamorro').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#guamanianOrChamorro').val();
	race.label="guamanian or chamorro";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#japanese').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#japanese').val();
	race.label="japanese";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#korean').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#korean').val();
	race.label="korean";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

if($('#nativeHawaiian').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#nativeHawaiian').val();
	race.label="native hawaiian";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#otherAsian').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#otherAsian').val();
	race.label="other asian";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#otherPacificIslander').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#otherPacificIslander').val();
	race.label="other pacific islander";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

if($('#samoan').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#samoan').val();
	race.label="samoan";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#vietnamese').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#vietnamese').val();
	race.label="vietnamese";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}
if($('#whiteOrCaucasian').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#whiteOrCaucasian').val();
	race.label="white or caucasian";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

if($('#raceOther').is(':checked')){
	var race =  {"label" : "","code" : "", "otherLabel": ""};
	race.code=$('#other_race').val();
	race.label="other_race";
	householdMemberObj.ethnicityAndRace.race.push(race);
	raceCount++;
}

webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;
}*/

}

function continueData65Json() {
if(useJSON == true){

	var householdCount = getNoOfHouseHolds();
	var primaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];

	for(var i=2;i<=householdCount;i++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];

		//this applicant is living at other address
		if($('#householdContactAddress'+i).is(":checked")){

			householdMemberObj.livesAtOtherAddressIndicator	= true;
			householdMemberObj.livesWithHouseholdContactIndicator = false;

			householdMemberObj.otherAddress.address.streetAddress1 = $('#applicant_or_non-applican_address_1'+i).val();
			householdMemberObj.otherAddress.address.streetAddress2 = $('#applicant_or_non-applican_address_2'+i).val();
			householdMemberObj.otherAddress.address.city = $('#city'+i).val();
			householdMemberObj.otherAddress.address.state = $('#applicant_or_non-applican_state'+i +" option:selected").val();
			householdMemberObj.otherAddress.address.postalCode = $('#zip'+i).val();
			householdMemberObj.otherAddress.address.county=$('#applicant_or_non-applican_county'+i +" option:selected").val();

			householdMemberObj.householdContact.homeAddressIndicator = false;

			householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
			householdMemberObj.householdContact.homeAddress.streetAddress2 = "";
			householdMemberObj.householdContact.homeAddress.city = "";
			householdMemberObj.householdContact.homeAddress.state = "";
			householdMemberObj.householdContact.homeAddress.postalCode = "";
			householdMemberObj.householdContact.homeAddress.county="";


			temporaryLiving = $('input[name=isTemporarilyLivingOutsideIndicator'+i+']:radio:checked').val();
			if(temporaryLiving == 'yes'){
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = true;
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = $('#applicant2city'+i).val();
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = $('#applicant_or_non-applican_stateTemp'+i + " option:selected").val();
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = $('#applicant-2-zip'+i).val();

			} else if(temporaryLiving == 'no')  {
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
			}

		}else if(!$('#householdContactAddress'+i).is(":checked")){

			householdMemberObj.livesAtOtherAddressIndicator	= false;
			householdMemberObj.livesWithHouseholdContactIndicator = true;

			householdMemberObj.householdContact.homeAddress.streetAddress1 = primaryContactObj.householdContact.homeAddress.streetAddress1;
			householdMemberObj.householdContact.homeAddress.streetAddress2 = primaryContactObj.householdContact.homeAddress.streetAddress2;
			householdMemberObj.householdContact.homeAddress.city = primaryContactObj.householdContact.homeAddress.city;
			householdMemberObj.householdContact.homeAddress.state = primaryContactObj.householdContact.homeAddress.state;
			householdMemberObj.householdContact.homeAddress.postalCode = primaryContactObj.householdContact.homeAddress.postalCode;
			householdMemberObj.householdContact.homeAddress.county=primaryContactObj.householdContact.homeAddress.county;

			householdMemberObj.householdContact.homeAddressIndicator = true;

			householdMemberObj.otherAddress.address.streetAddress1 = "";
			householdMemberObj.otherAddress.address.streetAddress2 = "";
			householdMemberObj.otherAddress.address.city = "";
			householdMemberObj.otherAddress.address.state = "";
			householdMemberObj.otherAddress.address.postalCode = "";
			householdMemberObj.otherAddress.address.county="";

			householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;
			householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
			householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
			householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
		}

		//mailling address will be always the same with PC's
		householdMemberObj.householdContact.mailingAddress.streetAddress1 = primaryContactObj.householdContact.mailingAddress.streetAddress1;
		householdMemberObj.householdContact.mailingAddress.streetAddress2 = primaryContactObj.householdContact.mailingAddress.streetAddress2;
		householdMemberObj.householdContact.mailingAddress.city = primaryContactObj.householdContact.mailingAddress.city;
		householdMemberObj.householdContact.mailingAddress.state = primaryContactObj.householdContact.mailingAddress.state;
		householdMemberObj.householdContact.mailingAddress.postalCode = primaryContactObj.householdContact.mailingAddress.postalCode;
		householdMemberObj.householdContact.mailingAddress.county=primaryContactObj.householdContact.mailingAddress.county;

		webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1]=householdMemberObj;
	}



/*var x=$('#noOfPeople53').val();

for(var y=1;y<=x;y++){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];
	var fname = householdMemberObj.name.firstName;
	var mname = householdMemberObj.name.middleName;
	var lname = householdMemberObj.name.lastName;

	var name = fname + "_" + mname + "_" + lname;

	//placeholders for address information
	var addressLine1=$('#home_addressLine1').val();
	var addressLine2=$('#home_addressLine2').val();
	var city = $('#home_primary_city').val();
	var zip=$('#home_primary_zip').val();
	var state=$('#home_primary_state').val();
	var secondCity="";
	var secondZip="";
	var secondState="";
	var livesAtOtherAddressIndicator=false;
	var temporaryLiving="no";

	householdMemberObj.householdContact.homeAddressIndicator = true;
	if(y!=1){
	if(!$('#householdContactAddress'+y).is(":checked")){
		applicantObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
		householdMemberObj.householdContact.homeAddress.streetAddress1 = applicantObj.householdContact.homeAddress.streetAddress1;
		householdMemberObj.householdContact.homeAddress.streetAddress2 = applicantObj.householdContact.homeAddress.streetAddress2;
		householdMemberObj.householdContact.homeAddress.city = applicantObj.householdContact.homeAddress.city;
		householdMemberObj.householdContact.homeAddress.state = applicantObj.householdContact.homeAddress.state;
		householdMemberObj.householdContact.homeAddress.postalCode = applicantObj.householdContact.homeAddress.postalCode;
		householdMemberObj.householdContact.homeAddress.county=applicantObj.householdContact.homeAddress.county;
		householdMemberObj.householdContact.homeAddressIndicator = true;

		householdMemberObj.otherAddress.address.streetAddress1 = "";
		householdMemberObj.otherAddress.address.streetAddress2 = "";
		householdMemberObj.otherAddress.address.city = "";
		householdMemberObj.otherAddress.address.state = "";
		householdMemberObj.otherAddress.address.postalCode = "";
		householdMemberObj.otherAddress.address.county="";
		householdMemberObj.livesWithHouseholdContactIndicator = true;

	}

	if($('#householdContactAddress'+y).is(":checked")) //this applicant is living at other address
		{
			householdMemberObj.otherAddress.address.streetAddress1 = $('#applicant_or_non-applican_address_1'+y).val();
			householdMemberObj.otherAddress.address.streetAddress2 = $('#applicant_or_non-applican_address_2'+y).val();
			householdMemberObj.otherAddress.address.city = $('#city'+y).val();
			householdMemberObj.otherAddress.address.state = $('#applicant_or_non-applican_state'+y +" option:selected").val();
			householdMemberObj.otherAddress.address.postalCode = $('#zip'+y).val();
			householdMemberObj.otherAddress.address.county="";
			householdMemberObj.householdContact.homeAddressIndicator = false;

			householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
			householdMemberObj.householdContact.homeAddress.streetAddress2 = "";
			householdMemberObj.householdContact.homeAddress.city = "";
			householdMemberObj.householdContact.homeAddress.state = "";
			householdMemberObj.householdContact.homeAddress.postalCode = "";
			householdMemberObj.householdContact.homeAddress.county="";

			livesAtOtherAddressIndicator= true;
			temporaryLiving = $('input[name=isTemporarilyLivingOutsideIndicator'+y+']:radio:checked').val();

			householdMemberObj.livesWithHouseholdContactIndicator = false;

			if(temporaryLiving == 'yes'){
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = true;
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = $('#applicant2city'+y).val();
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = $('#applicant_2_state'+y + " option:selected").text();
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = $('#applicant-2-zip'+y).val();

			} else if(temporaryLiving == 'no')  {
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyIndicator = false;
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.city = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.state = "";
				householdMemberObj.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode = "";
			}
		}
	}
	householdMemberObj.livesAtOtherAddressIndicator = livesAtOtherAddressIndicator;
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;

}*/
}
}
function continueData66Json () {

resetData66Json();
if(useJSON == true){

//var x=$('#noOfPeople53').val();
var x =	getNoOfHouseHolds();

if($("#disabilityNoneOfThese").is(":checked")){
	noneOftheseDisabilityIndicator = true;
}else{
	noneOftheseDisabilityIndicator = false;
}

if($("#assistanceServicesNoneOfThese").is(":checked")){
	noneOftheseAssistanceServices = true;
}else{
	noneOftheseAssistanceServices = false;
}

if($("#nativeNoneOfThese").is(":checked")){
	noneOftheseAmericanIndianAlaskaNativeIndicator = true;
}else{
	noneOftheseAmericanIndianAlaskaNativeIndicator = false;
}

if($("#personfosterCareNoneOfThese").is(":checked")){
	noneOftheseEverInFosterCareIndicator = true;
}else{
	noneOftheseEverInFosterCareIndicator = false;
}

if($("#deceasedPersonNoneOfThese").is(":checked")){
	noneoftheseDeceased = true;
}else{
	noneoftheseDeceased = false;
}

if(document.getElementById('pregnancyNoneOfThese')!=null && document.getElementById('pregnancyNoneOfThese').checked){
	noneofthesePregnancy = true;
}else{
	noneofthesePregnancy = false;
}

for(var y=1;y<=x;y++){

	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];

	  var disabled=false;
	  if($('#disability'+y).is(":checked")){
		  disabled=true;
	  }
	  /*
	  var assistanceServcices="no";
	  if($('#assistanceServices'+y).is(":checked")){
		  assistanceServices="yes";
	  }*/
	  var tribeNative=false;
	  var tribeName="";
	  var tribeState="";
	  var tribeFullName = "";
	  var stateFullName = "";
	  if($('#native'+y).is(":checked")){
		  tribeNative=true;
	  }
	  var fosterCare=false;
	  if($('#personfosterCare'+y).is(":checked")){
		  fosterCare=true;
	  }
	  /*var deceased="no";
	  if($('#deceased'+y).is(":checked")){

		  deceased="yes";

	  }
	  var medicaid="no";
	  if($('input[name=isPersonGettingHealthCareYes'+i+']:radio:checked').val()=="yes"){
		  medicaid="yes";
	  }*/

	  var pregnancy=false;
	  var numberBabiesExpectedInPregnancy = 0;
	  if($('#pregnancy'+y).is(":checked")){
		  pregnancy=true;
		  numberBabiesExpectedInPregnancy = $('#number-of-babies'+y).val();
	  }
	  var americanIndianAlaskaNative = false;
	  if(tribeNative==true){
		  if($('input[name=AmericonIndianQuestionRadio'+y+']:radio:checked').val()=="yes"){
			  americanIndianAlaskaNative  = true;
			  tribeName = $('#tribeName'+y).val();
			  tribeState = $('#americonIndianQuestionSelect'+y).val();
			  tribeFullName = $('#tribeName'+y+' option:selected').text();
			  stateFullName = $('#americonIndianQuestionSelect'+y+' option:selected').text();
		  }
	  }
	/*  if(document.getElementById('appscr78Medicare'+y).checked){
		  householdMemberObj.healthCoverage.currentOtherInsurance.medicareEligibleIndicator = true;
	  }

	  if(document.getElementById('appscr78Tricare'+y).checked){
		  householdMemberObj.healthCoverage.currentOtherInsurance.tricareEligibleIndicator = true;
	  }

	  if(document.getElementById('appscr78PeaceCorps'+y).checked){
		  householdMemberObj.healthCoverage.currentOtherInsurance.peaceCorpsIndicator = true;
	  }

	  if(document.getElementById('appscr78OtherStateFHBP'+y).checked){
		  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramIndicator = true;
	  }

	  if(document.getElementById('appscr78None'+y).checked){
	  	householdMemberObj.healthCoverage.currentOtherInsurance.nonofTheAbove = true;
	  }*/

	  var fedHealthVal = $("input:radio[name=fedHealth"+y+"]:checked").val();
	  if(fedHealthVal == "None"){
		  householdMemberObj.healthCoverage.otherInsuranceIndicator = false;
		  householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = "";

		  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
		  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
	  }else{
		  householdMemberObj.healthCoverage.otherInsuranceIndicator = true;
		  householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = fedHealthVal;

		  if(fedHealthVal == "Other"){
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = $("#appscr78TypeOfProgram" + y).val();
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = $("#appscr78NameOfProgram" + y).val();
		  }else{
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
			  householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
		  }

	  }

	  householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator = tribeNative;
	  householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator = americanIndianAlaskaNative;
	  if(americanIndianAlaskaNative == true){
		  householdMemberObj.americanIndianAlaskaNative.state = tribeState;
		  householdMemberObj.americanIndianAlaskaNative.tribeName = tribeName;
		  householdMemberObj.americanIndianAlaskaNative.tribeFullName = tribeFullName;
		  householdMemberObj.americanIndianAlaskaNative.stateFullName = stateFullName;
	  }
	  householdMemberObj.specialCircumstances.fosterChild = fosterCare;
	  householdMemberObj.disabilityIndicator = disabled;
	  householdMemberObj.specialCircumstances.pregnantIndicator = pregnancy;
	  householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy = numberBabiesExpectedInPregnancy;
	  webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;
}
}

}

function continueData69Json(currentID){
if(useJSON == true){
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
householdMemberObj.expeditedIncome.irsReportedAnnualIncome=null;
householdMemberObj.expeditedIncome.irsReportedAnnualIncome=getDecimalNumber(cleanMoneyFormat($('#expeditedIncome69').val()));

webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;
  $("#Financial_None").prop('checked', false);
  $("#appscr70radios4IncomeYes").prop('checked', false);
  $("#appscr70radios4IncomeNo").prop('checked', false);
  $("#incomeSourceDiv").hide();
  $.each(incomesecion, function(key, value) {
	    $("#"+key).prop('checked', false);
	});
}
}

function continueData70Json(currentID){

householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
householdMemberObj.detailedIncome.jobIncomeIndicator = true;
if( $('#Financial_Job').is(':checked') == false){
	householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes = null;
	householdMemberObj.detailedIncome.jobIncomeIndicator = null;
	householdMemberObj.detailedIncome.jobIncome[0].employerName = "";
	householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency = "";
	householdMemberObj.detailedIncome.jobIncome[0].workHours = null;
}
householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator = true;
if( $('#Financial_Self_Employment').is(':checked') == false){
	householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome = null;
	householdMemberObj.detailedIncome.selfEmploymentIncomeIndicator = null;
	householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork = "";
	//householdMemberObj.detailedIncome.selfEmploymentIncome.incomeFrequency="";
}
householdMemberObj.detailedIncome.socialSecurityBenefitIndicator = true;
if( $('#Financial_SocialSecuritybenefits').is(':checked') == false){
	householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount = null;
	householdMemberObj.detailedIncome.socialSecurityBenefitIndicator = null;
	householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency = "";
}
householdMemberObj.detailedIncome.unemploymentBenefitIndicator = true;
if( $('#Financial_Unemployment').is(':checked') == false)  {
	householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount = null;
	householdMemberObj.detailedIncome.unemploymentBenefitIndicator = null;
	householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName = "";
	householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency = "";
	householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate = "";
}
householdMemberObj.detailedIncome.retirementOrPensionIndicator = true;
if( $('#Financial_Retirement_pension').is(':checked') == false) {
	householdMemberObj.detailedIncome.retirementOrPension.taxableAmount = null;
	householdMemberObj.detailedIncome.retirementOrPensionIndicator = null;
	householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency = "";
}
householdMemberObj.detailedIncome.capitalGainsIndicator = true;
if( $('#Financial_Capitalgains').is(':checked') == false) {
	householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains = null;
	householdMemberObj.detailedIncome.capitalGainsIndicator = null;
	//householdMemberObj.detailedIncome.capitalGains.incomeFrequency = "";

}
householdMemberObj.detailedIncome.investmentIncomeIndicator = true;
if( $('#Financial_InvestmentIncome').is(':checked') == false) {
	householdMemberObj.detailedIncome.investmentIncome.incomeAmount = null;
	householdMemberObj.detailedIncome.investmentIncomeIndicator = null;
	householdMemberObj.detailedIncome.investmentIncome.incomeFrequency = "";
}
householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator = true;
if( $('#Financial_RentalOrRoyaltyIncome').is(':checked') == false){
	householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount = null;
	householdMemberObj.detailedIncome.rentalRoyaltyIncomeIndicator = null;
	householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency = ""
}
householdMemberObj.detailedIncome.farmFishingIncomeIndictor = true;
if( $('#Financial_FarmingOrFishingIncome').is(':checked') == false) {
	householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount = null;
	householdMemberObj.detailedIncome.farmFishingIncomeIndictor = null;
	householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency = "";
}
householdMemberObj.detailedIncome.alimonyReceivedIndicator = true;
if( $('#Financial_AlimonyReceived').is(':checked') == false) {
	householdMemberObj.detailedIncome.alimonyReceived.amountReceived = null;
	householdMemberObj.detailedIncome.alimonyReceivedIndicator = null;
	householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency = "";
}
householdMemberObj.detailedIncome.otherIncomeIndicator = true;
if( $('#Financial_OtherIncome').is(':checked') == false)  {
	householdMemberObj.detailedIncome.otherIncomeIndicator = null;
	householdMemberObj.detailedIncome.otherIncome[0].incomeAmount = null;
	householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency = "";
	householdMemberObj.detailedIncome.otherIncome[1].incomeAmount = null;
	householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency = "";
	householdMemberObj.detailedIncome.otherIncome[2].incomeAmount = null;
	householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency = "";
}

if( $('#Financial_None').is(':checked') == true)  {

	householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency = "";
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency = "";
	householdMemberObj.detailedIncome.deductions.otherDeductionAmount = null;
	householdMemberObj.detailedIncome.deductions.otherDeductionFrequency = "";
}


webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;

$('#incomesStatus'+currentID).text("No");

$.each(incomesecion, function(key, value) {
    incomesecion[key] = false;
    if($('#'+key).is(":checked") == true)
	{
    	incomesecion[key] = true;
    	$('#incomesStatus'+currentID).text("Yes");
	}
});





//Nothing to do here....this is just for knowing how many incomes there.

//householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

}
function continueData71Json(currentID){

if(useJSON == true){

//if($('#Financial_Job_select_id').val() != ""){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
	householdMemberObj.detailedIncome.jobIncome[0].employerName=$('#Financial_Job_EmployerName_id').val();
	householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes=getDecimalNumber(cleanMoneyFormat($('#Financial_Job_id').val()));
	householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency=$('#Financial_Job_select_id').val();
	householdMemberObj.detailedIncome.jobIncome[0].workHours=getIntegerNumber($('#Financial_Job_HoursOrDays_id').val());
//}

//if($('#Financial_Self_Employment_id').val() != ""){
	householdMemberObj.detailedIncome.selfEmploymentIncome.typeOfWork=$('#Financial_Self_Employment_TypeOfWork').val();
	householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome=cleanMoneyFormat($('#Financial_Self_Employment_id').val());
	//householdMemberObj.detailedIncome.selfEmploymentIncome.incomeFrequency="Monthly";
//}
//if($('#Financial_SocialSecuritybenefits_id').val() != ""){
	householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount=cleanMoneyFormat($('#Financial_SocialSecuritybenefits_id').val());
	householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency=$('#Financial_SocialSecuritybenefits_select_id').val();
//}
//if($('#Financial_Unemployment_select_id').val() != ""){
	householdMemberObj.detailedIncome.unemploymentBenefit.stateGovernmentName=$('#Financial_Unemployment_StateGovernment').val();
	householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount=cleanMoneyFormat($('#Financial_Unemployment_id').val());
	householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency=$('#Financial_Unemployment_select_id').val();
	householdMemberObj.detailedIncome.unemploymentBenefit.unemploymentDate=DBDateformat($('#unemploymentIncomeDate').val());
//}
//if($('#Financial_Retirement_pension_id').val() != ""){
	householdMemberObj.detailedIncome.retirementOrPension.taxableAmount=cleanMoneyFormat($('#Financial_Retirement_pension_id').val());
	householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency=$('#Financial_Retirement_pension_select_id').val();
//}

householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains=cleanMoneyFormat($('#Financial_Capitalgains_id').val());
//householdMemberObj.detailedIncome.capitalGains.incomeFrequency="Yearly";
//if($('#Financial_InvestmentIncome_id').val() != ""){
	householdMemberObj.detailedIncome.investmentIncome.incomeAmount=cleanMoneyFormat($('#Financial_InvestmentIncome_id').val());
	householdMemberObj.detailedIncome.investmentIncome.incomeFrequency=($('#Financial_InvestmentIncome_select_id').val());
//}

//if($('#Financial_RentalOrRoyaltyIncome_id').val() != ""){
	householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount=cleanMoneyFormat($('#Financial_RentalOrRoyaltyIncome_id').val());
	householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency=$('#Financial_RentalOrRoyaltyIncome_select_id').val();
//}

//if($('#Financial_FarmingOrFishingIncome_id').val() != ""){
	householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount=cleanMoneyFormat($('#Financial_FarmingOrFishingIncome_id').val());
	householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency=$('#Financial_FarmingOrFishingIncome_select_id').val();
//}
//if($('#Financial_AlimonyReceived_id').val() != ""){
	householdMemberObj.detailedIncome.alimonyReceived.amountReceived=cleanMoneyFormat($('#Financial_AlimonyReceived_id').val());
	householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency=$('#Financial_AlimonyReceived_select_id').val();
//}

	if( $('#appscr71CanceleddebtsCB').is(':checked') == true){
		householdMemberObj.detailedIncome.otherIncome[0].incomeAmount=cleanMoneyFormat($('#cancelled_Debts_amount').val());
		householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency=$('#cancelledDebtsFrequency').val();
		householdMemberObj.detailedIncome.otherIncome[0].otherIncomeTypeDescription='Canceled debts';
	}

	if( $('#appscr71CourtAwardsCB').is(':checked') == true){
		householdMemberObj.detailedIncome.otherIncome[1].incomeAmount=cleanMoneyFormat($('#court_awards_amount').val());
		householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency=$('#courtAwrdsFrequency').val();
		householdMemberObj.detailedIncome.otherIncome[1].otherIncomeTypeDescription='Court Awards';
	}

	if( $('#appscr71JuryDutyPayCB').is(':checked') == true){
		householdMemberObj.detailedIncome.otherIncome[2].incomeAmount=cleanMoneyFormat($('#jury_duty_pay_amount').val());
		householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency=$('#juryDutyPayFrequency').val();
		householdMemberObj.detailedIncome.otherIncome[2].otherIncomeTypeDescription='Jury duty pay';
	}


webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;

	/*var Financial_OtherTypeIncome = $('#Financial_OtherTypeIncome').val();
	var Financial_OtherIncome_id = $('#Financial_OtherIncome_id').val();
	var Financial_OtherIncome_select_id = $('#Financial_OtherIncome_select_id').val();*/
	//var Financial_OtherTypeIncome = $('#cancelled_Debts_amount').val();
	//var Financial_OtherIncome_id = $('#court_awards_amount').val();
	//var Financial_OtherIncome_select_id = $('#jury_duty_pay_amount').val();

	setTotalFamilyIncomeJSON(); // Count and set total family annual income
}

}

function continueData72Json(currentID){

if(useJSON == true){

householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

householdMemberObj.detailedIncome.deductions.deductionType[0] = "ALIMONY";
householdMemberObj.detailedIncome.deductions.deductionType[1] = "STUDENT_LOAN_INTEREST";
householdMemberObj.detailedIncome.deductions.deductionType[2] = $('#deductionTypeInput').val();

householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount = null;
householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency = "";

if($('#appscr72alimonyPaid').is(':checked')){
	householdMemberObj.detailedIncome.deductions.alimonyDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#alimonyAmountInput').val()));
	householdMemberObj.detailedIncome.deductions.alimonyDeductionFrequency=$('#alimonyFrequencySelect').val();
}
householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount = null;
householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency = "";
if($('#appscr72studentLoanInterest').is(':checked')){
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#studentLoanInterestAmountInput').val()));
	householdMemberObj.detailedIncome.deductions.studentLoanDeductionFrequency=$('#studentLoanInterestFrequencySelect').val();
}
householdMemberObj.detailedIncome.deductions.otherDeductionAmount = null;
householdMemberObj.detailedIncome.deductions.otherDeductionFrequency = "";
if($('#appscr72otherDeduction').is(':checked')){
	householdMemberObj.detailedIncome.deductions.otherDeductionAmount=getDecimalNumber(cleanMoneyFormat($('#deductionAmountInput').val()));
	householdMemberObj.detailedIncome.deductions.otherDeductionFrequency=$('#deductionFrequencySelect').val();
}

householdMemberObj.expeditedIncome.expectedIncomeVaration = null;
householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = null;
if($('input[name=aboutIncomeSpped]:radio:checked').val() == 'yes')
	householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = true;
else if($('input[name=aboutIncomeSpped]:radio:checked').val() == 'no')
	householdMemberObj.expeditedIncome.expectedAnnualIncomeUnknownIndicator = false;

if($('input[name=incomeHighOrLow]:radio:checked').val() == 'higher')
	householdMemberObj.expeditedIncome.expectedIncomeVaration =  'higher';
else if($('input[name=incomeHighOrLow]:radio:checked').val() == 'lower')
	householdMemberObj.expeditedIncome.expectedIncomeVaration = 'lower';

webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1]=householdMemberObj;

//var appscr72incomeNone = $('#appscr72incomeNone').is(':checked');
//var aboutIncomeSpped = $('input[name=aboutIncomeSpped]:radio:checked').val();

//var incomeHighOrLow = $('input[name=incomeHighOrLow]:radio:checked').val();
}
}

function continueData73Json(currentID){

householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];

if($('input[name=stopWorking]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator = true;
} else if ($('input[name=stopWorking]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.stopWorkingAtEmployerIndicator = false;
}

if($('input[name=workAt]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator = true;
} else if ($('input[name=workAt]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.didPersonEverWorkForEmployerIndicator = false;
}

if($('input[name=decresedAt]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator = true;
} else if ($('input[name=decresedAt]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.hasHoursDecreasedWithEmployerIndicator = false;
}

if($('input[name=salaryAt]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator = true;
} else if ($('input[name=salaryAt]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.hasWageOrSalaryBeenCutIndicator = false;
}
householdMemberObj.detailedIncome.discrepancies.explanationForJobIncomeDiscrepancy = $('#explanationTextArea').val();
if($('input[name=sesWorker]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator = true;
} else if ($('input[name=sesWorker]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.seasonalWorkerIndicator = false;
}
householdMemberObj.detailedIncome.discrepancies.explanationForDependantDiscrepancy = $('#appscr73ExplanationTaxtArea').val();
if($('input[name=OtherJointIncome]:radio:checked').val() == 'yes'){
	householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator = true;
} else if ($('input[name=OtherJointIncome]:radio:checked').val() == 'no'){
	householdMemberObj.detailedIncome.discrepancies.otherAboveIncomeIncludingJointIncomeIndicator = false;
}
}

function appscr58EditUdateJson(currentMemember){
if(useJSON == true){
var cnt = currentMemember;
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
householdMemberObj.name.firstName = trimSpaces($('#appscr57FirstName'+cnt).val());
householdMemberObj.name.middleName = trimSpaces($('#appscr57MiddleName'+cnt).val());
householdMemberObj.name.lastName = trimSpaces($('#appscr57LastName'+cnt).val());
householdMemberObj.name.suffix = $('#appscr57Suffix'+cnt).val();
householdMemberObj.dateOfBirth = DBDateformat($('#appscr57DOB'+cnt).val());
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
}

}

function continueData76P1Json(currentMemember){
if(useJSON == true){

	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];

	householdMemberObj.healthCoverage.isOfferedHealthCoverageThroughJobIndicator =  $('input[name=isHealthCoverageThroughJob]:radio:checked').val() == "yes" ? true:false;
	householdMemberObj.healthCoverage.employerWillOfferInsuranceIndicator =  $('input[name=willHealthCoverageThroughJob]:radio:checked').val() == "yes" ? true:false;
	householdMemberObj.healthCoverage.employerWillOfferInsuranceStartDate = DBDateformat($('#appscr76P1HealthCoverageDate').val());
	householdMemberObj.healthCoverage.haveBeenUninsuredInLast6MonthsIndicator =  $('input[name=wasUninsuredFromLast6Month]:radio:checked').val() == "yes" ? true:false;


	if($('#appscr76P1CheckBoxForEmployeeName').is(':checked')){

		householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactPersonName = $('#appscr76P1EmployerContactName').val();
		householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneNumber = $('#appscr76P1_firstPhoneNumber').val().replace(/\D+/g, "");
		householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneExtension = $('#appscr76P1_firstExt').val();
		householdMemberObj.healthCoverage.currentEmployer[0].employer.phone.phoneType = $('#appscr76P1_firstPhoneType').val();
		householdMemberObj.healthCoverage.currentEmployer[0].employer.employerContactEmailAddress = $('#appscr76P1_emailAddress').val();
	}

	if($('#appscr76P1CheckBoxForOtherEmployee').is(':checked')){

		householdMemberObj.healthCoverage.formerEmployer[0].employerName = $('#appscr76P1_otherEmployerName').val();
		householdMemberObj.healthCoverage.formerEmployer[0].employerIdentificationNumber = $('#appscr76P1_employerEIN').val();
		householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress1 = $('#appscr76P1_otherEmployerAddress1').val();
		householdMemberObj.healthCoverage.formerEmployer[0].address.streetAddress2 = $('#appscr76P1_otherEmployerAddress2').val();
		householdMemberObj.healthCoverage.formerEmployer[0].address.city = $('#appscr76P1_otherEmployerCity').val();
		householdMemberObj.healthCoverage.formerEmployer[0].address.state = $('#appscr76P1_otherEmployerState').val();
		householdMemberObj.healthCoverage.formerEmployer[0].address.zipCode = $('#appscr76P1_otherEmployerZip').val();

		householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneNumber = $('#appscr76P1_secondPhoneNumber').val().replace(/\D+/g, "");
		householdMemberObj.healthCoverage.formerEmployer[0].phone.phoneExtension = $('#appscr76P1_secondExt').val();

		householdMemberObj.healthCoverage.formerEmployer[0].employerContactPersonName = $('#appscr76P1_otherEmployerContactName').val();
		householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneNumber = $('#appscr76P1_thirdPhoneNumber').val().replace(/\D+/g, "");
		householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneExtension = $('#appscr76P1_thirdExt').val();
		householdMemberObj.healthCoverage.formerEmployer[0].employerContactPhone.phoneType = $('#appscr76P1_thirdPhoneType').val();
		householdMemberObj.healthCoverage.formerEmployer[0].employerContactEmailAddress = $('#appscr76P1_otherEmployer_emailAddress').val();
	}

	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
}

}

function continueData76P2Json(currentMemember){
if(useJSON == true){

	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];

	householdMemberObj.healthCoverage.currentlyEnrolledInCobraIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "corba" ? true:false;
	householdMemberObj.healthCoverage.currentlyEnrolledInRetireePlanIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "retire health plan" ? true:false;
	householdMemberObj.healthCoverage.currentlyEnrolledInVeteransProgramIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "veterans health program" ? true:false;
	householdMemberObj.healthCoverage.currentlyEnrolledInNoneIndicator =  $('input[name=appscr76p2radiosgroup]:radio:checked').val() == "none" ? true:false;

	householdMemberObj.healthCoverage.willBeEnrolledInCobraIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "corba" ? true:false;
	householdMemberObj.healthCoverage.willBeEnrolledInRetireePlanIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "retiree health plan" ? true:false;
	householdMemberObj.healthCoverage.willBeEnrolledInVeteransProgramIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "veterans health plans" ? true:false;
	householdMemberObj.healthCoverage.willBeEnrolledInNoneIndicator =  $('input[name=willBeEnrolledInHealthCoverageInFollowingYear]:radio:checked').val() == "none" ? true:false;

	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator = $('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;

	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
}

}

function continueData77Part1Json(currentMemember){
if(useJSON == true){

	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];

	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.isCurrentlyEnrolledInEmployerPlanIndicator = $('input[name=77P1CurrentlyEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;
	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.willBeEnrolledInEmployerPlanIndicator = $('input[name=77P1WillBeEnrolledInHealthPlan]:radio:checked').val() == "yes" ? true:false;

	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;
	/*
	 * Following data not available in json
	 *
	var appscr77P1WillBeEnrolledInHealthPlanDate = $('#77P1WillBeEnrolledInHealthPlanDate').val();
	var appscr77P1ExpectChangesToHealthCoverage = $('input[name=77P1ExpectChangesToHealthCoverage]:radio:checked').val();
	var appscr77P1WillNoLongerHealthCoverage = $('#77P1WillNoLongerHealthCoverage').is(':checked');
	var appscr77P1NoLongerHealthCoverageDate = $('#77P1NoLongerHealthCoverageDate').val();
	var appscr77P1PlanToDropHealthCoverage = $('#77P1PlanToDropHealthCoverage').is(':checked');
	var appscr77P1PlanToDropHealthCoverageDate = $('#77P1PlanToDropHealthCoverageDate').val();
	var appscr77P1WillOfferCoverage = $('#77P1WillOfferCoverage').is(':checked');
	var appscr77P1WillOfferCoverageDate = $('#77P1WillOfferCoverageDate').val();
	var appscr77P1PlaningToEnrollInHC = $('#77P1PlaningToEnrollInHC').is(':checked');
	var appscr77P1PlaningToEnrollInHCDate = $('#77P1PlaningToEnrollInHCDate').val();
	var appscr77P1HealthPlanOptionsGoingToChange = $('#appscr77P1HealthPlanOptionsGoingToChange').is(':checked');
	var appscr77P1HealthPlanOptionsGoingToChangeDate = $('#77P1HealthPlanOptionsGoingToChangeDate').val();*/

}

}

function continueData77Part2Json(currentMemember){
if(useJSON == true){

	var cnt = currentMemember;
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];


	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentLowestCostSelfOnlyPlanName = $('#currentLowestCostSelfOnlyPlanName').val();
	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.currentPlanMeetsMinimumStandardIndicator = $('#currentPlanMeetsMinimumStandard').is(':checked') == true  ? false : true;

	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingLowestCostSelfOnlyPlanName = $('#comingLowestCostSelfOnlyPlanName').val();
	householdMemberObj.healthCoverage.currentEmployer[0].currentEmployerInsurance.comingPlanMeetsMinimumStandardIndicator = $('#comingPlanMeetsMinimumStandard').is(':checked') == true ? false : true;


	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;

	/*
	 * Following data not found in json
	 * var lowestCostSelfOnlyPremiumAmount = $('#lowestCostSelfOnlyPremiumAmount').val();
	var lowestCostSelfOnlyPremiumFrequency = $('#lowestCostSelfOnlyPremiumFrequency').val();
	var appscr77p2IsAffordable = $('input[name=appscr77p2IsAffordable]:radio:checked').val();
	*/

}

}


function openJsonTab(){
something = window.open("data:text/json," + encodeURIComponent(JSON.stringify(webJson)), "_blank");
something.focus();
return false;
}

function cleanMoneyFormat(strMoney){
if(strMoney == null || strMoney == "") return null;
strMoney= strMoney.replace(/[&\/\\#,+()$~%'":*?<>{}]/g,'');
return getDecimalNumber(strMoney);
//return strMoney.replace(/[^0-9]/g,'');
}
/**********************************************************************************************************************************************/

/**************************************From Here start DATA Reset functions*********************************************/

/**********************************************************************************************************************************************/

function resetData53Json(){

householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
householdMemberObj.name.firstName = "";
householdMemberObj.name.middleName = "";
householdMemberObj.name.lastName = "";
householdMemberObj.name.suffix = "";
householdMemberObj.dateOfBirth = "";

householdMemberObj.householdContact.contactPreferences.emailAddress = "";
householdMemberObj.householdContact.contactPreferences.preferredSpokenLanguage = "";
householdMemberObj.householdContact.contactPreferences.preferredWrittenLanguage = "";

householdMemberObj.householdContact.contactPreferences.preferredContactMethod = "";

householdMemberObj.householdContact.homeAddressIndicator = false;
householdMemberObj.householdContact.homeAddress.city = "";
householdMemberObj.householdContact.homeAddress.county = "";
householdMemberObj.householdContact.homeAddress.postalCode = "";
householdMemberObj.householdContact.homeAddress.state = "";
householdMemberObj.householdContact.homeAddress.streetAddress1 = "";
householdMemberObj.householdContact.homeAddress.streetAddress2 = "";

householdMemberObj.householdContact.mailingAddressSameAsHomeAddressIndicator = true;
householdMemberObj.householdContact.mailingAddress.city = "";
householdMemberObj.householdContact.mailingAddress.county = "";
householdMemberObj.householdContact.mailingAddress.postalCode = "";
householdMemberObj.householdContact.mailingAddress.state = "";
householdMemberObj.householdContact.mailingAddress.streetAddress1 = "";
householdMemberObj.householdContact.mailingAddress.streetAddress2 = "";

householdMemberObj.householdContact.phone.phoneNumber = "";
householdMemberObj.householdContact.phone.phoneExtension = "";
householdMemberObj.householdContact.phone.phoneType = "CELL";

householdMemberObj.householdContact.otherPhone.phoneNumber = "";
householdMemberObj.householdContact.otherPhone.phoneExtension = "";
householdMemberObj.householdContact.otherPhone.phoneType = "HOME";
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0] = householdMemberObj;

}

function resetData54Json(){
var helpType = webJson.singleStreamlinedApplication.helpType;
BrokerObj=webJson.singleStreamlinedApplication.broker;
BrokerObj.brokerFederalTaxIdNumber = "";
BrokerObj.brokerName = "";
webJson.singleStreamlinedApplication.broker = BrokerObj;
assisterObj=webJson.singleStreamlinedApplication.assister;
assisterObj.assisterID = "";
assisterObj.assisterName = "";
webJson.singleStreamlinedApplication.broker = assisterObj;
webJson.singleStreamlinedApplication.authorizedRepresentativeIndicator = null;
webJson.singleStreamlinedApplication.getHelpIndicator = null;

AuthorizedRepresentativeObj = webJson.singleStreamlinedApplication.authorizedRepresentative;
AuthorizedRepresentativeObj.name.firstName = "";
AuthorizedRepresentativeObj.name.middleName = "";
AuthorizedRepresentativeObj.name.lastName = "";
AuthorizedRepresentativeObj.name.suffix = "";
AuthorizedRepresentativeObj.emailAddress ="";
AuthorizedRepresentativeObj.address.streetAddress1 = "";
AuthorizedRepresentativeObj.address.streetAddress2 = "";
AuthorizedRepresentativeObj.address.city = "";
AuthorizedRepresentativeObj.address.state = "";
AuthorizedRepresentativeObj.address.postalCode = "";

AuthorizedRepresentativeObj.phone[0].phoneNumber = "";
AuthorizedRepresentativeObj.phone[0].phoneExtension = "";
AuthorizedRepresentativeObj.phone[1].phoneNumber = "";
AuthorizedRepresentativeObj.phone[1].phoneExtension = "";
AuthorizedRepresentativeObj.phone[2].phoneNumber = "";
AuthorizedRepresentativeObj.phone[2].phoneExtension = "";

AuthorizedRepresentativeObj.partOfOrganizationIndicator = null;
AuthorizedRepresentativeObj.companyName = "";
AuthorizedRepresentativeObj.organizationId = "";
AuthorizedRepresentativeObj.partOfOrganizationIndicator = null;
AuthorizedRepresentativeObj.signatureisProofIndicator = null;
AuthorizedRepresentativeObj.signature = "";

webJson.singleStreamlinedApplication.authorizedRepresentative=AuthorizedRepresentativeObj;
}
function resetData55Json(){
webJson.singleStreamlinedApplication.applyingForhouseHold = null;
webJson.singleStreamlinedApplication.applyingForFinancialAssistanceIndicator = null;
}

function resetData60Json(i){
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
householdMemberObj.planToFileFTRIndicator = null;
householdMemberObj.marriedIndicator = null;
householdMemberObj.planToFileFTRJontlyIndicator = null;
householdMemberObj.taxFiler.liveWithSpouseIndicator = null;
householdMemberObj.taxFiler.claimingDependantsOnFTRIndicator = null;
householdMemberObj.taxFilerDependant.taxFilerDependantIndicator = null;
//householdMemberObj.applyingForCoverageIndicator = false; // HIX-50485 - According to Anmol applyingForCoverageIndicator should not be reset.
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1] = householdMemberObj;
}
function resetData61Json(currentID){
householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1];
householdMemberObj.gender = "";
householdMemberObj.socialSecurityCard.socialSecurityCardHolderIndicator = null;
//householdMemberObj.socialSecurityCard.socialSecurityNumber = "";
householdMemberObj.socialSecurityCard.nameSameOnSSNCardIndicator = null;
householdMemberObj.socialSecurityCard.firstNameOnSSNCard = "";
householdMemberObj.socialSecurityCard.middleNameOnSSNCard = "";
householdMemberObj.socialSecurityCard.lastNameOnSSNCard = "";
householdMemberObj.socialSecurityCard.suffixOnSSNCard = "";
householdMemberObj.socialSecurityCard.reasonableExplanationForNoSSN = "";
householdMemberObj.socialSecurityCard.individualMandateExceptionIndicator = null;
householdMemberObj.socialSecurityCard.individualManadateExceptionID = null;
webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[currentID-1] = householdMemberObj;
}
function resetData66Json () {
var x =	getNoOfHouseHolds();
for(var y=1;y<=x;y++){
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1];
	householdMemberObj.healthCoverage.otherInsuranceIndicator = null;
	householdMemberObj.healthCoverage.currentOtherInsurance.currentOtherInsuranceSelected = "";
	householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramType = "";
	householdMemberObj.healthCoverage.currentOtherInsurance.otherStateOrFederalProgramName = "";
	householdMemberObj.specialCircumstances.americanIndianAlaskaNativeIndicator = null;
	householdMemberObj.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator = null;
	householdMemberObj.americanIndianAlaskaNative.state = "";
	householdMemberObj.americanIndianAlaskaNative.tribeName = "";
	householdMemberObj.specialCircumstances.fosterChild = false;
	householdMemberObj.disabilityIndicator = false;
	householdMemberObj.specialCircumstances.pregnantIndicator = false;
	householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy = 0;
	webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[y-1]=householdMemberObj;
}
}


/*
****************************************************************************************************************
* ssap-zipcode.js                                                                                     *
****************************************************************************************************************
*/

/*template XHTML for address worksite*/
var zipIdFocused="";
var application = {
	/*remove the modal*/
	remove_modal:function(){

		/*console.log('remove_modal');*/
		$('#modal,.modal-backdrop,#addressProgressPopup,#check-address-error').remove();
		if($('#primary-contact-information-form').is(':visible')){
			$('#modal, .modal-backdrop').remove();
			$('#check-address-error, #address-failure-box, #suggestion-box, #addressProgressPopup').remove();
		}
	},
	/*reset county on all the callback from validAddressNew function*/
	resetCountyOnAllCallback : function(){
		var zipIdTarget = zipIdFocused;

		/*console.log(zipIdTarget,'get zipIdFocused value remove modal');*/

		var	formId = $(parent.document.forms).attr('id'),
			parentElement = jQuery('#'+zipIdTarget).parents('.addressBlock').find('.zipCode'),
			zipCodeVal = parentElement.val(),
			zipId = parentElement.attr('id'),
			indexValue = zipId.split('_');

		/*console.log(formId,'-----',parentElement,'-----1-----',zipCodeVal,'---2----',zipId,'----3---',indexValue);*/
		//jQuery('#'+zipIdTarget).parents('.addressBlock').find('.control-group .controls').find('input:text').eq(0).focus();

		application.populateCounty(zipId, indexValue, zipCodeVal);
	},
	setValueInFormForSplEnroll:function(){
		$('#suggestion-box').hide();
		$('#form-input-data').show();
	},
	/*return back to the input FORM div after after error from the SAME PAGE*/
	back_button_to_input_screen : function(zipIdFocus){
		application.modal_cross_button();
		$('#back_to_input_modal').bind('click',function(){
			/*console.log('close this modal');*/
			$('#check-address-error, #address-failure-box,#addressProgressPopup').hide();
			application.remove_modal();
			application.resetCountyOnAllCallback();
			//jQuery('#'+zipIdFocused).parents('.addressBlock').find('.control-group .controls').find('input:text').eq(0).focus();
		});
	},
	/*return back to the input FORM div from IFRAME WINDOW*/
	/*back_button_from_iframe:function(modalId){
		$(modalId.document).find('#back_button_from_iframe').bind('click',function(){
			if($('#editdependent').is(':visible')){
				$('input#firstName').focus();
				$('#check-address-error, #address-failure-box, #suggestion-box').hide();
				$('#form-input-data').show();
				if($('#address1_0').length){
					$('#address1_0').focus();
				}
			}else{
				$('#check-address-error, #address-failure-box, #suggestion-box').hide();
				application.remove_modal();
			}
		});
	},*/
	back_button_from_iframe_worksite:function(modalId){
		/*console.log("new zip code iframe window", modalId, $(modalId.document),$(modalId.document).find('#back_button_from_iframe'));*/

		/*var formTextBoxId = jQuery('#form-input-data').find('input[type="text"]:first').attr('id');*/
		/*console.log('formTextBoxIdformTextBoxId----',formTextBoxId,$(modalId.document));*/

		$(modalId.document).find('#back_button_from_iframe').bind('click',function(e){
			application.remove_modal();
			application.resetCountyOnAllCallback();
		});
	},
	modal_cross_button:function(modalId){
		/*console.log('modal cross button', modalId, $(modalId.document), $('.closeModal'));*/
		$('.closeModal').bind('click',function(e){
			application.remove_modal();
			application.resetCountyOnAllCallback();
		});
	},
	populateCounty: function(zipid, indexValue, zipCodeVal){
		/*console.log(zipid, indexValue, zipCodeVal,'zipcode js function',$('#'+zipid).val());*/
		if($('#'+zipid).val().length >= 5){
			/*console.log('zip code lenght greater than 5 ' + indexValue.length);
			console.log('index value before',indexValue[1]);*/
			parent.postPopulateIndex(zipCodeVal, zipid);
		}
	},
	/*SUBMIT FUNCTION inside IFRAME*/
	submit_button_of_iframe:function(buttonId){
		/*console.log('modal zipcode',button);*/
		var idss = $('#ids').val();
		var retVal=idss.split("~");

		if ($("input:radio:checked").val() == undefined){
			alert('Please select one address.');
			return false;
		}
		/*get the value of CHECKED RADIO*/
		var actualVal=$("input:radio:checked").val().split(","),
			selectedState = $("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val();

		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[0]).val(actualVal[0]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[2]).val($.trim(actualVal[2]));
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[3]).val(selectedState);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[4]).val($.trim(actualVal[4]));
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[5]).val(actualVal[5]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[6]).val(actualVal[6]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[7]).val(actualVal[7] == undefined ? "" : actualVal[7]);
		$("#modalData", top.document).parents('#frmworksites').find('#'+retVal[8]).val(actualVal[8] == undefined ? "" : actualVal[8]);
		/*trigger the BACK button*/
		$('#'+buttonId).parent().find("#back_button_from_iframe").trigger('click');
		parent.getCountyList($('#'+ zip_e).val(), '');
	},
	/*FOCUS OUT function on ZIP*/
	zipcode_focusout: function(e, that){

		var startindex = (e.target.id).indexOf("_");
		var index = (e.target.id).substring(0,startindex);

		var address1_e='_addressLine1'; var address2_e='_addressLine2'; var city_e= '_primary_city'; var state_e='_primary_state'; var zip_e='_primary_zip';
		var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
		/*check for the underscore in the id of the focus out event of the zipcode. for current scenarios id is "zip_0. hence startindex  ===3 "*/
		if (startindex== 4 || startindex ==7 )
		{
			address1_e=index+address1_e;
			address2_e=index+address2_e;
			city_e=index+city_e;
			state_e=index+state_e;
			zip_e=index+zip_e;
			lat_e=index+lat_e;
			lon_e=index+lon_e;
			rdi_e+=index;
			county_e+=index;
		}

		var model_address1 = address1_e + '_hidden' ;
		var model_address2 = address2_e + '_hidden' ;
		var model_city = city_e + '_hidden' ;
		var model_state = state_e + '_hidden' ;
		var model_zip = zip_e + '_hidden' ;
		//parent.getCountyList($('#'+ zip_e).val(), '');

		var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
		if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
			if(($('#'+ address2_e).val())==="Address Line 2"){
				$('#'+ address2_e).val('');
			}
		viewValidAddressListNew(index, $('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(),
				$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),
				idsText);
	/*		viewValidAddressListNew($('#home_addressLine1').val(),$('#home_addressLine2').val(),$('#home_primary_city').val(),$('#home_primary_state').val(),$('#home_primary_zip').val(),
					$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),
					idsText);*/

			zipIdFocused = $(e.target).attr('id');
			//console.log(zipIdFocused,'1111');
		}
	},
	/*RESET the content in the FORM*/
	input_content_screen_reset: function(that){
    	$('.errorAddress, .errorAddressBtn, .suggestedAddress').remove();
    	$('#modal').find('.modal-header h3').text('Additional Worksite');
    	$('#modal-inputs,#input-footer').show();
    },
    /*VALIDATE the FORM elements*/
    add_Address_function:function(){
    	$("#frmworksites").validate({
            errorClass: "error",
            errorPlacement: function(error, element) {
        		var elementId = element.attr('id');
        		error.appendTo( $("#" + elementId + "_error"));
        		$("#" + elementId + "_error").attr('class','error help-inline');
            },
            rules: {
            	'locations[0].location.address1': {
                    required: true
                },
                'locations[0].location.city': {
                    required: true
                },
                'locations[0].location.state': {
                    required: true
                },
                'locations[0].location.zip': {
                    required: true,
                    zipcheck : true,
                },
                'locations[0].location.county': {
                    required: true
                }
            }
        });

        /*error message for address1*/
        $('input[name="locations[0].location.address1"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter address.</span>"
            }
        });
        /*error message for city*/
        $('input[name="locations[0].location.city"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter city.</span>"
            }
        });
        /*error message for state*/
        $('*[name="locations[0].location.state"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter city.</span>"
            }
        });

        /*error message for zip*/
        $('input[name="locations[0].location.zip"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter zip code.</span>",
                zipcheck: "<span><em class='excl'>!</em> Please enter a valid zip code.</span>"
            }
        });

        /*error message for county*/
        $('*[name="locations[0].location.county"]').rules('add', {
            messages: {
                required: "<span> <em class='excl'>!</em> Please enter a county.</span>"
            }
        });

        /*check the value of ZIP*/
        jQuery.validator.addMethod("zipcheck", function(value, element, param) {
          	elementId = element.id;
          	var numzip = new Number($("#"+elementId).val());
            var filter = /^[0-9]+$/;
            if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
          		return false;
          	}return true;
        });
    },
    /*populate STATE DROPDOWN inside MODAL on show*/
    populate_state_dropdown:function(){
    	////console.log($('#jsonStateValue').val())
    	var stateObj = $('#jsonStateValue').val(),
    		stateJson = $.parseJSON(stateObj);

    	var defaultStateValue = $('#defaultStateValue').val();
    	$.each(stateJson, function(index, item){
    		if(item.code === defaultStateValue){
    			$('<option selected="selected" value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
    		}else{
    			$('<option value='+[item.code]+'>'+[item.name]+'</option>').appendTo("select[name='locations[0].location.state']");
    		}
    	});
    },
    /*RESIZE the IFRAME HEIGHT*/
    iframeHeight:function(){
    	/*console.log('modal zipcode',$(window.top.document).find("iframe")[0].contentDocument.body.scrollHeight);*/
    	/*setTimeout(function() {
    		$('#modalData').height($(window.top.document).find("iframe")[0].contentDocument.body.scrollHeight);
    	}, 1000);*/
    	$('#modalData').height(290);
    	$('#modalData').width('100%');
    },
    /*populate LOCATION on PARENT PAGE*/
    populate_location_on_parent_page: function(empDetailsJSON, contactlocationid){
		if($.browser.msie && $.browser.version <= 8){
			for(var i=0;i<empDetailsJSON.length;i++){
				var obj = empDetailsJSON[i];
				primarylocationid = obj['id'];
			}

			var selected = (obj['id'] == contactlocationid) ? contactlocationid : primarylocationid,
				selectLocationId = (locationval != undefined || locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];

			$('#location').prepend('<option value="'+selectLocationId+'" ' +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
			$('#location').val(selected);
		}else{
			for(var i=0;i<empDetailsJSON.length;i++){
				var obj = empDetailsJSON[i];
				var selected = '';
				if(contactlocationid == 0){
					if(obj['primary'] == 'YES'){
						selected = 'selected="selected"';
					}
				}else if(obj['id'] == contactlocationid){
					selected = 'selected="selected"';
				}

				var selectLocationId = (locationval != undefined && locationval == 'LOCATIONID' ) ? obj['id'] : obj['employer_location_id'];
                $('#location').prepend('<option value="'+selectLocationId+'" '+ selected +'>'+obj['address1']+", "+obj['city']+", "+obj['state']+'</option>');
			}
		}
    },
    /*ADDRESS Failure Message*/
    address_failure_message:function(message){
    	$('#address-failure-box').remove();
    	var addresFailureMessage = '<div id="address-failure-box"><div class="modal-header clearfix"><h3 class="pull-left">Confirm Address</h3>'+
    		'<button type="button" class="close closeModal">&times;</button></div>'+
			'<div class="modal-body"><div class="suggestedAddress">'+message+'</div></div>'+
			'<div class="modal-footer"><input type="button" class="btn errorAddressBtn" aria-hidden="true" id="back_to_input_modal" value="OK" /></div></div>';


		$('body').append(addresFailureMessage);
		$('#address-failure-box').addClass('modal popup-address');
		application.back_button_to_input_screen(zipIdFocused);

    },
    ssp_address_success_modal:function(){
    	$('#editdependent').hide();
    	$('#suggestion-box').show();
    },
    ssp_dependent_modal:function(){
    	$('#editdependent').show();
    	$('#suggestion-box').hide();
    }
};



/*FOCUS OUT event of ZIP CODE*/
/*$('.zipCode').live('focusout',function(e){
	application.zipcode_focusout(e, $(this));
});*/

$('body').on('change', '.addressValidation',function(){
	$(this).addClass('dirty');
});

$('body').on('blur', '.addressValidation.dirty',function(e){
	$(this).removeClass('dirty');
	application.zipcode_focusout(e);
});

/*$('.addressValidation').live('focusout',function(e){
	application.zipcode_focusout(e);
});*/


$('.removeModal').live('click',function(){
	/*console.log('zipcode util');*/
	application.remove_modal();
});

/*
****************************************************************************************************************
* ssap-zipcode-utils.js                                                                                     *
****************************************************************************************************************
*/

/**
 * viewValidAddressListNew is a javascript function. This performs AJAX call to validate address and refreshes the content on JSP.
 *
 * <p> Address Modal gets openup only for matching records returned from actual web service.
 *
 * Developer needs to invoke this from address section of JSP or modal by passing below params.
 * <p> All are mandatory.
 *
 * @param address1, value in the form
 * @param address2, value in the form
 * @param city, value in the form
 * @param state, value in the form
 * @param zip, value in the form
 *
 * @param model_address1, value fetched from appServer(db)
 * @param model_address2, value fetched from appServer(db)
 * @param model_city, value fetched from appServer(db)
 * @param model_state, value fetched from appServer(db)
 * @param model_zip, value fetched from appServer(db)
 *
 * @param ids - Should be ids for address1, address2, city, state, zip fields from JSP and be ~ separated.
 * 				Also there should be hidden fields ids for lat, lon, county and rdi.
 * 				see example:- address1_business~address2_business~city_business~state_business~zip_business~lat_business~lon_business~rdi_business~county_business
 */
function viewValidAddressListNew(addressType, address1, address2, city, state, zip, model_address1, model_address2, model_city, model_state, model_zip,ids) {
	
	if (!address1 || !city || !state || !zip){
		return;
	}else{
		var enteredAddress = {
			addressLine1 : address1,
			addressLine2 : address2,
			zipcode : zip,
			city: city,
			state: state
		};
		
		
		$.ajax({
			async: false,
			url: "/hix/address/validate",
			type: "POST",
		    data: JSON.stringify(enteredAddress),
		    contentType: "application/json; charset=utf-8",
		    dataType: "json",
		    headers: {'csrftoken': csrftoken},
		    success: function(response){
		    	addressValidationCallback(response, addressType, enteredAddress);
		    }
		});
	}
}


function addressValidationCallback(response, addressType, enteredAddress){
		
	//exact Match
	if(response.exactMatch){
		/*if(addressType.toUpperCase() === 'HOME'){
			ssapHomeAddressValidationFlag = true;
		}else{
			ssapMailingAddressValidationFlag = true;
		}*/
		
	//invalid
	}else if(response.suggestions === null){
		/*if(addressType.toUpperCase() === 'HOME'){
			ssapHomeAddressValidationFlag = false;
		}else{
			ssapMailingAddressValidationFlag = false;
		}*/
		var enteredAddressValue = enteredAddress.addressLine1 + ','+ enteredAddress.addressLine2 + ',' + enteredAddress.city + ',' + enteredAddress.state + ',' + enteredAddress.zipcode;
		var enteredAddressLabel = enteredAddress.addressLine1 +  enteredAddress.addressLine2 + ', ' + enteredAddress.city + ', ' + enteredAddress.state + ' ' + enteredAddress.zipcode;
		var enteredAddressHtml = '<label class="radio margin20-l"><input type="radio" checked name="addressSelect" value="' + enteredAddressValue + '">' + enteredAddressLabel + '</label>';
		$('#addressNotFoundModal .enteredAddress').html(enteredAddressHtml);
		
		$('#addressNotFoundModal').modal({
			keyboard: false,
			backdrop: 'static'
		});
	//valid but not exact Match	
	}else{
		
		/*if(addressType.toUpperCase() === 'HOME'){
			ssapHomeAddressValidationFlag = true;
		}else{
			ssapMailingAddressValidationFlag = true;
		}*/
		var enteredAddressValue = enteredAddress.addressLine1 + ','+ enteredAddress.addressLine2 + ',' + enteredAddress.city + ',' + enteredAddress.state + ',' + enteredAddress.zipcode;
		var enteredAddressLabel = enteredAddress.addressLine1 +  enteredAddress.addressLine2 + ', ' + enteredAddress.city + ', ' + enteredAddress.state + ' ' + enteredAddress.zipcode;
		var enteredAddressHtml = '<label class="radio margin20-l"><input type="radio" name="addressSelect" value="' + enteredAddressValue + '">' + enteredAddressLabel + '</label>';
		$('#addressMatchModal .enteredAddress').html(enteredAddressHtml);
		
		var suggestedAddressHtml = '';
		for(var i = 0; i < response.suggestions.length; i++){
			var suggestedAddressValue = response.suggestions[i].addressLine1 + ','+ response.suggestions[i].addressLine2 + ',' + response.suggestions[i].city + ',' + response.suggestions[i].state + ',' + response.suggestions[i].zipcode;
			var suggestedAddressLabel = response.suggestions[i].addressLine1 + (response.suggestions[i].addressLine2 === null? '' : response.suggestions[i].addressLine2) + ', ' + response.suggestions[i].city + ', ' + response.suggestions[i].state + ' ' + response.suggestions[i].zipcode;
			
			if(i === 0){
				suggestedAddressHtml += '<p class="margin10-l"><label class="radio margin20-l"><input name="addressSelect" type="radio" checked value="' + suggestedAddressValue + '">' + suggestedAddressLabel + '</label></p>';
			}else{
				suggestedAddressHtml += '<p class="margin10-l"><label class="radio margin20-l"><input name="addressSelect" type="radio" value="' + suggestedAddressValue + '">' + suggestedAddressLabel + '</label></p>';
			}
			
			var errorMessagesHtml = getErrorMessagesHtml(response.suggestions[i]);
			
			suggestedAddressHtml+= errorMessagesHtml;
		}
		$('#addressMatchModal .suggestedAddress').html(suggestedAddressHtml);
		

		$('#addressMatchModal .updateAddress').unbind('click');

		$('#addressMatchModal .updateAddress').click(function(){
			var addressSelected = $('#addressMatchModal [name=addressSelect]:checked').val().split(',');
						
			if(addressType.toUpperCase() === 'HOME'){
				$('#home_addressLine1').val(addressSelected[0]);
				if(addressSelected[1].toUpperCase() !== 'NULL'){
					$('#home_addressLine2').val(addressSelected[1]);
				}	
				$('#home_primary_city').val(addressSelected[2]);
				$('#home_primary_state').val(addressSelected[3]);
				
				
				var oldZip = $('#home_primary_zip').val();
				
				if(oldZip !== addressSelected[4]){
					getCountyList(addressSelected[4], '', 'home_primary_zip');
				}
				
				$('#home_primary_zip').val(addressSelected[4]);
				
			}else{
				$('#mailing_addressLine1').val(addressSelected[0]);
				if(addressSelected[1].toUpperCase() !== 'NULL'){
					$('#mailing_addressLine2').val(addressSelected[1]);
				}
				$('#mailing_primary_city').val(addressSelected[2]);
				$('#mailing_primary_state').val(addressSelected[3]);
				
				var oldZip = $('#mailing_primary_zip').val();
				var isZipChanged = (oldZip != addressSelected[4]);
				$('#mailing_primary_zip').val(addressSelected[4]);
				if(isZipChanged){
					getCountyListForState('mailing_primary_zip','mailing_primary_state',''); 
				}
			
			}
		});

								
		$('.addressMatchModal').modal({
			keyboard: false,
			backdrop: 'static'
		});
		
	};
}

function updateAddressClick(){
	
}


function getErrorMessagesHtml(suggestion){
	var addressValidationErrorMessages = getAddressValidationErrorMessage(suggestion);						
	var errorMessagesHtml = '';
	for(var i = 0; i < addressValidationErrorMessages.length; i++){
		errorMessagesHtml += '<p class="alert alert-error">' + addressValidationErrorMessages[i] + '</p>';
	}
	
	return errorMessagesHtml;
}

function getAddressValidationErrorMessage(suggestion){
	addressValidationErrorMessages = [];
	
	for(var index in suggestion){
		var errorMessage = '';
		if(suggestion[index] === true){
			switch(index) {
				case 'redundantSecondaryNumber':
					errorMessage = 'The secondary information (apartment, suite, etc.) does not match that on the record. Please check accuracy of the information.';
					break;
				case 'zipcodeCorrected':
					errorMessage = 'The address submitted has a different 5-digit ZIP. The correct ZIP Code is shown in the ZIP Code field.';
					break;
				case 'cityStateSpellingFixed':
					errorMessage = 'The spelling of the city name and/or state abbreviation has been updated with standard spelling or abbreviation.';
					break;
				case 'zipInvalid':
					errorMessage = 'The ZIP Code in the submitted address could not be found. Please check the accuracy of the submitted address.';
					break;
				case 'addressNotFound':
					errorMessage = 'The address, exactly as submitted, could not be found. Please verify the entire address is correct.';
					break;
				case 'missingSecondaryNumber':
					errorMessage = 'Our information indicates that this address is a multi-unit building. Please include apartment or suite number.';
					break;
				case 'insufficientAddressData':
					errorMessage = 'More than one record was found to satisfy the address as submitted. Please check the accuracy and completeness of the submitted address.';
					break;
				case 'fixedStreetSpelling':
					errorMessage = 'The spelling of the street name was changed in order to achieve a match.';
					break;	
				case 'fixedAbreviations':
					errorMessage = 'The mailing address you entered has been standardized. For example, if you entered STREET, our system would standardize that to ST.';
					break;
				case 'invalidSecondaryAddress':
					errorMessage = 'The secondary (apartment, suite, etc.)  information, is not valid. Please check accuracy of address.';
					break;
				case 'cityStateZipComboInvalid':
					errorMessage = 'The address, submitted, could not be found in the city, state, or ZIP Code provided.';
					break;
				case 'invalidDeliveryAddress':
					errorMessage = 'The address submitted is identified as a "small town default." The USPS does not deliver to this address. Please provide an address serviced by the post office.';
					break;	
				case 'otherReason':
					errorMessage = 'The submitted address did not contain complete or correct data to determine a single record. Please check the accuracy and completeness of the submitted address.';
					break;
				default:
					errorMessage = 'The submitted address did not contain complete or correct data to determine a single record. Please check the accuracy and completeness of the submitted address.';
				}
			
			addressValidationErrorMessages.push(errorMessage);
		}
	}
	
	return addressValidationErrorMessages;
}



function getNextApplicantId() {
	var applicantCounter = parseInt($("#applicantCounter").val());
	$("#applicantCounter").val(++ applicantCounter);
	return parseInt($("#applicantCounter").val());
}


function mailingAddressHandler(){
	saveData53();loadData54();
}

function triggerMailingAddressValidator(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}/*else if(ssapHomeAddressValidationFlag === false || ssapMailingAddressValidationFlag === false){
		$('#addressNotFoundModal').modal({
			keyboard: false,
			backdrop: 'static'
		});
	}*/else{
		saveData53();loadData54();
	}
	/*if($('#mailingAddressIndicator').is(':checked')){
		mailingAddressHandler();
	}else{
		$('#mailing_primary_zip').trigger("validateAddress");
	}*/
}




//added for mailing address validation 

 function addressSelected(){
	var address  = $('#addressModal input:radio:checked').val();
	if(address == undefined){
		alert("Please select address.");
		return;
	}
	hideModal();
	var addressArr = $('#addressModal input:radio:checked').val().split(",");
	var prefix = $('#addressModal').find('#modalAddressOk').attr('idPrefix');
	$("#"+prefix+"_addressLine1").val($.trim(addressArr[0]));
	$("#"+prefix+"_addressLine2").val($.trim(addressArr[1]));
	$("#"+prefix+"_primary_city").val($.trim(addressArr[2]));
	$("#"+prefix+"_primary_zip").val($.trim(addressArr[4]));
	if(addressArr.length>8){
		getCountyListForAddress($("#"+prefix+"_primary_zip").val(), $.trim(addressArr[8]), prefix+"_primary_zip");
	}else{
		eval($('#addressModal').find('#modalAddressOk').attr('callBackMethod'));
	}
	
}
 
 function countyLoaded(){
	 eval($('#addressModal').find('#modalAddressOk').attr('callBackMethod'));
 }


 function addressSameSelected(callBackMethod){
	hideModal();
	eval(callBackMethod);
}


function hideModal(){
	$('#addressModal').modal('hide');
	//$('.modal-backdrop').remove();
	//$('body').removeClass('modal-open');
}




//$('#modalOk').on('click',addressSelected);

function showProgress(){
	var imgpath = '/hix/resources/img/ajax_loader_blue_128.gif';
	$('#addressProgressPopup, .modal-backdrop').remove(); //remove wait popup
	/*lock the screen and inform user about validation 'Please wait... while we validate your address...'*/
	$('<div class="modal popup-address" id="addressProgressPopup"><div class="modal-header" style="border-bottom:0; "><h4 class="margin0">Please wait... while we validate your address...</h4></div><div class="modal-body center" style="max-height:470px; padding: 10px 0px;"><img src=\"' + imgpath + '\"></div></div>').modal({backdrop:"static",keyboard:false});
}

function stopProgress(){
	$('#addressProgressPopup').modal('hide');
	$('#addressProgressPopup').remove();
}



function addressValidatorEvent(event){
	var href = '/hix/platform/address/viewvalidaddressnew';
	//var href = '/hix/ssap/addressmatch?_=' + (new Date()).getTime();
	 showProgress();
	var callBackMethod = $(event.currentTarget).attr('callBackMethod');
	var startindex = (this.id).indexOf("_");
	var prefix = (this.id).substring(0,startindex);
	var enteredAddress = $("#"+prefix+"_addressLine1").val()+","+$("#"+prefix+"_addressLine2").val()+","+$("#"+prefix+"_primary_city").val()+","+$("#"+prefix+"_primary_state").val()+","+$("#"+prefix+"_primary_zip").val();
	var ids = prefix+"_addressLine1"+"~"+prefix+"_addressLine2"+"~"+prefix+"_primary_city"+"~"+prefix+"_primary_state"+"~"+prefix+"_primary_zip"+"~"+prefix+"lat"+"~"+prefix+"lon"+prefix+"rdi"+"~"+prefix+"county";
	$.get('/hix/platform/validateaddressnew?enteredAddress=' + enteredAddress+'&ids=' + ids
		).done(function(data) {
			var retVal=data.split("~");
			stopProgress();
			zipIdFocused = prefix+"_primary_zip";
			if(data == 'SUCCESS'){	
				//add to the call (new Date()).getTime() to clear cache in IE
				$.get(href,function(response){
					var html = $(response).find("#frmviewAddressList").html(); 
					$('#addressModal .modal-body').html(html);
					$('#addressModal').find('#submitAddr').remove();
					$('#addressModal').find('#back_button_from_iframe').remove();
					$('#addressModal').find('#modalAddressOk').attr('idPrefix',prefix);
					$('#addressModal').find('#modalAddressOk').attr('callBackMethod',callBackMethod);
					
					$('#addressModal').modal({
						keyboard: false,
						backdrop: 'static'
					});
					
				});
				
				}else if(retVal[0]=="FAILURE"){	
					/*console.log('FAILURE');*/
					$('#addressErrorModal .modal-body').html('<p>'+retVal[1]+'</p>');
					$('#addressErrorModal').modal();
				}else if(retVal[0]=="MATCH_FOUND"){
					addressSameSelected(callBackMethod);
				}
		}).fail(function() {
			$('#addressProgressPopup').modal('hide');
			$('#addressProgressPopup').remove();
			alert('Unable to validate address!');
		  })
	
}
