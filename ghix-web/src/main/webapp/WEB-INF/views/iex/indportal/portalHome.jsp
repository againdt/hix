<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>
<style>
div.modal-backdrop.fade.in + div.modal-backdrop.fade.in {
	z-index: 1046;
}
</style>
<script type="text/ng-template" id="portalHome">
<div class="multi-year" ng-init="showAgentPromo(); showReveiwPrefModal(${noOfDaysRemainToReviewPref})">
	<div spinning-loader="loader"></div>
	<ul class="nav nav-tabs multi-year-tab">
		<c:if test="${showPreviousYearTab == true}">
			<li <c:if test="${coverageYear == previousCoverageYear}">class="active"</c:if>><a href="javascript:void(0)" ng-click="getIndividualPortalPage(${previousCoverageYear})">${previousCoverageYear}</a></li>
		</c:if>
		<c:if test="${showCurrentYearTab == true}">
			<li <c:if test="${coverageYear == currentCoverageYear}">class="active"</c:if>><a href="javascript:void(0)" ng-click="getIndividualPortalPage(${currentCoverageYear})">${currentCoverageYear}</a></li>
        </c:if>
		<sec:accesscontrollist hasPermission="IND_PORTAL_SHOW_RENEWAL_TAB" domainObject="user">
		<c:if test="${showRenewalTab == true}">
			<li <c:if test="${coverageYear == renewalCoverageYear}">class="active"</c:if>><a href="javascript:void(0)" ng-click="getIndividualPortalPage(${renewalCoverageYear})">${renewalCoverageYear}</a></li>
        </c:if>
		</sec:accesscontrollist>
	</ul>
	<div class="multi-year-content">
	<div ng-if="${notifyCurrentYear=='true' && displayYear != null}">
        <span class="alert alert-info left span12">
        	<spring:message code="indportal.portalhome.notifyUserYearTab1" javaScriptEscape="true"/><b>${displayYear}</b>
			<spring:message code="indportal.portalhome.notifyUserYearTab2" javaScriptEscape="true"/><b>${displayYear}</b>
			<spring:message code="indportal.portalhome.notifyUserYearTab3" javaScriptEscape="true"/>
		</span>
    </div>
	<div ng-if="${notifyPrevYear=='true' && displayYear != null}">
        <span class="alert alert-info left span12">
        	<spring:message code="indportal.portalhome.notifyUserYearTab1" javaScriptEscape="true"/><b>${displayYear}</b>
			<spring:message code="indportal.portalhome.notifyUserYearTab2" javaScriptEscape="true"/><b>${displayYear}</b>
			<spring:message code="indportal.portalhome.notifyUserYearTab3" javaScriptEscape="true"/>
		</span>
    </div>
  <div class="row-fluid bubble-nav">

    </div>


    <div class="well">
        <div class="well-step" style="border-top: 1px solid #86C48C;">
          <!-- Enrollment Alert -->
          <div class="gutter20 margin20-b" <c:if test="${isInsideOe =='false' && isOeApproaching=='false' }">style="display:none"</c:if> ng-show="${enableSep=='false'  && enableQep=='false'}">
            <!--show before enrollment -->
            <p class="alert alert-info center span12" <c:if test="${isInsideOe =='true' && isOeApproaching=='false' }">style="display:none"</c:if> id="openEnrollAlert" style="color: #6AABD5;">
				<spring:message code="indportal.portalhome.openenrollpstart" javaScriptEscape="true"/>
				<c:if test="${coverageYear == previousCoverageYear}">
					<%=new SimpleDateFormat("MMMMMMMMMMMM dd, yyyy").format(new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_START_DATE)))%>.
				</c:if>
				<c:if test="${coverageYear == currentCoverageYear}">
					<%=new SimpleDateFormat("MMMMMMMMMMMM dd, yyyy").format(new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE)))%>.
				</c:if>
			</p>
            <!--show during enrollment -->
            <p class="alert alert-info center span12" <c:if test="${isInsideOe =='false' && isOeApproaching=='true' }">style="display:none"</c:if> id="openEnrollAlert" style="color: #6AABD5;">
            	<spring:message code="indportal.portalhome.openenrollpend" javaScriptEscape="true"/>
            	<c:if test="${coverageYear == previousCoverageYear}">
					<%=new SimpleDateFormat("MMMMMMMMMMMM dd, yyyy").format(new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_OE_END_DATE)))%>.
				</c:if>
				<c:if test="${coverageYear == currentCoverageYear}">
					<%=new SimpleDateFormat("MMMMMMMMMMMM dd, yyyy").format(new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE)))%>.
				</c:if>
			</p>
          </div>
          <div class="gutter20 margin20-b" ng-show="${enableSep=='true' && sepPlanSelection=='true' && daysforSEP>0}">
          	<p class="alert alert-info center span12" style="color: #6AABD5;" ng-if="${applicationValidationStatus != 'PENDING'}">
          		<spring:message code="indportal.portalhome.sepstartson" javaScriptEscape="true"/> ${sepEndDate}.
			</p>
			<p class="alert alert-error center span12" style="color: #b94a48;" ng-if="${applicationValidationStatus == 'PENDING'}">
          		Action Required
			</p>
          </div>

          <div class="gutter20 margin20-b" ng-show="${enableSep=='true' && sepLinkEvents=='true'}">
          	<p class="alert alert-info center span12" style="color: #6AABD5;"><spring:message code="indportal.portalhome.recentlifeevent" javaScriptEscape="true"/></p>
          </div>

          <div class="row-fluid gutter10-trb" ng-show="${enableSep=='true'}">
            <div  id="step-progress">
              <div id="progress-bar">
                 <ul>
                   <li id="step-one" class="step-desc completed-state">
                     <span class="s-no">1</span><span class="step-text" alt="Shop" aria-label="Shop"><spring:message code="indportal.portalhome.checkforsavings" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-two" class="step-desc completed-state">
                     <span class="s-no">2</span><span class="step-text"><spring:message code="indportal.portalhome.findaplan" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-three" class="step-desc completed-state">
                     <span class="s-no">3</span><span class="step-text" alt="Apply" aria-label="Apply"><spring:message code="indportal.portalhome.apply" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-four" class="step-desc <c:if test="${stage ==4}">active-state</c:if> <c:if test="${stage >4}">completed-state</c:if>">
                     <span class="s-no">4</span><span class="step-text" alt="Enroll" aria-label="Update"><spring:message code="indportal.portalhome.update" javaScriptEscape="true"/></span>
                   </li>
                </ul>
              </div>
            </div>
          </div>

<div id="oep-msg" class="blurb clearfix margin0" ng-if="${enableSep=='true'}">
  <div class="gutter10-lr">

    <c:if test="${sepLinkEvents=='true'}">
      <div id="initial-eligibility" class="span16">
        <p class="span7">
          <spring:message code="indportal.portalhome.understandLifeEvent" javaScriptEscape="true"/>
        </p>
        <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
          <c:set var="sepCaseNumber" ><encryptor:enc value="${sepEventsNumber}" isurl="true"/></c:set>
          <a ng-click="" alt="Enroll" href="<c:url value="/referral/lce/events/applicants/${sepCaseNumber}" />" class="btn btn-primary pull-right">
            <spring:message code="indportal.portalhome.confirmLifeChange" javaScriptEscape="true"/>
          </a>
        </sec:accesscontrollist>
      </div>
    </c:if>

    <div id="initial-eligibility" class="span16" ng-if="${sepDisenroll=='true'}">
      <p class="span12">
        <spring:message code="indportal.portalhome.youNeedToDisenroll" javaScriptEscape="true"/>
      </p>
      <sec:accesscontrollist hasPermission="IND_PORTAL_DISENROLL" domainObject="user">
        <a href="#disenrollsep" alt="Disenroll" class="btn btn-primary pull-right">
          <spring:message code="indportal.plansummary.disenrollfromplan" javaScriptEscape="true"/>
        </a>
      </sec:accesscontrollist>
    </div>

    <div id="initial-eligibility" class="span16" ng-if="${sepPlanSelection=='true' && daysforSEP>0}">
      <div ng-if="${applicationValidationStatus != 'PENDING'}">
        <p class="span6">
          <spring:message code="indportal.portalhome.SEPGranted" javaScriptEscape="true"/>
          <span ng-if="${isRecTribe=='Y'}">
            <spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/>
          </span>
        </p>
        <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
          <a ng-click="checkForAutoRenewal(null,null,null,$event,'enroll',${coverageYear})" alt="Enroll" class="btn btn-primary pull-right" id="${enrollCaseNumber}">
            <spring:message code="indportal.portalhome.entroll" javaScriptEscape="true"/>
          </a>
        </sec:accesscontrollist>
      </div>
      <div ng-if="${applicationValidationStatus == 'PENDING'}">
        <p class="span6">
          Documentation is required to determine eligibility for a Special Enrollment Period.
        </p>
        <a ng-href="/hix/indportal/viewVerificationResults?caseNumber=${enrollCaseNumber}" class="btn btn-primary pull-right">
            Upload Documents
        </a>
      </div>
    </div>

    <div id="initial-eligibility" class="span16" ng-if="${sepPlanSelection=='true' && sepQepOver == 'Y'}">
      <spring:message code="indportal.portalhome.sepOverMessage" javaScriptEscape="true"/>
    </div>
  </div>
</div>

		 <!-- QEP Section -->
		 <div class="gutter20 margin20-b" ng-show="${enableQep=='true' && qepPlanSelection=='true' && daysforSEP>0}">
			<p class="alert alert-info center span12" style="color: #6AABD5;" ng-if="${applicationValidationStatus != 'PENDING'}">
          		<spring:message code="indportal.portalhome.sepstartson" javaScriptEscape="true"/> ${sepEndDate}.
			</p>
			<p class="alert alert-error center span12" style="color: #b94a48;" ng-if="${applicationValidationStatus == 'PENDING'}">
          		Action Required
			</p>
          </div>

          <div class="gutter20 margin20-b" ng-show="${enableQep=='true' && qepLinkEvents=='true'}">
          	<p class="alert alert-info center span12" style="color: #6AABD5;"><spring:message code="indportal.portalhome.qepTop" javaScriptEscape="true"/></p>
          </div>
          <div class="gutter20 margin20-b" ng-show="${enableQep=='true' && qepDisenroll=='true'}">
          	<p class="alert alert-info center span12" style="color: #6AABD5;"><spring:message code="indportal.portalhome.needdisenroll" javaScriptEscape="true"/></p>
          </div>

          <div class="row-fluid gutter10-trb" ng-show="${enableQep=='true'}">
            <div  id="step-progress">
              <div id="progress-bar">
                 <ul>
                   <li id="step-one" class="step-desc completed-state">
                     <span class="s-no">1</span><span class="step-text" alt="Shop" aria-label="Shop"><spring:message code="indportal.portalhome.checkforsavings" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-two" class="step-desc completed-state">
                     <span class="s-no">2</span><span class="step-text"><spring:message code="indportal.portalhome.findaplan" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-three" class="step-desc completed-state">
                     <span class="s-no">3</span><span class="step-text" alt="Apply" aria-label="Apply"><spring:message code="indportal.portalhome.apply" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-four" class="step-desc <c:if test="${stage ==4}">active-state</c:if> <c:if test="${stage >4}">completed-state</c:if>">
                     <span class="s-no">4</span><span class="step-text" alt="Enroll" aria-label="Update"><spring:message code="indportal.portalhome.qepStageFour" javaScriptEscape="true"/></span>
                   </li>
                </ul>
              </div>
            </div>
          </div>

<div id="oep-msg" class="blurb clearfix margin0" ng-if="${enableQep=='true'}">
  <div class="gutter10-lr">
    <c:if test="${qepLinkEvents=='true'}">
      <div id="initial-eligibility" class="span16">
        <p class="span7">
          <spring:message code="indportal.portalhome.understandLifeEvent" javaScriptEscape="true"/>
        </p>
        <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
          <c:set var="qepCaseNumber" ><encryptor:enc value="${qepEventsNumber}" isurl="true"/></c:set>
          <a ng-click="" alt="Enroll" href="<c:url value="/referral/lce/events/applicants/${qepCaseNumber}" />" class="btn btn-primary pull-right">
            <spring:message code="indportal.portalhome.qepConfirmButton" javaScriptEscape="true"/>
          </a>
        </sec:accesscontrollist>
      </div>
    </c:if>

    <div id="initial-eligibility" class="span16" ng-if="${qepPlanSelection=='true' && daysforSEP>0}">
      <div ng-if="${applicationValidationStatus != 'PENDING'}">
        <p class="span6">
          <spring:message code="indportal.portalhome.SEPGranted" javaScriptEscape="true"/>
          <span ng-if="${isRecTribe=='Y'}"><spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/></span>
        </p>
        <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
          <a ng-click="checkForAutoRenewal(null,null,null,$event,'enroll',${coverageYear})" alt="Enroll" class="btn btn-primary pull-right" id="${enrollCaseNumber}">
            <spring:message code="indportal.portalhome.entroll" javaScriptEscape="true"/>
          </a>
        </sec:accesscontrollist>
      </div>

      <div ng-if="${applicationValidationStatus == 'PENDING'}">
        <p class="span6">
          Documentation is required to determine eligibility for a Special Enrollment Period.
        </p>
        <a ng-href="/hix/indportal/viewVerificationResults?caseNumber=${enrollCaseNumber}" class="btn btn-primary pull-right">
            Upload Documents
        </a>
      </div>
    </div>

    <div id="initial-eligibility" class="span16" ng-if="${qepPlanSelection=='true' && sepQepOver == 'Y'}">
      <spring:message code="indportal.portalhome.sepOverMessage" javaScriptEscape="true"/>
    </div>
  </div>
</div>

		 <!-- QEP Section -->
          <!-- Enrollment Alert -->
          <div class="row-fluid gutter10-trb" ng-show="${enableSep=='false' && enableQep=='false'}">
            <div  id="step-progress">
              <div id="progress-bar">
                 <ul>
                   <li id="step-one" class="step-desc <c:if test="${stage ==1}">active-state</c:if> <c:if test="${stage >1}">completed-state</c:if>">
                     <span class="s-no">1</span><span class="step-text" alt="Shop" aria-label="Shop"><spring:message code="indportal.portalhome.checkforsavings" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-two" class="step-desc <c:if test="${stage ==2}">active-state</c:if> <c:if test="${stage >2}">completed-state</c:if>">
                     <span class="s-no">2</span><span class="step-text"><spring:message code="indportal.portalhome.findaplan" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-three" class="step-desc <c:if test="${stage ==3.5 || stage ==3}">active-state</c:if> <c:if test="${stage >3.5}">completed-state</c:if>">
                     <span class="s-no">3</span><span class="step-text" alt="Apply" aria-label="Apply"><spring:message code="indportal.portalhome.apply" javaScriptEscape="true"/></span>
                   </li>
                   <li id="step-four" class="step-desc <c:if test="${stage ==4}">active-state</c:if> <c:if test="${stage >4}">completed-state</c:if>">
                     <span class="s-no">4</span><span class="<c:if test="${stage >4 && isPlanEffective==true}">step-text</c:if><c:if test="${stage >4 && isPlanEffective==false}">step-terminate</c:if>" alt="Enroll" aria-label="Enroll"><spring:message code="indportal.portalhome.enroll" javaScriptEscape="true"/></span>
                   </li>
                </ul>
              </div>
            </div>
          </div>

          <div id="oep-msg" class="blurb clearfix margin0" ng-show="${enableSep=='false' && enableQep=='false'}">
            <div class="gutter10-lr">
              <div class="span12" id="complete-oep">
                <div <c:if test="${stage !=3}">style="display:none"</c:if>>
                  <p <c:if test="${stage ==3 && applicationStartLabel =='Resume Your Application'}">style="display:none"</c:if>><spring:message code="indportal.portalhome.stepOne" htmlEscape="false" /></p>
                  <p <c:if test="${stage ==3 && applicationStartLabel !='Resume Your Application'}">style="display:none"</c:if>><spring:message code="indportal.portalhome.youareintheprocess" javaScriptEscape="true"/></p>
                </div>
                <div <c:if test="${stage >2}">style="display:none"</c:if>>
                  <p><spring:message code="indportal.portalhome.stepOne" htmlEscape="false" /> <spring:message code="indportal.portalhome.youcanskipcheckfor" javaScriptEscape="true"/></p>
                </div>
              </div>
                    <div id="initial-eligibility" class="alert span16" ng-show="${stage==3.5}">
                        <spring:message code="indportal.portalhome.wearestillprocessing" htmlEscape="false" javaScriptEscape="true"/> <br>
						<!-- <spring:message code="indportal.portalhome.oncereceived" htmlEscape="false" javaScriptEscape="true"/> -->
                    </div>
                    <div id="initial-eligibility" class="alert span16" ng-show="${stage==4 && enableEnrollment==false}">
                        <spring:message code="indportal.portalhome.ineligibleforshopping" javaScriptEscape="true"/> <%=DynamicPropertiesUtil
					.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE)%>
                    </div>
                    <div id="initial-eligibility" class="span10" ng-show="${stage==4 && enableEnrollment==true && daysEndOe>0 && isInsideOe=='true'}">
                        <spring:message code="indportal.portalhome.congratswehavecompleted" javaScriptEscape="true"/>
                    </div>
					<div id="initial-eligibility" class="span10" ng-show="${stage==4 && enableEnrollment==true && isRecTribe=='Y'}">
						<spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/>
					</div>
                  <div id="initial-eligibility" <c:if test="${stage >1}">style="display:none"</c:if> class="span4 action">
                      <button onclick="location.href='preeligibility?coverageYear=${coverageYear}'" alt="Initial Eligibility" class="btn btn-primary pull-right"><spring:message code="indportal.portalhome.checkforsavings" javaScriptEscape="true"/></button>
                  </div>
                  <div id="initial-eligibility" <c:if test="${stage !=2}">style="display:none"</c:if> class="span4 action">
                      <button onclick="location.href='./private/saveIndividualPHIX'" alt="Browse Plans" class="btn btn-primary pull-right"><spring:message code="indportal.portalhome.browseplans" javaScriptEscape="true"/></button>
                  </div>

                  <div id="initial-eligibility" <c:if test="${stage !=3}">style="display:none"</c:if> class="span4 action">
                <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
				<c:if test="${applicationStartLabel =='Start Your Application' && isInsideOe=='true'}">
					<button ng-show="modelattrs.sCode=='ID'" class="btn btn-primary pull-right" ng-click="showDialog = true">
						${applicationStartLabel}
					</button>
				</c:if>
				<c:if test="${applicationStartLabel =='Start Your Application' && isInsideQEP=='true'}">
					<button ng-show="modelattrs.sCode=='ID'" class="btn btn-primary pull-right" ng-click="goQephome()">
						<spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
					</button>
				</c:if>
                <button <c:if test="${applicationStartLabel =='Start Your Application'}">style="display:none"</c:if> ng-show="modelattrs.sCode=='ID'" class="btn btn-primary pull-right" ng-click="showApplication(modelattrs.caseNumber)">${applicationStartLabel}</button>
                <button ng-show="modelattrs.sCode=='NM'" class="btn btn-primary pull-right" ng-click="showApplication(modelattrs.caseNumber)">${applicationStartLabel}</button>
                </sec:accesscontrollist>
                  </div>
                  <div id="initial-eligibility" <c:if test="${stage !=4 || enableEnrollment=='false'}">style="display:none"</c:if> class="span4 action">
	                <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
                      <button ng-click="preEnroll($event,${coverageYear})" alt="Enroll" class="btn btn-primary pull-right" id="${enrollCaseNumber}" ng-if="${showEnrollButton =='Y'}"><spring:message code="indportal.portalhome.entroll" javaScriptEscape="true"/></button>
    		         </sec:accesscontrollist>
                     </div>
					<div id="initial-eligibility" ng-if="${stage>4}" class="span4 action">
                      <button ng-click="checkForAutoRenewal(${qepFlag},'${applicationStatus}','${enrollCaseNumber}',null,'reportAChange',${coverageYear})" alt="Enroll" class="btn btn-primary pull-right"><spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></button>
                    </div>
					<div id="initial-eligibility" class="span16" ng-if="${showEnrollButton=='N'}">
						<spring:message code="indportal.portalhome.oeOverMessage" javaScriptEscape="true"/>
				   </div>
              </div>
          </div>
        </div>


		<sec:accesscontrollist hasPermission="IND_PORTAL_ONE_TOUCH_RENEWAL,MYAPP_ONE_TOUCH_RENEWAL" domainObject="user">
			<!-- initOtr starts -->
			<div ng-init="initOtr(${enableOtr}, '${applicationStatus}')">
				<div ng-if="enableOtr === true && applicationStatus === 'Enrolled (Or Active)'">
					<!-- row-fluid starts -->
					<div class="row-fluid" ng-if="otr.pendingApp !== true && otr.showOTR !== false">
          				<div class="alert alert-info span12">
							<div class="span6 gutter10-l">
          						<span ng-if="otr.renewed === true">
									<spring:message code="indportal.portalhome.otr.renewedMessage" />
								</span>
    	      					<span ng-if="otr.renewed === false">
									<spring:message code="indportal.portalhome.otr.showOtrMessage" />
								</span>
							</div>

							<div class="span5 pull-right">
								<button class="btn btn-primary" disabled ng-if="otr.renewed === true">
									<spring:message code="indportal.portalhome.otr.renew" />
								</button>
								<button ng-click="checkOtr()" class="btn btn-primary" ng-if="otr.renewed === false">
									<spring:message code="indportal.portalhome.otr.renew" />
								</button>
							</div>
						</div>
					</div>
					<!-- row-fluid ends -->

					<!-- row-fluid starts -->
					<div class="row-fluid" ng-if="otr.pendingApp === true || (otr.showOTR === false && otr.pendingApp === false)">
						<div class="alert alert-success span12" ng-if="otr.pendingApp === true">
							<div class="span6 gutter10-l">
          						<spring:message code="indportal.portalhome.otr.pendingAppMessage" />
							</div>
							<div class="span5 pull-right">
								<button class="btn btn-primary" disabled>
									<spring:message code="indportal.portalhome.otr.renew" />
								</button>
							</div>
						</div>

						<div class="alert alert-success span12" ng-if="otr.showOTR === false && otr.pendingApp === false">
							<div class="span6 gutter10-l">
          						<spring:message code="indportal.portalhome.otr.enrolledNessage" />
							</div>
							<div class="span5 pull-right">
								<button class="btn btn-primary" disabled>
									<spring:message code="indportal.portalhome.otr.renew" />
								</button>
							</div>
						</div>
					</div>
					<!-- row-fluid ends -->
				</div>

				<div ng-if="enableOtr === true && applicationStatus !== 'Enrolled (Or Active)'">
					<!-- row-fluid starts -->
					<div class="row-fluid">
          				<div class="alert alert-info span12">
							<div class="span6 gutter10-l">
								<spring:message code="indportal.portalhome.otr.notEnrolledMessage" />
							</div>

							<div class="span5 pull-right">
								<button class="btn btn-primary" disabled>
									<spring:message code="indportal.portalhome.otr.renew" />
								</button>
							</div>
						</div>
					</div>
					<!-- row-fluid ends -->
				</div>
			</div>
			<!-- initOtr ends -->

			<div id="otrOverrideCommentModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="otrOverrideCommentHeader" aria-hidden="true">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="resetOtr()">x</button>
						<h3 id="otrOverrideCommentHeader"> <spring:message code="indportal.portalhome.otr.otrOverrideCommentHeader"/></h3>
					</div>

					<div class="modal-body">
						<form name="otrOverrideCommentModalForm" novalidate>
                			<textarea class="overrideText" ng-model="overrideComment" required maxlength="4000" placeholder="<spring:message code="indportal.enrollments.explainWhy" javaScriptEscape="true"/>"></textarea>

							<span>{{4000 - overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
							<div class="pull-right">
								<button class="btn" data-dismiss="modal" ng-click="resetOtr()">cancel</button>
								<button class="btn btn-primary" ng-click="saveOtrOverrideComment()" data-dismiss="modal" ng-disabled="otrOverrideCommentModalForm.$invalid">Continue</button>
							</div>
						</form>

					</div>
			</div>

			<div id="otrFailModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="otrFailHeader" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
					<h3 id="otrFailHeader">
						<spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/>
					</h3>
				</div>

				<div class="modal-body">
					<p> <spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
					<p> <spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>

					<div class="pull-right">
						<button class="btn btn-primary" data-dismiss="modal">OK</button>
					</div>
				</div>
			</div>


		  </sec:accesscontrollist>


		  <!-- Recognized tribe message and change plan button -->
		  <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
		  <div class="row-fluid margin10-b" ng-if="${stage>4 && isRecTribe=='Y' && changePlanInSEP!='Y' && showChangePlanInOE!='Y'}">
               <div id="initial-eligibility row-fluid" <c:if test="${stage ==3}">style="display:none"</c:if> >
				   <p class="span6" style="display:none"></p>
				   <p class="span6">
                        <spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/>
                    </p>
                    <div class="span5 pull-right">
                      	<a href="javascript:void(0);" ng-click="checkForAutoRenewal(null,null,null,$event,'changePlanTribe',${coverageYear})" class="btn btn-primary" id="${enrollCaseNumber}"><spring:message code="indportal.portalhome.changePlan" javaScriptEscape="true"/></a>
					</div>
               </div>
          </div>
          </sec:accesscontrollist>

		  <!-- Change Plan flow for SEP -->
		  <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
		  <div class="row-fluid margin10-b" ng-if="${stage>4 && changePlanInSEP=='Y'}">
               <div id="initial-eligibility row-fluid" <c:if test="${stage ==3}">style="display:none"</c:if> >
				   <p class="span6" style="display:none"></p>
				   <p class="span6">
                        <spring:message code="indportal.portalhome.sepChangePlanMessage" javaScriptEscape="true"/>
                    </p>
                    <div class="span5 pull-right">
                      	<button ng-click="checkForAutoRenewal(null,null,null,$event,'changePlan',${coverageYear})" id="${enrollCaseNumber}" class="btn btn-primary"><spring:message code="indportal.portalhome.changeSEPPlan" javaScriptEscape="true"/></button>
					</div>
               </div>
          </div>
          </sec:accesscontrollist>

		  <!-- Change Plan flow for Auto renewed application -->
		  <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
		  <div class="row-fluid margin10-b" ng-if="${stage>4 && showChangePlanInOE=='Y'}">
               <div id="initial-eligibility row-fluid" <c:if test="${stage ==3}">style="display:none"</c:if> >
				   <p class="span6" style="display:none"></p>
 				   <p class="span6">
                       <spring:message code="indportal.portalhome.autoRenewalMessage" javaScriptEscape="true"/>
                    </p>
                    <div class="span5 pull-right">
                      	<button ng-click="checkForAutoRenewal(null,null,null,$event,'changePlan',${coverageYear})" id="${enrollCaseNumber}" class="btn btn-primary"><spring:message code="indportal.portalhome.changeSEPPlan" javaScriptEscape="true"/></button>
					</div>
               </div>
          </div>
          </sec:accesscontrollist>

          <div class="row-fluid margin10-b">
                  <div id="initial-eligibility row-fluid" <c:if test="${stage ==3}">style="display:none"</c:if> >
                    <c:if test="${applicationStartLabel =='Start Your Application' && isInsideOe=='true'}">
						<p class="span6"><spring:message code="indportal.portalhome.beginapplication" javaScriptEscape="true"/></p>
					</c:if>
                    <p class="span6" <c:if test="${applicationStartLabel !='Resume Your Application'}">style="display:none"</c:if>><spring:message code="indportal.portalhome.youareintheprocess" javaScriptEscape="true"/></p>
                    <p class="span6" <c:if test="${applicationStartLabel !='View Your Application'}">style="display:none"</c:if>><spring:message code="indportal.portalhome.youcanselecttoviewyourapp" javaScriptEscape="true"/></p>
                    <div class="span5 pull-right">
                		<c:if test="${applicationStartLabel =='Start Your Application' && isInsideOe=='true'}">
							<button ng-show="modelattrs.sCode=='ID'" class="btn btn-primary pull-right" ng-click="showDialog = true">
								${applicationStartLabel}
							</button>
						</c:if>
						<c:if test="${applicationStartLabel =='Start Your Application' && isInsideQEP=='true'}">
							<button ng-show="modelattrs.sCode=='ID'" class="btn btn-primary pull-right" ng-click="goQephome()">
								<spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
							</button>
						</c:if>
               			<button <c:if test="${applicationStartLabel =='Start Your Application'}">style="display:none"</c:if> ng-show="modelattrs.sCode=='ID'" class="btn btn-primary" ng-click="showApplication(modelattrs.caseNumber)">${applicationStartLabel}</button>
                		<button ng-show="modelattrs.sCode=='NM'" class="btn btn-primary" ng-click="showApplication(modelattrs.caseNumber)">${applicationStartLabel}</button>
                   </div>
                  </div>
          </div>
         <div class="gutter20 margin20-b" ng-show="${enableSep=='true'}">
          	<p class="alert alert-info center span12"><spring:message code="indportal.portalhome.viewUpdatedEligibilityResults" javaScriptEscape="true"/></p>
         </div>

        <div class="row-fluid" ng-show="${enableSep=='false'}">
	          <div class="info-block">
	            <div class="">
	              <div class="header" <c:if test="${postEligibility ==false}">style="display:none"</c:if>>
	                <h4 ><spring:message code="indportal.portalhome.eligibilitysummary" javaScriptEscape="true"/></h4>
	              </div>
	              <div class="header" <c:if test="${postEligibility ==true}">style="display:none"</c:if>>
	                <h4 ><spring:message code="indportal.portalhome.preelegibilitysummary" javaScriptEscape="true"/></h4>
	              </div>
	              <div class="widget">
	              <div id="plan-summary" class="gutter10">
	                  <p ng-show="${prescreenerComplete ==false  && postEligibility ==false}"><spring:message code="indportal.portalhome.tofindouthowmuchyoucansave"/></p>
	                <div class="data-display well"> <!-- should be removed for the fifth scree and replaced it with the image madicaid -->
	                  	<dl class="dl-horizontal">
						 	<span class="block margin10-b">
	                      		<dt style="text-overflow: initial; white-space: pre-wrap;"><spring:message code="indportal.portalhome.premiumtaxcredit" javaScriptEscape="true"/> <a class="ttclass" rel="tooltip" href="javascript:void(0)" data-original-title="<spring:message code='indportal.portalhome.theaffordablecareact' htmlEscape="false" javaScriptEscape='true'/>"><i class="icon-question-sign"></i>:</a></dt>
	                      		<dd id="prem-tax-credit">
									<strong>
										<c:choose>
											<c:when test="${aptc=='' || aptc == null || aptc == 'null'}"><spring:message code="indportal.portalhome.estimateIncomplete" javaScriptEscape="true"/></c:when>
											<c:when test="${aptc=='N/A' || aptc=='$null'}">
												<c:if test="${postEligibility == false}">
													<img class="eligibility-mask" src="<c:url value="/resources/img/estimate-only.png"/>">
												</c:if>
												<spring:message code="indportal.portalhome.notEligible" javaScriptEscape="true"/>
											</c:when>
											<c:otherwise>
												<c:if test="${postEligibility == false}">
													<img class="eligibility-mask" src="<c:url value="/resources/img/estimate-only.png"/>">
												</c:if>
												{{modelattrs.aptc}}
											</c:otherwise>
										</c:choose>
									</strong>
								</dd>
						  	</span>
						  	<span class="block margin10-b">
	                      		<dt><spring:message code="indportal.portalhome.costsharingreduction" javaScriptEscape="true"/> <a class="ttclass" rel="tooltip" href="javascript:void(0)" data-original-title="<spring:message code='indportal.portalhome.adiscountthatlowerstheamount' htmlEscape="false" javaScriptEscape='true'/>"><i class="icon-question-sign"></i>:</a></dt>
	                      		<dd id="generic-medic">
									<strong>
										<c:choose>
											<c:when test="${csr=='' || csr==null}"><spring:message code="indportal.portalhome.estimateIncomplete" javaScriptEscape="true"/></c:when>
											<c:when test="${csr=='N/A'}">
												<spring:message code="indportal.portalhome.notEligible" javaScriptEscape="true"/>
											</c:when>
											<c:otherwise>
												<spring:message code="indportal.portalhome.available" javaScriptEscape="true"/>
											</c:otherwise>
										</c:choose>
									</strong>
								</dd>
						 	 </span>
	                    </dl>
	               	 	<div ng-class="${preEligibilityMedicaid ==true || preEligibilityChip==true || preEligibilityMedicare==true} ? 'alert' : ''" id="otherProgramEligibility" <c:if test="${postEligibility ==true}">style="display:none"</c:if>>
	                		<span ng-show="${preEligibilityMedicaid ==true && preEligibilityChip==false && preEligibilityMedicare==false}"><spring:message code="indportal.portalhome.familymembersmedicaid" javaScriptEscape="true"/></span>
	                		<span ng-show="${preEligibilityMedicaid ==false && preEligibilityChip==true && preEligibilityMedicare==false}"><spring:message code="indportal.portalhome.familymemberschip" javaScriptEscape="true"/></span>
	         	        	<span ng-show="${preEligibilityMedicaid ==false && preEligibilityChip==false && preEligibilityMedicare==true}"><spring:message code="indportal.portalhome.familymembersmedicare" javaScriptEscape="true"/></span>
	     	           	 	<span ng-show="${preEligibilityMedicaid ==true && preEligibilityChip==true && preEligibilityMedicare==false}"><spring:message code="indportal.portalhome.familymembersbothchipmedicaid" javaScriptEscape="true"/></span>
	                		<span ng-show="${preEligibilityMedicaid ==true && preEligibilityChip==false && preEligibilityMedicare==true}"><spring:message code="indportal.portalhome.familymembersbothmedicaremedicaid" javaScriptEscape="true"/></span>
	                		<span ng-show="${preEligibilityMedicaid ==false && preEligibilityChip==true && preEligibilityMedicare==true}"><spring:message code="indportal.portalhome.familymembersbothchipmedicare" javaScriptEscape="true"/></span>
	                		<span ng-show="${preEligibilityMedicaid ==true && preEligibilityChip==true && preEligibilityMedicare==true}"><spring:message code="indportal.portalhome.familymembersall3" javaScriptEscape="true"/></span>
	                	</div>
	                <div class="alert alert-info" ng-show="${postEligibility ==false}"><spring:message code="indportal.portalhome.resultsonlyestimate" javaScriptEscape="true"/></div>
	                </div>
	                  <div class="msg2">
	                    <a ng-show="${prescreenerComplete ==false}" href="./preeligibility?coverageYear=${coverageYear}" class="btn"><i class="icon-repeat"></i> Check for Savings</a>
	                    <a ng-show="${prescreenerComplete ==true && postEligibility ==false}" href="./preeligibility?coverageYear=${coverageYear}" class="btn"><i class="icon-repeat"></i> <spring:message code="indportal.portalhome.checkforsavingsagain" javaScriptEscape="true"/></a>

						<a <c:if test="${postEligibility ==false}">style="display:none"</c:if> href="javascript:void(0)" ng-click="showEligibilityDetailsFromHome($event)" id="{{modelattrs.caseNumber}}" class="btn"><spring:message code="indportal.portalhome.eligibilitydetails" javaScriptEscape="true"/></a>
	                  </div>
	              </div>
	            </div>
	            </div>
	          </div>
          </div>
          <br>
          <div ng-show="${enableSep=='false'}">
			  <div class="header">
				<h4 <c:if test="${stage==5}">style="display:none"</c:if>>Your Selected Plans (To Be Enrolled)</h4>
				<h4 <c:if test="${stage<5}">style="display:none"</c:if>>Your Plans Summary</h4>
			  </div>
			<div class="gutter10-lr">
			<div class="row-fluid">
	          <!-- Health plan tile -->
	          <div class="span6 info-block" id="summary">
					<div class="">
					<!--<div class="header">
							<div <c:if test="${stage==5}">style="display:none"</c:if>>
								<h4>
									<spring:message
										code="indportal.portalhome.favoritehealthplanselected"
										javaScriptEscape="true" />
								</h4>
							</div>
							<div <c:if test="${stage<5}">style="display:none"</c:if>>
								<h4>
									<spring:message code="indportal.portalhome.healthplansummary"
										javaScriptEscape="true" />
								</h4>
							</div>
						</div> -->
						<div class="plan-title">
							<img src="<c:url value="/resources/img/health-icon.png"/>" alt="Health Plan Image">
							Health Plan
						</div>
						<div class="widget">
							<div id="sample-plan-summary" class="gutter10"
								<c:if test="${favoriteHealthPlanSelected==true}">style="display:none"</c:if>>
								<p id="summary-msg1">
									<spring:message
										code="indportal.portalhome.whenyouselectaplanitwill"
										javaScriptEscape="true" />
								</p>
								<p id="summary-msg1" class="hidden">
									<!--<spring:message code="indportal.portalhome.rejectedmessege" javaScriptEscape="true"/>-->
								</p>
								<div class="plan-selected">
									<div class="info-header txt-center">
										<img src="<c:url value="/resources/img/sample-plan.png"/>"
											alt="Sample Plan Image">
										<p class="margin10-t">
											<a href="#"><spring:message
													code="indportal.portalhome.favPlanName" arguments="${coverageYear}"
													javaScriptEscape="true" /></a>
										</p>
										<h5 class="payment">
											<strong>$230/month</strong>
										</h5>
									</div>

									<div id="plan-mask-sample" class="mask">
										<img src="<c:url value="/resources/img/example.png"/>"
											alt="<spring:message code='indportal.portalhome.thisisasampleplansummary' javaScriptEscape='true'/>" />
									</div>
									<div class="data-display well">
										<table class="dl-horizontal">
											<caption class="aria-hidden">
												<spring:message code='indportal.portalhome.samplePlan'
													javaScriptEscape='true' />
											</caption>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.plantype"
														javaScriptEscape="true" />:</th>
												<td id="plan-type" class="plan-home">PPO</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.officevisit"
														javaScriptEscape="true" />:</th>
												<td id="off-visit" class="plan-home">$25</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.genericmedication"
														javaScriptEscape="true" />:</th>
												<td id="generic-medic" class="plan-home">$10</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.deductible"
														javaScriptEscape="true" />:</th>
												<td id="deductible" class="plan-home">$1,500</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.maxoutofpocket"
														javaScriptEscape="true" />:</th>
												<td id="m-o-p" class="plan-home">$5,000</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div id="selected-plan-summary" class="gutter10"
								<c:if test="${favoriteHealthPlanSelected!=true}">style="display:none"</c:if>>
								<div ng-include="'healthPlanSummarySec'"></div>
								<div class="gutter10-tb">
									<p class="margin20-b"
										<c:if test="${stage ==5}">style="display:none"</c:if>>
										<a
											href="/hix/private/planinfo/${healthCoverageDate}/${healthPlanId}"
											class="pull-right btn btn-primary"> <spring:message
												code="indportal.portalhome.viewplandetails"
												javaScriptEscape="true" /></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Dental plan tile -->
	            <div class="span6 info-block" id="dentalPlanSummary">
					<div class="">
						<!--<div class="header">
							<div <c:if test="${stage==5}">style="display:none"</c:if>>
								<h4>
									<spring:message
										code="indportal.portalhome.favoritedentalplanselected"
										javaScriptEscape="true" />
								</h4>
							</div>
							<div <c:if test="${stage<5}">style="display:none"</c:if>>
								<h4>
									<spring:message code="indportal.portalhome.dentalplansummary"
										javaScriptEscape="true" />
								</h4>
							</div>
						</div>-->
						<div class="plan-title">
							<img src="<c:url value="/resources/img/tooth-icon.png"/>" alt="Dental Plan Image">
							Dental Plan
						</div>
						<div class="widget">
							<div id="sample-dental-plan-summary" class="gutter10"
								<c:if test="${favoriteDentalPlanSelected==true}">style="display:none"</c:if>>
								<p id="dental-summary-msg1">
									<spring:message
										code="indportal.portalhome.whenyouselectadentalplanitwill"
										javaScriptEscape="true" />
								</p>
								<div class="plan-selected">
									<div class="info-header txt-center">
										<img src="<c:url value="/resources/img/sample-plan-dental.png"/>"
											alt="Sample Plan Image">
										<p class="margin10-t">
											<a href="#"><spring:message
													code="indportal.portalhome.favPlanName" arguments="${coverageYear}"
													javaScriptEscape="true" /></a>
										</p>
										<h5 class="payment">
											<strong>$230/month</strong>
										</h5>
									</div>

									<div id="dental-plan-mask-sample" class="mask">
										<img src="<c:url value="/resources/img/example.png"/>"
											alt="<spring:message code='indportal.portalhome.thisisasampleplansummary' javaScriptEscape='true'/>" />
									</div>
									<div class="data-display well">
										<table class="dl-horizontal">
											<caption class="aria-hidden">
												<spring:message code='indportal.portalhome.samplePlan'
													javaScriptEscape='true' />
											</caption>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.plantype"
														javaScriptEscape="true" />:</th>
												<td id="dental-plan-type" class="plan-home">PPO</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.officevisit"
														javaScriptEscape="true" />:</th>
												<td id="dental-off-visit" class="plan-home">$25</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.genericmedication"
														javaScriptEscape="true" />:</th>
												<td id="dental-generic-medic" class="plan-home">$10</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.deductible"
														javaScriptEscape="true" />:</th>
												<td id="dental-deductible" class="plan-home">$1,500</td>
											</tr>
											<tr>
												<th scope="row" class="plan-home"><spring:message
														code="indportal.portalhome.maxoutofpocket"
														javaScriptEscape="true" />:</th>
												<td id="dental-m-o-p" class="plan-home">$5,000</td>
											</tr>
										</table>
									</div>
								</div>
							</div>
							<div id="selected-dental-plan-summary" class="gutter10"
								<c:if test="${favoriteDentalPlanSelected!=true}">style="display:none"</c:if>>
								<div ng-include="'dentalPlanSummarySec'"></div>
								<div class="gutter10-tb">
									<p class="margin20-b"
										<c:if test="${stage ==5}">style="display:none"</c:if>>
										<a
											href="/hix/private/planinfo/${dentalCoverageDate}/${dentalPlanId}"
											class="pull-right btn btn-primary"> <spring:message
												code="indportal.portalhome.viewplandetails"
												javaScriptEscape="true" /></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
        </div>
        </div>
      </div>

		<div spinning-loader="loader"></div>
	<!-- Modal for failling submission of pre-eligibility-->
	<div ng-show= "openInitPreEnroll">
	      <div modal-show="openInitPreEnroll" id="preenroll" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="preEnrollModal" aria-hidden="true">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	            <h3 id="preEnrollModal"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
	          </div>
	          <div class="modal-body">
	          <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
	          </div>
	          <div class="modal-footer">
	            <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
	          </div>
	        </div><!-- /.modal-content-->
	      </div> <!-- /.modal-dialog-->
	     </div>
	</div>
	<!-- Modal for failling submission of pre-eligibility-->
	<!-- Modal for failling submission of pre-eligibility-->
	<div ng-show= "openPreEnrollESD">
	      <div modal-show="openPreEnrollESD" id="esd" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="esdModal" aria-hidden="true">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div>

	          </div>
	          <div class="modal-body">
	          <p><spring:message code="indportal.portalhome.esdRequired" javaScriptEscape="true"/></p>
	          </div>
	          <div class="modal-footer">
	            <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
	          </div>
	        </div><!-- /.modal-content-->
	      </div> <!-- /.modal-dialog-->
	     </div>
	</div>
	<!-- Modal for failling submission of pre-eligibility-->
	<!-- Modal for change plan warning-->
	<div ng-show= "disAlloweChangePlan">
	      <div modal-show="disAlloweChangePlan" id="preenroll" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="disAlloweChangePlan" aria-hidden="true">
	      <div class="modal-dialog">
	        <div class="modal-content">
	          <div class="modal-header">
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	            <h3 id="disAlloweChangePlan"><spring:message code="indportal.portalhome.comelater" javaScriptEscape="true"/></h3>
	          </div>
	          <div class="modal-body">
	          <p><spring:message code="indportal.portalhome.changeplanmessage" javaScriptEscape="true"/></p>
	          </div>
	          <div class="modal-footer">
	            <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
	          </div>
	        </div><!-- /.modal-content-->
	      </div> <!-- /.modal-dialog-->
	     </div>
	</div>
	<!-- Modal for change plan warning-->

	<!-- Modal for Review Preferences warning-->
	<div ng-show="reveiwPrefModal">
		<div modal-show="reveiwPrefModal" id="reveiwPref" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="reveiwPrefModal" aria-hidden="true">
 	      <div class="modal-dialog">
 	        <div class="modal-content">
 	          <div class="modal-body">
 	          <p><spring:message code="indportal.portalhome.reviewPref" javaScriptEscape="true"/></p>
 	          </div>
 	          <div class="modal-footer">
 	            <button class="btn pull-left" data-dismiss="modal" ng-click="setPrefReviewed()"><spring:message code="indportal.portalhome.reviewPrefButton" javaScriptEscape="true"/></button>
 	          </div>
 	        </div><!-- /.modal-content-->
 	      </div> <!-- /.modal-dialog-->
		</div>
 	</div>
	<!-- Modal for Review Preferences warning-->

  <!-- Modal-->
    <div ng-show= "showDialog" id="financialHelp" modal-show="showDialog" class="modal fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="financialHelpModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="financialHelpModal"><spring:message code="indportal.portalhome.financialhelp" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
		    <sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
			<c:choose>
			<c:when test="${not showMessage}">
				<p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
			</c:when>
			<c:otherwise>
				<p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
			</c:otherwise>
     		</c:choose>
           <!-- <p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"  htmlEscape="false"/></p>
			<sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
			<c:if test="${not showMessage}">
			<p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
			</c:if>-->
            <!-- <p><spring:message code="indportal.portalhome.ifyouselecttoapplyforfinancialhelp" javaScriptEscape="true"/></p>-->
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></button>
			<sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user">
            	<a href="javascript:void(0)" class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="openInSameTab('<%=GhixConstants.APP_URL%>individual/saml/idalinkredirect')"><spring:message code="indportal.portalhome.yestakemetoidalink" javaScriptEscape="true"/></a>
            </sec:accesscontrollist>
            <button class="btn btn-primary" data-dismiss="modal" ng-click="goToSsapOE()"><spring:message code="indportal.portalhome.stayon" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
	<!-- Modal-->
              <div ng-show= "openDialoglce" id="openDialoglce">
                <div modal-show="openDialoglce" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="reportChangeModal" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 id="reportChangeModal" style="font-size: 18px;border: none;"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
                      </div>
                      <div class="modal-body">
                      	<div>
                      		<p class="margin10-l"><spring:message code="indportal.portalhome.ifyouwouldliketomakechanges" javaScriptEscape="true"/></p>
							<div class="alert alert-info">
                        		<p><spring:message code="indportal.portalhome.moreInfoLifeEventsFin" htmlEscape="false"/></p>
							</div>
						</div>

                      </div>
                      <div class="modal-footer">
						<button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
                        <button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
                      </div>
                    </div><!-- /.modal-content-->
                  </div> <!-- /.modal-dialog-->
                </div>
              </div>
           <!-- Modal-->
		   <!-- Auto renewed LCE Modal-->
              <div ng-show= "openAutoRenewalLceDialog" id="openAutoRenewalLceDialog">
                <div modal-show="openAutoRenewalLceDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="autoRenewalReportChangeModal" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h3 id="autoRenewalReportChangeModal" style="font-size: 18px;border: none;"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
                      </div>
                      <div class="modal-body">
                      	<div>
							<spring:message code="indportal.portalhome.autorenewalLCE" arguments="${currentCoverageYear}" javaScriptEscape="true"/>
						</div>

                      </div>
                      <div class="modal-footer">
                        <button id="autoRenewalButton" class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
                      </div>
                    </div><!-- /.modal-content-->
                  </div> <!-- /.modal-dialog-->
                </div>
              </div>
           <!-- Auto renewed LCE Modal -->
</div>
</script>

<!-- Section for plan summary -->
<script type="text/ng-template" id="healthPlanSummarySec">
<div class="plan-selected">
	<div ng-show="loaderF" id="ngIncLoader">
		<img alt="loading" src="resources/images/loader_gray.gif"
			class="small-loader" />
	</div>
	<div ng-show="!loaderF">
		<dl>
			<p class="alert alert-info" ng-show="${healthEnMessage=='EN_PE'}">
				<spring:message code="indportal.portalhome.enMsgPending"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${healthEnMessage=='EN_EN'}">
				<spring:message code="indportal.portalhome.enMsgSuccessful"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${healthEnMessage=='EN_CN'}">
				<spring:message code="indportal.portalhome.enMsgRejected1"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${healthEnMessage=='EN_AB'}">
				<spring:message code="indportal.portalhome.enMsgRejected1"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${stage<=4}">
				<spring:message code="indportal.portalhome.enMsgFavorite"
					javaScriptEscape="true" />
			</p>


		</dl>
		<div class="info-header txt-center">
			<img src="${healthPlanTile.issuerLogo}" alt="Issuer Logo">
			<p>
				<a href="#">${healthPlanTile.planName}</a>
			</p>
			<c:if test="${not empty healthPlanTile.netPremium}">
				<h5 class="payment">{{${healthPlanTile.netPremium}|currency}}
					/month</h5>
			</c:if>
			<c:if test="${not empty healthPlanTile.monthlyPremium}">
				<h6 class="payment monthly-premium">
					was
					<del>
						{{${healthPlanTile.monthlyPremium}|currency}}
						<del>
				</h6>
			</c:if>
		</div>

		<div class="data-display well">

			<table class="dl-horizontal">
				<caption class="aria-hidden">${healthPlanTile.planName}</caption>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.plantype" javaScriptEscape="true" />:</th>
					<td id="plan-type" class="plan-home">${healthPlanTile.planType}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.officevisit" javaScriptEscape="true" />:</th>
					<td id="off-visit" class="plan-home">${healthPlanTile.officeVisit}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.genericmedication"
							javaScriptEscape="true" />:</th>
					<td id="generic-medic" class="plan-home">${healthPlanTile.genericMedication}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.deductible" javaScriptEscape="true" />:</th>
					<td id="deductible" class="plan-home">${healthPlanTile.deductible}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.maxoutofpocket"
							javaScriptEscape="true" />:</th>
					<td id="m-o-p" class="plan-home">${healthPlanTile.outOfPocket}</td>
				</tr>
			</table>
		</div>
		<div class="gutter10-tb">
			<p class="margin20-b"
				<c:if test="${stage !=5}">style="display:none"</c:if>>
				<a href="javascript:void(0)"
					ng-click="showPlanSummary(modelattrs.caseNumber)"
					class="pull-right btn"><spring:message
						code="indportal.portalhome.healthplandetails" javaScriptEscape="true" /></a>
			</p>
		</div>
	</div>
</div>
</script>
<script type="text/ng-template" id="dentalPlanSummarySec">
<div class="plan-selected">
	<div ng-show="loaderF" id="ngIncLoader">
		<img alt="loading" src="resources/images/loader_gray.gif"
			class="small-loader" />
	</div>
	<div ng-show="!loaderF">
		<dl>
			<p class="alert alert-info" ng-show="${dentalEnMessage=='EN_PE'}">
				<spring:message code="indportal.portalhome.dentalMsgPending"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${dentalEnMessage=='EN_EN'}">
				<spring:message code="indportal.portalhome.dentalMsgSuccessful"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${dentalEnMessage=='EN_CN'}">
				<spring:message code="indportal.portalhome.dentalMsgRejected1"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${dentalEnMessage=='EN_AB'}">
				<spring:message code="indportal.portalhome.dentalMsgRejected1"
					javaScriptEscape="true" />
			</p>
			<p class="alert alert-info" ng-show="${stage<=4}">
				<spring:message code="indportal.portalhome.enMsgFavorite"
					javaScriptEscape="true" />
			</p>


		</dl>
		<div class="info-header txt-center">
			<img src="${dentalPlanTile.issuerLogo}" alt="Issuer Logo">
			<p>
				<a href="#">${dentalPlanTile.planName}</a>
			</p>
			<c:if test="${not empty dentalPlanTile.netPremium}">
				<h5 class="payment">{{${dentalPlanTile.netPremium}|currency}}
					/month</h5>
			</c:if>
			<c:if test="${not empty dentalPlanTile.monthlyPremium}">
				<h6 class="payment monthly-premium">
					was
					<del>
						{{${dentalPlanTile.monthlyPremium}|currency}}
						<del>
				</h6>
			</c:if>
		</div>

		<div class="data-display well">

			<table class="dl-horizontal">
				<caption class="aria-hidden">${dentalPlanTile.planName}</caption>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.plantype" javaScriptEscape="true" />:</th>
					<td id="plan-type" class="plan-home">${dentalPlanTile.planType}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.officevisit" javaScriptEscape="true" />:</th>
					<td id="off-visit" class="plan-home">${dentalPlanTile.officeVisit}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.genericmedication"
							javaScriptEscape="true" />:</th>
					<td id="generic-medic" class="plan-home">${dentalPlanTile.genericMedication}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.deductible" javaScriptEscape="true" />:</th>
					<td id="deductible" class="plan-home">${dentalPlanTile.deductible}</td>
				</tr>
				<tr>
					<th scope="row" class="plan-home"><spring:message
							code="indportal.portalhome.maxoutofpocket"
							javaScriptEscape="true" />:</th>
					<td id="m-o-p" class="plan-home">${dentalPlanTile.outOfPocket}</td>
				</tr>
			</table>
		</div>
		<div class="gutter10-tb">
			<p class="margin20-b"
				<c:if test="${stage !=5}">style="display:none"</c:if>>
				<a href="javascript:void(0)"
					ng-click="showPlanSummary(modelattrs.caseNumber)"
					class="pull-right btn"><spring:message
						code="indportal.portalhome.dentalplandetails" javaScriptEscape="true" /></a>
			</p>
		</div>
	</div>
</div>
</script>
