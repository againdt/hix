
package com.getinsured.iex.hub.vlp37.webservice.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DHSVerifiedCasesRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DHSVerifiedCasesRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlpdvc.ee.sim.dsh.cms.hhs.gov}CaseListArray"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DHSVerifiedCasesRequestType", propOrder = {
    "caseListArray"
})
public class DHSVerifiedCasesRequestType {

    @XmlElement(name = "CaseListArray", required = true)
    protected CaseListArrayType caseListArray;

    /**
     * Gets the value of the caseListArray property.
     * 
     * @return
     *     possible object is
     *     {@link CaseListArrayType }
     *     
     */
    public CaseListArrayType getCaseListArray() {
        return caseListArray;
    }

    /**
     * Sets the value of the caseListArray property.
     * 
     * @param value
     *     allowed object is
     *     {@link CaseListArrayType }
     *     
     */
    public void setCaseListArray(CaseListArrayType value) {
        this.caseListArray = value;
    }

}
