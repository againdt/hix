<script type="text/ng-template" id="maritalstatus.jsp">
  <div class="gutter10" ng-controller="maritalStatus">
    <h3><spring:message code="label.lce.entereventdetails"/></h3>

    <form class="form-horizontal">
      <div class="control-group">
        <label class="control-label" for="selectcriteriahidden"><spring:message code="label.lce.selectcriteria"/></label>
        <input type="hidden" id="selectcriteriahidden">
        <div class="controls" ng-repeat="subcategory in maritaStatusSubCategory">
          <div ng-if="!((subcategory.value=='DIVORCE_OR_ANULLMENT' && marriedIndicator==null) || (subcategory.value=='DEATH' && marriedIndicator==null)|| (subcategory.value=='MARRIAGE' && marriedIndicator))">
            <label class="radio-inline" for="{{subcategory.value}}">
            <input type="radio" id="{{subcategory.value}}" name="maritalstatus" value="{{subcategory.value}}" ng-model="eventInfo.eventSubCategory" ng-click="removeeditable();currentEvent.dateOfChange=''">
            {{subcategory.label}}
            </label>
          </div>
        </div>
	</div>
    </form>

    <!-- MARRIAGE -->
    <form class="form-horizontal" id="maritalStatusMarriage" name="maritalStatusMarriage" ng-show="eventInfo.eventSubCategory== maritaStatusSubCategory[0].value" novalidate>
      <div class="control-group" >
        <label class="control-label" for="dateOfEvent{{$index}}">
          <spring:message code="label.lce.eventdate"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>">
        </label>
        <div class="controls">
          <div id="dateOfEventMarriage" class="input-append margin20-l" name="dateOfEventMarriage" ng-model="currentEvent.dateOfChange" datetimez ng-required="eventInfo.eventSubCategory== maritaStatusSubCategory[0].value" check-after="0"  >
            <input type="text" id="dateOfEvent{{$index}}" name="dateOfEvent" class="span12" ng-model="currentEvent.dateOfChange" placeholder="MM/DD/YYYY"></input>
            <span class="add-on">
              <i class="icon-calendar"></i>
            </span>
          </div>
        </div>
        <div class="controls">
          <div class="margin20-l">
            <div class="validation-error-text-container" ng-show="submitted && (maritalStatusMarriage.dateOfEventMarriage.$error.required || maritalStatusMarriage.dateOfEventMarriage.$error.pattern)">
              <spring:message code="label.lce.pleaseenterdateofmarriage"/>
            </div>
           
          </div>
		  <div class="margin20-l">
            <div class="validation-error-text-container" ng-show="submitted && maritalStatusMarriage.dateOfEventMarriage.$error.checkAfter">
              <spring:message code="label.lce.futureNotAllowed"/>
            </div>
           
          </div>
        </div>
      </div>
    </form>

    <!-- DATE OF DEATH -->
    <form class="form-horizontal" id="maritalStatusDeath" name="maritalStatusDeath" ng-show="eventInfo.eventSubCategory==maritaStatusSubCategory[2].value" novalidate>
      <div class="control-group">
        <label class="control-label" for="dateOfEvent2">
            <spring:message code="label.lce.eventdate"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>">
        </label>
        <div class="controls">
          <div class="input-append margin20-l" id="dateOfEventDeath" datetimez name="dateOfEventDeath" ng-model="currentEvent.dateOfChange" ng-required="eventInfo.eventSubCategory==maritaStatusSubCategory[2].value" check-after="0" >
            <input type="text" id="dateOfEvent2" name="dateOfEvent" class="span12" ng-model="currentEvent.dateOfChange" required placeholder="MM/DD/YYYY"/>
            <span class="add-on">
              <i class="icon-calendar"></i>
            </span>
          </div>
        </div>
        <div class="controls">
          <div class="margin20-l">
            <div class="validation-error-text-container" ng-if="submitted && (maritalStatusDeath.dateOfEventDeath.$error.required || maritalStatusMarriage.dateOfEventDeath.$error.pattern)">
              <spring:message code="label.lce.pleaseenterdateofdeath"/>
            </div>
			
			<div class="validation-error-text-container" ng-if="submitted && maritalStatusDeath.dateOfEventDeath.$error.checkAfter ">
              <spring:message code="label.lce.futureNotAllowed"/>
            </div>

            <div ng-if='householdMember.dateOfEventDeath.length == 10'>
              <div class="alert controls margin10-t span6 offset2">
                <spring:message code="label.lce.coverageforyourspouse"/>
                {{householdMember.dateOfEventDeath.name.firstName}} {{householdMember.dateOfEventDeath.name.lastName}}
                <spring:message code="label.lce.willendon"/>
                {{householdMember.dateOfEventDeath | date: 'MMMM d, y'}}.
              </div>
            </div>
          </div>
        </div>
      </div>
    </form>

    <!-- DIVORCE INPUT DATE -->
    <form class="form-horizontal" id="maritalStatusDivorce" name="maritalStatusDivorce" novalidate>
      <div class="control-group" ng-if="eventInfo.eventSubCategory==maritaStatusSubCategory[1].value">
        <label class="control-label" for="dateOfEvent3">
          <spring:message code="label.lce.eventdate"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>">
        </label>
        <div class="controls">
          <div class="input-append margin20-l" id="dateOfEventDivorce" datetimez name="dateOfEventDivorce" ng-model="currentEvent.dateOfChange" ng-required="eventInfo.eventSubCategory==maritaStatusSubCategory[1].value" check-after="0" >
            <input type="text" id="dateOfEvent3" name="dateOfEvent" class="span12" ng-model="currentEvent.dateOfChange" required placeholder="MM/DD/YYYY"></input>
            <span class="add-on">
              <i class="icon-calendar"></i>
            </span>
          </div>
        </div>
        <div class="controls">
          <div class="margin20-l">
            <div class="validation-error-text-container" ng-if="submitted && (maritalStatusDivorce.dateOfEventDivorce.$error.required || maritalStatusMarriage.dateOfEventDivorce.$error.pattern)">
              <spring:message code="label.lce.pleaseenterdateofdivorce"/>
            </div>
            
          </div>
		<div class="margin20-l">
            <div class="validation-error-text-container" ng-if="submitted && maritalStatusDivorce.dateOfEventDivorce.$error.checkAfter">
              <spring:message code="label.lce.futureNotAllowed"/>
            </div>
            
          </div>

        </div>

        <div class="margin30-t">
          <spring:message code="label.lce.doyouwanttoremoveyourspousedependent"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>">
          <div class="margin10-l">
            <label class="radio-inline" for="dependentsYes">
              <input type="radio" id="dependentsYes" name="updatedependents" value="Yes" ng-model="$parent.updatedependents" ng-click="$parent.editable=true" required>
              <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio-inline" for="dependentsNo">
              <input type="radio" id="dependentsNo" name="updatedependents" value="No" ng-model="$parent.updatedependents" ng-click="$parent.editable=false" required>
              <spring:message code="label.lce.no"/>
            </label>
          </div>
          <!-- Error -->
          <div class="margin30-l">
            <span class="validation-error-text-container" ng-if="submitted && maritalStatusDivorce.updatedependents.$error.required">
              <spring:message code="label.lce.thisvalueisrequired"/>
            </span>
          </div>
        </div>

      </div>
    </form>

    <table class="table table-striped" ng-if="updatedependentslegal=='Yes'" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
        <tbody>
            <tr>
              <td>
                <spring:message code="label.lce.firstname"/>
              </td>
              <td>
                <spring:message code="label.lce.lastname"/>
              </td>
              <td>
                <spring:message code="label.lce.relationship"/>
              </td>
              <td>
                <spring:message code="label.lce.seekingcoverage"/>
              </td>
              <td>
                <spring:message code="label.lce.edit"/>
              </td>
            </tr>
            <tr ng-repeat="householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
              <td>{{householdMember.name.firstName}}</td>
              <td>{{householdMember.name.lastName}}</td>
              <td>{{getRelationShipWithPrimaryHousehold(householdMember.personId)}}</td>
              <td>
                  <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                  <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
              </td>
              <td>
                <div ng-switch on="getRelationShipCodeWithPrimaryHousehold(householdMember.personId)">
                  <div ng-switch-when="18">
                    <a role="button" tabindex="0" class="btn-primary btn-small" ng-click="addChangedApplicants(householdMember.personId,currentEvent.dateOfChange); householdMember.active=false">
                      <spring:message code="label.lce.remove"/>
                    </a>
                  </div>
                  <div ng-switch-default>
                    ---
                  </div>
                </div>
              </td>
            </tr>
        </tbody>
    </table>

    <!-- Editable table -->
    <div ng-if="editable">
      <div class="margin30-t">
          <h4>Family and Household Summary</h4>
      </div>
      <table class="table table-striped" ng-if="updatedependents=='Yes'" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
          <tbody>
              <tr>
                  <td>
                      <spring:message code="label.lce.firstname"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.lastname"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.relationship"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.seekingcoverage"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.edit"/>
                  </td>
              </tr>
              <tr ng-repeat=" householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                  <td>{{householdMember.name.firstName}}</td>
                  <td>{{householdMember.name.lastName}}</td>
                  <td>{{getRelationShipWithPrimaryHousehold(householdMember.personId)}}</td>
                  <td>
                      <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                      <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                  </td>
                  <td>
                      <label for="remove{{$index}}">
                          <div ng-if="getRelationShipWithPrimaryHousehold(householdMember.personId)=='Self'">
                              ---
                          </div>
                          <div ng-if="getRelationShipWithPrimaryHousehold(householdMember.personId)!='Self' && getRelationShipWithPrimaryHousehold(householdMember.personId)!='Spouse'">
                              ---
                          </div>
                          <a role="button" tabindex="0" class="btn btn-secondary btn-small" ng-click="$parent.$parent.$parent.$parent.submitted=true" ng-if="getRelationShipWithPrimaryHousehold(householdMember.personId)=='Spouse' && !maritalStatusDivorce.$valid">
                              <spring:message code="label.lce.remove"/>
                          </a>
						              <a role="button" tabindex="0" class="btn btn-primary btn-small" ng-click='toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));' ng-if="getRelationShipWithPrimaryHousehold(householdMember.personId)=='Spouse' && maritalStatusDivorce.$valid">
                              <spring:message code="label.lce.remove"/>
                          </a>
                      </label>
                  </td>
              </tr>
          </tbody>
      </table>
    </div>

    <!-- disabled table -->
    <div ng-if="!editable">
      <div class="margin30-t">
          <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
      </div>
      <table class="table table-striped" ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
          <tbody>
              <tr>
                  <td>
                      <spring:message code="label.lce.firstname"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.lastname"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.relationship"/>
                  </td>
                  <td>
                      <spring:message code="label.lce.seekingcoverage"/>
                  </td>

              </tr>
              <tr ng-repeat=" householdMember in taxHousehold.householdMember | filter: activeDeathFilter track by $index" ng-if="filterNewlyAddedHousehold(householdMember)">
                  <td>
                      {{householdMember.name.firstName}}
                  </td>
                  <td>
                      {{householdMember.name.lastName}}
                  </td>
                  <td>
                      {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                  </td>
                  <td>
                      <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                      <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                  </td>
               </tr>
          </tbody>
      </table>
    </div>


    <div class="control-group" ng-if="eventInfo.eventSubCategory== maritaStatusSubCategory[0].value">
        <dl class="dl-horizontal pull-right">
            <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()">
                <spring:message code="label.lce.back"/>
            </a>
            <a role="button" tabindex="0" ng-if="maritalStatusMarriage.$valid" class="btn btn-primary" ng-click="maritalStatusNext(currentEvent.dateOfChange);next()">
                <spring:message code="label.lce.next"/>
            </a>
            <a role="button" tabindex="0" ng-if="!(maritalStatusMarriage.$valid)" class="btn btn-primary" ng-click="$parent.$parent.submitted=true">
                <spring:message code="label.lce.next"/>
            </a>
        </dl>
    </div>


    <div class="control-group" ng-if="eventInfo.eventSubCategory==maritaStatusSubCategory[2].value">
        <dl class="dl-horizontal pull-right">
            <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()">
                <spring:message code="label.lce.back"/>
            </a>
            <a role="button" tabindex="0" ng-if="maritalStatusDeath.$valid" class="btn btn-primary" ng-click="deathDivorseLegalNext(currentEvent.dateOfChange);">
                <spring:message code="label.lce.next"/>
            </a>
            <a role="button" tabindex="0" ng-if="!maritalStatusDeath.$valid" class="btn btn-primary" ng-click="$parent.$parent.submitted=true">
                <spring:message code="label.lce.next"/>
            </a>
        </dl>
    </div>
    <div class="control-group" ng-if="eventInfo.eventSubCategory==maritaStatusSubCategory[1].value">
        <dl class="dl-horizontal pull-right">
            <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()">
                <spring:message code="label.lce.back"/>
            </a>
            <a role="button" tabindex="0" ng-if="maritalStatusDivorce.$valid" class="btn btn-primary" ng-click="deathDivorseLegalNext(currentEvent.dateOfChange);">
                <spring:message code="label.lce.next"/>
            </a>
            <a role="button" tabindex="0" ng-if="!maritalStatusDivorce.$valid" class="btn btn-primary" ng-click="$parent.$parent.submitted=true">
                <spring:message code="label.lce.next"/>
            </a>
        </dl>
    </div>

    <!-- MODAL THAT POPS UP WHEN USER WANT TO EDIT  -->
    <modal-tiny show='modalShown' ng-model="modalHouseholdMember" modal-event-type="modalEventType">
        <form class="form-horizontal">
            <div class="gutter40 margin20-t">
              <spring:message code="lce.areyoursure"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="lce.fromhousehold"/>?
            </div>
            <div class="pull-right">
              <a role="button" tabindex="0" class="btn btn-primary" ng-click="closeModal();">
                <spring:message code="lce.cancelbutton"/>
              </a>
              <a role="button" tabindex="0" class="btn btn-primary" ng-if="!(updatedependents==='No')" ng-click="modalHouseholdMember.active=false;modalHouseholdMember.applyingForCoverageIndicator=false; modelOK(modalHouseholdMember);deathDivorseLegalNext(currentEvent.dateOfChange);">
                <spring:message code="label.lce.remove"/>
              </a>
            </div>
        </form>
    </modal-tiny>
  </div>
</script>