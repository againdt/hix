	<div id="concierge-details" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="concierge-details" aria-hidden="true">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="myModalLabel">GetInsured Concierge</h3>
                </div>
                
                <div class="modal-body">
                	<form class="form-horizontal" id="frmConciergeDetails" name="getinsuredconcierge" action="/hix/memberportal/conciergeDetail" method="POST">
	                	Please give us some more details
	                	
		              	<div class="control-group">
							<label for="subject" class="required control-label"	for="subject">Subject:</label>
							<div class="controls">
								<input type="text" name="subject" id="subject" class="input-xlarge" size="30" value="${conciergeSubject} " />
							</div>
						</div>
						<div class="control-group">
							<label for="message" class="required control-label"	for="message">Message:</label>
							<div class="controls">
								<textarea class="form-control" id="message" rows="4" name="message"></textarea>
							</div>
						</div>
						<div class="modal-footer">
						<input type="hidden" name="memberName" id="memberName" value="${memberName}"/>
						<input type="hidden" name="ticketCategory" id="ticketCategory" />
						<input type="hidden" name="ticketType" id="ticketType" />
                		<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>						
              			<button type="submit" onClick="mapInputData();" class="btn btn-primary pull-right" aria-hidden="true" alt="Go to Federal Marketplace">Next</button>
              	    </div>
					</form>
				</div><!-- /modal-body -->

				
              	           
	</div>
	
	<script>
		function mapInputData() {
			
			var subject = $("#subject").val();
			
			if(subject=='I have an improvement suggestion'||subject=='I am having technical problems with the site'){
				 $("#ticketCategory").val('Feedback');
				 $("#ticketType").val('Provide Feedback');
			}
			else{
				 $("#ticketCategory").val('Triage');
				 $("#ticketType").val('Triage');
			}
			$("#frmConciergeDetails").submit();
		}
	</script>
