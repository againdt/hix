package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.exchange._1_0;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;

/**
 * This class was generated by Apache CXF 2.7.5
 * 2014-03-20T13:11:49.747+05:30
 * Generated source version: 2.7.5
 * 
 */
@WebService(targetNamespace = "http://ridp.dsh.cms.gov/exchange/1.0", name = "FARSPortType")
@XmlSeeAlso({com.getinsured.iex.hub.ridp.fars.niem.niem.appinfo._2.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.exchange._1.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.niem.niem.usps_states._2.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.niem.niem.structures._2.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.niem.niem.niem_core._2.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.cms.hix._0_1.hix_core.ObjectFactory.class, com.getinsured.iex.hub.ridp.fars.niem.niem.proxy.xsd._2.ObjectFactory.class})
@SOAPBinding(parameterStyle = SOAPBinding.ParameterStyle.BARE)
public interface FARSPortType {

    @WebResult(name = "Response", targetNamespace = "http://ridp.dsh.cms.gov/exchange/1.0", partName = "body")
    @WebMethod(operationName = "FARS")
    public com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.ResponsePayloadType fars(
        @WebParam(partName = "body", name = "Request", targetNamespace = "http://ridp.dsh.cms.gov/exchange/1.0")
        com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.RequestPayloadType body
    );
}
