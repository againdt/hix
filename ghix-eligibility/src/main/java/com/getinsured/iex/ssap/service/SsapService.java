package com.getinsured.iex.ssap.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ssap.MultiHousehold;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.dto.SsapPreferencesDTO;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.sql.TSTimestamp;

public interface SsapService {

	int updateSsapApplicationStatus(String applicationStatus, long ssapApplicationId, Timestamp timeStamp) throws GIException;
	int updateSsapApplicationStatusByCaseNumber(String applicationStatus, String caseNumber, Timestamp timeStamp, BigDecimal userId) throws GIException;
	public Map<String,List<Long>> updateApplicantDateOfBirth(List<Long> applicantIds);
	public void updatePreferences(SsapPreferencesDTO ssapPreferencesDTO);
	void updateApplicationRenewalStatus(String caseNumber, RenewalStatus otr, Timestamp timestamp, BigDecimal userId);
	public void processQleValidation(String caseNumber);
	int updateSsapAppStatusOnMemberStatus(List<String> exchgIndivIdList, String applicationStatus, long ssapApplicationId, Timestamp timestamp) throws GIException;
	public List<MultiHousehold> getCMRHouseholdApplicationMembers(List<Integer> householdIds);
	public String getApplicationStatusById(Long ssapApplicationId);
	void updateDentalApplicationStatusByCaseNumber(String caseNumber, TSTimestamp tsTimestamp, BigDecimal userId);
	void updateHouseholdLocation(Location location, Integer cmrHouseholdId);
	boolean cleanupApplicationApplicantVerifications(Long ssapApplicationId);
	List<String> getApplicableVerifications(Long applicantId) throws GIException;
	HouseholdMember getHouseholdMemberForApplicant(SsapApplication ssapApplication, SsapApplicant applicant);
}
