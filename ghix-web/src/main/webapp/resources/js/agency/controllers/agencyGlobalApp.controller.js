(function() {
'use strict';
    angular.module('agencyApp')
    .controller('AgencyController', AgencyController);

    AgencyController.$inject = ['$scope', '$http', '$window', 'StatusService'];

    function AgencyController($scope, $http, $window, StatusService) {
        var vm = this;
	    var status = $("#agencyCertificationStatus").val();
        var logoutUrl = window.location.protocol + '//' + window.location.host + $('#logoutUrl').val();
        //var logoutUrl = $('#logoutUrl').val();
        $scope.blockTerminatedAgency = blockTerminatedAgency;
	    vm.agencyCertificationStatus = StatusService.CertificationStatus;
        vm.adminSwitchToAgency = StatusService.AdminSwitchToAgency;
        vm.currentUserRole = StatusService.CurrentUserRole;
        vm.isAdminRole = StatusService.IsAdminRole;

        if (!vm.adminSwitchToAgency && (vm.agencyCertificationStatus === 'TERMINATED') && !vm.isAdminRole) {
            blockTerminatedAgency();
        }
        //HIX-107631 - Changes http call to window.location.href so it works in internal servers
        function blockTerminatedAgency() {
            window.location.href = logoutUrl;
            $('#blockTerminatedAgencyManagerModal').modal('show');
            window.stop();
        }
    }
})();
