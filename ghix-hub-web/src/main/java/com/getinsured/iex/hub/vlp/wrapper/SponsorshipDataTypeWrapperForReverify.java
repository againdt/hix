package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.SponsorshipDataType;

/**
 * Wrapper Class for SponsorshipDataType
 * 
 * @author Nikhil Talreja
 * @since  14-Feb-2014 
 * 
 */
public class SponsorshipDataTypeWrapperForReverify implements JSONAware{
	
	private String lastName;
	private String firstName;
	private String middleName;
	private String addr1;
	private String addr2;
	private String city;
	private String stateCode;
	private String zipCode;
	private String ssn;
	
	private static final String LAST_NAME_KEY = "LastName";
	private static final String FIRST_NAME_KEY = "FirstName";
	private static final String MIDDLE_NAME_KEY = "MiddleName";
	private static final String ADDR1_KEY = "Addr1";
	private static final String ADDR2_KEY = "Cddr2";
	private static final String CITY_KEY = "City";
	private static final String STATECODE_KEY = "StateCode";
	private static final String ZIPCODE_KEY = "ZipCode";
	private static final String SSN_KEY = "SSN";
	
	public SponsorshipDataTypeWrapperForReverify(SponsorshipDataType sponsorshipDataType){
		this.lastName = sponsorshipDataType.getLastName();
		this.firstName = sponsorshipDataType.getFirstName();
		this.middleName = sponsorshipDataType.getMiddleName();
		this.addr1 = sponsorshipDataType.getAddr1();
		this.addr2 = sponsorshipDataType.getAddr2();
		this.city = sponsorshipDataType.getCity();
		this.stateCode = sponsorshipDataType.getStateCode();
		this.zipCode = sponsorshipDataType.getZipCode();
		this.ssn = sponsorshipDataType.getSSN();
	}
	
	
	
	public String getLastName() {
		return lastName;
	}



	public String getFirstName() {
		return firstName;
	}



	public String getMiddleName() {
		return middleName;
	}



	public String getAddr1() {
		return addr1;
	}



	public String getAddr2() {
		return addr2;
	}



	public String getCity() {
		return city;
	}



	public String getStateCode() {
		return stateCode;
	}



	public String getZipCode() {
		return zipCode;
	}



	public String getSsn() {
		return ssn;
	}



	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(LAST_NAME_KEY,this.lastName);
		obj.put(FIRST_NAME_KEY,this.firstName);
		obj.put(MIDDLE_NAME_KEY,this.middleName);
		obj.put(ADDR1_KEY,this.addr1);
		obj.put(ADDR2_KEY,this.addr2);
		obj.put(CITY_KEY,this.city);
		obj.put(STATECODE_KEY,this.stateCode);
		obj.put(ZIPCODE_KEY,this.zipCode);
		obj.put(SSN_KEY,this.ssn);
		return obj.toJSONString();
	}
	
}
