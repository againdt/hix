<style type="text/css">
</style>
<script type="text/ng-template" id="maritalstatus-address.jsp">
  <div class="gutter10" ng-controller="addressCtrl" >

    <h3><spring:message code="label.lce.address"/></h3>

    <form class="form-vertical" id="primaryContactInformationForm" name="primaryContactInformationForm" novalidate>

      <!-- ADDRESS -->
      <div class="control-group margin20-b">
        <div class="control-label">
          <label for="hidAddressLabel">
            <spring:message code="label.lce.wheredoes"/>
            <strong>{{householdMember.name.firstName| capitalize:true}} {{householdMember.name.middleName| capitalize:true}} {{householdMember.name.lastName| capitalize:true}}</strong>
            <spring:message code="label.lce.live"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
			<input type="hidden" id="hidAddressLabel" /> <!-- Accessibility use -->
        </div>
        <div class="controls">
          <label class="radio-inline" for="sameaddress">
            <input type="radio" id="sameaddress" name="sameaddress" value="Same" ng-value="false" ng-model="householdMember.livesAtOtherAddressIndicator" ng-change="changeAddress();householdMember.otherAddress.address.postalCode='';" required ng-click="updatePostalCode('home_primary_zip')" />
            <spring:message code="label.lce.sameasmyresidence"/>
          </label>
          <label class="radio-inline" for="diffaddress">
            <input type="radio" id="diffaddress" name="sameaddress" value="Different" ng-value="true" ng-model="householdMember.livesAtOtherAddressIndicator" ng-change="changeAddress()" required /> <spring:message code="label.lce.differentlocation"/>
          </label>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" for="home_addressLine2" ng-if="submitted && primaryContactInformationForm.sameaddress.$error.required">
              <spring:message code="label.lce.thisvalueisrequired"/>
            </div>
        </div>
      </div>

      <!-- DIFFERENT LOCATION -->
      <div ng-if="householdMember.livesAtOtherAddressIndicator== true" class="addressBlock form-horizontal">
        <!-- DIFFERENT ADDRESS1 -->
        <div class="control-group">
          <label class="control-label" for="home_addressLine1">
            <spring:message code="label.lce.address1"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <input id="home_addressLine1" name="home_addressLine1" type="text" class="span8" ng-required="householdMember.livesAtOtherAddressIndicator" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" ng-model="householdMember.otherAddress.address.streetAddress1" placeholder='<spring:message code="label.lce.address1"/>'>
          </div>
		
          <div class="controls">
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_addressLine1.$error.required"><spring:message code="label.lce.pleaseenteraddress1"/></div>
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_addressLine1.$error.pattern"><spring:message code="label.lce.entervalidaddress1"/></div>
          </div>
        </div>

        <!-- DIFFERENT ADDRESS2 -->
        <div class="control-group">
          <label class="control-label" for="home_addressLine2">
            <spring:message code="label.lce.address2"/>
          </label>
          <div class="controls">
            <input id="home_addressLine2" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{1,75}$/" name="home_addressLine2" type="text" class="span8" ng-model="householdMember.otherAddress.address.streetAddress2" placeholder='<spring:message code="label.lce.address2"/>'>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_addressLine2" ng-if="submitted && primaryContactInformationForm.home_addressLine2.$error.pattern"><spring:message code="label.lce.entervalidaddress2"/></div>
          </div>
        </div>

        <!-- DIFFERENT CITY -->
        <div class="control-group">
          <label class="control-label" for="home_primary_city">
            <spring:message code="label.lce.city"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <input id="home_primary_city" ng-required="householdMember.livesAtOtherAddressIndicator" ng-minlength="5" ng-maxlength="100" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" name="home_primary_city" type="text" class="span8" ng-model="householdMember.otherAddress.address.city" placeholder='<spring:message code="label.lce.city"/>'>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_primary_city" ng-if="submitted && (primaryContactInformationForm.home_primary_city.$error.minlength || primaryContactInformationForm.home_primary_city.$error.maxlength)"><spring:message code="label.lce.entervalidcity"/></div>
			<div class="validation-error-text-container margin30-l" for="home_primary_city" ng-if="submitted && primaryContactInformationForm.home_primary_city.$error.pattern"><spring:message code="label.lce.entervalidcity"/></div>
            <div class="validation-error-text-container margin30-l" for="home_primary_city" ng-if="submitted && primaryContactInformationForm.home_primary_city.$error.required"><spring:message code="label.lce.pleaseentercity"/></div>
          </div>
        </div>

        <!-- DIFFERENT STATE -->
        <div class="control-group">
          <label class="control-label" for="home_primary_state">
            <spring:message code="label.lce.state"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <select id="home_primary_state" name="home_primary_state" ng-required="householdMember.livesAtOtherAddressIndicator" ng-model="householdMember.otherAddress.address.state" ng-change="householdMember.otherAddress.address.postalCode=null;loadCounties(householdMember.otherAddress.address.state,householdMember.otherAddress.address.postalCode);">
              <option ng-repeat="state in states" value="{{state.stateCode}}"   ng-selected="{{state.stateCode == householdMember.otherAddress.address.state}}" label="{{state.stateName| capitalize:true}}">{{state.stateName| capitalize:true}}</option>
            </select>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_primary_state" ng-if="submitted && primaryContactInformationForm.home_primary_state.$error.required"><spring:message code="label.lce.pleaseselectstate"/></div>
          </div>
        </div>

        <!-- DIFFERENT ZIP -->
        <div class="control-group">
          <label class="control-label" for="home_primary_zip">
            <spring:message code="label.lce.zip"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <input id="home_primary_zip" name="home_primary_zip" ng-required="householdMember.livesAtOtherAddressIndicator" ng-pattern="/^[0-9]{5}(\-[0-9]{4})?$/"    type="text" class="span8" ng-model="householdMember.otherAddress.address.postalCode" ng-change="householdMember.otherAddress.address.county='';loadCounties(householdMember.otherAddress.address.state,householdMember.otherAddress.address.postalCode);" placeholder='<spring:message code="label.lce.zip"/>'>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_primary_zip.$error.required"><spring:message code="label.lce.pleaseenterzip"/></div>
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_primary_zip.$error.pattern"><spring:message code="label.lce.entervalidzip"/></div>
          </div>
        </div>

        <!-- DIFFERENT COUNTY -->
        <div class="control-group margin20-b">
          <label class="control-label" for="home_primary_county">
            <spring:message code="label.lce.county"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <select id="home_primary_county" ng-required="householdMember.livesAtOtherAddressIndicator" name="home_primary_county" ng-model="householdMember.otherAddress.address.county">
              <option value=""><spring:message code="label.lce.selectcounty"/></option>
              <option ng-repeat="(key,value) in counties" value="{{value}}" ng-selected="{{value == householdMember.otherAddress.address.county }}" label="{{key}}">{{key}}</option>
            </select>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_primary_county" ng-if="submitted && primaryContactInformationForm.home_primary_county.$error.required"><spring:message code="label.lce.pleaseselectcounty"/></div>
          </div>
        </div>

        <div class="control-group">
          <label class="control-label" for="hidFirstLastName">
              <spring:message code="label.lce.is"/>
              <strong>{{householdMember.name.firstName| capitalize:true}} {{householdMember.name.middleName| capitalize:true}} {{householdMember.name.lastName| capitalize:true}}</strong>
              <spring:message code="lce.livingoutsideofidaho"/>
              <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
			<input type="hidden" id="hidFirstLastName" /> <!-- Accessibility use -->
          <div class="controls">
            <label class="radio-inline" for="tempresidenceyes">
              <input type="radio" id="tempresidenceyes" name="temporarilyLivingOutsideIndicator" ng-required="householdMember.livesAtOtherAddressIndicator" ng-value="true" ng-model="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator"> <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio-inline" for="tempresidenceno">
              <input type="radio" id="tempresidenceno" name="temporarilyLivingOutsideIndicator" ng-required="householdMember.livesAtOtherAddressIndicator" ng-change="changeTempAddress()"  ng-value="false" ng-model="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator"> <spring:message code="label.lce.no"/>
            </label>
          </div>
          <div class="controls">
            <div class="validation-error-text-container" for="home_primary_county" ng-if="submitted && primaryContactInformationForm.temporarilyLivingOutsideIndicator.$error.required"><spring:message code="label.lce.temporarilyLivingOutside"/></div>
          </div>
        </div>
      </div>


      <!-- OUTSIDE CITY-->
      <div ng-if="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
        <div class="control-group">
          <label class="control-label" for="home_temp_city">
            <spring:message code="label.lce.city"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <input id="home_temp_city" name="home_temp_city" type="text" ng-minlength="5" ng-maxlength="100" ng-required="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && householdMember.livesAtOtherAddressIndicator" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" class="" ng-model="householdMember.otherAddress.livingOutsideofStateTemporarilyAddress.city" placeholder='<spring:message code="label.lce.city"/>'>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_temp_city.$error.required"><spring:message code="label.lce.pleaseentercity"/></div>
            <div class="validation-error-text-container margin30-l" ng-if="submitted && primaryContactInformationForm.home_temp_city.$error.pattern"><spring:message code="label.lce.entervalidcity"/></div>
			<div class="validation-error-text-container margin30-l" ng-if="submitted && (primaryContactInformationForm.home_temp_city.$error.minlength || primaryContactInformationForm.home_temp_city.$error.maxlength)"><spring:message code="label.lce.entervalidcity"/></div>
          </div>
        </div>
      </div>

      <!-- OUTSIDE STATE-->
      <div ng-if="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
        <div class="control-group">
          <label class="control-label" for="home_temp_state">
            <spring:message code="label.lce.state"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <select id="home_temp_state" name="home_temp_state" ng-required="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && householdMember.livesAtOtherAddressIndicator"  ng-model="householdMember.otherAddress.livingOutsideofStateTemporarilyAddress.state" ng-options="state.stateCode as state.stateName for state in states"></select>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_temp_state" ng-if="submitted && primaryContactInformationForm.home_temp_state.$error.required"><spring:message code="label.lce.pleaseselectstate"/></div>
          </div>
        </div>
      </div>

      <!-- OUTSIDE ZIP-->
      <div ng-if="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator== true" class="addressBlock form-horizontal">
        <div class="control-group">
          <label class="control-label" for="home_temp_zip">
            <spring:message code="label.lce.zip"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </label>
          <div class="controls">
            <input id="home_temp_zip" name="home_temp_zip" ng-required="householdMember.otherAddress.livingOutsideofStateTemporarilyIndicator && householdMember.livesAtOtherAddressIndicator" ng-pattern="/^[0-9]{5}(\-[0-9]{4})?$/" type="text" ng-model="householdMember.otherAddress.livingOutsideofStateTemporarilyAddress.postalCode" placeholder='<spring:message code="label.lce.zip"/>'>
          </div>
          <div class="controls">
            <div class="validation-error-text-container margin30-l" for="home_temp_zip" ng-if="submitted && primaryContactInformationForm.home_temp_zip.$error.required"><spring:message code="label.lce.pleaseenterzip"/></div>
			<div class="validation-error-text-container margin30-l" for="home_temp_zip" ng-if="submitted && primaryContactInformationForm.home_temp_zip.$error.pattern"><spring:message code="label.lce.entervalidzip"/></div>
          </div>
        </div>
      </div>
    </form>

  	<!-- BACK & NEXT BUTTON -->
  	<dl class="dl-horizontal pull-right" address-validation-directive="householdMember.otherAddress.address" address-validation-callback="next()" address-validation-same-address-dto="false">
  		<a role="button" tabindex="0" class="btn btn-secondary" ng-click="back();$root.isBackFromAdressPage=true;"><spring:message code="label.lce.back"/></a>
  		<a role="button" tabindex="0" ng-if="primaryContactInformationForm.$valid" class="btn btn-primary" ng-class="householdMember.livesAtOtherAddressIndicator? 'address-validation-click':''" ng-click="!householdMember.livesAtOtherAddressIndicator && next()"><spring:message code="label.lce.next"/></a>
  		<a role="button" tabindex="0" ng-if="!primaryContactInformationForm.$valid" class="btn btn-primary" ng-click="$parent.$parent.submitted=true"><spring:message code="label.lce.next"/></a>
  	</dl>
  </div>
</script>