import { OE, QEP } from '../constants/constants';
import axios from "axios";

import { APP_STATUSES } from '../constants/dashboardModelTypes';

/**
 * Redirect to AHBX pages (CA specific)
 */
export const stateSpecificAHBXRedirect = () => {
    const houseHoldCaseId = $("#householdCaseID").val();
    if (houseHoldCaseId && houseHoldCaseId !== 'null') {
        window.location = `https://${calheersEnv}/static/lw-web/account-home?householdCaseId=${houseHoldCaseId}`;
    } else {
        window.location = `https://${calheersEnv}/static/lw-web/account-home`;
    }
};

/**
 * For Idaho only redirect cost savings.
 */
export const applyForCostSavings = () => {
    window.location = '/hix/individual/saml/idalinkredirect';
};

export const goToSSAPStart = (caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus) => {
    let location;
    let applicationType = isInsideOEWindow ? OE : QEP;

    if (ssapFlowVersion && ssapFlowVersion === '2.0') {
        if (caseNumber && caseNumber !== 'undefined') {
            window.caseNumber = caseNumber;
            location = `/hix/newssap/start?caseNumber=${caseNumber}`;
        } else {
            location = `/hix/newssap/start?applicationType=${applicationType}&coverageYear=${applicationYear}`;
        }
    } else if (applicationStatus === APP_STATUSES.CL || applicationStatus === APP_STATUSES.CC) {
        location = isInsideOEWindow ? `/hix/indportal/startOEApp?coverageYear=${applicationYear}` : `/hix/indportal/startQepApp?coverageYear=${applicationYear}`;
    } else {
        if (caseNumber && caseNumber !== 'undefined') {
            window.caseNumber = caseNumber;
            location = "/hix/indportal/gotossap?caseNumber=" + caseNumber;
        } else {
            location = isInsideOEWindow ? `/hix/indportal/startOEApp?coverageYear=${applicationYear}` : `/hix/indportal/startQepApp?coverageYear=${applicationYear}`;
        }
    }

    window.location = location;
};

export const defaultReportAChange = (caseNumber, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus) => {
    if (ssapFlowVersion && ssapFlowVersion === '2.0') {
        const tokId = $('#tokid').val();
        
        axios.post(`/hix/indportal/cloneApplicationByCaseNumber/${caseNumber}?csrftoken=${tokId}`)
            .then((response) => {
                if (response && response.data) {
                    let clonedCaseId = response.data;
                    goToSSAPStart(clonedCaseId, applicationYear, isInsideOEWindow, ssapFlowVersion, applicationStatus);
                } else {
                    window.location = '/hix/indportal#/error';
                }
            });
    } else {
        if (typeof caseNumber !== 'undefined') {
            const tokId = $('#tokid').val();
            axios.post(`/hix/indportal/sepcheck?caseNumber=${caseNumber}&csrftoken=${tokId}`)
                .then((response) => {
                    if (response && response.data) {
                        window.location = `./iex/lce/reportyourchange?coverageYear=${applicationYear}`;
                    } else {
                        window.location = './indportal#/qephome';
                    }
                });
        } else {
            window.location = "./indportal#/qephome";
        }
    }
};
