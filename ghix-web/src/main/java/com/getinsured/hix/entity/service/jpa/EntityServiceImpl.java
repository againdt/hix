package com.getinsured.hix.entity.service.jpa;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.Blob;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.activation.MimetypesFileTypeMap;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.jsoup.helper.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.broker.BrokerConfirmationEmail;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.EntityDocumentDTO;
import com.getinsured.hix.dto.finance.PaymentMethodDTO;
import com.getinsured.hix.entity.service.EntityService;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GHIXApplicationContext;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods.PaymentType;
import com.getinsured.hix.model.VimoEncryptor;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * EntityServiceImpl is the implementation of {@link EntityService}
 * 
 */

@Service("entityService")
@Repository
@Transactional
public class EntityServiceImpl implements EntityService {

	private static final String BR = ",<br/>";

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EntityServiceImpl.class);
	
	private static final String IMAGE_TYPE = "image/jpeg";
	private static final String ASSISTER_IMAGE = "assister_image";
	private static final String JPG = "jpg";
	private static final String SYSTEM = "SYSTEM";

	private static final String SUCCESS = "Success";

	private static final String FAILURE = "Failure";

	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	private static final String HEADER_CONTENT = "headerContent";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String FOOTER_CONTENT = "footerContent";
	
	private static final String ENROLLMENTENTITY_NAME = "EnrollmentEntityName";
	private static final String ENROLLMENTENTITY_BUSINESS_NAME = "EnrollmentEntityBusinessName";
	private static final String ENROLLMENTENTITY_COMPANY_ADDRESS = "EnrollmentEntityCompanyAddress";
	private static final String PRIMARY_CONTACT_NAME = "PrimaryContactName";
	private static final String NEW_CERT_STATUS = "newCertificationStatus";
	private static final String PRIOR_CERT_STATUS = "priorCertificationStatus";
	private static final String STAT_COMMENT_CHANGE = "StatusChangeComment";
	private static final String BROKER_CERT_NOTIFICATION = "entitycertnotification_";
	private static final String PDF = ".pdf";
	private static final String ECMRELATIVEPATH = "entitycertnotifications";
	private static final String NOTIFICATION_TEMPLATE_NAME = "EnrollmentEntityCertificationNotification";
	private static final String NOTIFICATION_SEND_DATE = "notificationDate";
	public static final String PRIMARYSITE = "PRIMARY";
	private static final String ASSISTER_FIRST_NAME = "EnrollmentCounselorFirstName";
	private static final String ASSISTER_LAST_NAME = "EnrollmentCounselorLastName";
	private static final String NOTIFICATION_TEMPLATE_NAME_CEC = "CertifiedEnrollmentCounselorCertificationNotification";	
	private static final String NOTIFICATION_TEMPLATE_NAME_CEC_PAPER_NOTICE ="CertifiedEnrollmentCounselorCertificationNotificationWithPostalMail";
	
	private static final int ZERO = 0;
 	private static final int ONE = 1;
 	private static final int THREE = 3;
 	private static final int FOUR = 4;
 	private static final int FIVE = 5;
 	private static final int SIX = 6;
 	private static final int SEVEN = 7;
 	private static final int EIGHT = 8;
 	private static final int NINE = 9;
 	private static final int TEN = 10;
 	private static final int ELEVEN = 11;
 	private static final int TWELVE = 12;
 	private static final int THIRTEEN = 13;
	
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;

	@Autowired
	private ContentManagementService ecmService;
	
	@Autowired
	private EERestCallInvoker restClassCommunicator;

	@Autowired
	private UserService userService;
	
	@Autowired 
	private AccountActivationService accountActivationService;
	
	@Autowired
	private NoticeService noticeService;
	
	@Autowired 
	private ApplicationContext appContext;
	
	@Autowired
	private BrokerConfirmationEmail notificationAgent;	
	
	/**
	 * @see EntityService#getAssisterPhotoById(int)
	 * 
	 * @param assisterId
	 * 
	 * @param fileInput
	 * @throws Exception 
	 * 
	 */
	@Override
	public byte[] getAssisterPhotoById(int assisterId) throws Exception{
		
		LOGGER.debug(" Get Assister photo by assister id Starts");
		
		byte[] assisterPhoto = null;
		
		EntityDocumentDTO entityDocumentDTO = getAssisterPhotoAndDocumentId(assisterId);
		LOGGER.info("EntityDocumentDTO : "+entityDocumentDTO);
		Integer documentId = entityDocumentDTO.getAssisterPhotoDocumentId();
		LOGGER.info("Document Id : "+documentId);
		
		if(documentId!=null && documentId!=0){
			assisterPhoto = retrievePhotoFromECM(getECMDocumentId(documentId));
			LOGGER.info("Byte Array : "+assisterPhoto);
		} else {
			assisterPhoto = entityDocumentDTO.getAssisterPhoto();
			if(assisterPhoto!=null){
				saveAssisterPhoto(assisterId, assisterPhoto);
			}
		}
		
		LOGGER.info(" Get Assister photo by assister id Ends");
		
		return assisterPhoto;
	}
	
	/**
	 * @see EntityService#saveAssisterPhoto(int, MultipartFile)
	 * 
	 * @param assisterId
	 * 
	 * @param fileInput
	 * @throws Exception 
	 */
	@Override
	public void saveAssisterPhoto(int assisterId, MultipartFile fileInput) throws Exception {

		LOGGER.info("setPhoto: START");

		String documentId = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponse = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		int entityDocumentId = 0;
		EntityDocuments entityDocument = null;
		boolean updateDocumentId = false;
		
		if (fileInput != null && fileInput.getSize() > 0 && assisterId > 0) {
			try {
				LOGGER.debug("Saving Assister photo for Assister Id");
				
				documentId = savePhotoOnECM(fileInput, assisterId);
				
				if(!EntityUtils.isEmpty(documentId)){
					String createdBy = userService.getLoggedInUser().getEmail();

					Integer entityDocumentID = getEntityDocumentId(assisterId);
					
					if(entityDocumentID!=null && entityDocumentID!=0){
						entityDocument = findEntityDocuments(entityDocumentID);
					} else {
						entityDocument = new EntityDocuments();
						updateDocumentId = true;
					}
					
					MimetypesFileTypeMap mimeTypesMap = new MimetypesFileTypeMap();
					String mimeType = mimeTypesMap.getContentType(fileInput.getOriginalFilename());
					entityDocument.setCreatedBy(createdBy);
					entityDocument.setCreatedDate(new TSDate());
					entityDocument.setDocumentName(documentId);
					entityDocument.setOrgDocumentName(fileInput.getOriginalFilename());
					entityDocument.setMimeType(mimeType);
					enrollmentEntityRequestDTO.setEntityDocuments(entityDocument);

					// save document info in broker document
					String entityDocsStr = restClassCommunicator.saveDocument(enrollmentEntityRequestDTO);

					enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
					EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();
					entityDocumentId = entityDocuments.getID();
					
					if(updateDocumentId){
						updateAssisterPhotoDocumentId(assisterId, entityDocumentId);
					}
				}
				
				LOGGER.debug("Saved Assister Photo. Entity Document ID");
				
			} catch (Exception exception) {
				LOGGER.error("Exception occurred while Uploading Assister Photo : ", exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		}
		LOGGER.info("setPhoto: END");
	}

	private void saveAssisterPhoto(int assisterId, byte[] fileInput) throws Exception {

		LOGGER.info("setPhoto: START");

		String documentId = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponse = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		int entityDocumentId = 0;
		EntityDocuments entityDocument = null;
		
		if (fileInput != null && assisterId > 0) {
			try {
				LOGGER.debug("Saving Assister photo for Assister Id");
				
				documentId = saveExistingPhotoOnECM(fileInput, assisterId);
				
				if(!EntityUtils.isEmpty(documentId)){
					entityDocument = new EntityDocuments();
					entityDocument.setCreatedBy(SYSTEM);
					entityDocument.setCreatedDate(new TSDate());
					entityDocument.setDocumentName(documentId);
					entityDocument.setOrgDocumentName(getExistingAssisterFileName());
					entityDocument.setMimeType(IMAGE_TYPE);
					enrollmentEntityRequestDTO.setEntityDocuments(entityDocument);

					String entityDocsStr = restClassCommunicator.saveDocument(enrollmentEntityRequestDTO);

					enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
					EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();
					entityDocumentId = entityDocuments.getID();
					
					updateAssisterPhotoDocumentId(assisterId, entityDocumentId);
				}
				
				LOGGER.debug("Saved Assister Photo. Document ID");
			} catch (Exception exception) {
				LOGGER.error("Exception occurred while Uploading Photo : ", exception);
				throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			}
		}
		LOGGER.info("setPhoto: END");
	}
	
	private EntityDocumentDTO getAssisterPhotoAndDocumentId(int assisterId) throws Exception {
		
		LOGGER.debug(" Get Assister photo and document Id by assister id Starts");
		EntityManager em = null;

		Query query = null;
		String queryStr = null;
		EntityDocumentDTO entityDocumentDTO = new EntityDocumentDTO();
		
		try {
			em = entityManagerFactory.createEntityManager();
			
			queryStr = "SELECT ASSISTER_PHOTO_DOCUMENT_ID, PHOTO FROM EE_ASSISTERS WHERE ID = ?";
			
			query = em.createNativeQuery(queryStr);
			query.setParameter(1, assisterId);
			
			List<Object[]> resultList = query.getResultList();
			
			if ( resultList != null && !resultList.isEmpty() ) {
			 	Object[] assisterObjArr = resultList.get(0); 
			 	
			 	if (assisterObjArr[0] != null){   
					entityDocumentDTO.setAssisterPhotoDocumentId(Integer.valueOf(assisterObjArr[0].toString()));
				}
				
				if (assisterObjArr[1] != null){   
					Blob photoblob = (Blob)assisterObjArr[1];
					entityDocumentDTO.setAssisterPhoto(photoblob.getBytes(1, (int)photoblob.length()));
				}
			}	
		}
		catch (Exception exception) {
			LOGGER.error("Exception while retrieving assister photo in getAssisterPhotoAndDocumentId method: " + exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		LOGGER.debug(" Get Assister photo and document Id by assister id Ends.");
		
		return entityDocumentDTO;
	}
	
	private int updateAssisterPhotoDocumentId(int assisterId, int documentId)
			throws GIException {
		LOGGER.debug(" Update Assister table with ECM Document ID Starts.");

		int updateStatus = 0;
		final int assisterID = assisterId;
		final int documentID = documentId;
		EntityManager em = null;

		try {
			em = entityManagerFactory.createEntityManager();
			em.getTransaction().begin();
			
			Query query = em
					.createNativeQuery("UPDATE EE_ASSISTERS SET ASSISTER_PHOTO_DOCUMENT_ID = ?,PHOTO=NULL WHERE ID = ?");
			
			query.setParameter(1, documentID);
			query.setParameter(2, assisterID);
			updateStatus = query.executeUpdate();

			em.getTransaction().commit();
		}
		catch (Exception exception) {
			LOGGER.error("Exception while updating assister table with ECM Document ID : "+ exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		LOGGER.debug(" Update Assister table with ECM Document ID Ends. Status : " + updateStatus);
		
		return updateStatus;
	}
	
	private EntityDocuments findEntityDocuments(Integer entityDocumentId) throws JsonProcessingException, IOException{
		LOGGER.debug("Find Entity Document by ID Starts.");
		
		EnrollmentEntityResponseDTO enrollmentEntityResponse = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setModuleId(entityDocumentId);
		String entityDocsStr = restClassCommunicator.retrieveEntityDocumentsObjectById(enrollmentEntityRequestDTO);
		
		enrollmentEntityResponse = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class).readValue(entityDocsStr);
		EntityDocuments entityDocuments = enrollmentEntityResponse.getEntityDocuments();
		
		LOGGER.debug("Find Entity Document by ID Ends.");
		
		return entityDocuments;
	}
	
	private Integer getEntityDocumentId(int assisterId) throws GIException {

		LOGGER.debug(" Get Entity Document ID Starts.");

		String queryStr = null;
		Integer documentId = null;
		EntityManager em =  null;
		Query query = null;
		
		try {
			em  = entityManagerFactory.createEntityManager();
			queryStr = "SELECT ASSISTER_PHOTO_DOCUMENT_ID FROM EE_ASSISTERS WHERE ID=?";
			
			query = em.createNativeQuery(queryStr);
			query.setParameter(1, assisterId);

			List list = query.getResultList();

			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					documentId = Integer.valueOf(obj.toString());
				}
				break;
			}
		} catch (Exception exception) {

			LOGGER.error("Exception while retrieving assister photo document id: ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		LOGGER.debug(" Get Entity Document ID Ends.");

		return documentId;
	}
	
	private String savePhotoOnECM(MultipartFile file, int assisterId) throws Exception {
		LOGGER.debug("Save Photo on ECM Starts.");
		
		String strDocumentId = null;
		
		try {
			// creating a new file name with the time stamp
			String modifiedFileName = FilenameUtils.getBaseName(file
					.getOriginalFilename())
					+ GhixConstants.UNDERSCORE
					+ TimeShifterUtil.currentTimeMillis()
					+ GhixConstants.DOT
					+ FilenameUtils.getExtension(file.getOriginalFilename());

			strDocumentId = ecmService.createContent(
					GhixConstants.ASSISTER_IMAGE_PATH + "/" + assisterId + "/"
							+ "ADD_SUPPORT_FOLDER", modifiedFileName,
					file.getBytes()
					, ECMConstants.Entity.DOC_CATEGORY, ECMConstants.Entity.PHOTO, null);

		} catch (Exception exception) {
			LOGGER.error("Unable to upload photo", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.debug("Save Assister Photo on ECM Ends. Document ID.");
		return strDocumentId;
	}
	
	private String saveExistingPhotoOnECM(byte[] file, int assisterId) throws Exception {
		LOGGER.debug("Save Photo on ECM Starts.");
		String strDocumentId = null;
		
		try {
			strDocumentId = ecmService.createContent(
					GhixConstants.ASSISTER_IMAGE_PATH + "/" + assisterId + "/"
							+ "ADD_SUPPORT_FOLDER", getExistingAssisterFileName(), file
							, ECMConstants.Entity.DOC_CATEGORY, ECMConstants.Entity.PHOTO, null);

		} catch (Exception exception) {
			LOGGER.error("Unable to upload file", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("Save Photo on ECM Ends. Document ID.");
		
		return strDocumentId;
	}
	
	private byte[] retrievePhotoFromECM(String documentId) throws ContentManagementServiceException {
		LOGGER.debug("Retrieve Photo from ECM Starts.");
		
		byte[] attachment = null;
		
		try {
			
			LOGGER.info("UCM URL : "+AHBXEndPoints.PLATFORM_UCM_GET_FILE_CONTENT);
			attachment = ecmService.getContentDataById(documentId);
		} catch (ContentManagementServiceException e) {
			LOGGER.error("Unable to show attachment", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Retrieve Photo from ECM Ends");
		return attachment;
	}

	private String getECMDocumentId(int documentId) throws GIException {

		LOGGER.debug(" Get ECM Document ID by Assister Document ID Starts.");

		String queryStr = null;
		String ecmDocumentId = null;
		Query query = null;
		EntityManager em = null;
		
		try {
			em = entityManagerFactory.createEntityManager();

			queryStr = "SELECT DOCUMENT_NAME FROM EE_DOCUMENTS WHERE ID=?";
			
			query = em.createNativeQuery(queryStr);
			query.setParameter(1, documentId);
			
			List list = query.getResultList();

			Iterator iterator = list.iterator();
			while (iterator.hasNext()) {
				Object obj = iterator.next();
				if (obj != null) {
					ecmDocumentId = obj.toString();
				}
				break;
			}
		} catch (Exception exception) {
			LOGGER.error("Exception while retrieving ECM Document ID : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		LOGGER.info(" ECM Document ID by Assister Document ID Ends."+ecmDocumentId);
		
		return ecmDocumentId;
	}
	
	private String getExistingAssisterFileName(){
		return ASSISTER_IMAGE + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + JPG;
	}

	@Override
	public String generateAssisterActivationLink(Assister assister)
			throws GIException {
		String activationLinkResult = SUCCESS;
		
		Map<String,String> assisterActivationDetails = new HashMap<String, String>();
		assisterActivationDetails.put("assisterName", assister.getFirstName());
		assisterActivationDetails.put(EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		assisterActivationDetails.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		assisterActivationDetails.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		assisterActivationDetails.put("stateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		assisterActivationDetails.put("emailType","assisterActivationEmail");
		assisterActivationDetails.put("expirationDays", DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ASSISTER));

		CreatedObject createdObject = new CreatedObject();
		int createdObjectID = assister.getId();
		String createdObjectType = GhixRole.ASSISTER.toString();
		List<String> phoneList = new ArrayList<String>(); 
		phoneList.add(assister.getPrimaryPhoneNumber()); 

		createdObject.setObjectId(createdObjectID);
		createdObject.setEmailId(assister.getEmailAddress());
		createdObject.setRoleName(createdObjectType);
		createdObject.setPhoneNumbers(phoneList);
		createdObject.setFullName(assister.getFirstName() + " " + assister.getLastName());
		createdObject.setFirstName(assister.getFirstName());
		createdObject.setLastName(assister.getLastName());
		createdObject.setCustomeFields(assisterActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		int creatorObjectID = assister.getEntity().getId();
		String creatorObjectType = GhixRole.ENROLLMENTENTITY.getRoleName();
		creatorObject.setObjectId(creatorObjectID);
		creatorObject.setFullName(assister.getEntity().getEntityName());
		creatorObject.setRoleName(creatorObjectType);

		// 1. Get the Activation Object w.r.t createdObjectID , creatorObjectID,creatorObjectType,createdObjectType and status=notprocessed.
		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectIdAndCreatorObjectId(createdObjectID,createdObjectType,
				creatorObjectID, creatorObjectType, STATUS.NOTPROCESSED );

		// 2. If the activation object is not null, then call resendMail method and update the existing activation object.
		if(accountActivation != null) {
			accountActivation =  accountActivationService.resendActivationMail(createdObject, creatorObject, Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ASSISTER)), accountActivation);
		}
		else //3. If the activation object is null, then call initiateActivationForCreatedRecord method to create a new object
		{

			accountActivation =  accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject,
					Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ASSISTER)));
		}
		if (accountActivation != null) {
			LOGGER.info("Account activation link sent to the employer...");
			activationLinkResult = SUCCESS;			
		}
		else
		{
			activationLinkResult = FAILURE;

		}		
		return activationLinkResult;
	}
	
	@Override
	public void createEntityStatusChangeNotice(EnrollmentEntity enrollmentEntity, String priorCertificationStatus) throws NoticeServiceException, JsonProcessingException, IOException {
		//String documentId = null;
		Validate.notNull(enrollmentEntity);
		AccountUser objUser = enrollmentEntity.getUser();
		List<String> sendToEmailList = null;
		EntityLocation location = getSiteDataBySiteType(PRIMARYSITE, objUser.getId()).getMailingLocation();
		String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME;
		Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
		assisterTemplateData.put(ENROLLMENTENTITY_NAME, enrollmentEntity.getEntityName());
		assisterTemplateData.put(ENROLLMENTENTITY_BUSINESS_NAME, enrollmentEntity.getBusinessLegalName());
		assisterTemplateData.put(PRIMARY_CONTACT_NAME, enrollmentEntity.getPriContactName());
		assisterTemplateData.put(PRIOR_CERT_STATUS, priorCertificationStatus);
	    StringBuilder entityPrimaryAddressAddress = new StringBuilder("");
	
	    if(null != enrollmentEntity  &&  null != location ) {
	          
	        entityPrimaryAddressAddress.append(location.getAddress1() + BR);
	
	        if(null != location.getAddress2() && !location.getAddress2().isEmpty()) {
	                entityPrimaryAddressAddress.append(location.getAddress2() + BR);
	        }
	
	        entityPrimaryAddressAddress.append(location.getCity() + ",&nbsp;");
	        entityPrimaryAddressAddress.append(location.getState() + " ");
	        entityPrimaryAddressAddress.append(location.getZip());
	    }
	
	    //brokerTemplateData.put(BROKER_COMPANY_ADDRESS, brokerObj.getCompanyName() + "<br/>" + brokerCompanyAddress.toString());
	    assisterTemplateData.put(ENROLLMENTENTITY_COMPANY_ADDRESS , entityPrimaryAddressAddress.toString());
		assisterTemplateData.put(NEW_CERT_STATUS, enrollmentEntity.getRegistrationStatus());
	
		SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
		assisterTemplateData.put(STAT_COMMENT_CHANGE, enrollmentEntity.getComments());
		String fileName = BROKER_CERT_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;
	
		// tokens required for the notification template
		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		// HIX-20774
		
		assisterTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
	
		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
		
		Map<String, String> templateDataModified = getTemplateDataMap(assisterTemplateData);
		sendToEmailList = new LinkedList<String>();
		sendToEmailList.add(objUser.getEmail());
		sendToEmailList.add(enrollmentEntity.getPriContactEmailAddress());
		//noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileName, objUser);
		noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileName, ModuleUserService.ENROLLMENTENTITY_MODULE, enrollmentEntity.getId(), sendToEmailList, "Exchange Admin", enrollmentEntity.getPriContactName());
	}
	
	private Site getSiteDataBySiteType(String siteType, Integer id) throws JsonProcessingException, IOException {
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = new EnrollmentEntityRequestDTO();
		enrollmentEntityRequestDTO.setModuleId(id);
		enrollmentEntityRequestDTO.setUserId(id);
		enrollmentEntityRequestDTO.setSiteType(siteType);
		String siteDetails = restClassCommunicator.retrieveSiteByType(enrollmentEntityRequestDTO);
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityResponseDTO.class);
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = reader.readValue(siteDetails);
		return enrollmentEntityResponseDTO.getSite();
	}
	
	private String getTemplateContentWithTokensReplaced(Map<String, Object> tokens, String location ) throws Exception{
		InputStream inputStreamUrl = null;
		String templateContent = "";
		try {
			Resource resource = appContext.getResource("classpath:"+location);
			inputStreamUrl = resource.getInputStream();
			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (Exception e) {
			LOGGER.error("Error reading content from specified location.", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally{
			IOUtils.closeQuietly(inputStreamUrl);
		}
		
		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();	
		Template tmpl = null;
		String responseData = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
			responseData = sw.toString();
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(sw);
		}
		return responseData;
		
	}

	@Override
	public void createCECStatusChangeNotice(Assister assister,
			String priorCertificationStatus) throws NoticeServiceException, JsonProcessingException, IOException {
		LOGGER.info(" Get CEC Certification change notification method Starts : ");
		Validate.notNull(assister);
		Validate.notNull(assister.getEntity());
		AccountUser objUser = assister.getEntity().getUser();
		List<String> sendToEmailList = null;
		EntityLocation location = getSiteDataBySiteType(PRIMARYSITE, objUser.getId()).getMailingLocation();
		String noticeTemplateName = NOTIFICATION_TEMPLATE_NAME_CEC;
		Map<String, Object> assisterTemplateData = new HashMap<String, Object>();
		assisterTemplateData.put(ASSISTER_FIRST_NAME, assister.getFirstName());
		assisterTemplateData.put(ASSISTER_LAST_NAME, assister.getLastName());
		assisterTemplateData.put(PRIMARY_CONTACT_NAME, assister.getFirstName());
		assisterTemplateData.put(PRIOR_CERT_STATUS, priorCertificationStatus);
	    StringBuilder assisterPrimaryAddressAddress = new StringBuilder("");
	
	    if(null != assister  &&  null != location ) {
	          
	    	assisterPrimaryAddressAddress.append(location.getAddress1() + BR);
	
	                    if(null != location.getAddress2() && !location.getAddress2().isEmpty()) {
	                    	assisterPrimaryAddressAddress.append(location.getAddress2() + BR);
	                    }
	
	                    assisterPrimaryAddressAddress.append(location.getCity() + ",&nbsp;");
	                    assisterPrimaryAddressAddress.append(location.getState() + " ");
	                    assisterPrimaryAddressAddress.append(location.getZip());
	           
	    }
	    
	    //brokerTemplateData.put(BROKER_COMPANY_ADDRESS, brokerObj.getCompanyName() + "<br/>" + brokerCompanyAddress.toString());
	    assisterTemplateData.put(ENROLLMENTENTITY_COMPANY_ADDRESS , assisterPrimaryAddressAddress.toString());
		assisterTemplateData.put(NEW_CERT_STATUS, assister.getCertificationStatus());
	
		SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
		assisterTemplateData.put(NOTIFICATION_SEND_DATE, sdf.format(new TSDate()));
		assisterTemplateData.put(STAT_COMMENT_CHANGE, assister.getComments()!=null?assister.getComments():"");
		String fileName = BROKER_CERT_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;
	
		// tokens required for the notification template
		assisterTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		assisterTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		assisterTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		assisterTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		assisterTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		assisterTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		assisterTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		// HIX-20774
		
		assisterTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
	
		assisterTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		assisterTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		assisterTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		assisterTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);
		
		Map<String, String> templateDataModified = getTemplateDataMap(assisterTemplateData);
		sendToEmailList = new LinkedList<String>();
		sendToEmailList.add(objUser.getEmail());
		sendToEmailList.add(assister.getEmailAddress());
		//noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileName, objUser);
		String isAllowMailNotices= DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.ENROLLMENTENTITY_CEC_ALLOWMAILNOTICES);
		Location localLocation= new Location();
		if("Y".equalsIgnoreCase(assister.getPostalMail()) && "Y".equalsIgnoreCase(isAllowMailNotices)){
			localLocation.setAddOnZip(Integer.parseInt(location.getZip()));
			localLocation.setAddress1(location.getAddress1());
			localLocation.setAddress2(location.getAddress2());
			localLocation.setCity(location.getCity());
			localLocation.setCounty(location.getCounty());
			localLocation.setCountycode(location.getCounty());
			localLocation.setId(location.getId());
			localLocation.setLat(location.getLat());
			localLocation.setLon(location.getLon());
			localLocation.setRdi(location.getRdi());
			localLocation.setState(location.getState());
			localLocation.setUpdated(location.getUpdatedOn());
			localLocation.setZip(location.getZip()+"");
			
			noticeService.createModuleNotice(NOTIFICATION_TEMPLATE_NAME_CEC_PAPER_NOTICE, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileName, "assister", (long)assister.getId(), sendToEmailList, "Exchange Admin", assister.getFirstName(),localLocation,GhixNoticeCommunicationMethod.Mail);
			
		}else{
			noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, assisterTemplateData, ECMRELATIVEPATH, fileName, "assister", assister.getId(), sendToEmailList, "Exchange Admin", assister.getFirstName());
		}
		
		LOGGER.info(" Get CEC Certification change notification method Ends : ");
		
	}
	
	private Map<String, String> getTemplateDataMap(Map<String, Object> templateData) {
		Map<String, String> templateDataModified = new HashMap<String, String>();
	    Iterator<Entry<String, Object>> it = templateData.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
	        if(pairs.getValue() != null) {
	        	templateDataModified.put(pairs.getKey(), pairs.getValue().toString());
	        }
	    }
		return templateDataModified;
	}	
	
	public List<PaymentMethodDTO> getPaymentMethodMigrationData(int maxResult)
			throws GIRuntimeException {
		LOGGER.debug(" In getPaymentMethodMigrationData ");
		EntityManager em = null;

		Query query = null;
		List<PaymentMethodDTO> paymentMethodDTOList = null;
		try {
			em = entityManagerFactory.createEntityManager();
			StringBuilder buildquery = new StringBuilder();
			buildquery.append("SELECT pm.id, pm.payment_type, pm.payment_method_name, ");
			buildquery.append("u.first_name, u.last_name, u.email, u.phone, ");
			buildquery.append("ee_l.address1, ee_l.city, ee_l.state, ee_l.zip, ");
			buildquery.append("bi.account_number, bi.account_type, bi.routing_number ");
			buildquery.append("FROM payment_methods pm ");
			buildquery.append("INNER JOIN financial_info fi ");
			buildquery.append("ON pm.financial_info_id = fi.id ");
			buildquery.append("INNER JOIN bank_info bi ");
			buildquery.append("ON fi.bank_info_id = bi.id ");
			buildquery.append("INNER JOIN ee_entities ee ");
			buildquery.append("ON pm.module_id = ee.id ");
			buildquery.append("INNER JOIN ee_sites ee_s ");
			buildquery.append("ON ee.id = ee_s.ee_entity_id ");
			buildquery.append("INNER JOIN ee_locations ee_l ");
			buildquery.append("ON ee_l.id = ee_s.mailing_location_id ");
			buildquery.append("INNER JOIN users u" + " ON u.id = ee.user_id ");
			buildquery.append("WHERE pm.module_name = 'ASSISTERENROLLMENTENTITY' ");
			buildquery.append("AND pm.status=  'Active' ");
			buildquery.append("AND ee_s.site_type = 'PRIMARY' ");
			buildquery.append("AND pm.payment_type = 'EFT' ");
			buildquery.append("AND pm.subscription_id IS NULL ");
			query = em.createNativeQuery(buildquery.toString());
			query.setMaxResults(maxResult);
			List results = query.getResultList();
			if(!results.isEmpty()){
					paymentMethodDTOList = generatePaymentMethodDTO(results);
			}
			
		} catch (Exception exception) {
			LOGGER.error("Exception while geting  PaymentMethod Migration Data ", exception);
			throw new GIRuntimeException(exception,
							ExceptionUtils.getFullStackTrace(exception), Severity.HIGH,
							GIRuntimeException.Component.AEE.toString(), null);
		} finally {
			if (em != null && em.isOpen()) {
					em.clear();
					em.close();
			}
		}
		return paymentMethodDTOList;
	}


	private List<PaymentMethodDTO> generatePaymentMethodDTO(List results){
		LOGGER.info("generatePaymentMethodDTO : START");
		List<PaymentMethodDTO> paymentMethodDTOList = new ArrayList<PaymentMethodDTO>();
		Iterator rsIterator = results.iterator();
		VimoEncryptor ve = (VimoEncryptor) GHIXApplicationContext.getBean("vimoencryptor");
		try{
			while (rsIterator.hasNext()) {
				PaymentMethodDTO pmdto = new PaymentMethodDTO();
				Object[] objArray = (Object[]) rsIterator.next();

				pmdto.setPaymentMethodId(Integer.parseInt(objArray[ZERO].toString()));
				pmdto.setPaymentType(PaymentType.valueOf(objArray[ONE].toString()));
				pmdto.setPaymentMethodName(PaymentType.valueOf(objArray[ONE].toString()).toString());
				pmdto.setFirstName(objArray[THREE].toString());
				pmdto.setLastName(objArray[FOUR].toString());
				pmdto.setEmail(objArray[FIVE].toString());
				pmdto.setContactNumber(objArray[SIX].toString());
				pmdto.setAddress1(objArray[SEVEN].toString());
				pmdto.setCity(objArray[EIGHT].toString());
				pmdto.setState(objArray[NINE].toString());
				pmdto.setZipcode(objArray[TEN].toString());
				pmdto.setAccountNumber(ve.decrypt(objArray[ELEVEN].toString()));
				pmdto.setAccountType(objArray[TWELVE].toString());
				pmdto.setRoutingNumber(objArray[THIRTEEN].toString());
				paymentMethodDTOList.add(pmdto);
			}
		}catch(Exception exception){
			LOGGER.error("Exception generatePaymentMethodDTO : ",exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);	
		}
		LOGGER.info("generatePaymentMethodDTO : END");
		return paymentMethodDTOList;
	}
	
}
