import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';
import {changeData} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS, {getOptionsForHardcodedState} from '../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../constants/validationTypes';

// Components

// ActionCreators

// Utils

// Constants


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Enrolled In Other Health Insurance
 *
 */
const Page4_65 = props => {

    const {
        data,
        form: {
            Form: {
                isFormSubmitted
            }
        },
        householdMembers,
        hhmIndex : member,
        currentHhm,

        // pageName,
        //isFirstVisit
    } = props;
    const {
        healthCoverage : {
            stateHealthBenefit
        },
        householdContact: {
            homeAddress: {
                state
            }
        },
        name
    } = currentHhm;

    const [ stateAbbr ] = state ? getOptionsForHardcodedState(state) : [{}];
    const onFieldChange = newData => {
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = {...newData};
        props.actions.changeData(newState);
    };



    const checkTopLevelValidation = () => {

        return true;
    };
    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>
                <ValidationErrorMessage
                    isVisible={isFormSubmitted && !checkTopLevelValidation()}
                    messageText='Please select the employer that will offer health coverage'
                    errorClasses={['margin-l-0']}
                />
                <div id="aiStateHealthBenefit" className="fade-in">
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText:
                                    <span>
                                        Is&nbsp;
                                        <strong>{normalizeNameOfPerson(name)}</strong>&nbsp;
                                        offered the <strong>{stateAbbr.text}</strong>&nbsp;state employee health benefit plan through a job or a family member’s job?
                                    </span>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'stateHealthBenefit',
                                value: stateHealthBenefit,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: householdMembers[member],
                                fields: ['healthCoverage']

                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                            errorClasses={['margin-l-20-ve']}
                        />

                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    household: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0],

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    page: state.page
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page4_65.defaultProps = {
    name: 'Page4_65'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page4_65);
