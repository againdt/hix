<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}"> 
<input type="hidden" id="archiveEncTicketid" name="archiveEncTicketid" value="${archiveEncTicketid}">

<!-- Cancel confirmation dialog  -->
<div id="confirmArchiveDlg" class="modal hide fade" tabindex="-1" role="dialog"  aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div class="archiveTicklHeader">
    	<div class="header">
            <h4 class="pull-left">Archive ticket</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button"><spring:message code="btn.crossClose" /></button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					<h5 class="margin0 pull-left">Are you sure you want to archive this ticket?</br>Archiving the ticket will remove it from your ticket queue </h5>
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
    <button class="btn pull-left" data-dismiss="modal" aria-hidden="true" id="archiveCanelButton">Cancel</button>
    <button class="btn btn-primary" onclick="javascript:archiveTicket()" id="archiveSubmit">Archive Ticket</button>
	
  </div>
  <div id="ajaxloaderArchive" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>

<script>
function archiveTicket()
{
	var ticketId = $("#ticketId").val();
	var encTicketId = $("#archiveEncTicketid").val();
	$("#crossCloseArchive").attr("disabled",true); 	
	$("#archiveCanelButton").attr("disabled",true); 	
	$("#archiveSubmit").attr("disabled",true); 	
	
	var validateUrl ='<c:url value="/ticketmgmt/ticket/archive"></c:url>';
	$("#ajaxloaderArchive").show();
	$(".archiveTicklHeader").find("button").hide();
	
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
			ticketId : encTicketId,
		},
		success : function(response) {
			//if (response !=null && response != "") {
				$("#ajaxloaderArchive").hide();
				$("td", ".row_"+ticketId).css({
					"color":"rgb(184, 183, 183)"
				}).find("a").css({
					"color":"rgb(184, 183, 183)"
				});
				$('#tkmErrorDiv').html("Archived Successfull");
				$('#tkmErrorDiv').show();
				setTimeout(function () {
				    $('#tkmErrorDiv').hide("slow");
				}, 4000);
				$('#confirmArchiveDlg').modal('hide');
				$("#archiveTicket-"+ticketId).addClass("disabled");
				$("td", ".row_"+ticketId).find(".dropdown").find("li:first-child").find("a").attr("style","");

				var rowId = '#ticketTaskStatusId-'+ticketId;
	            var scope=angular.element(rowId).scope();
	            scope.ticket.allowArchive=false;
	            scope.archiveTicket(scope.ticket);
	            try{scope.$apply();}catch(e){};
				
				
			//} else {
				//$('#confirmArchiveDlg').modal('hide');
				//$("#ajaxloader").hide();
			//}
		},
		error : function(error) {
			$("#ajaxloaderArchive").hide();
			$('#confirmArchiveDlg').modal('hide');
			$('#tkmErrorDiv').html("Error while archiving ticket"); 

		}
	});	
}
</script>