package com.getinsured.hix.util;

import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum;
import com.getinsured.hix.platform.config.SecurityConfiguration.SecurityConfigurationEnum;

/**
 * Ghix configuration constants that are initalized via database and
 * {@link DynamicPropertiesUtil} file.
 * 
 * @author Deval
 * @author Yevgen Golubenko
 * 
 * @since 04/25/2014
 *        Refactored to get rid of combinedConfig spring expressions and using
 *        {@link DynamicPropertiesUtil} to
 *        get access to the values.
 * 
 * @deprecated Use {@link DynamicPropertiesUtil} class to read properties
 *             directly. This class will be removed by platform
 *             team in the future.
 */
@Deprecated
@Component
public final class GhixConfiguration
{
	//------------------START :: CONSTANTS RELATED TO SECURITY -------------------------
	public static Boolean IS_USER_REG_EXTERNAL           = false;
	public static Boolean IS_EMAIL_ACTIVATION            = false;
	//public static Boolean IS_SECURITY_QUESTIONS_REQUIRED = false;
	//------------------END :: CONSTANTS RELATED TO SECURITY START-------------------------

	public static String STATE_NAME;
	public static String STATE_CODE;
	public static String EXCHANGE_NAME;
	public static String EXCHANGE_PHONE;
	public static String EXCHANGE_URL;
	public static String CITY_NAME;
	public static String PIN_CODE;
	public static String EXCHANGE_ADDRESS_1;
	public static String EXCHANGE_ADDRESS_2;
	public static String COUNTRY_NAME;
	public static String EXCHANGE_ADDRESS_LINE_ONE;
	public static String EXCHANGE_FULL_NAME;
	public static String EXCHANGE_TYPE;

	public static Boolean IS_CA_CALL                     = false;
	public static Boolean IS_NM_CALL                     = false;

	static
	{
		STATE_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_NAME);
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.STATE_CODE);
		EXCHANGE_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_NAME);
		EXCHANGE_PHONE = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_PHONE);
		EXCHANGE_URL = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_URL);
		CITY_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.CITY_NAME);
		PIN_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.PIN_CODE);
		EXCHANGE_ADDRESS_1 = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_1);
		EXCHANGE_ADDRESS_2 = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_2);
		EXCHANGE_TYPE = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_TYPE);
		COUNTRY_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.COUNTRY_NAME);
		EXCHANGE_ADDRESS_LINE_ONE = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_ADDRESS_1);
		EXCHANGE_FULL_NAME = DynamicPropertiesUtil.getPropertyValue(GlobalConfigurationEnum.EXCHANGE_FULL_NAME);
		
		/*
		String bool = DynamicPropertiesUtil.getPropertyValue(SecurityConfigurationEnum.IS_SECURITY_QUESTIONS_REQUIRED);

		if (bool != null && "TRUE".equalsIgnoreCase(bool))
		{
			IS_SECURITY_QUESTIONS_REQUIRED = Boolean.TRUE;
		}

		*/
		String bool = DynamicPropertiesUtil.getPropertyValue(SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);

		if (bool != null && "TRUE".equalsIgnoreCase(bool))
		{
			IS_EMAIL_ACTIVATION = true;
		}

		bool = DynamicPropertiesUtil.getPropertyValue(SecurityConfigurationEnum.IS_EXTERNAL);

		if (bool != null && "TRUE".equalsIgnoreCase(bool))
		{
			IS_USER_REG_EXTERNAL = true;
		}

		if ("NM".equalsIgnoreCase(STATE_CODE) || ("MS").equalsIgnoreCase(STATE_CODE))
		{
			IS_NM_CALL = Boolean.TRUE;
		}
		else
		{
			IS_NM_CALL = Boolean.FALSE;
		}
	}

	//@Value("#{combinedConfig.getBoolean('Broker.IsCACall')}")
	public void setIsCACall(final Boolean iS_CA_CALL)
	{
		IS_CA_CALL = iS_CA_CALL;
		/*
		 * TODO: ygolubenko check this in: findtext "IS_CA_CAL" -I | grep
		 * --color -v "/target/"| grep --color "ghix-web"
		 */
	}
}
