package com.getinsured.eligibility.qlevalidation;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.at.ref.service.ReferralLCECompareService;
import com.getinsured.hix.platform.logging.GhixLogFactory;
import com.getinsured.hix.platform.logging.GhixLogger;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;

@Service("qleAutomationHandler")
public class QLEAutomationHandler {
	
	private static final GhixLogger LOGGER = GhixLogFactory.getLogger(QLEAutomationHandler.class);
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	@Qualifier("qleAutomationChangesHandlerService")
	private LceProcessHandlerService qleAutomationChangesHandlerService;
	
	@Autowired
	@Qualifier("referralLCECompareService")
	private ReferralLCECompareService referralLCECompareService;
	
	@Async
	public void doAutomation(Long currentAppId, int lastUpdatedUserId) {
		
		try{
			SsapApplication currentApplication = lockDashboard(currentAppId);
			LCEProcessRequestDTO lceProcessRequestDTO = populateAutomationDTO(currentApplication);
			if(lceProcessRequestDTO != null){
			// Added user id so that it will be used for logging into application_event 
			lceProcessRequestDTO.setLastUpdatedUserId(lastUpdatedUserId);
			qleAutomationChangesHandlerService.execute(lceProcessRequestDTO);
			}
		} catch (Exception e){
			LOGGER.error("Exception in automation flow - " + ExceptionUtils.getStackTrace(e));
		} finally {
			unlockDashboard(currentAppId);
		}
	}

	private LCEProcessRequestDTO populateAutomationDTO(SsapApplication currentApplication) throws Exception {
		
		final SsapApplication enrolledApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(currentApplication.getCmrHouseoldId(), currentApplication.getCoverageYear());
		if(enrolledApplication != null){
			final CompareMainDTO compareMainDTO = referralLCECompareService.executeCompareReadOnly(currentApplication.getId(), enrolledApplication.getId());
			return new LCEProcessRequestDTO(currentApplication.getId(), enrolledApplication.getId(), null, compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes());
		}
		else{
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("Did not find any previous EN/PN application. Current application's dental enrollment status is:{} ",currentApplication.getApplicationDentalStatus());
			}
		}
		return null;
	}

	private SsapApplication lockDashboard(Long currentAppId) {
		SsapApplication currentApplication = ssapApplicationRepository.findOne(currentAppId);
		currentApplication.setAllowEnrollment(ReferralConstants.N);
		ssapApplicationRepository.save(currentApplication);
		return currentApplication;
	}
	
	private SsapApplication unlockDashboard(Long currentAppId) {
		SsapApplication currentApplication = ssapApplicationRepository.findOne(currentAppId);
		if (!StringUtils.equals(currentApplication.getAllowEnrollment(), ReferralConstants.Y)){
			currentApplication.setAllowEnrollment(ReferralConstants.Y);
			ssapApplicationRepository.save(currentApplication);
		}
		return currentApplication;
	}

}
