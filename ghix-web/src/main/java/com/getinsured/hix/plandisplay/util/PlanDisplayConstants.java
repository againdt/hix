/**
 * 
 */
package com.getinsured.hix.plandisplay.util;

import java.util.HashMap;
import java.util.Map;


/**
 * @author Mishra_m
 *
 */
public class PlanDisplayConstants {
	
	public static final String ZIP = "zip";
	
	public static final String AGE = "age";
	
	public static final String STATE = "California";
	
	public static final String RELATION = "relation";
	
	public static final String SELF = "self";
	
	public static final String SPOUSE = "spouse";
	
	public static final String CHILD = "child";
	
	public static final String DOB = "DOB";
	
	public static final String COVERED = "Covered";
	
	public static final String NOT_COVERED = "Not Covered";
	
	public static final String SADP_FLAG = "SADP_FLAG";
	
	public static final String CURRENT_GROUP_ID = "currentGroupId";
	
	public static final String DENTAL_PLANS_ONOFF = "dentalPlansOnOff";
	
	public static final String PERSON_LIST = "personList";
	
	public static final String PLAN_PREMIUM = "planPremium";
	
	public static final int MIN_PLAN_SIZE = 3;
	
	public static final int CHILD_AGE = 19;
	
	public static final int MONTH = 12;
	
	public static final String CONTENT_TYPE = "application/json";
	public static final String MODULE_NAME= "PD";
	public static final String HYPHEN = " - ";
	
	public static Map<String,String> sortConfigs ;
	   
	static
	{
		sortConfigs = new HashMap<String, String>();
		sortConfigs.put("MONTHLY PRICE", "premiumAfterCredit");
		sortConfigs.put("EXPENSE ESTIMATE", "estimatedTotalHealthCareCost");
		sortConfigs.put("DEDUCTIBLE", "deductible");
	}
	
   public static final String	 REQUEST_IS_NULL= "Invalid input: Request is Null.";
   public static final String 	 REQUEST_ID_IS_NULL= "Invalid input: Id is Null or Empty.";
   public static final String 	 FAILED_TO_GET_PREFERENCES="Error occurred while processing getPreferences().";
   public static final String	 FAILED_TO_EXECUTE_CHECK_ITEM_IN_CART="Error occurred while executing checkItemInCart() ";
   public static final String	 FAILED_TO_EXECUTE_FIND_GROUP_BY_HOUSEHOLD_ID="Error occurred while executing findGroupsByHousehold()";
   public static final String	 FAILED_TO_EXECUTE_FIND_PLD_HOUSEHOLDBY_ID="Error occurred while executing findPldHouseholdById()";
   public static final String	 FAILED_TO_EXECUTE_FIND_PLDHOUSEHOLD_BY_SHOPPING_ID="Error occurred while executing findPldHouseholdByShoppingId()";
   public static final String	 FAILED_TO_FIND_PLDHOUSEHOLD_PERSON="Error occured while executing findPldHouseholdPerson() ";
   public static final String	 FAILED_TO_EXECUTE_POST_SPLITHOUSEHOLD="Error occurred while executing postSplitHousehold() ";
   public static final String	 FAILED_TO_EXECUTE_GET_SUBSCRIBER_DATA="Error occurred while executing getSubscriberData() ";
   public static final String    FAILED_TO_EXECUTE_SAVE_PREFERENCES="Error occurred while executing savePreferences() ";
   public static final String    FAILED_TO_EXECUTE_FIND_PLDGROUP_BY_ID = "Error occurred while executing findPldGroupById()";
   public static final String    FAILED_TO_EXECUTE_GET_PLANS ="Error occurred while executing getplans()";
   public static final String    FAILED_TO_EXECUTE_SAVE_CART_ITEMS = "Error occurred while executing saveCartItems()";
   public static final String    COVERAGE_START_DATE_NOT_FOUND = "Coverage start date not found";
   public static final String    FAILED_TO_EXECUTE_DELETE_ORDER_ITEM="Error occurred while executing deleteOrderItem()";
   public static final String    FAILED_TO_EXECUTE_GET_CART_ITEMS = "Error occurred while executing getCartItems()";
   public static final String    FAILED_TO_EXECUTE_KEEP_PLAN = "Error occurred while executing keepPlan()";
   public static final String    FAILED_TO_EXECUTE_SHOWING_CART="Error occurred while executing showingCart()";
   public static final String    FAILED_TO_EXECUTE_GET_PALN_INFO="Error occurred while executing getplanInfo()";
   public static final String    FAILED_TO_EXECUTE_GET_DISCLAIMER_INFO="Error occurred while executing getDisclaimerInfo()";
   public static final String    FAILED_TO_EXECUTE_GET_PLAN_BENEFIT_COST_DATA="Error occurred while executing getPlanBenefitCostData()";
   public static final String    FAILED_TO_EXECUTE_FIND_PLD_ORDER_ITEM_BY_ID="Error occurred while executing findPldOrderItemById()";
   public static final String    FAILED_TO_EXECUTE_SPECIAL_ENROLLMENT_PERSONDATA_UPDATE="Error occurred while executing specialEnrollmentPersonDataUpdate() ";
   public static final String    FAILED_TO_EXECUTE_FIND_PLDORDER_ITEM_BY_PLD_GROUP_ID="Error occurred while executing findPldOrderItemByPldGroupId()";
   public static final String	 FAILED_TO_EXECUTE_GET_ORDER_ID_BY_HOUSEDHOLD_ID_AND_STATUS="Error occurred while executing findOrderIdByHouseholdIdandStatus()";
   public static final String    FAILED_TO_EXECUTE_FORM_INDIVIDUAL_REQUEST_FOR_PHIX="Error occurred while executing formIndividualRequestForPHIX()";
   public static final String    FAILED_TO_EXECUTE_SAVE_INDIVIDUAL_PHIX = "Error occurred while executing saveIndividualPHIX()";
   public static final String    FAILED_TO_EXECUTE_FIND_HOUSEHOLD_ID_BY_LEAD_ID="Error occurred while executing findHouseholdIdByLeadId()";
   public static final String    FAILED_TO_EXECUTE_SAVE_APTC="Error occurred while executing saveAptc() ";
   public static final String    FAILED_TO_EXECUTE_SAVE_ORDER_ITEM="Error occurred while executing saveOrderItem() ";
   public static final String    FAILED_TO_EXECUTE_UPDATE_ELIG_LEAD_RECORD="Error occurred while executing updateEligLeadRecord() ";
   
   	public static final String FAILED_TO_EXECUTE_FIND_HOUSEHOLD_BY_SHOPPING_ID = "Error occurred while executing findHouseholdByShoppingId";
	public static final String FAILED_TO_EXECUTE_FIND_PESON_BY_HOUSEHOLD_ID = "Error occurred while executing findPersonByHouseholdId";
	public static final String FAILED_TO_EXECUTE_SAVE_CART_ITEM = "Error occurred while executing saveCartItem()";
	public static final String FAILED_TO_EXECUTE_DELETE_CART_ITEM="Error occurred while executing deleteCartItem()";
	public static final String FAILED_TO_EXECUTE_UPDATE_SEEK_COVERAGE_DENTAL="Error occurred while executing updateSeekCoverageDental() ";
	public static final String FAILED_TO_EXECUTE_FIND_CART_ITEM_BY_ID = "Error occurred while executing findCartItemById";
	public static final String FAILED_TO_EXECUTE_FIND_PERSHOPPING_CART_ITEMS = "Error occurred while executing findPreShoppingCartItems";
	public static final String FAILED_TO_EXECUTE_RE_CALCULATE_SUBSIDY = "Error occurred while executing reCalculateSubsidy";

	public static final String FAILED_TO_EXECUTE = "Error occurred while executing ";
}
