package com.getinsured.eligibility.ssap.integration.util;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Map;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.messaging.Message;
import org.springframework.web.client.HttpStatusCodeException;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;
import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.iex.ssap.repository.SsapIntegrationLogRepository;

public class IntegrationDBLoggingInterceptor implements MethodInterceptor {

	private static final Logger LOGGER = Logger.getLogger(IntegrationDBLoggingInterceptor.class);

	@Autowired
	private SsapIntegrationLogRepository ssapIntegrationLogRepository;

	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		Object result = null;
		Calendar endTime = null;
		Boolean succeeded = true;
		String exceptionTraceAsString = null;
		String httpServerErrorResponseBody = null;
		Calendar startTime = TSCalendar.getInstance();
		try {
			LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> Before method invocation");
			result = methodInvocation.proceed();
		} catch(Throwable t) {
			LOGGER.error("IntegrationDBLoggingInterceptor logged an error.", t);
			exceptionTraceAsString = ExceptionUtils.getFullStackTrace(t);
			if(t.getCause() instanceof org.springframework.web.client.HttpStatusCodeException) {
			    HttpStatusCodeException httpException = (HttpStatusCodeException)t.getCause();
			    httpServerErrorResponseBody = httpException.getResponseBodyAsString();
			}
			succeeded = false;
			throw t;
		} finally {
			endTime = TSCalendar.getInstance();
		     Long responseTime = endTime.getTimeInMillis() - startTime.getTimeInMillis();
		     LOGGER.debug(">>>>>>>>>>>>>>>>>>>>>>>>>> After method invocation " + responseTime + "ms");
		     saveIntegrationLog(methodInvocation, result, responseTime, succeeded, exceptionTraceAsString, httpServerErrorResponseBody);
		}
		return result;
    }

	@SuppressWarnings("rawtypes")
	private void saveIntegrationLog(MethodInvocation methodInvocation, Object result, Long responseTime, Boolean status, String exceptionTraceAsString, String httpServerErrorResponseBody) {
		try {
			SsapIntegrationLog ssapIntegrationLog = new SsapIntegrationLog();
			Message requestMessage = ((org.springframework.messaging.Message)(methodInvocation.getArguments())[0]);
			Message responseMessage = (Message) result;

			Map headers = requestMessage.getHeaders();
			String messageHistory = headers.get(SSAPIntegrationConstants.MESSAGE_HISTORY).toString();

			Object header = headers.get(SSAPIntegrationConstants.SSAP_APPLICATION_ID_HEADER);
			if(null != header) {
				ssapIntegrationLog.setSsapApplicationId(Long.valueOf(header.toString()));
			}

			header = headers.get(SSAPIntegrationConstants.SSAP_APPLICATION_EVENT_ID_HEADER);
			if(null != header) {
				ssapIntegrationLog.setSsapApplicationEventId(Long.valueOf(header.toString()));
			}

			header = headers.get(SSAPIntegrationConstants.CORRELATION_ID_HEADER);
			if(null != header) {
				ssapIntegrationLog.setCorrelationId(header.toString());
			}

            ssapIntegrationLog
                    .setPayload(new StringBuilder()
                            .append("{")
                            .append("\"request\": ")
                            .append((null != requestMessage && null != requestMessage.getPayload()) ? requestMessage
                                    .getPayload().toString() : null)
                            .append(", \"headers\": ")
                            .append(convertMessageHeadersToJson(headers))
                            .append(", \"response\": ")
                            .append(((null != responseMessage && null != responseMessage
                                    .getPayload()) ? responseMessage.getPayload().toString() : null))
                            .append(", \"exception_stack_trace\": ")
                            .append(exceptionTraceAsString)
                            .append(", \"http_response_body\": ").append(httpServerErrorResponseBody)
                            .append("}")
                            .toString());

			ssapIntegrationLog.setServiceName(messageHistory.substring(messageHistory.lastIndexOf(',') + 1));
			ssapIntegrationLog.setResponseTimeMillis(responseTime);

			if(status) {
				ssapIntegrationLog.setStatus(SSAPIntegrationConstants.STATUS_SUCCESS);
			} else {
				ssapIntegrationLog.setStatus(SSAPIntegrationConstants.STATUS_FAILURE);
			}


			if(null != responseMessage && null != responseMessage.getPayload()) {
				String responsePayload = responseMessage.getPayload().toString();
	            if("failed".equalsIgnoreCase(responsePayload) || !isHttpRequestSuccessful(responseMessage)) {

	                ssapIntegrationLog.setStatus(SSAPIntegrationConstants.STATUS_FAILURE);

	                if(null != responsePayload && responsePayload.contains("SERVICE_UNDER_MAINT")) {
	                    ssapIntegrationLog.setIsScheduledDowntime("Y");
	                }
	            }
			}
			ssapIntegrationLogRepository.save(ssapIntegrationLog);
		} catch (Throwable t) {
			LOGGER.error("IntegrationDBLoggingInterceptor logged an error recording the log", t);
		}

	}

	@SuppressWarnings("rawtypes")
    private boolean isHttpRequestSuccessful(Message responseMessage) {
	    Boolean responseStatus = true;
	    if(null != responseMessage.getHeaders()) {
	        if(null != responseMessage.getHeaders().get("http_statusCode")) {
	            HttpStatus httpStatus = (HttpStatus) responseMessage.getHeaders().get("http_statusCode");
	            if(httpStatus.is5xxServerError() || httpStatus.is4xxClientError()) {
	                responseStatus = false;
	            }
	        }
	    }
	    return responseStatus;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
	private String convertMessageHeadersToJson(Map headers) {
		String headerJSONString = null;
		if(null != headers) {
			JSONObject headerJSON = new JSONObject();
			headerJSON.putAll(headers);
			headerJSONString =  headerJSON.toJSONString();
		}
		return headerJSONString;
	}
}
