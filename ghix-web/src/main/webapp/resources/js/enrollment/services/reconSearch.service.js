(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .factory('searchService', searchService);

    searchService.$inject = ['$http', '$q'];

    function searchService($http, $q) {
        var searchFilters = {};
        var searchResultsData = {};

        return {
            getSearchFilters: getSearchFilters,
            getSearchResults : getSearchResults,
            getSearchResultsData : getSearchResultsData
        };

        function getSearchFilters (issuerDetails){

            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/getpopulatedsearchdiscrepancydata';

            var promise = $http.get(url);

            promise.success(function(response){
                //console.log("success");
                if(response.errCode === 200){
                    searchFilters = response;
                    deferred.resolve(response);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;

        }

        function getSearchResults(seearchQueryDetails){

            var deferred = $q.defer();

            var url = '/hix/enrollmentreconciliation/searchenrollmentdiscrepancy';

            var promise = $http.post(url, seearchQueryDetails);

            promise.success(function(response){
                //console.log("success");
                if(response.errCode === 200){
                    searchResultsData = response;
                    deferred.resolve(response);

                }else{
                    // failed to fetch issue activity
                }

            });

            promise.error(function(response){
                deferred.reject(response);
            });

            return deferred.promise;

        }

        function getSearchResultsData() {
            return searchResultsData;
        }
    }
})();
