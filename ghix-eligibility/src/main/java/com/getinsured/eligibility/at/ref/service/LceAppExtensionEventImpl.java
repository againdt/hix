package com.getinsured.eligibility.at.ref.service;

import java.util.Date;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 * 
 */
@Component("lceAppExtensionEvent")
@Scope("singleton")
@DependsOn("dynamicPropertiesUtil")
public class LceAppExtensionEventImpl implements LceAppExtensionEvent {
	private static final Logger LOGGER = LoggerFactory.getLogger(LceAppExtensionEventImpl.class);

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private static Map<String, SepEvents> sepEventsOther;
	
	private static final String SEP_AUTO_APTC_UPDATE = "AUTO_APTC_UPDATE";
	private static final String SEP_AUTO_APTC_INCREASE = "INCOME_CHANGE_APTC_INCREASE_AUTO";
	private static final String SEP_AUTO_APTC_DECREASE = "INCOME_CHANGE_APTC_DECREASE_AUTO";
	
	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;

	@Autowired
	private SsapApplicationEventService ssapApplicationEventService;

	@PostConstruct
	public void doPost() {
		sepEventsOther = sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
	}

	@Override
	public void createAptcOnlyEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension,Boolean isAPTCIncreased) {
		LOGGER.info("Create APTC only Event for Application id - " + currentApplication.getId());
		SepEvents event = findMatchingSepEvent(isAPTCIncreased);
		Date applicantEventDate = ApplicationExtensionEventUtil.defaultApplicantEventDate();
		Date applicantReportDate = ApplicationExtensionEventUtil.defaultApplicantReportDate();
		ssapApplicationEventService.createAptcOnlyApplicationEvent(currentApplication, event, applicantEventDate, applicantReportDate);
	}

	private SepEvents findMatchingSepEvent(Boolean isAPTCIncreased) {
		SepEvents event = null;
		if(Boolean.TRUE.equals(isAPTCIncreased)){
			event = sepEventsOther.get(SEP_AUTO_APTC_INCREASE);
		}
		else if(Boolean.FALSE.equals(isAPTCIncreased)){
			event = sepEventsOther.get(SEP_AUTO_APTC_DECREASE);
		}
		if(event == null){
			event = sepEventsOther.get(SEP_AUTO_APTC_UPDATE);
		}
		return event;
	}


	@Override
	public Map<String, ApplicantEvent> createCitizenshipEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension) {
		LOGGER.info("Create Citizenship Event for Application id - " + currentApplication.getId());
		final Map<String, ApplicantEvent> applicantExtensionEvent = ApplicationExtensionEventUtil.populateApplicantEventFromExtension(currentApplication.getSsapApplicants(), applicationExtension);
		ssapApplicationEventService.createCitizenshipApplicationEvent(currentApplication, applicantExtensionEvent);
		return applicantExtensionEvent;

	}

}
