package com.getinsured.eligibility.indportal.indCreation;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.indportal.dto.HouseholdData;
import com.getinsured.eligibility.indportal.dto.HouseholdMemberDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO;
import com.getinsured.eligibility.indportal.dto.MemberDTO.Relationships;
import com.getinsured.eligibility.indportal.dto.SsapApplicantsDataDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Race;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.ObjectFactory;
import com.getinsured.hix.webservice.enrollment.adminupdateindividual.YesNoVal;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.SsapApplicantResource;
import com.getinsured.iex.dto.ind19.Ind19ClientRequest;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.thoughtworks.xstream.XStream;



@Service("IND57Household")
public class IND57Household implements IHousehold{
	private	final ObjectFactory objectFactory = new ObjectFactory();
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private ResourceCreator resourceCreator;
	private static final Logger LOGGER = Logger.getLogger(IND57Household.class);
	
	@Override
	public Object createResponsiblePerson(HouseholdData householdData) {
		HouseholdMemberDTO householdMemberDTO = getHouseholdMemberDTO(householdData.getApplicationData(),SsapApplicantPersonType.PTF.getPersonType());
		ResponsiblePerson responsiblePerson = objectFactory.createAdminUpdateIndividualRequestHouseholdResponsiblePerson();
		responsiblePerson.setResponsiblePersonId(householdMemberDTO.getId());		
		responsiblePerson.setResponsiblePersonFirstName(householdMemberDTO.getFirstName());
		responsiblePerson.setResponsiblePersonLastName(householdMemberDTO.getLastName());
		responsiblePerson.setResponsiblePersonMiddleName(householdMemberDTO.getMiddleName());
		responsiblePerson.setResponsiblePersonSuffix(householdMemberDTO.getSuffix());
		responsiblePerson.setResponsiblePersonSsn(householdMemberDTO.getSsn());		
		responsiblePerson.setResponsiblePersonPrimaryPhone(householdMemberDTO.getPrimaryPhone());		
		responsiblePerson.setResponsiblePersonSecondaryPhone(householdMemberDTO.getSecondaryPhone());		
		responsiblePerson.setResponsiblePersonPreferredEmail(householdMemberDTO.getPreferredEmail());
		responsiblePerson.setResponsiblePersonPreferredPhone(householdMemberDTO.getPreferredPhone());
		responsiblePerson.setResponsiblePersonHomeAddress1(householdMemberDTO.getHomeAddress1());
		responsiblePerson.setResponsiblePersonHomeAddress2(householdMemberDTO.getHomeAddress2());
		responsiblePerson.setResponsiblePersonHomeCity(householdMemberDTO.getHomeCity());
		responsiblePerson.setResponsiblePersonHomeState(householdMemberDTO.getHomeState());
		responsiblePerson.setResponsiblePersonHomeZip(householdMemberDTO.getHomeZip());
		responsiblePerson.setResponsiblePersonType(householdMemberDTO.getType());
		return responsiblePerson;
	}

	@Override
	public Object createHouseHoldContact(HouseholdData householdData) {
		HouseholdMemberDTO householdMemberDTO = getHouseholdMemberDTO(householdData.getApplicationData(),SsapApplicantPersonType.PC.getPersonType());
		HouseHoldContact houseHoldContact = objectFactory.createAdminUpdateIndividualRequestHouseholdHouseHoldContact();
		houseHoldContact.setHouseHoldContactId(householdMemberDTO.getId());
		houseHoldContact.setHouseHoldContactFirstName(householdMemberDTO.getFirstName());
		houseHoldContact.setHouseHoldContactMiddleName(householdMemberDTO.getMiddleName());
		houseHoldContact.setHouseHoldContactLastName(householdMemberDTO.getLastName());
		houseHoldContact.setHouseHoldContactSuffix(householdMemberDTO.getSuffix());
		houseHoldContact.setHouseHoldContactFederalTaxIdNumber(householdMemberDTO.getSsn());
		houseHoldContact.setHouseHoldContactPrimaryPhone(householdMemberDTO.getPrimaryPhone());
		houseHoldContact.setHouseHoldContactSecondaryPhone(householdMemberDTO.getSecondaryPhone());
		houseHoldContact.setHouseHoldContactPreferredEmail(householdMemberDTO.getPreferredEmail());
		houseHoldContact.setHouseHoldContactPreferredPhone(householdMemberDTO.getPreferredPhone());
		houseHoldContact.setHouseHoldContactHomeAddress1(householdMemberDTO.getHomeAddress1());
		houseHoldContact.setHouseHoldContactHomeAddress2(householdMemberDTO.getHomeAddress2());
		houseHoldContact.setHouseHoldContactHomeCity(householdMemberDTO.getHomeCity());
		houseHoldContact.setHouseHoldContactHomeState(householdMemberDTO.getHomeState());
		houseHoldContact.setHouseHoldContactHomeZip(householdMemberDTO.getHomeZip());
		return houseHoldContact;
	}

	@Override
	public Object createIndividual(HouseholdData householdData, SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object createMembers(Object membersObject, SsapApplication ssapApplication, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData, String userName, List<String> memberGUIDs)
			throws GIException {
		
		Members members = (Members) membersObject;
		Long ssapApplicationId = ssapApplication.getId();
		
		Map<Long, SsapApplicantResource> ssapApplicants = null;
		try {
			ssapApplicants = resourceCreator.getApplicantsByApplicationId(ssapApplicationId);
		} catch (Exception e) {
			throw new GIException("Exception while creating ResourceCreator.");
		}
		
		List<SsapApplicantsDataDTO> ssapApplicantsDataList = populateSsapApplicantsDataDTO(ssapApplicants);
		List<MemberDTO> memberDTOList = populateMembersDTO(householdData.getApplicationData(), ssapApplicants);
		
		List<MemberDTO.Relationships> relationshipsList = new ArrayList<>();
		for(MemberDTO memberDTO : memberDTOList){
			if(memberDTO.getRelationships() != null && !memberDTO.getRelationships().isEmpty()){
				relationshipsList = memberDTO.getRelationships();
			}
		}		
		memberDTOList = filterOptionalMembers(memberDTOList, memberGUIDs);
		populateMailingAddress(householdData.getApplicationData(), memberDTOList);
		populateMemberMaintenanceReasonCode(memberDTOList);
		
		for(MemberDTO memberDTO : memberDTOList){
			Member member = (Member) populateMember(memberDTO);
			populateRelationship(member,  relationshipsList, ssapApplicantsDataList);								
			members.getMember().add(member);
		}
		
		Set<String> enrolledMembersIds = new HashSet<String>();
		assignEnrollmentDetails(ssapApplicationId, enrolledMembersIds, userName);	
		
		assignGuidsToMembers(members, ssapApplicantsDataList);
		Members eligibleMembers = objectFactory.createAdminUpdateIndividualRequestHouseholdMembers();
		for(Member member:members.getMember()){
			if(enrolledMembersIds.contains(member.getMemberId())){
				eligibleMembers.getMember().add(member);
			}
			
			member.getRelationships().clear();
		}
		return eligibleMembers;
	}

	private List<MemberDTO> filterOptionalMembers(List<MemberDTO> memberDTOList, List<String> memberGUIDs) {
		if(CollectionUtils.isEmpty(memberGUIDs))
		{
			return memberDTOList;
		}
		List<MemberDTO> filteredMembers = new ArrayList<MemberDTO>();
		for(MemberDTO member : memberDTOList)
		{
			if(memberGUIDs.contains(member.getApplicantGuid()))
			{
				filteredMembers.add(member);
			}
		}
		return filteredMembers;
	}

	@Override
	public Object createDisenrollMembers(Ind19ClientRequest ind19ClientRequest, HouseholdData householdData){
		// TODO Auto-generated method stub
		return null;
	}
	
	private void assignGuidsToMembers(Members members,	List<SsapApplicantsDataDTO> ssapApplicantsDataList) {
		List<Member>  memberList = members.getMember();
		if(memberList != null && !memberList.isEmpty()){
			for(Member member : memberList){
				if(member!=null && member.getMemberId() != null){
					if(ssapApplicantsDataList != null && !ssapApplicantsDataList.isEmpty()){
						for(SsapApplicantsDataDTO ssapApplicantsDataDTO : ssapApplicantsDataList){
							if(ssapApplicantsDataDTO.getPersonId() !=null && member.getMemberId().equalsIgnoreCase(String.valueOf(ssapApplicantsDataDTO.getPersonId()))){
								member.setMemberId(ssapApplicantsDataDTO.getGuid());
							}
						}	
					}									
				}	
			}
		}		
	}

	

	private void populateMailingAddress(SingleStreamlinedApplication applicationData, List<MemberDTO> memberDTOList) {
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		for (HouseholdMember householdMember : members) {
			Integer personId = householdMember.getPersonId();
			for(MemberDTO memberDTO : memberDTOList){
				if(personId == 1 && memberDTO != null && StringUtils.isNotBlank(memberDTO.getMemberId()) && memberDTO.getMemberId().equalsIgnoreCase(String.valueOf(personId))){
					if(householdMember.getHouseholdContact() != null){
						if(householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator() != null){
							if(householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()){
								if( householdMember.getHouseholdContact().getHomeAddress() != null) { 
									memberDTO.setMailingAddress1(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
									memberDTO.setMailingAddress2(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
									memberDTO.setMailingCity(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getCity()));
									memberDTO.setMailingState(nullCheckedValue(householdMember.getHouseholdContact().getHomeAddress().getState()));
									memberDTO.setMailingZip(getFiveDigitZipCode(householdMember.getHouseholdContact().getHomeAddress().getPostalCode()));
								}
							}else if(!householdMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()){
								if(householdMember.getHouseholdContact().getMailingAddress() != null){
									memberDTO.setMailingAddress1(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getStreetAddress1()));
									memberDTO.setMailingAddress2(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getStreetAddress2()));
									memberDTO.setMailingCity(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getCity()));
									memberDTO.setMailingState(nullCheckedValue(householdMember.getHouseholdContact().getMailingAddress().getState()));
									memberDTO.setMailingZip(getFiveDigitZipCode(householdMember.getHouseholdContact().getMailingAddress().getPostalCode()));
								}
							}							
						}	
					}
				}
			}
		}		
	}
	
	private void populateMemberMaintenanceReasonCode(List<MemberDTO> memberDTOList) {		
		for(MemberDTO memberDTO : memberDTOList){
			memberDTO.setMaintenanceReasonCode("25");
		}
	}

	
	private Object populateMember(HouseholdMemberDTO householdMemberDTO) {		
		Member member = new AdminUpdateIndividualRequest.Household.Members.Member();
		MemberDTO memberDTO = (MemberDTO) householdMemberDTO;
		member.setMemberId(memberDTO.getMemberId());
		member.setPreferredEmail(memberDTO.getPreferredEmail());
		member.setPreferredPhone(memberDTO.getPreferredPhone());
		member.setSpokenLanguageCode(memberDTO.getSpokenLanguageCode());
		member.setWrittenLanguageCode(memberDTO.getWrittenLanguageCode());
		member.setMailingAddress1(memberDTO.getMailingAddress1());
		member.setMailingAddress2(memberDTO.getMailingAddress2());		
		member.setMailingCity(memberDTO.getMailingCity());
		member.setMailingState(memberDTO.getMailingState());
		member.setMailingZip(memberDTO.getMailingZip());
		member.setMaintenanceReasonCode(memberDTO.getMaintenanceReasonCode());
		member.setFirstName(memberDTO.getFirstName());
		member.setMiddleName(memberDTO.getMiddleName());		
		member.setLastName(memberDTO.getLastName());
		member.setSuffix(memberDTO.getSuffix());		
		member.setDob(memberDTO.getDob());		
		member.setTobacco(memberDTO.getTobacco()!=null?YesNoVal.fromValue(memberDTO.getTobacco()):null);		
		member.setSsn(memberDTO.getSsn());		
		member.setPrimaryPhone(memberDTO.getPrimaryPhone());		
		member.setSecondaryPhone(memberDTO.getSecondaryPhone());		
		member.setHomeAddress1(memberDTO.getHomeAddress1());				
		member.setHomeAddress2(memberDTO.getHomeAddress2());			
		member.setHomeCity(memberDTO.getHomeCity());
		member.setHomeState(memberDTO.getHomeState());
		member.setHomeZip(memberDTO.getHomeZip());
		member.setHomeCountyCode(memberDTO.getCountyCode());           
		member.setGenderCode(memberDTO.getGenderCode());			
		member.setMaritalStatusCode(memberDTO.getMaritalStatusCode());
		member.setExternalMemberId(memberDTO.getExternalMemberId());
		if(memberDTO.getRace() != null && !memberDTO.getRace().isEmpty()){
			for(MemberDTO.Race raceOrEthnicity : memberDTO.getRace()){
				Race race = new Race();
				race.setRaceEthnicityCode(raceOrEthnicity.getRaceEthnicityCode());
				race.setDescription(raceOrEthnicity.getRaceEthnicityDescription());
				member.getRace().add(race);
			}
		}
		
		return member;
	}

	private void populateRelationship(Object memberObj, List<Relationships> relationshipsList, List<SsapApplicantsDataDTO> ssapApplicantsDataList) {
		Member member = (Member) memberObj;
		if(relationshipsList != null && !relationshipsList.isEmpty()){
			for(Relationships relationships : relationshipsList){
				if(member.getMemberId().equalsIgnoreCase(relationships.getIndividualId())){
					com.getinsured.hix.webservice.enrollment.adminupdateindividual.AdminUpdateIndividualRequest.Household.Members.Member.Relationships relationship = objectFactory.createAdminUpdateIndividualRequestHouseholdMembersMemberRelationships();
					if(ssapApplicantsDataList!=null && !ssapApplicantsDataList.isEmpty()){
						for(SsapApplicantsDataDTO ssapApplicantsDataDTO : ssapApplicantsDataList){
							if(ssapApplicantsDataDTO.getPersonId() != null && relationships.getMemberId().equalsIgnoreCase(String.valueOf(ssapApplicantsDataDTO.getPersonId()))){
								relationship.setRelationshipMemberId(ssapApplicantsDataDTO.getGuid());
							}
						}
					}
					relationship.setRelationshipToMember(relationships.getRelationshipCode());
					member.getRelationships().add(relationship);
				}
			}
		}
	}
	
	private void assignEnrollmentDetails(Long ssapApplicationGuid, Set<String> enrolledMembersIds, String userName){
		List<EnrollmentShopDTO> enrollmentDetails = getEnrollmentShopDto(ssapApplicationGuid,userName);
		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentDetails) {
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
			//if(enrollmentShopDTO.getPlanType().equalsIgnoreCase("health")){
				for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
					enrolledMembersIds.add(enrolleeMembers.getExchgIndivIdentifier());
				}
			//}
		}
	}
	
	private List<EnrollmentShopDTO> getEnrollmentShopDto(Long applicationId,String userName){
		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String xmlResponse = null;
		try {
			if(null  != applicationId){
				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(applicationId);
				xmlResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,xstream.toXML(enrolleeRequest))).getBody();
				enrolleeResponse = (EnrolleeResponse)xstream.fromXML(xmlResponse);
				if (null != enrolleeResponse & enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
					enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				}else{
					throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
				}
			}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			throw new GIRuntimeException("Exception occured while fetching enrollment details :");
		}
		return enrollmentShopDTOs;
	}

	@Override
	public void setHouseholdData(SsapApplication ssapForInd57, Ind19ClientRequest ind19ClientRequest, HouseholdData householdData) throws GIException {		
		SingleStreamlinedApplication applicationData = getSingleStreamlinedApplication(ssapForInd57.getApplicationData());
		householdData.setApplicationData(applicationData);
	}
	
	
}
