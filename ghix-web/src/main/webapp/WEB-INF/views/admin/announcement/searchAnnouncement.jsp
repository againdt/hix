<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>

<%@ page isELIgnored="false"%>

<div class="gutter10">	
    <div class="row-fluid">
        <ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li>Manage Announcement</li>
        </ul>
        <h1><a name="skip"></a>Manage Announcement</h1>
    </div>	
    
    <div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="gray">
            	<form class="form-vertical" id="frmsearchannouncement" name="frmsearchannouncement" action="manage" method="POST">
            	<div class="header">
                	<h4><spring:message code='label.refineresult' /></h4>
                </div>
                    <div class="gutter10 graybg">
                    	<div class="control-group">
                            <label for="name" class="control-label">Name</label>
                            <div class="controls">
                                <input class="input-medium" type="text" value="${searchCriteria.name}"  name="name" id="name"  size="20"/>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="effectiveStartDate" class="control-label">Effective Start Date</label>
                            <div class="controls">
                                <input type="text" title="MM/dd/yyyy" value="${searchCriteria.effectiveStartDate}" class="datepick input-medium"   name="effectiveStartDate" id="effectiveStartDate" />
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="effectiveEndDate" class="control-label">Effective End Date</label>
                            <div class="controls">
                                <input type="text" title="MM/dd/yyyy" value="${searchCriteria.effectiveEndDate}" class="datepick input-medium" name="effectiveEndDate" id="effectiveEndDate"  />
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="role" class="control-label">Role</label>
                            <div class="controls">
                                <c:set var="counter" value="0" />
                                <select name="role" class="input-medium" id="role"> 
                                    <option value="">SELECT</option> 
                                    <c:forEach var="role" items="${roles}"> 
                                        <option ${searchCriteria.role == role.id ? 'selected="selected"' : ''} value="${role.id}"> ${fn:replace(role.name, '_', ' ')}</option>
                                        <c:set var="counter" value="${counter+1}" />
                                    </c:forEach>  
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="status" class="control-label">Status</label>
                            <div class="controls">
                                <select  name="status" id="status" class="input-medium">
                                    <option value="">SELECT</option>
                                    <option ${searchCriteria.status == 'PENDING' ? 'selected="selected"' : ''} value="PENDING">PENDING</option>
                                    <option ${searchCriteria.status == 'APPROVED' ? 'selected="selected"' : ''} value="APPROVED">APPROVED</option>
                                    <option ${searchCriteria.status == 'DELETED' ? 'selected="selected"' : ''} value="DELETED">DELETED</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
                        
                        <div class="center">
                           <input type="submit" name="submitButton" id="submitButton" value="<spring:message  code="label.submit"/>" class="btn btn-primary" title="<spring:message  code="label.submit"/>"/>
                        </div>
                    </div>
                </form>
            </div>
		</div><!-- span3 ends-->
        
        <div class="span9 " id="rightpanel">
        	<form class="form-horizontal" id="frmManageQHP" name="frmManageQHP" method="POST">
            	<c:choose>
					<c:when test="${submit != '' || resultSize >= 1}">
                        <c:choose>
                            <c:when test="${fn:length(announcementlist) > 0}">
                                <table class="table">
                                    <thead>
                                        <tr class="header">
                                             <th scope="col"><dl:sort title="Announcement Id" sortBy="id" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Announcement Name" sortBy="name" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Effective Start Date" sortBy="effectiveStartDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Effective End Date" sortBy="effectiveEndDate" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
                                             <th scope="col"><dl:sort title="Status" sortBy="status" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>   
                                        </tr>
                                    </thead>
                                    <c:forEach items="${announcementlist}" var="announcement">
                                            <tr>
                                                <td><a href='edit/${announcement.id}'>${announcement.id} </a></td>
                                                <td>${announcement.name} </td>
                                                <td><fmt:formatDate value="${announcement.effectiveStartDate}" pattern="MM-dd-yyyy"/></td>
                                                <td><fmt:formatDate value="${announcement.effectiveEndDate}" pattern="MM-dd-yyyy"/></td>
                                                <td>${announcement.status} </td>
                                            </tr>
                                        </c:forEach>
                                </table>
                                <div class="center">
                                    <dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
                                </div>
                             </c:when>
                            <c:otherwise>
                               <h4 class="alert alert-info margin0"><spring:message  code='label.norecords'/></h4>
                            </c:otherwise>
                        </c:choose>	
		 			</c:when>
				</c:choose>
            </form>
		</div><!--span9 ends-->
	</div>	

    <div class="notes" style="display: none">
        <div class="row">
            <div class="span">

                <p>The prototype showcases three scenarios (A, B and C) dependent on
                    a particular Employer's eligibility to use the SHOP Exchange.</p>
            </div>
        </div>
    </div>

</div> <!-- row-fluid -->


<script type="text/javascript">
$(function() {
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
});
</script>