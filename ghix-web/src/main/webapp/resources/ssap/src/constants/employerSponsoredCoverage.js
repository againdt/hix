const EMPLOYER_DETAILS = {
    'companyName': null,
    'personId': null,
    'memberName': null,
    'name': {
        'firstName': '',
        'middleName': '',
        'lastName': '',
        'suffix': ''
    },
    'phone': null,
    'email': null,
    'address': {
        'postalCode': '',
        'county': null,
        'streetAddress1': '',
        'streetAddress2': '',
        'state': '',
        'city': ''
    },
    'ein': '',
    'minimumValuePlan': null,
    'employerPremium': 0,
    'employerPremiumFrequency': 'UNSPECIFIED',
    'employerOfferAffordable': false
};

export default EMPLOYER_DETAILS;
