package com.getinsured.ssap.json;

import java.io.IOException;
import java.text.SimpleDateFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.getinsured.ssap.model.hub.vlp.VlpPayload;
import com.getinsured.ssap.model.hub.vlp.documents.DhsDocument;
import com.getinsured.ssap.util.DhsDocumentSerializer;

/**
 * VLP Payload serializer.
 *
 * @author Yevgen Golubenko
 * @since 2/18/19
 */
public class VlpPayloadSerializer extends JsonSerializer<VlpPayload>
{
  private static final Logger log = LoggerFactory.getLogger(DhsDocumentSerializer.class);
  @Override
  public void serialize(final VlpPayload vlpPayload, final JsonGenerator jgen,
                        final SerializerProvider provider) throws IOException, JsonProcessingException
  {
    jgen.writeStartObject();

    jgen.writeObjectFieldStart("DHSID");

    String dhsId = "INVALID_DHS_ID";

    DhsDocument dhsDocument = vlpPayload.getDhsDocument();

    if(dhsDocument != null && dhsDocument.getDocumentType() != null) {
      dhsId = dhsDocument.getDocumentType().getDhsId();
    } else {
      log.error("DHS Document doesn't have DocumenType set! [documentType = {}]",
          (dhsDocument != null ? dhsDocument.getDocumentType() : dhsDocument ));
    }

    jgen.writeObjectField(dhsId, dhsDocument);
    jgen.writeEndObject(); // DHSID
    jgen.writeStringField("FirstName", vlpPayload.getFirstName());
    jgen.writeStringField("LastName", vlpPayload.getLastName());
    jgen.writeStringField("MiddleName", vlpPayload.getMiddleName());
    jgen.writeStringField("AKA", vlpPayload.getAka());

    String dateOfBirth = null;
    if(vlpPayload.getDateOfBirth() != null) {
      SimpleDateFormat sdf = new SimpleDateFormat(DhsDocument.DATE_FORMAT);
      dateOfBirth = sdf.format(vlpPayload.getDateOfBirth());
    }

    jgen.writeObjectField("DateOfBirth", dateOfBirth);
    jgen.writeBooleanField("RequestSponsorDataIndicator", vlpPayload.isRequestGrantDateIndicator());
    jgen.writeBooleanField("RequestGrantDateIndicator", vlpPayload.isRequestGrantDateIndicator());
    jgen.writeBooleanField("FiveYearBarApplicabilityIndicator", vlpPayload.isFiveYearBarApplicabilityIndicator());
    jgen.writeStringField("RequesterCommentsForHub", vlpPayload.getRequesterCommentsForHub());
    jgen.writeStringField("CasePOCFullName", vlpPayload.getCasePOCFullName());
    jgen.writeStringField("CasePOCPhoneNumber", vlpPayload.getCasePOCPhoneNumber());

    jgen.writeEndObject();
  }
}
