<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript"> window.history.forward(); </script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript">

function populateStateList(){
	var stateOptions = "";
	<c:forEach var="state" items="${statelist}">
		stateOptions += "<option id='${state.code}' value='${state.code}'>${state.name}</option> ";
	</c:forEach>
	
	return stateOptions;
}


function deleteEmployerLocation(employerlocationid, index) {
	var subUrl = '<c:url value="/shop/employer/application/deletelocation">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data: {employerLocationId: employerlocationid}, 
		success : function(response,xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			 if(response.toString().indexOf('Success') != -1){
				 if(employerlocationid > 0){
						checkLocationStateExist(employerlocationid);				
				  }
			 	  $('#employer_location_'+index).remove(); 
			 }else{
				 $('#employeeLocDiv').modal('show');
			 }
		},
		error : function(e) {
			alert("<spring:message code='label.employerfailedtodelete' javaScriptEscape='true'/>");
		}
	});
}

function checkLocationStateExist(employerlocationid) {
	var subUrl = '<c:url value="/shop/employer/application/checkLocationStateExist">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';

	$.ajax({
		type : "POST",
		url : subUrl,
		data: {employerLocationId: employerlocationid},
		success : function(response, xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			if(response == 'NO'){
				$('#statecheckdiv').modal('show');
			}
		},error : function(e) {
			alert("Error");
		}
	});
}


function getCountyList(eIndex, zip, county) {
	var subUrl = '<c:url value="/shop/employer/application/addCounties">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response,xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			populateCounties(response, eIndex, county);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, eIndex,county){
	$('#county_'+eIndex).html('');
	var optionsstring = '<option value="">Select County...</option>';
	var i =0;
	$.each(response, function(key, value) {
		var optionVal = key+'#'+value;
		var selected = (county == key) ? 'selected' : '';
		var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
		optionsstring = optionsstring + options;
		i++;
	});
	$('#county_'+eIndex).html(optionsstring);
	
}
	
</script>

<script type="text/javascript" src="<c:url value="/resources/js/employer_location.js" />"></script>  	
	<div class="gutter10">
		<div class="page-header">
			<h1><spring:message  code="label.worksitesh1"/><small> <spring:message  code="label.editcompanyinfo"/></small></h1>
		</div>
	
	<div class="row-fluid">	
		<div class="span3" id="sidebar">
           <div class="header">
               <h4 class="margin0"><spring:message  code="label.emprattesttxt1"/></h4>
           </div>
           <ul class="nav nav-list">
                   <li class="active"><a href="#"><spring:message  code="label.emprattesttxt2"/> </a></li>
                   <c:if test="${eStatus != 'NEW'}">
                   <li><a href="<c:url value="/shop/employer/application/attestationresult?prePage=${prePage}" />"><spring:message  code="label.emprattesttxt3"/></a></li>
                   </c:if>
           </ul>
		</div>
		<div class="span9" id="rightpanel">
				<form class="form-horizontal" id="frmeditcompanyinfo" name="frmeditcompanyinfo" action="editcompanyinfo" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="id" id="id" value="${employerOBJ.id}"/>
				<input type="hidden" name="prePage" id="prePage" value="${prePage}"/>
				
				<c:set var="location_id" value="${not empty empPrimaryLocationObj.location.id ? empPrimaryLocationObj.location.id : '0'}"/>
				<input type="hidden" id="id_0" name="locations[0].location.id" value="${location_id}"/>
				<c:set var="employer_location_id" value="${not empty empPrimaryLocationObj.id ? empPrimaryLocationObj.id : '0'}"/>  
				<input type="hidden" id="employer_location_id_0" name="locations[0].id" value="${employer_location_id}"/>
				<input type="hidden" id="location_FID" name="location[]" value="FID"/>
				<input type="hidden" id="lat_0" name="locations[0].location.lat" value="0.0" />
				<input type="hidden" id="lon_0" name="locations[0].location.lon" value="0.0" />
				<input type="hidden" id="countycode_0" name="locations[0].location.countycode" value="" />
				<input type="hidden" id="rdi_0" name="locations[0].location.rdi" value="" />
				<input type="hidden" id="primaryLocation_0" name="locations[0].primaryLocation" value="YES" />
				<div class="profile">
				
					<div class="header margin10-b">
						<h4><spring:message  code="label.companyidenti"/></h4>
					</div>
					<div class="control-group">
						<label for="name" class="control-label required"><spring:message  code="label.businessname"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				        <div class="controls">
							<input type="text" name="name" id="name" value="${employerOBJ.name}" class="input-large" size="30">
							<div id="name_error"></div>
						</div>	
					</div>
					
					
				    <div class="control-group">
		               <label for="federalEIN" class="control-label required">
		               		<spring:message  code="label.federalein"/>&#40;EIN&#41;
		               </label>
		                <div class="controls">
							<input type="text" name="federalEIN" id="federalEIN" value="${employerOBJ.federalEIN}" class="input-large" maxlength="9" size="30">
							&nbsp;&nbsp;
							<small>
								 	<a href="#einModal" role="button" class="" data-toggle="modal">
								 		<spring:message  code="label.eininfo"/>							
									</a>
							</small>
							<div id="federalEIN_error">
							</div>
					   </div>
		           </div>
		           
		           
		               <div class="control-group">
						<label for="orgType" class="control-label required"><spring:message  code="label.typeofcompany"/> </label>
				        <div class="controls">
							<select id="orgType" name="orgType">
								<option <c:if test="${employerOBJ.orgType == 'CORPORATION'}"> SELECTED </c:if> value="CORPORATION">Corporation</option>
								<option <c:if test="${employerOBJ.orgType == 'NON_PROFIT'}"> SELECTED </c:if> value="NON_PROFIT">Non-Profit</option>
								<option <c:if test="${employerOBJ.orgType == 'SOLE_PROPRIETORSHIP'}"> SELECTED </c:if> value="SOLE_PROPRIETORSHIP">Sole Proprietorship</option>
								<option <c:if test="${employerOBJ.orgType == 'PARTNERSHIP'}"> SELECTED </c:if>  value="PARTNERSHIP">Partnership</option>
							</select>
						</div>	
					</div>
				
				<h4><spring:message  code="label.emplEmpNos"/></h4>
				<div class="profile addressBlock">
					<div class="control-group">
						<label class="control-label required" for="fullTimeEmp"><spring:message code='label.emplFulltime' javaScriptEscape='true'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
				        <div class="controls">
							<input id="fullTimeEmp" class="input-mini" type="text" size="3" maxlength="3" value="${employerOBJ.fullTimeEmp}" name="fullTimeEmp"/>
							&nbsp;&nbsp;<small><a href="#fteModal" role="button" class="" data-toggle="modal">How do I calculate Full-Time Equivalent Employees?</a></small>
							<div id="fullTimeEmp_error"></div>
						</div>	
					</div>
				
					<h4><spring:message  code="label.primaryworksite"/></h4>
						<div class="control-group">
							<label for="address1_0" class="control-label"><spring:message  code="label.employeraddress1"/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
							<div class="controls">
								<input type="text" name="locations[0].location.address1" id="address1_0"  value="${empPrimaryLocationObj.location.address1}"  class="input-xlarge" size="30">
								<div id="address1_0_error"></div>	
								<input type="hidden" id="address1_0_hidden" name="address1_0_hidden">		
							</div>
						</div>
						
						<div class="control-group">
							<label for="address2_0" class="control-label required"><spring:message  code="label.employeraddress2"/></label>
							<div class="controls">
								<input type="text"  name="locations[0].location.address2" id="address2_0"   value="${empPrimaryLocationObj.location.address2}" class="input-xlarge" size="30">
								<input type="hidden"  id="address2_0_hidden" name="address2_0_hidden">
							</div>
						</div>
						
						<div class="control-group">
							<label for="city_0" class="control-label required"><spring:message  code="label.employercity"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="locations[0].location.city" id="city_0" class="input-xlarge"  value="${empPrimaryLocationObj.location.city}">
								<div id="city_0_error"></div>	
								<input type="hidden" id="city_0_hidden" name="city_0_hidden">		
							</div>
						</div>
						
						<div class="control-group">
							<label for="state_0" class="control-label required"><spring:message  code="label.employerstate"/> 
							<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select size="1" class="input-medium" name="locations[0].location.state" id="state_0"  >
									 <option id="${defaultStateCode}" value="${defaultStateCode}">${stateName}</option>
								</select>
								&nbsp;&nbsp;<small><a href="#businessmoves" role="button" class="" data-toggle="modal">
								<spring:message  code="label.businessmoves"/></a></small>
								<div id="state_0_error"></div>
								<input type="hidden" id="state_0_hidden" name="state_0_hidden" >	
							</div>
						</div>
						
						
						<div class="control-group">
							<label for="zip_0" class="control-label required"><spring:message  code="label.employerzipcode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<input type="text"  name="locations[0].location.zip" id="zip_0" maxlength="5" class="input-small  zipCode"  value="${empPrimaryLocationObj.location.zip}">
								<div id="zip_0_error"></div>	
								<input type="hidden" id="zip_0_hidden" name="zip_0_hidden" >		
							</div>
						</div>
						
						
						
						<div class="control-group">
							<label for="county_0" class="control-label required"><spring:message  code="label.country"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="controls">
								<select size="1" class="input-large" name="locations[0].location.county"  id="county_0">
								<option value="">Select County...</option>
								</select>
								<div id="county_0_error"></div>		
							</div>
						</div>
						<div class="control-group" id="validateZipDiv" class="fade" style="display:none">
								<div class="controls">
									<div class="error help-inline"><label class="error"><span><em class="excl">!</em> The primary worksite address must be in ${stateName} to participate in ${exchangeName}. <!-- <input type="button" value="OK" onclick="closeDiv();"/> --></span></label> </div>		
								</div>
						</div>
					</div>
					
					<hr>
					<div class="gutter10">
						<input id="addlocation" class="btn btn btn-small" type="button" name="addlocation" value="<spring:message  code="label.employeraddworksite"/>" onclick="employer_location.add_worksite('');">
						<small class="span9 pull-right">
									<spring:message  code='label.addworksitemessage1'/> ${exchangeName} <spring:message  code='label.addworksitemessage2'/>
						</small>
					</div>
					<div id="locationsBox"></div>
					
					</div><!-- profile -->
					<div class="form-actions">
					<c:if test="${prePage != null}">
						<input id="cancelBtn" class="btn" type="button" name="cancelBtn" value="<spring:message  code="label.cancelButton"/>" onclick="window.location.href='<c:url value='${prePage}' />'">
					</c:if>
					<c:if test="${prePage == null}">
						<input id="cancelBtn" class="btn" type="button" name="cancelBtn" value="<spring:message  code="label.cancelButton"/>" onclick="window.location.href='<c:url value='/shop/employer/application/attestation'/>'">
					</c:if>	
						<input type="button" name="submitbutton" id="submitbutton" class="btn btn-primary margin10-l" value="<spring:message  code='label.save'/>" title='Save' onclick="javascript:verifyFulltimeEmployees('${maxShopEmployee}');" />
					</div>
			</form>
		</div>
	</div>
</div>
    
    
			<div id="einModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<p id="myModalLabel">&nbsp;</p>
				</div>
				<div class="modal-body">
					<h3><spring:message  code='label.eininfotitle'/></h3>
					<p><spring:message  code='label.eininfodesc1'/> ${exchangeName} <spring:message  code='label.eininfodesc2'/></p>
					<a href="http://www.irs.gov/Businesses/Small-Businesses-&-Self-Employed/Apply-for-an-Employer-Identification-Number-(EIN)-Online">
						<spring:message  code='label.getnewein'/>
					</a>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.close'/></button>
				</div>
			</div>
			
			
<!-- Full-Time Equivalent Employees Modal -->
			<div id="fteModal" class="modal hide fade" tabindex="-1" data-backdrop="static" modal="true" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h3><spring:message code='label.emplFulltime' javaScriptEscape='true'/></h3>
				</div>
				<div class="modal-body">
					<p><spring:message  code="label.howcalculateFTEmodal1"/> ${maxShopEmployee} <spring:message  code="label.howcalculateFTEmodal2"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal3"/></p>
					<p><spring:message  code="label.howcalculateFTEmodal4"/> <a href="http://www.irs.gov/uac/Small-Business-Health-Care-Tax-Credit-Questions-and-Answers:-Determining-FTEs-and-Average-Annual-Wages"><spring:message  code="label.howcalculateFTEmodal5"/></a> <spring:message  code="label.howcalculateFTEmodal6"/></p>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code='label.close'/></button>
				</div>
			</div>
			
			
			
			<div id="businessmoves" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<p id="myModalLabel">&nbsp;</p>
				</div>
				<div class="modal-body">
					 <h3><spring:message  code='label.businessmovestitle'/></h3>
					 <p>
					 	<spring:message  code='label.businessmoves1'/> ${exchangeName} <spring:message  code='label.businessmoves2'/> ${stateName}.
					 </p>
					 <p>
					 	<spring:message code='label.businessmoves3' />${stateName}<spring:message code='label.businessmoves4'  />${exchangeName}
					 	<spring:message code='label.businessmoves5' />
					 </p>
					 <p>
					 	<spring:message code='label.businessmoves6' />
					 </p>
					 <div class="margin10 span12">
						 <ol>
						 	<li style="list-style: decimal;"><spring:message code='label.businessmoves7' /></li>
						 	<li style="list-style: decimal;"><spring:message code='label.businessmoves8' /></li>
						 </ol>
					 </div>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.close'/></button>
				</div>
			</div>			
			
			
			
			
<div id="statecheckdiv" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header modal-heading">
  	Worksite Location
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
  </div>
  <div class="modal-body">
    <p>The ${exchangeName} is only available to companies with at least one worksite in ${stateName}. </p>
    <br>
    <p>If you no longer have a worksite in ${stateName}, your participation in SHOP will be terminated by the Exchange.</p>
  </div>
  <div class="modal-footer">
		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.ok'/></button>
  </div>
</div>

<div id="employeeLocDiv" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header modal-heading">
	Worksite assigned to employees
   	 <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
   	
  </div>
  <div class="modal-body">
    <p>The worksite is currently assigned to employees. You must re-assign the employees to other worksites before you can delete this worksite address. </p>
  </div>
  <div class="modal-footer">
		<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true"><spring:message  code='label.ok'/></button>
  </div>
</div>

<div id="warningFTEDiv"  class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="model-inputs" aria-hidden="true">
  <div class="modal-header modal-heading">
	<spring:message code='label.emplFulltime' javaScriptEscape='true'/>
   	
  </div>
  <div class="modal-body">
    <p>For initial acceptance into the SHOP program, you must have between 1 to ${maxShopEmployee} full-time equivalent employees. If your company was previously approved for SHOP, you can continue to participate even if your company grows beyond ${maxShopEmployee} full-time equivalent employees. </p>
  </div>
  <div class="modal-footer">
		<button class="btn btn-primary" data-dismiss="modal" onclick="javascript:formsubmit();" aria-hidden="true"><spring:message  code='label.ok'/></button>
  </div>
</div>


<script type="text/javascript">
jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
    if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}return true;
});

var existing_ein = '${employerOBJ.federalEIN}';

jQuery.validator.addMethod("eincheck", function(value, element, param) {
	var einvalue = $('#federalEIN').val();
	einvalue = jQuery.trim(einvalue);
	existing_ein = jQuery.trim(existing_ein);
	if(existing_ein!=''){
		if(einvalue.length == 0){
			return false;	
		}
	}
	return true;	
});

var validator = $('#frmeditcompanyinfo').validate({ 
	rules : {
		name : { required : true},
		fullTimeEmp : {required : true, number: true},
		federalEIN  : {eincheck : true, number: true, minlength: 9},
		'locations[0].location.address1' : { required : true},
		'locations[0].location.zip' : { required : true, zipcheck:true},
		'locations[0].location.city' : {required: true},
		'locations[0].location.state' : {required: true},
		'locations[0].location.county' : {required: true}
	},
	messages : {
		name : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCompanyName' javaScriptEscape='true'/></span>"},
		fullTimeEmp : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeemployee' javaScriptEscape='true'/> </span>",
						number: "<span> <em class='excl'>!</em><spring:message code='label.validatefulltimeempnumber' javaScriptEscape='true'/></span>" },
		federalEIN  : { eincheck : "<span> <em class='excl'>!</em><spring:message code='label.validateeinnumber' javaScriptEscape='true'/></span>",
						number: "<span> <em class='excl'>!</em><spring:message code='label.validateeinnumber' javaScriptEscape='true'/></span>", 
						minlength: 	"<span> <em class='excl'>!</em><spring:message code='label.validateeinlength' javaScriptEscape='true'/></span>"
		}, 
		'locations[0].location.address1' : { required : "<span> <em class='excl'>!</em> <spring:message code='label.validateAddress' javaScriptEscape='true'/></span>"},
		'locations[0].location.zip' : { required : "<span> <em class='excl'>!</em> <spring:message code='label.validateZip' javaScriptEscape='true'/></span>" ,
					zipcheck : "<span><em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
				 },
		'locations[0].location.city' : {required : "<span> <em class='excl'>!</em> <spring:message code='label.validateCity' javaScriptEscape='true'/></span>"},
		'locations[0].location.state' : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validateState' javaScriptEscape='true'/></span>" },
		'locations[0].location.county' : { required: "<span> <em class='excl'>!</em> <spring:message code='label.validatecounty' javaScriptEscape='true'/></span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

function validateZipCode(eindex,zipval){
	
	var subUrl = '<c:url value="/shop/employer/application/validateZipCode">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zipval}, 
		dataType:'json',
		success : function(response,xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			if(response=='NO'){
				$('#validateZipDiv').show();
				$('#county_'+eindex).html('');
			}
			else{
				$('#validateZipDiv').hide();
				getCountyList(eindex, zipval, '');
			}
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
	
}
function closeDiv(){
	$('#validateZipDiv').hide();
}

function formsubmit(){
	$('#frmeditcompanyinfo').submit();
}

function verifyFulltimeEmployees(maxShopEmployee){
	var fteCount = parseInt($('#fullTimeEmp').val());
	var statecode = '${defaultStateCode}';
	
	if(fteCount > parseInt(maxShopEmployee) && statecode == 'MS' ){
		 $('#warningFTEDiv').modal('show');
	}else{
		formsubmit();
		return true;
	}
	
	return false;
}

function validateAddress(eindex){
	
	var address1_e='address1_'; var address2_e='address2_'; var city_e= 'city_'; var state_e='state_'; var zip_e='zip_';
	var lat_e='lat_';var lon_e='lon_'; var rdi_e='rdi_'; var county_e='county_';
	address1_e=address1_e+eindex;
	address2_e=address2_e+eindex;
	city_e=city_e+eindex;
	state_e=state_e+eindex;
	zip_e=zip_e+eindex;
	lat_e=lat_e+eindex;
	lon_e=lon_e+eindex;
	rdi_e=rdi_e+eindex;
	county_e=county_e+eindex;

	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
	
	
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
		if(($('#'+ address2_e).val())==="Address Line 2"){
			$('#'+ address2_e).val('');
		}	
	/* 	viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);	 */
	}
	
	//if zip not empty
	if($('#'+ zip_e).val()!=''){
			if(eindex==0){
				validateZipCode(eindex, $('#'+ zip_e).val());
			}
			if(eindex!=0){
				getCountyList(eindex, $('#'+ zip_e).val(), '');
			}
	}
	$('#'+address1_e).focusin(function(e) {
		if($('#'+zip_e).val().length >= 5){
			getCountyList(eindex, $('#'+ zip_e).val(), '');
			$('#'+address1_e).unbind( "focusin" );
		 }   
	});
	
}

function postPopulateIndex(indexValue, zipCodeValue, zipId){
	//console.log(indexValue, zipCodeValue, zipId);
	//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( indexValue,zipCodeValue, '' );
}


$(document).ready(function(){
	//$('#zip_0').bind("focusout",function () {validateAddress(0);});
	
	var primeAddDone=false;
	var index = 0;
	var empLocationJson = ${empLocationJson};
	if(empLocationJson != ""){
		for(var i=1; i<=empLocationJson.length; i++){
		    var empLocationObj = empLocationJson[i-1];
		    if(empLocationObj['primary'] == 'YES'){
		    	index = 0;
		    	primeAddDone=true;
		    }else{
		    	
		    	index = primeAddDone ? i-1 : i;
		    	employer_location.add_worksite(empLocationObj, '');
		    }
		    getCountyList(index, empLocationObj['zip'], empLocationObj['county']);
		};
	};
	
	$(document).on('onGetCountyList',getCountyList);
});
$('.info').tooltip();
</script>