import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import Form from './Form';
import NamesInputs from './pageIncludes/common/names/NamesInputs';
import PageHeader from '../headers/PageHeader';
import RepresentativeAddressInputs from './pageIncludes/common/address/RepresentativeAddressInputs';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData, fetchCounties, showModal } from '../../actions/actionCreators';

// Utils

import createSignature from '../../utils/createSignature';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import CONFIG from '../../.env';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import ENDPOINTS from '../../constants/endPointsList';
import INPUT_TYPES from '../../constants/inputTypes';
import MODAL_TYPES from '../../constants/modalTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import EMPTY_OBJECTS from '../../constants/emptyObjects';

// CustomHooks

import {useFetchEffect} from '../../hooks/customHooks';


/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Application Assistance' Page
 *
 */
const Page1_03 = props => {

    // LocalState of Functional Component

    const [localState, setLocalState] = useState({
        signature: ''
    });

    const {
        currentData,
        counties,
        data,
        headOfHousehold
    } = props;

    // Fetching needed counties for state / states:
    // a) on ComponentDidMount
    // b) on ComponentDidUpdate (if changes are related to <authorizedRepresentative.address.state>)

    const statesForCounties = [ currentData.authorizedRepresentative.address.state ];

    const { selectedCall } = useFetchEffect(
        statesForCounties,
        [
            ENDPOINTS.COUNTIES_FOR_STATE_ENDPOINT,
            ENDPOINTS.COUNTIES_FOR_STATES_ENDPOINT
        ],
        counties,
        props.actions.fetchCounties
    );

    useEffect(() => {
        if(CONFIG.ENV.PRODUCTION) {
            selectedCall && selectedCall !== null && selectedCall();
        }
    }, [selectedCall]);

    // Updating some properties in JSON

    const onFieldChange = (...args) => {
        let [ newData, , , [ name, value ] ] = args;
        let newState = { ...data };
        if(name === 'getHelpIndicator' && !value) {
            newData = {
                ...newData,
                authorizedRepresentativeIndicator: null,
                authorizedRepresentative: EMPTY_OBJECTS.AUTHORIZED_REPRESENTATIVE
            };
        }
        newState.singleStreamlinedApplication = newData;
        props.actions.changeData(newState);
    };

    // Updating <address> object in <authorizedRepresentative> object with the simultaneous check if state is changed

    const onFieldChange_name_and_address = newData => {
        const currentState = newData.address.state;

        if (currentState !== props.currentData.authorizedRepresentative.address.state) {
            newData.address.countyCode = null;
        }

        let newState = { ...data };
        newState.singleStreamlinedApplication.authorizedRepresentative = newData;
        props.actions.changeData(newState);
    };

    // Updating exact type of phone extension in <phone> object

    const onFieldChange_phone_extension = (newData, indexInArray) => {
        let newState = { ...data };
        newState.singleStreamlinedApplication.authorizedRepresentative.phone[indexInArray] = newData;
        props.actions.changeData(newState);
    };

    // Updating <signature> value in LocalState

    const onFieldChange_signature = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
    };

    const onFieldBlur_signing = () => {
        setLocalState(state => ({
            ...state,
            signature: state.signature.trim()
        }));
    };

    // Opening <Local Assistance Modal>

    const handleOpeningModal = () => {
        props.actions.showModal(MODAL_TYPES.LOCATE_ASSISTANCE_MODAL);
    };

    const {
        getHelpIndicator,
        authorizedRepresentativeIndicator,
        authorizedRepresentative: {
            //signatureisProofIndicator,
            phone,
            partOfOrganizationIndicator,
            address: {
                postalCode,
                countyCode,
                streetAddress1,
                streetAddress2,
                state,
                city
            },
            name: {
                middleName,
                lastName,
                firstName,
                suffix
            },
            emailAddress,
            organizationId,
            companyName
        },
    } = props.currentData;

    const { name } = headOfHousehold;

    const handlePhoneChange = (...args) => {
        const [ newData , indexInArray ] = args;
        const newState = { ...data };

        newState
            .singleStreamlinedApplication
            .authorizedRepresentative
            .phone[indexInArray]
            .phoneNumber = newData.phoneNumber;
        props.actions.changeData(newState);
    };

    return (
        <Fragment>
            <div id="saApplyingForCoverage" className="">
                <Form>
                    <div className="subsection">
                        <PageHeader
                            text="Who is Helping you?"
                            headerClassName={'usa-heading-alt'}
                        />
                        <UniversalInput
                            legend={{
                                legendText: 'Is anyone helping you with this application?'
                            }}
                            label={{
                                text: DROPDOWN_OPTIONS.AUTHORIZED_REPRESENTATIVES,
                                labelClass: ['usa-width-one-whole', 'layout--02', 'margin-t-0'],
                                ulClass:['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'getHelpIndicator',
                                value: getHelpIndicator,
                                options: DROPDOWN_OPTIONS.AUTHORIZED_REPRESENTATIVES,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: currentData,
                                fields: []

                            }}
                            errorClasses={['margin-l-0']}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        <p>
                            <small>(If you would like assistance, let us help you find a &nbsp;
                                <a
                                    className="usa-external_link"
                                    id="pop_getAssistance"
                                    href="javascript:void(0)"
                                    onClick={handleOpeningModal}
                                >
                                    licensed broker or certified enrollment counselor.
                                </a>
                            )</small>
                        </p>
                    </div>
                    {/*******************************************
                     Begin - If User is #getHelpIndicator (Yes)
                     ********************************************/}

                    {
                        getHelpIndicator && (
                            <div>
                                <div className="subsection fade-in">
                                    <PageHeader
                                        text="Authorized Representative"
                                        headerClassName={'usa-heading-alt'}
                                    />
                                    <p>
                                        If someone is helping you complete your application,
                                        you can designate that person as your Authorized Representative.
                                    </p>
                                    <p>
                                        An Authorized Representative is any adult who is sufficiently aware of
                                        the household circumstances and is authorized by the household to
                                        act on behalf of the household for eligibility purposes.
                                        By designating an Authorized Representative, you are giving permission
                                        for your authorized representative to:
                                    </p>
                                    <ul>
                                        <li>
                                            Sign the application on your behalf
                                        </li>
                                        <li>
                                            Act on your behalf for all matters related to the application and account
                                        </li>
                                    </ul>
                                    <p>
                                        Please note: An Authorized Representative is not certified by <strong>Nevada Health Link</strong>.
                                        This is different than designating an Agent or an Enrollment Counselor
                                        who has completed training and is certified by <strong>Nevada Health Link</strong>.
                                    </p>
                                </div>
                                <div className="subsection">
                                    <UniversalInput
                                        legend={{
                                            legendText: 'Do you want to name someone as your authorized representative?',
                                        }}
                                        label={{
                                            text: 'Do you want to name someone as your authorized representative?',
                                            labelClass: ['usa-width-one-whole', 'layout--02'],
                                            ulClass:['usa-width-one-half'],
                                            showRequiredIcon: true
                                        }}
                                        inputData={{
                                            isInputRequired: true,
                                            type: INPUT_TYPES.RADIO_BUTTONS,
                                            name: 'authorizedRepresentativeIndicator',
                                            value: authorizedRepresentativeIndicator,
                                            options: DROPDOWN_OPTIONS.YES_NO,
                                            validationType: VALIDATION_TYPES.CHOSEN,
                                            currentData: currentData,
                                            fields: []

                                        }}
                                        inputActions={{
                                            onFieldChange
                                        }}
                                    />
                                    {/************************************************************
                                        Begin - If User is #authorizedRepresentativeIndicator (Yes)
                                    *************************************************************/}

                                    {
                                        authorizedRepresentativeIndicator &&

                                        <Fragment>

                                            <PageHeader
                                                text="Authorized Representative Contact Information"
                                                headerClassName={'usa-heading-alt'}
                                            />
                                            <NamesInputs
                                                {...{
                                                    firstName,
                                                    middleName,
                                                    lastName,
                                                    suffix,
                                                    currentData: currentData.authorizedRepresentative,
                                                    fields: ['name'],
                                                    isRequired: true,
                                                    inputActions: {
                                                        onFieldChange: onFieldChange_name_and_address
                                                    }
                                                }}
                                            />
                                            <UniversalInput
                                                label={{
                                                    text: 'Email Address'
                                                }}
                                                inputData={{
                                                    isInputRequired: true,
                                                    type: INPUT_TYPES.REGULAR_INPUT,
                                                    name: 'emailAddress',
                                                    helperClasses: [],
                                                    value: emailAddress,
                                                    placeholder: 'Enter your email',
                                                    validationType: VALIDATION_TYPES.EMAIL,
                                                    currentData: currentData,
                                                    fields: ['authorizedRepresentative']
                                                }}
                                                errorClasses={['usa-offset-one-third']}
                                                inputActions={{
                                                    onFieldChange
                                                }}
                                            />

                                            <PageHeader
                                                text="Authorized Representative Home Address"
                                                headerClassName={'usa-heading-alt margin-tb-20-10'}
                                            />
                                            <RepresentativeAddressInputs
                                                {...{
                                                    postalCode,
                                                    countyCode,
                                                    streetAddress1,
                                                    streetAddress2,
                                                    state,
                                                    city,
                                                    counties,
                                                    currentData: currentData.authorizedRepresentative,
                                                    inputActions: {
                                                        onFieldChange: onFieldChange_name_and_address
                                                    }
                                                }}
                                            />


                                            <PageHeader
                                                text="Authorized Representative Phone"
                                                headerClassName={'usa-heading-alt margin-tb-20-10'}
                                            />

                                            {
                                                phone.map((ph, i) => {

                                                    const {
                                                        phoneExtension,
                                                        phoneNumber,
                                                        phoneType
                                                    } = ph;

                                                    const getPhoneType = type => {
                                                        switch(type) {
                                                        case 'WORK': return 'Work Phone Number';
                                                        case 'CELL': return 'Mobile Phone Number';
                                                        case 'HOME': return 'Home Phone Number';
                                                        }
                                                    };

                                                    return (
                                                        <Fragment key={i}>
                                                            <UniversalInput
                                                                label={{
                                                                    text: getPhoneType(phoneType)
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: false,
                                                                    type: INPUT_TYPES.PHONE_NUMBER_INPUT,
                                                                    name: 'phoneNumber',
                                                                    helperClasses: [],
                                                                    value: phoneNumber,
                                                                    placeholder: '(xxx) xxx-xxxx',
                                                                    validationType: VALIDATION_TYPES.PHONE_NUMBER,
                                                                    currentData: currentData.authorizedRepresentative.phone[i],
                                                                    fields: []
                                                                }}
                                                                inputActions={{
                                                                    onFieldChange: handlePhoneChange
                                                                }}
                                                                indexInArray={i}
                                                            />
                                                            <UniversalInput
                                                                label={{
                                                                    text: 'Phone Extension'
                                                                }}
                                                                inputData={{
                                                                    isInputRequired: false,
                                                                    type: INPUT_TYPES.REGULAR_INPUT,
                                                                    name: 'phoneExtension',
                                                                    helperClasses: [],
                                                                    value: phoneExtension,
                                                                    placeholder: 'Ext.',
                                                                    validationType: VALIDATION_TYPES.PHONE_EXTENSION,
                                                                    currentData: currentData.authorizedRepresentative.phone[i],
                                                                    fields: []
                                                                }}
                                                                inputActions={{
                                                                    onFieldChange: onFieldChange_phone_extension
                                                                }}
                                                                indexInArray={i}
                                                            />
                                                        </Fragment>
                                                    );
                                                })
                                            }


                                            <UniversalInput
                                                legend={{
                                                    legendText: 'Is this person part of an organization helping you apply for health insurance?'
                                                }}
                                                label={{
                                                    text: DROPDOWN_OPTIONS.YES_NO,
                                                    labelClass: ['usa-width-one-whole', 'layout--02', 'margin-tb-20-10'],
                                                    ulClass:['usa-width-one-half'],
                                                    showRequiredIcon: true
                                                }}
                                                inputData={{
                                                    isInputRequired: true,
                                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                                    name: 'partOfOrganizationIndicator',
                                                    value: partOfOrganizationIndicator,
                                                    options: DROPDOWN_OPTIONS.YES_NO,
                                                    validationType: VALIDATION_TYPES.CHOSEN,
                                                    currentData: currentData,
                                                    fields: ['authorizedRepresentative']
                                                }}
                                                inputActions={{
                                                    onFieldChange
                                                }}
                                            />

                                            {/*****************************************************
                                             Begin - If User is #partOfOrganizationIndicator (Yes)
                                             ******************************************************/}

                                            {
                                                partOfOrganizationIndicator &&

                                                <Fragment>
                                                    <UniversalInput
                                                        label={{
                                                            text: 'Organization Name'
                                                        }}
                                                        inputData={{
                                                            isInputRequired: true,
                                                            type: INPUT_TYPES.REGULAR_INPUT,
                                                            name: 'companyName',
                                                            helperClasses: [],
                                                            value: companyName,
                                                            placeholder: 'Organization Name',
                                                            // validationType: VALIDATION_TYPES.IS_ALL_LETTERS,
                                                            currentData: currentData,
                                                            fields: ['authorizedRepresentative']
                                                        }}
                                                        inputActions={{
                                                            onFieldChange
                                                        }}
                                                    />
                                                    <UniversalInput
                                                        label={{
                                                            text: 'Organization ID'
                                                        }}
                                                        inputData={{
                                                            isInputRequired: true,
                                                            type: INPUT_TYPES.REGULAR_INPUT,
                                                            name: 'organizationId',
                                                            helperClasses: [],
                                                            value: organizationId,
                                                            placeholder: 'Organization ID',
                                                            // validationType: VALIDATION_TYPES.IS_ALL_NUMERIC,
                                                            currentData: currentData,
                                                            fields: ['authorizedRepresentative']
                                                        }}
                                                        inputActions={{
                                                            onFieldChange
                                                        }}
                                                    />
                                                </Fragment>
                                            }

                                            {/****************************************************
                                             End - If User is #partOfOrganizationIndicator (Yes)
                                             *****************************************************/}
                                            <div className="usa-width-one-whole margin-t-20 margin-b-20">
                                                <p>
                                                    To make someone an Authorized Representative, &nbsp;
                                                    <strong>
                                                        {normalizeNameOfPerson(name)} &nbsp;
                                                    </strong>
                                                    needs to sign here.
                                                </p>
                                            </div>
                                            {/*<UniversalInput*/}
                                            {/*    legend={{*/}
                                            {/*        legendText: 'Select an option below'*/}
                                            {/*    }}*/}
                                            {/*    label={{*/}
                                            {/*        text: 'Select an option below',*/}
                                            {/*        labelClass: ['usa-width-one-whole', 'layout--02'],*/}
                                            {/*        ulClass:['usa-width-one-whole'],*/}
                                            {/*        showRequiredIcon: false*/}
                                            {/*    }}*/}
                                            {/*    inputData={{*/}
                                            {/*        isInputRequired: false,*/}
                                            {/*        type: INPUT_TYPES.RADIO_BUTTONS,*/}
                                            {/*        name: 'signatureisProofIndicator',*/}
                                            {/*        value: signatureisProofIndicator,*/}
                                            {/*        options: DROPDOWN_OPTIONS.SIGNATURE,*/}
                                            {/*        validationType: VALIDATION_TYPES.CHOSEN,*/}
                                            {/*        currentData: currentData,*/}
                                            {/*        fields: ['authorizedRepresentative']*/}

                                            {/*    }}*/}
                                            {/*    errorClasses={['usa-offset-one-third']}*/}
                                            {/*    inputActions={{*/}
                                            {/*        onFieldChange*/}
                                            {/*    }}*/}
                                            {/*/>*/}

                                            {/**************************************************
                                             Begin - If User is #signatureisProofIndicator (Yes)
                                             ***************************************************/}

                                            {/*{*/}
                                            {/*    signatureisProofIndicator &&*/}

                                            <UniversalInput
                                                label={{
                                                    text: 'Type your full name here'
                                                }}
                                                inputData={{
                                                    isInputRequired: true,
                                                    type: INPUT_TYPES.REGULAR_INPUT,
                                                    name: 'signature',
                                                    helperClasses: [],
                                                    value: localState.signature,
                                                    valuesToMatch: createSignature(name),
                                                    placeholder: 'Signature',
                                                    validationType: VALIDATION_TYPES.IS_SIGNED,
                                                    currentData: localState,
                                                    fields: []
                                                }}
                                                errorClasses={['usa-offset-one-third']}
                                                inputActions={{
                                                    onFieldChange: onFieldChange_signature,
                                                    onFieldBlur: onFieldBlur_signing
                                                }}
                                            />
                                            {/*}*/}

                                            {/*************************************************
                                             End - If User is #signatureisProofIndicator (Yes)
                                             **************************************************/}

                                            {/*<div className="usa-width-one-whole">*/}
                                            {/*    <div className="usa-alert usa-alert-info alert--info">*/}
                                            {/*        <div className="usa-alert-body" >*/}
                                            {/*            <p className="usa-alert-text">*/}
                                            {/*                Note to Authorized Representative: You will be contacted directly by*/}
                                            {/*                Nevada Health Link to confirm that you accept the terms and conditions*/}
                                            {/*                of the Authorized Representative role.*/}
                                            {/*            </p>*/}
                                            {/*        </div>*/}
                                            {/*    </div>*/}
                                            {/*</div>*/}
                                        </Fragment>
                                    }

                                    {/************************************************************
                                     End - If User is #authorizedRepresentativeIndicator (Yes)
                                     *************************************************************/}
                                </div>
                            </div>
                        )
                    }

                    {/*****************************************
                     End - If User is #getHelpIndicator (Yes)
                     ******************************************/}

                </Form>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    currentData: state
        .data
        .singleStreamlinedApplication,

    counties: state.counties,
    data: state.data,

    headOfHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember[0]
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        fetchCounties,
        showModal
    }, dispatch)
});

Page1_03.defaultProps = {
    name: 'Page1_03'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page1_03);
