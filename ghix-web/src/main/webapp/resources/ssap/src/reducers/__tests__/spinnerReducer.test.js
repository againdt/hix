import spinnerReducer, { initialState } from '../spinnerReducer';
import { spinnerActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const clearAction = {
    type: spinnerActionTypes.CLEAR_STATE
};

const startAction = {
    type: spinnerActionTypes.START_SPINNER,
    message: 'Loading data...'
};

const stopSpinner = {
    type: spinnerActionTypes.STOP_SPINNER
};

describe('Test for spinnerReducer: ', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(
            spinnerReducer(
                initialState,
                anotherAction
            )
        ).toEqual(initialState);
    });

    it('Should start spinner', () => {
        expect(spinnerReducer(
            initialState,
            startAction
        )).toEqual({
            activeSpinnersQty: initialState.activeSpinnersQty + 1,
            message: startAction.message
        });
    });

    it('Should warn that stop spinner invoked before start spinner', () => {
        expect(() => {
            spinnerReducer(
                initialState,
                stopSpinner
            );
        }).toThrow('Stop spinner invoked before start spinner');
    });

    it('Should stop spinner and keep message', () => {
        const currentState = (() => ({
            activeSpinnersQty: initialState.activeSpinnersQty + 2,
            message: 'The second message'
        }))();

        expect(spinnerReducer(
            currentState,
            stopSpinner
        )) .toEqual({
            activeSpinnersQty: currentState.activeSpinnersQty - 1,
            message: currentState.activeSpinnersQty === 1 ? null : currentState.message
        });
    });

    it('Should stop spinner and clear state', () => {
        const currentState = (() => ({
            activeSpinnersQty: initialState.activeSpinnersQty + 1,
            message: 'The only one message'
        }))();

        expect(spinnerReducer(
            currentState,
            stopSpinner
        )) .toEqual({
            activeSpinnersQty: currentState.activeSpinnersQty - 1,
            message: currentState.activeSpinnersQty === 1 ? null : currentState.message
        });
    });

    it('Should clear state', () => {
        expect(spinnerReducer(
            {
                activeSpinnersQty: 12,
                message: 'Some message'
            },
            clearAction
        )).toEqual(initialState);
    });
});