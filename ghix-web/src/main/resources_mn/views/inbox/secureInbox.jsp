<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
  String timeZone = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.TIME_ZONE);
%>
<c:set var="timeZone" value="<%=timeZone%>" />

<table id="inbox" class="table table-condensed table-border-none">
	<thead>
		<tr class="header">
			<th scope="col" class="txt-center"><span class="hidden">Messages</span><i class="icon-envelope"></i></th>
			<th scope="col" class="sortable" width="60"><dl:sort title="From" sortBy="fromUserName" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
			<th scope="col" class="sortable"><dl:sort title="Subject" sortBy="msgSub" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
			<th scope="col" class="sortable span2"><dl:sort title="Date" sortBy="dateCreated" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
			<th scope="col"><span class="hidden">Attachments</span><i class="icon-attachment"></i></th>
		</tr>
	</thead>
	<tbody class="msg">
		<c:choose>
			<c:when test="${inboxMsgList != null}">
				<c:forEach var="inboxMsg" items="${inboxMsgList}">
					<tr class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
						<td><i class="${inboxMsg.status == 'N' ? 'icon-envelope-unread' : 'icon-envelope-read'}"></i></td>
						<td class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
							<a href="javascript: doReadSubmit(${inboxMsg.id});"><i class="icon-person"></i>${inboxMsg.fromUserName}</a>
						</td>
						<td><a href="javascript: doReadSubmit(${inboxMsg.id});">${inboxMsg.msgSub}</a></td>
						<td>
							<jsp:useBean id="now" class="java.util.Date"/>
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="MMM dd, yyyy" var="dateCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="MMM dd, yyyy" var="dateNow" />
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="yyyy" var="yearCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="yyyy" var="yearNow" />
							<c:choose>
								<c:when test="${dateCreated eq dateNow}">
									<fmt:parseDate value="${inboxMsg.dateCreated}" var="time" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:timeZone value="${timeZone}">
									<fmt:formatDate type="TIME" value="${time}" pattern="hh:mm a" timeZone="${timeZone}"/>
									</fmt:timeZone>
								</c:when>
								<c:when test="${yearCreated eq yearNow}">
									<fmt:parseDate value="${inboxMsg.dateCreated}" var="sameYearDate" pattern="yyyy-MM-dd HH:mm:ss"/>
									<fmt:timeZone value="${timeZone}">
									<fmt:formatDate type="DATE" value="${sameYearDate}" pattern="EEE MMM dd" timeZone="${timeZone}"/>
									</fmt:timeZone>
								</c:when>
								<c:otherwise>
									<fmt:parseDate value="${dateCreated}" var="otherYearDate" pattern="MMM dd, yyyy"/>
									<fmt:timeZone value="${timeZone}">
									<fmt:formatDate type="DATE" value="${otherYearDate}" pattern="MMM dd, yyyy" timeZone="${timeZone}"/>
									</fmt:timeZone>
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<i class="${inboxMsg.messageDocs == null || empty inboxMsg.messageDocs ? '': 'icon-attachment'}"></i>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr class="read">
					<td colspan="7"><spring:message code='label.norecords' /></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
<div class="center">
	<dl:paginate resultSize="${totalRecords + 0}" pageSize="${pageSize + 0}" firstAndLastView="true" hideTotal="true" />
</div>