
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View account</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link type="text/css" rel="stylesheet" media="screen" href="//fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.9.1.custom.css" rel="stylesheet">
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">
<style>
	table.table.table-bordered{
		font-size:12px;
		border-left: 1px solid #ddd;
	}
	td.border-left{
		border-left:1px solid #d6e2e3;
	}
/* 	.table td{
		border-left: 1px solid #d6e2e3;
	}
	td.border-left{
		border-right:1px solid #d6e2e3;
	}
	td.no-lt-border{
		border-left:none;
		border-top:none;
	} */
	table.txt-center tr td{
		text-align:center;
	}
</style>

<form class="form-horizontal">
<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="span3">
       <!--  <a class="btn btn-link" href="e1-1-start-app">Back </a> -->
      </div>
      <div class="span9 pull-left">
        <h3>Your Application Status</h3>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
     <!--  <div class="gutter10">
        <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> <i class="icon-ok"></i> Additional Questions </a></li>
          <li class="active"><a href="e6-review-declare-file"> <i class="icon-ok"></i> Review, Declare &amp; File </a></li>
        </ul>
        </div>gutter10 -->
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
      	<p class="alert alert-success">Your application is accepted.</p>

      	<div class="header">
      		<h4>Eligibility Results</h4>
      	</div>
      	
		<div class="gutter10">
      	<table class="table table-bordered txt-center">
      	<thead>
      			<tr>
				 	<td><strong>Person</strong></td>
                      <td><strong>Applying for Coverage</strong></td>
                      <td><strong>Program</strong></td>
                      <td><strong>Status</strong></td>
                      <td><strong>Household Eligiblity Amount</strong></td>
                      </tr>
           </thead>
           <tbody>
                  <tr>
                  	<td>Stan Jacobs</td>
                    <td>No</td>
                    <td>--</td>
                    <td>--</td>
                    <td rowspan="3" class="border-left">$4,595</td>
                  </tr>
                  <tr>
                    <td>Janet Jacobs</td>
                    <td>Yes</td>
                    <td>Premium Tax Credit</td>
                    <td>Eligible</td>
                    
                  </tr>
                  <tr>
                    <td>Joe Jacobs</td>
                    <td>Yes</td>
                    <td >Premium Tax Credit</td>
                    <td>Eligible</td>
                  </tr>
            </tbody>
        </table>
      	<small><a href="#"><i class="icon-info-sign"></i> Click here for more information</a></small> 				
      	</div>
      	
        <div class="header">
  			<h4>More Information &amp; Appeals</h4>
  		</div>
  		<div class="gutter10">
	  		<p>If I think the Health Insurance Marketplace or Medicaid/Children's Health Insurance Program (CHIP) has made a mistake, 
	  		I can appeal its decision. To appeal means to tell someone at the Marketplace or Medicaid/CHIP that I think the action is wrong 
	  		and ask for a fair review of the action. I know that I can find out how to appeal by contacting the Marketplace at 1-800-XXX-XXXX. 
	  		I know that I can be represented in the process by someone other than myself. My eligibility and other information will be explained 
	  		to me. <a href="#">Find out more about how to appeal</a> </p>
	  		
	  		<p>Following federal law, discrimination isn't permitted on the basis of race, color, national origin, sex, age, sexual 
	  		orientation, gender identity or disability. I can file a complaint of discrimination by visiting <a href="#">www.hhs.gov/ocr/office/file</a> </p>
  		</div>


		
		<h5>Does anyone in the household want to register to vote? <em>(Optional)</em></h5>
			<div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" value="Yes" >
                  Yes </label>
                <label class="checkbox inline">
                   <input type="checkbox" value="No" checked="" >
                  No </label>
             </div>
  
  

<form class="form-vertical">
<div class="header">
	<h4>Tax Credit</h4>
</div>
<div class="gutter10">
<div class="calculator well">
<div class="control-group span2">
<div class="controls">
 <div id="slider" style="height: 170px; width: 50px;"></div>
    </div>
     </div>
    <div class="control-group span8">
	    <div class="controls ">
		    <label for="mtc">Monthly Tax Credit</label>
			    <input type="text" name="mtc" id="mtc" class="span8" value="333"> x 12 months
			    
		 </div>
	</div>
	<div class="control-group span8">
		<div class="controls">
			<label for="atc">Annual Tax Credit</label>
			    <input type="text" name="atc" placeholder="1,500" class="span8" id="atc">
		</div>
	</div>
	<div class="control-group span8">
		<div class="controls">
			<p class="btn btn-primary clear span8">Confirm</p>
		</div>
	</div>
<span class="clear"></span>
</div>
</div>
        </form>
        <div class="well well-small">
    <a class="btn btn-primary offset10"  href="http://ghixdemo.com/hix/plandisplay/welcome">Find a Plan</a>
    </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
<!--container-->
</div>
<!--main-->

</form>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<!-- <script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script> -->
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script src="../resources/eligibilitynm/js/jquery-ui-1.9.1.custom.js"></script>
<script>
function redirectPage(){
	window.location = "/hix/plandisplay/populateHousehold?aptc="+$('#mtc').val()+"&totaltc=1500";
}
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});


$( "#slider" ).slider({
						orientation: "vertical",
														range: "min",
														min: 0,
														max: 4000,
														value: 0,
														slide: function( event, ui ) {
															if(ui.value == 0){
																$( "#mtc" ).val(383);
																$( "#atc" ).val(0);
															}else if(ui.value == 4000){
																$( "#mtc" ).val(0);
																$( "#atc" ).val(4000);
															}
															else {
																$( "#mtc" ).val(383 - Math.round(ui.value/12));
																$( "#atc" ).val( ui.value );

															}

																													}
													});
													$( "#mtc" ).val( 383 );
													$( "#atc" ).val( $( "#slider" ).slider( "value" ) );

/*
$( "#slider" ).slider({
	orientation: "vertical",
            range: "min",
            min: 0,
            max: 1500,
            value: 0,
			rmtc:0,
            slide: function( event, ui ) {
               var atc = $( "#atc" ).val( ui.value );
			   var mtc = 125
				var mtc = (atc.val()/12);
				var rmtc = Math.round(mtc*100)/100;
				var rmtc = (rmtc*12)/12;
				console.log(rmtc);
				$("#mtc").val(rmtc);
            }
        });
        $( "#atc" ).val( $( "#slider" ).slider( "value" ) );
		     */

</script>

