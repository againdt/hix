package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrollmentReconciliationService;

public class EnrlReconNotificationTask  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconNotificationTask.class);
	private EnrollmentReconciliationService enrollmentReconciliationService;

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		LOGGER.info("EnrlReconNotificationTask.execute : START");
		
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		
		enrollmentReconciliationService.sendReconciliationNotification(jobId);
		
		LOGGER.info("EnrlReconNotificationTask.execute : END");
		
		return RepeatStatus.FINISHED;
	}

	public EnrollmentReconciliationService getEnrollmentReconciliationService() {
		return enrollmentReconciliationService;
	}

	public void setEnrollmentReconciliationService(EnrollmentReconciliationService enrollmentReconciliationService) {
		this.enrollmentReconciliationService = enrollmentReconciliationService;
	}
}
