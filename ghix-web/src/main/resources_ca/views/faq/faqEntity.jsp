<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
.table th,.table th a {
	background: #666;
	color: #fff
}

table th {
	height: 22px
}

/*edited css*/
#sidebar h4,#sidebar .header,.gray h4,.header {
	max-height: 10%;
}

h4 small.headerSmall {
	font-size: 12px;
	color: #666;
}

.grayBgTable {
	background-color: #F8F8F8;
}

.page-breadcrumb {
	display: inline;
}

h4.rule,.span9 .rule {
	border-bottom: 1px solid #999;
	padding-bottom: 14px;
}

.top30 {
	margin-top: 30px;
}
</style>



<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1><a name="skip"></a><spring:message code="label.FAQ.Enrollment" htmlEscape="false"/></h1>
		<!--
		<small><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>
		-->
	</div>
	<!--  end of row-fluid -->
	<div class="row-fluid">

		<div class="span3" id="sidebar">
			<div class="header">
				<h4>&nbsp;</h4>
			</div>
				<df:csrfToken/>
				<ul class="nav nav-list faq-nav" id="nav">
					<li><a href="#entity1"><spring:message code="label.FAQ.Enrollment.entity1" htmlEscape="false"/></a></li>
					<li><a href="#entity2"><spring:message code="label.FAQ.Enrollment.entity2" htmlEscape="false"/></a></li>
					<li><a href="#entity3"><spring:message code="label.FAQ.Enrollment.entity3" htmlEscape="false"/></a></li>
					<li><a href="#entity4"><spring:message code="label.FAQ.Enrollment.entity4" htmlEscape="false"/></a></li>
					<li><a href="#entity5"><spring:message code="label.FAQ.Enrollment.entity5" htmlEscape="false"/></a></li>
				</ul>
		</div>

		<div class="span9 " id="rightpanel">
			<div class="entityFaq1" id="entity1">
				<div class="header">
					<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Enrollment.entity1" htmlEscape="false"/></h4>
				</div>

				<div class="gutter10">
				<p><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step1" htmlEscape="false"/></p>

				<ul>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step2" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step3" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step4" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step5" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step6" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step7" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step8" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step9" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step10" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step11" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step12" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step13" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step14" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step15" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step16" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step17" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step18" htmlEscape="false"/></li>
				</ul>

				<p><spring:message code="label.FAQ.Enrollment.entity1.understandRole1.step19" htmlEscape="false"/></p>

				<h4><spring:message code="label.FAQ.Enrollment.entity2.learnMore" htmlEscape="false"/></h4>
				<div class="accordion" id="role-accordian">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian" href="#collapseOne">
								<spring:message code="label.FAQ.Enrollment.entity2.learnMore.1" htmlEscape="false"/></a>
						</div>
						<div id="collapseOne" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity2.learnMore.1.1" htmlEscape="false"/></p>

								<ul>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.1.2" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.1.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.1.4" htmlEscape="false"/></li>

								</ul>

								<p><spring:message code="label.FAQ.Enrollment.entity2.learnMore.1.5" htmlEscape="false"/></p>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian" href="#collapseTwo"> <spring:message code="label.FAQ.Enrollment.entity2.learnMore.2" htmlEscape="false"/> </a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.1" htmlEscape="false"/></p>

								<ul>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.2" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.4" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.6" htmlEscape="false"/></li>


								</ul>

								<p><spring:message code="label.FAQ.Enrollment.entity2.learnMore.2.7" htmlEscape="false"/></p>
							</div>
						</div>
					</div>

					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian" href="#collapseThree"><spring:message code="label.FAQ.Enrollment.entity2.learnMore.3" htmlEscape="false"/></a>
						</div>
						<div id="collapseThree" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity2.learnMore.3.1" htmlEscape="false"/>http://www.healthexchange.ca.gov/Pages/AssistersProgram.aspx.</p>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
			<!--end of .entityFaq1-->

			<div class="entityFaq2" id="entity2">
				<div class="header">
					<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Enrollment.entity2" htmlEscape="false"/><a id="role-top" href="#" class="pull-right"><spring:message code="label.FAQ.backToTop" htmlEscape="false"/></a></h4>
				</div>

				<div class="gutter10">
				<p><spring:message code="label.FAQ.Enrollment.entity2.step1" htmlEscape="false"/></p>

				<p><spring:message code="label.FAQ.Enrollment.entity2.step2" htmlEscape="false"/></p>

				<p><spring:message code="label.FAQ.Enrollment.entity2.step3" htmlEscape="false"/></p>
				<ol>

					<li><spring:message code="label.FAQ.Enrollment.entity2.step4" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step5" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step6" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step7" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step8" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step9" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step10" htmlEscape="false"/>
						<ol>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step10.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step10.2" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step10.3" htmlEscape="false"/></li>
						</ol>
					</li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step11" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step12" htmlEscape="false"/>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step13" htmlEscape="false"/>
						<ul>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step13.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step13.2" htmlEscape="false"/>
								<ol>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step13.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step13.4" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step13.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step13.6" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step13.7" htmlEscape="false"/></li>
								</ol>
							</li>
						</ul>
					</li>

					<li><spring:message code="label.FAQ.Enrollment.entity2.step14" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step15" htmlEscape="false"/>
						<ul>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step15.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity2.step15.2" htmlEscape="false"/>
								<ol>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step15.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step15.4" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step15.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step15.6" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity2.step15.7" htmlEscape="false"/></li>
								</ol>
							</li>
						</ul>
					</li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step16" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step17" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity2.step18" htmlEscape="false"/></li>
				</ol>
				</div>
			</div>
			<!--/.entityFaq2-->

			<div class="entityFaq3" id="entity3">

				<div class="header">
					<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Enrollment.entity3" htmlEscape="false"/><a id="role-top" href="#" class="pull-right"><spring:message code="label.FAQ.backToTop" htmlEscape="false"/></a></h4>
				</div>

				<div class="gutter10">
				<p><spring:message code="label.FAQ.Enrollment.entity3.step1" htmlEscape="false"/></p>

				<p><spring:message code="label.FAQ.Enrollment.entity3.step2" htmlEscape="false"/></p>

				<ul>

					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.1" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.2" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.3" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.4" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.5" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step2.6" htmlEscape="false"/></li>

				</ul>

				<p><spring:message code="label.FAQ.Enrollment.entity3.step3" htmlEscape="false"/></p>

				<ol>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step3.1" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step3.2" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity3.step3.3" htmlEscape="false"/></li>
				</ol>
				</div>
			</div>
			<!--/.entityFaq3-->


			<div class="entityFaq4" id="entity4">
				<div class="header">
				<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Enrollment.entity4.1" htmlEscape="false"/><a id="role-top" href="#" class="pull-right"><spring:message code="label.FAQ.backToTop" htmlEscape="false"/></a></h4>
				</div>

				<div class="gutter10">
				<p><spring:message code="label.FAQ.Enrollment.entity4.1.1" htmlEscape="false"/></p>


				<div class="accordion" id="role-accordian2">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsOne"><spring:message code="label.FAQ.Enrollment.entity4.2" htmlEscape="false"/></a>
						</div>
						<div id="counselorsOne" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity4.2.1" htmlEscape="false"/></p>

								<ul>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.2" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.3" htmlEscape="false"/></li>

								</ul>

								<p><spring:message code="label.FAQ.Enrollment.entity4.2.4" htmlEscape="false"/></p>
								<ol>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.6" htmlEscape="false"/>
										<ol>
											<li><spring:message code="label.FAQ.Enrollment.entity4.2.7" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Enrollment.entity4.2.8" htmlEscape="false"/></li>
											<li><spring:message code="label.FAQ.Enrollment.entity4.2.9" htmlEscape="false"/></li>
										</ol>
									</li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.10" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.11" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.2.12" htmlEscape="false"/></li>
								</ol>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsTwo"> <spring:message code="label.FAQ.Enrollment.entity4.3" htmlEscape="false"/> </a>
						</div>
						<div id="counselorsTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity4.3.1" htmlEscape="false"/></p>
							</div>
						</div>
					</div>

					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsThree"> <spring:message code="label.FAQ.Enrollment.entity4.4" htmlEscape="false"/> </a>
						</div>
						<div id="counselorsThree" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity4.4.1" htmlEscape="false"/></p>

								<p><spring:message code="label.FAQ.Enrollment.entity4.4.2" htmlEscape="false"/></p>

								<ol>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.4" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.6" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.7" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.8" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.9" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.4.10" htmlEscape="false"/></li>
								</ol>
							</div>
						</div>
					</div>


					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsFour"> <spring:message code="label.FAQ.Enrollment.entity4.5" htmlEscape="false"/></a>
						</div>
						<div id="counselorsFour" class="accordion-body collapse">
							<div class="accordion-inner">
								<h4><spring:message code="label.FAQ.Enrollment.entity4.5.1" htmlEscape="false"/></h4>

								<p><spring:message code="label.FAQ.Enrollment.entity4.5.2" htmlEscape="false"/></p>

								<p><spring:message code="label.FAQ.Enrollment.entity4.5.3" htmlEscape="false"/></p>

								<p><spring:message code="label.FAQ.Enrollment.entity4.5.4" htmlEscape="false"/></p>

								<ul>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.6" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.7" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.8" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.9" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.10" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.11" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.12" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.13" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.14" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.15" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.16" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.17" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.18" htmlEscape="false"/></li>
								</ul>


								<h4><spring:message code="label.FAQ.Enrollment.entity4.5" htmlEscape="false"/></h4>

								<p><spring:message code="label.FAQ.Enrollment.entity4.5.19" htmlEscape="false"/></p>

								<p><spring:message code="label.FAQ.Enrollment.entity4.5.20" htmlEscape="false"/></p>

								<ol>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.21" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.22" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.23" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.24" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.25" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.26" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.27" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.5.28" htmlEscape="false"/></li>
								</ol>
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsFive"> <spring:message code="label.FAQ.Enrollment.entity4.6" htmlEscape="false"/> </a>
						</div>
						<div id="counselorsFive" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity4.6.1" htmlEscape="false"/></p>

								<p><spring:message code="label.FAQ.Enrollment.entity4.6.2" htmlEscape="false"/></p>

								<ol>

									<li><spring:message code="label.FAQ.Enrollment.entity4.6.3" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.4" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.5" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.6" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.7" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.8" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.9" htmlEscape="false"/></li>
									<li><spring:message code="label.FAQ.Enrollment.entity4.6.10" htmlEscape="false"/></li>
								</ol>
								</ol>
							</div>
						</div>
					</div>

					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse"
								data-parent="#role-accordian2" href="#counselorsSix"><spring:message code="label.FAQ.Enrollment.entity4.7" htmlEscape="false"/></a>
						</div>
						<div id="counselorsSix" class="accordion-body collapse">
							<div class="accordion-inner">
								<p><spring:message code="label.FAQ.Enrollment.entity4.7.1" htmlEscape="false"/>
									http://www.healthexchange.ca.gov/Pages/AssistersProgram.aspx.</p>
							</div>
						</div>
					</div>
				</div>

				</div>
			</div>
			<!--/.entityFaq4-->

			<div class="entityFaq5" id="entity5">
				<div class="header">
					<h4 class="rule" id="role-top"><spring:message code="label.FAQ.Enrollment.entity5" htmlEscape="false"/> <a id="role-top" href="#" class="pull-right"><spring:message code="label.FAQ.backToTop" htmlEscape="false"/></a></h4>
				</div>

				<div class="gutter10">
				<p><spring:message code="label.FAQ.Enrollment.entity5.1" htmlEscape="false"/></p>

				<p><spring:message code="label.FAQ.Enrollment.entity5.2" htmlEscape="false"/></p>

				<ol>
					<li><spring:message code="label.FAQ.Enrollment.entity5.3" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity5.4" htmlEscape="false"/></li>
					<li><spring:message code="label.FAQ.Enrollment.entity5.5" htmlEscape="false"/>
						<ol>
							<li><spring:message code="label.FAQ.Enrollment.entity5.5.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity5.5.2" htmlEscape="false"/></li>
						</ol>
					</li>
					<li><spring:message code="label.FAQ.Enrollment.entity5.6" htmlEscape="false"/>
						<ol>
							<li><spring:message code="label.FAQ.Enrollment.entity5.6.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity5.6.2" htmlEscape="false"/></li>
						</ol>
					</li>
					<li><spring:message code="label.FAQ.Enrollment.entity5.7" htmlEscape="false"/>
						<ol>
							<li><spring:message code="label.FAQ.Enrollment.entity5.7.1" htmlEscape="false"/></li>
							<li><spring:message code="label.FAQ.Enrollment.entity5.7.2" htmlEscape="false"/></li>
						</ol>
					</li>
				</ol>
				</div>
			</div>


			<a id="role-top" href="#" class="pull-right"><spring:message code="label.FAQ.backToTop" htmlEscape="false"/></a>


		</div>



	</div>
	<!--  end of row-fluid -->
</div>
<!--  end of gutter10 -->


<script>
$(document).ready(function(){
	document.title="Covered California | Frequently Asked Questions";

	 $('a[href*=#]:not([href=#]):not([class=accordion-toggle])').click(function() {
		    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
		        || location.hostname == this.hostname) {

		        var target = $(this.hash);
		        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		           if (target.length) {
		             $('html,body').animate({
		                 scrollTop: target.offset().top-80
		            }, 1000);
		            return false;
		        }
		    }
		});
		$('.collapse').collapse({
			toggle: false
		});

});
</script>
