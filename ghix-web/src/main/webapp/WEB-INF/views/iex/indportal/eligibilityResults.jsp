<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<script>
  $(document).ready(function() {
		var scope = angular.element(document.getElementById("eligibilityResults")).scope();
		if (scope) {
			scope.$apply(function() {
				scope['eligibilityResults'] = {title: '<spring:message code="indportal.eligibilitysummary.myeligibility" javaScriptEscape="true"/>'};
			});
		}
	});
</script>
<script type="text/ng-template" id="eligibilityResults">
        <div class="header margin20-b eligibility-header">
          <h4><spring:message code="indportal.eligibilitysummary.myeligibility" javaScriptEscape="true"/></h4>
      </div>
      <div class="gutter10" ng-init="showEligibilityDetails()">
			  <p><spring:message code="indportal.eligibilitysummary.congratulations" javaScriptEscape="true"/></p>

<!-- Household-->
        <div class="gutter20" style="background: #EDEDED">
          <div class="row-fluid" style="background: #FFF;">
            <div class="well eligibility-well center span5" style="padding-left: 25px;">
              <img src="<gi:cdnurl value="/resources/img/fam.png" />" alt="Household icon" width="100px" height="100px" class="house-hold-logo margin20-b margin30-t"/>
              <p style="font-size: 18px;" class="margin5-t"><strong><spring:message code="indportal.eligibilitysummary.yourhousehold" javaScriptEscape="true"/></strong></p>
              <p ng-if="stateCode !== 'MN'" style="font-size: 14px"><spring:message code="indportal.eligibilitysummary.eligibilitystatus" javaScriptEscape="true"/>: <strong style="color:#3177A1;">{{householdEligibilityDetails.eligibilityStatus}}</strong></p>
            </div>

          <div class="span7">
            <div class="margin10-t gutter10">
              <div class="">
              <p style="color:#3177A1; font-size: 17px" ng-show="stateCode !== 'MN'" && householdEligibilityDetails.eligibleProgram ==='Qualified Health Plan (QHP)'"> <strong><spring:message code="indportal.eligibilitysummary.bothqhpandqdp" javaScriptEscape="true"/></strong></p>
              <p style="color:#3177A1; font-size: 17px" ng-show="stateCode === 'MN'" && householdEligibilityDetails.eligibleProgram ==='Qualified Health Plan (QHP)'"> <strong><spring:message code="indportal.eligibilitysummary.bothqhpqdp" javaScriptEscape="true"/></strong></p>
              <div ng-show="householdEligibilityDetails.eligibleProgram ==='Qualified Health Plan (QHP)'">
				<!--<span> IMPORTANT: YOUR ACTION REQUIRED </span>-->
				<p ng-show="householdEligibilityDetails.eligibilityStatus === 'Eligible'"><spring:message code="indportal.eligibilitysummary.eligibilityStatusMsg" javaScriptEscape="true"/> <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME)%>.</p>
				<br>

				<p ng-show="stateCode === 'NV' && householdEligibilityDetails.medicaidEligible"><strong style="color:#3177A1; font-size: 17px"><spring:message code="indportal.eligibilitysummary.medicaidchp" javaScriptEscape="true"/><br></strong> <spring:message code="indportal.eligibilitysummary.medicaideligibletext" javaScriptEscape="true"/></p>
                <p ng-show="stateCode === 'MN' && householdEligibilityDetails.aptcEligible"><strong style="color:#3177A1; font-size: 17px"><spring:message code="indportal.eligibilitysummary.premuimtaxcredit" javaScriptEscape="true"/><br></strong> <spring:message code="indportal.eligibilitysummary.aptcMsg1" javaScriptEscape="true"/> {{householdEligibilityDetails.aptc | currency}}. <spring:message code="indportal.eligibilitysummary.aptcMsg2" javaScriptEscape="true"/> </p>
                <p ng-show="stateCode !== 'MN' && householdEligibilityDetails.aptcEligible"><strong style="color:#3177A1; font-size: 17px"><spring:message code="indportal.eligibilitysummary.premuimtaxcredit" javaScriptEscape="true"/><br></strong> <spring:message code="indportal.eligibilitysummary.aptcMsg1" javaScriptEscape="true"/> {{householdEligibilityDetails.aptc | currency}}. <spring:message code="indportal.eligibilitysummary.aptcMsg2" javaScriptEscape="true"/> {{householdEligibilityDetails.aptc*12 | currency}} <spring:message code="indportal.eligibilitysummary.aptcMsg3" javaScriptEscape="true"/> </p>
                <p ng-show="householdEligibilityDetails.csrEligible"><strong style="color:#3177A1; font-size: 17px"><spring:message code="indportal.eligibilitysummary.costsharingreduction" javaScriptEscape="true"/><br></strong> <spring:message code="indportal.eligibilitysummary.costsharingreductionMsg" javaScriptEscape="true"/></p>

				<div ng-show="householdEligibilityDetails.eligibilityStatus === 'Conditional'"><strong style="color:#3177A1; font-size: 17px"><spring:message code="indportal.eligibilitysummary.conditionalEliWarning" javaScriptEscape="true"/> <br></strong>
				<spring:message code="indportal.eligibilitysummary.conditionalEliMsg1" javaScriptEscape="true"/>
				<br/>
				<spring:message code="indportal.eligibilitysummary.conditionalEliMsg2" javaScriptEscape="true"/></div>


				</div>
				<div ng-show="householdEligibilityDetails.eligibilityStatus === 'Not Eligible'">
				<spring:message code="indportal.eligibilitysummary.noteligibileMsg"/>
				</div>
            </div>
          </div>

          </div>
          <div class="row-fluid" style="border-bottom: 1px solid #3177A1;">
            <p class="eligibility-results_next-steps pull-right margin20-r margin10-b" ng-show="householdEligibilityDetails.eligibilityStatus !== 'Not Eligible'">
				<spring:message code="indportal.eligibilitysummary.instructions" javaScriptEscape="true"/>&nbsp;
				<a href="javascript:void(0)" ng-click="nextSteps = !nextSteps"><spring:message code="indportal.eligibilitysummary.whatnext" javaScriptEscape="true"/></a>
			</p>
          </div>
          <div class="row-fluid" style="background: #FFF" ng-show="nextSteps">
            <div class="gutter20">
              <p><spring:message code="indportal.eligibilitysummary.afterenrollMsg1" javaScriptEscape="true"/> <%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME)%>.&nbsp;<spring:message code="indportal.eligibilitysummary.afterenrollMsg2" javaScriptEscape="true"/></p>
				<p><spring:message code="indportal.eligibilitysummary.afterenrollMsg3" javaScriptEscape="true"/> </p>
				<!--<p><spring:message code="indportal.eligibilitysummary.openenrollMsg4" javaScriptEscape="true"/> </p>-->



			  <!-- <p>
				<spring:message code="indportal.eligibilitysummary.openenrollMsg5" javaScriptEscape="true"/> <%=new SimpleDateFormat("MMMMMMMMMMMM dd, yyyy").format(new SimpleDateFormat("MM/dd/yyyy").parse(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_END_DATE)))%>.
              	&nbsp;<spring:message code="indportal.eligibilitysummary.openenrollMsg6" javaScriptEscape="true"/>
			  </p> -->
            </div>
          </div>

<!--Member-->
          <div class="applicationState" ng-repeat="member in memberEligibilityDetails"  ng-if="memberEligibilityDetails.length>0">
              <div class="margin30-t gutter10" style="background: #FFF;">
                <div class="row-fluid">

                  <div class="gutter5" style="border-bottom: 1px solid #3177A1;">
                      <p style="font-size: 17px; color:#666;"><i class="icon-user"></i> <strong>{{member.firstName}} {{member.middleName}} {{member.lastName}} {{member.suffix}}</strong></p>
                  </div>

                  <div class="margin20-t margin20-l" style="border-bottom: 0px solid #3177A1;height: 25px;">
                    <div class="span6">
            		<div class="applicationState" ng-repeat="eligibilities in member.memberEligibilities"  ng-if="member.memberEligibilities.length>0">
                      <p style="font-size: 14px"><i class="icon-list-alt"></i> <spring:message code="indportal.portalhome.program" javaScriptEscape="true"/>: <strong style="color:#3177A1;font-weight:bold">{{eligibilities.eligibilityStatus}}</strong></p>
                    </div>
                    <div class="applicationState" ng-if="member.memberEligibilities.length==0">
                      <p style="font-size: 14px"><i class="icon-list-alt"></i> <strong style="color:#3177A1;font-weight:bold"><spring:message code="indportal.eligibilitysummary.noteligibileMember" javaScriptEscape="true"/></strong></p>
                    </div>
                    </div>
                  <div class="margin20-t margin20-l" style="border-bottom: 0px solid #3177A1;height: 25px;" ng-show="member.memberEligibilities.length>0">
                    <div class="span6">
                    	<p ng-if="stateCode !== 'MN'" style="font-size: 14px"><i class="icon-check-sign"></i> <spring:message code="indportal.eligibilitysummary.eligibilitystatus" javaScriptEscape="true"/>:
												<strong style="color:#3177A1;">
													<span ng-show="member.eligibilityStatus">
														<spring:message code="indportal.eligibilitysummary.eligible" javaScriptEscape="true"/>
													</span>
													<span ng-show="!member.eligibilityStatus">
														<spring:message code="indportal.eligibilitysummary.conditionaleligible" javaScriptEscape="true"/>
													</span>
												</strong>
										  </p>
                    </div>
                  </div>

                  <!--<div class="gutter10">
                    What Next?
                    <div ng-show="eligibilities.eligibilityStatus=='QHP'">
                      <p>Open enrollment for <!--VariableYour Health Idaho<!--Variable ends on March 31, so you ust enroll in a plan and pay the first month's bill (the "premium") by then. To make a plan selection, select Enroll and compare.</p>
                      <p>If you miss the deadline, your may not be able to enroll.</p>
                    </div>
                  </div>-->

              </div>
            </div>
          </div>

        </div>
      </div>

      <div class="form-actions form-horisontal">
        <a href="#/" class="btn pull-left"><spring:message code="indportal.eligibilitysummary.gobacktobashboard" javaScriptEscape="true"/></a>
<!--         <sec:accesscontrollist hasPermission="IND_PORTAL_CONTACT_US" domainObject="user"> -->
<!-- 		<a href="#/contactus" class="btn btn-primary pull-right"><spring:message code="indportal.portalhome.contactus" javaScriptEscape="true"/></a> -->
<!-- 		</sec:accesscontrollist>           -->
         <!--<sec:accesscontrollist hasPermission="IND_PORTAL_APPEALS" domainObject="user">
		 <a href="#/submitappeal"ng-show="{{householdEligibilityDetails.enableAppealsSubmission}}==true" role="button" class="btn btn-primary pull-right" data-toggle="modal"><spring:message code="indportal.eligibilitysummary.submitanappeal" javaScriptEscape="true"/></a>
		</sec:accesscontrollist>     -->
      </div>

<!-- Submit an Appeal Modal -->
<div id="submitAppealModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="submitAppealModal" aria-hidden="true">
  <form class="form-horizontal">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
      <h3 id="submitAppealModal"><spring:message code="indportal.eligibilitysummary.submitanappeal" javaScriptEscape="true"/></h3>
    </div>
    <div class="modal-body">
      <p><spring:message code="indportal.eligibilitysummary.ifyourdontagree" javaScriptEscape="true"/></p>
      <p><spring:message code="indportal.eligibilitysummary.youhave60days" javaScriptEscape="true"/></p>
      <p><spring:message code="indportal.eligibilitysummary.youmayreceiveanotification" javaScriptEscape="true"/></p>
      <p><spring:message code="indportal.eligibilitysummary.ingeneralwe" javaScriptEscape="true"/></p>
      <p><strong><spring:message code="indportal.eligibilitysummary.step1" javaScriptEscape="true"/></strong> <a href="javascript:void(0)" onclick="openInNewTab('https://www.yourhealthidaho.org/wp-content/uploads/2014/10/Your-Health-Idaho-Appeal-Request-Form_rev.10092014.pdf')"><spring:message code="indportal.eligibilitysummary.downloadyourappealform" javaScriptEscape="true"/></a></p>
      <p><strong><spring:message code="indportal.eligibilitysummary.step2" javaScriptEscape="true"/></strong> <spring:message code="indportal.eligibilitysummary.pleasefillintheappeal" javaScriptEscape="true"/>
        <div class="control-group">
          <label class="control-label" for="reason"><spring:message code="indportal.eligibilitysummary.typeofindividualappeal" javaScriptEscape="true"/>:</label>
          <div class="controls">
            <label class="checkbox">
                <input type="checkbox" name="reason" id="appealAptcCsrDenidedTerminatedIncorrectly" value="appealAptcCsrDenidedTerminatedIncorrectly">
                <spring:message code="indportal.eligibilitysummary.aptc" javaScriptEscape="true"/>
            </label>
            <label class="checkbox">
                <input type="checkbox" name="reason" id="appealAptcCstAmountIncorrect" value="appealAptcCstAmountIncorrect">
                <spring:message code="indportal.eligibilitysummary.aptcorcsramountisincorrent" javaScriptEscape="true"/>
            </label>
            <label class="checkbox">
                <input type="checkbox" name="reason" id="appealMedicalCHIPDeniedIncorrectly" value="appealMedicalCHIPDeniedIncorrectly">
                <spring:message code="indportal.eligibilitysummary.medicaidorchipdenied" javaScriptEscape="true"/>
            </label>
              <label class="checkbox">
                <input type="checkbox" name="reason" id="appealWrongPeople" value="appealWrongPeople">
                <spring:message code="indportal.eligibilitysummary.peopleincludedinmyhouseholdarewrong" javaScriptEscape="true"/>
            </label>
            <label class="checkbox">
                <input type="checkbox" name="reason" id="appealOtherReason" value="appealOtherReason">
                <spring:message code="indportal.eligibilitysummary.other" javaScriptEscape="true"/>
            </label>
            <input type="text" name="appealOtherReasonExplanation" id="appealOtherReasonExplanation"  placeholder="Please explain" class="hide">
          </div>
        </div>
      </p>
      <p><strong><spring:message code="indportal.eligibilitysummary.step3" javaScriptEscape="true"/></strong> <spring:message code="indportal.eligibilitysummary.uploadyourappealform" javaScriptEscape="true"/>
        <div class="control-group">
          <label class="control-label" for="fileUpload"><spring:message code="indportal.eligibilitysummary.uploadform" javaScriptEscape="true"/>:</label>
          <div class="controls">
            <input type="file" class="custom-file-input"  multiple="" id="fileUpload" name="fileUpload">
          </div>
        </div>
      </p>
    </div>
    <div class="modal-footer">
      <button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
      <button class="btn btn-primary" type="submit"><spring:message code="indportal.eligibilitysummary.submitanappeal" javaScriptEscape="true"/></button>
    </div>
  </form>
</div>
<!-- Submit an Appeal Modal Ends -->

</script>