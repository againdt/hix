package com.getinsured.hix.indportal.dto;

/*
 * DTO used for to update enrollment status
 * @author Sunil Sahoo
 */
public class CoveredFamilyMember {

	private String exchgIndivIdentifier;
	private String name;
	private String issuerAssignedMemberId;
	private String startDate;
	private String endDate;
	private String relationship;
	
	
	public String getExchgIndivIdentifier() {
		return exchgIndivIdentifier;
	}
	public void setExchgIndivIdentifier(String exchgIndivIdentifier) {
		this.exchgIndivIdentifier = exchgIndivIdentifier;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIssuerAssignedMemberId() {
		return issuerAssignedMemberId;
	}
	public void setIssuerAssignedMemberId(String issuerAssignedMemberId) {
		this.issuerAssignedMemberId = issuerAssignedMemberId;
	}
	public String getStartDate() {
		return startDate;
	}
	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}
	public String getEndDate() {
		return endDate;
	}
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}
	public String getRelationship() {
		return relationship;
	}
	public void setRelationship(String relationship) {
		this.relationship = relationship;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CoveredFamilyMember [exchgIndivIdentifier=");
		builder.append(exchgIndivIdentifier);
		builder.append(", name=");
		builder.append(name);
		builder.append(", issuerAssignedMemberId=");
		builder.append(issuerAssignedMemberId);
		builder.append(", startDate=");
		builder.append(startDate);
		builder.append(", endDate=");
		builder.append(endDate);
		builder.append(", relationship=");
		builder.append(relationship);
		builder.append("]");
		return builder.toString();
	}
	
}
