//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for cost
 * 				sharing reduction (CSR) eligibility determination outcome.
 * 			
 * 
 * <p>Java class for CSREligibilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CSREligibilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-ee}EligibilityType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}CSRAdvancePayment" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CSREligibilityType", propOrder = {
    "csrAdvancePayment"
})
public class CSREligibilityType
    extends EligibilityType
{

    @XmlElement(name = "CSRAdvancePayment")
    protected CSRAdvancePaymentType csrAdvancePayment;

    /**
     * Gets the value of the csrAdvancePayment property.
     * 
     * @return
     *     possible object is
     *     {@link CSRAdvancePaymentType }
     *     
     */
    public CSRAdvancePaymentType getCSRAdvancePayment() {
        return csrAdvancePayment;
    }

    /**
     * Sets the value of the csrAdvancePayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link CSRAdvancePaymentType }
     *     
     */
    public void setCSRAdvancePayment(CSRAdvancePaymentType value) {
        this.csrAdvancePayment = value;
    }

}
