package com.getinsured.iex.hub.vlp;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants.VLP_DHS_ID_TYPES;
import com.getinsured.iex.hub.vlp.wrapper.Agency3InitVerifRequestTypeWrapper;


/**
 * This class is a test class for VLP services
 * 
 * HIX-26932
 * 
 * @author Nikhil Talreja
 * @since 14-Jan-2014
 *
 */
public final class VerifyLawfulPrescenceRequest{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyLawfulPrescenceRequest.class);
	
	private VerifyLawfulPrescenceRequest(){
		
	}
	
	/**
	 * Method to create VLP request
	 * 
	 * @author - Nikhil Talreja
	 * @throws JAXBException 
	 * @throws BridgeException 
	 * @throws HubMappingException 
	 * @throws HubServiceException 
	 * @throws HubException 
	 * @since - 13-Feb-2014
	 * 
	 */
	public static void createVLPRequest() {
		
		try{
			HubServiceBridge vlpServiceBridge = HubServiceBridge.getHubServiceBridge("VerifyLawfulPresence");
			JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp");
			vlpServiceBridge.getServiceHandler().setJsonInput("{\"DHSID\":{\"I571DocumentID\":{\"AlienNumber\":\"992222334\",\"DocExpirationDate\":\"2012-01-29\"}},\"CategoryCode\":\"C40\",\"Comments\":\"Comment\",\"RequestedCoverageStartDate\":\"2014-01-03\",\"I94Number\":null,\"LastName\":\"Griffis\",\"RequestSponsorDataIndicator\":false,\"DateOfBirth\":\"Mar 03, 1979 10:00:00 PM\",\"MiddleName\":null,\"FiveYearBarApplicabilityIndicator\":true,\"AKA\":\"J\",\"RequestGrantDateIndicator\":true,\"FirstName\":\"Jeff\",\"RequesterCommentsForHub\":\"Initial verification request\"}");
			Map<String, Object> context = new HashMap<String, Object>();
			context.put("objectIdentifier",VLP_DHS_ID_TYPES.I571.toString());
			vlpServiceBridge.getServiceHandler().setContext(context);
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new VLPNamespacePrefixMapper());
			//m.marshal(vlpServiceBridge.getServiceHandler().getRequestpayLoad(), System.out);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}
	
	public static void testWrappers() throws HubServiceException{
		
		Agency3InitVerifRequestTypeWrapper tempI551Request = Agency3InitVerifRequestTypeWrapper.fromJsonString("{\"dhsid\" : {\"tempI551DocumentID\":{\"alienNumber\":\"123456789\",\"docExpirationDate\":\"2020-05-20\"}},\"firstName\":\"Nikhil\"}");
		LOGGER.debug("Incomplete Temp I551 Request " + tempI551Request.toJSONString());
		Agency3InitVerifRequestTypeWrapper i327Request = Agency3InitVerifRequestTypeWrapper.fromJsonString("{\"dhsid\" : {\"i327DocumentID\":{\"alienNumber\":\"123456789\",\"docExpirationDate\":\"2020-05-20\"}	},\"firstName\":\"Nikhil\"}");
		LOGGER.debug("I327 Request " + i327Request.toJSONString());
		Agency3InitVerifRequestTypeWrapper completeTempI551Request = Agency3InitVerifRequestTypeWrapper.fromJsonString("{\"dhsid\" : {\"tempI551DocumentID\":{\"alienNumber\":\"123456789\",\"docExpirationDate\":\"2020-05-20\",\"passportCountry\":{\"passportNumber\":\"J12345\",\"countryOfIssuance\":\"IND\"}}},\"firstName\":\"Nikhil\"}");
		LOGGER.debug("Complete Temp I551 Request " + completeTempI551Request.toJSONString());
		
	}
	
	/**
	 * Main method to test the class
	 * 
	 * @author - Nikhil Talreja
	 * @throws HubServiceException 
	 * @throws BridgeException 
	 * @throws HubMappingException 
	 * @throws JAXBException 
	 * @throws HubException 
	 * @since - 14-Jan-2014
	 * 
	 */
	public static void main(String[] args) {
		createVLPRequest();
	}
}
