(function(){
	'use strict';
	
	angular.module("verificationResultDashboardApp", ['spring-security-csrf-token-interceptor', 'datepickerComponent', 'popoverDirective']);
})();
