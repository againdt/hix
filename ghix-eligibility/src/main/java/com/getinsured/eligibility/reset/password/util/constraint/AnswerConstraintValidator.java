package com.getinsured.eligibility.reset.password.util.constraint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.reset.password.dto.Question;
import com.getinsured.eligibility.reset.password.dto.Question.DataType;

@Component
public class AnswerConstraintValidator implements ConstraintValidator<QuestionAnswerType, Question> {

	private boolean isValidDate(String dateValue) {
		SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("MM/dd/yyyy");
        Date date = null;
        try {
            date = DATE_FORMATTER.parse(dateValue);
            if (!dateValue.equals(DATE_FORMATTER.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            return false;
        }
        return date != null;
    }

	@Override
	public void initialize(QuestionAnswerType constraintAnnotation) {
	}

	@Override
	public boolean isValid(Question question, ConstraintValidatorContext context) {

		boolean isValid = true;
		if (question != null && question.getAnswer() != null){
			if (question.getType() == DataType.INT){
				
				if (!StringUtils.isNumeric(question.getAnswer())){
					isValid = false;
				}
			} else if (question.getType() == DataType.DOB){
				if (!isValidDate(question.getAnswer())){
					isValid = false;
				}
			}
		}
		return isValid;
	}




}
