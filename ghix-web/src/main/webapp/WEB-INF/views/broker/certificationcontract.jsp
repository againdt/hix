<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	<div class="gutter10">
	<div class="row-fluid">
		<h1><a name="skip"></a>Agent Contract with State Health Benefit Exchange.</h1>
	</div>
	<div class="row-fluid">
	    <div class="span3" id="sidebar">
		    <div class="header">
		    	<h4>Agent Home</h4>
		    </div>
		    <!--  beginning of side bar -->
		        <ul class="nav nav-list">
		            <li class="active"><a href="../broker/viewprofile">Profile</a></li>
		            <li><a href="../broker/certificationstatus">Certification Status</a></li>
		        </ul>
		    <!-- end of side bar -->
	    </div><!-- end of span3 -->
	    <div class="span9" id="rightpanel">
	            
	        <p class="gutter10">To be certified by State Health Benefit Exchange, please read and accept the following terms and conditions.</p>
	
	        <form class="form-vertical" id="frmbrokercontract" name="frmbrokercontract" action="certificationcontract" method="POST">
	            <div class="gutter10">
	                <p style="border: 5px solid red;">Terms and conditions(verbiage needed)</p>
	                 <div class="clearfix">
	                        <label class="checkbox"></label><!--end of label -->
	                        <input type="checkbox" value="true" name="optionsCheckbox" id = "optionsCheckbox"> I attest that I have read all the information and I agree to the terms and conditions stated above.
	                           <div id="optionsCheckbox_error"></div>
	                       </div>    <!-- end of control-group -->
	
	                <div class="clearfix">
	                    <label for="esignature">Your First and Last Name</label>
	                    <div class="input">
	                        <input type="text" name="esignature" id="esignature" value="" class="xlarge" size="30">
	                        <div id="esignature_error"></div>
	                    </div>
	                </div>
	
	                <div class="control-group">
	                    <div class="control-label"><b>Date:</b></div>
	                    <div class="control-value">${Date}</div>
	                </div>
	
	                <div class ="actions">
	                    <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-primary" title="Submit"/> 
	                </div><!-- end of form-actions -->
	            </div>
	        </form>
	    </div><!-- end of span9 -->
	</div><!--  end of row-fluid -->


