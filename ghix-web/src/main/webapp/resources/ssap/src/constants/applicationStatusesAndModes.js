

const ELIGIBILITY_RECEIVED = 'ELIGIBILITY_RECEIVED';
const OPEN ='OPEN';
const UNCLAIMED = 'UNCLAIMED';

export const APPLICATION_STATUSES = {
    ELIGIBILITY_RECEIVED,
    OPEN,
    UNCLAIMED
};


const VIEW = 'view';
const EDIT = 'edit';

export const USER_MODES = {
    EDIT,
    VIEW
};