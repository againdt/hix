package com.getinsured.hix.planmgmt.provider.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Specialty;

public interface ISpecialtyRepository extends JpaRepository<Specialty,Integer> {

	@Query("SELECT count(*) FROM Specialty s where LOWER(s.groupName)=LOWER(:groupName) AND s.parentSpecialtyId=:specialtyId order by s.groupName")
	Long getCountOfSpecialty(@Param("groupName") String groupName, @Param("specialtyId") int specialtyId);
	
	@Query("SELECT s FROM Specialty s where LOWER(s.name) = LOWER(:name) AND LOWER(s.groupName) = LOWER(:groupName) order by s.groupName")
	Specialty getSpecialtyByNameGroupName(@Param("name") String name,@Param("groupName") String groupName);
	
	@Query("SELECT s.id,s.name,s.parentSpecialtyId FROM Specialty s where LOWER(s.groupName) = LOWER(:groupName) AND s.parentSpecialtyId=0 order by s.groupName")
	List<Object[]> getParentSpecialtiesByGroupName(@Param("groupName") String groupName);
	
	@Query("SELECT s.id, s.name FROM Specialty s where  LOWER(s.groupName) = LOWER(:groupName) AND s.parentSpecialtyId=:parentSpecialtyId order by s.groupName")
	List<Object[]> getChildSpecialtiesByParentId(@Param("groupName") String groupName, @Param("parentSpecialtyId") int parentSpecialtyId);
	
	@Query("SELECT s FROM Specialty s where LOWER(s.code) = LOWER(:specialtyCode)")
	Specialty getSpecialtyByCode(@Param("specialtyCode") String specialtyCode);
}
