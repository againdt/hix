package com.getinsured.iex.ssap.repository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.springframework.data.repository.query.Param;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.iex.ssap.model.SsapApplication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SsapApplicationRepositoryImpl implements CustomSsapApplicationRepository {
	private static final Logger logger = LoggerFactory.getLogger(SsapApplicationRepositoryImpl.class);
	
	@PersistenceUnit
	private EntityManagerFactory emf;

	@Override
	public List<SsapApplication> findByCmrHouseoldIdAndCoverageYearNoAppData(@Param("cmrHouseoldId") BigDecimal cmrHouseoldId, @Param("coverageYear") long coverageYear) {
		
		EntityManager entityManager = null;
		
		try
		{
			entityManager = emf.createEntityManager();
			
			Query query=entityManager.createNativeQuery("select ID,CREATION_TIMESTAMP, LAST_UPDATE_TIMESTAMP,SOURCE,START_DATE,"+
														"       CASE_NUMBER,ESIGN_FIRST_NAME,ESIGN_MIDDLE_NAME,ESIGN_LAST_NAME,ESIGN_DATE,"+
														"       APPLICATION_STATUS , APPLICATION_TYPE, SSAP_APPLICATION_SECTION_ID , "+
														"       CMR_HOUSEOLD_ID , CREATED_BY , LAST_NOTICE_ID, ELIGIBILITY_STATUS ,"+
														"       ENROLLMENT_STATUS, FINANCIAL_ASSISTANCE_FLAG , CURRENT_PAGE_ID , EXCHANGE_ELIGIBILITY_STATUS ,"+
														"       CSR_LEVEL , MAXIMUM_APTC , ELECTED_APTC , ENABLE_ENROLLMENT , COVERAGE_YEAR , STATE , ZIPCODE ,"+
														"       COUNTY_CODE , EXTERNAL_APPLICATION_ID  , PARENT_SSAP_APPLICATION_ID ,"+
														"       AVAILABLE_APTC ,   EXCHANGE_TYPE  , LAST_UPDATED_BY , ELIGIBILITY_RECEIVED_DATE ,"+
														"       ELIGIBILITY_RESPONSE_TYPE , NATIVE_AMERICAN_FLAG ,  EHB_AMOUNT , "+
														"       EXEMPT_HOUSEHOLD   ,"+
														"      RENEWAL_STATUS , SEP_QEP_DENIED, VALIDATION_STATUS, APPLICATION_DENTAL_STATUS "+
														"from SSAP_APPLICATIONS where CMR_HOUSEOLD_ID= ?  and COVERAGE_YEAR= ? order by CREATION_TIMESTAMP desc");
			query.setParameter(1, cmrHouseoldId);
			query.setParameter(2, coverageYear);
			List<Object[]> lst=query.getResultList();
			
			List<SsapApplication> ssapAppList= new ArrayList<SsapApplication>();
			SsapApplication localSsapApplication;
			for(Object [] objArray:lst){
				localSsapApplication= new SsapApplication();
				localSsapApplication.setId( objArray[0]!=null? Long.valueOf(objArray[0].toString()) :0);
				localSsapApplication.setCreationTimestamp(objArray[1]!=null? (Timestamp) objArray[1]  :null);
				localSsapApplication.setLastUpdateTimestamp(objArray[2]!=null? (Timestamp)objArray[2]  :null);
				localSsapApplication.setSource(objArray[3]!=null?  objArray[3].toString() :null);
				localSsapApplication.setStartDate(objArray[4]!=null? (Timestamp)objArray[4]  :null);
				
				localSsapApplication.setCaseNumber(objArray[5]!=null?  objArray[5].toString() :null);
				localSsapApplication.setEsignFirstName(objArray[6]!=null?  objArray[6].toString() :null);
				localSsapApplication.setEsignMiddleName(objArray[7]!=null?  objArray[7].toString() :null);
				localSsapApplication.setEsignLastName(objArray[8]!=null?  objArray[8].toString() :null);
				localSsapApplication.setEsignDate(objArray[9]!=null?(Timestamp)objArray[9]  :null);
				
				localSsapApplication.setApplicationStatus(objArray[10]!=null?  objArray[10].toString() :null);
				localSsapApplication.setApplicationType(objArray[11]!=null?  objArray[11].toString() :null);
				localSsapApplication.setSsapApplicationSectionId(objArray[12]!=null? (BigDecimal)objArray[12]  :null);
				
				localSsapApplication.setCmrHouseoldId(objArray[13]!=null? (BigDecimal)objArray[13]  :null);
				localSsapApplication.setCreatedBy(objArray[14]!=null? (BigDecimal)objArray[14]  :null);
				localSsapApplication.setLastNoticeId(objArray[15]!=null? (BigDecimal)objArray[15]  :null);
				localSsapApplication.setEligibilityStatus(objArray[16]!=null?  EligibilityStatus.valueOf(objArray[16].toString()) :null);
				
				localSsapApplication.setEnrollmentStatus(objArray[17]!=null?  objArray[17].toString() :null);
				localSsapApplication.setFinancialAssistanceFlag(objArray[18]!=null?  objArray[18].toString() :null);
				localSsapApplication.setCurrentPageId(objArray[19]!=null?  objArray[19].toString() :null);
				localSsapApplication.setExchangeEligibilityStatus(objArray[20]!=null?  ExchangeEligibilityStatus.valueOf(objArray[20].toString()) :null);
				
				localSsapApplication.setCsrLevel(objArray[21]!=null?  objArray[21].toString() :null);
				localSsapApplication.setMaximumAPTC(objArray[22]!=null? (BigDecimal)objArray[22]  :null);
				localSsapApplication.setElectedAPTC(objArray[23]!=null? (BigDecimal)objArray[23]  :null);
				localSsapApplication.setAllowEnrollment(objArray[24]!=null?  objArray[24].toString() :null);
				localSsapApplication.setCoverageYear(objArray[25]!=null?  Long.valueOf(objArray[25].toString())  :0);
				localSsapApplication.setState(objArray[26]!=null?  objArray[26].toString() :null);
				localSsapApplication.setZipCode(objArray[27]!=null?  objArray[27].toString() :null);
				
				localSsapApplication.setCountyCode(objArray[28]!=null?  objArray[28].toString() :null);
				localSsapApplication.setExternalApplicationId(objArray[29]!=null?  objArray[29].toString() :null);
				localSsapApplication.setParentSsapApplicationId(objArray[30]!=null? (BigDecimal)objArray[30]  :null);
				
				localSsapApplication.setAvailableAPTC(objArray[31]!=null? (BigDecimal)objArray[31]  :null);
				localSsapApplication.setExchangeType(objArray[32]!=null?  objArray[32].toString() :null);
				localSsapApplication.setLastUpdatedBy(objArray[33]!=null? (BigDecimal)objArray[33]  :null);
				localSsapApplication.setEligibilityReceivedDate(objArray[34]!=null? new TSDate(((Timestamp)objArray[34]).getTime()) :null);
				
				
				localSsapApplication.setEligibilityResponseType(objArray[35]!=null?  objArray[35].toString() :null);
				localSsapApplication.setNativeAmerican(objArray[36]!=null?  objArray[36].toString() :null);
				localSsapApplication.setEhbAmount(objArray[37]!=null? (BigDecimal)objArray[37]  :null);
				
				localSsapApplication.setExemptHousehold(objArray[38]!=null?  objArray[38].toString() :null);
				
				localSsapApplication.setRenewalStatus(objArray[39]!=null?  RenewalStatus.valueOf(objArray[39].toString()) :null);
				localSsapApplication.setSepQepDenied(objArray[40]!=null?  objArray[40].toString() :null);
				String validationStatus = objArray[41]!=null?  objArray[41].toString() :null;
				ApplicationValidationStatus status = null;
				if(validationStatus != null){
					status = ApplicationValidationStatus.valueOf(validationStatus);
				}
				localSsapApplication.setValidationStatus(status);
				localSsapApplication.setApplicationDentalStatus(objArray[42]!=null?  objArray[42].toString() :null);
				ssapAppList.add(localSsapApplication);
			} 
			return ssapAppList;
		}
		finally
        {
          if (entityManager != null && entityManager.isOpen())
          {
            try
            {
              entityManager.close();
            }
            catch (IllegalStateException ex)
            {
               if(logger.isErrorEnabled())
            	{
	              logger.error("EntityManager is managed by application server", ex);
	            }
            }
          }
        }
	}
}
