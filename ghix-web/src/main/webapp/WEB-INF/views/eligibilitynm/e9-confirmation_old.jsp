<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View account</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/bootstrap.css">
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghixcustom.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghix-blue-theme.css"> -->
<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ms-custom.css">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/eligibilitynm/css/planView-v2.css" type="text/css">
<link rel="stylesheet" href="../resources/eligibilitynm/css/planViewStyles-v2.css" type="text/css">
	<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.9.1.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/datepicker.css" />

<style type="text/css">
.row-fluid .span12 {
	width:99.9%;
}
.clear {
	clear:both;
	display:block;
	margin-top:5px;
}
.form-vertical hr {
	margin: 10px 0;
	clear: both;
	display: block;
}
h4 {
	font-size:18px;
	line-height:30px;
	padding:10px 0 0;
}
h5 {
	padding:10px 0 0;
}
input.no-margin {
	margin:0;
}
input.txt-center {
	text-align:center;

}
</style>
</head>

<body>

<div class="row-fluid" id="main">
  <div class="container">
    <div class="row-fluid" id="titlebar">
      <div class="gutter10 span3">
        <ul class="nav nav-pills">
          <li> <a href="e1-1-start-app">Back </a> </li>
        </ul>
      </div>
      <div class="span9 pull-left">
        <h3>Your application is accepted.</h3>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked gutter10"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> <i class="icon-ok"></i> Additional Questions </a></li>
          <li class="active"><a href="e6-review-declare-file"> <i class="icon-ok"></i> Review, Declare &amp; File </a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="gutter10">
          <h5>Your confirmation number is 1234456X.</h5>

        <p>An email confirmation will be sent to <strong>eli.manning@gmail.com</strong></p>

        <p>Your eligibility determination is described below. if you do not agree with this determinations, you have the right to appeal. <a href="#">Find out more about the appeal process</a> or <a href="#">Get Assistance</a> to connect with us directly.
        </p>
        <div class="well well-small">
        <h5>Your Eligibility Determination <button class="btn offset1">Submit an Appeal</button> &nbsp;
        <a class="btn"><i class="icon-print"></i> Print</a> &nbsp; <a class="btn"><i class="icon-download-alt"></i> Download</a> </h5>        </div>
         <div class="accordion" id="accordion2">
  <h4>Cynthia Manning</h4>
  <div class="accordion-group gutter10">

    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
      <span class="span1"> <i class="icon-chevron-right"></i></span> <span class="span4"><strong>Premium Tax Credit</strong></span>
        <span class="span4"><strong>Eligible up to $1500<br>01/01/2014 - 12/31/2014</strong></span>
      </a>
    </div>
    <div id="collapseOne" class="accordion-body collapse clear">
      <div class="accordion-inner">
        <p>More info</p>
      </div>
    </div>
  </div>
  <div class="accordion-group gutter10">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
        <span class="span1"> <i class="icon-chevron-right"></i></span> <span class="span4"><strong>Cost Sharing Reduction</strong><br>(for applicable plans)</span>
        <span class="span4"><strong>Eligible<br>
        01/01/2014 - 12/31/2014</strong></span>
      </a>
    </div>
    <div id="collapseTwo" class="accordion-body collapse clear">
      <div class="accordion-inner">
        <p>More info</p>
      </div>
    </div>
  </div>
  <div class="accordion-group gutter10">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree">
      <span class="span1"> <i class="icon-chevron-down"></i></span> <span class="span4"><strong>Medicaid</strong></span> <span class="span4"><strong>Not eligible</strong></span>
      </a>
    </div>
    <div id="collapseThree" class="accordion-body collapse in clear">
      <div class="accordion-inner">
        <h5>Why not eligible?</h5>
        <p><em>Cynthia Manning has been denied eligibility for Medicaid due to her immigration status. Legal permanent residents are ineligible for Medicaid during their first five years in the U.S. If you have different information you would like to provide that may affect this decision, please contact customer service. <a href="#">Read more</a></em></p>
        </div>
    </div>
  </div>
  <br>
<div class="accordion-group gutter10 clear">
        <h5>Exemption?</h5>
        <p>The Current health care law also permits individuals to seek an exemption from the personal responsibility to have health insurance based on several categories. Click the following button to start the proccess. </p><p><a href="#" class="btn offset8">Seek an exemption</a></p>
        </div>
    <h4>Joe Manning</h4>

  <div class="accordion-group gutter10">
    <div class="accordion-heading">
      <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseFour">
         <span class="span1"> <i class="icon-chevron-right"></i></span> <span class="span4"><strong>Medicaid</strong></span>
        <span class="span4"><strong>Eligible<br>
        01/01/2014 - 12/31/2014</strong></span>
      </a>
    </div>
    <div id="collapseFour" class="accordion-body collapse clear">
      <div class="accordion-inner">
        <span>More Info</span>
        </p>
      </div>
    </div>
  </div>
</div> <!--.accordion2 -->
<h3>Understanding your Premium Tax Credit (PTC)</h3>
 <h4><span class="badge badge-success gutter10">1</span> Cynthia Manning has a premium tax credit of up to $1,500</h4>
<p>Your Premium Tax Credit (PTC) will lower your health coverage costs.</p>
<p>The amount of the tax credit is an estimate based on your expected income for 2014. The final amount of the credit will depend on your actual income for 2014, as determined when you file your tax return. When you file, you may be able to get a refund of any unused PTC. If you chose to take some of the PTC on a monthly basis in advance, you may have to repay some or all of the advance if your actual income is greater than the income you expected.</p>
<p>Your monthly PTC can only be used to lower your premium costs. You will still be required to pay for any remaining premium amount.</p>
<p>If you chose to take advantage of this tax credit, you are required to file taxes.</p>
<hr>
<h4><span class="badge badge-success gutter10">2</span> There are two ways to use your PTC</h4>
<div class="span5">
<h5>Monthly Credit</h5>
<p>You can chose to use some or all of your PTC in advance to lower your monthly premium.</p>
<p><strong>Pros:</strong> Your plans cost less each month.</p>
<p><strong>Cons:</strong> If your household income increases, you could owe money at tax time.</p>
</div>
<div class="span5">
<h5>Annual Tax Credit</h5>
<p>You can claim the PTC on your annual tax IRS return, which will lower the tax you owe or increase your refund.</p>
<p><strong>Pros:</strong> No risk of having to repay at tax time.</p>
<p><strong>Cons:</strong> Your plans cost more each month.</p>
</div>
<hr class="clear">
<h4><span class="badge badge-success gutter10">3</span> Adjust the slider or enter an amount to set your $1,500 tax credit.</h4>
<p>If your actual household income for 2014 is more than you expected, you may have to repay some or all of the monthly tax credit. You will be able to adjust this PTC setting again when you chose your health plan.</p>
<form class="form-vertical">
<div class="calculator well">
<div class="control-group span2">
<div class="controls">
 <div id="slider" style="height: 170px; width: 50px;"></div>
    </div>
     </div>
    <div class="control-group span5">
    <div class="controls ">
    <label for="mtc">Monthly Tax Credit</label>
    <input type="text" name="mtc" id="mtc" placeholder="$125" class="span8"> x 12 months
<label for="atc">Annual Tax Credit</label>
    <input type="text" name="atc" placeholder="$1,500" class="span8" id="atc">
    <button class="btn btn-primary clear span8">Confirm</button>
 </div>
</div>
<span class="clear"></span>
</div>
<div class="control-group">
              <div class="controls">
                <label class="checkbox">
                  <input type="checkbox" value="">
                  On behalf of my household, I acknowledge and accept this eligibility determination as accurate.  </label>

              </div>
            </div>

        </form>
        <div class="well well-small">
    <a class="btn btn-primary offset9" href="../A0-Household">Find a Plan</a>
    </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->
</div>
</div>

<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script src="../resources/eligibilitynm/js/jquery-ui-1.9.1.custom.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
$( "#slider" ).slider({
						orientation: "vertical",
														range: "min",
														min: 0,
														max: 1500,
														value: 0,
														slide: function( event, ui ) {
															if(ui.value == 0){
																$( "#mtc" ).val(125);
																$( "#atc" ).val(0);
															}else if(ui.value == 1500){
																$( "#mtc" ).val(0);
																$( "#atc" ).val(1500);
															}
															else {
																$( "#mtc" ).val(125 - Math.round(ui.value/12));
																$( "#atc" ).val( ui.value );

															}

																													}
													});
													$( "#mtc" ).val( $( "#slider" ).slider( "value" ) );
													$( "#atc" ).val( $( "#slider" ).slider( "value" ) );

/*
$( "#slider" ).slider({
	orientation: "vertical",
            range: "min",
            min: 0,
            max: 1500,
            value: 0,
			rmtc:0,
            slide: function( event, ui ) {
               var atc = $( "#atc" ).val( ui.value );
			   var mtc = 125
				var mtc = (atc.val()/12);
				var rmtc = Math.round(mtc*100)/100;
				var rmtc = (rmtc*12)/12;
				console.log(rmtc);
				$("#mtc").val(rmtc);
            }
        });
        $( "#atc" ).val( $( "#slider" ).slider( "value" ) );
		     */

</script>
</body>
</html>
