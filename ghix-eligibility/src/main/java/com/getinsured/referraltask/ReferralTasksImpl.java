package com.getinsured.referraltask;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.service.ReferralWorkFlowService;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("referralTasks")
public class ReferralTasksImpl implements ReferralTasks {
	private static final Logger LOGGER = Logger.getLogger(ReferralTasksImpl.class);

	@Autowired
	@Qualifier("referralWorkFlowService")
	private ReferralWorkFlowService referralWorkFlowService;
	
	@Override
	@Async
    public void processRidpAfterDocumentApproval(Long referralId) {
		LOGGER.debug("processRidpAfterDocumentApproval for referral - " + referralId);
		//TODO: Added temp fix for access code
		referralWorkFlowService.processRidpAndLinking(referralId, ReferralConstants.NONE, 0);
		LOGGER.debug("processRidpAfterDocumentApproval for referral - done");
    }
}
