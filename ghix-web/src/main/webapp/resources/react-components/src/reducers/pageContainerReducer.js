import { ActionTypes, PAGE_CONTAINERS } from '../constants/constants';

const INITIAL_STATE = PAGE_CONTAINERS.DASHBOARD;

const pageContainerReducer = (state=INITIAL_STATE, {type, payload}) => {
    switch (type) {
        case (ActionTypes.CHANGE_PAGE_CONTAINER):
            return payload;
        default:
            return state;
    }
};

export default pageContainerReducer;