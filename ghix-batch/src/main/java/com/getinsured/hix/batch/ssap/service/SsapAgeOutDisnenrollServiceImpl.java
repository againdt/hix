package com.getinsured.hix.batch.ssap.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestClientException;

import com.getinsured.eligibility.at.sep.repository.SepEventsRepository;
import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantEventService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicantService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationEventService;
import com.getinsured.eligibility.lce.service.LifeChangeEventApplicationService;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutDependant;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutNoticeDTO;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentDisEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.dto.planmgmt.PediatricPlanResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.lce.ChangedApplicant;
import com.getinsured.iex.lce.RequestParamDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.lce.SepTransientDTO;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.model.SsapVerification;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapVerificationRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.thoughtworks.xstream.XStream;


@Service
public class SsapAgeOutDisnenrollServiceImpl implements SsapAgeOutDisnenrollService {
	
	private static final String ENROLLMENT_STATUS_LIST = "enrollmentStatusList";
	private static final String PLAN_LEVEL = "planLevel";
	private static final String DATE_FORMAT_MMMM_DD_YYYY = "MMMM dd, YYYY";
	private static final String AGE_OUT_DISENROLL_NOTIFICATION = "AgeOutDisenrollNotification";
	private static final String TERMINATION_DATE = "teminationDate";
	private static final String ABORTED = "ABORTED";
	private static final String OPEN_APPLICATION_FOUND = "OPEN_APPLICATION_FOUND";
	private static final String HOUSEHOLD_DISENROLL_SUCCESS = "HOUSEHOLD_DISENROLL_SUCCESS";
	private static final String NO_AGE_OUT_DEPENDENTS = "NO_AGE_OUT_DEPENDENTS";
	private static final String ENROLLMENT_NOT_ACTIVE = "ENROLLMENT_NOT_ACTIVE";
	private static final String CASTAROPHIC_PLAN_FOUND = "CASTAROPHIC_PLAN_FOUND";
	private static final String CHILD_RELATIONS = "childRelations";
	private static final String APPLICATION_STATUS = "applicationStatus";
	private static final String FINANCIAL_ASSISTANCE_FLAG = "financialAssistanceFlag";
	private static final String COVERAGE_YEAR = "coverageYear";
	private static final String EXCEPTION_STACK_TRACE = "exceptionStackTrace";
	private static final String EXECUTION_TIME = "executionTime";
	private static final String APPLICATION_ID = "applicationId";
	private static final String PROCESSING_RESULT = "processingResult";
	private static final String AGE_OUT_DISENROLL_BATCH = "AGE_OUT_DISENROLL_BATCH";
	private static final String IS_ENROLLMENT_ACTIVE = "isEnrollmentActive";
	private static final String IS_CATASTRPHIC_PLAN = "isCatastrphicPlan";
	private static final String CANCEL = "CANCEL";
	private static final String CATASTROPHIC = "CATASTROPHIC";
	private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	private static final String DISENROLLMENT_REASON_CODE = "07";
	private static final String IN_ACTIVE_HEALTH_ENROLLEE = "inActiveHealthEnrollee";
	private static final String IN_ACTIVE_DENTAL_ENROLLEE = "inActiveDentalEnrollee";
	private static final String DEPENDENT_CHILD_AGES_OUT = "DEPENDENT_CHILD_AGES_OUT";
	private static final String NON_COMPLIANCE_FOR_VERIFICATIONS = "Non-compliance for verifications.";
	private static final String FALSE = "FALSE";
	private static final String DENTAL = "Dental";
	private static final String HEALTH = "Health";
	private static final String DENTAL_ENROLLEE = "dentalEnrollee";
	private static final String HEALTH_ENROLLEE = "healthEnrollee";
	private static final String EXADMIN_USERNAME = "exadmin@ghix.com";
	private static final String SUCCESS = "SUCCESS";
	private static final String DISENROLL_FROM_DENTAL = "disenrollFromDental";
	private static final String DISENROLL_FROM_HEALTH = "disenrollFromHealth";
	private static final String IS_AGE_MORE_THAN26 = "isAgeMoreThan26";
	private static final String IS_AGE_MORE_THAN19 = "isAgeMoreThan19";
	private static final Logger LOGGER = Logger.getLogger(SsapAgeOutDisnenrollServiceImpl.class);
	private static final String HEALTH_ENROLLMENT_DTO = "HealthEnrollmentDTO";
	private static final String DENTAL_ENROLLMENT_DTO = "DentalEnrollmentDTO";
	private static final List<String> OPEN_APPLICATION_STATUS = new ArrayList<String>(){
		{
		add(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		add(ApplicationStatus.OPEN.getApplicationStatusCode());
		add(ApplicationStatus.SUBMITTED.getApplicationStatusCode());
		add(ApplicationStatus.SIGNED.getApplicationStatusCode());
		}
	};
	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	SsapVerificationRepository ssapVerificationRepository;
	@Autowired
	ILocationRepository iLocationRepository;
	@Autowired
	SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	SepEventsRepository sepEventsRepository;
	@Autowired 
	GIMonitorService giMonitorService;
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	@Autowired
	LifeChangeEventApplicationService lifeChangeEventApplicationService;
	@Autowired
	LifeChangeEventApplicantService lifeChangeEventApplicantService;
	@Autowired
	LifeChangeEventApplicationEventService lifeChangeEventApplicationEventService;
	@Autowired
	LifeChangeEventApplicantEventService lifeChangeEventApplicantEventService;
	@Autowired
	IEligibilityProgram iEligibilityProgram;
	@Autowired 
	UserRepository userRepository;
	@Autowired 
	IntegrationLogService integrationLogService;
	@PersistenceUnit
	private EntityManagerFactory emf;
	@Autowired
	SsapApplicantRepository ssapApplicantRepository;
	@Autowired
	NoticeService noticeService;
	@Autowired
	PreferencesService preferencesService;
	
	@Override
	public Map<String,String> disenrollQHP(Long applicationId){
		String result  = null;
		Map<String,String> map = new HashMap<String,String>();
		LOGGER.info("Age out process started :"+applicationId);
		long currentTimeMillis = System.currentTimeMillis();
		try{
			result  = processDisenroll(applicationId);
			map.put(PROCESSING_RESULT, result);
			map.put(APPLICATION_ID, String.valueOf(applicationId));
		}catch (Exception ex) {
			LOGGER.error("Error occured while processing age out disenroll parent application :"+applicationId);
			map.put(PROCESSING_RESULT, GhixConstants.RESPONSE_FAILURE);
			map.put(APPLICATION_ID, String.valueOf(applicationId));
			map.put(EXCEPTION_STACK_TRACE, ExceptionUtils.getFullStackTrace(ex));
		}
		map.put(EXECUTION_TIME, String.valueOf((System.currentTimeMillis()-currentTimeMillis)));
		LOGGER.info("Age out process completed:"+applicationId);
		return map; 
	}
	
	private String processDisenroll(Long applicationId) throws Exception {
		SsapApplication ssapApplication = null;
		SsapApplication currentApplication = null;
		Map<String,Set<String>> disenrollMap = null;
		Set<String> disenrollFromHealth = null;
		Set<String> disenrollFromDental = null;
		EnrolleeResponse enrolleeResponse = null;
		Map<String,Set<String>> enrollee = new HashMap<String, Set<String>>();
		Map<String, EnrollmentShopDTO> enrollmentIdMap = new HashMap<String, EnrollmentShopDTO>();
		Map<String,Boolean> enrolleeResult = new HashMap<String,Boolean>();
		long currentMillis= 0;
		try {
			//get Latest application with applicants
			ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(applicationId);
			currentMillis= System.currentTimeMillis();
			//invoke enrollment API to get enrollee
			enrolleeResponse = invokeEnrollmentApi(ssapApplication.getId(), EXADMIN_USERNAME);
			LOGGER.debug("Get Enrollee by application id completed in "+(System.currentTimeMillis() - currentMillis));
			// create set for heath and Dental Enrollee
			enrollee = prepareEnrollee(enrolleeResponse,enrollmentIdMap,enrollee,enrolleeResult);
			//check if any enrollment is active
			if(!enrolleeResult.get(IS_ENROLLMENT_ACTIVE)){
				return ENROLLMENT_NOT_ACTIVE;
			}
			//check if any age out dependent in application
			disenrollMap = dependentAgingCheck(ssapApplication,enrollee);
			//get the age out data
			disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
			disenrollFromDental = disenrollMap.get(DISENROLL_FROM_DENTAL);
			//if disenroll from dental is not empty, check if enrolled into dental family plan
			if(!disenrollFromDental.isEmpty()){
				currentMillis= System.currentTimeMillis();
				//call plan management API
				boolean isChildOnlyPlan = isChildOnlyPlan(enrollmentIdMap,ssapApplication.getCoverageYear());
				LOGGER.debug("Get pediatric info by hios id completed in "+(System.currentTimeMillis() - currentMillis));
				//if it is dental family plan do not disenroll from dental, clear the disnenrollFromDetal map
				if(!isChildOnlyPlan){
					disenrollFromDental.clear();
				}
				
				//add age out 26 members in dental enrollment if they are enrolled in dental enrollment.
				if(!disenrollFromHealth.isEmpty()){
					updateDisenrollFromDental(disenrollFromDental,disenrollFromHealth,enrollee);
				}
			}
			//if not age out dependent return
			if(disenrollFromHealth.isEmpty() && disenrollFromDental.isEmpty()){
				return NO_AGE_OUT_DEPENDENTS;
			}
			
			//check if plan level is catastrophic
			if(enrolleeResult.get(IS_CATASTRPHIC_PLAN)){
				return CASTAROPHIC_PLAN_FOUND;
			}
			
			//check if household disenroll case
			if(isHouseholdDisenroll(enrolleeResponse, disenrollFromHealth, disenrollFromDental)){
				processDisenrollment(ssapApplication);
				triggerAgeOutNotice(ssapApplication, disenrollMap, enrollmentIdMap);
				return HOUSEHOLD_DISENROLL_SUCCESS;
			}
			
			//check if already open application for coverage year 
			long count = ssapApplicationRepository.countOfApplicationsByStatusAndCoverageYear(ssapApplication.getCmrHouseoldId(),"SEP",ssapApplication.getCoverageYear(),OPEN_APPLICATION_STATUS,"N");
			if(count>0){
				return OPEN_APPLICATION_FOUND;
			}
			//cloneSsapApplication
			currentMillis= System.currentTimeMillis();
			LOGGER.debug("Cloning application started");
			currentApplication = cloneSsapApplication(ssapApplication,disenrollMap,enrolleeResponse);
			//save application and update application id in json 
			currentApplication = saveSsapApplication(currentApplication);
			//load new application with applicants
			currentApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplication.getId());
			//update vid 
			updateVerifications(currentApplication);
			//save program eligibility
			saveEligibilityPrograms(currentApplication.getSsapApplicants(), ssapApplication.getSsapApplicants(),disenrollMap);
			LOGGER.debug("Cloning application completed in "+(System.currentTimeMillis() - currentMillis));
			//trigger auto enroll 
			currentMillis = System.currentTimeMillis();
			triggerAutoEnroll(enrollmentIdMap,enrollee, currentApplication,disenrollMap);
			LOGGER.debug("Autenroll API completed in "+(System.currentTimeMillis() - currentMillis));
		} catch (Exception e) {
			LOGGER.error("Error occured while processing age out disenroll parent application :"+ssapApplication.getId() +",current application:" +(currentApplication==null?"":currentApplication.getId()),e);
			giMonitorService.saveOrUpdateErrorLog(null, new Date(), this.getClass().getName(), e.getMessage()+'\n'+ExceptionUtils.getStackTrace(e), null, ssapApplication.getCaseNumber(), GIRuntimeException.Component.BATCH.getComponent(), null);
			throw e;
		}
		return SUCCESS;
	}
	
	private void updateDisenrollFromDental(Set<String> disenrollFromDental,Set<String> disenrollFromHealth, Map<String, Set<String>> enrollee) {
		Set<String> dentalEnrollee  =  enrollee.get(DENTAL_ENROLLEE);
		Set<String> inActiveDentalEnrollee  =  enrollee.get(IN_ACTIVE_DENTAL_ENROLLEE);
		for(String applicantGuid  : disenrollFromHealth){
			if(dentalEnrollee.contains(applicantGuid) && !inActiveDentalEnrollee.contains(applicantGuid)){
				disenrollFromDental.add(applicantGuid);
			}
		}
		
	}

	private void updateVerifications(SsapApplication currentApplication) {
		for(SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()){
			List<SsapVerification> ssapVerifications  = ssapVerificationRepository.findBySsapApplicant(ssapApplicant);
			for(SsapVerification ssapVerification : ssapVerifications){
				readVerificationStatus(ssapApplicant, ssapVerification.getVerificationType(), ssapVerification.getVerificationStatus(), ssapVerification.getId());
			}
			ssapApplicantRepository.save(ssapApplicant);
		}
		
	}

	@Override
	public void logData(Set<Future<Map<String,String>>> tasks){
		List<Map<String,String>> jsonResult = new ArrayList<Map<String,String>>();
		Gson gson = new Gson();
		Map<String, String> result =null;
		try {
			for (Future<Map<String,String>> future : tasks) {
				if(future.isDone()) {
						result = future.get();
						if(result!= null && result.get(PROCESSING_RESULT) !=null && !result.get(PROCESSING_RESULT).equals(NO_AGE_OUT_DEPENDENTS)){
							jsonResult.add(result);
						}
				}
			}
			
			String jsonString = (gson.toJsonTree(jsonResult, new TypeToken<List<Map<String,String>>>() {}.getType()).getAsJsonArray().toString());
			integrationLogService.save(jsonString, AGE_OUT_DISENROLL_BATCH, GhixConstants.RESPONSE_SUCCESS, null, null);
		} catch (Exception ex) {
			LOGGER.error("Exception occured while genereate age out batch execution details",ex);
			giMonitorService.saveOrUpdateErrorLog(null, new Date(), this.getClass().getName(), ExceptionUtils.getStackTrace(ex), null, null, GIRuntimeException.Component.BATCH.getComponent(), null);
		}
	}
	
	private boolean isCastarophicPlan(EnrollmentShopDTO enrollmentShopDTO) {
		boolean isCatastrophicPlan = false;
		if((enrollmentShopDTO != null && enrollmentShopDTO.getPlanLevel().equals(CATASTROPHIC))){
			isCatastrophicPlan = true;
		}
		return isCatastrophicPlan;
	}

	private boolean isChildOnlyPlan(Map<String, EnrollmentShopDTO> enrollmentIdMap,Long coverageYear) {
		EnrollmentShopDTO dentalEnrollmentShopDTO  = enrollmentIdMap.get(DENTAL_ENROLLMENT_DTO);
		Map<String,Object> requestMap = new HashMap<>();
		Gson gson = new Gson();
		requestMap.put("hiosPlanId", dentalEnrollmentShopDTO.getCmsPlanId());
		requestMap.put("planYear", coverageYear);
		String request = gson.toJson(requestMap);
		PediatricPlanResponseDTO pediatricPlanResponseDTO=ghixRestTemplate.postForObject(GhixEndPoints.PlanMgmtEndPoints.GET_PEDIATRIC_PLAN_INFO, request,PediatricPlanResponseDTO.class);
			return pediatricPlanResponseDTO.isChildOnly();
	}

	private void triggerAutoEnroll(Map<String, EnrollmentShopDTO> enrollmentIdMap,Map<String,Set<String>> enrollee,SsapApplication ssapApplication,Map<String,Set<String>> disenrollMap) throws GIException {
		Gson gson = new Gson();
		Map<String,Object> request = new HashMap<String,Object>();
		request.put("enrollee", enrollee);
		request.put("enrollmentIdMap", enrollmentIdMap);
		request.put("enrolledSsapApplicationId", ssapApplication.getParentSsapApplicationId());
		request.put("ssapApplicationId", ssapApplication.getId());
		request.put("disenrollMap", disenrollMap);
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "/application/autoEnroll/", EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, gson.toJson(request));
		if(response == null || response.getBody() == null || !response.getBody().equals(GhixConstants.RESPONSE_SUCCESS)){
			updateApplicationStatus(ssapApplication,ApplicationStatus.CLOSED);
			throw new GIException("Exception occurred while auto enrollment");
		}
		
	}

	private void updateApplicationStatus(SsapApplication ssapApplication,ApplicationStatus applicationStatus) {
		ssapApplication = ssapApplicationRepository.findOne(ssapApplication.getId());
		if(ssapApplication.getApplicationStatus().equals(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode())){
			ssapApplication.setApplicationStatus(applicationStatus.getApplicationStatusCode());
			ssapApplicationRepository.saveAndFlush(ssapApplication);
		}
	}
	
	public Map<String, EnrollmentShopDTO> fetchEnrollmentID(EnrolleeResponse enrolleeResponse) {
		Map<String, EnrollmentShopDTO> enrollmentIdMap = new HashMap<String, EnrollmentShopDTO>();
		
		try {
			if (null != enrolleeResponse && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				final List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
				for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
					if (HEALTH.equalsIgnoreCase(enrollmentShopDTO.getPlanType())) {
						enrollmentIdMap.put(HEALTH_ENROLLMENT_DTO, enrollmentShopDTO);
					}
					if (DENTAL.equalsIgnoreCase(enrollmentShopDTO.getPlanType())) {
						enrollmentIdMap.put(DENTAL_ENROLLMENT_DTO, enrollmentShopDTO);
					}
				}
			} else {
				if (enrolleeResponse != null) {
					throw new GIRuntimeException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
				}
			}

		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching enrollment details :", e);
		}
		return enrollmentIdMap;
	}
	
	private boolean isHouseholdDisenroll(EnrolleeResponse enrolleeResponse,Set<String> disenrollFromHealth,Set<String> disenrollFromDental){
			boolean isHouseholdDisenroll = true;
			List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
			for(EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs){
				List<EnrolleeShopDTO> enrolleeShopDTOList =  enrollmentShopDTO.getEnrolleeShopDTOList();
				for(EnrolleeShopDTO enrolleeShopDTO:enrolleeShopDTOList){
					
					if(enrollmentShopDTO.getPlanType().equals(HEALTH)){
						if(enrolleeShopDTO.getEffectiveEndDate().after(getLastDayOfCurrentMonth().getTime()) && !disenrollFromHealth.contains(enrolleeShopDTO.getExchgIndivIdentifier())){
							return false;
						}
						
					}else if(enrollmentShopDTO.getPlanType().equals(DENTAL)){
						if(enrolleeShopDTO.getEffectiveEndDate().after(getLastDayOfCurrentMonth().getTime()) && !disenrollFromDental.contains(enrolleeShopDTO.getExchgIndivIdentifier())){
							return false;
						}
					}
					
				}
				
		}
		return isHouseholdDisenroll;
	}
	
	private Map<String,Set<String>> prepareEnrollee(EnrolleeResponse enrolleeResponse,Map<String, EnrollmentShopDTO> enrollmentIdMap,Map<String,Set<String>> enrollee,Map<String,Boolean> enrolleeResult){
		Set<String> healthEnrollee  =  new HashSet<String>();
		Set<String> dentalEnrollee  =  new HashSet<String>();
		Set<String> inActiveHealthEnrollee  =  new HashSet<String>();
		Set<String> inActiveDentalEnrollee  =  new HashSet<String>();
		Boolean isCatastrophicPlan = false;
		Boolean isEnrollmentActive = false;
		List<EnrollmentShopDTO> enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
		//sort to consider latest enrollments for age out.
		Collections.sort(enrollmentShopDTOs, new Comparator<EnrollmentShopDTO>(){
		     public int compare(EnrollmentShopDTO enrollment1, EnrollmentShopDTO enrollment2){
		          return enrollment2.getEnrollmentCreationTimestamp().compareTo(enrollment1.getEnrollmentCreationTimestamp());
		     }

		});
		
		for(EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs){
			if(enrollmentShopDTO.getPlanType().equals(HEALTH)){
				populateFilteredEnrollment(enrollmentIdMap, enrollmentShopDTO,HEALTH_ENROLLMENT_DTO);
			}else if(enrollmentShopDTO.getPlanType().equals(DENTAL)){
				populateFilteredEnrollment(enrollmentIdMap, enrollmentShopDTO,DENTAL_ENROLLMENT_DTO);
			}
		}
		
		
		for(String enrollmentType : enrollmentIdMap.keySet()){
			EnrollmentShopDTO enrollmentShopDTO = enrollmentIdMap.get(enrollmentType);
			if(isCastarophicPlan(enrollmentShopDTO)){
				isCatastrophicPlan =  true;
			}
			if(isEnrollmentActive(enrollmentShopDTO)){
				isEnrollmentActive = true;
			}
			List<EnrolleeShopDTO> enrolleeShopDTOList =  enrollmentShopDTO.getEnrolleeShopDTOList();
			if(enrollmentShopDTO.getPlanType().equals(HEALTH)){
				populateEnrollee(enrolleeShopDTOList, healthEnrollee, inActiveHealthEnrollee);
			}else if(enrollmentShopDTO.getPlanType().equals(DENTAL)){
				populateEnrollee(enrolleeShopDTOList, dentalEnrollee, inActiveDentalEnrollee);
			}
		}
		enrollee.put(HEALTH_ENROLLEE, healthEnrollee);
		enrollee.put(DENTAL_ENROLLEE, dentalEnrollee);
		enrollee.put(IN_ACTIVE_HEALTH_ENROLLEE, inActiveHealthEnrollee);
		enrollee.put(IN_ACTIVE_DENTAL_ENROLLEE, inActiveDentalEnrollee);
		enrolleeResult.put(IS_CATASTRPHIC_PLAN, isCatastrophicPlan);
		enrolleeResult.put(IS_ENROLLMENT_ACTIVE, isEnrollmentActive);
		return enrollee;
	}

	private void populateFilteredEnrollment(Map<String, EnrollmentShopDTO> enrollmentIdMap,EnrollmentShopDTO enrollmentShopDTO,String enrollmentType) {
		EnrollmentShopDTO enrollmentDTO  = enrollmentIdMap.get(enrollmentType);
		if(enrollmentDTO != null){
			if(!isEnrollmentActive(enrollmentDTO) && isEnrollmentActive(enrollmentShopDTO)){
				enrollmentIdMap.put(enrollmentType, enrollmentShopDTO);
			}
		}else{
			enrollmentIdMap.put(enrollmentType, enrollmentShopDTO);
		}
	}
	
	private boolean isEnrollmentActive(EnrollmentShopDTO enrollmentShopDTO) {
		if(enrollmentShopDTO.getEnrollmentStatusValue().equals(CANCEL) || enrollmentShopDTO.getEnrollmentStatusValue().equals(ABORTED) || (!enrollmentShopDTO.getCoverageEndDate().after(getLastDayOfCurrentMonth().getTime()))){
			return false;
		}	
		return true;
	}

	private void populateEnrollee(List<EnrolleeShopDTO> enrolleeShopDTOList,Set<String> enrolleeSet,Set<String> inActiveEnrolleeSet){
		for(EnrolleeShopDTO enrolleeShopDTO:enrolleeShopDTOList){
			if(enrolleeShopDTO.getEnrolleeLookUpValue().equals(CANCEL) || enrolleeShopDTO.getEnrolleeLookUpValue().equals(ABORTED) || (!enrolleeShopDTO.getEffectiveEndDate().after(getLastDayOfCurrentMonth().getTime()))){//TODO update condition for active enrolee
				inActiveEnrolleeSet.add(enrolleeShopDTO.getExchgIndivIdentifier());
			}
			enrolleeSet.add(enrolleeShopDTO.getExchgIndivIdentifier());
		}
	}

	private SsapApplication saveSsapApplication(SsapApplication ssapApplication) throws Exception {
		String updatedJson = null;
		ssapApplication = ssapApplicationRepository.saveAndFlush(ssapApplication);
		// update application id in json
		updatedJson = LifeChangeEventUtil.setApplicationIdAndGuid(ssapApplication.getId(), ssapApplication.getApplicationData(), ssapApplication.getCaseNumber());
		ssapApplication.setApplicationData(updatedJson);
		ssapApplication = ssapApplicationRepository.saveAndFlush(ssapApplication);
		return ssapApplication;
	}

	private Map<String,Set<String>> dependentAgingCheck(SsapApplication currentApplication,Map<String,Set<String>> enrollee){
		List<String> bloodRelations = new ArrayList<String>();
		String childRelations = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_CHILDRELATIONCODE);
		String[] childRelationArr = childRelations.split(",");
		Set<String> disenrollFromHealth = new HashSet<String>();
		Set<String> disenrollFromDental = new HashSet<String>();
		Map<String,Set<String>> disenrollMap = new HashMap<String,Set<String>>();
		Set<String> healthEnrollee  =  enrollee.get(HEALTH_ENROLLEE);
		Set<String> dentalEnrollee  =  enrollee.get(DENTAL_ENROLLEE);
		Set<String> inActiveHealthEnrollee  =  enrollee.get(IN_ACTIVE_HEALTH_ENROLLEE);
		Set<String> inActiveDentalEnrollee  =  enrollee.get(IN_ACTIVE_DENTAL_ENROLLEE);
		
		for (String childRelation : childRelationArr) {
			bloodRelations.add(childRelation);
		}
	
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		List<BloodRelationship> relationShip = getBloodRelationship(members);
		if(relationShip == null){
			throw new GIRuntimeException("Can't find relationship for application :"+currentApplication.getId());
		}
		List<SsapApplicant> ssapApplicants = currentApplication.getSsapApplicants();
		
/*			if(isPrimaryNotSeekingCoverage(ssapApplicants)  && isChildOnlyPlan(relationShip, bloodRelations)) {
			return true;
		}*/
	
		for (BloodRelationship bloodRelationship : relationShip) {
			if (bloodRelationship.getRelatedPersonId() != null && Integer.parseInt(bloodRelationship.getRelatedPersonId()) == 1) {
				if(bloodRelations.contains(bloodRelationship.getRelation())){	
					for (SsapApplicant ssapApplicant : ssapApplicants) {
						//TODO - check if enrolled API to be called
						if(ssapApplicant.getPersonId() == Integer.parseInt(bloodRelationship.getIndividualPersonId())) {
							//age comparison
							Map<String,Boolean> ageComparisonResult =  checkForAge(ssapApplicant.getBirthDate());
							boolean isAgeMoreThan26  = ageComparisonResult.get(IS_AGE_MORE_THAN26);
							boolean isAgeMoreThan19  = ageComparisonResult.get(IS_AGE_MORE_THAN19);
							
							if(isAgeMoreThan26 && healthEnrollee.contains(ssapApplicant.getApplicantGuid()) && !inActiveHealthEnrollee.contains(ssapApplicant.getApplicantGuid())){
								disenrollFromHealth.add(ssapApplicant.getApplicantGuid());
							}
							if(isAgeMoreThan19 && dentalEnrollee.contains(ssapApplicant.getApplicantGuid()) && !inActiveDentalEnrollee.contains(ssapApplicant.getApplicantGuid()) ){
								disenrollFromDental.add(ssapApplicant.getApplicantGuid());
							}
							
						}
					}
				}
			}
		}
		
		disenrollMap.put(DISENROLL_FROM_HEALTH, disenrollFromHealth);
		disenrollMap.put(DISENROLL_FROM_DENTAL, disenrollFromDental);
		return disenrollMap;
	}
	
	private List<BloodRelationship> getBloodRelationship(List<HouseholdMember> members) {
		for(HouseholdMember householdMember : members){
			if(householdMember.getPersonId() == 1){
				return householdMember.getBloodRelationship();
			}
		}
		return null;
	}

	private Map<String, Boolean> checkForAge(Date birthDate) {
		Calendar lastDayOfMonth = getLastDayOfCurrentMonth();
		int age = getAgeOnDate(birthDate,lastDayOfMonth.getTime());
		boolean isAgeMoreThan19 =  (age >= 19);
		boolean isAgeMoreThan26 = (age >= 26);
		Map<String, Boolean> result= new HashMap<String, Boolean>();
		result.put(IS_AGE_MORE_THAN19, isAgeMoreThan19);
		result.put(IS_AGE_MORE_THAN26, isAgeMoreThan26);
		return result;
	}

	private Calendar getLastDayOfCurrentMonth() {
		Calendar lastDayOfMonth = Calendar.getInstance();
		lastDayOfMonth.set(Calendar.DAY_OF_MONTH,lastDayOfMonth.getActualMaximum(Calendar.DAY_OF_MONTH));
		lastDayOfMonth.set(Calendar.HOUR_OF_DAY, 23);
		lastDayOfMonth.set(Calendar.MINUTE, 59);
		lastDayOfMonth.set(Calendar.SECOND, 59);
		lastDayOfMonth.set(Calendar.MILLISECOND, 999);
		return lastDayOfMonth;
	}
	
	
	
	private int getAgeOnDate(Date birthDate,Date onDate) {
		Calendar dob = Calendar.getInstance();  
		dob.setTime(birthDate);  
		Calendar toDate = Calendar.getInstance();
		toDate.setTime(onDate);
		int age = toDate.get(Calendar.YEAR) - dob.get(Calendar.YEAR);  
		if (toDate.get(Calendar.MONTH) < dob.get(Calendar.MONTH)) {
		  age--;  
		} else if (toDate.get(Calendar.MONTH) == dob.get(Calendar.MONTH)
		    && toDate.get(Calendar.DAY_OF_MONTH) < dob.get(Calendar.DAY_OF_MONTH)) {
		  age--;  
		}
		return age;
	}
	
	
	

	private SsapApplication cloneSsapApplication(SsapApplication parentApplication,Map<String,Set<String>> disenrollMap,EnrolleeResponse enrolleeResponse) throws Exception {
		SepTransientDTO sepTransientDTO = new SepTransientDTO();
		SepRequestParamDTO sepRequestParamDTO = null;
		SepResponseParamDTO sepResponseParamDTO = new SepResponseParamDTO();
		SsapApplication currentApplication = null;
		List<SsapApplicant> ssapApplicants = null;
		List<SsapApplicationEvent> ssapApplicationEvents = null;
		List<SsapApplicantEvent> ssapApplicantEvents = null;
		String ssapJson = null;
		Map<String, Boolean> demographicFlagMap = new HashMap<String, Boolean>();
		Map<Long, List<String>> demographicEventsData = new HashMap<Long, List<String>>();
		List<Boolean> homeAddressChangeDemoFlags = new ArrayList<Boolean>();
		Date demographicEventDate = null;
		Date dobEventDate = null;
		List<SsapApplicant> parentApplicants = parentApplication.getSsapApplicants();
		sepRequestParamDTO = createSepRequestParamDTOForDisenroll(parentApplication, parentApplicants,disenrollMap);
		sepTransientDTO.setDemographicEventDate(demographicEventDate);
		sepTransientDTO.setDemographicEventsData(demographicEventsData);
		sepTransientDTO.setDemographicFlagMap(demographicFlagMap);
		sepTransientDTO.setHomeAddressChangeDemoFlags(homeAddressChangeDemoFlags);
		sepTransientDTO.setDobEventDate(dobEventDate);
		sepTransientDTO.setCmrHouseholdId(sepRequestParamDTO.getHouseholdId());
		//get enrolled applicant's person Id
		LifeChangeEventDTO lifeChangeEventDTO =  extractEnrolledData(enrolleeResponse.getEnrollmentShopDTOList(),parentApplicants);
		sepTransientDTO.setEnrolledPersons(lifeChangeEventDTO.getPersonIds());
		sepTransientDTO.setUserName(sepRequestParamDTO.getUserName());
		sepTransientDTO.setUserId(sepRequestParamDTO.getUserId());
		List<RequestParamDTO> events = sepRequestParamDTO.getEvents();
		updateDemographicMaps(demographicFlagMap);
		//remove applicant from json
		updateJson(parentApplication,disenrollMap);
		ssapJson = parentApplication.getApplicationData();
		currentApplication = lifeChangeEventApplicationService.cloneSsapApplicationFromParent(ssapJson, currentApplication, parentApplication, sepRequestParamDTO, "SEP", "ON", ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(), parentApplication.getCoverageYear());
		ssapApplicants = lifeChangeEventApplicantService.cloneSsapApplicants(currentApplication, events, sepTransientDTO.getUserId(), sepResponseParamDTO);
		ssapApplicationEvents = lifeChangeEventApplicationEventService.cloneSsapApplicationEvent(currentApplication, sepTransientDTO.getUserId(), SsapApplicationEventTypeEnum.SEP);
		ssapApplicantEvents = lifeChangeEventApplicantEventService.cloneSsapApplicantEvents(currentApplication, ssapApplicationEvents.get(0), ssapApplicants, events, sepTransientDTO);
		//set eligliblity status same as parent application
		setApplicationData(currentApplication,parentApplication);
		//set applicant data
		setApplicantData(ssapApplicants,disenrollMap);
		//create verifications
		setVerifcations(parentApplication.getSsapApplicants(),ssapApplicants);
		ssapApplicationEvents.get(0).setChangePlan("Y");
		ssapApplicationEvents.get(0).setSsapApplicantEvents(ssapApplicantEvents);
		currentApplication.setSsapApplicants(ssapApplicants);
		currentApplication.setSsapApplicationEvents(ssapApplicationEvents);
		return currentApplication;
		
	}
	
	private void setApplicantData(List<SsapApplicant> ssapApplicants,Map<String,Set<String>> disenrollMap){
		Set<String> disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental = disenrollMap.get(DISENROLL_FROM_DENTAL);
		for(SsapApplicant ssapApplicant : ssapApplicants){
			if(!StringUtils.isEmpty(ssapApplicant.getApplicantGuid()) && (disenrollFromHealth.contains(ssapApplicant.getApplicantGuid()) || disenrollFromDental.contains(ssapApplicant.getApplicantGuid()))){
				ssapApplicant.setEligibilityStatus("NONE");
			}
		}
		
	}
	
	private void setApplicationData(SsapApplication currentApplication,SsapApplication parentApplication) {
		currentApplication.setEligibilityStatus(parentApplication.getEligibilityStatus());
		currentApplication.setExchangeEligibilityStatus(parentApplication.getExchangeEligibilityStatus());
		currentApplication.setAllowEnrollment(parentApplication.getAllowEnrollment());
		currentApplication.setEligibilityReceivedDate(parentApplication.getEligibilityReceivedDate());
		currentApplication.setNativeAmerican(parentApplication.getNativeAmerican());
		currentApplication.setExemptHousehold(parentApplication.getExemptHousehold());
		currentApplication.setSsapApplicationSectionId(parentApplication.getSsapApplicationSectionId());
		currentApplication.setLastNoticeId(parentApplication.getLastNoticeId());
		currentApplication.setEnrollmentStatus(parentApplication.getEnrollmentStatus());
		currentApplication.setCsrLevel(parentApplication.getCsrLevel());
		currentApplication.setMaximumAPTC(parentApplication.getMaximumAPTC());
		currentApplication.setElectedAPTC(parentApplication.getElectedAPTC());
		currentApplication.setState(parentApplication.getState());
		currentApplication.setZipCode(parentApplication.getZipCode());
		currentApplication.setCountyCode(parentApplication.getCountyCode());
		currentApplication.setExternalApplicationId(parentApplication.getExternalApplicationId());
		currentApplication.setAvailableAPTC(parentApplication.getAvailableAPTC());
		currentApplication.setExchangeType(parentApplication.getExchangeType());
		currentApplication.setEhbAmount(parentApplication.getEhbAmount());
		
	}

	private SepRequestParamDTO createSepRequestParamDTOForDisenroll(SsapApplication parentApplication, List<SsapApplicant> ssapApplicants,Map<String,Set<String>> disenrollMap) throws InvalidUserException {
		SepRequestParamDTO sepRequestParamDTO = new SepRequestParamDTO();
		
		if(parentApplication.getEsignDate() != null) {
			sepRequestParamDTO.setEsignDate(parentApplication.getEsignDate().toString());
		}
		
		sepRequestParamDTO.setEsignFirstName(parentApplication.getEsignFirstName());
		sepRequestParamDTO.setEsignLastName(parentApplication.getEsignLastName());
		sepRequestParamDTO.setEsignMiddleName(parentApplication.getEsignMiddleName());
		sepRequestParamDTO.setEvents(getEvents(ssapApplicants,disenrollMap));
		sepRequestParamDTO.setHouseholdId(parentApplication.getCmrHouseoldId().longValue());
		sepRequestParamDTO.setSsapJSON(parentApplication.getApplicationData());
		//get exadmin user id 
		AccountUser accountUser =  userRepository.findByEmail(EXADMIN_USERNAME);
		sepRequestParamDTO.setUserId((long)accountUser.getId());
		sepRequestParamDTO.setUserName(EXADMIN_USERNAME);
		return sepRequestParamDTO;
	}
	
	
	private List<RequestParamDTO> getEvents(List<SsapApplicant> ssapApplicants,Map<String,Set<String>> disenrollMap) {
		Set<String> disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental = disenrollMap.get(DISENROLL_FROM_DENTAL);
		List<RequestParamDTO> requestParamDTOList = new ArrayList<RequestParamDTO>();
		RequestParamDTO requestParamDTO = new RequestParamDTO();
		requestParamDTO.setDataChanged(true);
		requestParamDTO.setEventCategory(DEPENDENT_CHILD_AGES_OUT);
		requestParamDTO.setEventSubCategory(DEPENDENT_CHILD_AGES_OUT);
		requestParamDTO.setEventSubCategoryDate(new Date().toString());
		List<ChangedApplicant> changedApplicants = new  ArrayList<ChangedApplicant>();
		
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid()) || disenrollFromDental.contains(ssapApplicant.getApplicantGuid())){
				ChangedApplicant changedApplicant = new ChangedApplicant();
				changedApplicant.setEventDate(new Date().toString());
				changedApplicant.setIsChangeInZipCodeOrCounty(false);
				changedApplicant.setPersonId(ssapApplicant.getPersonId());
				changedApplicants.add(changedApplicant);
			}
		}
		
		requestParamDTO.setChangedApplicants(changedApplicants);
		requestParamDTOList.add(requestParamDTO);
		return requestParamDTOList;
	}
	
	
	private void updateJson(SsapApplication ssapApplication,Map<String,Set<String>> disenrollMap) {
		Set<String> disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
		JsonParser parser = new JsonParser();
		Object obj = parser.parse(ssapApplication.getApplicationData());
		JsonObject jsonObject = (JsonObject) obj;
		JsonObject singleStreamlinedApplication = jsonObject.get(LifeChangeEventConstant.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
		JsonArray taxHousehold = singleStreamlinedApplication.getAsJsonArray(LifeChangeEventConstant.TAX_HOUSEHOLD);
		JsonArray householdMembers = taxHousehold.get(0).getAsJsonObject().get(LifeChangeEventConstant.HOUSEHOLD_MEMBER).getAsJsonArray();
		Iterator<JsonElement> itr = householdMembers.iterator();
		while(itr.hasNext()){
			JsonElement householdMember = itr.next();
			//get applicant guid 
			String applicantGuid = LifeChangeEventUtil.getJsonAsString(householdMember.getAsJsonObject().get(LifeChangeEventConstant.APPLICANT_GUID)); 
			if(!StringUtils.isEmpty(applicantGuid) && (disenrollFromHealth.contains(applicantGuid))){
				householdMember.getAsJsonObject().addProperty(LifeChangeEventConstant.APPLYING_FOR_COVERAGE_INDICATOR,false);
				//householdMember.getAsJsonObject().addProperty(LifeChangeEventConstant.ACTIVE, false);
			}
		}
		
		ssapApplication.setApplicationData(jsonObject.toString());
	}

	private void setVerifcations(List<SsapApplicant> parentApplicant,List<SsapApplicant> ssapApplicants){
		//create applicant map 
		Map<String,SsapApplicant> applicantMap = getApplicants(ssapApplicants);
		for(SsapApplicant parrentApplicant : parentApplicant){
			if(parrentApplicant.getOnApplication()!=null && parrentApplicant.getOnApplication().equals("Y")){
				if(applicantMap.get(parrentApplicant.getApplicantGuid()) != null){
					setSsapVertifcations(applicantMap.get(parrentApplicant.getApplicantGuid()),parrentApplicant);
				}
			}
		}
		
	}
	
	private Map<String,SsapApplicant> getApplicants(List<SsapApplicant> ssapApplicants){
		Map<String,SsapApplicant> applicantMap = new HashMap<String, SsapApplicant>();
		for(SsapApplicant ssapApplicant : ssapApplicants){
			applicantMap.put(ssapApplicant.getApplicantGuid(), ssapApplicant);
		}
		return applicantMap;
	}
	
	private void setSsapVertifcations(SsapApplicant ssapApplicant,SsapApplicant parentApplicant) {
		
		List<SsapVerification> ssapVerifications  = ssapVerificationRepository.findBySsapApplicant(parentApplicant);
		if(ssapVerifications !=null){
			for(SsapVerification ssapVerification :  ssapVerifications){
				ssapVerification.setId(0);
				ssapVerification.setCreationTimestamp(new Timestamp(new Date().getTime()));
				ssapVerification.setLastUpdateTimestamp(null);
				ssapVerification.setSsapApplicant(ssapApplicant);
				ssapVerification.setSsapVerificationGiwspayloads(null);
			}
		}
		if(ssapApplicant !=null){
			ssapApplicant.setSsapVerifications(ssapVerifications);
		}
		
	}

	

	private void saveEligibilityPrograms(List<SsapApplicant> ssapApplicants,List<SsapApplicant> parentApplicant,Map<String,Set<String>> disenrollMap) {
		//create applicant map 
		Map<String,SsapApplicant> applicantMap = getApplicants(ssapApplicants);
		for(SsapApplicant parrentApplicant : parentApplicant){
			if(parrentApplicant.getOnApplication()!=null && parrentApplicant.getOnApplication().equals("Y")){
				if(applicantMap.get(parrentApplicant.getApplicantGuid()) != null){
					setEligibilityPrograms(applicantMap.get(parrentApplicant.getApplicantGuid()),parrentApplicant.getId(),disenrollMap);
				}
			}
		}
		
	}
	
	private void setEligibilityPrograms(SsapApplicant ssapApplicant,long parentApplicantId,Map<String,Set<String>> disenrollMap) {
		Set<String> disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
		List<EligibilityProgram> eligibilityPrograms =  iEligibilityProgram.findBySsapApplicantId(parentApplicantId);
		if(eligibilityPrograms!= null){
			for(EligibilityProgram eligibilityProgram : eligibilityPrograms){
				if(!StringUtils.isEmpty(ssapApplicant.getApplicantGuid()) && (disenrollFromHealth.contains(ssapApplicant.getApplicantGuid()))){
					eligibilityProgram.setEligibilityIndicator(FALSE);
					eligibilityProgram.setIneligibleReason(NON_COMPLIANCE_FOR_VERIFICATIONS);
				}
				eligibilityProgram.setId(null);
				eligibilityProgram.setSsapApplicant(ssapApplicant);
				eligibilityProgram.setEligibilityProgramGiwspayload(null);
				iEligibilityProgram.save(eligibilityProgram);
			}
		}
		ssapApplicant.setEligibilityProgram(eligibilityPrograms);
		
	}

	public EnrolleeResponse invokeEnrollmentApi(long applicationId, String userName) throws RestClientException, Exception {
		LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
		lifeChangeEventDTO.setOldApplicationId(applicationId);
		lifeChangeEventDTO.setUserName(userName);
		EnrolleeResponse enrolleeResponse = invokeEnrollmentApi(lifeChangeEventDTO);
		return enrolleeResponse;
	}
	
	protected EnrolleeResponse invokeEnrollmentApi(LifeChangeEventDTO lifeChangeEventDTO) throws Exception {
		XStream xstream = GhixUtils.getXStreamStaxObject();
		EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
		enrolleeRequest.setSsapApplicationId(lifeChangeEventDTO.getOldApplicationId());
		//EnrolleeResponse enrolleeResponse = enrolleeService.findEnrolleeByApplicationid(enrolleeRequest);
		ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID, lifeChangeEventDTO.getUserName(), HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, xstream.toXML(enrolleeRequest));
		EnrolleeResponse enrolleeResponse = (EnrolleeResponse) xstream.fromXML(response.getBody());
		if (null != enrolleeResponse & enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
			return enrolleeResponse;
		} else {
			throw new GIException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
		}
	}
	
	private LifeChangeEventDTO extractEnrolledData(List<EnrollmentShopDTO> enrollmentShopDTOs, List<SsapApplicant> ssapApplicants) {
		Set<Long> personIds = new HashSet<>();
		LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO(); 
		for (EnrollmentShopDTO enrollmentShopDTO : enrollmentShopDTOs) {
			List<EnrolleeShopDTO> enrolleeShopDTO = enrollmentShopDTO.getEnrolleeShopDTOList();
			for (EnrolleeShopDTO enrolleeMembers : enrolleeShopDTO) {
				for (SsapApplicant ssapApplicant : ssapApplicants) {
					if (enrolleeMembers.getExchgIndivIdentifier().equals(ssapApplicant.getApplicantGuid())) {
						personIds.add(ssapApplicant.getPersonId());
					}
				}
			}
		}
		
		lifeChangeEventDTO.setPersonIds(personIds);
		
		return lifeChangeEventDTO;
	}
	
	private void updateDemographicMaps(Map<String, Boolean> demographicFlagMap) {
		
		demographicFlagMap.put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_DOB_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_OTHER_CHANGE, false);
		demographicFlagMap.put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, false);
		
	}
	
	public void processDisenrollment(SsapApplication ssapApplication) {
		Date coverageEndDate = getLastDayOfCurrentMonth().getTime();
		invokeDisenrollmentAPI(ssapApplication, coverageEndDate);
		/*if(response.equals(GhixConstants.RESPONSE_SUCCESS)){
			updateApplicationStatus(ssapApplication, ApplicationStatus.CLOSED);
		}*/
	}
	
	public String invokeDisenrollmentAPI(SsapApplication ssapApplication, Date terminationDate) {
		EnrollmentResponse enrollmentResponse;
		String responseValue = GhixConstants.RESPONSE_FAILURE;
		String enrollmentRequestXml = "";
		String enrollmentResponseXmlString = "";
		EnrollmentRequest enrollmentRequest = null;
		try {
			XStream xStream = GhixUtils.getXStreamStaxObject();
			enrollmentRequest = createRequestForDisenrollment(ssapApplication, terminationDate);
			LOGGER.info("Invoking  ind disenrollement Service for " + ssapApplication.getId());
			enrollmentRequestXml = xStream.toXML(enrollmentRequest);
			enrollmentResponseXmlString = (ghixRestTemplate.exchange(EnrollmentEndPoints.DISENROLL_BY_APPLICATION_ID_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, enrollmentRequestXml)).getBody();
			enrollmentResponse = (EnrollmentResponse) xStream.fromXML(enrollmentResponseXmlString);
			if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus()) && enrollmentResponse.getErrCode() == 200) {
				responseValue = GhixConstants.RESPONSE_SUCCESS;
			} else {
				responseValue = GhixConstants.RESPONSE_FAILURE;
				throw new GIRuntimeException("Error occured while invoking disenrollment api:"+ssapApplication.getCaseNumber());
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while invoking disenrollment api:"+ssapApplication.getCaseNumber(),e);
			throw new GIRuntimeException("Error occured while invoking disenrollment api:"+ssapApplication.getCaseNumber(), e);
		} 
		return responseValue;
	}

	private EnrollmentRequest createRequestForDisenrollment(SsapApplication enrolledApplication, Date terminationDate) {
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentDisEnrollmentDTO disEnrollDTO = new EnrollmentDisEnrollmentDTO();
		disEnrollDTO.setSsapApplicationid(enrolledApplication.getId());
		disEnrollDTO.setTerminationDate(formatDate(terminationDate));
		disEnrollDTO.setTerminationReasonCode(DISENROLLMENT_REASON_CODE);
		enrollmentRequest.setEnrollmentDisEnrollmentDTO(disEnrollDTO);
		return enrollmentRequest;
	}
	
	private String formatDate(Date date){
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_MM_DD_YYYY);
		return dateFormat.format(date);
	}
	
	private String formatDate(Date date,String pattern){
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}
	
   public List<Long> getSsapAppForAgeOut(Long coverageYear,String isFinancialAssistanceFlag,String applicationStatus){
	   EntityManager em = null;
	   List<String> enrollmentStatus = new ArrayList<String>();
	   enrollmentStatus.add(CANCEL);
	   enrollmentStatus.add(ABORTED);
	   List<Long> applicationList = new  ArrayList<Long>();
	   Query query = null;
	   StringBuilder sb =  new StringBuilder();
		try{
			sb.append(" SELECT DISTINCT S.ID ");
			sb.append(" FROM SSAP_APPLICATIONS S ");
			sb.append(" INNER JOIN ENROLLMENT E ");
			sb.append(" ON S.ID = E.SSAP_APPLICATION_ID ");
			sb.append(" INNER JOIN LOOKUP_VALUE LV1 ");
			sb.append(" ON E.ENROLLMENT_STATUS_LKP = LV1.LOOKUP_VALUE_ID ");
			sb.append(" WHERE S.COVERAGE_YEAR = :").append(COVERAGE_YEAR).append(" AND S.FINANCIAL_ASSISTANCE_FLAG = :").append(FINANCIAL_ASSISTANCE_FLAG).append(" AND S.APPLICATION_STATUS = :").append(APPLICATION_STATUS);
			sb.append(" AND LV1.LOOKUP_VALUE_CODE NOT IN :").append(ENROLLMENT_STATUS_LIST);
			sb.append(" AND E.BENEFIT_END_DATE > :").append(TERMINATION_DATE);
			em = emf.createEntityManager();
			query = em.createNativeQuery(sb.toString());	
			query.setParameter(COVERAGE_YEAR, coverageYear);
			query.setParameter(FINANCIAL_ASSISTANCE_FLAG, isFinancialAssistanceFlag);
			query.setParameter(APPLICATION_STATUS, applicationStatus);
			query.setParameter(ENROLLMENT_STATUS_LIST, enrollmentStatus);
			query.setParameter(TERMINATION_DATE, getLastDayOfCurrentMonth().getTime());
			List<BigDecimal> objList = query.getResultList();	
			Iterator<BigDecimal> iterator = objList.iterator();
			while (iterator.hasNext()) {			
				BigDecimal objArray = iterator.next();
				applicationList.add(objArray.longValue());			
			}
		}finally{
			if(em!=null && em.isOpen()){
				em.close();
			}
		}
		return applicationList;
   }
   
   private void readVerificationStatus(SsapApplicant ssapApplicant, String type, String status, Long vId){
		BigDecimal bigVid = BigDecimal.valueOf(vId);
		switch (type) {
		case "SSN":
			ssapApplicant.setSsnVerificationVid(bigVid);
			break;
		case "CITIZENSHIP":
			ssapApplicant.setCitizenshipImmigrationVid(bigVid);
			break;
		case "ELIGIBLE_IMMIGRATION_STATUS":
			ssapApplicant.setVlpVerificationVid(bigVid);
			break;
		case "ANNUAL_INCOME":
			ssapApplicant.setIncomeVerificationVid(bigVid);
			break;
		case "CURRENT_INCOME":
			ssapApplicant.setIncomeVerificationVid(bigVid);
			break;
		case "INCARCERATION_STATUS":
			ssapApplicant.setIncarcerationVid(bigVid);
			break;
		case "ESI_MEC":
			ssapApplicant.setMecVerificationVid(bigVid);
			break;
		case "DEATH":
			ssapApplicant.setDeathVerificationVid(bigVid);
			break;
		case "INCARCERATION":
			ssapApplicant.setIncarcerationVid(bigVid);
			break;
		case "RESIDENCY":
			ssapApplicant.setResidencyVid(bigVid);
			break;
		case "VLP":
			ssapApplicant.setVlpVerificationVid(bigVid);
			break;
		default:
			LOGGER.warn(EligibilityConstants.INVALID_VERIFICATION_TYPE_FOUND + type);
			break;
		}

	}
   
   private void triggerAgeOutNotice(SsapApplication ssapApplication,Map<String,Set<String>> disenrollMap,Map<String, EnrollmentShopDTO> enrollmentIdMap){
		AgeOutNoticeDTO ageOutNoticeDTO = new AgeOutNoticeDTO();
		List<AgeOutDependant> healthDependants = new ArrayList<AgeOutDependant>();
		List<AgeOutDependant> dentalDependants = new ArrayList<AgeOutDependant>();
		Set<String> disenrollFromHealth =  disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental =  disenrollMap.get(DISENROLL_FROM_DENTAL);
		EnrollmentShopDTO healthEnrollmentShopDTO  = enrollmentIdMap.get(HEALTH_ENROLLMENT_DTO);
		EnrollmentShopDTO dentalEnrollmentShopDTO  = enrollmentIdMap.get(DENTAL_ENROLLMENT_DTO);
		
		for(SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()){
			//disenroll from health
			if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())){
				createAgeOutDependent(healthDependants,healthEnrollmentShopDTO, ssapApplicant,26);
			}
			//disenroll from dental
			if(disenrollFromDental.contains(ssapApplicant.getApplicantGuid())){
				if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())){
					createAgeOutDependent(dentalDependants,dentalEnrollmentShopDTO, ssapApplicant,26);
				}else{
					createAgeOutDependent(dentalDependants,dentalEnrollmentShopDTO, ssapApplicant,19);
				}
			}
		}
		ageOutNoticeDTO.setApplicationID(ssapApplication.getCaseNumber());
		SsapApplicant primaryApplicant =  getPrimaryApplicantDB(ssapApplication);
		ageOutNoticeDTO.setPrimaryFirstName(primaryApplicant.getFirstName());
		ageOutNoticeDTO.setPrimaryLastName(primaryApplicant.getLastName());
		ageOutNoticeDTO.setEmailId(primaryApplicant.getEmailAddress());
		ageOutNoticeDTO.setsEPendDate(formatDate(getLastDayOfCurrentMonth().getTime(),DATE_FORMAT_MMMM_DD_YYYY));
		//ageOutNoticeDTO.setHouseholdId(ssapApplication.getCmrHouseoldId().intValue());
		if(!(healthDependants.isEmpty())){
			ageOutNoticeDTO.setHealthDependants(healthDependants);
		}
		if(!(dentalDependants.isEmpty())){
			ageOutNoticeDTO.setDentalDependants(dentalDependants);
		}
		sendNotification(ageOutNoticeDTO, AGE_OUT_DISENROLL_NOTIFICATION, ssapApplication);
	}
	
	
	
	public void sendNotification(AgeOutNoticeDTO ageOutNoticeDTO,String templateName,SsapApplication ssapApplication){
		List<String> sendToEmailList = new LinkedList<String>();;
		Map<String, Object> tokens = new HashMap<String, Object>();
		Location location = null;
		try {
			tokens.put("primaryApplicantName", ageOutNoticeDTO.getPrimaryFirstName()+ " " +ageOutNoticeDTO.getPrimaryLastName());
			tokens.put("ApplicationID", ssapApplication.getCaseNumber());
			tokens.put("Date", DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
			tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			tokens.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
			tokens.put("exgCityName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
			tokens.put("exgStateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
			tokens.put("zip",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
			tokens.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			tokens.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
			tokens.put("exchangeAddressEmail",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			tokens.put(TemplateTokens.EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			tokens.put("ageOutDependents",ageOutNoticeDTO);
			SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
			tokens.put("spanishDate", formatter.format(new Date()));
			tokens.put("caseNumber", ssapApplication.getCaseNumber());	
			tokens.put("ssapApplicationId", ssapApplication.getId());
			String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
			String fileName = templateName+"_" +String.valueOf(ssapApplication.getCmrHouseoldId())+ System.currentTimeMillis() + ".pdf";
			String relativePath = "cmr/" + String.valueOf(ssapApplication.getCmrHouseoldId()) + "/ssap/" + ssapApplication.getId() + "/notifications/"; 
			
			//preference communication logic
			PreferencesDTO preferencesDTO  = preferencesService.getPreferences(ssapApplication.getCmrHouseoldId().intValue(), false);
			location = getLocationFromDTO(preferencesDTO.getLocationDto());
			sendToEmailList.add(preferencesDTO.getEmailAddress());
			
			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, tokens, relativePath, fileName,
					moduleName, ssapApplication.getCmrHouseoldId().longValue(), sendToEmailList, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), 
					ageOutNoticeDTO.getPrimaryFirstName()+" "+ageOutNoticeDTO.getPrimaryLastName(),location,preferencesDTO.getPrefCommunication());
		} catch(Exception exception){
			LOGGER.error("Error occurred while sending age out notification:"+ssapApplication.getId(),exception);
		}
		
		
	}


	
	private Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
	

	private void createAgeOutDependent(List<AgeOutDependant> healthDependants,
				EnrollmentShopDTO healthEnrollmentShopDTO,SsapApplicant ssapApplicant,int age) {
		AgeOutDependant ageOutDependant= new AgeOutDependant();
		ageOutDependant.setFirstName(ssapApplicant.getFirstName());
		ageOutDependant.setLastName(ssapApplicant.getLastName());
		ageOutDependant.setPlanId(healthEnrollmentShopDTO.getPlanName());
		ageOutDependant.setDropOutDate(formatDate(getLastDayOfCurrentMonth().getTime(),DATE_FORMAT_MMMM_DD_YYYY));
		ageOutDependant.setDropOutReasonText(ssapApplicant.getFirstName()+ " will be "+age+" years old on %s", getAgeOutDate(ssapApplicant.getBirthDate(),age));
		healthDependants.add(ageOutDependant);
	}
	
	
	
	private String getAgeOutDate(Date birthDate,int age) {
		Calendar cal = Calendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		cal.setTime(birthDate);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.add(Calendar.YEAR, age);
		//if birth year is leap year add 1 day
		if(!(new GregorianCalendar().isLeapYear(currentYear)) && day == 29 && month == 1){
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return formatDate(cal.getTime(), DATE_FORMAT_MMMM_DD_YYYY);
	}
	
	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.getPersonId() == 1){
				primaryApplicantDB = applicant;
				break;
			}
		}
		return primaryApplicantDB;
	}
   
}
