<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page
	import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page
	import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page
	import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@page import="java.util.Properties"%>
<%@page import="org.springframework.context.ApplicationContext"%>
<%!private static ApplicationContext ctx = null;%>
<%!private static Properties prop = null;%>
<%
	if (ctx == null) {
		ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	}
	if (prop == null) {
		prop = (Properties) ctx.getBean("configProp");
	}
%>
<script src="//www.google.com/recaptcha/api.js?render=explicit&onload=vcRecaptchaApiLoaded" async defer></script>
<script type="text/javascript">
var captchaKey = '<%=prop.getProperty("Recaptcha.siteKey")%>';
</script>


<div class="gutter10" ng-app="securityQuestionApp"
	ng-controller="securityQuestionController" ng-cloak>
	<h1>
		<spring:message code="label.dynamicSecurityQuestion.referralLinking" />
	</h1>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>
					<spring:message code="label.dynamicSecurityQuestion.process" />
				</h4>
			</div>
			<ul class="nav nav-list">
				<li class="active"><spring:message
						code="label.dynamicSecurityQuestion.securityQuestions" /></li>
				<li><a href="#"><spring:message
							code="label.dynamicSecurityQuestion.linkApplication" /></a></li>
			</ul>
		</div>
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4>
					<spring:message
						code="label.dynamicSecurityQuestion.securityQuestions" />
				</h4>
			</div>
			<div class="gutter10">
				<div class="alert alert-info">
					<ul>
						<li><spring:message
								code="label.dynamicSecurityQuestion.privacy" /></li>
						<li><spring:message
								code="label.dynamicSecurityQuestion.successfullyAnswered" /></li>
					</ul>
				</div>
				<% 
				 Object fieldErrorObj = session.getAttribute("fieldErrors");
				 if(fieldErrorObj != null)
		         	{
		        	 %>
		        	 <c:forEach items="${fieldErrors}" var="msg">
							<font color="red">${msg.code}</font><br/>
					</c:forEach>
					<%         	 
		         	session.removeAttribute("fieldErrors");
		        	 }
		        	 %>
		        	 <br>
				<form id="dynamicSecurityQuestions" name="dynamicSecurityQuestions"
					action="<c:url value="/referral/dynamicsecurityquestions"/>"
					method="POST" class="form-horizontal" novalidate>
					<df:csrfToken />
					<div class="control-group" ng-if="randomNumber.indexOf('1') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.firstName" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="firstName" class="input-large"
								ng-pattern="/^[a-zA-Z ,.'-]{1,45}$/" required
								ng-model="answers.firstName" />

							<div class="help-inline" id="firstName_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.firstName.$dirty && dynamicSecurityQuestions.firstName.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.firstNameError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.firstName.$dirty && dynamicSecurityQuestions.firstName.$error.pattern">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.firstNameError2" /></span>
								</label>
							</div>
						</div>
					</div>

					<div class="control-group" ng-if="randomNumber.indexOf('2') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.lastName" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="lastName" class="input-large"
								ng-pattern="/^[a-zA-Z ,.'-]{1,45}$/" required 
								ng-model="answers.lastName" />

							<div class="help-inline" id="lastName_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.lastName.$dirty && dynamicSecurityQuestions.lastName.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.lastNameError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.lastName.$dirty && dynamicSecurityQuestions.lastName.$error.pattern">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.lastNameError2" /></span>
								</label>
							</div>
						</div>
					</div>

					<div class="control-group" ng-if="randomNumber.indexOf('3') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.dob" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="birthDate" class="input-medium"
								date-validation="birthDate" ui-mask="99/99/9999"
								model-view-value="true" required ng-model="answers.birthDate"
								placeholder="MM/DD/YYYY" />

							<div class="help-inline" id="birthDate_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.birthDate.$dirty && dynamicSecurityQuestions.birthDate.$error.required">
									<span> <em class="excl">!</em>
									<spring:message code="label.dynamicSecurityQuestion.dobError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.birthDate.$dirty && dynamicSecurityQuestions.birthDate.$error.birthDate">
									<span> <em class="excl">!</em>
									<spring:message code="label.dynamicSecurityQuestion.dobError2" /></span>
								</label>
							</div>
						</div>
					</div>


					<!-- RANDOM STARTS -->


					<div class="control-group" ng-if="randomNumber.indexOf('4') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.ssn" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="hidden" name="ssn" ng-value="answers.ssnMask.substring(0, 3)+'-'+answers.ssnMask.substring(3, 5)+'-'+answers.ssnMask.substring(5, 9)">
							<input type="text" name="ssnMask" class="input-medium"
								ui-mask="sss-ss-ssss" required update-ssn-view 
								ng-model="answers.ssnMask" value="{{answers.ssnMask | ssnMask}}"/>
							<div class="help-inline" id="ssn_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.ssnMask.$dirty && dynamicSecurityQuestions.ssnMask.$error.required">
									<span> <em class="excl">!</em>
									<spring:message code="label.dynamicSecurityQuestion.ssnError1" /></span>
								</label>
							</div>
						</div>
					</div>

					<div class="control-group" ng-if="randomNumber.indexOf('5') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.zipCode" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="zipCode" class="input-small"
								ng-pattern="/^[0-9]{5}$/" required ng-model="answers.zipCode" maxlength="5"/>

							<div class="help-inline" id="zipCode_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.zipCode.$dirty && dynamicSecurityQuestions.zipCode.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.zipCodeError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.zipCode.$dirty && dynamicSecurityQuestions.zipCode.$error.pattern">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.zipCodeError2" /></span>
								</label>
							</div>
						</div>
					</div>

					<div class="control-group" ng-if="randomNumber.indexOf('6') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.gender" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<label class="help-inline margin5-t"><input type="radio"
								name="gender" value="male" required ng-model="answers.gender" />
							<spring:message code="label.dynamicSecurityQuestion.male" /></label> <label
								class="help-inline margin5-t"><input type="radio"
								name="gender" value="female" required ng-model="answers.gender" />
							<spring:message code="label.dynamicSecurityQuestion.female" /></label>
						</div>
					</div>


					<div class="control-group" ng-if="randomNumber.indexOf('7') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.email" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="email" name="emailAddress"
								class="input-large" required ng-model="answers.emailAddress" />

							<div class="help-inline" id="emailAddress_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.emailAddress.$dirty && dynamicSecurityQuestions.emailAddress.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.emailError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.emailAddress.$dirty && dynamicSecurityQuestions.emailAddress.$error.emailAddress">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.emailError2" /></span>
								</label>
							</div>
						</div>
					</div>


					<div class="control-group" ng-if="randomNumber.indexOf('8') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.phone" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="phoneNumber" class="input-medium"
								ui-mask="(999) 999-9999" model-view-value="true" required
								ng-model="answers.phoneNumber" />

							<div class="help-inline" id="phoneNumber_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.phoneNumber.$dirty && dynamicSecurityQuestions.phoneNumber.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.phoneError1" /></span>
								</label>
							</div>
						</div>
					</div>


					<div class="control-group" ng-if="randomNumber.indexOf('9') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.county" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="county" class="input-medium"
								ng-pattern="/^[a-zA-Z .]+$/" required ng-model="answers.county" />

							<div class="help-inline" id="county_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.county.$dirty && dynamicSecurityQuestions.county.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.countyError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.county.$dirty && dynamicSecurityQuestions.county.$error.pattern">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.countyError2" /></span>
								</label>
							</div>
						</div>
					</div>


					<div class="control-group" ng-if="randomNumber.indexOf('10') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.numberOfMembers" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="householdTotal" class="input-small"
								ng-pattern="/^[0-9]+$/" required
								ng-model="answers.housholdTotal" />

							<div class="help-inline" id="householdTotal_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.householdTotal.$dirty && dynamicSecurityQuestions.householdTotal.$error.required">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.numberOfMembersError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.householdTotal.$dirty && dynamicSecurityQuestions.householdTotal.$error.pattern">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.numberOfMembersError2" /></span>
								</label>
							</div>
						</div>
					</div>


					<div class="control-group" ng-if="randomNumber.indexOf('11') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.medicaidId" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="medicaidId" class="input-xlarge"
								ng-focus ng-pattern="/^[0-9a-zA-Z-]{1,30}$/" required
								ng-model="answers.medicaidId" />

							<div class="help-inline" id="medicaidId_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.medicaidId.$dirty && dynamicSecurityQuestions.medicaidId.$error.required && !dynamicSecurityQuestions.medicaidId.$focused">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.medicaidIdError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.medicaidId.$dirty && dynamicSecurityQuestions.medicaidId.$error.pattern && !dynamicSecurityQuestions.medicaidId.$focused">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.medicaidIdError2" /></span>
								</label>
							</div>
						</div>
					</div>

					<div class="control-group" ng-if="randomNumber.indexOf('12') > -1">
						<label class="control-label"> <spring:message
								code="label.dynamicSecurityQuestion.applicationCaseNumber" /> <img
							src='<c:url value="/resources/images/requiredAsterix.png" />'
							width="10" height="10" alt="Required!" />
						</label>
						<div class="controls">
							<input type="text" name="caseNumber" class="input-xlarge"
								ng-focus ng-pattern="/^[0-9]*$/" required
								ng-model="answers.caseNumber" />

							<div class="help-inline" id="caseNumber_error">
								<label class="error"
									ng-if="dynamicSecurityQuestions.caseNumber.$dirty && dynamicSecurityQuestions.caseNumber.$error.required && !dynamicSecurityQuestions.caseNumber.$focused">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.applicationCaseNumberError1" /></span>
								</label> <label class="error"
									ng-if="dynamicSecurityQuestions.caseNumber.$dirty && dynamicSecurityQuestions.caseNumber.$error.pattern && !dynamicSecurityQuestions.caseNumber.$focused">
									<span> <em class="excl">!</em>
									<spring:message
											code="label.dynamicSecurityQuestion.applicationCaseNumberError2" /></span>
								</label>
							</div>
						</div>
					</div>

					<!-- RANDOM ENDS -->


					<!-- CAPTCHA STARTS -->
					
					<sec:authorize access="!isAuthenticated()"
						var="isUserAuthenticated">
						<div vc-recaptcha key="siteKey" class="inline-block" style="margin-left: 80px"></div>
					</sec:authorize>
					<!-- CAPTCHA ENDS -->
					<div class="form-actions">
						<input type="submit" name="submit" id="submit" value="Submit"
							class="btn btn-primary"
							ng-disabled="dynamicSecurityQuestions.$invalid">
					</div>
					<input type="hidden" value="${randomList}" id="randomNumber">
				</form>

			</div>
		</div>
	</div>
</div>


<script src="<c:url value="/resources/angular/mask.js" />"></script>
<script src="<c:url value="/resources/js/moment.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/angular-recaptcha.min.js" />"></script>
<script
	src='<c:url value="/resources/js/dynamic_question/dynamicSecurityQuestion.js" />'></script>
