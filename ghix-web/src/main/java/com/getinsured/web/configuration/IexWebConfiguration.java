package com.getinsured.web.configuration;

import java.util.Locale;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

/**
 * IEX Web configuration.
 *
 * @author Yevgen Golubenko
 * @since 5/27/19
 */
@EnableWebMvc
@Configuration
@Import({ I18nConfiguration.class })
public class IexWebConfiguration extends WebMvcConfigurerAdapter {
  private static final Logger log = LoggerFactory.getLogger(IexWebConfiguration.class);

  private static final String LOCALE_CHANGE_PARAM = "lang";
  private static final String LOCALE_COOKIE_NAME = LOCALE_CHANGE_PARAM;

  @PostConstruct
  public void postConstruct() {
    log.info("Initialized [IexWebConfiguration]");
  }

  /**
   * Locale resolver configuration.
   *
   * @return configured session-based {@link SessionLocaleResolver}.
   */
  @Bean
  public LocaleResolver localeResolver() {
    CookieLocaleResolver localeResolver = new CookieLocaleResolver();
    localeResolver.setDefaultLocale(Locale.ENGLISH);
    localeResolver.setCookieHttpOnly(true);
    localeResolver.setCookieName(LOCALE_COOKIE_NAME);

    return localeResolver;
  }

  @Bean
  @Primary
  public LocaleChangeInterceptor localeChangeInterceptor() {
    LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
    localeChangeInterceptor.setParamName(LOCALE_CHANGE_PARAM);
    return localeChangeInterceptor;
  }

  @Override
  public void addInterceptors(InterceptorRegistry registry) {
    registry.addInterceptor(localeChangeInterceptor());
  }
}
