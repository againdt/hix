<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.8.2.min.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script> 
 <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
  

<style>
body {background: #fff !important;}
</style>
	<div class="gutter10">
	<div class="row-fluid"> <div class="span12 gutter20-lr">
		<h3 class="margin30-lr">
			<spring:message code="label.assister.header1"/>
		</h3></div>
	</div>
	<div class="row-fluid">
	<div class="gutter10">
	
	<div class="span12">
			<form class="form-horizontal repsonseForm" id="frmDesignateAssisterEsign" name="frmDesignateAssisterEsign" action="eSignature" method="POST">
			 <df:csrfToken/>
			<input type="hidden" name="id" id="id" value="<encryptor:enc value="${assisterId}"/>"/>
				<div class="gutter20-lr margin20-l">
					<h4><spring:message code="label.assister.header4"/>&#58; ${assister.firstName} ${assister.lastName}</h4>
					<div class="control-group gutter10 margin0">
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox"	class="terms" name="terms1" id="terms1" value="1">
								<spring:message code="label.assister.terms1"/>
							</label>    
							<div id="terms1_error"></div>
						</div>
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox" class="terms" name="terms2" id="terms2" value="2">
								<spring:message code="label.assister.terms2"/>
							</label>
							<div id="terms2_error"></div>
						</div>
						<div class="control-group">
							<label class="checkbox"> <input type="checkbox" class="terms" name="terms3" id="terms3" value="3">
								<spring:message code="label.assister.terms3"/>
							</label>
							<div id="terms3_error"></div>
						</div>
					</div>
				</div>

				<h4 class="graydrkbg gutter10"><spring:message code="label.assister.signature"/></h4>

				<div class="control-group">
					<label class="control-label"><spring:message code="label.assister.applicantname"/></label>
					<div class="controls">
						<input type="hidden" class="input-xlarge" id="appName" name="appName" value="${applicantName}"> 
						<label class="paddingT5">${applicantName}</label>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message code="label.assister.applicantsignature"/></label>
					<div class="controls">
						<input type="text" class="input-xlarge" id="esignName"  name="esignName" size="30"> <br> 
						<small><spring:message code="label.assister.youresignature"/></small>
						<div id="esignName_error"></div>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label"><spring:message code="label.assister.Todaydate"/></label>
					<div class="controls">
						<input type="text" readonly class="input-mini" value="${month}">
						<input type="text" readonly class="input-mini" value="${day}">
						<input type="text" readonly class="input-mini" value="${year}">
					</div>
				</div>
				<hr>
				<div>
					<input type="submit" name="submitButton" id="submitButton" class="btn btn-primary pull-right responseBtn margin20-r" value="<spring:message code="label.assister.confirmbtn"/>" />
				</div>
			</form>
		</div>
		</div>
</div>
</div>
<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->
<script type="text/javascript">

	$('.responseBtn').click(function(){
		if($('.repsonseForm').valid() == true){
			setTimeout( "$('.responseBtn').attr('disabled','disabled');",50 );
		}
	
	});

	 var validator = $("#frmDesignateAssisterEsign").validate({
						rules : {
							esignName : {
								required : true,
								esignNameCheck : true
							},
							terms1 : {
								required : true
							},
							terms2 : {
								required : true
							},
							terms3 : {
								required : true
							}
						},
						messages : {
							esignName : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.esignaturevalidationmsg.esignnamereq'/></span>",
								esignNameCheck : "<span> <em class='excl'>!</em><spring:message code='label.esignaturevalidationmsg.esignnamecheck'/></span>"
							},
							terms1 : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.esignaturevalidationmsg.termsreq'/></span>"
							},
							terms2 : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.esignaturevalidationmsg.termsreq'/></span>"
							},
							terms3 : {
								required : "<span> <em class='excl'>!</em><spring:message code='label.esignaturevalidationmsg.termsreq'/></span>"
							}
						},
						errorClass: "error",
						errorPlacement: function(error, element) {
							var elementId = element.attr('id');
							error.appendTo( $("#" + elementId + "_error"));
							$("#" + elementId + "_error").attr('class','error help-inline');
						} 
					});
 

	jQuery.validator.addMethod("esignNameCheck", function(value, element, param) {
		appName = $("#appName").val();
		confirmEsignName = $("#esignName").val();
		
		if($("#appName").val().toLowerCase() === $("#esignName").val().toLowerCase()){
			return true;
		}
		return false;
		/* if (validateText(appName) != validateText(confirmEsignName)) {
			return false;
		} 
		return true; */
	});
	
/* 	function validateText(textVal) {
		if(textVal != null) {
			return textVal.toUpperCase().trim();
		}
	} */
</script>
