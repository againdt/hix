<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.openEnrollmentEnd}" var="enollEndDate" />
<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.coverageDateStart}" var="coverageEffDt" />

 <!-- Getting Started v2: previously Enrolled  -->
 <p><spring:message  code="label.prevselect"/> ${employerName} <spring:message  code="label.coverageenroll"/> ${enollEndDate}.  <spring:message  code="label.selectnext"/></p>
             
             <h4 class="lightgray">${employerName} <spring:message  code="label.sponsoredhealthcov"/></h4>
             <table class="table table-border-none">
             	<tbody>
             		<tr>
             			<td><spring:message  code="label.coveredlives"/></td>
             			<td colspan="2"><strong>${coveredLives}</strong></td>
             		</tr>
             		<tr>
             			<td><spring:message  code="label.coveragestarts"/></td>
             			<td colspan="2"><strong>${coverageEffDt}</strong></td>
		            </tr>
             		<tr>
             			<td><spring:message  code="label.planid"/></td>
             			<td colspan="2"><strong>${empPlans.planId}</strong></td>
             		</tr>
            		<tr>
             			<td><img src='<c:url value="/resources/img/logo_${issuerText}.png" />' alt="Logo ${planObj.issuer.name}" /><br></td>
             			<td>${planObj.name}<br><h4 class="planType silver"><small>${planObj.planHealth.planLevel}</small></h4></td>
             			<td><spring:message  code="label.monthlyprem"/><br><spring:message  code="label.employercontribution"/></td>
             			<td class="txt-right"><strong>$${empPlans.totalPremium}<br>-$${employerPays} </strong></td>
             		</tr>
		            <tr>            
			             <td>&nbsp;</td>
			             <td>&nbsp;</td>
			             <td>&nbsp;</td>
			             <td>&nbsp;</td>
		            </tr>
					<tr class="lightgray">            
						<td></td>
						<td></td>
						<td><h4><spring:message  code="label.monthlypaydeduct"/></h4></td>
						<td class="txt-right"><h4>$${empPlans.totalPremium - employerPays} / month</h4></td>
					</tr>
             	</tbody>
             </table>
             <div class="form-actions margin-top50">
             	<a href="verifyemployee" class="btn btn-primary pull-right"> <spring:message  code="label.editselection"/> </a>
             </div> 


