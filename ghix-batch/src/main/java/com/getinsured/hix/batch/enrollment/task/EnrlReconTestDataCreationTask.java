package com.getinsured.hix.batch.enrollment.task;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.EnrlReconTestDataCreationService;
import com.getinsured.hix.platform.util.exception.GIException;

public class EnrlReconTestDataCreationTask implements Tasklet {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconTestDataCreationTask.class);
	private EnrlReconTestDataCreationService enrlReconTestDataCreationService;
	private JobService jobService;
	private String hiosIssuerIds;
	private String strYear;
	private int applicableYear;

	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
		if(null != enrlReconTestDataCreationService){
			setApplicableYear(); 
			LOGGER.trace(" EnrlReconTestDataCreationTask calling enrlReconTestDataCreationService ");
			long jobExecutionId=-1;
			jobExecutionId=chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			enrlReconTestDataCreationService.generateTestDataForIssuer(getHiosIdList(), applicableYear, jobService, jobExecutionId);
		}else{
			LOGGER.error(" enrlReconTestDataCreationService is null ");
			throw new RuntimeException("enrlReconTestDataCreationService is null") ;
		}
		return RepeatStatus.FINISHED;
	}
	
	private List<String> getHiosIdList() {
		String[] hiosIdArr =hiosIssuerIds.split("[|]");
		return Arrays.asList(hiosIdArr);
	}

	/**
	 * @return the enrlReconTestDataCreationService
	 */
	public EnrlReconTestDataCreationService getEnrlReconTestDataCreationService() {
		return enrlReconTestDataCreationService;
	}

	/**
	 * @param enrlReconTestDataCreationService the enrlReconTestDataCreationService to set
	 */
	public void setEnrlReconTestDataCreationService(EnrlReconTestDataCreationService enrlReconTestDataCreationService) {
		this.enrlReconTestDataCreationService = enrlReconTestDataCreationService;
	}

	/**
	 * @return the jobService
	 */
	public JobService getJobService() {
		return jobService;
	}

	/**
	 * @param jobService the jobService to set
	 */
	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	/**
	 * @return the hiosIssuerIds
	 */
	public String getHiosIssuerIds() {
		return hiosIssuerIds;
	}

	/**
	 * @param hiosIssuerIds the hiosIssuerIds to set
	 */
	public void setHiosIssuerIds(String hiosIssuerIds) {
		this.hiosIssuerIds = hiosIssuerIds;
	}

	/**
	 * @return the strYear
	 */
	public String getStrYear() {
		return strYear;
	}

	/**
	 * @param strYear the strYear to set
	 */
	public void setStrYear(String strYear) {
		this.strYear = strYear;
	}
	
	private void setApplicableYear() throws GIException{
		int intYear = 0;
		Calendar calObj = Calendar.getInstance();
		int currentYear = calObj.get(Calendar.YEAR);
		if(NumberUtils.isNumber(strYear)){
			intYear = Integer.parseInt(strYear);
			applicableYear = intYear;
//			if(intYear <= currentYear){
//				applicableYear = intYear;
//			}else{
//				throw new GIException("Please provide valid month(mm) in the range [1,12] (Jan - Dec) and year <= current year");
//			}
		}else if(null == strYear){
			applicableYear = currentYear;
		}

	}

}
