/**
 * 
 */
package com.getinsured.iex.household.service;

import com.getinsured.hix.dto.indportal.PreferencesDTO;

/**
 * @author vishwanath_s
 *
 */
public interface PreferencesService {

	PreferencesDTO getPreferences(Integer householdId, boolean editMode);
	
}
