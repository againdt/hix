package com.getinsured.eligibility.prescreen.calculator;

import org.springframework.stereotype.Component;

/**
 * This class contains the implementation of the APTC calculator.
 * <p>
 * APTC calculation is based on the Benchmark Plan premium, which is obtained
 * from the teaser plan API. It also depends on the household income, FPL based
 * applicable percentage.
 * </p>
 * 
 * @author Sunil Desu
 * @since April 18 2013
 * 
 */
@Component
public class AptcCalculator {

	/*private static Logger LOGGER = Logger.getLogger(AptcCalculator.class);

	private double maxAptc = 0.0;
	private double fpl = 0.0;
	private double applicablePercentage = 0.0;
	private double benchmarkPlanPremium = 0.0;

	private double householdIncome = 0.0;
	private PrescreenProfile prescreenProfile;
	private PrescreenHousehold prescreenHousehold;
	private PrescreenDeductions prescreenDeductions;
	//private PrescreenRequest prescreenRequest;

	private FplRequest fplRequest = new FplRequest();

	@Autowired
	private FplCalculator fplCalculator = new FplCalculator();

	@Autowired
	private MagiCalculator magiCalculator;

	private static final String INVALID_APTC_PARAM = "Invalid APTC object passed to APTC calculator.";
	private static final String INVALID_PROFILE_PARAM = "Invalid PrescreenProfile object passed to APTC calculator.";
	private static final String INVALID_HOUSEHOLD_PARAM = "Invalid PrescreenHousehold object passed to APTC calculator.";
	private static final String INVALID_DEDUCTIONS_PARAM = "Invalid PrescreenDeductions object passed to APTC calculator.";
	private static final String INVALID_STATE_CODE = "Invalid State code passed to APTC calculator.";

	public PrescreenRequest calculateAptc(PrescreenRequest aptcRequest) {
		LOGGER.info("APTC calculator invoked.");
		try {

			// Validating request
			validateAptcRequest(aptcRequest);

			// Calculating MAGI household income
			// if (prescreenProfile.getIsIncomePageLatest()) {
			// prescreenRequest.setPrescreenHousehold(prescreenHousehold);
			// prescreenRequest.setPrescreenDeductions(prescreenDeductions);
			// householdIncome = magiCalculator
			// .calculateMagi(prescreenRequest);
			// } else {
			// //prescreenRequest.setPrescreenProfile(prescreenProfile);
			// //prescreenRequest.setPrescreenDeductions(prescreenDeductions);
			// householdIncome = magiCalculator
			// .calculateMagi(aptcRequest);
			// }
			householdIncome = magiCalculator.calculateMagi(aptcRequest);
			if (householdIncome <= 0.0) {
				maxAptc = 0.0;
				aptcRequest.setAptcValue(Math.round(maxAptc));
				aptcRequest.setFplValue(0.0);
				aptcRequest.setApplicablePercentage(2.0);
				return aptcRequest;
			}
			LOGGER.info("Total/MAGI household Income for the given household is "
					+ householdIncome);

			// Calculating FPL
			fplRequest.setFamilyIncome(householdIncome);
			fplRequest.setFamilySize(prescreenHousehold.getHouseholdMembers()
					.size());
			fplRequest.setState(aptcRequest.getPrescreenProfile().getStateCode());
			fpl = fplCalculator.calculateFpl(fplRequest,false,0).getFplPercentage();
			LOGGER.info("FPL for the given household is " + fpl);
			aptcRequest.setFplValue(fpl);
			if (fpl > 420.0) {
				maxAptc = 0.0;
				aptcRequest.setAptcValue(Math.round(maxAptc));
				aptcRequest.setApplicablePercentage(0.0);
				return aptcRequest;
			}
			// Calculating applicable percentage
			calculateApplicablePercentage(fpl);
			LOGGER.info("The Applicable percentage for the given household is "
					+ applicablePercentage);
			aptcRequest.setApplicablePercentage(applicablePercentage);

			LOGGER.info("Number of people in the household seeking coverage is "
					+ prescreenHousehold.getNumberOfMembersSeekingCoverage());

			// Calculating benchmark plan premium
			// TODO Teaser plan API integration
			//benchmarkPlanPremium = 300.0 * 12;
			benchmarkPlanPremium = aptcRequest.getBenchmarkPlanPremium()*12.0;
			LOGGER.info("BenchmarkPlanPremium for the given household is "
					+ benchmarkPlanPremium);

			// Calculating Max APTC
			maxAptc = benchmarkPlanPremium
					- (householdIncome * applicablePercentage / 100);
			LOGGER.info("Max Aptc for the given household is " + maxAptc);

		} catch (InvalidParameterException ipe) {

			LOGGER.info("Invalid arguments passed to APTC calculator. "
					+ ipe.getMessage());
			maxAptc = 0.0;
			aptcRequest.setFplValue(0.0);
			aptcRequest.setApplicablePercentage(0.0);
		} catch (Exception ex) {

			LOGGER.info("Exception occurred during APTC calculation."
					+ ex.getMessage());
			maxAptc = 0.0;
			aptcRequest.setFplValue(0.0);
			aptcRequest.setApplicablePercentage(0.0);

		}
		aptcRequest.setAptcValue(Math.round(maxAptc));
		return aptcRequest;
	}

	private void validateAptcRequest(PrescreenRequest aptcRequest) {
		if (aptcRequest == null) {
			throw new InvalidParameterException(INVALID_APTC_PARAM);
		}

		prescreenProfile = aptcRequest.getPrescreenProfile();

		if (prescreenProfile == null) {
			throw new InvalidParameterException(INVALID_PROFILE_PARAM);
		}
		if(null == prescreenProfile.getStateCode() || prescreenProfile.getStateCode().trim().length()<=0){
			throw new InvalidParameterException(INVALID_STATE_CODE);			
		}
		prescreenHousehold = aptcRequest.getPrescreenHousehold();

		if (prescreenHousehold == null
				|| prescreenHousehold.getHouseholdMembers() == null
				|| prescreenHousehold.getHouseholdMembers().size() == 0) {
			throw new InvalidParameterException(INVALID_HOUSEHOLD_PARAM);
		}

		prescreenDeductions = aptcRequest.getPrescreenDeductions();

		if (prescreenDeductions == null) {
			throw new InvalidParameterException(INVALID_DEDUCTIONS_PARAM);
		}

	}

	*//**
	 * This method computes the Applicable Percentage value based on the FPL
	 * value for the household.
	 * 
	 * @author Sunil Desu
	 * @since April 18 2013
	 * 
	 * @param fpl
	 *            value for the household
	 *//*
	private void calculateApplicablePercentage(double fpl) {
		if (fpl < 133.0) {
			applicablePercentage = 2.0;
		} else if (fpl >= 133.0 && fpl < 150.0) {
			applicablePercentage = computePercentage(133.0, 150.0, fpl, 3.0,
					4.0);
		} else if (fpl >= 150.0 && fpl < 200.0) {
			applicablePercentage = computePercentage(150.0, 200.0, fpl, 4.0,
					6.3);
		} else if (fpl >= 200.0 && fpl < 250.0) {
			applicablePercentage = computePercentage(200.0, 250.0, fpl, 6.3,
					8.05);
		} else if (fpl >= 250.0 && fpl < 300.0) {
			applicablePercentage = computePercentage(250.0, 300.0, fpl, 8.05,
					9.5);
		} else if (fpl >= 300.0 && fpl < 420.0) {
			applicablePercentage = 9.5;
		}
	}

	private double computePercentage(double lowerLimit, double upperLimit,
			double actualFpl, double lowerLimitPercentage,
			double upperLimitPercentage) {
		double applPercent = 0.0;
		applPercent = lowerLimitPercentage
				+ ((upperLimitPercentage - lowerLimitPercentage)
						* (actualFpl - lowerLimit) / (upperLimit - lowerLimit));
		return applPercent;
	}*/
}
