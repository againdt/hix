package com.getinsured.iex.hub.vlp.validator;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;

/**
 * Validator for I94 number
 * 
 * @author Nikhil Talreja
 * @since 31-Jan-2014
 *
 */
public class I94NumberValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I94NumberValidator.class);
	
	/** 
	 * Validations for I94 number
	 * 1. Should be a number
	 * 2. Should be exactly 11 digits
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		String i94Number = null;
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		else{
			i94Number = value.toString();
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		if(i94Number.length() < VLPServiceConstants.I94_NUMBER_MIN_LEN
				|| i94Number.length() > VLPServiceConstants.I94_NUMBER_MAX_LEN){
			throw new InputDataValidationException(VLPServiceConstants.I94_NUMBER_LEN_ERROR_MESSAGE);
		}
		
		try{
			Long.parseLong(i94Number);
		}
		catch(NumberFormatException e){
			throw new InputDataValidationException(name + ": "+VLPServiceConstants.NON_NUMERIC_ERROR_MESSAGE, e);
		}
		
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
