package com.getinsured.hix.platform.config;

/**
 * Contains configuration properties for HUB 
 * 
 * @author Nikhil Talreja
 * @since 25-Mar-2014
 *
 */
public class HubConfiguration {
	
	public enum HubConfigurationEnum implements PropertiesEnumMarker
	{  
		HUB_SSAC_QC_INDICATOR ("hub.ssac.qcIndicator"),
		HUB_RIDP_PROOFING_LEVEL ("hub.ridp.levelOfProofingCode");
		
		private final String value;	  
		@Override
		public String getValue(){return this.value;}
		HubConfigurationEnum(String value){
	        this.value = value;
	    }
	};

}
