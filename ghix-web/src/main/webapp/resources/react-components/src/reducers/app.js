const INITIAL_STATE = {
    stateCode: $("#stateCode").val(),
    userRole: $("#userActiveRoleName").val(),
    superUserRole: $("#userActiveRoleLabel").val(),
    defaultRoleName: $("#defaultRoleName").val()
};

function app(state=INITIAL_STATE, {type, payload}) {
    switch (type) {
        default:
            return state;
    }
}

export default app;