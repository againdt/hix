import React, {Fragment} from 'react';
import {FormattedMessage} from 'react-intl';

/**
 * This Component creates Pageheader for every page
 */
const PageHeader = props => {
    const {
        additionalElement,
        additionalElementClassName,
        additionalElementStyle,
        auxClassName,
        auxText,
        headerClassName,
        text,
        textCode
    } = props;

    return (
        <Fragment>
            <div className="page-header usa-grid">
                <h3
                    className={`${headerClassName}`}
                >
                    {textCode &&
                        <FormattedMessage id={`${textCode}`} />
                    }
                    {!textCode &&
                        text
                    } &nbsp;
                    {
                        auxText && <span className={`${auxClassName}`}> {auxText} </span>
                    }
                </h3>
                {
                    additionalElement && (
                        <span
                            className={additionalElementClassName}
                            style={additionalElementStyle}
                        >
                            {additionalElement}
                        </span>
                    )
                }
            </div>
        </Fragment>
    );
};

export default PageHeader;
