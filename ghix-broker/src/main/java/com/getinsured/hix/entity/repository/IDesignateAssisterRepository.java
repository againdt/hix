package com.getinsured.hix.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.DesignateAssister.Status;

/**
 * Repository for CRUD operations on {@link DesignateAssister}
 */
public interface IDesignateAssisterRepository extends JpaRepository<DesignateAssister, Integer> {

	/**
	 * Finds {@link DesignateAssister} based on external individual id coming
	 * from AHBX.
	 * 
	 * @param extIndividualId
	 * @return {@link DesignateAssister}
	 */
	DesignateAssister findDesigAssisterByIndividualId(int extIndividualId);

	/**
	 * Finds {@link DesignateAssister} based on external individual id and assister id coming
	 * from AHBX.
	 * 
	 * @param assister Id
	 * @param extIndividualId
	 * @return {@link DesignateAssister}
	 */
	@Query("FROM DesignateAssister d where d.assisterId = :assisterId AND d.individualId = :extIndividualId")
	DesignateAssister findDesigAssisterByIndividualAndAssisterId(@Param("assisterId") int assisterId,
			@Param("extIndividualId") int extIndividualId);
	

	/**
	 * Finds {@link DesignateAssister} based on external individual id and assister id coming
	 * from AHBX and status
	 * 
	 * @param assister Id
	 * @param extIndividualId
	 * @param status
	 * @return {@link DesignateAssister}
	 */
	@Query("FROM DesignateAssister d where d.assisterId = :assisterId AND d.individualId = :extIndividualId AND d.status = :status")
	DesignateAssister findDesigAssisterByIndividualAndAssisterIdAndStatus(@Param("assisterId") int assisterId,
			@Param("extIndividualId") int extIndividualId, @Param("status") Status status);
}