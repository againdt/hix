package com.getinsured.hix.broker.util;

import org.springframework.ws.soap.server.endpoint.annotation.FaultCode;
import org.springframework.ws.soap.server.endpoint.annotation.SoapFault;


@SoapFault(faultCode = FaultCode.SERVER)
public class WebServiceFault extends RuntimeException{
	private static final long serialVersionUID = 1L;
	private String faultDetail;
	
	public WebServiceFault(String faultDetail){
		this.faultDetail = faultDetail;
	}
		
	public String getFaultDetail() {
		return faultDetail;
	}
	
	public void setFaultDetail(String faultDetail) {
		this.faultDetail = faultDetail;
	}
	
	@Override
	public String toString() {
		return faultDetail;
	}
}