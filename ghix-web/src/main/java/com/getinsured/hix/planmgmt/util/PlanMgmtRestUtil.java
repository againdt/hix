package com.getinsured.hix.planmgmt.util;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.thoughtworks.xstream.XStream;

@Component
@DependsOn("dynamicPropertiesUtil")
public class PlanMgmtRestUtil {
	private static final Logger LOGGER = Logger.getLogger(PlanMgmtRestUtil.class);
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	public Long getNumberOfEnrollmentForPlanId(String cmsPlanId, Integer coverageYear, String terminationDate){
		Map<String, String> requestMap = new HashMap<String, String>();
		Long enrollmentCount = 0L;
		try{
			requestMap.put(PlanMgmtConstants.CMSPLAN_ID, cmsPlanId);
			requestMap.put(PlanMgmtConstants.COVERAGE_YEAR, String.valueOf(coverageYear));
			requestMap.put(PlanMgmtConstants.TERM_DATE, terminationDate);
			String xmlResponse = ghixRestTemplate.postForObject(EnrollmentEndPoints.COUNT_ENROLLMEN_BY_CMS_PLANID_URL, requestMap, String.class);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("xmlResponse : " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlResponse)));
			}
			//XStream xstream = GhixUtils.getXStreamStaxObject();
			//HIX-93225, HIX-91520 
			XStream xstream = GhixUtils.getXStreamStaxObject();
			EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstream.fromXML(xmlResponse);
			if(null != enrollmentResponse){
				enrollmentCount = enrollmentResponse.getEnrollmentCount();
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in getNumberOfEnrollmentForPlanId : ", ex);
		}
		return enrollmentCount;
	}
	
	public String terminateEnrollmentByPlanId(String cmsPlanId, Integer coverageYear, String terminationDate){
		Map<String, String> requestMap = new HashMap<String, String>();
		String terminationStatusResponse = null;
		try{
			requestMap.put(PlanMgmtConstants.CMSPLAN_ID, cmsPlanId);
			requestMap.put(PlanMgmtConstants.COVERAGE_YEAR, String.valueOf(coverageYear));
			requestMap.put(PlanMgmtConstants.TERM_DATE, terminationDate);
			String xmlResponse = ghixRestTemplate.postForObject(EnrollmentEndPoints.TERMINATE_ENROLLMEN_BY_CMS_PLANID_URL, requestMap, String.class);
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("xmlResponse : " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlResponse)));
			}
			//XStream xstream = GhixUtils.getXStreamStaxObject();
			//HIX-93225, HIX-91520 
			XStream xstream = GhixUtils.getXStreamStaxObject();
			EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstream.fromXML(xmlResponse);
			if(null != enrollmentResponse){
				terminationStatusResponse = enrollmentResponse.getStatus();
			}
		}catch(Exception ex){
			LOGGER.error("Exception occured in terminateEnrollmentByPlanId : ", ex);
		}
		return terminationStatusResponse;
	}
}
