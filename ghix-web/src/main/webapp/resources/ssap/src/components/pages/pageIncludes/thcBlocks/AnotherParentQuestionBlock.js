import React from 'react';



// Components

import UniversalInput from '../../../../lib/UniversalInput';

// Utils

import bitWiseTools from '../../../../utils/bitWiseOperatorTools';
import { getHhmByPersonId } from '../../../../utils/getHhmByPersonId';

// Constants

import DROPDOWN_OPTIONS from '../../../../constants/dropdownOptions';
import INPUT_TYPES from '../../../../constants/inputTypes';
import MASKS from '../../../../constants/masksConstants';
import VALIDATION_TYPES from '../../../../constants/validationTypes';



const AnotherParentQuestionBlock = props => {

    const {
        listOfClaimedBySeparatelyLivingParent,
        householdMembers,
        onFieldChange,
        ...restProps
    } = props;

    const {
        taxHouseholdComposition: taxHouseholdCompositionMasks,
    } = MASKS;

    return listOfClaimedBySeparatelyLivingParent.length !== 0 ?
        <div className="subsection">
            {
                listOfClaimedBySeparatelyLivingParent.map(d => {

                    const {
                        dependentFullName,
                        dependentPersonId
                    } = d;

                    const {
                        flagBucket: {
                            taxHouseholdCompositionFlags
                        }
                    } = getHhmByPersonId(dependentPersonId, householdMembers);

                    const {
                        livesWithAnotherParent
                    } = bitWiseTools
                        .convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks);

                    return (
                        <UniversalInput
                            key={`${dependentPersonId}_${dependentFullName}`}
                            legend={{
                                legendText:
                                    <p>
                                        Does &nbsp;
                                        <strong>
                                            {dependentFullName}
                                        </strong>
                                        &nbsp; live with another parent and/or stepparent?
                                    </p>
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole margin-b-20'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                classes: ['disp-styl-none'],
                                name: 'livesWithAnotherParent',
                                value: livesWithAnotherParent,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: {},
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                            indexInArray={dependentPersonId - 1}
                            {...restProps}
                        />
                    );
                })
            }
        </div>: null;
};

export default AnotherParentQuestionBlock;
