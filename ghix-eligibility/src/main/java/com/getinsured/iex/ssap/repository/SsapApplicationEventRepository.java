package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

@RestResource(path="SsapApplicationEvent")
public interface SsapApplicationEventRepository  extends JpaRepository<SsapApplicationEvent, Long> {

	List<SsapApplicationEvent> findBySsapApplication(SsapApplication ssapApplication);

	/*@RestResource(path="findBySssapApplicationId")
	@Query("from SsapApplicationEvent where ssapApplication.id = :applicationId and changeEventType.lookupValueId = :lookupValueId)")
	public SsapApplicationEvent findEventForOpenEnrollment(@Param("applicationId") long applicationId, @Param("lookupValueId") int lookupValueId);*/

	@RestResource(path="findEventForOpenEnrollment")
	@Query("from SsapApplicationEvent sae  where sae.ssapApplication.id = :applicationId and sae.eventType = 'OE')")
	public SsapApplicationEvent findEventForOpenEnrollment(@Param("applicationId") long applicationId);

	@RestResource(path="findBySssapApplicationEventId")
	@Query("from SsapApplicationEvent where id = :applicationId)")
	public SsapApplicationEvent findBySssapApplicationEventId(@Param("applicationId") long applicationId);

	@RestResource(path="findEventBySsapApplication")
	@Query("from SsapApplicationEvent where ssapApplication.id = :applicationId)")
	public SsapApplicationEvent findEventBySsapApplication(@Param("applicationId") long applicationId);

	@RestResource(path="findEventBySsapApplicationId")
	@Query("Select event " +
			" FROM SsapApplicationEvent as event "+
			" inner join fetch event.ssapApplication as application"+
			" where application.id = :applicationId ")
	List<SsapApplicationEvent> findEventBySsapApplicationId(@Param("applicationId") long applicationId);
	
	@Query("Select applicationEvent " +
			" FROM SsapApplicationEvent as applicationEvent "+
			" join fetch applicationEvent.ssapApplication as application " +
			" left outer join fetch applicationEvent.ssapApplicantEvents as applicantEvent "+
			" where application.id = :applicationId " )
	SsapApplicationEvent findApplicationEventAndApplicantEventsBySsapApplication(@Param("applicationId") long applicationId);

	@Modifying
	@Transactional(readOnly=false)																  
	@Query("update SsapApplicationEvent sae set sae.coverageStartDate = null where ssapApplication.id = :applicationId")
	Integer updateDatesForEvent(@Param("applicationId") long applicationId);
	
	@Modifying
	@Transactional(readOnly=false)																  
	@Query("update SsapApplicationEvent sae set sae.changePlan = :changePlan where ssapApplication.id = :applicationId")
	Integer updateChangePlan(@Param("applicationId") long applicationId,@Param("changePlan") String changePlan);
}
