package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ReverifyAgency3InitVerifRequestType;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for ReverifyAgency3InitVerifRequestType
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 */

public class ReverifyAgency3InitVerifRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory;
	private ReverifyAgency3InitVerifRequestType reverifyAgency3InitVerifRequestType;
	
	private String caseNumber;
	private String receiptNumber;
	private String alienNumber;
	private String i94Number;
	private String sevisid;
	private String passportNumber;
	private String countryOfIssuance;
	private PassportCountryWrapperForReverifyRequest passportCountry;
	private String naturalizationNumber;
	private String citizenshipNumber;
	private String visaNumber;
	private String lastName;
	private String firstName;
	private String middleName;
	private String dateOfBirth;
	private String requestedCoverageStartDate;
	private boolean fiveYearBarApplicabilityIndicator;
	private String requesterCommentsForHub;
	private String categoryCode;
    
	private static final String CASENUMBER_KEY = "CaseNumber";
	private static final String RECEIPT_NUMBER_KEY = "ReceiptNumber";
	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String I94_NUMBER_KEY = "I94Number";
	private static final String SEVIS_ID_KEY = "SEVISID";
	private static final String PASSPORT_COUNTRY_KEY = "PassportCountry";
	private static final String NATURALIZATION_NUMBER_KEY = "NaturalizationNumber";
	private static final String CITIZENSHIP_NUMBER_KEY = "CitizenshipNumber";
	private static final String VISA_NUMBER_KEY = "VisaNumber";
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String REQUESTEDCOVERAGESTARTDATE_KEY = "RequestedCoverageStartDate";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String CATEGORYCODE_KEY = "CategoryCode";
	
    public ReverifyAgency3InitVerifRequestTypeWrapper(){
		factory = new ObjectFactory();
		reverifyAgency3InitVerifRequestType = factory.createReverifyAgency3InitVerifRequestType();
	}
    
    /**
     * Request specific validations
     */
    public ReverifyAgency3InitVerifRequestType getAgency3VerifRequestType() throws HubServiceException {
    	
    	if(this.reverifyAgency3InitVerifRequestType.getCaseNumber() == null){
			throw new HubServiceException("Case Number is required for ReverifyAgency3InitVerifRequest");
		}
		
		if(this.reverifyAgency3InitVerifRequestType.getRequestedCoverageStartDate() == null){
			throw new HubServiceException("Requested Coverage Start Date is required for ReverifyAgency3InitVerifRequest");
		}
    	
		if(this.getPassportCountry() != null){
			
			String passportNumberLocal = this.getPassportCountry().getPassportNumber();
			if(passportNumberLocal != null && this.getPassportCountry().getCountryOfIssuance() == null){
				throw new HubServiceException("Passport Country is required if Passport Number is provided for ReverifyAgency3InitVerifRequest");
			}
			
			this.reverifyAgency3InitVerifRequestType.setPassportCountry(this.getPassportCountry().getPassportCountryType());
		}
		
		//Exactly one of I-94 number or alien number should be present
		if(alienNumber != null && i94Number != null){
			throw new HubServiceException("Cannot have both an Alien number and an I-94 number for ReverifyAgency3InitVerifRequest");
		}
		
		if(alienNumber == null && i94Number == null){
			throw new HubServiceException("Either an A Number or an I-94 Number is required for ReverifyAgency3InitVerifRequest");
		}
		
		return reverifyAgency3InitVerifRequestType;
	}
	
	public String getCaseNumber() {
		return caseNumber;
	}
	
	public void setCaseNumber(String caseNumber) {
		this.reverifyAgency3InitVerifRequestType.setCaseNumber(caseNumber);
		this.caseNumber = caseNumber;
	}
	public String getReceiptNumber() {
		return receiptNumber;
	}
	public void setReceiptNumber(String receiptNumber) {
		this.reverifyAgency3InitVerifRequestType.setReceiptNumber(receiptNumber);
		this.receiptNumber = receiptNumber;
	}
	public String getAlienNumber() {
		return alienNumber;
	}
	public void setAlienNumber(String alienNumber) {
		this.reverifyAgency3InitVerifRequestType.setAlienNumber(alienNumber);
		this.alienNumber = alienNumber;
	}
	public String getI94Number() {
		return i94Number;
	}
	public void setI94Number(String i94Number) {
		this.reverifyAgency3InitVerifRequestType.setI94Number(i94Number);
		this.i94Number = i94Number;
	}
	
	public PassportCountryWrapperForReverifyRequest getPassportCountry() {
		
		if(this.passportCountry == null){
			this.passportCountry = new PassportCountryWrapperForReverifyRequest();
		}
		
		return passportCountry;
	}
	
	public void setPassportCountry(PassportCountryWrapperForReverifyRequest passportCountry) throws HubServiceException {
		
		if(passportCountry != null &&
			passportCountry.getPassportCountryType().getPassportNumber() != null &&
					passportCountry.getPassportCountryType().getCountryOfIssuance() == null){
				throw new HubServiceException("Passport Country is required if Passport number is provided for ReverifyAgency3InitVerifRequest");
		}
		
		this.reverifyAgency3InitVerifRequestType.setPassportCountry(passportCountry.getPassportCountryType());
		this.passportCountry = passportCountry;
	}
	
	public String getNaturalizationNumber() {
		return naturalizationNumber;
	}
	public void setNaturalizationNumber(String naturalizationNumber) {
		this.reverifyAgency3InitVerifRequestType.setNaturalizationNumber(naturalizationNumber);
		this.naturalizationNumber = naturalizationNumber;
	}
	public String getCitizenshipNumber() {
		return citizenshipNumber;
	}
	public void setCitizenshipNumber(String citizenshipNumber) {
		this.reverifyAgency3InitVerifRequestType.setCitizenshipNumber(citizenshipNumber);
		this.citizenshipNumber = citizenshipNumber;
	}
	public String getVisaNumber() {
		return visaNumber;
	}
	public void setVisaNumber(String visaNumber) {
		this.reverifyAgency3InitVerifRequestType.setVisaNumber(visaNumber);
		this.visaNumber = visaNumber;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.reverifyAgency3InitVerifRequestType.setLastName(lastName);
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.reverifyAgency3InitVerifRequestType.setFirstName(firstName);
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.reverifyAgency3InitVerifRequestType.setMiddleName(middleName);
		this.middleName = middleName;
	}
	
	public ReverifyAgency3InitVerifRequestType getReverifyAgency3InitVerifRequestType() {
		return reverifyAgency3InitVerifRequestType;
	}

	public void setReverifyAgency3InitVerifRequestType(
			ReverifyAgency3InitVerifRequestType reverifyAgency3InitVerifRequestType) {
		this.reverifyAgency3InitVerifRequestType = reverifyAgency3InitVerifRequestType;
	}

	public String getSevisid() {
		return sevisid;
	}

	public void setSevisid(String sevisid) {
		this.reverifyAgency3InitVerifRequestType.setSEVISID(sevisid);
		this.sevisid = sevisid;
	}

	public String getPassportNumber() {
		return passportNumber;
	}

	public void setPassportNumber(String passportNumber) throws HubServiceException {
		this.getPassportCountry().setPassportNumber(passportNumber);
		this.passportNumber = passportNumber;
	}

	public String getCountryOfIssuance() {
		return countryOfIssuance;
	}

	public void setCountryOfIssuance(String countryOfIssuance) throws HubServiceException {
		this.getPassportCountry().setCountryOfIssuance(countryOfIssuance);
		this.countryOfIssuance = countryOfIssuance;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) throws HubServiceException {
		
		try{
			this.reverifyAgency3InitVerifRequestType.setDateOfBirth(VLPServiceUtil.stringToXMLDate(dateOfBirth, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		
		this.dateOfBirth = dateOfBirth;
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.reverifyAgency3InitVerifRequestType.setRequesterCommentsForHub(requesterCommentsForHub);
		this.requesterCommentsForHub = requesterCommentsForHub;
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.reverifyAgency3InitVerifRequestType.setCategoryCode(categoryCode);
		this.categoryCode = categoryCode;
	}

	public String getRequestedCoverageStartDate() {
		return requestedCoverageStartDate;
	}
	public void setRequestedCoverageStartDate(String requestedCoverageStartDate) throws HubServiceException {
		
		try{
			this.reverifyAgency3InitVerifRequestType.setRequestedCoverageStartDate(VLPServiceUtil.stringToXMLDate(requestedCoverageStartDate, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		
		this.requestedCoverageStartDate = requestedCoverageStartDate;
	}
	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}
	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.reverifyAgency3InitVerifRequestType.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
	}
	
	/**
	 * Converts the JSON String to ReverifyAgency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for ReverifyAgency3InitVerifRequestTypeWrapper
	 * @return ReverifyAgency3InitVerifRequestTypeWrapper Object 
	 */
	public static ReverifyAgency3InitVerifRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create ReverifyAgency3InitVerifRequestTypeWrapper from null or empty input");
		}
		
		ReverifyAgency3InitVerifRequestTypeWrapper wrapper = new ReverifyAgency3InitVerifRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(CASENUMBER_KEY);
		if(param != null){
			wrapper.setCaseNumber((String)param);
		}
		
		param = obj.get(ALIEN_NUMBER_KEY);
		if(param != null){
			wrapper.setAlienNumber((String)param);
		}
		
		param = obj.get(RECEIPT_NUMBER_KEY);
		if(param != null){
			wrapper.setReceiptNumber((String)param);
		}
		
		param = obj.get(I94_NUMBER_KEY);
		if(param != null){
			wrapper.setI94Number((String)param);
		}
		
		param = obj.get(SEVIS_ID_KEY);
		if(param != null){
			wrapper.setSevisid((String)param);
		}
		
		param = obj.get(PASSPORT_COUNTRY_KEY);
		if(param != null){
			wrapper.setPassportCountry(PassportCountryWrapperForReverifyRequest.fromJSONObject((JSONObject)param));
		}
		
		param = obj.get(NATURALIZATION_NUMBER_KEY);
		if(param != null){
			wrapper.setNaturalizationNumber((String)param);
		}
		
		param = obj.get(CITIZENSHIP_NUMBER_KEY);
		if(param != null){
			wrapper.setCitizenshipNumber((String)param);
		}
		
		param = obj.get(VISA_NUMBER_KEY);
		if(param != null){
			wrapper.setVisaNumber((String)param);
		}
		
		param = obj.get(FIRSTNAME_KEY);
		if(param != null){
			wrapper.setFirstName((String)param);
		}
		
		param = obj.get(MIDDLENAME_KEY);
		if(param != null){
			wrapper.setMiddleName((String)param);
		}
		
		param = obj.get(LASTNAME_KEY);
		if(param != null){
			wrapper.setLastName((String)param);
		}
		
		param = obj.get(DATEOFBIRTH_KEY);
		if(param != null){
			try {
				wrapper.setDateOfBirth((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
		
		param = obj.get(REQUESTEDCOVERAGESTARTDATE_KEY);
		if(param != null){
			try {
				wrapper.setRequestedCoverageStartDate((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
		
		param = obj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY);
		if(param != null){
			wrapper.setFiveYearBarApplicabilityIndicator((Boolean)param);
		}
		
		param = obj.get(REQUESTERCOMMENTSFORHUB_KEY);
		if(param != null){
			wrapper.setRequesterCommentsForHub((String)param);
		}
		
		param = obj.get(CATEGORYCODE_KEY);
		if(param != null){
			wrapper.setCategoryCode((String)param);
		}
		
		return wrapper;
		
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASENUMBER_KEY, this.caseNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(NATURALIZATION_NUMBER_KEY, this.naturalizationNumber);
		obj.put(CITIZENSHIP_NUMBER_KEY, this.citizenshipNumber);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);
		return obj.toJSONString();

	}
	
	public ReverifyAgency3InitVerifRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		ReverifyAgency3InitVerifRequestTypeWrapper wrapper = new ReverifyAgency3InitVerifRequestTypeWrapper();
		wrapper.setCaseNumber((String)jsonObj.get(CASENUMBER_KEY));
		wrapper.setReceiptNumber((String)jsonObj.get(RECEIPT_NUMBER_KEY));
		wrapper.setAlienNumber((String)jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setI94Number((String)jsonObj.get(I94_NUMBER_KEY));
		wrapper.setSevisid((String)jsonObj.get(SEVIS_ID_KEY));
		if(jsonObj.get(PASSPORT_COUNTRY_KEY) != null){
			wrapper.setPassportCountry(PassportCountryWrapperForReverifyRequest.fromJSONObject((JSONObject) jsonObj.get(PASSPORT_COUNTRY_KEY)));
		}
		wrapper.setNaturalizationNumber((String)jsonObj.get(NATURALIZATION_NUMBER_KEY));
		wrapper.setCitizenshipNumber((String)jsonObj.get(CITIZENSHIP_NUMBER_KEY));
		wrapper.setVisaNumber((String)jsonObj.get(VISA_NUMBER_KEY));
		wrapper.setFirstName((String)jsonObj.get(FIRSTNAME_KEY));
		wrapper.setMiddleName((String)jsonObj.get(MIDDLENAME_KEY));
		wrapper.setLastName((String)jsonObj.get(LASTNAME_KEY));
		wrapper.setDateOfBirth((String)jsonObj.get(DATEOFBIRTH_KEY));
		wrapper.setI94Number((String)jsonObj.get(I94_NUMBER_KEY));
		wrapper.setRequestedCoverageStartDate((String)jsonObj.get(REQUESTEDCOVERAGESTARTDATE_KEY));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		wrapper.setCategoryCode((String)jsonObj.get(CATEGORYCODE_KEY));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		
		obj.put(CASENUMBER_KEY, this.caseNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(I94_NUMBER_KEY, this.i94Number);
		obj.put(SEVIS_ID_KEY, this.sevisid);
		if(this.passportCountry != null){
			obj.put(PASSPORT_COUNTRY_KEY, this.passportCountry.toJSONObject());
		}
		obj.put(NATURALIZATION_NUMBER_KEY, this.naturalizationNumber);
		obj.put(CITIZENSHIP_NUMBER_KEY, this.citizenshipNumber);
		obj.put(VISA_NUMBER_KEY, this.visaNumber);
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);
		return obj;
	}

}
