<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
h4 small {
	font-size: 12px;
	margin-top: 4px;
}
</style>

<div class="gutter10">
	<div class="row-fluid">
		<h1 class="gutter10"><a name="skip"></a><spring:message code="label.faq.employer-employee.header" htmlEscape="false"/></h1>
		<!--<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>-->
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
			<df:csrfToken/>
			<ul class="nav nav-list faq-nav" id="nav">
						<li><a href="#employergroup1"><spring:message code="label.faq.employer-employee.Q1" htmlEscape="false"/></a></li>
						<li><a href="#employergroup2"><spring:message code="label.faq.employer-employee.Q2" htmlEscape="false"/></a></li>
						<li><a href="#employergroup3"><spring:message code="label.faq.employer-employee.Q3" htmlEscape="false"/></a></li>
						<li><a href="#employergroup4"><spring:message code="label.faq.employer-employee.Q4" htmlEscape="false"/></a></li>
			</ul>
		</div>
		
		
		<div class="span9" id="rightpanel">
		
		
			<!-------------------------------question 1----------------------------------------------->
			<div id="employergroup1" class="margin20-b">
				<div class="header">
					<h4><spring:message code="label.faq.employer-employee.Q1" htmlEscape="false"/></h4>
				</div>
				<div class="gutter10">	  		
	  				<p><spring:message code="label.faq.employer-employee.A1.p1" htmlEscape="false"/></p>
	  			</div>				
			</div>
			
			
			
			
			<!-------------------------------question 2----------------------------------------------->
			<div id="employergroup2" class="margin20-b">
				<div class="header">
					<h4><spring:message code="label.faq.employer-employee.Q2" htmlEscape="false"/> <small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<div class="gutter10">
					<p><spring:message code="label.faq.employer-employee.A2.p1" htmlEscape="false"/></p>
					<ul>
						<li><spring:message code="label.faq.employer-employee.A2.li1" htmlEscape="false"/></li>
						<li><spring:message code="label.faq.employer-employee.A2.li2" htmlEscape="false"/></li>
						<li><spring:message code="label.faq.employer-employee.A2.li3" htmlEscape="false"/></li>
						<li><spring:message code="label.faq.employer-employee.A2.li4" htmlEscape="false"/></li>
					</ul>
	  			</div>
			</div>
			
			
			
			<!-------------------------------question 3----------------------------------------------->
			<div id="employergroup3" class="margin20-b">
				<div class="header">
					<h4><spring:message code="label.faq.employer-employee.Q3" htmlEscape="false"/> <small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<div class="gutter10">
					<p><spring:message code="label.faq.employer-employee.A3.p1" htmlEscape="false"/></p>
	  			</div>
			</div>
			
			
			
			<!-------------------------------question 4----------------------------------------------->
			<div id="employergroup4" class="margin20-b">
				<div class="header">
					<h4><spring:message code="label.faq.employer-employee.Q4" htmlEscape="false"/> <small class="pull-right"><a id="role-top" href="#">Back to Top</a></small></h4>
				</div>
				<div class="gutter10">
					<p><spring:message code="label.faq.employer-employee.A4.p1" htmlEscape="false"/></p>
					<p><spring:message code="label.faq.employer-employee.A4.p2" htmlEscape="false"/></p>
					<ul>
						<li><spring:message code="label.faq.employer-employee.A4.li1" htmlEscape="false"/></li> 
						<li><spring:message code="label.faq.employer-employee.A4.li2" htmlEscape="false"/></li> 
						<li><spring:message code="label.faq.employer-employee.A4.li3" htmlEscape="false"/></li> 
					</ul>
					<p><spring:message code="label.faq.employer-employee.A4.p3" htmlEscape="false"/></p>
	  			</div>
			</div>
			
			
			

		</div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Covered California | Frequently Asked Questions";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		