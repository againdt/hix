package com.getinsured.ssap.model.hub.vlp;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * VLP Document types.
 *
 * @author Yevgen Golubenko
 * @since 2/14/19
 */
public enum DocumentType
{
  I327(                           "I327",                     3, "I327DocumentID",                 "I-327 Reentry Permit"),
  I551(                           "I551",                     4, "I551DocumentID",                 "I-551 Permanent Resident Card"),
  I571(                           "I571",                     5, "I571DocumentID",                 "I-571 Refugee Travel Document"),
  I766(                           "I766",                     9, "I766DocumentID",                 "I-766 Employment Authorization Card"),
  CERTIFICATE_OF_CITIZENSHIP(     "CertOfCit",               23, "CertOfCitDocumentID",            "Certificate of Citizenship"),
  NATURALIZATION_CERTIFICATE(     "NatrOfCert",              20, "NatrOfCertDocumentID",           "Naturalization Certificate"),
  MACHINE_READABLE_IMMIGRANT_VISA("MacReadI551",             22, "MacReadI551DocumentID",          "Machine Readable Immigrant Visa (with Temporary I-557 Language"),
  TEMPORARY_I551_STAMP(           "TempI551",                21, "TempI551DocumentID",             "Temporary I-551 Stamp (on passport or I-94"),
  I94(                            "I94Document",              2, "I94DocumentID",                  "Arrival/Departure Record"),
  I94_IN_PASSPORT(                "I94UnexpForeignPassport", 10, "I94UnexpForeignPassportDocumentID","Arrival/Departure Record in Unexpired Foreign Passport"),
  UNEXPIRED_FOREIGN_PASSPORT(     "UnexpForeignPassport",    30, "UnexpForeignPassportDocumentID", "Unexpired Foreign Passport"),
  I20(                            "I20",                     26, "I20DocumentID",                  "Certificate of Eligibility for Non-immigrant (F-1) Student Status"),
  DS2019(                         "DS2019",                  27, "DS2019DocumentID",               "DS2019 Certificate of Eligibility for Exchange Visitor (J-1) Status"),
  OTHER_ALIEN(                    "OtherCase1",               1, "OtherCase1DocumentID",           "Other Document with Alien Number"),
  OTHER_I94(                      "OtherCase2",               2, "OtherCase2DocumentID",           "Other Document with I94 Number"),
  UNSUPPORTED(                    "Unsupported",              0, "Unsupported",                    "Some other document which is not supported");

  private String name;
  private int id;
  private String dhsId;
  private String description;

  DocumentType(String name, int id, String dhsId, String description) {
    this.name = name;
    this.id = id;
    this.dhsId = dhsId;
    this.description = description;
  }

  /**
   * Returns name of this document type.
   * @return document type name.
   */
  @JsonValue
  public String getName() {
    return this.name;
  }

  /**
   * Returns identification number for this document.
   * @return identification number of this document.
   */
  public int getId() {
    return this.id;
  }

  /**
   * Returns ID to be used for DHS for this document type.
   * @return ID used for communication with DHS.
   */
  public String getDhsId() {
    return this.dhsId;
  }

  /**
   * Returns description of this document type.
   * @return description of this document type.
   */
  public String getDescription() {
    return this.description;
  }

  /**
   * Returns JSON representation of this document.
   * @return JSON representation of this document.
   */
  public String toJson() {
    return String.format("{\"name\": \"%s\", \"id\": \"%s\", \"dhsId\": \"%s\", \"description\": \"%s\"}",
        this.name, this.id, this.dhsId, this.description);
  }

  /**
   * Returns name of this document.
   * @return name of this document.
   */
  @Override
  public String toString() {
    return this.name;
  }
}
