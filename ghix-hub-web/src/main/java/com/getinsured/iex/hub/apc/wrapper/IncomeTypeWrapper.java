package com.getinsured.iex.hub.apc.wrapper;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_core.ObjectFactory;
import com.getinsured.iex.hub.apc.cms.hix._0_1.hix_types.PercentageType;
import com.getinsured.iex.hub.apc.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.hub.apc.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.hub.platform.HubServiceException;

public class IncomeTypeWrapper {
	
	private  com.getinsured.iex.hub.apc.niem.niem.niem_core._2.ObjectFactory niemFactory;
	private com.getinsured.iex.hub.apc.cms.hix._0_1.hix_types.ObjectFactory typeFactory;
	private IncomeType incomeType;
	
    private String incomeAmount;
    private String incomeDate;
    private String incomeFederalPovertyLevelPercent;
      
    public IncomeTypeWrapper()
    {
    	ObjectFactory hixFactory = new ObjectFactory();
    	incomeType = hixFactory.createIncomeType();
    	niemFactory = new com.getinsured.iex.hub.apc.niem.niem.niem_core._2.ObjectFactory();
    	typeFactory = new com.getinsured.iex.hub.apc.cms.hix._0_1.hix_types.ObjectFactory();
    }
    
    public IncomeType getSystemRepresentation()
    {
    	return incomeType;
    }
    
    public String getIncomeFederalPovertyLevelPercent()
    {
    	return incomeFederalPovertyLevelPercent;
    }
    
    public void setIncomeFederalPovertyLevelPercent(String incomeFederalPovertyLevelPercent)
    {
    	PercentageType percentageType = typeFactory.createPercentageType();
    	percentageType.setValue(BigDecimal.valueOf(Double.valueOf(incomeFederalPovertyLevelPercent)));
    	incomeType.setIncomeFederalPovertyLevelPercent(percentageType);
    	this.incomeFederalPovertyLevelPercent = incomeFederalPovertyLevelPercent;
    }
    
    public String getIncomeAmount()
    {
    	return incomeAmount;
    }
    
    public void setIncomeAmount(String incomeAmount)
    {
    	AmountType amountType = niemFactory.createAmountType();
    	amountType.setValue(BigDecimal.valueOf(Double.valueOf(incomeAmount)));
    	incomeType.setIncomeAmount(amountType);
    	this.incomeAmount= incomeAmount;
    }
    
	public String getIncomeDate() {
		return incomeDate;
	}

	public void setIncomeDate(String incomeYear) throws HubServiceException {
		GYear gYear = new GYear();
	
		try {
		    DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
            GregorianCalendar calendar = new GregorianCalendar();
            // reset all fields
            calendar.clear();
 
            // set date from parameter and leave time as default calendar values
            calendar.set(Calendar.YEAR, Integer.valueOf(incomeYear)); 
            
            XMLGregorianCalendar xmlCalendar = datatypeFactory.newXMLGregorianCalendar( calendar );
            
            // clears default timezone
            xmlCalendar.setTimezone( DatatypeConstants.FIELD_UNDEFINED );
            gYear.setValue(xmlCalendar);
            
            incomeType.setIncomeDate(gYear);
			this.incomeDate = incomeYear;
            
		} catch (DatatypeConfigurationException e) {
			throw new HubServiceException(e.getMessage(), e);
		}
	}
}
