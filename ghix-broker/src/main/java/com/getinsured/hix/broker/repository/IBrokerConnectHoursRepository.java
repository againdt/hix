package com.getinsured.hix.broker.repository;

import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerConnect;
import com.getinsured.hix.model.BrokerConnectHours;
import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.BrokerConnectHours;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IBrokerConnectHoursRepository extends JpaRepository<BrokerConnectHours, Integer> {
    @Query("FROM BrokerConnectHours WHERE brokerConnectId = :brokerId ORDER BY day asc")
    List<BrokerConnectHours> findBrokerConnectHoursById(@Param("brokerId") BrokerConnect brokerConnect);
}
