
<link rel="stylesheet" href="../resources/css/eligibility.css" type="text/css">

 <link rel="stylesheet" type="text/css" href="../resources/css/datepicker.css" />
<script type="text/javascript" src="../resources/js/bootstrap-datepicker.js"></script>
 <link href="../resources/css/jquery.ui.datepicker.css" rel="stylesheet"  type="text/css" />


<body>
<form class="form-horizontal">
<div class="row-fluid" >
    <div class="row-fluid" id="titlebar">
      <div class="span3">
          <a class="btn btn-link" href="e1-1-start-app">Back </a>
      </div>
      <div class="span9 pull-left">
        <h1>Build Your Household</h1>
      </div>
    </div>
    <!--titlebar-->
    
    <div class="row-fluid" id="main">
      <div class="span3 gutter10" id="sidebar">
         <ul class="nav nav-tabs nav-stacked"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li class="active"><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> Additional Questions </a></li>
          <li><a href="e6-review-declare-file"> Review &amp; Sign </a></li>
        </ul>
      </div>
      <!--sidebar-->
     <div class="span9" id="rightpanel">
      <img src="../resources/eligibilitynm/img/build.png" alt="header image" class="img-polaroid">
        <div class="margin-right"> <!-- gutter10  -->
          <div class="span8">
            <h4>Coming up in this section</h4>
            <p> In this section, you will be asked for identifying information about who is applying for health coverage and who lives in your household.</p>
          </div>
          <div class="span4">
            <h5>Documents you may need</h5>
            <hr>
            <p> <i class="icon-ok"></i> Immigration Documents</p>
            <p> <i class="icon-ok"></i> Social Security Numbers</p>
            <p> <i class="icon-time"></i> Estimated time needed to complete this section: 10 minutes.</p>
          </div>
           
           <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household1">Next</a> 
	                  <a class="btn edit secondary">Next</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #numberpeople .section -->   
           

            <div id="household1" class="section">
            <div class="opacity">
              <div class="header" >
            <h4>Who needs health coverage?</h4>
          </div>
          <h5 class="clear">Who are you applying for health coverage for?</h5>
          <div class="control-group">
                    <label class="checkbox">
                      <input class="flip" type="checkbox">
                      Stan Jacobs only</label>
                    <label class="checkbox">
                      <input type="checkbox">
                    Stan Jacobs &amp; other family members </label>
                    <label class="checkbox">
                      <input type="checkbox" checked="">
                    Other family members, not Stan Jacobs </label>
                  </div>
                  
            <h5 class="clear">Do you want to find out if your family can get help paying for health coverage?</h5>
              <div class="control-group">
                    <label class="checkbox">
                      <input class="flip" type="checkbox" checked>
                     Yes. You'll answer questions about your income to see what help your family qualifies for. </label>
                    <label class="checkbox">
                      <input type="checkbox" >
                  No. You'll answer fewer questions, but you won't get help paying for coverage.  </label>
                    <label class="checkbox">
                      <input type="checkbox">
                   Not sure? Answer 3 questions to figure out your next steps. <a href="#">See if you Qualify</a></label>
                  </div>
             
           
             <h5 class="clear">How many people in your family and household want health coverage? Including yourself if applicable.</h5>
             <div class="control-group">
	              <p class="btn">+</p> <!--<button class="btn">+</button>-->
	              <input type="number" value="2" class="span1 no-margin img-circle txt-center">
	              <p class="btn">-</p> <!--<button class="btn">-</button>-->
              </div>
             </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household2">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household2 .section -->   
                
                
                
            <div id="household2" class="section">
              <div class="opacity">
                <div class="header" id="household2"> 
                <h4 class="clear">You'll fill out information for each person in your family and household who wants coverage.</h4>  
             </div>
              <h5 class="clear">Tell us about Family Member 1:</h5> 
             <div class="row-fluid gutter10">
            
              <div class="control-group">
                  <label class="control-label" for="inputfName">First Name</label>
                  <div class="controls">
                    <input type="text" name="inputfName" placeholder="" value="Janet">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="inputfName">Middle Name</label>
                  <div class="controls">
                    <input type="text" name="inputfName" placeholder="" value="">
                  </div>
                </div>
               </div>
               
                <div class="control-group">
                  <label class="control-label" for="inputlName">Last Name</label>
                  <div class="controls">
                    <input type="text" name="inputlName" placeholder="" value="Jacobs">
                  </div>
                </div>
                
                <div class="control-group ">
                  <label class="control-label" for="inputSuffix">Suffix</label>
                  <div class="controls">
                    <input type="text" name="inputSuffix" class="input-small" placeholder="">
                  </div>
                </div>
                
                 <div class="control-group">
                  <div class="control-group">
                    <label class="control-label" for="inputDob">Date of Birth</label>
                   <div class="controls">
                      <div class="input-append date" data-date="06-24-1985" data-date-format="mm-dd-yyyy">
                        <input class="span6" type="text" value="06-24-1985" id="text" role="">
                        <span class="add-on"><i class="icon-calendar"></i></span> 
                    </div>
                    </div>
                  </div>
                </div>
                                
                <div class="control-group">
                  <label class="control-label" for="inputlName">How is <strong>Family Member 1</strong> realted to <strong>Stan Jacobs?</strong></label>
                  <div class="controls">
                    <select>
                      <option>&nbsp;</option>
                      <option selected>Wife</option>
                      <option>Husband</option>
                      <option>Mother</option>
                      <option>Father</option>
                      <option>Sibling</option>
                    </select>
                  </div>
                </div>

                 <h5 class="clear">Tell us about Family Member 2:</h5> 
            
              <div class="control-group">
                  <label class="control-label" for="inputfName">First Name</label>
                  <div class="controls">
                    <input type="text" name="inputfName" placeholder="" value="Joe">
                  </div>
                </div>
                <div class="control-group">
                  <label class="control-label" for="inputfName">Middle Name</label>
                  <div class="controls">
                    <input type="text" name="inputfName" placeholder="" value="">
                  </div>
                </div>
               
                <div class="control-group">
                  <label class="control-label" for="inputlName">Last Name</label>
                  <div class="controls">
                    <input type="text" name="inputlName" placeholder="" value="Jacobs">
                  </div>
                </div>
                
                <div class="control-group ">
                  <label class="control-label" for="inputSuffix">Suffix</label>
                  <div class="controls">
                    <input type="text" name="inputSuffix" class="input-small" placeholder="">
                  </div>
                </div>
                <div class="control-group">
                <div class="control-group">
                  <label class="control-label" for="inputDob">Date of Birth</label>
                  <div class="controls">
                    <div class="input-append date" data-date="04-05-2011" data-date-format="mm-dd-yyyy">
                      <input class="span6" type="text" value="04-05-2011" id="text" role="">
                      <span class="add-on"><i class="icon-calendar"></i></span> </div>
                  </div>
                </div>
                </div>
                
                <div class="control-group">
                  <label class="control-label" for="inputlName">How is <strong>Family Member 2</strong> realted to <strong>Stan Jacobs</strong>?</label>
                  <div class="controls">
                    <select>
                      <option>&nbsp;</option>
                      <option>Wife</option>
                      <option>Husband</option>
                      <option>Mother</option>
                      <option>Father</option>
                      <option>Sibling</option>
                      <option selected>Son</option>
                      <option>Daughter</option>
                    </select>
                  </div>
                </div>
              </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household3">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household .section -->
              
            
            
            
            
            
            <!-- .household-->
            <div id="household3" class="section">
              <div class="opacity">
                 <div class="header">
                  <h4 class="clear">Personal Information</h4>
                </div>
          
               <h5 class="clear">Janet Jacobs</h5>
               <p> Personal Information</p>
              <div class="control-group">
                <label class="control-label" for="inputfName">Sex</label>
                <div class="controls">
                   <label class="checkbox inline">
                  <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Male" >
                  Male </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Female" checked>
                  Female </label>

                </div>
              </div>
              
              <p>We need a Social Security number (SSN) if you want health coverage and have an SSN or can
          get one. We use SSNs to check income and other information to see who is eligible for help 
          paying for health coverage. If Janet Jacobs needs help getting an SSN, visit socialsecurity.gov, or 
          call 1-800-722-1213. TTY users should call 1-800-325-0778.</p>
              
          

                <label class="control-label" for="inputSuffix">Social Security Number</label>
                <div class="controls">
                        <input type="text" name="phone1" id="phone1" value="XXX" maxlength="3" class="area-code input-mini" onkeyup="shiftbox(this, 'phone2')" aria-required="true" aria-label="Primary contact number">
              <input type="text" name="phone2" id="phone2" value="XX" maxlength="2" class="input-mini" onkeyup="shiftbox(this, 'phone3')" aria-label="Primary contact number">
              <input type="text" name="phone3" id="phone3" value="5555" maxlength="4" class="input-mini" aria-label="Primary contact number">
                </div>

              <h5 class="clear">Is Janet Jacobs the same name that appears on her Social Security Card?</h5>
               <div class="control-group">
                <div class="controls">
                    <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked>
                    Yes </label>
                  <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No">
                    No </label>
                  </div>
                </div>

              <!-- <h5 class="clear">Social Security Number</h5> -->
                
             <!--   <div class="controls">
                <input type="text" class="input-mini" placeholder="XXX">
                <input type="text" class="span1" placeholder="XX">
                <input type="text" class="input-mini" placeholder="3265">
              </div> -->
             <!--  <p><a href="#">Don't have an SSN?</a><br><a href="#">How will your SSN be used?</a></p> -->
              
           
            <!-- .household-->
              <h5>Joe Jacobs</h5>
              <p> Personal Information</p>
                <div class="control-group">
                <label class="control-label" for="inputfName">Sex</label>
                <div class="controls">
                   <label class="checkbox inline">
                  <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Male" checked>
                  Male </label>
                <label class="checkbox inline">
                  <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Female" >
                  Female </label>

                </div>
              </div>
              
              
              <p>We need a Social Security number (SSN) if you want health coverage and have an SSN or can
          get one. We use SSNs to check income and other information to see who is eligible for help 
          paying for health coverage. If Janet Jacobs needs help getting an SSN, visit socialsecurity.gov, or 
          call 1-800-722-1213. TTY users should call 1-800-325-0778.</p>
              
          

                <label class="control-label" for="inputSuffix">Social Security Number</label>
                <div class="controls">
                        <input type="text" name="phone1" id="phone1" value="XXX" maxlength="3" class="area-code input-mini" onkeyup="shiftbox(this, 'phone2')" aria-required="true" aria-label="Primary contact number">
              <input type="text" name="phone2" id="phone2" value="XX" maxlength="2" class="input-mini" onkeyup="shiftbox(this, 'phone3')" aria-label="Primary contact number">
              <input type="text" name="phone3" id="phone3" value="5555" maxlength="4" class="input-mini" aria-label="Primary contact number">
                </div>

              <h5 class="clear">Is Joe Jacobs the same name that appears on her Social Security Card?</h5>
               <div class="control-group">
                <div class="controls">
                    <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked>
                    Yes </label>
                  <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No">
                    No </label>
                  </div>
                </div>
              </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household4">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household2 .section -->
              

            <!-- .household -->
            <div id="household4" class="section">
            <div class="opacity">
                  <div class="header">
              <h4 class="clear">Citizenship/Immigration Status</h4>
            </div>  
               <div class="row-fluid gutter10">
              <h5 class="clear">Is Janet Jacobs a U.S. citizen or U.S. national?</h5>
               <div class="control-group">
               
                  <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" >
                    Yes </label>
                  <label class="checkbox inline">
                    <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No"checked>
                    No </label>
                </div>
               <label class="checkbox inline">
             <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked >
             Check here if Janet Jacobs has eligible immigration status. <a href="#">What is eligible immigration status?</a></label>
               
               </div>
                
               <h5>Document type: (select one)</h5>
                  <div class="control-group">
		                      <select class="span10">
					              <option selected>Permanent Resident Card ("Green Card," I-551)</option>
					              <option>Alien Number A 200 345 678 (editable text field)</option>
					              <option>Temporary I-551 Stamp (on passport or I-94, I-94A)</option>
					              <option>Machine Readable Immigrant Visa (with temporary I-551 language)</option>
					              <option>Employment Authorization Card (EAD, I-766)</option>
					              <option>Arrival/Departure Record (I-94, I-94A)</option>
					              <option>Arrival/Departure Record in foreign passport (I-94) </option>
					              <option>Foreign passport </option>
					              <option>Reentry Permit (I-327) </option>
					              <option>Refugee Travel Document (I-571)</option>
					              <option>Certificate of Eligibility for Nonimmigrant (F-1) Student Status (I-20)</option>
					              <option>Certificate of Eligibility for Exchange Visitor (J-1) Status (DS2019)</option>
					              <option>Notice of Action (I-797)*</option>
					              <option>Other documents or status types</option>
		                      </select>
                        </div>
                    <p>Alien Number</p>
                    <div class="control-group">
                    			 <input class="input-small" value="A-200-345-678" type="text" name="documentType">
                  	</div>
               
               <div class="row-fluid">
                 <h5 class="clear">Is Janet Jacobs the same name that appears on her docuent?</h5>
                 <div class="control-group">
                 
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked>
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No">
                      No </label>
                  </div>
                  
                  <h5 class="clear">Has Janet Jacobs lived in the U.S. since 1996?</h5>
                 <div class="control-group">
                 
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked>
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No">
                      No </label>
                  </div>
                  
                  <h5 class="clear">Is Joe Jacobs a U.S. citizen or U.S. national?</h5>
                 <div class="control-group">
                 
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked>
                      Yes </label>
                    <label class="checkbox inline">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No">
                      No </label>
                  </div>
                </div>
            </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household5">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #household3 .section -->
            
                
                
                
                
            <!-- .household-->
            <div id="household5" class="section">
            <div class="opacity">
            <div class="header">
              <h4>Family and Household</h4>
            </div>      
                  <h5 class="clear">Does Stan Jacobs plan to file a federal income tax return for 2014? You don't have to file taxes to apply for coverage.</h5>
                  <div class="control-group">
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked="" aria-label="healthcoverage">
                      Yes </label>
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      No </label>
                  </div>
                <h5 class="clear">Does Stan Jacobs plan to file a joint federal income tax return with his spouse for 2014?</h5>
                  <div class="control-group">
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked="" aria-label="healthcoverage">
                      Yes </label>
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      No </label>
                  </div>
              
              <h5 class="clear">Will Stan Jacobs and Janet Jacobs claim any dependents on their joint federal income tax return for 2014?</h5>
                  <div class="control-group">
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked="" aria-label="healthcoverage">
                      Yes
                      	<select>
                      		<option selected>Joe Jacobs</option>
                      	</select>
                      
                      
                      </label>
                      
                    <label class="checkbox ">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      No </label>
                  </div>
			     </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household6">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #morecyntia .section -->
			


            <div id="household6" class="section">
            <div class="opacity">
            <div class="header">
                 <h4>Other Addresses</h4>
             </div>    
              <h5 class="clear">What's Janet Jacobs' home address?</h5>
                  <div class="control-group">
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked="" aria-label="healthcoverage">
                      1971 Homer Ave. Boise, ID 83725 </label>
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      Other address </label>
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      No home address </label>
                  </div>
                <h5 class="clear">What's Joe Jacobs' home address?</h5>
                  <div class="control-group">
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="Yes" checked="" aria-label="healthcoverage">
                      1971 Homer Ave. Boise, ID 83725 </label>
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      Other address </label>
                    <label class="checkbox">
                      <input type="checkbox" name="healthcoverage" id="healthcoverage" value="No" aria-label="healthcoverage">
                      No home address </label>
                  </div>
              </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn next" href="#household8">Save</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #morecyntia .section -->
	            
     
     
     
     
     
      <div id="household8" class="section">
            <div class="opacity">
            <div class="header">
                <h4>Ethnicity and Race</h4>
             </div>
              <p><em>Optional information: This information will help the U.S. Department of Health and Human Services
          (HHS) better understand and improve the health of and health care for all Americans. Providing this
          information won't impact your eligibility for health coverage, your health plan options, or your costs in
          any way.</em></p>

          <h5>Is Janet Jacobs of Hispanic, Latino, or Spanish origin?</h5>
               <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" name="other-income" value="Yes" >
                    Yes </label>
                  <label class="checkbox inline">
                    <input type="checkbox" name="other-income" value="No" checked>
                    No </label>
               </div>
               
              <h5>Race: (Check all that apply)</h5>
              <div class="control-group">
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                American Indian or Alaska Native </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Asian Indian </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Chinese </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Filipino </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Guamanian or Chamorro </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Japanese</label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Korean </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Native Hawaiian</label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Other Asian </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Other Pacific Islander </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Samoan </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
              Vietnamese</label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" checked>
              White </label>
              <label class="checkbox">
                <input type="checkbox" >
                <input type="text" class="input-medium" placeholder="Other">
               </label>
               </div>
               
               
               <h5>Is Joe Jacobs of Hispanic, Latino, or Spanish origin?</h5>
               <div class="control-group">
                  <label class="checkbox inline">
                    <input type="checkbox" name="other-income" value="Yes" >
                    Yes </label>
                  <label class="checkbox inline">
                    <input type="checkbox" name="other-income" value="No" checked>
                    No </label>
               </div>
               
               <h5>Race: (Check all that apply)</h5>
               <div class="control-group">
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                American Indian or Alaska Native </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Asian Indian </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Chinese </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Filipino </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Guamanian or Chamorro </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
                Japanese</label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Korean </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Native Hawaiian</label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Other Asian </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Other Pacific Islander </label>
                 <label class="checkbox ">
                <input type="checkbox" name="" value="" >
               Samoan </label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" >
              Vietnamese</label>
              <label class="checkbox ">
                <input type="checkbox" name="" value="" checked>
              White </label>
              <label class="checkbox">
                <input type="checkbox" >
                <input type="text" class="input-medium" placeholder="Other">
               </label>
               </div>

             </div><!-- opacity -->
                <div class="well well-small clear">
                  <div class="buttons pull-right"> 
                    <a class="btn next" href="#household9">Save</a> 
                    <a class="btn edit secondary">Edit</a> 
                    <a class="btn cancel secondary">Cancel</a>
                    <a class="btn save btn-primary">Save</a> 
                  </div><!-- .buttons pull-right -->
                </div><!-- .well well-small clear -->
              </div><!-- #residential .section -->
     
     
     
     
     
     
     
     
            <div id="household9" class="section">
            <div class="opacity">
                <div class="header">
        <h4>More about this Household</h4>
      </div>
     
          <h5>Do any of these people have a physical disability or mental health condition that limits their
          ability to work, attend school, or take care of their daily needs? <a href="#"><i class="icon-question-sign"></i></a>
          </h5>
          <div class="control-group">
                <label class="checkbox ">
                  <input type="checkbox" >
                  Janet Jacobs </label>
                <label class=" checkbox">
                  <input type="checkbox">
                  Joe Jacobs </label>
                   <label class=" checkbox">
                  <input type="checkbox" checked>
                  None of these people</label>
              </div>  
                
                <h5>Do any of these people need help with activities of daily living (like bathing, dressing, and
                using the bathroom), or live in a medical facility or nursing home?</h5>
              
              <div class="control-group">
                <label class="checkbox ">
                  <input type="checkbox" >
                  Janet Jacobs </label>
                <label class=" checkbox">
                  <input type="checkbox">
                  Joe Jacobs </label>
                   <label class=" checkbox">
                  <input type="checkbox" checked>
                  None of these people</label>
               </div> 
               
                <h5>Are any of these people American Indian or Alaska Native?</h5>
                <div class="control-group">
                <label class="checkbox ">
                  <input type="checkbox" >
                  Janet Jacobs </label>
                <label class=" checkbox">
                  <input type="checkbox">
                  Joe Jacobs </label>
                   <label class=" checkbox">
                  <input type="checkbox" checked>
                  None of these people</label>
              </div>
      

             </div><!-- opacity -->
	              <div class="well well-small clear">
	                <div class="buttons pull-right"> 
	                  <a class="btn btn-primary" href="e4-household-income">Save &amp; Continue</a> 
	                  <a class="btn edit secondary">Edit</a> 
	                  <a class="btn cancel secondary">Cancel</a>
	                  <a class="btn save btn-primary">Save</a> 
	                </div><!-- .buttons pull-right -->
	              </div><!-- .well well-small clear -->
	            </div><!-- #residential .section -->
             
           </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->

  <!--rightpanel-->
  
</div>
<!--main-->
</form>

<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script>
    $(document).ready(function () {
        $(".flip").click(function () {
            $(".mailingAddress").fadeToggle();
        });
        $('input, textarea').placeholder();
    });
    $(".next").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        var hash = $(this.hash);
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(hash).closest('.section').show();
        $(currentsection).next().addClass('activediv');
        $(hash).prev('div').removeClass('activediv');
        $('html,body').delay(300).animate({
            scrollTop: $(this.hash).offset().top - 100
        }, 500)
        $(hash).delay(700).prev('div').find('.edit').show();
        $(hash).delay(700).prev('div').find('.next').hide();
    });
    $(".edit").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 1.0
        });
        $(currentsection).find('.cancel, .save').show();
        $('.activediv').animate({
            opacity: 0.45
        });
        $(currentsection).find('.edit').hide();
    });
    $(".save").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $(currentsection).nextAll('.activediv').offset().top - 100
        }, 500)
    });
    $(".cancel").click(function (e) {
        e.preventDefault();
        var currentsection = $(this).parents('.section');
        $(this).parents('.section').find('.opacity').animate({
            opacity: 0.45
        });
        $(currentsection).find('.cancel, .save').hide();
        $(currentsection).find('.edit').show();
        $('.activediv').animate({
            opacity: 1
        });
        $('html,body').delay(300).animate({
            scrollTop: $('div').nextAll('.activediv').offset().top - 100
        }, 500)
    });
</script>
<script>
$(document).ready(function(){
$('.qualify,.immigration, .showtribe').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();

$('input:radio[name="citizen"]').change(
    function(){
		if ($(this).is(':checked') && $(this).val() == 'No') {
	$('.immigration').fadeToggle();
		$('.documents').hide();

		}
		else{
			$('.immigration').hide();
			}
});


$('input:radio[name="citizen"]').change(
    function(){
		if ($(this).is(':checked') && $(this).val() == 'No') {
	//$('.immigration').fadeToggle();
		//$('.documents').hide();
		}
		else{
			$('.immigration').hide();
			}
});


$('input:radio[name="immigration"]').change(
    function(){
		if ($(this).is(':checked') && $(this).val() == 'Yes') {
	$('.documents').fadeToggle();
		}
		else{
			$('.documents').hide();
			}
});


$('input:radio[name="member"]').change(
    function(){
		if ($(this).is(':checked') && $(this).val() == 'Yes') {
	$('.showtribe').fadeToggle();
		}
		else{
			$('.showtribe').hide();
			}
});

$('input, textarea').placeholder();

});
$(document).ready(function(){
	  $('.date').datepicker();
	  });
</script>
<script>
$(document).ready(function(){
	$('.date').datepicker();
});

</script>

</body>
</html>
