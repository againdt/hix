package com.getinsured.iex.hub.ridp;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceBridge;

public final class RIDPPrimary {
	
	private RIDPPrimary() {
		
	}
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPPrimary.class);
	
	public static void main(String[] args)  {
		try{
			HubServiceBridge ridpPrimary = HubServiceBridge.getHubServiceBridge("RIDPPrimaryRequest");
			ridpPrimary.processInputParameter("personBirthDate", "09/09/1977");
			ridpPrimary.processInputParameter("personGivenName", "John");
			ridpPrimary.processInputParameter("personMiddleName", "middle");
			ridpPrimary.processInputParameter("personSurName", "Doe");
			ridpPrimary.processInputParameter("personSSNIdentification", "333-44-5555");
			ridpPrimary.processInputParameter("personNameSuffixText", "Jr.");
			ridpPrimary.processInputParameter("fullTelephoneNumber", "15555555555");
			ridpPrimary.processInputParameter("streetName", "Some Street");
			ridpPrimary.processInputParameter("locationCityName", "Some City");
			ridpPrimary.processInputParameter("locationStateUSPostalServiceCode", "CA");
			ridpPrimary.processInputParameter("locationPostalCode", "94539");
			ridpPrimary.processInputParameter("locationPostalExtensionCode", "1234");
			//Object payLoad = ridpPrimary.getServiceHandler().getRequestpayLoad();
			
			JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.ridp.cms.dsh.ridp.exchange._1");
			Marshaller m = jc.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new RIDPNamespaceMapper());
			//m.marshal(payLoad, System.out);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}
	
}
