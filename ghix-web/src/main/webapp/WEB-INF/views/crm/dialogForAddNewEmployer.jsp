<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">

</script>

<!--start page-breadcrumb -->

<!-- gutter10 -->
<!--  Latest UI -->
<!-- Modal 
<form name="dialogForm" id="dialogForm" action="<c:url value="/crm/employer/home/${employerId}" />" novalidate="novalidate">
<form name="dialogForm" id="dialogForm" action="<c:url value="/account/user/switchUserRole/employer/${employerId}" />" novalidate="novalidate">-->
<form name="dialogForm" id="dialogForm" method="POST"  action="<c:url value="/crm/employer/home" />" novalidate="novalidate">
<df:csrfToken/>
<div id="markCompleteDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="markCompleteDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left">View Employer Account</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Through this portal you will be able to  add new employer and take actions on behalf of the employer, such as view <br/>
					and edit employee list, fill out employer eligibility etc.<br/>
					Proceed to Employer view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
   
     <a data-role="button"  class="btn btn" href="/hix/ticketmgmt/ticket/ticketlist">Cancel</a>
	<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="0" />
	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="New Employer" /> 
   <button class="btn btn-primary" type="submit" >Employer View</button >
    <!--a href='<c:url value="/account/user/switchUserRole/employer/${employer.id}"/>'><button class="btn btn-primary">Employer View</button></a -->
  </div>
</div>
</form>
<script type="text/javascript">
$(document).ready(function(){
	
	
	        $('#markCompleteDialog').modal({
	            backdrop: true 
	        });
	
});

</script>