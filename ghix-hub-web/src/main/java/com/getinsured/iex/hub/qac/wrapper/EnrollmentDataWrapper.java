package com.getinsured.iex.hub.qac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.InformationExchangeSystemType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.StatusType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_types.InsuranceCategoryCodeSimpleType;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_types.InsuranceCategoryCodeType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.EnrollmentDataType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.IdentificationType;

public class EnrollmentDataWrapper implements JSONAware {
	private String exchangeId = null;
	private StatusWrapper qhpStatusWrapper;
	private StatusWrapper csrStatusWrapper;
	private StatusWrapper aptcStatusWrapper;
	private ResponseMetadataWrapper responseWrapper;
	private String insuranceCategory;
	
	
	public EnrollmentDataWrapper(EnrollmentDataType enrollmentData){
		if(enrollmentData != null){
			InformationExchangeSystemType hubExchange = enrollmentData.getInformationExchangeSystem();
			
			if(hubExchange != null){
				IdentificationType  identificationType = hubExchange.getInformationExchangeSystemIdentification();
				if(identificationType != null){
					this.exchangeId = identificationType.getIdentificationID().getValue();
				}
				
			}
			
			StatusType hubAptcStatus = enrollmentData.getInsuranceApplicantAPTCStatus();
			if(hubAptcStatus != null){
				this.setAptcStatus(hubAptcStatus);
			}
			
			StatusType hubCSRStatus = enrollmentData.getInsuranceApplicantCSRStatus();
			if(hubCSRStatus != null){
				this.setCsrStatus(hubCSRStatus);
			}
			
			StatusType hubQHPStatus = enrollmentData.getInsuranceApplicantQHPStatus();
			if(hubQHPStatus != null){
				this.setQhpStatus(hubQHPStatus);
			}
			
			InsurancePlanType hubInsurancePlan = enrollmentData.getInsurancePlan();
			
			if(hubInsurancePlan != null){
				this.setInsurancePlan(hubInsurancePlan);
			}
			
			ResponseMetadataType metadata = enrollmentData.getResponseMetadata();
			
			if(metadata != null){
				this.responseWrapper = new ResponseMetadataWrapper(metadata);
			}
		}
	}
	
	private void setInsurancePlan(InsurancePlanType hubInsurancePlan) {
		InsuranceCategoryCodeType codeType = hubInsurancePlan.getInsurancePlanInsuranceCategoryCode();
		if(codeType != null){
			InsuranceCategoryCodeSimpleType code = codeType.getValue();
			if(code != null){
				this.insuranceCategory = code.name();
			}
		}
	}
	
	public String getExchangeId() {
		return exchangeId;
	}

	public void setExchangeId(String exchangeId) {
		this.exchangeId = exchangeId;
	}


	private void setAptcStatus(StatusType status) {
		if(status != null){
			this.aptcStatusWrapper = new StatusWrapper(status);
		}
	}

	private void setCsrStatus(StatusType status) {
		if(status != null){
			this.csrStatusWrapper = new StatusWrapper(status);
		}
	}

	private void setQhpStatus(StatusType qhpStatus) {
		if(qhpStatus != null){
			this.qhpStatusWrapper = new StatusWrapper(qhpStatus);
		}
	}

	public StatusWrapper getQhpStatusWrapper() {
		return qhpStatusWrapper;
	}

	public StatusWrapper getCsrStatusWrapper() {
		return csrStatusWrapper;
	}

	public StatusWrapper getAptcStatusWrapper() {
		return aptcStatusWrapper;
	}

	public ResponseMetadataWrapper getResponseWrapper() {
		return responseWrapper;
	}

	public String getInsuranceCategory() {
		return insuranceCategory;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ExchangeIdentification",this.exchangeId);
		obj.put("AptcStatus", this.aptcStatusWrapper);
		obj.put("QHPStatus", this.qhpStatusWrapper);
		obj.put("CSRStatus", this.csrStatusWrapper);
		obj.put("InsuranceCategoryCode", insuranceCategory);
		if(this.responseWrapper != null){
			obj.put("ResponseMetadata", this.responseWrapper);
		}
		return obj.toJSONString();
	}
	

}
