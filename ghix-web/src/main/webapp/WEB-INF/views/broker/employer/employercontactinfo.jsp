<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript">
	$(function() {
		$(".newcommentiframe").click(function(e){
	        e.preventDefault();
	        var href = $(this).attr('href');
	        if (href.indexOf('#') != 0) {
	        	$('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal();
			}
		});
	});
	
	function closeIFrame() {
		$("#newcommentiframe").remove();
		window.location.href = '/hix/broker/employer/employercomments/'+'${encryptedId}';
	}
	function closeCommentBox() {
		$("#newcommentiframe").remove();		
		var url = '/hix/broker/employer/employercomments/'+'${encryptedId}';
		window.location.assign(url);
	}
</script>
<div class="gutter10-lr">
	<c:set var="encryptedId" ><encryptor:enc value="${employer.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/employers"/>"><spring:message code="label.employers"/></a></li>
			<li><spring:message code="label.brkactive"/></li>
			<li>${employer.name}</li>
			<li><spring:message code="label.agent.employers.Contacts"/></li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->

	<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div><!--  Latest UI -->
	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>
        <div class="row-fluid">
	    <div id="sidebar" class="span3">
	       <div class="nav nav-list">
		      <h1>${employer.name}</h1>
		   </div>
		</div>
	 </div>
		<%-- <h1>${employer.name} <small>&nbsp;<spring:message code="label.brkcontactname"/>: ${employer.contactFirstName} ${employer.contactLastName}</small></h1>
 --%>	<div class="row-fluid">
			<div id="sidebar" class="span3">
					<div class="header">
						<h4><spring:message code="label.agent.employers.aboutThisEmployer"/></h4>
					</div>
						<ul class="nav nav-list">
							<li><a href="/hix/broker/employer/employercase/${encryptedId}"><spring:message code="label.agent.employers.Summary"/></a></li>
		                 	<li class="active"><a href="#"><spring:message code="label.agent.employers.contactInformation"/></a></li>
		                  	<li><a href="/hix/broker/employer/employercomments/${encryptedId}"><spring:message code="label.agent.employers.Comments"/></a></li>
		                </ul>
				<br>
					<div class="header">
						<h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
					</div>
						<ul class="nav nav-list">
		                  	<c:choose>
								<c:when test="${showSwitchRolePopup == \"N\"}">
				                    <c:if test="${showPopupInFuture == null}">  
										<li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
									</c:if>
									<c:if test="${showPopupInFuture != null}">
					 					<li><a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
									</c:if>
								</c:when>
								<c:otherwise>
									<c:if test="${showPopupInFuture == null}">
										<li><a href="#viewempModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>  
									</c:if>
									<c:if test="${showPopupInFuture != null}">
						 				<li><a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employer.id}&switchToResourceName=${employer.name}&showPopupInFuture=on"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.agent.employers.ViewEmployerAccount"/></a></li>
									</c:if>
								</c:otherwise>
							</c:choose>
		                	<%-- <li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"><i class="icon-envelope-unread"></i> <spring:message code="label.agent.employers.ComposeMessage"/></a></li>
              				 --%>
              				 <li><a name="addComment" href="<c:url value="/broker/employer/newcomment?target_id=${encryptedId}&target_name=DESIGNATEBROKER&employerName=${employer.name}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.agent.employers.NewComment"/></a></li>
              				 <c:if test="${employerActivated == false}">
								<li><a href="<c:url value="/broker/sendActivationLink/${encryptedId}"/>" class=""><spring:message code="label.sendActivationLink"/></a></li>
							</c:if>
		                </ul>
				</div>

		<!-- Modal -->
		<%-- Secure Inbox Start--%>
		<%-- <jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
		<%-- Secure Inbox End--%>
		
		<div id="rightpanel" class="span9">
		
			<form class="form-horizontal" id="" name="" action="" method="POST">
			 <div class="header">
				<h4 class="pull-left">
					<spring:message code="label.agent.employers.contactInformation"/></h4>
			 </div>				
				<div class="gutter10">
				 <table class="table table-border-none">		
					<tbody>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.brkcontactname"/></th>
							<td><strong>${employer.contactFirstName} ${employer.contactLastName}</strong></td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.PreferredContactMethod"/></th>
							<td><strong>${employer.communicationPref}</strong></td>
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.brkContactNumber"/></th>
							<c:if test= "${employer.contactNumber ne \"0\"}">
								<td><strong>${employer.contactNumber}</strong></td>
							</c:if>							
						</tr>
						<tr>
							<th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.emailAddress"/></th>
							<td><strong>${employer.contactEmail}</strong></td>
						</tr>
						 <tr>
						 <th class="span4 txt-right" scope="row"><spring:message code="label.agent.employers.Address"/></th>
						 <td><strong>
								${employer.contactLocation.address1} <br />
								<c:if test="${!empty fn:trim(employer.contactLocation.address2) }">
									${employer.contactLocation.address2}</br>
								</c:if>
								${employer.contactLocation.city},
								${employer.contactLocation.state} <br />
								${employer.contactLocation.zip}
						</strong></td>
						</tr> 
						<%-- <tr>
							
						</tr>  --%>
					</tbody>
				</table>
				</div>
				<input type="hidden" name="employerName" id="employerName" value="${employer.name}" />
			</form>

		</div>
		</div>	
	</div>	
		
		 
		<!-- Modal -->
			<jsp:include page="employerviewmodal.jsp" />
		<!-- Modal end -->
<!--  Latest UI -->