<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<!--Report Change-->
<script type="text/javascript">
$(document).ready(function(){

function validateForm(){
	var isValid=true;
	var checked = $("#reportChange-form input:radio").length > 0;
	if (!$("input[type=radio]:checked").val()) {
    	isValid=false;
        alert("Please select at least one event");
    }
    else{
    		var isEmployee = '${isEmployee}';
	    	var loopLimit = (isEmployee=='Y') ? 8 : 12;
    		for(var i=0;i<loopLimit;i++){
        		
        		var checkEvent=document.getElementsByName('employeeEvents['+i+'].eventType');
        		var dateVal=document.getElementsByName('employeeEvents['+i+'].eventDate');
        		
        		if(checkEvent[0].checked){
        			if(dateVal[0].value == '' || dateVal[0].value == 'undefined'){
        				alert("Please select event date for the event selected.");	
        				isValid=false;
        				break;
        			}
        		} 
        	}    	
        }
	return isValid;
}
	    
$('#qualifyingeventbtn').click(function() {
	var isEmployee = '${isEmployee}';
   	if(validateForm()) {
   		if(isEmployee=='Y'){
   	   		var dateText=$("#marriage-date").val()+ '~' + $("#birth-date").val() + '~' + $("#adoption-date").val() + '~' + $("#divorce-date").val() + '~' +
   	      	 			 $("#seperation-date").val() + '~' + $("#expired-date").val() + '~' + $("#g-eligibility-date").val() + '~' + $("#l-eligibility-date").val();
   	   	}
   	   	else{
   	   		var dateText=$("#marriage-date").val()+ '~' + $("#birth-date").val() + '~' + $("#adoption-date").val() + '~' + $("#divorce-date").val() + '~' +
   	      	 			 $("#seperation-date").val() + '~' + $("#expired-date").val() + '~' + $("#g-eligibility-date").val() + '~' + $("#l-eligibility-date").val()+ '~' +
   	      				 $("#ne-eligibility-date").val() + '~' +$("#ex-eligibility-date").val() + '~' +$("#fc-eligibility-date").val() + '~' +$("#ec-eligibility-date").val();
   	   	}
   		
    var subUrl = '<c:url value="/shop/employee/spplanselection/validatequalifyingevent"> 
                    <c:param name="${df:csrfTokenParameter()}"> 
                      <df:csrfToken plainToken="true" />
                    </c:param> 
                  </c:url>';
   	$.ajax(
   			{
           		type: "POST",
          		url: subUrl,
          		data : {dateListStr : dateText}, 
           		dataType:'json',
           		success: function(response,xhr){
                if(isInvalidCSRFToken(xhr))
                  return;

           			handleResponse(response);
           		},
          			error: function(e){
          				alert('Failed to validate Event Date.');

           		}
           });
   	}
   	
   });
});

</script>
<div class="gutter10"  id="">

<!--titlebar-->
<div class="row-fluid">
      <div class="span9">
        <h3 id="skip">${employer_name}</h3>
      </div>
</div>
<div class="row-fluid">
    <div class="row-fluid">
      <div class="span3" id="sidebar">
        <div class="header">
        	<h4><spring:message  code='label.youshouldknow'/></h4>
        </div>
        <div class="gutter10">
          <p><spring:message  code='label.terminateYouShouldKnowComment1'/> ${exchangeName} <spring:message  code='label.terminateYouShouldKnowComment2'/> ${exchangeName} <spring:message  code='label.terminateYouShouldKnowComment3'/></p>
          <p><spring:message  code='label.terminateYouShouldKnowComment4'/></p>
        </div>
      </div>
      
      <c:if test="${not empty result_message}">
      	<div class="span9" id="rightpanelerror">
      		<div class="gutter10">
      		${result_message}
      		</div>
      	</div>	
      </c:if>
      <!--sidebar-->
      <c:if test="${empty result_message}">
      <div class="span9" id="rightpanel">
      	<div class="header">
			<h4>Report Change in circumstances</h4>
		</div>
		<!--titlebar-->
		 <h3>Please provide information about qualifying event(s) you wish to report.</h3>
          <p>Based on the information you provide, you may be eligible for Special Enrollment in which you can add/remove family members from coverage or change your selected plan.</p>
		<div class="gutter10">
		<h1>Please provide information about qualifying event you wish to report</h1>
			<form class="form-horizontal" id="frmterminatecoverage" name="frmterminatecoverage" action="<c:url value='/shop/employee/spplanselection/coverageterminate/${employeeApplication.id}'/>" method="post" >
              <df:csrfToken/>
	        <div id="report-change" >
          <div id="select-event">
            <fieldset>
              <legend>Event Type</legend>
                  <div class="row-fluid height30">
                      <input type="hidden" name="employeeEvents[0].employeeApplicationId" value="${employeeApplication.id}"/>
                      <input type="hidden" name="employeeEvents[0].createdBy" value="${userid}"/>
                  	  <input type="hidden" name="employeeEvents[0].eventCode" value="32"/>
                  	  <input type="hidden" name="employeeEvents[0].spEventCode" value="32"/>
                      <label class="radio inline" for="marriage">
                        <input type="radio" name="employeeEvents[0].eventType" id="marriage" value="Marriage">
                            Marriage
                      </label>
                      <span data-date-format="mm/dd/yyyy" class="input-append date pull-right hide">
                          <input type="text" readonly="readonly" id="marriage-date" name="employeeEvents[0].eventDate" class="span7 datePicker1" >
                          <span class="add-on"><i class="icon-calendar"></i></span>
                      </span>
                  </div>
                  <div class="row-fluid height30">
                    <input type="hidden" name="employeeEvents[1].employeeApplicationId" value="${employeeApplication.id}"/>
                    <input type="hidden" name="employeeEvents[1].createdBy" value="${userid}"/>
                  	<input type="hidden" name="employeeEvents[1].eventCode" value="02"/>
                  	<input type="hidden" name="employeeEvents[1].spEventCode" value="02"/>
                    <label  class="radio inline" for="birth">
                      <input type="radio" name="employeeEvents[1].eventType" id="birth" value="Birth of Child">
                      Birth of Child
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="birth-date" readonly="readonly"  name="employeeEvents[1].eventDate" class="span7 datePicker1" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                  	 <input type="hidden" name="employeeEvents[2].employeeApplicationId" value="${employeeApplication.id}"/>
                     <input type="hidden" name="employeeEvents[2].createdBy" value="${userid}"/>
                  	 <input type="hidden" name="employeeEvents[2].eventCode" value="05"/>
                  	 <input type="hidden" name="employeeEvents[2].spEventCode" value="05"/>
                    <label class="radio inline" for="adoption">
                      <input type="radio" name="employeeEvents[2].eventType" id="adoption" value="Adoption">
                      Adoption
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="adoption-date" readonly="readonly"  name="employeeEvents[2].eventDate" class="span7 datePicker1" aria-label="Adoption" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                   <input type="hidden" name="employeeEvents[3].employeeApplicationId" value="${employeeApplication.id}"/>
                   <input type="hidden" name="employeeEvents[3].createdBy" value="${userid}"/>
                   <input type="hidden" name="employeeEvents[3].eventCode" value="01"/>
                   <input type="hidden" name="employeeEvents[3].spEventCode" value="07"/>
                    <label class="radio inline" for="divorce">
                      <input type="radio" name="employeeEvents[3].eventType" id="divorce" value="Divorce">
                      Divorce
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="divorce-date" readonly="readonly"  name="employeeEvents[3].eventDate" class="span7 datePicker1" aria-label="Divorce" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                    <input type="hidden" name="employeeEvents[4].employeeApplicationId" value="${employeeApplication.id}"/>
                    <input type="hidden" name="employeeEvents[4].createdBy" value="${userid}"/>
                  	<input type="hidden" name="employeeEvents[4].eventCode" value="31"/>
                  	<input type="hidden" name="employeeEvents[4].spEventCode" value="07"/>
                    <label class="radio inline" for="seperation">
                      <input type="radio" name="employeeEvents[4].eventType" id="seperation" value="Legal Seperation">
                      Legal Separation
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="seperation-date" readonly="readonly" name="employeeEvents[4].eventDate" class="span7 datePicker1" aria-label="Seperation" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                   <input type="hidden" name="employeeEvents[5].employeeApplicationId" value="${employeeApplication.id}"/>
                   <input type="hidden" name="employeeEvents[5].createdBy" value="${userid}"/>
                   <input type="hidden" name="employeeEvents[5].eventCode" value="03"/>
                   <input type="hidden" name="employeeEvents[5].spEventCode" value="07"/>
                    <label class="radio inline" for="death">
                      <input type="radio" name="employeeEvents[5].eventType" id="death" value="Death of a Family Member">
                      Death of a Family Member
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="expired-date" readonly="readonly"  name="employeeEvents[5].eventDate" class="span7 datePicker1" aria-label="Death" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                   <input type="hidden" name="employeeEvents[6].employeeApplicationId" value="${employeeApplication.id}"/>
                   <input type="hidden" name="employeeEvents[6].createdBy" value="${userid}"/>
                   <input type="hidden" name="employeeEvents[6].eventCode" value="07"/>
                   <input type="hidden" name="employeeEvents[6].spEventCode" value="07"/>
                  <label for="gained-eligibility" class="radio inline">
                    <input type="radio" name="employeeEvents[6].eventType" id="gained-eligibility" value="Gained Eligibility for other">
                    Loss of MEC(Minimum Eligibility Coverage)
                  </label>
                  <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                    <input type="text" id="g-eligibility-date" readonly="readonly"  name="employeeEvents[6].eventDate" class="span7 datePicker1" aria-label="Gained Eligibility for other" tabindex="0">
                    <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
                  <div class="row-fluid height30">
                   <input type="hidden" name="employeeEvents[7].employeeApplicationId" value="${employeeApplication.id}"/>
                   <input type="hidden" name="employeeEvents[7].createdBy" value="${userid}"/>
                   <input type="hidden" name="employeeEvents[7].eventCode" value="07"/>
                   <input type="hidden" name="employeeEvents[7].spEventCode" value="07"/>
                    <label class="radio inline" for="lost-eligibility">
                   <input type="radio" name="employeeEvents[7].eventType" id="lost-eligibility" value="Lost Eligibility for other Coverage">
                      Loss or Medicaid or CHP coverage
                    </label>
                    <span data-date-format="mm/dd/yyyy" data-date="" class="input-append date  pull-right hide">
                          <input type="text" id="l-eligibility-date" readonly="readonly"  name="employeeEvents[7].eventDate" class="span7 datePicker1" aria-label="Lost Eligibility for other" tabindex="0">
                          <span class="add-on"><i class="icon-calendar"></i></span>
                    </span>
                  </div>
  
              </fieldset>
          </div>
        </div> 
				
			
			
			<div class="form-actions">
				<div  class="pull-right">
					<a  href="javascript:history.back();" name="cancelButton" id="cancelButton" class="btn"><spring:message code="label.cancelButton" /></a>
        			<input type="button" name="mainSubmitButton" id="mainSubmitButton" onclick="javascript:submitForm();"  class="btn btn-primary" value="<spring:message code="label.submit"/>" /></div>
       			</div>
			
			</form>
			</div><!--rightpanel-->
			</div>
		</c:if>
      </div>
    </div>
  </div>

<script type="text/javascript">
function submitForm(){
	if ($("#frmterminatecoverage").validate().form()){
		$("#frmterminatecoverage").submit();
	}
}

jQuery.validator.addMethod("esignNamecheck", function(value, element, param) {
	var firstname = '${loggedInUser.firstName}'.replace('&amp;','&');
	var lastname = '${loggedInUser.lastName}'.replace('&amp;','&');
	var esign = $("#esignature").val();
	var userName = firstname + " " + lastname;
	if( esign.toLowerCase() == userName.toLowerCase() ){
		return true ;
	}
	return false;
});

var validator = $("#frmterminatecoverage").validate({ 
	onkeyup: false,
	onclick: false,
	onfocusout: false,
	onSubmit: true,
	rules : {
		'terminationDate' : { required : true},
		'reason' : { required : true},
		'esignature' : { required : true , esignNamecheck : true}
	
	} ,// rules end
	
	messages : {
		'terminationDate' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateTerminationDate' javaScriptEscape='true'/></span>"},
		'reason' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateReason' javaScriptEscape='true'/></span>"},
		'esignature' : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateesignature' javaScriptEscape='true'/></span>",
			 esignNamecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEsignUserName'/></span>"}
	},
	
	});
</script>	