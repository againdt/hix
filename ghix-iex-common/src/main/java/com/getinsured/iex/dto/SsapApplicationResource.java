package com.getinsured.iex.dto;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Date;
import org.springframework.hateoas.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;


@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicationResource extends ResourceSupport {

	public long applicationId;
	public String applicationData;
	public String currentPageId;
	private String applicationStatus;
	private ApplicationValidationStatus validationStatus;
	private String applicationType;
	private BigDecimal cmrHouseoldId;
	private BigDecimal createdBy;
	private Timestamp creationTimestamp;
	private Timestamp esignDate;
	private String esignFirstName;
	private String esignLastName;
	private String esignMiddleName;
	private String caseNumber;
	private BigDecimal lastNoticeId;
	private Timestamp lastUpdateTimestamp;
	private String source;
	private BigDecimal ssapApplicationSectionId;
	private long coverageYear;
	private String allowEnrollment;
	private String financialAssistanceFlag;
	private String exchangeEligibilityStatus;
	private String eligibilityStatus;
	private String exemptHouseHold;
	
	private Date eligibilityReceivedDate;
	private BigDecimal ehbAmount;
	private String csrLevel;
	private String nativeAmerican;
	private BigDecimal maximumAPTC;
	private BigDecimal electedAPTC;
	private BigDecimal availableAPTC;
	//HIX-113026
	private BigDecimal maximumStateSubsidy;
	private BigDecimal electedStateSubsidy;
	private BigDecimal availableStateSubsidy;
	private Timestamp startDate;
	private BigDecimal lastUpdatedBy;
	
	private String eligibilityResponseType;
	
	private Long parentSsapApplicationId;
	private String enrollmentStatus;
	private String renewalStatus;
	
	private String sepQepDenied;
	
	private String ptfFirstName;
	
	private String ptfMiddleName;
	
	private String ptfLastName;

	//HIX-113602 adding eligibility start date,end date and due date from at_span_info
	private Date dueDate;
	private Date eligibilityStartDate;
	private Date eligibilityEndDate;
	private String queuedStatus;
	private String applicationDentalStatus;

	public String getQueuedStatus() {
		return queuedStatus;
	}

	public void setQueuedStatus(String queuedStatus) {
		this.queuedStatus = queuedStatus;
	}

	public long getApplicationId()
	{
		return applicationId;
	}

	public void setApplicationId(final long applicationId)
	{
		this.applicationId = applicationId;
	}

	public String getSepQepDenied() {
		return sepQepDenied;
	}

	public void setSepQepDenied(String sepQepDenied) {
		this.sepQepDenied = sepQepDenied;
	}
	
	public String getRenewalStatus() {
		return renewalStatus;
	}

	public void setRenewalStatus(String renewalStatus) {
		this.renewalStatus = renewalStatus;
	}

	public String getEnrollmentStatus() {
		return enrollmentStatus;
	}

	public void setEnrollmentStatus(String enrollmentStatus) {
		this.enrollmentStatus = enrollmentStatus;
	}
	public Long getParentSsapApplicationId() {
		return parentSsapApplicationId;
	}

	public void setParentSsapApplicationId(Long parentSsapApplicationId) {
		this.parentSsapApplicationId = parentSsapApplicationId;
	}
	
	private String externalApplicationId;
	
	public String getExchangeEligibilityStatus() {
		return exchangeEligibilityStatus;
	}

	public void setExchangeEligibilityStatus(String exchangeEligibilityStatus) {
		this.exchangeEligibilityStatus = exchangeEligibilityStatus;
	}

	public String getFinancialAssistanceFlag() {
		return financialAssistanceFlag;
	}

	public void setFinancialAssistanceFlag(String financialAssistanceFlag) {
		this.financialAssistanceFlag = financialAssistanceFlag;
	}

	public ApplicationValidationStatus getValidationStatus() {
		return validationStatus;
	}

	public void setValidationStatus(ApplicationValidationStatus validationStatus) {
		this.validationStatus = validationStatus;
	}

	public String getAllowEnrollment() {
		return allowEnrollment;
	}

	public void setAllowEnrollment(String allowEnrollment) {
		this.allowEnrollment = allowEnrollment;
	}

	public String getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(String applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
	}

	public BigDecimal getCmrHouseoldId() {
		return cmrHouseoldId;
	}

	public void setCmrHouseoldId(BigDecimal cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}

	public BigDecimal getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(BigDecimal createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEsignDate() {
		return esignDate;
	}

	public void setEsignDate(Timestamp esignDate) {
		this.esignDate = esignDate;
	}

	public String getEsignFirstName() {
		return esignFirstName;
	}

	public void setEsignFirstName(String esignFirstName) {
		this.esignFirstName = esignFirstName;
	}

	public String getEsignLastName() {
		return esignLastName;
	}

	public void setEsignLastName(String esignLastName) {
		this.esignLastName = esignLastName;
	}

	public String getEsignMiddleName() {
		return esignMiddleName;
	}

	public void setEsignMiddleName(String esignMiddleName) {
		this.esignMiddleName = esignMiddleName;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public BigDecimal getLastNoticeId() {
		return lastNoticeId;
	}

	public void setLastNoticeId(BigDecimal lastNoticeId) {
		this.lastNoticeId = lastNoticeId;
	}

	public Timestamp getLastUpdateTimestamp() {
		return lastUpdateTimestamp;
	}

	public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
		this.lastUpdateTimestamp = lastUpdateTimestamp;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public BigDecimal getSsapApplicationSectionId() {
		return ssapApplicationSectionId;
	}

	public void setSsapApplicationSectionId(BigDecimal ssapApplicationSectionId) {
		this.ssapApplicationSectionId = ssapApplicationSectionId;
	}

	public Object _links;

	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}

	public String getApplicationData() {
		return applicationData;
	}

	public void setApplicationData(String applicationData) {
		this.applicationData = applicationData;
	}
	
	public String getCurrentPageId() {
		return currentPageId;
	}

	public void setCurrentPageId(String currentPageId) {
		this.currentPageId = currentPageId;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	
	public String getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(String eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getExemptHouseHold() {
		return exemptHouseHold;
	}

	public void setExemptHouseHold(String exemptHouseHold) {
		this.exemptHouseHold = exemptHouseHold;
	}

	public Date getEligibilityReceivedDate() {
		return eligibilityReceivedDate;
	}

	public void setEligibilityReceivedDate(Date eligibilityReceivedDate) {
		this.eligibilityReceivedDate = eligibilityReceivedDate;
	}

	public BigDecimal getEhbAmount() {
		return ehbAmount;
	}

	public void setEhbAmount(BigDecimal ehbAmount) {
		this.ehbAmount = ehbAmount;
	}

	public String getCsrLevel() {
		return csrLevel;
	}

	public void setCsrLevel(String csrLevel) {
		this.csrLevel = csrLevel;
	}

	public String getNativeAmerican() {
		return nativeAmerican;
	}

	public void setNativeAmerican(String nativeAmerican) {
		this.nativeAmerican = nativeAmerican;
	}

	public BigDecimal getMaximumAPTC() {
		return maximumAPTC;
	}

	public void setMaximumAPTC(BigDecimal maximumAPTC) {
		this.maximumAPTC = maximumAPTC;
	}

	public BigDecimal getElectedAPTC() {
		return electedAPTC;
	}

	public void setElectedAPTC(BigDecimal electedAPTC) {
		this.electedAPTC = electedAPTC;
	}

	public BigDecimal getAvailableAPTC() {
		return availableAPTC;
	}

	public void setAvailableAPTC(BigDecimal availableAPTC) {
		this.availableAPTC = availableAPTC;
	}

	public Timestamp getStartDate() {
		return startDate;
	}

	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	public BigDecimal getLastUpdatedBy() {
		return lastUpdatedBy;
	}

	public void setLastUpdatedBy(BigDecimal lastUpdatedBy) {
		this.lastUpdatedBy = lastUpdatedBy;
	}

	public String getEligibilityResponseType() {
		return eligibilityResponseType;
	}

	public void setEligibilityResponseType(String eligibilityResponseType) {
		this.eligibilityResponseType = eligibilityResponseType;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public String getPtfFirstName() {
		return ptfFirstName;
	}

	public void setPtfFirstName(String ptfFirstName) {
		this.ptfFirstName = ptfFirstName;
	}

	public String getPtfMiddleName() {
		return ptfMiddleName;
	}

	public void setPtfMiddleName(String ptfMiddleName) {
		this.ptfMiddleName = ptfMiddleName;
	}

	public String getPtfLastName() {
		return ptfLastName;
	}

	public void setPtfLastName(String ptfLastName) {
		this.ptfLastName = ptfLastName;
	}
	public Date getDueDate() {
		return dueDate;
	}

	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}

	public Date getEligibilityStartDate() {
		return eligibilityStartDate;
	}

	public void setEligibilityStartDate(Date eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}

	public Date getEligibilityEndDate() {
		return eligibilityEndDate;
	}

	public void setEligibilityEndDate(Date eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}

	

	public BigDecimal getMaximumStateSubsidy() {
		return maximumStateSubsidy;
	}

	public void setMaximumStateSubsidy(BigDecimal maximumStateSubsidy) {
		this.maximumStateSubsidy = maximumStateSubsidy;
	}

	public BigDecimal getElectedStateSubsidy() {
		return electedStateSubsidy;
	}

	public void setElectedStateSubsidy(BigDecimal electedStateSubsidy) {
		this.electedStateSubsidy = electedStateSubsidy;
	}

	public BigDecimal getAvailableStateSubsidy() {
		return availableStateSubsidy;
	}

	public void setAvailableStateSubsidy(BigDecimal availableStateSubsidy) {
		this.availableStateSubsidy = availableStateSubsidy;
	}
	
	public String getApplicationDentalStatus() {
		return applicationDentalStatus;
	}

	public void setApplicationDentalStatus(String applicationDentalStatus) {
		this.applicationDentalStatus = applicationDentalStatus;
	}

}
