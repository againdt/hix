package com.getinsured.ssap.model.hub.vlp;

import java.util.StringJoiner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * VLP Reverify Request.
 * @author Yevgen Golubenko
 * @since 7/19/19
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class VlpReverifyRequest {
  private long applicationId;
  private String clientIp;

  @JsonProperty("CasePOCFullName")
  private String casePocFullName;

  @JsonProperty("CasePOCPhoneNumber")
  private String casePocPhoneNumber;

  private VlpReverifyPayload payload;

  public long getApplicationId() {
    return applicationId;
  }

  public void setApplicationId(long applicationId) {
    this.applicationId = applicationId;
  }

  public String getClientIp() {
    return clientIp;
  }

  public void setClientIp(String clientIp) {
    this.clientIp = clientIp;
  }

  public VlpReverifyPayload getPayload() {
    return payload;
  }

  public void setPayload(VlpReverifyPayload payload) {
    this.payload = payload;
  }

  public String getCasePocFullName() {
    return casePocFullName;
  }

  public void setCasePocFullName(String casePocFullName) {
    this.casePocFullName = casePocFullName;
  }

  public String getCasePocPhoneNumber() {
    return casePocPhoneNumber;
  }

  public void setCasePocPhoneNumber(String casePocPhoneNumber) {
    this.casePocPhoneNumber = casePocPhoneNumber;
  }

  @Override
  public String toString() {
    return new StringJoiner(", ", VlpReverifyRequest.class.getSimpleName() + "[", "]")
      .add("applicationId=" + applicationId)
      .add("clientIp='" + clientIp + "'")
      .add("casePocFullName='" + casePocFullName + "'")
      .add("casePocPhoneNumber='" + casePocPhoneNumber + "'")
      .add("payload=" + payload)
      .toString();
  }
}
