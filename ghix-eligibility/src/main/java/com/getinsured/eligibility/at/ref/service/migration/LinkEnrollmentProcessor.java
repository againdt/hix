package com.getinsured.eligibility.at.ref.service.migration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.repository.IAccountTransferMigrationRepository;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentMemberDTO;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.ssap.model.AccountTransferMigration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.EnrollmentReferralRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.service.SsapService;
import com.getinsured.timeshift.util.TSDate;

@Component
public class LinkEnrollmentProcessor {
	
	private static final Logger LOGGER = Logger.getLogger(LinkEnrollmentProcessor.class);
	
	private static final String SUCCESS = "SUCCESS";
	private static final String HEALTH = "HLT";
	
	@Autowired
	private IAccountTransferMigrationRepository iAccountTransferMigrationRepository;
	
	@Autowired 
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired 
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired 
	private SsapService ssapService;
	
	@Autowired
	private MigrationUtil migrationUtil;
	
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	
	@Autowired
	private EnrollmentReferralRepository enrollmentReferralRepository;
	
	public void linkEnrollment(long applicationId){
		
		try {
			SsapApplication ssapApplication = ssapApplicationRepository.findById(applicationId);
			
			List<AccountTransferMigration> accountTransferMigrations = iAccountTransferMigrationRepository.findByNewSsapApplicationId(applicationId);
			if(accountTransferMigrations!=null){
				for(AccountTransferMigration accountTransferMigration:accountTransferMigrations){
					if(accountTransferMigration.getEnrollmentId()!=null){
						EnrollmentMemberDTO enrollmentMemberDTO = new EnrollmentMemberDTO();
						enrollmentMemberDTO.setEnrollmentId(accountTransferMigration.getEnrollmentId().intValue());
						enrollmentMemberDTO.setSsapApplicationId(applicationId);
						
						enrollmentMemberDTO = ghixRestTemplate.postForObject(EnrollmentEndPoints.UPDATE_ENROLLMENT_SSAP_APPLICATION_ID, enrollmentMemberDTO, EnrollmentMemberDTO.class);
						
						LOGGER.info("Enrollment Response : "+ enrollmentMemberDTO);
						
						if(enrollmentMemberDTO!=null && SUCCESS.equalsIgnoreCase(enrollmentMemberDTO.getStatus())){
							Enrollment enrollment = enrollmentReferralRepository.findById(enrollmentMemberDTO.getEnrollmentId());
							if(!ApplicationStatus.CLOSED.getApplicationStatusCode().toString().equalsIgnoreCase(ssapApplication.getApplicationStatus()) && 
									!ApplicationStatus.CANCELLED.getApplicationStatusCode().toString().equalsIgnoreCase(ssapApplication.getApplicationStatus()) && 
									HEALTH.equalsIgnoreCase(enrollment.getInsuranceTypeLkp().getLookupValueCode()) && 
									enrollmentMemberDTO.getMemberIdSet()!=null && enrollmentMemberDTO.getMemberIdSet().size()>0){
								SsapApplicationRequest ssapApplicationRequest = new SsapApplicationRequest();
								ssapApplicationRequest.setExchgIndivIdList(new ArrayList<>(enrollmentMemberDTO.getMemberIdSet()));
								ssapApplicationRequest.setSsapApplicationId(enrollmentMemberDTO.getSsapApplicationId());
								ssapApplicationRequest.setApplicationStatus(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
								
								SsapApplication updatedSsapApplication = ssapApplicationRepository.findById(applicationId);
								if(!ApplicationStatus.ENROLLED_OR_ACTIVE.toString().equalsIgnoreCase(updatedSsapApplication.getApplicationStatus())){
									updateSsapApplicationStatusById(ssapApplicationRequest);
								}
							}
							migrationUtil.updateStatus(accountTransferMigration, AccountTransferMigration.StatusType.ENROLLMENT_LINKED.getValue());
						} else {
							LOGGER.info("Enrollment Linking Failed for Application : "+ applicationId);
						}
					}
				}
			} 
		} catch(Exception exception){
			LOGGER.error("Exception occurred while linking enrollment : ", exception);
			migrationUtil.persistGiMonitorId(applicationId, exception);
		}
	}
	
	private int updateSsapApplicationStatusById(SsapApplicationRequest ssapApplicationRequest) throws GIException {
		String applicationStatus = ssapApplicationRequest.getApplicationStatus();
		long ssapApplicationId = ssapApplicationRequest.getSsapApplicationId();
		int count = 0;
		
		try {
			count = ssapService.updateSsapAppStatusOnMemberStatus(ssapApplicationRequest.getExchgIndivIdList(),applicationStatus, ssapApplicationId, new Timestamp(new TSDate().getTime()));

			if (count == 1 && "EN".equalsIgnoreCase(applicationStatus)) {
				ssapApplicationEventRepository.updateDatesForEvent(ssapApplicationId);
			}
			
			if ("EN".equalsIgnoreCase(applicationStatus)) {
				ssapApplicationEventRepository.updateChangePlan(ssapApplicationId, "N");
			}
		} catch(Exception ex) {
			LOGGER.error("Error in updating the ssap application status for ssapApplicationId:" + ssapApplicationId, ex);
			throw new GIException(ex);
		}
		
		return count;
	}
	
}
