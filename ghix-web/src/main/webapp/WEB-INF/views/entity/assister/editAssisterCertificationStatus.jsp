<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page contentType="text/html" import="java.util.*" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
			
			<style type="text/css">
.table tr th{
               background: none !important;
               height: 14px;
               font-weight: normal;
               color: #5A5A5A;
       }
</style>

<%
Calendar date = Calendar.getInstance();
date.setTime(new Date());
date.add(Calendar.YEAR, 1);
Date defaultRenewalDate = date.getTime();
Date currDateOnLocalScope = new Date();
%>

				<fmt:formatDate value="<%= currDateOnLocalScope %>" pattern="MM/dd/yyyy" var="dateForJscript" />
				<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
				<div class="gutter10-lr">
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.manage"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${assisterId}" />">${assister.firstName} ${assister.lastName}</a></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
					<div class="row-fluid">
						<h1><a id="skip"></a>${assister.firstName} ${assister.lastName}</h1>
					</div>
					<div class="row-fluid">
						<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
						<div class="span9">
							<form class="form-horizontal" id="frmupdatecertification"  name="frmupdatecertification" enctype="multipart/form-data" method="post" action="<c:url value="/entity/assisteradmin/uploaddocument"/>">
							<df:csrfToken/>
								<input type="hidden" id="brokerId" name="brokerId" value="194"><input type="hidden" id="licenseNumber" name="licenseNumber" value="3242183">
								<input type="hidden" id="documentId" name="documentId" value="<encryptor:enc value="${uploadedDocument}"/>"> 
								<div class="row-fluid">
									<div class="header">
										<h4 class="pull-left"><spring:message code="label.assister.certificationStatus"/></h4>
										<a class="btn btn-small pull-right" onclick="clearUploadComponents();" href="<c:url value="/entity/assisteradmin/certificationstatus?assisterId=${assisterId}" />" title="<spring:message code="label.assister.cancel"/>"><spring:message code="label.assister.cancel"/></a>
									</div>
									<input type="hidden" name="assisterId" id="assisterId" value="<encryptor:enc value="${assister.id}"/>" />
									<div class="gutter10">
										<table class="table table-border-none table-condensed table-auto">
												<tr>
												   <th class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterNumber"/></th>
												   <td><strong>${assister.assisterNumber}</strong></td>
											    </tr>
												<tr>
													<th class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationStatus"/></th>
													<td><strong>${assister.certificationStatus}</strong></td>
													
												</tr>
												<tr>
													<th class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationRenewalDate"/></th>
													<td><strong><fmt:formatDate value="${assister.reCertificationDate}" pattern="MM/dd/yyyy"/></strong></td>
												</tr>
												<tr>
													<th class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationNumber"/></th>
													<td><strong><fmt:formatNumber minIntegerDigits="10" value="${assister.certificationNumber}" groupingUsed="FALSE"/></strong></td>
												</tr>
												<tr id = "date1234">
													<th class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationStartDate"/></th>
												    <td> 
												    	<div class="input-append date date-picker" id="date" data-date="">
															
															<c:choose>
																<c:when test="${currDate < assisterCertificationDate}">
																	 <input class="span10" type="text" name="certificationStartDate" id="certificationStartDate"  value= "<fmt:formatDate value= '${assister.certificationDate}' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>	
																</c:when>
																<c:otherwise>
																	<input class="span10" type="text" name="certificationStartDate" id="certificationStartDate"  value= "<fmt:formatDate value= '<%= new java.util.Date() %>' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
																</c:otherwise>
															</c:choose>
														</div>	
														<div id="certificationStartDate_error"></div>					         
													</td>
												</tr>
												<tr id = "date123">
													<th class="span4 txt-right" scope="row"><spring:message code="label.assister.certificationRenewalDate"/></th>
												    <td> 
												    	<div class="input-append date date-picker" id="date" data-date="">
															
														<%-- 	<c:choose>
																<c:when test="${currDate < assisterCertificationRenewalDate}">
																	 <input class="span10" type="text" name="certificationRenewalDate" id="certificationRenewalDate"  value= "<fmt:formatDate value= '${assister.reCertificationDate}' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>	
																</c:when>
																<c:otherwise> --%>
																	<input class="span10" type="text" name="certificationRenewalDate" id="certificationRenewalDate"  value= "<fmt:formatDate value= '<%= defaultRenewalDate %>' pattern="MM/dd/yyyy"/>" pattern="MM/dd/yyyy"/> <span class="add-on"><i class="icon-calendar"></i></span>
																<%-- </c:otherwise>
															</c:choose> --%>
														</div>	
														<div id="certificationRenewalDate_error"></div>					         
													</td>
												</tr>
												<tr>
													<th class="span4 txt-right" scope="row"><label for="certificationStatus"><spring:message code="label.assister.newStatus"/></label></th>
													<td>	
														<select size="1" id="certificationStatus" name="certificationStatus" class="span10">
															<option value=""><spring:message code="label.assister.select"/></option>
															<c:forEach var="status" items="${statusList}">
																<option value="${status.lookupValueLabel}" class="hourclass" <c:if test="${newAssister.certificationStatus == status.lookupValueLabel}"> SELECTED </c:if>>
																			${status.lookupValueLabel}</option>																													
															</c:forEach>
														</select>
														<div id="certificationStatus_error"></div>
													</td>
												</tr>
												<tr>
													<th class="span4 txt-right" scope="row"><label for="comments"><spring:message code="label.assister.comment"/>	</label></th>													
													<td><textarea name="comments" id="comments" class="input-xlarge" rows="3" maxlength="4000">${newAssister.comments}</textarea></td>												
												</tr>
												<tr>
													<th class="txt-right"><label for="fileInput"><spring:message code="label.assister.uploadSupportingDocument"/></label></th>
													<td>
						                   				<div>
						                   					<input type="file" class="input-file" id="fileInput" name="fileInput">
						                   					<input type="hidden" id="fileInput_Size" name="fileInput_Size" value="1"/>	
							                   				<input type="button" id="btnUploadDoc" name="btnUploadDoc" value="<spring:message code="label.assister.upload"/>" class="btn"/>
							                   				<div id="uploadedFileDiv"><label id="fileName">${fileName}</label></div>
						                   				</div>
						                   				<div>
															<spring:message code="label.uploadDocumentCaption"/>
														</div>
						                   			</td>	
												</tr>
										</table>
									</div>
								</div>
								<div class="form-actions noBackground">
									<input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"/>
									<input type="button" class="btn btn-primary" name="update" id="update" title="<spring:message code="label.entity.submit"/>" value="<spring:message code="label.entity.submit"/>" onClick="submitForm('update')" />	
									<span id=processing class="margin10-l hide"> <spring:message code="ssap.footer.processing" text="Processing please wait"/><img class="margin10-l" src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></span>
								</div>
								<input type="hidden" name="formAction" id="formAction" value="" />
							</form><!-- Showing comments -->    
                               	<p><spring:message code="label.assister.viewCertificationHistory"/></p>
                               		<div class="header">
                					  <h4><spring:message code="label.assister.certificationHistory"/></h4>   
                					</div>  
                					<div class="gutter10">             
                                      <display:table id="assisterRegisterStatusHistory" name="assisterRegisterStatusHistory" list="assisterRegisterStatusHistory" requestURI="" sort="list" class="table" >
                                          <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MMM dd, yyyy}" sortable="false" />
                                          <display:column property="previousRegistrationStatus" titleKey="label.entity.previousStatus" sortable="false"/>
                                          <display:column property="newRegistrationStatus" titleKey="label.NewStatus" sortable="false"/>
                                                <display:column property="comments" titleKey="label.entity.vieComment" sortable="false"/>
                                                    <display:column titleKey="label.assister.viewAttachment" > 
                                                       <c:choose>
                                                            <c:when test="${assisterRegisterStatusHistory.documentId!=null}">     
                                                               <a href="#" onClick="showdetail('${assisterRegisterStatusHistory.documentId}');" id="edit_${assisterRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a> 
                                                         </c:when>
                                                         <c:otherwise>  
                                                               <spring:message code="label.assister.noAttachment"/>
                                                         </c:otherwise>
                                                    </c:choose>
                                                    </display:column>
                                      </display:table> 
                                      </div>                               
						</div><!-- end of span9 -->
					</div>
				</div>
                <div id="modalBox" class="modal hide fade">
					<div class="modal-header">
						<a class="close" data-dismiss="modal" data-original-title="">x</a>
						<h3 id="myModalLabel">
							<spring:message code="label.assister.viewComment"/>
						</h3>
					</div>
					<div id="commentDet" class="modal-body">
						<label id="commentlbl" ></label>						
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
					</div>
				</div>
	<script type="text/javascript">
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
	        String.prototype.trim = function() {
	        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
	        };
		}
	}
		$(document).ready(function() {
			$("#assisterCertification").removeClass("link");
			$("#assisterCertification").addClass("active");
			$("#uploadedFileDiv").hide();
			
			var button_id_names= '#btnUploadDoc';
			
			$(button_id_names).click(function(){

				
				//$('#fileToUpload').val('fileInput');
				var file = $('#fileInput').val();
				
			if(file!=""){
					
				var hiddenVar='fileInput_Size';
				
				if(document.getElementById(hiddenVar).value==1){
					$("#btnUploadDoc").attr('disabled','disabled');
					$('#frmupdatecertification').ajaxSubmit({
						url: "<c:url value='/entity/assisteradmin/uploaddocument/${assisterId}'/>",
						
						success: function(responseText){
							var val = responseText.split("|");  /* For IE*/
							
							$("#btnUploadDoc").removeAttr('disabled');
							if(val[2]==0){
									$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
								} 
								else if(val[2]==-1){
									$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});	
								}
								else if(val[0]==""){
									$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});	
								}
								else{
									//alert("File uploaded Successfully")	;
									
									if(val[0]=='fileInput'){
										$("#documentId").val(val[2]);
									 }
									$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
								}
								if(val[0]=='fileInput'){
									$("#btnUploadDoc").removeAttr('disabled');
								 }
						},
						error : function(responseText){   /* For Firefox*/
							var parsedJSON = eval(responseText);
							var val = parsedJSON.responseText.split("|");
							//alert("error function value: ",val);
							if(val[2]==0){
								//alert("Failed to Upload File");
								$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.failedtouoload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
								} 
							  else if(val[2]==-1){
								$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});	
							   }
							  else if(val[0]==""){
									$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selecteachfilelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});	
								}
								else{
									//alert("File uploaded Successfully")	;
										$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.fileuploadedsuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
								}
							if(val[0]=='fileInput'){
								 $("#btnUploadDoc").removeAttr('disabled');
				     	 }
						}
					});
				}
				
				else if(document.getElementById(hiddenVar).value==0) {
					//alert("Please select a file with size less than 5 MB");
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});

					if ( idName == 'btnUploadDoc') {
						 $("#btnUploadDoc").removeAttr('disabled');
					}					
				}
				
				else if(document.getElementById(hiddenVar).value==-1){
					//alert("Please select a file with size greater than zero");
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectfilegreaterthanzero"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});

					if ( idName == 'btnUploadDoc') {
						 $("#btnUploadDoc").removeAttr('disabled');
					}
				}
			}
			else{
					//alert("Please select a file before upload");
						$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});

						if ( idName == 'btnUploadDoc') {
							 $("#btnUploadDoc").removeAttr('disabled');
						}
					}
				return false; 
			});	// button click close	
			
// 			var uploadedDocument = '${uploadedDocument}';
// 			var uploadedDocumentCheck = '${uploadedDocumentCheck}';
// 			if(uploadedDocumentCheck !="SIZE_FAILURE") {
// 				if(uploadedDocument != '') {
// 					if(uploadedDocument != 0) {
// 						$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 170px;"><h4><spring:message code="label.entity.fileUploadedSuccessfully"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});										
// 					}else{
// 						$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 170px;"><h4><spring:message code="label.entity.UnableToUploadTheFile"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});													
// 					}
// 				}
// 				}
// 				else{
// 					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.validatePleaseSelectFileWithSizeLessThan5MB" javaScriptEscape='true'/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
// 				}
					
		}); // (document).ready -- close
		
		function clearUploadComponents() {
			$("#uploadedFileDiv").hide();
			$("#fileName").val(null);

			
			$("#btnUploadDoc").removeAttr('disabled');
			$.post("/hix/entity/assisteradmin/resetuploads",
					 '',
					  function(data, status){
				});
		}
		
		function submitForm(formAction) {
			ie8Trim();
			$('#formAction').val(formAction);
			if(((formAction == 'upload' && $("#fileInput").val() != '')) || (formAction =='update')) {
				if(formAction == 'update') {
					
					if($("#frmupdatecertification").valid()){
						$("#update").prop("disabled",true);
						$("#processing").show();
					}else{
						$("#update").prop("disabled",false);
						$("#processing").hide();
					};
					$('#frmupdatecertification').attr('action', '/hix/entity/assisteradmin/editcertificationstatus');
					$('#frmupdatecertification').attr('method', 'POST');
					
					
				}
				if($("#fileInput_Size").val()==1){	
				$('#frmupdatecertification').submit();
				}else{
					$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.filelessthan5MB"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
				}
			} else {
				$('<div class="modal popup-address" id="fileUoload"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 100px;"><h4><spring:message code="label.selectfilebeforeupload"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
			}
		}
		
		function getComment(comments)
		{	
			ie8Trim();
			$('#commentlbl').html("<p> Loading Comment...</p>");
			comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br
			comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
			$('#commentlbl').html("<p> "+ comments + "</p>");
			
		}
		
		var validator = $("#frmupdatecertification").validate({ 
			 rules : {
				 certificationStatus : {required : true},
				 certificationStartDate :{requiredWhenCertified :true,dateValidate : true,greaterThanToday : true},
				 certificationRenewalDate :{requiredWhenCertified :true,greaterThanToday : true},
			 },
			 messages : {
				 certificationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateCertificationStatus' javaScriptEscape='true'/></span>"},
			 							
			     certificationStartDate: {requiredWhenCertified: "<span><em class='excl'>!</em><spring:message  code='label.validateCertDate' javaScriptEscape='true'/></span>",
			    	 						dateValidate: "<span><em class='excl'>!</em><spring:message  code='label.validateCertStartAndRenewalDate' javaScriptEscape='true'/></span>",
			    	 						greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='label.errorDate' javaScriptEscape='true'/></span>"},
			    certificationRenewalDate: {requiredWhenCertified : "<span><em class='excl'>!</em><spring:message  code='label.validateCertDate' javaScriptEscape='true'/></span>",
			    	                      greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='label.errorDate' javaScriptEscape='true'/></span>"},
			 },
			 errorClass: "error",
			 errorPlacement: function(error, element) {
			 var elementId = element.attr('id');
			 error.appendTo( $("#" + elementId + "_error"));
			 $("#" + elementId + "_error").attr('class','error help-inline');
			}
		});	
		
		jQuery.validator.addMethod("dateValidate", function(value, element, param) {
			var status=document.getElementById('certificationStatus').value;
					
			var a = $('#certificationStartDate').val();
			
			//new Date(a.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3"));
						
			var b = $('#certificationRenewalDate').val();
			//new Date(b.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3"));
						
			if(status == 'Certified'){
					if((new Date(a.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))  > (new Date(b.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))){
							return false;
						}
					}  
			return true;
		});
		
		$.validator.addMethod("greaterThanToday", function(value, element) {
			
			if(Date.parse(value) >= Date.parse('${dateForJscript}')){
				return true;
			}
			return false;
		});
		
		jQuery.validator.addMethod("requiredWhenCertified", function(value, element, param) {
			var status=document.getElementById('certificationStatus').value;
				if(status == 'Certified'){
					if(value == ""){
						return false;
					}
				}
			return true;
		});
		
		 $(function(){
			 $('#fileInput').change(function(){
					var rv = -1; // Return value assumes failure.
					 if (navigator.appName == 'Microsoft Internet Explorer')
					 {
					    var ua = navigator.userAgent;
					    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
					    if (re.exec(ua) != null)
					       rv = parseFloat( RegExp.$1 );
					 }
					 if(!(rv <= 9.0 && navigator.appName == 'Microsoft Internet Explorer')){
					 var f=this.files[0];
					if((f.size > 5242880)||(f.fileSize > 5242880)){
						$("#fileInput_Size").val(0);
					}
					else{
						$("#fileInput_Size").val(1);	
					} 
					}
					else{
						$("#fileInput_Size").val(1);	
					} 
				});
			});	
		 
		 
//		 certifiedSelection();

$(document).ready(function(){
	$('#date123').hide();
	$('#date1234').hide();
	//document.getElementById('newRegistrationRenewalDate').style.display='none';
});
certifiedSelection();
function showdetail(documentId) {
	ie8Trim();	 
	 var rv = -1; // Return value assumes failure.
	 if (navigator.appName == 'Microsoft Internet Explorer')
	 {
	    var ua = navigator.userAgent;
	    var re  = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
	    if (re.exec(ua) != null)
	       rv = parseFloat( RegExp.$1 );
	 }
	 if(rv <= 8.0 && navigator.appName == 'Microsoft Internet Explorer'){
		 $('<div class="modal popup-address" id="addressIFrame"><div class="modal-header padding0" style="border-bottom:0; "><div class="header"><h4><spring:message  code='label.assister.upgradeBrowser' javaScriptEscape='true'/></h4><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">�</button></div></div><div class="modal-body" style="max-height:470px; padding: 10px 0px;"><spring:message  code='label.assister.upgradeBrowserMessage' javaScriptEscape='true'/></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true">OK</button></div></div>').modal({backdrop:"static",keyboard:"false"});
		 } else {
			 var  contextPath =  "<%=request.getContextPath()%>";
			 var documentUrl = contextPath + "/entity/assisteradmin/viewAttachment?documentId="+documentId;
		     window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
		}
	}	
  
 function changeDate(){
	 ie8Trim();
	 document.getElementById('newRegistrationRenewalDate').style.display='inline';
 }
	
 
 function certifiedSelection(){
	 $(function() {
			
		   var jqDdl = $('#certificationStatus'),
		    onChange = function(event) {
		        if ($(this).val() === 'Certified') {
		            $('#date123').show();
		            $('#date123').focus().select();
		            $('#date1234').show();
		            $('#date1234').focus().select();
		        } else {
		            $('#date123').hide();
			    	$('#date1234').hide();
		        }
		    };
		    onChange.apply(jqDdl.get(0)); // To show/hide the Other textbox initially
		    jqDdl.change(onChange);
		});
 }
		
		$('.date-picker').datepicker({
	        autoclose: true,
	        format: 'mm/dd/yyyy'
		});
</script>
	