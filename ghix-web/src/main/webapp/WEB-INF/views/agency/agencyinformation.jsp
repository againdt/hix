<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<link rel="stylesheet" type="text/css" media="screen" href="<c:url value='/resources/css/agency_portal.css' />"/>

<script type="text/javascript" src="<c:url value='/resources/js/moment.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/angular-moment.min.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/lib/angular-ui-router.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/amplify.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/spring-security-csrf-token-interceptor.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/lib/ui-bootstrap-tpls-0.12.0.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/components/filter/formatPhone.filter.module.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/addressValidation.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/generalModal.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/components/modal.directive.module.js' />"></script>

<script type="text/javascript" src="<c:url value='/resources/js/agency/lib/statehelper.min.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/agency.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/agency.routes.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/agency.module.translate.js' />"></script>

<!--  ************************************  App Services  *************************** -->
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/nav.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/status.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/http.service.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/agencyinfo.service.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/locationhours.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/fileupload.service.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/datastorage.service.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/strtruncate.factory.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/services/timefilter.factory.module.js' />"></script>

<!-- ************************************  Directives/ Components  *************************** -->
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/state.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/timepicker.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/loader.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/enablelink.directive.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/fileUpload.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/yearSelect.directive.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/popover.directive.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/calendar.directive.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/components/autofocus.directive.module.js' />"></script>

<!-- ************************************  Controllers *************************** -->
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyApp.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyGlobalApp.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffApp.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/viewAgentListController.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/viewAdminStaffList.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyInformation.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/locationHour.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/documentUpload.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/certificationStatus.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyDelegation.controller.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/agencyBOB.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/transferConsumerDelegations.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaff.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffApproval.controller.module.js' />"></script>
<script type="text/javascript" src="<c:url value='/resources/js/agency/controllers/adminStaffStatus.controller.module.js' />"></script>

<%
	String agencyId =  (String) request.getSession().getAttribute("landingAgencyId");
	String agencyCertificationStatus =  (String) request.getSession().getAttribute("agencyCertificationStatus");
	String logoutUrl = request.getContextPath()+"/account/user/logout";
	String agencyToIndPopupDisable =  (String) request.getSession().getAttribute("agencyToIndPopupDisable");
	String agencyManagerCertificationStatus =  (String) request.getSession().getAttribute("agencyManagerCertificationStatus");
	String showPopupInFutureForAgency =  (String) request.getSession().getAttribute("showPopupInFutureForAgency");
	String currentUserRole =  (String) request.getSession().getAttribute("currentUserRole");
	String adminStaffApprovalStatus =  (String) request.getSession().getAttribute("adminStaffApprovalStatus");
	String adminStaffActivityStatus =  (String) request.getSession().getAttribute("adminStaffActivityStatus");
%>

<c:set var="agencyId" value="<%=agencyId%>"/>
<c:set var="logoutUrl" value="<%=logoutUrl%>" />
<c:set var="agencyToIndPopupDisable" value="<%=agencyToIndPopupDisable%>" />
<c:set var="agencyManagerCertificationStatus" value="<%=agencyManagerCertificationStatus%>" />
<c:set var="showPopupInFutureForAgency" value="<%=showPopupInFutureForAgency%>" />
<c:set var="currentUserRole" value="<%=currentUserRole%>" />
<c:set var="adminStaffApprovalStatus" value="<%=adminStaffApprovalStatus%>" />
<c:set var="adminStaffActivityStatus" value="<%=adminStaffActivityStatus%>" />

<!-- <c:out value="${switchAccUrl}" /> -->
<input type="hidden" id="agencyId" value="${agencyId}"/>
<input type="hidden" id="agencyCertificationStatus" value="${agencyCertificationStatus}"/>
<input type="hidden" id="agencyManagerCertificationStatus" value="${agencyManagerCertificationStatus}"/>
<input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" id="stateCode" name="stateCode">
<input type="hidden" id="agencyToIndPopupDisable" value="${agencyToIndPopupDisable}"/>
<input type="hidden" id="showPopupInFutureForAgency" value="${showPopupInFutureForAgency}"/>
<input id="currentUserRole" name="currentUserRole" type="hidden" value="${currentUserRole}"/>
<input id="adminSwitchToAgency" type="hidden" value="${sessionScope.adminSwitchToAgency}"/>
<input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>
<input type="hidden" id="logoutUrl" value="${logoutUrl}"/>
<input type="hidden" id="switchAccUrl" value="${switchAccUrl}"/>
<input type="hidden" id="recordType" value="${recordType}"/>
<input type="hidden" id="adminStaffApprovalStatus" value="${adminStaffApprovalStatus}"/>
<input type="hidden" id="adminStaffActivityStatus" value="${adminStaffActivityStatus}"/>

<div ng-app="agencyApp" id="aid-agency-portal" ng-controller="AgencyController">
	<ui-view></ui-view>
</div>

<div id="blockTerminatedAgencyManagerModal" class="modal hide fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-hidden="true">
  	<div class="modal-body">

  		<spring:message code='label.blockTerminatedAgency'/>
	</div>
</div>
