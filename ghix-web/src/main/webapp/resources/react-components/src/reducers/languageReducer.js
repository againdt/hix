import { languageActionTypes } from '../constants/constants';

const initialState = 'en';
export default (state = initialState, action) => {
    switch (action.type) {
        case (languageActionTypes.SET_LANGUAGE):
            return action.language;
        case (languageActionTypes.CLEAR_STATE):
            return initialState;
        default:
            return state;
    }
};