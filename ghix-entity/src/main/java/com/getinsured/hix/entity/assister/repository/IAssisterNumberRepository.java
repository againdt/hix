package com.getinsured.hix.entity.assister.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.entity.AssisterNumber;

/**
 * Spring-JPA repository that encapsulates methods that interact with the AssisterNumber
 * table
 */
public interface IAssisterNumberRepository extends RevisionRepository<AssisterNumber, Integer, Integer>,
		JpaRepository<AssisterNumber, Integer> {

	/**
	 * Fetches the AssisterNumber record against the given assister Id
	 * 
	 * @param assisterId
	 *            the Assister id
	 * @return AssisterNumber record against the given assister Id
	 */
	AssisterNumber findByAssisterId(int assisterId);
}