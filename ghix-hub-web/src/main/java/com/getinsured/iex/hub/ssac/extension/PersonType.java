
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.niem_core._2_0.DateType;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a human being.
 * 
 * <p>Java class for PersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonSSNIdentification"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}PersonName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonBirthDate"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonType", propOrder = {
    "personSSNIdentification",
    "personName",
    "personBirthDate"
})
public class PersonType
    extends ComplexObjectType
{

    @XmlElement(name = "PersonSSNIdentification", required = true)
    protected PersonSSNIdentificationType personSSNIdentification;
    @XmlElement(name = "PersonName", required = true)
    protected PersonNameType personName;
    @XmlElement(name = "PersonBirthDate", namespace = "http://niem.gov/niem/niem-core/2.0", required = true, nillable = true)
    protected DateType personBirthDate;

    /**
     * Gets the value of the personSSNIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link PersonSSNIdentificationType }
     *     
     */
    public PersonSSNIdentificationType getPersonSSNIdentification() {
        return personSSNIdentification;
    }

    /**
     * Sets the value of the personSSNIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonSSNIdentificationType }
     *     
     */
    public void setPersonSSNIdentification(PersonSSNIdentificationType value) {
        this.personSSNIdentification = value;
    }

    /**
     * Gets the value of the personName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getPersonName() {
        return personName;
    }

    /**
     * Sets the value of the personName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setPersonName(PersonNameType value) {
        this.personName = value;
    }

    /**
     * Gets the value of the personBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getPersonBirthDate() {
        return personBirthDate;
    }

    /**
     * Sets the value of the personBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setPersonBirthDate(DateType value) {
        this.personBirthDate = value;
    }

}
