package com.getinsured.hix.planmgmt.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ProviderNetwork;

public interface IProviderNetworkRepository extends JpaRepository<ProviderNetwork, Integer> {
	@Query("SELECT network.name,network.type FROM Network network WHERE network.id = :providerNetworkId")
	Object[] getProvNetDetailsById(@Param("providerNetworkId") int providerNetworkId);
}
