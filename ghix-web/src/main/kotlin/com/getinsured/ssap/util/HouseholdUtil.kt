package com.getinsured.ssap.util

import com.getinsured.iex.ssap.BloodRelationship
import com.getinsured.iex.ssap.HouseholdMember
import com.getinsured.iex.ssap.SingleStreamlinedApplication
import com.getinsured.iex.ssap.TaxHousehold
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus
import com.getinsured.iex.ssap.financial.type.TaxRelationship
import com.getinsured.ssap.service.HouseholdSizeCalculator
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.collections.HashSet

/**
 * Utility methods to provide some information about tax household.
 *
 * @author Yevgen Golubenko
 * @since 6/20/19
 */
class HouseholdUtil {
  companion object {
    private val log = LoggerFactory.getLogger(HouseholdUtil::class.java)

    /**
     * Returns list of members who lives with given person id.
     *
     * @param members List of all [HouseholdMember]'s
     * @param personId peron id of [HouseholdMember]
     * @return List of members living with given person id.
     */
    @JvmStatic
    fun membersLivingWith(members: List<HouseholdMember>, personId: Int): List<HouseholdMember> {
      val member: HouseholdMember? = members.find { it.personId == personId }

      if (member != null) {
        return members.filter { it.personId != personId && it.addressId == member.addressId }
      }

      return emptyList()
    }

    /**
     * Returns list of members who lives with given {@link HouseholdMember}.
     *
     * @param members List of all [HouseholdMember]'s
     * @param member [HouseholdMember]
     * @return List of members living with given member.
     */
    @JvmStatic
    fun membersLivingWith(members: List<HouseholdMember>, member: HouseholdMember): List<HouseholdMember> {
      return membersLivingWith(members, member.personId)
    }

    /**
     * Returns list of members who are NOT living with given person id.
     *
     * @param members List of all [HouseholdMember]'s
     * @param personId person id of the household member
     * @return List of members NOT living with given person id.
     */
    @JvmStatic
    fun membersNotLivingWith(members: List<HouseholdMember>, personId: Int): List<HouseholdMember> {
      val member: HouseholdMember? = members.find { it.personId == personId }

      if (member != null) {
        return members.filter { it.personId != member.personId && it.addressId != member.addressId }
      }

      return emptyList()
    }

    /**
     * Returns list of members who are NOT living with given [HouseholdMember]
     *
     * @param members List of all [HouseholdMember]'s
     * @param member household member
     * @return List of members NOT living with given [HouseholdMember]
     */
    @JvmStatic
    fun membersNotLivingWith(members: List<HouseholdMember>, member: HouseholdMember): List<HouseholdMember> {
      return membersNotLivingWith(members, member.personId)
    }

    /**
     * Returns [Boolean] if given person id has someone with given [BloodRelationshipCode] living with them
     * at the same address.
     *
     * @param members List of all [HouseholdMember]'s
     * @param personId person id of the household member
     * @param relationshipCode relationship code
     * @return true if there is established blood relationship and person living at the same address with them, if you
     * checking if some CHILD lives with PARENT_OF_CHILD, then it will return true as long as ANY parent lives with the
     * given child.
     */
    @JvmStatic
    fun isLivingWith(members: List<HouseholdMember>, personId: Int, relationshipCode: BloodRelationshipCode, primaryTaxFilerPersonId: Int): Boolean {
      val primaryMember = members.find { it.personId == primaryTaxFilerPersonId }
      var member: HouseholdMember? = null
      if (primaryMember != null) {
        member = members.find { it.personId == personId }

        if (member != null) {
          val bloodRelationships = getAllBloodRelationships(members)
          val relatedPeople: List<HouseholdMember> = getRelatedPeople(members, personId, primaryTaxFilerPersonId)

          // TODO: Refactor with streams when this is 100% working
          for (relatedPerson in relatedPeople) {
            if (relatedPerson.personId.toInt() == member.personId) {
              continue;
            }
            for (br in bloodRelationships) {
//                            println("Blood relationship of " + relatedPerson.name.firstName + " [" +
//                                    relatedPerson.personId + "] to [individual: " + br.relatedPersonId + "] is ["
//                                    + br.relation + "] (need: " + relationshipCode.code + ")");

              if (br.individualPersonId == relatedPerson.personId.toString()
                && br.relatedPersonId == member.personId.toString() &&
                br.relation == relationshipCode.code) {
                if (relatedPerson.addressId == member.addressId) {
//                                    println("Found person ${relatedPerson.name.firstName} ${relatedPerson.name.lastName}" +
//                                            " who lives with ${member.name.firstName} ${member.name.lastName} and has $relationshipCode")
                  return true;
                }
              }
            }
          }
        }
      }

      if (member != null) {
        println("No person lives with ${member.name.firstName} ${member.name.lastName} and given relationship of $relationshipCode in given household")
      }
      return false
    }

    /**
     * Returns [Boolean] if given [HouseholdMember] has someone with given [BloodRelationshipCode] living with them
     * at the same address.
     *
     * @param members List of all [HouseholdMember]'s
     * @param member [HouseholdMember]
     * @param relationshipCode relationship code
     * @param primaryTaxFilerPersonId primary tax filer person id.
     * @return true if there is established blood relationship and person living at the same address with them, if you
     * checking if some CHILD lives with PARENT_OF_CHILD, then it will return true as long as ANY parent lives with the
     * given child.
     */
    @JvmStatic
    fun isLivingWith(members: List<HouseholdMember>, member: HouseholdMember, relationshipCode: BloodRelationshipCode, primaryTaxFilerPersonId: Int): Boolean {
      return isLivingWith(members, member.personId, relationshipCode, primaryTaxFilerPersonId)
    }

    /**
     * Returns [Boolean] if given [member] is living with any of the given [members].
     * @param members List of all [HouseholdMember]'s
     * @param member [HouseholdMember]
     * @return [Boolean] if [member] lives with any given [members]
     */
    @JvmStatic
    fun isLivingWith(members: List<HouseholdMember>, member: HouseholdMember): Boolean {
      return members.stream().anyMatch { m -> m.addressId == member.addressId };
    }

    /**
     * Returns related people for given [HouseholdMember]
     * @param members all household members
     * @param personId personId for who related people should be returned
     * @return list of related people
     */
    @JvmStatic
    fun getRelatedPeople(members: List<HouseholdMember>, personId: Int, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      var relatedPeople = ArrayList<HouseholdMember>()
      val primaryMember = getPrimaryTaxFiler(members, primaryTaxFilerPersonId)

      // TODO: Change to stream() once finalized
      val bloodRelationships = getAllBloodRelationships(members)

      val relatedPeopleRelationships: List<BloodRelationship> = bloodRelationships
        .filter { it.individualPersonId == personId.toString() }

      for (br in relatedPeopleRelationships) {
        val hhm = members.find { it.personId == br.relatedPersonId.toInt() }
        if (hhm != null) {
          if (hhm.personId == personId) {
            continue;
          }

          relatedPeople.add(hhm)
        }
      }

      return relatedPeople
    }

    /**
     * Returns related people for given [HouseholdMember]
     * @param members all household members
     * @param member person for who related people should be returned
     * @param primaryTaxFilerPersonId primary's tax filer person id.
     * @return list of related people
     */
    @JvmStatic
    fun getRelatedPeople(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      return getRelatedPeople(members, member.personId, primaryTaxFilerPersonId)
    }

    @JvmStatic
    fun getRelationOf(members: List<HouseholdMember>, to: HouseholdMember, of: HouseholdMember): BloodRelationshipCode {
      val relationships = getAllBloodRelationships(members)

      for (relationship in relationships) {
        if (relationship.individualPersonId == of.personId.toString() &&
          relationship.relatedPersonId == to.personId.toString()) {
          return BloodRelationshipCode.getBloodRelationForCode(relationship.relation)
        }
      }

      return BloodRelationshipCode.UNSPECIFIED_RELATIONSHIP;
    }

    /**
     * Returns boolean flag indicating if given [BloodRelationshipCode] is
     * type of custodial parent.
     *
     * @param bloodRelationshipCode [BloodRelationshipCode] to check.
     * @return true if given [BloodRelationshipCode] is type of custodial parent.
     */
    @JvmStatic
    fun isCustodialParentRelationship(bloodRelationshipCode: BloodRelationshipCode): Boolean {
      val custodialRelationships = HashSet<BloodRelationshipCode>()
      custodialRelationships.add(BloodRelationshipCode.PARENTS_DOMESTIC_PARTNER)
      custodialRelationships.add(BloodRelationshipCode.PARENT_CARETAKER_OF_THE_WARD)
      custodialRelationships.add(BloodRelationshipCode.PARENT_OF_CHILD)
      custodialRelationships.add(BloodRelationshipCode.PARENT_OF_FOSTER_CHILD)
      custodialRelationships.add(BloodRelationshipCode.PARENT_OF_ADOPTED_CHILD)
      custodialRelationships.add(BloodRelationshipCode.STEPPARENT)

      return custodialRelationships.contains(bloodRelationshipCode)
    }

    /**
     * Returns list of all blood relationships in current household.
     *
     * UI currently saves [BloodRelationship] list inside of primary tax household object.
     *
     * @param members all [HouseholdMember]'s
     * @return list of all [BloodRelationship] defined in this household.
     */
    @JvmStatic
    fun getAllBloodRelationships(members: List<HouseholdMember>): List<BloodRelationship> {
      var bloodRelationships =  members[0].bloodRelationship

      when {
        bloodRelationships.size == 0 -> {
          val potentialMemberWithRelationships = members.find { m -> m.bloodRelationship.size > 0 }

          bloodRelationships = when {
            potentialMemberWithRelationships != null -> potentialMemberWithRelationships.bloodRelationship
            else -> mutableListOf()
          }
        }
      }

      return bloodRelationships
    }

    /**
     * Returns primary tax filer from given list of household members.
     * @param members List of [HouseholdMember]'s.
     * @param primaryTaxFilerPersonId primary tax filer person id.
     * @return primary tax filer.
     * @throws IllegalStateException if passed list of household members doesn't contain all members (e.g. sublist/filtered list was passed).
     */
    @JvmStatic
    fun getPrimaryTaxFiler(members: List<HouseholdMember>, primaryTaxFilerPersonId: Int): HouseholdMember {
      log.debug("Looking up primary tax filer from {} members", members.size)

      return members.stream().filter { hhm -> hhm.personId == primaryTaxFilerPersonId }
        .findFirst()
        .orElseThrow {
          IllegalStateException("Could not get primary tax filer Household Member, " +
            "did you pass whole list of household members to this method? household size: ${members.size}; " +
            "primaryTaxFilerPersonId: $primaryTaxFilerPersonId")
        }
    }

    @JvmStatic
    fun getAllParents(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      val relatedPeople = getRelatedPeople(members, member, primaryTaxFilerPersonId)
      val parentCodes = HashSet<BloodRelationshipCode>()
      parentCodes.add(BloodRelationshipCode.PARENT_OF_ADOPTED_CHILD)
      parentCodes.add(BloodRelationshipCode.PARENT_OF_FOSTER_CHILD)
      parentCodes.add(BloodRelationshipCode.PARENT_OF_CHILD)
      parentCodes.add(BloodRelationshipCode.PARENT_CARETAKER_OF_THE_WARD)
      parentCodes.add(BloodRelationshipCode.PARENTS_DOMESTIC_PARTNER)

      val parents = ArrayList<HouseholdMember>()
      for (related in relatedPeople) {
        val code = getRelationOf(members, member, related)
        if (parentCodes.contains(code)) {
          parents.add(related)
        }
      }

      return parents
    }

    @JvmStatic
    fun getAllSiblings(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      val relatedPeople = getRelatedPeople(members, member, primaryTaxFilerPersonId)
      val siblingCodes = HashSet<BloodRelationshipCode>()
      siblingCodes.add(BloodRelationshipCode.SIBLING_BROTHER_SISTER)
      siblingCodes.add(BloodRelationshipCode.BROTHER_SISTER_IN_LAW)

      val siblings = ArrayList<HouseholdMember>()
      for (related in relatedPeople) {
        val code = getRelationOf(members, member, related)
        if (siblingCodes.contains(code)) {
          siblings.add(related)
        }
      }

      return siblings
    }

    @JvmStatic
    fun getAllChildren(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      val relatedPeople = getRelatedPeople(members, member, primaryTaxFilerPersonId)
      val childrenCodes = HashSet<BloodRelationshipCode>()
      childrenCodes.add(BloodRelationshipCode.CHILD)
      childrenCodes.add(BloodRelationshipCode.CHILD_OF_DOMESTIC_PARTNER)
      childrenCodes.add(BloodRelationshipCode.FOSTER_CHILD_SON_DAUGHTER)
      childrenCodes.add(BloodRelationshipCode.STEPCHILD)

      val children = ArrayList<HouseholdMember>()
      for (related in relatedPeople) {
        val code = getRelationOf(members, member, related)
        if (childrenCodes.contains(code)) {
          children.add(related)
        }
      }

      return children
    }

    @JvmStatic
    fun getNumberOfExpectedChildrenForAllPregnantExcept(members: List<HouseholdMember>, member: HouseholdMember): Int {
      return 0
    }

    @JvmStatic
    fun isMarried(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): Boolean {
      val relatedPeople = getRelatedPeople(members, member, primaryTaxFilerPersonId)
      relatedPeople.forEach { related ->
        val code = getRelationOf(members, related, member)
        if (code == BloodRelationshipCode.SPOUSE) {
          return true;
        }
      }
      return false;
    }

    @JvmStatic
    fun getParentsLivingWith(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): List<HouseholdMember> {
      val parents: MutableList<HouseholdMember> = getAllParents(members, member, primaryTaxFilerPersonId).toMutableList();
      parents.add(member); // Add member to parents list as next method is using member.addressId.

      val parentsLivingWith = membersLivingWith(parents, member);
      return parentsLivingWith;
    }

    @JvmStatic
    fun getParentsLivingWithTaxFilingStatus(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): TaxFilingStatus {
      val parents = getParentsLivingWith(members, member, primaryTaxFilerPersonId);

      if (parents.isNotEmpty()) {
        val parent = parents[0];
        return parent.taxHouseholdComposition.taxFilingStatus;
      }
      return TaxFilingStatus.UNSPECIFIED;
    }

    @JvmStatic
    fun isParentsLivingWithFilingStatus(members: List<HouseholdMember>, member: HouseholdMember, taxFilingStatus: TaxFilingStatus, primaryTaxFilerPersonId: Int): Boolean {
      val parentTaxFilingStatus = getParentsLivingWithTaxFilingStatus(members, member, primaryTaxFilerPersonId);
      return parentTaxFilingStatus == taxFilingStatus
    }

    /**
     * Returns all members who are seeking coverage and have seeksQhp flag set to true.
     * @param application [SingleStreamlinedApplication]
     * @return [List] of [HouseholdMember] who are applying for coverage and want to get QHP.
     */
    @JvmStatic
    fun filterHouseholdMembersForVerifications(application: SingleStreamlinedApplication): List<HouseholdMember> {
      return getPrimaryTaxHouseholdMembers(application).filter {
        m ->
        m.applyingForCoverageIndicator != null && m.applyingForCoverageIndicator == true && m.isSeeksQhp
      }
    }

    /**
     * Returns [Boolean] if anyone in given application wants to deal with IRS.
     * @param application [SingleStreamlinedApplication] object.
     */
    @JvmStatic
    fun anyoneHasTaxRelationship(application: SingleStreamlinedApplication): Boolean {
      return anyoneHasTaxRelationship(application.taxHousehold[0])
    }

    /**
     * Returns [Boolean] if anyone in given tax household wants to deal with IRS.
     * @param taxHousehold tax household.
     */
    @JvmStatic
    fun anyoneHasTaxRelationship(taxHousehold: TaxHousehold): Boolean {
      return taxHousehold.householdMember.any { hhm -> hhm.taxHouseholdComposition?.taxRelationship != TaxRelationship.UNSPECIFIED }
    }

    /**
     * Returns [Boolean] flag indicating if this person is a dangling member who does not belong to any tax household.
     *
     * @param householdMember [HouseholdMember] object.
     */
    @JvmStatic
    fun isDanglingMember(householdMember: HouseholdMember): Boolean {
      return householdMember.taxHouseholdComposition.taxFilingStatus == TaxFilingStatus.UNSPECIFIED && householdMember.taxHouseholdComposition.taxRelationship ==
        TaxRelationship.UNSPECIFIED
    }

    /**
     * Returns [List] of [HouseholdMember] who do not belong to any tax household.
     * @param taxHousehold [TaxHousehold] object.
     * @return [List] of [HouseholdMember] objects.
     */
    @JvmStatic
    fun getDanglingMembers(taxHousehold: TaxHousehold): List<HouseholdMember> {
      return taxHousehold.householdMember.filter { isDanglingMember(it) }
    }

    /**
     * Returns [List] of [HouseholdMember] who do not belong to any tax household.
     * @param application [SingleStreamlinedApplication] object.
     * @return [List] of [HouseholdMember] objects.
     */
    @JvmStatic
    fun getDanglingMembers(application: SingleStreamlinedApplication): List<HouseholdMember> {
      return getDanglingMembers(application.taxHousehold[0])
    }

    /**
     * Returns list of all members that belong to primary tax household group.
     *
     * @param application [SingleStreamlinedApplication] that contains all [HouseholdMember] in question.
     * @return [List] of [HouseholdMember] who belong to primary tax household group.
     */
    @JvmStatic
    fun getPrimaryTaxHouseholdMembers(application: SingleStreamlinedApplication): List<HouseholdMember> {
      val taxHousehold = application.taxHousehold[0]
      val primaryTaxFiler = getPrimaryTaxHouseholdMember(taxHousehold)

      val taxFilingHousehold = anyoneHasTaxRelationship(application)

      // Non financial app tax relationship is UNSPECIFIED, and for financial when nobody has any interest to deal with IRS:
      if(!taxFilingHousehold || (application.applyingForFinancialAssistanceIndicator != null && !application.applyingForFinancialAssistanceIndicator)) {
        return taxHousehold.householdMember.filter {
          it.householdId == primaryTaxFiler.householdId
        }
      }

      var spouse: HouseholdMember? = null
      var domesticPartner: HouseholdMember? = null

      // If married filing separately, householdId is different for each spouse, but spouse is always
      // part of the primary tax household member list.
      if(primaryTaxFiler.taxHouseholdComposition?.taxFilingStatus == TaxFilingStatus.FILING_SEPARATELY) {
        spouse = HouseholdSizeCalculator.getSpouse(taxHousehold.householdMember, primaryTaxFiler);
      }

      var primaryHasChildrenWithDomesticPartner = false

      if(primaryTaxFiler.taxHouseholdComposition?.taxFilingStatus == TaxFilingStatus.HEAD_OF_HOUSEHOLD) {
        domesticPartner = HouseholdSizeCalculator.getDomesticPartner(taxHousehold.householdMember, primaryTaxFiler)
        primaryHasChildrenWithDomesticPartner = hasChildrenWithDomesticPartner(taxHousehold.householdMember, primaryTaxFiler, primaryTaxFiler.personId)
      }

      val primaryHouseholdMembers: List<HouseholdMember>?

      primaryHouseholdMembers = if (primaryHasChildrenWithDomesticPartner && domesticPartner != null) {
        taxHousehold.householdMember.filter { hhm ->
          (hhm.personId == spouse?.personId) ||
            (hhm.householdId == primaryTaxFiler.householdId ||
                  hhm.personId == domesticPartner.personId)
          /* && hhm.taxHouseholdComposition?.taxRelationship != TaxRelationship.UNSPECIFIED*/
        }
      } else {
        taxHousehold.householdMember.filter { hhm ->
          (hhm.personId == spouse?.personId) ||
              (hhm.householdId == primaryTaxFiler.householdId
                && hhm.personId != domesticPartner?.personId
                /* && hhm.taxHouseholdComposition?.taxRelationship != TaxRelationship.UNSPECIFIED*/)
        }
      }

      return primaryHouseholdMembers
    }

    /**
     * Looks up domestic partner for given member and checks if there is any children together.
     * @param members all household members
     * @param member for which member we need to find domestic partner and check
     * @param primaryTaxFilerPersonId primary tax filer person id.
     * @return true if there is any children with domestic partner of given member.
     */
    @JvmStatic
    fun hasChildrenWithDomesticPartner(members: List<HouseholdMember>, member: HouseholdMember, primaryTaxFilerPersonId: Int): Boolean {
      val domesticPartner = HouseholdSizeCalculator.getDomesticPartner(members, member)
      var primaryHasChildrenWithDomesticPartner = false
      if(domesticPartner != null) {
        val childrenOfPrimary = getAllChildren(members, member, primaryTaxFilerPersonId)

        childrenOfPrimary.forEach { child ->
          val parents = getAllParents(members, child, primaryTaxFilerPersonId);
          parents.forEach{parent ->
            if(parent.personId == domesticPartner.personId) {
              primaryHasChildrenWithDomesticPartner = true
            }
          }
        }
      }

      return primaryHasChildrenWithDomesticPartner
    }

    /**
     * Returns all household members who are not part of the primary tax household group.
     *
     * @param taxHousehold [TaxHousehold] that contains all [HouseholdMember] in question.
     * @return [List] of [HouseholdMember] who are not part of the primary tax household group.
     */
    @JvmStatic
    fun getNonPrimaryTaxHouseholdMembers(application: SingleStreamlinedApplication): List<HouseholdMember> {
      val primaryHouseholdMembers = getPrimaryTaxHouseholdMembers(application)
      return application.taxHousehold[0].householdMember.filter {
        hhm -> !primaryHouseholdMembers.contains(hhm)
      }
    }

    /**
     * Returns primary tax household member by checking [TaxHousehold.primaryTaxFilerPersonId].
     * If there is no member found with personId equal [TaxHousehold.primaryTaxFilerPersonId], then it finds
     * member where [HouseholdMember.personId] equals to 1, if that fails, it just returns 1st member in the list.
     * @param taxHousehold [TaxHousehold] that contains household members.
     */
    @JvmStatic
    fun getPrimaryTaxHouseholdMember(taxHousehold: TaxHousehold): HouseholdMember {
      return Optional.ofNullable(
        taxHousehold.householdMember.find {
          hhm -> hhm.personId == taxHousehold.primaryTaxFilerPersonId
        })
        .orElseGet {
          Optional.ofNullable(taxHousehold.householdMember.find { hhm -> hhm.personId == 1 })
          .orElseGet {
            taxHousehold.householdMember[0]
          }
        }
    }

    /**
     * Returns primary tax household member by checking [TaxHousehold.primaryTaxFilerPersonId].
     * @param taxHouseholds List of [TaxHousehold] objects from which first members of 0-th element will be
     * taken into consideration.
     */
    @JvmStatic
    fun getPrimaryTaxHouseholdMember(taxHouseholds: List<TaxHousehold>): HouseholdMember {
      return getPrimaryTaxHouseholdMember(taxHouseholds[0])
    }

    /**
     * Returns list of [BloodRelationship] from given tax household.
     * [BloodRelationship] list is stored for in [HouseholdMember] where personId is 1.
     *
     * @return List of [BloodRelationship] or empty [List]
     */
    @JvmStatic
    fun getBloodRelationships(taxHousehold: TaxHousehold): List<BloodRelationship> {
      return Optional.ofNullable(taxHousehold.householdMember.find { hhm -> hhm.personId == 1 }?.bloodRelationship).orElseGet { emptyList() }
    }

    /**
     * Returns [Boolean] indicating if given [HouseholdMember] is claimed by someone and thus
     * a dependent of someone.
     * @param householdMember [HouseholdMember] to check
     * @return [Boolean] if given member is a dependent of someone.
     */
    @JvmStatic
    fun isDependent(householdMember: HouseholdMember): Boolean {
      return householdMember.taxHouseholdComposition.claimerIds.isNotEmpty()
    }

    /**
     * Returns [Boolean] if anyone on the given [SingleStreamlinedApplication] among
     * all primary tax household members is seeking coverage, native american and qualified for QHP.
     * @param application [SingleStreamlinedApplication]
     * @return [Boolean] flag if anyone in primary tax household is seeking coverage, native american and qualified for QHP.
     */
    @JvmStatic
    fun anyoneNativeAmericanAndQhp(application: SingleStreamlinedApplication): Boolean {
      val primaryHousehold = getPrimaryTaxHouseholdMembers(application)
      return primaryHousehold.stream().anyMatch { hhm ->
        hhm.applyingForCoverageIndicator != null &&
          hhm.applyingForCoverageIndicator &&
          hhm.americanIndianAlaskaNative != null &&
          hhm.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator != null &&
          hhm.americanIndianAlaskaNative.memberOfFederallyRecognizedTribeIndicator &&
          hhm.eligibilityResponse != null &&
          hhm.eligibilityResponse.isExchangeEligible
      }
    }
  }
}
