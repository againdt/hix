import React, { Fragment, useEffect } from 'react';
import { Field, stopSubmit } from 'redux-form';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _ from 'lodash';

import { days as daysOfWeek } from '../constants/DefaultData';
import {
  START,
  END,
  SELECT_CHOICE,
  CLOSED_NAME
} from '../constants/AvailabilityConstants';
import { Checkbox, Select } from './form';
import timeOptions from '../constants/timeList';
import { FormattedMessage } from 'react-intl';

const TimeEntry = props => {
  const {
    dayOfWeek,
    closedText,
    change,
    formInfo,
    closedValues,
    disabled
  } = props;

  // Read redux closed checkboxes and mark corresponding dropdowns to select
  useEffect(() => {
    if (
      closedValues !== null &&
      closedValues !== 'undefined' &&
      closedValues[_.findIndex(daysOfWeek, day => day === dayOfWeek)] &&
      formInfo[dayOfWeek + START] !== SELECT_CHOICE &&
      formInfo[dayOfWeek + END] !== SELECT_CHOICE
    ) {
      change(dayOfWeek + START, SELECT_CHOICE);
      change(dayOfWeek + END, SELECT_CHOICE);
    }
  }, [closedValues]);

  const getInputStatus = (dayOfWeek, info) => {
    if (info !== null) {
      return info[CLOSED_NAME + dayOfWeek];
    }

    return false;
  };

  const populateOptions = origin => {
    const choices = timeOptions.map((time, index) => {
      return (
        <option value={time.value} key={index}>
          {time.label}
        </option>
      );
    });

    return (
      <Field
        component={Select}
        name={dayOfWeek + origin}
        id={dayOfWeek + origin}
        choices={choices}
        defaultOptionLabel="Select"
        defaultOptionValue={SELECT_CHOICE}
        disabled={getInputStatus(dayOfWeek, formInfo) || disabled}
        origin={origin}
      />
    );
  };

  return (
    <Fragment>
      <div className="ds-l-row ds-u-align-items--center control-group">
        {/*<div className="ds-l-col--3 broker-connect">*/}
        <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--2 broker-connect">
          <span class="gutter10">{dayOfWeek}</span>
        </div>
        <div className="ds-l-col--12 ds-l-sm-col--4 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">{populateOptions('Start')}</div>
        <div className="ds-l-col--12 ds-l-sm-col--1 ds-l-md-col--1 ds-l-lg-col--1 ds-l-xl-col--1">
          <div className="ds-u-text-align--center">
            <p className="gutter10">
              <FormattedMessage
                id="broker.availability.time.to.label"
                defaultMessage="To"
              />
            </p>
          </div>
        </div>
        <div className="ds-l-col--12 ds-l-sm-col--4 ds-l-md-col--3 ds-l-lg-col--3 ds-l-xl-col--3">{populateOptions('End')}</div>
        <div className="ds-l-col--12 ds-l-sm-col--3 ds-l-md-col--2 ds-l-lg-col--2 ds-l-xl-col--3 broker-con">
          <Field
            className="checkbox-1"
            component={Checkbox}
            name={CLOSED_NAME + dayOfWeek}
            id={CLOSED_NAME + dayOfWeek}
            label={closedText}
            dayOfWeek={dayOfWeek}
            disabled={disabled}
          />
        </div>

      </div>
    </Fragment>
  );
};

const mapStateToProps = state => {
  let formInfo = null;
  let applyToAll = null;
  let closedValues = [];

  if (
    state.form &&
    state.form.BrokerConnectHours &&
    state.form.BrokerConnectHours.values
  ) {
    formInfo = state.form.BrokerConnectHours.values;

    if (
      formInfo.closed_Monday !== 'undefined' &&
      formInfo.closed_Tuesday !== 'undefined' &&
      formInfo.closed_Wednesday !== 'undefined' &&
      formInfo.closed_Thursday !== 'undefined' &&
      formInfo.closed_Friday !== 'undefined' &&
      formInfo.closed_Saturday !== 'undefined' &&
      formInfo.closed_Sunday !== 'undefined'
    ) {
      closedValues = [
        formInfo.closed_Monday,
        formInfo.closed_Tuesday,
        formInfo.closed_Wednesday,
        formInfo.closed_Thursday,
        formInfo.closed_Friday,
        formInfo.closed_Saturday,
        formInfo.closed_Sunday
      ];
    }
  }

  return {
    formInfo,
    applyToAll,
    closedValues
  };
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(
    {
      stopSubmit
    },
    dispatch
  )
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TimeEntry);
