<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>


  <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {enable_split_word_search:false},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });
    
  </script>



<!-- HIX-11032 -->
<!-- Breadcrumbs -->
<div class="l-page-breadcrumb hide">
	<!--start page-breadcrumb -->
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<c:choose>          
          	<c:when test="${loggedUser =='entityAdmin' }">  
			<li><a href="#">&lt; <spring:message code="label.entity.back"/></a></li>
			<li><a href="<c:url value="/entity/entityadmin/managelist"/>"><spring:message code="label.entity.entities"/></a></li>
			<li>${enrollmentEntity.entityName}</li>
			 </c:when>
            <c:otherwise>
            </c:otherwise>
           </c:choose>
		</ul>
		<!--page-breadcrumb ends-->
	</div><!-- end of .row-fluid -->
</div><!-- l-page-breadcrumb -->
<!-- Breadcrumbs ENDS-->
<!-- HIX-11032 ends -->

<div class="gutter10">
<%-- <div class="row-fluid">
	<div class="page-header">
		<h1><spring:message code="label.entity.step"/> 1: <spring:message code="label.entity.entityInformation"/></h1>
	</div>
</div> --%>


<!-- Content -->
      <div class="row-fluid margin20-t">
      
   <!-- #sidebar -->      
       <jsp:include page="../leftNavigationMenu.jsp" />
    <!-- #sidebar ENDS -->     
    <!-- #section -->
        <div class="span9" id="rightpanel">
          <div id="section">
			 <div class="header">
			 	<h4><spring:message code="label.entity.step"/> 1: <spring:message code="label.entity.entityInformation"/></h4>
			 </div>
             <form class="form-horizontal" id="entityinfo" name="entityinfo"  action="<c:url value="/entity/enrollmententity/entityinformation"/>" method="POST">
             <df:csrfToken/>
            	<input type="hidden" id="id" name="entityId" value="<encryptor:enc value="${enrollmentEntity.id}"/>" />
            	<input type="hidden" id="userId" name="userId" value="<encryptor:enc value="${enrollmentEntity.user.id}"/>" />          
              <div class="control-group">
              <fieldset>
                <legend class="control-label"><spring:message  code="label.entity.entityType"/>
                	<span class="aria-hidden"><spring:message  code="label.entity.required"/></span>
                	<img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
                </legend>
                <div class="controls">
                  <label class="radio" for="inpersonassistance">                   
                     <c:choose>
						<c:when test="${enrollmentEntity.entityType  == 'In-Person Assistance'}">
							<input type="radio" name="entityType" id="inpersonassistance" value="In-Person Assistance" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="entityType" id="inpersonassistance" value="In-Person Assistance" checked='checked'/>
						</c:otherwise>
					</c:choose>                    
                    <spring:message  code="label.entity.inpersonassistance"/>
                  </label>
                  <label class="radio" for="navigationorganization"> 
                  <c:choose>
						<c:when test="${enrollmentEntity.entityType  == 'Navigation Organization'}">
							<input type="radio" name="entityType" id="navigationorganization" value="Navigation Organization" checked/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="entityType" id="navigationorganization" value="Navigation Organization"/>
						</c:otherwise>
					</c:choose>                   
                    <spring:message  code="label.entity.navigatorOrganization"/>
                  </label>
                  <label class="radio" for="certifiedApplicationCounselor"> 
                    <c:choose>
						<c:when test="${enrollmentEntity.entityType  == 'Certified Application Counselor'}">
							<input type="radio" name="entityType" id="certifiedApplicationCounselor" value="Certified Application Counselor" checked/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="entityType" id="certifiedApplicationCounselor" value="Certified Application Counselor"/>
						</c:otherwise>
					 </c:choose>                   
                    <spring:message  code="label.entity.certifiedApplicationCounselor"/>
                  </label>
                  <div id="entityType_error"></div>
                </div><!-- .controls -->
                </fieldset>
              </div><!-- .control-group -->
              <div class="control-group">
                <label class="control-label" for="entityName"><spring:message  code="label.entity.entityName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">
                   <input type="text" name="entityName" id="entityName" value="${fn:escapeXml(enrollmentEntity.entityName)}" class="input-xlarge" size="30" maxlength="100" placeholder="<spring:message code="label.assister.entityName"/>"/>
                  <div id="entityName_error"></div>
                </div><!-- .controls -->
                </div><!-- .control-group -->         
              <div class="control-group">
                <label class="control-label" for="businessLegalName"><spring:message  code="label.entity.adminBusinessLegalName"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">
                 <%--   <c:choose>
						<c:when test="${enrollmentEntity.businessLegalName!=null && enrollmentEntity.businessLegalName != ''}">						
							<input type="text" name="businessLegalName_readonly" id="businessLegalName" value="${enrollmentEntity.businessLegalName}" readonly="readonly" class="input-xlarge"/>
							<input type="hidden" name="businessLegalName" id="businessLegalName" value="${enrollmentEntity.businessLegalName}" />                                        
						</c:when>
						<c:otherwise> --%>
							 <input type="text" name="businessLegalName" id="businessLegalName" value="${enrollmentEntity.businessLegalName}" class="input-xlarge" size="30" maxlength="50" placeholder="<spring:message code="label.assister.businessLegalName"/>"/>
		<%-- 				</c:otherwise>
					</c:choose>	 --%>	
                                     
                	<div id="businessLegalName_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
             <div class="control-group">
                <label class="control-label" for="primaryEmailAddress"><spring:message  code="label.entity.primaryEmailAddress"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">
                  <c:choose>
						<c:when test="${CA_STATE_CODE}">
							<c:choose>						
								<c:when test="${enrollmentEntity.primaryEmailAddress!=null && enrollmentEntity.primaryEmailAddress != ''}">						
									<input type="text" name="primaryEmailAddress" id="primaryEmailAddress" value= "${enrollmentEntity.primaryEmailAddress}" class="input-xlarge" size="30" maxlength="50"/>                               
								</c:when>
								<c:otherwise>
									<input type="text" name="primaryEmailAddress" id="primaryEmailAddress" value="${enrollmentEntity.primaryEmailAddress}" class="input-xlarge" size="30" maxlength="50" placeholder="<spring:message code="label.entity.entCompany@email.com"/>"/>
								</c:otherwise>
							</c:choose>                                      
						</c:when>
						<c:otherwise>
                  <c:choose>
						<c:when test="${enrollmentEntity.primaryEmailAddress!=null && enrollmentEntity.primaryEmailAddress != ''}">						
							<input type="text" name="primaryEmailAddress_readonly" id="primaryEmailAddress" value="${enrollmentEntity.primaryEmailAddress}" readonly="readonly" class="input-xlarge"/>
							<input type="hidden" name="primaryEmailAddress" id="primaryEmailAddress" value="${enrollmentEntity.primaryEmailAddress}" />                                        
						</c:when>
						<c:otherwise>
							 <input type="text" name="primaryEmailAddress" id="primaryEmailAddress" value="${enrollmentEntity.primaryEmailAddress}" class="input-xlarge" size="30" maxlength="50" placeholder="<spring:message code="label.entity.entCompany@email.com"/>"/>
						</c:otherwise>
					</c:choose>						
						</c:otherwise>
					</c:choose>	              
                  					
                 
                	<div id="primaryEmailAddress_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="primaryPhone1"><spring:message code="label.entity.primaryPhoneNumber"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /> <span class="aria-hidden"><spring:message code="label.firstPhone"/></span></label>
                <label class="aria-hidden" for="primaryPhone2"><spring:message code="label.entity.primaryPhoneNumber"/> <spring:message code="label.secondPhone"/></label>
                <label class="aria-hidden" for="primaryPhone3"><spring:message code="label.entity.primaryPhoneNumber"/> <spring:message code="label.thirdPhone"/></label>
                
                <div class="controls">
                  <input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" aria-required="true" />
                  <input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" aria-required="true"/>
                  <input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx" aria-required="true"/>  
                  <input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="" />      
                  <div id="primaryPhone1_error"></div>
                  <div id="primaryPhone3_error"></div>                            
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">                
                <label class="control-label" for="secondaryPhone1"><spring:message code="label.entity.secondaryPhoneNumber"/> <span class="aria-hidden"><spring:message code="label.firstPhone"/></span></label>
                <label class="aria-hidden" for="secondaryPhone2"><spring:message code="label.entity.secondaryPhoneNumber"/> <spring:message code="label.secondPhone"/></label>
                <label class="aria-hidden" for="secondaryPhone3"><spring:message code="label.entity.secondaryPhoneNumber"/> <spring:message code="label.thirdPhone"/></label>
                <div class="controls">
                  <input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx"  />
                  <input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx"  />
                  <input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>
                  <input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="" />
                  <div id="secondaryPhone1_error"></div>
                  <div id="secondaryPhone3_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
             
              
              <div class="control-group">
              <fieldset>
                <legend class="control-label"><spring:message code="label.entity.preferredMethodofCommunication"/></legend>
                <div class="controls">
                  <label class="radio" for="email">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.communicationPreference  == 'Email'}">
							<input type="radio" name="communicationPreference" id="email" value="Email" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="communicationPreference" id="email" value="Email" checked='checked'/>
						</c:otherwise>
					</c:choose>          
               <spring:message code="label.entity.email"/>
                  </label>
                  <label class="radio" for="phone">
                  	<c:choose>
						<c:when test="${enrollmentEntity.communicationPreference  == 'Phone'}">
							<input type="radio" name="communicationPreference" id="phone" value="Phone" checked/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="communicationPreference" id="phone" value="Phone"/>
						</c:otherwise>
					</c:choose>                          
                   <spring:message code="label.entity.phone"/>
                  </label>
                  <label class="radio" for="fax">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.communicationPreference  == 'Fax'}">
							<input type="radio" name="communicationPreference" id="fax" value="Fax" checked/>
						</c:when>
						<c:otherwise>
							<input type="radio" name="communicationPreference" id="fax" value="Fax"/>
						</c:otherwise>
					</c:choose>     
                    <spring:message code="label.entity.fax"/>
                  </label>
                  <label class="radio" for="mail">                   
                    <c:choose>
						<c:when test="${enrollmentEntity.communicationPreference  == 'Mail'}">
							<input type="radio" name="communicationPreference" id="mail" value="Mail" checked/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="communicationPreference" id="mail" value="Mail"/>
						</c:otherwise>
					</c:choose>
                   <spring:message code="label.entity.mail"/> 
                  </label>
                  <div id="communicationPreference_error"></div>
                </div><!-- .controls -->
                </fieldset>
              </div><!-- .control-group -->
              
               <div class="control-group">                 
                <label class="control-label" for="faxNumber1"><spring:message code="label.entity.faxNumber"/> <img id="secReq" src="<c:url value="/resources/img/requiredAsterisk.png"/>"  alt="Required!" /> <span class="aria-hidden"><spring:message code="label.firstPhone"/></span></label>
                <label class="aria-hidden" for="faxNumber2"><spring:message code="label.entity.faxNumber"/> <spring:message code="label.secondPhone"/></label>
                <label class="aria-hidden" for="faxNumber3"><spring:message code="label.entity.faxNumber"/> <spring:message code="label.thirdPhone"/></label>
                <div class="controls">
                  <input type="text" name="faxNumber1" id="faxNumber1" value="${faxNumber1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="faxNumber2" id="faxNumber2" value="${faxNumber2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
                  <input type="text" name="faxNumber3" id="faxNumber3" value="${faxNumber3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>
                  <input type="hidden" name="faxNumber" id="faxNumber" value="" />
                  <div id="faxNumber1_error"></div>
                  <div id="faxNumber3_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="federalTaxID"><spring:message code="label.entity.federalTaxID"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">
                                
               			<c:choose>
							<c:when test="${enrollmentEntity.federalTaxID!=null && enrollmentEntity.federalTaxID != ''}">						
								<input type="text" name="federalTaxID_readonly" id="federalTaxID" value="${enrollmentEntity.federalTaxID}" readonly="readonly" class="input-xlarge"/>
							    <input type="hidden" name="federalTaxID" id="federalTaxID" value="${enrollmentEntity.federalTaxID}" />                                        
							</c:when>
							<c:otherwise>
								<input type="text" name="federalTaxID" id="federalTaxID" value="${enrollmentEntity.federalTaxID}" class="input-xlarge" size="30" maxlength="9" placeholder="xxxxxxxxx"/>
							</c:otherwise>
						</c:choose>						
					                  
                   <div id="federalTaxID_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="stateTaxID"><spring:message code="label.entity.stateTaxID"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">                  
                  
<%-- 					<c:choose>
						<c:when test="${enrollmentEntity.stateTaxID!=null && enrollmentEntity.stateTaxID != ''}">						
							<input type="text" name="stateTaxID_readonly" id="stateTaxID" value="${enrollmentEntity.stateTaxID}" readonly="readonly" class="input-xlarge" />
							<input type="hidden" name="stateTaxID" id="stateTaxID" value="${enrollmentEntity.stateTaxID}" />                                        
						</c:when>
						<c:otherwise> --%>
							<input type="text" name="stateTaxID" id="stateTaxID" value="${enrollmentEntity.stateTaxID}" class="input-xlarge" size="30" maxlength="12" placeholder="xxxxxxxxxxxx"/>
<%-- 						</c:otherwise>
					</c:choose>		  --%>    
                                    
                  <div id="stateTaxID_error"></div>
                  <div id="number_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label for="orgType" class="control-label"><spring:message code="label.entity.organizationType"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">                  
                  <select id="orgType" name="orgType" path="organizationtypelist" class="input-xlarge">
                    <option value=""><spring:message code="label.entity.select"/></option>
					<c:forEach var="orgtype" items="${organizationtypelist}">
						<option id="${orgtype}" <c:if test="${orgtype == enrollmentEntity.orgType}"> selected="selected"</c:if> value="<c:out value="${orgtype}" />">
							<c:out value="${orgtype}" />
						</option>
					</c:forEach>
					</select>
					 <div id="orgType_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="countiesServed"><spring:message code="label.entity.countiesServed"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                <div class="controls">                                                                 
					<%--  <input placeholder="Dorado, Kings, Alpine" type="text" name="countiesServed" id="countiesServed" class="input-xlarge" size="30" onKeyUp="this.value = removecomma(this.value);" data-items="10" 
                  data-mode="multiple" data-provide="typeahead" data-source="${countiesServedlist}" 
                  autocomplete = "off" size="30" value="${enrollmentEntity.countiesServed}" />	 --%>		
                  <%-- <input type="text" id="countiesServed" data-mode="multiple" data-items="10"  name="countiesServed" placeholder="Dorado, Kings, Alpine" data-source="" data-provide="typeahead" size="30" value="${enrollmentEntity.countiesServed}" onblur="checkCountiesServed()"> --%>	  
                  <select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="countiesServed" name="countiesServed"  class="chosen-select" multiple style="width:350px;" onchange="checkCountiesServed()"></select>
               	  <input type="hidden" id="countiesCheck" name="countiesCheck" value="${enrollmentEntity.countiesServed}">
               	 <div id="countiesCheck_error"></div>
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
             
              
              <div class="control-group">
              <fieldset>
                <legend class="control-label"><spring:message code="label.entity.didYourOrganizationReceiveAnOutReachndEducationGrant"/></legend>
                <div class="controls">
                  <label class="radio" for="educationGrant1">                   
                    <c:choose>
						<c:when test="${enrollmentEntity.grantContractNo != null || enrollmentEntity.grantAwardAmount != null}">											
							 <input type="radio" name="educationGrant" id="educationGrant1" value="yes" checked='checked'/>
						</c:when>
						<c:otherwise>
							 <input type="radio" name="educationGrant" id="educationGrant1" value="yes" />
						</c:otherwise>
					</c:choose>                                
                    <spring:message code="label.entity.yes"/> 
                    </label>                  
                  <label class="radio inline" for="educationGrant2">                    
                    <c:choose>
						<c:when test="${enrollmentEntity.grantContractNo != null || enrollmentEntity.grantAwardAmount != null}">													 
							 <input type="radio" name="educationGrant" id="educationGrant2" value="no">
						</c:when>
						<c:otherwise>
							 <input type="radio" name="educationGrant" id="educationGrant2" value="no" checked="checked" >
						</c:otherwise>
					</c:choose>    
                    <spring:message code="label.entity.no"/> 
                  </label>
                </div><!-- .controls -->
                <div id="educationGrant_error"></div>
                </fieldset>
              </div><!-- .control-group -->
              
	
                <div class="row-fluid" id="educationGrantForm" style="display:none;">
                  <div class="span10 offset1">
                    <h4><spring:message code="label.entity.grantDetails"/></h4>
                    
                    <div class="control-group">
                      <label class="control-label" for="grantContractNo"><spring:message code="label.entity.grantContractNumber"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                      <div class="controls">                        
                         <input type="text" name="grantContractNo" id="grantContractNo" maxlength="20" value="${enrollmentEntity.grantContractNo}"  class="input-xlarge" size="30"/>
                         <div id="grantContractNo_error"></div>
                      </div><!-- .controls -->
                    </div><!-- .control-group -->
                    
                    <div class="control-group">
                      <label class="control-label" for="grantAwardAmount"><spring:message code="label.entity.grantAwardAmount"/> <img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
                      <div class="controls">                        
                        <input type="text" name="grantAwardAmount" id="grantAwardAmount" value="${enrollmentEntity.grantAwardAmount}" class="input-xlarge" size="30"/>
                        <div id="grantAwardAmount_error"></div>
                      </div><!-- .controls -->
                    </div><!-- .control-group -->
                    
                  </div><!-- .span10 offset1 -->
                </div><!-- .row-fluid -->
          
             <c:if test="${enrollmentEntity.id == 0}" >
            <div class="form-actions clear">            	
            	<input type="submit" name="SaveEntityInfo" id="SaveEntityInfo" value="<spring:message code="label.entity.next"/>" class="btn btn-primary offset9" />		            							
			</div>
			</c:if>
			<input type="hidden" name="countiesServedCheck" id="countiesServedCheck" value="0" />
			 <c:if test="${(enrollmentEntity.id != 0)}" >
			 <div class="form-actions">  
	             			                     
		                <c:choose>          
	 		              	 <c:when test="${loggedUser =='entityAdmin'}">                    	                           
   	                             <%-- <a class="btn pull-left"  href="/hix/entity/entityadmin/viewentityinformation/${enrollmentEntity.user.id}"><spring:message code="label.entity.cancel"/></a>
   	                             <a class="btn btn-primary pull-right" onClick="document.getElementById('SaveEnrollCerti').click();" id="SaveEnrollCerti" name="SaveEnrollCerti"><spring:message code="label.entity.save"/></a> --%>
   	                            <c:set var="enrollmentEntityUserId" ><encryptor:enc value="${enrollmentEntity.user.id}" isurl="true"/></c:set>
   	                             <input type="button" class="btn cancel-btn" onclick="window.location.href='/hix/entity/entityadmin/viewentityinformation/${enrollmentEntityUserId}'" value="<spring:message code="label.entity.cancel"/>">
   	                             <input type="button" class="btn btn-primary" onClick="document.getElementById('SaveEnrollCerti').click();" id="SaveEnrollCerti" name="SaveEnrollCerti" value="<spring:message code="label.entity.save"/>">
    		               	 </c:when>
    		               	 <c:when test="${loggedUser != 'entityAdmin' && enrollmentEntity.registrationStatus != 'Incomplete'}">
    		               	 	<input type="button" class="btn cancel-btn" onclick="window.location.href='/hix/entity/enrollmententity/viewentityinformation'" value="<spring:message code="label.entity.cancel"/>">
   	                             <input type="button" class="btn btn-primary" onClick="document.getElementById('SaveEnrollCerti').click();" id="SaveEnrollCerti" name="SaveEnrollCerti" value="<spring:message code="label.entity.save"/>">
    		               	 </c:when>
       		               	 <c:otherwise>             		                       
       		                      <%-- <a class="btn btn-primary pull-right" onClick="document.getElementById('SaveEnrollCerti').click();" id="SaveEnrollCerti" name="SaveEnrollCerti"><spring:message code="label.entity.next"/></a> --%>
       		               		  <input type="button" class="btn btn-primary" onClick="document.getElementById('SaveEnrollCerti').click();" id="SaveEnrollCerti" name="SaveEnrollCerti" value="<spring:message code="label.entity.next"/>">
       		                 </c:otherwise>
           	            </c:choose>
			             
             	</div>
			</c:if>
            </form><!-- .form-horizontal -->                      
          </div><!-- #section --> 
        </div><!-- .span9 -->
    <!-- #section ENDS -->
      </div><!-- .row-fluid -->
<!-- Content ENDS -->
</div>

<script type="text/javascript">
function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
          String.prototype.trim = function() {
           	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
          }
	}
}
var validator = $("#entityinfo").validate({ 
	onkeyup: true,
	onclick: true,
	 ignore: "",
	rules : {
		primaryPhone1 : { numberStartsWithZeroCheck: true},
		primaryPhone3 : { primaryphonecheck : true},
		secondaryPhone1 : { numberStartsWithZeroCheck: true},
		secondaryPhone3 : { secondaryphonecheck : true},
		faxNumber1 : { numberStartsWithZeroCheck: false},
		faxNumber3 : { faxnumbercheck: true, IDCheck: true},
		entityType : { required : true},
		entityName : { required : true,alphaNumeric:false},
		businessLegalName : { required : true},
		primaryEmailAddress : { required : true,email: true},
		communicationPreference : { required : true},
		federalTaxID : { required : true,federalIDCheck: true},
		stateTaxID : { required : true,digits:true},
		orgType : { required : true},
		receivedPayments : { required : true},
		grantContractNo :{grantContractNoRequired :true},
		grantAwardAmount:{grantAwardAmountRequired :true,grantAwardAmountMax :true},
		countiesCheck : { required : true} 		
 },
	messages : {
		primaryPhone1: { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
		primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},		
		secondaryPhone1 : { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
		secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
		faxNumber1 : { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFaxNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
		faxNumber3 : { faxnumbercheck : "<span> <em class='excl'>!</em><spring:message code='label.validateFaxNumber' javaScriptEscape='true'/></span>",
			IDCheck: "<span> <em class='excl'>!</em><spring:message code='label.validateFaxNumber' javaScriptEscape='true'/></span>"},
	    entityType: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEntityType' javaScriptEscape='true'/></span>"},
	    entityName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateEntityName' javaScriptEscape='true'/></span>",
	    	           alphaNumeric:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterAlphaNumericName' javaScriptEscape='true'/></span>"},
	    businessLegalName: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateBusinessLegalName' javaScriptEscape='true'/></span>"},	
	    primaryEmailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryEmailAddress' javaScriptEscape='true'/></span>",
	    	email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
	    communicationPreference: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateCommunicationPreference' javaScriptEscape='true'/></span>"},
	    federalTaxID: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateFederalTaxID' javaScriptEscape='true'/></span>",
	    	federalIDCheck : "<span> <em class='excl'>!</em></em><spring:message code='label.validatePleaseEnterValidFederalID' javaScriptEscape='true'/></span>"},
	    stateTaxID: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateStateTaxID' javaScriptEscape='true'/></span>",
	    	stateTaxIDCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidStateTaxID' javaScriptEscape='true'/></span>"},
	    orgType : { required : "<span> <em class='excl'>!</em><spring:message code='label.validateORG_TYPE' javaScriptEscape='true'/></span>"},
	    countiesCheck: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatCountiesServed' javaScriptEscape='true'/></span>"},
    	grantContractNo:{grantContractNoRequired :"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidGrantContractNo' javaScriptEscape='true'/></span>"},
     	receivedPayments: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatReceivedPayments' javaScriptEscape='true'/></span>"},
  		grantAwardAmount :{grantAwardAmountRequired :"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidGrantAwardAmount' javaScriptEscape='true'/></span>",
	    grantAwardAmountMax :"<span> <em class='excl'>!</em></em><spring:message code='label.validatePleaseEnterAmountLessThan' javaScriptEscape='true'/></span>"}
 	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
	var a=(/^[0-9]*$/.test(primphone1)&& /^[0-9]*$/.test(primphone2)&&/^[0-9]*$/.test(primphone3));
	
	if(!a){
		return false;
	}
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 ) || (primphone1 == '000')) 
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
});

jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
	ie8Trim();
	// This is added to check if user has not entered anything into the textbox
	if((value.length == 0)) {
		return true;
	}
	
	// If user has entered anything then test, if entered value is valid
	var firstChar = value.charAt(0);
	if(firstChar == 0) {
			return false;
	} else{
	    return true;
	}
});

jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {	
	ie8Trim();
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 

	var a=(/^[0-9]*$/.test(secondaryphone1)&& /^[0-9]*$/.test(secondaryphone2)&&/^[0-9]*$/.test(secondaryphone3));	
	
	// This is added because secondary number is not mandatory
	if((secondaryphone1.length == 0) && (secondaryphone2.length == 0)  && (secondaryphone3.length == 0)) {
		return true;
	}
	// If user has entered anything then test, if entered value is valid
	if(!a){
		return false;
	}
	if((secondaryphone1.length < 3 ) || (secondaryphone2.length < 3 )  || (secondaryphone3.length < 4 ) || (isNaN(secondaryphone1)) || (isNaN(secondaryphone2)) || (isNaN(secondaryphone3)) || (secondaryphone1 == '000'))
	{
		return false;
	} else {
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);		
		return true;
	}	
} );

jQuery.validator.addMethod("faxnumbercheck", function(value, element, param) {
	ie8Trim();
	faxnum1 = $("#faxNumber1").val().trim(); 
	faxnum2 = $("#faxNumber2").val().trim(); 
	faxnum3 = $("#faxNumber3").val().trim(); 
	var a=(/^[0-9]*$/.test(faxnum1)&& /^[0-9]*$/.test(faxnum2)&&/^[0-9]*$/.test(faxnum3));
	
	// This is added because fax number is not mandatory
	if((faxnum1.length == 0) && (faxnum2.length == 0)  && (faxnum3.length == 0)) {
		return true;
	}
	if(!a){
		return false;
	}
	// If user has entered anything then test, if entered value is valid
	if((faxnum1.length < 3 ) || (faxnum2.length < 3 )  || (faxnum3.length < 4 ) || (isNaN(faxnum1)) || (isNaN(faxnum2)) || (isNaN(faxnum3)) ||  (faxnum1 == '000')) {
		return false;
	} else {
		$("#faxNumber").val(faxnum1 + faxnum2 + faxnum3);		
		return true;
	}	
});

	function checkCountiesServed(){
		var test1 = document.getElementById("countiesServed").value;
		document.getElementById("countiesCheck").value = test1;
		
		if(test1!=null && test1!=""){
			$('#countiesCheck_error').hide();
		} else {
			$('#countiesCheck_error').show();
		}
	}

jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
	ie8Trim();
	 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
	 
});

jQuery.validator.addMethod("federalIDCheck", function(value, element, param) {
	var fid = $('#federalTaxID').val();
	var check=(/^[0-9]*$/.test(fid));	
	
	if(!check){
		return false;
	}
	if((value == "")  || (isNaN(value)) || (value.length < 9 ) ){
		return false; 
	}
	return true;
});

jQuery.validator.addMethod("stateTaxIDCheck", function(value, element, param) {
	var sid = $('#stateTaxID').val();
	var checking=(/^[a-zA-Z0-9-]+$/.test(sid));	
	
	if(!checking){
		return false;
	}else if(sid.length > 12){
		return false;
	}

	return true;
});

/*Radio button event on educationGrant*/	
    $("input[name=educationGrant]").click(function() {
    	var optionVal = $("input[name=educationGrant]:checked").val();
    	
    	if(optionVal == 'yes') {
    		$('#educationGrantForm').show();		
    	} else if(optionVal == 'no') {		
    		$('#educationGrantForm').hide();		
    	}    	
	});
	    
// 	function shiftbox(element, nextelement) {
// 		maxlength = parseInt(element.getAttribute('maxlength'));
// 		if (element.value.length == maxlength) {
// 			nextelement = document.getElementById(nextelement);
// 			nextelement.focus();
// 		}
// 	}
    
    $(function() {
    	$("#SaveEnrollCerti").click(function(e){
    			
    		if($("#entityinfo").validate().form()) {
    			$("#entityinfo").submit();
    		}
    	});
    });
    

	function split(val) {
	    return val.split(/,\s*/);
	}
	function extractLast(term) {
	    return split(term).pop();
	}
	
	
	 $(document).ready(function() {
		$("#entityInformation").removeClass("link");
		$("#entityInformation").addClass("active");
	   	     
	    $( "#countiesServed").autocomplete({
	        source: function (request, response) {
	            $.getJSON("${pageContext. request. contextPath}/get_countiesServed_list", {
	                term: extractLast(request.term)
	            }, response);
	        },
	        search: function () {
	            // custom minLength
	            var term = extractLast(this.value);
	            if (term.length < 1) {
	                return false;
	            }
	        },
	        focus: function () {
	            // prevent value inserted on focus
	            return false;
	        },
	        select: function (event, ui) {
	            var terms = split(this.value);
	            // remove the current input
	            terms.pop();
	            // add the selected item
	            terms.push(ui.item.value);
	            // add placeholder to get the comma-and-space at the end
	            terms.push("");
	            this.value = terms.join(", ");
	            return false;
	        }
	    });
	    
	    //Added this code so that if education grant exists, it displays this block
		var optVal = $("input[name=educationGrant]:checked").val();    	
    	if(optVal == 'yes') {
    		$('#educationGrantForm').show();		
    	} else if(optVal == 'no') {		
    		$('#educationGrantForm').hide();		
    	}   
    	
    	//HIX-113053 Prepended zero on federalTaxId if the length is less than 9 so that user can proceed even if the fetched ferderalTaxId is less than 9 digits
    	prependZerosOnfederalTaxID();
	});
	 
	 jQuery.validator.addMethod("grantContractNoRequired", function(value, element, param) {
			ie8Trim();
			var education = $("input[name=educationGrant]:checked").val();
			var grantContractNoCheck = $('#grantContractNo').val();
			//alert(grantContractNoCheck);
			var check=(/^[0-9]*$/.test(grantContractNoCheck));
			//alert(check);
		    if(education=="yes"){
				if((value == "")||(isNaN(value))){
					return	false;
				}
				if(!check){
					return false;
				}
		    }
			return true;
		});

		jQuery.validator.addMethod("grantAwardAmountMax", function(value, element, param) {
			
			ie8Trim();
			var education = $("input[name=educationGrant]:checked").val();
		    if(education=="yes"){
				var no=parseFloat(value);
				var decNo=no.toFixed(2);
				if(decNo > 999999999 || decNo < 0){
					return	false;
				} else{
					$("#grantAwardAmount").val(decNo);
					return true;
				}
		   } else{
				return true;
		   }
		});
		
		jQuery.validator.addMethod("grantAwardAmountRequired", function(value, element, param) {
			
			ie8Trim();
			var education = $("input[name=educationGrant]:checked").val();
		    	if(education=="yes"){
					if((value == "")||(isNaN(value))){
						return	false;
					}
			    }
				return true;
			
		});
		
		jQuery.validator.addMethod("IDCheck", function(value, element, param) {
			ie8Trim();
        	var optionVal = $("input[name=communicationPreference]:checked").val();
         	faxnum1 = $("#faxNumber1").val().trim(); 
         	faxnum2 = $("#faxNumber2").val().trim(); 
         	faxnum3 = $("#faxNumber3").val().trim(); 
         
	         if(optionVal == 'Fax') {
                  // This is added because fax number is not mandatory(isNaN(value))
                  if((faxnum1.length == 0) && (faxnum2.length == 0)  && (faxnum3.length == 0)) {
                      return false;
                  }
                  // If user has entered anything then test, if entered value is valid
                  if((faxnum1.length < 3 ) || (faxnum2.length < 3 )  || (faxnum3.length < 4 ) || (faxnum1 == '000')||((isNaN(faxnum1)) && (isNaN(faxnum2))  && (isNaN(faxnum3)))) {
                      return false;
                  } else {
                      $("#faxNumber").val(faxnum1 + faxnum2 + faxnum3);                 
                      return true;
                  }              
	         } else  {       
       	 		$("#faxNumber").val(faxnum1 + faxnum2 + faxnum3);	
       			return true;                        
   		     }           
         });
		$('#countiesServed').focusout(function() {
			ie8Trim();
			 county=$("#countiesServed").val().trim();
			 var test= (county).substring((county).length-1,(county).length);
			 if(test==","){
				 var index = (county).substring(0,(county).length-1);
                $('#countiesServed').val(index);
			 }
			 else{
				  }
     });
</script>
<script type="text/javascript">
	function loadUsers() {
		var countiesServedlist = $("#countiesServed option:selected").text();
		var pathURL = "/hix/entity/enrollmententity/entityinformation/getcountiesServedList";
		$('#errorMsg').val("");
		$.ajax({
			url : pathURL,
			type : "GET",
			data : {
				countiesServedlist : countiesServedlist
			},
			success: function(response){
				loadData(response);
    		},
   			error: function(e){
    			alert("Failed to load Counties");
    		},
		});
		
   	}
	
   $('input[type=radio][name=communicationPreference]').change(function () {
        if ($(this).val() == 'Fax') {
            $('#secReq').show();
            $('#faxNumber1').attr("aria-required","true");
            $("#faxNumber2").attr("aria-required","true");
            $("#faxNumber3").attr("aria-required","true");
        } else {
        	 $('#secReq').hide();
        	 $('#faxNumber1').attr("aria-required","");
        	 $("#faxNumber2").attr("aria-required","");
             $("#faxNumber3").attr("aria-required","");
        }
        
    });
	
    if ($('input:radio[name=communicationPreference]:checked').val() == 'Fax' ) {
        $('#secReq').show();
    } else {
   	 $('#secReq').hide();
    }   

	//load the jquery chosen plugin with the AJAX data
	function loadData() {
		$("#countiesServed").html('');
		var respData = $.parseJSON('${countiesServedlist}');
		var counties='${enrollmentEntity.countiesServed}';
		
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties(respData[key]);
		      }
		      if(isSelected){
		    	  $('#countiesServed').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#countiesServed').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#countiesServed').trigger("liszt:updated");
	   }

	function checkCounties(county){
	     var counties='${enrollmentEntity.countiesServed}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	function pad(number, length) {
		   
	    var str = '' + number;
	    while (str.length < length) {
	        str = '0' + str;
	    }
	   
	    return str;
	}
	
	function prependZerosOnfederalTaxID(){
		var fetchedValue = $('input[name=federalTaxID]').val();
		if(fetchedValue!=null && fetchedValue!=''){
			$('input[name=federalTaxID_readonly').val(pad(fetchedValue,9));
			$('input[name=federalTaxID').val(pad(fetchedValue,9));
		}
	}
	
	$(document).ready(function() {
		loadData();
	});
	
</script>	