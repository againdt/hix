
package com.getinsured.iex.ssap.enums;

/**
 * 
 * @author raguram_p
 *
 *
 */

public enum SuffixEnum {
	
    JR("Jr."),
    SR("Sr."),
    II("II"),
    III("III"),
    IV("IV"),
	V("V"),
	I("I"),
	VI("VI"),
	VIII("VIII"),
	X("X"),
	VII("VII"),
	IX("IX");
    
    private String s = null;
    private SuffixEnum(String s)
    {
    	this.s = s;
    }
    
    public String value() {
        return s;
    }

    public static String value(String v) {
		if (v != null) {
			final SuffixEnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static SuffixEnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static SuffixEnum readValue(String v) {
		SuffixEnum data = null;
		for (SuffixEnum c : SuffixEnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}
}
