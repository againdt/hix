package com.getinsured.eligibility.at.resp.si.enums;

public enum NonNativeAmericanCSREnum {
	
	CS4("73PercentActuarialVarianceLevelSilverPlanCSR"),
	CS5("87PercentActuarialVarianceLevelSilverPlanCSR"),
	CS6("94PercentActuarialVarianceLevelSilverPlanCSR");
	
	private String s = null;
    private NonNativeAmericanCSREnum(String s)
    {
    	this.s = s;
    }
    
    public String value() {
        return s;
    }

    public static String value(String v) {
		if (v != null) {
			final NonNativeAmericanCSREnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static NonNativeAmericanCSREnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static NonNativeAmericanCSREnum readValue(String v) {
		NonNativeAmericanCSREnum data = null;
		for (NonNativeAmericanCSREnum c : NonNativeAmericanCSREnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

}
