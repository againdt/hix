<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>

<script type="text/ng-template" id="mypreferences">

<div ng-controller="CommunicationPreferencesCtrl as vm">
	<div ng-if="vm.loading" class="center margin50-t">
		<img alt="loading" src="<c:url value="/resources/images/loader_gray.gif" />"/>
	</div>
	<div ng-if="!vm.loading">
		<h1 ng-if="vm.isEditMode !== true"><spring:message code="indportal.communicationPreferences.communicationPreferences" /></h1>
		<div class="header" ng-if="vm.isEditMode === true">
  			<h4><spring:message code="indportal.communicationPreferences.communicationPreferences" /></h4>
		</div>
		<div class="alert alert-info">
			<spring:message code="indportal.communicationPreferences.proceedFurther" />
		</div>

		<div class="warning-alert" ng-if="vm.formData.hasActiveEnrollment === true" >
			<spring:message code="indportal.communicationPreferences.linkToDashboard" />
		</div>

		<div class="alert alert-info"  ng-if="!vm.isEditMode" >
			<spring:message code="indportal.communicationPreferences.changeCommunicationMethod" />
		</div>

		<form class="form-horizontal" autocomplete="off" novalidate>


			<ng-form name="vm.addressValidationForm">
				<div class="form-group">
					<label for="addressLine1" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.mailingaddress1" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" name="addressLine1" id="addressLine1" class="input-large" ng-model="vm.formData.locationDto.addressLine1" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{5,75}$/" required ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)">

						<span class="error-message" ng-if="vm.addressValidationForm.addressLine1.$error.required && vm.addressValidationForm.addressLine1.$dirty" >
							<spring:message code="indportal.communicationPreferences.addressError1" />
						</span>
						<span class="error-message" ng-if="vm.addressValidationForm.addressLine1.$error.pattern && vm.addressValidationForm.addressLine1.$dirty" >
							<spring:message code="indportal.communicationPreferences.addressError2" />
						</span>
					</div>
				</div>

				<div class="form-group">
					<label for="addressLine2" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.mailingaddress2" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" name="addressLine2" id="addressLine2" class="input-large" ng-model="vm.formData.locationDto.addressLine2" ng-pattern="/^[A-Za-z0-9'\.\-\s\/\\\\'\#\,]{1,75}$/" ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)">

						<span class="error-message" ng-if="vm.addressValidationForm.addressLine2.$error.pattern && vm.addressValidationForm.addressLine2.$dirty" >
							<spring:message code="indportal.communicationPreferences.addressError2" />
						</span>
					</div>
				</div>

				<div class="form-group">
					<label for="city" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="label.city" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" name="city" id="city" class="input-large" ng-model="vm.formData.locationDto.city" ng-pattern="/^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/" required ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)">

						<span class="error-message" ng-if="vm.addressValidationForm.city.$error.required && vm.addressValidationForm.city.$dirty">
							<spring:message code="indportal.communicationPreferences.cityError1" />
						</span>
						<span class="error-message" ng-if="vm.addressValidationForm.city.$error.pattern && vm.addressValidationForm.city.$dirty">
							<spring:message code="indportal.communicationPreferences.cityError2" />
						</span>
					</div>
				</div>


				<div class="form-group">
					<label for="state" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="label.state" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<select id="state" ng-model="vm.formData.locationDto.state" name="state" class="input-large" required ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)" ng-options="state.value as state.displayName for state in vm.states" ng-change="vm.getCounty('state')"></select>
						<span class="error-message" ng-if="vm.addressValidationForm.state.$error.required && vm.addressValidationForm.state.$dirty">
							Please select state
						</span>
						<span class="error-message" ng-if="!vm.addressValidationForm.state.$error.required && vm.addressValidationForm.state.$error.validState && vm.addressValidationForm.state.$dirty">
							Please select a valid state
						</span>
					</div>
				</div>


				<div class="form-group">
					<label for="zipcode" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.zipcode" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" name="zipcode" id="zipcode" class="input-mini" maxlength="5" ng-minlength="5" number-only ng-model="vm.formData.locationDto.zipcode" required ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)" ng-change="vm.getCounty('zipCode')">

						<span class="error-message" ng-if="vm.addressValidationForm.zipcode.$error.required && vm.addressValidationForm.zipcode.$dirty">
							<spring:message code="indportal.communicationPreferences.zipCodeError1" />
						</span>
						<span class="error-message" ng-if="(vm.addressValidationForm.zipcode.$error.minlength || vm.addressValidationForm.zipcode.$error.validZipCode ) && vm.addressValidationForm.zipcode.$dirty && !vm.addressValidationForm.zipcode.$error.required">
							<spring:message code="indportal.communicationPreferences.zipCodeError2" />
						</span>
					</div>
				</div>

				<div class="form-group">
					<label for="county" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.county" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<select class="input-large" name="county" id="county" ng-model="vm.formData.locationDto.countyName" ng-options="key as key for (key, value) in vm.countyList" required ng-disabled="vm.formData.hasActiveEnrollment === true || (vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true)">
							<option value="" ng-disabled="true"><spring:message code="indportal.communicationPreferences.pleaseSelect" /></option>
						</select>
					</div>
				</div>

				<div class="form-group" ng-if="vm.isEditMode" address-validation-directive="vm.formData.locationDto" address-validation-callback="vm.mailingAddressUpdated=true;vm.contactPreferencesUpdated=false;vm.saveData()">
					<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
                        <button class="btn btn-primary btn_save address-validation-click col-xs-12" ng-disabled="vm.addressValidationForm.$invalid || vm.addressValidationForm.$pristine" ng-if="vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication !== true">
                            <spring:message code="indportal.communicationPreferences.updateMailingAddress"/>
                        </button>

                        <button class="btn btn-primary btn_save" ng-click="vm.showErStatusModal()" ng-if="vm.formData.hasActiveEnrollment !== true && vm.formData.activeApplication === true">
                            <spring:message code="indportal.communicationPreferences.updateMailingAddress"/>
                        </button>

                        <button class="btn btn-primary btn_save" ng-click="vm.reportChange()" ng-if="vm.formData.hasActiveEnrollment === true && vm.formData.insideOEEnrollementWindow !==true">
                            <spring:message code="indportal.communicationPreferences.reportChange"/>
                        </button>
                    </div>
					<hr>
				</div>
			</ng-form>



			<ng-form name="vm.communicationPreferencesForm">
				<div class="margin20-t">
				<p class="alert alert-info" ng-if="vm.isEditMode">
					<spring:message code="indportal.communicationPreferences.changeCommunicationMethod" />
				</p>

				<div class="form-group">
					<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label" for="phone1"><spring:message code="label.phone" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
						<a class="ttclass" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='<spring:message code="signup_lbl.mobiletooltip" />'>
                            <span class="aria-hidden">More Information</span><i class="icon-question-sign"></i>
						</a>
					</label>

					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input type="text" id="phone1" name="phone1" class="input-mini" maxlength="3" ng-minlength="3" ng-model="vm.formData.phone1" title="Enter first 3 digits of your phone number" number-only required>
						<input type="text" id="phone2" name="phone2" class="input-mini" maxlength="3" ng-minlength="3" ng-model="vm.formData.phone2" title="Enter second 3 digits of your phone number" number-only required>
						<input type="text" id="phone3" name="phone3" class="input-mini" maxlength="4" ng-minlength="4" ng-model="vm.formData.phone3" title="Enter last 4 digits of your phone number"number-only required>
					</div>
					<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
						<label for="textMe" class="checkbox margin10-t">
							<input type="checkbox" id="textMe" name="textMe" ng-model="vm.formData.textMe" ng-change="vm.showTextModal()"> <spring:message code="indportal.communicationPreferences.textMe" />
						</label>
					</div>
				</div>

				<div class="form-group">
					<label for="emailAddress" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.emailAddress" />
						<a ng-if="vm.isEditMode" class="ttclass" bootstrap-tooltip rel="tooltip" href="javascript:void(0)" data-original-title='<spring:message code="indportal.communicationPreferences.emailChanges" />'>
                            <span class="aria-hidden">More Information</span><i class="icon-question-sign"></i>
						</a>
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<input class="input-large" type="text" name="emailAddress" id="emailAddress" ng-model="vm.formData.emailAddress" ng-disabled="true">
					</div>
				</div>

				<div class="form-group">
					<label for="prefSpokenLang" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.spokenLanguage" />
						<a class="ttclass" rel="tooltip" target="_blank" href="javascript:void(0)" bootstrap-tooltip data-original-title='<spring:message code="indportal.communicationPreferences.communicationMethodTooltip"/>'>
                            <span class="aria-hidden">More Information</span><i class="icon-question-sign"></i>
						</a>
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<select class="input-large" ng-model="vm.formData.prefSpokenLang" id="prefSpokenLang" >
							<option value="SP-ENG"><spring:message code="indportal.communicationPreferences.english" /></option>
							<option value="SP-SPA"><spring:message code="indportal.communicationPreferences.spanish" /></option>
						</select>
					</div>
				</div>

				<div class="form-group">
					<label for="prefWrittenLang" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.writtenLanguage" />
						<a class="ttclass" rel="tooltip" target="_blank" href="javascript:void(0)" bootstrap-tooltip data-original-title='<spring:message code="indportal.communicationPreferences.communicationMethodTooltip"/>'>
                           <span class="aria-hidden">More Information</span><i class="icon-question-sign"></i>
						</a>
					</label>
					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<select class="input-large" ng-model="vm.formData.prefWrittenLang" id="prefWrittenLang">
							<option value="SP-ENG"><spring:message code="indportal.communicationPreferences.english" /></option>
							<option value="SP-SPA"><spring:message code="indportal.communicationPreferences.spanish" /></option>
						</select>
					</div>
				</div>


				<div class="form-group">
					<label class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label">
						<spring:message code="indportal.communicationPreferences.receiveInformationBy" />
						<img src='<c:url value="/resources/images/requiredAsterix.png" />' width="10" height="10" alt="required" />
					</label>

					<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
						<label class="radio" for="recieveTypeEmail">
							<input type="radio" name="recieveTypeEmail" id="recieveTypeEmail" value="Email" ng-model="vm.formData.prefCommunication" required ng-disabled="!vm.formData.emailAddress">
							<spring:message code="indportal.communicationPreferences.email" />
						</label>
						<label class="radio" for="recieveTypePostal">
							<input type="radio" name="recieveTypePostal" id="recieveTypePostal" value="Mail" ng-model="vm.formData.prefCommunication" ng-disabled="vm.formData.locationDto === null || !vm.formData.locationDto.addressLine1">
							<spring:message code="indportal.communicationPreferences.postalMail" />
						</label>
						<%if((!"ID".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) && (!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) ){%>
							<label class="radio" for="recieveTypeEmailAndPostal">
								<input type="radio" name="recieveTypePostal" id="recieveTypeEmailAndPostal" value="EmailAndMail" ng-model="vm.formData.prefCommunication" ng-disabled="vm.formData.locationDto === null || !vm.formData.locationDto.addressLine1">
								<spring:message code="indportal.communicationPreferences.postalEmailAndMail" />
							</label>
						<% }%> 
						<label class="radio" for="recieveTypeNone">
							<input type="radio" name="recieveTypePostal" id="recieveTypeNone" value="None" ng-model="vm.formData.prefCommunication" ng-click="vm.showOptOutNotificationsModal()">
							<spring:message code="indportal.communicationPreferences.None" />
						</label>
					</div>
				</div>

				<p class="alert alert-info" ng-if="${showPaperless1095Option=='ON'}">
					<spring:message code="indportal.communicationPreferences.optInPaperless1095" />
				</p>
				
				<div class="form-group" ng-if="${showPaperless1095Option=='ON'}">
					<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
						<label for="optInPaperless1095" class="checkbox margin10-t">
							<input type="checkbox" id="optInPaperless1095" name="optInPaperless1095" ng-model="vm.formData.optInPaperless1095"> <spring:message code="indportal.communicationPreferences.optInPaperless1095CheckBox" />
						</label>
					</div>
				</div>

				<div ng-if="vm.isEditMode" class="form-group">
                    <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
                        <button class="btn btn-primary btn_save" ng-click="vm.mailingAddressUpdated=false;vm.contactPreferencesUpdated=true;vm.saveData()" ng-disabled="vm.communicationPreferencesForm.$invalid || vm.communicationPreferencesForm.$pristine">
                            <spring:message code="indportal.communicationPreferences.updatePreferences"/>
                        </button>
				    </div>
                </div>
				</div>
			</ng-form>

			<div class="form-group margin20-t" ng-if="!vm.isEditMode" address-validation-directive="vm.formData.locationDto" address-validation-callback="vm.mailingAddressUpdated=true;vm.contactPreferencesUpdated=true;vm.saveData()">
                <div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
                    <button class="btn btn-primary btn_save address-validation-click" id="submitAdditionalInfo" type="button" ng-disabled="vm.communicationPreferencesForm.$invalid || vm.addressValidationForm.$invalid">
                        <spring:message code="indportal.communicationPreferences.continue" />
                    </button>
			    </div>
            </div>
		</form>
	</div>

	<div id="addressValidationModal" class="modal hide fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><spring:message code="indportal.communicationPreferences.checkYourAddress" /></h3>
		</div>
		<div class="modal-body">
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.cancel" /></button>
			<button class="btn btn-primary" data-dismiss="modal" ng-click="vm.updateAddress()"><spring:message code="indportal.communicationPreferences.ok" /></button>
		</div>
	</div>

	<div id="addressNotFoundModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h3><spring:message code="indportal.communicationPreferences.addressNotFound" /></h3>
		</div>
		<div class="modal-body">
		</div>
		<div class="modal-footer">
			<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.close" /></button>
		</div>
	</div>


	<div id="communicationPreferencesUpdateSuccessModal" class="modal hide fade" tabindex="-1" role="dialog"  aria-hidden="true">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
		</div>
		<div class="modal-body">
			<p ng-if="vm.mailingAddressUpdated"><spring:message code="indportal.communicationPreferences.mailingAddressUpdate" /></p>
			<p ng-if="vm.contactPreferencesUpdated"><spring:message code="indportal.communicationPreferences.preferredMethodUpdate" /></p>
		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" ng-click="vm.goToDashboard()"><spring:message code="indportal.communicationPreferences.goDashboard" /></button>
			<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.communicationPreferences.close" /></button>
		</div>
	</div>

	<!-- Modal-->
	<div id="reportAChangeModal" class="modal fade hide" tabindex="-1" role="dialog" aria-labelledby="reportChangeModalHeader" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="reportChangeModalHeader" style="font-size: 18px;border: none;"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
				</div>
				<div class="modal-body">
					<p class="margin10-l"><spring:message code="indportal.portalhome.ifyouwouldliketomakechanges" javaScriptEscape="true"/></p>
					<div class="alert alert-info">
						<p><spring:message code="indportal.portalhome.moreInfoLifeEventsFin" htmlEscape="false"/></p>
					</div>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
				</div>
			</div><!-- /.modal-content-->
		</div> <!-- /.modal-dialog-->
	</div>
	<!-- Modal-->

	<!-- Modal-->
	<div id="erStatusModal" class="modal fade hide" tabindex="-1" role="dialog" aria-labelledby="erStatusModalHeader" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="erStatusModalHeader" style="font-size: 18px;border: none;"><spring:message code="indportal.communicationPreferences.updateMailingAddress"/></h3>
				</div>
				<div class="modal-body">
					<p class="margin10-l"><spring:message code="indportal.communicationPreferences.erStatusModal" javaScriptEscape="true"/></p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
				</div>
			</div><!-- /.modal-content-->
		</div> <!-- /.modal-dialog-->
	</div>
	<!-- Modal-->


	<!-- Modal-->
	<div id="optOutNotificationsModal" class="modal fade hide" tabindex="-1" role="dialog" aria-labelledby="optOutNotificationsModalHeader" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div id="optOutNotificationsModalHeader" class="modal-header modal-header-alert-danger">
					Important
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<p class="margin20-l"><spring:message code="indportal.communicationPreferences.optOutNotifications" javaScriptEscape="true"/></p>
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
				</div>
			</div><!-- /.modal-content-->
		</div> <!-- /.modal-dialog-->
	</div>
	<!-- Modal-->

	<!-- Modal-->
	<div id="textMeModal" class="modal fade hide" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div class="modal-body">
					<spring:message code="indportal.communicationPreferences.textMeModalContent" />
				</div>
				<div class="modal-footer">
					<button class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
				</div>
			</div><!-- /.modal-content-->
		</div> <!-- /.modal-dialog-->
	</div>
	<!-- Modal-->
</div>
</script>
