<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<link href="<c:url value="/resources/css/indportal_qep.css" />" rel="stylesheet"  type="text/css" />
<script>
// data retriving from spring source has to be in jsp files
var qepStorage = {
	 marriage:{title: "<spring:message code='label.lce.qep.marriage'/>", 
		content: "<spring:message code='label.lce.qep.marriageContent'/>"},
	 divorce:{title: "<spring:message code='label.lce.qep.divorce'/>", 
		content: "<spring:message code='label.lce.qep.divorceContent'/>"},
	 birth:{title: "<spring:message code='label.lce.qep.birth'/>", 
	 	content: "<spring:message code='label.lce.qep.birthContent'/>"},	
	 death:{title: "<spring:message code='label.lce.qep.death'/>", 
	 	content: "<spring:message code='label.lce.qep.deathContent'/>"},	
	 move:{title: "<spring:message code='label.lce.qep.move'/>", 
		 content: "<spring:message code='label.lce.qep.moveContent'/>"},
	 income:{title: "<spring:message code='label.lce.qep.income'/>", 
		 content: "<spring:message code='label.lce.qep.incomeContent'/>"},
	 immigration:{title: "<spring:message code='label.lce.qep.immigration'/>", 
		 content: "<spring:message code='label.lce.qep.immigrationContent'/>"},	
	 incarceration:{title: "<spring:message code='label.lce.qep.incarceration'/>", 
		 content: "<spring:message code='label.lce.qep.incarcerationContent'/>"},
	 tribe:{title: "<spring:message code='label.lce.qep.tribe'/>", 
		content: "<spring:message code='label.lce.qep.tribeContent'/>"}
};
</script>

<script type="text/ng-template" id="qephome"> 
  <div class="row-fluid">
    <div>
      <div class="header">
        <h4><spring:message code="label.lce.qualifyingEvents"/></h4>
      </div><!-- header -->

      <div class="margin20 gutter10 alert alert-info">
		<p><spring:message code="label.lce.home.explanation1"/></p>
		<p><spring:message code="label.lce.home.explanation2"/></p>
      </div>
       <div class="gutter20">

        
          <!--<div class="span2">
            <section id="set-1" class="margin20-t">
              <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                <label class="checkbox inline">
                  <input type="checkbox" ng-model="losecoverage">
                  <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                    <img ng-src="resources/img/lce/other.png" alt="Lost coverage">
                    <div class="emptygreendiv"></div>
                    <div></div>
                  </span>
                </label>
              </div>
            </section>
          </div>


          <div class="span6">
            <div class="eachsection">
              <div class="header">
                <h4><spring:message code="label.lce.home.lossCoverage"/></h4>
              </div>
              <div id="container" class="row-fluid">
              	<div class="span6">
          		
            	  <spring:message code="label.lce.home.lossCoverageContent"/>
     
        	  </div>

			</div>
		  </div>
		</div>  -->


          <div class="row-fluid">
<div class="eachsection">
            <div class="header">
              <h4><spring:message code="label.lce.home.lossCoverage"/></h4>
            </div>
<div class="row-fluid">
            <div class="span2">
              <section id="set-1" class="margin20-t">
                <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                  <label class="checkbox inline">
                    <input type="checkbox" ng-model="losecoverage">
                    <span href="#set-1" class="hi-icon hi-icon-mobile" id="lossCoverage">
                      <img ng-src="resources/img/lce/other.png"  alt="Lost coverage">
                      <div></div>
                    </span>
                  </label>
                </div>
              </section>
            </div>

            <div class="span9">
              <div class="contents">
                <spring:message code="label.lce.home.lossCoverageContent"/>
              </div>
            </div>
          </div>
</div>
</div>

          
        <div class="row-fluid" >
          <div class="span6">
          
			<%--<div class="eachsection">
              <div class="header">
                <h4><spring:message code="label.lce.home.lossCoverage"/></h4>
              </div>

                <div id="container" class="row-fluid">
                  <div class="span3">
                    <section id="set-1" class="margin20-t">
                      <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                        <label class="checkbox inline">
                          <input type="checkbox" ng-model="losecoverage">
                          <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                            <img ng-src="resources/img/lce/other.png" alt="Lost coverage">
                            <%--<div class="emptygreendiv"></div>
                            <div></div>
                          </span>
                        </label>
                      </div>
                    </section>
                  </div>

                  <div class="span9">
                    <div class="contents">
                      <h5> <spring:message code="label.lce.home.lossCoverageContent"/></h5>
                      <!-- <small>{{item.content}}</small> -->
                    </div>
                  </div>
                </div>
 			</div>--%>


            <div class="eachsection">
              <div class="header">
                <h4><spring:message code="label.lce.home.changesHouseholdSize"/></h4>
              </div>
              <div ng-repeat="item in changeinhouseholdsize" class="repeateditems">
                <div id="container" class="row-fluid">
                  <div class="span3">
                    <section id="set-1" class="margin20-t">
                      <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                        <label class="checkbox inline">
                          <input type="checkbox" ng-model="item.eventName$index">
                          <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                            <img ng-src="{{item.image}}" alt="Lost coverage">
                            <%--<div class="emptygreendiv"></div>--%>
                            <div></div>
                          </span>
                        </label>
                      </div>
                    </section>
                  </div>

                  <div class="span9">
                    <div class="contents">
                      <h5 class="title"> {{item.eventName}}</h5>
                      <small>{{item.content}}</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="span6">
            <div class="eachsection">
              <div class="header">
                <h4>Changes in circumstances</h4>
              </div>
              <div ng-repeat="item in changeincircumstances" class="repeateditems">
                <div id="container" class="row-fluid">
                  <div class="span3">
                    <section id="set-1" class="margin20-t">
                      <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                        <label class="checkbox inline">
                          <input type="checkbox" ng-model="item.eventName$index">
                          <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                            <img ng-src="{{item.image}}" alt="Lost coverage">
                            <%--<div class="emptygreendiv"></div>--%>
                            <div></div>
                          </span>
                        </label>
                      </div>
                    </section>
                  </div>

                  <div class="span9">
                    <div class="contents">
                      <h5 class="title">{{item.eventName}}</h5>
                      <small>{{item.content}}</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="eachsection">
              <div class="header">
                <h4>Changes in status</h4>
              </div>
              <div ng-repeat="item in changeinstatus" class="repeateditems">
                <div id="container" class="row-fluid">
                  <div class="span3">
                    <section id="set-1" class="margin20-t">
                      <div class="hi-icon-wrap hi-icon-effect-1 hi-icon-effect-1a">
                        <label class="checkbox inline">
                          <input type="checkbox" ng-model="item.eventName$index">
                          <span href="#set-1" class="hi-icon hi-icon-mobile" id="individualItem{{$index}}">
                            <img ng-src="{{item.image}}" alt="Lost coverage">
                            <%--<div class="emptygreendiv"></div>--%>
                            <div></div>
                          </span>
                        </label>
                      </div>
                    </section>
                  </div>

                  <div class="span9">
                    <div class="contents">
                      <h5 class="title">{{item.eventName}}</h5>
                      <small>{{item.content}}</small>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

      <!-- NEXT BUTTON -->
      <dl class="dl-horizontal pull-right">
        <a class="btn btn-primary" ng-click="losecoverage || ((changeinhouseholdsize | filter:'true').length > 0 || (changeincircumstances | filter:'true').length > 0 || (changeinstatus | filter:'true').length > 0 || (inatribe | filter:'true').length > 0) || goTo('qepnoteligible/')" ng-disabled="losecoverage || ((changeinhouseholdsize | filter:'true').length > 0 || (changeincircumstances | filter:'true').length > 0 || (changeinstatus | filter:'true').length > 0 || (inatribe | filter:'true').length > 0)">
          <spring:message code="indportal.portalhome.noneApply"/>
        </a>
        <a class="btn btn-primary" ng-click="!(losecoverage || (changeinhouseholdsize | filter:'true').length > 0 || (changeincircumstances | filter:'true').length > 0 || (changeinstatus | filter:'true').length > 0) || goTo('qepeligible/')" ng-disabled="!(losecoverage || (changeinhouseholdsize | filter:'true').length > 0 || (changeincircumstances | filter:'true').length > 0 || (changeinstatus | filter:'true').length > 0)">
          <spring:message code="indportal.portalhome.continue"/> 
        </a>
        <!--<a href="#qepnoteligible/" class="btn btn-primary" ng-if="losecoverage && !((changeinhouseholdsize | filter:'true').length > 0 || (changeincircumstances | filter:'true').length > 0 || (changeinstatus | filter:'true').length > 0 || (inatribe | filter:'true').length > 0)">
          <spring:message code="indportal.portalhome.continue"/> 
        </a>-->

      </dl><!-- NEXT BUTTON -->

    </div><!-- rightside -->
  </div> <!-- row-fluid -->
</script>
