<script type="text/ng-template" id="income.jsp">
  <div class="gutter10" ng-controller="incomeChangeController">
    <h3><spring:message code="label.lce.entereventdetails"/></h3>
    <div class="gutter10">

  		<table id="" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
  		  <tbody>
  		    <tr>
  		      <td><spring:message code="label.lce.firstname"/></td>
  		      <td><spring:message code="label.lce.lastname"/></td>
  		      <td><spring:message code="label.lce.relationship"/></td>
  		      <td><spring:message code="label.lce.seekingcoverage"/>?</td>
  		      <td><spring:message code="label.lce.selecttoeditincome"/></td>
  		      <td><spring:message code="label.lce.date"/></td>
  		    </tr>
  		    <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
  		      <td>{{householdMember.name.firstName}}</td>
  		      <td>{{householdMember.name.lastName}}</td>
  		      <td>
  		        <div ng-repeat="relationship in householdMember.bloodRelationship">
  		          {{getRelation(relationship.relationshipCode)}}
  		        </div>
  		      </td>
  		      <td>
  		        <label class="radio inline" for="applyingforhealthcare">
  		          <input id="yes{{fIndex}}" name="person{{fIndex}}" type="radio" checked="" value='true' ng-value="true" ng-model="householdMember.applyingForCoverageIndicator" ng-change="addChangedApplicants(householdMember.personId)">
  		          <label for="yes{{fIndex}}" onclick=""><spring:message code="label.lce.yes"/></label>
  		        </label>
  		        <label class="radio inline" for="applyingforhealthcare">
  		          <input id="no{{fIndex}}" name="person{{fIndex}}" type="radio" value='false' ng-value="false" ng-model="householdMember.applyingForCoverageIndicator" ng-change="addChangedApplicants(householdMember.personId)">
  		          <label for="no{{fIndex}}" onclick=""><spring:message code="label.lce.no"/></label>
  		        </label>
  		      </td>
  		      <td>
  		        <label class="checkbox inline">
                <input type="checkbox" ng-model="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].incomeChangeIndicator" ng-init="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].index = $index" value="false" >
  		          <label for=""><spring:message code="label.lce.editincome"/></label>
  		        </label>
  		      </td>

  		       <td>
            		<div id="date" class="input-append" datetimez ng-model="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].incomeChangeDate">
  			            <input data-format="MM/dd/yyyy" type="text" ng-model="eventInfoArrayTemp[getOriginalIndex(householdMember,taxHousehold.householdMember)].incomeChangeDate" name="input1" class="span5"></input>
  			            <span class="add-on">
  			              <i class="icon-calendar"></i>
  			            </span>
            		</div>
            </td>


  		    </tr>
  		  </tbody>

  		  <!-- <pre>{{(jsonObject.singleStreamlinedApplication.taxHousehold[0].householdMember | filter: {'multIncomeChange':'true'}:'true').length}}</pre> -->
  		</table>

      <div ng-hide="otherincome"></div>
      <div class="gutter10"  ng-show="otherincome">
       <form class="form-horizontal" id="maritalStatusIncome" name="maritalStatusIncome" novalidate>
         <!-- If user has other income this displays -->
         <div class="control-group">
           <label class="control-label" for=""><spring:message code="label.lce.incomesourcefor"/> {{householdMember.name.firstName}}: </label>
           <div class="controls" ng-repeat="income in incomeTypes">
             <label class="checkbox inline">
               <input type="checkbox" ng-model="income.value" name="incomeTypes" ng-required="otherincome=='Yes' && selectedIncomeCount()==0"> {{income.type}}
             </label>
           </div>
           <div class="controls margin5-t">
             <span class="validation-error-text-container" ng-show="submitted && maritalStatusIncome.incomeTypes.$error.required"><spring:message code="label.lce.pleaseselectincometypes"/></span>
           </div>
         </div><!-- If user has other income this displays -->
       </form>
      </div>

    </div>

    <!-- BACK & NEXT BUTTON -->
    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()" ng-hide="lifeEventCounter >= 0 || counter >= 1"><spring:message code="label.lce.back"/></a>
      <a ng-show="maritalStatusIncome.$valid" class="btn btn-primary" ng-click="updateEventInfo();addDep(numDependents);next();"><spring:message code="label.lce.next"/></a>
      <a ng-show="!maritalStatusIncome.$valid" class="btn btn-primary" ng-click="submitted=true"><spring:message code="label.lce.next"/></a>
    </dl>
  </div>
</script>
