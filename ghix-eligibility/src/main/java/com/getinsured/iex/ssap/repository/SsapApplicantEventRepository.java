package com.getinsured.iex.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.ApplicantEventValidationStatus;


@RestResource(path="SsapApplicantEvent")
public interface SsapApplicantEventRepository  extends JpaRepository<SsapApplicantEvent, Long> {

	public List<SsapApplicantEvent> findBySsapAplicationEvent(SsapApplicationEvent ssapApplicationEvent);

	@RestResource(path="findBySsapApplicationEventId")
	@Query("select a from SsapApplicantEvent sae, SsapApplicant a where a.id = sae.ssapApplicant.id and sae.ssapAplicationEvent.id = :applicationEventId)")
	public List<SsapApplicant> findBySssapApplicationEventId(@Param("applicationEventId") long applicationEventId);

	List<SsapApplicantEvent> findBySsapApplicant(SsapApplicant ssapApplicant);
	
	@RestResource(path="findBySsapApplicantId")
	@Query("select sae from SsapApplicantEvent sae, SsapApplicant a where a.id = sae.ssapApplicant.id and sae.ssapApplicant.id = :applicantId)")
	public List<SsapApplicantEvent> findBySsapApplicantId(@Param("applicantId") long applicantId);
	
	@RestResource(path="findByApplicationEventId")
	@Query("select sae from SsapApplicantEvent sae, SsapApplicationEvent a where a.id = sae.ssapAplicationEvent.id and sae.ssapAplicationEvent.id = :applicationEventId)")
	public List<SsapApplicantEvent> findByApplicationEventId(@Param("applicationEventId") long applicationEventId);
	
	@RestResource(path="findEventsByApplicationId")
	@Query("select sep from SepEvents sep where sep.id in (select sae.sepEvents.id from SsapApplicantEvent sae, SsapApplicant a, SsapApplication sa " +
			"where a.id = sae.ssapApplicant.id and a.ssapApplication.id = sa.id and sa.id = :applicationId)")
	public List<SepEvents> findEventsByApplicationId(@Param("applicationId") long applicationId);
	
	@RestResource(path="findByApplicationEventIdAndPersonIdAndSepEventId")
	@Query("select sae from SsapApplicantEvent sae, SsapApplicant a where a.id = sae.ssapApplicant.id and a.personId = :personId and " +
			" sae.ssapAplicationEvent.id = :applicationEventId and sae.sepEvents.id = :sepEventId")
	public List<SsapApplicantEvent> findByApplicationEventIdAndPersonIdAndSepEventId(@Param("applicationEventId") long applicationEventId, 
			@Param("personId") long personId, @Param("sepEventId") int sepEventId);
	
	@Query("select sae from SsapApplicantEvent sae where sae.ssapAplicationEvent.ssapApplication.id = :applicationEventId  and " +
			"sae.sepEvents.name = :sepEventName")
	public List<SsapApplicantEvent> findByApplicationIdAndSepEvent(@Param("applicationEventId") long applicationEventId, @Param("sepEventName") String sepEventName);
	
	@RestResource(path="findByApplicationEventIdAndSepEventId")
	@Query("select a from SsapApplicantEvent sae, SsapApplicant a where a.id = sae.ssapApplicant.id and " +
			" sae.ssapAplicationEvent.id = :applicationEventId and sae.sepEvents.id = :sepEventId")
	public List<SsapApplicant> findByApplicationEventIdAndSepEventId(@Param("applicationEventId") long applicationEventId, @Param("sepEventId") int sepEventId);
	
	@RestResource(path="findMinEnrollmentStartDate")
	@Query("select a from SsapApplicantEvent a where a.ssapAplicationEvent.id =:applicationEventId and a.enrollmentStartDate=(select min(appEvent.enrollmentStartDate) from SsapApplicantEvent appEvent where appEvent.ssapAplicationEvent.id =:applicationEventId)")
	public List<SsapApplicantEvent> findMinEnrollmentStartDate(@Param("applicationEventId") long applicationEventId);
	
	@RestResource(path="findMaxEnrollmentEndDate")
	@Query("select a from SsapApplicantEvent a where a.ssapAplicationEvent.id =:applicationEventId and a.enrollmentEndDate=(select max(appEvent.enrollmentEndDate) from SsapApplicantEvent appEvent where appEvent.ssapAplicationEvent.id =:applicationEventId)")
	public List<SsapApplicantEvent> findMaxEnrollmentEndDate(@Param("applicationEventId") long applicationEventId);
	
	@Query("SELECT ssapApplicantEvent " +
			" FROM SsapApplicantEvent as ssapApplicantEvent "+
			" WHERE  ssapApplicantEvent.ssapAplicationEvent.ssapApplication.caseNumber =  :caseNumber AND "+
			"        ssapApplicantEvent.sepEvents.name <> 'OTHER' ")
	List<SsapApplicantEvent> getApplicantEventByApplicationCaseNumber(@Param("caseNumber") String caseNumber);

	@Query("select sae from SsapApplicantEvent sae where sae.id in (:applicantEventIds)")
	public List<SsapApplicantEvent> findBySsapApplicantEventId(@Param("applicantEventIds") List<Long> applicantEventIds);
	
	@Query("select sae from SsapApplicantEvent sae where sae.id = :applicantEventId and sae.validationStatus = :validationStatus")
	public SsapApplicantEvent findBySsapApplicantIdAndValidationStatus(@Param("applicantEventId") long applicantEventId, @Param("validationStatus") ApplicantEventValidationStatus validationStatus);
	
	@Query("Select ssapApplicantEvent " +
			" FROM SsapApplicantEvent as ssapApplicantEvent "+
			" inner join fetch ssapApplicantEvent.sepEvents as sepEvents"+
			" inner join fetch ssapApplicantEvent.ssapApplicant as ssapApplicant"+
			" where ssapApplicant.id = :ssapApplicantId ")
	List<SsapApplicantEvent> getApplicantEventByApplicantId(@Param("ssapApplicantId") long ssapApplicantId);
	
}
