import { FetchService } from './../../services/fetch.service';
import { DataKeepService } from './../../services/data-keep.service';
import { DrugInfo } from './../../models/data.model';
import 'rxjs/Rx';
import { Component, EventEmitter, Output, ViewChild, ElementRef, Input } from '@angular/core';
import * as querystring from 'querystring';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs/Observer';
import { Subject } from 'rxjs/Subject';

import { NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'drug-search-typeahead',
    templateUrl: './drug-search-typeahead.component.html',
    styles: [`
        .alert {
            margin-top:15px;
            float:left;}
        .drug-template{
            padding:7px 0 3px 0;
        }
        .drugName{
            font-size:14px;
        }
    `
    ]
})
export class DrugSearchTypeaheadComponent {
	@ViewChild('ngbTypeahead') ngbTypeahead: NgbTypeahead;
    @ViewChild("input") private input: ElementRef;
    @ViewChild("drugInput") private drugInput: ElementRef;
    @Output() public selectedDrug = new EventEmitter<DrugInfo>();
    @Output() private focus = new EventEmitter();
    @Output() private getFocus = new EventEmitter<boolean>();
    @Output() private getDrug = new EventEmitter<boolean>();
    @Output() private getChange = new EventEmitter<string>();
    public drug: string;
    public typeaheadLoading: boolean;
    //public typeaheadNoResults: boolean;
    public statesComplex: DrugInfo[] = [];
    public drugSearchAlert = false;
    public drugList: string[];


    searching: boolean = false;
    hideSearchingWhenUnsubscribed = new Observable(() => () => {this.searching = false; });

    constructor(
        public datakeep: DataKeepService,
        private fetch: FetchService
    ) { }s


    drugSearch = (text$: Observable<string>) => {
    	return text$.debounceTime(300)
        	.filter((term: string) => {
        		return  term.length > 2 && this.datakeep.personalization.drug.drugSearch.length < 5;
        	})
        	.distinctUntilChanged()
        	.do(() => this.searching = true)
        	.switchMap((term: string) => {
        		return this.getDrugsAsObservable(term)
	            	.do(() => this.searching = false)
		            .catch(() => {
		            	this.searching = false;
		                this.getDrug.emit(false);
		                return Observable.of([]);
		            })
        	})


	        .merge(this.hideSearchingWhenUnsubscribed);
    }

	formatter = (result: string) => {
		return result.toLowerCase()
			.split(' ')
			.map((s) => s.charAt(0).toUpperCase() + s.substring(1)).join(' ');
	}
	
    public getDrugsAsObservable(queryString: string): Observable<any> {
        let query = new RegExp(queryString, 'ig');
        let isQueried$ = new Subject<boolean>();
        return Observable.of(queryString)
            .delayWhen((queryString: string) => {
                this.fetch.searchDrug(encodeURIComponent(queryString)).subscribe(
                    (val) => {
                        if (val.drugs) {
                            this.statesComplex = val.drugs;
                            this.drugList = val.drugs.map(drug => drug.drugID);
                        } else {
                            this.statesComplex = [];
                            this.drugList = [];

                           	this.getDrug.emit(false);
                        }
                    },
                    err => {},
                    () => {
                        isQueried$.next(true);
                    }
                );
                return isQueried$;
            }).switchMap((queryString: string) => {
                return Observable.of(this.drugList);
            })
    }

    public onSelect(e: any): void {
       this.drug = "";
       this.drugInput.nativeElement.value = "";
       this.drugInput.nativeElement.blur();
       let selDrug =  this.statesComplex.filter(x => x.drugID === e.item);
       if(selDrug && selDrug.length > 0){
           this.selectedDrug.emit(selDrug[0]);
       } else {
           this.selectedDrug.emit(e.item);
       }
   }

    public onFocus() {
        this.getFocus.emit(true);
    }

    public onBlur() {
    	this.getFocus.emit(false);
    }

    public onChange(drug) {
    	this.getChange.emit(drug);
        if(this.drug.length < 3){
            this.ngbTypeahead.dismissPopup();
        }
    }


}
