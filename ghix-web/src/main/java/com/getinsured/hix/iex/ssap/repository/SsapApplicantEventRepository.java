package com.getinsured.hix.iex.ssap.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.iex.ssap.model.SsapApplicantEvent;

/**
 * 
 * @author Abhijeet Duraphe
 * 
 */
public interface SsapApplicantEventRepository extends JpaRepository<SsapApplicantEvent, Long> {

}
