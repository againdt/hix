package com.getinsured.eligibility.at.resp.si.handler.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;

public final class EligibilityStatusHelper {

	private static final String QHP = "QHP";

	private static final String CSR = "CSR";

	private static final String APTC = "APTC";

	public static final String STATESUBSIDY = "STATESUBSIDY";

	private static final String MEDICAID = "MEDICAID";

	private static final Logger LOGGER = Logger.getLogger(EligibilityStatusHelper.class);

	private EligibilityStatusHelper(){}

	public static String readEligibilityType(String type){

		String eligType = "";
		switch (type) {
		case EligibilityConstants.MEDICAID_ELIGIBILITY_TYPE: case EligibilityConstants.EMERGENCY_MEDICAID_ELIGIBILITY_TYPE:
		case EligibilityConstants.REFUGEE_MEDICAL_ASSISTANCE_ELIGIBILITY_TYPE: case EligibilityConstants.MEDICAID_MAGI_ELIGIBILITY_TYPE:
		case EligibilityConstants.MEDICAID_NON_MAGI_ELIGIBILITY_TYPE: case EligibilityConstants.CHIP_ELIGIBILITY_TYPE:
			eligType = MEDICAID;
			break;
		case EligibilityConstants.APTC_ELIGIBILITY_TYPE:
			eligType = APTC;
			break;
		case EligibilityConstants.STATE_SUBSIDY_ELIGIBILITY_TYPE:
			eligType = STATESUBSIDY;
			break;
		case EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE:
			eligType = QHP;
			break;
		case EligibilityConstants.CSR_ELIGIBILITY_TYPE:
			eligType = CSR;
			break;
		default:
			LOGGER.warn(EligibilityConstants.INVALID_ELIGIBILITY_TYPE_FOUND + type);
			break;

		}

		return eligType;
	}

	@Deprecated
	private static ExchangeEligibilityStatus determineExchangeEligibilityStatusEnum(Set<String> applicationEligibility) {
		if (applicationEligibility.contains(CSR)){
			// APTC_CSR
			return ExchangeEligibilityStatus.APTC_CSR;
		} else if (applicationEligibility.contains(APTC)){
			// APTC
			return ExchangeEligibilityStatus.APTC;
		} else {
			// QHP
			return ExchangeEligibilityStatus.QHP;
		}

	}


	public static Set<String> prepareApplicationEligibilitySet(Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap) {

		Set<String> applicationEligibility = new HashSet<>();
		String exchangeEligibilityEnabled = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ELIGIBILITY_ENABLE);
		for (Long ssapApplicantId : eligibilityProgramMap.keySet()) {
			List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityList = eligibilityProgramMap.get(ssapApplicantId);
			boolean isQHP = false;
			
			for (com.getinsured.eligibility.model.EligibilityProgram ep : eligibilityList) {
				if (StringUtils.equalsIgnoreCase(ep.getEligibilityIndicator(), EligibilityConstants.TRUE)){
					String eligibilityType = readEligibilityType(ep.getEligibilityType());
					if (StringUtils.equalsIgnoreCase(eligibilityType, QHP)){
						isQHP = true;
					}
					applicationEligibility.add(ssapApplicantId+"-"+eligibilityType);
				}
			}
			if(!isQHP) {
				applicationEligibility.remove(ssapApplicantId+"-"+APTC);
				applicationEligibility.remove(ssapApplicantId+"-"+CSR);
				}
			}
		
		Set<String> newApplicationElig = new HashSet<String>();
		for (String s : applicationEligibility) {
			String[] parts = s.split("-");
			if(parts.length>0) {
				newApplicationElig.add(parts[1]);
		}
		}

		return newApplicationElig;
	}

	public static Set<String> prepareApplicantEligibilitySet(List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityList) {

		Set<String> applicationEligibility = new HashSet<>();

		for (com.getinsured.eligibility.model.EligibilityProgram ep : eligibilityList) {
			if (StringUtils.equalsIgnoreCase(ep.getEligibilityIndicator(), EligibilityConstants.TRUE)){
				applicationEligibility.add(readEligibilityType(ep.getEligibilityType()));
			}
		}

		return applicationEligibility;
	}

	public static void assessmentStatus(Set<String> applicationEligibility, SsapApplication ssapApplication){
		if (applicationEligibility.isEmpty()) {
			// Denied DE..
			ssapApplication.setEligibilityStatus(EligibilityStatus.DE);
		} else if (applicationEligibility.size() == 1 && applicationEligibility.contains(MEDICAID)) {
			setStatusEnum(ssapApplication, EligibilityStatus.CAM);
		} else if (applicationEligibility.size() >= 1) {
			assessmentExchangeStatus(applicationEligibility, ssapApplication);
		}
	}

	private static void assessmentExchangeStatus(Set<String> applicationEligibility, SsapApplication ssapApplication) {
		if (applicationEligibility.contains(MEDICAID)){
			if (applicationEligibility.contains(APTC) || applicationEligibility.contains(CSR) || applicationEligibility.contains(QHP)){
				// Mixed AX
				//ExchangeEligibilityStatus exchangeEligibilityStatus = determineExchangeEligibilityStatusEnum(applicationEligibility);
				setStatusEnum(ssapApplication, EligibilityStatus.CAX);
				//ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
			}
		} else if (applicationEligibility.contains(APTC) || applicationEligibility.contains(CSR) || applicationEligibility.contains(QHP)){
			//ExchangeEligibilityStatus exchangeEligibilityStatus = determineExchangeEligibilityStatusEnum(applicationEligibility);
			setStatusEnum(ssapApplication, EligibilityStatus.CAE);
			//ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
		}
	}

	public static void fullDeterminationStatus(Set<String> applicationEligibility, SsapApplication ssapApplication){
		if (applicationEligibility.isEmpty()) {
			// Denied DE..
			ssapApplication.setEligibilityStatus(EligibilityStatus.DE);
		} else if (applicationEligibility.size() == 1 && applicationEligibility.contains(MEDICAID)) {
			setStatusEnum(ssapApplication, EligibilityStatus.AM);
		} else if (applicationEligibility.size() >= 1) {
			fullDeterminationExchangeStatus(applicationEligibility, ssapApplication);
		}
	}

	private static void fullDeterminationExchangeStatus(Set<String> applicationEligibility, SsapApplication ssapApplication) {
		if (applicationEligibility.contains(MEDICAID)){
			if (applicationEligibility.contains(APTC) || applicationEligibility.contains(CSR) || applicationEligibility.contains(QHP)){
				// Mixed AX
				//ExchangeEligibilityStatus exchangeEligibilityStatus = determineExchangeEligibilityStatusEnum(applicationEligibility);
				setStatusEnum(ssapApplication, EligibilityStatus.AX);
				//ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
			}
		} else if (applicationEligibility.contains(APTC) || applicationEligibility.contains(CSR) || applicationEligibility.contains(QHP)){
			//ExchangeEligibilityStatus exchangeEligibilityStatus = determineExchangeEligibilityStatusEnum(applicationEligibility);
			setStatusEnum(ssapApplication, EligibilityStatus.AE);
			//ssapApplication.setExchangeEligibilityStatus(exchangeEligibilityStatus);
		}
	}

	private static EligibilityStatus setStatusEnum(SsapApplication ssapApplication, EligibilityStatus eligibilityStatus) {

		ssapApplication.setEligibilityStatus(eligibilityStatus);
		return eligibilityStatus;
	}



}
