package com.getinsured.eligibility.at.resp.si.handler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.util.ExceptionUtil;
import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantDetails;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicantEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicationEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.MatchHandler;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.QHPApplicationEligibilityHelper;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.util.ReferralUtil;

/**
 * EligibilityHandler to compare stored ssapApplicants with the received state's response.
 *
 * @author Ekram
 *
 */
@Component
public class EligibilityHandler {

	private static final Logger LOGGER = Logger.getLogger(EligibilityHandler.class);

	@Autowired private ApplicantEligibilityHelper applicantEligibilityHelper;
	@Autowired private ApplicationEligibilityHelper applicationEligibilityHelper;
	@Autowired private QHPApplicationEligibilityHelper qHPApplicationEligibilityHelper;
	@Autowired private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;
	@Autowired private GIWSPayloadRepository giwsPayloadRepository;
	@Autowired private SsapApplicationService ssapApplicationService;
	@Autowired
	private ExceptionUtil exceptionUtil;

	private String STATE_CODE;

	private List<String> csPriorityList = new ArrayList<>(Arrays.asList("CS1", "CS4", "CS5", "CS6", "CS3", "CS2"));
	public static final String STATE_SUBSIDY_PROGRAM = "StateSubsidyProgram";
	public static final String STATE_SUBSIDY_ELIG = "StateSubsidyElig";

	@PostConstruct
	public void createStateContext() {
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public String processERP(Message<ERPResponse> message){

		ERPResponse erpResponse = (ERPResponse) message.getHeaders().get(EligibilityConstants.ERP_RESPONSE);

		Map<String, Object> resultMap = new HashMap<>();
		resultMap.put(EligibilityConstants.SSAP_APPLICATION_ID, erpResponse.getApplicationID());

		try {
			boolean flag = process(erpResponse);

			if (flag){
				resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.AUTO);
			} else {
				resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.MANUAL);
			}

		} catch (Exception e) {

			StringBuilder errorReason = new StringBuilder().append(EligibilityConstants.ERROR_PROCESSING_ERP_REQUEST_FOR_SSAP_APPLICATION_ID).
					append(erpResponse.getApplicationID()).append(EligibilityConstants.REASON).append(e.getMessage());

			LOGGER.error(errorReason.toString());
			resultMap.put(EligibilityConstants.ELIGIBILTY_MATCH_RESULT, EligibilityConstants.ERROR);
			resultMap.put(EligibilityConstants.ERROR_REASON, errorReason.toString());
			exceptionUtil.persistGiMonitorId(erpResponse.getSsapApplicationPrimaryKey(), e);
		} finally {
			resultMap.put(EligibilityConstants.ERP_RESPONSE, erpResponse);
		}

		return EligibilityUtils.marshal(resultMap);
	}

	private boolean process(ERPResponse erpResponse){
		//1. Fetch all incoming applicants from AT Response
		List<TaxHouseholdMember> taxHHList =  erpResponse.getTaxHouseholdMemberList();

		//2. Fetch all existing applicants from SSAP_APPLICANT table
		List<TaxHouseholdMember> ssapHHList =  erpResponse.getSsapApplicantList();

		//3. Fetch corresponding giWsPayload object from db for ssapApplicationId..
		Integer giWsPayloadId = erpResponse.getGiWsPayloadId();

		List<Map<String,Boolean>> nonQHPList = new ArrayList<Map<String,Boolean>>();
		if(erpResponse.getApplicationExtension() != null){
			List<ApplicantDetails> applicanExternsion = erpResponse.getApplicationExtension().getExtendedApplicant();
			if (applicanExternsion != null && !applicanExternsion.isEmpty()) {
				for (ApplicantDetails applicantDetails : applicanExternsion) {
					Map<String, Boolean> nonQHPMap = new HashMap<String, Boolean>();
					boolean isNonQHP = applicantDetails.getApplicantEventsNonQHP() != null && !applicantDetails.getApplicantEventsNonQHP().isEmpty()
									? applicantDetails.getApplicantEventsNonQHP().size() > 0
									: false;
					String id = applicantDetails.getIdentificationId() != null ? applicantDetails.getIdentificationId()
							: null;
					if (id != null) {
						nonQHPMap.put(id, isNonQHP);
						nonQHPList.add(nonQHPMap);
					}
				}
			}
		}
		
		//4. Compare every AT Response applicant with DB..
		Map<Long, List<com.getinsured.eligibility.model.EligibilityProgram>> eligibilityProgramMap = new HashMap<>();
		Map<String, Object> hhLevelEliDetails = new HashMap<>();
		com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram hhAPTCElig = null;
		Set<String> hhCSLevel = new HashSet<String>();
		hhLevelEliDetails.put("HHCSLevel",hhCSLevel);
		hhLevelEliDetails.put("nonQHPListMap",nonQHPList);
		
		for (TaxHouseholdMember ssapApplicant : ssapHHList) {
			
			TaxHouseholdMember atTaxPerson = MatchHandler.findMatchingMember(ssapApplicant, taxHHList);

			if (ssapApplicant != null){
				// Persist latest eligibility data..
				List<com.getinsured.eligibility.model.EligibilityProgram> eligibilityPrograms = applicantEligibilityHelper.processEligibility(atTaxPerson, ssapApplicant, giWsPayloadId, hhLevelEliDetails);
				eligibilityProgramMap.put(ssapApplicant.getPersonInfo().getPrimaryKey(), eligibilityPrograms);
			} else {
				LOGGER.error(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED);
				throw new GIRuntimeException(EligibilityConstants.MEMBER_MATCHING_LOGIC_FAILED +
						"SSAP Person Detail - " + ssapApplicant +
						"AT HH List - " + taxHHList);
			}

		}
		Boolean isAPTCElig = Boolean.FALSE;
		Boolean isStateSubsidyElig = Boolean.FALSE;
		com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram hhStateSubsidyElig = null;
		if (!hhLevelEliDetails.isEmpty()){
			if (hhLevelEliDetails.containsKey("APTCElig")){
				isAPTCElig = (Boolean) hhLevelEliDetails.get("APTCElig");
				
				/**
				 * HIX-115401
				 * 	If APTC amount is passed in InsuranceApplicant tag who is "NOT SEEKING COVERAGE", it is not set on HH level.
				 * 	To handle this scenario, following code is added. 
				 * 	It will check if MAX_APTC_AMT is not set and HH is APTC eligible, it will fetch data from InsuranceApplicants.
				 * */
				if(!hhLevelEliDetails.containsKey(EligibilityConstants.MAX_APTC_AMT) || 
						(hhLevelEliDetails.containsKey(EligibilityConstants.MAX_APTC_AMT) && hhLevelEliDetails.get(EligibilityConstants.MAX_APTC_AMT) == null)){
					setMaxAPTCAmountOnHHLevel(taxHHList,hhLevelEliDetails);
				}
			}
			if (hhLevelEliDetails.containsKey(STATE_SUBSIDY_ELIG)){
				isStateSubsidyElig = (Boolean) hhLevelEliDetails.get(STATE_SUBSIDY_ELIG);
			}
			if (hhLevelEliDetails.containsKey(STATE_SUBSIDY_PROGRAM)){
				hhStateSubsidyElig = (com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram) hhLevelEliDetails.get(STATE_SUBSIDY_PROGRAM);
				hhLevelEliDetails.remove(STATE_SUBSIDY_PROGRAM);

			}
			if (hhLevelEliDetails.containsKey("APTCProgram")){
				hhAPTCElig = (com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram) hhLevelEliDetails.get("APTCProgram");
				hhLevelEliDetails.remove("APTCProgram");
			}
			if(hhLevelEliDetails.containsKey("HHCSLevel")) {
				hhCSLevel = (HashSet<String>)hhLevelEliDetails.get("HHCSLevel");
				List<String> hhMemberCSLevels = new ArrayList<String>();
				hhMemberCSLevels.addAll(hhCSLevel);
				String HHLevellowestCS = getLowPriorityCSLevel(hhMemberCSLevels);
				hhLevelEliDetails.put(EligibilityConstants.CSR_LEVEL, HHLevellowestCS);
			}
		}
		//5. Derive Application Eligibility Status and update application
		applicationEligibilityHelper.processEligibility(eligibilityProgramMap, erpResponse.isFullAssessment(), erpResponse.getApplicationID(), hhLevelEliDetails);

		//6. handle DE and CAM case separately...
		if ("NM".equals(STATE_CODE)){
			qHPApplicationEligibilityHelper.processEligibility(erpResponse);
		}

		//7. determine application is Native American Application...
		nativeAmericanEligibilityHelper.processEligibility(erpResponse.getApplicationID());
		BigDecimal maxAPTCAmount = null;
		if(hhLevelEliDetails.containsKey(EligibilityConstants.MAX_APTC_AMT)) {
			maxAPTCAmount = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_APTC_AMT);
		}
		BigDecimal maxStateSubsidyAmount = null;
		if(hhLevelEliDetails.containsKey(EligibilityConstants.MAX_STATE_SUBSIDY_AMT)) {
			maxStateSubsidyAmount = (BigDecimal) hhLevelEliDetails.get(EligibilityConstants.MAX_STATE_SUBSIDY_AMT);
		}
		
		if("NV".equals(STATE_CODE)){
			if(this.isNVInboundATRequest(giWsPayloadId) ==  false){
				applicationEligibilityHelper.insertHHAptc(hhAPTCElig, hhStateSubsidyElig, erpResponse.getApplicationID(),maxAPTCAmount, maxStateSubsidyAmount, erpResponse.getSlcspAmount());
			}
		}else{
			applicationEligibilityHelper.insertHHAptc(hhAPTCElig, hhStateSubsidyElig, erpResponse.getApplicationID(),maxAPTCAmount, maxStateSubsidyAmount, erpResponse.getSlcspAmount());
		}
		return true;
	}
	
	/**
	 *  HIX-115401
	 * 	Find max aptc amount from the  insurance applicant list irrespective of their seeking coverage status.
	 * 	This method is called conditionally only when HH level max aptc amount is not set even if HH is eligible for APTC
	 * 
	 * */
	
	private void setMaxAPTCAmountOnHHLevel(List<TaxHouseholdMember> taxHHList,Map<String, Object> hhLevelEliDetails) {
		if(taxHHList != null && taxHHList.size() >0){
			for (TaxHouseholdMember taxHouseholdMember : taxHHList) {
				if(taxHouseholdMember!= null &&  taxHouseholdMember.getEligibilityProgramMap() != null && taxHouseholdMember.getEligibilityProgramMap().get("APTCEligibilityType") != null){
					EligibilityProgram eligibilityProgram =  taxHouseholdMember.getEligibilityProgramMap().get("APTCEligibilityType");
					if(eligibilityProgram != null && eligibilityProgram.getMaxAPTCAmount() != null){
						hhLevelEliDetails.put(EligibilityConstants.MAX_APTC_AMT, eligibilityProgram.getMaxAPTCAmount()); 
						break;
					}
				}
			}
		}
		
		
	}

	private String getLowPriorityCSLevel(List<String> hhMemberCSLevels){
		String CSLevel = null;
		Integer lowestIndex = null;
		Integer currentIndex = null;
		if(CollectionUtils.isNotEmpty(hhMemberCSLevels)) {
			for(String memberCS : hhMemberCSLevels) {
				currentIndex = csPriorityList.indexOf(memberCS);
				if(lowestIndex == null || lowestIndex>currentIndex) {
					lowestIndex = currentIndex;
				}
			}
			CSLevel = csPriorityList.get(lowestIndex);
		}
		return CSLevel;
	}

	private boolean isNVInboundATRequest(Integer giWsPayloadId){
		GIWSPayload giwsPayload = giwsPayloadRepository.findOne(giWsPayloadId);
		AccountTransferRequestPayloadType accountTransferRequest = null;
		try {
			if(!"NV".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)))
			{
				String requestBody = ReferralUtil.extractSoapBoady(giwsPayload.getRequestPayload());
				accountTransferRequest = ReferralUtil.unmarshal(requestBody);
			}
			else
			{
				accountTransferRequest = ReferralUtil.getAccountTransferRequestObject(giwsPayload.getRequestPayload());
			}
		} catch (Exception e) {
			LOGGER.error("Error occured while extracting AT request body for " + giWsPayloadId);
			accountTransferRequest = null;
		}
		boolean isNvInitiatedAT = false;
		if(accountTransferRequest != null){
			List<com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType > iTypes = accountTransferRequest.getInsuranceApplication().getApplicationIdentification();
			for(com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType tIdentificationType : iTypes) {
				if( tIdentificationType.getIdentificationID() != null && tIdentificationType.getIdentificationCategoryText() != null && "Exchange".equalsIgnoreCase(tIdentificationType.getIdentificationCategoryText().getValue())){
					isNvInitiatedAT = true;
				}
			}
		}
		return isNvInitiatedAT;
	}
}
