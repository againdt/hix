package com.getinsured.hix.indportal.dto.dm.application;

import java.util.List;

public class Medicaid {
	 private Boolean medicaidDeniedDueToImmigrationIndicator;
	 private Boolean medicaidDeniedIndicator;
	 private String medicaidDeniedDate; 
	 private Boolean appliedDuringOeOrLifeChangeIndicator;
	 private Boolean coveredDependentChildIndicator;
	 private Boolean enrolledInHealthCoverageIndicator;
	 private List<EnrolledCoverage> insuranceCoverage;


	 // Getter Methods 

	 public Boolean getMedicaidDeniedDueToImmigrationIndicator() {
	  return medicaidDeniedDueToImmigrationIndicator;
	 }

	 public Boolean getMedicaidDeniedIndicator() {
	  return medicaidDeniedIndicator;
	 }

	 public Boolean getAppliedDuringOeOrLifeChangeIndicator() {
	  return appliedDuringOeOrLifeChangeIndicator;
	 }

	 public Boolean getEnrolledInHealthCoverageIndicator() {
	  return enrolledInHealthCoverageIndicator;
	 }

	 // Setter Methods 

	 public void setMedicaidDeniedDueToImmigrationIndicator(Boolean medicaidDeniedDueToImmigrationIndicator) {
	  this.medicaidDeniedDueToImmigrationIndicator = medicaidDeniedDueToImmigrationIndicator;
	 }

	 public void setMedicaidDeniedIndicator(Boolean medicaidDeniedIndicator) {
	  this.medicaidDeniedIndicator = medicaidDeniedIndicator;
	 }

	 public void setAppliedDuringOeOrLifeChangeIndicator(Boolean appliedDuringOeOrLifeChangeIndicator) {
	  this.appliedDuringOeOrLifeChangeIndicator = appliedDuringOeOrLifeChangeIndicator;
	 }

	 public void setEnrolledInHealthCoverageIndicator(Boolean enrolledInHealthCoverageIndicator) {
	  this.enrolledInHealthCoverageIndicator = enrolledInHealthCoverageIndicator;
	 }

	public List<EnrolledCoverage> getInsuranceCoverage() {
		return insuranceCoverage;
	}

	public void setInsuranceCoverage(List<EnrolledCoverage> insuranceCoverage) {
		this.insuranceCoverage = insuranceCoverage;
	}

	public String getMedicaidDeniedDate() {
		return medicaidDeniedDate;
	}

	public void setMedicaidDeniedDate(String medicaidDeniedDate) {
		this.medicaidDeniedDate = medicaidDeniedDate;
	}

	public Boolean getCoveredDependentChildIndicator() {
		return coveredDependentChildIndicator;
	}

	public void setCoveredDependentChildIndicator(Boolean coveredDependentChildIndicator) {
		this.coveredDependentChildIndicator = coveredDependentChildIndicator;
	}
	
	}
