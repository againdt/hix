package com.getinsured.hix.serff.service;

/**
 * DTO class is used to get data from Issuer table to generate Drop-Down box.
 */
public class IssuerDropDownDTO {

	String hiosIssuerId;
	String shortName;
	String name;

	public IssuerDropDownDTO(String hiosIssuerId, String shortName, String name) {
		this.hiosIssuerId = hiosIssuerId;
		this.shortName = shortName;
		this.name = name;
	}

	public String getHiosIssuerId() {
		return hiosIssuerId;
	}

	public String getShortName() {
		return shortName;
	}

	public String getName() {
		return name;
	}
}
