@ECHO OFF
set dirPath=D:\GHIX\Project\ghix\
set targetPath=D:\GHIX\Project\ghix\ghix-ear\target
set deployPath=E:\ProgramFiles\EAP-6.0.0.GA\jboss-eap-6.0\standalone\deployments

pushd %dirPath%\ghix-ahbx
@ECHO ON
call mvn -DskipTests -q -PDEV clean install

copy %dirPath%ghix-ahbx\target\ghix-ahbx.war %deployPath%
echo>> %deployPath%\ghix-ahbx.war.dodeploy


pause
