<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<style type="text/css">
	#sidebar .csrm-sidebar  li{border: 1px solid #E9E9E9;}
	#sidebar .nav-list li a:hover {
  		background: #D37100 !important;
	}
#sidebar .nav-list li a{color: #fff !important;text-shadow: none;}
</style>
<div class="gutter10">
	<div class="row-fluid">
        <h1 class="span10 margin0 gutter20-b"><spring:message code="label.multiplier"/></h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.userfulInfo"/></h4>
       		</div>
       		<ul class="nav nav-list graybg csrm-sidebar">
	       		<li> 
		       		<spring:message code="label.csrInfo"/> 
				</li>
    			<li> <a class="btn btn-primary" href="<c:url value="/admin/planmgmt/csrmultiplier/history"/>"><spring:message code="label.viewUpdateHistory"/></a></li>
			</ul>
        </div>
		<div class="span9" id="rightpanel">
			<display:table name="csrMultipliers" list="csrMultipliers" sort="list" class="table table-condensed table-border-none table-striped">
	           <display:column titleKey="label.planCSRVariation" property="CSRVariation" sortable="false"/>
	           <display:column titleKey="label.metalTier" property="planLevel" sortable="false"/>
	           <display:column titleKey="label.csrMultiplier" property="multiplier" sortable="false"/>
	           <display:column title='<input type="submit" onclick="editCSRMultiplier();" class="btn btn-primary" value="Edit">'/>	
			</display:table>
		</div>
		<form name="editCSRMultiplierForm" id="editCSRMultiplierForm" action="<c:url value="/admin/planmgmt/csrmultiplier/edit"/>" method="get"></form>
	</div>
</div>
<script type="text/javascript">
	function editCSRMultiplier(){
		document.getElementById("editCSRMultiplierForm").submit();
	}
</script>