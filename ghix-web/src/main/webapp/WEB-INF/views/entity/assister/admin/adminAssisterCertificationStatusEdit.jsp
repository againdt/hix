<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>

<style>
	form .box-loose .form-actions {
		margin-bottom: 0;
	}
	.table-auto {
		width: auto;
	}
	table#brokerStatusHistory {
	    margin: 0 25%;
    	width: 75%;
	}
	th.header .btn {
		background-color: #F5F5F5;
		background-image: linear-gradient(to bottom, #FFFFFF, #E6E6E6);
		background-repeat: repeat-x;
		box-shadow: 0 1px 0 rgba(255, 255, 255, 0.2) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
	}
</style>
	<div class="gutter10-lr">
		<div class="l-page-breadcrumb hide">
			<!--start page-breadcrumb -->
			<div class="row-fluid">
				<ul class="page-breadcrumb">
					<li><a href="javascript:history.back()">&lt; <spring:message
								code="label.back" /></a></li>
					<li><a href="<c:url value="/admin/broker/manage"/>"><spring:message
								code="label.brokers" /></a></li>
					<li><spring:message code="label.assister.manage"/></li>
				</ul><!--page-breadcrumb ends-->
			</div>
			<!-- end of .row-fluid -->
		</div>
		<!--l-page-breadcrumb ends-->
		<div class="row-fluid">
			<h1><a name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
		</div>

	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<div class="header"></div>
				<ul class="nav nav-list">						
					<li><a href="/entity/assisteradmin/viewassisterinformation/${assister.id}"><spring:message code="label.assister.assisterInformation"/></a></li>
					<li><a href="/entity/assisteradmin/viewassisterprofile/${assister.id}"><spring:message code="label.assister.profile"/></a></li>
					<li class="active"><a href="/entity/assisteradmin/viewassistercertificationstatus/${assister.id}"><spring:message code="label.assister.certificationStatus"/></a></li>						
				</ul>		
		</div><!-- end of span3 -->
	<div class="span9" id="rightpanel">	
	 <form class="form-horizontal" id="frmupdateassistercertification"  enctype="multipart/form-data" name="frmupdateassistercertification" method="POST" action="<c:url value="/admin/assister/updateassistercertificationstatus"/>">	 
			<input type="hidden" id="assisterId" name="assisterId" value="${assister.id}"/>
			<input type="hidden" id="documentId" name="documentId" value=""/>						
				<div class="row-fluid">									
<!-- 					<div class="header"> -->
<%-- 						<h4 class="span10"><spring:message code="label.brkCertificationStatus"/></h4> --%>
<%-- 							<a class="btn btn-small" type="button"  href="/hix/admin/broker/certificationstatus/${broker.id}"><spring:message code="label.brkCancel"/></a> --%>
<!-- 					</div> -->
					<div class="">				
					<table class="table table-border-none table-condensed table-auto verticalThead">
						<thead>
							<tr>
								<th class="header span3"><spring:message code="label.brkCertificationStatus"/></th>
								<th class="header span6"><a class="btn btn-small pull-right" href="/hix/admin/broker/certificationstatus/${broker.id}"><spring:message code="label.brkCancel"/></a></th>
							</tr>
						</thead>
						<!-- header class for accessibility -->
								<tbody>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.brkCertificationStatus"/></th>
										<td><strong>${assister.certificationStatus}</strong></td>
									</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.brkCertificationRenewalDate"/></th>
										<td><strong>${assister.reCertificationDate}</strong></td>
									</tr>
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.brkCertificationNumber"/></th>
										<td><strong>${assister.certificationNumber}</strong></td>
									</tr>
									<%-- <tr>
										<td class="span4 txt-right"><label for="certificationStatus"><spring:message code="label.brkNewStatus"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label></td>
										<td>
										<select  size="1"  id="certificationStatus" name="certificationStatus" class="input-medium">
											<option value="">Select</option>
											<c:forEach  items="${statuslist}" var= "value">
												<option value="${value}">${value}</option> 
											</c:forEach>
										</select>
										<div id="certificationStatus_error"></div>
										</td>
									</tr> --%>
									
									<tr>
										<td class="span4 txt-right"><label for="certificationStatus"><spring:message code="label.assister.newStatus"/></label></td>
											<td>
												<select size="1" id="${status.lookupValueLabel}" name="${status.lookupValueLabel}" class="ie8">
													<option value="">Select</option>
														<c:forEach var="status" items="${statusList}">
																<option id="${status.lookupValueLabel}" class="hourclass" value="<c:out value="${status.lookupValueCode}" />">
																${status.lookupValueLabel}
																</option>
													    </c:forEach>
												</select>
														<div id="certificationStatus_error"></div>
										   </td>
									</tr>
									<tr>
										<td class="span4 txt-right"><label for="comments"><spring:message code="label.brkComment"/></label></td>
										<td><textarea id="comments" name= "comments"></textarea></td>
									</tr>
									
									<tr>
		                   			<td class="txt-right"><label for="fileInput"><spring:message code="label.brkUploadSupportingDocs"/></label></td>
		                   			<td>		                   				
		                   				<input type="file" class="input-file" id="fileInput" name="fileInput">			                   				
		                   				<input type="submit" id="btnUploadDoc" name="btnUploadDoc" value="Upload" class="btn">		                   	   			
		                   			</td>
	                  				</tr>																											
							</tbody>
					</table>
					</div>							
				</div>			
			<div class="form-actions">	
			     <input type="hidden" id="fileToUpload" name="fileToUpload" value="fileInput"/>						 				
				<input type="submit" class="btn btn-primary" name="update" id="update" value="Submit"/>	
			</div>
			</form>		
			<!-- Showing comments -->			 					
			</div><!-- end of span9 -->			
		</div>
		<div class="gutter10">	
                	<p><spring:message code="label.assister.viewCertificationHistory"/></p>
                    <h4><spring:message code="label.assister.certificationHistory"/></h4>			
					<display:table id="enrollmentRegisterStatusHistory" name="enrollmentRegisterStatusHistory" list="enrollmentRegisterStatusHistory" requestURI="" sort="list" class="table table-condensed table-border-none table-striped" >
				           <display:column property="updatedOn" title="Date"  format="{0,date,MMM dd, yyyy}" sortable="false"  />
				           <display:column property="previousRegistrationStatus" title="Previous status" sortable="false" />
				           <display:column property="newRegistrationStatus" title="New Status" sortable="false" />
						   <display:column property="comments" title="View Comment" sortable="false" />
							<display:column title="View Attachment" > 
				                 <c:choose>
				                      <c:when test="${enrollmentRegisterStatusHistory.documentId!=null}">	
			                                <a href="#" onClick="showdetail('${enrollmentRegisterStatusHistory.documentId}');" id="edit_${enrollmentRegisterStatusHistory.documentId}"><spring:message code="label.assister.viewAttachment"/></a> 
			                          </c:when>
			                          <c:otherwise>	
			                                <spring:message code="label.assister.noAttachment"/>
			                          </c:otherwise>
			                     </c:choose>
						   </display:column>
					</display:table>					
				</div><!-- end of .gutter10 -->
				<!-- end of form-actions -->
</div>
	<script type="text/javascript">
		$(document).ready(function(){			
			$("#btnUploadDoc").click(function(){	
				if($("#fileInput").val() != '') {					
					$('#frmupdatecertification').ajaxSubmit({					
						url: "<c:url value='/admin/broker/uploaddocument?licenseNumber=${broker.licenseNumber}'/>",					
						success: function(responseText){							
							if(responseText != "0"){
								$('#documentId').val(responseText);
								alert("File uploaded successfully");											
							}else{
								alert("Unable to upload the file");													
							}	
				       	}
					});
				}
			  return false; 
			});	// button click close			
		});
		
		function getComment(comments)
		{					
				
			    $('#commentDet').html("<p> Loading Comment...</p>");
				comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br				
				comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
				$('#commentDet').html("<p> "+ comments + "</p>");			
		}
		
		var validator = $("#frmupdatecertification").validate({ 
			 rules : {
				certificationStatus : {required : true},
			 },
			 messages : {
				certificationStatus : { required : "<span><em class='excl'>!</em><spring:message  code='label.validateCertificationStatus' javaScriptEscape='true'/></span>"}
			 },
			 errorClass: "error",
			 errorPlacement: function(error, element) {
			 var elementId = element.attr('id');
			 error.appendTo( $("#" + elementId + "_error"));
			 $("#" + elementId + "_error").attr('class','error help-inline');
			}
		});			
</script>
<script >
 function showdetail(documentId) {
		
	 
	 var  contextPath =  "<%=request.getContextPath()%>";
		 
	    var documentUrl = contextPath + "/entity/assisteradmin/viewAttachment?documentId="+documentId;
	    window.open(documentUrl,"_blank","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
 }

</script>
