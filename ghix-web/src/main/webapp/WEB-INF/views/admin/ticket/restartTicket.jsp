<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>    

<div id="restartTicketModal" class="modal" style="display:none">
	 <div class="modal-body">
        <h3>Are you sure you want to Restart this ticket?</h3>
        <p>Restarting a ticket will bring you back to the start of the ticket workflow</p>
      </div>
      <div class="modal-footer">
        <button type="button" id="cancelRestartBtn" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <a href="#" type="button" id="submitRestartBtn" class="btn btn-primary" id="btnRestartTicket" >Restart</a>
      </div>
      <div id="restartAjaxloader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
    </div>
</div>

<script>
	function restartTicketFunction(ticketid){
		 $("#restartTicketModal #submitRestartBtn").unbind("click",restartTicket).bind("click",{ticketid:ticketid},restartTicket);
		 $("#restartTicketModal").modal();
	}
	
	function restartTicket(e)
	{   
		$("#cancelRestartBtn").attr("disabled",true); 	
		$("#submitRestartBtn").attr("disabled",true); 
		
		var ticketId = e.data.ticketid;	
		
		var validateUrl ='<c:url value="/ticketmgmt/ticket/restart"></c:url>';
		var redirectUrl ='<c:url value="/ticketmgmt/ticket/ticketdetail/'+ticketId+'"> </c:url>';
		
		$("#restartAjaxloader").show();
		$.ajax({
			url : validateUrl,
			type : "POST",
			data : {
				${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				ticketId : ticketId
			},
			success : function(response) {
				if (response !=null && response != "") {
					window.location.href = redirectUrl;
					return false;
				  
				} else {
					alert("Could not restart");
					//show the error
					$("#cancelRestartBtn").attr("disabled",false); 	
					$("#submitRestartBtn").attr("disabled",false); 
				}
				
				
			},
			error : function(error) {
				alert("restart error");
				$("#cancelRestartBtn").attr("disabled",false); 	
				$("#submitRestartBtn").attr("disabled",false); 
				//show the error
			}
		});
	}
</script>