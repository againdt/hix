<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.LocalDate"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.timeshift.util.TSDate"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
    <%
        String householdCaseID =  (String) request.getSession().getAttribute("externalHouseholdCaseId");
    	String userActiveRoleLabel =  (String) session.getAttribute("userActiveRoleLabel");
    %>
<script src="<c:url value="/resources/js/angular-bootstrap-datepicker.js"/>"></script>
<c:set var="calheersEnv" value='<%=GhixConstants.CALHEERS_ENV%>'></c:set>
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<%
	String gracePeriod = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
	String maCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);
	String coverageYearOptionCutOffDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB);
	String coverageYearOptionCutOffDatePriviledgeUser = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IND_PORTAL_SHOW_CURRENT_YEAR_TAB_PRIVILEDGE_USER);
	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	try{
			if(coverageYearOptionCutOffDatePriviledgeUser !=null && coverageYearOptionCutOffDatePriviledgeUser.trim().length()!=0 && !"Individual".equalsIgnoreCase(userActiveRoleLabel)){
				coverageYearOptionCutOffDate=coverageYearOptionCutOffDatePriviledgeUser;
			}
		    if(coverageYearOptionCutOffDate != null && coverageYearOptionCutOffDate.trim().length() != 0){
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				Date todaysDate = new TSDate();
				Date cutOffDate = DateUtils.parseDateStrictly(coverageYearOptionCutOffDate, new String[]{"MM/dd/yyyy"});
				if (todaysDate.before(cutOffDate)) {
					maCurrentCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PREV_COVERAGE_YEAR);
				}
		    }
		}
		catch(Exception ex){}
	String maRenewalCoverageYear = maCurrentCoverageYear;
	String maServerDateTime = new TSDate().toString();
	String renewalCoverageYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_RENEWAL_COVERAGE_YEAR);
	if(null!=renewalCoverageYear && renewalCoverageYear.trim().length()>0){
		maRenewalCoverageYear = renewalCoverageYear;
	}
	String exchangeStartYear = "2015";
	String exchangeStartYearProperty = DynamicPropertiesUtil.getPropertyValue("global.exchangeStartYear");
	if(null!=exchangeStartYearProperty && exchangeStartYearProperty.trim().length()>0){
	    exchangeStartYear = exchangeStartYearProperty;
	}
	String ssapFlowVersion = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_SSAP_FLOW_VERSION);
%>

<input id="maCurrentCoverageYear" name="maCurrentCoverageYear" type="hidden" value="<%=maCurrentCoverageYear%>"/>
<input id="exchangeStartYear" name="exchangeStartYear" type="hidden" value="${exchangeStartYear}"/>
<input id="gracePeriod" name="gracePeriod" type="hidden" value="<%=gracePeriod%>"/>
<input id="maServerDateTime" name="maServerDateTime" type="hidden" value="<%=maServerDateTime%>" />
<%--<input id="ssapFlowVersion" name="ssapFlowVersion" type="hidden" value="<%=ssapFlowVersion%>" />--%>
<c:set var="stateCode" value="<%=stateCode%>" />
<c:set var="householdCaseID" value="<%=householdCaseID%>" />
<script>
// data retriving from spring source has to be in jsp files
var csrStorage = {
		edit:{header: '<spring:message code="indportal.portalhome.editapplication2"/>', content: '<spring:message code="indportal.portalhome.editapplicationcontent" javaScriptEscape="true"/> '},
		initiate:{header: '<spring:message code="indportal.portalhome.initiateverifications2" />', content: '<spring:message code="indportal.portalhome.initiateverificationscontent" javaScriptEscape="true"/> '},
		rerun:{header: '<spring:message code="indportal.portalhome.reruneligibility2"/>', content: '<spring:message code="indportal.portalhome.reruneligibilitycontent" javaScriptEscape="true"/> '},
		cancelTerm:{header: '<spring:message code="indportal.portalhome.cancelorterminatemyplan2" />', content: '<spring:message code="indportal.portalhome.cancelorterminatemyplancontent" javaScriptEscape="true"/>'},
		update:{header: '<spring:message code="indportal.portalhome.updatecarrier2" />', content: '<spring:message code="indportal.portalhome.updatecarriercontent" javaScriptEscape="true"/>'},
		view:{header: 'view', content: 'csrStorage.view.content'},
		specialEnroll:{header: '<spring:message code="indportal.portalhome.openspecialenorollment2"/>', content: '<spring:message code="indportal.portalhome.openspecialenorollmentcontent" javaScriptEscape="true"/>'},
		coverageDate:{header: '<spring:message code="indportal.portalhome.changecoveragedate2" />', content: '<spring:message code="indportal.portalhome.changecoveragedatecontent" javaScriptEscape="true"/>'},
		reinstate:{header: '<spring:message code="indportal.portalhome.reinstateenrollment2" />', content: '<spring:message code="indportal.portalhome.reinstateenrollmentcontent" javaScriptEscape="true"/> '},
		overrideSEPDenial:{header: '<spring:message code="indportal.portalhome.overridesepdenial2"/>', content: '<spring:message code="indportal.portalhome.overridesepdenialcontent" javaScriptEscape="true"/>'}
};
var calheersEnv = '${calheersEnv}';
$(document).ready(function() {
	var scope = angular.element(document.getElementById("myApplications")).scope();
	if (scope) {
		scope.$apply(function() {
			scope['myApplications'] = {title: '<spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/>'};
		});
	}
});
</script>

<script type="text/ng-template" id="myApplications">
	<style>
		.no-pb {
			padding-bottom: 0 !important;
		}
		.pt-10 {
			padding-top: 10px !important;
		}
	</style>
<div class="row-fluid" ng-init="init()">

<div spinning-loader="loader"></div>

<div class="row-fluid header">
  <h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8 no-pb pt-10"><spring:message code="indportal.portalhome.myapplications" javaScriptEscape="true"/>
  </h4>
   <span id="aid_coverageYearDd" data-automation-id="coverageYearDd" class="col-xs-12 col-sm-5 col-md-4 col-lg-4 dropdown coverage-year coverage-year_dropdown coverage-year_right">
        <label for="application_year" class="bs3-control-label-inline"><spring:message code="indportal.portalhome.applicationYear"/></label>
        <select id="application_year" class="input-small" ng-model="maCurrentCoverageYear" ng-options="coverageYear for coverageYear in maCurrentCoverageYearArray" ng-change="init()"></select>
   </span>
</div>


  <div class="dashboard-rightpanel" id="rightpanel">
    <div class="well" ng-if="currentApplications.length==0">
      <div class="well-step">
        <div class="row-fluid">
          <div class="gutter20">
          <spring:message code="indportal.portalhome.stepOne" htmlEscape="false" javaScriptEscape="false"/>

					<c:if test="${stateCode != 'MN'}">
						<spring:message code="indportal.portalhome.noapplicationyet" htmlEscape="false" javaScriptEscape="false"/>
					</c:if>
			</div>
			<div class="gutter10">
			<div class="applicationState">
            <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
             	<c:if test="${applicationStartLabel =='Start Your Application'}">
					<button ng-if="modelattrs.sCode=='ID' && myAppsInsideOE === true" id="aid_appStartLabelBtn" data-automation-id="appStartLabelBtn" class="btn btn-primary btn_save pull-right" ng-click="$parent.showDialogMyApps = true">
						${applicationStartLabel}
					</button>
				</c:if>
				<c:if test="${applicationStartLabel =='Start Your Application'}">
					<button ng-if="modelattrs.sCode=='ID' && myAppsInsideQEP === true" id="aid_startYourAppQEPBtn_01" data-automation-id="startYourAppQEPBtn" class="btn btn-primary btn_save pull-right" ng-click="goQephome()">
						<spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
					</button>
				</c:if>
                <button ng-if="modelattrs.sCode == 'NV' && myAppsInsideOE === true" ng-click="goToNewSSAP('OE')" id="aid_startYourAppQEPBtn_02" data-automation-id="startYourAppQEPBtn" class="btn btn-primary btn_save pull-right">
                    <spring:message code="indportal.portalhome.startyourapplication" javaScriptEscape="true"/>
                </button>
                <button ng-if="modelattrs.sCode == 'NV' && myAppsInsideQEP === true" ng-click="goToNewSSAP('QEP')" id="aid_startYourAppQEPBtn_02" data-automation-id="startYourAppQEPBtn" class="btn btn-primary btn_save pull-right">
                    <spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
                </button>
								<button ng-if="modelattrs.sCode == 'MN' && myAppsInsideOE === false" ng-click="startApplication2()" id="aid_startYourAppQEPBtn_02" data-automation-id="startYourAppQEPBtn" class="btn btn-primary btn_save pull-right">
									<spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
								</button>
								<button ng-if="modelattrs.sCode != 'MN' && modelattrs.sCode != 'NV' && modelattrs.sCode !='ID'" ng-click="startApplication2()" id="aid_startYourAppQEPBtn_02" data-automation-id="startYourAppQEPBtn" class="btn btn-primary btn_save pull-right">
									<spring:message code="indportal.portalhome.startyourapplicationqep" javaScriptEscape="true"/>
								</button>
            </sec:accesscontrollist>
            </div>
          </div>

          <!-- Modal Starts-->
          <div ng-show= "showDialogMyApps" modal-show="showDialogMyApps" id="aid_financialHelpModal" data-automation-id="financialHelpModal" class="modal fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="financialHelpModal" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h3 id="financialHelpModal"><spring:message code="indportal.portalhome.financialhelp" javaScriptEscape="true"/></h3>
                </div>
                <div class="modal-body">
				  <sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
				  <c:choose>
					<c:when test="${not showMessage}">
						<p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" id="aid_restrictUserToRedirectLink" data-automation-id="restrictUserToRedirectLink" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
					</c:when>
					<c:otherwise>
						<p data-automation-id="doyouwanttoapplyforfinancialhelp"><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
					</c:otherwise>
				  </c:choose>
                  <!--<p><spring:message code="indportal.portalhome.doyouwanttoapplyforfinancialhelp" javaScriptEscape="true"/></p>
				  <sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user" var="showMessage"/>
				  <c:if test="${not showMessage}">
			      <p><spring:message code="indportal.portalhome.restrictusertoredirectidaholink" javaScriptEscape="true"/><a href="javascript:void(0)" onclick="openInNewTab('<spring:message code="indportal.portalhome.idalinkUrl"/>')"> <spring:message code="indportal.portalhome.idalinkUrl"/></a>. <spring:message code="indportal.portalhome.restrictusertoredirectidaholinknext" javaScriptEscape="true"/></p>
			      </c:if>-->
                  <!-- <p><spring:message code="indportal.portalhome.ifyouselecttoapplyforfinancialhelp" javaScriptEscape="true"/></p>-->
                </div>
                <div class="modal-footer">
                  <button id="aid_cancel" class="btn pull-left" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.contactus.cancel" javaScriptEscape="true"/></button>
				  <sec:accesscontrollist hasPermission="IND_PORTAL_APPLY_COST_SAVINGS" domainObject="user">
                 	 <a href="javascript:void(0)" id="aid_yesTakeMeToLink" data-automation-id="yesTakeMeToLink" class="btn btn-primary" data-dismiss="modal" onclick="openInSameTab('<%=GhixConstants.APP_URL%>individual/saml/idalinkredirect')"><spring:message code="indportal.portalhome.yestakemetoidalink" javaScriptEscape="true"/></a>
				  </sec:accesscontrollist>
                    <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
					<button id="aid_stayOn" data-automation-id="stayOn" class="btn btn-primary" data-dismiss="modal" ng-click="goToSsapOE()"><spring:message code="indportal.portalhome.stayon" javaScriptEscape="true"/></button>
		            </sec:accesscontrollist>
                </div>
              </div><!-- /.modal-content-->
            </div> <!-- /.modal-dialog-->
          </div>
          <!-- Modal Ends-->

        </div>
      </div><!--  well-steps ends -->
    </div><!--  well ends -->

  <div class="applicationState margin20-b" ng-repeat="application in currentApplications"  ng-if="currentApplications.length>0">
    <div class="well-step" id="currentAppStatus">
    <div class="appsHeaders margin10-b row-fluid">
        <div aria-label="Shop" class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
            <p class="strong">
            <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
						<spring:message code="indportal.portalhome.applicationName" arguments='{{application.coverageYear}}' javaScriptEscape="true"/>
            </sec:accesscontrollist>
            </p>
            <div ng-if="application.isNonFinancial === false">
                <p>
                    <span id="aid_APTC" class="brktip blue-color" rel="tooltip" placement="top" data-original-title="<spring:message code="indportal.portalhome.APTCToolTip" htmlEscape="true"/>">
                    <spring:message code="indportal.portalhome.aptc" htmlEscape="true"/>:
                    </span>
                    <span class="strong">{{application.aptc === null ? "Not Eligible" : (application.aptc | currency)}}</span>
                </p>
                <p ng-if="application.stateSubsidy">
                    <span id="aid_STATESUBSIDY" class="brktip blue-color" rel="tooltip" placement="top" data-original-title="<spring:message code="indportal.portalhome.StateSubsidy" htmlEscape="true"/>">
                    <spring:message code="indportal.portalhome.StateSubsidy" htmlEscape="true"/>:
                    </span>
                    <span class="strong">{{application.stateSubsidy === null ? "Not Eligible" : (application.stateSubsidy | currency)}}
                    </span>
                </p>
                <p>
                    <span id="aid_CSR" class="brktip blue-color" rel="tooltip" placement="top" data-original-title="<spring:message code="indportal.portalhome.CSRToolTip" htmlEscape="true"/>">
                    <spring:message code="indportal.portalhome.csr" htmlEscape="true"/>:
                    </span>
                    <span class="strong">{{application.csr === null ? "Not Eligible" : application.csr}}
                    </span>
                </p>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div id="aid_appType_01" data-autamation-id="appType">
                <spring:message code="indportal.portalhome.type" javaScriptEscape="true"/>: {{getAppType(application.appType)}}
            </div>
            <div id="aid_casenumber_01" data-autamation-id="casenumber">
                <c:if test="${stateCode != 'CA'}">
                <spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>: {{application.caseNumber}}
                </c:if>
                <c:if test="${stateCode == 'CA'}">
                <spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>: {{householdCaseNumber}}
                </c:if>
            </div>
            <div id="aid_createdDate_01" data-autamation-id="createdDate">
								<spring:message code="indportal.portalhome.creationdate" javaScriptEscape="true"/>: {{application.createdDate}}
            </div>
            <div id="aid_lastUpdated_01" data-autamation-id="lastUpdated">
								<spring:message code="indportal.portalhome.lastupdated" javaScriptEscape="true"/>: {{application.lastUpdated}}
            </div>
        </div>
    </div>

    <!-- CURRENT APPLICATIONS -->
    <div id="currentApp">
        <div class="row-fluid" ng-show="{{application.applicationStatus == 'Enrolled (Or Active)' || application.applicationStatus == 'Partially Enrolled'}}">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 gutter0">
                    <p><spring:message code="indportal.portalhome.youhaveenrolledinqhp" javaScriptEscape="true"/></p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 gutter0">
                    <a href="javascript:void(0)" ng-click="showPlanSummary(application.caseNumber)" id="planSummary" data-autamation-id="planSummary" class="pull-right btn btn-primary btn_save"> <spring:message code="indportal.plansummary.title" javaScriptEscape="true"/></a>
                </div>
            </div>
        </div>
        <!-- Go to dashboard button for tribes-->
        <div class="row-fluid" ng-if="${stage>4 && isRecTribe=='Y' && changePlanInSEP!='Y'}">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 gutter0">
                    <p><spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/> To update your plan, please Go Back To Dashboard</p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 gutter0">
                    <a href="javascript:void(0)" ng-click="goDash()" id="aid_goToDash_01" data-autamation-id="goToDash" class="pull-right btn btn-primary btn_save"> <spring:message code="indportal.portalhome.gotodash" javaScriptEscape="true"/></a>
                </div>
            </div>
        </div>
        <!-- Go to dashboard button for SEP change plan-->
        <div class="row-fluid" ng-if="${stage>4 && changePlanInSEP=='Y'}">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 gutter0">
                    <p><spring:message code="indportal.portalhome.sepChangePlanMessage" javaScriptEscape="true"/> To change your plan, please Go Back To Dashboard</p>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 gutter0">
                    <a href="javascript:void(0)" ng-click="goDash()" id="aid_goToDash_02"  data-autamation-id="goToDash" class="pull-right btn btn-primary btn_save"> <spring:message code="indportal.portalhome.gotodash" javaScriptEscape="true"/></a>
                </div>
            </div>
        </div>
      <!-- was ng-show now ng-if - this is causing js console errors- until a cleaner way is thought about, reverting to ng-show-->
        <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
        <div class="row-fluid" ng-show="{{application.applicationInProgress}}==false">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div ng-show="{{application.applicationStatus == 'Signed' || application.applicationStatus == 'Submitted'}}">
                    <p id="aid_yourAppHasEsigned_01" data-autamation-id="yourAppHasEsigned"><spring:message code="indportal.portalhome.yourapplicationhasbeenesigned1" javaScriptEscape="true"/></p>
                    <p id="aid_yourAppHasEsigned_02" data-autamation-id="yourAppHasEsigned"><spring:message code="indportal.portalhome.yourapplicationhasbeenesigned2" htmlEscape="false" javaScriptEscape="true"/> </p>
                    <p id="aid_yourAppHasEsigned_03" data-autamation-id="yourAppHasEsigned"><spring:message code="indportal.portalhome.yourapplicationhasbeenesigned3" htmlEscape="false" javaScriptEscape="true"/> </p>
                </div>
                <div ng-show="{{application.applicationStatus == 'Eligibility Received' || application.applicationStatus == 'Enrolled (Or Active)' || application.applicationStatus == 'Partially Enrolled'}}">
                <c:if test="${stateCode != 'CA'}">
                <p id="aid_toViewYourApp_01" data-autamation-id="toViewYourApp">
                    <spring:message code="indportal.portalhome.toviewyourapp1" javaScriptEscape="true"/> <a href="javascript:void(0)"
                    <c:choose>
                    <c:when test = "${stateCode == 'NV' && isUserSwitchToOtherView =='Y'}"> ng-click="showApplicationInViewMode(application.caseNumber)" </c:when>
                    <c:otherwise> ng-click="showApplication(application.caseNumber)" </c:otherwise>
                    </c:choose>
                    class="link"><spring:message code="indportal.portalhome.toviewyourapp2" javaScriptEscape="true"/></a>.
                </p>
                </c:if>
                <c:if test="${stateCode == 'CA'}">
                    <p id="aid_toViewYourApp_02" data-autamation-id="toViewYourApp"><a href="javascript:void(0)"  ng-click="showApplication(application.caseNumber)" class="link"><spring:message code="indportal.portalhome.toviewyourapp1" javaScriptEscape="true"/></a>. </p>
                </c:if>
                </div>
            </div>
        </div>
        <div class="row-fluid" ng-show="{{application.applicationInProgress}}==true">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 gutter0">
                    <p id="aid_yourAppInProgress" data-autamation-id="yourAppInProgress"><spring:message code="indportal.portalhome.yourapplicationisinprogress" javaScriptEscape="true"/></p>
                </div>
                <c:if test="${stateCode != 'MN'}">
                <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 gutter0">
                    <a id="aid_resumeApp" data-autamation-id="resumeApp" href="javascript:void(0)" ng-click="showApplication(application.caseNumber)" class="btn btn-primary btn-small btn_save pull-right"><spring:message code="indportal.portalhome.resumeapplication" javaScriptEscape="true"/></a>
                </div>
                </c:if>
            </div>
        </div>
        </sec:accesscontrollist>
        <sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
            <div class="row-fluid" ng-if="application.applicationStatus == 'Eligibility Received' && application.isSepApp==false && application.isQep == false && application.showEnrollButton =='Y'">
                <c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
                <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                    <p id="aid_yourEligibilityProg_01"><spring:message code="indportal.portalhome.youreligibilityforprograms1" javaScriptEscape="true"/></p>
                    <div ng-show="{{application.enableEnrollButton == 'true'}}">
                        <div class="col-xs-12 col-sm-7 col-md-7 col-lg-8 gutter0">
                            <p id="aid_yourEligibilityProg_02"><spring:message code="indportal.portalhome.youreligibilityforprograms2" htmlEscape="false" javaScriptEscape="true"/></p>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-5 col-lg-4 gutter0">
                            <a href="javascript:void(0)" ng-click="checkForAutoRenewal(null,null,null,$event,'enroll',application.coverageYear)" class="btn btn-primary btn-small btn_save pull-right" id="{{application.caseNumber}}"> <spring:message code="indportal.portalhome.finalizeenroll" javaScriptEscape="true"/></a>
                        </div>
                    </div>
                </div>
                </c:if>
            </div>
            <!-- Applicant event summary -->
            <div class="row-fluid" ng-if="(application.isSepApp==true || application.isQep == true ) && (application.applicationStatus == 'Eligibility Received' || application.applicationStatus == 'Enrolled (Or Active)'  || application.applicationStatus == 'Partially Enrolled')">
                <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                    <p id="aid_toViewYourChanges" data-autamation-id="toViewYourChanges"><spring:message code="indportal.portalhome.toviewyourchanges" javaScriptEscape="true"/>&nbsp;<a href="javascript:void(0)" ng-click="showEventSummary(application.caseNumber)" class="link"><spring:message code="indportal.portalhome.toviewyourapp2" javaScriptEscape="true"/></a>. </p>
                </div>
            </div>
        </sec:accesscontrollist>

	<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
	    <sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_VER_RESULTS" domainObject="user">
          <div class="row-fluid" ng-show="{{application.enableVerificationResultsButton=='true' && (application.applicationStatus == 'Eligibility Received' || application.applicationStatus == 'Submitted' || application.applicationStatus == 'Enrolled (Or Active)'  || application.applicationStatus == 'Partially Enrolled')}}">
            <div id="aid_toViewYourDoc" data-autamation-id="toViewYourDoc" class="alert alert-info gutter10 row margin10-tb currentAppBody">
             <p>To view your documents click&nbsp;<a href="javascript:void(0)" ng-click="showVerificationResults(application.caseNumber)" class="link">here</a>.</p>
            </div>
          </div>
	    </sec:accesscontrollist>
    </c:if>

    <!-- HIX-110583 - Removed  the below section for CA -->
    <c:if test="${stateCode != 'CA' && stateCode != 'NV'}">
	<sec:accesscontrollist hasPermission="IND_PORTAL_ENROLL_APP" domainObject="user">
        <div class="row-fluid" ng-show="{{application.applicationStatus == 'Eligibility Received' && (application.isSepApp==true || application.isQep == true)}} && ${daysforSEP>0}">
            <div class="alert alert-info gutter10 row margin10-tb currentAppBody">
                <div ng-show="{{application.enableEnrollButton == 'true'}}">
                    <div class="col-xs-12 col-sm-8 col-md-8 col-lg-9 gutter0">
                        <p id="aid_pendingevent" data-autamation-id="pendingevent"><spring:message code="indportal.portalhome.pendingevent" javaScriptEscape="true"/></p>
                        <p id="aid_recTribeMessage" data-autamation-id="recTribeMessage" ng-if="${isRecTribe=='Y'}">NOTE: <spring:message code="indportal.portalhome.recTribeMessage" javaScriptEscape="true"/></p>
                    </div>
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-3 gutter0">
                        <a id="aid_goToDash_03" data-autamation-id="goToDash" href="javascript:void(0)" ng-click="goDash()" class="btn btn-primary btn_save btn-small pull-right">Go to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
	</sec:accesscontrollist>
    </c:if>

<div class="row-fluid currentAppLinks">
    <sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_ELIG_RESULTS" domainObject="user">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin10-t" ng-show="{{application.applicationStatus == 'Eligibility Received' || application.applicationStatus == 'Enrolled (Or Active)'  || application.applicationStatus == 'Partially Enrolled'}}">
            <c:if test="${stateCode == 'CA'}">
            <a href="https://${calheersEnv}/static/lw-web/eligibility?householdCaseId=${householdCaseID}" id="aid_eligibilityResults_01" data-autamation-id="eligibilityResults" class="currentAppLink"><i class="icon-check"></i> <spring:message code="indportal.portalhome.eligibilityresults" javaScriptEscape="true"/></a>
            </c:if>
            <c:if test="${stateCode != 'CA'}">
            <a href="javascript:void(0)" ng-click="showEligibilityResults(application.caseNumber)" id="aid_eligibilityResults_02" data-autamation-id="eligibilityResults" class="currentAppLink"><i class="icon-check"></i> <spring:message code="indportal.portalhome.eligibilityresults" javaScriptEscape="true"/></a>
            </c:if>
        </div>
    </sec:accesscontrollist>
      <div ng-controller="indPortalAppCtrl">
    <sec:accesscontrollist hasPermission="IND_PORTAL_REPORT_CHANGES" domainObject="user">
	<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin10-t"  ng-if="!application.applicationInProgress && application.editable">
            <!--<a href="javascript:void(0)" ng-click="reportChange(application.caseNumber)" class=""> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></a>-->
          <!-- TODO: Refactor -->
            <a href="javascript:void(0)" ng-click="cloneEditApplication(application.caseNumber)"
               id="aid_editApplication" data-autamation-id="editApplication" class="currentAppLink">
                <i class="icon-edit">
                </i><spring:message code="indportal.portalhome.editcurrentapplication" htmlEscape="false" javaScriptEscape="true"/>
            </a>
        </div>
	</c:if>
	<c:if test="${stateCode == 'MN'}">
		<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin10-t"  ng-if="!application.applicationInProgress">
			<!--<a href="javascript:void(0)" ng-click="reportChange(application.caseNumber)" class=""> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></a>-->
			<!-- TODO: Refactor -->
				<a href="https://www.mnsure.org/current-customers/manage-account/report-change/private.jsp" target="_blank" id="aid_reportChange" data-autamation-id="reportChange" class="currentAppLink"><i class="icon-edit"></i><spring:message code="indportal.portalhome.reportchangeupper" htmlEscape="false" javaScriptEscape="true"/></a>
		</div>
	</c:if>
	</sec:accesscontrollist>
            <!-- Modal-->
              <div ng-show= "openDialoglce" id="openDialoglce">
                <div modal-show="openDialoglce" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="reportChangeModal" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 id="reportChangeModal"  data-autamation-id="reportChangeModal" style="font-size: 18px;border: none;"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
                      </div>
                      <div class="modal-body">
                      	<div>
                      		<p id="aid_makeChanges" data-autamation-id="makeChanges" class="margin10-l"><spring:message code="indportal.portalhome.ifyouwouldliketomakechanges" javaScriptEscape="true"/></p>
							<div class="alert alert-info">
                        		<p id="aid_lifeEventsFin" data-autamation-id="lifeEventsFin"><spring:message code="indportal.portalhome.moreInfoLifeEventsFin" htmlEscape="false"/></p>
							</div>
						</div>

                      </div>
                      <div class="modal-footer">
						<button id="aid_eligibilitySummary" data-autamation-id="eligibilitySummary" class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></button>
                        <button id="aid_contactUs" data-autamation-id="contactUs" class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
                      </div>
                    </div><!-- /.modal-content-->
                  </div> <!-- /.modal-dialog-->
                </div>
              </div>
           <!-- Modal-->
			<!-- Auto renewed LCE Modal-->
              <div ng-show= "openAutoRenewalLceDialog" id="openAutoRenewalLceDialog">
                <div modal-show="openAutoRenewalLceDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="autoRenewalReportChangeModal" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h3 id="autoRenewalReportChangeModal" style="font-size: 18px;border: none;"> <spring:message code="indportal.portalhome.reportachange" javaScriptEscape="true"/></h3>
                      </div>
                      <div class="modal-body">
                      	<div id="aid_autorenewalLCE">
							<spring:message code="indportal.portalhome.autorenewalLCE" arguments="${currentCoverageYear}" javaScriptEscape="true"/>
						</div>

                      </div>
                      <div class="modal-footer">
                        <button id="autoRenewalButton" data-autamation-id="autoRenewalButton" class="btn btn-primary pull-right" data-dismiss="modal"><spring:message code="indportal.contactus.okay" javaScriptEscape="true"/></button>
                      </div>
                    </div><!-- /.modal-content-->
                  </div> <!-- /.modal-dialog-->
                </div>
              </div>
           <!-- Auto renewed LCE Modal -->
			        </div>
		    <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
		        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin10-t" ng-if="application.enableCancelButton == 'true'">
		          <a href="javascript:void(0)"  ng-click="cancelApplicationAlert(application.caseNumber,application.coverageYear, application.applicationDentalStatus, application.applicationStatus)"   data-autamation-id="cancelApp" id="aid_cancelApp_01" class="currentAppLink" ng-show="{{application.enableCancelButton}}==true"><i class="icon-remove"></i> <spring:message code="indportal.portalhome.cancelapplication" javaScriptEscape="true"/></a>
		        </div>
			</c:if>
			</sec:accesscontrollist>
		    <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
			<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
		        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 margin10-t" ng-if="application.enableCancelOfEnrolledApp">
		          <a href="javascript:void(0)"  data-autamation-id="cancelApp" ng-click="cancelEnrolledApplication(application.caseNumber,application.coverageYear)" id="aid_cancelApp_02" class="currentAppLink" ng-show="{{application.enableCancelOfEnrolledApp}}==true"><i class="icon-remove"></i> <spring:message code="indportal.portalhome.cancelapplication" javaScriptEscape="true"/></a>
		        </div>
			</c:if>
			</sec:accesscontrollist>
      </div>
	</div>
	</div>

    </div>
    <!-- Past Applications Starts-->
<c:if test="${stateCode != 'MN'}">
    <div class="header row-fluid margin20-t" ng-if="pastApplications.length>0">
        <h4 class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
            <spring:message code="indportal.portalhome.pastapplications" javaScriptEscape="true"/>
        </h4>
        <span class="col-xs-12 col-sm-5 col-md-4 col-lg-4 margin10-t coverage-year_right coverage-year">
            <spring:message code="indportal.portalhome.applicationYear"/> {{maCurrentCoverageYear}}
        </span>
    </div>
<div class="well-step" id="pastAppStatus" ng-repeat="application in pastApplications">

	<div class="appsHeaders">
         <div aria-label="Shop" class="col-xs-12 col-sm-6 col-md-6 col-lg-8">
            {{application.coverageYear}} {{application.applicationName}}
			<div class="margin10-t" ng-if="application.isNonFinancial === false">
				<div class="aid_aptc">
				    <spring:message code="indportal.portalhome.aptc" htmlEscape="true"/>: {{application.aptc === null ? "Not Eligible" : (application.aptc | currency)}}
				</div>
				<div ng-if="application.stateSubsidy" class="aid_STATESUBSIDY">
				    <spring:message code="indportal.portalhome.StateSubsidy" htmlEscape="true"/>: {{application.stateSubsidy === null ? "Not Eligible" : (application.stateSubsidy | currency)}}
				</div>
				<div class="aid_csr">
				    <spring:message code="indportal.portalhome.csr" htmlEscape="true"/>: {{application.csr === null ? "Not Eligible" : application.csr}}
				</div>
			</div>
         </div>
        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4">
            <div data-autamation-id="appType" id="aid_appType">
                <spring:message code="indportal.portalhome.type" javaScriptEscape="true"/>: {{getAppType(application.appType)}}
            </div>
            <div data-autamation-id="casenumber" id="aid_casenumber">
                <c:if test="${stateCode != 'CA'}">
                    <spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>: {{application.caseNumber}}
                </c:if>
                <c:if test="${stateCode == 'CA'}">
                    <spring:message code="indportal.portalhome.casenumber" javaScriptEscape="true"/>: {{householdCaseNumber}}
                </c:if>
            </div>
            <div data-autamation-id="createdDate" id="aid_createdDate">
						 <spring:message code="indportal.portalhome.creationdate" javaScriptEscape="true"/>: {{application.createdDate}}
            </div>
            <div data-autamation="aid_lastUpdated" id="aid_lastUpdated">
						 <spring:message code="indportal.portalhome.lastupdated" javaScriptEscape="true"/>: {{application.lastUpdated}}
            </div>
        </div>
    </div>
    <div class="row-fluid pastAppsLinks">
		<div class="row-fluid">
		    <sec:accesscontrollist hasPermission="IND_PORTAL_VIEW_ELIG_RESULTS" domainObject="user">
				<c:if test="${stateCode != 'CA'}">
		          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 margin10-t">
		            <a href="javascript:void(0)" ng-click="showEligibilityResults(application.caseNumber)" ng-show="{{application.enableEligibilityResultsButton}}== true" id="aid_eligibilityresults_03" data-autamation-id="eligibilityresults" class="currentAppLink"><i class="icon-tasks"></i> <spring:message code="indportal.portalhome.eligibilityresults" javaScriptEscape="true"/></a>
		          </div>
			    </c:if>
				<c:if test="${stateCode == 'CA'}">
		          <div class="col-xs-12 col-sm-6 col-md-6 col-lg-8 margin10-t">
		            <a href="https://${calheersEnv}/static/lw-web/eligibility?householdCaseId=${householdCaseID}" id="aid_eligibilityresults_04" data-autamation-id="eligibilityresults" class="currentAppLink"><i class="icon-tasks"></i> <spring:message code="indportal.portalhome.eligibilityresults" javaScriptEscape="true"/></a>
		          </div>
			    </c:if>
			</sec:accesscontrollist>

		    <sec:accesscontrollist hasPermission="IND_PORTAL_START_APP" domainObject="user">
				<div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 margin10-t">
				  <a href="javascript:void(0)" ng-click="showApplication(application.caseNumber)" ng-show="{{application.applicationInProgress}}==false" id="aid_viewApplication" data-automation-id="viewApplication" class="currentAppLink aid_viewApplication"><i class="icon-list-alt"></i> <spring:message code="indportal.portalhome.viewapplication" javaScriptEscape="true"/></a>
				</div>
		    </sec:accesscontrollist>
        </div>
    </div>
</div>
</c:if>
  </div>
</div>

  <!-- Cancel Application Modal-->
  <div ng-show="modalForm.openDialog">
   <div modal-show="modalForm.openDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="cancelApplicationModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header" id="aid_cancelApplicationModal">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="cancelApplicationModal" data-automation-id="cancelApplicationModalHeader" ><spring:message code="indportal.portalhome.cancelapplication" htmlEscape="false" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
		  <p id="aid_selectedtocancel" data-autamation-id="selectedtocancel"><spring:message code="indportal.portalhome.youhaveselectedtocancel" javaScriptEscape="true"/></p>
          <p id="aid_cancelapplicationmodal" data-autamation-id="cancelapplicationmodal"><spring:message code="indportal.portalhome.cancelapplicationmodal" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <a href="javascript:void(0)" id="aid_nogoback" data-autamation-id="nogoback" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.nogoback" javaScriptEscape="true"/></a>
            <a href="javascript:void(0)" id="aid_yescancel" data-autamation-id="yescancel" class="btn btn-primary" ng-click="cancelApplication(caseNumber)" class=""><spring:message code="indportal.portalhome.yescancel" javaScriptEscape="true"/></a>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
  </div>
  <!-- Cancel Application Modal-->
	<!-- Pending application Dialog -->
			<div ng-show="pendingAppDialog">
			 <div modal-show="pendingAppDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="pendingAppModal" aria-hidden="true">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			          <h3 id="pendingAppModal" style="font-size: 18px;border: none;"><spring:message code="indportal.pendingAppModal.header" javaScriptEscape="true"/></h3>
			        </div>
			        <div class="modal-body">
						<spring:message code="indportal.pendingAppModalForCancel.body"/>
			        </div>
			        <div class="modal-footer">
			          <a href="javascript:void(0);" class="btn btn-secondary" data-automation-id="pendingAppDialogOk" ata-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
			        </div>
			      </div><!-- /.modal-content-->
			    </div> <!-- /.modal-dialog-->
			   </div>
			</div>


			<!-- Active enrollment Dialog -->
			<div ng-show="activeEnrollmentDialog">
			 <div modal-show="activeEnrollmentDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="activeEnrollmentModal" aria-hidden="true">
			    <div class="modal-dialog">
			      <div class="modal-content">
			        <div class="modal-header">
			          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			          <h3 id="activeEnrollmentModal" style="font-size: 18px;border: none;"><spring:message code="indportal.activeEnrollmentModal.header" javaScriptEscape="true"/></h3>
			        </div>
			        <div class="modal-body">
						<spring:message code="indportal.activeEnrollmentModal.body" javaScriptEscape="true"/>
			        </div>
			        <div class="modal-footer">
			          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
			        </div>
			      </div><!-- /.modal-content-->
			    </div> <!-- /.modal-dialog-->
			   </div>
			</div>
			<!-- Event Summary modal -->
			<div ng-show="eventSummaryDialog">
			 	<div modal-show="eventSummaryDialog" class="modal fade eventSummaryDialog" tabindex="-1" role="dialog" aria-labelledby="eventSummaryModal" aria-hidden="true">
			    	<div class="modal-dialog">
				      	<div class="modal-content">
					        <div class="modal-header">
					          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					          <h3 id="eventSummaryModal" style="font-size: 18px;border: none;"><spring:message code="indportal.eventSummaryModal.header" javaScriptEscape="true"/></h3>
					        </div>
					        <div class="modal-body">
								<table class="table table-bordered table-striped" ng-if ="applicantEvents.length > 0">
									<thead>
										<tr>
											<td><spring:message code="indportal.eventSummaryModal.name" javaScriptEscape="true"/></td>
											<td><spring:message code="indportal.eventSummaryModal.event" javaScriptEscape="true"/></td>
											<td><spring:message code="indportal.eventSummaryModal.eventDate" javaScriptEscape="true"/></td>
										</tr>
									</thead>
									 <tbody>
										<tr ng-repeat="event in applicantEvents"><td>{{event.applicantName}}</td><td>{{event.eventName}}</td><td>{{event.eventDate}}</td></tr>
									</tbody>
								</table>
                                <div ng-if ="applicantEvents.length == 0">
                                    <spring:message code="indportal.eventSummaryModal.empty" javaScriptEscape="true"/>
                                </div>
							</div>
					        <div class="modal-footer">
					          <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
					        </div>
						</div><!-- /.modal-content-->
			      </div><!-- /.modal-dialog-->
			    </div>
			</div>
<!-- Modal for failing init-->
<div ng-show= "openInitFail">
      <div modal-show="openInitFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="failedInitModal" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="failedInitModal"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
					</div>
					<div class="modal-body">
					<p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
					</div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal">Close</button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for failing init-->


  <!-- CSR Modal-->
  <div ng-show="modalForm.csr">
     <div modal-show="modalForm.csr" class="modal fade" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" ng-click="cancelCsrAction()">&times;</button>
              <h3 id="myModalLabel"  ng-if="currentCsr.header">
				<span>{{currentCsr.header}}</span>
			  </h3>
            </div>

		  <div ng-hide ="csrInputOverride || csrInputTermDate || SPEopened || ReinstateenrollmentData.showReinstateEnrollmentOptions || changeCovStart || modalForm.subResult || overrideNewSEPDate || activefailuremsg">
            <div class="modal-body">
				<p ng-class="{fadeFont: fadeFont}">{{currentCsr.content}}</p>
            </div>

            <div class="modal-body" >
                <span class="oRReason"><spring:message code="indportal.portalhome.viewoverridehistorycontent" javaScriptEscape="true"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>

                <textarea aria-label="change reason" class="overrideText" ng-model="modalForm.overrideComment" placeholder="Please explain the change you are making and why"></textarea>

                <div>
                  <a class="btn btn-secondary pull-right" data-dismiss="modal" ng-click="cancelCsrAction()" ng-disabled = "csrInputTermDate"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
                  <a class="btn btn-primary pull-right" ng-click="modalForm.overrideComment.length > 0 && submitOverrideComment(modalForm.overrideComment)" class="" ng-disabled = "csrInputTermDate || modalForm.overrideComment.length < 1"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
                  <span class="pull-left">{{maxLength - modalForm.overrideComment.length}} <spring:message code="indportal.portalhome.charactersleft" javaScriptEscape="true"/></span>
                </div>
            </div>

            </div>


	        <div ng-show="csrInputTermDate  && !modalForm.subResult">
	       	  <div class="modal-body">
	            <span><spring:message code="indportal.portalhome.csrtermdatecontent" javaScriptEscape="true"/></span>


	            <span class="termDate"><spring:message code="indportal.portalhome.csrentertermdate" javaScriptEscape="true"/></span>

	           	<input aria-label="cancel date" type="text" class="dateinput" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="modalForm.cancelDate" ng-blur="dateCheck($event, 'termDateErr')"  num-only></input>
							<span ng-show="termDateErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
							<div spinning-loader="loader"></div>
		      	</div>

	       	  <div class="modal-footer">
	            <a href="javascript:void(0)" class="btn btn-primary" ng-click="termDateErr || !modalForm.cancelDate || getSubsq()" class="" ng-disabled="termDateErr || !modalForm.cancelDate"><spring:message code="indportal.portalhome.disenroll" javaScriptEscape="true"/></a>
	          	<a href="javascript:void(0)" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>

	          </div>
	        </div>

	        <div ng-if="SPEopened && !modalForm.subResult">
	          <div class="modal-body">
						 <form name="modalForm.sepDates">
		         <span class="termDate"><spring:message code="indportal.portalhome.csrenterspestart" javaScriptEscape="true"/></span>

		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="start" ui-mask="99/99/9999" ui-mask-use-viewvalue="true"  ng-model="SEPData.sepStartDate" ng-disabled="!SEPData.enableEditing" ng-blur="dateCheck($event, 'SEPStartErr')" num-only></input>
						 <span ng-show="SEPStartErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>


		         <span class="termDate"><spring:message code="indportal.portalhome.csrenterspeend" javaScriptEscape="true"/></span>

		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="end" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="SEPData.sepEndDate" ng-disabled="!SEPData.enableEditing" ng-blur="dateCheck($event, 'SEPEndErr')" num-only></input>
						<span ng-show="SEPEndErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
							</form>
							<div ng-show="!SEPData.enableEditing" >
									<spring:message code="indportal.portalhome.csrspedisabled" javaScriptEscape="true"/>
							</div>
						<div spinning-loader="loader"></div>
						</div>

		        <div class="modal-footer">
							<a class="btn btn-primary" ng-click="SEPStartErr || SEPEndErr || !SEPData.sepStartDate || !SEPData.sepEndDate || getSubsq()" ng-show="SEPData.enableEditing" ng-disabled="SEPStartErr || SEPEndErr || !SEPData.sepStartDate || !SEPData.sepEndDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          </div>

	       </div>


	        <div ng-if="changeCovStart && !modalForm.subResult">
	          <div class="modal-body">
						 <form name="modalForm.sepDates">
		         <span class="termDate"><spring:message code="indportal.portalhome.csrentercovstart" javaScriptEscape="true"/></span>

		         <input type="text" class="dateinput" placeholder="mm/dd/yyyy" name="cov" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="SEPData.covStartDate" ng-blur="dateCheck($event, 'SEPCovErr')" num-only></input>
						 <span ng-show="SEPCovErr" class="popover-content ng-binding zip-warning redBorder"><spring:message code="indportal.portalhome.csrspedateerror" javaScriptEscape="true"/></span>
						 </form>
						<div spinning-loader="loader"></div>
						</div>

		        <div class="modal-footer">
	            <a class="btn btn-primary" ng-click="SEPCovErr || !SEPData.covStartDate || getSubsq($event)" ng-disabled="SEPCovErr || !SEPData.covStartDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            <a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          </div>

	       </div>

	        <div ng-if="ReinstateenrollmentData.showReinstateEnrollmentOptions && !modalForm.subResult">
	          	 <div class="modal-body">
					<div ng-if="ReinstateenrollmentData.dentalEnrollmentId || ReinstateenrollmentData.healthEnrollmentId">
						<span><spring:message code="indportal.portalhome.reinstateenrollmentMessage" javaScriptEscape="true"/></span>
				 		<form name="modalForm.reinstateEnrollmentOptions">
				 			<div>
								<input type="hidden" name="healthEnrollmentId" id="healthEnrollmentId" value="{{ReinstateenrollmentData.healthEnrollmentId}}" ng-model="ReinstateenrollmentData.healthEnrollmentId">
								<label class="checkbox" for="reinstateHealth">
									<input type="checkbox" name="reinstateHealth" id="reinstateHealth" ng-model="ReinstateenrollmentData.reinstateHealth" ng-disabled="!ReinstateenrollmentData.healthEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateHealth" javaScriptEscape="true"/>
								</label>
      						</div>
      						<div>
								<input type="hidden" name="dentalEnrollmentId" id="dentalEnrollmentId" value="{{ReinstateenrollmentData.dentalEnrollmentId}}" ng-model="ReinstateenrollmentData.dentalEnrollmentId">
      							<label class="checkbox" for="reinstateDental">
									<input type="checkbox" name="reinstateDental" id="reinstateDental" ng-model="ReinstateenrollmentData.reinstateDental" ng-disabled="!ReinstateenrollmentData.dentalEnrollmentId">
									<spring:message code="indportal.portalhome.reinstateDental" javaScriptEscape="true"/>
								</label>
      						</div>
				 		</form>
					</div>
					<div ng-if="ReinstateenrollmentData.noenrollmentsPresent">
						<spring:message code="indportal.portalhome.noenrollmentsMessage" javaScriptEscape="true"/></span>
					</div>
					<div spinning-loader="loader"></div>
				</div>

		        <div class="modal-footer">
					<a class="btn btn-primary" ng-click="getSubsq()" ng-show="!ReinstateenrollmentData.noenrollmentsPresent" ng-disabled="!ReinstateenrollmentData.reinstateDental && !ReinstateenrollmentData.reinstateHealth"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	            	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>
	          	</div>
	       </div>

			<div ng-show="overrideNewSEPDate  && !modalForm.subResult">
	       	  <div class="modal-body">

	            <span class="termDate"><spring:message code="indportal.portalhome.overridesepdenialdate" javaScriptEscape="true"/></span>


				<input type="hidden" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD)%>" id="sepDenialGracePeriod" name="sepDenialGracePeriod">
				<div class="input-append date newSEPEndDate"  data-date="">
	           	<input aria-label="end date" type="text" name="newSEPEndDate" id="aid_newSEPEndDate" class="dateinput newSEPEndDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true" ng-model="modalForm.newSEPEndDate" ng-blur="validateSepEndDate($event, 'newSEPEndDateErr')"  num-only></input>
				<span class="add-on dateSpan js-dateTrigger"><i class="icon-calendar"></i></span>
				</div>
				<span ng-show="newSEPEndDateErr" class="popover-content ng-binding zip-warning redBorder">{{newSEPEndDateErrMsg}}</span>
							<div spinning-loader="loader"></div>
		      	</div>

	       	  <div class="modal-footer">
	            <a href="javascript:void(0)" id="aid_newSEPEndDateErr" class="btn btn-primary" ng-click="newSEPEndDateErr || !modalForm.newSEPEndDate || getSubsq()" class="" ng-disabled="newSEPEndDateErr || !modalForm.newSEPEndDate"><spring:message code="indportal.portalhome.continue" javaScriptEscape="true"/></a>
	          	<a href="javascript:void(0)" id="aid_newSEPEndDateCancel" class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.cancel" javaScriptEscape="true"/></a>

	          </div>
	        </div>
			<div ng-if="activefailuremsg  && !modalForm.subResult">
	       	  <div class="modal-body">

				<p><spring:message code="indportal.portalhome.overridesepdenialactivemsg" javaScriptEscape="true"/></p>
				</div>

	       	  <div class="modal-footer">
				<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.communicationPreferences.close" javaScriptEscape="true"/></a>
	          </div>
	        </div>

					<div ng-if = "modalForm.subResult == 'success'">
					  <div class="modal-body">
			    		<h3 ng-if="currentCsr.title!== 'Initiate Verifications'"><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
			        <h3 ng-if="currentCsr.title=== 'Initiate Verifications'"><spring:message code="indportal.portalhome.csrprocesssubmitted" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
			    	  </div>

			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></a>
			          </div>
					</div>

					<div ng-if = "modalForm.subResult == 'failure'">
					  <div class="modal-body">
			    		<h3> <spring:message code="indportal.portalhome.csrsubmissionfailtitle" javaScriptEscape="true"/></h3>
			    		<p> <spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
						<p>	<spring:message code="indportal.portalhome.csrsubmissionfailcontent2" javaScriptEscape="true"/></p>
			    	  </div>

			       	  <div class="modal-footer">
			          	<a class="btn btn-secondary" data-dismiss="modal" ng-click="cancelCsrAction()">OK</a>
			          </div>
					</div>

        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
    </div>
  </div>
  <!-- CSR Modal-->

	<!-- Eligibility Result modal -->
	<div ng-show="eligResultDialog">
	<div modal-show="eligResultDialog" id="aid_eligResultDialog" class="modal fade eligResultDialog" tabindex="-1" role="dialog" aria-labelledby="aid_eligResultDialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			    <div class="modal-header">
			        <button type="button" id="aid_eligResultDialogClose" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    </div>
			    <div class="modal-body">
					<spring:message code="indportal.eligibilitysummary.noresultsfound" javaScriptEscape="true"/>
				</div>
			</div>
			<div class="modal-footer">
			    <a href="javascript:void(0);" class="btn btn-secondary" data-dismiss="modal"><spring:message code="indportal.eligibilitysummary.close" javaScriptEscape="true"/></a>
			</div>
		</div><!-- /.modal-content-->
	</div> <!-- /.modal-dialog-->
	</div>

		<!-- Modal for Override History-->
		<div ng-show= "modalForm.ORHist">
	    <div modal-show="modalForm.ORHist" class="modal fade">
		    <div class="modal-dialog">
		      <div class="modal-content">
						<div class="modal-header">
						  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						  <h3 id="myModalLabel"><spring:message code="indportal.portalhome.overridehistory" javaScriptEscape="true"/></h3>
						</div>
						<div class="modal-body">

              <div ng-if="modalForm.oComments.length === 0">
                <spring:message code="indportal.portalhome.nohistory" javaScriptEscape="true"/>
              </div>

							<div class="well-step gutter10 nomargin" ng-repeat= "record in modalForm.oComments | orderBy:'commentedDate':true">
		          	<div>
		          	  <span class="commentdetail"><strong>{{record.commenterName}}</strong> <spring:message code="indportal.portalhome.addedcomment" javaScriptEscape="true"/>  <i>{{record.commentedDate | date:'medium'}}</i></span>

		       				<span>{{record.overrideComment}}</span>
								</div>
          		</div>
          </div>

			    	<div class="modal-footer">
			      	<button class="btn pull-left" data-dismiss="modal">Close</button>
			      </div>
		    	</div><!-- /.modal-content-->
		    </div> <!-- /.modal-dialog-->
	  	</div>
		</div>
		<!-- Modal for Override History-->
    </div>
  </div>
</div>


<div id="overrideSepModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="overrideSepModalHeader" aria-hidden="true">
  <div class="modal-header" id="overrideSepModalHeader">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3 id="myModalLabel">Override Special Enrollment Details</h3>
  </div>

  <form name="sepEventDetailsForm" id="aid_sepEventDetailsForm" class="form-horizontal" ng-show="isSepEventDetailsForm === true" novalidate>
    <div class="modal-body">
      <p class="alert alert-info" id="aid_sepEventDetailsFormAlert">
		Please identify the events that have impacted the household in the last 60 days. Some of the events reported may require additional documentation. For more information on special enrollment periods please click <a class="" target="_blank" href="https://www.yourhealthidaho.org/special-enrollment/">here</a>.
		<br>Clicking continue will calculate the special enrollment deadline based on the event dates provided.
	  </p>

		<div class="row-fluid header">
			<h4>
				<span class="span3">Members</span>
				<span class="span5">Event Type <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
				<span class="span4">Event Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></span>
			</h4>
		</div>

		<div ng-repeat="applicant in sepDetails.applicants | orderBy: 'personId'">
			<div class="row-fluid gutter10-b" ng-repeat="event in applicant.events track by $index" ng-form="eventForm" ng-if="sepDetails.applicationType === 'SEP' || (sepDetails.applicationType === 'QEP' && applicant.personId === 1)">
				<div class="span3 gutter10-l">{{applicant.firstName}} {{applicant.middleName}} {{applicant.lastName}} <span ng-if="applicant.personId === 1">(Primary)</span></div>
				<div class="span5">
					<select class="input-xlarge" id="aid_sepEventDetailsEvent" name="eventType" ng-model="event.eventId" required ng-change="validateEventAndDate(eventForm, event, applicant)" ng-options='event.eventId as event.eventNameLabel for event in event.eventList'></select>
				</div>
				<div class="span4 input-append date" date-picker>
					<input type="text" name="eventDate" id="aid_sepEventDetailsEventDate" class="input-small eventDate" placeholder="mm/dd/yyyy" ng-model="event.eventDate" ui-mask="99/99/9999" model-view-value="true" required sep-date-validation="date" ng-change="eventDateValidation(eventForm, event, applicant)">
					<span class="add-on"><i class="icon-calendar"></i></span>
				</div>
				<div ng-if="eventForm.$invalid">
					<div class="span3"></div>
					<div class="span9 sep-override-error-block">
						<div ng-if="!eventForm.eventDate.$error.required">
							<div id="aid_sepEventDetailsDateFormat" class="error-message block" ng-if="eventForm.eventDate.$error.date">
								Please enter a valid event date.
							</div>

							<div id="aid_sepEventDetailsDateSameYear"class="error-message block" ng-if="!eventForm.eventDate.$error.date && eventForm.eventDate.$error.sameYear">
								Please enter a valid event date based on the application year.
							</div>

							<div ng-if="!eventForm.eventDate.$error.date">
								<div id="aid_sepEventDetailsDatePastDays" class="error-message block" ng-if="eventForm.eventDate.$error.withinPast60Days">
									Event date should be within 60 days in the past.
								</div>
								<div id="aid_sepEventDetailsDatewithinFuture60Days" class="error-message block" ng-if="eventForm.eventDate.$error.withinFuture60Days">
									Event date should be within 60 days in the future.
								</div>
							</div>
						</div>
						<div class="error-message block" ng-if="eventForm.eventDate.$error.required" >
							Please enter an event date.
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="error-message block margin10-t" ng-if="sameEventIsSelected">
			Member cannot report same event multiple times.
		</div>

    </div>
    <div class="modal-footer">
	     <button class="btn btn-primary" ng-click="getSepDateDetails()" ng-if="isSepEventDetailsForm === true" ng-disabled="sepEventDetailsForm.$invalid || sameEventIsSelected">Continue</button>
       <button class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
     </div>
  </form>

  <form name="sepDateDetailsForm" id="aid_sepDateDetailsForm" class="form-horizontal" ng-show="isSepDateDetailsForm === true" novalidate>
    <div class="modal-body">
	  <p id="aid_sepDateDetailsFormAlert" class="alert alert-info">
		Please confirm dates for the Special Enrollment Period.
	  </p>
      <div class="header margin20-tb">
        <h4 id="aid_sepDateDetailsFormHeader">SEP Enrollment Period</h4>
      </div>
      	<div class="control-group gutter10-lr">
			<div class="row-fluid">
	       	 <label class="span6">
    	    	  Start Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
        	 	 <span class="input-append date" date-picker>
            		<input type="text" id="aid_sepDateDetailsFormDate" class="input-small" name="sepStartDate" ng-model="sepDetails.sepStartDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" model-view-value="true" sep-date-validation="date" required ng-change="validateStartBeforeEnd()">
	           	 <span class="add-on"><i class="icon-calendar"></i></span>
    	     	 </span>
        		</label>

	      	  <label class="span6">
    	  	    End Date <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
        		  <span class="input-append date" date-picker>
           	 	<input type="text" id="aid_sepDateDetailsFormEndDate" class="input-small" name="sepEndDate" ng-model="sepDetails.sepEndDate" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" model-view-value="true" sep-date-validation="date_future" required ng-change="validateStartBeforeEnd()">
  	        	  <span class="add-on"><i class="icon-calendar"></i></span>
   	      	 </span>
     	   </label>
	</div>

		<div id="aid_sepDateDetailsFormFormatError" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepStartDate.$error.required">
				Please enter a special enrollment start date.
		</div>
		<div id="aid_sepDateDetailsFormStartDate" class="error-message block margin10-t" ng-if="!sepDateDetailsForm.sepStartDate.$error.required && sepDateDetailsForm.sepStartDate.$error.date">
				Please enter a valid special enrollment start date.
		</div>
		<div id="aid_sepDateDetailsFormEndDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.required">
				Please enter a special enrollment end date.
		</div>
		<div ng-if="!sepDateDetailsForm.sepEndDate.$error.required">
			<div id="aid_sepDateDetailsFormNoEndDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.date">
					Please enter a valid special enrollment end date.
			</div>
			<div id="aid_sepDateDetailsFormFutureDate" class="error-message block margin10-t" ng-if="sepDateDetailsForm.sepEndDate.$error.future">
					Special enrollment end date is in the past. Please enter a future date.
			</div>
		</div>
		<div id="aid_sepDateDetailsFormSEPEndDate" class="error-message block margin10-t" ng-if="!sepDateDetailsForm.sepStartDate.$error.date && !sepDateDetailsForm.sepEndDate.$error.date && sepDateDetailsForm.sepEndDate.$error.startBeforeEnd">
				Special enrollment start date cannot be after special enrollment end date.
		</div>
      </div>


      <div class="margin50-t">
        <div class="header margin10-b">
          <h4 id="aid_sepOverrideReasonHeader">Override Reason</h4>
        </div>

        <div class="gutter10-lr">
          <p>
            Please specify the reason for overriding the special enrollment for this household.
            <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
          </p>

          <div class="control-group">
            <textarea aria-label="change reason" id="aid_sepOverrideReason" class="override-reason" ng-model="sepDetails.overrideCommentText" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
            <span>{{4000 - sepDetails.overrideCommentText.length}} characters left</span>
          </div>
        </div>
      </div>
    </div>
    <div class="modal-footer">
    	<button id="aid_backToSepDetails" class="btn pull-left" ng-click="backToSepDetails()">Back</button>
      <button id="aid_saveSepOverride" class="btn btn-primary" data-dismiss="modal" ng-click="saveSepOverride()" ng-disabled="sepDateDetailsForm.$invalid">Save</button>
      <button id="aid_SepOverrideCancel" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
    </div>
  </form>
</div>


<div id="overrideStartDateModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="overrideStartDateModalLabel" aria-hidden="true">
  <div class="modal-header">
		<button type="button" id="aid_enrollmentEndDate" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="overrideStartDateModalLabel">
			<spring:message code="indportal.portalhome.overrideEligibilityStartDate" javaScriptEscape="true"/>
		</h3>
	</div>
	<form class="form-horizontal" name="overrideEligibilityStartDateForm">
		<div class="modal-body">
			<p>The consumer is enrolled with the following dates:</p>

			<div class="row-fluid">
				<span id="aid_nrollmentStartDate" class="span5 txt-right">Enrollment Begin Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.enrollmentStartDate}}</strong>
			</div>
			<div class="row-fluid">
				<span id="aid_enrollmentEndDate" class="span5 txt-right">Enrollment End Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.enrollmentEndDate}}</strong>
			</div>

			<div ng-if="overrideEligibilityStartDateData.status === 'OVERRIDDEN' || overrideEligibilityStartDateData.status === 'REQUIRED'">
			<div class="control-group margin10-t">
				<label id="aid_eligibilityStartDateLbl" class="control-label">Eligibility Start Date</label>
				<div class="controls">
					<input type="text" id="aid_eligibilityStartDate" class="input-small" placeholder="mm/dd/yyyy" name="eligibilityStartDate" ui-mask="99/99/9999" required sep-date-validation="date_sameCoverageYear_eligibilityStartDate_firstDate" model-view-value="true" ng-model="overrideEligibilityStartDateData.financialEffectiveDate">
				</div>
				<div ng-if="!overrideEligibilityStartDateForm.eligibilityStartDate.$error.required">
					<div id="aid_eligibilityStartDateFormat" class="error-message margin50-l" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.date">
						Please enter a valid eligibility start date.
					</div>
					<div id="aid_eligibilityStartDatefirstDate" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.firstDate">
						Eligibility start date should be the 1st day of the month.
					</div>
					<div id="aid_sameCoverageYear" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.sameCoverageYear">
						The year of eligibility start date should be the same as coverage year.
					</div>
					<div id="aid_eligibilityStartDateEndDate" class="error-message" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.eligibilityStartDate">
						Eligibility start date should not be greater than enrollment end date.
					</div>
				</div>
				<div id="aid_eligibilityStartDateEmpty" class="error-message margin50-l" ng-if="overrideEligibilityStartDateForm.eligibilityStartDate.$error.required">
					Please enter an eligibility start date.
				</div>
			</div>

			<div>
          		<p class="margin30-t">
            		Please specify the reason for overriding the APTC eligibility start date for this household.
           		 	<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/>
          		</p>

				<div class="control-group">
            		<textarea class="override-reason" ng-model="overrideEligibilityStartDateData.comments" placeholder="Please explain the change you are making and why" maxlength="4000" required></textarea>
            		<span>{{4000 - overrideEligibilityStartDateData.comments.length}} characters left</span>
          		</div>
			</div>

			<p class="alert alert-danger">
					<spring:message code="indportal.portalhome.esdBeforeEnmtBeginDateNote" htmlEscape="false"/>
			</p>
			</div>



		    <div class="row-fluid margin10-t" ng-if="overrideEligibilityStartDateData.status === 'NOTREQUIRED' || overrideEligibilityStartDateData.status === 'VALID'">
				<span class="span5 txt-right">Eligibility Start Date: </span> <strong class="span7">{{overrideEligibilityStartDateData.financialEffectiveDate}}</strong>
			</div>

		</div>
		<div class="modal-footer">
			<button class="btn btn-primary" data-dismiss="modal" ng-disabled="overrideEligibilityStartDateForm.$invalid" ng-if="overrideEligibilityStartDateData.status === 'OVERRIDDEN' || overrideEligibilityStartDateData.status === 'REQUIRED'" ng-click="saveOverrideEligibilityDate()">Approve</button>
			<button class="btn" data-dismiss="modal" ng-click="cancelCsrAction()">Cancel</button>
		</div>
	</form>
</div>


<div id="errorModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="errorModalLabel" aria-hidden="true">
  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="errorModalLabel">
			<spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/>
		</h3>
	</div>
	<div class="modal-body">
    <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
  </div>
</div>


<div id="successModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="successModal" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="successModalLabel" ng-if="successModalTitle">
			{{successModalTitle}}
		</h3>
	</div>
	<div class="modal-body">
    	<h3><spring:message code="indportal.portalhome.csrsubmissionsucceed" javaScriptEscape="true"/></h3>
		<p><spring:message code="indportal.portalhome.csroktogoback" javaScriptEscape="true"/></p>
  	</div>
  	<div class="modal-footer">
    	<button class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="indportal.portalhome.ok" javaScriptEscape="true"/></button>
  	</div>
</div>

</div>

</script>
