package com.getinsured.eligibility.ssap.integration.household.service;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value = "residencyRuleRequestServiceActivator")
public class ResidencyRuleRequestServiceActivator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ResidencyRuleRequestServiceActivator.class);

    @ServiceActivator
    public Message<Map<String, String>> receive(Message<String> message) {
        String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.FILTERED_SSAP_JSON_HEADER).toString();
        Map<String, String> residencyRulePayload = new HashMap<String, String>();
        residencyRulePayload.put("ssapJson", ssapJSON);
        Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(residencyRulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
        LOGGER.debug(">>>>>>>>>>>>>>>>>> Residency Request: " + ssapJSON);
        return ruleMessage;
    }
}
