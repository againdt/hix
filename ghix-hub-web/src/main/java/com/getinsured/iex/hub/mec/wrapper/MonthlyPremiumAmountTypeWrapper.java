package com.getinsured.iex.hub.mec.wrapper;

import java.math.BigDecimal;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.MonthlyPremiumAmountType;
import com.getinsured.iex.hub.mec.cms.hix._0_1.hix_ee.InsurancePremiumType;
import com.getinsured.iex.hub.mec.niem.niem.niem_core._2.AmountType;

public class MonthlyPremiumAmountTypeWrapper implements JSONAware{

	private BigDecimal employeePremiumAmount;
	private BigDecimal familyPremiumAmount;

	public MonthlyPremiumAmountTypeWrapper(MonthlyPremiumAmountType monthlyPremiumAmount)
	{
		if(monthlyPremiumAmount != null){
			InsurancePremiumType insurancePremiumType = monthlyPremiumAmount.getEmployeePremiumAmount();
			if(insurancePremiumType != null){
				AmountType amountType = insurancePremiumType.getInsurancePremiumAmount();
				if(amountType != null){
					employeePremiumAmount = amountType.getValue();
				}
			}
			
			insurancePremiumType = monthlyPremiumAmount.getFamilyPremiumAmount();
			if(insurancePremiumType != null){
				AmountType amountType = insurancePremiumType.getInsurancePremiumAmount();
				if(amountType != null){
					familyPremiumAmount = amountType.getValue();
				}
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("EmployeePremiumAmount", this.employeePremiumAmount);
		obj.put("FamilyPremiumAmount", this.familyPremiumAmount);
		return obj.toJSONString();
	}
}
