package com.getinsured.hix.batch.externalnotices.service;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.externalnotices.task.ExternalNoticesFailureNotification;
import com.getinsured.hix.dto.externalnotices.ExternalNoticesCsvDTO;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.Notification;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.timeshift.util.TSDate;

@Service("externalNoticesFailureReportService")
public class ExternalNoticesFailureReportServiceImpl implements ExternalNoticesFailureReportService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalNoticesFailureReportServiceImpl.class);

	@Autowired 
	private ExternalNoticesFailureNotification externalNoticesFailureNotification;

	@Override
	public void processFailureRecords(List<ExternalNotice> externalNotices) {

		LOGGER.info(" Inside ExternalNoticesFailureReportServiceImpl");
		List<ExternalNoticesCsvDTO> externalNoticesCsvDTOList = null;
		StringBuilder fileNameBuilder = EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(
				EnrollmentConstants.EXTERNAL_FAILURE_NOTICES, EnrollmentConstants.TRANSFER_DIRECTION_OUT);
		String outboundDir = fileNameBuilder.toString();

		if (externalNotices != null && !externalNotices.isEmpty()) {
			try {
				externalNoticesCsvDTOList = processExternalNoticesFailureReport(externalNotices);
				createDiscrepancyFileAndUpdateStatus(externalNoticesCsvDTOList, outboundDir);
			} catch (Exception e) {
				LOGGER.error("Could not create failure report for the external Notices :: ");
			}
		}
	}

	private void createDiscrepancyFileAndUpdateStatus(List<ExternalNoticesCsvDTO> externalNoticesCsvDTOList, String failureReportPath) {

		boolean isFileCreated = false;
		String noticesFailureReportFileName = null;
		String noticesReportPath = null;

		Writer writer = null;
		noticesReportPath = failureReportPath;
		Collections.sort(externalNoticesCsvDTOList, new Comparator<ExternalNoticesCsvDTO>() {
			public int compare(ExternalNoticesCsvDTO o1, ExternalNoticesCsvDTO o2) {
				if (o1.getNoticeId() != null && o2.getNoticeId() != null) {
					return o1.getNoticeId().compareTo(o2.getNoticeId());
				}
				return 0;
			}
		});

		try {
			EnrollmentUtils.createDirectory(noticesReportPath);
			noticesFailureReportFileName = "to_" + "EXTERNAL_NOTICES_FAILURE_REPORT" + "_"
					+ DateUtil.dateToString(new TSDate(), "yyyyMMddHHmmss") + ".OUT";
			writer = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(noticesReportPath + File.separatorChar + noticesFailureReportFileName),
					"utf-8"));
			writer.write(ExternalNoticesCsvDTO.HEADER + "\n");
			Iterator<ExternalNoticesCsvDTO> dtoIterator = externalNoticesCsvDTOList.iterator();
			while (dtoIterator.hasNext()) {
				ExternalNoticesCsvDTO dto = dtoIterator.next();
				if (dtoIterator.hasNext()) {
					writer.write(dto.toCsv(',') + "\n");
				} else {
					writer.write(dto.toCsv(','));
				}
			}
			isFileCreated = true;

		} catch (Exception e) {
			LOGGER.error("Exception in creating External Notices failure report :: " + e.getMessage(), e);
		} finally {
			if (writer != null) {
				try {
					writer.close();
				} catch (IOException e) {
					LOGGER.error("Exception closing writer in external notices failure report :: " + e.getMessage(), e);
				}
			}
		}
		if (isFileCreated) {
			prepareDetailMapAndSendNotification(noticesFailureReportFileName, noticesReportPath);
		} else {
			LOGGER.error("Error in creating a failure report for External Notices");
		}
	}

	private void prepareDetailMapAndSendNotification(String noticesFailureReportFileName, String noticesReportPath) {

		Map<String, String> emailDataMap = new HashMap<>();
		emailDataMap.put("File_Name", Paths.get(noticesFailureReportFileName).getFileName().toString());
		emailDataMap.put("File_Location", noticesReportPath);
		emailDataMap.put("Action", "Verify records in the failure file.");
		emailDataMap.put("Environment", GhixConstants.APP_URL);
		emailDataMap.put("Subject", "External Notices Job Failure Record : Records Failed");
		try {
			sendFailureNotificationEmail(emailDataMap);
		} catch (GIException e) {
			LOGGER.debug("Error sending External Notices failure report email :: ", e);
		}
	}

	private void sendFailureNotificationEmail(Map<String, String> emailDataMap) throws GIException {
		try {
			Map<String, Object> emailData = new HashMap<String, Object>();
			String recipientEmailAddress = DynamicPropertiesUtil
					.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.EMAIL_GRP_IRS_REPORTING);
			String fileLocation = emailDataMap.get("File_Location");
			String fileName = emailDataMap.get("File_Name");
			if (StringUtils.isEmpty(recipientEmailAddress)) {
				throw new GIException(
						"External Notices failure report email:: Null or Empty Email Receipient address refer application config 'enrollment.EmailGroupForIRSReporting'");
			}
			if (emailDataMap == null || (emailDataMap != null && emailDataMap.isEmpty())) {
				throw new GIException("External Notices failure report email:: Received null or empty RequestMap");
			}
			emailData.put("recipient", recipientEmailAddress);
			emailData.put("Subject", emailDataMap.get("Subject"));
			externalNoticesFailureNotification.setEmailData(emailData);
			externalNoticesFailureNotification.setRequestData(emailDataMap);
			Notice noticeObj = externalNoticesFailureNotification.generateEmail();
			noticeObj.setAttachment(fileLocation+"/"+fileName);
			Notification notificationObj = externalNoticesFailureNotification.generateNotification(noticeObj);
			externalNoticesFailureNotification.sendEmailToMultipleRecipient(notificationObj, noticeObj);
		} catch (Exception ex) {
			throw new GIException(ex);
		}
	}

	private List<ExternalNoticesCsvDTO> processExternalNoticesFailureReport(List<ExternalNotice> externalNoticesList) {
		List<ExternalNoticesCsvDTO> csvDtoList = new ArrayList<>();
		if (externalNoticesList != null && !externalNoticesList.isEmpty()) {
			for (ExternalNotice externalNotice : externalNoticesList) {
				ExternalNoticesCsvDTO dto = new ExternalNoticesCsvDTO();
				dto.setNoticeId(externalNotice.getNoticeId());
				dto.setExternalHouseholdCaseId(externalNotice.getExternalHouseholdCaseId());
				dto.setStatus(externalNotice.getStatusType().toString());
				csvDtoList.add(dto);
			}
		}
		return csvDtoList;
	}
}
