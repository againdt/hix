package com.getinsured.eligibility.at.sep.service;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.sep.repository.SepEventsRepository;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Financial;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Component(value = "sepEventsService")
@Transactional(readOnly = true)
public class SepEventsServiceImpl implements SepEventsService {

	@Autowired
	private SepEventsRepository sepEventsRepository;

	
	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#id}", unless = "#result == null")
	public SepEvents findSepEventById(Integer id) {
		SepEvents result = sepEventsRepository.findOne(id);

		if (result == null ) {
			throw new GIRuntimeException("No SEP Event found in SPECIAL_ENROLLMENT_EVENTS table for given id");
		}

		return result;

	}
	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName}", unless = "#result == null")
	public List<SepEvents> findFinancialSepEvents() {
		List<SepEvents> results = sepEventsRepository.findByFinancialOrderByLabelAsc(Financial.Y);

		if (results == null || results.size() == 0) {
			throw new GIRuntimeException("No financial data found in SPECIAL_ENROLLMENT_EVENTS table");
		}

		return results;

	}

	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#category}", unless = "#result == null")
	@Deprecated
	public Map<String, SepEvents> findFinancialSepEventsByCategory(String category) {
		if (StringUtils.isEmpty(category)) {
			return Collections.emptyMap();
		}

		return filterRecords(category.toUpperCase(), findFinancialSepEvents());

	}
	
	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#changeType}", unless = "#result == null")
	public Map<String, SepEvents> findFinancialSepEventsByChangeType(String changeType) {
		if (StringUtils.isEmpty(changeType)) {
			return Collections.emptyMap();
		}

		return filterRecordsByChangeType(changeType.toUpperCase(), findFinancialSepEvents());

	}

	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName}", unless = "#result == null")
	public List<SepEvents> findNonFinancialSepEvents() {
		List<SepEvents> results = sepEventsRepository.findByFinancialOrderByLabelAsc(Financial.N);

		if (results == null || results.size() == 0) {
			throw new GIRuntimeException("No non-financial data found in SPECIAL_ENROLLMENT_EVENTS table");
		}

		return results;
	}
	
	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#name,#category}", unless = "#result == null")
	public SepEvents findFinancialSepEventsByEventNameAndCategory(String name,String category) {
		SepEvents results = sepEventsRepository.findByEventNameAndFinancial(name, category,Financial.Y);

		if (results == null ) {
			throw new GIRuntimeException("No financial data found in SPECIAL_ENROLLMENT_EVENTS table for given input");
		}

		return results;
	}

	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#category}", unless = "#result == null")
	@Deprecated
	public Map<String, SepEvents> findNonFinancialSepEventsByCategory(String category) {
		if (StringUtils.isEmpty(category)) {
			return Collections.emptyMap();
		}

		return filterRecords(category.toUpperCase(), findNonFinancialSepEvents());
	}
	
	@Override
	@Cacheable(value = "sepevents", key = "{#root.methodName,#changeType}", unless = "#result == null")
	public Map<String, SepEvents> findNonFinancialSepEventsByChangeType(String changeType) {
		if (StringUtils.isEmpty(changeType)) {
			return Collections.emptyMap();
		}

		return filterRecordsByChangeType(changeType.toUpperCase(), findNonFinancialSepEvents());
	}

	private Map<String, SepEvents> filterRecords(final String category, List<SepEvents> allFinRecords) {
		Map<String, SepEvents> categoryResults = new LinkedHashMap<String, SepEvents>();
		for (SepEvents sepEvent : allFinRecords) {
			if (category.equals(sepEvent.getCategory())) {
				categoryResults.put(sepEvent.getName(), sepEvent);
			}
		}
		return categoryResults;
	}

	private Map<String, SepEvents> filterRecordsByChangeType(final String changeType, List<SepEvents> allFinRecords) {
		Map<String, SepEvents> changeTypeResults = new LinkedHashMap<String, SepEvents>();
		for (SepEvents sepEvent : allFinRecords) {
			if (changeType.equals(sepEvent.getChangeType())) {
				changeTypeResults.put(sepEvent.getName(), sepEvent);
			}
		}
		return changeTypeResults;
	}

}
