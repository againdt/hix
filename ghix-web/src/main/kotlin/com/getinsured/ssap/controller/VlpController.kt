package com.getinsured.ssap.controller

import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository
import com.getinsured.hix.ssap.SsapConstants
import com.getinsured.iex.ssap.CitizenshipImmigrationStatus
import com.getinsured.iex.ssap.HouseholdMember
import com.getinsured.iex.ssap.SingleStreamlinedApplication
import com.getinsured.iex.ssap.model.SsapApplicant
import com.getinsured.iex.ssap.model.SsapApplication
import com.getinsured.ssap.model.hub.vlp.VlpIntegrationLog
import com.getinsured.ssap.repository.VlpIntegrationLogRepository
import com.getinsured.ssap.service.HubIntegration
import com.getinsured.ssap.util.JsonUtil
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.web.bind.annotation.*

/**
 * Provides methods to resubmit more details for VLP verifications.
 *
 * @author Yevgen Golubenko
 * @since 6/9/19
 */
@RestController
open class VlpController @Autowired constructor(
  private val vlpIntegrationLogRepository: VlpIntegrationLogRepository,
  private val ssapApplicationRepository: SsapApplicationRepository,
  private val ssapApplicantRepository: SsapApplicantRepository,
  private val hubIntegration: HubIntegration
) {
  companion object {
    val log: Logger = LoggerFactory.getLogger(VlpController::class.java)
  }

  @PreAuthorize(SsapConstants.SSAP_VIEW)
  @RequestMapping(value = ["/newssap/vlp/citizenshipImmigrationStatus/{vlpIntegrationLogId}"], method = [RequestMethod.GET])
  open fun getHouseholdMemberByVlpIntegrationLogId(@PathVariable vlpIntegrationLogId: Long): ResponseEntity<Any> {
    log.info("Returning citizenship and immigration status/document by given VLP Integration Log id: {}", vlpIntegrationLogId)

    val vlpIntegrationLog: VlpIntegrationLog = vlpIntegrationLogRepository.findOne(vlpIntegrationLogId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Fetching SsapApplication record by given ssap id: {}", vlpIntegrationLog.ssapApplicantId)
    val ssapApplication: SsapApplication = ssapApplicationRepository.getOne(vlpIntegrationLog.ssapApplicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Fetching Applicants by given ssap id: {}", vlpIntegrationLog.ssapApplicantId)
    val ssapApplicants: List<SsapApplicant> = ssapApplicantRepository.findBySsapApplicationId(vlpIntegrationLog.ssapApplicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Finding Applicant for given vlp integration id: {}", vlpIntegrationLog.ssapApplicantId)
    val ssapApplicant: SsapApplicant = ssapApplicants.find { applicant -> applicant.id == vlpIntegrationLog.ssapApplicantId }
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Parsing SSAP Json")
    val singleStreamlinedApplication: SingleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplication)

    log.info("Finding household member by applicant guid: {}", ssapApplicant.applicantGuid)
    val householdMember: HouseholdMember =
      singleStreamlinedApplication.taxHousehold[0].householdMember.find { hhm -> hhm.applicantGuid == ssapApplicant.applicantGuid }
        ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Getting CitizenshipImmigrationStatus document from household member")
    val citizenshipImmigrationStatus: CitizenshipImmigrationStatus = householdMember.citizenshipImmigrationStatus

    log.info("Returning CitizenshipImmigrationStatus document: {}", citizenshipImmigrationStatus)
    return ResponseEntity.ok(citizenshipImmigrationStatus);
  }

  @RequestMapping(value = ["/newssap/configuration/affordability/{coverageYear}"], method = [RequestMethod.GET])
  open fun getRatesConfiguration(@PathVariable coverageYear: Int): ResponseEntity<Map<String, Any>>? {
    log.info("Returning rates configurations for coverageYear: {}", coverageYear)
    return when (coverageYear) {
      2020 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.78))
      2019 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.86))
      2018 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.56))
      2017 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.69))
      2016 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.66))
      2015 -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.56))
      else -> ResponseEntity.ok(mapOf("year" to coverageYear, "percent" to 9.86))
    }
  }

  @PreAuthorize(SsapConstants.SSAP_EDIT)
  @RequestMapping(value = ["/newssap/application/vlp/update/{applicationId}/{applicantId}"], method = [RequestMethod.POST])
  open fun updateApplicationDetails(@PathVariable applicationId: Long,
                               @PathVariable applicantId: Long,
                               @RequestBody citizenshipImmigrationStatus: CitizenshipImmigrationStatus): ResponseEntity<HashMap<String, Any>> {
    log.info("Updating SSAP VLP details for application id: {} and applicant id: {}", applicationId, applicantId)

    var vlpIntegrationLog: VlpIntegrationLog? = vlpIntegrationLogRepository.findBySsapApplicationIdAndSsapApplicantId(applicationId, applicantId)

    val response: HashMap<String, Any> = HashMap()
    if (vlpIntegrationLog == null) {
      response["code"] = HttpStatus.NOT_FOUND
      response["message"] = "VLP Integration Log not found for given ssap id and applicant id."
      return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response)
    }
    // TODO: Temporarily enable for debugging SEVISID
//    if(citizenshipImmigrationStatus.citizenshipDocument.getSevisId() == null && citizenshipImmigrationStatus.citizenshipDocument.getSEVISId() == null) {
//      return ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(null);
//    }

    log.info("Fetching SsapApplication record by given ssap id: {}", vlpIntegrationLog.ssapApplicantId)
    response["code"] = HttpStatus.NOT_FOUND
    response["message"] = "SSAP Not found for given id"
    var ssapApplication: SsapApplication = ssapApplicationRepository.getOne(vlpIntegrationLog.ssapApplicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response)

    log.info("Fetching Applicants by given ssap id: {}", vlpIntegrationLog.ssapApplicantId)
    response["code"] = HttpStatus.NOT_FOUND
    response["message"] = "Applicant not found for given id"
    val ssapApplicants: List<SsapApplicant> = ssapApplicantRepository.findBySsapApplicationId(vlpIntegrationLog.ssapApplicationId)
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response)

    log.info("Finding Applicant for given vlp integration id: {}", vlpIntegrationLog.ssapApplicantId)
    response["code"] = HttpStatus.NOT_FOUND
    response["message"] = "VLP Integration Log applicant id doesn't match applicant ids in given SSAP"
    val ssapApplicant: SsapApplicant = ssapApplicants.find { applicant -> applicant.id == vlpIntegrationLog.ssapApplicantId }
      ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response)

    val singleStreamlinedApplication: SingleStreamlinedApplication = JsonUtil.parseApplicationDataJson(ssapApplication)

    log.info("Finding household member by applicant guid: {}", ssapApplicant.applicantGuid)
    response["code"] = HttpStatus.NOT_FOUND
    response["message"] = "Household member not found for given applicant guid."
    val householdMember: HouseholdMember =
      singleStreamlinedApplication.taxHousehold[0].householdMember.find { hhm -> hhm.applicantGuid == ssapApplicant.applicantGuid }
        ?: return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null)

    log.info("Updating existing citizenship immigration status object with: {}", citizenshipImmigrationStatus)
    householdMember.citizenshipImmigrationStatus = citizenshipImmigrationStatus;

    ssapApplication = JsonUtil.setApplicationData(ssapApplication, singleStreamlinedApplication);

    log.info("Saving SSAP with updated citizenship immigration status document");
    ssapApplication = ssapApplicationRepository.save(ssapApplication);

    log.info("Launching hub integration for updated application id: {} and applicant id: {}", applicationId, applicantId)
    hubIntegration.process(applicationId, setOf(applicantId))

    response["code"] = HttpStatus.OK
    response["message"] = "success"
    return ResponseEntity.ok(response)
  }
}
