package com.getinsured.hix.batch.enrollment.xmlEnrollments.processor;
 
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.enrollment.Enrollment;

@Component("EnrollmentsProcessor")
public class EnrollmentsProcessor implements ItemProcessor<Enrollment, Enrollment> {
 
	@Override
	public Enrollment process(Enrollment item) throws Exception {
	    System.out.println("Processing..." + item);
		return item;
	}
 
}