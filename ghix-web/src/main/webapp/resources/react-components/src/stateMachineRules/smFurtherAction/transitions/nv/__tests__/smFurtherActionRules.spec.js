import { getFurtherActionData } from '../../../../processDashboardModel';
import { FURTHER_ACTION_LINKS } from '../../../../../constants/dashboardModelTypes';

describe('MP: FurtherAction section', () => {

    let tabModelMock;
    let configMock;
    beforeEach(() => {
        tabModelMock = {
            "applicationYear": "2019",
            "applicationStatus": "ER",
            "applicationDentalStatus":"",
            "applicationType": "QEP",
            "renewalApplication": false,
            "caseNumber": "104669000",
            "encyptedCaseNumber": "o-VM9Icuu39feBJ95W3aCoGYhuAf9YOATMc09ayfB0EqTN4LEBQZzgYH8m7iQ9OC",
            "whereUserLeftOff": "APPSCR51",
            "aptc": 150.0,
            "isFinancial": "Y",
            "applicants": [
                {
                    "memberId": "74550056",
                    "guid": "121411150",
                    "firstName": "Bill",
                    "lastName": "Jhonson",
                    "relation": "SELF",
                    "seekingCoverage": "Y",
                    "exchangeEligible": false,
                    "aptcEligible": true,
                    "stateSubsidyEligible": false,
                    "csrEligible": true,
                    "nativeAmerican": false,
                    "csrLevel": "CS4",
                    "middleName": "",
                    "nameSuffix": "",
                    "chipEligible": false,
                    "medicaidMAGIEligibility": false
                },
                {
                    "memberId": "74550057",
                    "guid": "1214111502",
                    "firstName": "Ann",
                    "lastName": "Jhonson",
                    "relation": "SPOUSE",
                    "seekingCoverage": true,
                    "exchangeEligible": false,
                    "aptcEligible": true,
                    "stateSubsidyEligible": false,
                    "csrEligible": true,
                    "nativeAmerican": false,
                    "csrLevel": "CS4",
                    "middleName": "",
                    "nameSuffix": "",
                    "chipEligible": false,
                    "medicaidMAGIEligibility": false
                },
                {
                    "memberId": "74550058",
                    "guid": "1214111503",
                    "firstName": "Baby",
                    "lastName": "Jhonson",
                    "relation": "CHILD",
                    "seekingCoverage": true,
                    "exchangeEligible": false,
                    "aptcEligible": true,
                    "stateSubsidyEligible": false,
                    "csrEligible": true,
                    "nativeAmerican": false,
                    "csrLevel": "CS4",
                    "middleName": "",
                    "nameSuffix": "",
                    "chipEligible": false,
                    "medicaidMAGIEligibility": false
                }
            ],
            "preEligibility": {
                "plans": []
            },
            "enrollments": [
                {
                    "id": "3355154",
                    "type": "HEALTH",
                    "aptc": 150.0,
                    "csr": "04",
                    "coverageStartDate": "02/01/2019",
                    "coverageEndDate": "12/31/2019",
                    "renewalEnrollment": false,
                    "enrollees": [
                        {
                            "memberId": "121411150",
                            "guid": "121411150",
                            "firstName": "Bill",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SELF",
                            "dob": "07/31/1967",
                            "status": "PENDING"
                        },
                        {
                            "memberId": "1214111503",
                            "guid": "1214111503",
                            "firstName": "Baby",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "CHILD",
                            "dob": "10/15/2018",
                            "status": "PENDING"
                        },
                        {
                            "memberId": "1214111502",
                            "guid": "1214111502",
                            "firstName": "Ann",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SPOUSE",
                            "dob": "03/05/1964",
                            "status": "PENDING"
                        }
                    ],
                    "plan": {
                        "hiosId": "27603",
                        "grossPremium": 300.0,
                        "netPremium": 150.0,
                        "planLevel": "SILVER",
                        "planName": "Silver 73 EPO",
                        "issuerName": "Anthem",
                        "planId": 1639792
                    },
                    "ssapApplicationId": 73450053,
                    "priorSsapApplicationId": 73450053,
                    "enrollmentStatus": "PENDING",
                    "allowChangePlan": false,
                    "enrollmentCreationDate": "01/24/2019"
                },
                {
                    "id": "2895154",
                    "type": "HEALTH",
                    "aptc": 100.0,
                    "csr": "04",
                    "coverageStartDate": "01/01/2019",
                    "coverageEndDate": "01/31/2019",
                    "renewalEnrollment": false,
                    "enrollees": [
                        {
                            "memberId": "121411150",
                            "guid": "121411150",
                            "firstName": "Bill",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SELF",
                            "dob": "07/31/1967",
                            "status": "TERM"
                        },
                        {
                            "memberId": "1214111502",
                            "guid": "1214111502",
                            "firstName": "Ann",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SPOUSE",
                            "dob": "03/05/1964",
                            "status": "TERM"
                        }
                    ],
                    "plan": {
                        "hiosId": "27603",
                        "grossPremium": 200.0,
                        "netPremium": 100.0,
                        "planLevel": "SILVER",
                        "planName": "Silver 73 EPO",
                        "issuerName": "Anthem",
                        "planId": 1639792
                    },
                    "ssapApplicationId": 73450053,
                    "priorSsapApplicationId": 73450052,
                    "enrollmentStatus": "TERM",
                    "allowChangePlan": false,
                    "enrollmentCreationDate": "12/18/2018"
                },
                {
                    "id": "3356154",
                    "type": "DENTAL",
                    "csr": "01",
                    "coverageStartDate": "02/01/2019",
                    "coverageEndDate": "12/31/2019",
                    "renewalEnrollment": false,
                    "enrollees": [
                        {
                            "memberId": "121411150",
                            "guid": "121411150",
                            "firstName": "Bill",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SELF",
                            "dob": "07/31/1967",
                            "status": "PENDING"
                        },
                        {
                            "memberId": "1214111502",
                            "guid": "1214111502",
                            "firstName": "Ann",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SPOUSE",
                            "dob": "03/05/1964",
                            "status": "PENDING"
                        },
                        {
                            "memberId": "1214111503",
                            "guid": "1214111503",
                            "firstName": "Baby",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "CHILD",
                            "dob": "10/15/2018",
                            "status": "PENDING"
                        }
                    ],
                    "plan": {
                        "hiosId": "27603",
                        "grossPremium": 300.0,
                        "netPremium": 300.0,
                        "planLevel": "HIGH",
                        "planName": "Sample dental plan 67",
                        "issuerName": "Anthem",
                        "planId": 1489792
                    },
                    "ssapApplicationId": 73450053,
                    "priorSsapApplicationId": 73450053,
                    "enrollmentStatus": "PENDING",
                    "allowChangePlan": false,
                    "enrollmentCreationDate": "01/24/2019"
                },
                {
                    "id": "2896154",
                    "type": "DENTAL",
                    "aptc": 0.0,
                    "csr": "01",
                    "coverageStartDate": "01/01/2019",
                    "coverageEndDate": "01/31/2019",
                    "renewalEnrollment": false,
                    "enrollees": [
                        {
                            "memberId": "1214111503",
                            "guid": "1214111503",
                            "firstName": "Baby",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "CHILD",
                            "dob": "10/15/2018",
                            "status": "CANCEL"
                        },
                        {
                            "memberId": "121411150",
                            "guid": "121411150",
                            "firstName": "Bill",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SELF",
                            "dob": "07/31/1967",
                            "status": "TERM"
                        },
                        {
                            "memberId": "1214111502",
                            "guid": "1214111502",
                            "firstName": "Ann",
                            "lastName": "Jhonson",
                            "middleName": "",
                            "suffix": "",
                            "relation": "SPOUSE",
                            "dob": "03/05/1964",
                            "status": "TERM"
                        }
                    ],
                    "plan": {
                        "hiosId": "27603",
                        "grossPremium": 300.0,
                        "netPremium": 300.0,
                        "planLevel": "HIGH",
                        "planName": "Sample dental plan 67",
                        "issuerName": "Anthem",
                        "planId": 1489792
                    },
                    "ssapApplicationId": 73450053,
                    "priorSsapApplicationId": 73450053,
                    "enrollmentStatus": "TERM",
                    "allowChangePlan": false,
                    "enrollmentCreationDate": "12/18/2018"
                }
            ],
            "sepEvent": {
                "isIdentified": true,
                "isVerified": true
            },
            "isSepOpen": true,
            "isInsideOEEnrollmentWindow": true,
            "isInsideOEWindow": true,
            "validationStatus": "PENDING_INFO",
            "defaultRoleName": ""
        };
        configMock = {
            "oepStartDate": "10/15/2018",
            "oepEndDate": "01/15/2019",
            "oeApproachingdays": -238,
            "tobbacoEnabled": "OFF",
            "hardshipEnabled": "OFF",
            "serverDate": "06/10/2019",
            "ssapFlowVersion": ""
        }
    });

    describe('High level flow: OE', () => {

        describe('OE: Simple cases', () => {
            it('should have s_startApplicationOE1 App Status: CL', () => {
                tabModelMock.applicationStatus = 'CL';
                const res = {
                    btnText: 'further_action.cta.btn_start_new_application',
                    paragraphText: 'further_action.further_text.OE1',
                    link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_resumeApplicationOE2 App Status: OP', () => {
                tabModelMock.applicationStatus = 'OP';
                const res = {
                    btnText: 'further_action.cta.btn_resume_application',
                    paragraphText: 'further_action.further_text.OE2',
                    link: FURTHER_ACTION_LINKS.RESUME_APPLICATION
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_noButtonOE3, App Status: SG', () => {
                tabModelMock.applicationStatus = 'SG';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE3',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_noButtonOE3, App Status: SU', () => {
                tabModelMock.applicationStatus = 'SG';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE3',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_startApplicationOE1, No app', () => {
                tabModelMock = {
                    "applicationYear": "2019",
                    "renewalApplication": false,
                    "isFinancial": false,
                    "preEligibility": {
                        "aptcEligible": false,
                        "csrEligible": false
                    },
                    "isSepOpen": false,
                    "isInsideOEWindow": true,
                    "isConditional": false
                };
                const res = {
                    btnText: 'further_action.cta.btn_start_new_application',
                    paragraphText: 'further_action.further_text.OE1',
                    link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });
            it('should have s_noButtonOE10, App Status: QU', () => {
                tabModelMock.applicationStatus = 'QU';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE10',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });
        });

        describe('OE: AppStatus ER/PN/EN', () => {

            describe('OE OEP', () => {
                beforeEach(() => {
                    tabModelMock.applicationType = 'OE';
                });

                describe('OE OEP Renewal Status NOT set', () => {
                    describe('OE OEP ER', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'ER';
                        });

                        it('should have s_continueShoppingOE11', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE11',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_shopForPlansOE12', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_shop_for_plans',
                                paragraphText: 'further_action.further_text.OE12',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_shopForPlansOE12 NOT_TO_CONSIDER dentalRenewalStatus', () => {
                            tabModelMock.dentalRenewalStatus = "NOT_TO_CONSIDER";
                            tabModelMock.healthRenewalStatus = "APP_ERROR";
                            const res = {
                                btnText: 'further_action.cta.btn_shop_for_plans',
                                paragraphText: 'further_action.further_text.OE12',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_shopForPlansOE12 APP_ERROR healthRenewalStatus', () => {
                            tabModelMock.healthRenewalStatus = "APP_ERROR";
                            tabModelMock.dentalRenewalStatus = "NOT_TO_CONSIDER";
                            const res = {
                                btnText: 'further_action.cta.btn_shop_for_plans',
                                paragraphText: 'further_action.further_text.OE12',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('OE OEP PN', () => {
                        it('should have s_continueShoppingOE13', () => {
                            tabModelMock.applicationStatus = 'PN';
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE13',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('OE OEP EN', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'EN';
                        });

                        it('should have s_continueShoppingOE4', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE4',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_changePlansOE15', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_change_plans',
                                paragraphText: 'further_action.further_text.OE15',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });
                });

                describe('OE OEP Renewal Status set', () => {
                    beforeEach(() => {
                        tabModelMock.dentalRenewalStatus = 'TO_CONSIDER';
                        tabModelMock.healthRenewalStatus = 'RENEWED';
                    });

                    it('should have s_finalizePlansREN6 No QHP Eligible applicant', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_confirm_finalize_plans',
                            paragraphText: 'further_action.further_text.REN6',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    describe('OE OEP QHP Eligible ER', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'ER';
                            tabModelMock.applicants[0].exchangeEligible = true;
                        });

                        it('should have s_finalizePlansREN1', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_confirm_finalize_plans',
                                paragraphText: 'further_action.further_text.REN1',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_continueShoppingREN2', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.REN2',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('OE OEP QHP Eligible PN', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'PN';
                            tabModelMock.applicants[0].exchangeEligible = true;
                        });

                        it('should have s_continueShoppingREN3', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.REN3',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('OE OEP QHP Eligible EN', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'EN';
                            tabModelMock.applicants[0].exchangeEligible = true;
                        });

                        it('should have s_changePlansREN4', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_change_plans',
                                paragraphText: 'further_action.further_text.REN4',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_continueShoppingREN5', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.REN5',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });
                });

            });

            describe('OE QEP', () => {
                beforeEach(() => {
                    tabModelMock.applicationType = 'QEP';
                });

                it('should have s_shopForPlansOE12', () => {
                    tabModelMock.applicationStatus = 'ER';
                    const res = {
                        btnText: 'further_action.cta.btn_shop_for_plans',
                        paragraphText: 'further_action.further_text.OE12',
                        link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                    };
                    const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                    expect(a).toMatchObject(res);
                });

                it('should have s_uploadDocumentsQLE3', () => {
                    tabModelMock.sepEvent.isVerified = false;
                    tabModelMock.validationStatus = 'PENDING';
                    const res = {
                        btnText: 'further_action.cta.btn_upload_documents',
                        paragraphText: 'further_action.further_text.QLE3',
                        link: FURTHER_ACTION_LINKS.UPLOAD_DOCUMENTS
                    };
                    const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                    expect(a).toMatchObject(res);
                });
            });

            describe('OE SEP', () => {
                beforeEach(() => {
                    tabModelMock.applicationType = 'SEP';
                });

                describe('OE SEP PN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'PN';
                    });

                    it('should have s_continueShoppingSEP5', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.SEP5',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });

                describe('OE SEP EN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'EN';
                    });

                    it('should have s_continueShoppingOE4', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.OE4',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansOE15', () => {
                        tabModelMock.applicationDentalStatus = "EN";
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.OE15',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });
            });
        });
    });

    describe('High level flow: Outside OE', () => {
        beforeEach(() => {
            tabModelMock.isInsideOEWindow = false;
        });

        describe('Outside OE: Simple cases', () => {
            it('should have s_startApplicationOE5, No app', () => {
                tabModelMock = {
                    "applicationYear": "2019",
                    "renewalApplication": false,
                    "isFinancial": false,
                    "preEligibility": {
                        "aptcEligible": false,
                        "csrEligible": false
                    },
                    "isSepOpen": false,
                    "isInsideOEWindow": false,
                    "isConditional": false
                };
                const res = {
                    btnText: 'further_action.cta.btn_start_new_application',
                    paragraphText: 'further_action.further_text.OE5',
                    link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_startApplicationOE5 App Status: OP', () => {
                tabModelMock.applicationStatus = 'OP';
                const res = {
                    btnText: 'further_action.cta.btn_start_new_application',
                    paragraphText: 'further_action.further_text.OE5',
                    link: FURTHER_ACTION_LINKS.START_NEW_APPLICATION
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_noButtonOE3 App Status: SG', () => {
                tabModelMock.applicationStatus = 'SG';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE3',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_noButtonOE3 App Status: SU', () => {
                tabModelMock.applicationStatus = 'SU';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE3',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });

            it('should have s_noButtonOE10, App Status: QU', () => {
                tabModelMock.applicationStatus = 'QU';
                const res = {
                    btnText: '',
                    paragraphText: 'further_action.further_text.OE10',
                    link: ''
                };
                const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                expect(a).toMatchObject(res);
            });
        });

        describe('Outside OE: AppStatus ER/PN/EN', () => {

            describe('Outside OE QEP', () => {

                beforeEach(() => {
                    tabModelMock.applicationType = 'QEP'
                });

                describe('Period Outside OE QEP ER', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'ER';
                    });

                    it('should have s_confirmEventAndShopQLE1', () => {
                        tabModelMock.sepEvent.isIdentified = false;
                        const res = {
                            btnText: 'further_action.cta.btn_confirm_event_and_shop',
                            paragraphText: 'further_action.further_text.QLE1',
                            link: FURTHER_ACTION_LINKS.CONFIRM_EVENT
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeQLE4', () => {
                        tabModelMock.isSepOpen = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.QLE4',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_uploadDocumentsQLE3', () => {
                        tabModelMock.sepEvent.isVerified = false;
                        tabModelMock.validationStatus = 'PENDING';
                        const res = {
                            btnText: 'further_action.cta.btn_upload_documents',
                            paragraphText: 'further_action.further_text.QLE3',
                            link: FURTHER_ACTION_LINKS.UPLOAD_DOCUMENTS
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_provideAdditionalInfoQLE15', () => {

                        tabModelMock.sepEvent.isVerified = false;
                        tabModelMock.validationStatus = 'PENDING_INFO';
                        const res = {
                            btnText: 'further_action.cta.btn_provide_additional_info',
                            paragraphText: 'further_action.further_text.QLE15',
                            link: FURTHER_ACTION_LINKS.VERIFICATION
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingQLE2', () => {
                        tabModelMock.applicationDentalStatus = "EN";
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.QLE2',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_shopForPlansQLE6', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_shop_for_plans',
                            paragraphText: 'further_action.further_text.QLE6',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });

                describe('Period Outside OE QEP PN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'PN';
                    });

                    it('should have s_continueShoppingQLE5', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.QLE5',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeQLE7', () => {
                        tabModelMock.isSepOpen = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.QLE7',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansQLE8', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.applicants[0].nativeAmerican = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.QLE8',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });

                describe('Period Outside OE QEP EN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'EN';
                    });

                    it('should have s_reportAChangeQLE7', () => {
                        tabModelMock.isSepOpen = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.QLE7',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeQLE7', () => {
                        tabModelMock.applicationDentalStatus = 'EN';
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.QLE7',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansQLE8', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.applicants[0].nativeAmerican = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.QLE8',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansQLE8', () => {
                        tabModelMock.applicationDentalStatus = 'EN';
                        tabModelMock.applicants[0].nativeAmerican = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.QLE8',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingQLE10', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.QLE10',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });
            });

            describe('Outside OE OEP', () => {
                beforeEach(() => {
                    tabModelMock.applicationType = 'OE'
                });

                describe('Period NOT within OE Enrollment Window', () => {
                    beforeEach(() => {
                        tabModelMock.isInsideOEEnrollmentWindow = false;
                    });

                    it('should have s_noButtonOE8', () => {
                        configMock.serverDate = "06/10/2019";
                        configMock.oepStartDate = "07/10/2019";
                        configMock.oepEndDate = "11/01/2019";
                        tabModelMock.applicationYear = '2020';
                        const res = {
                            btnText: '',
                            paragraphText: 'further_action.further_text.OE8',
                            link: ''
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    describe('Not Prior to OE start date', () => {
                        beforeEach(() => {
                            configMock.serverDate = "12/10/2019";
                        });

                        it('should have s_reportAChangeOE9', () => {
                            tabModelMock.applicationStatus = 'ER';

                            const res = {
                                btnText: 'further_action.cta.btn_report_a_change',
                                paragraphText: 'further_action.further_text.OE9',
                                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_reportAChangeQLE11', () => {
                            tabModelMock.applicationStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_report_a_change',
                                paragraphText: 'further_action.further_text.QLE11',
                                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_reportAChangeQLE11', () => {
                            tabModelMock.applicationStatus = 'PN';
                            const res = {
                                btnText: 'further_action.cta.btn_report_a_change',
                                paragraphText: 'further_action.further_text.QLE11',
                                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_changePlansQLE8', () => {
                            tabModelMock.applicationStatus = 'EN';
                            tabModelMock.applicants[0].nativeAmerican = true;
                            const res = {
                                btnText: 'further_action.cta.btn_change_plans',
                                paragraphText: 'further_action.further_text.QLE8',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });
                });

                describe('Period within OE Enrollment Window (D)', () => {
                    beforeEach(() => {
                        tabModelMock.isInsideOEEnrollmentWindow = true;
                    });

                    describe('Outside OE OEP ER', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'ER';
                        });

                        it('should have s_shopForPlansOE12', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_shop_for_plans',
                                paragraphText: 'further_action.further_text.OE12',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_continueShoppingOE11', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE11',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('Outside OE OEP PN', () => {
                        it('should have s_continueShoppingOE3', () => {
                            tabModelMock.applicationStatus = 'PN';
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE3',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                    describe('Outside OE OEP EN', () => {
                        beforeEach(() => {
                            tabModelMock.applicationStatus = 'EN';
                        });

                        it('should have s_reportAChangeSEP4', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            const res = {
                                btnText: 'further_action.cta.btn_report_a_change',
                                paragraphText: 'further_action.further_text.SEP4',
                                link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_continueShoppingOE4', () => {
                            const res = {
                                btnText: 'further_action.cta.btn_continue_shopping',
                                paragraphText: 'further_action.further_text.OE4',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });

                        it('should have s_changePlansQLE9', () => {
                            tabModelMock.applicationDentalStatus = 'EN';
                            tabModelMock.applicants[0].nativeAmerican = true;
                            const res = {
                                btnText: 'further_action.cta.btn_change_plans',
                                paragraphText: 'further_action.further_text.QLE9',
                                link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                            };
                            const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                            expect(a).toMatchObject(res);
                        });
                    });

                });

            });

            describe('Outside OE SEP', () => {
                beforeEach(() => {
                    tabModelMock.applicationType = 'SEP'
                });

                describe('Outside OE SEP ER', () => {

                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'ER';
                    });

                    it('should have s_confirmEventAndFinalizePlansSEP1', () => {
                        tabModelMock.sepEvent.isIdentified = false;
                        const res = {
                            btnText: 'further_action.cta.btn_confirm_event_and_finalize_plans',
                            paragraphText: 'further_action.further_text.SEP1',
                            link: FURTHER_ACTION_LINKS.CONFIRM_EVENT
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeSEP4', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.SEP4',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingOE11', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = true;
                        tabModelMock.applicationDentalStatus = 'EN';
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.OE11',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_shopForPlansOE12', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = true;
                        tabModelMock.applicationDentalStatus = '';
                        const res = {
                            btnText: 'further_action.cta.btn_shop_for_plans',
                            paragraphText: 'further_action.further_text.OE12',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_finalizePlansSEP2', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_confirm_finalize_plans',
                            paragraphText: 'further_action.further_text.SEP2',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingSEP3', () => {
                        tabModelMock.applicationDentalStatus = 'EN';
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.SEP3',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_uploadDocumentsQLE3', () => {
                        tabModelMock.sepEvent.isVerified = false;
                        tabModelMock.validationStatus = 'PENDING';
                        const res = {
                            btnText: 'further_action.cta.btn_upload_documents',
                            paragraphText: 'further_action.further_text.QLE3',
                            link: FURTHER_ACTION_LINKS.UPLOAD_DOCUMENTS
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_provideAdditionalInfoQLE15', () => {
                        tabModelMock.sepEvent.isVerified = false;
                        tabModelMock.validationStatus = 'PENDING_INFO';
                        const res = {
                            btnText: 'further_action.cta.btn_provide_additional_info',
                            paragraphText: 'further_action.further_text.QLE15',
                            link: FURTHER_ACTION_LINKS.VERIFICATION
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });

                describe('Outside OE SEP PN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'PN';
                    });

                    it('should have s_continueShoppingSEP5', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.SEP5',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeSEP4', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.SEP4',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansOE15', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = true;
                        tabModelMock.applicationDentalStatus = 'EN';

                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.OE15',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingOE4', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = true;
                        tabModelMock.applicationDentalStatus = '';

                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.OE4',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansQLE9', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.applicants[0].nativeAmerican = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.QLE9',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                });

                describe('Outside OE SEP EN', () => {
                    beforeEach(() => {
                        tabModelMock.applicationStatus = 'EN';
                    });

                    it('should have s_reportAChangeSEP4', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.isInsideOEEnrollmentWindow = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.SEP4',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansQLE9', () => {
                        tabModelMock.isSepOpen = false;
                        tabModelMock.applicants[0].nativeAmerican = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.QLE9',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN_IF_ALL_ENROLLMENTS_NOT_WITHIN_THIS_MONTH
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingSEP3', () => {
                        tabModelMock.enrollments = [
                            {
                                "id": "3355154",
                                "type": "HEALTH",
                                "aptc": 150.0,
                                "csr": "04",
                                "coverageStartDate": "02/01/2019",
                                "coverageEndDate": "12/31/2019",
                                "renewalEnrollment": false,
                                "enrollees": [
                                    {
                                        "memberId": "121411150",
                                        "guid": "121411150",
                                        "firstName": "Bill",
                                        "lastName": "Jhonson",
                                        "middleName": "",
                                        "suffix": "",
                                        "relation": "SELF",
                                        "dob": "07/31/1967",
                                        "status": "PENDING"
                                    },
                                    {
                                        "memberId": "1214111503",
                                        "guid": "1214111503",
                                        "firstName": "Baby",
                                        "lastName": "Jhonson",
                                        "middleName": "",
                                        "suffix": "",
                                        "relation": "CHILD",
                                        "dob": "10/15/2018",
                                        "status": "PENDING"
                                    },
                                    {
                                        "memberId": "1214111502",
                                        "guid": "1214111502",
                                        "firstName": "Ann",
                                        "lastName": "Jhonson",
                                        "middleName": "",
                                        "suffix": "",
                                        "relation": "SPOUSE",
                                        "dob": "03/05/1964",
                                        "status": "PENDING"
                                    }
                                ],
                                "plan": {
                                    "hiosId": "27603",
                                    "grossPremium": 300.0,
                                    "netPremium": 150.0,
                                    "planLevel": "SILVER",
                                    "planName": "Silver 73 EPO",
                                    "issuerName": "Anthem",
                                    "planId": 1639792
                                },
                                "ssapApplicationId": 73450053,
                                "priorSsapApplicationId": 73450053,
                                "enrollmentStatus": "PENDING",
                                "allowChangePlan": false,
                                "enrollmentCreationDate": "01/24/2019"
                            }
                        ];
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.SEP3',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_continueShoppingSEP3', () => {
                        const res = {
                            btnText: 'further_action.cta.btn_continue_shopping',
                            paragraphText: 'further_action.further_text.SEP3',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_changePlansSEP6', () => {
                        tabModelMock.applicationDentalStatus = 'EN';
                        tabModelMock.enrollments[0].allowChangePlan = true;
                        const res = {
                            btnText: 'further_action.cta.btn_change_plans',
                            paragraphText: 'further_action.further_text.SEP6',
                            link: FURTHER_ACTION_LINKS.GROUPING_SCREEN
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });

                    it('should have s_reportAChangeSEP4', () => {
                        tabModelMock.applicationDentalStatus = 'EN';
                        tabModelMock.isInsideOEEnrollmentWindow = false;
                        const res = {
                            btnText: 'further_action.cta.btn_report_a_change',
                            paragraphText: 'further_action.further_text.SEP4',
                            link: FURTHER_ACTION_LINKS.REPORT_CHANGE
                        };
                        const a = getFurtherActionData(tabModelMock, configMock, 'NV');
                        expect(a).toMatchObject(res);
                    });
                })
            });
        });
    });
});