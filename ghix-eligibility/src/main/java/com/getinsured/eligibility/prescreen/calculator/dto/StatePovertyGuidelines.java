package com.getinsured.eligibility.prescreen.calculator.dto;

import java.util.HashMap;
import java.util.Map;

public class StatePovertyGuidelines {

	private Integer coverageYear;
	private String stateCode;
	private Map<Integer, Float> thresholds = new HashMap<Integer, Float>();
	private Float incrementPerAdditionalPerson;

	public Integer getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(Integer coverageYear) {
		this.coverageYear = coverageYear;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public Map<Integer, Float> getThresholds() {
		return thresholds;
	}

	public void setThresholds(Map<Integer, Float> thresholds) {
		this.thresholds = thresholds;
	}

	public Float getIncrementPerAdditionalPerson() {
		return incrementPerAdditionalPerson;
	}

	public void setIncrementPerAdditionalPerson(Float incrementPerAdditionalPerson) {
		this.incrementPerAdditionalPerson = incrementPerAdditionalPerson;
	}

}
