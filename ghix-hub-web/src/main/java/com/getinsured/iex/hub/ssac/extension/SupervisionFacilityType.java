
package com.getinsured.iex.hub.ssac.extension;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for the Supervision facility. 
 *             
 * 
 * <p>Java class for SupervisionFacilityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SupervisionFacilityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}FacilityName"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}FacilityLocation"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}FacilityContactInformation"/>
 *         &lt;element ref="{http://extn.ssac.ee.sim.dsh.cms.hhs.gov}FacilityCategoryCode"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupervisionFacilityType", propOrder = {
    "facilityName",
    "facilityLocation",
    "facilityContactInformation",
    "facilityCategoryCode"
})
public class SupervisionFacilityType
    extends ComplexObjectType
{

    @XmlElement(name = "FacilityName", required = true)
    protected String facilityName;
    @XmlElement(name = "FacilityLocation", required = true)
    protected FacilityLocationType facilityLocation;
    @XmlElement(name = "FacilityContactInformation", required = true)
    protected FacilityContactInformationType facilityContactInformation;
    @XmlElement(name = "FacilityCategoryCode", required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    protected String facilityCategoryCode;

    /**
     * Gets the value of the facilityName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacilityName() {
        return facilityName;
    }

    /**
     * Sets the value of the facilityName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilityName(String value) {
        this.facilityName = value;
    }

    /**
     * Gets the value of the facilityLocation property.
     * 
     * @return
     *     possible object is
     *     {@link FacilityLocationType }
     *     
     */
    public FacilityLocationType getFacilityLocation() {
        return facilityLocation;
    }

    /**
     * Sets the value of the facilityLocation property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityLocationType }
     *     
     */
    public void setFacilityLocation(FacilityLocationType value) {
        this.facilityLocation = value;
    }

    /**
     * Gets the value of the facilityContactInformation property.
     * 
     * @return
     *     possible object is
     *     {@link FacilityContactInformationType }
     *     
     */
    public FacilityContactInformationType getFacilityContactInformation() {
        return facilityContactInformation;
    }

    /**
     * Sets the value of the facilityContactInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link FacilityContactInformationType }
     *     
     */
    public void setFacilityContactInformation(FacilityContactInformationType value) {
        this.facilityContactInformation = value;
    }

    /**
     * Gets the value of the facilityCategoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFacilityCategoryCode() {
        return facilityCategoryCode;
    }

    /**
     * Sets the value of the facilityCategoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFacilityCategoryCode(String value) {
        this.facilityCategoryCode = value;
    }

}
