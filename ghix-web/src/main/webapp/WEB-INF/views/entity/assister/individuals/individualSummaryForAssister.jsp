<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@page import="com.getinsured.hix.model.Broker"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page contentType="text/html" import="java.util.*" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<c:set var="externalIndividualNewId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<c:set var="isUserCreated" value="${isIndividualUserExists}" ></c:set>

<div class="row-fluid">
		<h1 tabindex="0" aria-flowto="sidebar rightpanel"><a name="skip"></a>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</h1>
	</div>
<div class="row-fluid">
	<div id="sidebar" class="span3">
				   <ul class="nav nav-list">         
				  <li class="active"><spring:message code="label.assister.summary"/></li>
				  <c:set var="externalIndividualId" ><encryptor:enc value="${externalIndividual.id}" isurl="true"/> </c:set>
				  <li><a href="/hix/entity/assister/individualcomments/${externalIndividualId}"><spring:message code="label.assister.comments"/></a></li>
				  </ul>
				 <br>
				 <div class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.assister.actions"/></div>
				<ul class="nav nav-list">
				
					<li class="navmenu"><a name="addComment" href="<c:url value="/entity/assister/newcomment?target_id=${externalIndividualNewId}&target_name=DESIGNATEASSISTER&individualName=${externalIndividual.firstName} ${externalIndividual.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.assister.newComment"/></a></li>
					<c:if test="${checkDilog == null}">
					<li class="navmenu"><a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
					</c:if>
					<c:if test="${checkDilog != null}">
					<li class="navmenu"><a href="/hix/switchToIndividualView/dashboard?switchToModuleName=<encryptor:enc value="individual"/>&switchToModuleId=<encryptor:enc value="${externalIndividual.id}" />&switchToResourceName=${externalIndividual.firstName} ${externalIndividual.lastName}" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.assister.switchToIndividualView"/></a></li>
					</c:if>
					<c:if test="${isConsumerActivated == false}">
				  		<li><a name ="send-activation-link"  href="#"  id="send-activation-link"><i class="icon-envelope"></i><spring:message code="label.assister.resendActivationEmail"/></a></li>
				  	</c:if>
				</ul>
				
			</div>
	<div class="span9" id="rightpanel">
	<div class="header"><h4 class="pull-left"><spring:message code="label.assister.summary"/></h4>
				
				<a class="btn btn-small pull-right save-form" href="javascript:void(0)">Save</a>
			</div>
	<div class="gutter10-tb">
	<form class="form-horizontal" id="frmindsummaryinfo"
			name="frmindsummaryinfo" action="<c:url value="/entity/assister/indsummaryinfo/${externalIndividualNewId}"/>" method="POST">
			<df:csrfToken/>
	 <div class="control-group">
	 <label for="primaryApplicantName" class="control-label"><spring:message code="label.assister.primaryApplicant"/></label><div class="controls"><span style="display: inline-block;padding-top: 6px;"><strong>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</strong></span></div>
	 </div>
	<div class="control-group">
							<label for="priContactEmailAddress" class="required control-label"><spring:message code="label.brkEmail"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<!-- end of label -->
							<div class="controls">
								<input type="email" placeholder="<spring:message code="label.entity.entCompany@email.com"/>" name="priContactEmailAddress" id="priContactEmailAddress"
									value="${externalIndividual.emailAddress}" class="xlarge"	size="30" />
								<div id="priContactEmailAddress_error" class="help-inline"></div>
							</div>
							 
							<!-- end of controls-->
				</div>
				<div class="control-group phone-group">
						<label for="priContactPrimaryPhoneNumber1" class="control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber"/><span class="aria-hidden"> <spring:message code="label.entity.first3digits"/></span><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
	                    	<label for="priContactPrimaryPhoneNumber2" class="aria-hidden control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber2"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
	                    	<label for="priContactPrimaryPhoneNumber3" class="aria-hidden control-label"><spring:message code="label.entity.enterPrimaryPhoneNumber3"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<input type="text" name="priContactPrimaryPhoneNumber1" id="priContactPrimaryPhoneNumber1"	value="${priContactPrimaryPhoneNumber1}" maxlength="3" 	placeholder="xxx" class="area-code input-mini" /> 
								<input	type="text" name="priContactPrimaryPhoneNumber2" id="priContactPrimaryPhoneNumber2"	value="${priContactPrimaryPhoneNumber2}" maxlength="3"	placeholder="xxx" class="input-mini"  /> 
								<input type="text" name="priContactPrimaryPhoneNumber3" id="priContactPrimaryPhoneNumber3" value="${priContactPrimaryPhoneNumber3}" maxlength="4"	placeholder="xxxx" class="input-small" /> 
								<input type="hidden" name="priContactPrimaryPhoneNumber" id="priContactPrimaryPhoneNumber" value="" />
								<div id="priContactPrimaryPhoneNumber1_error"></div>
								<div id="priContactPrimaryPhoneNumber3_error"></div> 
							</div>
				</div>
				<div class="form-actions txt-right">
						<!-- <a class="btn btn-small pull-right save-form" href="javascript:void(0)" >Save</a> -->						
				</div>										
				
				
	</form>
	<jsp:include page="individualViewModal.jsp" />
	</div>		
	</div>
</div>

<script type="text/javascript">

/* function validateForm()
{
	if($("#frmindsummaryinfo").validate() == true){
		$('#frmindsummaryinfo').submit()
	  }
	} */

function ie8Trim(){
	if(typeof String.prototype.trim !== 'function') {
        String.prototype.trim = function() {
        	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
        }
	}
}

var validator = $("#frmindsummaryinfo").validate(
		{	onkeyup : false,
			onclick : false,
			rules : {
				priContactEmailAddress : { required : true,email: true},
				priContactPrimaryPhoneNumber1 : {numberStartsWithZeroCheck: false,digits:true},
				priContactPrimaryPhoneNumber2:{digits:true},
				priContactPrimaryPhoneNumber3 : { priContactPrimaryPhoneNumberCheck : true,digits:true}
			},
			groups: {
			priContactPrimaryPhoneNumber:"priContactPrimaryPhoneNumber1 priContactPrimaryPhoneNumber2 priContactPrimaryPhoneNumber3"
			
			},
			messages : {
					priContactPrimaryPhoneNumber3 : {priContactPrimaryPhoneNumberCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>",
													 digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
					priContactPrimaryPhoneNumber1 :{numberStartsWithZeroCheck :"<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>",
													digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
					priContactPrimaryPhoneNumber2:{digits:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidPhoneNumber' javaScriptEscape='true'/></span>"},
					priContactEmailAddress: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePriContactEmailAddress' javaScriptEscape='true'/></span>",
				    						  email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"}
				
			},

			errorClass: "error",
			errorPlacement: function(error, element) {
				var elementId = element.attr('id');
				if(elementId == "priContactSecondaryPhoneNumber1" || elementId == "priContactSecondaryPhoneNumber2" || elementId == "priContactSecondaryPhoneNumber3" )
				{
					error.appendTo( $("#" + "priContactSecondaryPhoneNumber3" + "_error"));
				}
				if(elementId == "priContactPrimaryPhoneNumber1" || elementId == "priContactPrimaryPhoneNumber2" || elementId == "priContactPrimaryPhoneNumber3" )
				{
					error.appendTo( $("#" + "priContactPrimaryPhoneNumber3" + "_error"));
				}else{	
					error.appendTo( $("#" + elementId + "_error"));
				}
				$("#" + elementId + "_error").attr('class','error help-inline');
			} 
		});

jQuery.validator.addMethod("priContactPrimaryPhoneNumberCheck",	function(value, element, param) {
	ie8Trim();
	priContactPrimaryPhoneNumber1 = $("#priContactPrimaryPhoneNumber1").val().trim();
	priContactPrimaryPhoneNumber2 = $("#priContactPrimaryPhoneNumber2").val().trim();
	priContactPrimaryPhoneNumber3 = $("#priContactPrimaryPhoneNumber3").val().trim();
	var a=(/^[0-9]*$/.test(priContactPrimaryPhoneNumber1)&& /^[0-9]*$/.test(priContactPrimaryPhoneNumber2)&&/^[0-9]*$/.test(priContactPrimaryPhoneNumber3));
	
	var firstChar = priContactPrimaryPhoneNumber1.charAt(0);
	if(firstChar == 0) {
			return false;
	}
	if(!a){
		return false;
	}
	if ((priContactPrimaryPhoneNumber1 == "" || priContactPrimaryPhoneNumber2 == "" || priContactPrimaryPhoneNumber3 == "")	|| (isNaN(priContactPrimaryPhoneNumber1)) || (priContactPrimaryPhoneNumber1.length < 3) || (isNaN(priContactPrimaryPhoneNumber2)) || (priContactPrimaryPhoneNumber2.length < 3) || (isNaN(priContactPrimaryPhoneNumber3)) || (priContactPrimaryPhoneNumber3.length < 4)) 
	{
		return false;
	} else {
		$("#priContactPrimaryPhoneNumber").val(priContactPrimaryPhoneNumber1+priContactPrimaryPhoneNumber2+priContactPrimaryPhoneNumber3);
		return true;
	}
});

jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
		ie8Trim();
		// This is added to check if user has not entered anything into the textbox
		if((value.length == 0)) {
		return true;
		}
		
		// If user has entered anything then test, if entered value is valid
		var firstChar = value.charAt(0);
		if(firstChar == 0) {
		return false;
		} else{
		return true;
}
});

</script>
<script type="text/javascript">

$("document").ready(function (){
	var csrValue = $("#csrftoken").val();
	
	$("#send-activation-link").click(function (){
		
	     if(${isConsumerActivated}) {
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	            + 'activation email cannot be sent.</p></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        } 
	     
	     else if(${isUserCreated}) {
	         $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0;"><h3>Failure</h3></div><div class="modal-body" style="max-height:470px;"><p> ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName} has already created an account. A new'  
	            + 'activation email cannot be sent.</p></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
	        }
	     else {
	        	 $("#processing").show();
	        	 var  strPath =  "<%=  request.getContextPath()  %>";
                 var  pathURL=strPath+"/assister/individual/sendactivationlink/${externalIndividualId}";
                 $.ajax({ 	type: "POST",
                	 		url: pathURL,
                	 		 data:{ csrftoken:'<df:csrfToken plainToken="true"/>' },
                	 		success: function (data,status) {
                	 			if(status === 'success') {
    	              				if(-1 == data.search("SUCCESS")) { 
    	              					$("#processing").hide();
    	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}. Cause:' + data + '.</p></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	              				}else {
    	              					$("#processing").hide();
    	               					$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Success</h3></div><div class="modal-body" style="max-height:470px; "><p>A new activation email has been sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	              				}
    	             			}else {
    	             				$("#processing").hide();
    	              				$('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure</h3></div><div class="modal-body" style="max-height:470px;"><p>A new activation email could not be sent to ${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}.</p></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
    	             			}
                	           
                	        },
                	        error: function(e) {
                	            console.log("Error:" + e);
                	        }
                 });
	        }
	     
    
	  });
	
	$(".save-form").click(function (){
		if($("#frmindsummaryinfo").valid()) {
			$("#send-activation-link").parent().remove();
			$('<div id="confirm-popup" class="modal fade"><div class="modal-header"><h3 class="pull-left">Do you want to continue?</h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload();">&times;</button></div><div class="modal-body"><p>This action will send a new account activation email to <strong>${externalIndividual.firstName}&nbsp;${externalIndividual.lastName}</strong>. Do you want to continue?</p></div><div class="modal-footer txt-right"><button class="btn" data-dismiss="modal" aria-hidden="true"type="button" onClick="window.location.reload();">No</button><button class="btn btn-primary submit-pop"  aria-hidden="true" type="button" onClick="$(\'#frmindsummaryinfo\').submit()">Yes</button></div></div>').modal();
		}
	});
});
</script>