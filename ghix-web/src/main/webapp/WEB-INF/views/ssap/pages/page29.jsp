<%@ include file="../include.jsp" %>   
<div id="appscr79" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page29.americanIndianQuestions" text="American Indian Questions for" />
				<strong class="nameOfHouseHold"></strong>
			</h4>
		</div>
		<form id="appscr79Form" class="form-horizontal">
			<div class="control-group">
				<p>
					<spring:message code="ssap.is" text="Is" />&nbsp;<strong class="nameOfHouseHold"></strong>&nbsp;<spring:message code="ssap.page29.aMemberOfAFederally" text="a member of a federally recognized tribe" />?
				</p><!-- end of label -->
				<div class="margin5-l">
					<label>
						<input type="radio" name="AmericonIndianQuestionRadio" id="AmericonIndianQuestionRadioYes" value="yes" checked="checked" onchange="checkAmericonIndianQuestion();" />
						<spring:message code="ssap.yes" text="Yes" />
					</label>
					<label>
						<input type="radio" name="AmericonIndianQuestionRadio" id="AmericonIndianQuestionRadioNo" value="no"  onchange="checkAmericonIndianQuestion();">
						<spring:message code="ssap.no" text="No" />
					</label>
				</div>
			</div><!-- control-group ends -->
			<div id="americonIndianQuestionDiv">
				<div class="control-group">
					<label class="control-label">
						<spring:message code="ssap.State" text="State" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="input-medium" id="appscr79State">
			        		<option value='0'><spring:message code="ssap.State" text="State" /></option>
							<option value='AL'><spring:message code="ssap.state.Albama" text="Alabama" /></option>
							<option value='AK'><spring:message code="ssap.state.Alaska" text="Alaska" /></option>
							<option value='AS'><spring:message code="ssap.state.AmericanSamoa" text="American Samoa" /></option>
							<option value='AZ'><spring:message code="ssap.state.Arizona" text="Arizona" /></option>
							<option value='AR'><spring:message code="ssap.state.Arkansas" text="Arkansas" /></option>
							<option value='CA'><spring:message code="ssap.state.California" text="California" /></option>
							<option value='CO'><spring:message code="ssap.state.Colorado" text="Colorado" /></option>
							<option value='CT'><spring:message code="ssap.state.Connecticut" text="Connecticut" /></option>
							<option value='DE'><spring:message code="ssap.state.Delaware" text="Delaware" /></option>
							<option value='DC'><spring:message code="ssap.state.DistrictOfColumbia" text="District of Columbia" /></option>
							<option value='FL'><spring:message code="ssap.state.Florida" text="Florida" /></option>
							<option value='GA'><spring:message code="ssap.state.Georgia" text="Georgia" /></option>
							<option value='GU'><spring:message code="ssap.state.Guam" text="Guam" /></option>
							<option value='HI'><spring:message code="ssap.state.Hawaii" text="Hawaii" /></option>
							<option value='ID'><spring:message code="ssap.state.Idaho" text="Idaho" /></option>
							<option value='IL'><spring:message code="ssap.state.Illinois" text="Illinois" /></option>
							<option value='IN'><spring:message code="ssap.state.Indiana" text="Indiana" /></option>
							<option value='IA'><spring:message code="ssap.state.Iowa" text="Iowa" /></option>
							<option value='KS'><spring:message code="ssap.state.Kansas" text="Kansas" /></option>
							<option value='KY'><spring:message code="ssap.state.Kentucky" text="Kentucky" /> </option>
							<option value='LA'><spring:message code="ssap.state.Louisiana" text="Louisiana" /></option>
							<option value='ME'><spring:message code="ssap.state.Maine" text="Maine" /></option>
							<option value='MD'><spring:message code="ssap.state.Maryland" text="Maryland" /></option>
							<option value='MA'><spring:message code="ssap.state.Massachusetts" text="Massachusetts" /></option>
							<option value='MI'><spring:message code="ssap.state.Michigan" text="Michigan" /></option>
							<option value='MN'><spring:message code="ssap.state.Minnesota" text="Minnesota" /></option>
							<option value='MS'><spring:message code="ssap.state.Mississippi" text="Mississippi" /></option>
							<option value='MO'><spring:message code="ssap.state.Missouri" text="Missouri" /></option>
							<option value='MT'><spring:message code="ssap.state.Montana" text="Montana" /></option>
							<option value='NE'><spring:message code="ssap.state.Nebraska" text="Nebraska" /></option>
							<option value='NV'><spring:message code="ssap.state.Nevada" text="Nevada" /></option>
							<option value='NH'><spring:message code="ssap.state.NewHampshire" text="New Hampshire" /></option>
							<option value='NJ'><spring:message code="ssap.state.NewJersey" text="New Jersey" /></option>
							<option value='NM'><spring:message code="ssap.state.NewMexico" text="New Mexico" /></option>
							<option value='NY'><spring:message code="ssap.state.NewYork" text="New York" /></option>
							<option value='NC'><spring:message code="ssap.state.NorthCarolina" text="North Carolina" /></option>
							<option value='ND'><spring:message code="ssap.state.NorthDakota" text="North Dakota" /></option>
							<option value='MP'><spring:message code="ssap.state.NorthernMarianasIslands" text="Northern Marianas Islands" /></option>
							<option value='OH'><spring:message code="ssap.state.Ohio" text="Ohio" /></option>
							<option value='OK'><spring:message code="ssap.state.Oklahoma" text="Oklahoma" /></option>
							<option value='OR'><spring:message code="ssap.state.Oregon" text="Oregon" /></option>
							<option value='PA'><spring:message code="ssap.state.Pennsylvania" text="Pennsylvania" /></option>
							<option value='PR'><spring:message code="ssap.state.PuertoRico" text="Puerto Rico" /></option>
							<option value='RI'><spring:message code="ssap.state.RhodeIsland" text="Rhode Island" /></option>
							<option value='SC'><spring:message code="ssap.state.SouthCarolina" text="South Carolina" /></option>
							<option value='SD'><spring:message code="ssap.state.SouthDakota" text="South Dakota" /></option>
							<option value='TN'><spring:message code="ssap.state.Tennessee" text="Tennessee" /></option>
							<option value='TX'><spring:message code="ssap.state.Texas" text="Texas" /></option>
							<option value='UT'><spring:message code="ssap.state.Utah" text="Utah" /></option>
							<option value='VT'><spring:message code="ssap.state.Vermont" text="Vermont" /></option>
							<option value='VA'><spring:message code="ssap.state.Virginia" text="Virginia" /></option>
							<option value='VI'><spring:message code="ssap.state.VirginIslands" text="Virgin Islands" /></option>
							<option value='WA'><spring:message code="ssap.state.Washington" text="Washington" /></option>
							<option value='WV'><spring:message code="ssap.state.WestVirginia" text="West Virginia" /></option>
							<option value='WI'><spring:message code="ssap.state.Wisconsin" text="Wisconsin" /></option>
							<option value='WY'><spring:message code="ssap.state.Wyoming" text="Wyoming" /></option>
			    		</select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
				<div class="control-group">
					<label class="control-label">
						<spring:message code="ssap.page29.tribeName" text="Tribe Name" />
					</label><!-- end of label -->
					<div class="controls">
						<select class="input-medium" id="appscr79TribeName">
							<option value="TribeName"><spring:message code="ssap.page29.tribeName" text="Tribe Name" /></option>
							<option value="AlaskaNative"><spring:message code="ssap.page29.alaskaNative" text="Alaska Native" /></option>
							<option value="AmericanIndian"><spring:message code="ssap.page29.americanIndian" text="American Indian" /></option>
					    </select>
					</div><!-- end of controls-->
				</div><!-- control-group ends -->
				
			</div><!-- americonIndianQuestionDiv ends -->
		</form>
	</div>
</div>