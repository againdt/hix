package com.getinsured.hix.entity.enrollmententity.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.ISiteLanguagesRepository;
import com.getinsured.hix.entity.enrollmententity.service.SiteLangaugesService;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;


@Service("siteLanguageService")
@Transactional
public class SiteLanguagesServiceImpl implements SiteLangaugesService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteLanguagesServiceImpl.class);

	@Autowired
	private ISiteLanguagesRepository iSiteLanguagesRepository;
	
	/**
	 * @see SiteLangaugesService#findBySiteLanguagesId(int)
	 */
	@Override
	@Transactional
	public SiteLanguages findBySiteLanguagesId(long siteLanguagesId) {
		
		LOGGER.info("findBySiteLanguagesId: START");
		LOGGER.info("findBySiteLanguagesId: END");
		
		return  iSiteLanguagesRepository.findBySiteLanguagesId(siteLanguagesId);
	}

	/**
	 * @see SiteLangaugesService#saveSiteLanguage(SiteLanguages)
	 */
	@Override
	@Transactional
	public SiteLanguages saveSiteLanguage(SiteLanguages siteLanguages) {
		
		LOGGER.info("saveSiteLanguage: START");
		LOGGER.info("saveSiteLanguage: END");
		
		return iSiteLanguagesRepository.save(siteLanguages);
	}
	
	/**
	 * @see SiteLangaugesService#findSiteLanguagesBySite(Site)
	 */
	@Override
	@Transactional
	public SiteLanguages findSiteLanguagesBySite(Site site)
	{
		
		LOGGER.info("findSiteLanguagesBySite: START");
		LOGGER.info("findSiteLanguagesBySite: END");
		
		return iSiteLanguagesRepository.findSiteLanguagesBySite(site);
	}

}
