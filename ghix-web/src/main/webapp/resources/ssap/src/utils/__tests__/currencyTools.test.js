import currencyTools from '../currencyTools';

describe('Test for currencyTools', () => {

    const options = {
        maximumFractionDigits : 2,
        currency              : 'USD',
        style                 : 'currency',
        currencyDisplay       : 'symbol'
    };

    it('Options should match: ', () => {
        expect(currencyTools.options).toEqual(options);
    });

    it('LocalStringToNumber should convert string to number', () => {
        expect(currencyTools.localStringToNumber('23456')).toBe(23456);
    });

    it('LocalStringToNumber should convert "0" to number 0', () => {
        expect(currencyTools.localStringToNumber('0')).toBe(0);
    });

    it('LocalStringToNumber should convert string, i.e. (24756), to negative number', () => {
        expect(currencyTools.localStringToNumber('(24756)')).toBe(-24756);
    });

    it('Check if a valid type has been provided to localStringToNumber', () => {
        expect(() => {
            currencyTools.localStringToNumber({});
        }).toThrow('Not a string or number has been passed to currencyTool');
    });

    // it('Case 0. Check if getMoneyFormat returns a negative value', () => {
    //     expect(currencyTools.getMoneyFormat('(12345)')).toMatch('-US$ 12,345.00');
    // });
    //
    // it('Case 1. Check if getMoneyFormat returns a negative value if cents have been provided', () => {
    //     expect(currencyTools.getMoneyFormat(-12345, true)).toBe('(US$ 123.45)');
    // });
    //
    //
    // it('Case 2. Check if getMoneyFormat returns a positive value', () => {
    //     expect(currencyTools.getMoneyFormat('56926')).toBe('US$ 56,926.00');
    // });
    //
    // it('Case 3. Check if getMoneyFormat returns a positive value', () => {
    //     expect(currencyTools.getMoneyFormat(56926, true)).toBe('US$ 569.26');
    // });
    //
    //
    // it('Case 4. Check if getMoneyFormat returns a 0 for 0 value', () => {
    //     expect(currencyTools.getMoneyFormat('0')).toBe('US$ 0.00');
    // });
    //
    // it('Check if getMoneyFormat returns a 0 for 0 value', () => {
    //     expect(currencyTools.getMoneyFormat(0, true)).toBe('');
    // });

});