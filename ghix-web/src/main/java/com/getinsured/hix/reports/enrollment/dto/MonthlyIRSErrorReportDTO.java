package com.getinsured.hix.reports.enrollment.dto;

import java.io.Serializable;

public class MonthlyIRSErrorReportDTO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer month;
	private Integer year;
//	outboundFileName
	
	private Integer householdCaseId;
	private String errorCode;
	private String errorMessage;
	private String errorFieldName;
	private String xpathContent;
	private String batchId;
	
	
	public Integer getHouseholdCaseId() {
		return householdCaseId;
	}
	public void setHouseholdCaseId(Integer householdCaseId) {
		this.householdCaseId = householdCaseId;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	public String getErrorFieldName() {
		return errorFieldName;
	}
	public void setErrorFieldName(String errorFieldName) {
		this.errorFieldName = errorFieldName;
	}
	public String getXpathContent() {
		return xpathContent;
	}
	public void setXpathContent(String xpathContent) {
		this.xpathContent = xpathContent;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public Integer getMonth() {
		return month;
	}
	public void setMonth(Integer month) {
		this.month = month;
	}
	public Integer getYear() {
		return year;
	}
	public void setYear(Integer year) {
		this.year = year;
	}
}
