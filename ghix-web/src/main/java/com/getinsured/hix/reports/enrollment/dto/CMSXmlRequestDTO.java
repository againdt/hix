package com.getinsured.hix.reports.enrollment.dto;

import java.util.List;
import java.util.Map;

public class CMSXmlRequestDTO {

	private List<String> asc;
	private List<String> desc;
	private Map<String, String> filterExact;
	private Map<String, String> filterLike;
	
	public CMSXmlRequestDTO() {
		super();
	}

	public List<String> getAsc() {
		return asc;
	}
	public void setAsc(List<String> asc) {
		this.asc = asc;
	}

	public List<String> getDesc() {
		return desc;
	}

	public void setDesc(List<String> desc) {
		this.desc = desc;
	}

	public Map<String,String> getFilterExact() {
		return filterExact;
	}

	public void setFilterExact(Map<String,String> filterExact) {
		this.filterExact = filterExact;
	}

	
	public Map<String,String> getFilterLike() {
		return filterLike;
	}

	public void setFilterLike(Map<String,String> filterLike) {
		this.filterLike = filterLike;
	}
	
}
