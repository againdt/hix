<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />


				<div class="gutter10-lr">
				 <c:set var="encAssisterId" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
				 <c:set var="encAssisterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true"/> </c:set>
				<jsp:include page="/WEB-INF/views/entity/activationLinkResult.jsp" />
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/manageassister" />"><spring:message code="label.assister.manage"/></a></li>
								<li><a href="<c:url value="/entity/assisteradmin/viewassisterinformation?assisterId=${encAssisterId}" />">${assister.firstName} ${assister.lastName}</a></li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					
	<div class="row-fluid">
		      <h1><class name="skip"></a>${assister.firstName} ${assister.lastName}</h1>
	</div>
	
	<div class="row-fluid">
			<jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
			<!-- end of span3 -->
			<div class="span9">
			<form class="form-vertical" id="frmviewassisterprofile" name="frmviewassisterprofile" action="viewprofile" method="POST">
				<df:csrfToken/>
				<div class="content-header header"><h4 class="pull-left"><spring:message code="label.profile"/></h4>
				<c:choose>
					<c:when test="${CA_STATE_CODE}">
					</c:when>
					<c:otherwise>
						<c:if test="${loggedUser == 'assisterLogin' && assister.certificationStatus== 'Certified'}">
							<a class="btn btn-small pull-right" href="<c:url value="/entity/assister/editassisterprofile?assisterId=${encAssisterId}&entityId=${encAssisterEntityId}" />"><spring:message code="label.assister.edit"/></a>
						</c:if>
					</c:otherwise>
				</c:choose>
				
				</div>
				<div class="row-fluid gutter10-tb">
					<div class="span">
						<table class="table table-border-none span6">
							<tbody>
								<tr>
									<td class="txt-right vertical-align-top "><spring:url value="/entity/assisteradmin/photo/${encAssisterId}"
											var="photoUrl" /> <img src="${photoUrl}" alt='<spring:message code="label.assister.profileImgOfAssister" />'
										class="profilephoto thumbnail" style="max-width:none;"/>
									</td>
									<td class="vertical-align-top ">
										<h4 class="wrapword margin0">${assister.firstName} ${assister.lastName}</h4>
										<p>
											${assister.mailingLocation.address1}<br>
											<c:if test="${assister.mailingLocation.address2!=null}">
												${assister.mailingLocation.address2}<br> 
											</c:if>
											${assister.mailingLocation.city}<br>
											${assister.mailingLocation.state}<br>
											${assister.mailingLocation.zip}
										</p>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.PhoneNumber"/></td>
									<td>(${primaryPhone1})${primaryPhone2}-${primaryPhone3}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.emailAddress"/></td>
									<td>${assister.emailAddress}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.languageSpoken"/></td>
									<td class="comma">${assisterLanguages.spokenLanguages}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.languageWritten"/></td>
									<td class="comma">${assisterLanguages.writtenLanguages}</td>
								</tr>
								<tr>
									<td class="txt-right"><spring:message code="label.assister.education"/></td>
									<td>${assister.education}</td>
								</tr>
							</tbody>
						</table>
						<div class="span5 gutter10">
							<iframe width="190" height="190" frameborder="0" scrolling="no"
								marginheight="0" marginwidth="0" class="thumbnail"
								src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${assister.mailingLocation.address1}+${assister.mailingLocation.city},+${assister.mailingLocation.state}+${assister.mailingLocation.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${assister.mailingLocation.address1}+${assister.mailingLocation.city},+${assister.mailingLocation.state}+${assister.mailingLocation.zip}&amp;gl=us&amp;t=m&amp;ll=${assister.mailingLocation.lat},${assister.mailingLocation.lon}&amp;z=13&amp;iwloc=A&amp;output=embed" title="Assister Location">Assister Location</iframe>
						</div>
					</div>
				</div>
				</form>
			</div>
	</div>
	<!-- #row-fluid -->
</div>

<script type="text/javascript">
$(document).ready(function() {
	$("#assisterProfile").removeClass("link");
	$("#assisterProfile").addClass("active");
});	
</script>

<script>
$(".comma").text(function(i, val) {
    return val.replace(/,/g, ", ");
});
</script>

