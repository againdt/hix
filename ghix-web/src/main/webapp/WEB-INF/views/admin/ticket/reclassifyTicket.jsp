<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>
<input type="hidden" id="ticketId" name="ticketId" value="${encTicketid}"> 

<!-- Cancel confirmation dialog  -->
<div id="reclassifyDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="reclassifyDialog" aria-hidden="true" data-keyboard="false" data-backdrop="static">
	<div>
    	<div class="header">
            <h4 class="pull-left">Re-Classify</h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossCloseReclassify" class="dialogClose clearForm" title="x" type="button" onclick="javascript:cancelModel();"><spring:message code="btn.crossClose" /></button>
        </div>
    </div>
  
  <div class="modal-body">  
 		<div id="ticketReclasssifyDialogControls" >
		  <div class="control-group">
				<label class="control-label span4"><spring:message  code="label.ticketType"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label> 						
				<div class="controls" id="">
					<select class="input-medium" id="ticketType" onchange="javascript:loadSubTicketType();" >
									<option value=""><spring:message code="label.select" /></option>
					</select>			
				</div>
			</div>
			<div class="control-group" style="display:none;" id="ticketSubTypeDiv">	
				<label class="control-label span4"><spring:message  code="label.ticketSubType"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
				<div class="controls">
					<select class="input-medium" id="ticketSubType" >	</select>
				</div>
						
			</div>
		</div>	
		<div id="tktFormPropertiesDialog" >
		
		</div>
		
  </div>
  <div id="reclassifyDialogLoader" style="display:none;width:80px;height:80px;position:absolute;top:50%;left:50%;padding:2px;margin-top: -52px;margin-left: -47px;">
		<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
  </div>
  <div class="modal-footer clearfix">
    <button id="reclassifyDialogCancelButton" class="btn pull-left clearForm" data-dismiss="modal" aria-hidden="true" onclick="javascript:cancelModel();"><spring:message code='button.cancel' /></button>
   <%--  <button id="reclassifyBtn" class="btn btn-primary" onclick="javascript:reclassifySubmit();"><spring:message code='sp_button.submit' /></button> --%>
   <button id="reclassifyBtn" class="btn btn-primary abc" disabled="disabled" onclick="performReclassify();"><spring:message code='sp_button.submit' /></button>
  </div>
</div>
<!-- Cancel ticket dialog ends -->
<script type="text/javascript">
var queueName=null;
var taskClaimed=true;
var ticketTypeArray=new Array();
var ticketSubTypeArray=new Array();
var existingTicketId
var map = new Object()
function populateTicketType(){
	var validateUrl = '<c:url value="/ticketmgmt/ticket/fetchTicketTypes"></c:url>';
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />"
		},
		success: function(response,xhr){
			       if(isInvalidCSRFToken(xhr))
				      return;
					var subListJSON  = JSON.parse(response);
					for (var key in subListJSON) {
						map[key]=subListJSON[key]
					}
					loadTicketType();
   		},
  			error: function(e){
   			alert("Failed To Fetch Ticket Types for Ticket Re-Classify");
   		},
	});
}	

function cancelModel(){
	$('#reclassifyDialog').modal('hide');
	
	$('#ticketSubTypeDiv').hide();
	$('#tktFormPropertiesDialog').empty();
	$('#reclassifyBtn').attr("disabled","disabled");
	
}
function loadTicketType(){
	$('#ticketType')
    .find('option')
    .remove()
    .end()
    .append('<option value="Select">Select</option>')
    $('#ticketType')
    .find('option')
    .remove()
    .end()
    .append('<option value="Select">Select</option>')
	for (var key in map) {
	    if (key != 'Document Verification') {
	        $('#ticketType')
             .append($("<option></option>")
             .attr("value", key)
             .text(key));
	    };
	}
	
	$('#reclassifyDialog').modal('show');
	
}
function loadSubTicketType() {
	$('#reclassifyBtn').attr("disabled","disabled");
	var activitiUserCombo = $('#ticketSubType').length
	if(activitiUserCombo>0){
		$("#ticketSubType").empty();
		var ticketType = $("#ticketType option:selected").text();
		var ticketSubType = new Array();
		ticketSubType = map[ticketType];
		  $('#ticketSubType')
	      .append($("<option></option>")
	      .attr("value","")
	      .text("Select")); 
		  for (var i = 0; i < ticketSubType.length; i++) {
			  var ticketSubTypeValue = ticketSubType[i]
			    $('#ticketSubType')
		         .append($("<option></option>")
		         .attr("value",ticketSubTypeValue)
		         .text(ticketSubTypeValue)); 
			}
		
	}
	$( "#ticketSubTypeDiv" ).show();
	
}


	
function reclassifySubmit(){
	
	//Check if group is selected
	 if($('#ticketType').val()==''||$('#ticketType').val()=='Select'){
		 $('#reclassifyBtn').attr("disabled","disabled");
		 alert("Please select the Ticket Type");
		 return;
	 }
	 
	 //If task is claimed, check the users selection
		 //If the user is not selected
		 if($('#ticketSubType').length>0&&($('#ticketSubType').val()==''||$('#ticketSubType').val()=='Select')){
			 $('#reclassifyBtn').attr("disabled","disabled");
		 	alert("Please select the Ticket Sub TypeAssignee");
		 	return;
		} 	
	 
		 
	 getReclassificationParameters();
}

function getReclassificationParameters(){
	var validateUrl = '<c:url value="/ticketmgmt/ticket/fetchReclassfiedTargetProperties"></c:url>';
	var ticketTypeValue = $("#ticketType").val();
	var ticketSubTypeValue=$('#ticketSubType').val();
	existingTicketId=$("#ticketId").val();
	
	
	$.ajax({
		url: validateUrl,
		type : "POST",
		data: {${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				ticketType : ticketTypeValue,
			   ticketSubType : ticketSubTypeValue,
			   ticketId : existingTicketId},
	    success: function(response,xhr){
			       if(isInvalidCSRFToken(xhr))
				      return;
		    	 	showTicketProperties(response);
		},
		});
	
}

function showTicketProperties(response){
	var ticketFormProperties = JSON.parse(response);
	for (var i = 0 ; i < ticketFormProperties.length ; i++){
			var propertyLabel = document.createElement("label");
			
			propertyLabel.className="control-label span4";
			if(ticketFormProperties[i].type=="enum"){
				var controlDiv = document.createElement("div");
				controlDiv.className = "control-group";
				propertyLabel.innerHTML=ticketFormProperties[i].name
				$('#tktFormPropertiesDialog').append(propertyLabel);
				newSelect = document.createElement("SELECT");
				$(newSelect).data("selectKey",ticketFormProperties[i].id);
				newSelect.name="selectField"
					for(j=0 ; j<ticketFormProperties[i].values.length;j++)
					{
					   var opt = document.createElement("option");
					   opt.value= j;
					   opt.innerHTML = ticketFormProperties[i].values[j]; // whatever property it has
					   newSelect.appendChild(opt);
					}
				$('#tktFormPropertiesDialog').append(newSelect);
				td=document.createElement("br");
				$('#tktFormPropertiesDialog').append(td);
				
			}
			else{
						propertyLabel.innerHTML=ticketFormProperties[i].name
						var controlDiv = document.createElement("div");
						controlDiv.className = "control-group";
					//	$('#tktFormPropertiesDialog').append(propertyLabel);
						controlDiv.appendChild(propertyLabel);
						var divv = document.createElement("div");
						divv.className = "controls";
						txtField = document.createElement("input");
						txtField.name="formPropertyTextField";
						$(txtField).data("txtKey",ticketFormProperties[i].id);
						if(ticketFormProperties[i].values[0]== null || ticketFormProperties[i].values[0]==''){
							txtField.value='';
						}
						else{
							txtField.value=ticketFormProperties[i].values[0];
							txtField.id='disabledTextProperty'
							txtField.setAttribute('disabled', 'disabled');
						}
						
						divv.appendChild(txtField);
						
						controlDiv.appendChild(divv)
						
						$('#tktFormPropertiesDialog').append(controlDiv);
						
						td=document.createElement("br");
						$('#tktFormPropertiesDialog').append(td);	
			}
		td=document.createElement("br");
		$('#markCompleteDialogcontrols').append(" <div hidden=true id=flag></div>");
	}
	$('#reclassifyBtn').removeAttr("disabled");
}

function performReclassify(){
	
	
	var myObject = new Object();

	var textfields =[]
	var formPropertiesMap = {};
	
	dropdownfields = document.getElementsByName("selectField");
	textfields = document.getElementsByName("formPropertyTextField");
	
	for(var i=0 ; i<dropdownfields.length; i++) {
	    selectedValue = dropdownfields[i].options[dropdownfields[i].selectedIndex].text;
	    formPropertiesMap[$(dropdownfields[i]).data("selectKey")] = selectedValue;
	}
	
	for(var i=0 ; i<textfields.length; i++) {
		if(textfields[i].id!="disabledTextProperty"){
		    selectedValue = textfields[i].value;
		    if (!selectedValue.trim()) {
		        alert("Please provide value for all properties");
		        return;
		    }
		    formPropertiesMap[$(textfields[i]).data("txtKey")] = selectedValue;
		} else {
			selectedValue = textfields[i].value;
		    if (selectedValue.trim()) {
				formPropertiesMap[$(textfields[i]).data("txtKey")] = selectedValue;
		    }
		}
	}

	
	
	
	myObject.formPropertiesMap = formPropertiesMap;
	
	var myStringJSON = JSON.stringify(myObject);
	var validateUrl = '<c:url value="/ticketmgmt/ticket/submitTicketReclassifyAction"></c:url>';
	var ticketTypeValue = $("#ticketType").val();
	var ticketSubTypeValue=$('#ticketSubType').val();
	
	
	$.ajax({
		url: validateUrl,
		type : "POST",
		data: {${df:csrfTokenParameter()} : "<df:csrfToken plainToken="true" />",
				ticketType : ticketTypeValue,
			   ticketSubType : ticketSubTypeValue,
			   ticketId : existingTicketId,
			   propertiesMap : myStringJSON},
	    success: function(response,xhr){
			       if(isInvalidCSRFToken(xhr))
				      return;	
			       $('#reclassifyDialog').modal('hide');	
			       notifyReClassifyResponse(true);
		},
		});
	
}

$('#ticketSubType').change(function(){
	$('#tktFormPropertiesDialog').empty();
	reclassifySubmit();
});

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
	rv = true;
	}
	return rv;
}

</script>