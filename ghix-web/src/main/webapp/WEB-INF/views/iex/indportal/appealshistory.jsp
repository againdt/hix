<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="myappeals">
<div class="row-fluid dashboard-rightpanel" ng-init="showAppeals()">
  <div class="header ng-scope">
    <h4><spring:message code="indportal.appealshistory.myappeals" javaScriptEscape="true"/></h4>
  </div>

  <div id="appealsHistoryIndPortal"  >
    <div class="dashboard-rightpanel" id="rightpanel">
      <div class="well">
        <div class="gutter20">
          <div ng-if="appealsDetails.length === 0">
            There is no appeal to show at the moment.
          </div>
          <table class="table table-striped" ng-if="appealsDetails.length>0">
            <thead>
              <tr class="header">
                <th scope="col"><spring:message code="indportal.appealshistory.appeal" javaScriptEscape="true"/></th>
                <th scope="col"><spring:message code="indportal.appealshistory.appealdesc" javaScriptEscape="true"/></th>
                <th scope="col"><spring:message code="indportal.appealshistory.datecreated" javaScriptEscape="true"/></th>
                <th scope="col"><spring:message code="indportal.appealshistory.currentstatus" javaScriptEscape="true"/></th>
                <th scope="col"><spring:message code="indportal.appealshistory.dateclosed" javaScriptEscape="true"/></th>
              </tr>
            </thead>
            <tbody ng-repeat="appeal in appealsDetails">
              <tr >
                <td scope="row">{{appeal.appealId}}</td>
                <td>{{appeal.appealDescription}}</td>
                <td>{{appeal.createdDate}}</td>
								<td>{{appeal.currentStatus}}</td>
                <td>{{appeal.appealClosedDate}}</td>
              </tr>
            </tbody>
          </table>
          <!--  well-steps ends -->
        </div>
      </div><!--  well ends -->
    </div>
  </div>
</div>
</script>
