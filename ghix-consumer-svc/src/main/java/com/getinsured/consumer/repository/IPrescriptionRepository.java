package com.getinsured.consumer.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.consumer.Prescription;


/**
 * This is the repository for CMR_PRESCRIPTIONS table
 * @author Suhasini Goli
 * @since  12th Dec, 2013
 */
@Repository
@Transactional
public interface IPrescriptionRepository extends JpaRepository<Prescription, Long>{

	
	/**
	 * Fetches the list of Prescriptions for a given cmr_household_id
	 * @param  
	 * @return List of Prescriptions associated with that user
	 */
	List<Prescription> findByHousehold(Household household);
	
	
	@Query("FROM Prescription p where p.household.id = :householdId")
	List<Prescription> findByHouseholdId(@Param("householdId") Integer householdId); 
	
		
	@Query("FROM Prescription p where UPPER(p.prescriptionName) LIKE '%' || UPPER(:prescriptionName) || '%' and p.household.id = :householdId)")
	Prescription findByPrescriptionAndHousehold(@Param("prescriptionName") String prescriptionName,  
			                                    @Param("householdId") Integer householdId);
	       	
}

