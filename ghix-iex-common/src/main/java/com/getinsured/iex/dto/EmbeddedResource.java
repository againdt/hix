package com.getinsured.iex.dto;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class EmbeddedResource extends ResourceSupport{

	public Object _embedded;

	public Object get_embedded() {
		return _embedded;
	}

	public void set_embedded(Object _embedded) {
		this._embedded = _embedded;
	}
}

