
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class ChpInsurance {

    @Expose
    private String reasonInsuranceEndedOther;
    @Expose
    private Boolean insuranceEndedDuringWaitingPeriodIndicator=false;

    public String getReasonInsuranceEndedOther() {
        return reasonInsuranceEndedOther;
    }

    public void setReasonInsuranceEndedOther(String reasonInsuranceEndedOther) {
        this.reasonInsuranceEndedOther = reasonInsuranceEndedOther;
    }

    public Boolean getInsuranceEndedDuringWaitingPeriodIndicator() {
        return insuranceEndedDuringWaitingPeriodIndicator;
    }

    public void setInsuranceEndedDuringWaitingPeriodIndicator(Boolean insuranceEndedDuringWaitingPeriodIndicator) {
        this.insuranceEndedDuringWaitingPeriodIndicator = insuranceEndedDuringWaitingPeriodIndicator;
    }

}
