var incomeNumber = 0;		
	var deductionNumber = 0;
	var totalIncomes = new Array();
	var totalIncomeDeductions = new Array();
	var job_income_name='';


	var financial_DetailsArray = new Array();
	financial_DetailsArray[0] = 'Financial_NoneFinancial_None';
	financial_DetailsArray[1]=  'Financial_Job';
	financial_DetailsArray[2] = 'Financial_Self_Employment';
	financial_DetailsArray[3] = 'Financial_SocialSecuritybenefits';
	financial_DetailsArray[4] = 'Financial_Unemployment';
	financial_DetailsArray[5] = 'Financial_Retirement_pension';
	financial_DetailsArray[6] = 'Financial_Capitalgains';
	financial_DetailsArray[7] = 'Financial_InvestmentIncome';
	financial_DetailsArray[8] = 'Financial_RentalOrRoyaltyIncome';
	financial_DetailsArray[9] = 'Financial_FarmingOrFishingIncome';
	financial_DetailsArray[10] = 'Financial_AlimonyReceived';
	financial_DetailsArray[11] = 'Financial_OtherIncome';
	
	var indicationIncomeObjectArray = new Array();
	var incomeAmount = new Object();
	var incomeFrequency = new Object();
	
	

var financialHouseHoldDone = 1;

function addFinancialHouseHold() {	
	
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	
	/*$('.parsley-error-list').hide();*/
	if((currentPage == "appscr73" || ($('input[name=appscr70radios4Income]:radio:checked').val() == 'No') && currentPage == "appscr70") && (editFlag == true || flagToUpdateFromResult== true) && useJSON == false)					// Edit functionality code
	{	
			appscr74EditUpdateValues(editMemberNumber);
			showFinancialDetailsSummary();
		
			if(flagToUpdateFromResult == true)
				{
				flagToUpdateFromResult=false;
				if(checkStillUpdateRequired())
					{
					
					return false;
					}
				else
					{
						goToPageById('appscrBloodRel');
						$('#finalEditDiv').show();
						$('#contBtn').text('Submit');
						$('#prevBtn').hide();
					return false;
					}
				
				}
			else
			goToPageById('appscr74');
			editFlag = false;
			return false;
	}
	else if(editFlag == true || flagToUpdateFromResult== true)
	{
		$('#back_button_div').show();
		if(editMemberNumber == 1)
			job_income_name = $("#Financial_Job_EmployerName_id").val();
	}
	
	if(validation(currentPage)==false)  
		return false;
	$('#prevBtn').attr('onclick','prev();');

	$('#goBack').show();
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	
	if (currentPage == 'appscr71') {
		
		
		if(editFlag == false)
		{
			if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined ){		
				totalIncomeAt71(financialHouseHoldDone); }
			else
			{
				editFlag = true;
				totalIncomeAt71(financialHouseHoldDone-1);
				editFlag = false;
			}
			
		}
		else
		{
			totalIncomeAt71(financialHouseHoldDone);
			
		}
	}
	else if(currentPage == 'appscr70')
		{
		
			if($('#Financial_None').is(":checked") == true)
			{
				
				if(editFlag == true)
				{
				
					appscr74EditUpdateValues(editMemberNumber);
					showFinancialDetailsSummary();
					goToPageById('appscr74');
					editFlag = false;
					return false;
				}
				totalIncomeAt71(financialHouseHoldDone);
				
				continueData71();
				continueData72();
				continueData73();
				next();
				
				
				if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined )
				{
				addCurrentIncomeDetail(); 
				}
				else
					{
					updateCurrentIncomeDetail();
					}
				setJobIncomeExistDetails();
				next();
				
				if( $("#incomesStatus"+financialHouseHoldDone).html() == undefined)
				{
				setFinancialIncomeDetails();
				incomeDidcrepancies();
				}
				else
				{
				updateFinancialIncomeDetails();
				updateIncomeDiscrepancies();
				}
				
				if (currentPage == 'appscr73' && financialHouseHoldDone < noOfHouseHold) {
					financialHouseHoldDone++;
					resetAddCurrentIncomeDetail();
					resetPage71Values();
					resetPage69Values();
					
					if(editFlag == true)
					{
						showFinancialDetailsSummary();
						next();
						editFlag = false;
					}else{					
						goToPageById('appscr69');
					}
					
					//if(havaData == true)
					//{
						haveDataFlag = true;
						if(financialHouseHoldDone<=houseHoldNumberValue)
							appscr74EditController(financialHouseHoldDone);
						haveDataFlag = false;
					//}
				}
				else
					{
						showFinancialDetailsSummary();
						if(totalIncomes.length == noOfHouseHold && currentPage == 'appscr73')
							totalIncomeShowMethod();			
						setEmployerNameToAll();
						next();
					
					}

				setNonFinancialPageProperty(financialHouseHoldDone);
			}
			else
				showFinancialDiv();
		}
	else if(currentPage == 'appscr72')
		{		
		if(editFlag ==  false)
		{
			if($('#appscr57HouseHoldalimonyAmountTD'+financialHouseHoldDone).html()==undefined )
				{
			
					addCurrentIncomeDetail();
				}
					
		
			else 
				{
		
				updateCurrentIncomeDetail();
				}
		}
			setJobIncomeExistDetails();
		}
	
	else if(currentPage == 'appscr73')
	{
		
	
		if( $("#incomesStatus"+financialHouseHoldDone).html() == undefined)
		{
		setFinancialIncomeDetails();
		incomeDidcrepancies();
		
		}
		else
		{
		updateFinancialIncomeDetails();
		updateIncomeDiscrepancies();
		}
	}

	if (currentPage == 'appscr73' && financialHouseHoldDone < noOfHouseHold) {

		
		financialHouseHoldDone++;

		resetAddCurrentIncomeDetail();
		 resetPage71Values();
		 resetPage69Values();
		 resetPage72DeductionValues();
		 if(editFlag == true)
			{
				showFinancialDetailsSummary();
				next();
				editFlag = false;
			}else{					
				goToPageById('appscr69');
			}
		
		
		
		//if(haveData == true)
		//{
			haveDataFlag = true;
			
			if(useJSON == true){
					appscr74EditController(financialHouseHoldDone);
			}else {
				if(financialHouseHoldDone<=houseHoldNumberValue)
					appscr74EditController(financialHouseHoldDone);	
			}					
			
			
			haveDataFlag = false;
		//}
	}
	else
		{
		
			showFinancialDetailsSummary();
			
			if(totalIncomes.length == noOfHouseHold && currentPage == 'appscr73')
				totalIncomeShowMethod();	
		
			
			setEmployerNameToAll();
			
		next();
	
		}

	setNonFinancialPageProperty(financialHouseHoldDone);

}

function showFinancialDiv() {
	var incomeCBStatus = new Object();
	var dataIndex=0;
	
	hideAllFinancial(financial_DetailsArray);
	
	for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		var data = financial_DetailsArray[dataIndex];

	
		
		if(dataIndex==0)
			{
			if ($('#'+data).is(':checked')) { 
				hideAllFinancial(financial_DetailsArray);
				var finData=0;
				for ( ;finData < financial_DetailsArray.length ; finData++) {
					
					if(finData==0)
						continue;
					var myData = financial_DetailsArray[finData];
				
					
					$('#' + myData + '_id').val('');
					
					$('#' + myData + '_select_id').val(jQuery.i18n.prop('ssap.SelectFrequency')); //KEY
				
				}
				
				
				
				
				break;
			}
			else
				{
				continue;
				}
			}
		
		
		if ($('#' + data).is(':checked')) {
			incomeCBStatus[data] = "true";
			
			$('#' + data + '_Div').show();
			//$('#' + data + '_Div').css("margin-bottom","160px");
		}
		else
			{
			incomeCBStatus[data] = "false";
			incomeAmount[data]=0;
			incomeFrequency[data]='N/A';
			
			$('#' + data + '_id').val('');
			$('#' + data + '_select_id').val(jQuery.i18n.prop('ssap.SelectFrequency')); //KEY
			
			$('#' + data + '_Div').hide();
			$('#' + data + '_Div').css("margin-bottom","2%");
			}

	}

	//var RowId =($('#houseHoldTRNo').val()).substring(9);
	var RowId = getCurrentApplicantID();
	indicationIncomeObjectArray[parseInt(RowId)-1]=incomeCBStatus;

}

function hideAllFinancial(financial_DetailsArray){

	
	var dataI=0;
	for ( ;dataI < financial_DetailsArray.length ; dataI++) {
		
		if(dataI==0)
			continue;
		var data = financial_DetailsArray[dataI];
		$('#' + data + '_Div').hide();
		
	}
	
}
var financial_styleClass = new Array();

financial_styleClass[0] = "class='income'";
financial_styleClass[1] = "class='i_job'";
financial_styleClass[2] = "class='i_self'";
financial_styleClass[3] = "class='i_social'";
financial_styleClass[4] = "class='i_unemploy'";
financial_styleClass[5] = "class='i_pension'";
financial_styleClass[6] = "class='i_capital'";
financial_styleClass[7] = "class='i_investment'";
financial_styleClass[8] = "class='i_rental'";
financial_styleClass[9] = "class='i_farming'";
financial_styleClass[10] = "class='i_alimony'";
financial_styleClass[11] = "class='i_other'";


function setFinancialIncomeDetails()
{
	

	
		var dataIndex=0;
		var appendRowData='';
	
		appendRowData+="<td "+styleClass+" id='incomesStatus"+financialHouseHoldDone+"'>"+$('input[name=appscr70radios4Income]:radio:checked').val()+"</td>";
		
		for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		
		var styleClass=financial_styleClass[dataIndex];
		if(dataIndex==0)
		{
			var irsIncome= $('#irsIncome').val();
				irsIncome = (irsIncome == '')?0:irsIncome;
		appendRowData+="<td "+styleClass+" id='appscrirsIncome57TD"+financialHouseHoldDone+"'>"+irsIncome+"</td>";
		
		$('#irsIncome').val('');
		
		continue;
		}
		var data = financial_DetailsArray[dataIndex];
		var fieldData = $('#'+data+'_id').val();
	
		if(fieldData == '' || fieldData == undefined)
			{
			
			fieldData = 0;
			}
			
		incomeAmount[data]=fieldData;
		
		
		var fieldFrequencyData= $('#'+data+'_select_id').val();
	
		incomeFrequency[data]=fieldFrequencyData;
		
		if(dataIndex == 1)
		{
			appendRowData+="<td class='employerName' id='appscr"+data+"EmployerName57TD"+financialHouseHoldDone+"'>"+$('#Financial_Job_EmployerName_id').val()+"</td>";
			appendRowData+="<td class='hoursOrDays' id='appscr"+data+"HoursOrDays57TD"+financialHouseHoldDone+"'>"+$('#Financial_Job_HoursOrDays_id').val()+"</td>";
		}
		else if(dataIndex == 2)
			appendRowData+="<td class='typeOfWork' id='appscr"+data+"TypeOfWork57TD"+financialHouseHoldDone+"'>"+$('#Financial_Self_Employment_TypeOfWork').val()+"</td>";
		else if(dataIndex == 4)
		{
			appendRowData+="<td class='stateGovt' id='appscr"+data+"StateGovernment57TD"+financialHouseHoldDone+"'>"+$('#Financial_Unemployment_StateGovernment').val()+"</td>";
			appendRowData+="<td class='unemploymentIncomeDate' id='appscr"+data+"Date57TD"+financialHouseHoldDone+"'>"+$('#unemploymentIncomeDate').val()+"</td>";
		}
		else if(dataIndex == 11)
			appendRowData+="<td class='otherTypeIncome' id='appscr"+data+"OtherTypeIncome57TD"+financialHouseHoldDone+"'>"+$('#Financial_OtherTypeIncome').val()+"</td>";
		
		
			appendRowData+="<td "+styleClass+" id='appscr"+data+"57TD"+financialHouseHoldDone+"'>"+fieldData+"</td>";
		
			if(dataIndex!=2 && dataIndex!=6)
				appendRowData+="<td class='"+data+"Frequency' id='appscr"+data+"Frequency57TD"+financialHouseHoldDone+"'>"+fieldFrequencyData+"</td>";
		}
		/*var expectedIncomeByHouseHoldMember= $('#expectedIncomeByHouseHoldMember').val();
		if(expectedIncomeByHouseHoldMember=='')
			expectedIncomeByHouseHoldMember="N/A";
		appendRowData+="<td class='i_expectedIncome' id='appscrexpectedIncome57TD"+financialHouseHoldDone+"'>"+expectedIncomeByHouseHoldMember+"</td>";*/
		
		//$('#appscr57HouseHoldPersonalInformation').find('#'+$('#houseHoldTRNo').val()).append(appendRowData);
		$('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+getCurrentApplicantID()).append(appendRowData);
	
}

function resetAddCurrentIncomeDetail()
{

}


function totalIncomeAt71(incomeArrayNumber)
{
	
	if(useJSON == true){
		totalIncomeAt71JSON(incomeArrayNumber);
		return;
	}
	
	var totalIncome71 = 0;
		
	for(var i=1; i<financial_DetailsArray.length; i++)
	{
		if($('#'+financial_DetailsArray[i]).is(":checked") == true)
		{	
			var frquency = $('#'+financial_DetailsArray[i]+'_select_id').val();
			var income = $('#'+financial_DetailsArray[i]+'_id').val();
			
			if(income == "")
				income = 0;
			
			
			if(financial_DetailsArray[i] == 'Financial_Self_Employment')
			{
				totalIncome71 = totalIncome71 + ($('#'+financial_DetailsArray[i]+'_id').val())*12;
				
			
			}
			else if(financial_DetailsArray[i] == 'Financial_Capitalgains')
			{
				totalIncome71 = totalIncome71 + ($('#'+financial_DetailsArray[i]+'_id').val())*1;
				
			
			}
			else if(!(frquency == "SelectFrequency"))
			{
			
				totalIncome71 = totalIncome71 + frequencyIncome(frquency, income);
				
			}
	
		}
	}
	
	totalIncome71 = (totalIncome71/12).toFixed(2);
	
	$('#totalIncomeAt72LabelHead').html('<h3> $  '+totalIncome71+' /month </h3>');
	$('#totalIncomeAt72Label').text('$  '+totalIncome71+' /month');
	$('.amountAt72PerYear').text('$  '+(totalIncome71*12));

	
	checkBoxSameChecke(financial_DetailsArray);
}

function totalIncomeToArray(arrayIndex)
{
	var idIndex = 1;
	
	var totalIncome = 0.0;
	var frequencyF = "";
	var income = 0;
	var vId = "";
	
	for(; idIndex<financial_DetailsArray.length; idIndex++)
	{
		vId = financial_DetailsArray[idIndex];
		
		income = $('#appscr'+vId+'57TD'+arrayIndex).text();
		frequencyF = $('#appscr'+vId+'Frequency57TD'+arrayIndex).text();
		

		if(financial_DetailsArray[idIndex] == 'Financial_Self_Employment')
		{
			totalIncome+= income*12;
		}
		else if(financial_DetailsArray[idIndex] == 'Financial_Capitalgains')
		{
			totalIncome+= income*1;
		}
		else
		{
			totalIncome+= frequencyIncome(frequencyF, income);
		}
		
	
	}
	
	totalIncomes[(arrayIndex-1)] = (totalIncome/12).toFixed(2);
}



function totalDeductionInArray(arrayIndex)
{
	var totalDeduction = 0.0;
	var frequencyF = "";
	var income = 0;
	
	income = $('#appscr57HouseHoldalimonyAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHoldalimonyFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);
	
	income = $('#appscr57HouseHoldstudentLoanAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHoldstudentLoanFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);
	
	
	
	income = $('#appscr57HouseHolddeductionsAmountTD'+arrayIndex).text();
	frequencyF = $('#appscr57HouseHolddeductionsFrequencyTD'+arrayIndex).text();
	totalDeduction+= frequencyIncome(frequencyF, income);	
	totalIncomeDeductions[(arrayIndex-1)] = (totalDeduction/12).toFixed(2);	
}


function frequencyIncome(frequency, income)
{
	if(frequency == 'Hourly')
	{
		return income*8640;
	}
	else if(frequency == 'Daily')
	{
		return income*365;
	}
	else if(frequency == 'Weekly')
	{
		return income*52;
	}
	else if(frequency == 'EveryTwoWeeks')
	{
		return income*26;
	}
	else if(frequency == 'Monthly')
	{
		return income*12;
	}
	else if(frequency == 'Yearly')
	{
		return income*1;
	}
	else if(frequency == 'OneTimeOnly')
	{
		return income*1;
	}
	else if(frequency == 'TwiceAMonth')
	{
		return income*24;
	}
	else if(frequency == 'Quarterly')
	{
		return income*3;
	}
	else
		return 0.0;
}


function checkBoxSameChecke(idOfChecks)
{
	for(var i=1; i<idOfChecks.length; i++)
	{
		if($('#'+idOfChecks[i]).is(":checked") == true)
		{
			document.getElementById(idOfChecks[i]+'1').checked = true;			
		}
		else
		{
			document.getElementById(idOfChecks[i]+'1').checked = false;
		}
		$('#'+idOfChecks[i]+'1').attr("disabled",true);
	}
	if($('#Financial_None').is(":checked") == true)
		document.getElementById('Financial_None1').checked = true;
	else
		document.getElementById('Financial_None1').checked = false;
	
	$('#Financial_None1').attr("disabled",true);
	
}



function setJobIncomeExistDetails()
{
	//var index =($('#houseHoldTRNo').val()).substring(9);	
	var index = getCurrentApplicantID();
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[index-1];
	var job_income = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	//var job_income= $('#'+'Financial_Job'+'_id').val();
	//if(index==1)
	//	job_income_name= $("#Financial_Job_EmployerName_id").val();
	job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;	
	 
	if(job_income=='0' || job_income=='' || job_income== undefined){
		$('#appscr73EmployerNameDiv').hide();
	}
	else
		{		
		$('.appscr73EmployerName').html(job_income_name);
		$('#appscr73EmployerNameDiv').show();
		}
	
	
}



function setHouseHoldData()
{

	//var index =($('#houseHoldTRNo').val()).substring(9),
	var index = getCurrentApplicantID(),
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[index-1],
		job_income_name = householdMemberObj.detailedIncome.jobIncome[0].employerName;
	
	$('.houseHoldNameHere').html('');
	$('.houseHoldNameHere').html(job_income_name);
	$('#houseHoldNameHere2').html(job_income_name);
	
	/*if(job_income_name=='')
		{
		$(".memberNameDataDiv").hide();
		$("#memberNameDataDivHide").hide();
		}
	else
		{
	//	$(".memberNameDataDiv").show();
	//	$("#memberNameDataDivHide").show();
		}*/
	
	if(householdMemberObj.detailedIncome.jobIncomeIndicator == true){
		$("#employerOfJobDiv").show();
	}else{
		$("#employerOfJobDiv").hide();
	}
}

function setEmployerNameToAll()
{
	$('.EmployerNameHere').html(job_income_name);
}

function checkEmployerNameExist()
{
	
if(job_income_name=='' || job_income_name ==undefined)
	{
	
	next();
	}
if(currentPage == "appscr77Part1"){
	continueData77Part1();
}
next();
}

function totalIncomeAt71JSON(incomeArrayNumber)
{	
	var totalIncome71 = getFamilyMemberIncome(incomeArrayNumber);
	
	totalIncome71 = (totalIncome71/12).toFixed(2); //income per month
	
	$('#totalIncomeAt72LabelHead').html('<h3> $  '+ Moneyformat(totalIncome71)+' /month </h3>');
	$('#totalIncomeAt72Label').text('$  '+Moneyformat(totalIncome71)+' /month');
	$('.amountAt72PerYear').text('$  '+Moneyformat(totalIncome71*12));

	
	checkBoxSameChecke(financial_DetailsArray);
}

function getFamilyMemberIncome(familyMemberNo){
	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[familyMemberNo-1];
	
	if(typeof householdMemberObj == 'undefined'){
		return 0;
	}
	
	var editJobIncome 							  = householdMemberObj.detailedIncome.jobIncome[0].incomeAmountBeforeTaxes;
	var editJobFrequency 						  = householdMemberObj.detailedIncome.jobIncome[0].incomeFrequency;
	var editSelfEmploymentIncome 				  = householdMemberObj.detailedIncome.selfEmploymentIncome.monthlyNetIncome;	
	var editSocialSecurityBenefitsIncome 		  = householdMemberObj.detailedIncome.socialSecurityBenefit.benefitAmount;
	var editSocialSecurityBenefitsIncomeFrequency = householdMemberObj.detailedIncome.socialSecurityBenefit.incomeFrequency;
	var editUnemploymentIncome 					  = householdMemberObj.detailedIncome.unemploymentBenefit.benefitAmount;
	var editUnemploymentIncomeFrequency 		  = householdMemberObj.detailedIncome.unemploymentBenefit.incomeFrequency;	
	var editRetirementPensionIncome 			  = householdMemberObj.detailedIncome.retirementOrPension.taxableAmount;
	var editRetirementPensionIncomeFrequency 	  = householdMemberObj.detailedIncome.retirementOrPension.incomeFrequency;
	var editCapitalgainsIncome 					  = householdMemberObj.detailedIncome.capitalGains.annualNetCapitalGains;
	var editInvestmentIncome 					  = householdMemberObj.detailedIncome.investmentIncome.incomeAmount;
	var editInvestmentIncomeFrequency 			  = householdMemberObj.detailedIncome.investmentIncome.incomeFrequency;
	var editRentalOrRoyaltyIncome 				  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.netIncomeAmount;
	var editRentalOrRoyaltyIncomeFrequency 		  = householdMemberObj.detailedIncome.rentalRoyaltyIncome.incomeFrequency;
	var editFarmingOrFishingIncome 				  = householdMemberObj.detailedIncome.farmFishingIncome.netIncomeAmount;
	var editFarmingOrFishingIncomeFrequency 	  = householdMemberObj.detailedIncome.farmFishingIncome.incomeFrequency;
	var editAlimonyReceivedIncome 				  = householdMemberObj.detailedIncome.alimonyReceived.amountReceived;
	var editAlimonyReceivedIncomeFrequency 		  = householdMemberObj.detailedIncome.alimonyReceived.incomeFrequency;
	
	//We don't have other income in JSON
	var editOtherIncome 						  = $('#appscrFinancial_OtherIncome57TD'+i).text();
	var editOtherIncomeFrequency 				  = $('#appscrFinancial_OtherIncomeFrequency57TD'+i).text();
	
	
	
	
	var totalIncome71 = 0;
	
	if(editJobIncome != ""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editJobFrequency, editJobIncome);
	}
	
	if(editSelfEmploymentIncome!=""){		
		totalIncome71 = totalIncome71 + (editSelfEmploymentIncome*12);
	}
	
	if(editSocialSecurityBenefitsIncome!=""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editSocialSecurityBenefitsIncomeFrequency, editSocialSecurityBenefitsIncome);
	}
	
	if(editUnemploymentIncome != "") {		
		totalIncome71 = totalIncome71 + frequencyIncome(editUnemploymentIncomeFrequency, editUnemploymentIncome);
	}
	
	if(editRetirementPensionIncome != ""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editRetirementPensionIncomeFrequency, editRetirementPensionIncome);
	}
	
	if(editCapitalgainsIncome != "") {		
		totalIncome71 = totalIncome71 + editCapitalgainsIncome*1;
	}
	
	if(editInvestmentIncome != ""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editInvestmentIncomeFrequency, editInvestmentIncome);
	}
	
	if(editRentalOrRoyaltyIncome!=""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editRentalOrRoyaltyIncomeFrequency, editRentalOrRoyaltyIncome);
	}
	
	if(editFarmingOrFishingIncome != ""){	
		totalIncome71 = totalIncome71 + frequencyIncome(editFarmingOrFishingIncomeFrequency, editFarmingOrFishingIncome);
	}
	
	if(editAlimonyReceivedIncome!=""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editAlimonyReceivedIncomeFrequency, editAlimonyReceivedIncome);
	}
	
	if(editOtherIncome != ""){		
		totalIncome71 = totalIncome71 + frequencyIncome(editOtherIncomeFrequency, editOtherIncome);
	}
	
	if(householdMemberObj.detailedIncome.otherIncome[0].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[0].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[0].incomeAmount);
	}
	
	if(householdMemberObj.detailedIncome.otherIncome[1].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[1].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[1].incomeAmount);
	}
	
	if(householdMemberObj.detailedIncome.otherIncome[2].incomeAmount != ""){
		totalIncome71 = totalIncome71 + frequencyIncome(householdMemberObj.detailedIncome.otherIncome[2].incomeFrequency, householdMemberObj.detailedIncome.otherIncome[2].incomeAmount);
	}
	
	//totalIncome71 = (totalIncome71/12).toFixed(2);
	
	return getDecimalNumber(totalIncome71); //family member yearly income
	
}

function setTotalFamilyIncomeJSON() {
	
	totalFamilyIncome = 0;	
	
	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	
	for ( var cnt = 1; cnt <= noOfHouseHold; cnt++) {
		totalFamilyIncome = totalFamilyIncome + getFamilyMemberIncome(cnt);		
	}
	
	webJson.singleStreamlinedApplication.taxHousehold[0].houseHoldIncome = getDecimalNumber(totalFamilyIncome);
	
}