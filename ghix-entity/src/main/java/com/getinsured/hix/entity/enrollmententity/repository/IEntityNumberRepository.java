package com.getinsured.hix.entity.enrollmententity.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.entity.EntityNumber;

public interface IEntityNumberRepository extends RevisionRepository<EntityNumber, Integer, Integer>,
		JpaRepository<EntityNumber, Integer> {

	EntityNumber findByEntityId(int entityId);

	EntityNumber findById(long entityNumber);

}