/**
 * 
 */
package com.getinsured.hix.planmgmt.dto;

import java.io.Serializable;

import com.getinsured.hix.model.GHIXResponse;

/**
 * 
 * @author Krishna Gajulapalli
 *
 */
public class PlanLightResponseDTO extends GHIXResponse implements Serializable{

	/**
	 * Attribute long serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int planId;
	private String networkType;
	private String planName;
	private String issuerName;
	private String deductibleFamily;
	private String officeVisit;
	private String genericMedications;
	private String issuerLogo;
	private String deductible;
	
	
	/**
	 * @return the planId
	 */
	public int getPlanId() {
		return planId;
	}
	/**
	 * @param planId the planId to set
	 */
	public void setPlanId(int planId) {
		this.planId = planId;
	}
	/**
	 * @return the networkType
	 */
	public String getNetworkType() {
		return networkType;
	}
	/**
	 * @param networkType the networkType to set
	 */
	public void setNetworkType(String networkType) {
		this.networkType = networkType;
	}
	/**
	 * @return the planName
	 */
	public String getPlanName() {
		return planName;
	}
	/**
	 * @param planName the planName to set
	 */
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	/**
	 * @return the issuerName
	 */
	public String getIssuerName() {
		return issuerName;
	}
	/**
	 * @param issuerName the issuerName to set
	 */
	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}
	/**
	 * @return the deductibleFamily
	 */
	public String getDeductibleFamily() {
		return deductibleFamily;
	}
	/**
	 * @param deductibleFamily the deductibleFamily to set
	 */
	public void setDeductibleFamily(String deductibleFamily) {
		this.deductibleFamily = deductibleFamily;
	}
	/**
	 * @return the officeVisit
	 */
	public String getOfficeVisit() {
		return officeVisit;
	}
	/**
	 * @param officeVisit the officeVisit to set
	 */
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	/**
	 * @return the genericMedications
	 */
	public String getGenericMedications() {
		return genericMedications;
	}
	/**
	 * @param genericMedications the genericMedications to set
	 */
	public void setGenericMedications(String genericMedications) {
		this.genericMedications = genericMedications;
	}
	/**
	 * @return the issuerLogo
	 */
	public String getIssuerLogo() {
		return issuerLogo;
	}
	/**
	 * @param issuerLogo the issuerLogo to set
	 */
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	/**
	 * @return the deductible
	 */
	public String getDeductible() {
		return deductible;
	}
	/**
	 * @param deductible the deductible to set
	 */
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}	

}
