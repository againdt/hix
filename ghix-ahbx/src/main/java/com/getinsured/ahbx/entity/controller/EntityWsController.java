package com.getinsured.ahbx.entity.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBElement;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.ahbx.entity.certificationInfo.AddUpdateCertificationInfo;
import com.getinsured.ahbx.entity.certificationInfo.AddUpdateCertificationInfoResponse;
import com.getinsured.ahbx.entity.certificationInfo.CertificationCode;
import com.getinsured.ahbx.entity.certificationInfo.CertificationInfoRequestDTO;
import com.getinsured.ahbx.entity.certificationInfo.CertificationInfoResponseDTO;
import com.getinsured.ahbx.entity.certificationInfo.ChangeType;
import com.getinsured.ahbx.entity.certificationInfo.EntityType;
import com.getinsured.ahbx.entity.certificationInfo.ObjectFactory;
import com.getinsured.ahbx.entity.certificationInfo.OrgType;
import com.getinsured.ahbx.entity.certificationInfo.ReceivePaymentType;
import com.getinsured.ahbx.entity.certificationInfo.RecordType;
import com.getinsured.ahbx.entity.certificationInfo.RegistrationCode;
import com.getinsured.ahbx.entity.designate.AcceptanceStatusValue;
import com.getinsured.ahbx.entity.designate.AgentAssisterInfo;
import com.getinsured.ahbx.entity.designate.AgentAssisterInfoResponse;
import com.getinsured.ahbx.entity.designate.AgentAssisterType;
import com.getinsured.ahbx.entity.designate.IdentityConfirmationRequestDTO;
import com.getinsured.ahbx.entity.designate.IdentityConfirmationResponseDTO;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.dto.entity.DesignateEntityDTO;
import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.webclient.SoapWebClient;

/**
 * The controller class that receives Rest requests from ghix-web and ghix-entity. It then sends
 * this information to AHBX by consuming their web services.
 */
@Controller
@RequestMapping(value = "/entity")
public class EntityWsController {
	private static final String RECORD_TYPE_ASSISTER = "Assister";

	private static final String RECORD_TYPE_AGENT = "Agent";

	private static final Logger LOGGER = Logger.getLogger(EntityWsController.class);

	private Map<String, OrgType> organisationTypeMap = null;

	private Map<String, EntityType> entityTypeMap = null;

	private Map<String, ReceivePaymentType> receivePaymentMap = null;
	
	private Map<String, ChangeType> changeTypeMap = null;

	@Autowired
	private WebServiceTemplate brokerWebServiceTemplate;
	
	@Autowired
	private SoapWebClient soapWebClient;

	@PostConstruct
	public void init() {

		LOGGER.info("Init method invoked in EntityWsController");
		
		// Populate the Organization Type Map
		organisationTypeMap = new HashMap<String, OrgType>();

		organisationTypeMap.put("Community-Based organization", OrgType.COMMUNITY_BASED_ORGANIZATION);
		organisationTypeMap.put("Community Clinic", OrgType.COMMUNITY_CLINIC);
		organisationTypeMap.put("Faith-based organization", OrgType.FAITH_BASED_ORGANIZATION);
		organisationTypeMap.put("School", OrgType.SCHOOL);
		organisationTypeMap.put("Private Partnership", OrgType.PRIVATE_PARTNERSHIP);
		organisationTypeMap.put("Public Partnership", OrgType.PUBLIC_PARTNERSHIP);
		LOGGER.debug("Organization Type Map populated");

		// Populate the Entity Type map
		entityTypeMap = new HashMap<String, EntityType>();
		entityTypeMap.put("In-Person Assistance", EntityType.IN_PERSON_ASSISTANCE);
		entityTypeMap.put("Navigation Organization", EntityType.NAVIGATOR_ORGANIZATION);
		LOGGER.debug("Entity Type Map populated");

		// Populate the Receive Payment map
		receivePaymentMap = new HashMap<String, ReceivePaymentType>();
		receivePaymentMap.put("Yes", ReceivePaymentType.YES);
		receivePaymentMap.put("No", ReceivePaymentType.NO);
		LOGGER.debug("Receive Payment Type Map populated");
		
		changeTypeMap = new HashMap<String, ChangeType>();
		changeTypeMap.put("AgentToAgencyManager", ChangeType.AGENT_TO_AGENCY_MANAGER);
		changeTypeMap.put("AgencyManagerToAgent", ChangeType.AGENCY_MANAGER_TO_AGENT);
		changeTypeMap.put("AdminStaffL1ToAdminStaffL2", ChangeType.ADMIN_STAFF_L_1_TO_ADMIN_STAFF_L_2);
		changeTypeMap.put("AdminStaffL2ToAdminStaffL1", ChangeType.ADMIN_STAFF_L_2_TO_ADMIN_STAFF_L_1);
	}

	/**
	 * To test {@link EntityWSController}
	 * 
	 * @return welcome message
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() throws HTTPException, Exception {
		LOGGER.info("Welcome to Enrollment WebService module");
		return "Welcome to Enrollment WebService module";
	}

	/**
	 * Invokes AHBX web service to send agent / assister / enrollment entity details.
	 * 
	 * @param entityRequestDTO
	 *            {@link EntityRequestDTO} the object encapsulating agent / assister / enrollment
	 *            details
	 * @return response String returned by AHBX
	 */
	@RequestMapping(value = "/sendentitydetail", method = RequestMethod.POST)
	@ResponseBody
	public String sendEntityDetail(@RequestBody String strEntityRequestDTOs) {

		List<EntityRequestDTO> entityRequestDTOs = (List) GhixUtils.xStreamHibernateXmlMashaling().fromXML(
				strEntityRequestDTOs);

		EntityResponseDTO EntityResponseDTO = new EntityResponseDTO();
		List<EntityResponseDTO> EntityResponseDTOList = new ArrayList<EntityResponseDTO>();
		JAXBElement<AddUpdateCertificationInfoResponse> addUpdateCertificationInfoResponseFromAHBX = null;
		List<CertificationInfoRequestDTO> certificationInfoRequestDTOList = null;
		AddUpdateCertificationInfoResponse addUpdateCertificationInfoResponse = null;

		if (entityRequestDTOs != null && !entityRequestDTOs.isEmpty()) {
			LOGGER.info("IND35: Successfully received EntityRequestDTO List from ghix-web over rest call");
						
			// Populate AHBX request object
			certificationInfoRequestDTOList = populateAHBXRequestObject(entityRequestDTOs);
			LOGGER.debug("IND35: CertificationInfoRequestDTO successfully populated");
			try {
				// Creating AHBX request object
				JAXBElement<AddUpdateCertificationInfo> requestElement = createAHBXRequestObject(certificationInfoRequestDTOList);

				// Invoking AHBX Web Service
				brokerWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND35);
				//brokerWebServiceTemplate.setDefaultUri("http://localhost:8088/mockCertificationInfoEndPointImplServiceSoapBinding?wsdl");
/*				LOGGER.debug("IND35: Set following URI for WS client: " + brokerWebServiceTemplate.getDefaultUri());
				LOGGER.info("IND35: Invoking Entity Details service. Sending following data to AHBX: "
						+ certificationInfoRequestDTOList);
*/
				/*addUpdateCertificationInfoResponseFromAHBX = (JAXBElement<AddUpdateCertificationInfoResponse>) brokerWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				addUpdateCertificationInfoResponseFromAHBX = (JAXBElement<AddUpdateCertificationInfoResponse>) soapWebClient.
						send(requestElement, GhixAhbxConstants.WSDL_URL_IND35, "IND35", brokerWebServiceTemplate);

				addUpdateCertificationInfoResponse = addUpdateCertificationInfoResponseFromAHBX.getValue();

/*				LOGGER.debug("IND35: Retrieved addUpdateCertificationInfoResponse object from response : "
						+ addUpdateCertificationInfoResponse.toString());
*/				
			}
			catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND35: Exception occurred while invoking Entity Details service "
						, webServiceIOException);


				EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				EntityResponseDTO.setErrMsg(webServiceIOException.getMessage());
			}
			catch (Exception exception) {
				LOGGER.error("IND35: Exception occurred while invoking Entity Details service" , exception);
				EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				EntityResponseDTO.setErrMsg(exception.getMessage());
			}

			EntityResponseDTOList.add(EntityResponseDTO);
			// Processing AHBX response
			processAHBXResponse(EntityResponseDTOList, addUpdateCertificationInfoResponse);
		}
		else {
			LOGGER.error("IND35: Entity Request Object is null. Unable to send Entity Details to AHBX.");

			EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			EntityResponseDTO.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
			EntityResponseDTO.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
			EntityResponseDTOList.add(EntityResponseDTO);
		}

		LOGGER.info("IND35: Marshalling and returning EntityResponseDTO to ghix-web");
		// Marshaling the response before it is sent out to ghix-web
		return GhixUtils.xStreamHibernateXmlMashaling().toXML(EntityResponseDTOList);
	}

	/**
	 * Processes responses received from AHBX
	 * 
	 * @param EntityResponseDTO
	 *            {@link EntityResponseDTO}
	 * @param addUpdateCertificationInfoResponse
	 *            {@link AddUpdateCertificationInfoResponse}
	 */
	private void processAHBXResponse(List<EntityResponseDTO> EntityResponseDTOList,
			AddUpdateCertificationInfoResponse addUpdateCertificationInfoResponse) {
		List<CertificationInfoResponseDTO> certificationInfoResponseDTOs;
		LOGGER.info("IND35: Entity Details service call successful. Received following response from AHBX : ");
		
		if (addUpdateCertificationInfoResponse != null && addUpdateCertificationInfoResponse.getReturn0() != null
				&& !addUpdateCertificationInfoResponse.getReturn0().isEmpty()) {

			certificationInfoResponseDTOs = addUpdateCertificationInfoResponse.getReturn0();

			if (certificationInfoResponseDTOs != null && !certificationInfoResponseDTOs.isEmpty()) {
				int i = 0;
				for (CertificationInfoResponseDTO certificationInfoResponseDTO : certificationInfoResponseDTOs) {
					EntityResponseDTO EntityResponseDTO = new EntityResponseDTO();
/*					LOGGER.info("IND35: certificationInfoResponseDTO response " + i++ + " :\n "
							+ certificationInfoResponseDTO.toString());
*/
					// Convert certificationInfoResponseDTO into
					// EntityResponseDTO
					EntityResponseDTO.setBrokerId(certificationInfoResponseDTO.getRecordId());
					EntityResponseDTO.setResponseCode(certificationInfoResponseDTO.getResponseCode());
					EntityResponseDTO.setResponseDescription(certificationInfoResponseDTO.getResponseDesc());

/*					LOGGER.debug("IND35: Populated EntityResponseDTO to be sent to ghix-web: "
							+ EntityResponseDTO.toString());
*/
					EntityResponseDTOList.add(EntityResponseDTO);
				}
			}
		}
		else {
			LOGGER.error("IND35:Entity Response object from AHBX is null. ");			
		}
	}

	/**
	 * Creating AHBX request object
	 * 
	 * @param certificationInfoRequestDTO
	 *            {@link CertificationInfoRequestDTO}
	 * @return {@link JAXBElement} of {@link AddUpdateCertificationInfo}
	 */
	private JAXBElement<AddUpdateCertificationInfo> createAHBXRequestObject(
			List<CertificationInfoRequestDTO> certificationInfoRequestDTOList) {

		LOGGER.debug("IND35: Creating AHBX request object");
		AddUpdateCertificationInfo addUpdateCertificationInfo;
		ObjectFactory objectFactory = new ObjectFactory();
		addUpdateCertificationInfo = objectFactory.createAddUpdateCertificationInfo();
		addUpdateCertificationInfo.getArg0().addAll(certificationInfoRequestDTOList);

		JAXBElement<AddUpdateCertificationInfo> requestElement = objectFactory
				.createAddUpdateCertificationInfo(addUpdateCertificationInfo);
		LOGGER.debug("IND35: Set CertificationInfoRequestDTO and created request element");
		return requestElement;
	}

	/**
	 * Populates AHBX request DTO
	 * 
	 * @param entityRequestDTO
	 *            {@link EntityRequestDTO}
	 * @return certificationInfoRequestDTO {@link CertificationInfoRequestDTO}
	 */
	private List<CertificationInfoRequestDTO> populateAHBXRequestObject(List<EntityRequestDTO> entityRequestDTOs) {

		List<CertificationInfoRequestDTO> certificationInfoRequestDTOList = new ArrayList<CertificationInfoRequestDTO>();
		CertificationInfoRequestDTO certificationInfoRequestDTO;

		for (EntityRequestDTO entityRequestDTO : entityRequestDTOs) {
			//LOGGER.debug("IND35: Populate AHBX request object : " + entityRequestDTO.toString());

			certificationInfoRequestDTO = new CertificationInfoRequestDTO();

			certificationInfoRequestDTO.setRecordId(entityRequestDTO.getRecordId());
			setRecordType(certificationInfoRequestDTO, entityRequestDTO.getRecordType());
			certificationInfoRequestDTO.setFirstName(entityRequestDTO.getFirstName());
			certificationInfoRequestDTO.setMiddleName(entityRequestDTO.getMiddleName());
			certificationInfoRequestDTO.setLastName(entityRequestDTO.getLastName());
			if(entityRequestDTO.getBusinessLegalName().length() > 50 && entityRequestDTO.getRecordType().equalsIgnoreCase("Agent")){
				certificationInfoRequestDTO.setBusinessLegalName(entityRequestDTO.getBusinessLegalName().substring(0, 50));
			}else{
				certificationInfoRequestDTO.setBusinessLegalName(entityRequestDTO.getBusinessLegalName());
			}
			
			if(entityRequestDTO.getChangeType()!=null){
				certificationInfoRequestDTO.setChangeType(changeTypeMap.get(entityRequestDTO.getChangeType()));
			}

			certificationInfoRequestDTO.setAgentLicenseNum(entityRequestDTO.getAgentLicenseNum());
			certificationInfoRequestDTO.setFunctionalIdentifier(entityRequestDTO.getFunctionalIdentifier());
			certificationInfoRequestDTO.setEntityIdNumber(entityRequestDTO.getEntityIdNumber());

			certificationInfoRequestDTO.setFederalEIN(entityRequestDTO.getFederalEIN());
			certificationInfoRequestDTO.setStateEIN(entityRequestDTO.getStateEIN());

			certificationInfoRequestDTO.setAddressLine1(entityRequestDTO.getAddressLine1());
			certificationInfoRequestDTO.setAddressLine2(entityRequestDTO.getAddressLine2());
			certificationInfoRequestDTO.setCity(entityRequestDTO.getCity());
			certificationInfoRequestDTO.setState(entityRequestDTO.getState());
			certificationInfoRequestDTO.setZipCode(entityRequestDTO.getZipCode());

			certificationInfoRequestDTO.setEmail(entityRequestDTO.getEmail());
			certificationInfoRequestDTO.setPhoneNumber(entityRequestDTO.getPhoneNumber());

			certificationInfoRequestDTO.setDirectDepositFlag(entityRequestDTO.getDirectDepositFlag());
			certificationInfoRequestDTO.setAcctHolderName(entityRequestDTO.getAcctHolderName());
			certificationInfoRequestDTO.setBankAcctType(entityRequestDTO.getBankAcctType());
			certificationInfoRequestDTO.setBankRoutingNumber(entityRequestDTO.getBankRoutingNumber());
			certificationInfoRequestDTO.setBankAcctNumber(entityRequestDTO.getBankAcctNumber());

			certificationInfoRequestDTO.setStatusDate(entityRequestDTO.getStatusDate());
			certificationInfoRequestDTO.setCertiStartDate(entityRequestDTO.getCertiStartDate());
			certificationInfoRequestDTO.setCertiEndDate(entityRequestDTO.getCertiEndDate());
			certificationInfoRequestDTO.setCertificatonNumber(entityRequestDTO.getCertificationNumber());

			if (entityRequestDTO.getCertiStatusCode() != null) {
				//Inactive and certified
				if(entityRequestDTO.getCertiStatusCode().equalsIgnoreCase("Certified") && "InActive".equals(entityRequestDTO.getStatus())) {
					setCertificaitonStatusCode(certificationInfoRequestDTO, entityRequestDTO.getStatus() + " " + entityRequestDTO.getCertiStatusCode() );
				}
				else {
					setCertificaitonStatusCode(certificationInfoRequestDTO, entityRequestDTO.getCertiStatusCode());
				}
			}

			// In case of Assister this will be a assister activity status
			if (entityRequestDTO.getRegistrationStatusCd() != null) {
				setRegistrationStatusCode(certificationInfoRequestDTO, entityRequestDTO.getRegistrationStatusCd());
			}

			certificationInfoRequestDTO.setRecordIndicator(entityRequestDTO.getRecordIndicator());

			// Adding new fields for AEE as per latest spec from AHBX
			certificationInfoRequestDTO.setRegistrationStartDate(entityRequestDTO.getRegiStartDate());
			certificationInfoRequestDTO.setRegistrationEndDate(entityRequestDTO.getRegiEndDate());
			certificationInfoRequestDTO.setEntityType(entityTypeMap.get(entityRequestDTO.getEntityType()));
			certificationInfoRequestDTO
					.setReceivePayments(receivePaymentMap.get(entityRequestDTO.getReceivePayments()));
			certificationInfoRequestDTO.setOrgType(organisationTypeMap.get(entityRequestDTO.getOrgType()));
			certificationInfoRequestDTO.setCertiRenewalDate(entityRequestDTO.getCertiRenewalDate());

			certificationInfoRequestDTOList.add(certificationInfoRequestDTO);
		}
		return certificationInfoRequestDTOList;
	}

	// TODO set certification based on value after WSDL is updated
	private void setCertificaitonStatusCode(CertificationInfoRequestDTO certificationInfoRequestDTO, String certiStatus) {
		Map<String, CertificationCode> statusMap = new HashMap<String, CertificationCode>();

		statusMap.put("Pending", CertificationCode.PE);
		statusMap.put("Withdrawn", CertificationCode.WI);
		statusMap.put("Eligible", CertificationCode.EL);
		statusMap.put("Certified", CertificationCode.CE);
		statusMap.put("Denied", CertificationCode.DE);
		statusMap.put("Terminated-Vested", CertificationCode.TV);
		statusMap.put("Terminated-For-Cause", CertificationCode.TC);
		statusMap.put("Terminated", CertificationCode.TV);
		statusMap.put("Deceased", CertificationCode.DC);

		statusMap.put("Withdrawn - Assister Request", CertificationCode.WR);
		statusMap.put("Denied - Not Eligible For Training", CertificationCode.DT);
		statusMap.put("Denied - Did Not Complete Required Training", CertificationCode.DC);
		statusMap.put("Denied - Background Check", CertificationCode.DB);
		statusMap.put("Denied - Conflict of Interest", CertificationCode.DI);
		statusMap.put("Denied - Unable to Satisfy Program Requirements", CertificationCode.DS);
		statusMap.put("Denied", CertificationCode.DE);
		statusMap.put("Decertified - Misconduct", CertificationCode.DM);
		statusMap.put("Decertified - Assister Request", CertificationCode.DA);
		statusMap.put("Entity Deregistered", CertificationCode.ED);
		statusMap.put("Entity Deregistered - Failed to Renew", CertificationCode.EF);
		statusMap.put("Entity Deregistered - Misconduct", CertificationCode.EM);
		statusMap.put("InActive Certified", CertificationCode.IC);
	
		statusMap.put("Decertified - Failure to Renew", CertificationCode.DF);
		statusMap.put("Denied - Prior Conviction", CertificationCode.DP);
		statusMap.put("Approved", CertificationCode.AP);
		statusMap.put("Suspended", CertificationCode.SP);
		
		certificationInfoRequestDTO.setCertiStatusCode(statusMap.get(certiStatus));
	}

	// TODO set registration based on value after WSDL is updated
	/**
	 * Set registration status code based on registration status
	 * 
	 * @param certificationInfoRequestDTO
	 *            {@link CertificationInfoRequestDTO}
	 * @param regiStatus
	 *            String
	 */
	private void setRegistrationStatusCode(CertificationInfoRequestDTO certificationInfoRequestDTO, String regiStatus) {
		Map<String, RegistrationCode> statusMap = new HashMap<String, RegistrationCode>();

		statusMap.put("Pending", RegistrationCode.PE);
		statusMap.put("Registered", RegistrationCode.RE);
		statusMap.put("Active", RegistrationCode.AC);
		statusMap.put("Withdrawn-Entity Request", RegistrationCode.WE);
		statusMap.put("Denied-Other", RegistrationCode.DO);
		statusMap.put("Denied-Eligibility", RegistrationCode.DE);
		statusMap.put("Denied-Questionable Business Practices", RegistrationCode.DQ);
		statusMap.put("Denied-Conflict of Interest", RegistrationCode.DC);
		statusMap.put("Denied-Unable to Satisfy Program Requirements", RegistrationCode.DU);
		statusMap.put("Denied-Failed Training", RegistrationCode.DT);
		statusMap.put("Deregistered", RegistrationCode.DR);
		statusMap.put("Deregistered-Failed to renew", RegistrationCode.DF);
		statusMap.put("Deregistered-Misconduct", RegistrationCode.DM);

		certificationInfoRequestDTO.setRegistrationStatusCd(statusMap.get(regiStatus));
	}

	private void setRecordType(CertificationInfoRequestDTO certificationInfoRequestDTO, String recordType) {
		if (RECORD_TYPE_AGENT.equalsIgnoreCase(recordType)) {
			certificationInfoRequestDTO.setRecordType(RecordType.AGENT);
		} else if (RECORD_TYPE_ASSISTER.equalsIgnoreCase(recordType)) {
			certificationInfoRequestDTO.setRecordType(RecordType.ASSISTER);
		} else if(GhixAhbxConstants.APPROVEDADMINSTAFFL1.equalsIgnoreCase(recordType)) 	{
			certificationInfoRequestDTO.setRecordType(RecordType.APPROVED_ADMIN_STAFF_L_1);
		} else if(GhixAhbxConstants.APPROVEDADMINSTAFFL2.equalsIgnoreCase(recordType)) {
			certificationInfoRequestDTO.setRecordType(RecordType.APPROVED_ADMIN_STAFF_L_2);
		} else {
			certificationInfoRequestDTO.setRecordType(RecordType.ENTITY);
		}
	}

	/**
	 * Invokes AHBX web service to send agent/assister designation details.
	 * 
	 * @param designateEntityDto
	 *            object encapsulating agent/assister designation details
	 * @return response returned by AHBX
	 */
	@RequestMapping(value = "/senddesignationdetail", method = RequestMethod.POST)
	@ResponseBody
	public String sendEntityDesignationInfo(@RequestBody DesignateEntityDTO designateEntityDto) {
		IdentityConfirmationResponseDTO identityConfirmationResponseDTO = null;
		EntityResponseDTO EntityResponseDTO = new EntityResponseDTO();
		AgentAssisterInfoResponse agentAssisterInfoResponse = null;
		AgentAssisterInfo brokerAssisterInfo = null;
		IdentityConfirmationRequestDTO identityConfirmationRequestDTO = null;
		JAXBElement<AgentAssisterInfoResponse> agentAssisterInfoResponseFromAHBX = null;

		if (designateEntityDto != null) {
/*			LOGGER.info("IND47: Successfully received DesignateBrokerDto from ghix-web over rest call: "
					+ designateEntityDto.toString());
*/			identityConfirmationRequestDTO = new IdentityConfirmationRequestDTO();

			LOGGER.warn("IND47: Setting AgentAssisterId");
			identityConfirmationRequestDTO.setAgentAssisterId(designateEntityDto.getBrokerAssisterId());
			LOGGER.warn("IND47: Setting IndividualID");
			identityConfirmationRequestDTO.setIndividualID(designateEntityDto.getIndividualId() != null
					&& designateEntityDto.getIndividualId() != 0 ? new Long(designateEntityDto.getIndividualId())
					: null);
			LOGGER.warn("IND47: Setting EmployerID");
			identityConfirmationRequestDTO.setEmployerID(designateEntityDto.getEmployerId() != null
					&& designateEntityDto.getEmployerId() != 0 ? new Long(designateEntityDto.getEmployerId()) : null);

			// Add specific status here
			LOGGER.warn("IND47: Setting AcceptanceStatus");
			if (GhixAhbxConstants.STATUS_INACTIVE.toString().equalsIgnoreCase(designateEntityDto.getStatus())) {
				identityConfirmationRequestDTO.setAcceptanceStatus(AcceptanceStatusValue.DECLINED);
			}
			else if (GhixAhbxConstants.STATUS_ACTIVE.equalsIgnoreCase(designateEntityDto.getStatus())) {
				identityConfirmationRequestDTO.setAcceptanceStatus(AcceptanceStatusValue.ACCEPTED);
			}
			else if (GhixAhbxConstants.STATUS_PENDING.toString().equalsIgnoreCase(designateEntityDto.getStatus())) {
				identityConfirmationRequestDTO.setAcceptanceStatus(AcceptanceStatusValue.PENDING);
			}

			// Add Record Type as per latest AHBX spec to support Assister and Agent
			LOGGER.warn("IND47: Setting RecordType");
			if (GhixAhbxConstants.ASSISTER.equals(designateEntityDto.getRecordType())) {
				identityConfirmationRequestDTO.setRecordType(AgentAssisterType.ASSISTER);
			}
			else if (GhixAhbxConstants.AGENT.equals(designateEntityDto.getRecordType())) {
				identityConfirmationRequestDTO.setRecordType(AgentAssisterType.AGENT);
			}
			//LOGGER.info("IND47: IdentityConfirmationRequestDTO successfully populated : "+identityConfirmationRequestDTO);
			

			try {
				// Creating the AHBX response object
				LOGGER.warn("IND47: Creating AgentAssisterInfo");
				com.getinsured.ahbx.entity.designate.ObjectFactory objectFactory = new com.getinsured.ahbx.entity.designate.ObjectFactory();
				brokerAssisterInfo = objectFactory.createAgentAssisterInfo();

				LOGGER.warn("IND47: Setting AgentAssisterInfo");
				brokerAssisterInfo.setAgentAssisterInfo(identityConfirmationRequestDTO);
				LOGGER.warn("IND47: Creating Request Element");
				JAXBElement<AgentAssisterInfo> requestElement = objectFactory
						.createAgentAssisterInfo(brokerAssisterInfo);

				//LOGGER.info("IND47: Set IdentityConfirmationRequestDTO into BrokerAssisterInfo and created request element : "+requestElement);
				LOGGER.warn("IND47: Setting DefaultURI to AgentWebserviceTemplate");
				brokerWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND47);
				
/*				LOGGER.info("IND47: Set following URI for WS client: " + brokerWebServiceTemplate.getDefaultUri());
				LOGGER.info("IND47: Invoking Designate Broker service. Sending following data to AHBX: "
						+ identityConfirmationRequestDTO.toString());
*/				/*agentAssisterInfoResponseFromAHBX = (JAXBElement<AgentAssisterInfoResponse>) brokerWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				agentAssisterInfoResponseFromAHBX = (JAXBElement<AgentAssisterInfoResponse>) soapWebClient.
						send(requestElement, GhixAhbxConstants.WSDL_URL_IND47, "IND47", brokerWebServiceTemplate);

				LOGGER.warn("IND47: Getting value from AHBX response");
				agentAssisterInfoResponse = agentAssisterInfoResponseFromAHBX.getValue();

/*				LOGGER.info("IND47: Retrieved BrokerAssisterInfoResponse object from response: "
						+ agentAssisterInfoResponse.toString());
*/
			}
			catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND47: Exception occurred while invoking Broker Designation Details service: "
						, webServiceIOException);

				EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				EntityResponseDTO.setErrMsg(webServiceIOException.getMessage());
			}
			catch (Exception exception) {
				LOGGER.error("IND47: Exception occurred while invoking Broker Designation Details service: "
						, exception);
				EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				EntityResponseDTO.setErrMsg(exception.getMessage());
			}

			// Processing AHBX response
			if (agentAssisterInfoResponse != null && agentAssisterInfoResponse.getReturn() != null) {
				// Assuming that only one record will be sent
				identityConfirmationResponseDTO = agentAssisterInfoResponse.getReturn();

/*				LOGGER.info("IND47: Designate Broker service call successful. Received following response from AHBX: "
						+ identityConfirmationResponseDTO.toString());
*/
				// Save only if response is successful. Log both cases
				if (identityConfirmationResponseDTO.getResponseCode() == 200) {
					LOGGER.warn("IND47: Response code is 200");
						
					EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
				} else {
					LOGGER.warn("IND47: Failure response received from AHBX service call. Response code is :"
							+ identityConfirmationResponseDTO.getResponseCode());

					
					EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				}

				// Convert AHBX response into EntityResponseDTO object
				EntityResponseDTO.setResponseCode(identityConfirmationResponseDTO.getResponseCode());
				EntityResponseDTO.setResponseDescription(identityConfirmationResponseDTO.getResponseDesc());
				LOGGER.warn("IND47: Response received from AHBX service call. Response code is :"
						+ identityConfirmationResponseDTO.getResponseCode());
				LOGGER.warn("IND47: Response received from AHBX service call. Response status is :"
						+ EntityResponseDTO.getStatus());
/*				LOGGER.info("IND47: Populated EntityResponseDTO to be sent to ghix-web: "
						+ EntityResponseDTO.toString());
*/
			}
		}
		else {
			LOGGER.warn("IND47: Broker Object is null. Unable to send Broker designation details to AHBX");

			EntityResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			EntityResponseDTO.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
			EntityResponseDTO.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
		}

		LOGGER.warn("IND47: Marshalling EntityResponseDTO and sending response to ghix-web");
		String response = GhixUtils.getXStreamStaxObject().toXML(EntityResponseDTO);
		LOGGER.warn("IND47: response"+response);
		// Sending response details back to ghix-web
		return response;
	}

}
