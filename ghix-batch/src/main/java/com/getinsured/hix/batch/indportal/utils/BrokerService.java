package com.getinsured.hix.batch.indportal.utils;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.BrokerDocuments;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.Employer;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.model.BrokerPhoto;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.dto.agency.AgencyBrokerListDTO;
import com.getinsured.hix.dto.agency.AgencyDetailsListDTO;
import com.getinsured.hix.dto.agency.AgencyInformationDto;
import com.getinsured.hix.dto.agency.SearchAgencyDto;
import com.getinsured.hix.dto.agency.SearchBrokerDTO;
import com.getinsured.hix.dto.broker.BrokerExportResponseDTO;

public interface BrokerService {

	/**
	 * Saves {@link Broker} object along with {@link Location}
	 * 
	 * @param broker
	 *            {@link Broker}
	 * @param triggerIND
	 *            Flag to check if AHBX interface is to be called
	 * @return
	 */
	Broker saveBrokerWithLocation(Broker broker);

	/**
	 * Retrieves broker by license number
	 * 
	 * @param licenseNumber
	 *            license number of the broker
	 * @return {@link Broker}
	 */
	Broker findBrokerByLicenseNumber(String licenseNumber);
	
	/**
	 * Retrieves broker by npn
	 * 
	 * @param npn
	 *            npn of the broker
	 * @return {@link Broker}
	 */
	Broker findBrokerByNpn(String npn);


}