package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.ToConsiderService;
import com.getinsured.hix.batch.ssap.renewal.util.ToConsiderPartitionerParams;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * ToConsiderProcessor class is used to get data from database.
 * 
 * @since July 19, 2020
 */
@Component("toConsiderProcessor")
public class ToConsiderProcessor implements ItemProcessor<SsapApplication,SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToConsiderProcessor.class);

	private ToConsiderService toConsiderService;
	private ToConsiderPartitionerParams toConsiderPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");
			LOGGER.info("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
		}
	}

	@Override
	public SsapApplication process(SsapApplication ssapApplication) throws Exception {
		LOGGER.info("Returning SsapApplication is null : {}", (null == ssapApplication));
		return ssapApplication;
	}

	public ToConsiderService getToConsiderService() {
		return toConsiderService;
	}

	public void setToConsiderService(ToConsiderService toConsiderService) {
		this.toConsiderService = toConsiderService;
	}

	public ToConsiderPartitionerParams getToConsiderPartitionerParams() {
		return toConsiderPartitionerParams;
	}

	public void setToConsiderPartitionerParams(
			ToConsiderPartitionerParams toConsiderPartitionerParams) {
		this.toConsiderPartitionerParams = toConsiderPartitionerParams;
	}
}