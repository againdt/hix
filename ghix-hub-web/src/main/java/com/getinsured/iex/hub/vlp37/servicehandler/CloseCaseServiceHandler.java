package com.getinsured.iex.hub.vlp37.servicehandler;

import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseRequestType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.CloseCaseResponseType;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.ObjectFactory;
import com.getinsured.iex.hub.vlp37.prefixmappers.VLPNamespacePrefixMapper;
import com.getinsured.iex.hub.vlp37.wrapper.CloseCaseRequestWrapper;
import com.getinsured.iex.hub.vlp37.wrapper.CloseCaseResponseWrapper;


public class CloseCaseServiceHandler extends AbstractServiceHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CloseCaseServiceHandler.class);
	private static ObjectFactory factory;
	private CloseCaseRequestType reqObj;
	private Jaxb2Marshaller marshaller;
	
	static{
		
		if(factory == null){
			factory = new ObjectFactory();
		}
	}
	
	@SuppressWarnings("unchecked")
	private void processJSONObject(JSONObject obj) throws HubServiceException, HubMappingException{
	        Set<Entry<String, Object>> keySet = obj.entrySet();
	        Iterator<Entry<String, Object>> cursor = keySet.iterator();
	        Entry<String, Object> jsonElement = null;
	        while(cursor.hasNext()){
	               jsonElement = cursor.next();
	               handleJsonElement(jsonElement.getKey(), jsonElement.getValue());
	        }
	 }
	
	 private void handleJsonElement(String key, Object value) throws HubServiceException, HubMappingException {
	        if(value instanceof JSONObject){
	           processJSONObject((JSONObject)value);
	        }else if(value instanceof JSONArray){
	        	processJSONArray((JSONArray) value);
	        }
	        else if(value instanceof String || value instanceof Boolean){
	        	this.handleInputParameter(key, value);
	        }
	 }

	
	private void processJSONArray(JSONArray value) throws HubServiceException, HubMappingException {
		int arrayLen = value.size();
		Object tmp;
		for(int i = 0; i < arrayLen; i++){
			tmp = value.get(i);
			if(tmp instanceof JSONObject){
				processJSONObject((JSONObject)tmp);
			}
			// should we expect anything other than JSONObject in this array?
		}
	}
	
	public Object getRequestpayLoad() throws HubServiceException{
		if(this.reqObj != null) {
			return this.reqObj;
		}
		try{
			JSONParser parser = new JSONParser();
			JSONObject payloadObj = (JSONObject) parser.parse(getJsonInput());
			
			
			processJSONObject(payloadObj);
			
			CloseCaseRequestWrapper obj = (CloseCaseRequestWrapper)getNamespaceObjects().get(CloseCaseRequestWrapper.class.getName());
			if(obj == null){
				throw new HubServiceException("required VLP Request parameters not available");
			}
			CloseCaseRequestType reqObj = obj.getRequest();
			setJsonInput(obj.toJSONString());
			return reqObj;
		}catch(Exception pe){
			throw new HubServiceException("Failed to generate the payload:",pe);
		}
	}	
	
	public String handleResponse(Object responseObject)
			throws HubServiceException {
		
		String message = null;
		try{
			@SuppressWarnings("unchecked")
			JAXBElement<CloseCaseResponseType> response = (JAXBElement<CloseCaseResponseType>)responseObject;
			CloseCaseResponseWrapper wrapper = new CloseCaseResponseWrapper(response.getValue());
			if(response != null){
				this.setResponseCode(wrapper.getResponseMetadata().getResponseCode());
				message = wrapper.toJSONString();
				this.requestStatus = RequestStatus.SUCCESS;
				if(LOGGER.isDebugEnabled()){
					LOGGER.debug("Response CloseCase " + message);
				}
			}
			
			LOGGER.debug("Response message " + message);
		}catch(ClassCastException ce){
			message = "Class in response: " + responseObject.getClass().getName()+" Expecting:"+ CloseCaseResponseType.class.getName();
			this.requestStatus = RequestStatus.FAILED;
			throw new HubServiceException(message, ce);
		}
		return message;
	}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}
	
	@Override
	public String getServiceIdentifier() {
		return "H95";
	}

	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc",new VLPNamespacePrefixMapper());
		}
		return this.marshaller;
	}

}
