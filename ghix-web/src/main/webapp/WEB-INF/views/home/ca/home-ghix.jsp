<%@page import="java.util.Date"%>
<%@page import="org.apache.commons.lang3.time.DateUtils"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags" %>

<%
  String stateExchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_EXCHANGE_TYPE);
  String enrollmentStartDate = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_START_DATE);
  String showIdaLinkLogo = "Y";
  if(enrollmentStartDate != null){
	  Date startDate = DateUtils.parseDateStrictly(enrollmentStartDate, new String[]{"MM/dd/yyyy"});
	  if(new Date().before(startDate)){
		  showIdaLinkLogo = "N";
	  }
  }
  pageContext.setAttribute("showIdaLinkLogo", showIdaLinkLogo);
%>
<c:set var="stateExchangeType" value="<%=stateExchangeType%>" />
<c:set var="showIdaLinkLogo" value="<%=showIdaLinkLogo%>" />

 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget'
 };
 </script>
<div class="row-fluid" role="main">
	<div class="landing-signup">
		<h1><spring:message code="label.homePage.headerCopy"/></h1>
		<div class="row-fluid" role="main">
			  <c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'individual'.equalsIgnoreCase(stateExchangeType)}">
				<div class="register-group">
					<c:set var="individualRoleName" ><encryptor:enc value="individual" isurl="true"/> </c:set>
					<div class="icons-family"></div>
					<p><span><spring:message code="label.homePage.individual"/></span> <br> <br> 
						<spring:message code="label.homePage.individualCopy"/></p>
					<a class="btn pull-right" href="<c:url value='/preeligibility' />">START SHOPPING<i class="icon-large icon-arrow-right"></i><span class="aria-hidden">Individual</span></a>
				</div>
		    </c:if>
		    
		    <c:if test="${'both'.equalsIgnoreCase(stateExchangeType) || 'shop'.equalsIgnoreCase(stateExchangeType)}">
 				<div class="register-group"> 
 					<c:set var="employerRoleName" ><encryptor:enc value="employer" isurl="true"/> </c:set>
 					<div class="icons-employer"></div>
 					<p><span><spring:message code="label.homePage.employer"/></span> <br>
 						<spring:message code="label.homePage.employerCopy"/></p>
 					<a class="btn pull-right" href="<c:url value='/account/signup/${employerRoleName}' />"><i class="icon-large icon-arrow-right"></i><span class="aria-hidden">Employer</span></a>
 				</div>
         </c:if> 
        

	</div>
			
	<div class="row-fluid">	
	 	 	<div class="span6">
	 	 		<div class="register-signin">
	 	 			<p><i class="icon-lock"></i> <spring:message code="label.homePage.haveAccountQuestion"/> 
	 	 			<a class="btn btn-primary" href="<c:url value='/account/user/login'/>"><spring:message code="label.homePage.haveAccountAction"/></a></p>
	 	 			<c:if test="${showIdaLinkLogo == 'Y' }">
		 	 			<div>
		 	 				<img src="<gi:cdnurl value="/resources/img/idaLink_logo.png"/>" alt="Welcome to idalink! Your online portal for managing benefits from Idaho's Department of Health and Welfare. idaLink" title="idalink"  />
		 	 				<small><spring:message code="label.homePage.haveAccountOption"/></small>
		 	 			</div>
		 	 		</c:if>	
	 	 		</div>
	 	 		</div> 	
	 	 		<div class="span6">
	 	 		<div class="access-code register-signin" ng-app="accessCodeApp" ng-controller="accessCodeController" ng-cloak>
	 	 			<form id="access-code-form" name="accessCodeForm" action="<c:url value="/referral/verifyaccesscode"/>" method="post" novalidate autocomplete="off">
	 	 				<df:csrfToken/>
		 	 			<p for="access-code">
		 	 				<i class="icon-lock"></i><spring:message code="label.homePage.haveAccessCode"/>
		 	 			</p>
		 	 			<input type="text" id="accessCode" name="accessCode" length="10" required ng-focus ng-pattern="/^[0-9]{10}$/" ng-model="accessCode" ng-keypress="formSubmit($event)">
		 	 			
		 	 			<div class="help-inline" id="accessCode_error">
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.required && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeRequired"/></span>
							</label>
							<label class="error" ng-if="accessCodeForm.accessCode.$dirty && accessCodeForm.accessCode.$error.pattern && !accessCodeForm.accessCode.$focused">
								<span> <em class="excl">!</em><spring:message code="label.homePage.accessCodeValidation"/></span>
							</label>
						</div>
		 	 			<div class="error help-inline" id="accessCode_error" name="accessCode_error">
		 	 				<c:if test="${not empty errorMsg}">
   								${errorMsg}
							</c:if>
		 	 			</div>
							
						<!--custom captcha starts-->
						<div id="recaptcha_widget" style="display: none">

							<div id="recaptcha_image"></div>
							<div class="recaptcha_only_if_incorrect_sol help-inline">
								<label class="error">
									<span> <em class="excl">!</em>Incorrect please try again</span>
								</label>
							</div>
							<div class="margin30-t margin10-b">
								<span class="recaptcha_only_if_image"><spring:message code="label.homePage.enterabovewords"/></span> 
								<span class="recaptcha_only_if_audio"><spring:message code="label.homePage.enternumberyouheard"/></span> 
							</div>
							<input type="text" id="recaptcha_response_field" name="recaptcha_response_field" ng-focus required ng-model="captcha" ng-keypress="formSubmit($event)"/>
							
							<div class="btn-group pull-right">
								<button onclick="javascript:Recaptcha.reload();return false;" class="btn">
									<i class="icon-refresh"></i>
								</button>

								<button class="recaptcha_only_if_image btn" onclick="javascript:Recaptcha.switch_type('audio');return false;">
									<i class="icon-volume-up"></i>
								</button>
								<button class="recaptcha_only_if_audio btn" onclick="javascript:Recaptcha.switch_type('image');return false;">
									<i class="icon-picture"></i>
								</button>

								<button class="btn" onclick="javascript:Recaptcha.showhelp();return false;">
									<i class="icon-question-sign"></i>
								</button>
							</div>
							
							<div class="help-inline pull-left margin20-l" id="recaptchaCode_error">
								<label class="error" ng-if="accessCodeForm.recaptcha_response_field.$dirty && accessCodeForm.recaptcha_response_field.$error.required && !accessCodeForm.recaptcha_response_field.$focused">
									<span> <em class="excl">!</em><spring:message code="label.homePage.captchaRequired"/></span>
								</label>
							</div>

						</div>

						<script type="text/javascript" src="https://www.google.com/recaptcha/api/challenge?k=6Lf2yuoSAAAAANGmqGjcb_5jw1vUEsK5AxYj9QHj"></script>
						
						<noscript>
							<iframe src="https://www.google.com/recaptcha/api/noscript?k=6Lf2yuoSAAAAANGmqGjcb_5jw1vUEsK5AxYj9QHj" height="300" width="500" frameborder="0"></iframe>
							<br>
							<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
							<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
						</noscript>
						<!--custom captcha ends-->

						<div id="access-code-button">
			 	 			<input type="submit" class="btn btn-primary" value="Submit" ng-disabled="accessCodeForm.$invalid">
		 	 			</div>
	 	 			</form>
	 	 			<div id="info" style="color: red;"></div>
	 	 		</div>
	 	 	</div>
		
		</div>
		
		
				
	</div>				
</div>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script>

var accessCodeApp = angular.module('accessCodeApp',[]);

accessCodeApp.controller('accessCodeController', function($scope) {	
	$scope.formSubmit = function($event){
		 if($event.which === 13) {
			 $event.preventDefault();
			 if($scope.accessCodeForm.$valid === true){
				 $('#access-code-form').submit();
			}else if($scope.accessCodeForm.accessCode.$valid === false){
				$scope.accessCodeForm.accessCode.$dirty = true;				
			}else if($scope.accessCodeForm.recaptcha_response_field.$valid === false){
				$scope.accessCodeForm.recaptcha_response_field.$dirty = true;				
			}
			
         }
		
	}
	
	
});

accessCodeApp.directive('ngFocus', [function() {
	var FOCUS_CLASS = "ng-focused";
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, element, attrs, ctrl) {
			ctrl.$focused = false;
			element.bind('focus', function(evt) {
				element.addClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = true;
				});
			}).bind('blur', function(evt) {
				element.removeClass(FOCUS_CLASS);
				scope.$apply(function() {
					ctrl.$focused = false;
				});
			});
		}
	};
}]);
</script>