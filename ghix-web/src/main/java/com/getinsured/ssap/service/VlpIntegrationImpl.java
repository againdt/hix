package com.getinsured.ssap.service;

import java.io.IOException;
import java.util.Properties;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.ssap.model.hub.vlp.*;
import com.getinsured.ssap.model.hub.vlp.documents.*;
import com.getinsured.ssap.model.hub.vlp.response.CaseStatusResponse;
import com.getinsured.ssap.model.hub.vlp.response.ResponseCode;
import com.getinsured.ssap.model.hub.vlp.response.VlpResponse;
import com.getinsured.ssap.util.VlpEndpoint;

/**
 * Implementation of {@link VlpIntegration} interface.
 * <p>
 *   This contains various methods to invoke and process calls to
 *   Government Hub Integration (ghix-hub) that is residing on AWS Government Cloud.
 * </p>
 * @author Yevgen Golubenko
 * @since 2/7/19
 */
@Service("vlpIntegration")
public class VlpIntegrationImpl implements VlpIntegration
{
  private static final Logger log = LoggerFactory.getLogger(VlpIntegrationImpl.class);

  private final GhixRestTemplate ghixRestTemplate;
  private final ObjectMapper om;
  private final Properties configProp;

  /*
   * Local static variables needed for communication with AWS Hub Integration
   */
  private static String hubIntegrationUrl;

  @Autowired
  public VlpIntegrationImpl(GhixRestTemplate ghixRestTemplate,
                            Properties configProp)
  {
    this.ghixRestTemplate = ghixRestTemplate;
    this.configProp = configProp;
    this.om = new ObjectMapper();
    om.enable(SerializationFeature.INDENT_OUTPUT);
    // ConsumerDocumentServiceImpl has some old logic
  }

  @PostConstruct
  public void postConstruct() {
    hubIntegrationUrl = this.configProp.getProperty("ghixHubIntegrationURL");

    if(hubIntegrationUrl == null || "".equals(hubIntegrationUrl)) {
      hubIntegrationUrl = "https://7ntk685owc.execute-api.us-gov-west-1.amazonaws.com/nv/ghix/";
      log.warn("Unable to get AWS Hub integration URL from configuration.properties, using default: {}", hubIntegrationUrl);
    }

    // Remove last slash from hub integration URL.
    if(hubIntegrationUrl.endsWith("/")) {
      log.info("Removing last slash from hub integration url: {}", hubIntegrationUrl);
      hubIntegrationUrl = hubIntegrationUrl.substring(0, hubIntegrationUrl.length() - 1);
      log.info("After removing last slash from hub integration url: {}", hubIntegrationUrl);
    }

    log.info("Initialized VlpIntegration with HUB integration URL: {}", hubIntegrationUrl);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse verify(VlpRequest vlpRequest)
  {
    final String url = hubIntegrationUrl + VlpEndpoint.VERIFICATION.url();
    log.info("Calling VLP verify: {}", url);

    return getVlpResponse(vlpRequest, url, VlpAction.VERIFY);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse reverify(VlpRequest vlpRequest)
  {
    final String url = hubIntegrationUrl + VlpEndpoint.REVERIFICATION.url();
    log.info("Calling VLP re-verify: {}", url);

    vlpRequest.setCasePOCFullName(vlpRequest.getPayload().getCasePOCFullName());
    vlpRequest.setCasePOCPhoneNumber(vlpRequest.getPayload().getCasePOCPhoneNumber());

    VlpReverifyRequest vlpReverifyRequest = new VlpReverifyRequest();
    vlpReverifyRequest.setApplicationId(vlpRequest.getApplicationId());
    vlpReverifyRequest.setClientIp(vlpRequest.getClientIp());
    // vlpReverifyRequest.setDocumentType(vlpRequest.getDocumentType().getName());
    vlpReverifyRequest.setCasePocFullName(vlpRequest.getCasePOCFullName());
    vlpReverifyRequest.setCasePocPhoneNumber(vlpRequest.getCasePOCPhoneNumber());

    VlpReverifyPayload payload = new VlpReverifyPayload();
    VlpPayload vlpPayload = vlpRequest.getPayload();
    DhsDocument document = vlpPayload.getDhsDocument();

    switch(vlpRequest.getDocumentType()) {
      case DS2019: {
        Ds2019Document doc = (Ds2019Document) document;
        payload.setI94Number(doc.getI94Number());
        payload.setSevisId(doc.getSevisId());
        // document expiration date is missing
        break;
      }
      case I20: {
        I20Document doc = (I20Document) document;
        payload.setI94Number(doc.getI94Number());
        payload.setSevisId(doc.getSevisId());
        // payload.setPassportCountry(doc.getPassportCountry());
        // document expiration date is missing
        // country of issuance missing
        break;
      }
      case I94: {
        I94Document doc = (I94Document) document;
        payload.setI94Number(doc.getI94Number());
        payload.setSevisId(doc.getSevisId());
        // document expiration date is missing
        break;
      }
      case I94_IN_PASSPORT: {
        I94InPassportDocument doc = (I94InPassportDocument) document;
        payload.setI94Number(doc.getI94Number());
        payload.setVisaNumber(doc.getVisaNumber());
        payload.setSevisId(doc.getSevisId());
        // payload.setPassportCountry(doc.getCountryOfIssuance());
        // document expiration date is missing
        // country of issuance missing
        break;
      }
      case I327: {
        I327Document doc = (I327Document) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setSevisId(doc.getSevisId());
        // document expiration date is missing
        break;
      }
      case I551: {
        I551Document doc = (I551Document) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setReceiptNumber(doc.getReceiptNumber());
        // document expiration date is missing
        break;
      }
      case CERTIFICATE_OF_CITIZENSHIP: {
        CertificateOfCitizenshipDocument doc = (CertificateOfCitizenshipDocument) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setCitizenshipNumber(doc.getCitizenshipNumber());
        break;
      }
      case I571: {
        I571Document doc = (I571Document) document;
        payload.setAlienNumber(doc.getAlienNumber());
        // document expiration date is missing
        break;
      }
      case I766: {
        I766Document doc = (I766Document) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setReceiptNumber(doc.getReceiptNumber());
        // document expiration date is missing
        break;
      }
      case MACHINE_READABLE_IMMIGRANT_VISA: {
        MachineReadableImmigrantVisaDocument doc = (MachineReadableImmigrantVisaDocument) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setVisaNumber(doc.getVisaNumber());
        // payload.setPassportCountry(doc.getCountryOfIssuance());
        // document expiration date is missing
        // passport number is missing
        // country of issuance missing
        break;
      }
      case OTHER_ALIEN: {
        OtherAlienDocument doc = (OtherAlienDocument) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setPassportCountry(doc.getPassportCountry());
        payload.setSevisId(doc.getSevisId());
        // document expiration date is missing
        // country of issuance missing
        // doc desc req is missing
        break;
      }
      case TEMPORARY_I551_STAMP: {
        TemporaryI551StampDocument doc = (TemporaryI551StampDocument) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setPassportCountry(doc.getPassportCountry());
        // country of issuance missing
        // passport number is missing
        // doc expiration date is missing
        break;
      }
      case UNEXPIRED_FOREIGN_PASSPORT: {
        UnexpiredForeignPassportDocument doc = (UnexpiredForeignPassportDocument) document;
        payload.setI94Number(doc.getI94Number());
        payload.setSevisId(doc.getSevisId());
        // country of issuance missing
        // passport number is missing
        // doc expiration date is missing
        break;
      }
      case NATURALIZATION_CERTIFICATE: {
        NaturalizationCertificateDocument doc = (NaturalizationCertificateDocument) document;
        payload.setAlienNumber(doc.getAlienNumber());
        payload.setNaturalizationNumber(doc.getNaturalizationNumber());
        break;
      }
      case OTHER_I94: {
        OtherI94Document doc = (OtherI94Document) document;
        payload.setI94Number(doc.getI94Number());
        payload.setPassportCountry(doc.getPassportCountry());
        payload.setSevisId(doc.getSevisId());
        // document expiration date is missing
        // country of issuance missing
        // doc desc req is missing
        // passport number is missing
        // document expiration date is missing
        break;
      }
    }
    payload.setCaseNumber(document.getCaseNumber());
    payload.setDateOfBirth(vlpPayload.getDateOfBirth());
    payload.setFirstName(vlpPayload.getFirstName());
    payload.setLastName(vlpPayload.getLastName());
    payload.setMiddleName(vlpPayload.getMiddleName());
    payload.setFiveYearBarApplicabilityIndicator(vlpPayload.isFiveYearBarApplicabilityIndicator());
    payload.setRequesterCommentsForHub(vlpPayload.getRequesterCommentsForHub());

    vlpReverifyRequest.setPayload(payload);

    return getVlpResponse(vlpReverifyRequest, url);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse resubmitWithSevisId(VlpRequest vlpRequest)
  {
    final String url = hubIntegrationUrl + VlpEndpoint.RESUBMIT_WITH_SAVISID.url();
    log.info("Calling VLP re-submit with SEVIS id: {}", url);

    vlpRequest.setCasePOCFullName(vlpRequest.getPayload().getCasePOCFullName());
    vlpRequest.setCasePOCPhoneNumber(vlpRequest.getPayload().getCasePOCPhoneNumber());
    return getVlpResponse(vlpRequest, url, VlpAction.RESUBMIT_WITH_SEVISID);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse secondVerification(VlpRequest vlpRequest)
  {
    final String url = hubIntegrationUrl + VlpEndpoint.SECOND_VERIFICATION.url();
    log.info("Calling VLP second verification: {}", url);

    return getVlpResponse(vlpRequest, url, VlpAction.SECOND_VERIFICATION);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse thirdVerification(VlpRequest vlpRequest)
  {
    final String url = hubIntegrationUrl + VlpEndpoint.THIRD_VERIFICATION.url();
    log.info("Calling VLP third verification: {}", url);

    return getVlpResponse(vlpRequest, url, VlpAction.THIRD_VERIFICATION);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public CaseStatusResponse caseStatus(final String caseNumber) {
    final String url = hubIntegrationUrl + VlpEndpoint.CASE_STATUS.url();
    return getCaseStatus(caseNumber, url);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse closeCase(final VlpRequest vlpRequest) {
    final String url = hubIntegrationUrl + VlpEndpoint.CLOSE_CASE.url();
    return getVlpResponse(vlpRequest, url, VlpAction.CLOSE_CASE);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse getStepTwoCaseResolution(final VlpRequest vlpRequest) {
    final String url = hubIntegrationUrl + VlpEndpoint.STEP_TWO_CASE_RESOLUTION.url();
    return getVlpResponse(vlpRequest, url, VlpAction.CHECK_CASE_RESOLUTION);
  }

  /**
   * {@inheritDoc}
   */
  @Override
  public VlpResponse getStepThreeCaseResolution(final VlpRequest vlpRequest) {
    final String url = hubIntegrationUrl + VlpEndpoint.STEP_THREE_CASE_RESOLUTION.url();
    return getVlpResponse(vlpRequest, url, VlpAction.CHECK_CASE_RESOLUTION_3);
  }

  private void logVlpResponseError(String error) {
    try {
      if (error != null) {
        JsonNode jsonNode = om.readTree(error);
        log.error("VlpRequest generated HTTP Error response: {}", om.writeValueAsString(jsonNode));
      }
    } catch (IOException e) {
      log.error("Unable to read JSON tree from given error string: {}, error: {}", error, e.getMessage());
    }
  }

  private VlpResponse getVlpResponse(VlpReverifyRequest vlpRequest, String url) {
    VlpResponse vlpResponse = new VlpResponse();

    try
    {
      if(log.isDebugEnabled()) {
        log.debug("VLP Request Object:\n{}", om.writeValueAsString(vlpRequest));
      }
      vlpResponse = ghixRestTemplate.postForObject(url, vlpRequest, VlpResponse.class);

      if(vlpResponse != null)
      {
        vlpResponse.setActionResult(VlpActionResult.get(vlpResponse));
        if(log.isDebugEnabled()) {
          log.debug("VLP Response Object:\n{}", om.writeValueAsString(vlpResponse));
          log.debug("=> VLP Action Result : {}", vlpResponse.getActionResult());
        }
      }
    }
    catch(Exception e) {
      // Our GhixRestTemplate hides original exception from us, so I'm using this instanceof crap to get it.
      // TODO: Modify GhixRestTemplate to pass down at least original HTTP status code to the caller.
      if(e instanceof RestClientException) {
        RestClientException cause = (RestClientException) e.getCause();
        if(cause != null && cause.getRootCause() instanceof HttpClientErrorException) {
          HttpClientErrorException clientErrorException = (HttpClientErrorException) cause.getRootCause();
          if(clientErrorException.getStatusCode() == HttpStatus.NOT_ACCEPTABLE) {
            vlpResponse.setResponseCode(ResponseCode.VALIDATION_FAILED);
            log.warn("There was a problem [{}] with calling: {} => {}; Response Code set to: {}", HttpStatus.NOT_ACCEPTABLE,
              url, e.getMessage(), ResponseCode.VALIDATION_FAILED);
          } else {
            vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
          }
        } else {
          vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
        }
      } else {
        vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
      }

      if(vlpResponse.getResponseCode() == ResponseCode.COMMUNICATION_ERROR) {
        log.error("There was a problem [{}] with calling: {} => {}", ResponseCode.COMMUNICATION_ERROR, url, e.getMessage());
        log.error("Detailed Exception", e);
      }
    }

    if(vlpResponse != null) {
      vlpResponse.setRemoteUrl(url);
      vlpResponse.setActionResult(VlpActionResult.get(vlpResponse));
    }

    return vlpResponse;
  }

  private VlpResponse getVlpResponse(VlpRequest vlpRequest, String url, VlpAction vlpAction) {
    VlpResponse vlpResponse = new VlpResponse();

    try
    {
      if(vlpRequest != null && vlpRequest.getDocumentType() != DocumentType.UNSUPPORTED) {
        log.info("VLP Request Object:\n{}", om.writeValueAsString(vlpRequest));

        // Do the conversion of our VlpRequest to different VlpRequest JSON based on the endpoint ... doh!
        if (vlpAction == VlpAction.RESUBMIT_WITH_SEVISID) {
          VlpResubmitWithSevisRequest request = new VlpResubmitWithSevisRequest();
          request.setApplicationId(vlpRequest.getApplicationId());
          request.setClientIp(vlpRequest.getClientIp());
          request.setCasePocFullName(vlpRequest.getCasePOCFullName());
          request.setCasePocPhoneNumber(vlpRequest.getCasePOCPhoneNumber());

          if (request.getCasePocFullName() == null) {
            request.setCasePocFullName(vlpRequest.getPayload().getCasePOCFullName());
          }

          if (request.getCasePocPhoneNumber() == null) {
            request.setCasePocPhoneNumber(vlpRequest.getPayload().getCasePOCPhoneNumber());
          }

          VlpResubmitWithSevisRequest.Payload payload = new VlpResubmitWithSevisRequest.Payload();
          payload.setCaseNumber(vlpRequest.getPayload().getDhsDocument().getCaseNumber());
          payload.setComments(vlpRequest.getPayload().getRequesterCommentsForHub());
          payload.setFiveYearBar(vlpRequest.getPayload().isFiveYearBarApplicabilityIndicator());
          payload.setSevisId(getSevisIdFromDocument(vlpRequest.getPayload().getDhsDocument()));

          request.setPayload(payload);

          vlpResponse = ghixRestTemplate.postForObject(url, request, VlpResponse.class);
        } else {
          vlpResponse = ghixRestTemplate.postForObject(url, vlpRequest, VlpResponse.class);
        }

        if (vlpResponse != null) {
          vlpResponse.setActionResult(VlpActionResult.get(vlpResponse));
          log.info("VLP Response Object:\n{}", om.writeValueAsString(vlpResponse));
          log.info("=> VLP Action Result : {}", vlpResponse.getActionResult());
        }
      } else {
        if(vlpRequest != null) {
          log.info("VLP integration, DhsDocument type = {}, marking as {}", vlpRequest.getDocumentType(), VlpActionResult.VALIDATION_FAILED);
        }
        vlpResponse.setResponseCode(ResponseCode.SUCCESS);
        vlpResponse.setActionResult(VlpActionResult.VALIDATION_FAILED);
      }
    }
    catch(Exception e) {
      // Our GhixRestTemplate hides original exception from us, so I'm using this instanceof crap to get it.
      // TODO: Modify GhixRestTemplate to pass down at least original HTTP status code to the caller.
      if(e instanceof RestClientException) {
        if(e.getCause() instanceof GIRuntimeException) {
          GIRuntimeException giRuntimeException = (GIRuntimeException)e.getCause();
          vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
          log.error("GIRuntimeException occurred while parsing VLP response from HUB: {}", giRuntimeException.getMessage());
        }
        else if(e.getCause() instanceof RestClientException)
        {
          RestClientException cause = (RestClientException) e.getCause();

          if (cause != null && cause.getRootCause() instanceof HttpClientErrorException) {
            HttpClientErrorException clientErrorException = (HttpClientErrorException) cause.getRootCause();
            if (clientErrorException.getStatusCode() == HttpStatus.NOT_ACCEPTABLE) {
              vlpResponse.setResponseCode(ResponseCode.VALIDATION_FAILED);
              log.warn("There was a problem [{}] with calling: {} => {}; Response Code set to: {}", HttpStatus.NOT_ACCEPTABLE,
                url, e.getMessage(), ResponseCode.VALIDATION_FAILED);
            } else {
              vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
            }
          } else {
            vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
          }
        }
        else {
          vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
        }
      } else {
    	  vlpResponse.setResponseCode(ResponseCode.COMMUNICATION_ERROR);
      }

      if(vlpResponse.getResponseCode() == ResponseCode.COMMUNICATION_ERROR) {
        log.error("There was a problem [{}] with calling: {} => {}", ResponseCode.COMMUNICATION_ERROR, url, e.getMessage());
        log.debug("Detailed Exception", e);
      }
    }

    if(vlpResponse != null) {
      vlpResponse.setRemoteUrl(url);
      if(vlpResponse.getActionResult() == null) {
        vlpResponse.setActionResult(VlpActionResult.get(vlpResponse));
      }
    }

    return vlpResponse;
  }

  private CaseStatusResponse getCaseStatus(final String caseNumber, String url) {
    CaseStatusResponse caseStatusResponse = null;

    try {
      url += caseNumber;
      log.info("Getting case status for case number: {}, invoking GET: {}", caseNumber, url);
      caseStatusResponse = ghixRestTemplate.getForObject(url, CaseStatusResponse.class);
      log.info("CaseStatusResponse: {}", caseStatusResponse);
    } catch(Exception see) {
      log.error("Exception while checking case status for case: {}, error: {}", caseNumber, see.getMessage());
      log.error("Exception details", see);
    }

    return caseStatusResponse;
  }

  private String getSevisIdFromDocument(DhsDocument document) {
    switch(document.getDocumentType()) {
      case DS2019: {
        Ds2019Document doc = (Ds2019Document) document;
        return doc.getSevisId();
      }
      case I20: {
        I20Document doc = (I20Document) document;
        return doc.getSevisId();
      }
      case I94: {
        I94Document doc = (I94Document) document;
        return doc.getSevisId();
      }
      case I94_IN_PASSPORT: {
        I94InPassportDocument doc = (I94InPassportDocument) document;
        return doc.getSevisId();
      }
      case I327: {
        I327Document doc = (I327Document) document;
        return doc.getSevisId();
      }
      case OTHER_ALIEN: {
        OtherAlienDocument doc = (OtherAlienDocument) document;
        return doc.getSevisId();
      }
      case UNEXPIRED_FOREIGN_PASSPORT: {
        UnexpiredForeignPassportDocument doc = (UnexpiredForeignPassportDocument) document;
        return doc.getSevisId();
      }
      case OTHER_I94: {
        OtherI94Document doc = (OtherI94Document) document;
        return doc.getSevisId();
      }
      default: {
        log.error("Document of type: {} does not have SEVIS id", document.getDocumentType());
        return null;
      }
    }
  }
}
