package com.getinsured.hix.batch.referral.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.notification.dto.ReferralLCENotificationDTO;
import com.getinsured.hix.batch.repository.BatchNoticeRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

@Service("lceReminderNotice")
public class LceReminderNoticeImpl implements LceReminderNotice {

	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private BatchNoticeRepository batchNoticeRepository;
	@Autowired
	private ILocationRepository iLocationRepository;
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	private static final Logger LOGGER = LoggerFactory.getLogger(LceReminderNoticeImpl.class);

	@PersistenceUnit
	private EntityManagerFactory emf;

	@SuppressWarnings("unchecked")
	@Override
	public List<PendingLCEDTO> appDetailsForRecurringLCENotice(String query) {
		List<Object[]> rsObj = null;
		List<PendingLCEDTO> pendingLCEDTOs = new ArrayList<PendingLCEDTO>();
		PendingLCEDTO pendingLCEDTO = null;
		int column_4 = 4;
		EntityManager em = null;
		try {
			em = emf.createEntityManager();
			
			rsObj = em.createNativeQuery(query).getResultList();

			for (Object result : rsObj) {
				Object[] row = (Object[]) result;
				pendingLCEDTO = new PendingLCEDTO();
				pendingLCEDTO.setSsapApplicationId(((BigDecimal) row[0]).longValue());
				pendingLCEDTO.setCaseNumber((String) row[1]);
				pendingLCEDTO.setSsapApplicationCreationTimeStamp((Timestamp) row[2]);
				pendingLCEDTO.setCmrHouseHoldId(row[3] != null ? ((BigDecimal) row[3]).intValue() : 0);
				pendingLCEDTO.setIntervalFactor(((BigDecimal) row[column_4]).intValue());
				pendingLCEDTOs.add(pendingLCEDTO);
			}

		} catch (Exception e) {
			LOGGER.error("error while fetching pening LCE applications with Exception " + e.getMessage());
			throw e;
		}
		finally
        {
          if (em != null && em.isOpen())
          {
            try
            {
              em.close();
            }
            catch (IllegalStateException ex)
            {
            	if(LOGGER.isErrorEnabled())
            	{
            		LOGGER.error("EntityManager is managed by application server", ex);
	            }
            }
          }
        }
		return pendingLCEDTOs;
	}

	@Autowired private PreferencesService preferencesService;
	
	public String generate(String caseNumber, String noticeTemplateName) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(caseNumber);

		int moduleId = extractHouseholdId(ssapApplication);
		ReferralLCENotificationDTO referralLCENotificationDTO = getReferralLCENotificationDTO(ssapApplication, moduleId);

		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		
		PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);
		Location location = getLocation(preferencesDTO);
		//Location location = getLocation(ssapApplication);

		String relativePath = "cmr/" + moduleId + "/ssap/" + ssapApplication.getId() + "/notifications/";
		String ecmFileName = noticeTemplateName + "_" + moduleId + (new Date().getTime()) + ".pdf";

		//String emailId = getEmailId(ssapApplication);
		String emailId = getEmailId(preferencesDTO);

		GhixNoticeCommunicationMethod noticeCommunicationPref = getCommunicationMethod(preferencesDTO);
		
		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(referralLCENotificationDTO,noticeTemplateName), relativePath, ecmFileName, moduleName, moduleId, validEmails,
		        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), fullName, location, noticeCommunicationPref);

		return notice.getEcmId();
	}
	
	private GhixNoticeCommunicationMethod getCommunicationMethod(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getPrefCommunication() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getPrefCommunication() is null");
		}
		
		return preferencesDTO.getPrefCommunication();
	}

	private String getEmailId(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null){
			throw new GIRuntimeException("preferencesDTO is null");
		}
		
		return !StringUtils.isEmpty(preferencesDTO.getEmailAddress()) ? preferencesDTO.getEmailAddress() : null;
	}

	private Location getLocation(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getLocationDto() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getLocationDto() is null");
		}
		
		LocationDTO locationDto = preferencesDTO.getLocationDto();
		
		if (StringUtils.isEmpty(locationDto.getAddressLine1()) || StringUtils.isEmpty(locationDto.getCity()) || 
				StringUtils.isEmpty(locationDto.getState()) || StringUtils.isEmpty(locationDto.getZipcode())){
			throw new GIRuntimeException("preferencesDTO.getLocationDto() address fields missing");
		}
		
		Location location = new Location();
		location.setAddress1(locationDto.getAddressLine1());
		location.setAddress2(locationDto.getAddressLine2());
		location.setCity(locationDto.getCity());
		location.setState(locationDto.getState());
		location.setZip(locationDto.getZipcode());
		
		return location;
	}

	private int extractHouseholdId(SsapApplication ssapApplication) {

		if (ssapApplication.getCmrHouseoldId() != null && ssapApplication.getCmrHouseoldId().intValue() != 0) {
			return ssapApplication.getCmrHouseoldId().intValue();
		} else {
			throw new GIRuntimeException("Cmr Household Id not found for SSAP Application " + ssapApplication.getId());
		}

	}

	private Location getLocation(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		return null;
	}

	private ReferralLCENotificationDTO getReferralLCENotificationDTO(SsapApplication ssapApplication, int moduleId) {
		ReferralLCENotificationDTO referralLCENotificationDTO = new ReferralLCENotificationDTO();

		referralLCENotificationDTO.setUserName(getUserName(moduleId));

		referralLCENotificationDTO.setCaseNumber(ssapApplication.getCaseNumber());

		referralLCENotificationDTO.setPrimaryApplicantName(getName(ssapApplication));

		referralLCENotificationDTO.setApplicationValidationStatus(ssapApplication.getValidationStatus() != null ? ssapApplication.getValidationStatus().name() : StringUtils.EMPTY);

		List<SsapApplicationEvent> applicationEvents = ssapApplicationEventRepository.findEventBySsapApplicationId(ssapApplication.getId());
		SsapApplicationEvent ssapApplicationEvent = (applicationEvents != null && applicationEvents.size() > 0) ? applicationEvents.get(0) : null;

		if (ssapApplicationEvent != null) {
			referralLCENotificationDTO.setEnrollmentEndDate(ssapApplicationEvent.getEnrollmentEndDate());

			referralLCENotificationDTO.setKeepOnly(ssapApplicationEvent.getKeepOnly());
		}
		
		referralLCENotificationDTO.setSsapApplicationId(ssapApplication.getId());

		return referralLCENotificationDTO;
	}

	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}

		return null;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName();
			}
		}

		return null;
	}

	private SsapApplication getSSAPApplicationData(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException("SSAP Id is null.Unable to fetch household id. Email cannot be triggered for" + caseNumber);
	}

	private String getUserName(int householdId) {

		return cmrHouseholdRepository.findUserNameByCmrId(householdId);
	}

	private Map<String, Object> getReplaceableObjectData(ReferralLCENotificationDTO referralLCENotificationDTO, String noticeTemplateName) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();

		tokens.put("referralLCENotificationDTO", referralLCENotificationDTO);
		if (noticeTemplateName.equalsIgnoreCase("SEPEvent") && referralLCENotificationDTO.getEnrollmentEndDate() != null) {
			tokens.put("updateSubject", "Your Special Enrollment Period Expires on " + ReferralUtil.formatDate(referralLCENotificationDTO.getEnrollmentEndDate(), ReferralConstants.DEFDATEPATTERN));

		}
		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new Date()));
		tokens.put("todaysDate", DateUtil.dateToString(new Date(), "MMMM dd, YYYY"));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("exgName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", referralLCENotificationDTO.getSsapApplicationId());

		return tokens;
	}

	@Override
	public boolean isNoticeSent(PendingLCEDTO pendingLCEDTO, Integer INTERVAL, Integer BUFFER_DAYS_TO_RETRY) {
		long count;
		DateTime startDate = new DateTime(pendingLCEDTO.getSsapApplicationCreationTimeStamp()).plusDays(INTERVAL * pendingLCEDTO.getIntervalFactor());
		DateTime endDate = new DateTime(pendingLCEDTO.getSsapApplicationCreationTimeStamp()).plusDays((INTERVAL * pendingLCEDTO.getIntervalFactor()) + BUFFER_DAYS_TO_RETRY);
		count = batchNoticeRepository.lCENoticeCreatedForModuleIdBetween(startDate.toDate(), endDate.toDate(), pendingLCEDTO.getCmrHouseHoldId(), pendingLCEDTO.getNotificationName());
		LOGGER.debug(" isNoticeSent " + pendingLCEDTO.getSsapApplicationId() + " as " + count);
		return count > 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PendingLCEDTO> appDetailsForRecurringLCESEPNotice(String query) {
		List<Object[]> rsObj = null;
		List<PendingLCEDTO> pendingLCEDTOs = new ArrayList<PendingLCEDTO>();
		PendingLCEDTO pendingLCEDTO = null;
		int column_4 = 4;
		EntityManager em = null;
		
		try {
			em = emf.createEntityManager();
			
			rsObj = em.createNativeQuery(query).getResultList();

			for (Object result : rsObj) {
				Object[] row = (Object[]) result;
				pendingLCEDTO = new PendingLCEDTO();
				pendingLCEDTO.setSsapApplicationId(((BigDecimal) row[0]).longValue());
				pendingLCEDTO.setCaseNumber((String) row[1]);
				pendingLCEDTO.setSsapApplicationCreationTimeStamp((Timestamp) row[2]);
				pendingLCEDTO.setCmrHouseHoldId(row[3] != null ? ((BigDecimal) row[3]).intValue() : 0);
				pendingLCEDTO.setIntervalFactor(((BigDecimal) row[column_4]).intValue());
				pendingLCEDTO.setKeepOnly((String) row[5]);
				String validationStatus =  row[6]!=null?  row[6].toString():null;
				ApplicationValidationStatus status = null;
				if(validationStatus != null){
					status = ApplicationValidationStatus.valueOf(validationStatus);
				}
				pendingLCEDTO.setApplicationValidationStatus(status);
				pendingLCEDTOs.add(pendingLCEDTO);
			}

		} catch (Exception e) {
			LOGGER.error("error while fetching pening LCE applications with Exception " + e.getMessage());
			throw e;
		}
		finally
        {
          if (em != null && em.isOpen())
          {
            try
            {
              em.close();
            }
            catch (IllegalStateException ex)
            {
            	if(LOGGER.isErrorEnabled())
            	{
	              LOGGER.error("EntityManager is managed by application server", ex);
	            }
            }
          }
        }

		return pendingLCEDTOs;
	}

}
