
package com.getinsured.iex.ssap;

import java.util.Date;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class CurrentEmployerInsurance {

    @Expose
    private String currentLowestCostSelfOnlyPlanName;
    @Expose
    private Boolean willBeEnrolledInEmployerPlanIndicator=false;
    @Expose
    private Date memberEmployerPlanStartDate;
    @Expose
    private Date willBeEnrolledInEmployerPlanDate;
    @Expose
    private Boolean comingPlanMeetsMinimumStandardIndicator=false;
    @Expose
    private Boolean memberPlansToDropEmployerPlanIndicator=false;
    @Expose
    private Boolean employerWillOfferPlanIndicator=false;
    @Expose
    private Boolean expectedChangesToEmployerCoverageIndicator=false;
    @Expose
    private Date employerCoverageEndingDate;
    @Expose
    private Boolean memberPlansToEnrollInEmployerPlanIndicator=false;
    @Expose
    private Date employerPlanStartDate;
    @Expose
    private Date memberEmployerPlanEndingDate;
    @Expose
    private String comingLowestCostSelfOnlyPlanName;
    @Expose
    private Boolean currentPlanMeetsMinimumStandardIndicator=false;
    @Expose
    private Boolean employerWillNotOfferCoverageIndicator=false;
    @Expose
    private Boolean isCurrentlyEnrolledInEmployerPlanIndicator=false;

    @Expose
    private String insuranceName;

    @Expose
    private String policyNumber;

    public String getCurrentLowestCostSelfOnlyPlanName() {
        return currentLowestCostSelfOnlyPlanName;
    }

    public void setCurrentLowestCostSelfOnlyPlanName(String currentLowestCostSelfOnlyPlanName) {
        this.currentLowestCostSelfOnlyPlanName = currentLowestCostSelfOnlyPlanName;
    }

    public Boolean getWillBeEnrolledInEmployerPlanIndicator() {
        return willBeEnrolledInEmployerPlanIndicator;
    }

    public void setWillBeEnrolledInEmployerPlanIndicator(Boolean willBeEnrolledInEmployerPlanIndicator) {
        this.willBeEnrolledInEmployerPlanIndicator = willBeEnrolledInEmployerPlanIndicator;
    }

    public Date getMemberEmployerPlanStartDate() {
        return memberEmployerPlanStartDate;
    }

    public void setMemberEmployerPlanStartDate(Date memberEmployerPlanStartDate) {
        this.memberEmployerPlanStartDate = memberEmployerPlanStartDate;
    }

    public Date getWillBeEnrolledInEmployerPlanDate() {
        return willBeEnrolledInEmployerPlanDate;
    }

    public void setWillBeEnrolledInEmployerPlanDate(Date willBeEnrolledInEmployerPlanDate) {
        this.willBeEnrolledInEmployerPlanDate = willBeEnrolledInEmployerPlanDate;
    }

    public Boolean getComingPlanMeetsMinimumStandardIndicator() {
        return comingPlanMeetsMinimumStandardIndicator;
    }

    public void setComingPlanMeetsMinimumStandardIndicator(Boolean comingPlanMeetsMinimumStandardIndicator) {
        this.comingPlanMeetsMinimumStandardIndicator = comingPlanMeetsMinimumStandardIndicator;
    }

    public Boolean getMemberPlansToDropEmployerPlanIndicator() {
        return memberPlansToDropEmployerPlanIndicator;
    }

    public void setMemberPlansToDropEmployerPlanIndicator(Boolean memberPlansToDropEmployerPlanIndicator) {
        this.memberPlansToDropEmployerPlanIndicator = memberPlansToDropEmployerPlanIndicator;
    }

    public Boolean getEmployerWillOfferPlanIndicator() {
        return employerWillOfferPlanIndicator;
    }

    public void setEmployerWillOfferPlanIndicator(Boolean employerWillOfferPlanIndicator) {
        this.employerWillOfferPlanIndicator = employerWillOfferPlanIndicator;
    }

    public Boolean getExpectedChangesToEmployerCoverageIndicator() {
        return expectedChangesToEmployerCoverageIndicator;
    }

    public void setExpectedChangesToEmployerCoverageIndicator(Boolean expectedChangesToEmployerCoverageIndicator) {
        this.expectedChangesToEmployerCoverageIndicator = expectedChangesToEmployerCoverageIndicator;
    }

    public Date getEmployerCoverageEndingDate() {
        return employerCoverageEndingDate;
    }

    public void setEmployerCoverageEndingDate(Date employerCoverageEndingDate) {
        this.employerCoverageEndingDate = employerCoverageEndingDate;
    }

    public Boolean getMemberPlansToEnrollInEmployerPlanIndicator() {
        return memberPlansToEnrollInEmployerPlanIndicator;
    }

    public void setMemberPlansToEnrollInEmployerPlanIndicator(Boolean memberPlansToEnrollInEmployerPlanIndicator) {
        this.memberPlansToEnrollInEmployerPlanIndicator = memberPlansToEnrollInEmployerPlanIndicator;
    }

    public Date getEmployerPlanStartDate() {
        return employerPlanStartDate;
    }

    public void setEmployerPlanStartDate(Date employerPlanStartDate) {
        this.employerPlanStartDate = employerPlanStartDate;
    }

    public Date getMemberEmployerPlanEndingDate() {
        return memberEmployerPlanEndingDate;
    }

    public void setMemberEmployerPlanEndingDate(Date memberEmployerPlanEndingDate) {
        this.memberEmployerPlanEndingDate = memberEmployerPlanEndingDate;
    }

    public String getComingLowestCostSelfOnlyPlanName() {
        return comingLowestCostSelfOnlyPlanName;
    }

    public void setComingLowestCostSelfOnlyPlanName(String comingLowestCostSelfOnlyPlanName) {
        this.comingLowestCostSelfOnlyPlanName = comingLowestCostSelfOnlyPlanName;
    }

    public Boolean getCurrentPlanMeetsMinimumStandardIndicator() {
        return currentPlanMeetsMinimumStandardIndicator;
    }

    public void setCurrentPlanMeetsMinimumStandardIndicator(Boolean currentPlanMeetsMinimumStandardIndicator) {
        this.currentPlanMeetsMinimumStandardIndicator = currentPlanMeetsMinimumStandardIndicator;
    }

    public Boolean getEmployerWillNotOfferCoverageIndicator() {
        return employerWillNotOfferCoverageIndicator;
    }

    public void setEmployerWillNotOfferCoverageIndicator(Boolean employerWillNotOfferCoverageIndicator) {
        this.employerWillNotOfferCoverageIndicator = employerWillNotOfferCoverageIndicator;
    }

    public Boolean getIsCurrentlyEnrolledInEmployerPlanIndicator() {
        return isCurrentlyEnrolledInEmployerPlanIndicator;
    }

    public void setIsCurrentlyEnrolledInEmployerPlanIndicator(Boolean isCurrentlyEnrolledInEmployerPlanIndicator) {
        this.isCurrentlyEnrolledInEmployerPlanIndicator = isCurrentlyEnrolledInEmployerPlanIndicator;
    }

    public Boolean getCurrentlyEnrolledInEmployerPlanIndicator()
    {
        return isCurrentlyEnrolledInEmployerPlanIndicator;
    }

    public void setCurrentlyEnrolledInEmployerPlanIndicator(final Boolean currentlyEnrolledInEmployerPlanIndicator)
    {
        isCurrentlyEnrolledInEmployerPlanIndicator = currentlyEnrolledInEmployerPlanIndicator;
    }

    public String getInsuranceName()
    {
        return insuranceName;
    }

    public void setInsuranceName(final String insuranceName)
    {
        this.insuranceName = insuranceName;
    }

    public String getPolicyNumber()
    {
        return policyNumber;
    }

    public void setPolicyNumber(final String policyNumber)
    {
        this.policyNumber = policyNumber;
    }
}
