package com.getinsured.hix.admin.service.jpa;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.getinsured.hix.admin.repository.IAnnouncementRepository;
import com.getinsured.hix.admin.service.AnnouncementRoleService;
import com.getinsured.hix.admin.service.AnnouncementService;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.Announcement.Status;
import com.getinsured.hix.model.AnnouncementRole;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.QueryBuilder;
import com.getinsured.hix.platform.util.QueryBuilder.ComparisonType;
import com.getinsured.hix.platform.util.QueryBuilder.DataType;
import com.getinsured.hix.platform.util.QueryBuilder.SortOrder;

@Service("announcementService")
@Repository
public class AnnouncementServiceImpl implements AnnouncementService {
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementServiceImpl.class);
	
	@Autowired IAnnouncementRepository announcementRepository;
	@Autowired private RoleService roleService;
	@Autowired private UserService userService;
	@Autowired private AnnouncementRoleService announcementRoleService;
	@Autowired private ObjectFactory<QueryBuilder> delegateFactory;
	
	public Announcement findById(Integer id){
		return announcementRepository.findOne(id);
	}
	
	public Announcement save(Announcement announcement){
		return announcementRepository.save(announcement);
	}
	
	public List<Announcement> findAll(){
		return announcementRepository.findAll();
	}
	
	public Announcement editAnnouncement(Announcement announcementFormObj){
		Announcement announcementDbObj = this.findById(announcementFormObj.getId());
		
		announcementRoleService.deleteAnnouncementRole(announcementDbObj);
		
		announcementDbObj.setName(announcementFormObj.getName());
		announcementDbObj.setAnnouncementText(announcementFormObj.getAnnouncementText());
		announcementDbObj.setEffectiveStartDate(announcementFormObj.getEffectiveStartDate());
		announcementDbObj.setEffectiveEndDate(announcementFormObj.getEffectiveEndDate());
		announcementDbObj.setEffectiveEndDate(announcementFormObj.getEffectiveEndDate());
		announcementDbObj.setStatus(announcementFormObj.getStatus());
		announcementDbObj.setUpdatedBy(announcementFormObj.getUpdatedBy());
		
		List<AnnouncementRole> announcementRole = announcementFormObj.getAnnouncementRole();
		ArrayList<AnnouncementRole> newRolelist = new ArrayList<AnnouncementRole>();
		int count = announcementRole.size();
		for(int i=0; i < count; i++)
		{
			if(announcementRole.get(i).getRole() != null){
				newRolelist.add(announcementRole.get(i));
			}
		}
		announcementDbObj.setAnnouncementRole(newRolelist);
		return this.save(announcementDbObj);
	}
	
	public Announcement findByName(String name){
		return announcementRepository.findByName(name);
	}
	
	public void deleteAnnouncement(Integer id){
		announcementRepository.delete(id);
	}
	
	public List<AnnouncementRole> setAnnouncementRoleList(Announcement announcement, String[] roles){
		List<AnnouncementRole> announcementRoleList = new ArrayList<AnnouncementRole>();
		 if (roles != null) 
		 {
			 for (int i = 0; i < roles.length; i++) 
		      {
				 if(roles[i] != null){
					 Role role = roleService.findById(Integer.parseInt(roles[i]));
					 AnnouncementRole announcementRole = announcementRoleService.setAnnouncementRole(announcement, role);
					 announcementRoleList.add(announcementRole);
				 }
		      }
		 }
		 return announcementRoleList;
	}
	
	public List<String> viewAnnouncement(){
		List<String> announcementDesc = new ArrayList<String>();
		AccountUser user = null;
		int roleid = 0;
		try{
           user = userService.getLoggedInUser();
           Set<UserRole> userRoles = user.getUserRole();
           
	   		for  (UserRole userRole : userRoles)
	   	    {
	   			roleid = userRole.getRole().getId();
	   			break;
	        }
        }catch(InvalidUserException ex){
	      LOGGER.error("User not logged in");
	      return announcementDesc;
        }
		return announcementRepository.viewAnnouncement(Status.APPROVED, roleid, new TSDate());
	}
	
public  Map<String,Object> searchAnnouncement(Map<String,Object> searchCriteria){
		
		List<String> joinTables = new ArrayList<String>();
		joinTables.add("announcementRole");
		QueryBuilder<Announcement> announcementQuery = delegateFactory.getObject();
		announcementQuery.buildSelectQuery(Announcement.class, joinTables);
		
		String name = null;
		if(searchCriteria != null && searchCriteria.get("name") != null && !searchCriteria.get("name").equals("") ){
			name = (String) searchCriteria.get("name");
		}
		DateFormat formatter;
	    formatter = new SimpleDateFormat("MM/dd/yyyy");
		Date effectiveStartDate = null;
		if(searchCriteria != null && searchCriteria.get("effectiveStartDate") != null){
			try {
				effectiveStartDate = (Date) formatter.parse((String) searchCriteria.get("effectiveStartDate"));
			} catch (ParseException e) {
				//e.printStackTrace();
				LOGGER.error("",e);
			} 
		}
		Date effectiveEndDate = null;
		if(searchCriteria != null && searchCriteria.get("effectiveEndDate") != null){
			try {
				effectiveEndDate = (Date) formatter.parse((String) searchCriteria.get("effectiveEndDate"));
			} catch (ParseException e) {
				//e.printStackTrace();
				LOGGER.error("",e);
			} 
		}
		Status status =  null;
		if(searchCriteria != null && searchCriteria.get("status") != null){
			status = Status.valueOf((String) searchCriteria.get("status"));
		}
		int role = 0;
		if(searchCriteria != null && searchCriteria.get("role") != null){
			role = Integer.parseInt((String) searchCriteria.get("role"));
		}
		if (name != null && name.length() > 0)
		{			
			announcementQuery.applyWhere("name", name.toUpperCase(), DataType.STRING, ComparisonType.LIKE);
		}
		if (status != null)
		{			
			announcementQuery.applyWhere("status", status, DataType.ENUM, ComparisonType.LIKE);
		}
		if (role > 0)
		{			
			announcementQuery.applyWhere("announcementRole.role.id", role, DataType.NUMERIC, ComparisonType.EQ);
		}
		
		if(effectiveStartDate != null){
			announcementQuery.applyWhere("effectiveStartDate", effectiveStartDate, DataType.DATE, ComparisonType.GE);
		}
		
		if(effectiveEndDate != null){
			announcementQuery.applyWhere("effectiveEndDate", effectiveEndDate, DataType.DATE, ComparisonType.LE);
		}
		announcementQuery.applySort(searchCriteria.get("sortBy").toString(), SortOrder.valueOf(searchCriteria.get("sortOrder").toString()));   
		
		List<String> columns = new ArrayList<String>();
        columns.add("id");
        columns.add("name");
        columns.add("effectiveStartDate");
        columns.add("effectiveEndDate");
        columns.add("status");
        announcementQuery.applySelectColumns(columns);
		
		Map<String,Object> announcementListAndRecordCount = new HashMap<String,Object>();
		try {
			//List<Announcement> announcements = announcementQuery.getRecords(startRecord, pageSize);
			announcementQuery.setFetchDistinct(true);
			List<Map<String, Object>> announcements = announcementQuery.getData((Integer)searchCriteria.get("startRecord"), (Integer)searchCriteria.get("pageSize"));
			announcementListAndRecordCount.put("announcementlist", announcements);
			announcementListAndRecordCount.put("recordCount", announcementQuery.getRecordCount());
		} catch (Exception e) {
			//e.printStackTrace();
			LOGGER.error("",e);
		}
		return announcementListAndRecordCount;
	}

	@Override
	public List<Announcement> getActiveAnnoucements(String moduleName) {
		
		List<Announcement> announcementList = announcementRepository.getActiveAnnoucements(moduleName, new java.sql.Date(new TSDate().getTime()));
		
		return announcementList;
	}
	
}

