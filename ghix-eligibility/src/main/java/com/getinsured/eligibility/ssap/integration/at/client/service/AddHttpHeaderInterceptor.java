package com.getinsured.eligibility.ssap.integration.at.client.service;

import org.apache.log4j.Logger;
import org.springframework.ws.client.WebServiceClientException;
import org.springframework.ws.client.support.interceptor.ClientInterceptor;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.transport.context.TransportContext;
import org.springframework.ws.transport.context.TransportContextHolder;
import org.springframework.ws.transport.http.AbstractHttpSenderConnection;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferConstants;

public class AddHttpHeaderInterceptor implements ClientInterceptor {
	
	private static Logger lOGGER = Logger.getLogger(AddHttpHeaderInterceptor.class);
	
	public boolean handleFault(MessageContext messageContext)
	        throws WebServiceClientException {
	    return true;
	}
	
	public boolean handleRequest(MessageContext messageContext)
	        throws WebServiceClientException {
	     TransportContext context = TransportContextHolder.getTransportContext();
	     AbstractHttpSenderConnection connection =(AbstractHttpSenderConnection) context.getConnection();
	     try {
			connection.addRequestHeader(AccountTransferConstants.REQUEST_SOURCE, AccountTransferConstants.REQUEST_SOURCE_VALUE.toString());
		} catch (Exception e) {
			lOGGER.error(e.getMessage());
		}
	
	    return true;
	}

	public boolean handleResponse(MessageContext messageContext)
	        throws WebServiceClientException {
	    return true;
	}
	
	@Override
	public void afterCompletion(MessageContext messageContext, Exception ex)
			throws WebServiceClientException {
		// TODO Auto-generated method stub
		
	}

}