package com.getinsured.hix.iex.lce;

/*import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.getinsured.hix.account.web.UserController;
import com.getinsured.hix.iex.lce.LifeChangeEventController.ChangedApplicant;
import com.getinsured.hix.iex.lce.LifeChangeEventController.RequestParamDTO;
import com.getinsured.hix.iex.lce.utils.LifeChangeEventConstant;
import com.getinsured.hix.platform.test.GhixBaseMvcTest;
import com.google.gson.Gson;*/

import com.getinsured.hix.platform.test.GhixBaseMvcTest;

public class LifeChangeEventControllerTest extends GhixBaseMvcTest{
	
	/*public static class MockSecurityContext implements SecurityContext {

        private static final long serialVersionUID = -1386535243513362694L;

        private Authentication authentication;

        public MockSecurityContext(Authentication authentication) {
            this.authentication = authentication;
        }

        @Override
        public Authentication getAuthentication() {
            return this.authentication;
        }

        @Override
        public void setAuthentication(Authentication authentication) {
            this.authentication = authentication;
        }
    }
	
	@Autowired UserController userController; 
	private MockMvc mockMvc;
	private MockHttpSession session = new MockHttpSession();

    @Resource
    private FilterChainProxy springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    @Autowired
	LifeChangeEventController lifeChangeEventController;
	
	
    @Before
    public void mySetUp() {

    	// setup mock MVC
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .addFilters(this.springSecurityFilterChain)
                .build();
        
		UsernamePasswordAuthenticationToken principal = 
		userController.getPrincipal("testevent@ghix.com");
		SecurityContextHolder.getContext().setAuthentication(principal);        

		session.setAttribute(
    		  HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, 
    		  new MockSecurityContext(principal));
        
    }
    
	@Test
    public void signedIn() throws Exception {
        mockMvc.perform(get("/account/user/login")
                .session(session))
        .andExpect(status().isOk());
	}
    
   @Test
	public void testMaritalStatus() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "03/03/2013";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_MARITAL_STATUS);
    		ssapJson = LifeChangeEventControllerMock.loadMaritalStatusJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(gson.toJson(events));
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}

	@Test
	public void testIncarceration() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "03/03/2013";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_INCARCERATION);
    		ssapJson = LifeChangeEventControllerMock.loadIncarcerationJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}

	@Test
	public void testNoOfDependents() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "03/03/2013";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS);
    		ssapJson = LifeChangeEventControllerMock.loadNoOfDependentsJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}

	@Test
	public void testChangeInAddress() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "03/03/2013";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS);
    		ssapJson = LifeChangeEventControllerMock.loadChangeInAddressJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testChangeInName() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_CHANGE_IN_NAME);
    		ssapJson = LifeChangeEventControllerMock.loadNameChangeJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testChangeInIncome() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_INCOME);
    		ssapJson = LifeChangeEventControllerMock.loadChangeInIncomeJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testLawfulPresence() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE);
    		ssapJson = LifeChangeEventControllerMock.loadLawfulPresenceJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testLossOfMinimumEssentialCoverage() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE);
    		ssapJson = LifeChangeEventControllerMock.loadLossOfMinimumEssentialCoverageJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testGainInMinimumEssentialCoverage() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE);
    		ssapJson = LifeChangeEventControllerMock.loadGainInMinimumEssentialCoverageJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	@Test
	public void testChangeInTribeStatus() {
		String ssapJson;
		List<RequestParamDTO> events;
		String esignDate = "11/06/2014";
		Gson gson = new Gson();
		
    	try {
    		events = createRequestParamDTO(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS);
    		ssapJson = LifeChangeEventControllerMock.loadChangeInTribeStatusJson();
        	MvcResult result = mockMvc.perform(post("/iex/lce/savessapapplication")
    				.param("ssapJson", ssapJson)
    				.param("ESIGN_DATE", esignDate)
    				.param("eventInfo", gson.toJson(events))
    				.session(session))
    				.andReturn();
    		String content = result.getResponse().getContentAsString();
    		System.out.println(content);
    	} catch(Exception exception){
    		Assert.fail();
    	}
	}
	
	private List<RequestParamDTO> createRequestParamDTO(String type) {
		List<RequestParamDTO> events = new ArrayList<>();

		try {
			LifeChangeEventController.RequestParamDTO requestParamDTO = lifeChangeEventController.new RequestParamDTO();

			if (LifeChangeEventConstant.LCE_MARITAL_STATUS.equals(type)) {
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_MARITAL_STATUS);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_MARITAL_STATUS_MARRIAGE);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_MARITAL_STATUS));
			} else if(LifeChangeEventConstant.LCE_INCARCERATION.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_INCARCERATION);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_INCARCERATION_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_INCARCERATION));
			} else if(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS_ADD_DEPENDENT);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS));
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS));
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_NAME.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_CHANGE_IN_NAME);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_CHANGE_IN_NAME_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_CHANGE_IN_NAME));
			} else if(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE_OTHER_MINIMUM_ESSENTIAL_COVERAGE_ENDS);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE));
			} else if(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE_REACH_AGE_65_OR_MEDICARE);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE));
			} else if(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE));
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS));
			} else if(LifeChangeEventConstant.LCE_INCOME.equals(type)){
				requestParamDTO.setEventCategory(LifeChangeEventConstant.LCE_INCOME);
				requestParamDTO.setEventSubCategory(LifeChangeEventConstant.LCE_INCOME_SUB_CATEGORY);
				requestParamDTO.setChangedApplicants(getChangedApplicants(LifeChangeEventConstant.LCE_INCOME));
			}

			events.add(requestParamDTO);
		} catch (Exception exception) {
			throw exception;
		}
		return events;
	}

	private List<ChangedApplicant> getChangedApplicants(String type) {
		List<ChangedApplicant> list = new ArrayList<>();
		LifeChangeEventController.ChangedApplicant changedApplicant = lifeChangeEventController.new ChangedApplicant();

		try {
			if (LifeChangeEventConstant.LCE_MARITAL_STATUS.equals(type)) {
				changedApplicant.setPersonId(1l);
				changedApplicant.setEventDate("01/01/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_INCARCERATION.equals(type)){
				changedApplicant.setPersonId(2l);
				changedApplicant.setEventDate("01/01/2013");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_NUMBER_OF_DEPENDENTS.equals(type)){
				changedApplicant.setPersonId(3l);
				changedApplicant.setEventDate("01/01/2012");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_RESIDENCE_OR_MAILING_ADDRESS.equals(type)){
				changedApplicant.setPersonId(3l);
				changedApplicant.setEventDate("01/01/2011");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_NAME.equals(type)){
				changedApplicant.setPersonId(2l);
				changedApplicant.setEventDate("05/26/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_LOSS_OF_MINIMUM_ESSENTIAL_COVERAGE.equals(type)){
				changedApplicant.setPersonId(2l);
				changedApplicant.setEventDate("05/27/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_GAIN_OTHER_MINIMUM_ESSENTIAL_COVERAGE.equals(type)){
				changedApplicant.setPersonId(1l);
				changedApplicant.setEventDate("05/29/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_LAWFUL_PRESENCE.equals(type)){
				changedApplicant.setPersonId(1l);
				changedApplicant.setEventDate("05/27/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_CHANGE_IN_TRIBE_STATUS.equals(type)){
				changedApplicant.setPersonId(2l);
				changedApplicant.setEventDate("05/27/2014");
				list.add(changedApplicant);
			} else if(LifeChangeEventConstant.LCE_INCOME.equals(type)){
				changedApplicant.setPersonId(1l);
				changedApplicant.setEventDate("05/30/2014");
				list.add(changedApplicant);
			}
		} catch (Exception exception) {
			throw exception;
		}
		return list;
	}*/
}
