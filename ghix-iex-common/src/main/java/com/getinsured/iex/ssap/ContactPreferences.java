
package com.getinsured.iex.ssap;

import org.hibernate.validator.constraints.Email;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class ContactPreferences {

	@Email
    @Expose
    private String emailAddress;
    @Expose
    private String preferredSpokenLanguage;
    @Expose
    private String preferredContactMethod;
    @Expose
    private String preferredWrittenLanguage;

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getPreferredSpokenLanguage() {
        return preferredSpokenLanguage;
    }

    public void setPreferredSpokenLanguage(String preferredSpokenLanguage) {
        this.preferredSpokenLanguage = preferredSpokenLanguage;
    }

    public String getPreferredContactMethod() {
        return preferredContactMethod;
    }

    public void setPreferredContactMethod(String preferredContactMethod) {
        this.preferredContactMethod = preferredContactMethod;
    }

    public String getPreferredWrittenLanguage() {
        return preferredWrittenLanguage;
    }

    public void setPreferredWrittenLanguage(String preferredWrittenLanguage) {
        this.preferredWrittenLanguage = preferredWrittenLanguage;
    }

}
