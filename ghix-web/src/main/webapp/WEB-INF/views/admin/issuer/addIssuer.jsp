<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="label.issuerTitle"/></a></li>
            <li><a href="<c:url value="/admin/addnewissuer"/>"><spring:message  code='label.addNewIssuer'/></a></li>
            <li><spring:message  code='label.issuerDetails'/></li>
        </ul><!--page-breadcrumb ends-->
        <h1><a name="skip"></a><spring:message  code='label.addNewIssuer'/></h1>
	</div>
    
	<div class="row-fluid">
        <div class="span3" id="sidebar">
       		<div class="header">
            	<h4 class="margin0"><spring:message  code='label.steps'/></h4>
            </div>
            <!--  beginning of side bar -->
            <ol class="nav nav-list">
                <li class="active"><a href="<c:url value="/admin/addnewissuer"> <c:param name="isBack" value="true"/>  </c:url>"><spring:message  code='label.issuerDetails'/></a></li>
                <c:choose>
    				<c:when test="${showOtherNavigation == 'true'}">
    				 	<li> <a href="<c:url value="/admin/issuer/primaryrepinfo"> <c:param name="isBack" value="true"/>  </c:url>" ><spring:message  code="label.primarycontactdetails"/></a></li>
                   		<%--  <li><a href="<c:url value="/admin/issuer/primaryrepinfo"/>"><spring:message  code='label.primarycontactdetails'/></a></li> --%>

						<c:choose>
							<c:when test="${allowReviewAndSubmit == 'true'}">
								<li><a href="<c:url value="/admin/issuer/reviewissuer"/>"><spring:message code='label.reviewandsubmit' /></a></li>
							</c:when>
							<c:otherwise>
								<li><a class="disableLink" href="#"><spring:message code='label.reviewandsubmit' /></a></li>
							</c:otherwise>							
						</c:choose>
					</c:when>
                    <c:otherwise>
                    	<li><a class="disableLink" href="#"><spring:message  code='label.primarycontactdetails'/></a></li>
                    	<li><a class="disableLink" href="#"><spring:message  code='label.reviewandsubmit'/></a></li>
                    </c:otherwise>
                 </c:choose>   
            </ol>
            <!-- end of side bar -->
        </div>
        
		<%--
		<c:if test="${duplicateIssuerErr == 'true'}">
  	 		<div class="error help-inline">
               <label class="error"><span><spring:message  code="err.duplicateIssuer"/></span></label>
           </div> 
       </c:if>
		--%>
       
        <div class="span9" id="rightpanel">
        	<div class="header">
                <h4><spring:message  code="label.issuerDetails"/></h4>
            </div>
            <div class="gutter10">
                <form class="form-horizontal" id="frmIssuerAdd" name="frmIssuerAdd" action="<c:url value='/admin/addnewissuer'/>" method="POST">
                	<df:csrfToken/>
                	<div class="control-group">
                        <label for="hiosIssuerId" class="required control-label"><spring:message  code="label.HIOSIssuerId"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="hiosIssuerId" id="hiosIssuerId" value="${issuerInfoSubmitted.hiosIssuerId}" class="input-xlarge"  maxlength="5">
                                <div id="hiosIssuerId_error" class="error help-block"></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
    				<c:if test="${duplicateIssuerErr == 'true'}">
                      <div class="error">
                         <label class="error"><span><spring:message  code="err.duplicateIssuer"/></span></label>
                      </div> 
                    </c:if>
                    <div class="control-group">
                        <label for="name" class="required control-label"><spring:message  code="label.name"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="name" id="name" value="${issuerInfoSubmitted.name}" class="input-xlarge" maxlength="75">
                                <div id="name_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="companyLegalName" class="required control-label"><spring:message  code="label.companyLegalName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="companyLegalName" id="companyLegalName" class="input-xlarge" value="${issuerInfoSubmitted.companyLegalName}" maxlength="100" />
                                <div id="companyLegalName_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="shortName" class="required control-label"><spring:message  code="label.shortName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="shortName" id="shortName" value="${issuerInfoSubmitted.shortName}" class="input-xlarge"  maxlength="50">
                                <div id="shortName_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <%--
                    This code is commented as per JIRA Number HIX-6752 
                    <div class="control-group">
                        <label for="licenseNumber" class="required control-label"><spring:message  code="label.licenseNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="licenseNumber" id="licenseNumber" value="${issuerInfoSubmitted.licenseNumber}" class="input-medium">
                                <div id="licenseNumber_error" class=""></div>
                            </div> <!-- end of controls-->
                        
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="licenseStatus" class="required control-label"><spring:message  code="label.licenseStatus"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <select size="1"  id="licenseStatus" name="licenseStatus" path="licenseStatusList">
                                <c:forEach var="issuerLicenseStatus" items="${issuerLicenseStatusList}">
                                    <option <c:if test="${issuerLicenseStatus.key == issuerInfoSubmitted.licenseStatus}"> SELECTED </c:if> value="${issuerLicenseStatus.key}">${issuerLicenseStatus.value}</option>
                                </c:forEach>
                                </select>
                                <div id="licenseStatus_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group --> 
                    --%>
                    
                   <div class="control-group">                    
		           		<label for="naicCompanyCode" class="required control-label"><spring:message  code="label.NAICCompanyNumber"/> <c:if test="${exchangeState != 'CA'}"> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></c:if></label>
		                <div class="controls">
		                    <input type="text" name="naicCompanyCode" id="naicCompanyCode" value="${issuerInfoSubmitted.naicCompanyCode}" class="input-xlarge"  maxlength="25">
		                    <div id="naicCompanyCode_error" class=""></div>
		                </div> <!-- end of controls-->
		           </div><!-- end of control-group -->
                   
                   <div class="control-group">
		           		<label for="naicGroupCode" class="required control-label"><spring:message  code="label.NAICGroupCode"/> <c:if test="${exchangeState != 'CA'}"> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></c:if></label>
		                <div class="controls">
		                     <input type="text" name="naicGroupCode" id="naicGroupCode" value="${issuerInfoSubmitted.naicGroupCode}" class="input-xlarge"  maxlength="25">
		                     <div id="naicGroupCode_error" class=""></div>
		                 </div> <!-- end of controls-->
		          </div><!-- end of control-group -->
                    
                   <div class="control-group">
                        <label for="federalEin" class="control-label"><spring:message  code="label.FederalEmployerId"/> </label>
                            <div class="controls">
                                <input type="text" name="federalEin" id="federalEin" class="input-xlarge" value="${issuerInfoSubmitted.federalEin}"  maxlength="9"/>
                                <div id="federalEin_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div> <!-- end of control-group -->
                    
                    <%-- <div class="control-group">
                        <label for="Txn834Version" class="required control-label"><spring:message  code="label.txn834Version"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="txn834Version" id="txn834Version" value="${issuerInfoSubmitted.txn834Version}" class="input-medium">
                                <div id="txn834Version_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="Txn820Version" class="required control-label"><spring:message  code="label.txn820Version"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="txn820Version" id="txn820Version" value="${issuerInfoSubmitted.txn820Version}" class="input-medium" >
                                <div id="txn820Version_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group --> --%>
                    
                    <div class="control-group">
                        <label for="addressLine1" class="required control-label"><spring:message  code="label.streetaddress1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="addressLine1" id="addressLine1" value="${issuerInfoSubmitted.addressLine1}" class="input-xlarge"  maxlength="100">
                                <div id="addressLine1_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="addressLine2" class="required control-label"><spring:message  code="label.streetaddress2"/><%-- <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%></label>
                            <div class="controls">
                                <input type="text" name="addressLine2" id="addressLine2" value="${issuerInfoSubmitted.addressLine2}" class="input-xlarge"  maxlength="100">
                                <div id="addressLine2_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                        <label for="city" class="required control-label"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <input type="text" name="city" id="city" value="${issuerInfoSubmitted.city}" class="input-xlarge"  maxlength="30">
                                <div id="city_error" class=""></div>
                            </div> <!-- end of controls-->
                    </div><!-- end of control-group -->
                    
                    <div class="control-group">
                            <label for="state" class="required control-label"><spring:message  code="label.emplState"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                            <div class="controls">
                                <select id="state" name="state" path="statelist" class="input-xlarge">
                                     <option value="">Select</option>
                                     <c:forEach var="state" items="${statelist}">
                                        <option <c:if test="${state.code == issuerInfoSubmitted.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                    </c:forEach>
                                </select>
                                <div id="state_error"></div>
                            </div>
                        </div><!-- end of control-group -->
                        
                        <div class="control-group">
                            <label for="zip" class="required control-label"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                <div class="controls">
                                    <input type="text" name="zip" id="zip" value="${issuerInfoSubmitted.zip}" class="input-small" maxlength="5">
                                    <div id="zip_error" class=""></div>
                                </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
   				
                <div class="form-actions">
                	<input type="hidden" id="existhios"/>
                	 <input type="hidden" id="isNext" name="isNext" value="true"/>
                     <input type="submit" name="submit1" id="submit1" value="<spring:message  code="label.next"/>" class="btn btn-primary" title="<spring:message  code="label.next"/>"/>
                </div><!-- end of form-actions -->
             </form>
			</div>
        </div>
        <!-- end of span9 -->
        <!-- broker registration -->

    </div>
    <!--  end of row-fluid -->
	</div>



<script type="text/javascript">
var exchangeState = '${exchangeState}';
var csrValue = $("#csrftoken").val();
var flagValue = true;
if(exchangeState == 'CA'){	
	flagValue = false;
}else {	
	flagValue = true;
}

$.validator.addMethod("alphanumericRegex", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
});


$.validator.addMethod("isDuplicate", function() {
	return isDuplicateHios();
	});	


var validator = $("#frmIssuerAdd").validate({ 
	rules : {
		name : {required : true},
		companyLegalName : {required : true},
		shortName : {required : true},
		licenseNumber : {required : true},
		licenseStatus : { required : true},
		naicCompanyCode : { required : flagValue, alphanumericRegex:true},
		naicGroupCode : { required : flagValue, alphanumericRegex:true},
		federalEin : {digits: true,  minlength:9, maxlength:9},
		hiosIssuerId : { required : true,  number: true, minlength:5, maxlength:5,  isDuplicate:true, isGreaterThanZero: true},
		addressLine1 : { required : true},
		//addressLine2 : { required : true},
		city : {required : true},
		state : { required : true},
		zip : { required : true, digits: true, minlength:5, maxlength:5}
	},
	messages : {
		name : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerName' javaScriptEscape='true'/></span>"},
		companyLegalName : { required : "<span><em class='excl'>!</em><spring:message  code='err.companyLegalName' javaScriptEscape='true'/></span>"},
		shortName : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerShortName' javaScriptEscape='true'/></span>"},
		licenseNumber: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerLicenseNumber' javaScriptEscape='true'/></span>"},
		licenseStatus : { required : "<span><em class='excl'>!</em><spring:message  code='err.licenseStatus' javaScriptEscape='true'/></span>"},
		naicCompanyCode: { required : "<span><em class='excl'>!</em><spring:message  code='err.naicCompanyCode' javaScriptEscape='true'/></span>",
						   alphanumericRegex : "<span><em class='excl'>!</em><spring:message  code='err.validNaicCompanyCode' javaScriptEscape='true'/></span>"
						 },		
		naicGroupCode: { required : "<span><em class='excl'>!</em><spring:message  code='err.naicGroupCode' javaScriptEscape='true'/></span>",
						 alphanumericRegex : "<span><em class='excl'>!</em><spring:message  code='err.validNaicGroupCode' javaScriptEscape='true'/></span>"
					   },
		federalEin: {digits : "<span><em class='excl'>!</em><spring:message  code='err.validFederalEmployerId' javaScriptEscape='true'/></span>", 
			minlength : "<span><em class='excl'>!</em><spring:message  code='err.federalEmployerId' javaScriptEscape='true'/></span>"
			},
		hiosIssuerId: { required : "<span><em class='excl'>!</em><spring:message  code='err.hiosIssuerId' javaScriptEscape='true'/></span>",			
			isDuplicate:"",
			number:"<span><em class='excl'>!</em><spring:message  code='err.validHios' javaScriptEscape='true'/></span>",
			isGreaterThanZero:"<span><em class='excl'>!</em><spring:message  code='err.validHios' javaScriptEscape='true'/></span>",
			//maxlength : "<span><em class='excl'>!</em><spring:message  code='err.hiosIdLength' javaScriptEscape='true'/></span>"
			minlength:"<span><em class='excl'>!</em><spring:message  code='err.hiosIdLength' javaScriptEscape='true'/></span>"
		},		
		addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerAddress' javaScriptEscape='true'/></span>"},
		//addressLine2 : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerAddress' javaScriptEscape='true'/></span>"},
		city : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
		state: { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
		zip : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>", 
			minlength:"<span><em class='excl'>!</em><spring:message  code='err.zipLenght' javaScriptEscape='true'/></span>",
			digits:"<span><em class='excl'>!</em><spring:message  code='err.zipOnlyDigits' javaScriptEscape='true'/></span>"}		
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
	}
});

function isDuplicateHios(){	
	
	if($("#hiosIssuerId").val() != ""){  // if hiosId is not empty
		//var returnVal = true ;
		$.ajax({
			type : "POST",
			url: "<c:url value='/admin/issuer/checkDuplicateHios'/>",
			data: { "hiosId": $("#hiosIssuerId").val(),"csrftoken":csrValue},
			success: function(responseText){			
				
				if(responseText!=""){						
					$('#hiosIssuerId_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><em class='excl'>!</em><spring:message  code='err.duplicateHios' javaScriptEscape='true'/></span></label>");
					$("#existhios").val("1");
					
				} 
				else{						
					$("#existhios").val("0");
					
				}
	       	}
		});
		

		if ($('#existhios').val() > 0 ){			
			return false;
		}else if ($('#existhios').val() == 0 ){
			$('#hiosIssuerId_error').html("");		
			return true ;
		}
	
	}
	
	
	
}

jQuery.validator.addMethod("isGreaterThanZero", function (value, element, param) {
	
    var hiosIssuerId = $("#hiosIssuerId").val();      
    if(hiosIssuerId != null && hiosIssuerId >0) {
    	return true
    }
    
    return false;
});
</script>
