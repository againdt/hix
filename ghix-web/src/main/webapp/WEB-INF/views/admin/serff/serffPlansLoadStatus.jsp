<!doctype html>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>

<link rel="stylesheet" href="//cdn.rawgit.com/angular-ui/bower-ui-grid/master/ui-grid.min.css">

<style>
.ui-grid-pager-control input {
    height: 14px;
}

.ui-grid-pager-control .ui-grid-pager-max-pages-number {
    vertical-align: middle;
}

#containsOfLogsArea {
  width: 95%;
  height: 95%;
}

#containerLogs {
    width: 80%;
	height: 80%;
}
</style>

<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/angular-animate.1.5.6.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/angular-touch.1.5.6.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/ui-grid.min.4.0.2.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/csv.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/pdfmake.js" />'></script>
<script type="text/javascript" src='<c:url value="/resources/js/reports/lib/vfs_fonts.js" />'></script>

<script src="<gi:cdnurl value="/resources/js/lce/ui-bootstrap-tpls-0.11.0.js"/>"></script>
<script src="<gi:cdnurl value="/resources/js/dashboardAnnouncement/angular-ui-router.min.js"/>"></script>

<script src='<c:url value="/resources/js/planMgmt/planLoadStatusList.js" />'></script>

<div class="gutter10" ng-app="planLoadStatusListApp" ng-controller="PlanLoadStatusListCtrl">

	<div id="containerLogs" class="modal hide fade in" style="display: none;">
		<div class="modal-header">
			<a class="close" data-dismiss="modal" data-original-title="">x</a>
			<span><spring:message code="label.planLoadStatus.popup"/></span>
		</div>

		<div class="modal-body" align="center">
			<textarea id="containsOfLogsArea" width="100%" name="containsOfLogsArea" rows="20" cols="100"></textarea>
		</div>
	</div>

	<div class="row-fluid">
		<div class="row-fluid">
			<h1><spring:message code="label.planLoadStatus.header"/></h1>
		</div>

		<div class="row-fluid">
			<form class="form-vertical gutter10 lightgray" id="frmPlanLoadStatus" name="frmPlanLoadStatus" action="<c:url value="/admin/serff/serffPlansLoadStatus" />" method="POST">
				<df:csrfToken/>
				<input type="hidden" name="exchangeState" id="exchangeState" value="${exchangeState}" />

				<div class="control-group span3">
					<label for="issuerFilter" class="control-label"><spring:message code="label.planLoadStatus.issuer"/></label>
					<div class="controls">
						<select name="issuerFilter" ng-model="issuerFilter">
							<option value="" selected="selected"><spring:message code="label.selectOption"/></option>
							<c:forEach var="issuer" items="${issuerList}">
								<option value="${issuer.hiosIssuerId}">[${issuer.hiosIssuerId}] ${issuer.shortName}</option>
							</c:forEach>
						</select>
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span3">
					<label for="typeFilter" class="control-label"><spring:message code="label.planLoadStatus.type"/></label>
					<div class="controls">
						<select name="typeFilter" ng-model="typeFilter">
							<option value="" selected="selected"><spring:message code="label.selectOption"/></option>
							<c:forEach var="type" items="${planTypeList}">
								<option value="${type.key}">${type.value}</option>
							</c:forEach>
						</select>
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span3">
					<label for="statusFilter" class="control-label"><spring:message code="label.planLoadStatus.status"/></label>
					<div class="controls">
						<select name="statusFilter" ng-model="statusFilter">
							<option value="" selected="selected"><spring:message code="label.selectOption"/></option>
							<c:forEach var="status" items="${planStatusList}">
								<option value="${status.key}">${status.value}</option>
							</c:forEach>
						</select>
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span2">
					<label for="dateFilter" class="control-label"><spring:message code="label.planLoadStatus.date"/></label>
					<div class="controls input-append date date-picker" id="date" data-date-format="mm/dd/yyyy">
						<input	type="text" id="dateFilter" name="dateFilter" class="datepick input-small" title="MM/dd/yyyy">
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div> <!-- end of controls-->
				</div> <!-- end of control-group -->

				<div class="control-group span4">
					<div class="controls">
						<input type="button" class="btn btn-primary" ng-click="getPlanDataReport()" value="<spring:message code='label.go' />" title="<spring:message  code='label.go' />">&nbsp;&nbsp;
						<input type="button" class="btn" ng-click="resetFilter()" id="ResetFilter" name="ResetFilter" value="Reset" title="Reset" />
						<c:if test="${exchangeState == 'WA' || exchangeState == 'CT'}">
						<a href="${pageContext.request.contextPath}/admin/serff/executeBatch?sourceScreen=plansLoadStatus">
							<input type="button" class="btn btn-primary offset1" id="executeBatch" name="executeBatch" value="Process waiting" <c:if test="${hasWaitingPlansToLoad == false}">disabled="disabled"</c:if> />
						</a>
						</c:if>
					</div>
				</div> <!-- end of control-group -->
			</form>
		</div>
	</div><!--  end of row-fluid -->

   	<div>
		<div ui-grid="gridOptions" style="height: 100%" class="grid" ui-grid-pagination ui-grid-resize-columns></div>
	</div>
</div>
