<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="org.springframework.web.servlet.support.RequestContextUtils"%>
<%@page import="org.springframework.web.context.WebApplicationContext"%>
<%@page import="org.springframework.context.i18n.LocaleContextHolder"%>
<%@page import="org.springframework.context.MessageSource"%>
<%@page import="org.springframework.beans.factory.annotation.Autowired"%>
<%@page import="com.getinsured.hix.platform.util.DisplayUtil"%>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<style>
	#masthead .nav-collapse li {
		display: none;
	}
	#masthead .nav-collapse li.dropdown,
	#masthead .nav-collapse li.dropdown li {
		display: block;
	}
	#menu {
		display: none!important;
	}
	p {
		font-size: 15px;
		line-height: 24px;
	}
</style>


<div class="gutter10">
	<div class="row-fluid">
		<div class="span10 offset1" id="rightpanel">
			<div class="alert alert-info margin20-t margin20-b txt-center">
				<div id="referralModal" class="modal hide fade">
					<div class="modal-body">
						<p class="alert alert-error">Your account information does not match the primary applicant data. Do you want to override?</p>
						<table class="table table-condensed">
							<thead>
								<tr>
									<td></td>
									<td>Household Applicant</td>
									<td>Referral Applicant</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>First Name:</td><td>${referralLinkingOverride.householdApplicant.firstName}</td><td>${referralLinkingOverride.referralApplicant.firstName}</td>
								</tr>
								<tr>
									<td>Last Name:</td><td>${referralLinkingOverride.householdApplicant.lastName}</td><td>${referralLinkingOverride.referralApplicant.lastName}</td>
								</tr>
								<tr>
									<td>Date of Birth:</td><td>${referralLinkingOverride.householdApplicant.formattedDateofBirth}</td><td>${referralLinkingOverride.referralApplicant.formattedDateofBirth}</td>
								</tr>
								<tr>
									<td>Zip Code:</td><td>${referralLinkingOverride.householdApplicant.zipCode}</td><td>${referralLinkingOverride.referralApplicant.zipCode}</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="modal-footer">
						<button class="btn btn-primary" onclick="override('${referralActivationId}');">Yes</button>
						<button class="btn btn-primary" onclick="gotoDashBoard();">No</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(window).load(function() {
		$('#referralModal').modal();
	});
	
	function override(referralActivationId) {
		document.location.href="<%=GhixConstants.APP_URL%>referral/ridp/link/"+referralActivationId;
	}
	
	function gotoDashBoard() {
		document.location.href="<%=GhixConstants.APP_URL%>indportal";
	}

</script>
