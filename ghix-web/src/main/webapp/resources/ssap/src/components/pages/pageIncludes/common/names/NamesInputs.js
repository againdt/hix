import React, {Fragment} from 'react';
import _ from 'lodash';

import UniversalInput from '../../../../../lib/UniversalInput';

import DROPDOWN_OPTIONS from '../../../../../constants/dropdownOptions';
import INPUT_TYPES from '../../../../../constants/inputTypes';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../../../../constants/validationErrorMessages';

const NamesInputs = props => {

    const {
        firstName,
        middleName,
        lastName,
        suffix,
        currentData,
        fields,
        isRequired,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <Fragment>
            <UniversalInput
                label={{
                    labelClasses: ['gi-input'],
                    text: 'First Name',
                    isLabelRequired: isRequired
                }}
                inputData={{
                    inputClasses: [],
                    isInputRequired: isRequired,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'firstName',
                    helperClasses: [],
                    value: firstName,
                    placeholder: 'Enter First Name',
                    validationType: VALIDATION_TYPES.NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.FIRST_NAME,
                    currentData: currentData,
                    fields
                }}
                errorClasses={['usa-offset-one-third']}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    labelClasses: ['gi-input'],
                    text: 'Middle Name',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'middleName',
                    helperClasses: [],
                    value: middleName,
                    placeholder: 'Enter Middle Name',
                    validationType: VALIDATION_TYPES.MIDDLE_NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.MIDDLE_NAME,
                    currentData: currentData,
                    fields
                }}
                errorClasses={['usa-offset-one-third']}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    labelClasses: ['gi-input'],
                    text: 'Last Name',
                    isLabelRequired: isRequired
                }}
                inputData={{
                    isInputRequired: isRequired,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'lastName',
                    helperClasses: [],
                    value: lastName,
                    placeholder: 'Enter Last Name',
                    validationType: VALIDATION_TYPES.NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.LAST_NAME,
                    currentData: currentData,
                    fields
                }}
                errorClasses={['usa-offset-one-third']}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Suffix',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'suffix',
                    value: suffix,
                    options: DROPDOWN_OPTIONS.SUFFIX,
                    currentData: currentData,
                    fields
                }}
                errorClasses={['usa-offset-one-third']}
                {...cleanedRestProps}
            />
        </Fragment>
    );
};

export default NamesInputs;
