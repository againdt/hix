var app = angular.module('providerLoadStatusListApp', ['ui.grid', 'ui.bootstrap', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('ProviderLoadStatusListCtrl', ['$scope', 'ProviderLoadStatusListService', 'uiGridConstants', function ($scope, ProviderLoadStatusListService, uiGridConstants) {

	var paginationOptions = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	$scope.statusList = null;
	$scope.setStatusList = function(dataList) {
		$scope.statusList = dataList;
	};

	$scope.issuerList = null;
	$scope.setIssuerList = function(dataList) {
		$scope.issuerList = dataList;
	};

	$scope.downloadDataFile = function(data) {
		ProviderLoadStatusListService.downloadProviderFile(data);
	};

	$scope.downloadProviderLogs = function(data) {
		ProviderLoadStatusListService.downloadProviderLogs(data);
	};

	$scope.colDef = [];

	var displayName = {
		fileName : 'File Name',
		planYear : 'Plan Year',
		providerType : 'Provider Type',
		status : 'Status',
		createdOn : 'Date',
		logs : 'Logs',
		file : 'File'
	};

	var setPropertiesFn = function (elem, key) {

        switch (key) {
			case 'fileName':
			case 'planYear':
			case 'providerType':
			case 'status':
			case 'createdOn':
				elem.displayName = displayName[key];
        }
	};

	var excludeFields = ['id', 'logs', 'ecmFileID'];

	ProviderLoadStatusListService.getReport(paginationOptions.pageNumber).success(function(data) {

//		debugger;
		$scope.gridOptions.data = data.content;

		if (data.size != 0) {

			angular.forEach(data.content[0], function(value, key) {

				if (!excludeFields.includes(key)) {
					var elem = null;

					if (key == 'fileName')  {
						elem = {name:key, width: 400};
					}
					else if (key == 'planYear' || key == 'status')  {
						elem = {name:key, width: 95};
					}
					else if (key == 'createdOn')  {
						elem = {name:key, width: 100};
					}
					else if (key == 'providerType')  {
						elem = {name:key, width: 125};
					}
					else {
						elem = {name:key, width: 150};	// Default width
					}
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
			});

			// Add Logs column as last
			$scope.colDef.push({
				name: 'logs',
				visible: true,
				displayName: 'Logs',
				enableSorting: false,
				cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.downloadProviderLogs(COL_FIELD)">Download</a>',
				width: 90
			});

			// Add File column as last
			$scope.colDef.push({
				name: 'ecmFileID',
				visible: true,
				displayName: 'File',
				enableSorting: false,
				cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.downloadDataFile(COL_FIELD)">Download</a>',
				width: 90
			});
		}
		$scope.gridOptions.totalItems = data.totalElements;
	});

	$scope.gridOptions = {
		paginationPageSizes: [10],
		paginationPageSize: paginationOptions.pageSize,
		useExternalPagination: true,
		columnDefs:  $scope.colDef,
		enableGridMenu: false,
		enableFiltering: false,
		enableSorting: true,
		// exporterPdfFilename: 'download.pdf',

		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;

				ProviderLoadStatusListService.getReport(newPage).success(function(data) {
//					debugger;
					$scope.gridOptions.data = data.content;
					$scope.gridOptions.totalItems = data.totalElements;
				});
			});
		}
	};
}]);

app.service('ProviderLoadStatusListService',['$http', function ($http) {

	function getReport(pageNumber) {
//		debugger;
		pageNumber = pageNumber > 0 ? pageNumber - 1 : 0;

		return  $http({
			method: 'GET',
			url: '/hix/admin/provider/getProviderLoadStatusDataList?page=' + pageNumber
		});
	}

	function downloadProviderFile(selectedDocumentId) {
//		debugger;

		if (!selectedDocumentId) {
			alert("Document ID is empty");
		}
		else {
			window.location.href = '/hix/admin/provider/downloadProviderFile?selectedDocumentId=' + selectedDocumentId;
		}
	}

	function downloadProviderLogs(selectedLogs) {
//		debugger;

		if (!selectedLogs) {
			alert("Logs is empty");
		}
		else {
			var loForm = document.forms["frmDownloadProviderLogs"];
			loForm["selectedLogs"].value = selectedLogs;
			loForm.method = "post";
			loForm.submit();
		}
	}

	return {
		getReport:getReport,
		downloadProviderFile:downloadProviderFile,
		downloadProviderLogs:downloadProviderLogs
	};
}]);
