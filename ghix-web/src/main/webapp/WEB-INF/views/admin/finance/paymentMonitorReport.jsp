<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	<!-- <script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>-->
<script type="text/javascript">
var showAndHideRowNum = 0;
</script>

<%@ page isELIgnored="false"%>
<style>
	.sss{
		max-width:600px !important;
		overflow-x:auto;
	}
</style>

<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
	<li><a href="<c:url value="/admin/paymentMonitorReport"/>"><spring:message  code="label.back"/></a></li>
	<li class="active">Payment Status Report</li>
	</ul>
			<h1>View Payment Monitor Report <a href="#" rel="tooltip" data-placement="top"
			data-original-title="Report to review detailed payment statuses for the invoices.
			You can filter by date range, invoice number, transaction status,  as well as other criteria." 
			class="info"><i class="icon-question-sign"></i></a></h1>

</div>


<div class="row-fluid">
<div class="span12 gray" id="rightpanel">
<div class="header">
				<h4 class="margin0">Refine Results</h4>
	</div>
	
	<form class="form-vertical margin10" id="frmpaymentmonitorfilter"
			name="frmpaymentmonitorfilter" action="paymentMonitorReport" method = "post">
            <df:csrfToken/>
            <div class="span2 margin20-lr">
             	<div class="control-group">
					<label class="control-label required" for="startdate" style="text-align: left">Start Date </label>
					<div class="input-append date date-picker" id="date">
						<input class="input-small" type="text" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" onChange="setdate();"/> 
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>	
				 	<!-- <input title="MM/DD/YYYY" type = "text" class="datepick input-small" name="startdate" id="mystartdate" value="${searchCriteria.startdate}" onChange="setdate();"/>-->					<!-- 	<div id="mystartdate_error"></div> -->
				</div>
						
				<div class="control-group">
					<label class="control-label required" for="enddate" style="text-align: left">End Date </label>
					<div class="input-append date date-picker" id="date">
						<input class="input-small" type="text" name="enddate" id="myenddate" value="${searchCriteria.enddate}" onChange="setdate();"/> 
						<span class="add-on"><i class="icon-calendar"></i></span>
					</div>
					<!-- <div class="controls">
						<input title="MM/DD/YYYY" type = "text" class="datepick input-small" name="enddate" id="myenddate" value="${searchCriteria.enddate}"  onChange="setdate();"/>
					</div> -->
				</div>
            
            </div>
            
             <div class="span2 margin20-lr">
             <div class="control-group">
            			<label class="control-label for="status" style="text-align: left">Status </label>
							<input type = "text" class="input-medium" placeholder="Status" name="status" id="status" value="${searchCriteria.status}">
							<div id="carrierName_error"></div>
						</div>
						
						<div class="control-group">
            			<label class="control-label for="txId" style="text-align: left">Transaction Id </label>
							<input type = "text" class="input-medium" placeholder="Transaction Id" name="requestId" id="requestId" value="${searchCriteria.requestId}">
							<div id="carrierName_error"></div>
						</div>
             
             </div>
             
              <div class="span2 margin20-lr">
              	<div class="control-group">
            			<label class="control-label for="carrierName" style="text-align: left">Carrier</label>
							<input  type = "text" class="input-medium" placeholder="Carrier" name="carrierName" id="carrierName" value="${searchCriteria.carrierName}">
							<div id="carrierName_error"></div>
						</div>
						
						<div class="control-group">
            			<label class="control-label for="employerName" style="text-align: left">Employer</label>
							<input type = "text" class="input-medium" placeholder="Employer" name="employerName" id="employerName" value="${searchCriteria.employerName}">
							<div id="carrierName_error"></div>
						</div>
              
              </div>
              
               <div class="span2 margin20-lr">
              	<div class="control-group">
            			<label class="control-label for="invoiceNumber" style="text-align: left">Invoice Number</label>
							<input type = "text" class="input-medium" placeholder="Invoice Number" name="invoiceNumber" id="invoiceNumber" value="${searchCriteria.invoiceNumber}">
							<div id="carrierName_error"></div>
						</div>
						
						<div class="control-group">
            			<label class="control-label for="merchantId" style="text-align: left">Merchant Id</label>
							<input type = "text" class="input-medium" placeholder="Merchant Id" name="merchantId" id="merchantId" value="${searchCriteria.merchantId}">
							<div id="carrierName_error"></div>
						</div>
              
              </div>
              
                 <div class="span2 margin68-t">       	
                 	<div class="control-group">
                 	   <div id="myenddate_error"></div>
					<input type="submit" name="submitBtn" id="submitBtn" Value="Search" title="Search"
						class="btn btn-primary pull-right">
                 	</div>
                 
                 </div>
                 
            
					</form>
</div>
</div>

 <div class="row-fluid">	
<div class="span12">
		<form id="paymentStatusReportList"
			name="paymentStatusReportList" action="paymentMonitorReport" method="POST" style="overflow-x:auto;">
			<df:csrfToken/>
		<!-- 	 <input type="hidden" value="" name="carrierId" id="carrierId" />
			  <input type="hidden" value="" name="employerId" id="employerId" /> -->
			<%-- <input type="hidden" value="${employerid}" name="id" id="id" />
			<input type="hidden" value="" name="startdate" id="startdate" />
		    <input type="hidden" value="" name="enddate" id="enddate" /> --%>
			<%-- <c:choose>
				<c:when test="${resultSize >= 1}"> --%>
					<c:choose>
					
					     <c:when test="${not empty ErrorMsg}">
                         <h4 class="alert alert-error h4">${ErrorMsg}</h4>
                         </c:when>
						<c:when test="${fn:length(paymentEventLogList) gt 0}">
								<!-- <table class="table table-condensed table-border-none table-striped"> -->
								<table class="table table-condensed table924 table-striped">
									<thead>
										<tr class="header">
											<!--  span9 header -->
											<!-- <th><input type="checkbox"> </th> -->
								
									
									        <%-- <th class="sortable" scope="col"><dl:sort title="Carrier Id" sortBy="carrierId"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
													<th>Carrier Name</th>
											<%-- <th class="sortable" scope="col"><dl:sort title="Employer Id" sortBy="employerId"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>		
									
											<th>Employer Name</th>
											<%-- <th class="sortable" scope="col"><dl:sort title="Transaction Reason Code" sortBy="txnReasonCode"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
													<th>Invoice Number</th>
													<th>Amount Paid</th>
											<th>Transaction Reason Code</th>		
											<th class="sortable" scope="col"><dl:sort title="Transaction Status" sortBy="txnStatus"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th class="sortable" scope="col"><dl:sort title="Payment Date" sortBy="createdOn"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>		
									<%-- 		<th class="sortable" scope="col"><dl:sort title="Event Type" sortBy="eventType"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
											<th class="sortable" scope="col"><dl:sort title="Transaction Id" sortBy="requestId"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<!-- //<th></th> -->
											<th class="sortable" scope="col"><dl:sort title="MerchantRefCode" sortBy="merchantRefCode"
													sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
											<th></th>
										</tr>
									</thead>
									
									<c:forEach items="${paymentEventLogList}" var="paymentEventLog" varStatus="vs">
							<tr >
								<!-- <td><input type="checkbox"> </td> -->
								<%-- <td>${payable.issuerId}</td> --%>
								
								 <td style="width:100px"> <c:choose>
								 <c:when test="${empty paymentEventLog.issuerInvoices}">
       							 &nbsp;
   								 </c:when>
    							 <c:otherwise> ${paymentEventLog.issuerInvoices.issuer.name}</c:otherwise>
    							 </c:choose> </td>
    							 <td> <c:choose>
								 <c:when test="${empty paymentEventLog.employerInvoices}">
       							 &nbsp;
   								 </c:when>
    							<c:otherwise> ${paymentEventLog.employerInvoices.employer.name}</c:otherwise>
    							</c:choose> </td>
    							
    							 <td>
       							 <c:choose>
       							 <c:when test="${not empty paymentEventLog.employerInvoices}">
    							 ${paymentEventLog.employerInvoices.invoiceNumber}
       							 </c:when>
       							 <c:when test="${not empty paymentEventLog.issuerInvoices}">
       							 ${paymentEventLog.issuerInvoices.invoiceNumber}
       							 </c:when>
       							 <c:otherwise>&nbsp;</c:otherwise>
       							 </c:choose>
       							 </td>
       							 
       							 <td>
       							 <c:choose>
       							 <c:when test="${not empty paymentEventLog.employerPayments}">
       							 ${paymentEventLog.employerPayments.amountEnclosed}
       							 </c:when>
       							 <c:when test="${not empty paymentEventLog.issuerPaymentDetail}">
       							 ${paymentEventLog.issuerPaymentDetail.amount}
       							 </c:when>
       							 <c:otherwise>&nbsp;</c:otherwise>
       							 </c:choose>
       							 </td>
    							
								<td>${paymentEventLog.txnReasonCode}</td>
								<td>${paymentEventLog.txnStatus}</td>
								<td>${paymentEventLog.createdOn}</td>
							<%-- 	<td>${paymentEventLog.eventType}</td> --%>
								<td>${paymentEventLog.requestId}</td>
								<td>${paymentEventLog.merchantRefCode}</td>
								<td><c:choose>
										<c:when test="${'ACCEPT' == paymentEventLog.txnStatus}">
											<button type="button" id="dropdown"
												onclick="javascript:getPaymentEventData(${paymentEventLog.id},'${paymentEventLog.merchantRefCode}');">
												<b>v</b>
											</button>
										</c:when>
										<c:otherwise>&nbsp;</c:otherwise>
									</c:choose></td>
							</tr>
							
							<!-- dropdown Row code starts here -->
							<tr style="display: none;" id="hide${paymentEventLog.id}">
								<td></td>
								<td colspan="7">
									<table id="excelDataTable${paymentEventLog.id}">
									</table>
								</td>
							</tr>
							<!-- dropdown row code ends here -->

						</c:forEach>
								</table>
                                <div class="pagination">
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}"/>
								</div>
						</c:when>
							<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
						
					</c:choose>
		</form>
		<div class="center">
		<c:if test="${fn:length(paymentEventLogList) gt 0}">
         <button class="btn" data-dismiss="modal" aria-hidden="true" id="downloadPaymentTransactions">Download</button>
        </c:if>
		</div>
	</div>
	</div>
</div>



<script type="text/javascript">
$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});
/* $(function() {
			$('.datepick').each(function() {
				var ctx = "${pageContext.request.contextPath}";
				var imgpath = ctx+'/resources/images/calendar.gif';
				$(this).datepicker({
					showOn : "button",
					buttonImage : imgpath,
					buttonImageOnly : true
				});
			});
		});
 */		

function setdate(){
	$("#startdate").val($("#mystartdate").val());
	$("#enddate").val($("#myenddate").val());
}
</script>

<script type="text/javascript">
function getPaymentEventData(id, merchantRefCode)
{
	var hideId = "hide"+ id;
	
	var validateUrl = '<c:url value="/admin/getpaymenteventdata"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> 	</c:param> </c:url>';
	if(showAndHideRowNum != id)
	{
		hideRow("hide"+showAndHideRowNum);
		showAndHideRowNum = id;
	$.ajax({
		url : validateUrl,
		type : "POST",
		data : {
			merchantRefCode : merchantRefCode
		},
		success: function(response, xhr)
		{
			if(isInvalidCSRFToken(xhr))	    				
				return;
			$('#hideId').attr('style', 'display:block');
			$("#excelDataTable"+id).empty();
			buildHtmlTable(response, id);
			showAndHideRow(hideId);
   			},
  			error: function(e)
  			{
   				alert("Failed to Fetch Data");
   			}
	});
	}
	else
	{
		hideRow(hideId);
		showAndHideRowNum = 0;
	}
}

function buildHtmlTable(response, id) 
{
	var newresponse = JSON.parse(response); 
	var myList = newresponse;

	 //var myList = response;
    if(myList != '')
    {
    	//alert("MyList :"+myList);
	 var columns = addAllColumnHeaders(myList, id);
    //alert(columns);
        for (var i = 0 ; i < myList.length ; i++) {
            var row$ = $('<tr/>');
            for (var colIndex = 0 ; colIndex < columns.length ; colIndex++) {
                var cellValue = myList[i][columns[colIndex]];
    
                if (cellValue == null) { cellValue = ""; }
    
                row$.append($('<td/>').html(cellValue));
            }
            $("#excelDataTable"+id).append(row$);
        }
    }
    else
    {
    	//alert(response);
    	var row$ = $('<tr/>');
    	var message = '<h4 class="alert alert-info">Currently there are no payment event to display </h4>';
    	
    	row$.append($('<td/>').html(message));
    	$("#excelDataTable"+id).append(row$);
    }
}

function showAndHideRow(id) 
{
	
	var hideId = id;
	/* var showHideBool; */
	if( document.getElementById(hideId).style.display=='none')
	{
		document.getElementById(hideId).style.display = '';
	}
	else
	{
		document.getElementById(hideId).style.display = 'none';
	}
}
function hideRow(id)
{
	//alert($('#id').size()+"      "+id);
	if(id != 'hide0')
	{
		document.getElementById(id).style.display = 'none';
	}
}


</script>
<script type="text/javascript">
function addAllColumnHeaders(myList, id)
{
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0 ; i < myList.length ; i++) {
        var rowHash = myList[i];
        for (var key in rowHash) {
            /* alert(key); */
            if ($.inArray(key, columnSet) == -1){
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $("#excelDataTable"+id).append(headerTr$);

    return columnSet;
}
</script>

<script type="text/javascript">
//for download handler
$('#downloadPaymentTransactions').click(function(){
	$('#frmpaymentmonitorfilter').attr('action', "${pageContext.request.contextPath}"+'/admin/downloadPaymentTransactions');
	/* document.getElementByName("frmpaymentmonitorfilter").submit(); */
	 $('#frmpaymentmonitorfilter').submit();
	$('#frmpaymentmonitorfilter').attr('action', 'paymentMonitorReport');
});
	
$('.info').tooltip();

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}
</script>



  