<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.update.inidividualMarketProfile"/></li>
        </ul><!--page-breadcrumb ends-->
       <%--  <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
       <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
    </div>
    
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="individualProfile"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">  
                    <h4 class="pull-left"><spring:message  code="pgheader.update.inidividualMarketProfile"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/planmgmt/issuer/account/profile/market/individual/edit"/>"><spring:message  code="label.edit"/></a>
                </div>
				
		
				<form class="form-horizontal" id="frmIssuerAcctIndvdlMarktProfInfo" name="frmIssuerAcctIndvdlMarktProfInfo" action="" method="POST">
				
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="txt-right span5">
								<spring:message  code="label.customerServicePhone"/>
							</td>
							<td>
								<c:set var="custServPhone" value="${issuerObj.indvCustServicePhone}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong>  
								&nbsp; &nbsp;
								<spring:message  code="label.phone.ext"/>&nbsp;
								<strong>${issuerObj.indvCustServicePhoneExt}</strong>
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.customerServiceTollFreeNumber"/>
							</td>
							<td>
								<c:set var="custTollFree" value="${issuerObj.indvCustServiceTollFree}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong>  
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.customerServiceTTY"/>
							</td>
							<td>
								<c:set var="custServTTY" value="${issuerObj.indvCustServiceTTY}"/>
								<strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong>  
							</td>
						</tr>
						<tr>
							<td class="txt-right">
								<spring:message  code="label.consumerFacingWebSiteURL"/>
							</td>
							<td>
								<a href="${issuerObj.indvSiteUrl}" target="_blank"><strong>${issuerObj.indvSiteUrl}</strong></a>
							</td>
						</tr>
					</tbody>
				</table>				
				
				<br>
				
                <!--      <input type="hidden" id="market" name="market" value="individual"/>
                    <div class="control-group">
						<label class="control-label" for="cust-serv-phone"><spring:message  code="label.customerServicePhone"/></label>
						<c:set var="custServPhone" value="${issuerObj.indvCustServicePhone}"/>                                        
						<div class="controls span4 paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServPhone"))%></strong></div>
                        <label class="control-label span2 marginR10" for="cust-ext"><spring:message  code="label.phone.ext"/></label>
						<div class="controls paddingT5"><strong>${issuerObj.indvCustServicePhoneExt}</strong></div>
					</div><!-- end of control-group-->
                 <!--    
                    <div class="control-group">
						<label class="control-label" for="cust-toll-free"><spring:message  code="label.customerServiceTollFreeNumber"/></label>
						<c:set var="custTollFree" value="${issuerObj.indvCustServiceTollFree}"/>
						<div class="controls paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custTollFree"))%></strong></div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for="cust-serv-tty"><spring:message  code="label.customerServiceTTY"/></label>
						<c:set var="custServTTY" value="${issuerObj.indvCustServiceTTY}"/>
						<div class="controls paddingT5"><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("custServTTY"))%></strong></div>
					</div>
                    
                    <div class="control-group">
						<label class="control-label" for="cust-website-url"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls paddingT5"><strong>${issuerObj.indvSiteUrl}</strong></div>
					</div>
					-->
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
		</div>
<script type="text/javascript">
	$(document).ready(function(){
		$($.find('a[title="Account"]')).parent().addClass('active');	
	});
</script>

