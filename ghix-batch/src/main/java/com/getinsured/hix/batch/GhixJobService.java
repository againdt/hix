package com.getinsured.hix.batch;

import org.springframework.batch.admin.service.SearchableJobExecutionDao;
import org.springframework.batch.admin.service.SearchableJobInstanceDao;
import org.springframework.batch.admin.service.SearchableStepExecutionDao;
import org.springframework.batch.admin.service.SimpleJobService;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.configuration.ListableJobLocator;
import org.springframework.batch.core.launch.JobExecutionNotRunningException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.JobOperator;
import org.springframework.batch.core.launch.NoSuchJobExecutionException;
import org.springframework.batch.core.launch.support.SimpleJobOperator;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.dao.ExecutionContextDao;

/**
 * Created by song_s on 7/25/17.
 */
public class GhixJobService extends SimpleJobService {
    JobOperator jobOperator;
    public GhixJobService(SearchableJobInstanceDao jobInstanceDao, SearchableJobExecutionDao jobExecutionDao,
                          SearchableStepExecutionDao stepExecutionDao, JobRepository jobRepository, JobLauncher jobLauncher,
                          ListableJobLocator jobLocator, ExecutionContextDao executionContextDao, JobOperator jobOperator) {
        super(jobInstanceDao, jobExecutionDao, stepExecutionDao, jobRepository, jobLauncher, jobLocator, executionContextDao);
        this.jobOperator = jobOperator;
    }

    public JobOperator getJobOperator() {
        return jobOperator;
    }

    @Override
    public JobExecution stop(Long jobExecutionId) throws NoSuchJobExecutionException, JobExecutionNotRunningException {
        JobExecution jobExecution = super.stop(jobExecutionId);
        try {
            jobOperator.stop(jobExecutionId);
        }
        finally {
            return jobExecution;
        }
    }
}
