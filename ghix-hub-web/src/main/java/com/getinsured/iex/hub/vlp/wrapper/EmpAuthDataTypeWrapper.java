package com.getinsured.iex.hub.vlp.wrapper;

import java.text.ParseException;

import javax.xml.datatype.DatatypeConfigurationException;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.EmpAuthDataType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Wrapper Class for EmpAuthDataType
 * 
 * @author Vishaka Tharani
 * @since  18-Feb-2014 
 * 
 */
public class EmpAuthDataTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmpAuthDataTypeWrapper.class);
	
	private ObjectFactory factory;
	private EmpAuthDataType empAuthDataType;
	
	private String startDate;
	private String expireDate;
	private String provOfLawCode;
	private String officeCode;
	
	private static final String START_DATE_KEY = "startDate";
	private static final String EXPIRE_DATE_KEY = "expireDate";
	private static final String PROV_OF_LAW_KEY = "provOfLawCode";
	private static final String OFFICE_CODE_KEY = "officeCode";

	private static final String DATE_FORMAT = "YYYY-MM-DD";
	public EmpAuthDataTypeWrapper(EmpAuthDataType empAuthDataType){
		this.factory = new ObjectFactory();
		this.empAuthDataType = empAuthDataType;
		
		try {
			startDate = PlatformServiceUtil.xmlDateToString(empAuthDataType.getStartDate(), DATE_FORMAT);
			expireDate = PlatformServiceUtil.xmlDateToString(empAuthDataType.getExpireDate(), DATE_FORMAT);
			provOfLawCode = empAuthDataType.getProvOfLawCodeText();
			officeCode = empAuthDataType.getOfficeCodeText();
		}
		catch (ParseException e) {
			LOGGER.error(e.getMessage(),e);
		} catch (DatatypeConfigurationException e) {

			LOGGER.error(e.getMessage(),e);
		}  
	}
	
	public EmpAuthDataTypeWrapper(){
		this.factory = new ObjectFactory();
		this.empAuthDataType = factory.createEmpAuthDataType();
	}
	
	public ObjectFactory getFactory() {
		return factory;
	}
	public void setFactory(ObjectFactory factory) {
		this.factory = factory;
	}
	public EmpAuthDataType getEmpAuthDataType() {
		return empAuthDataType;
	}
	public void setEmpAuthDataType(EmpAuthDataType empAuthDataType) {
		this.empAuthDataType = empAuthDataType;
	}
	
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) throws HubServiceException {

		if(startDate == null){
			throw new HubServiceException("No startDate provided");
		}
		this.startDate = startDate;
		try {
			this.empAuthDataType.setStartDate(PlatformServiceUtil.stringToXMLDate(startDate, DATE_FORMAT));
		} catch (ParseException e) {

			LOGGER.error(e.getMessage(),e);
		} catch (DatatypeConfigurationException e) {

			LOGGER.error(e.getMessage(),e);
		}
	}

	public String getExpireDate() {
		return expireDate;
	}
	

	public void setExpireDate(String expireDate) throws HubServiceException {

		if(expireDate == null){
			throw new HubServiceException("No expireDate provided");
		}
		this.expireDate = expireDate;
		try {
			this.empAuthDataType.setExpireDate(PlatformServiceUtil.stringToXMLDate(expireDate, DATE_FORMAT));
		} catch (ParseException e) {

			LOGGER.error(e.getMessage(),e);
		} catch (DatatypeConfigurationException e) {

			LOGGER.error(e.getMessage(),e);
		}
	}
	
	
	public String getProvOfLawCode() {
		return provOfLawCode;
	}
	
	public void setProvOfLawCode(String provOfLawCode) throws HubServiceException {

		if(provOfLawCode == null){
			throw new HubServiceException("No provOfLawCode provided");
		}
		this.provOfLawCode = provOfLawCode;
		this.empAuthDataType.setProvOfLawCodeText(provOfLawCode);
	}
	
	public String getOfficeCode() {
		return officeCode;
	}
	
	public void setOfficeCode(String officeCode) throws HubServiceException {

		if(officeCode == null){
			throw new HubServiceException("No officeCode provided");
		}
		this.officeCode = officeCode;
		this.empAuthDataType.setOfficeCodeText(officeCode);
	}
	
	/**
	 * Converts the JSON String to SponsorshipDataTypeWrapper object
	 * 
	 * @param jsonString - String representation for SponsorshipDataTypeWrapper
	 * @return SponsorshipDataTypeWrapper Object
	 */
	public static EmpAuthDataTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create CertOfCitDocumentID23Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		EmpAuthDataTypeWrapper wrapper = new EmpAuthDataTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(START_DATE_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create EmpAuthDataTypeWrapper, No start date found");
		}
		try {
			wrapper.startDate = (String)tmpObj;
			wrapper.empAuthDataType.setStartDate(PlatformServiceUtil.stringToXMLDate(wrapper.startDate, DATE_FORMAT));
			
			tmpObj = obj.get(EXPIRE_DATE_KEY);
			if(tmpObj == null){
				throw new HubServiceException("Failed to create EmpAuthDataTypeWrapper, No expiry date found");
			}
			wrapper.expireDate = (String)tmpObj;
			wrapper.empAuthDataType.setExpireDate(PlatformServiceUtil.stringToXMLDate(wrapper.expireDate, DATE_FORMAT));
		} 
		catch (ParseException e) {

			LOGGER.error(e.getMessage(),e);
		} catch (DatatypeConfigurationException e) {

			LOGGER.error(e.getMessage(),e);
		}
		
		tmpObj = obj.get(PROV_OF_LAW_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create EmpAuthDataTypeWrapper, No ProvOfLaw found");
		}
		wrapper.provOfLawCode = (String)tmpObj;
		wrapper.empAuthDataType.setProvOfLawCodeText(wrapper.provOfLawCode);
		
		tmpObj = obj.get(OFFICE_CODE_KEY);
		if(tmpObj == null){
			throw new HubServiceException("Failed to create EmpAuthDataTypeWrapper, No Office code found");
		}
		wrapper.officeCode = (String)tmpObj;
		wrapper.empAuthDataType.setOfficeCodeText(wrapper.officeCode);
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(START_DATE_KEY,this.startDate);
		obj.put(EXPIRE_DATE_KEY,this.expireDate);
		obj.put(PROV_OF_LAW_KEY,this.provOfLawCode);
		obj.put(OFFICE_CODE_KEY,this.officeCode);
		return obj.toJSONString();
	}
	
}
