package com.getinsured.iex.hub.apc.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
/**
 * Date validator for date fields
 *
 * @author Vishaka Tharani
 * @since 11-Feb-2014
 *
 */
public class IncomeDateValidator extends Validator {

	private String dateFormat;

	/** Validates a field using following rules
	 *  1. isRequired - If "Y", the value is mandatory
	 *  2. dateFormat - To match a specific format of date
	 *
	 *  @param name - Name of the field
	 *  @param value - Value for the parameter
	 *
	 *  @throws InputDataValidationException - If a validation fails
	 *
	 */
	public void addContextParameter(String name, Object value){
		super.addContextParameter(name,value);
		if(name.equalsIgnoreCase("dateFormat")){
			this.dateFormat = (String)value;
		}
	}

	public void validate(String name, Object value)
			throws InputDataValidationException {
		if(this.checkForNullOrEmpty(name, value)){
			return;
		}
		if(!(value instanceof String)){
			throw new InputDataValidationException("Incompatible value for date validation of \""+name+"\"");
		}
		if(this.dateFormat == null){
			throw new InputDataValidationException("Can not validate the date without the format");
		}
		SimpleDateFormat sdf = new SimpleDateFormat(this.dateFormat);
		try{
			sdf.parse((String)value);
		}catch(ParseException pe){
				throw new InputDataValidationException("failed to validate the parameter \""+name+" invalid format ["+this.dateFormat+"] provided",pe);
		}
		
		int year = Integer.parseInt((String) value);
		int currentYear = Calendar.getInstance().get(Calendar.YEAR);
		int nextYear = currentYear + 1;
		if(year != currentYear && year != nextYear){
			throw new InputDataValidationException("Invalid " + name + " provided. Allowed values : [" + currentYear + " , " + nextYear + "]");
		}
		
	}
}
