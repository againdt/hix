/**
 *
 */
package com.getinsured.iex.util;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TimeZone;
import java.util.UUID;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPMessage;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.BeanUtils;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;

import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ApplicationExtensionType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityReasonCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author chopra_s
 *
 */
public final class ReferralUtil {
	private static final String ERROR_WHILE_UNMARSHALLING_REQUEST = "Error occurred while unmarshalling request string - ";

	private static final Logger LOGGER = Logger.getLogger(ReferralUtil.class);

	private ReferralUtil() {
	}

	/**
	 * Returns whether the string argument has value
	 *
	 * @param s
	 * @return
	 */
	public static boolean isValidString(String s) {
		return !(s == null || s.trim().equals(""));
	}

	public static boolean isTrue(Boolean b) {
		return b != null ? b : false;
	}

	/**
	 * Returns a Trimmed string
	 *
	 * @param s
	 * @return
	 */
	public static String validString(String s) {
		return s.trim();
	}

	public static String convertToValidString(Object s) {
		return (s == null || s.toString().equals(ReferralConstants.NULL_STRING)) ? null : s.toString();
	}

	public static String convertToString(Object s) {
		return (s != null) ? String.valueOf(s) : null;
	}

	public static boolean isValidInt(Integer o) {
		return !(o == null || o.intValue() == 0);
	}

	public static Integer convertToInteger(Object s) {
		return (s != null) ? Integer.parseInt(s + "") : null;
	}

	public static int convertToInt(Object s) {
		return (s != null) ? Integer.parseInt(s + "") : 0;
	}

	public static boolean convertToBoolean(Object s) {
		return (s != null) && Boolean.parseBoolean(s + "");
	}

	public static long convertToLong(Object s) {
		return (s != null) ? Long.parseLong(s + "") : 0;
	}

	public static long convertToValidLong(Integer i) {
		return (i != null) ? i.longValue() : 0;
	}

	public static int convertToInt(BigDecimal o) {
		return (o != null) ? o.intValue() : 0;
	}

	public static Float convertToFloat(BigDecimal o) {
		return (o != null) ? o.floatValue() : null;
	}

	/**
	 * Returns the exception stack trace in string format
	 *
	 * @param e
	 * @return
	 */
	public static String getStackTrace(Exception e) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		return stringWriter.toString();
	}

	public static int listSize(List<?> data) {
		return data != null ? data.size() : 0;
	}

	public static int sizeForJSONArray(JSONArray data) {
		return data != null ? data.length() : 0;
	}

	public static int collectionSize(Collection<?> data) {
		return data != null ? data.size() : 0;
	}

	public static int mapSize(Map<?, ?> data) {
		return data != null ? data.size() : 0;
	}

	public static String getValue(TextType data) {
		return data != null ? data.getValue() : "";
	}

	public static String getValue(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String data) {
		return data != null ? data.getValue() : "";
	}

	public static Date extractDate(DateType inputDate) {
		if (inputDate == null || inputDate.getDate() == null || inputDate.getDate().getValue() == null) {
			return null;
		}
		Calendar cal =  GregorianCalendar.getInstance();
		cal.set(inputDate.getDate().getValue().getYear(), (inputDate.getDate().getValue().getMonth()-1), inputDate.getDate().getValue().getDay());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date extractDate(com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date inputDate) {
		if (inputDate == null || inputDate.getValue() == null) {
			return null;
		}
		Calendar cal =  GregorianCalendar.getInstance();
		cal.set(inputDate.getValue().getYear(), (inputDate.getValue().getMonth()-1), inputDate.getValue().getDay());
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	public static Date extractDate(XMLGregorianCalendar input) {
		return input.toGregorianCalendar().getTime();
	}

	public static <T extends Enum<T>> List<T> enumValues(Class<T> enumType, List<TextType> values) {
		List<T> data = new ArrayList<T>();
		final int size = listSize(values);

		for (int i = 0; i < size; i++) {
			for (T c : enumType.getEnumConstants()) {
				if (c.name().equals(values.get(i).getValue())) {
					data.add(c);
					break;
				}
			}
		}
		return data;
	}

	public static <T extends Enum<T>> List<String> enumValuesAsString(Class<T> enumType, List<TextType> values) {
		List<String> data = new ArrayList<String>();
		final int size = listSize(values);

		for (int i = 0; i < size; i++) {
			for (T c : enumType.getEnumConstants()) {
				if (c.name().equalsIgnoreCase(values.get(i).getValue())) {
					data.add(c.name());
					break;
				}
			}
		}
		return data;
	}

	public static <T extends Enum<T>> T enumValue(Class<T> enumType, String value) {
		T data = null;

		for (T c : enumType.getEnumConstants()) {
			if (c.name().equals(value)) {
				data = c;
				break;
			}
		}
		return data;
	}

	public static <T extends Enum<T>> String enumValueAsString(Class<T> enumType, String value) {
		String data = null;

		for (T c : enumType.getEnumConstants()) {
			if (c.name().equals(value)) {
				data = c.name();
				break;
			}
		}
		return data;
	}

	public static boolean isNotNullAndEmpty(final String strParam) {
		return ((strParam != null) && !strParam.equals(""));
	}

	public static String checkAndValidString(final String strParam) {
		return isNotNullAndEmpty(strParam) ? strParam : "";
	}

	public static boolean compareInteger(final Integer iParam1, final Integer iParam2) {
		if (iParam1 == null && iParam2 == null) {
			return true;
		} else if (iParam1 != null && iParam2 == null) {
			return false;
		} else if (iParam1 == null && iParam2 != null) {
			return false;
		} else {
			return iParam1.compareTo(iParam2) == 0;
		}
	}

	public static boolean compareString(final String sParam1, final String sParam2) {
		return checkAndValidString(sParam1).equalsIgnoreCase(checkAndValidString(sParam2));
	}

	public static boolean compareEnum(final Enum<?> sParam1, final Enum<?> sParam2) {
		if (sParam1 == null && sParam2 == null) {
			return true;
		} else if (sParam1 != null && sParam2 == null) {
			return false;
		} else if (sParam1 == null && sParam2 != null) {
			return false;
		} else {
			return sParam1.name().equals(sParam2.name());
		}
	}

	public static boolean compareAddressData(Address address1, Address address2) {
		return compareString(address1.getStreetAddress1(), address2.getStreetAddress1()) && compareString(address1.getStreetAddress2(), address2.getStreetAddress2()) && compareString(address1.getPostalCode(), address2.getPostalCode())
		        && compareString(address1.getCountyCode(), address2.getCountyCode()) && compareString(address1.getCounty(), address2.getCounty()) && compareString(address1.getCity(), address2.getCity())
		        && compareString(address1.getState(), address2.getState());
	}

	public static boolean compareAddress(Address address1, Address address2) {
		if (address1 == null && address2 == null) {
			return false;
		}

		if (address1 == address2) {
			return true;
		}

		if (address1 == null && address2 != null) {
			return false;
		}

		if (address1 != null && address2 == null) {
			return false;
		}

		return compareAddressData(address1, address2);
	}

	public static HouseholdMember primaryMember(final SingleStreamlinedApplication singleStreamlinedApplication) {
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();

		final int size = ReferralUtil.listSize(householdMemberList);
		HouseholdMember primaryMember = null;
		for (int i = 0; i < size; i++) {
			if (householdMemberList.get(i).getPersonId().compareTo(ReferralConstants.PRIMARY) == 0) {
				primaryMember = householdMemberList.get(i);
				break;
			}
		}

		return primaryMember;

	}

	public static HouseholdMember member(final SingleStreamlinedApplication singleStreamlinedApplication, final int personId) {
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();

		final int size = ReferralUtil.listSize(householdMemberList);
		HouseholdMember member = null;
		for (int i = 0; i < size; i++) {
			if (householdMemberList.get(i).getPersonId().compareTo(personId) == 0) {
				member = householdMemberList.get(i);
				break;
			}
		}
		return member;

	}

	public static List<String> fetchPhonenumbers(HouseholdMember primaryMember) {
		List<String> numbers = new ArrayList<String>();
		if (primaryMember.getHouseholdContact().getPhone() != null && isNotNullAndEmpty(primaryMember.getHouseholdContact().getPhone().getPhoneNumber())) {
			numbers.add(primaryMember.getHouseholdContact().getPhone().getPhoneNumber());
		}

		if (primaryMember.getHouseholdContact().getOtherPhone() != null && isNotNullAndEmpty(primaryMember.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			numbers.add(primaryMember.getHouseholdContact().getOtherPhone().getPhoneNumber());
		}

		return numbers;
	}

	public static String fetchPhonenumber(HouseholdMember member) {
		String number = null;
		if (member.getHouseholdContact().getPhone() != null && isNotNullAndEmpty(member.getHouseholdContact().getPhone().getPhoneNumber())) {
			number = member.getHouseholdContact().getPhone().getPhoneNumber();
		}
		if (number == null) {
			if (member.getHouseholdContact().getOtherPhone() != null && isNotNullAndEmpty(member.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
				number = member.getHouseholdContact().getOtherPhone().getPhoneNumber();
			}
		}
		return number;
	}

	public static String converToYN(Boolean bln) {
		return (bln != null && bln) ? "Y" : "N";
	}

	public static String converToYesNo(Boolean bln) {
		return (bln != null && bln) ? "Yes" : "No";
	}

	private static JAXBContext jAXBContext = null;
	static {
		try {
			jAXBContext = JAXBContext.newInstance(AccountTransferRequestPayloadType.class);
		} catch (JAXBException je) {
			LOGGER.error(ERROR_WHILE_UNMARSHALLING_REQUEST, je);
		}

	}

	public static AccountTransferRequestPayloadType unmarshal(String request) throws Exception {
		AccountTransferRequestPayloadType payload = null;
		InputStream is = null;
		try {

			/*reader = new StringReader(request);
			payload = (AccountTransferRequestPayloadType) unmarshaller.unmarshal(reader); */
			is = IOUtils.toInputStream(request, "UTF-8");
			payload = (AccountTransferRequestPayloadType) GhixUtils.inputStreamToObject(is, AccountTransferRequestPayloadType.class);
		} catch (Exception e) {
			LOGGER.error(ERROR_WHILE_UNMARSHALLING_REQUEST, e);
			throw e;
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (Exception e2) {
					LOGGER.error(e2.getMessage());
				}
			}

		}

		return payload;
	}

	@SuppressWarnings("unchecked")
	public static String handleExceptionForJson(Throwable exception) {
		if (null == exception) {
			return "Unknown";
		}
		JSONObject exceptionJSONObj = new JSONObject();
		exceptionJSONObj.put("exception", exception.getClass().getName());
		exceptionJSONObj.put("message", exception.getMessage());
		exceptionJSONObj.put("cause", handleExceptionForJson(exception.getCause()));
		return exceptionJSONObj.toJSONString();
	}

	public static boolean isFullDetermination(AccountTransferRequestPayloadType request, String stateCode) {
		boolean isFullDetermination = false;

		if ("NM".equals(stateCode)) {
			isFullDetermination = isFullDeterminationNM(request);
		} else if ("ID".equals(stateCode)) {
			isFullDetermination = isFullDeterminationID(request);
		}

		return isFullDetermination;
	}

	private static boolean isFullDeterminationNM(AccountTransferRequestPayloadType request) {
		List<InsuranceApplicantType> insuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();

		boolean isFullDetermination = false;
		for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypeList) {

			ReferralActivityReasonCodeSimpleType referralActivityReasonCode = insuranceApplicantType.getReferralActivity() != null ? insuranceApplicantType.getReferralActivity().getReferralActivityReasonCode() != null ? insuranceApplicantType
			        .getReferralActivity().getReferralActivityReasonCode().getValue() : null : null;

			if (referralActivityReasonCode == ReferralActivityReasonCodeSimpleType.FULL_DETERMINATION) {
				isFullDetermination = true;
				break;
			}

		}
		return isFullDetermination;
	}

	private static boolean isFullDeterminationID(AccountTransferRequestPayloadType request) {

		List<InsuranceApplicantType> insuranceApplicantTypeList = request.getInsuranceApplication().getInsuranceApplicant();

		boolean isFinal = false;

		for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypeList) {
			ReferralActivityStatusCodeSimpleType value = insuranceApplicantType.getReferralActivity() != null ? insuranceApplicantType.getReferralActivity().getReferralActivityStatus() != null ? insuranceApplicantType.getReferralActivity()
			        .getReferralActivityStatus().getReferralActivityStatusCode() != null ? insuranceApplicantType.getReferralActivity().getReferralActivityStatus().getReferralActivityStatusCode().getValue() : null : null : null;

			if (ReferralActivityStatusCodeSimpleType.UPDATED.equals(value)) {
				isFinal = true;
				break;
			}
		}

		return isFinal;
	}

public static String extractSoapBoady(String requestPayload) {
		
		/*
		 *  The following code causes failures when the client passes the xml encoding line in the request, which is standard for many clients like IIB 
		 *  <?xml version="1.0" encoding="utf-8"?> 
		final String soapNameSpace = requestPayload.substring(requestPayload.indexOf("<") + 1, requestPayload.indexOf(":"));
		return StringUtils.substringBetween(requestPayload, "<" + soapNameSpace + ":Body>", "</" + soapNameSpace + ":Body>");
		*
		*/
		// Extract Name space String and extract the soap body from it

    	final String soapEnvelopeSubString = requestPayload.substring(requestPayload.indexOf("<") + 1, requestPayload.indexOf(":Envelope"));
		final String soapEnvelopeString = soapEnvelopeSubString.substring(soapEnvelopeSubString.lastIndexOf("<") + 1, soapEnvelopeSubString.length());
		return StringUtils.substringBetween(requestPayload, "<" + soapEnvelopeString + ":Body>", "</" + soapEnvelopeString + ":Body>");
	}


	private static MessageFormat SSN_FORMAT = new MessageFormat("{0}-{1}-{2}");

	public static String formatSsn(String socialSecurityNumber) {
		if (socialSecurityNumber == null || "".equals(socialSecurityNumber)) {
			return null;
		}
		if (socialSecurityNumber.contains("-")) {
			return socialSecurityNumber;
		} else {
			String st = null;
			final int len = socialSecurityNumber.length();
			if (len == 9) {
				st = SSN_FORMAT.format(new String[] { socialSecurityNumber.substring(0, 3), socialSecurityNumber.substring(3, 5), socialSecurityNumber.substring(5) });
			}
			return st;
		}

	}

	public static SsapApplicant retreiveApplicant(SsapApplication ssapApplication, int personId) {
		SsapApplicant returnSsapApplicant = null;
		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			if (ssapApplicant.getPersonId() == personId) {
				returnSsapApplicant = ssapApplicant;
				break;
			}
		}
		return returnSsapApplicant;
	}

	public static SsapApplicant retreiveApplicantbasedonGuid(SsapApplication ssapApplication, String applicantGuid) {
		SsapApplicant returnSsapApplicant = null;
		for (SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()) {
			if (ssapApplicant.getApplicantGuid().equalsIgnoreCase(applicantGuid)) {
				returnSsapApplicant = ssapApplicant;
				break;
			}
		}
		return returnSsapApplicant;
	}

	public static String formatDate(final Date date, final String strFormat) {
		String strReturnDate = null;
		DateFormat objFormat = null;

		objFormat = new SimpleDateFormat(strFormat);
		strReturnDate = objFormat.format(date);
		objFormat = null;

		return strReturnDate;
	}

	public static int compareDate(final Date objDate1, final Date objDate2) {
		int iReturn = -1;
		if ((objDate2.compareTo(objDate1)) < 0) {
			iReturn = -1;
		} else if ((objDate2.compareTo(objDate1)) == 0) {
			iReturn = 0;
		} else if ((objDate2.compareTo(objDate1)) > 0) {
			iReturn = 1;
		}

		return iReturn;
	}

	public static Date convertStringToDate(final String strDate, String strCurrentFormat) {
		Date objDate = null;
		final DateFormat objFormat = new SimpleDateFormat(strCurrentFormat);
		final ParsePosition pos = new ParsePosition(0);
		objDate = objFormat.parse(strDate, pos);
		return objDate;
	}

	public static Date convertStringToDate(final String strDate) {
		return convertStringToDate(strDate, ReferralConstants.DEFDATEPATTERN);
	}

	public static Date currentDate() {
		return TSCalendar.getInstance(TimeZone.getDefault()).getTime();
	}

	public static String formatMessage(final String strMessage, final Object[] objParam) {
		return MessageFormat.format(strMessage, objParam);
	}

	public static long dayDifference(final Date objDate1, final Date objDate2) {
		long lDifference = 0;
		long lDate1 = objDate1.getTime();
		long lDate2 = objDate2.getTime();
		lDifference = (lDate1 - lDate2) / ReferralConstants.MILLISECS_IN_DAY;
		return lDifference;
	}

	public static String getPhoneNumberWithoutFormat(String phone) {
		StringBuilder phoneClean = null;

		if(phone != null) {
			phoneClean = new StringBuilder();

			for (int i = 0; i < phone.length(); i++) {
				if (StringUtils.isNumeric(String.valueOf(phone.charAt(i)))) {
					phoneClean.append(phone.charAt(i));
				}
			}

			return phoneClean.toString();
		}

		return phone;
	}

	public static void copyProperties(Object source, Object destination) {
		BeanUtils.copyProperties(source, destination);
	}

	public static String removeExtraSpace(String source) {
		StringBuilder target = new StringBuilder();
		source = source.trim();
		final int srcLength = source.length();
		boolean blnCheck = false;
		for (int i = 0; i < srcLength; i++) {
			if (source.charAt(i) == ' ') {
				if (blnCheck) {
					continue;
				}
				target.append(source.charAt(i));
				blnCheck = true;
			} else {
				blnCheck = false;
				target.append(source.charAt(i));
			}
		}
		return target.toString();
	}

	public static boolean applyingForCoverage(InsuranceApplicantType insuranceApplicant) {
		String eligibilityIndicatorType = null;
		boolean isApplying = false;
		final List<EligibilityType> eligibilityTypeList = insuranceApplicant.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
		if (eligibilityTypeList != null) {
			for (EligibilityType eligibilityType : eligibilityTypeList) {
				eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), ReferralConstants.EE_PACKAGE_NAME);
				if (eligibilityIndicatorType.equalsIgnoreCase(ReferralConstants.APTC_ELIGIBILITY_TYPE)) {
					isApplying = eligibilityType.getEligibilityReasonText() != null && eligibilityType.getEligibilityReasonText().getValue() != null && !eligibilityType.getEligibilityReasonText().getValue().equals(ReferralConstants.NOTAPPLYING_CODE);
					if (!isApplying) {
						break;
					}
				}
			}
		}
		return isApplying;
	}

	public static int arraySize(Object[] array) {
		int returnVal = 0;
		if (array != null) {
			returnVal = array.length;
		}
		return returnVal;
	}

	public static boolean compareBoolean(Boolean source, Boolean target) {
		if (source == null && target == null) {
			return true;
		} else if (target == null || source == null) {
			return false;
		}
		return source.equals(target);
	}

	public static long extractCoverageYear(ApplicationExtensionType applicationExtension) {
		return applicationExtension.getCoverageYear().getValue().getYear();
	}

	public static Set<Integer> fetchCoverageYears(String atCoverageYears) {
		Set<Integer> coverageYears = new HashSet<Integer>();
		StringTokenizer stringTokenizer = new StringTokenizer(atCoverageYears, ",");

		while (stringTokenizer.hasMoreTokens()) {
			coverageYears.add(Integer.parseInt(stringTokenizer.nextToken()));
		}
		return coverageYears;
	}

	public static Timestamp getEnrollmentStartDateForFutureEvent(Date eventStartDate) {
		DateTime currentDate = new DateTime(new TSDate());
		if(isFutureDate(eventStartDate))
			return new Timestamp(currentDate.toDate().getTime());
		else
		return new Timestamp(eventStartDate.getTime());
	}
	
	public static boolean isFutureDate(Date source) {
		DateTime eventDate = new DateTime(source.getTime());
		DateTime currentDate = new DateTime(new TSDate());

		return eventDate.isAfter(currentDate);
	}

	public static Message<String> buildMessageWithHeader(String response, String headerKey, String headerValue, Message<String> message) {
		return MessageBuilder.withPayload(response).copyHeadersIfAbsent(message.getHeaders()).setHeaderIfAbsent(headerKey, headerValue).build();
	}

	public static boolean isCoverageDatePastEnrollmentDate(Date coverageStartDate, Date enrollmentEndDate) {
		DateTime coverageDate = new DateTime(coverageStartDate.getTime());
		DateTime enrollmentDate = new DateTime(enrollmentEndDate.getTime());

		return coverageDate.isAfter(enrollmentDate);
	}

	public static String capitalizeFirstLetter(String source) {
		return WordUtils.capitalizeFully(source);
	}
	
	public static String lowercase(String source) {
		if (StringUtils.isEmpty(source)) {
            return source;
        }
		source = source.toLowerCase();
		return source;
	}

	public static String fullName(String firstName, String MiddleName, String LastName) {
		String fullName;
		fullName = firstName + " " + (MiddleName == null ? LastName : MiddleName + " " + LastName);
		return fullName;
	}
	
	public static AccountTransferRequestPayloadType getAccountTransferRequestObject(String soapRequest)
	{
		AccountTransferRequestPayloadType accountTransferRequest = null;
		try {
			SOAPMessage messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL).createMessage(new MimeHeaders(), new ByteArrayInputStream(soapRequest.getBytes()));
			Unmarshaller unmarshaller = JAXBContext.newInstance(AccountTransferRequestPayloadType.class).createUnmarshaller();
			accountTransferRequest = (AccountTransferRequestPayloadType)unmarshaller.unmarshal(messageFactory.getSOAPBody().extractContentAsDocument());
		} catch (Exception e) {
			LOGGER.error(ERROR_WHILE_UNMARSHALLING_REQUEST, e);
		}
		return accountTransferRequest; 
	}
}
