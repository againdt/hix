var app = angular.module('planLoadStatusListApp', ['ui.grid', 'ui.bootstrap', 'ui.grid.resizeColumns', 'ui.grid.pagination']);

app.controller('PlanLoadStatusListCtrl', ['$scope', 'PlanLoadStatusListService', 'uiGridConstants', function ($scope, PlanLoadStatusListService, uiGridConstants) {

	$(function(){$("#dateFilter").datepicker().attr("readOnly", "false");});

//	debugger;
	var paginationOptions = {
		pageNumber: 1,
		pageSize: 10,
		sort: null
	};

	$scope.statusList = null;
	$scope.setStatusList = function(dataList) {
		$scope.statusList = dataList;
	};

	$scope.issuerList = null;
	$scope.setIssuerList = function(dataList) {
		$scope.issuerList = dataList;
	};

	$scope.resetFilter = function(logsData) {
		$("#issuerFilter").val("");
		$("#selectedIssuer").val("");
		$("#statusFilter").val("");
		$("#selectedStatus").val("");
		$("#dateFilter").val("");
		$("#selectedDate").val("");
		$("#containsOfRequestArea").html("");
		$("#containsOfResponseArea").html("");
	};

	$scope.downloadTemplate = function(data) {
		PlanLoadStatusListService.downloadSerffTemplate(data);
	};

	$scope.attachmentsColDef = [];
	$scope.viewAttachmentsInPopup = function(selectedRecordID) {

		$("#attachmentsModal").modal();

		PlanLoadStatusListService.getAttachmentsGrid(selectedRecordID).success(function(data) {
//			debugger;
			$scope.gridOptionsForAttachments.data = data;

			$scope.attachmentsColDef.push({
				name: 'documentName',
				displayName: 'Document Name',
				cellClass: 'grid-left-align',
				width: 330
			});

			$scope.attachmentsColDef.push({
				name: 'documentSize',
				displayName: 'Size',
				width: 80
			});

			$scope.attachmentsColDef.push({
				name: 'documentId',
				displayName: 'Download',
				cellTemplate: '<button ng-show="row.entity.downloadable" ng-value="row.entity.documentId" ng-click="grid.appScope.downloadTemplate(row.entity.documentId)">download</button>',
				// cellTemplate: '<button value="{{COL_FIELD}}" onclick="var colValue = this.value; var scope = angular.element(document.getElementById(\'planLoadStatusListDiv\')).scope(); scope.$apply(function () {scope.downloadTemplate(colValue);});">download</button>',
				width: 100
			});
		});
	};

	$scope.gridOptionsForAttachments = {
		columnDefs:  $scope.attachmentsColDef,
		enableColumnMenus: false
	};

	$scope.colDef = [];

	var displayName = {serffReqID: 'Record ID', hiosIssuerID: 'Issuer HIOS ID', planNumber: 'Plan ID', serffTrackNumber: 'SERFF Track No', stateTrackNumber: 'State Track No', serffRequest: 'Request', serffResponse: 'Response', startDate : 'Start', endDate : 'End', status : 'Status', statusDescription : 'Remarks'};

	var setPropertiesFn = function (elem, key) {

        switch (key) {
			case 'serffReqID':
			case 'hiosIssuerID':
			case 'planNumber':
			case 'serffTrackNumber':
			case 'stateTrackNumber':
			case 'serffRequest':
			case 'serffResponse':
			case 'startDate':
			case 'endDate':
			case 'status':
			case 'statusDescription':
				elem.displayName = displayName[key];
        }
	};

	$scope.getStatusDisplayValue = function (key) {

		var displayValue = "";

		if (key == 'S') {
			displayValue = "Success";
		}
		else if (key == 'F') {
			displayValue = "Failure";
		}
		else if (key == 'P') {
			displayValue = "In Progress";
		}
		return displayValue;
	};

	$scope.addRequestDataInPopup = function(logsData) {
		$("#containsOfRequestArea").html(logsData);
		$("#requestModal").modal();
	};

	$scope.addResponseDataInPopup = function(logsData) {
		$("#containsOfResponseArea").html(logsData);
		$("#responseModal").modal();
	};

	$scope.displayNone = {
		"background-color" : "white"
	};

	PlanLoadStatusListService.getGridData(paginationOptions.pageNumber, $("#selectedIssuer").val(), $("#selectedStatus").val(), $("#selectedDate").val()).success(function(data) {

		$scope.displayNone = {
			"display" : "none"
		};

		$("#issuerFilter").val($("#selectedIssuer").val());
		$("#statusFilter").val($("#selectedStatus").val());
		$("#dateFilter").val($("#selectedDate").val());

//		debugger;
		if (data) {
			$scope.gridOptions.data = data.content;
		}
		else {
			$scope.gridOptions.data = null;
		}

		if (data && data.content) {

			angular.forEach(data.content[0], function(value, key) {

				if (key != 'status' && key != 'serffRequest' && key != 'serffResponse') {
					var elem = null;

					if (key == 'serffReqID' || key == 'hiosIssuerID' || key == 'startDate' || key == 'endDate')  {
						elem = {name:key, width: 120};
					}
					else if (key == 'description')  {
						elem = {name:key, width: 340};
					}
					else {
						// Default width
						elem = {name:key, width: 150};
					}
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
				else if (key == 'status')  {
					elem = {
						name:key,
						enableSorting: false,
						cellTemplate: '<span>{{grid.appScope.getStatusDisplayValue(COL_FIELD)}}</span>',
						width: 120
					};
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
				else if (key == 'serffRequest') {
					elem = {
						name:key,
						enableSorting: false,
						cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.addRequestDataInPopup(COL_FIELD)">request</a>',
						width: 90
					};
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
				else if (key == 'serffResponse') {
					elem = {
						name:key,
						enableSorting: false,
						cellTemplate: '<a ng-show="COL_FIELD" href="javascript:void(0);" ng-click="grid.appScope.addResponseDataInPopup(COL_FIELD)">response</a>',
						width: 90
					};
					setPropertiesFn(elem, key);
					$scope.colDef.push(elem);
				}
			});

			$scope.colDef.push({
				name: 'attachements',
				displayName: 'Attachements',
				enableSorting: false,
				cellTemplate: '<a href="javascript:void(0);" ng-click="grid.appScope.viewAttachmentsInPopup(row.entity.serffReqID)">attachements</a>',
				width: 130
			});
			$scope.gridOptions.totalItems = data.totalElements;
		}
	});

	$scope.gridOptions = {
		paginationPageSizes: [10],
		paginationPageSize: paginationOptions.pageSize,
		useExternalPagination: true,
		columnDefs:  $scope.colDef,
		enableGridMenu: false,
		enableFiltering: false,
		enableSorting: true,
//		exporterPdfFilename: 'download.pdf',

		onRegisterApi: function(gridApi) {
			$scope.gridApi = gridApi;

			gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
				paginationOptions.pageNumber = newPage;
				paginationOptions.pageSize = pageSize;

				PlanLoadStatusListService.getGridData(newPage, $("#selectedIssuer").val(), $("#selectedStatus").val(), $("#selectedDate").val()).success(function(data) {

//					debugger;
					$("#issuerFilter").val($("#selectedIssuer").val());
					$("#statusFilter").val($("#selectedStatus").val());
					$("#dateFilter").val($("#selectedDate").val());

					$scope.gridOptions.data = data.content;
					$scope.gridOptions.totalItems = data.totalElements;
				});
			});
		}
	};
}]);

app.service('PlanLoadStatusListService', ['$http', function ($http) {

	function getGridData(pageNumber, selectedIssuer, selectedStatus, selectedDate) {
//		debugger;

		if (!selectedIssuer) {
			selectedIssuer = "";
		}

		if (!selectedStatus) {
			selectedStatus = "";
		}

		if (!selectedDate) {
			selectedDate = "";
		}

		return  $http({
			method: 'GET',
			url: '/hix/admin/serff/getSERFFTransferStatusDataList?page=' + pageNumber + '&issuerFilter=' + selectedIssuer + '&statusFilter=' + selectedStatus + '&dateFilter=' + selectedDate
		});
	}

	function getAttachmentsGrid(selectedRecordID) {
//		debugger;

		if (!selectedRecordID) {
			alert("Attachment Record ID is empty");
		}
		else {
			return  $http({
				method: 'GET',
				url: '/hix/admin/serff/getSERFFPlanAttachmentsList?selectedRecordID=' + selectedRecordID
			});
		}
	}

	function downloadSerffTemplate(selectedDocumentId) {
//		debugger;

		if (!selectedDocumentId) {
			alert("Document ID is empty");
		}
		else {
			window.location.href = '/hix/admin/serff/downloadSerffTemplate?selectedDocumentId=' + selectedDocumentId;
		}
	}

	return {
		getGridData:getGridData,
		getAttachmentsGrid:getAttachmentsGrid,
		downloadSerffTemplate:downloadSerffTemplate
	};
}]);
