package com.getinsured.hix.batch.ssap.redetermination.task;

import java.io.StringWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestClientException;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.indportal.CustomGroupingUtil;
import com.getinsured.eligibility.indportal.customGrouping.CustomGroupingController.EligibilityType;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.redetermination.service.SsapCloneApplicationService;
import com.getinsured.hix.batch.repository.RenewalEligibilityProgramRepository;
import com.getinsured.hix.indportal.dto.AptcRatioRequest;
import com.getinsured.hix.indportal.dto.AptcRatioResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.config.RenewalConfiguration;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.HouseHoldContact;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Individual;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Relationship;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.ResponsiblePerson;
import com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalResponse;
import com.getinsured.hix.webservice.plandisplay.autorenewal.EnrollmentVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ObjectFactory;
import com.getinsured.hix.webservice.plandisplay.autorenewal.ProductType;
import com.getinsured.hix.webservice.plandisplay.autorenewal.RoleTypeVal;
import com.getinsured.hix.webservice.plandisplay.autorenewal.YesNoVal;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.model.RenewalApplicant;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.RenewalApplicantRepository;
import com.getinsured.iex.ssap.repository.RenewalApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;

/**
 * Spring batch Tasklet to execute Auto renewal of SSAP Applications. 
 * 
 * @author Sahay_B
 *  
 * GI_APP_CONFIG Properties used (iex.ssap.autorenewal.parentappyear, iex.ssap.autorenewal.renewalyear, global.open.enrollment.start.date
 * and global.open.enrollment.end.date)
 * 
 * This batch is expected to run once and leave financial and non financial applications, enrolled in a renewal year plan
 * This calls IND71 which determines through plan cross walk if corresponding plan is available and creates enrollment
 */
@Deprecated
@DependsOn("dynamicPropertiesUtil")
public class AutoEnroll extends StepExecutionListenerSupport implements Tasklet {
	
	private static final Logger LOGGER = Logger.getLogger(AutoEnroll.class);
	private SsapCloneApplicationService ssapCloneApplicationService;
	private ThreadPoolTaskExecutor taskExecutor;
	private GIMonitorService giMonitorService;
	private String processErrorRecords;
	private boolean processErrorRecordFlag;
	private static volatile boolean isBatchRunning = false;
	
	private SsapJsonBuilder ssapJsonBuilder;
	private RenewalApplicationRepository renewalApplicationRepository;
	private RenewalApplicantRepository renewalApplicantRepository;
	private SsapApplicationRepository ssapApplicationRepository;
	private SsapApplicantRepository ssapApplicantRepository;
	private RenewalEligibilityProgramRepository renewalEligibilityProgramRepository;
	private WebServiceTemplate ind71WebServiceTemplate;
	private GhixRestTemplate ghixRestTemplate;
	private CustomGroupingUtil customGroupingUtil;
	private LookupService lookupService;
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		synchronized (this) {
			if(isBatchRunning) {
				throw new GIException("Batch is already running");
			}
			else {
				isBatchRunning = true;
			}
		}
		
		try {	
			Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
			String batchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			batchSize = chunkContext.getStepContext().getStepExecution().getJobParameters().getString("BATCH_SIZE",batchSize);
			
			LOGGER.info("Ssap Auto Renewal Batch job to Process new cloned Application for " + renewalYear);
			Long batchSizeValue = Long.valueOf(batchSize);
			
			if(batchSizeValue == 0) {
				throw new GIException("Invalid Batch Size");
			}
			
			if(processErrorRecords != null && processErrorRecords.equalsIgnoreCase("Y")) {
				processErrorRecordFlag = true;
			}
			
			List<RenewalApplication> renewalApplicationList = null;
			List<RenewalStatus> statusList = null;
	        
			if(processErrorRecordFlag) {
				
				statusList = Arrays.asList(RenewalStatus.TO_CONSIDER,RenewalStatus.APP_ERROR);
				renewalApplicationList = ssapCloneApplicationService.getAutoRenewalApplicationsByCoverageYear(statusList,renewalYear, batchSizeValue);
				
			}else {
				statusList = Arrays.asList(RenewalStatus.TO_CONSIDER);
				renewalApplicationList = ssapCloneApplicationService.getAutoRenewalApplicationsByCoverageYear(statusList,renewalYear, batchSizeValue);
			}
			
			if(renewalApplicationList != null && renewalApplicationList.size() > 0) {
				
				Set<Future<String>> tasks = new HashSet<Future<String>>(renewalApplicationList.size());
				
				for (RenewalApplication renewalApplication : renewalApplicationList) {
					LOGGER.info("Processing Application " + renewalApplication.getSsapApplicationId() + "for renewal year " + renewalYear);
					try {

						tasks.add(taskExecutor.submit(new SsapRedeterminationProcessor(renewalApplication, processErrorRecordFlag, renewalYear)));
						
					} catch (Exception e) {
						
						logToGIMonitor(e, 50003, Long.toString(renewalApplication.getSsapApplicationId()));
					}
				}
		
				waitForAllTaskToComplete(tasks);
			}
			
		} finally {
			isBatchRunning = false;
		}	
			
		return RepeatStatus.FINISHED;

	}
	
	private Integer logToGIMonitor(Exception e, int errorCode, String caseNumber) {
		Integer giMonitorId = null;
		GIMonitor giMonitor = giMonitorService.saveOrUpdateErrorLog("RENEWALBATCH_" + errorCode, new Date(), this.getClass().getName(), e.getLocalizedMessage() + "\n" +e.getMessage()+"\n"+ExceptionUtils.getFullStackTrace(e), null, caseNumber, GIRuntimeException.Component.BATCH.getComponent(), null);
		if(giMonitor != null) {
			giMonitorId =  giMonitor.getId();
		}
		return giMonitorId;
	}

	private void waitForAllTaskToComplete(Set<Future<String>> tasks) {
		boolean batchIsNotCompleted = true; 
		long currentNanoTime = System.nanoTime();
		long elapsedNanoTime = System.nanoTime();
		
		while(batchIsNotCompleted) {
			boolean isAllTaskCompleted = true;
			for (Future<String> future : tasks) {
				if(!future.isDone()) {
					isAllTaskCompleted = false;
					break;
				}
			}
			batchIsNotCompleted = !isAllTaskCompleted;
			elapsedNanoTime = System.nanoTime() - currentNanoTime;
			if(elapsedNanoTime/1000000000 >  60 * 15) {// break if running for 15 min.
				break;
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				LOGGER.error("Thread interrupted from sleep.");
			}
		}
	}

	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}

	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}

	public SsapCloneApplicationService getSsapCloneApplicationService() {
		return ssapCloneApplicationService;
	}

	public void setSsapCloneApplicationService(
			SsapCloneApplicationService ssapCloneApplicationService) {
		this.ssapCloneApplicationService = ssapCloneApplicationService;
	}

	public GIMonitorService getGiMonitorService() {
		return giMonitorService;
	}

	public void setGiMonitorService(GIMonitorService giMonitorService) {
		this.giMonitorService = giMonitorService;
	}

	public String getProcessErrorRecords() {
		return processErrorRecords;
	}

	public void setProcessErrorRecords(String processErrorRecords) {
		this.processErrorRecords = processErrorRecords;
	}
	
	private class SsapRedeterminationProcessor implements Callable<String> {
		private static final String DENTAL = "Dental";
		private static final String HEALTH = "Health";
		private RenewalApplication renewalApplication;
		private Long renewalYear;
		private boolean processErrorRecordFlag;
		private ObjectFactory factory = new ObjectFactory();
		private static final int HARDSHIP_EXEMPTION_AGE = 30;
		private static final String Y = "Y";
		private static final String FAILURE = "Failure";
		private static final String SUCCESS = "Success";
		private String fipsCode;
		private SimpleDateFormat format = new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT);
		private Float ehbAmount;
		private static final String _200 = "200";
		private static final String _131 = "131";
		private static final String _129 = "129";
		private static final String US_CITIZEN = "1";
		private static final String NON_US_CITIZEN = "3";
		private static final String APTC = "APTC";
		private static final String SPTC = "SPTC";
		
		public SsapRedeterminationProcessor(RenewalApplication renewalApplication, boolean processErrorRecordFlag, Long renewalYear) {
			this.renewalApplication = renewalApplication;
			this.renewalYear = renewalYear;
			this.processErrorRecordFlag = processErrorRecordFlag;
		}
		
		@Override
		public String call() throws Exception {
			try {
			SsapApplication currentApplication = loadCurrentApplication(renewalApplication.getSsapApplicationId());

			
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
			
			Map<Long, SsapApplicant> applicantMap = currentApplication.getSsapApplicants().stream()
					                         						  .collect(Collectors.toMap(SsapApplicant::getPersonId, Function.identity()));
		
			List<RenewalApplicant> renewalApplicantList = renewalApplicantRepository.findBySsapApplicationIdAndRenewalStatus(renewalApplication.getSsapApplicationId());
	        
	        Map<String, List<RenewalApplicant>> applicantPlantypetMap = null;
	        
	        if(processErrorRecordFlag) {
	            applicantPlantypetMap = renewalApplicantList.stream()
	                    				.filter(renewalApplicant -> (RenewalStatus.APP_ERROR.equals(renewalApplicant.getRenewalStatus()) || 
	                    						                     RenewalStatus.TO_CONSIDER.equals(renewalApplicant.getRenewalStatus())))
	                    				.collect(Collectors.groupingBy(RenewalApplicant::getPlanType));
	        } else {
	            applicantPlantypetMap = renewalApplicantList.stream()
	                    				.filter(renewalApplicant -> RenewalStatus.TO_CONSIDER.equals(renewalApplicant.getRenewalStatus()))
	                    				.collect(Collectors.groupingBy(RenewalApplicant::getPlanType));
	        }

			return processSsap(currentApplication, renewalApplication, applicantPlantypetMap, applicantMap, singleStreamlinedApplication);
			}catch(Exception e) {
				LOGGER.error("Exception occured for calling auto renewal"+ ExceptionUtils.getFullStackTrace(e));
				Integer giMonitorId =  AutoEnroll.this.logToGIMonitor(e, 50011,Long.toString(renewalApplication.getSsapApplicationId()));
				saveGiMonitorId(renewalApplication, giMonitorId);
				throw new Exception("Error occoured while getting health and dental enrollment count " + e);
			}
		}
		
		private void saveGiMonitorId(RenewalApplication renewalApplication, Integer giMonitorId) {
			if(renewalApplication != null && giMonitorId != null) {
				renewalApplication.setGiMonitorId(Long.valueOf(giMonitorId.longValue()));
				renewalApplicationRepository.save(renewalApplication);
			}			
		}
		
		private void saveGiMonitorId(List<RenewalApplicant> renewalApplicantList, Integer giMonitorId) {
			if(renewalApplicantList != null && giMonitorId != null) {
				for (RenewalApplicant renewalApplicant : renewalApplicantList) {
					renewalApplicant.setGiMonitorId(Long.valueOf(giMonitorId.longValue()));
				}
				renewalApplicantRepository.save(renewalApplicantList);
			}
		}
		
		private String processSsap(SsapApplication currentApplication, RenewalApplication renewalApplication, Map<String, List<RenewalApplicant>> applicantPlantypetMap, 
                Map<Long, SsapApplicant> applicantMap, 
                SingleStreamlinedApplication singleStreamlinedApplication) throws Exception {

				List<RenewalApplicant> applicanHealthPlantList = applicantPlantypetMap.get(HEALTH);
				List<RenewalApplicant> applicanDentalPlantList = applicantPlantypetMap.get(DENTAL);
				Map<Long, String> ssapIDToGuidMap = new HashMap<>();
				boolean healthFailSetDentalAPTCNull = false;
				boolean appErroInHealthRenewal = false;
				
				//Calculate APTC and SPTC for each Health plan member
				Map<Long, BigDecimal> memberAptcMap = currentApplication.getMaximumAPTC() != null ? 
																getAptcSptcMemberList(currentApplication, applicanHealthPlantList, APTC) : null;
				Map<Long, BigDecimal> memberSptcMap = currentApplication.getMaximumStateSubsidy() != null ? 
																getAptcSptcMemberList(currentApplication, applicanHealthPlantList, SPTC) : null;
				
				
				//HEALTH RENEWAL
				Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap = new HashMap<>();
				if (CollectionUtils.isNotEmpty(applicanHealthPlantList)) {
					applicantEnrollmentIdHealthtMap = applicanHealthPlantList.stream().collect(Collectors.groupingBy(RenewalApplicant::getEnrollmentId));
				}
				
				Map<Long, String> healthStatusMap = new HashMap<>();
				List<SsapApplicant> healthApplicants = new ArrayList<>();
				
				if (CollectionUtils.isNotEmpty(applicanHealthPlantList)) {
				for(RenewalApplicant renewalApplicant : applicanHealthPlantList) {
				 	for(SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
				 		if(ssapApplicant.getId()==renewalApplicant.getSsapApplicantId()) {
				 			ssapIDToGuidMap.put(ssapApplicant.getPersonId(), ssapApplicant.getApplicantGuid());
				 		}
						}
					}
				}

				for (Long enrollmentId : applicantEnrollmentIdHealthtMap.keySet()) {

					LOGGER.info("Started processing health enrollment ID : " + enrollmentId);
					if (enrollmentId != null) {

						List<RenewalApplicant> renewalApplicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);
						
						String healthResponseCodeStatus = preapareAndSendIND71ForHealth(renewalApplicantList, currentApplication, singleStreamlinedApplication, 
																						ssapIDToGuidMap, applicantMap, healthApplicants, 
																						healthFailSetDentalAPTCNull, enrollmentId, memberAptcMap, memberSptcMap);

						if (CollectionUtils.isNotEmpty(renewalApplicantList)) {

							List<SsapApplicant> healthApplicantListToCreateEvents = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
							ssapCloneApplicationService.createApplicationEvents(currentApplication, healthApplicantListToCreateEvents, healthResponseCodeStatus,
								enrollmentId, renewalApplicantList.get(0).getPlanId(), HEALTH);
						}
						else {
							LOGGER.warn("Renewal Applicant List is not found for creating Application Events.");
						}
						healthStatusMap.put(enrollmentId, healthResponseCodeStatus);
					}
				}
				
				for(Long enrollmentId : healthStatusMap.keySet()) {
				
					if(!healthStatusMap.get(enrollmentId).equalsIgnoreCase(_200)) {
						if(healthStatusMap.get(enrollmentId).equalsIgnoreCase(FAILURE)) {
							appErroInHealthRenewal = true;
						}
						healthFailSetDentalAPTCNull = true;
					}
				}
				
				boolean processDental = true;
				
				if(healthFailSetDentalAPTCNull)
				{
					String renewDental = DynamicPropertiesUtil.getPropertyValue(RenewalConfiguration.RenewalConfigurationEnum.RENEW_DENTAL);
					if(renewDental.equals("N") && !(ExchangeEligibilityStatus.QHP.equals(currentApplication.getExchangeEligibilityStatus())))
					{
						processDental = false;
					}
				}
				
				//DENTAL RENEWAL
				Map<Long, String> dentalStatusMap = new HashMap<>();
				Map<Long, List<RenewalApplicant>> applicantEnrollmentIdDentalMap = new HashMap<>();
				List<SsapApplicant> dentalApplicants = new ArrayList<>();
				if(processDental) {
				/* As per current logic SSAP Application can only have one Dental enrollment
				* Hence applicantEnrollmentIdDentalMap should contain only one entry. */
					
				if (CollectionUtils.isNotEmpty(applicanDentalPlantList)) {
				     applicantEnrollmentIdDentalMap = applicanDentalPlantList.stream().collect(Collectors.groupingBy(RenewalApplicant::getEnrollmentId));
				}
				
				ssapIDToGuidMap.clear();
				if (CollectionUtils.isNotEmpty(applicanDentalPlantList)) {
				for(RenewalApplicant renewalApplicant : applicanDentalPlantList) {
				 	for(SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
				 		if(ssapApplicant.getId()==renewalApplicant.getSsapApplicantId()) {
				 			ssapIDToGuidMap.put(ssapApplicant.getPersonId(), ssapApplicant.getApplicantGuid());
				 		}
						}
					}
				}

					for (Long enrollmentId : applicantEnrollmentIdDentalMap.keySet()) {

						LOGGER.info("Started processing dental enrollment ID : " + enrollmentId);
						if (enrollmentId != null) {

							List<RenewalApplicant> renewalApplicantList = applicantEnrollmentIdDentalMap.get(enrollmentId);
							String dentalResponseCodeStatus = preapareAndSendIND71ForDental(renewalApplicantList,
									currentApplication, singleStreamlinedApplication, ssapIDToGuidMap, applicantMap,
									dentalApplicants, enrollmentId, healthFailSetDentalAPTCNull, renewalYear);

							if (CollectionUtils.isNotEmpty(renewalApplicantList)) {

								List<SsapApplicant> dentalApplicantListToCreateEvents = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
								ssapCloneApplicationService.createApplicationEvents(currentApplication, dentalApplicantListToCreateEvents, dentalResponseCodeStatus,
									enrollmentId, renewalApplicantList.get(0).getPlanId(), DENTAL);
							}
							else {
								LOGGER.warn("Renewal Applicant List is not found for creating Application Events.");
							}
							dentalStatusMap.put(enrollmentId, dentalResponseCodeStatus);
						}
					}
				}else if(CollectionUtils.isNotEmpty(applicanDentalPlantList) && !appErroInHealthRenewal) {
					renewalApplication.setDentalRenewalStatus(RenewalStatus.NOT_TO_CONSIDER);
					Integer reasonCodeValueId = getReasonCode("DNC");
					renewalApplication.setDentalReasonCode(reasonCodeValueId != null ? reasonCodeValueId : null);
					renewalApplication = renewalApplicationRepository.save(renewalApplication);
				}
				updateApplicationStatus(healthStatusMap, dentalStatusMap, applicantEnrollmentIdHealthtMap, applicantEnrollmentIdDentalMap, currentApplication, renewalApplication);
				return SUCCESS;
			}

		private Map<Long, BigDecimal> getAptcSptcMemberList(SsapApplication currentApplication, List<RenewalApplicant> applicantHealthPlantList, String type) 
				                                            throws GIException {
			List<AptcRatioResponse.Member> aptcMemberList;
			Map<Long, BigDecimal> memberAptcSptcMap = new HashMap<>();
			Date coverageDate =  ssapCloneApplicationService.getCoverageStartDate(currentApplication);
			List<SsapApplicant> applicantList = new ArrayList<>();
			Map<Long, Boolean> programEligibilityMap = new HashMap<>();
			List<SsapApplicant> newApplicants = ssapApplicantRepository.findBySsapApplication(currentApplication);
			for(SsapApplicant applicant : newApplicants ){
				if(type.equalsIgnoreCase(APTC)) {
					
					programEligibilityMap.put(applicant.getId(), 
			                  checkEligibility(getProgramEligibility(applicant.getId()), EligibilityType.APTC_ELIGIBILITY_TYPE));
					
				} else if (type.equalsIgnoreCase(SPTC)) {
					
					programEligibilityMap.put(applicant.getId(), 
			                  checkEligibility(getProgramEligibility(applicant.getId()), EligibilityType.STATE_SUBSIDY_ELIGIBILITY_TYPE));
				}
			}
			
			for(SsapApplicant applicant : newApplicants ) {
				
				if(programEligibilityMap.get(applicant.getId())) {
					
					applicantList.add(applicant);
					}
				}
			
			AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
			aptcRatioRequest.setCoverageYear((int) currentApplication.getCoverageYear());
			
			if (type.equalsIgnoreCase(APTC)) {

				aptcRatioRequest.setMaxAptc(currentApplication.getMaximumAPTC());
				
			} else if (type.equalsIgnoreCase(SPTC)) {

				aptcRatioRequest.setMaxAptc(currentApplication.getMaximumStateSubsidy());
			}
			
			List<AptcRatioRequest.Member> requestMemberList = new ArrayList<AptcRatioRequest.Member>();
			AptcRatioRequest.Member requestMember = null;
			
			for (SsapApplicant ssapApplicant : applicantList) {
				requestMember = aptcRatioRequest.new Member();
				requestMember.setAge(getAgeFromDob(DateUtil.dateToString(ssapApplicant.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT), coverageDate));
				requestMember.setId(Long.toString(ssapApplicant.getId()));
				
				requestMemberList.add(requestMember);
			}
			
			aptcRatioRequest.setMemberList(requestMemberList);
			AptcRatioResponse aptcRatioResponse = null;
			
			try{
				Object responseObj = customGroupingUtil.getAptcDistribution(aptcRatioRequest);
				
				if(responseObj instanceof String) {
					String errorMessage = (String) responseObj;
					throw new GIRuntimeException("Error occoured while getting aptc ratio: " + errorMessage);
				} else if(responseObj instanceof Exception){
					String errorMessage = (String) responseObj;
					throw new GIRuntimeException("Error occoured while getting aptc ratio: " + errorMessage);
				} else {
					aptcRatioResponse = (AptcRatioResponse) responseObj;
				}
			}catch(Exception e){
    			LOGGER.error("Exception occured for calling FETCH_APTC_RATIO api for APTC",e);
    			throw new GIRuntimeException("Error occoured while getting aptc ratio: " + e);
    		}
			
			if(aptcRatioResponse != null && aptcRatioResponse.getMemberList() != null) {
				aptcMemberList = aptcRatioResponse.getMemberList();
				
				for (AptcRatioResponse.Member member : aptcMemberList) {
					memberAptcSptcMap.put(Long.valueOf(member.getId()), member.getAptc());
				}
				
				return memberAptcSptcMap;
			}
			
			return null;
		}
		
		private List<EligibilityProgram> getProgramEligibility(Long ssapApplicantId) {
			
			return renewalEligibilityProgramRepository.getApplicantEligibilities(ssapApplicantId);
		}

		private boolean checkEligibility(List<EligibilityProgram> programs,EligibilityType eligibilityType) 
		{
			boolean flag1 = false;
			boolean flag2 = false;
			
			if(programs != null && programs.size() > 0) {
				
				for(EligibilityProgram eligibilityProgram : programs){
					if (eligibilityType.toString().equals(eligibilityProgram.getEligibilityType()) && "TRUE".equals(eligibilityProgram.getEligibilityIndicator())){
						flag1 = true;
					}
					
					if (eligibilityProgram.getEligibilityType().toString().equals(EligibilityType.EXCHANGE_ELIGIBILITY_TYPE.toString()) && "TRUE".equals(eligibilityProgram.getEligibilityIndicator())){	
						flag2 = true;
					}
				}
			}
			
			return flag1 && flag2;
		}

		/**
		 * Method is used to get Applicant List to Create Events.
		 */
		private List<SsapApplicant> getApplicantListToCreateEvents(SsapApplication currentApplication,
				List<RenewalApplicant> renewalApplicantList, Map<Long, SsapApplicant> applicantMap) {

			List<SsapApplicant> applicantListToCreateEvents = new ArrayList<>();
			
			for(RenewalApplicant renewalApplicant : renewalApplicantList) {

				for(SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {

					if (ssapApplicant.getId() == renewalApplicant.getSsapApplicantId()) {
						applicantListToCreateEvents.add(applicantMap.get(ssapApplicant.getPersonId()));
			 		}
				}
			}
			return applicantListToCreateEvents;
		}
			
			private void updateApplicationStatus(Map<Long, String> healthStatusMap, Map<Long, String> dentalStatusMap, 
										 Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap, 
										 Map<Long, List<RenewalApplicant>> applicantEnrollmentIdDentalMap, 
										 SsapApplication currentApplication, 
										 RenewalApplication renewalApplication) {
			
			Map<RenewalStatus,Integer> applicationStatusMap = new HashMap<>();
			
			
			for(Long enrollmentId : healthStatusMap.keySet()) {
			
			updateApplicantStatus(enrollmentId, healthStatusMap.get(enrollmentId), applicationStatusMap, applicantEnrollmentIdHealthtMap, HEALTH);
			}
			
			rollupAppliationRenewalStatus(renewalApplication,applicationStatusMap,Integer.valueOf(healthStatusMap.size()),HEALTH);
			
			applicationStatusMap = new HashMap<>(); // RESET applicationStatusMap
			
			for(Long enrollmentId : dentalStatusMap.keySet()) {
			
			updateApplicantStatus(enrollmentId, dentalStatusMap.get(enrollmentId), applicationStatusMap, applicantEnrollmentIdDentalMap, DENTAL);
			}
			
			rollupAppliationRenewalStatus(renewalApplication,applicationStatusMap,Integer.valueOf(dentalStatusMap.size()),DENTAL);
			
			if((RenewalStatus.MANUAL_ENROLMENT.equals(renewalApplication.getDentalRenewalStatus()) || RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getDentalRenewalStatus())
					|| RenewalStatus.RENEWED.equals(renewalApplication.getDentalRenewalStatus())) && (RenewalStatus.MANUAL_ENROLMENT.equals(renewalApplication.getHealthRenewalStatus()) || RenewalStatus.NOT_TO_CONSIDER.equals(renewalApplication.getHealthRenewalStatus())
							|| RenewalStatus.RENEWED.equals(renewalApplication.getHealthRenewalStatus()))) {
			
			renewalApplication.setHouseholdRenewalStatus(RenewalStatus.RENEWAL_PROCESS_COMPLETED);
			}
			
			renewalApplicationRepository.save(renewalApplication);
			
			}
			
			private void updateApplicantStatus(Long enrollmentId, String responseCode, Map<RenewalStatus, Integer> applicationStatusMap, 
			                        Map<Long, List<RenewalApplicant>> applicantEnrollmentIdHealthtMap, String type) {
			
			List<RenewalApplicant> applicantList = null;
			
			if(_200.equalsIgnoreCase(responseCode)) {
			
			applicationStatusMap.putIfAbsent(RenewalStatus.RENEWED, 0);
			applicationStatusMap.put(RenewalStatus.RENEWED, applicationStatusMap.get(RenewalStatus.RENEWED) + 1);
			
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);
			
			for (RenewalApplicant renewalApplicant : applicantList) {
				
				renewalApplicant.setRenewalStatus(RenewalStatus.RENEWED);
				if(HEALTH.equals(type)) {
				renewalApplicant.setGroupRenewalStatus(RenewalStatus.RENEWED);
			}
			}
			
			renewalApplicantRepository.save(applicantList);
			
			}else if(_131.equalsIgnoreCase(responseCode) || _129.equalsIgnoreCase(responseCode)) {
			
			applicationStatusMap.putIfAbsent(RenewalStatus.MANUAL_ENROLMENT, 0);
			applicationStatusMap.put(RenewalStatus.MANUAL_ENROLMENT, applicationStatusMap.get(RenewalStatus.MANUAL_ENROLMENT) + 1);
			
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);
			
			for (RenewalApplicant renewalApplicant : applicantList) {
				
				renewalApplicant.setRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
				
				if(_131.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setMemberFalloutReasonCode(getReasonCode("NO_PLAN"));
				}else if(_129.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setMemberFalloutReasonCode(getReasonCode("NO_CROSS_WALK_PLAN"));
				}
				
				if(HEALTH.equals(type)) {
				renewalApplicant.setGroupRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
				if(_131.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setGroupReasonCode(getReasonCode("NO_PLAN"));
				}else if(_129.equalsIgnoreCase(responseCode)) {
					renewalApplicant.setGroupReasonCode(getReasonCode("NO_CROSS_WALK_PLAN"));
				}
			}
			}
			
			renewalApplicantRepository.save(applicantList);
			
			}else {
			
			applicationStatusMap.putIfAbsent(RenewalStatus.APP_ERROR, 0);
			applicationStatusMap.put(RenewalStatus.APP_ERROR, applicationStatusMap.get(RenewalStatus.APP_ERROR) + 1);
			
			applicantList = applicantEnrollmentIdHealthtMap.get(enrollmentId);
			
			for (RenewalApplicant renewalApplicant : applicantList) {
				
				renewalApplicant.setRenewalStatus(RenewalStatus.APP_ERROR);
				renewalApplicant.setMemberFalloutReasonCode(getReasonCode("AUTO_RENEWAL_ERROR"));
				if(HEALTH.equals(type)) {
				renewalApplicant.setGroupRenewalStatus(RenewalStatus.APP_ERROR);
				renewalApplicant.setGroupReasonCode(getReasonCode("AUTO_RENEWAL_ERROR"));
			}
			}
			
			renewalApplicantRepository.save(applicantList);
			}
			}
			
			private void rollupAppliationRenewalStatus(RenewalApplication renewalApplication, Map<RenewalStatus,Integer> applicationStatusMap, Integer groupCount ,String insuranceType) {

				if(groupCount>0) {
					if(applicationStatusMap.containsKey(RenewalStatus.APP_ERROR) && applicationStatusMap.get(RenewalStatus.APP_ERROR)>0) {
						if(HEALTH.equalsIgnoreCase(insuranceType)) {
						renewalApplication.setHealthRenewalStatus(RenewalStatus.APP_ERROR);
						}else {
							renewalApplication.setDentalRenewalStatus(RenewalStatus.APP_ERROR);
						}
						
					}else if(applicationStatusMap.containsKey(RenewalStatus.RENEWED) && groupCount.equals(applicationStatusMap.get(RenewalStatus.RENEWED))) {
						if(HEALTH.equalsIgnoreCase(insuranceType)) {
						renewalApplication.setHealthRenewalStatus(RenewalStatus.RENEWED);
						}else {
							renewalApplication.setDentalRenewalStatus(RenewalStatus.RENEWED);
						}
					}else if(applicationStatusMap.containsKey(RenewalStatus.RENEWED)) {
						if(HEALTH.equalsIgnoreCase(insuranceType)) {
						renewalApplication.setHealthRenewalStatus(RenewalStatus.PARTIAL_RENEWED);
						}else {
							renewalApplication.setDentalRenewalStatus(RenewalStatus.PARTIAL_RENEWED);
						}
					}else{
						if(HEALTH.equalsIgnoreCase(insuranceType)) {
						renewalApplication.setHealthRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
						}else {
							renewalApplication.setDentalRenewalStatus(RenewalStatus.MANUAL_ENROLMENT);
						}
					}
				}
			}
			
			private String preapareAndSendIND71ForDental(List<RenewalApplicant> renewalApplicantList, SsapApplication currentApplication, SingleStreamlinedApplication singleStreamlinedApplication, 
			                                  Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap, List<SsapApplicant> dentalApplicants, 
			                                  Long enrollmentId, boolean healthFailSetDentalAPTCNull, Long renewalYear) throws Exception {
			
			AutoRenewalResponse autoRenewalResponse = null;
			AutoRenewalRequest autoRenewalRequest = null;
			String dentalRenewalStatus = FAILURE;
			
			//DENTAL RENEWAL
			try {
			    if(CollectionUtils.isNotEmpty(renewalApplicantList)) {
					LOGGER.info("Started processing dental renewal for application " + currentApplication.getCaseNumber());
					
							
					// set var for dental enrollment
					//existingSADPEnrollmentID = enrollmentId;
				
				// create map of eligible enrollee;
				//prepareDentalEnrollee(currentApplication, ssapIDToGuidMap, singleStreamlinedApplication,  applicantMap);
				
				// create auto renewal Request
				autoRenewalRequest = factory.createAutoRenewalRequest();
				autoRenewalRequest.setHousehold(getHousehold(currentApplication, singleStreamlinedApplication, ssapIDToGuidMap, applicantMap, renewalApplicantList, healthFailSetDentalAPTCNull, DENTAL,enrollmentId, null, null));
				
				// trigger ind71
				autoRenewalResponse = invokeInd71(autoRenewalRequest);
				
				// fetch latest application
				currentApplication = ssapApplicationRepository.findOne(currentApplication.getId());
				
				// process autorenewal reposne
				dentalRenewalStatus = processAutoRenewalResponse(autoRenewalResponse);
						
				prepareApplicantsForLogging(ssapIDToGuidMap, applicantMap, dentalApplicants);
			}
			}
		    catch (GIException e) {
	    	Integer giMonitorId = AutoEnroll.this.logToGIMonitor(e, 50011,renewalApplication.getSsapApplicationId()+"--"+DENTAL+"--"+enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
			} catch (Exception e) {
			
			Integer giMonitorId = AutoEnroll.this.logToGIMonitor(e, 50011,renewalApplication.getSsapApplicationId()+"--"+DENTAL+"--"+enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
			}
			
			LOGGER.info("End processing dental enrollment for application with status: " + dentalRenewalStatus);
			return dentalRenewalStatus;
			}
			
			private String preapareAndSendIND71ForHealth(List<RenewalApplicant> renewalApplicantList, SsapApplication currentApplication, SingleStreamlinedApplication singleStreamlinedApplication,
														 Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap, List<SsapApplicant> healthApplicants, 
														 boolean healthFailSetDentalAPTCNull, Long enrollmentId, Map<Long, BigDecimal> memberAptcMap, 
														 Map<Long, BigDecimal> memberSptcMap) throws Exception {
			
			AutoRenewalResponse autoRenewalResponse = null;
			AutoRenewalRequest autoRenewalRequest = null;
			String healthRenewalStatus = FAILURE;
			
			try {
				if(CollectionUtils.isNotEmpty(renewalApplicantList)) {
			LOGGER.info("Started processing health enrollment for application " + currentApplication.getCaseNumber());
			autoRenewalRequest = factory.createAutoRenewalRequest();
			
			Household household = getHousehold(currentApplication, singleStreamlinedApplication, ssapIDToGuidMap,
											   applicantMap, renewalApplicantList, healthFailSetDentalAPTCNull, HEALTH, enrollmentId, memberAptcMap, memberSptcMap );
			autoRenewalRequest.setHousehold(household);
			
			autoRenewalResponse = invokeInd71(autoRenewalRequest);
			
			currentApplication = ssapApplicationRepository.findOne(currentApplication.getId());
			
			healthRenewalStatus = processAutoRenewalResponse(autoRenewalResponse);
			
			prepareApplicantsForLogging(ssapIDToGuidMap, applicantMap, healthApplicants);
				}
			} catch (GIException e) {
			Integer giMonitorId = AutoEnroll.this.logToGIMonitor(e, 50011,renewalApplication.getSsapApplicationId()+"--"+HEALTH+"--"+enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
			} catch (Exception e) {
			Integer giMonitorId = AutoEnroll.this.logToGIMonitor(e, 50011,renewalApplication.getSsapApplicationId()+"--"+HEALTH+"--"+enrollmentId);
			saveGiMonitorId(renewalApplicantList, giMonitorId);
			}
			
			LOGGER.info("End processing health enrollment for application with status: " + healthRenewalStatus);
			return healthRenewalStatus;
			}
			
			private void prepareApplicantsForLogging(Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap, List<SsapApplicant> healthApplicants) {
				if(ssapIDToGuidMap != null ) {
					for (Long key : ssapIDToGuidMap.keySet()) {
						healthApplicants.add(applicantMap.get(key));
					}
				}
			}
			
			
			private String processAutoRenewalResponse(AutoRenewalResponse autoRenewalResponse) throws Exception{
			
			if(autoRenewalResponse != null) {
			
			return autoRenewalResponse.getResponseCode();
			}
			
			return null;
			}
			
			private AutoRenewalResponse invokeInd71(AutoRenewalRequest autoRenewalRequest) throws Exception {
				
				AutoRenewalResponse autoRenewalResponse = factory.createAutoRenewalResponse();
				
				try{
					
					long time = System.currentTimeMillis();
					LOGGER.info("calling ind71 for " + autoRenewalRequest.getHousehold().getApplicationId());
					JAXBContext context  = JAXBContext.newInstance(AutoRenewalRequest.class);
					StringWriter s = new StringWriter();
					context.createMarshaller().marshal(autoRenewalRequest, s);
					LOGGER.warn("Invoking IND71 Request XML : "+ s);
					ind71WebServiceTemplate.setDefaultUri(GhixEndPoints.PLANDISPLAY_URL+"webservice/plandisplay/endpoints/AutoRenewalService.wsdl");
					autoRenewalResponse = (AutoRenewalResponse) ind71WebServiceTemplate.marshalSendAndReceive(autoRenewalRequest);
					LOGGER.info("received ind71 response for " + autoRenewalRequest.getHousehold().getApplicationId()+ " in "+ (System.currentTimeMillis()-time)+" ms");
				}catch(SoapFaultClientException e){
				LOGGER.error("Error while invoking the Auto Renewal Service ", e);
				LOGGER.error("fault string"+e.getFaultStringOrReason());
				LOGGER.error("msg"+e.getMessage());
				LOGGER.error("fault code "+e.getFaultCode());
				LOGGER.error("root cause"+e.getRootCause());
				throw e;
				}catch(Exception ex){
				LOGGER.error("IND71 from ghix-plandisplay failed", ex);
				throw ex;
				}		
				return autoRenewalResponse;
			}
			
			
			private Household getHousehold(SsapApplication currentApplication, SingleStreamlinedApplication singleStreamlinedApplication, Map<Long, String> ssapIDToGuidMap, 
			                    		   Map<Long, SsapApplicant> applicantMap, List<RenewalApplicant> renewalApplicantList, boolean healthFailSetDentalAPTCNull, 
			                    		   String enrollmentType, Long enrollmentId, Map<Long, BigDecimal> memberAptcMap, Map<Long, BigDecimal> memberSptcMap) 
			                    		   throws GIException, ParseException {
			
			Household household = factory.createAutoRenewalRequestHousehold();
			
			household.setApplicationId(currentApplication.getId());
			
			if(enrollmentType.equals(HEALTH)){
			household.setProductType(ProductType.H);
			}else{
			household.setProductType(ProductType.D);
			}
			
			HouseHoldContact houseHoldContact = getHouseHoldContact(singleStreamlinedApplication);
			
			household.setHouseHoldContact(houseHoldContact);
			
			household.setIndividual(getIndividual(currentApplication, renewalApplicantList, enrollmentType, healthFailSetDentalAPTCNull, applicantMap, memberAptcMap, memberSptcMap));
			
			String coverageDate = household.getIndividual().getCoverageStartDate();
			household.setMembers(getMembers(currentApplication, houseHoldContact, singleStreamlinedApplication, 
				                        ssapIDToGuidMap, applicantMap, renewalApplicantList, enrollmentType, coverageDate,enrollmentId));
			
			household.setResponsiblePerson(getResponsiblePerson(singleStreamlinedApplication));	
			
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
            if(!"MN".equalsIgnoreCase(stateCode)) {
            	try {
        			ResponseEntity<DesignateBroker> brokerDesignationResponse = ghixRestTemplate.exchange(GhixEndPoints.BrokerServiceEndPoints.GET_INDIVIDUAL_DESIGNATION, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateBroker.class, new Long(household.getIndividual().getHouseholdCaseId()).intValue());
        			
        			if(brokerDesignationResponse != null) {
        				DesignateBroker designateBroker = brokerDesignationResponse.getBody();
        				
        				if(designateBroker != null && designateBroker.getStatus().toString().equalsIgnoreCase("Active")) {						
        					household.setUserRoleId(designateBroker.getBrokerId() + "");
        					household.setUserRoleType(RoleTypeVal.AGENT);
        					
        				} else {
        					ResponseEntity<DesignateAssister> assisterDesignationResponse = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEntityEndPoints.GET_INDIVIDUAL_DESIGNATION, "exadmin@ghix.com", HttpMethod.POST, MediaType.APPLICATION_JSON, DesignateAssister.class, new Long(household.getIndividual().getHouseholdCaseId()).intValue());
        					
        					if(assisterDesignationResponse != null) {
        						DesignateAssister designateAssister = assisterDesignationResponse.getBody();
        						
        						if(designateAssister != null && designateAssister.getStatus().toString().equalsIgnoreCase("Active")) {
        							household.setUserRoleId(designateAssister.getAssisterId() + "");
        							household.setUserRoleType(RoleTypeVal.ASSISTER);
        						}
        					}
        				}
        			}
        			} catch (RestClientException e) {
        			LOGGER.error("Exception finding designated assister or broker");
        			}
            }
			
			return household;
			}
			
			private HouseHoldContact getHouseHoldContact(SingleStreamlinedApplication singleStreamlinedApplication) {
			HouseHoldContact houseHoldContact = factory.createAutoRenewalRequestHouseholdHouseHoldContact();
			
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
			
			for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
				
				if(houseHoldMember.getPersonId() == 1) {	
					
					if(houseHoldMember.getSocialSecurityCard() != null && !StringUtils.isBlank(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber())) {
						houseHoldContact.setHouseHoldContactFederalTaxIdNumber(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber());
					}
					
					if(houseHoldMember.getName() != null) {
						houseHoldContact.setHouseHoldContactFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));
						houseHoldContact.setHouseHoldContactLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						houseHoldContact.setHouseHoldContactMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
						houseHoldContact.setHouseHoldContactSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getHomeAddress() != null) {
					houseHoldContact.setHouseHoldContactHomeAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
					houseHoldContact.setHouseHoldContactHomeAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
					houseHoldContact.setHouseHoldContactHomeCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getCity()));
					houseHoldContact.setHouseHoldContactHomeState(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getState()));
					houseHoldContact.setHouseHoldContactHomeZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getPostalCode()));
					
					fipsCode = houseHoldMember.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
					}
					
					houseHoldContact.setHouseHoldContactId(nullCheckedValue(houseHoldMember.getApplicantGuid()));
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getContactPreferences() != null 
							&& !(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL")
									|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						houseHoldContact.setHouseHoldContactPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getPhone() != null) {
						houseHoldContact.setHouseHoldContactPrimaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getOtherPhone() != null && nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						houseHoldContact.setHouseHoldContactSecondaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
					
				}
			}				
			}
					
			return houseHoldContact;
			}
			
			private String nullCheckedValue(String value) {
			if(StringUtils.isBlank(value)) {
			return null;
			}
			return value;
			}
			
			
			private Individual getIndividual(SsapApplication currentApplication, List<RenewalApplicant> renewalApplicantList, String enrollmentType, 
					                         boolean healthFailSetDentalAPTCNull, Map<Long, SsapApplicant> applicantMap, Map<Long, BigDecimal> memberAptcMap, 
					                         Map<Long, BigDecimal> memberSptcMap) throws GIException, ParseException {
				
			Individual individual = factory.createAutoRenewalRequestHouseholdIndividual();
			
			Date coverageDate =  ssapCloneApplicationService.getCoverageStartDate(currentApplication);
			individual.setCoverageStartDate(getFormatedDate(coverageDate));
			individual.setEnrollmentType(EnrollmentVal.A);
			String planId = "";
			
			for(RenewalApplicant renewalApplicant : renewalApplicantList ) {
				 if(StringUtils.isNotBlank(renewalApplicant.getPlanId())) {
					 planId = renewalApplicant.getPlanId();
					 break;
				 }
			}
			
			individual.setPlanId(planId);
			BigDecimal totalAptc = new BigDecimal("0.0");
			BigDecimal totalSptc = new BigDecimal("0.0");
			
			if(enrollmentType.equals(HEALTH)){
				
			if(memberAptcMap != null ) {
				
				for (RenewalApplicant renewalApplicant : renewalApplicantList ) {
					if(memberAptcMap.containsKey(renewalApplicant.getSsapApplicantId())) {
						totalAptc = totalAptc.add(memberAptcMap.get(renewalApplicant.getSsapApplicantId()));
					}
				}
			}
				
			individual.setAptc(totalAptc.floatValue());
			
			if(memberSptcMap != null) {

				for (RenewalApplicant renewalApplicant : renewalApplicantList ) {
					if(memberSptcMap.containsKey(renewalApplicant.getSsapApplicantId())) {
						totalSptc = totalSptc.add(memberSptcMap.get(renewalApplicant.getSsapApplicantId()));
					}
				}
			}
			
			individual.setStateSubsidy(totalSptc);
			
			}else if(healthFailSetDentalAPTCNull){
			
			individual.setAptc(null); // As Health renewals not suceesfull setting dental APTC as null
			
			}else {
			
			final String applyAptcForDental =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_APPLY_APTC_FOR_DENTAL);
			
			if(applyAptcForDental != null && applyAptcForDental.equalsIgnoreCase("Y")) {
				
				individual.setAptc(ssapCloneApplicationService.getRemainingAPTCForDental(currentApplication));
			}
			
			}
			if(enrollmentType.equals(HEALTH)){
				List<SsapApplicant> groupApplicants = getApplicantListToCreateEvents(currentApplication, renewalApplicantList, applicantMap);
				Set<String> set = new LinkedHashSet<String>(); 
				List<String> groupCSList = new ArrayList<String>();
				for (SsapApplicant ssapApplicant : groupApplicants) {
					if("QHP".equalsIgnoreCase(ssapApplicant.getEligibilityStatus())) {
						if(StringUtils.isBlank(ssapApplicant.getCsrLevel())){
							set.add("CS1");
						}else {
							set.add(ssapApplicant.getCsrLevel());
						}
					}
				}
				groupCSList.addAll(set);
				String groupCS = getLowPriorityCSLevel(groupCSList);
			
				individual.setCsr(groupCS);
			}
			setEhbAmount(currentApplication, coverageDate,individual,enrollmentType );
			
			if(currentApplication.getCmrHouseoldId() != null) {
			individual.setHouseholdCaseId(currentApplication.getCmrHouseoldId().longValue());
			}
			
			return individual;
			}
			
			private void setEhbAmount(SsapApplication currentApplication, Date coverageDate, Individual individual, String enrollmentType) {
			if(ehbAmount == null){
			ehbAmount = ssapCloneApplicationService.getEhbAmount(currentApplication.getCaseNumber(),coverageDate);
			}
			
			individual.setEhbAmount(ehbAmount);
			
			}
			
			private String getFormatedDate(Date date)
			throws ParseException {
			return format.format(date.getTime());
			}
			
			
			private String getLowPriorityCSLevel(List<String> hhMemberCSLevels){
				List<String> csPriorityList = new ArrayList<>(Arrays.asList("CS1", "CS4", "CS5", "CS6", "CS3", "CS2"));
				String CSLevel = null;
				Integer lowestIndex = null;
				Integer currentIndex = null;
				if(CollectionUtils.isNotEmpty(hhMemberCSLevels)) {
					for(String memberCS : hhMemberCSLevels) {
						currentIndex = csPriorityList.indexOf(memberCS);
						if(lowestIndex == null || lowestIndex>currentIndex) {
							lowestIndex = currentIndex;
						}
					}
					CSLevel = csPriorityList.get(lowestIndex);
				}
				return CSLevel;
			}
			
			private Members getMembers(SsapApplication currentApplication, HouseHoldContact houseHoldContact, SingleStreamlinedApplication singleStreamlinedApplication, 
			                Map<Long, String> ssapIDToGuidMap, Map<Long, SsapApplicant> applicantMap, List<RenewalApplicant> renewalApplicantList, 
			                String enrollmentType,String coverageDate, Long enrollmentId) throws ParseException, GIException {
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			Members members = factory.createAutoRenewalRequestHouseholdMembers();
			boolean isEveryoneUnder30 = true;
			
			String primaryTaxFilerPersonId = getPTFPersonId(singleStreamlinedApplication);
			
			Collection<SsapApplicant> ssapApplicantCollection= applicantMap.values();
			List<Long> renewSsapApplicantPersonIdList = new ArrayList<>();
			Map<Long,String> memberReasonCode = new HashMap<Long,String>();
			
			for (RenewalApplicant renewalApplicant : renewalApplicantList) {
			for (SsapApplicant ssapApplicant : ssapApplicantCollection) {
					if(renewalApplicant.getSsapApplicantId().equals(ssapApplicant.getId())) {
						if(renewalApplicant.getMemberFalloutReasonCode()!=null) {
							LookupValue lookupValue = lookupService.findLookupValuebyId(renewalApplicant.getMemberFalloutReasonCode());
							if(lookupValue!=null) {
								memberReasonCode.put(ssapApplicant.getPersonId(), lookupValue.getLookupValueCode());
							}
						}
				renewSsapApplicantPersonIdList.add(ssapApplicant.getPersonId());
			}
			}
			}
			
			List<HouseholdMember> hhMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
			HouseholdMember responsiblePerson = getApplicantJson(hhMembers,singleStreamlinedApplication.getPrimaryTaxFilerPersonId());
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
			
			for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
				
				if(ssapIDToGuidMap.get(new Long(houseHoldMember.getPersonId())) != null 
				   && houseHoldMember.getApplyingForCoverageIndicator() 
				   && renewSsapApplicantPersonIdList.contains(Long.valueOf(houseHoldMember.getPersonId()))) {	
					
					Member member = factory.createAutoRenewalRequestHouseholdMembersMember();
					member.setMemberId(ssapIDToGuidMap.get(new Long(houseHoldMember.getPersonId())));
					member.setNewPersonFLAG(YesNoVal.N);
					boolean newMember = false;
					if(memberReasonCode.get(new Long(houseHoldMember.getPersonId()))!=null && 
							"NEW_MEMBER".equalsIgnoreCase(memberReasonCode.get(new Long(houseHoldMember.getPersonId())))) {
						newMember = true;
					}
					if(newMember) {
						member.setNewPersonFLAG(YesNoVal.Y);
					}
					member.setCatastrophicEligible(isExemptHousehold(currentApplication) ? YesNoVal.Y : YesNoVal.N);
					member.setChildOnlyPlanEligibile(YesNoVal.N);
					
					if(houseHoldMember.getLivesWithHouseholdContactIndicator()) {
						
						member.setCountyCode(nullCheckedValue(fipsCode));
						member.setHomeAddress1(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeAddress1()));
						member.setHomeAddress2(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeAddress2()));
						member.setHomeCity(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeCity()));
						member.setHomeState(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeState()));
						member.setHomeZip(nullCheckedValue(houseHoldContact.getHouseHoldContactHomeZip()));
						
					} else {
						Address otherAddress = new Address();
						if(houseHoldMember.getOtherAddress() != null && houseHoldMember.getOtherAddress().getAddress() != null && houseHoldMember.getOtherAddress().getAddress().getPostalCode() != null){
							otherAddress = houseHoldMember.getOtherAddress().getAddress();	
						}else if(singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses() != null && singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses().size() > 0){	
							for(Address otherAddressObj : singleStreamlinedApplication.getTaxHousehold().get(0).getOtherAddresses()) {
								if(otherAddressObj.getAddressId() == houseHoldMember.getAddressId()) {
									otherAddress = otherAddressObj;
								}
							}							
						}
						
						member.setHomeAddress1(nullCheckedValue(otherAddress.getStreetAddress1()));
						member.setHomeAddress2(nullCheckedValue(otherAddress.getStreetAddress2()));
						member.setHomeCity(nullCheckedValue(otherAddress.getCity()));
						member.setHomeState(nullCheckedValue(otherAddress.getState()));
						member.setHomeZip(nullCheckedValue(otherAddress.getPostalCode()));
						member.setCountyCode(nullCheckedValue(otherAddress.getCountyCode()));
					}
			
					if(houseHoldMember.getDateOfBirth() != null) {
						
						member.setDob(nullCheckedValue(getFormatedDate(houseHoldMember.getDateOfBirth()).toString()));
					}
					if(!newMember) {
					if(enrollmentType.equals(HEALTH) && enrollmentId != null && enrollmentId != 0){
						
						member.setExistingMedicalEnrollmentID(nullCheckedValue(String.valueOf(enrollmentId)));
						
					}else{
						
						member.setExistingSADPEnrollmentID(nullCheckedValue(String.valueOf(enrollmentId)));
					}
					}
					member.setFinancialHardshipExemption(isExemptHousehold(currentApplication) ? YesNoVal.Y : YesNoVal.N);
					member.setFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));
					
					
					if ("male".equalsIgnoreCase(houseHoldMember.getGender())) {
						member.setGenderCode("M");
					} else if ("female".equalsIgnoreCase(houseHoldMember.getGender())) {
						member.setGenderCode("F");
					}
					
					for(BloodRelationship relationship: responsiblePerson.getBloodRelationship()){
						
						if(houseHoldMember.getPersonId().toString().equalsIgnoreCase(relationship.getIndividualPersonId())){
							
							Relationship ind71Relationship = factory.createAutoRenewalRequestHouseholdMembersMemberRelationship();
							String memberId = ssapIDToGuidMap.get(Long.valueOf(relationship.getRelatedPersonId()));
							
							if(memberId != null) {
								ind71Relationship.setMemberId(nullCheckedValue(memberId));
								ind71Relationship.setRelationshipCode(relationship.getRelation());
								member.getRelationship().add(ind71Relationship);
							}
							
							if(relationship.getRelatedPersonId().equalsIgnoreCase(primaryTaxFilerPersonId) && relationship.getIndividualPersonId().equalsIgnoreCase(houseHoldMember.getPersonId().toString())){
								member.setResponsiblePersonRelationship(nullCheckedValue(relationship.getRelation()));
							}
						}
					}
					
					if(houseHoldMember.getName() != null) {
						member.setLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						member.setSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getMailingAddressSameAsHomeAddressIndicator()) {
			         member.setMailingAddress1(nullCheckedValue(member.getHomeAddress1()));
			         member.setMailingAddress2(nullCheckedValue(member.getHomeAddress2()));
			         member.setMailingCity(nullCheckedValue(member.getHomeCity()));
			         member.setMailingState(nullCheckedValue(member.getHomeState()));
			         member.setMailingZip(nullCheckedValue(member.getHomeZip()));
					}
					else if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getMailingAddress() != null){
						
						member.setMailingAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getStreetAddress1()));
			         member.setMailingAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getStreetAddress2()));
			         member.setMailingCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getCity()));
			         member.setMailingState(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getState()));
			         member.setMailingZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getMailingAddress().getPostalCode()));
					}
					
					if ("CA".equalsIgnoreCase(stateCode)) {
						if(houseHoldMember.getMarriedIndicatorCode() != null) {
							member.setMaritalStatusCode(houseHoldMember.getMarriedIndicatorCode());
						}else {
							member.setMaritalStatusCode("R");
						}
					}else {
						if ("Yes".equalsIgnoreCase(ReferralUtil.converToYesNo(houseHoldMember.getMarriedIndicator()))) {
							member.setMaritalStatusCode("M");
						}else {
							member.setMaritalStatusCode("R");
						}
					}
					
					if(houseHoldMember.getCitizenshipImmigrationStatus()!=null) {
						if(houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null && houseHoldMember.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator()) {
							member.setCitizenshipStatusCode(US_CITIZEN);
						}else{
							member.setCitizenshipStatusCode(NON_US_CITIZEN);
						}
					}
			     
					if(houseHoldMember.getName() != null) {
						member.setMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getContactPreferences() != null 
							&& !!(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("Email")
									|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						member.setPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()) != null ? houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress() : houseHoldContact.getHouseHoldContactPreferredEmail());
					}
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getContactPreferences() != null 
							&& !!houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("TextMessage")) {
						member.setPreferredPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber() : houseHoldContact.getHouseHoldContactPrimaryPhone()));
					}
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getPhone() != null) {
			/*							if(houseHoldMember.getHouseholdContact().getContactPreferences() != null && nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod()) != null 
								&& !(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("EMAIL") 
										|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL"))) {
							member.setPreferredPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber() : houseHoldContact.getHouseHoldContactPrimaryPhone()));
						}	*/						
						member.setPrimaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : (nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber() : houseHoldContact.getHouseHoldContactPrimaryPhone()));
					}
					
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getOtherPhone() != null && nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						member.setSecondaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
			     
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getContactPreferences() != null) {
			         String preferredSpokenLanguage = houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
			         String languageSpokenCode = null;
			         if(StringUtils.isNotBlank(preferredSpokenLanguage)) {
			 			languageSpokenCode = LanguageEnum.getCode(preferredSpokenLanguage);								
			 		}
			         member.setSpokenLanguageCode(languageSpokenCode);
			         
			         String languageWrittenCode = null;
			         
						String preferredWrittenLanguage = houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
						if(StringUtils.isNotBlank(preferredWrittenLanguage)) {
							languageWrittenCode = LanguageEnum.getCode(preferredWrittenLanguage);								
			 		}
						member.setWrittenLanguageCode(languageWrittenCode);
					}
					
					if(houseHoldMember.getSocialSecurityCard() != null) {
						member.setSsn(nullCheckedValue(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber()));
					}
					
					
					if(houseHoldMember.getPersonId() != null && null != applicantMap.get(houseHoldMember.getPersonId().longValue()) ) {
						member.setTobacco("Y".equalsIgnoreCase(applicantMap.get(houseHoldMember.getPersonId().longValue()).getTobaccouser())?   YesNoVal.Y : YesNoVal.N);
					}
					
					if(houseHoldMember.getEthnicityAndRace() != null) {
			          //List<Ethnicity> ethnicities = houseHoldMember.getEthnicityAndRace().getEthnicity();
			          List<Race> races = houseHoldMember.getEthnicityAndRace().getRace();
			          
			          if(races != null) {
				            for (Race race : races) {
				            	com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Race ind71Race = factory.createAutoRenewalRequestHouseholdMembersMemberRace();
				            	/*if(race.getCode().equalsIgnoreCase("0000-0")){
									continue;
								}*/
								if(race.getCode().equalsIgnoreCase("2131-1")){
									ind71Race.setDescription(race.getOtherLabel());
								}
								ind71Race.setRaceEthnicityCode(race.getCode());
								member.getRace().add(ind71Race);
				            }
			          }
			          /*if(ethnicities != null) {
				            for (Ethnicity race : ethnicities) {
				            	com.getinsured.hix.webservice.plandisplay.autorenewal.AutoRenewalRequest.Household.Members.Member.Race ind71Race = factory.createAutoRenewalRequestHouseholdMembersMemberRace();
				            	if(race.getCode().equalsIgnoreCase("0000-0")){
									continue;
								}
								if(race.getCode().equalsIgnoreCase("2131-1")){
									ind71Race.setDescription(race.getOtherLabel());
								}
								ind71Race.setRaceEthnicityCode(race.getCode());
								member.getRace().add(ind71Race);
				            }
			          }*/
			      }
					if(getAgeFromDob(member.getDob(),format.parse(coverageDate)) >= HARDSHIP_EXEMPTION_AGE){
						isEveryoneUnder30 = false;
					}
			      
			     members.getMember().add(member);
			
				}
			}
			}
			     
			if(isEveryoneUnder30){
			LOGGER.debug("Not hardship exempt. But everyone under 30");
			for(int i=0;i<members.getMember().size();i++){
				members.getMember().get(i).setCatastrophicEligible(YesNoVal.Y);
			}
			
			}
			
			return members;
			}
			
			
			private String getPTFPersonId(SingleStreamlinedApplication singleStreamlinedApplication) {
			String primaryTaxFilerPersonId="1";// Default behaviour
			
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
			for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
				if(houseHoldMember != null && houseHoldMember.isPrimaryTaxFiler()){
					primaryTaxFilerPersonId = String.valueOf(houseHoldMember.getPersonId());
				}
			}
			}
			return primaryTaxFilerPersonId;
			}
			
			public int getAgeFromDob(String dob, Date coverageStartDate){
			
			if(StringUtils.isBlank(dob)){
			return 0;
			}
			
			//Implementation change as per Joda Time
			LocalDate now = new LocalDate(coverageStartDate.getTime());
			
			Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
			LocalDate birthdate = new LocalDate(dobDate.getTime());
				
			Years years = Years.yearsBetween(birthdate, now);
			int age =  years.getYears();
			
			return age;
			
			
			}
			
			private boolean isExemptHousehold(SsapApplication currentApplication) {
			return currentApplication.getExemptHousehold()!=null 
				&& currentApplication.getExemptHousehold().trim().length()>0 
				&& currentApplication.getExemptHousehold().equalsIgnoreCase(Y);
			}
			
			private ResponsiblePerson getResponsiblePerson(SingleStreamlinedApplication singleStreamlinedApplication) {
			ResponsiblePerson responsiblePerson = factory.createAutoRenewalRequestHouseholdResponsiblePerson();
			
			Integer primaryTaxFilerPersonId = singleStreamlinedApplication.getPrimaryTaxFilerPersonId();
			
			if(primaryTaxFilerPersonId == null || primaryTaxFilerPersonId == 0) {
			primaryTaxFilerPersonId = 1;
			}
			
			for(TaxHousehold taxHousehold : singleStreamlinedApplication.getTaxHousehold()) {
			for(HouseholdMember houseHoldMember : taxHousehold.getHouseholdMember()) {
				if(houseHoldMember.getPersonId() == primaryTaxFilerPersonId) {
					if(houseHoldMember.getName() != null) {
						responsiblePerson.setResponsiblePersonFirstName(nullCheckedValue(houseHoldMember.getName().getFirstName()));
						responsiblePerson.setResponsiblePersonLastName(nullCheckedValue(houseHoldMember.getName().getLastName()));
						responsiblePerson.setResponsiblePersonMiddleName(nullCheckedValue(houseHoldMember.getName().getMiddleName()));
						responsiblePerson.setResponsiblePersonSuffix(nullCheckedValue(houseHoldMember.getName().getSuffix()));
					}
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getHomeAddress() != null) { 
						responsiblePerson.setResponsiblePersonHomeAddress1(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress1()));
						responsiblePerson.setResponsiblePersonHomeAddress2(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getStreetAddress2()));
						responsiblePerson.setResponsiblePersonHomeCity(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getCity()));
						responsiblePerson.setResponsiblePersonHomeState(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getState()));
						responsiblePerson.setResponsiblePersonHomeZip(nullCheckedValue(houseHoldMember.getHouseholdContact().getHomeAddress().getPostalCode()));
					}
					responsiblePerson.setResponsiblePersonId(nullCheckedValue(houseHoldMember.getApplicantGuid()));
			
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getContactPreferences() != null 
							&& !!(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL")
									|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("POSTAL MAIL"))) {
						responsiblePerson.setResponsiblePersonPreferredEmail(nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getEmailAddress()));
					}
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getPhone() != null) {
						
			/*							if(houseHoldMember.getHouseholdContact().getContactPreferences() != null && nullCheckedValue(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod()) != null 
								&& !(houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("EMAIL") 
										|| houseHoldMember.getHouseholdContact().getContactPreferences().getPreferredContactMethod().equalsIgnoreCase("MAIL"))) {
							responsiblePerson.setResponsiblePersonPreferredPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
						}	*/						
						responsiblePerson.setResponsiblePersonPrimaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null ? houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber() : nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
					if(houseHoldMember.getHouseholdContact() != null && houseHoldMember.getHouseholdContact().getOtherPhone() != null && nullCheckedValue(houseHoldMember.getHouseholdContact().getPhone().getPhoneNumber()) != null) {
						responsiblePerson.setResponsiblePersonSecondaryPhone(nullCheckedValue(houseHoldMember.getHouseholdContact().getOtherPhone().getPhoneNumber()));
					}
					if(houseHoldMember.getSocialSecurityCard() != null) {
						responsiblePerson.setResponsiblePersonSsn(nullCheckedValue(houseHoldMember.getSocialSecurityCard().getSocialSecurityNumber()));
					}
					
					responsiblePerson.setResponsiblePersonType("QD");
				}
					
			}
			}
			
			return responsiblePerson;
			}
			
			
	}

	public SsapJsonBuilder getSsapJsonBuilder() {
		return ssapJsonBuilder;
	}

	public void setSsapJsonBuilder(SsapJsonBuilder ssapJsonBuilder) {
		this.ssapJsonBuilder = ssapJsonBuilder;
	}

	public RenewalApplicationRepository getRenewalApplicationRepository() {
		return renewalApplicationRepository;
	}

	public void setRenewalApplicationRepository(RenewalApplicationRepository renewalApplicationRepository) {
		this.renewalApplicationRepository = renewalApplicationRepository;
	}

	public RenewalApplicantRepository getRenewalApplicantRepository() {
		return renewalApplicantRepository;
	}

	public void setRenewalApplicantRepository(RenewalApplicantRepository renewalApplicantRepository) {
		this.renewalApplicantRepository = renewalApplicantRepository;
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	public SsapApplicantRepository getSsapApplicantRepository() {
		return ssapApplicantRepository;
	}

	public void setSsapApplicantRepository(SsapApplicantRepository ssapApplicantRepository) {
		this.ssapApplicantRepository = ssapApplicantRepository;
	}

	public WebServiceTemplate getInd71WebServiceTemplate() {
		return ind71WebServiceTemplate;
	}

	public void setInd71WebServiceTemplate(WebServiceTemplate ind71WebServiceTemplate) {
		this.ind71WebServiceTemplate = ind71WebServiceTemplate;
	}

	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}
	
	public CustomGroupingUtil getCustomGroupingUtil() {
		return customGroupingUtil;
	}

	public void setCustomGroupingUtil(CustomGroupingUtil customGroupingUtil) {
		this.customGroupingUtil = customGroupingUtil;
	}
	
	public LookupService getLookupService() {
		return lookupService;
	}

	public void setLookupService(LookupService lookupService) {
		this.lookupService = lookupService;
	}
	
	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}
	
	public HouseholdMember getApplicantJson(List<HouseholdMember> members, long personId) {
		HouseholdMember applicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) personId) == 0) {
				applicantJson = householdMember;
				break;
			}
		}
		return applicantJson;
	}
	
	private Integer getReasonCode(String lookupValueCode)
	{
		Integer reasonCode =  lookupService.getlookupValueIdByTypeANDLookupValueCode("RENEWALS",lookupValueCode );
		return reasonCode;
	}

	public RenewalEligibilityProgramRepository getRenewalEligibilityProgramRepository() {
		return renewalEligibilityProgramRepository;
	}

	public void setRenewalEligibilityProgramRepository(
			RenewalEligibilityProgramRepository renewalEligibilityProgramRepository) {
		this.renewalEligibilityProgramRepository = renewalEligibilityProgramRepository;
	}
}
