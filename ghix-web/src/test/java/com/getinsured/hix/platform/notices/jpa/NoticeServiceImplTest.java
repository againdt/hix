package com.getinsured.hix.platform.notices.jpa;

import static org.junit.Assert.assertNotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.timeshift.TimeShifterUtil;

public class NoticeServiceImplTest extends BaseTest{

	@Autowired NoticeService noticeService;
	@Autowired IUserRepository iUserRepository;

	@Test(expected=NoticeServiceException.class)
	public void testCreatePDFInvalidTemplateName() throws NoticeServiceException{

		String noticeTemplateName = "TestNoticeTemplate2";
		AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
		noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), "ekramtest", "somepdf65.pdf", user);

	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreatePDFBlankTemplateName() throws NoticeServiceException{

		String noticeTemplateName = "";
		AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
		noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), "ekramtest", "somepdf65.pdf", user);

	}


	@Test(expected=IllegalArgumentException.class)
	public void testCreatePDFInvalidEcmFilePath() throws NoticeServiceException {

		String noticeTemplateName = "TestNoticeTemplate";
		AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
		noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), null, "somepdf65.pdf", user);

	}


	@Test(expected=IllegalArgumentException.class)
	public void testCreatePDFInvalidEcmFileName() throws NoticeServiceException {

		String noticeTemplateName = "TestNoticeTemplate";
		AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
		noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), "ekramtest", "", user);

	}

	@Test(expected=IllegalArgumentException.class)
	public void testCreatePDFInvalidUser() throws NoticeServiceException {

		String noticeTemplateName = "TestNoticeTemplate";
		AccountUser user = new AccountUser();
		noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), "ekramtest", "somepdf65.pdf", user);

	}


	@Test
	public void testCreatePDF() throws NoticeServiceException {

		String noticeTemplateName = "TestNoticeTemplate";
		String fileName = "testNotification_" + TimeShifterUtil.currentTimeMillis() + ".pdf";

		AccountUser user = iUserRepository.findByUserName("exadmin@ghix.com");
		/*
		RequestObject requestObject = new RequestObject();
		requestObject.setDataMap(getReplaceableObjectData());
		requestObject.setFormat("PDF");
		requestObject.setLangId("US_en");
		requestObject.setTemplateName(noticeTemplateName);

		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		String xml=xstream.toXML(requestObject);

		System.out.println(xml);
		 */


		Notice notice = noticeService.createNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(), "ekramtest", fileName, user);

		assertNotNull(notice);
		assertNotNull(notice.getId());
		assertNotNull(notice.getEcmId());
	}

	public Map<String, Object> getReplaceableObjectData()
	{

		List<Chapter> chaptersList = new ArrayList<Chapter>();

		Chapter chapter1 = new Chapter();
		chapter1.setPage("10"); chapter1.setTitle("Chapter 1");

		Chapter chapter2 = new Chapter();
		chapter2.setPage("20"); chapter2.setTitle("Chapter 2");

		Chapter chapter3 = new Chapter();
		chapter3.setPage("30"); chapter3.setTitle("Chapter 3");

		chaptersList.add(chapter1); chaptersList.add(chapter2);chaptersList.add(chapter3);

		List<Book> bookList = new ArrayList<Book>();
		Book book = new Book();
		book.setAuthor("Kuldeep");
		book.setPages(350);
		book.setPublications("Himalaya Publications");
		book.setTitle("Java Developer");
		book.setPrice(350.50f);
		book.setChapters(chaptersList);
		bookList.add(book);

		book = new Book();
		book.setAuthor("Ram");
		book.setPages(400);
		book.setPublications("Future Group Publications");
		book.setTitle("Struts Developer");
		book.setPrice(500.95f);
		book.setChapters(new ArrayList<Chapter>());
		bookList.add(book);

		book = new Book();
		book.setAuthor("Laxman");
		book.setPages(550);
		book.setPublications(" Apache Publications");
		book.setTitle("Tomcat Server");
		book.setPrice(399.00f);
		book.setChapters(chaptersList);
		bookList.add(book);

		EmployerInvoice employer = new EmployerInvoice();
		employer.setAddressLine1("54321 Main St. Apt B");
		employer.setAddressLine2("Palo Alto, CA 95008 USA");
		employer.setAdjustments(0.00f);
		employer.setAmountDueFromLastInvoice(29.00f);
		employer.setCaseId(0000001);
		employer.setEmployerName("Bradley Cooper");
		employer.setExchangeFees(456.78f);
		employer.setInvoiceNumber("INV-2013-01");
		employer.setPaymentDate(new java.util.Date());
		employer.setPeriodCovered("11/01/12 - 12/01/12");
		employer.setPremiumThisPeriod(650.00f);
		employer.setStatementDate(new java.util.Date());
		employer.setTotalPayentReceived(5000000.00f);

		String signature = "<img src="+'"'+"{host}/resources/images/signature.jpg"+'"'+ " alt="+'"'+'"'+ " />";
		SimpleDateFormat simpleFormatter = new SimpleDateFormat("MMMMM DD, yyyy");

		Map<String, Object> tokens = new HashMap<String, Object>();
		tokens.put("bookList", bookList);
		tokens.put("employerInvoice", employer);
		tokens.put("SystemDate", simpleFormatter.format(new java.util.Date()));
		tokens.put("EmployeeName", "Kuldeep Sharma");
		tokens.put("EmployerName", "Xoriant Solutions");
		tokens.put("EnrolledPlanName", "Ghix_Health_01");
		tokens.put("IssuerName", "United Health Insurance");
		tokens.put("TerminationDate", "02/01/2013");
		tokens.put("ExchangeSignature", signature);
		tokens.put("ExchangePhone", "1800554400");
		tokens.put("EmployerPhone", "022-30437208");

		return tokens;
	}

}
