package com.getinsured.eligibility.ssap.pdf.generation;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import com.getinsured.eligibility.notification.dto.SsapApplicantEligibilityDTO;

public class ReviewSummaryDTO {
	
	private String email;
	private String birthDate;
	private String firstName;
	private String middleName;
	private String lastName;
	private String suffixName;
	private String fullName;
	
	private String homeAddress;
	private String MailingAddress;
	private List<HouseholdMembersDTO> householdMembersDTO;
	
	private String Phone;
	private String PhoneNumber;
	private String otherPhone;
	private String preferredSpokenLanguage;
	private String preferredWrittenLanguage;
	private String preferredContactMethod;
	private String secondPhoneNumber;
	
	private BigDecimal cmrHouseholdId;
	private String caseNumber;
	private long primaryKey;
	
	private String applicationId;
	
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthdate) {
		this.birthDate = birthdate;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getSuffixName() {
		return suffixName;
	}
	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}
	public String getHomeAddress() {
		return homeAddress;
	}
	public void setHomeAddress(String homeAddress) {
		this.homeAddress = homeAddress;
	}
	public String getMailingAddress() {
		return MailingAddress;
	}
	public void setMailingAddress(String mailingAddress) {
		MailingAddress = mailingAddress;
	}
	
	public String getPreferrendSpokenLanguage() {
		return preferredSpokenLanguage;
	}
	public void setPreferrendSpokenLanguage(String preferrendSpokenLanguage) {
		this.preferredSpokenLanguage = preferrendSpokenLanguage;
	}
	public String getSecondPhoneNumber() {
		return secondPhoneNumber;
	}
	public void setSecondPhoneNumber(String secondPhoneNumber) {
		this.secondPhoneNumber = secondPhoneNumber;
	}
	public String getPreferredSpokenLanguage() {
		return preferredSpokenLanguage;
	}
	public void setPreferredSpokenLanguage(String preferredSpokenLanguage) {
		this.preferredSpokenLanguage = preferredSpokenLanguage;
	}
	public String getPreferredWrittenLanguage() {
		return preferredWrittenLanguage;
	}
	public void setPreferredWrittenLanguage(String preferredWrittenLanguage) {
		this.preferredWrittenLanguage = preferredWrittenLanguage;
	}
	public String getPreferredContactMethod() {
		return preferredContactMethod;
	}
	public void setPreferredContactMethod(String preferredContactMethod) {
		this.preferredContactMethod = preferredContactMethod;
	}
	public BigDecimal getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(BigDecimal cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public long getPrimaryKey() {
		return primaryKey;
	}
	public void setPrimaryKey(long primaryKey) {
		this.primaryKey = primaryKey;
	}
	public String getPhoneNumber() {
		return PhoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		PhoneNumber = phoneNumber;
	}
	public String getOtherPhone() {
		return otherPhone;
	}
	public void setOtherPhone(String otherPhone) {
		this.otherPhone = otherPhone;
	}
	public List<HouseholdMembersDTO> getHouseholdMembersDTO() {
		return householdMembersDTO;
	}
	public void setHouseholdMembersDTO(List<HouseholdMembersDTO> householdMembersDTO) {
		this.householdMembersDTO = householdMembersDTO;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getApplicationId() {
		return applicationId;
	}
	public void setApplicationId(String applicationId) {
		this.applicationId = applicationId;
	}
	public String getPhone() {
		return Phone;
	}
	public void setPhone(String phone) {
		Phone = phone;
	}
	
	
	
	
	
	
}
