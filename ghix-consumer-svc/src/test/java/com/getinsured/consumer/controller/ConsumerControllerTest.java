package com.getinsured.consumer.controller;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.hix.model.consumer.FFMHouseholdResponse;
import com.getinsured.hix.model.consumer.FFMMemberResponse;
import com.getinsured.hix.platform.util.GhixEndPoints;


public class ConsumerControllerTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	
	
	@Test
	public void testFFMResponseService(){
		FFMHouseholdResponse houseHoldResponse = new FFMHouseholdResponse();
		houseHoldResponse.setGiHouseholdId("234234-sadfasdf");
		houseHoldResponse.setPhoneNumber("8009000000");
		
		FFMMemberResponse member1 = new FFMMemberResponse();
		member1.setFirstName("hola");
		member1.setLastName("mola");
		
		FFMMemberResponse member2 = new FFMMemberResponse();
		member2.setFirstName("robert");
		member2.setLastName("prat");
		houseHoldResponse.addMember(member1);
		houseHoldResponse.addMember(member2);
		
		RestTemplate restTemplate = new RestTemplate() ;
		
		ObjectMapper mapper = new ObjectMapper();
		try {
			String query = mapper.writeValueAsString(houseHoldResponse);
			System.out.println(query);
			String restResponse = restTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.SAVE_FFM_RESPONSE, query, String.class);
			System.out.println(restResponse);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}

}
