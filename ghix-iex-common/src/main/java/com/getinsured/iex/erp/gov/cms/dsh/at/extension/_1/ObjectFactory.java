//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.dsh.at.extension._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ResponseMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.PhysicalHouseholdType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.getinsured.iex.erp.gov.cms.dsh.at.extension._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TransferHeader_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "TransferHeader");
    private final static QName _TransferActivityReferralQuantity_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "TransferActivityReferralQuantity");
    private final static QName _TransferActivity_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "TransferActivity");
    private final static QName _RecipientTransferActivityStateCode_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "RecipientTransferActivityStateCode");
    private final static QName _RecipientTransferActivityCode_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "RecipientTransferActivityCode");
    private final static QName _PhysicalHousehold_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "PhysicalHousehold");
    private final static QName _ResponseMetadata_QNAME = new QName("http://at.dsh.cms.gov/extension/1.0", "ResponseMetadata");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.getinsured.iex.erp.gov.cms.dsh.at.extension._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TransferActivityCodeType }
     * 
     */
    public TransferActivityCodeType createTransferActivityCodeType() {
        return new TransferActivityCodeType();
    }

    /**
     * Create an instance of {@link TransferActivityType }
     * 
     */
    public TransferActivityType createTransferActivityType() {
        return new TransferActivityType();
    }

    /**
     * Create an instance of {@link TransferHeaderType }
     * 
     */
    public TransferHeaderType createTransferHeaderType() {
        return new TransferHeaderType();
    }

    /**
     * Create an instance of {@link AccountTransferResponsePayloadType }
     * 
     */
    public AccountTransferResponsePayloadType createAccountTransferResponsePayloadType() {
        return new AccountTransferResponsePayloadType();
    }

    /**
     * Create an instance of {@link AccountTransferRequestPayloadType }
     * 
     */
    public AccountTransferRequestPayloadType createAccountTransferRequestPayloadType() {
        return new AccountTransferRequestPayloadType();
    }

    /**
     * Create an instance of {@link AtVersionCodeType }
     * 
     */
    public AtVersionCodeType createAtVersionCodeType() {
        return new AtVersionCodeType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferHeaderType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "TransferHeader")
    public JAXBElement<TransferHeaderType> createTransferHeader(TransferHeaderType value) {
        return new JAXBElement<TransferHeaderType>(_TransferHeader_QNAME, TransferHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuantityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "TransferActivityReferralQuantity")
    public JAXBElement<QuantityType> createTransferActivityReferralQuantity(QuantityType value) {
        return new JAXBElement<QuantityType>(_TransferActivityReferralQuantity_QNAME, QuantityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferActivityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "TransferActivity")
    public JAXBElement<TransferActivityType> createTransferActivity(TransferActivityType value) {
        return new JAXBElement<TransferActivityType>(_TransferActivity_QNAME, TransferActivityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link USStateCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "RecipientTransferActivityStateCode")
    public JAXBElement<USStateCodeType> createRecipientTransferActivityStateCode(USStateCodeType value) {
        return new JAXBElement<USStateCodeType>(_RecipientTransferActivityStateCode_QNAME, USStateCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TransferActivityCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "RecipientTransferActivityCode")
    public JAXBElement<TransferActivityCodeType> createRecipientTransferActivityCode(TransferActivityCodeType value) {
        return new JAXBElement<TransferActivityCodeType>(_RecipientTransferActivityCode_QNAME, TransferActivityCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PhysicalHouseholdType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "PhysicalHousehold")
    public JAXBElement<PhysicalHouseholdType> createPhysicalHousehold(PhysicalHouseholdType value) {
        return new JAXBElement<PhysicalHouseholdType>(_PhysicalHousehold_QNAME, PhysicalHouseholdType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://at.dsh.cms.gov/extension/1.0", name = "ResponseMetadata")
    public JAXBElement<ResponseMetadataType> createResponseMetadata(ResponseMetadataType value) {
        return new JAXBElement<ResponseMetadataType>(_ResponseMetadata_QNAME, ResponseMetadataType.class, null, value);
    }

}
