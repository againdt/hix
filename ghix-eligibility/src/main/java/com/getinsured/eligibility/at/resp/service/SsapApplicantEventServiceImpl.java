package com.getinsured.eligibility.at.resp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;

@Service("ssapApplicantEventService")
public class SsapApplicantEventServiceImpl implements SsapApplicantEventService {

	@Autowired private SsapApplicantEventRepository ssapApplicantEventRepository;

	@Override
	public SsapApplicantEvent createDoBApplicantEvent() {
		SsapApplicantEvent applicantEvent = new SsapApplicantEvent();

		return ssapApplicantEventRepository.save(applicantEvent);
	}

	@Override
	public SsapApplicantEvent createApplicantEvent() {
		SsapApplicantEvent applicantEvent = new SsapApplicantEvent();

		return ssapApplicantEventRepository.save(applicantEvent);
	}

}
