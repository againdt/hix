package com.getinsured.hix.shop.web;

import java.util.Collections;
import java.util.List;

import junit.framework.Assert;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.flash;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.test.GhixBaseMvcTest;


public class EmployeeControllerTest extends GhixBaseMvcTest {
	//@Autowired EmployeeController employeeController;

	@Autowired private UserService userService;     
    private MockMvc mockMvc;
    private MockHttpSession session = new MockHttpSession();
     
    @Autowired
    private WebApplicationContext webApplicationContext;
    
    int employeeid;
    
    @Before
    public void mySetUp() {
        // setup mock MVC
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .build();
        List<Integer> moduleUserIdList = userService.getModuleIdsByModuleName(ModuleUserService.EMPLOYEE_MODULE);
        Collections.sort(moduleUserIdList);
        
        employeeid = moduleUserIdList.get(moduleUserIdList.size()-1);        
        
    }          
     
    @Test
    public void reActivationEmail() throws Exception {
        MvcResult result = mockMvc.perform(get("/shop/employer/manage/employeeinfo/generatereactivationlink/"+employeeid)
                .param("id", employeeid+"")
                .session(session))
                .andExpect(status().isMovedTemporarily())
                .andExpect(redirectedUrl("/shop/employer/manage/list"))
                .andExpect(flash().attribute("generatereactivationlinkInfo", "Account ReActivation link sent to the employee."))
                .andReturn();
        String content = result.getResponse().getContentAsString();
        System.out.println(content);
    }

}