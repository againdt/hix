import React, { Fragment } from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';
import { Field } from 'redux-form';

import { TextField } from '../components/form';
import phoneNumberUtils from '../utils/phoneNumberUtils';

const PhoneNumber = () => (
  <Fragment>
    <div className="ds-l-row">
      <div className="ds-l-col ds-u-text-align--left">
        <p className="gutter10">
          <FormattedMessage
            id="broker.participation.phone.info"
            defaultMessage="Please provide a phone number below on which you would like to receive
            calls."
          />
        </p>
      </div>
    </div>
    <div className="ds-l-form-row control-group">
      <div className="ds-l-col--auto ds-u-text-align--left ds-u-padding-top--1">
        <label className="control-label" htmlFor="brokerConnectPhone">
          <FormattedMessage
            id="broker.participation.phone.label"
            defaultMessage="Broker Connect Phone Number"
          />
        </label>
      </div>
      <div className="ds-l-col--12 ds-l-sm-col--12 ds-l-md-col--9 ds-l-lg-col--6 ds-l-xl-col--5">
        <Field
          component={TextField}
          name="brokerConnectPhone"
          id="brokerConnectPhone"
          className={classNames('ds-c-field ds-c-field--medium')}
          normalize={phoneNumberUtils.normalizePhone}
        />
      </div>
    </div>
    <div className="ds-l-row">
      <div className="ds-l-col ds-u-text-align--left">
        <p className="gutter10">
          <FormattedMessage
            id="broker.participation.phone.disclosure"
            defaultMessage="Please note that this number will be called only during business hours that you will provide on the next screen."
          />
        </p>
      </div>
    </div>
  </Fragment>
);

export default PhoneNumber;
