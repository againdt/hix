<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<jsp:useBean id="now" class="java.util.Date" />

<style>
.agreement {
	background: #ebebeb
}

#eSignature {
	border-bottom: 1px solid #bbb;
}

.esign {
	background: #f2f2f2
}

.agree::-webkit-scrollbar {
	width: 10px;
	height: 10px;
}

.agree::-webkit-scrollbar-track {
	background: #fff;
}

.agree::-webkit-scrollbar-thumb {
	background: #bbb;
}
i.icon-print {
	color: inherit;
}
</style>
<script type="text/javascript">

    window.history.forward();
	function noBack() {
		window.history.forward();
		$("#submitButton").removeAttr("disabled");
	}
</script>
<body onload="noBack();"
    onpageshow="if (event.persisted) noBack();" onunload="" aria-hidden="true"></body>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript">
	function validateForm() {
		if ($("#frmesign").validate().form()) {
			if ($("input[name=terms]:checked").val() == undefined) {
				alert('<spring:message code='label.validateTerms' javaScriptEscape='true'/>');
				return false;
			} else if ($("input[name=agreement]:checked").val() == undefined && $("input[name=agreement]").length > 0) {
				alert('<spring:message code='label.validateAgreement' javaScriptEscape='true'/>');
				return false;
			} else {
				window.dataLayer.push({
			        'event': 'checkout',
			        'eventCategory': 'ecommerce',
			        'eventAction': 'Enroll Button Click',
			        'ecommerce': {
			            'checkout': {
			                'actionField': {'step': 4}
			            }
			        }
			    });
				$("#frmesign").submit();
				$("#submitButton").attr("disabled", true);
			}
		}
	}

</script>

<div class="gutter10">
<div class="row-fluid">
	<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != ''}">
			<p>
				<c:out value="${errorMsg}"></c:out>
			<p />
		</c:if>
		<br>
	</div>
</div>
<c:if test="${errorMsg == ''}">
	<div class="row-fluid">
		<%-- <div class="span3" id="sidebar">
			<div id="accordion2" class="accordion accordion-nav hide">
				<div class="accordion-group">
					<div class="accordion-heading">
						<a href="#collapseSideOne" data-parent="#accordion2"
							data-toggle="collapse" class="accordion-toggle"> <spring:message code="enroll.esignature.shopForCoverage"/> <i class="icon-plus pull-right lighter"></i>
						</a>
					</div>
					<div class="accordion-body in" id="collapseSideOne"
						style="height: auto;">
						<div class="accordion-inner">
							<ul class="nav nav-list">
								<li class="active"><a href="#"> <i class="icon-ok"></i>
										<spring:message code="enroll.esignature.HEALTH"/>
								</a></li>
								<li><a href="#"> <spring:message code="enroll.esignature.DENTAL"/></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="header">
				<h4><spring:message code="enroll.chkOut"/></h4>
			</div>
			<ul class="nav nav-list">
			   <li><a href="<c:url value="/plandisplay/showcart"/>"><i class="icon-ok"></i><spring:message code="enroll.cart"/></a></li>
			   <li class="active"><a href="#"><spring:message code="enroll.esignature.provideSignature"/></a></li>
			   <li><a href="#"><spring:message code="enroll.conf"/></a></li>
			</ul>
		</div> --%>
		<!--sidebar-->

		<form method="post" action="esignature" name="frmesign" id="frmesign">
			<input type="hidden" name="cart_id" id="cart_id" value="${cartId}" />
			<input type="hidden" name="fininfoId" id="fininfoId"
				value="${fininfoId}" />

			<div class="" id="rightpanel">
				<h3><spring:message code="enroll.eSignature"/></h3>
				<p><spring:message code="enroll.esignature.header"/></p>
				<h4 class="graydrkbg gutter10">
					<spring:message code="enroll.esignature.exchangeAgreement"/> <a data-original-title="" href="#"
						class="btn btn-small pull-right" onclick="printContent('user-agreement');"><i
						class="icon-print" title="<spring:message code="enroll.aria.print"/>"></i>&nbsp;<spring:message code="enroll.print"/> </a>
				</h4>
				<div class="agreement">
				<label for="user-agreement" class="hide"><spring:message code="enroll.esignature.exchangeAgreement"/></label>
					<div class="gutter10">
						<div class="control-group">
							<div class="controls">
								<c:choose>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<%--Different Text For Individual --%>
										<textarea rows="10" class="span12 agree" id="user-agreement" readonly="readonly"><spring:message code="enroll.esignature.individual.agreement"/></textarea>
									</c:when>
									<c:otherwise>
										<%--Different Text For Employees --%>
										<textarea rows="10" class="span12 agree" id="user-agreement" readonly="readonly"><spring:message code="enroll.esignature.employee.agreement"/></textarea>
									</c:otherwise>
								</c:choose>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<c:choose>
									<%-- Different Text For Individual --%>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<label class="checkbox label-Lowercase" for="ind-terms"> <input type="checkbox"
											name="terms" id="ind-terms" value="1"> <spring:message code="enroll.esignature.individual.agreeTermsofService"/>
										</label>
									</c:when>
									<%--Different Text For Employees --%>
									<c:otherwise>
										<label class="checkbox label-Lowercase" for="emp-terms"> <input type="checkbox"
											name="terms" id="emp-terms" value="1"> <spring:message code="enroll.esignature.employee.agreeTermsofService"/>
										</label>
									</c:otherwise>
								</c:choose>
								<%-- APTC Validation not applicable for Employees --%>
								<c:if test="${showFileTaxReturn==true && enrollment_type == enrollment_type_individual}">
									<label class="checkbox" for="agreement"> <input type="checkbox"
										name="agreement" id="agreement" value="1"><spring:message arguments="${taxYearObj.yearOfEffectiveDate},${taxYearObj.taxDueDate},${taxYearObj.taxDueYear}" code="enroll.esignature.aptcAgreement" htmlEscape="false"/>
									</label>
								</c:if>
							</div>
						</div>

						<c:if test="${enrErrorMsg != ''}">
							<div class="" style="font-weight: bold; font-size: medium; color: red;" >
								<p>
									<c:out value="${enrErrorMsg}"></c:out>
								<p/>
								<br>
							</div>
						</c:if>

						<div class="row-fluid">
							<c:if test="${showpin==true}">
								<br />
								<div class="control-group">
									<label for="pin_esig"><spring:message code="enroll.esignature.PIN"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a href="#"
										rel="tooltip" data-placement="top"
										data-original-title="<spring:message code="enroll.esignature.pinTooltip"/>"
										class="info"><i class="icon-question-sign"></i></a></label>
									<div class="controls">
										<input type="text" class="input-small" id="pin_esig"
											name="pin_esig" maxlength="4" value="${pin_esig }" />
									</div>
									<div class="" id="pin_esig_error"></div>

								</div>
							</c:if>
							<div class="control-group">
								<label for="applicant_esig"><spring:message code="enroll.esignature.signature"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
								<div class="controls">
									<input type="text" class="input-xxlarge" id="applicant_esig" name="applicant_esig" maxlength="50" value="${applicant_esig }" />
								</div>
								<div class="" id="applicant_esig_error"></div>
							</div>
						</div>
						<!-- gutter10 -->
					</div>
					<div class="control-group esign">
						<div class="gutter10">
							<span class="span3"><spring:message code="enroll.eSignature"/>:</span>
							<div id="eSignature" class="controls span6"></div>
							<span class="pull-left marginL20"><spring:message code="enroll.date"/>: &nbsp; </span>
							<fmt:formatDate value="${now}" type="both" pattern="MM/dd/yyyy" />
						</div>
						<!-- gutter10 -->
					</div>
				</div>
				<div class="form-actions">
					<a href="<c:url value="/plandisplay/showcart"/>" class="btn" aria-label="<spring:message code="enroll.aria.backtocart"/>"><spring:message code="enroll.back"/></a>
					<input type="button" name="submitButton" id="submitButton"
						onClick="javascript:validateForm();"
						class="btn btn-primary pull-right gtm_esign_enroll" value="<spring:message code="enroll.esignature.submitButtonValue"/>" aria-label="<spring:message code="enroll.esignature.submitButtonValue"/>" aria-label="Submit your Application"/>
				</div>
			</div>
			<!--rightpanel-->
		</form>
		<!-- End Form -->
	</div>
	<!--row-fluid-->
	</div>
</c:if>
<iframe name=print_frame width=400 height=400 frameborder=0 src=about:blank class="hide" aria-hidden="true"></iframe>
<script type="text/javascript">
	$(document).ready(function() {
		title = document.getElementsByTagName("title")[0].innerHTML;
	    document.title = title + ' - E-Sign Page';

	    // Data layer variable to be pushed at the end of shell.jsp
    	dl_pageCategory = 'Checkout';

		$('.info').tooltip();
	});
</script>

<script type="text/javascript">
	jQuery.validator.addMethod("alphabetsOnly", function(value, element) {
		return /^[a-zA-Z\s]+$/i.test(value);
	});

	var validator = $("#frmesign")
			.validate(
					{
						rules : {
							'applicant_esig' : {
								required : true,
								alphabetsOnly : true
							},
							'pin_esig' : {
								required : true,
								number : true,
								maxlength : 4,
								minlength : 4
							},
						},
						messages : {
							'pin_esig' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validatePinEsig' javaScriptEscape='true'/></span>"
							},
							'applicant_esig' : {
								required : "<span><em class='excl'>!</em><spring:message code='label.validateApplicantEsig' javaScriptEscape='true'/></span>",
								alphabetsOnly : "<span><em class='excl'>!</em><spring:message code="label.validateSignature"/></span>"
							},
						},
						errorClass : "error",
						errorPlacement : function(error, element) {
							var elementId = element.attr('id');
							error.appendTo($("#" + elementId + "_error"));
							$("#" + elementId + "_error")
									.attr('class', 'error');
						}
					});

function printContent(id){
	 var str ="<html>";
		 str=str+"<head></head>";
		 	str=str+"<body>";
		 		str=str+"<table align='center'>";
		 			str=str+"<tr><td align='center'>Exchange Agreement</td></tr>";
		 			str=str+"<tr><td>"+document.getElementById(id).innerHTML+"</td></tr>";
		 		str=str+"</table>";
		 	str=str+"</body>";
		 str=str+"</html>";

	 window.frames["print_frame"].document.body.innerHTML=str;
	 window.frames["print_frame"].window.focus();
	 window.frames["print_frame"].window.print();
	}
</script>
