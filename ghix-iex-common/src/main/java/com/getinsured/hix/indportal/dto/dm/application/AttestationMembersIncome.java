package com.getinsured.hix.indportal.dto.dm.application;

import java.util.List;
import java.util.Map;

public class AttestationMembersIncome {
	 private AnnualTaxIncome annualTaxIncome;
	 private Map<String,CurrentIncome> currentIncome;
	 private String incomeExceedFPL;
	 private List<DocumentedAnnualIncome> documentedAnnualIncome;
	 private Boolean seasonalWorkerIndicator;


	 // Getter Methods 

	 public AnnualTaxIncome getAnnualTaxIncome() {
	  return annualTaxIncome;
	 }


	 public String getIncomeExceedFPL() {
	  return incomeExceedFPL;
	 }

	 public Boolean getSeasonalWorkerIndicator() {
	  return seasonalWorkerIndicator;
	 }

	 // Setter Methods 

	 public void setAnnualTaxIncome(AnnualTaxIncome annualTaxIncome) {
	  this.annualTaxIncome = annualTaxIncome;
	 }


	 public void setIncomeExceedFPL(String incomeExceedFPL) {
	  this.incomeExceedFPL = incomeExceedFPL;
	 }

	 public void setSeasonalWorkerIndicator(Boolean seasonalWorkerIndicator) {
	  this.seasonalWorkerIndicator = seasonalWorkerIndicator;
	 }


	public Map<String, CurrentIncome> getCurrentIncome() {
		return currentIncome;
	}


	public void setCurrentIncome(Map<String, CurrentIncome> currentIncome) {
		this.currentIncome = currentIncome;
	}


	public List<DocumentedAnnualIncome> getDocumentedAnnualIncome() {
		return documentedAnnualIncome;
	}


	public void setDocumentedAnnualIncome(List<DocumentedAnnualIncome> documentedAnnualIncome) {
		this.documentedAnnualIncome = documentedAnnualIncome;
	}

	}
