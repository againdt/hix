package com.getinsured.hix.serff.repository.templates;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;


public interface ISerffPlanRepository extends JpaRepository<Plan, Integer>, RevisionRepository<Plan, Integer, Integer> {
    @Query("SELECT p FROM Plan p WHERE p.issuer.id=:issuerID and p.issuerPlanNumber=:issuerPlanNumber")
    Plan getRequiredPlan(@Param("issuerID") int issuerID,@Param("issuerPlanNumber") String issuerPlanNumber);

    @Query("SELECT p FROM Plan p WHERE p.issuerPlanNumber=:HIOSID")
    Plan getPlanByHIOSID(@Param("HIOSID") String hiosId);

    List<Plan> findByIssuerPlanNumberStartingWith(String planNumber);

    List<Plan> findByIssuerAndIsDeletedAndIssuerPlanNumberStartingWith(Issuer issuer,  String isDeleted, String planId);
    
    @Query("SELECT COUNT(p) FROM Plan p WHERE (LOWER(p.issuerPlanNumber) LIKE LOWER(:planId) || '%') AND p.isDeleted='N'")
    Long getPlanCount(@Param("planId") String planId);
    
    List<Plan> findByIsDeletedAndIssuerPlanNumberStartingWith(@Param("isDeleted")String isDeleted, @Param("issuerPlanNumber")String issuerPlanNumber);
}
