package com.getinsured.ssap.model.financial.nmec.exception;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Describes exception cause details for {@link NonEsiMecExceptionCause}
 *
 * @author Suresh Kancherla
 * @since 2019-06-12
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NonEsiMecExceptionCauseDetails {
  @JsonProperty("Exception Type")
  private String exceptionType;

  @JsonProperty("ExceptionMessage")
  private String exceptionMessage;

  @JsonProperty("ExceptionCause")
  private String exceptionCause;

  public String getExceptionType() {
    return exceptionType;
  }

  public void setExceptionType(String exceptionType) {
    this.exceptionType = exceptionType;
  }

  public String getExceptionMessage() {
    return exceptionMessage;
  }

  public void setExceptionMessage(String exceptionMessage) {
    this.exceptionMessage = exceptionMessage;
  }

  public String getExceptionCause() {
    return exceptionCause;
  }

  public void setExceptionCause(String exceptionCause) {
    this.exceptionCause = exceptionCause;
  }

  @Override
  public String toString() {
    return "IfsvExceptionCauseDetails{" +
        "exceptionType='" + exceptionType + '\'' +
        ", exceptionMessage='" + exceptionMessage + '\'' +
        ", exceptionCause='" + exceptionCause + '\'' +
        '}';
  }
}
