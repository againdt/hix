<%@ include file="../include.jsp" %>
<div id="appscr65" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page15.otherAddress" text="Other Addresses"/>
			</h4>
		</div>
		
		<form id="appscr65Form" class="form-horizontal" data-parsley-excluded=":hidden">
			<div class="header">
				<h4><spring:message code="ssap.page15.header"  /></h4>
			</div>
			<div class="gutter10">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page15.addressOtherThan" text="Do any of the people below live at an address other than "/>
					<strong id="houseHoldAddressDiv"></strong><spring:message code="ssap.page15.QM" text="?"/>
					<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">
					<small><spring:message code="ssap.page15.checkAllThatApply" text=" (check all that apply.)"/></small>
				</p>
				<div class="margin5-l" id="noOfOtherApplicant">
					<%-- <label>
						<input type="checkbox" name="householdContactAddress" value="None of these people" checked />
						<spring:message code="ssap.page15.noneOfThese" text="None of these people"/>
					</label> --%>
				</div>
			</div><!-- control-group ends -->
			<div id="HouseHoldAddressDiv">
			</div>
			</div>
		</form>
	</div>
</div>