package com.getinsured.hix.serff.service;

import static com.getinsured.hix.platform.util.GhixConstants.BENEFITS_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.FTP_UPLOAD_PLAN_PATH;
import static com.getinsured.hix.platform.util.GhixConstants.NETWORK_ID_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.PRESCRIPTION_DRUG_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.RATING_TABLE_FILENAME_SUFFIX;
import static com.getinsured.hix.platform.util.GhixConstants.SERVICE_AREA_FILENAME_SUFFIX;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.activation.DataHandler;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.JAXBIntrospector;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import org.apache.axiom.attachments.ByteArrayDataSource;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.account.repository.ITenantRepository;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferAttachmentResponseDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferInfoDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchRequestDTO;
import com.getinsured.hix.dto.planmgmt.microservice.SerffTransferSearchResponseDTO;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerPaymentInformation;
import com.getinsured.hix.model.Tenant;
import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATE;
import com.getinsured.hix.model.serff.SerffPlanMgmt.REQUEST_STATUS;
import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;
import com.getinsured.hix.planmgmt.repository.IIssuerRepository;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ftp.GHIXSFTPClient;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.serff.repository.IIssuerPaymentInfoRepo;
import com.getinsured.hix.serff.repository.ISerffDocument;
import com.getinsured.hix.serff.repository.ISerffPlanMgmt;
import com.getinsured.hix.serff.repository.IserffPlamMgmtBatch;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.google.gson.Gson;
import com.jcraft.jsch.SftpException;
import com.serff.planmanagementexchangeapi.common.model.pm.SerffStatus;
import com.serff.planmanagementexchangeapi.common.model.pm.SupportingDocuments;
import com.serff.planmanagementexchangeapi.exchange.model.pm.AccreditationInfo;
import com.serff.planmanagementexchangeapi.exchange.model.pm.CompanyInfo;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Insurer;
import com.serff.planmanagementexchangeapi.exchange.model.pm.InsurerAttachment;
import com.serff.planmanagementexchangeapi.exchange.model.pm.MarketType;
import com.serff.planmanagementexchangeapi.exchange.model.pm.Plan;
import com.serff.planmanagementexchangeapi.exchange.model.pm.PlanMetalLevel;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplate;
import com.serff.planmanagementexchangeapi.exchange.model.pm.TransferDataTemplates;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlan;
import com.serff.planmanagementexchangeapi.exchange.model.service.TransferPlanResponse;
import com.serff.template.admin.extension.InsuranceCompanyType;
import com.serff.template.admin.extension.IssuerType;
import com.serff.template.admin.extension.PayloadType;
import com.serff.template.admin.hix.core.OrganizationAugmentationType;
import com.serff.template.admin.hix.core.OrganizationType;
import com.serff.template.admin.hix.core.PersonType;
import com.serff.template.admin.hix.pm.InsurancePlanLevelCodeType;
import com.serff.template.admin.niem.core.AddressType;
import com.serff.template.admin.niem.core.ContactInformationType;
import com.serff.template.admin.niem.core.FullTelephoneNumberType;
import com.serff.template.admin.niem.core.IdentificationType;
import com.serff.template.admin.niem.core.LocationType;
import com.serff.template.admin.niem.core.PersonNameType;
import com.serff.template.admin.niem.core.ProperNameTextType;
import com.serff.template.admin.niem.core.StreetType;
import com.serff.template.admin.niem.core.StructuredAddressType;
import com.serff.template.admin.niem.core.TelephoneNumberType;
import com.serff.template.admin.niem.core.TextType;
import com.serff.template.admin.niem.proxy.AnyURI;
import com.serff.template.admin.niem.usps.states.USStateCodeSimpleType;
import com.serff.template.admin.niem.usps.states.USStateCodeType;

import reactor.util.CollectionUtils;

/**
 * Class is used to provide services of SERFF.
 * @author Bhavin Parmar
 */
@Service("serffService")
public class SerffServiceImpl implements SerffService {

	private static final Logger LOGGER = Logger.getLogger(SerffServiceImpl.class);
	private static final String DISK_SERFF_FILES_PATH = System.getProperty("GHIX_HOME") + File.separatorChar + "ghix-docs";
	private static final String INTERNAL_APP_ERROR = "Internal application error!";

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private ISerffPlanMgmt iSerffPlanMgmt;
	@Autowired private ISerffDocument iSerffDocument;
	@Autowired private IserffPlamMgmtBatch iSerffPlanMgmtBatch;
	@Autowired private IIssuerRepository iIssuerRepository;
	@Autowired private ITenantRepository iTenantRepository;
	@Autowired private GHIXSFTPClient ftpClient;	
	@Autowired private IIssuerPaymentInfoRepo issuerPaymentInfoRepo;
	@Autowired public GhixRestTemplate ghixRestTemplate;
	@Autowired private Gson platformGson;

	//Constants
	private static String SERVICE_URL;
	@Value("#{configProp.serffServiceURL}") 
	public void serServiceUrl(String ServiceUrl) {
		SERVICE_URL = ServiceUrl;
	}
	
	public static String getServiceUrl() {
		return SERVICE_URL;
	}
	
	private static final String SOAP_HEADER = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\">"+
								"<soapenv:Header/>"+
								"<soapenv:Body>";	
	
	private static final String SOAP_FOOTER = "</soapenv:Body></soapenv:Envelope>";
	private boolean isJobRunning = false;
	
	/**
	 * Method is used to get a record from SERFF_PLAN_MGMT table.
	 * @author Bhavin Parmar
	 * @param long, SerffPlanMgmt Id
	 */
	@Override
	public SerffPlanMgmt getSerffPlanMgmtById(long id) {
		
		if (id > 0) {
			return iSerffPlanMgmt.findOne(id);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Method is used to save a record in SERFF_PLAN_MGMT table.
	 * @author Bhavin Parmar
	 * @param SerffPlanMgmt, POJO object.
	 */
	@Override
	@Transactional
	public SerffPlanMgmt saveSerffPlanMgmt(final SerffPlanMgmt serffPlanMgmt) {

		if (null != serffPlanMgmt) {
			return iSerffPlanMgmt.save(serffPlanMgmt);
		}
		else {
			return null;
		}
	}
	
	/**
	 * Method is used to get a record from SERFF_DOCUMENT table.
	 * @author Bhavin Parmar
	 * @param long, SerffDocument Id
	 */
	@Override
	public SerffDocument getSerffDocumentById(long id) {
		
		if (id > 0) {
			return iSerffDocument.findOne(id);
		}
		else {
			return null;
		}
	}

	/**
	 * Method is used to save a record in SERFF_DOCUMENT table.
	 * @author Bhavin Parmar
	 * @param SerffDocument, POJO object.
	 */
	@Override
	@Transactional
	public SerffDocument saveSerffDocument(final SerffDocument serffDocument) {
		
		if (null != serffDocument) {
			return iSerffDocument.save(serffDocument);
		}
		else {
			return null;
		}
	}
	/**
	 * Method is used to fetch a record from SERFF_PLAN_MGMT_BATCH table.
	 * @author Vani Sharma
	 * @param id
	 */
	@Override
	public SerffPlanMgmtBatch getSerffPlanMgmtBatchById(long id) {
		if (id > 0) {
			return iSerffPlanMgmtBatch.findOne(id);
		}
		else {
			return null;
		}
	}
	/**
	 * Method is used to save a record in SERFF_PLAN_MGMT_BATCH table.
	 * @author Vani Sharma
	 * @param SerffPlanMgmtBatch, POJO object.
	 */
	@Override
	public SerffPlanMgmtBatch saveSerffPlanMgmtBatch(
			SerffPlanMgmtBatch serffPlanMgmtBatch) {
		
		if (null != serffPlanMgmtBatch) {
			return iSerffPlanMgmtBatch.save(serffPlanMgmtBatch);
		}else{
			return null;
		}
	}

	/**
	 * Method is used to process SERFF Batch.
	 * @return Status message of SERFF Batch.
	 */
	@Override
	public String processSerffPlanMgmtBatch() {
		LOGGER.info("processSerffPlanMgmtBatch() Begin");
		SerffPlanMgmtBatch waitingBatch = null;
		String message = "There is no job in waiting state to be processed !!";
		int failCount = 0, passCount = 0;
		if(!isJobRunning) {
			isJobRunning = true;
			try {
				while(isJobRunning) {
					//1. Search in SERFF_PLAN_MGMT_BATCH table and find ready to process job (First in First out), Don't pick deleted rows
					waitingBatch = iSerffPlanMgmtBatch.getPlanByBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.WAITING);
					boolean isBatchSucess = false;
					
					if(null != waitingBatch) {
						//Search in SERFF_PLAN_MGMT table and find idle SERFF system
						String serverList = GhixConstants.SERVICE_SERVER_LIST;
						
						if(StringUtils.isNotBlank(serverList)) {
							
							String[] servers = serverList.split(",");
							
							if (null != servers) {
								String server = getIdleServerFromList(servers);
								if (null != server) {
									isBatchSucess = executePlanLoadJob(waitingBatch, server);
									if(isBatchSucess) {
										passCount++;
									} else {
										failCount++;
									}
								} else {
									LOGGER.error("No Server is available for processing !!");
									message = "All Server are busy, please try gain later !!";
									break;
								}
							} else {
								LOGGER.error("Error with SERFF-Service Server list configuration !! ");
								message = "Error with SERFF-Service Server list configuration !! ";
								break;
							}
						} else {
							LOGGER.error("SERFF-Service Server list is not configured !!");
							message = "ERFF-Service Server list is not configured !! ";
							break;
						}
						
					} else {
						break;
					}
				}
				
				isJobRunning = false;
			} finally {
				isJobRunning = false;
				LOGGER.info("executeSerffBatch() End");
			}
		} else {
			message = "Job has been queued to run later...";
		}
		
		if((failCount + passCount) == 0) {
			return message;
		} else { 
			return "Total Job executed = " + (failCount + passCount) + " Passed:" + passCount + " Failed:" + failCount ;
		}
	}
	
	private boolean executePlanLoadJob(SerffPlanMgmtBatch waitingBatch, String server) {
		boolean jobCompleted = false;
		Date startTime = new TSDate();
		String folderPath = GhixConstants.getFTPUploadPlanPath() + waitingBatch.getIssuerId() 
				+ waitingBatch.getState() + waitingBatch.getId() + "/";
		LOGGER.debug("Start of Loading plans from location " + folderPath + " and server " + server + " Issuer HIOS ID : " + waitingBatch.getIssuerId());
		//Update row that job is picked up
		waitingBatch.setBatchStartTime(new java.sql.Date((startTime).getTime()));
		waitingBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS);
		waitingBatch = iSerffPlanMgmtBatch.save(waitingBatch);

		Issuer issuer = iIssuerRepository.getIssuerByHiosID(waitingBatch.getIssuerId());
		if (null != issuer) {
			Map<String, String> fileMap = getAllTemplatesFilelist(folderPath);
			LOGGER.debug("Found " + fileMap.size() + " templates for loading plans from location " + folderPath );
			Map<String, byte[]> uploadedTemplatesMap = getAllTemplatesForPlans(issuer, folderPath, fileMap, waitingBatch.getState() );
			//Form SOAP UI project
			LOGGER.debug("Creating payload for webservice call for Job ID " + waitingBatch.getId() );
			StringEntity soapRequest = createSOAPPayload(issuer, fileMap, uploadedTemplatesMap, waitingBatch.getExchangeType(), waitingBatch.getDefaultTenant());
			//post to available server
			LOGGER.debug("Making webservice call for Job ID " + waitingBatch.getId() );
			TransferPlanResponse serffResponse = postToSerffService(server, soapRequest);
			LOGGER.info("RESPONSE from webservice call for Job ID :- " + waitingBatch.getId() + " is " + (serffResponse == null ? "null: " : "Not Null"));
			
			if (null != serffResponse) {
				/*//String requestID = serffResponse.getRequestId();
				LOGGER.info("Serff Request ID:- " + requestID);
				
				SerffPlanMgmt serffPlanMgmt = null;
				if (StringUtils.isNotBlank(requestID)
						&& StringUtils.isNumeric(requestID)) {
					serffPlanMgmt = iSerffPlanMgmt.findOne(Long.parseLong(requestID));
				}
				
				if (null != serffPlanMgmt) {
					waitingBatch.setSerffReqId(serffPlanMgmt);
				}
				else {
					LOGGER.info("Serff Request ID is not present in SerffPlanMgmt table.");
				}*/
			
				//Update response status in SERFF_PLAN_MGMT_BATCH table depending on response
				if (null != serffResponse.getValidationErrors() 
						&& null != serffResponse.getValidationErrors().getErrors()
						&& !serffResponse.getValidationErrors().getErrors().isEmpty()) {
					LOGGER.debug("SERFF Batch has been failed !!");
				}
				else {
					jobCompleted = true;
					LOGGER.debug("SERFF Batch has been completed successfully !!");
				}
			}
			else {
				LOGGER.debug("SERFF Response is empty !!");
			}
		}
		else {
			LOGGER.debug("SERFF Batch has been failed due to Issuer is not available with HIOS ID " + waitingBatch.getIssuerId());
		}
		if (jobCompleted) {
			waitingBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.COMPLETED);
		}
		else {
			waitingBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.FAILED);
		}
		waitingBatch.setBatchEndTime(new TSDate());
		waitingBatch = iSerffPlanMgmtBatch.save(waitingBatch);
		return jobCompleted;
	}
	
	private Map<String, String> getAllTemplatesFilelist(String folderPath) {
		
		Map<String, String> filesMap = null;
		
		if (null != folderPath) {
			
			List<String> fileList = ftpClient.getFileNames(folderPath);
			filesMap = new HashMap<String, String>();
			String fileNameLowerCase = null;
			
			for (String fileName : fileList) {
				
				if(StringUtils.isNotBlank(fileName)) {
					
					fileNameLowerCase = fileName.toLowerCase();
					
					if (fileNameLowerCase.endsWith(NETWORK_ID_FILENAME_SUFFIX)) {
						filesMap.put(GhixConstants.DATA_NETWORK_ID, fileName);
					}
					else if (fileNameLowerCase.endsWith(BENEFITS_FILENAME_SUFFIX)) {
						filesMap.put(GhixConstants.DATA_BENEFITS, fileName);
					}
					else if (fileNameLowerCase.endsWith(RATING_TABLE_FILENAME_SUFFIX)) {
						filesMap.put(GhixConstants.DATA_RATING_TABLE, fileName);
					}
					else if (fileNameLowerCase.endsWith(SERVICE_AREA_FILENAME_SUFFIX)) {
						filesMap.put(GhixConstants.DATA_SERVICE_AREA, fileName);
					}
					else if (fileNameLowerCase.endsWith(PRESCRIPTION_DRUG_FILENAME_SUFFIX)) {
						filesMap.put(GhixConstants.DATA_PRESCRIPTION_DRUG, fileName);
					}
					else {
						LOGGER.info("Ignoring unknown file for plan load." + folderPath + fileName);
					}
				}
			}
		}
		return filesMap;
	}
	private Map<String, byte[]> getAllTemplatesForPlans(Issuer issuer, String folderPath, Map<String, String> filesMap, String issuerState) {
		Map<String, byte[]> templateMap = new HashMap<String, byte[]>();
		if(null != filesMap) {
			try {
				for (String fileType : filesMap.keySet()) {
					 try {
						byte[] contents = ftpClient.getFileContent(folderPath+filesMap.get(fileType));
						templateMap.put(fileType, contents);
					} catch (Exception e) {
						LOGGER.error("Error while reading file for plan load from FTP " + folderPath + filesMap.get(fileType), e);
						//TODO:RK throw exception here
					}
				}
				templateMap.put(GhixConstants.DATA_ADMIN_DATA, getIssuerTemplate(issuer, issuerState).getBytes());
				filesMap.put(GhixConstants.DATA_ADMIN_DATA, "admin.xml");
			} catch (GIException e1) {
				//TODO:RK throw exception here
				//e1.printStackTrace();
				LOGGER.error("getAllTempaletsForPlan() : "+e1.getErrorMsg());
			}
		}

		return templateMap;
	}

	private TransferPlanResponse postToSerffService(String server, StringEntity soapRequest) {
		
		TransferPlanResponse serffResponse = null;
		String serviceURL = "http://" + server +SERVICE_URL; 
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(serviceURL);
		httppost.setEntity(soapRequest);
		//LOGGER.info("Request :\n" + soapRequest);
		httppost.addHeader("Accept", "text/xml");
		HttpResponse response;
		
		try {
			response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			
			if (null != entity) {
				
				XMLInputFactory xif = XMLInputFactory.newFactory();
				XMLStreamReader xsr = xif.createXMLStreamReader(entity.getContent());
				
				if (null != xsr) {
			        xsr.nextTag();
			        
			        while(!xsr.getLocalName().equals(TransferPlanResponse.XML_ROOT_ELEMENT)) {
			            xsr.nextTag();
			        }
					
					Object objResponse = this.streamReaderToObject(xsr, TransferPlanResponse.class);
					if (null != objResponse
							&& objResponse instanceof TransferPlanResponse) {
						serffResponse = (TransferPlanResponse) objResponse;
					}
					xsr.close();
				}
			}
		}
		catch (ClientProtocolException e) {
			//TODO:RK throw exception here
			LOGGER.error(" : "+e.getMessage());
		}
		catch (IOException e) {
			//TODO:RK throw exception here
			LOGGER.error(" : "+e.getMessage());
		}
		catch (JAXBException e) {
			// TODO Auto-generated catch block
			LOGGER.error(" : "+e.getMessage());
		}
		catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			LOGGER.error(" : "+e.getMessage());
		}
		return serffResponse;
	}

	
	private String getIdleServerFromList(String[] servers) {
		String idleServer = null;
		for (String server : servers) {
			SerffPlanMgmt serffPlanRequest = iSerffPlanMgmt.getRecentPlanRequestByProcessIP(server);
			if(null != serffPlanRequest) {
				REQUEST_STATUS status = serffPlanRequest.getRequestStatus();
				REQUEST_STATE state = serffPlanRequest.getRequestState();
				if(state == REQUEST_STATE.E && status != REQUEST_STATUS.P) {
					//There is most recent record for job completed by this server so it is idle
					idleServer = server;
					break;
				} else {
					//There is most recent record for job not completed by this server 
					//check the time when this record was created, if more than 15 mins old, assume this server as idle
					Date startedAt = serffPlanRequest.getCreatedOn();
					Date cutOffTime = new TSDate(System.currentTimeMillis() - (15 * 60000));
					if(startedAt.before(cutOffTime)) {
						idleServer = server;
						break;
					}
				}
			} else {
				// There are no record for this server so it is idle
				idleServer = server;
				break;
			}
		}
		return idleServer;
	}

	
	private StringEntity createSOAPPayload(Issuer issuer, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, String exchangeType, String tenantCode) {
		StringEntity stringentity = null;
		String soapRequest = null;
		try {
			TransferPlan transferPlanRequest = createTransferPlan(issuer, fileMap, uploadedTemplatesMap, exchangeType, tenantCode);
			soapRequest = this.objectToXmlString(transferPlanRequest);
			soapRequest = soapRequest.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>", "");
			stringentity = new StringEntity(SOAP_HEADER+soapRequest+SOAP_FOOTER);
			stringentity.setChunked(true);
			stringentity.setContentType("text/xml");			
		} catch (IOException e) {
			//TODO:RK throw exception here
			LOGGER.error(" : "+e.getMessage());
		}
		return stringentity;
	}


	private TransferPlan createTransferPlan(Issuer issuer, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, String exchangeType, String tenantCode) {
		GregorianCalendar c = new GregorianCalendar();
		c.setTimeInMillis(TimeShifterUtil.currentTimeMillis());
		XMLGregorianCalendar currentTime = null;
		try {
			currentTime = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
		} catch (DatatypeConfigurationException e) {
			// TODO:RK Handle exception
			LOGGER.error(" : "+e.getMessage());
		}
		TransferPlan transferPlan = new TransferPlan();
		transferPlan.setRequestId("PHIX-" + TimeShifterUtil.currentTimeMillis());
		transferPlan.setTransferRequestTimestamp(currentTime);
		transferPlan.setPlan(createPlan(issuer, currentTime, fileMap, uploadedTemplatesMap, exchangeType, tenantCode));
		return transferPlan;
	}

	private Plan createPlan(Issuer issuer, XMLGregorianCalendar currentTime, Map<String, String> fileMap, Map<String, byte[]> uploadedTemplatesMap, String exchangeType, String tenantCode) {
		Plan plan = new Plan();
		plan.setStandardComponentId(issuer.getHiosIssuerId() + "CA0720001");
		//TODO:RK where do we get this from?
		plan.setPlanYear((short)2014);
		//TODO:RK - for PHIX it is always going to be QHP, no dental
		plan.setDental(false);
		plan.setSerffTrackingNumber("SERFF-" + currentTime.normalize());
		plan.setStateTrackingNumber("STATE-" + currentTime.normalize());
		plan.setHiosProductId(issuer.getHiosIssuerId());
		//FOR PHIX-This contains the exchange type filter (ON, OFF, ANY) as prefix
		plan.setPlanName(tenantCode + "-" + exchangeType + "-Exchange PHIX Plan");
		plan.setMarketType(MarketType.INDIVIDUAL);
		//TODO:RK where do we get this from?
		plan.setPlanLevel(PlanMetalLevel.BRONZE);
		plan.setSerffStatus(SerffStatus.SUBMITTED);
		plan.setSerffStatusChangeDate(currentTime);
		plan.setStatePlanStatus("StateStatus");
		plan.setStatePlanStatusChangeDate(currentTime);
		plan.setPlanDispositionStatus("DispositionStatus");
		plan.setPlanDispositionStatusChangeDate(currentTime);
		plan.setCertificationStatus(false);
		plan.setCertificationStatusChangeDate(currentTime);
		plan.setExchangeWorkflowStatus("Complete");
		plan.setExchangeWorkflowStatusChangeDate(currentTime);
		plan.setStateAbbreviation("XX");
		plan.setDateSubmitted(currentTime);
		plan.setInsurer(getInsurer(issuer));
		plan.setDataTemplates(getDataTemplates(issuer, currentTime, fileMap, uploadedTemplatesMap));
		plan.setSupportingDocuments(getSupportingDocuments(issuer, currentTime));
		return plan;
	}

	private SupportingDocuments getSupportingDocuments(Issuer issuer, XMLGregorianCalendar currentTime) {
		SupportingDocuments supDocs = new SupportingDocuments();
		return supDocs;
	}

	private TransferDataTemplates getDataTemplates(Issuer issuer, XMLGregorianCalendar currentTime, Map<String, String> fileMap,
			Map<String, byte[]> uploadedTemplatesMap) {
		TransferDataTemplates tfrDataTemplates = new TransferDataTemplates();
		List<TransferDataTemplate> dataTemplate = new ArrayList<TransferDataTemplate>();
		for (String fileType : fileMap.keySet()) {
			byte[] bytes = uploadedTemplatesMap.get(fileType);
			if(bytes != null) {
				ByteArrayDataSource ds = new ByteArrayDataSource(bytes);
				DataHandler templateData = new DataHandler(ds);
				TransferDataTemplate template = new TransferDataTemplate();
				template.setComment("PHIX plan template " + fileType);
				template.setDataTemplateId(Integer.toString(issuer.getId()));
				template.setDataTemplateType(fileType);
				template.setDateSubmitted(currentTime);
				template.setScheduleItemStatus("schedStat");
				template.setScheduleItemStatusChangeDate(currentTime);
				InsurerAttachment attachment = new InsurerAttachment();
				attachment.setFileName(fileMap.get(fileType));
				attachment.setDateLastModified(currentTime);
				attachment.setInsurerSuppliedData(templateData);
				template.setInsurerAttachment(attachment);
				dataTemplate.add(template);
			}
		}
		tfrDataTemplates.setDataTemplate(dataTemplate);
		return tfrDataTemplates;
	}

	private Insurer getInsurer(Issuer issuer) {
		Insurer insurer = new Insurer();
		insurer.setIssuerId(issuer.getId());
		insurer.setCompanyInfo(new CompanyInfo());
		insurer.setAccreditationInfo(new AccreditationInfo());
		return insurer;
	}

	
	/**
	 * Method is used to generate Admin Template XML from Issuer.
	 * @param issuerId in String format
	 * 
	 * @exception GIException
	 */
	public String getIssuerTemplate(Issuer issuer, String issuerState) throws GIException {
		
		LOGGER.info("getIssuerTemplate START");
		String issuerData = null;
		
		try {
		
			if (null != issuer) {
				
				PayloadType adminTemplate = new PayloadType();
				adminTemplate.setIssuer(new IssuerType());
				// Issuer Individual Market Contact--OrgChart
				PersonType pt = new PersonType();
				pt.setPersonName(new PersonNameType());
				adminTemplate.getIssuer().setIssuerIndividualMarketContact(pt);
				adminTemplate.getIssuer().setIssuerSmallGroupMarketContact(pt);
				adminTemplate.getIssuer().setIssuerIdentification(new IdentificationType());
				adminTemplate.getIssuer().getIssuerIdentification().setIdentificationID(new com.serff.template.admin.niem.proxy.String());
				adminTemplate.getIssuer().getIssuerIdentification().getIdentificationID().setValue(issuer.getHiosIssuerId());
				adminTemplate.getIssuer().setIssuerStateCode(new USStateCodeType());
				adminTemplate.getIssuer().setIssuerProposedExchangeMarketCoverageCode(new InsurancePlanLevelCodeType());
				if (null != issuer.getStateOfDomicile()) {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuer.getStateOfDomicile()));
				} else if (null != issuer.getState()) {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
				} else {
					adminTemplate.getIssuer().getIssuerStateCode().setValue(USStateCodeSimpleType.valueOf(issuerState));
				}
				
				adminTemplate.getIssuer().setIssuerSHOPCustomerService(getContactInformationType());
				adminTemplate.getIssuer().setIssuerIndividualMarketCustomerService(getContactInformationType());
				
				ContactInformationType contactInformationType = null;
				// Customer Service - Shop
				contactInformationType = adminTemplate.getIssuer().getIssuerSHOPCustomerService();
				if (null != issuer.getShopCustServicePhone()) {
					contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServicePhone());
					if (null != issuer.getShopCustServicePhoneExt()) {
						contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().setValue(issuer.getShopCustServicePhoneExt());
					}
				}
				
				if (null != issuer.getShopCustServiceTTY()) {
					contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServiceTTY());
				}
				
				if (null != issuer.getShopCustServiceTollFree()) {
					contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getShopCustServiceTollFree());
				}
				
				if (null != issuer.getShopSiteUrl()) {
					contactInformationType.getContactWebsiteURI().setValue(issuer.getShopSiteUrl());
				}
				
				// Customer Service - Individual Market
				contactInformationType = adminTemplate.getIssuer().getIssuerIndividualMarketCustomerService();
				if (null != issuer.getIndvCustServicePhone()) {
					contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServicePhone());
					if (null != issuer.getIndvCustServicePhoneExt()) {
						contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().getTelephoneSuffixID().setValue(issuer.getIndvCustServicePhoneExt());
					}
				}
				
				if (null != issuer.getIndvCustServiceTTY()) {
					contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServiceTTY());
				}
				
				if (null != issuer.getIndvCustServiceTollFree()) {
					contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().getTelephoneNumberFullID().setValue(issuer.getIndvCustServiceTollFree());
				}
				
				if (null != issuer.getIndvSiteUrl()) {
					contactInformationType.getContactWebsiteURI().setValue(issuer.getIndvSiteUrl());
				}
				
				OrganizationType organizationType = null;
				organizationType = new OrganizationType();
				adminTemplate.setInsuranceCompanyOrganization(organizationType);
				adminTemplate.getInsuranceCompanyOrganization().setOrganizationAugmentation(new OrganizationAugmentationType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().setOrganizationLegalName(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().setOrganizationLocation(new LocationType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().setLocationAddress(new AddressType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().setStructuredAddress(new StructuredAddressType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStreet(new StreetType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetFullText(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetExtensionText(new TextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationCityName(new ProperNameTextType());
				adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStateUSPostalServiceCode(new USStateCodeType());

				if (null != issuer.getCompanyLegalName()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().setValue(issuer.getCompanyLegalName());
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationAugmentation().getOrganizationLegalName().setValue(issuer.getName());
				}
				
				if (null != issuer.getCompanyAddressLine1()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getCompanyAddressLine1());
					if (null != issuer.getCompanyAddressLine2()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getCompanyAddressLine2());
					}
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getAddressLine1());
					if (null != issuer.getAddressLine2()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getAddressLine2());
					}
				}
				
				if (null != issuer.getCompanyCity()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCompanyCity());
					if (null != issuer.getCompanyState()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getCompanyState()));
					}
				} else {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCity());
					if (null != issuer.getState()) {
						adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
					}
				}
				
				if (null != issuer.getCompanyZip()) {
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationPostalCode(new com.serff.template.admin.niem.proxy.String());
					adminTemplate.getInsuranceCompanyOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().setValue(issuer.getCompanyZip());
				}
				
				adminTemplate.setInsuranceCompany(new InsuranceCompanyType());
				if (null != issuer.getFederalEin()) {
					adminTemplate.getInsuranceCompany().setInsuranceCompanyTINID(new TextType());
					adminTemplate.getInsuranceCompany().getInsuranceCompanyTINID().setValue(issuer.getFederalEin());
				}
				
				organizationType = new OrganizationType();
				adminTemplate.setIssuerOrganization(organizationType);
				adminTemplate.getIssuerOrganization().setOrganizationLocation(new LocationType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().setLocationAddress(new AddressType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().setStructuredAddress(new StructuredAddressType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStreet(new StreetType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetFullText(new TextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().setStreetExtensionText(new TextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationCityName(new ProperNameTextType());
				adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationStateUSPostalServiceCode(new USStateCodeType());
				adminTemplate.getIssuerOrganization().setOrganizationAugmentation(new OrganizationAugmentationType());
				adminTemplate.getIssuerOrganization().getOrganizationAugmentation().setOrganizationMarketingName(new TextType());

				if (null != issuer.getAddressLine1()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetFullText().setValue(issuer.getAddressLine1());
					if (null != issuer.getAddressLine2()) {
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStreet().getStreetExtensionText().setValue(issuer.getAddressLine2());
					}
				}
				
				if (null != issuer.getCity()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationCityName().setValue(issuer.getCity());
					if (null != issuer.getState()) {
						adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationStateUSPostalServiceCode().setValue(USStateCodeSimpleType.valueOf(issuer.getState()));
					}
				}
				
				if (null != issuer.getZip()) {
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().setLocationPostalCode(new com.serff.template.admin.niem.proxy.String());
					adminTemplate.getIssuerOrganization().getOrganizationLocation().getLocationAddress().getStructuredAddress().getLocationPostalCode().setValue(issuer.getZip());
				} 
				
				if (null != issuer.getMarketingName()) {
					adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().setValue(issuer.getMarketingName());
				} else {
					adminTemplate.getIssuerOrganization().getOrganizationAugmentation().getOrganizationMarketingName().setValue(issuer.getName());
				}
				// FIXME: generate orgChart from fields. Refer saveIssuerBean() method from PlanMgmtSerffServiceImpl.java.
				// issuer.getOrgChart();
				issuerData = this.objectToXmlString(adminTemplate);
			} else {
				throw new GIException("Issuer account does not exist.");
			}
		}
		finally {
			LOGGER.info("getIssuerTemplate END");
		}
		return issuerData;
	}	
	
	private ContactInformationType getContactInformationType() {
		ContactInformationType contactInformationType = new ContactInformationType();
		contactInformationType.setContactMainTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactMainTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactMainTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactTTYTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactTTYTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactTTYTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactTollFreeTelephoneNumber(new TelephoneNumberType());
		contactInformationType.getContactTollFreeTelephoneNumber().setFullTelephoneNumber(new FullTelephoneNumberType());
		contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().setTelephoneNumberFullID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.getContactTollFreeTelephoneNumber().getFullTelephoneNumber().setTelephoneSuffixID(new com.serff.template.admin.niem.proxy.String());
		contactInformationType.setContactWebsiteURI(new AnyURI());
		return contactInformationType;
	}

	/**
	 * Method is get list of Issuer Id and Name in Issuer object.
	 * @author Bhavin Parmar
	 */
	@Override
	public List<IssuerDropDownDTO> getIssuerNameList() {
		return iIssuerRepository.getIssuerNameList();
	}
	
	/**
	 * Converts Stream Reader to Object
	 * @throws JAXBException
	 */
	@SuppressWarnings("rawtypes")
	private Object streamReaderToObject(XMLStreamReader xmlData, Class c) throws JAXBException {
		
		if (null == c) {
			return null;
		}
		LOGGER.info("Creating Unmarshaller for class " + c.getName());
		
		JAXBContext jaxbContext = JAXBContext.newInstance(c);
		Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
		return unmarshaller.unmarshal(xmlData);
	}
	
	private String objectToXmlString(Object object) {

		try {
			JAXBContext context = JAXBContext.newInstance(object.getClass());
			JAXBIntrospector introspector = context.createJAXBIntrospector();
			Marshaller marshaller = context.createMarshaller();
			//marshaller.setProperty("jaxb.encoding", "US-ASCII");
			//marshaller.setProperty(Marshaller.JAXB_FRAGMENT, Boolean.TRUE); 
			//marshaller.setProperty("com.sun.xml.bind.xmlHeaders", "<?xml version=\"1.0\"?>");
		    marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			StringWriter stringWriter = new StringWriter();

			if (null == introspector.getElementName(object)) {
				JAXBElement<Object> jaxbElement = new JAXBElement<Object>(new QName("soap"), Object.class, object);
				marshaller.marshal(jaxbElement, stringWriter);
			}
			else {
				marshaller.marshal(object, stringWriter);
			}
			return stringWriter.toString();
		}
		catch (JAXBException e) {
			LOGGER.error("Error creating request xml",e);
			return StringUtils.EMPTY;
		}
	}
	
	@Override
	public List<Tenant> getTenantNameList() {		
		return iTenantRepository.findAll();
	}

	@Override
	public List<SerffDocumentPlanPOJO> getSerffDocumentByIssuerId(String issuerId, String serffOperation) {
		return iSerffDocument.getAllDocumentsforThisIssuer(issuerId, serffOperation);
	}
	
	@Override
	public IssuerPaymentInformation getIssuerPaymentInfo(Integer issuerId) {
		return issuerPaymentInfoRepo.findByIssuerId(issuerId);
	}

	@Override
	public IssuerPaymentInformation saveIssuerPaymentInfo(
			IssuerPaymentInformation issuerPayment) {
		if (null != issuerPayment) {
			return issuerPaymentInfoRepo.save(issuerPayment);
		}
		else {
			return null;
		}
	}

	/**
	 * Method is used to upload SERFF templates at FTP server.
	 */
	public String uploadTemplateAttachments(final SerffPlanTemplatesDTO templatePOJO) {

		LOGGER.debug("uploadTemplateAttachments Begin");
		String uploadStatus = INTERNAL_APP_ERROR;

		try {
			// Step1: Fetch ID from SERFF_PLAN_MGMT_BATCH TABLE and update status to “FTP started”, User_ID.
			SerffPlanMgmtBatch serffPlanMgmtBatch = createBatchRecord(templatePOJO);

			if (null == serffPlanMgmtBatch) {
				LOGGER.error("Error occurred while creating SERFF Batch Record.");
				return uploadStatus;
			}
			// Step2: Create Folder in FTP Location and Download all files to this location. ISSUER_ID + STATE_CODE + SERFF_PLAN_MGMT_BATCH_ID
			final String uploadPath;
			final String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			final boolean useDiskForTemplate = PlanMgmtConstants.STATE_CODE_CA.equalsIgnoreCase(STATE_CODE);

			if (useDiskForTemplate) {
				uploadPath = DISK_SERFF_FILES_PATH
						+ FTP_UPLOAD_PLAN_PATH
						+ serffPlanMgmtBatch.getIssuerId()
						+ serffPlanMgmtBatch.getState()
						+ serffPlanMgmtBatch.getId() + PlanMgmtConstants.FILE_SEPARATOR;

				File diskFileDirectory = new File(uploadPath);
				if (!diskFileDirectory.exists()) {
					diskFileDirectory.mkdirs();
				}
			}
			else {
				uploadPath = GhixConstants.getFTPUploadPlanPath()
						+ serffPlanMgmtBatch.getIssuerId()
						+ serffPlanMgmtBatch.getState()
						+ serffPlanMgmtBatch.getId() + PlanMgmtConstants.FILE_SEPARATOR;
				ftpClient.createDirectory(uploadPath);
			}
			LOGGER.debug("Created Directory Name with path: " + uploadPath);

			if (null != templatePOJO.getFileList()) {
				String fileName = null;
				LOGGER.debug("Start process to upload template files at Server.");

				for (MultipartFile multipartFile : templatePOJO.getFileList()) {
					fileName = multipartFile.getOriginalFilename();

					if (useDiskForTemplate) {
						FileUtils.writeByteArrayToFile(new File(uploadPath + fileName), multipartFile.getBytes());
					}
					else {
						ftpClient.uplodadFile(multipartFile.getInputStream(), uploadPath + fileName);
					}
	                LOGGER.info("Template saved for loading File Name: " + fileName);
	            }
				LOGGER.debug("End process to upload template files at Server.");
				// Step3: Update SERFF_PLAN_MGMT_BATCH TABLE status to “FTP Completed”
				serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.COMPLETED);
	        }
			else {
				// Update SERFF_PLAN_MGMT_BATCH TABLE status to “FTP Failed”
				serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.FAILED);
			}
			serffPlanMgmtBatch.setFtpEndTime(new TSDate());
			serffPlanMgmtBatch = saveSerffPlanMgmtBatch(serffPlanMgmtBatch);

			if (null != serffPlanMgmtBatch) {
				uploadStatus = PlanMgmtConstants.SUCCESS;
			}
		}
		catch (IOException e) {
			LOGGER.error("IOException: " + e.getMessage(), e);
		}
		catch(SftpException e) {
			uploadStatus = "Error: Please check FTP configuration and try again.";
			LOGGER.error("SftpException: ", e);
		}
		catch (Exception e) {
			LOGGER.error("Exception: " + e.getMessage(), e);
		}
		finally {
			LOGGER.debug("uploadTemplateAttachments End with uploadStatus: " + uploadStatus);
		}
		return uploadStatus;
	}

	/**
	 * Method is used to Create new record for SERFF_PLAN_MGMT_BATCH table.
	 */
	private SerffPlanMgmtBatch createBatchRecord(final SerffPlanTemplatesDTO templatePOJO) {
		
		LOGGER.debug("createBatchRecord Begin");
		SerffPlanMgmtBatch serffPlanMgmtBatch = null;
		
		try {
			final String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			serffPlanMgmtBatch = new SerffPlanMgmtBatch();
			serffPlanMgmtBatch.setIsDeleted(SerffPlanMgmtBatch.IS_DELETED.N);
			serffPlanMgmtBatch.setFtpStartTime(new TSDate());
			serffPlanMgmtBatch.setFtpStatus(SerffPlanMgmtBatch.FTP_STATUS.IN_PROGRESS);
			serffPlanMgmtBatch.setOperationType(templatePOJO.getOperationType().name());
			serffPlanMgmtBatch.setIssuerId(templatePOJO.getIssuerId());
			serffPlanMgmtBatch.setBatchStatus(SerffPlanMgmtBatch.BATCH_STATUS.WAITING);

			String carrierText = templatePOJO.getCarrierName();
			if (StringUtils.isNotBlank(carrierText) && StringUtils.isNotBlank(templatePOJO.getIssuerId())) {

				final int lenOfIssuerName = 100;

				if (carrierText.length() > lenOfIssuerName) {
					serffPlanMgmtBatch.setCareer(carrierText.replaceAll("\\[" + templatePOJO.getIssuerId() + "\\]", StringUtils.EMPTY).substring(0, lenOfIssuerName));
				}
				else {
					serffPlanMgmtBatch.setCareer(carrierText.replaceAll("\\[" + templatePOJO.getIssuerId() + "\\]", StringUtils.EMPTY));
				}
			}
			serffPlanMgmtBatch.setState(STATE_CODE);
			serffPlanMgmtBatch.setExchangeType(SerffPlanMgmtBatch.EXCHANGE_TYPE.ON.name());

			if (SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.equals(templatePOJO.getOperationType())
					|| SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.equals(templatePOJO.getOperationType())) {
				serffPlanMgmtBatch.setDefaultTenant(templatePOJO.getDefaultTenant());
			}
			else {
				serffPlanMgmtBatch.setDefaultTenant(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.DEFAULT_TENANT));
			}
			serffPlanMgmtBatch = saveSerffPlanMgmtBatch(serffPlanMgmtBatch);
		}
		finally {
			LOGGER.debug("createBatchRecord End");
		}
		return serffPlanMgmtBatch;
	}

	/**
	 * Method is used to get Plan Load Status Data List from SerffPlanMgmtBatch & SerffPlanMgmt tables.
	 */
	@Override
	public Page<PlanLoadStatusDTO> getPlanLoadStatusDataList(int page, int size, String selectedIssuer, String selectedType, String selectedStatus, String selectedDate) {

		LOGGER.debug("getPlanLoadStatusDataList start");
		Page<PlanLoadStatusDTO> planLoadStatusDataList = null;
		EntityManager em = null;
    	Query query = null;
		int resultCount = 0;

		try {
			em = entityManagerFactory.createEntityManager();

			//Record count query
			String queryForCount = "SELECT COUNT(serffBatch) "
					+ "FROM SerffPlanMgmtBatch AS serffBatch LEFT JOIN serffBatch.serffReqId AS serffPlanMgmt "
					+ "WHERE serffBatch.operationType IN('"+ SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.name() +"','"+ SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.name() +"','"+ SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.name() +"','"+ SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.name() +"') ";

			queryForCount += getPlanLoadStatusClause(selectedIssuer, selectedType, selectedStatus, selectedDate);
			LOGGER.info("Plan Load Status Data Count Query: " + queryForCount);

			query = em.createQuery(queryForCount);

			if (StringUtils.isNotBlank(selectedDate)) {
				query.setParameter("uploadedDate", DateUtil.dateToString(getSelectedDate(selectedDate), "MM/dd/yyyy"));
			}

		   	Object objCount = query.getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
			LOGGER.info("Result Count: " + resultCount);
			//Actual record query
			String queryStr = "SELECT new com.getinsured.hix.serff.service.PlanLoadStatusDTO(serffPlanMgmt.serffReqId, serffBatch.career, "
					+ "serffBatch.issuerId, serffBatch.ftpEndTime, serffBatch.operationType, serffBatch.batchStatus, serffPlanMgmt.remarks, "
					+ "serffPlanMgmt.responseXml, serffPlanMgmt.pmResponseXml) "
					+ "FROM SerffPlanMgmtBatch AS serffBatch LEFT JOIN serffBatch.serffReqId AS serffPlanMgmt "
					+ "WHERE serffBatch.operationType IN('"+ SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.name() +"','"
					+ SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.name() +"','"+ SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.name() +"','"
					+ SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.name() +"') ";

			queryStr += getPlanLoadStatusClause(selectedIssuer, selectedType, selectedStatus, selectedDate);
			queryStr += "ORDER BY serffBatch.id DESC";
			LOGGER.info("Plan Load Status Data Query: " + queryStr);

			query = em.createQuery(queryStr);

			if (StringUtils.isNotBlank(selectedDate)) {
				query.setParameter("uploadedDate", DateUtil.dateToString(getSelectedDate(selectedDate), "MM/dd/yyyy"));
			}
			query.setFirstResult(page * size);
			query.setMaxResults(size);

			planLoadStatusDataList = new PageImpl<PlanLoadStatusDTO>(query.getResultList(), new PageRequest(page, size), resultCount);
			LOGGER.info("planLoadStatusDataList is null: " + (null == planLoadStatusDataList));
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
			LOGGER.debug("getPlanLoadStatusDataList start");
		}
		return planLoadStatusDataList;
	}

	/**
	 * Method is used to add a clause for Plan Load Status Data.
	 */
	private String getPlanLoadStatusClause(String selectedIssuer, String selectedType, String selectedStatus, String selectedDate) {

		StringBuffer clause = new StringBuffer();

		if (StringUtils.isNotBlank(selectedType)) {

			try {
				switch (PlanLoadStatusDTO.TYPE.valueOf(selectedType.trim())) {
					case DRUG_LIST: {
						clause.append(" AND serffBatch.operationType IN ('");
						clause.append(SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_JSON.toString());
						clause.append("','");
						clause.append(SerffPlanMgmtBatch.OPERATION_TYPE.DRUGS_XML.toString());
						clause.append("') ");
						break;
					}
					case PLANS: {
						clause.append(" AND serffBatch.operationType IN ('");
						clause.append(SerffPlanMgmtBatch.OPERATION_TYPE.PLAN.toString());
						clause.append("','");
						clause.append(SerffPlanMgmtBatch.OPERATION_TYPE.PRESCRIPTION_DRUG.toString());
						clause.append("') ");
						break;
					}
				}
			}
			catch (IllegalArgumentException ex) {
				LOGGER.warn("Illegal Request type filter.");
			}
		}

		if (StringUtils.isNotBlank(selectedStatus)) {

			try {

				switch (SerffPlanMgmtBatch.BATCH_STATUS.valueOf(selectedStatus.trim())) {
					case IN_PROGRESS: {
						clause.append(" AND serffBatch.batchStatus IN ('");
						clause.append(SerffPlanMgmtBatch.BATCH_STATUS.WAITING.toString());
						clause.append("','");
						clause.append(SerffPlanMgmtBatch.BATCH_STATUS.IN_PROGRESS.toString());
						clause.append("','");
						clause.append(SerffPlanMgmtBatch.BATCH_STATUS.IN_QUEUE.toString());
						clause.append("') ");
						break;
					}
					default: {
						clause.append(" AND serffBatch.batchStatus = ('");
						clause.append(selectedStatus.trim());
						clause.append("') ");
						break;
					}
				}
			}
			catch (IllegalArgumentException ex) {
				LOGGER.warn("Illegal Request status filter.");
			}
		}

		if (StringUtils.isNotBlank(selectedIssuer)) {
			clause.append(" AND serffBatch.issuerId = '");
			clause.append(selectedIssuer.trim());
			clause.append("' ");
		}

		if (StringUtils.isNotBlank(selectedDate)) {

			SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");

			try {
				inSDF.parse(selectedDate);
				clause.append(" AND TO_DATE(TO_CHAR(serffBatch.ftpEndTime, 'MM/DD/YYYY'), 'MM/DD/YYYY') = TO_DATE(:uploadedDate, 'MM/DD/YYYY') ");
			}
			catch (ParseException e) {
				LOGGER.warn("Illegal Date filter.");
			}
		}
		return clause.toString();
	}

	/**
	 * Method is used to convert String value to Date form.
	 */
	private Date getSelectedDate(String selectedDate) {

		Date planDate = null;

		if (StringUtils.isNotBlank(selectedDate)) {
			planDate = DateUtil.StringToDate(selectedDate, "MM/dd/yyyy");
		}
		return planDate;
	}

	@Override
	public Page<SerffTransferInfoDTO> getSERFFTransferInfoDataList(int page, String selectedIssuer, String selectedStatus, String selectedDate) {

		LOGGER.debug("getSERFFTransferInfoDataList() start");
		Page<SerffTransferInfoDTO> serffTransferInfoList = null;

		try {

			SerffTransferSearchRequestDTO requestDTO = new SerffTransferSearchRequestDTO();
			requestDTO.setPageNumber(String.valueOf(page));
			requestDTO.setHiosIssuerID(selectedIssuer);
			requestDTO.setStatus(selectedStatus);
			requestDTO.setStartDate(selectedDate);

			String apiResponse = ghixRestTemplate.postForObject(GhixEndPoints.PLAN_MGMT_URL + "serfftransfer/", requestDTO, String.class);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("SERFF Transfer API response is null: " + StringUtils.isBlank(apiResponse));
			}

		    if (StringUtils.isNotBlank(apiResponse)) {
		    	SerffTransferSearchResponseDTO responseDTO = platformGson.fromJson(apiResponse, SerffTransferSearchResponseDTO.class);

		    	if (null != responseDTO) {

		    		if (null == responseDTO.getErrorCode()) {

		    			if (LOGGER.isInfoEnabled()) {
		    				LOGGER.info("Page size: " + responseDTO.getPageSize() + ", Total Record Count: " + responseDTO.getTotalRecordCount());
		    			}

		    			if (!CollectionUtils.isEmpty(responseDTO.getSerffTransferInfoList())
		    					&& null != responseDTO.getPageSize()
		    					&& null != responseDTO.getTotalRecordCount()) {
							serffTransferInfoList = new PageImpl<SerffTransferInfoDTO>(responseDTO.getSerffTransferInfoList(),
									new PageRequest((page - 1), responseDTO.getPageSize()),
									responseDTO.getTotalRecordCount());
		    			}
		    			else if (CollectionUtils.isEmpty(responseDTO.getSerffTransferInfoList())) {
		    				LOGGER.info("SERFF Transfer Info List is empty");
		    			}
		    			else {
		    				LOGGER.warn("SERFF Transfer API response is empty");
		    			}
		    		}
		    		else {
		    			LOGGER.error("Error Code: " + responseDTO.getErrorCode() + ", Error Message: " + responseDTO.getErrorMsg());
		    		}
		    	}
		    	else {
		    		LOGGER.error("API SerffTransferSearchRequestDTO object is null.");
		    	}
		    }
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("getSERFFTransferInfoDataList() end with serffTransferInfoList List is empty: " + (null == serffTransferInfoList));
			}
		}
		return serffTransferInfoList;
	}

	/**
	 * Method is used to get SERFF Plan Attachments List from database.
	 */
	@Override
	public List<SerffTransferAttachmentInfoDTO> getSERFFPlanAttachmentsList(long selectedRecordID) {

		LOGGER.debug("getSERFFPlanAttachmentsList() start");
		List<SerffTransferAttachmentInfoDTO> attachementsList = null;
		SerffTransferAttachmentResponseDTO responseDTO = null;

		try {

			String apiResponse = ghixRestTemplate.getForObject(GhixEndPoints.PLAN_MGMT_URL + "serfftransfer/"+ selectedRecordID +"/attachments", String.class);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("SERFF Transfer API response is null: " + StringUtils.isBlank(apiResponse));
			}

		    if (StringUtils.isNotBlank(apiResponse)) {
		    	responseDTO = platformGson.fromJson(apiResponse, SerffTransferAttachmentResponseDTO.class);

		    	if (null != responseDTO) {

		    		if (null == responseDTO.getErrorCode()) {

		    			if (!CollectionUtils.isEmpty(responseDTO.getSerffTransferAttachmentInfoList())) {
		    				attachementsList = responseDTO.getSerffTransferAttachmentInfoList();
		    			}
		    			else {
		    				LOGGER.info("SerffTransferAttachmentInfoDTO List is empty");
		    			}
		    		}
		    		else {
		    			LOGGER.error("Error Code: " + responseDTO.getErrorCode() + ", Error Message: " + responseDTO.getErrorMsg());
		    		}
		    	}
		    	else {
		    		LOGGER.error("API SerffTransferAttachmentResponseDTO object is null.");
		    	}
		    }
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("getSERFFPlanAttachmentsList() end with attachementsList List is empty: " + CollectionUtils.isEmpty(attachementsList));
			}
		}
		return attachementsList;
	}

	/**
	 * Method is used to get SERFF Plan Attachments List from database.
	 */
	@Override
	public boolean checkWaitingPlansToLoad() {

		LOGGER.debug("checkWaitingPlansToLoad() start");
		Boolean hasWaitingPlansToLoad = Boolean.FALSE;

		try {

			String apiResponse = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_SERFF_SERVICE_URL + "serff/checkWaitingPlansToLoad", String.class);

		    if (StringUtils.isNotBlank(apiResponse)) {
		    	hasWaitingPlansToLoad = platformGson.fromJson(apiResponse, Boolean.class);

		    	if (null == hasWaitingPlansToLoad) {
		    		LOGGER.error("Check waiting Plans to Load API's response is null.");
		    	}
		    }
		    else {
		    	LOGGER.error("Check waiting Plans to Load API's response is empty");
		    }
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("checkWaitingPlansToLoad() end with hasWaitingPlansToLoad: " + hasWaitingPlansToLoad);
			}
		}
		return hasWaitingPlansToLoad;
	}

	/**
	 * Method is used to get SERFF Plan Attachments List from database.
	 */
	@Override
	public String executeBatch() {

		LOGGER.debug("executeBatch() start");
		String batchResponse = null;

		try {

			batchResponse = ghixRestTemplate.getForObject(GhixEndPoints.GHIX_SERFF_SERVICE_URL + "serff/executeBatch", String.class);

		    if (StringUtils.isBlank(batchResponse)) {
		    	LOGGER.error("Execute Batch  API's response is empty");
		    }
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("executeBatch() end with batchResponse: " + batchResponse);
			}
		}
		return batchResponse;
	}
}
