(function() {
'use strict';

angular.module('agencyBOBCtrlModule', ['translateApp'])
.controller('AgencyBOBCtrl', AgencyBOBCtrl);

	AgencyBOBCtrl.$inject = ['$http', '$stateParams', '$scope', '$state', 'StatusService', 'dataStorageService', '$translate'];

    function AgencyBOBCtrl($http, $stateParams, $scope, $state, StatusService, dataStorageService, $translate) {
		var vm = this;
		var language = $('#lang').val() || 'en';
		var switchAccUrl = $('#switchAccUrl').val();
		var recordTypeVar = $('#recordType').val();
		vm.agencyToIndPopupDisable = $('#agencyToIndPopupDisable').val();

		vm.agencyManagerCertificationStatus = StatusService.AgencyManagerCertificationStatus;
		vm.agencyCertificationStatus = StatusService.CertificationStatus;
		vm.currentUserRole = StatusService.CurrentUserRole;

		vm.init = init;
		vm.searchFilterSlider = searchFilterSlider;
		vm.reset = reset;
		vm.getAgencyBobData = getAgencyBobData;
		vm.getCurrentYear = getCurrentYear;
		vm.getSortOptions = getSortOptions;
		vm.loadIssuerData = loadIssuerData;
		vm.selectedIssuer = selectedIssuer;
		vm.isResultsFound = true;
		vm.callModal = callModal;
		vm.showAccountModal = true;
		vm.callAccountModal = callAccountModal;
		vm.isAccountCheckboxChecked = isAccountCheckboxChecked;
		vm.callHouseholdEligibilityInfoModal = callHouseholdEligibilityInfoModal;
		vm.markInactiveConfirm = markInactiveConfirm;
		vm.updateTransferConsumers = updateTransferConsumers;

		$scope.accountCheckboxChecked = false;

		vm.houseHoldFlag = false;
		vm.consumerSelected = true;
		vm.transferConsumers = [];
		vm.agencyCertificationStatus = StatusService.CertificationStatus;
		function init(){
			if(vm.agencyCertificationStatus !== 'CERTIFIED') {
				$state.go("viewagentlist");
			}
			vm.loader = true;
			vm.inputData = {
			  	firstName : '',
	      		lastName : '',
				applicationYear : '',
				issuer : '',
				agentFirstName : '',
				agentLastName : '',
				agentLicenseNumber : '',
				caseNumber: '',
				pageNumber:"1",
				soryBy : 'undefined',
				sortOrder : 'undefined'
			};
			vm.selectedIssuerData = '';
			$scope.years = [];
			$scope.years.push(vm.getCurrentYear() - 1);
			$scope.years.push(vm.getCurrentYear());
			$scope.years.push(vm.getCurrentYear() + 1);

			$scope.accountModalPath = "/hix/resources/html/agency/templates/agencyBobAccountModal.html";
			$scope.householdModalPath = "/hix/resources/html/agency/templates/agencyBobHouseholdModal.html";
			$scope.eligibilityModalPath = "/hix/resources/html/agency/templates/agencyBobEligibilityModal.html";
			$scope.markAsInactiveModalPath = "/hix/resources/html/agency/templates/agencyBobMarkAsInactiveModal.html";
			$scope.changeDelegationModalPath = "/hix/resources/html/agency/templates/agencyBobChangeDelegationModal.html";

			vm.sortData = [
				{
					"key": "FIRSTNAME",
					"value": "agency.bob.first_name_AZ",
					"order": "ASC"
				},
				{
					"key": "FIRSTNAME",
					"value": "agency.bob.first_name_ZA",
					"order": "DESC"
				},
				{
					"key": "LASTNAME",
					"value": "agency.bob.last_name_AZ",
					"order": "ASC"
				},
				{
					"key": "LASTNAME",
					"value": "agency.bob.last_name_ZA",
					"order": "DESC"
				}
			];
			vm.filterData = {};
			vm.isSliderOpen = false;
			loadIssuerData();
			getAgencyBobData();
		}

		function loadIssuerData() {
			var promise = $http.get('/hix/broker/loadIssuers');
			promise.success(function(response){
				vm.QHPIssuerNames = response.QHPIssuerNames;
	    	 	vm.QDPIssuerNames = response.QDPIssuerNames;
			})
			promise.error(function(response){
				console.log(response);
			});
		}

		function selectedIssuer(issuer) {
			vm.selectedIssuerData = issuer;
		}

		function getSortOptions(key, order) {
				vm.filterData.sortBy = key;
				vm.filterData.sortOrder = order;
		}

		function reset(){
			vm.inputData = {};
			vm.filterData = {};
			getAgencyBobData();
		}

		function getAgencyBobData() {
			vm.loader = true;

			if(vm.refineResult) {

				var properties = ['firstName', 'lastName', 'applicationYear', 'agentFirstName', 'agentLastName', 'agentLicenseNumber', 'caseNumber', 'sortBy', 'sortorder']
				for (var i=0; i<properties.length; i++) {
					if(vm.inputData[properties[i]] != undefined ) {
						vm.filterData[properties[i]] = vm.inputData[properties[i]];
					}
				}
				vm.filterData.pageNumber = 1;
			}
			if (vm.inputData.issuer !== undefined) {
				vm.filterData.issuer = vm.inputData.issuer;
			}
			if(vm.yearFlag) {
				vm.filterData.applicationYear = vm.inputData.applicationYear;
			}
			if(vm.sortFlag) {
				vm.getSortOptions(vm.selectedData.key, vm.selectedData.order);
			}
			if(vm.paginationFlag) {
				vm.filterData.pageNumber = vm.agencyBOBData.pageNumber;
			} else {
				vm.filterData.pageNumber = 1;
			}
			var promise = $http.post('/hix/agency/bookofbusiness', vm.filterData);
			promise.success(function(response){
				vm.loader = false;
				vm.refineResult = false;
				vm.sortFlag = false;
				vm.paginationFlag = false;
				vm.agencyBOBData = response;

				if(vm.agencyBOBData.count > 0) {
					vm.isResultsFound = true;
					var individualPlanDetailsData = {
						"individualPlanDetails" :{}
					};
					angular.forEach(vm.agencyBOBData.listOfIndividuals, function (value, key) {
						if (value.planId) {
							individualPlanDetailsData.individualPlanDetails[value.individualId] = {
						      "planId": value.planId,
						      "enrollmentId": value.enrollmentId,
						      "issuerHiosId": value.issuerHiosId,
						      "officeVisit": null,
						      "genericDrugs": null,
						      "deductible": null
						    }
						}
					});
					var promiseTwo = $http.post('/hix/agency/plandetails', individualPlanDetailsData);
					promiseTwo.success(function(response){
						vm.getResponseData = response
						angular.forEach(vm.agencyBOBData.listOfIndividuals, function(response1Value, response1Key) {
							angular.forEach(response1Value, function(listOfIndividualsValue, listOfIndividualsKey){
								if(listOfIndividualsKey === "individualId") {
									angular.forEach(vm.getResponseData.individualPlanDetails, function(response2Value, response2Key){
										if (listOfIndividualsValue === response2Key) {
											response1Value.officeVisit = response2Value.officeVisit;
											response1Value.genericDrugs = response2Value.genericDrugs;
											response1Value.deductible = response2Value.deductible;
											response1Value.issuerLogo = response2Value.issuerLogo;
											response1Value.issuerName = response2Value.issuerName;
											response1Value.planName = response2Value.planName;
											response1Value.planType = response2Value.planType;
										}
									});
								}
							});
		            	});
					})
					promiseTwo.error(function(response){

					});
				} else {
					vm.isResultsFound = false;
				}
			})
			promise.error(function(response){
                vm.loader = false;
                vm.refineResult = false;
            });
		}

		function getCurrentYear() {
			var today = new Date();
			var currentYear = today.getFullYear();
			return currentYear;
		}

		function searchFilterSlider() {
			vm.isSliderOpen = !vm.isSliderOpen;
			$( ".searchPanel" ).slideToggle( "slow" );
		}

		function updateTransferConsumers() {
			vm.transferConsumers = vm.agencyBOBData.listOfIndividuals.filter(function(consumer){
				return consumer.isSelected === true;
			})
		}

		function callModal(consumerData) {
			$scope.name = {name : consumerData.firstName + ' ' + consumerData.lastName};
			$scope.consumerData = consumerData;
			callHouseholdEligibilityInfoModal(consumerData);
		}

		function callAccountModal(consumerData) {
			var recordTypeNumber=3;
			if(recordTypeVar == 'agency_manager'){
				recordTypeNumber=2;
			}
			var existURL = switchAccUrl+'&_pageLabel=individualHomePage&ahbxUserType=1&recordId='+consumerData.brokerId+'&newAppInd=N&lang='+language+'&ahbxId' + "=" + consumerData.individualId + '&recordType=' + recordTypeNumber;
			window.location.href = existURL;

		}
		function isAccountCheckboxChecked(event) {
			if($("#showPopupInFuture").attr('checked')) {
				var promise = $http.get('/hix/agency/disablepopup');
				promise.success(function(response){
					vm.agencyToIndPopupDisable = 'Y'
				})
				promise.error(function(response){
					console.log(response);
				});
			}
		 }

		function markInactiveConfirm(consumerData) {
			var promise = $http.get('/hix/agency/decline/individual/'+ consumerData.brokerId + '/' + consumerData.individualId);
			promise.success(function(response){
				vm.loader = true;
				if(response === 'ok'){
	    		  		vm.filterData.pageNumber = 1;
	    		  		vm.loader = false;
						getAgencyBobData();
	    		}
			})
			promise.error(function(response){
				console.log(response);
			});
		}

		function callHouseholdEligibilityInfoModal(consumerData) {
			var data = {
				"encryptedIndividualId": consumerData.individualId,
				"applicationYear": consumerData.applicationYear
			}
			if(vm.accountFlag) {
				var recordTypeNumber=3;
				vm.accountFlag = false;
				if(recordTypeVar == 'agency_manager'){
					recordTypeNumber=2;
				}
				if(vm.showAccountModal && (vm.agencyToIndPopupDisable === '' || vm.agencyToIndPopupDisable === 'N')){
					vm.showAccountModal = true;
					$('#individualAccountModal').modal('show');
				}
				if(!vm.showAccountModal || vm.agencyToIndPopupDisable === 'Y') {
					vm.loader = true;
					vm.showAccountModal = false;
					var existURL = switchAccUrl+'&_pageLabel=individualHomePage&ahbxUserType=1&recordId='+consumerData.brokerId+'&newAppInd=N&lang='+language;
					window.location.href = existURL;
				}

			}
			if (vm.householdFlag || vm.eligibilityFlag) {
				if(vm.householdFlag) {
					var promise = $http.post('/hix/agency/HouseholdMemberInformation', data);
				}
				if(vm.eligibilityFlag) {
					var promise = $http.post('/hix/agency/HouseholdEligibilityInformation', data);
				}
				promise.success(function(response){
					if (response != 'null'){
						$scope.responseData = response;
						vm.householdFlag = false;
						vm.eligibilityFlag = false;
					}
				})
				promise.error(function(response, status) {
					vm.callHouseholdEligibilityStatus = status;
					console.log(response);
				});
			}

        }
	}

})();
