package com.getinsured.hix.batch.notification.ssap;

import java.util.List;

public class AgeOutNoticeDTO {

	private String applicationID;
	private String primaryFirstName;
	private String primaryLastName;
	private int householdId;
	private String emailId;
	private String sEPendDate;
	
	public String getsEPendDate() {
		return sEPendDate;
	}
	public void setsEPendDate(String sEPendDate) {
		this.sEPendDate = sEPendDate;
	}
	private List<AgeOutDependant> healthDependants;
	private List<AgeOutDependant> dentalDependants;
	
	public String getApplicationID() {
		return applicationID;
	}
	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}
	public String getPrimaryFirstName() {
		return primaryFirstName;
	}
	public void setPrimaryFirstName(String primaryFirstName) {
		this.primaryFirstName = primaryFirstName;
	}
	public String getPrimaryLastName() {
		return primaryLastName;
	}
	public void setPrimaryLastName(String primaryLastName) {
		this.primaryLastName = primaryLastName;
	}
	public List<AgeOutDependant> getHealthDependants() {
		return healthDependants;
	}
	public void setHealthDependants(List<AgeOutDependant> healthDependants) {
		this.healthDependants = healthDependants;
	}
	public List<AgeOutDependant> getDentalDependants() {
		return dentalDependants;
	}
	public void setDentalDependants(List<AgeOutDependant> dentalDependants) {
		this.dentalDependants = dentalDependants;
	}
	public int getHouseholdId() {
		return householdId;
	}
	public void setHouseholdId(int householdId) {
		this.householdId = householdId;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
}
