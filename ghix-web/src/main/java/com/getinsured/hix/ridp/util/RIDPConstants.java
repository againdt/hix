package com.getinsured.hix.ridp.util;

import java.util.HashSet;
import java.util.Set;

public class RIDPConstants {

	// RIDP JSON Response field tags
	public static final String RESPONSE_CODE_FIELD = "ResponseCode";
	public static final String UI_RESPONSE_CODE_FIELD = "ResponseCode";
	public static final String RESPONSE_DESC_FIELD = "ResponseDescriptionText";
	public static final String RESPONSE_METADATA = "ResponseMetadata";
	public static final String HUB_DOWN_RESPONSE = "Hub service temporarily unavailable. Please try again later";
	public static final String FINAL_DECISION_FIELD = "FinalDecisionCode";
	public static final String UI_FINAL_DECISION_FIELD = "FinalDecision";
	public static final String RIDP_SOURCE = "ridpSource";
	public static final String SERVICE_CONTACT_NAME = "serviceContactName";
	public static final String SERVICE_CONTACT_CALL_CENTER_NUMBER = "serviceContactCallCenterNumber";
	public static final String DHS_REFERENCE_NUMBER = "DSHReferenceNumber";
	public static final String UI_DHS_REFERENCE_NUMBER = "dhsReferenceNumber";
	public static final String VERIFICATION_RESPONSE = "VerificationResponse";
	public static final String UI_VERIFICATION_RESPONSE = "verificationResponse";
	
	//FARS JSON Response field tags
	public static final String FARS_FINAL_DECISION_FIELD = "FinalDecisionCode";
	public static final String SUCCESS_CODE = "HS000000";
	
	public static final String HOUSEHOLD_RIDP_VERIFIED = "Y";
	public static final String RIDP_VERIFY_SOURCE_MANUAL = "MANUAL";
	public static final String RIDP_VERIFY_SOURCE_RIDP = "RIDP";
	public static final String RIDP_VERIFY_SOURCE_FARS = "FARS";
	public static final String RIDP_VERIFY_SOURCE_PAPER = "PAPER";
	public static final String RIDP_VERIFY_SOURCE_CSR_OVERRIDE = "CSR_OVERRIDE";
	
	// TKM Constants
	public static final String RIDP_MANUAL_VERIFICATION_DOCUMENTS = "ridpManualVerificationDocuments";
	public static final String RIDP_TKM_CATEGORY = "Document Verification";
	public static final String RIDP_TKM_TYPE = "RIDP";
	public static final String RIDP_TKM_MODULE_NAME = "HOUSEHOLD";
	public static final String INDIVIDUAL_ROLE = "INDIVIDUAL";
	public static final String HUB_DOWN_RESPONSE_CODE = "HE200005";
    public static final String ALLOW_MANUAL_VERIFICATION = "allowManualVerification";
    
    public static final Set<String> FARS_RESPONSE_CODES = new HashSet<String>();
    public static final Set<String> MANUAL_VERIFICATION_RESPONSE_CODES = new HashSet<String>();
    public static final Set<String> ERROR_RESPONSE_CODES = new HashSet<String>();
    public static final Set<String> FARS_ERROR_RESPONSE_CODES = new HashSet<String>();
	public static final String RIDP_PRIMARY_FINAL_DECISION_FIELD = "finalDecision";
    
    static{
    	FARS_RESPONSE_CODES.add("RF1");
    	FARS_RESPONSE_CODES.add("REF");
    	FARS_RESPONSE_CODES.add("RF2");
    	
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("RF4");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200001");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200002");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200003");
    	
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200029");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200030");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200031");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200038");
    	MANUAL_VERIFICATION_RESPONSE_CODES.add("HE200039");
    	
    	ERROR_RESPONSE_CODES.add("HE009999");
    	ERROR_RESPONSE_CODES.add("HE200024");
    	ERROR_RESPONSE_CODES.add("HE200025");
    	ERROR_RESPONSE_CODES.add("HE200026");
    	ERROR_RESPONSE_CODES.add("HE200027");
    	ERROR_RESPONSE_CODES.add("HE200028");
    	ERROR_RESPONSE_CODES.add("HE200029");
    	ERROR_RESPONSE_CODES.add("HE200030");
    	ERROR_RESPONSE_CODES.add("HE200031");
    	ERROR_RESPONSE_CODES.add("HE200037");
    	ERROR_RESPONSE_CODES.add("HE200038");
    	ERROR_RESPONSE_CODES.add("HE200039");
    	
    	ERROR_RESPONSE_CODES.add("HE200040");
    	ERROR_RESPONSE_CODES.add("HE200041");
    	ERROR_RESPONSE_CODES.add("HE200042");
    	ERROR_RESPONSE_CODES.add("HX005000");
    	ERROR_RESPONSE_CODES.add("HX005001");
    	ERROR_RESPONSE_CODES.add("HX009000");
    	ERROR_RESPONSE_CODES.add("HX200004");
    	ERROR_RESPONSE_CODES.add("HX200005");
    	ERROR_RESPONSE_CODES.add("HX200008");
    	ERROR_RESPONSE_CODES.add("HX200009");
    	ERROR_RESPONSE_CODES.add("HX200010");
    	
    	FARS_ERROR_RESPONSE_CODES.add("HE200006");
    	FARS_ERROR_RESPONSE_CODES.add("HE200045");
    	FARS_ERROR_RESPONSE_CODES.add("HX009000");
    	FARS_ERROR_RESPONSE_CODES.add("HX005001");
    	FARS_ERROR_RESPONSE_CODES.add("HE009999");
    		
    	
    }
    
    public static boolean isFarsError(String responseCode){
    	if(responseCode == null ){
    		return true;
    	}
    	return !SUCCESS_CODE.equalsIgnoreCase(responseCode);
    }
    
    public static boolean isError(String responseCode){
    	if(responseCode == null){
    		return true;
    	}
    	return ERROR_RESPONSE_CODES.contains(responseCode.toUpperCase());
    }
    
    
    public static boolean isManualVarificationRequired(String responseCode){
    	if(responseCode == null){
    		return false;
    	}
    	return MANUAL_VERIFICATION_RESPONSE_CODES.contains(responseCode.toUpperCase());
    }
    
    public static boolean isFarsVarificationRequired(String responseCode){
    	if(responseCode == null){
    		return false;
    	}
    	return FARS_RESPONSE_CODES.contains(responseCode.toUpperCase());
    }
    
    public static boolean isTemporaryFailure(String responseCode){
    	if(responseCode == null){
    		return false;
    	}
    	return responseCode.toUpperCase().equalsIgnoreCase("RF3".intern());
    }
}
