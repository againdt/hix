package com.getinsured.iex.ssap.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.iex.ssap.model.SsapIntegrationLog;

public interface SsapIntegrationLogRepository extends JpaRepository<SsapIntegrationLog, Long> {

	@Query("select count(sil) from SsapIntegrationLog sil where sil.ssapApplicationId = :applicationId and sil.serviceName = :serviceName and isScheduledDowntime is null")
	Long getRetryCountForService(@Param("applicationId") Long applicationId, @Param("serviceName") String serviceName);
	
	@Query("select sil from SsapIntegrationLog sil where sil.giWsPayloadId = :giWsPayloadId ")
	SsapIntegrationLog findByGiWsPayload(@Param("giWsPayloadId") Long giWsPayloadId);

}
