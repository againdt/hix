import React, {Fragment} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';

import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {hideModal} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

const LackOfAddressesModal = props => {

    console.log(props);

    const {
        modal: {
            modalOwner,
            data: [
                membersWithoutAddress
            ],
            callbacks
        },
    } = props;

    console.log(callbacks);

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__validate__address">
                <h2>
                    Not all addresses provided
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div id="iiAddIncomeOrDeductionModal" className="usa-grid">
                <div className="modal-body">
                    We discovered that for these persons you have not provided addresses:
                    {
                        membersWithoutAddress.map((hhm, i) => (
                            <p key={`${hhm.firstName}_${i}`}>
                                { normalizeNameOfPerson(hhm) }
                            </p>
                        ))
                    }
                </div>
            </div>
            <div className='usa-grid controls'>
                <div className='usa-offset-one-third txt-left'>
                    <Button
                        className='modal-cancel-btn usa-button-secondary'
                        onClick={handleHideClick}
                    >
                        Cancel
                    </Button>
                    <Button
                        className='modal-confirm-btn'
                        onClick={callbacks}
                    >
                        Save
                    </Button>
                </div>
            </div>

        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(LackOfAddressesModal);
