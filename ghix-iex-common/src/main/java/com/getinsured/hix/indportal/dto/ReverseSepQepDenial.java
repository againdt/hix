package com.getinsured.hix.indportal.dto;

public class ReverseSepQepDenial {
	
	private String caseNumber;
	private String enrollmentEndDate;
	private Long cmrHouseholdId;
	
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getEnrollmentEndDate() {
		return enrollmentEndDate;
	}
	public void setEnrollmentEndDate(String enrollmentEndDate) {
		this.enrollmentEndDate = enrollmentEndDate;
	}
	public Long getCmrHouseholdId() {
		return cmrHouseholdId;
	}
	public void setCmrHouseholdId(Long cmrHouseholdId) {
		this.cmrHouseholdId = cmrHouseholdId;
	}
	
}
