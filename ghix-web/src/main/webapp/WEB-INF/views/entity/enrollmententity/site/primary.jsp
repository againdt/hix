<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<META http-equiv="Cache-Control" content="max-age=0" />
<META http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" />
<META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT">
<META http-equiv="Pragma" content="no-cache" />

<link href="<c:url value="/resources/css/chosen.css" />" rel="stylesheet" type="text/css" media="screen,print">
<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>

 <script type="text/javascript">
  $(document).ready(function(){
	  var config = {
		      '.chosen-select'           : {enable_split_word_search:false},
		      '.chosen-select-deselect'  : {allow_single_deselect:true},
		      '.chosen-select-no-single' : {disable_search_threshold:10},
		      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
		      '.chosen-select-width'     : {width:"95%"}
		    }
		    for (var selector in config) {
		      $(selector).chosen(config[selector]);
		    }
  });
  
  $(document).ready(function() {
		// disable autocomplete
		if (document.getElementsByTagName) {
			var inputElements = null;
			var elemIds = ["physicalLocation_address1", "physicalLocation_address2", "physicalLocation_city", "physicalLocation_state", "physicalLocation_zip", "mailingLocation_address1", "mailingLocation_address2", "mailingLocation_city", "mailingLocation_state", "mailingLocation_zip"];
			
			for(i=0; elemIds[i]; i++) {
				inputElements = document.getElementById(elemIds[i]);
				
				for (j=0; inputElements[j]; j++) {							
					if (inputElements[j].className && (inputElements[j].className.indexOf("disableAutoComplete") != -1)) {
						inputElements[j].setAttribute("autocomplete","off");
					}
				}
			}
		}
	});
    
</script>

<div class="gutter10">
<%-- <div class="row-fluid">
	<div class="page-header">
		<h1><spring:message code="label.entity.step"/> 3: <spring:message code="label.entity.locationAndHours"/></h1>
	</div>
</div> --%>

      <div class="row-fluid margin20-t">
         <jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" />
    <!-- #sidebar -->      
       
        
    <!-- #section -->
        <div class="span9" id="rightpanel">
          <div id="section">
          	<div class="header">
				<h4><spring:message code="label.entity.step"/> 3: <spring:message code="label.entity.locationAndHours"/></h4>
			</div>
			<div class="gutter10">
 			<p><spring:message code="label.entity.inThisSectionUWillListOutAllOfUrOrgLocAndTheirCorrBussHour"/></p>
     		<c:if test="${param.pme == 'true' }">
     			<div style="color: red;font-size: medium;"><spring:message code="err.primarySiteExist"/></div>
     		</c:if>
              <form class="form-horizontal entityAddressValidation" name="frmaddSite" id="frmaddSite"  method="POST" action="<c:url value="/entity/enrollmententity/primarysite"></c:url>"  autocomplete="off" >
              <df:csrfToken/>
              <input type="hidden" name="entityId" value="<encryptor:enc value="${site.entity.id}"/>"/>
           		<div class="control-group">
	                <label class="control-label" id="siteNamelbl" for="siteLocationName"><spring:message code="label.entitity.primarySiteLocation"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
	                <div class="controls">
	                  <input type="text" id="siteLocationName" name ="siteLocationName" placeholder="<spring:message code="label.entity.siteName"/>" value="${fn:escapeXml(site.siteLocationName)}">
	               	  <div id="siteLocationName_error"></div>
	                </div><!-- .controls -->
	            </div><!-- .control-group -->
          
              <div class="control-group">
                <label class="control-label" for="primaryEmailAddress"><spring:message code="label.entity.primaryEmailAddress"/></label>
                <div class="controls">
                  <input type="text" id="primaryEmailAddress" name="primaryEmailAddress" placeholder="<spring:message code="label.entity.primaryEmailAddress"/>" value="${site.primaryEmailAddress}">
                  <div id="primaryEmailAddress_error"></div>       
                </div><!-- .controls -->
              </div><!-- .control-group -->	
              
              <div class="control-group">
                <label class="control-label" for="primaryPhone"><spring:message code="label.entity.primaryPhoneNumber"/></label>
                <input class="hide" id="primaryPhone">
                <label class="hide" for="primaryPhone1">Primary Phone 1</label>
                <label class="hide" for="primaryPhone2">Primary Phone 2</label>
                <label class="hide" for="primaryPhone3">Primary Phone 3</label>
                <div class="controls">
	                  <input type="text" name="primaryPhone1" id="primaryPhone1" value="${primaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
	                  <input type="text" name="primaryPhone2" id="primaryPhone2" value="${primaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
	                  <input type="text" name="primaryPhone3" id="primaryPhone3" value="${primaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>  
	                  <input type="hidden" name="primaryPhoneNumber" id="primaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
	                  <div id="primaryPhone1_error"></div>
	                   <div id="primaryPhone3_error"></div>		              
                </div><!-- .controls -->
              </div><!-- .control-group -->
              
              <div class="control-group">
                <label class="control-label" for="secondaryPhone"><spring:message code="label.entity.secondaryPhoneNumber"/></label>
                	<input class="hide" id="secondaryPhone">
                <label class="hide" for="secondaryPhone1">Secondary Phone 1</label>
                <label class="hide" for="secondaryPhone2">Secondary Phone 2</label>
                <label class="hide" for="secondaryPhone3">Secondary Phone 3</label>
	                <div class="controls">
		                  <input type="text" name="secondaryPhone1" id="secondaryPhone1" value="${secondaryPhone1}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
		                  <input type="text" name="secondaryPhone2" id="secondaryPhone2" value="${secondaryPhone2}" size="3" maxlength="3" class="input-mini" placeholder="xxx" />
		                  <input type="text" name="secondaryPhone3" id="secondaryPhone3" value="${secondaryPhone3}" size="4" maxlength="4" class="input-mini" placeholder="xxxx"/>
		                  <input type="hidden" name="secondaryPhoneNumber" id="secondaryPhoneNumber" value="${site.primaryPhoneNumber}" /> 
						  <div id="secondaryPhone1_error"></div>  
						  <div id="secondaryPhone3_error"></div>            
	                </div><!-- .controls -->
              </div><!-- .control-group -->
               <div class="control-group">
          		  <div id="from-to-day">
          		  <fieldset>
            		    <h4>
            		      <spring:message code="label.entity.hoursOfOperation"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" />
           		      </h4>
            				<c:forEach var="day" items="${daysList}">
					                <div class="control-group" id="from-to">
					                		
							                <label id="dayId" class="control-label" for="${day.lookupValueLabel}"> ${day.lookupValueLabel}</label>
							                <input id="${day.lookupValueLabel}" class="hide">
							                <label for="${day.lookupValueLabel}From" class="hide">${day.lookupValueLabel}From</label>
							                <label for="${day.lookupValueLabel}To" class="hide">${day.lookupValueLabel}To</label>
							                <div class="controls">				          
												<select id="${day.lookupValueLabel}From" name="${day.lookupValueLabel}From" class="input-medium">
													<option value=""><spring:message code="label.entity.select"/></option>
													<c:forEach var="fromhour" items="${fromTimelist}">
														<option id="DayStart:${day.lookupValueLabel}" class="hourclass" value="<c:out value="${fromhour.lookupValueCode}" />"<c:if test="${fromhour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].fromTime}"> SELECTED </c:if>>
														${fromhour.lookupValueLabel} 
														</option>
															
													</c:forEach>
												</select>		
											<span>&nbsp;<spring:message code="label.entity.to"/> &nbsp;</span>	
											<select id="${day.lookupValueLabel}To" name="${day.lookupValueLabel}To" class="input-medium">
											<option value=""><spring:message code="label.entity.select"/></option>
											<c:forEach var="tohour" items="${toTimelist}">
												<option id="DayEnd:${day.lookupValueLabel}" class="hourclass" value="<c:out value="${tohour.lookupValueCode}" />"<c:if test="${tohour.lookupValueCode == siteLocationHoursMap[day.lookupValueLabel].toTime}"> SELECTED </c:if>>
														${tohour.lookupValueLabel}
												</option>
											</c:forEach>
										</select>	
												
									</div>										
								 </div>
							</c:forEach>	
							<label for="siteLocationHoursErrorField" class="hide"><spring:message code="label.entity.siteLocationHoursError"/></label>		
							 	<input name="siteLocationHoursErrorField" id="siteLocationHoursErrorField" style="display:none;"/>		
					 			<div id="siteLocationHoursErrorField_error"></div>						
					 			</fieldset>	
							</div>
							</div>
     						<input type="hidden" id="siteLocationHoursHidden" name="siteLocationHoursHidden" value=""/>
     			
				<h4 class="legend-title"><spring:message code="label.entity.mailingAddres"/></h4>
     			<fieldset>
            		  	<div class="control-group">
		                	<label class="control-label" for="mailingLocation_address1"><spring:message code="label.entity.streetAddress"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                <div class="controls">
			                 	<input type="text" id="mailingLocation_address1" name="mailingLocation.address1" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.mailingLocation.address1}" maxlength="25" autocomplete="off" >
			                  	<input type="hidden" id="address1_mailing_hidden" name="address1_mailing_hidden" value="${site.mailingLocation.address1}">
			            	   	<div id="mailingLocation_address1_error"></div>
			            	</div>		            	   		
	            	   	</div>
	            	   		<div class="control-group">
		            	   		<label class="control-label" for="mailingLocation_address2"><spring:message code="label.entity.suite"/></label>
			                		<div class="controls">
			                			<c:choose>
											<c:when test="${CA_STATE_CODE}">
			                  			<input type="text" id="mailingLocation_address2" name="mailingLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.mailingLocation.address2}" maxlength="25" autocomplete="off" >
											</c:when>
											<c:otherwise>
												<input type="text" id="mailingLocation_address2" name="mailingLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.mailingLocation.address2}" autocomplete="off" >
											</c:otherwise>
										</c:choose>	
			                  			<input type="hidden" id="address2_mailing_hidden" name="address2_mailing_hidden" value="${site.mailingLocation.address2}">
			            	   		</div>
			            	 </div> 
			            	 <div class="control-group">
		            	   		<label class="control-label" for="mailingLocation_city"><spring:message code="label.entity.city"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                		<div class="controls">
			                			<c:choose>
											<c:when test="${CA_STATE_CODE}">
			                  			<input type="text" id="mailingLocation_city" name="mailingLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.mailingLocation.city}" maxlength="30" autocomplete="off" >
											</c:when>
											<c:otherwise>
												<input type="text" id="mailingLocation_city" name="mailingLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.mailingLocation.city}" autocomplete="off" >
											</c:otherwise>
										</c:choose>	
			                  			<input type="hidden" id="city_mailing_hidden" name="city_mailing_hidden" value="${site.mailingLocation.city}">
			            	   			<div id="mailingLocation_city_error"></div>
			            	   		</div>
			            	   		
			            	 </div>  
			            	 
			            	 <div class="control-group"> 
			            	  <label for="mailingLocation_state" class="control-label"><spring:message code="label.entity.state"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
							<div class="controls">
								<select id="mailingLocation_state" name="mailingLocation.state"   class="input-medium" autocomplete="off" >
								<option value=""><spring:message code="label.entity.select"/></option>
								<c:forEach var="state" items="${statelist}">
									<option id="${state.code}" <c:if test="${state.code == site.mailingLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
										<c:out value="${state.name}" />
									</option>
								</c:forEach>
							</select>
							<input type="hidden" id="state_mailing_hidden" name="state_mailing_hidden" value="${site.mailingLocation.state}">
							<div id="mailingLocation_state_error"></div> 		
							</div>								
						</div><!-- end of control-group --> 
						
						<div class="control-group"> 		
		            	   			<label class="control-label" for="mailingLocation_zip"><spring:message code="label.entity.zipCode"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></label>
			                		<div class="controls">
			                  			<input type="text" id="mailingLocation_zip" name="mailingLocation.zip" class="input-mini entityZipCode" placeholder="<spring:message code="label.assister.zipCode"/>" value ="${site.mailingLocation.zip}" maxlength="5" autocomplete="off" >
			          					<input type="hidden" id="zip_mailing_hidden" name="zip_mailing_hidden" value="${site.mailingLocation.zip}">
			            	   			<div id="mailingLocation_zip_error"></div> 
			            	   		</div>
			            	  </div>
			          </fieldset>       	   		    	   		
			            	    
			         	     
            	 	
            	<h4 class="legend-title"><spring:message code="label.entity.physicalAddress"/></h4>																										          
               	<p><spring:message code="label.entity.physicalAddress.searchWarn"/></p>
               	<div class="control-group">
					<label class="control-label" for="physicalAddressCheck"> <spring:message code="label.entity.sameAsMailingAddress"/></label>
					<div class="controls">
							<input type="checkbox" name="physicalAddressCheck" id="physicalAddressCheck" ${locationMatching=='Y' ? 'checked' : 'unchecked'}>
					        <input type="hidden" id="physicalAddressCheckFlag" name="physicalAddressCheckFlag" value="0">
					 </div>
				</div>
				
          		<div class="physicalmailingAddress">
					     		
						<div class="control-group">					
							  	<label class="control-label" for="physicalLocation_address1"><spring:message code="label.entity.streetAddress"/></label>
								<div class="controls">
									<input type="text" id="physicalLocation_address1" name="physicalLocation.address1" placeholder="<spring:message code="label.entity.streetAddress"/>" value="${site.physicalLocation.address1}" maxlength="25" autocomplete="off" >
									<input type="hidden" id="address1_physical_hidden" name="address1_physical_hidden" value="${site.physicalLocation.address1}">
									<div id="physicalLocation_address1_error"></div> 
								</div>
							</div>
							<div class="control-group">
	            	   		<label class="control-label" for="physicalLocation_address2"><spring:message code="label.entity.suite"/></label>
		                		<div class="controls">
		                  			<c:choose>
										<c:when test="${CA_STATE_CODE}">
		                  			<input type="text" id="physicalLocation_address2" name ="physicalLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.physicalLocation.address2}" maxlength="25" autocomplete="off" >
										</c:when>
										<c:otherwise>
											<input type="text" id="physicalLocation_address2" name ="physicalLocation.address2" placeholder="<spring:message code="label.entity.suite"/>" value="${site.physicalLocation.address2}" autocomplete="off" >
										</c:otherwise>
									</c:choose>	
		                  			<input type="hidden" id="address2_physical_hidden" name="address2_physical_hidden" value="${site.physicalLocation.address2}">
		            	   		</div>
		            	   	</div>
		            	   	 <div class="control-group">
		            	   		<label class="control-label" for="physicalLocation_city"><spring:message code="label.entity.city"/></label>
			                		<div class="controls">
			                			<c:choose>
											<c:when test="${CA_STATE_CODE}">
			                  			<input type="text" id="physicalLocation_city" name="physicalLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.physicalLocation.city}" maxlength="30" autocomplete="off" >
											</c:when>
											<c:otherwise>
												<input type="text" id="physicalLocation_city" name="physicalLocation.city" placeholder="<spring:message code="label.entity.city"/>" value="${site.physicalLocation.city}" autocomplete="off" >
											</c:otherwise>
										</c:choose>
			                  			<input type="hidden" id="city_physical_hidden" name="city_physical_hidden" value="${site.physicalLocation.city}">
			            	   			<div id="physicalLocation_city_error"></div>
			            	   		</div>
			            	   			
			            	 </div>  
			            	 <div class="control-group">
								<label for="physicalLocation_state" class="control-label"><spring:message code="label.entity.state"/></label>
								<div class="controls">
									<select id="physicalLocation_state" name="physicalLocation.state" class="input-medium" autocomplete="off" >
									<option value=""><spring:message code="label.entity.select"/></option>
									<c:forEach var="state" items="${statelist}">
										<option id="${state.code}" <c:if test="${state.code == site.physicalLocation.state}"> selected="selected" </c:if> value="<c:out value="${state.code}" />">
											<c:out value="${state.name}" />
										</option>
									</c:forEach>								
								</select>		
								<input type="hidden" name="state_physical_hidden" id="state_physical_hidden" value="${site.physicalLocation.state}"/>			
								<div id="physicalLocation_state_error"></div> 									
								</div><!-- end of control-group -->        	   		
	            	 		</div>
            	 			<div class="control-group">	
	            	   			<label class="control-label" for="physicalLocation_zip"><spring:message code="label.entity.zipCode"/></label>
		                		<div class="controls">
		                  			<input type="text" id="physicalLocation_zip" name="physicalLocation.zip" class="input-mini entityZipCode"  placeholder="<spring:message code="label.entity.zipCode"/>" value="${site.physicalLocation.zip}" maxlength="5" autocomplete="off" >
		                  			<input type="hidden" id="zip_physical_hidden" name="zip_physical_hidden" value="${site.physicalLocation.zip}">		            	   		 
			            	   		<div id="physicalLocation_zip_error"></div>
			            	   	</div>
		            	   	</div>
            	 	</div>
            	 		                	
				
				<h4 class="legend-title"><spring:message code="label.entity.languageSupported"/></h4>
            	<fieldset>
            	   <table  id="languageTable" class="margin5-b">
            	   <thead>
									 <tr>				                     
				                      <th><spring:message code="label.entity.chooseAllSPOKENLanguagesSupported"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></th>	
				                     </tr>				      
				                     </thead>            
				                  <tbody>
				                
				                  <c:forEach var="language" items="${listOflanguages}">
				                    <tr>				                      
					                      <%-- <td id="spoken">
					                      <label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
					                      <input type="checkbox" name='spoken' id="${language.lookupValueLabel}"  value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' } >
					                      <c:out value="${language.lookupValueLabel}" /></td> --%>
					                      <td id="spoken">
						                      <label for="speak_${language.lookupValueLabel}">
							                      <input type="checkbox" name='spoken' id="spokenLanguagesId"  value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.spokenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' } >
							                      <span class="aria-hidden"><spring:message code="label.entity.chooseAllSPOKENLanguagesSupported"/></span> <c:out value="${language.lookupValueLabel}" />
						                      </label>
					                      </td>
				                      </tr>
				                      </c:forEach>
				                     <tr>				                      
				                     	<td>
				                     		<div class="selectPlugin">
					                     		<label for="otherSpokenLanguageCheckbox" class="hide"><spring:message code="label.entity.otherSpokenLanguagesCheckbox"/></label>
					                     		<label for="otherSpokenLanguage" class="hide"><spring:message code="label.entity.otherSpokenLangugesInput"/></label>
					                     		<input type="checkbox" name="otherSpokenLanguageCheckbox" id="otherSpokenLanguageCheckbox"  value="${otherSpokenLanguage}" ${otherSpokenLanguage != null ? 'checked="checked"' : '' }> 
					                     			<spring:message code="label.entity.other"/> &nbsp; 
			                      				<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherSpokenLanguage" name="otherSpokenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="0" ></select>
		                      				</div>
				                     	</td>
				                     </tr>
				                       <tr>
				                     	<td id="spokenLanguagesId_error" class="spokenLanguagesId_error"></td>
		                      			<td id="otherSpokenLanguage_error" class="spokenLanguagesId_error"></td>
				                     </tr>
				                     </table>
				                     <div id="otherSpokenLanguageCheckbox_error"></div>
				                     
				                     <table class="margin5-b">
				                     <thead>
				                     	<tr>				                     
				                      		<th><spring:message code="label.entity.chooseAllWRITTENLanguagesSupported"/><img src="<c:url value="/resources/img/requiredAsterisk.png" />" alt="Required!" /></th>
				                      	</tr>	
				                      </thead>
				                      <tbody>
				                   
				                     <c:forEach var="language" items="${listOfLanguagesWritten}"> 
				                     	<tr>
					                     <%--  <td>
					                      <label for="${language.lookupValueLabel}" class="hide">${language.lookupValueLabel}</label>
					                      <input type="checkbox" id="writtenLanguagesId" name='written' value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
					                      <c:out value="${language.lookupValueLabel}" /></td> --%>
					                       <td>
						                      <label for="written_${language.lookupValueLabel}">
							                      <input type="checkbox" id="writtenLanguagesId" name='written' value="${language.lookupValueLabel}" ${fn:contains(siteLanguages.writtenLanguages,language.lookupValueLabel) ? 'checked="checked"' : '' }>
							                      <span class="aria-hidden"><spring:message code="label.entity.chooseAllWRITTENLanguagesSupported"/></span> <c:out value="${language.lookupValueLabel}" />
						                      </label>
					                      </td>
				                    	</tr>
				                  	</c:forEach>
				                  
								  <tr>
				                   	<td>
				                   		<div class="selectPlugin">
						                   	<label for="otherWrittenLanguageCheckbox" class="hide"><spring:message code="label.entity.otherWrittenLanguageCheckbox"/></label>
						                    <input type="checkbox" name="otherWrittenLanguageCheckbox" id="otherWrittenLanguageCheckbox"  value="${otherWrittenLanguage}" ${otherWrittenLanguage != null  ? 'checked="checked"' : '' }>  <spring:message code="label.entity.other"/> &nbsp;  
				                      		<label for="otherWrittenLanguageCheckbox" class="hide">${otherWrittenLanguage}</label>
				                      		<select data-placeholder="<spring:message code="label.selectsomeoptions"/>" id="otherWrittenLanguage" name="otherWrittenLanguage"  class="chosen-select" multiple style="width:350px;" tabindex="0" ></select>
		                           		</div>
		                           </td>
				                 </tr> 
				                  <tr>
				                  	<td id="writtenLanguagesId_error" class="margin10-t"></td>				              	                  		
									<td id="otherWrittenLanguage_error" class="margin10-t"></td>				                  	
				                  </tr>
				              </tbody>
				             </table>
				              <div id="otherWrittenLanguageCheckbox_error"></div>	
				 </fieldset>   
				 <input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="0" />
				  <input type="hidden" name="languagesWrittenCheck" id="languagesWrittenCheck" value="0" />
				 <div class="clear form-actions">  
							<c:set var="entityIdEncrypted" ><encryptor:enc value="${site.entity.id}" isurl="true"/> </c:set>		      
			              	<c:set var="entityUserIdEncrypted" ><encryptor:enc value="${site.entity.user.id}" isurl="true"/> </c:set>
			              	<c:choose>	
			              		<c:when test="${loggedUser == 'entityAdmin' && site != null}">
                    			<input type="button" name="cancel" id="primarySiteBack" value="<spring:message code="label.entity.cancel"/>" class="btn cancel-btn" onClick="window.location.href='/hix/entity/entityadmin/viewsite/${entityUserIdEncrypted}'"/>
          						<input type="submit" name="save" id="save" class="btn btn-primary" value="<spring:message code="label.entity.save"/>" />
                   				</c:when>
                   				<c:when test="${loggedUser == 'enrollmentEntity' && registrationStatus != 'Incomplete' && site != null}">
								<input type="button" name="cancel" id="primarySiteBack" value="<spring:message code="label.entity.cancel"/>" class="btn cancel-btn" onClick="window.location.href='/hix/entity/enrollmententity/viewsite'"/>
								<input type="submit" name="save" id="save" class="btn btn-primary" value="<spring:message code="label.entity.save"/>" />
								</c:when>
				 			    <c:when test="${site != null}">		      
					              	<input type="button" name="back" id="primarySiteBack" value="<spring:message code="label.entity.back"/>" class="btn back-btn" onClick="window.location.href='/hix/entity/enrollmententity/getPopulationServed?entityId=${entityIdEncrypted}'"/>
						  			<input type="submit" name="save" id="save" class="btn btn-primary" value="<spring:message code="label.entity.save"/>" />
						  		</c:when> 
			  					<c:otherwise>
					              	<input type="button" name="back" id="primarySiteBack" value="<spring:message code="label.entity.back"/>" class="btn back-btn" onClick="window.location.href='/hix/entity/enrollmententity/getPopulationServed?entityId=${entityIdEncrypted}'"/>
						  			<input type="submit" name="save" id="save" class="btn btn-primary" value="<spring:message code="label.entity.next"/>" />
								</c:otherwise>
				  			</c:choose>
				  		          
             	</div> 

					<input type="hidden" id="county_physical" name="location.county" value="${site.physicalLocation.county}">
				<!-- 	<input type="hidden" name="location.lat" id="lat_physical" value="${site.physicalLocation.lat != null ? site.physicalLocation.lat : 0.0}" />
					<input type="hidden" name="location.lon" id="lon_physical" value="${site.physicalLocation.lon != null ? site.physicalLocation.lon : 0.0}" />	 -->
					<input type="hidden" name="location.rdi" id="rdi_physical" value="${site.physicalLocation.rdi != null ? site.physicalLocation.rdi : ''}" />
					
			<!-- 	<input type="hidden" name="mailingLocation.lat" id="lat_mailing" value="${site.mailingLocation.lat != null ? site.mailingLocation.lat : 0.0}" />
					<input type="hidden" name="mailingLocation.lon" id="lon_mailing" value="${site.mailingLocation.lon != null ? site.mailingLocation.lon : 0.0}" /> -->
					<input type="hidden" name="mailingLocation.rdi" id="rdi_mailing" value="${site.mailingLocation.rdi != null ? site.mailingLocation.rdi : ''}" />
					<input type="hidden" name="mailingLocation.county" id="county_mailing" value="${site.mailingLocation.county != null ? site.mailingLocation.county : ''}" />	
						
             <input type="hidden" name="spokenLanguages" id="spokenLanguages" value=""/>
             <input type="hidden" name="writtenLanguages" id="writtenLanguages" value=""/>
   	         <input type="hidden" name="siteId" id ="siteId" value="<encryptor:enc value="${site.id}"/>"/>
	         <input type="hidden" name="siteLanguageId" value="<encryptor:enc value="${siteLanguages.id}"/>"/>
	         
	         <input type="hidden" name="zipcodeID" id="zipcodeID" value="">
          </form> 
          </div>
          </div> 	         				
		</div>
     </div>
</div>
<!-- Content ENDS -->


  
<script type="text/javascript">

$(document).ready(function() {
	$("#locationAndHours").removeClass("link");
	$("#locationAndHours").addClass("active2");
	$('li#locationAndHours ul li').show();
	$('.primarySiteSubpage').addClass('activeSubpage');
	
	/* if($("#physicalAddressCheck").is(":checked")) {
		$("#physicalAddressCheckFlag").val("1");
		$("#physicalLocation_address1").attr('readonly', true);
		$("#physicalLocation_address2").attr('readonly', true);
		$("#physicalLocation_city").attr('readonly', true);
		$("#physicalLocation_state").attr('disabled', true);
		$("#physicalLocation_zip").attr('readonly', true);
	} else {
		$("#physicalAddressCheckFlag").val("0");
		$("#physicalLocation_address1").removeAttr("readonly"); 
		$("#physicalLocation_address2").removeAttr("readonly"); 
		$("#physicalLocation_city").removeAttr("readonly");
		$("#physicalLocation_state").removeAttr("disabled"); 
		$("#physicalLocation_zip").removeAttr("readonly");
	} */
	
	if($("#mailingAddressCheck").is(":checked")) {
		$('.physicalmailingAddress').hide();
	}
	
	var el; 
	$("select").each(function() { 
		el = $(this); 
		el.data("origWidth", el.outerWidth()) // IE 8 can haz padding 
	}).mouseenter(function(){ 
		el = $(this); 
		el.css("width", el.data("origWidth")); 
	});
	
	$(".input-medium").change(function(){
		var currentId = $(this).attr('id');
		currentElement = $(this).attr('id');
		currentId = currentId.replace(/From|To/g, "");
		if(currentElement.indexOf("From") > 0){
			if($("#"+currentId+"From").val() == "Closed"){
				$("#" + currentId + "To").val("Closed");
				$("#" + currentId + "To").attr("disabled",true);
				return;
			}
			if($("#"+currentId+"From").val() != "Select" && $("#"+currentId+"From").val() != "Closed"){
				if($("#" + currentId + "To").val() == "Closed"){
					$("#" + currentId + "To").val("Select");
				}
				$("#" + currentId + "To").attr("disabled", false);
				return;
			}
		}
		/* else{
			if($("#" + currentId + "To").val() == "Closed"){
				$("#" + currentId + "From").val("Closed");
				//$("#" + currentId + "To").attr("disabled","disabled");
				return;
			}else{
				
			}
			/* if($("#" + currentId + "To").val() != "Select" && $("#" + currentId + "To").val() != "Closed"){
				if($("#" + currentId + "From").val() == "Closed"){
					$("#" + currentId + "From").val("Select");
				}
				return;
			} 
		} */
		
	});
});

	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            }
		}
	}
	$("#save").click(function(){
		ie8Trim();
		var siteLocationHoursHidden = '';
		var days='${daysListNames}';
		days=days.replace("[","");
		days=days.replace("]","");
		days=days.replace(" ","");
		var dayarray=days.split(',');

		for (var i = 0; i < dayarray.length; i++) {
			
			var fromId = dayarray[i] + 'From';
			fromId=fromId.replace(" ","");
			var fromVal = $("#" + fromId).val();
			
			var toId = dayarray[i] + 'To';
			toId=toId.replace(" ","");
			var toVal = $("#" + toId).val();						
			siteLocationHoursHidden = siteLocationHoursHidden + dayarray[i] + ',' + fromVal + ',' + toVal + '~';			
		}
		//setting siteLocation Hours in the hidden field 
		$("#siteLocationHoursHidden").val(siteLocationHoursHidden);
		var spokenVal ="";
		var writtenVal="";
		$.each($("input[name='spoken']:checked"), function() {
			var spokenValcurrent = $(this).attr('value');
			if(spokenVal == ""){ 
				spokenVal=spokenValcurrent;
			} else {
				spokenVal=spokenVal+","+spokenValcurrent;
			}			
			
		});
		$.each($("input[name='written']:checked"), function() {
			var  writtenValcurrent = $(this).attr('value');
			
			if(writtenVal == ""){ 
				writtenVal=writtenValcurrent;
			} else {
				writtenVal=writtenVal+","+writtenValcurrent;
			}
			
		});
		otherSpokenLanguage = $("#otherSpokenLanguage").val();
		if(otherSpokenLanguage != "" &&  otherSpokenLanguage !=null){
				if(spokenVal!="")
					{
						spokenVal = spokenVal+","+otherSpokenLanguage;
					}
				else
					{
						
						spokenVal=otherSpokenLanguage;
					}
		}
		
		otherWrittenLanguage = $("#otherWrittenLanguage").val();
		if(otherWrittenLanguage != "" &&  otherWrittenLanguage !=null){ 
			if(writtenVal!="")
				{
					writtenVal = writtenVal+","+otherWrittenLanguage;
				}
			else
				{
					writtenVal =otherWrittenLanguage;
				}
			
		}		
		 $("#spokenLanguages").val(spokenVal);
		 $("#writtenLanguages").val(writtenVal);
	
	});
	jQuery.validator.addMethod("OtherSpokenLanguageCheck", function(value, element, param) {
		ie8Trim();
		otherSpokenLanguage = $("#otherSpokenLanguage").val();
		if(otherSpokenLanguage != null && otherSpokenLanguage != ""){ 
			var languages='${listOflanguageNames}';
			languages=languages.replace("[","");
			languages=languages.replace("]","");
			languages=languages.replace(" ","");
			var languagesArray=languages.split(',');
			
			var found = false;
			var languagesTocompare;
			for (i = 0; i < languagesArray.length && !found; i++) {
				languagesTocompare=languagesArray[i];
				 if (languagesTocompare.toLowerCase().match(otherSpokenLanguage.toLowerCase())) {									
				  found = true;
			  }
			}
			if(found){
				return false;
			}
			else
				{
					return true;
				}
		}
		else
			{
				if($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' && (otherSpokenLanguage == null || otherSpokenLanguage == ""))
				{					
						return false;
				}
				else
					{
						return true;
					}
			}	
	});
	jQuery.validator.addMethod("OtherWrittenLanguageCheck", function(value, element, param) {
		ie8Trim();
		otherWrittenLanguage = $("#otherWrittenLanguage").val();
		if(otherWrittenLanguage != "" || otherWrittenLanguage== null){ 
			
			var languages='${listOfLanguagesWritten}';
			languages=languages.replace("[","");
			languages=languages.replace("]","");
			languages=languages.replace(" ","");
			var languagesArray=languages.split(',');
		
			var found = false;
			var languagesTocompare;
			for (i = 0; i < languagesArray.length && !found; i++) {
				languagesTocompare=languagesArray[i];				
			  if (languagesTocompare.toLowerCase().match(otherWrittenLanguage.toLowerCase())) {				 				
				  found = true;
			  }			
			}			
			if(found){
				return false;
			}
			else
				{
					return true;
				}
		}
		else
		{	
			if($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' && otherWrittenLanguage =="")
			{
				
				return false;
			}
			else
				{
					return true;
				}
		}
	});
	jQuery.validator.addMethod("SpokenLanguageCheck", function(value, element, param) {
		ie8Trim();
		 if(($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' )){
		      return true;
		     }
		var fields = $("input[name='spoken']").serializeArray(); 
 		if (fields.length == 0) 
	    { 
	        otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
	    	if(otherSpokenLanguage == null || otherSpokenLanguage == ""){
	    		return false;
	    	}
	    } 
		return true;
	});

	jQuery.validator.addMethod("otherSpokenLanguageCheckboxCheck", function(value, element, param) {
		ie8Trim();
		
		otherSpokenLanguage = $("#otherSpokenLanguage").val(); 
		if (otherSpokenLanguage != null ) 
	    { 
		    if((otherSpokenLanguage != null || otherSpokenLanguage != "") && !($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	});

	jQuery.validator.addMethod("otherSpokenLanguageSelectCheck", function(value, element, param) {
		ie8Trim();
		
		temp = $("#otherSpokenLanguageCheckbox").attr("checked")=='checked';
		if (temp != false ) 
	    { 
		    if((otherSpokenLanguage == null || otherSpokenLanguage == "") && ($("#otherSpokenLanguageCheckbox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	});
	jQuery.validator.addMethod("WrittenLanguageCheck", function(value, element, param) {
		ie8Trim();
		 if(($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' )){
		      return true;
		     }
		var fields = $("input[name='written']").serializeArray(); 
	    if (fields.length == 0) 
	    { 
	       if(otherWrittenLanguage  == null || otherWrittenLanguage == ""){ 
	    		return false;
	    	}
	    } 
		return true;
	});
		

	jQuery.validator.addMethod("otherWrittenLanguageCheckboxCheck", function(value, element, param) {
		ie8Trim();
		otherWrittenLanguage = $("#otherWrittenLanguage").val(); 
		if (otherWrittenLanguage != null) 
	    { 
	    	if((otherWrittenLanguage != null || otherWrittenLanguage != "") && !($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' )){
	    			return false;
	    	}
	    } 
		return true;
	});

	jQuery.validator.addMethod("otherWrittenLanguageSelectCheck", function(value, element, param) {
		ie8Trim();
		
		temp = $("#otherWrittenLanguageCheckbox").attr("checked")=='checked';
		if (temp != false ) 
	    { 
		    if((otherWrittenLanguage == null || otherWrittenLanguage == "") && ($("#otherWrittenLanguageCheckbox").attr("checked")=='checked' )){
		    		return false;
		    	}
	    } 
		return true;
	});
		

	jQuery.validator.addMethod("checkPrimarySiteName", function(value, element, param) {
		  ie8Trim();
		  primarySiteName = $("#siteLocationName").val(); 
		  return /^[^<^>]*$/i.test(primarySiteName);
	});

var validator = $("#frmaddSite").validate({ 
	onkeyup: false,
	onclick: false,
	ignore : "hidden",
	rules : {
		siteLocationName:{required : true, checkPrimarySiteName : true},
		primaryPhone1 : { numberStartsWithZeroCheck: true},
		primaryPhone3 : { primaryphonecheck : true},
		secondaryPhone1 : { numberStartsWithZeroCheck: true},
		secondaryPhone3 : { secondaryphonecheck : true},
		 primaryEmailAddress : { email : true},
		 "physicalLocation.zip" :{digits:true,PhysicalZipCodecheck:true},
		 "mailingLocation.zip":{required : true, digits  : true, MailingZipCodecheck : true},
		 "mailingLocation.state":{required : true},
		 "mailingLocation.city" : { required: true},
		 "mailingLocation.address1":{required : true},
		 spoken : {SpokenLanguageCheck: true},
		 otherSpokenLanguageCheckbox : {otherSpokenLanguageCheckboxCheck : true, otherSpokenLanguageSelectCheck : true},
		 otherWrittenLanguageCheckbox : {otherWrittenLanguageCheckboxCheck : true, otherWrittenLanguageSelectCheck : true},
		 /*otherSpokenLanguage : {OtherSpokenLanguageCheck: false, languagesSpokenCheck: true},
		 otherWrittenLanguage : {OtherWrittenLanguageCheck: false, languagesWrittenCheck : true},*/
		 written : {WrittenLanguageCheck: true},
		 siteLocationHoursErrorField:{siteLocationHoursHiddenCheck:true,siteLocationHoursGreaterThanCheck:true,siteLocationHoursClosedAndTimeCheck:true}
	},
	messages : {
		siteLocationName: { required : "<span> <em class='excl'>!</em><spring:message code='label.entername' javaScriptEscape='true'/></span>",
			   checkPrimarySiteName : "<span> <em class='excl'>!</em>Enter alphanumeric value for primary site name.</span>" },
		primaryEmailAddress:{email : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidEmail' javaScriptEscape='true'/></span>"},
		primaryPhone1: { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
		primaryPhone3: { primaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePrimaryPhoneNo' javaScriptEscape='true'/></span>"},		
		secondaryPhone1 : { numberStartsWithZeroCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNumberShouldNotStartWith0AllowsOnlyNumbersBetween1-9' javaScriptEscape='true'/></span>"},
		secondaryPhone3: { secondaryphonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSecondaryPhoneNo' javaScriptEscape='true'/></span>"},
		"physicalLocation.zip":{PhysicalZipCodecheck:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"},
	    "mailingLocation.zip" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterZipCode' javaScriptEscape='true'/></span>",
	    MailingZipCodecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterValidZipCode' javaScriptEscape='true'/></span>"},	    
	    "mailingLocation.city" :{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterCity' javaScriptEscape='true'/></span>"},
		"mailingLocation.state":{ required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseSelectState' javaScriptEscape='true'/></span>"},
		"mailingLocation.address1" : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterMailingAddress' javaScriptEscape='true'/></span>"},
		spoken : {SpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateSpokenLanguage' javaScriptEscape='true'/></span>"},
		written : {WrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateWrittenLanguage' javaScriptEscape='true'/></span>"},
		otherSpokenLanguageCheckbox : {otherSpokenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckbox'/></span>",
		otherSpokenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforother'/></span>"},
		otherWrittenLanguageCheckbox : {otherWrittenLanguageCheckboxCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectothercheckboxforwritten'/></span>",
	 	otherWrittenLanguageSelectCheck : "<span> <em class='excl'>!</em><spring:message code='label.selectlangforotherwritten'/></span>"},
 		/*otherSpokenLanguage : {OtherSpokenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherSpokenLanguage' javaScriptEscape='true'/></span>",
	 	languagesSpokenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageSpokenFromDropDown' javaScriptEscape='true'/></span>"},
	 	otherWrittenLanguage : {OtherWrittenLanguageCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateOtherWrittenLanguage' javaScriptEscape='true'/></span>",
		languagesWrittenCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterLanguageWrittenFromDropDown' javaScriptEscape='true'/></span>"},*/
	 	siteLocationHoursErrorField:{siteLocationHoursHiddenCheck:"<span><em class='excl'>!</em><spring:message code='label.validateSiteLocationHours' javaScriptEscape='true'/></span>",
		siteLocationHoursGreaterThanCheck:"<span><em class='excl'>!</em><spring:message code='label.validateClosingHours' javaScriptEscape='true'/></span>",
		siteLocationHoursClosedAndTimeCheck:"<span><em class='excl'>!</em><spring:message code='label.validateSiteLocationHours' javaScriptEscape='true'/></span>"}
	}
	,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

jQuery.validator.addMethod("alphaNumeric", function(value, element, param) {
	ie8Trim();
	 return this.optional(element) || /^[a-z0-9\-]+$/i.test(value);
	 
});


jQuery.validator.addMethod("siteLocationHoursGreaterThanCheck", function(value, element, param) {
	ie8Trim();
	// This validation is added to check if user enters all the Sitelocation hours.
	 var days='${daysListNames}';
		days=days.replace("[","");
		days=days.replace("]","");
		days=days.replace(" ","");
		var dayarray=days.split(',');
		
		for (var i = 0; i < dayarray.length; i++) {
			
			var fromId = dayarray[i] + 'From';
			fromId=fromId.replace(" ","");
			var fromVal = $("#" + fromId).val();
			fromVal = convertTo24Hour(fromVal);
			
			var toId = dayarray[i] + 'To';
			toId=toId.replace(" ","");
			var toVal = $("#" + toId).val();
			toVal = convertTo24Hour(toVal);
			
			if((fromVal.indexOf(':') != -1)&&(toVal.indexOf(':') != -1)){
			
			var starttime = fromVal.split(':');
			var startHour = starttime[0];
			var startMinute = starttime[1];
			//Create date object and set the time to that
			var startTimeObject = new Date();
			startTimeObject.setHours(startHour, startMinute, 00);
			
			var endtime = toVal.split(':');
			var endHour = endtime[0];
			var endMinute = endtime[1];
			//Create date object and set the time to that
			var endTimeObject = new Date();
			endTimeObject.setHours(endHour, endMinute, 00);
				
			//Now compare both the dates
			if(startTimeObject >= endTimeObject)
			{	
				if(!$("#siteLocationName_error label").is(":visible")){
					$(window).scrollTop($("#from-to-day").offset().top - 100);
				}
				return false;
			}
			}
			else if ((fromVal == 'Closed' && toVal != 'Closed') || (fromVal != 'Closed' && toVal == 'Closed')){
				return false;
			}
		}

		return true;
});

function convertTo24Hour(time) {
	
    var hours = parseInt(time.substr(0, 2));
    if(time.indexOf('am') != -1 && hours == 12) {
        time = time.replace('12', '0');
    }
    if(time.indexOf('pm')  != -1 && hours < 12) {
        time = time.replace(hours, (hours + 12));
    }
    return time.replace(/(am|pm)/, '');
}


//If closed is selected in from time and Time is selected in To Time
jQuery.validator.addMethod("siteLocationHoursClosedAndTimeCheck", function(value, element, param) {
	ie8Trim();
	// This validation is added to check if user enters all the Sitelocation hours.
	 var days='${daysListNames}';
		days=days.replace("[","");
		days=days.replace("]","");
		days=days.replace(" ","");
		var dayarray=days.split(',');
		
		for (var i = 0; i < dayarray.length; i++) {
			
			var fromId = dayarray[i] + 'From';
			fromId=fromId.replace(" ","");
			var fromVal = $("#" + fromId).val();
			
			var toId = dayarray[i] + 'To';
			toId=toId.replace(" ","");
			var toVal = $("#" + toId).val();
			
			if( (fromVal == 'Closed' && toVal != 'Closed') || (fromVal != 'Closed' && toVal == 'Closed')){
				if(!$("#siteLocationName_error label").is(":visible")){
					$(window).scrollTop($("#from-to-day").offset().top - 100);
				}
				
				return false;
			}
				
		}
		return true;
});


jQuery.validator.addMethod("siteLocationHoursHiddenCheck", function(value, element, param) {
	ie8Trim();
	// This validation is added to check if user enters all the Sitelocation hours.
	 siteLocationHoursHidden = $("#siteLocationHoursHidden").val().trim();
	 var sitelocationarray = siteLocationHoursHidden.split('~');	
		for (var i = 0; i < sitelocationarray.length-1; i++) {		
			var siteloc = sitelocationarray[i];									
			var slocationarray = siteloc.split(',');						
			//var slocarray = slocationarray[1].split(',');
			if((slocationarray[1] == "") || (slocationarray[2] == ""))
			{		
				if(!$("#siteLocationName_error label").is(":visible")){
					$(window).scrollTop($("#from-to-day").offset().top - 100);
				}	
				return false;
			}			
		}
		return true;
});

jQuery.validator.addMethod("primaryphonecheck", function(value, element, param) {
	ie8Trim();
	primphone1 = $("#primaryPhone1").val().trim(); 
	primphone2 = $("#primaryPhone2").val().trim(); 
	primphone3 = $("#primaryPhone3").val().trim(); 
 
	if((primphone1.length == 0) && (primphone2.length == 0)  && (primphone3.length == 0)) {
		return true;
	}
	if( (primphone1 == "" || primphone2 == "" || primphone3 == "")  || (isNaN(primphone1)) || (primphone1.length < 3 ) || (isNaN(primphone2)) || (primphone2.length < 3 ) || (isNaN(primphone3)) || (primphone3.length < 4 ) || (primphone1 == '000') || (!isPositiveInteger(primphone1)) || (!isPositiveInteger(primphone2)) || (!isPositiveInteger(primphone3))) 
	{
		return false;
	}
	else
	{
		$("#primaryPhoneNumber").val(primphone1 + primphone2 + primphone3);
		return true;
	}	
} );
jQuery.validator.addMethod("numberStartsWithZeroCheck", function(value, element, param) {
	ie8Trim();
	// This is added to check if user has not entered anything into the textbox
	if((value.length == 0)) {
		return true;
	}
	
	// If user has entered anything then test, if entered value is valid
	var firstChar = value.charAt(0);
	if(firstChar == 0) {
			return false;
	} else{
	    return true;
	}
});

jQuery.validator.addMethod("secondaryphonecheck", function(value, element, param) {
	ie8Trim();
	secondaryphone1 = $("#secondaryPhone1").val().trim(); 
	secondaryphone2 = $("#secondaryPhone2").val().trim(); 
	secondaryphone3 = $("#secondaryPhone3").val().trim(); 
 
	if((secondaryphone1.length == 0) && (secondaryphone2.length == 0)  && (secondaryphone3.length == 0)) {
		return true;
	}
	if((secondaryphone1.length < 3 ) || (secondaryphone2.length < 3 )  || (secondaryphone3.length < 4 ) || (isNaN(secondaryphone1)) || (isNaN(secondaryphone2)) || (isNaN(secondaryphone3)) || (secondaryphone1 == '000') || (!isPositiveInteger(secondaryphone1)) || (!isPositiveInteger(secondaryphone2)) || (!isPositiveInteger(secondaryphone3)))
	{
		return false;
	} else {
		$("#secondaryPhoneNumber").val(secondaryphone1 + secondaryphone2 + secondaryphone3);		
		return true;
	}		
} );
// function shiftbox(element,nextelement){
// 	ie8Trim();
// 	maxlength = parseInt(element.getAttribute('maxlength'));
// 	if(element.value.length == maxlength){
// 		nextelement = document.getElementById(nextelement);
// 		nextelement.focus();
// 	}
// }
jQuery.validator.addMethod("PhysicalZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#physicalLocation_zip").val().trim(); 
	if(zip == "")
	{
			return true; 
	}
	if((isNaN(zip) || (zip.length < 5 ) || (zip == "00000")))
	{
			return false; 
	}
	return true;
});
jQuery.validator.addMethod("MailingZipCodecheck", function(value, element, param) {
	ie8Trim();
	zip = $("#mailingLocation_zip").val().trim(); 
	if((zip == "")  || (isNaN(zip) || (zip.length < 5 ) || (zip == "00000") )){ 
		return false; 
	}
	return true;
});

function isPositiveInteger(s)
{
    return /^[0-9]*$/.test(s);
}

$('#physicalAddressCheck').change(function(){
	if($(this).is(":checked")) {
		$('.physicalmailingAddress').hide();
		$("#physicalAddressCheckFlag").val("1");
		
		var strMailingAddress1 = $("#mailingLocation_address1").val(),
			strMailingSuite = $("#mailingLocation_address2").val(),
			strMailingState = $("#mailingLocation_state").val(),
			strMailingZip = $("#mailingLocation_zip").val(),
			strcity = $("#mailingLocation_city").val();
		
		$("#physicalLocation_address1").val(strMailingAddress1);
		$("#physicalLocation_address2").val(strMailingSuite);
		$("#physicalLocation_state").val(strMailingState);
		$("#physicalLocation_zip").val(strMailingZip);
		$("#physicalLocation_city").val(strcity);
		
	}else{
		$('.physicalmailingAddress').show();
		$("#physicalAddressCheckFlag").val("0");
		$("#physicalLocation_address1").val("");
		$("#physicalLocation_address2").val("");
		$("#physicalLocation_state").val("");	
		$("#physicalLocation_zip").val("");		
		$("#physicalLocation_city").val("");
	}
});


$('#physicalLocation_address1').focusin(function() {
	
	if(($('#physicalLocation_address2').val())==="Address Line 2"){
		$('#physicalLocation_address2').val('');
	}



});

$('#mailingLocation_address1').focusin(function() {

if(($('#mailingLocation_address2').val())==="Address Line 2"){
	$('#mailingLocation_address2').val('');
}



});

 //load the jquery chosen plugin 
	function loadLanguages() {
		$("#otherSpokenLanguage").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${otherSpokenLanguage}';
		for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherSpokenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	     
	     $('#otherSpokenLanguage').trigger("liszt:updated");
	   }

	function checkCounties(county){
	     var counties='${otherSpokenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguages();
	});
	


 //load the jquery chosen plugin 
	function loadLanguagesForWritten() {
		$("#otherWrittenLanguage").html('');
		var respData = $.parseJSON('${languagesLists}');
		var counties='${otherWrittenLanguage}';
	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkCounties1(respData[key]);
		      }
		      if(isSelected){
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#otherWrittenLanguage').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }
	    $('#otherWrittenLanguage').trigger("liszt:updated");
	   
	   }

	function checkCounties1(county){
	     var counties='${otherWrittenLanguage}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;
			    	
			     }
			 }
		   return found;
    }
	
	$(document).ready(function() {
		loadLanguagesForWritten();
		$("#otherSpokenLanguage").attr("tabindex","0");
	});
	
</script>

	