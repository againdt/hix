<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
				<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
				<li><spring:message  code="pgheader.update.companyProfile"/></li>
			</ul><!--page-breadcrumb ends-->
		  <%-- <h1><issuerLogo:view issuerId="${issuerObj.id}"/> ${issuerObj.name}</h1> --%>
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>" class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="companyProfile"/>
				</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message code="pgheader.update.companyProfile"/></h4>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/company/profile/edit/${encIssuerId}"/>"><spring:message  code="label.edit"/></a>
                </div>
		
				<form class="form-horizontal" id="frmIssuerAcctComProInfo" name="frmIssuerAcctComProInfo" action="#" method="POST">
					<table class="table table-border-none">		
						<tr>
							<th class="txt-right span4"><spring:message  code="label.companyLegalName"/></th>
							<td><strong>${issuerObj.companyLegalName}</strong></td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message code="label.companyLogo"/><a class="ttclass" rel="tooltip" href="#" data-original-title="JPEG or PNG file of the Issuer logo"><i class="icon-question-sign"></i></a></th>
							<td>
								<strong>
									<c:if test="${not empty companyLogoExist}">	
										<a href="<c:url value="/admin/issuer/downloadlogo/${issuerObj.hiosIssuerId}"/>" target="_blank" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a>
									</c:if>
								</strong>
							</td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message  code="label.stateOfDomicile"/></th>
							<td><strong>${issuerObj.stateOfDomicile}</strong></td>
						</tr>
						<c:if test="${exchangeType == 'PHIX'}">
						<tr>
							<th class="txt-right span4"><spring:message  code="label.nationalProducerNumber"/></th>
							<td><strong>${issuerObj.nationalProducerNumber}</strong></td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message  code="label.agentFirstName"/></th>
							<td><strong>${issuerObj.agentFirstName}</strong></td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message  code="label.agentLastName"/></th>
							<td><strong>${issuerObj.agentLastName}</strong></td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message  code="label.pmdisclaimer"/></th>
							<td><strong>${issuerObj.disclaimer}</strong></td>
						</tr>
						</c:if>
					</table>
					
					<div class="header">
	                   <h4><spring:message  code="pgheader.update.companyAddress"/></h4>
	                </div>
					
					
					<table class="table table-border-none">
						<tr>
							<th class="txt-right span4"><spring:message  code="label.addressLine1"/></th>
							<td><strong>${issuerObj.companyAddressLine1}</strong></td>
						</tr>
						
						<tr>
							<th class="txt-right span4"><spring:message  code="label.addressLine2"/></th>
							<td><strong>${issuerObj.companyAddressLine2}</strong></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.city"/></th>
							<td><strong>${issuerObj.companyCity}</strong></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.state"/></th>
							<td><strong>${issuerObj.companyState}</strong></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.zipCode"/></th>
							<td><strong>${issuerObj.companyZip}</strong></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.issuerWebsite"/></th>
							<td><a href="${issuerObj.siteUrl}"><strong>${issuerObj.siteUrl}</strong></a></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.consumerFacingWebSiteURL"/></th>
							<td><a href="${issuerObj.companySiteUrl}"><strong>${issuerObj.companySiteUrl}</strong></a></td>
						</tr>
						<tr>
							<th class="txt-right span4"><spring:message  code="label.paymentUrl"/></th>
							<td><a href="${issuerObj.paymentUrl}"><strong>${issuerObj.paymentUrl}</strong></a></td>
						</tr>
					</table>

<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="comp-legal-name"><spring:message  code="label.companyLegalName"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyLegalName}</strong></div> --%>
<!-- 					</div> -->
<!-- <!-- 					end of control-group -->
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="comp-logo"><spring:message  code="label.companyLogo"/><a class="ttclass" rel="tooltip" href="#" data-original-title="Company logo"><i class="icon-question-sign"></i></a></label> --%>
<!-- 						<div class="controls"> -->
<%-- 						<c:if test="${not empty issuerObj.companyLogo}">	 --%>
<%-- 							<a href="<c:url value="/ecm/filedownloadbyid?uploadedFileId=${issuerObj.companyLogo}"/>" class="btn btn-small"><i class="icon-eye-open"></i> <spring:message  code="label.view"/></a> --%>
<%-- 						</c:if> --%>
<!-- 						</div> -->
<!-- 					</div> -->
<!-- <!-- 					end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="state"><spring:message  code="label.stateOfDomicile"/><a class="ttclass" rel="tooltip" href="#" data-original-title="State of Domicile"><i class="icon-question-sign"></i></a></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.stateOfDomicile}</strong></div> --%>
<!-- 					</div> -->
<!-- <!-- 					end of control-group --> 
                    
<%--                     <h4 class="lightgray"><spring:message  code="pgheader.update.companyAddress"/></h4> --%>
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="bank-address-1"><spring:message  code="label.addressLine1"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyAddressLine1}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="bank-address-2"><spring:message  code="label.addressLine2"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyAddressLine2}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="bank-city"><spring:message  code="label.city"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyCity}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                    
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="bank-state"><spring:message  code="label.state"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyState}</strong></div> --%>
<!-- 					</div>end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="zipcode"><spring:message  code="label.zipCode"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companyZip}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="issuer-website"><spring:message  code="label.issuerWebsite"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.siteUrl}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                    
<!--                     <div class="control-group"> -->
<%-- 						<label class="control-label" for="facing-website"><spring:message  code="label.consumerFacingWebSiteURL"/></label> --%>
<%-- 						<div class="controls paddingT5"><strong>${issuerObj.companySiteUrl}</strong></div> --%>
<!-- 					</div> -->
<!-- 					end of control-group -->
                   
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->

 </div>
  
<script type="text/javascript">
	$('.ttclass').tooltip();
</script>
