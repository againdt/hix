package com.getinsured.ahbx.broker.controller;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;
import javax.xml.ws.http.HTTPException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.WebServiceIOException;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.ahbx.broker.bookofbusiness.AgentAssisterBOBDetailsRequest;
import com.getinsured.ahbx.broker.bookofbusiness.AgentAssisterBOBDetailsResponse;
import com.getinsured.ahbx.broker.bookofbusiness.AssociatedIndividuals;
import com.getinsured.ahbx.broker.bookofbusiness.IndividualDetails;
import com.getinsured.ahbx.broker.bookofbusiness.RetrieveAssociatedIndividualDetails;
import com.getinsured.ahbx.broker.bookofbusiness.RetrieveAssociatedIndividualDetailsResponse;
import com.getinsured.ahbx.broker.bookofbusiness.RetrieveAssociatedIndividualDetailsResponseDTO;
import com.getinsured.ahbx.broker.employerbookofbusiness.EmployerBOBDetailsResponse;
import com.getinsured.ahbx.broker.employerbookofbusiness.EmployersBOBStatusResponse;
import com.getinsured.ahbx.broker.employerbookofbusiness.RetrieveEmployerBOBDetails;
import com.getinsured.ahbx.broker.employerbookofbusiness.RetrieveEmployerBOBDetailsResponse;
import com.getinsured.ahbx.broker.employerbookofbusiness.RetrieveEmployersBOBStatus;
import com.getinsured.ahbx.broker.employerbookofbusiness.RetrieveEmployersBOBStatusResponse;
import com.getinsured.ahbx.broker.individualbookofbusiness.IndividualBOBDetailsResponse;
import com.getinsured.ahbx.broker.individualbookofbusiness.IndividualsBOBStatusResponse;
import com.getinsured.ahbx.broker.individualbookofbusiness.RetrieveIndividualBOBDetails;
import com.getinsured.ahbx.broker.individualbookofbusiness.RetrieveIndividualBOBDetailsResponse;
import com.getinsured.ahbx.broker.individualbookofbusiness.RetrieveIndividualsBOBStatus;
import com.getinsured.ahbx.broker.individualbookofbusiness.RetrieveIndividualsBOBStatusResponse;
import com.getinsured.ahbx.broker.individualbookofbusinessdetails.HouseholdMembers;
import com.getinsured.ahbx.broker.individualbookofbusinessdetails.IndvHouseHoldDetails;
import com.getinsured.ahbx.broker.individualbookofbusinessdetails.IndvHouseHoldEligDetail;
import com.getinsured.ahbx.broker.individualbookofbusinessdetails.IndvHouseHoldEligDetails;
import com.getinsured.ahbx.broker.service.ExternalEmployerService;
import com.getinsured.ahbx.broker.service.ExternalIndividualService;
import com.getinsured.ahbx.util.AHBXUtils;
import com.getinsured.ahbx.util.GhixAhbxConstants;
import com.getinsured.hix.dto.broker.BrokerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.BrokerBOBSearchParamsDTO;
import com.getinsured.hix.dto.broker.BrokerBobResponseDTO;
import com.getinsured.hix.dto.broker.EmployerBOBDetailsDTO;
import com.getinsured.hix.dto.broker.EmployerIndividualBOBDTO;
import com.getinsured.hix.dto.broker.HouseholdEligibilityInformationDTO;
import com.getinsured.hix.dto.broker.IndividualDTO;
import com.getinsured.hix.dto.broker.IndividualHouseholdDTO;
import com.getinsured.hix.dto.consumer.ssap.SsapApplicantDTO;
import com.getinsured.hix.model.EmployersBOBResponse;
import com.getinsured.hix.model.ExternalEmployer;
import com.getinsured.hix.model.ExternalIndividual;
import com.getinsured.hix.model.IndividualBOBresponse;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.webclient.SoapWebClient;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * The controller class that receives Rest requests from ghix-web. It then sends
 * this information to AHBX by consuming their broker web services.
 */
@Controller
@RequestMapping(value = "/broker")
public class BrokerWsController {
	private static final Logger LOGGER = Logger.getLogger(BrokerWsController.class);

	@Autowired
	private WebServiceTemplate employerWebServiceTemplate;

	@Autowired
	private ExternalEmployerService externalEmployerService;

	@Autowired
	private WebServiceTemplate individualWebServiceTemplate;
	
	@Autowired
	private WebServiceTemplate agentAssisterBobWebServiceTemplate;
	
	@Autowired
	private WebServiceTemplate individualBobDetailsWebServiceTemplate;
	
	@Autowired
	private SoapWebClient soapWebClient;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	private ExternalIndividualService externalIndividualService;
	// For IND51-52
	private static final int INDIVIDUAL_SUCCESS_RESPONSE_CODE = 200;
	// For IND49-50
	private static final int EMPLOYER_SUCCESS_RESPONSE_CODE = 200;

	private static final int EMPLOYER_NO_ELIGIBILITY_CODE = 403;
	private static final String ELIGIBILITY = "Eligibility";
	private static final String HOUSEHOLD = "Household";


	/**
	 * To test {@link BrokerWsController}
	 * 
	 * @return welcome message
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() throws HTTPException, Exception {
		LOGGER.info("Welcome to Enrollment WebService module");
		return "Welcome to Broker SOAP Service module";
	}

	/**
	 * Invokes AHBX EndPoint to retrieve Employers Book of Business Status
	 * 
	 * @param employerIdList
	 *            the object encapsulating list of employer ids
	 * @return marshaled EmployersBOBResponse from AHBX response
	 */
	@RequestMapping(value = "/retrieveemployersbobstatus", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEmployersBOBStatus(@RequestBody EmployerIndividualBOBDTO employerIndividualBOBDTO) {
		RetrieveEmployersBOBStatusResponse response = null;
		RetrieveEmployersBOBStatus retrieveEmployersBOBStatus;
		List<EmployersBOBStatusResponse> listOfEmployersBOBStatusResponse;
		JAXBElement<RetrieveEmployersBOBStatusResponse> employersBOBStatusResponseFromAHBX = null;
		EmployersBOBResponse employersBOBResponse = new EmployersBOBResponse();

		if (employerIndividualBOBDTO.getListOfIDs() != null && employerIndividualBOBDTO.getListOfIDs().size() != 0) {

/*			LOGGER.info("IND49:Invoking External Employer BOB Status service. Sending following data to AHBX: "
					+ employerIndividualBOBDTO.toString());
*/
			try {
				com.getinsured.ahbx.broker.employerbookofbusiness.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.employerbookofbusiness.ObjectFactory();
				retrieveEmployersBOBStatus = objectFactory.createRetrieveEmployersBOBStatus();
				retrieveEmployersBOBStatus.setEmployerRequest(employerIndividualBOBDTO.getListOfIDs());

				JAXBElement<RetrieveEmployersBOBStatus> requestElement = objectFactory
						.createRetrieveEmployersBOBStatus(retrieveEmployersBOBStatus);

				// Calling AHBX EndPoint to External Employer BOB Status
				employerWebServiceTemplate.setDefaultUri(GhixAhbxConstants.retrieveEmployerBOBEndPointImplPort);

				// employerWebServiceTemplate
				// .setDefaultUri("http://localhost:8088/mockEmployerBookOfBusinessEndPointImplServiceSoapBinding?wsdl");

				/*employersBOBStatusResponseFromAHBX = (JAXBElement<RetrieveEmployersBOBStatusResponse>) employerWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				employersBOBStatusResponseFromAHBX = (JAXBElement<RetrieveEmployersBOBStatusResponse>) soapWebClient
						.send(requestElement, GhixAhbxConstants.retrieveEmployerBOBEndPointImplPort, "IND49",
								employerWebServiceTemplate);

				response = employersBOBStatusResponseFromAHBX.getValue();
			} catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND49:Exception occurred while retrieving Employer BOB Status from AHBX: "
						, webServiceIOException);
				employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				employersBOBResponse.setErrMsg(webServiceIOException.getMessage());
			} catch (Exception exception) {
				LOGGER.error("IND49:Exception occurred while retrieving Employer BOB Status from AHBX: "
						, exception);
				employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				employersBOBResponse.setErrMsg(exception.getMessage());
			}

			// processing response received from AHBX
			if (response.getEmployer() != null && !response.getEmployer().isEmpty()) {

				listOfEmployersBOBStatusResponse = response.getEmployer();

				List<EmployerBOBDetailsDTO> employerBOBDetailList = new ArrayList<EmployerBOBDetailsDTO>();

				// Updating data in GI Database
				for (EmployersBOBStatusResponse employerResponse : listOfEmployersBOBStatusResponse) {

					EmployerBOBDetailsDTO employerBOBDetailsDTO = new EmployerBOBDetailsDTO();

					// Set the response code and response description for each
					// detail object
					employerBOBDetailsDTO.setResponseCode(employerResponse.getResponseCode());
					employerBOBDetailsDTO.setResponseDescription(employerResponse.getResponseDescription());

/*					LOGGER.info("IND49:External Employer BOB Status service call successful. Received following response from AHBX: "
							+ employerResponse.toString());
*/
					int responseCode = employerResponse.getResponseCode();
					LOGGER.debug("IND49: responseCode:" + responseCode);

					if (responseCode == EMPLOYER_SUCCESS_RESPONSE_CODE || responseCode == EMPLOYER_NO_ELIGIBILITY_CODE) {

						LOGGER.info("IND49:Response Code is 200 or 403. Updating External Employer Status in DB");

						try {
							updateExternalEmployerStatus(employerResponse);
							employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
						} catch (Exception exception) {
							LOGGER.error("IND49:Exception occurred while updating External Employer table : "
									, exception);
							employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
							employersBOBResponse.setErrMsg(exception.getMessage());
						}
					} else {
						LOGGER.info("IND49:Response Code is" + employerResponse.getResponseCode()
								+ "Response Description" + employerResponse.getResponseDescription());
					}

					employerBOBDetailList.add(employerBOBDetailsDTO);
				}

				employersBOBResponse.setEmployersBOBDetailsDTOList(employerBOBDetailList);

			} else {
				LOGGER.error("IND49:External Employer Status response is null. Could not update Employer Status in DB");
			}
		} else {
			LOGGER.error("IND49:EmployerIndividualBOBDTO Object is null. Unable to retrieve external employer bob status from AHBX");

			employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			employersBOBResponse.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
			employersBOBResponse.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
		}

		// Marshaling the response before sending to ghix-web
		return AHBXUtils.marshal(employersBOBResponse);
	}

	/**
	 * Invokes AHBX EndPoint to retrieve Employers Book of Business Details
	 * 
	 * @param employerIdList
	 *            the object encapsulating list of employer ids
	 * @return marshaled EmployersBOBResponse from AHBX response
	 */
	@RequestMapping(value = "/retrieveemployerbobdetails", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEmployerBOBDetails(@RequestBody EmployerIndividualBOBDTO employerIndividualBOBDTO) {
		RetrieveEmployerBOBDetailsResponse response = null;
		EmployersBOBResponse employersBOBResponse = new EmployersBOBResponse();
		JAXBElement<RetrieveEmployerBOBDetailsResponse> employerBOBDetailsResponseFromAHBX = null;
		RetrieveEmployerBOBDetails retrieveEmployerBOBDetails;
		EmployerBOBDetailsResponse employerBOBDetailsResponse;
		ExternalEmployer externalEmployer = null;

		if (employerIndividualBOBDTO != null && employerIndividualBOBDTO.getId() != 0) {

/*			LOGGER.info("IND50:Invoking External Employer BOB service. Sending following data to AHBX: "
					+ employerIndividualBOBDTO.toString());
*/
			try {
				// creating AHBX request object
				com.getinsured.ahbx.broker.employerbookofbusiness.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.employerbookofbusiness.ObjectFactory();
				retrieveEmployerBOBDetails = objectFactory.createRetrieveEmployerBOBDetails();
				retrieveEmployerBOBDetails.setEmployerID(employerIndividualBOBDTO.getId());
				JAXBElement<RetrieveEmployerBOBDetails> requestElement = objectFactory
						.createRetrieveEmployerBOBDetails(retrieveEmployerBOBDetails);

				// Calling AHBX EndPoint to fetch External Employer BOB Details
				employerWebServiceTemplate.setDefaultUri(GhixAhbxConstants.retrieveEmployerBOBEndPointImplPort);

				// employerWebServiceTemplate
				// .setDefaultUri("http://localhost:8088/mockEmployerBookOfBusinessEndPointImplServiceSoapBinding?wsdl");

				/*employerBOBDetailsResponseFromAHBX = (JAXBElement<RetrieveEmployerBOBDetailsResponse>) employerWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				employerBOBDetailsResponseFromAHBX = (JAXBElement<RetrieveEmployerBOBDetailsResponse>) soapWebClient
						.send(requestElement, GhixAhbxConstants.retrieveEmployerBOBEndPointImplPort, "IND50",
								employerWebServiceTemplate);

				response = employerBOBDetailsResponseFromAHBX.getValue();
			} catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND50:Exception occurred while retrieving Employer BOB Details from AHBX: "
						, webServiceIOException);
				employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				employersBOBResponse.setErrMsg(webServiceIOException.getMessage());
			} catch (Exception exception) {
				LOGGER.error("IND50:Exception occurred while retrieving Employer BOB Details from AHBX: "
						, exception);
				employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				employersBOBResponse.setErrMsg(exception.getMessage());
			}

			// processing response received from AHBX
			if (response != null && response.getEmployerdetails() != null) {
				employerBOBDetailsResponse = response.getEmployerdetails();

				employersBOBResponse
						.setResponseCode(employerBOBDetailsResponse.getResponseCode() != null ? employerBOBDetailsResponse
								.getResponseCode().intValue() : null);
				employersBOBResponse.setResponseDescription(employerBOBDetailsResponse.getResponseDescription());

/*				LOGGER.info("IND50:External Employer BOB Details service call successful. Received following response from AHBX: "
						+ employerBOBDetailsResponse.toString());
*/
				try {
					int responseCode = employerBOBDetailsResponse.getResponseCode().intValue();
					LOGGER.debug("IND50: responseCode:" + responseCode);

					// Saves external employer data in GI Database
					if (responseCode == EMPLOYER_SUCCESS_RESPONSE_CODE || responseCode == EMPLOYER_NO_ELIGIBILITY_CODE) {

						// Populates ExternalEmployer object with response
						// received from AHBX
						ExternalEmployer existingExternalEmployer = externalEmployerService.findByExternalEmployerId(employerBOBDetailsResponse.getEmployerID());
						
						if(existingExternalEmployer!=null){
							externalEmployer = createExternalEmployerDetail(employerBOBDetailsResponse, existingExternalEmployer);
						} else {
							externalEmployer = createExternalEmployerDetail(employerBOBDetailsResponse, new ExternalEmployer());
						}

						LOGGER.info("IND50:External Employer BOB Details response code is 200 or 403. Inserting Employer details into DB");

						saveExternalEmployerDetail(externalEmployer);
						employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
					} else {
						LOGGER.info("IND50:Response Code is" + employerBOBDetailsResponse.getResponseCode()
								+ "Response Description is" + employerBOBDetailsResponse.getResponseDescription());
					}
				} catch (Exception exception) {
					LOGGER.error("IND50:Exception occurred while saving data to GI Database : "
							, exception);

					employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_HANDLING_FAILURE);
					employersBOBResponse.setErrMsg(exception.getMessage());
				}
			} else {
				LOGGER.error("IND50:External Employer BOB Details response is null. Could not insert Employer Details into DB");
			}
		} else {
			LOGGER.error("IND50:EmployerIndividualBOBDTO Object is null. Unable to retrieve external employer bob detail from AHBX");

			employersBOBResponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			employersBOBResponse.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
			employersBOBResponse.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
		}

		// Marshaling the response before sending to ghix-web
		return AHBXUtils.marshal(employersBOBResponse);
	}

	/**
	 * This method creates External Employer Entity and populate data
	 * 
	 * @param employerBOBDetailsResponse
	 *            response received from AHBX
	 * @return ExternalEmployer object with values populated from AHBX response
	 * @throws Exception
	 *             throws exception if data type conversion fails
	 */
	private ExternalEmployer createExternalEmployerDetail(EmployerBOBDetailsResponse employerBOBDetailsResponse, ExternalEmployer externalEmployer)
			throws Exception {

		externalEmployer.setId(employerBOBDetailsResponse.getEmployerID());
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getApplicationEndDate())) {
			externalEmployer.setApplicationEndDate(AHBXUtils.convertUtilToSQLDate(AHBXUtils
					.formatDate(employerBOBDetailsResponse.getApplicationEndDate())));
		}
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getApplicationInitiatedDate())) {
			externalEmployer.setApplicationInitiatedDate(AHBXUtils.convertUtilToSQLDate(AHBXUtils
					.formatDate(employerBOBDetailsResponse.getApplicationInitiatedDate())));
		}
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getApplicationSubmitDate())) {
			externalEmployer.setApplicationSubmitDate(AHBXUtils.convertUtilToSQLDate(AHBXUtils
					.formatDate(employerBOBDetailsResponse.getApplicationSubmitDate())));
		}
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getApplicationWithdrawalDate())) {
			externalEmployer.setApplicationWithdrawalDate(AHBXUtils.convertUtilToSQLDate(AHBXUtils
					.formatDate(employerBOBDetailsResponse.getApplicationWithdrawalDate())));
		}

		externalEmployer
				.setAverageAnnualSalary(employerBOBDetailsResponse.getAverageAnnualSalary() != null ? employerBOBDetailsResponse
						.getAverageAnnualSalary().floatValue() : null);

		externalEmployer.setName(employerBOBDetailsResponse.getBusinessLegalName());
		externalEmployer.setCity(employerBOBDetailsResponse.getContactAddressCity());
		externalEmployer.setState(employerBOBDetailsResponse.getContactAddressState());
		externalEmployer.setCountry(employerBOBDetailsResponse.getContactAddressCounty());
		externalEmployer.setAddress1(employerBOBDetailsResponse.getContactAddressFirstLine());
		externalEmployer.setAddress2(employerBOBDetailsResponse.getContactAddressSecondLine());
		externalEmployer.setZip(employerBOBDetailsResponse.getContactAddressZipCode());
		externalEmployer.setContactEmail(employerBOBDetailsResponse.getContactEmailID());
		externalEmployer.setContactFirstName(employerBOBDetailsResponse.getContactFirstName());
		externalEmployer.setContactLastName(employerBOBDetailsResponse.getContactLastName());
		externalEmployer.setContactNumber(employerBOBDetailsResponse.getContactPhoneNumber());
/*		externalEmployer.setEligibilityStatus(employerBOBDetailsResponse.getEligibilityStatus());
		externalEmployer.setEnrollmentStatus(employerBOBDetailsResponse.getEnrollmentStatus());
*/		externalEmployer.setCommunicationPref(employerBOBDetailsResponse.getPreferedCommunicationMethod());
		externalEmployer
				.setEnrolleesTotalCount(employerBOBDetailsResponse.getEnrolleesTotalCount() != null ? employerBOBDetailsResponse
						.getEnrolleesTotalCount().intValue() : null);
		externalEmployer
				.setTotalEmployees(employerBOBDetailsResponse.getTotalEmployees() != null ? employerBOBDetailsResponse
						.getTotalEmployees().intValue() : null);
		externalEmployer
				.setFullTimeEmp(employerBOBDetailsResponse.getTotalFullTimeEmployees() != null ? employerBOBDetailsResponse
						.getTotalFullTimeEmployees().intValue() : null);

		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getStateEIN())) {
			externalEmployer.setStateEIN(Integer.valueOf(employerBOBDetailsResponse.getStateEIN()));
		}
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getFederalEIN())) {
			externalEmployer.setFederalEIN(Integer.valueOf(employerBOBDetailsResponse.getFederalEIN()));
		}
		externalEmployer.setReportingName(employerBOBDetailsResponse.getReportingName());
		externalEmployer.setPrimaryWorkSiteAddressLine1(employerBOBDetailsResponse.getPrimaryWorksiteAddressLine1());
		externalEmployer.setPrimaryWorkSiteAddressLine2(employerBOBDetailsResponse.getPrimaryWorksiteAddressLine2());
		externalEmployer.setPrimaryWorkSiteCity(employerBOBDetailsResponse.getPrimaryWorksiteCityName());
		externalEmployer.setPrimaryWorkSiteState(employerBOBDetailsResponse.getPrimaryWorksiteStateCode());
		if (!AHBXUtils.isEmpty(employerBOBDetailsResponse.getPrimaryWorksiteZipCode())) {
			externalEmployer
			.setPrimaryWorkSiteZip(Integer.parseInt(employerBOBDetailsResponse.getPrimaryWorksiteZipCode()));
		}
		
		externalEmployer.setOrganizationType(employerBOBDetailsResponse.getOrganizationType());
		externalEmployer.setBenchMarkPlan(employerBOBDetailsResponse.getBenchmarkPlan());
		externalEmployer.setPlanTier(employerBOBDetailsResponse.getPlanTier());
		externalEmployer.setLocations(employerBOBDetailsResponse.getLocations());

		return externalEmployer;
	}

	/**
	 * This method will save given data into External_Employer table
	 * 
	 * @param externalEmployer
	 *            the object encapsulating external employer fetched from AHBX
	 */
	private void saveExternalEmployerDetail(ExternalEmployer externalEmployer) {

		ExternalEmployer existingExternalEmployer = externalEmployerService.findByExternalEmployerId(externalEmployer
				.getId());

		if (existingExternalEmployer != null) {
			updateExternalEmployer(externalEmployer, existingExternalEmployer.getSerialID());
		} else {
			externalEmployerService.save(externalEmployer);
		}
	}

	/**
	 * This method will update the status in External_Employer table
	 * 
	 * @param response
	 *            the object encapsulating latest external employer status data
	 *            fetched from AHBX
	 * @throws Exception 
	 */
	private void updateExternalEmployerStatus(EmployersBOBStatusResponse response) throws Exception {
		ExternalEmployer externalEmployer = new ExternalEmployer();
		externalEmployer.setId(response.getEmployerID());
		if(!AHBXUtils.isEmpty(response.getEligibilityStatus())){
			externalEmployer.setEligibilityStatus(response.getEligibilityStatus());
		}
		if(!AHBXUtils.isEmpty(response.getEnrollmentStatus())){
			externalEmployer.setEnrollmentStatus(response.getEnrollmentStatus());
		}
		
		externalEmployer.setName(response.getBuisnessLegalName());
		externalEmployer.setEnrolleesTotalCount(response.getEnrolleesTotalCount());

		if (response.getEffectiveDate() != null) {
			externalEmployer.setEffectiveDate(AHBXUtils.convertUtilToSQLDate(AHBXUtils.formatDate(response
					.getEffectiveDate())));
		}
		externalEmployerService.update(externalEmployer);
	}

	/**
	 * This method will update existing record irrespective of any changes made
	 * or not
	 * 
	 * @param externalEmployer
	 *            the object encapsulating latest external employer data fetched
	 *            from AHBX
	 * @param id
	 *            external employer id
	 */
	private void updateExternalEmployer(ExternalEmployer externalEmployer, long id) {
		externalEmployer.setSerialID(id);
		externalEmployerService.save(externalEmployer);
	}

	/**
	 * Invokes AHBX EndPoint to retrieve Individuals Book of Business Details
	 * 
	 * @param individualId
	 * @return marshaled IndividualBOBResponse from AHBX response
	 */

	@RequestMapping(value = "/retrieveindividualbobdetails", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveIndividualBOBDetails(@RequestBody IndividualDTO individualdto) {

		RetrieveIndividualBOBDetailsResponse response = null;
		IndividualBOBDetailsResponse individualresponse = null;
		RetrieveIndividualBOBDetails retrieveIndividualBOBDetails = null;
		JAXBElement<RetrieveIndividualBOBDetailsResponse> individualBOBDetailsResponseFromAHBX = null;
		IndividualBOBresponse individualBOBresponse = new IndividualBOBresponse();
		if (individualdto != null) {

/*			LOGGER.info("IND52:Invoking Individual BOB Details service. Sending following data to AHBX as RequestParameter: "
					+ individualdto.getIndividualid());
*/
			try {
				com.getinsured.ahbx.broker.individualbookofbusiness.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.individualbookofbusiness.ObjectFactory();
				retrieveIndividualBOBDetails = objectFactory.createRetrieveIndividualBOBDetails();
				retrieveIndividualBOBDetails.setIndividualID(individualdto.getIndividualid());
				//LOGGER.debug("IND52:Individual Id" + individualdto.getIndividualid());
				JAXBElement<RetrieveIndividualBOBDetails> requestElement = objectFactory
						.createRetrieveIndividualBOBDetails(retrieveIndividualBOBDetails);

				// Calling AHBX EndPoint
				individualWebServiceTemplate.setDefaultUri(GhixAhbxConstants.retrieveIndividualBOBEndPointImplPort);
			/*	individualBOBDetailsResponseFromAHBX = (JAXBElement<RetrieveIndividualBOBDetailsResponse>) individualWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				individualBOBDetailsResponseFromAHBX = (JAXBElement<RetrieveIndividualBOBDetailsResponse>) soapWebClient
						.send(requestElement, GhixAhbxConstants.retrieveIndividualBOBEndPointImplPort, "IND52",
								individualWebServiceTemplate);

				response = individualBOBDetailsResponseFromAHBX.getValue();

				if (response != null && response.getIndividualdetails() != null) {

					individualresponse = response.getIndividualdetails();
					individualBOBresponse = setIndividualBOBResponse(individualresponse);

/*					LOGGER.info("IND52:Individual BOB Details service call successful. Received following response from AHBX:"
							+ individualBOBresponse.toString());
*/
					try {
						if (individualresponse.getResponseCode() == INDIVIDUAL_SUCCESS_RESPONSE_CODE) {

							LOGGER.info("IND52:Individual BOB Details response code is 200. Inserting Individual details into DB");
							ExternalIndividual ind = externalIndividualService.findByExternalIndividualID(individualresponse.getIndividualID());
							if(ind!=null){
								saveIndividualDetail(createIndividualDetail(individualresponse, ind));
							} else {
								ind = new ExternalIndividual();
								saveIndividualDetail(createIndividualDetail(individualresponse, ind));
							}
							
							individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);

						} else {
							LOGGER.info("IND52:Response Code" + individualresponse.getResponseCode()
									+ "Response Description" + individualresponse.getResponseDesc() + "IND52");
						}

					} catch (Exception exception) {
						LOGGER.error("IND52:Exception occurred while saving data to GI Database : "
								, exception);

						individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_HANDLING_FAILURE);
						individualBOBresponse.setErrMsg(exception.getMessage());
					}
				} else {
					LOGGER.error("IND52:Individual response object is null. Unable to insert Individual Details into DB");
				}
			} catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("Exception occurred: " , webServiceIOException);
				individualBOBresponse.setErrMsg(webServiceIOException.getMessage());
				individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);

			} catch (Exception exception) {

				LOGGER.error("Exception occurred: " , exception);
				individualBOBresponse.setErrMsg(exception.getMessage());
				individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			}
		} else {
			LOGGER.error("IND52:Individualdto Object is null. Unable to retrieve external individual bob detail from AHBX");
		}
		return AHBXUtils.marshal(individualBOBresponse);
	}

	/**
	 * This method will save/update Individual record into the external
	 * individual table
	 * 
	 * @param externalIndividual
	 */
	private void saveIndividualDetail(ExternalIndividual externalIndividual) {
		ExternalIndividual ind = externalIndividualService.findByExternalIndividualID(externalIndividual.getId());
		if (ind != null) {
			updateExternalIndividual(externalIndividual, externalIndividual.getSerialId());
		} else {
			externalIndividualService.save(externalIndividual);
		}
	}

	/**
	 * Creates IndividualBOBresponse Object
	 * 
	 * @param IndividualBOBDetailsResponse
	 * @return IndividualBOBresponse Object
	 */
	private IndividualBOBresponse setIndividualBOBResponse(IndividualBOBDetailsResponse resp) {

		IndividualDTO individualDTO = new IndividualDTO();

		IndividualBOBresponse individualBOBresponse = new IndividualBOBresponse();
		individualDTO.setIndividualid(resp.getIndividualID());
		individualDTO.setCsdate(resp.getCaseStartDate());
		individualDTO.setCedate(resp.getCaseEndDate());
		individualDTO.setCounty(resp.getContactAddressCounty());
		individualDTO.setAddressline1(resp.getContactAddressFirstLine());
		individualDTO.setAddressline2(resp.getContactAddressSecondLine());
		individualDTO.setState(resp.getContactAddressState());
		individualDTO.setCity(resp.getContactAddressCity());
		individualDTO.setZip(resp.getContactAddressZipCode());
		individualDTO.setCitizen(resp.getCitizen());
		individualDTO.setApplicationStatus(resp.getApplicationStatus());
		individualDTO.setEmailid(resp.getEmailID());

		individualDTO.setCaseStatus(resp.getCaseStatus());
		individualDTO.setFname(resp.getFirstName());
		individualDTO.setLname(resp.getLastName());
		individualDTO.setMinitial(resp.getMiddileInitial());
		individualDTO.setGender(resp.getGender());
		individualDTO.setId(resp.getIndividualID());
		individualDTO.setCommunicationPref(resp.getPrefferedCommunicationMethod());
		individualDTO.setPrefspokenlang(resp.getPrefferedSpokenLanguage());
		individualDTO.setPhone(resp.getPrimaryPhoneNumber() != null ? resp.getPrimaryPhoneNumber() : 0);
		// Check for Null SSN
		if (resp.getSsn() != null) {
			individualDTO.setSsn(new Long(resp.getSsn()).longValue());
		}
		else {
			individualDTO.setSsn(0);
		}
		individualDTO.setSuffix(resp.getSuffix());
		
		individualDTO.setDob(resp.getDateOfBirth());
		individualDTO.setWrittenlangpref(resp.getPrefferedWrittenLanguage());
		individualBOBresponse.setResponseCode(resp.getResponseCode());
		LOGGER.info("ResponseCode for Individual Details:" + resp.getResponseCode());
		individualBOBresponse.setResponseDescription(resp.getResponseDesc());
		LOGGER.info("ResponseDescription for Individual Details:" + resp.getResponseDesc());
		individualBOBresponse.setIndividualDTO(individualDTO);

		return (individualBOBresponse);
	}

	/**
	 * This method creates External Individual Entity and populate data
	 * 
	 * @param IndividualBOBDetailsResponse
	 *            response received from AHBX
	 * @return ExternalIndividual object with values populated from AHBX
	 *         response
	 */
	private ExternalIndividual createIndividualDetail(IndividualBOBDetailsResponse resp, ExternalIndividual external_ind) throws Exception {
		if (!AHBXUtils.isEmpty(resp.getCaseStartDate())) {

			external_ind.setCaseStartDate(AHBXUtils.formatPlainDate(resp.getCaseStartDate()));
		}
		if (!AHBXUtils.isEmpty(resp.getCaseEndDate())) {
			external_ind.setCaseEndDate(AHBXUtils.formatPlainDate(resp.getCaseEndDate()));
		}
		if (!AHBXUtils.isEmpty(resp.getDateOfBirth())) {
			external_ind.setDob((AHBXUtils.formatPlainDate(resp.getDateOfBirth())));
		}

		external_ind.setContactAddressCounty(resp.getContactAddressCounty());
		external_ind.setAddress1(resp.getContactAddressFirstLine());
		external_ind.setAddress2(resp.getContactAddressSecondLine());
		external_ind.setState(resp.getContactAddressState());
		external_ind.setCity(resp.getContactAddressCity());
		external_ind.setZip(resp.getContactAddressZipCode());
		external_ind.setCitizen(resp.getCitizen());
		external_ind.setEmail(resp.getEmailID());
		/*	if (!AHBXUtils.isEmpty(resp.getApplicationStatus())) {
			external_ind.setEnrollmentStatus(resp.getApplicationStatus());
		}
		if (!AHBXUtils.isEmpty(resp.getCaseStatus())) {
			external_ind.setEligibilityStatus(resp.getCaseStatus());
		}*/
		external_ind.setFirstName(resp.getFirstName());
		external_ind.setLastName(resp.getLastName());
		external_ind.setMiddleName(resp.getMiddileInitial());
		external_ind.setGender(resp.getGender());
		external_ind.setId(resp.getIndividualID());
		external_ind.setPrefCommMethod(resp.getPrefferedCommunicationMethod());
		external_ind.setPrefSpokenLang(resp.getPrefferedSpokenLanguage());

		//HIX-26167 : setting  NUMBEROFHOUSEHOLDMEMBERS, SSN, HOUSEHOLDINCOME, PRIMARYPHONE to 0 if it's null
		if (resp.getSsn() != null) {
			external_ind.setSsn(new Long(resp.getSsn()).longValue());
		}
		else {
			external_ind.setSsn(0);
		}
		
		external_ind.setHouseholdIncome(external_ind.getHouseholdIncome() != null ? external_ind.getHouseholdIncome() : 0.0);
		
		external_ind.setSuffix(resp.getSuffix());
		external_ind.setPhone(resp.getPrimaryPhoneNumber() != null ? resp.getPrimaryPhoneNumber() : 0);
		external_ind.setPrefWrittenLang(resp.getPrefferedWrittenLanguage());
		
		
		external_ind.setNumberOfHouseholdMembers(resp.getNumberOfHouseholdMembers() != null && resp.getNumberOfHouseholdMembers().intValue() > 0 ? resp.getNumberOfHouseholdMembers().intValue() : 0);
		
		return external_ind;
	}

	/**
	 * Invokes AHBX EndPoint to retrieve Individual Book of Business Status
	 * 
	 * @param IndividualDTO
	 *            consisting of a list of individual Ids ind
	 * @return String marshaled IndividualBOBResponse from AHBX response
	 */
	@RequestMapping(value = "/retrieveindividualBOBStatus", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveIndividualBOBStatus(@RequestBody IndividualDTO ind) {

		IndividualBOBresponse individualBOBresponse = new IndividualBOBresponse();

		RetrieveIndividualsBOBStatus retriveIndividualBOBStatus = new RetrieveIndividualsBOBStatus();
		JAXBElement<RetrieveIndividualsBOBStatusResponse> retrieveIndividualBOBStatusResponseFromAHBX = null;
		RetrieveIndividualsBOBStatusResponse response = null;
		List<IndividualsBOBStatusResponse> individualBOBStatusResponselist = new ArrayList<IndividualsBOBStatusResponse>();
		IndividualHouseholdDTO individualHouseHoldDTO = null;

		List<IndividualHouseholdDTO> individualHouseHoldDTOlist = new ArrayList<IndividualHouseholdDTO>();

		com.getinsured.ahbx.broker.individualbookofbusiness.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.individualbookofbusiness.ObjectFactory();
		if (ind.getIdlist() != null && ind.getIdlist().size() != 0) {

/*			LOGGER.info("IND51:Invoking External Individual BOB Status service. Sending list of IndividualCase Ids to AHBX: "
					+ ind.toString());
*/
			try {

				retriveIndividualBOBStatus = objectFactory.createRetrieveIndividualsBOBStatus();
				retriveIndividualBOBStatus.getIndividualRequest().addAll(ind.getIdlist());

				if (retriveIndividualBOBStatus != null) {
					//LOGGER.info("IND51 RequestParameters:" + retriveIndividualBOBStatus.getIndividualRequest());
				}
				JAXBElement<RetrieveIndividualsBOBStatus> requestElement = objectFactory
						.createRetrieveIndividualsBOBStatus(retriveIndividualBOBStatus);

				individualWebServiceTemplate.setDefaultUri(GhixAhbxConstants.retrieveIndividualBOBEndPointImplPort);
				/*retrieveIndividualBOBStatusResponseFromAHBX = (JAXBElement<RetrieveIndividualsBOBStatusResponse>) individualWebServiceTemplate
						.marshalSendAndReceive(requestElement);*/
				
				retrieveIndividualBOBStatusResponseFromAHBX = (JAXBElement<RetrieveIndividualsBOBStatusResponse>) soapWebClient
						.send(requestElement, GhixAhbxConstants.retrieveIndividualBOBEndPointImplPort, "IND51",
								individualWebServiceTemplate);

				response = retrieveIndividualBOBStatusResponseFromAHBX.getValue();

				individualBOBStatusResponselist = response.getIndividual();
				if (response.getIndividual() != null && !response.getIndividual().isEmpty()) {
					for (IndividualsBOBStatusResponse individualBOBStatusResponse : individualBOBStatusResponselist) {

						individualHouseHoldDTO = new IndividualHouseholdDTO();
						individualHouseHoldDTO.setEligStatus(individualBOBStatusResponse.getEligStatus());
						individualHouseHoldDTO.setEnrollStatus(individualBOBStatusResponse.getEnrollStatus());
						individualHouseHoldDTO.setHouseIncome(individualBOBStatusResponse.getHouseIncome());

						individualHouseHoldDTO.setHeadOfHouseHoldName(individualBOBStatusResponse
								.getHeadOfHouseholdName());

						individualHouseHoldDTO.setIndividualID(individualBOBStatusResponse.getIndividualID());
						individualHouseHoldDTO.setNumberOfHouseholdMembers(individualBOBStatusResponse
								.getNumberOfHouseholdMembers());
						individualHouseHoldDTO.setResponseCode(individualBOBStatusResponse.getResponseCode());
						LOGGER.info("IND51:ResponseCode for IndividualStatus"
								+ individualBOBStatusResponse.getResponseCode());
						individualHouseHoldDTO.setResponseDesc(individualBOBStatusResponse.getResponseDesc());
						LOGGER.info("IND51:ResponseDescription for IndividualStatus"
								+ individualBOBStatusResponse.getResponseDesc());
						individualHouseHoldDTOlist.add(individualHouseHoldDTO);

						if (individualBOBStatusResponse.getResponseCode() == INDIVIDUAL_SUCCESS_RESPONSE_CODE) {

							LOGGER.info("IND52:Individual Status response code is 400. Inserting Individual details into DB");

							updateExternalndividualStatus(individualBOBStatusResponse);
							individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);

						} else {

							LOGGER.info("IND51:Response Code is" + individualBOBStatusResponse.getResponseCode()
									+ "Response Description" + individualBOBStatusResponse.getResponseDesc() + "IND51");
						}

					}
					individualBOBresponse.setIndividualHouseholdDTOlist(individualHouseHoldDTOlist);

/*					LOGGER.info("IND51:External Individual BOB Status service call successful. "
							+ "Received following response from AHBX for IND51: " + individualBOBresponse.toString());
*/				} else {
					LOGGER.error("IND51:Individual stautus response object is null. Unable to update Individual status in DB");
				}
			} catch (WebServiceIOException webServiceIOException) {

				LOGGER.error("IND51:Exception occurred while retrieving Individual Status from AHBX: "
						, webServiceIOException);

				individualBOBresponse.setErrMsg(webServiceIOException.getMessage());
				individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);

			} catch (Exception exception) {

				LOGGER.error("IND51:Exception occurred while retrieving Individual Status from AHBX: "
						, exception);

				individualBOBresponse.setErrMsg(exception.getMessage());
				individualBOBresponse.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);

			}
		} else {
			LOGGER.info("IND51:IndividualDTO request object null. Could not retrieve Individual Status from AHBX");
		}
		return AHBXUtils.marshal(individualBOBresponse);
	}

	/**
	 * This method will update existing record irrespective of any changes made
	 * or not
	 * 
	 * @param ExternalIndividual
	 *            the object encapsulating latest external individual data
	 *            fetched from AHBX
	 * @param id
	 *            external individual id
	 */
	private void updateExternalIndividual(ExternalIndividual externalIndividual, long id) {

		externalIndividual.setSerialId(id);
		externalIndividualService.save(externalIndividual);
	}

	/**
	 * This method will update the status in External Individual table
	 * 
	 * @param response
	 *            the object encapsulating latest external individual status
	 *            data fetched from AHBX
	 */
	private void updateExternalndividualStatus(IndividualsBOBStatusResponse response) {
		ExternalIndividual externalIndividual = new ExternalIndividual();
		externalIndividual.setId(response.getIndividualID());
		if(!AHBXUtils.isEmpty(response.getEnrollStatus())){
			externalIndividual.setEnrollmentStatus(response.getEnrollStatus());
		}
		if(!AHBXUtils.isEmpty(response.getEligStatus())){
			externalIndividual.setEligibilityStatus(response.getEligStatus());
		}
		
		if (response.getHouseIncome() != null) {
			externalIndividual.setHouseholdIncome(response.getHouseIncome());
		}
		else {
			externalIndividual.setHouseholdIncome(0.0);
		}
			
		externalIndividual.setHeadOfHouseholdName(response.getHeadOfHouseholdName());
		//HIX-26167 : setting number of household members to 0 if it's null
		externalIndividual.setNumberOfHouseholdMembers(response.getNumberOfHouseholdMembers() != null && response.getNumberOfHouseholdMembers().intValue() > 0 ? response.getNumberOfHouseholdMembers().intValue() : 0);
		
		externalIndividualService.update(externalIndividual);
	}
	
	
	@RequestMapping(value = "/retrieveindividualbobdetailslist", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveIndividualBOBDetailList(@RequestBody BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {
		BrokerBobResponseDTO brokerBobResponseDTO = new BrokerBobResponseDTO();
		JAXBElement<RetrieveAssociatedIndividualDetailsResponse>  associatedIndividualDetailsResponse=null;
		
		long startTime = TimeShifterUtil.currentTimeMillis();
		LOGGER.info("IND72: Record Id : "+brokerBOBSearchParamsDTO.getRecordId()+" : Record Type : "+brokerBOBSearchParamsDTO.getRecordType()+" : Designation Status : " +brokerBOBSearchParamsDTO.getDesignationStatus() +" : Start Time : "+startTime);
		
		com.getinsured.ahbx.broker.bookofbusiness.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.bookofbusiness.ObjectFactory(); 
		
		AgentAssisterBOBDetailsRequest  requestElement = objectFactory.createAgentAssisterBOBDetailsRequest() ; 
		RetrieveAssociatedIndividualDetails requestWrapper = objectFactory.createRetrieveAssociatedIndividualDetails();
		
		requestWrapper.setAgentAssisBOBDetailReq(requestElement);
		
		JAXBElement<RetrieveAssociatedIndividualDetails> requestWrapperElement = objectFactory.createRetrieveAssociatedIndividualDetails(requestWrapper);
		
		RetrieveAssociatedIndividualDetailsResponse retrieveAssociatedIndividualDetailsResponse = null;
		List<BrokerBOBDetailsDTO> brokerBOBDetailsDTOList = null;
		
		if (brokerBOBSearchParamsDTO != null) {
			LOGGER.info("IND72: Successfully received BrokerBOBSearchParamsDTO from ghix-web over rest call");

			populateAgentAssisterBOBRequest(brokerBOBSearchParamsDTO, requestElement);
			LOGGER.debug("IND72: AgentAssisterBOBDetailsRequest successfully populated");
			try {
				// Invoking AHBX Web Service
				agentAssisterBobWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND72);
				associatedIndividualDetailsResponse = (JAXBElement<RetrieveAssociatedIndividualDetailsResponse>) soapWebClient.send(requestWrapperElement, GhixAhbxConstants.WSDL_URL_IND72, "IND72",agentAssisterBobWebServiceTemplate);

				retrieveAssociatedIndividualDetailsResponse = associatedIndividualDetailsResponse.getValue();
				
				RetrieveAssociatedIndividualDetailsResponseDTO retrieveAssociatedIndividualDetailsResponseDTO = retrieveAssociatedIndividualDetailsResponse.getRetrieveAssociatedIndividualDetailsResponseDTO();
				if(retrieveAssociatedIndividualDetailsResponseDTO.getResponseCode()==INDIVIDUAL_SUCCESS_RESPONSE_CODE){
					AgentAssisterBOBDetailsResponse agentAssisterBOBDetailsResponse = retrieveAssociatedIndividualDetailsResponseDTO.getResponse();
					if(agentAssisterBOBDetailsResponse.getIndividualIds()!=null){
						brokerBobResponseDTO.setIndividualsId(agentAssisterBOBDetailsResponse.getIndividualIds().getIndividualId());
					}
					brokerBOBDetailsDTOList = processIND72AHBXResponse(agentAssisterBOBDetailsResponse);
					brokerBobResponseDTO.setBrokerBobDetailsDTO(brokerBOBDetailsDTOList); 
					if(agentAssisterBOBDetailsResponse.getIndvCount()!=null){
						brokerBobResponseDTO.setCount(agentAssisterBOBDetailsResponse.getIndvCount());
					}
				}
				
				brokerBobResponseDTO.setResponseCode(retrieveAssociatedIndividualDetailsResponseDTO.getResponseCode());  
				brokerBobResponseDTO.setResponseDesc(retrieveAssociatedIndividualDetailsResponseDTO.getResponseDesc());
				brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_SUCCESS);
			}catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND72: Exception occurred while invoking broker book of business details service "
						, webServiceIOException);
				brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				brokerBobResponseDTO.setErrMsg(webServiceIOException.getMessage());
			}
			catch (Exception exception) {
				LOGGER.error("IND72: Exception occurred while invoking broker book of business details service " , exception);
				brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				brokerBobResponseDTO.setErrMsg(exception.getMessage());
			}
			
			
		}else {
			LOGGER.error("IND72: BrokerBOBSearchParamsDTO is null. Unable to send Broker details to AHBX.");

			brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			brokerBobResponseDTO.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
			brokerBobResponseDTO.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
		}

		LOGGER.info("IND72: Marshalling and returning brokerBobResponseDTO to ghix-web");
		long endTime   = TimeShifterUtil.currentTimeMillis();
		long totalTime = endTime - startTime;
		LOGGER.info("IND72: Response Code : "+brokerBobResponseDTO.getResponseCode()+" : End Time : " +endTime+" : Total Time : "+totalTime);

		return AHBXUtils.marshal(brokerBobResponseDTO);
	}

	private void populateAgentAssisterBOBRequest(BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO, AgentAssisterBOBDetailsRequest requestElement) {
		if(brokerBOBSearchParamsDTO.getActiveSinceFromDate()!=null && !brokerBOBSearchParamsDTO.getActiveSinceFromDate().isEmpty()){
		requestElement.setActiveSinceFrmDt(brokerBOBSearchParamsDTO.getActiveSinceFromDate());
		}
		if(brokerBOBSearchParamsDTO.getActiveSinceToDate()!=null && !brokerBOBSearchParamsDTO.getActiveSinceToDate().isEmpty()){
		requestElement.setActiveSinceToDt(brokerBOBSearchParamsDTO.getActiveSinceToDate());
		}
		if(brokerBOBSearchParamsDTO.getAplicationStatus()!=null && !brokerBOBSearchParamsDTO.getAplicationStatus().isEmpty()){
		requestElement.setApplicationStatus(brokerBOBSearchParamsDTO.getAplicationStatus());
		}
		if(brokerBOBSearchParamsDTO.getApplicationType()!=null && !brokerBOBSearchParamsDTO.getApplicationType().isEmpty()){
		requestElement.setApplicationType(brokerBOBSearchParamsDTO.getApplicationType());
		}
		if(brokerBOBSearchParamsDTO.getCoverageYear()!=null && !brokerBOBSearchParamsDTO.getCoverageYear().isEmpty()){
		requestElement.setApplicationYear(brokerBOBSearchParamsDTO.getCoverageYear());
		}
		if(brokerBOBSearchParamsDTO.getCurrentStatus()!=null && !brokerBOBSearchParamsDTO.getCurrentStatus().isEmpty()){
		requestElement.setCurrentStatus(brokerBOBSearchParamsDTO.getCurrentStatus());
		}
		if(brokerBOBSearchParamsDTO.getDesignationStatus()!=null && !brokerBOBSearchParamsDTO.getDesignationStatus().isEmpty()){
		requestElement.setDesignationStatus(brokerBOBSearchParamsDTO.getDesignationStatus());
		}
		if(brokerBOBSearchParamsDTO.getDueDate()!=null && !brokerBOBSearchParamsDTO.getDueDate().isEmpty()){
			requestElement.setDueDate(brokerBOBSearchParamsDTO.getDueDate());
		}
		
		if(brokerBOBSearchParamsDTO.getEligibilityStatus() != null && !brokerBOBSearchParamsDTO.getEligibilityStatus().isEmpty()){
			requestElement.setEligibilityStatus(brokerBOBSearchParamsDTO.getEligibilityStatus());
		}
		 
		if(brokerBOBSearchParamsDTO.getCounselorFirstName()!=null && !brokerBOBSearchParamsDTO.getCounselorFirstName().isEmpty()){
		requestElement.setEnrlCounselorFname(brokerBOBSearchParamsDTO.getCounselorFirstName().trim());
		}
		if(brokerBOBSearchParamsDTO.getCounselorLastName()!=null && !brokerBOBSearchParamsDTO.getCounselorLastName().isEmpty()){
		requestElement.setEnrlCounselorLname(brokerBOBSearchParamsDTO.getCounselorLastName().trim());
		}
		if(brokerBOBSearchParamsDTO.getFirstName()!=null && !brokerBOBSearchParamsDTO.getFirstName().isEmpty()){
		requestElement.setFirstName(brokerBOBSearchParamsDTO.getFirstName().trim());
		}
		if(brokerBOBSearchParamsDTO.getInactiveSinceFrmDt()!=null && !brokerBOBSearchParamsDTO.getInactiveSinceFrmDt().isEmpty()){
		requestElement.setInactiveSinceFrmDt(brokerBOBSearchParamsDTO.getInactiveSinceFrmDt());
		}
		if(brokerBOBSearchParamsDTO.getInactiveSinceToDt()!=null && !brokerBOBSearchParamsDTO.getInactiveSinceToDt().isEmpty()){
		requestElement.setInactiveSinceToDt(brokerBOBSearchParamsDTO.getInactiveSinceToDt());
		}
		if(brokerBOBSearchParamsDTO.getIssuerval()!=null && !brokerBOBSearchParamsDTO.getIssuerval().isEmpty()){
		requestElement.setIssuerName(brokerBOBSearchParamsDTO.getIssuerval());
		}
		if(brokerBOBSearchParamsDTO.getLastName()!=null && !brokerBOBSearchParamsDTO.getLastName().isEmpty()){
		requestElement.setLastName(brokerBOBSearchParamsDTO.getLastName().trim());
		}
		if(brokerBOBSearchParamsDTO.getNextStep()!=null && !brokerBOBSearchParamsDTO.getNextStep().isEmpty()){
		requestElement.setNextStep(brokerBOBSearchParamsDTO.getNextStep());
		}
		if(brokerBOBSearchParamsDTO.getPageNumber()!=null){
		requestElement.setPageNumber(brokerBOBSearchParamsDTO.getPageNumber());
		}
		if(brokerBOBSearchParamsDTO.getPageSize()!=null){
		requestElement.setPageSize(brokerBOBSearchParamsDTO.getPageSize());
		}
		if(brokerBOBSearchParamsDTO.getRecordId()!=null){
		requestElement.setRecordId(brokerBOBSearchParamsDTO.getRecordId());
		}
		if(brokerBOBSearchParamsDTO.getRecordType()!=null && !brokerBOBSearchParamsDTO.getRecordType().isEmpty()){
		requestElement.setRecordType(brokerBOBSearchParamsDTO.getRecordType());  
		}
		if(brokerBOBSearchParamsDTO.getRequestSentFrmDt()!=null && !brokerBOBSearchParamsDTO.getRequestSentFrmDt().isEmpty()){
		requestElement.setRequestSentFrmDt(brokerBOBSearchParamsDTO.getRequestSentFrmDt());
		}
		if(brokerBOBSearchParamsDTO.getRequestSentToDt()!=null && !brokerBOBSearchParamsDTO.getRequestSentToDt().isEmpty()){
		requestElement.setRequestSentToDt(brokerBOBSearchParamsDTO.getRequestSentToDt());
		}
		requestElement.setIndividualIds(brokerBOBSearchParamsDTO.getIndividualIds());
		if(brokerBOBSearchParamsDTO.getSortBy()!=null && !brokerBOBSearchParamsDTO.getSortBy().isEmpty()){
		requestElement.setSortBy(brokerBOBSearchParamsDTO.getSortBy());
		}
		if(brokerBOBSearchParamsDTO.getSortOder()!=null && !brokerBOBSearchParamsDTO.getSortOder().isEmpty()){
		requestElement.setSortOrder(brokerBOBSearchParamsDTO.getSortOder());
		}
		if(brokerBOBSearchParamsDTO.getEnrollmentStatus()!=null && !brokerBOBSearchParamsDTO.getEnrollmentStatus().isEmpty()){
		requestElement.setEnrollmentStatus(brokerBOBSearchParamsDTO.getEnrollmentStatus());
	}
	
	}
	
	private List<BrokerBOBDetailsDTO> processIND72AHBXResponse(AgentAssisterBOBDetailsResponse agentAssisterBOBDetailsResponse){
		AssociatedIndividuals associatedIndividuals = agentAssisterBOBDetailsResponse.getAssociatedIndividuals();
		List<BrokerBOBDetailsDTO> brokerBOBDetailsDTOList= new ArrayList<BrokerBOBDetailsDTO>();
		if(associatedIndividuals!=null){
			List<IndividualDetails> associatedIndividualList = associatedIndividuals.getAssociatedIndividual();
			
			if(associatedIndividualList!=null){
				for (IndividualDetails associatedIndividual : associatedIndividualList) {
					BrokerBOBDetailsDTO brokerBOBDetailsDTO = new BrokerBOBDetailsDTO();
					if (associatedIndividual.getIndividualID() < Integer.MIN_VALUE || associatedIndividual.getIndividualID() > Integer.MAX_VALUE) {
				        throw new IllegalArgumentException
				            (associatedIndividual.getIndividualID() + " cannot be cast to int without changing its value.");
				    }
					brokerBOBDetailsDTO.setIndividualId((int)associatedIndividual.getIndividualID());
					brokerBOBDetailsDTO.setEncryptedIndividualId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(associatedIndividual.getIndividualID())));
					brokerBOBDetailsDTO.setFirstName(associatedIndividual.getFirstName());
					brokerBOBDetailsDTO.setLastName(associatedIndividual.getLastName());
					brokerBOBDetailsDTO.setMiddleName(associatedIndividual.getMiddleName());
					
					brokerBOBDetailsDTO.setEmailAddress(associatedIndividual.getEmailID());
					brokerBOBDetailsDTO.setPhoneNumber(associatedIndividual.getPrimaryPhoneNumber());
					
					brokerBOBDetailsDTO.setAddress1(associatedIndividual.getContactAddressFirstLine());
					brokerBOBDetailsDTO.setAddress2(associatedIndividual.getContactAddressSecondLine());
					brokerBOBDetailsDTO.setCity(associatedIndividual.getContactAddressCity());
					brokerBOBDetailsDTO.setState(associatedIndividual.getContactAddressState());
					brokerBOBDetailsDTO.setZipCode(associatedIndividual.getContactAddressZipCode());
					brokerBOBDetailsDTO.setCounty(associatedIndividual.getContactAddressCounty());
					
					brokerBOBDetailsDTO.setNoOfHouseholdMembers(associatedIndividual.getNumberOfHouseholdMembers()!= null?associatedIndividual.getNumberOfHouseholdMembers():0);
					
					brokerBOBDetailsDTO.setApplicationType(associatedIndividual.getApplicationType());
					brokerBOBDetailsDTO.setEnrollmentStatus(associatedIndividual.getEnrollmentStatus());
					brokerBOBDetailsDTO.setCurrentStatus(associatedIndividual.getCurrentStatus()); 
					
					brokerBOBDetailsDTO.setNextStep(associatedIndividual.getNextStep());
					brokerBOBDetailsDTO.setPlanName(associatedIndividual.getPlanName());
					brokerBOBDetailsDTO.setPlanType(associatedIndividual.getPlanType());
					brokerBOBDetailsDTO.setIssuerName(associatedIndividual.getIssuerName());
					brokerBOBDetailsDTO.setHiosPlanId(associatedIndividual.getHiosPlanId());
					
					if(associatedIndividual.getNetPremiumAmt()!=null){
						brokerBOBDetailsDTO.setPremium(associatedIndividual.getNetPremiumAmt().toString());
					}
					
					brokerBOBDetailsDTO.setOfficeVisit(associatedIndividual.getOfficeVisitsCost());
					brokerBOBDetailsDTO.setDeductible(associatedIndividual.getDeductibleCost());
					brokerBOBDetailsDTO.setGenericDrugs(associatedIndividual.getGenericDrugsCost());
					
					if(associatedIndividual.getEnrlCounselorRecordId()!=null){
						brokerBOBDetailsDTO.setCecId(associatedIndividual.getEnrlCounselorRecordId().intValue());
					}
					brokerBOBDetailsDTO.setEcFirstName(associatedIndividual.getEnrlCounselorFname());
					brokerBOBDetailsDTO.setEcLastName(associatedIndividual.getEnrlCounselorLname());
					
					brokerBOBDetailsDTO.setRequestSentDt(associatedIndividual.getRequestSentDt());
					brokerBOBDetailsDTO.setActiveSinceFrmDt(associatedIndividual.getActiveSinceFrmDt());
					brokerBOBDetailsDTO.setInactiveSinceFrmDt(associatedIndividual.getInactiveSinceFrmDt());
					brokerBOBDetailsDTO.setTotalCount(agentAssisterBOBDetailsResponse.getIndvCount());
						
					brokerBOBDetailsDTO.setCaseNumber(associatedIndividual.getAhbxCaseId());
					brokerBOBDetailsDTO.setApplicationYear(associatedIndividual.getApplicationYear());
					
					brokerBOBDetailsDTOList.add(brokerBOBDetailsDTO);
				}
			}
		}
		
		return brokerBOBDetailsDTOList;
	}
	
	@RequestMapping(value = "/retrieveindividualhouseholdeligibilitydetails", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveindividualhouseholdeligibilitydetails(@RequestBody BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO) {
		BrokerBobResponseDTO brokerBobResponseDTO = new BrokerBobResponseDTO();
		HouseholdEligibilityInformationDTO householdEligibilityInformationDTO = new HouseholdEligibilityInformationDTO();
		
		com.getinsured.ahbx.broker.individualbookofbusinessdetails.ObjectFactory objectFactory = new com.getinsured.ahbx.broker.individualbookofbusinessdetails.ObjectFactory(); 
		
		com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetails  requestElement = objectFactory.createRetrieveIndividualBOBDetails();
		
		JAXBElement<com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetails> requestWrapperElement = objectFactory.createRetrieveIndividualBOBDetails(requestElement);
		
		JAXBElement<com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetailsResponse> retrieveAssociatedIndividualDetailsResponse = null;
		
		if (brokerBOBSearchParamsDTO != null) {
			LOGGER.info("IND73: Successfully received BrokerBOBSearchParamsDTO from ghix-web over rest call");

			try {
				requestElement.setIndividualID(brokerBOBSearchParamsDTO.getRecordId());
				requestElement.setInformationType(brokerBOBSearchParamsDTO.getInformationType());
				requestElement.setApplicationYear(brokerBOBSearchParamsDTO.getApplicationYear());
				
				// Invoking AHBX Web Service
				individualBobDetailsWebServiceTemplate.setDefaultUri(GhixAhbxConstants.WSDL_URL_IND73);
				retrieveAssociatedIndividualDetailsResponse = (JAXBElement<com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetailsResponse>) soapWebClient.send(requestWrapperElement, GhixAhbxConstants.WSDL_URL_IND73, "IND73",individualBobDetailsWebServiceTemplate);

				com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetailsResponse retrieveIndividualBOBDetailsResponse = retrieveAssociatedIndividualDetailsResponse.getValue();
				
				if(retrieveIndividualBOBDetailsResponse!=null){
					com.getinsured.ahbx.broker.individualbookofbusinessdetails.RetrieveIndividualBOBDetailsResponseDTO retrieveIndividualBOBDetailsResponseDTO = retrieveIndividualBOBDetailsResponse.getRetrieveIndividualBOBDetailsResponseDTO();
					com.getinsured.ahbx.broker.individualbookofbusinessdetails.IndividualBOBResponse individualBOBResponse = retrieveIndividualBOBDetailsResponseDTO.getResponse();
					if(individualBOBResponse!=null){
						processIND73Response(individualBOBResponse, householdEligibilityInformationDTO, brokerBOBSearchParamsDTO);
						brokerBobResponseDTO.setHouseholdEligibilityInformationDTO(householdEligibilityInformationDTO);
					}
					
					brokerBobResponseDTO.setResponseCode(retrieveIndividualBOBDetailsResponseDTO.getResponseCode());
					brokerBobResponseDTO.setResponseDesc(retrieveIndividualBOBDetailsResponseDTO.getResponseDesc());
				}
			}catch (WebServiceIOException webServiceIOException) {
				LOGGER.error("IND73: Exception occurred while invoking individual book of business details service "
						, webServiceIOException);
				brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				brokerBobResponseDTO.setErrMsg(webServiceIOException.getMessage());
			}
			catch (Exception exception) {
				LOGGER.error("IND73: Exception occurred while invoking individual book of business details service " , exception);
				brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
				brokerBobResponseDTO.setErrMsg(exception.getMessage());
			}
		}else {
			LOGGER.error("IND73: BrokerBOBSearchParamsDTO is null. Unable to send Broker details to AHBX.");

			brokerBobResponseDTO.setStatus(GhixAhbxConstants.RESPONSE_FAILURE);
			brokerBobResponseDTO.setErrMsg(GhixAhbxConstants.INVALID_INPUT_MSG);
			brokerBobResponseDTO.setErrCode(GhixAhbxConstants.INVALID_INPUT_CODE);
		}

		LOGGER.info("IND73: Marshalling and returning brokerBobResponseDTO to ghix-web");

		return AHBXUtils.marshal(brokerBobResponseDTO);
	}
	
	private HouseholdEligibilityInformationDTO processIND73Response(com.getinsured.ahbx.broker.individualbookofbusinessdetails.IndividualBOBResponse individualBOBResponse, HouseholdEligibilityInformationDTO householdEligibilityInformationDTO, BrokerBOBSearchParamsDTO brokerBOBSearchParamsDTO){
		householdEligibilityInformationDTO.setFirstName(individualBOBResponse.getFirstName());
		householdEligibilityInformationDTO.setLastName(individualBOBResponse.getLastName());
		if(individualBOBResponse.getMonthlyAptcAmt()!=null){
			householdEligibilityInformationDTO.setAdvancedPremiumTaxCredit(Double.valueOf(individualBOBResponse.getMonthlyAptcAmt()));
		}
		householdEligibilityInformationDTO.setCostSharingReduction(individualBOBResponse.getCsrReduction());
		householdEligibilityInformationDTO.setEligibilityStatus(individualBOBResponse.getEligibilityStatusApp());
		
		if(HOUSEHOLD.equalsIgnoreCase(brokerBOBSearchParamsDTO.getInformationType())){
			HouseholdMembers householdMembers = individualBOBResponse.getHouseholdMembers();
			if(householdMembers!=null){
				List<SsapApplicantDTO> listOfHouseholdMembers = new ArrayList<>();
				for(IndvHouseHoldDetails member:householdMembers.getHouseholdMember()){
					SsapApplicantDTO ssapApplicantDTO = new SsapApplicantDTO();
					ssapApplicantDTO.setFirstName(member.getFirstName());
					ssapApplicantDTO.setLastName(member.getLastName());
					ssapApplicantDTO.setMiddleName(member.getMiddleName());
					ssapApplicantDTO.setRelationship(member.getRelationShip());
					ssapApplicantDTO.setBirthDateString(member.getDateOfBirth());
					ssapApplicantDTO.setGender(member.getGender());
					ssapApplicantDTO.setSsn(member.getSsn());
					ssapApplicantDTO.setHomeAddress(getHomeAddress(member));
					ssapApplicantDTO.setMailingLocation(getMailingAddress(member));
					if(member.getSeekingCoverageFlag()!=null){
						ssapApplicantDTO.setSeekingCoverage(member.getSeekingCoverageFlag().value());
					}
					if(member.getUsCitizenFlag()!=null){
						ssapApplicantDTO.setCitizenShipStatus(member.getUsCitizenFlag().value());
					}
					
					listOfHouseholdMembers.add(ssapApplicantDTO);
				}
				
				householdEligibilityInformationDTO.setApplicantEligibility(listOfHouseholdMembers);
			}
		}
		
		if(ELIGIBILITY.equalsIgnoreCase(brokerBOBSearchParamsDTO.getInformationType())){
			IndvHouseHoldEligDetails indvHouseHoldEligDetails = individualBOBResponse.getIndvHouseHoldEligDetails();
			if(indvHouseHoldEligDetails!=null){
				List<SsapApplicantDTO> listOfHouseholdMembers = new ArrayList<>();
				for(IndvHouseHoldEligDetail member:indvHouseHoldEligDetails.getEligibilityDetail()){
					SsapApplicantDTO ssapApplicantDTO = new SsapApplicantDTO();
					ssapApplicantDTO.setFirstName(member.getFirstName());
					ssapApplicantDTO.setLastName(member.getLastName());
					ssapApplicantDTO.setMiddleName(member.getMiddleName());
					if(member.getEligibilityStatusFlag()!=null){
						ssapApplicantDTO.setEligibilityStatus(member.getEligibilityStatusFlag().value());
					}
					if(member.getMedicalFlag()!=null){
						ssapApplicantDTO.setMedicalEligibilityStatus(member.getMedicalFlag().value());
					}
					if(member.getAptcAvailFlag()!=null){
						ssapApplicantDTO.setAdvancedPremiumTaxCredit(member.getAptcAvailFlag().value());
					}
					if(member.getCsrReductionFlag()!=null){
						ssapApplicantDTO.setCostSharingReduction(member.getCsrReductionFlag().value());
					}
					
					listOfHouseholdMembers.add(ssapApplicantDTO);
				}
				
				householdEligibilityInformationDTO.setApplicantEligibility(listOfHouseholdMembers);
			}
		}
		
		return householdEligibilityInformationDTO;
	}
	
	private Location getHomeAddress(IndvHouseHoldDetails member){
		Location address = new Location();
		address.setAddress1(member.getHomeAddressFirstLine());
		address.setAddress2(member.getHomeAddressSecondLine());
		address.setCity(member.getHomeAddressCity());
		address.setState(member.getHomeAddressState());
		address.setCounty(member.getHomeAddressCounty());
		address.setZip(member.getHomeAddressZipCode());
		
		return address;
	}
	
	private Location getMailingAddress(IndvHouseHoldDetails member){
		Location address = new Location();
		address.setAddress1(member.getMailAddressFirstLine());
		address.setAddress2(member.getMailAddressSecondLine());
		address.setCity(member.getMailAddressCity());
		address.setState(member.getMailAddressState());
		address.setCounty(member.getMailAddressCounty());
		address.setZip(member.getMailAddressZipCode());
		
		return address;
	}
}
