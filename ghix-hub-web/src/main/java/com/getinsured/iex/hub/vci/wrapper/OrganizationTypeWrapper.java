package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.OrganizationType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.OrganizationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class OrganizationTypeWrapper implements JSONAware{

	private String organizationIdentification;
	private String organizationName;
	
	private static final String ORG_ID_KEY = "OrganizationIdentification";
	private static final String ORG_NAME_KEY ="OrganizationName";
	
	
	public OrganizationTypeWrapper(OrganizationType organizationType) {
		
		if(organizationType == null){
			return;
		}
		
		this.organizationIdentification = organizationType.getOrganizationIdentification() != null ? organizationType.getOrganizationIdentification().getIdentificationID().getValue() : null;
		this.organizationName = organizationType.getOrganizationName() != null ? organizationType.getOrganizationName().getValue() : null;
	}


	public String getOrganizationIdentification() {
		return organizationIdentification;
	}


	public String getOrganizationName() {
		return organizationName;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ORG_ID_KEY, this.organizationIdentification);
		obj.put(ORG_NAME_KEY, this.organizationName);
		return obj.toJSONString();
	}

}
