package com.getinsured.ridpv2.wrapper;


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PersonGivenNameType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PersonMiddleNameType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PersonNameSuffixTextType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PersonSurnameType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.PrimaryRequestInformationType;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.ridpv2.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.DateType;
import com.getinsured.ridpv2.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.ridpv2.niem.niem.proxy.xsd._2.Date;

public class PersonWrapper {
	
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PersonWrapper.class);

	
	private static com.getinsured.ridpv2.cms.hix._0_1.hix_core.ObjectFactory hixCoreFactory;
	private static com.getinsured.ridpv2.niem.niem.niem_core._2.ObjectFactory niemCoreFactory;
	private static com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory extFactory;
	private static com.getinsured.ridpv2.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	
	private PersonType personType = null;
	private PersonNameType personNameType = null;
	private PersonGivenNameType personGivenNameType = null;
	private PersonMiddleNameType personMiddleNameType = null;
	private PersonSurnameType personSurnameType = null;
	private PersonNameSuffixTextType personNameSuffixTextType = null;
	
	private String personBirthDate = null;
	private String personGivenName = null;
	private String personMiddleName = null;
	private String personSurName = null;
	private String personNameSuffixText = null;
	private String personSSNIdentification = null;
	
	public PersonWrapper(){
		hixCoreFactory = new com.getinsured.ridpv2.cms.hix._0_1.hix_core.ObjectFactory();
		niemCoreFactory = new com.getinsured.ridpv2.niem.niem.niem_core._2.ObjectFactory();
		extFactory = new com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory();
		proxyFactory = new com.getinsured.ridpv2.niem.niem.proxy.xsd._2.ObjectFactory();
		
		this.personNameType = niemCoreFactory.createPersonNameType();
		this.personType = hixCoreFactory.createPersonType();
		this.personType.setPersonName(this.personNameType);
	}
	
	public void setPersonBirthDate(String mmddyyyy) throws HubServiceException{
		
		try{
			if(mmddyyyy!=null && mmddyyyy.trim().length() > 0){
				
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				sdf.setLenient(false);
				java.util.Date javaDate = sdf.parse(mmddyyyy);
	
				DateType dateType = niemCoreFactory.createDateType();
				Date date = proxyFactory.createDate();
				GregorianCalendar gc = new GregorianCalendar();
				gc.setTime(javaDate);
				date.setValue(DatatypeFactory.newInstance().newXMLGregorianCalendarDate(gc.get(Calendar.YEAR), gc.get(Calendar.MONTH)+1, gc.get(Calendar.DAY_OF_MONTH),DatatypeConstants.FIELD_UNDEFINED));
				
				dateType.getDate().add(date);
				this.personType.setPersonBirthDate(dateType);
				
				this.personBirthDate = mmddyyyy;
			}
		}
		catch(Exception e){
			LOGGER.error("Failed to set date of birth",e);
			throw new HubServiceException(e);
		}
	}
	
	public String getPersonBirthDate(){
		if(this.personType != null){
			
			return personBirthDate;
			
		}
		return null;
	}
	
	public void setPersonGivenName(String personGivenName){

		if(personGivenName != null && personGivenName.trim().length() > 0){
			personGivenNameType = extFactory.createPersonGivenNameType();
			personGivenNameType.setValue(personGivenName);
			personNameType.setPersonGivenName(personGivenNameType);
			
			this.personGivenName = personGivenName;
		}
	}
	
	public String getPersonGivenName(){
		if(this.personNameType != null){
			return this.personGivenName;
		}
		return null;
	}
	
	public void setPersonMiddleName(String personMiddleName){

		if(personMiddleName != null && personMiddleName.trim().length() > 0){
			personMiddleNameType = extFactory.createPersonMiddleNameType();
			personMiddleNameType.setValue(personMiddleName);
			personNameType.setPersonMiddleName(personMiddleNameType);
			
			this.personMiddleName = personMiddleName;
		}
	}
	
	public String getPersonMiddleName(){
		if(this.personNameType != null){
			return this.personMiddleName;
		}
		return null;
	}
	
	public void setPersonSurName(String personSurName){

		if(personSurName != null && personSurName.trim().length() > 0){
			personSurnameType = extFactory.createPersonSurnameType();
			personSurnameType.setValue(personSurName);
			personNameType.setPersonSurName(personSurnameType);
			
			this.personSurName = personSurName;
		}
	}
	
	public String getPersonSurName(){
		if(this.personNameType != null){
			return this.personSurName;
		}
		return null;
	}
	
	public String getPersonNameSuffixText() {
		return personNameSuffixText;
	}

	public void setPersonNameSuffixText(String personNameSuffixText) {
		if(personNameSuffixText!=null && personNameSuffixText.trim().length() > 0){
			personNameSuffixTextType = extFactory.createPersonNameSuffixTextType();
			this.personNameSuffixTextType.setValue(personNameSuffixText);
			personNameType.setPersonNameSuffixText(personNameSuffixTextType);	
			
			this.personNameSuffixText = personNameSuffixText;
		}
	}

	public String getPersonSSNIdentification() {
		return personSSNIdentification;
	}

	public void setPersonSSNIdentification(String personSSNIdentification) {
		if(personSSNIdentification!=null && personSSNIdentification.trim().length() > 0){
			this.personType.setPersonSSNIdentification(personSSNIdentification);
			this.personSSNIdentification = personSSNIdentification;			
		}
	}

	public PersonType getSystemRepresentation(){
		return this.personType;
	}
	
	public static void main(String[] args) throws JAXBException, IllegalArgumentException, DatatypeConfigurationException, HubServiceException{
		PersonWrapper p = new PersonWrapper();
		p.setPersonBirthDate("11/24/1968");
		p.setPersonGivenName("given");
		p.setPersonMiddleName("middle");
		p.setPersonSurName("surname");
		p.setPersonNameSuffixText("personNameSuffixText");
		p.setPersonSSNIdentification("123-123-1234");
		
		PrimaryRequestInformationType req = extFactory.createPrimaryRequestInformationType();
		req.setPerson(p.getSystemRepresentation());
		JAXBElement<PrimaryRequestInformationType> pReq = extFactory.createPrimaryRequest(req);
		RequestPayloadType reqPayLoad = extFactory.createRequestPayloadType();
		reqPayLoad.setRequest(pReq);
		JAXBElement<Object> reqObj = extFactory.createRequest(reqPayLoad);
		
		JAXBContext jc = JAXBContext.newInstance("com.getinsured.ridpv2.cms.dsh.ridp.extension._1");
		Marshaller m = jc.createMarshaller();
		m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		//m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new GINamespaceFrefixMapper());
		m.marshal(reqObj, System.out);
	}

	
}
