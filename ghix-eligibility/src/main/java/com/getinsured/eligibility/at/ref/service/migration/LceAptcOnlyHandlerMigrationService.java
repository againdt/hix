package com.getinsured.eligibility.at.ref.service.migration;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;

@Component("lceAptcOnlyHandlerMigrationService")
public class LceAptcOnlyHandlerMigrationService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceAptcOnlyHandlerMigrationService.class);

	@Autowired
	@Qualifier("lceAppExtensionEventMigration")
	private LceAppExtensionEventMigration lceAppExtensionEventMigration;
	
	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;
	
	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceAptcOnlyHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		final ApplicationExtension applicationExtension = lceProcessRequestDTO.getApplicationExtension();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		boolean isConversion = false;
		if("APTC".equalsIgnoreCase(enrolledApplication.getExchangeEligibilityStatus().toString()) && 
				"QHP".equalsIgnoreCase(currentApplication.getExchangeEligibilityStatus().toString()))  //For F-NF
		{
			Map<String, ApplicantEvent> data = new HashMap<String, ApplicantEvent>();
			ssapApplicationEventMigrationService.createDeniedHouseholdApplicationEvent(currentApplication, data);
			isConversion = true;
		}else if("QHP".equalsIgnoreCase(enrolledApplication.getExchangeEligibilityStatus().toString()) && 
				"APTC".equalsIgnoreCase(currentApplication.getExchangeEligibilityStatus().toString()))  //For NF-F 
		{
			manageApplicationApplicantEventForConversion(currentApplication, "QE");
			isConversion = true;
		}else {
			createAPTCUpdateEvent(currentApplication, applicationExtension);
		}
		currentApplication = loadCurrentApplication(currentApplicationId);
		
		updateAllowEnrollment(currentApplication, Y);
		updateCurrentAppToER(currentApplication);
		
		LOGGER.info("LceAptcOnlyHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}


	private void createAPTCUpdateEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension) {
		lceAppExtensionEventMigration.createAptcOnlyEvent(currentApplication, applicationExtension);
	}
	
	private void manageApplicationApplicantEventForConversion(SsapApplication currentApplication, String accountTransferCategory) {
		LOGGER.info("manageApplicationApplicantEventForConversion  for application - " + currentApplication.getId());
		if (AccountTransferCategoryEnum.QE.value().equals(accountTransferCategory)) {
			ssapApplicationEventMigrationService.createAndUpdateApplicationEventForNonFinancialQE(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y);
		} else if (AccountTransferCategoryEnum.OE.value().equals(accountTransferCategory)) {
			ssapApplicationEventMigrationService.updateSsapApplicationEventForNonFinancial(currentApplication, SsapApplicationEventTypeEnum.SEP, ReferralConstants.Y);
		}
	}

}

