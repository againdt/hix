import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// Action creators

import { hideModal } from '../../actions/actionCreators';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const LocateAssistanceModal = props => {
    const {
        modal: {
            modalOwner
        }
    } = props;

    const handleHideClick = () => {
        console.log(modalOwner);
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header">
                <div className="modal__btn__close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body">
                <iframe id="brokersearchBox" src="/hix/broker/search" className="searchModal-body"></iframe>
            </div>
        </Fragment>
    );
};

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(null, mapDispatchToProps)(LocateAssistanceModal);
