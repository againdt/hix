package com.getinsured.hix.tkm.activiti;

import java.util.logging.Logger;

import org.activiti.engine.delegate.DelegateExecution;
import org.activiti.engine.delegate.JavaDelegate;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.tkm.service.TicketListService;
import com.getinsured.hix.tkm.service.TkmMgmtService;
import com.getinsured.hix.tkm.service.TkmNotificationService;

@Transactional
public class CancelTicketTaskListener implements JavaDelegate {

	/**
	 * 
	 */
	private static Logger log = Logger.getLogger(CancelTicketTaskListener.class
			.getName());
	@SuppressWarnings("unused")
	private TkmMgmtService tkmMgmtService;
	
	@SuppressWarnings("unused")
	private TkmNotificationService tkmNotificationService;
	
	@SuppressWarnings("unused")
	private TicketListService ticketListService;
	
	public void setTicketListService(TicketListService ticketListService) {
		this.ticketListService = ticketListService;
	}
	
	public void setTkmNotificationService(TkmNotificationService tkmNotificationService) {
		this.tkmNotificationService = tkmNotificationService;
	}

	public void setTkmMgmtService(TkmMgmtService tkmMgmtService) {
		this.tkmMgmtService = tkmMgmtService;
	}

	@Override
	@Transactional
	public void execute(DelegateExecution execution) {
		log.info("Inside CancelTicketTaskListener commented as per HIX-63524");
	/*	TaskService taskService;
		try {
			taskService = execution.getEngineServices().getTaskService();
			List<Task> activeTaskList = taskService.createTaskQuery()
					.processInstanceId(execution.getProcessInstanceId()).list();
			TkmTickets tkmTickets = ticketListService.getTkmTicketsByActivitiTaskId(activeTaskList.get(0).getId()).get(0);
			String subject ="Ticket " + tkmTickets.getSubject()+" is cancelled ";
			// both user name and group name are null
			for (Task activeTasks : activeTaskList) {
				StringBuilder message = new StringBuilder("");
				message.append("<br>Your task execution have run out of time hence closing the ticket. <br/><br/>");
				message.append("Task Id: "+activeTasks.getId()+"<br>");
				message.append("Task Name: "+activeTasks.getName()+"<br>");
				message.append("</a>");
				String assignee = activeTasks.getAssignee();
				String owner = null;
				if (assignee == null) {
					// ASSUMPTION: Owner should always be a group (only one) in
					// BPM file
					List<IdentityLink> links = taskService
							.getIdentityLinksForTask(activeTasks.getId());
					for (IdentityLink identityLink : links) {
						List<Group> group = Context
								.getProcessEngineConfiguration()
								.getIdentityService().createGroupQuery()
								.groupName(identityLink.getGroupId()).list();
						owner = group.get(0).getName();

						if (owner != null) {
							break;
						}
					}
					if (owner != null) {
						List<User> users = Context
								.getProcessEngineConfiguration()
								.getIdentityService().createUserQuery()
								.memberOfGroup(owner).list();
						for (User user : users) {
							Map<String,String> emailTokens = new HashMap<String, String>();
							emailTokens.put("recipient", user.getEmail());
							emailTokens.put("subject", subject);
							emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
							emailTokens.put("taskDetail",message.toString() );
							
							tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);	
					   }
					}
				} else {
					List<User> users = Context.getProcessEngineConfiguration().getIdentityService().createUserQuery().userId(assignee).list();
					User user = users.get(0);
					Map<String,String> emailTokens = new HashMap<String, String>();
					emailTokens.put("recipient", user.getEmail());
					emailTokens.put("subject", subject);
					emailTokens.put("name", user.getFirstName()+" "+user.getLastName());
					emailTokens.put("taskDetail",message.toString() );
					tkmNotificationService.sendEmailNotification(emailTokens,tkmTickets);
				}
			}
			tkmMgmtService.autoCancelProcess(execution.getProcessInstanceId());
		} catch (Exception e) {
			log.info("Exception in notification listener: "+ e);
		}*/
	}


}
