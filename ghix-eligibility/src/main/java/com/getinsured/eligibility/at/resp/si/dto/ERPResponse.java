package com.getinsured.eligibility.at.resp.si.dto;

import java.math.BigDecimal;
import java.util.List;

import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.at.ref.dto.EnrollmentAttributesDTO;

public class ERPResponse {

	private String applicationID;

	private Integer giWsPayloadId;

	private boolean isFullAssessment;

	private String atResponseType;

	private List<TaxHouseholdMember> taxHouseholdMemberList; // change this to atTaxHouseholdMemberList

	private List<TaxHouseholdMember> ssapApplicantList;

	private Long ssapApplicationPrimaryKey;

	private Long compareEnrolledApplicationId;

	private Long compareToApplicationId;
	
	private Long renewalCompareToApplicationId;

	private String accountTransferCategory;

	private List<ApplicationsWithSameIdDTO> applicationsWithSameId;

	private ApplicationExtension applicationExtension;

	private EnrollmentAttributesDTO enrolledApplicationAttributes;

	private boolean cmrAutoLinking = false;

	private boolean multipleCmr = false;

	private boolean nonFinancialCmr = false;
	
	private int nonFinancialCmrId;
	
	private BigDecimal slcspAmount;
	
	private String requester;

	public Long getRenewalCompareToApplicationId() {
		return renewalCompareToApplicationId;
	}

	public void setRenewalCompareToApplicationId(Long renewalCompareToApplicationId) {
		this.renewalCompareToApplicationId = renewalCompareToApplicationId;
	}

	public int getNonFinancialCmrId() {
		return nonFinancialCmrId;
	}

	public void setNonFinancialCmrId(int nonFinancialCmrId) {
		this.nonFinancialCmrId = nonFinancialCmrId;
	}

	public boolean isMultipleCmr() {
		return multipleCmr;
	}

	public void setMultipleCmr(boolean multipleCmr) {
		this.multipleCmr = multipleCmr;
	}

	public boolean isNonFinancialCmr() {
		return nonFinancialCmr;
	}

	public void setNonFinancialCmr(boolean nonFinancialCmr) {
		this.nonFinancialCmr = nonFinancialCmr;
	}

	public boolean isCmrAutoLinking() {
		return cmrAutoLinking;
	}

	public void setCmrAutoLinking(boolean cmrAutoLinking) {
		this.cmrAutoLinking = cmrAutoLinking;
	}

	public String getAccountTransferCategory() {
		return accountTransferCategory;
	}

	public void setAccountTransferCategory(String accountTransferCategory) {
		this.accountTransferCategory = accountTransferCategory;
	}

	public Long getCompareToApplicationId() {
		return compareToApplicationId;
	}

	public void setCompareToApplicationId(Long compareToApplicationId) {
		this.compareToApplicationId = compareToApplicationId;
	}

	public EnrollmentAttributesDTO getEnrolledApplicationAttributes() {
		return enrolledApplicationAttributes;
	}

	public void setEnrolledApplicationAttributes(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		this.enrolledApplicationAttributes = enrolledApplicationAttributes;
	}

	public ApplicationExtension getApplicationExtension() {
		return applicationExtension;
	}

	public void setApplicationExtension(ApplicationExtension applicationExtension) {
		this.applicationExtension = applicationExtension;
	}

	public List<ApplicationsWithSameIdDTO> getApplicationsWithSameId() {
		return applicationsWithSameId;
	}

	public void setApplicationsWithSameId(List<ApplicationsWithSameIdDTO> applicationsWithSameId) {
		this.applicationsWithSameId = applicationsWithSameId;
	}

	public String getApplicationID() {
		return applicationID;
	}

	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}

	public List<TaxHouseholdMember> getTaxHouseholdMemberList() {
		return taxHouseholdMemberList;
	}

	public void setTaxHouseholdMemberList(List<TaxHouseholdMember> taxHouseholdMemberList) {
		this.taxHouseholdMemberList = taxHouseholdMemberList;
	}

	public List<TaxHouseholdMember> getSsapApplicantList() {
		return ssapApplicantList;
	}

	public void setSsapApplicantList(List<TaxHouseholdMember> ssapApplicantList) {
		this.ssapApplicantList = ssapApplicantList;
	}

	/*
	 * @Override public String toString() { return ToStringBuilder.reflectionToString(this,ToStringStyle.MULTI_LINE_STYLE); }
	 */

	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}

	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}

	public boolean isFullAssessment() {
		return isFullAssessment;
	}

	public void setFullAssessment(boolean isFullAssessment) {
		this.isFullAssessment = isFullAssessment;
	}

	public String getAtResponseType() {
		return atResponseType;
	}

	public void setAtResponseType(String atResponseType) {
		this.atResponseType = atResponseType;
	}

	public Long getSsapApplicationPrimaryKey() {
		return ssapApplicationPrimaryKey;
	}

	public void setSsapApplicationPrimaryKey(Long ssapApplicationPrimaryKey) {
		this.ssapApplicationPrimaryKey = ssapApplicationPrimaryKey;
	}

	public Long getCompareEnrolledApplicationId() {
		return compareEnrolledApplicationId;
	}

	public void setCompareEnrolledApplicationId(Long compareEnrolledApplicationId) {
		this.compareEnrolledApplicationId = compareEnrolledApplicationId;
	}

	public BigDecimal getSlcspAmount() {
		return slcspAmount;
	}

	public void setSlcspAmount(BigDecimal slcspAmount) {
		this.slcspAmount = slcspAmount;
	}
	
	public String getRequester() {
		return requester;
	}

	public void setRequester(String requester) {
		this.requester = requester;
	}

}
