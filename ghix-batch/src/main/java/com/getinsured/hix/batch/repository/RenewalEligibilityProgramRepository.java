package com.getinsured.hix.batch.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.eligibility.model.EligibilityProgram;

public interface RenewalEligibilityProgramRepository  extends JpaRepository<EligibilityProgram, Long>{
    
    @Query("Select ep " +
            " FROM EligibilityProgram as ep "+
            " inner join fetch ep.ssapApplicant as ssapApplicant"+
            " where ssapApplicant.id = :ssapApplicantId ")
    List<EligibilityProgram> getApplicantEligibilities(@Param("ssapApplicantId") Long ssapApplicantId);

}
