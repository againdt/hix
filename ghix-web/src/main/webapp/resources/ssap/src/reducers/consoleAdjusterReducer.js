import { consoleActionTypes } from '../constants/actionTypes';

export const initialState = {
    isFormValidationShown: false,
    isInvalidInputsShown: false,
    isValidatorsGeneratorResultsShown: false,
};

export default (state = initialState, action) => {

    switch(action.type) {

    case consoleActionTypes.SWITCH_CONSOLE_MODE:
        return {
            ...state,
            ...action.payload
        };

    case consoleActionTypes.CLEAR_STATE:
        return initialState;

    default:
        return state;
    }
};