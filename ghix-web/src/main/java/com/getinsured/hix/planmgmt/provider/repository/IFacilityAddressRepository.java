package com.getinsured.hix.planmgmt.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.FacilityAddress;

public interface IFacilityAddressRepository extends JpaRepository<FacilityAddress, Integer> {

	@Query("SELECT f FROM FacilityAddress f where LOWER(f.county) = LOWER(:county) AND LOWER(f.address1) = LOWER(:address) AND LOWER(f.facility) = LOWER(:facility)")
    FacilityAddress getFacilityAddress(@Param("county") String county,@Param("address") String address,@Param("facility") int facility);
}
