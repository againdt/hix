<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df" %>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <!DOCTYPE html>
    <html lang="en">
    <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link id="favicon" rel="shortcut icon" type="image/x-icon" href="">
    <title>SSAP</title>
    <script type="text/javascript">
    window.csrf_token = '<df:csrfToken plainToken="true"/>';
    window.ssapApplicationObj = {
    "ssapId" : ${ssapId},
    "coverageYear": "${coverageYear}",
    "applicationType": "${applicationType}",
    "mode" : "${mode}"
    };
    </script>
    <link rel="stylesheet" href="<c:url value="/resources/ssap/assets/uswds-1.6.9/css/uswds.min.css"/>" />
    <link rel="stylesheet" href="<c:url value="/resources/ssap/build/css/index.css"/>" />
    <script src="<c:url value="/resources/ssap/assets/uswds-1.6.9/js/uswds.min.js"/>"></script>
    </head>
    <body>
    <div id="root"></div>
    <script src="<c:url value="/resources/ssap/build/static/js/main.js"/>"></script>
    </body>
    </html>
