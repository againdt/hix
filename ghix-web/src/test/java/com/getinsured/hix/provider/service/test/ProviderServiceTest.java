package com.getinsured.hix.provider.service.test;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.Specialty.GroupName;
import com.getinsured.hix.planmgmt.provider.repository.IFacilityRepository;
import com.getinsured.hix.planmgmt.provider.repository.IPhysicianRepository;

public class ProviderServiceTest extends BaseTest {
	
	@Autowired private IPhysicianRepository iPhysicianRepository;
	
	@Autowired private IFacilityRepository iFacilityRepository;
	
//	@Autowired private ISpecialtyRepository iSpecialtyRepository;
//	
//	@Autowired private INetworkRepository iNetworkRepository;
	
	@Test
	public void findDoctor(){
		String name="Shekhar Jaiswar";
		String gender="M";
		int specialities=94;
		String languages="ENGLISH";
		String doctorNearZip="87505";
		String groupname=GroupName.DOCTOR.toString().toLowerCase();
		List<Physician> physicianslist_doctor=iPhysicianRepository.getPhysicians(name, gender, specialities, languages, doctorNearZip, groupname);
		System.out.println("No of Doctors: " + physicianslist_doctor.size());
		for(int i=0;i<physicianslist_doctor.size();i++){
			System.out.println("Doctors Info: " + physicianslist_doctor.get(i));
		}
		
	}
	
	@Test
	public void findDentist(){
		String name="Sandeep Jaiswal";
		String gender="M";
		int specialities=95;
		String languages="ENGLISH";
		String doctorNearZip="87505";
		String groupname=GroupName.DENTIST.toString().toLowerCase();
		List<Physician> physicianslist_dentist=iPhysicianRepository.getPhysicians(name, gender, specialities, languages, doctorNearZip, groupname);
		System.out.println("No of Dentist: " + physicianslist_dentist.size());
		for(int i=0;i<physicianslist_dentist.size();i++){
			System.out.println("Dentist Info: " + physicianslist_dentist.get(i));
		}
		
	}
	
}
