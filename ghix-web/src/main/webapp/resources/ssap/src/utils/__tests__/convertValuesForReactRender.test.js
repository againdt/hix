import { convertValuesForReactRender } from '../convertValuesForReactRender';

describe('Test for convertNonRenderedToStrings', () => {
    describe('Check a representation for an Array', () => {
        it('Should be empty array', () => {
            expect(convertValuesForReactRender([]))
                .toBe('[ ]');
        });

        it('Should be not empty array', () => {
            expect(convertValuesForReactRender([1, 7, 'd']))
                .toBe('[ 1,7,d ]');
        });
    });

    describe('Check non-rendered values', () => {
        it('Should render null', () => {
            expect(convertValuesForReactRender(null))
                .toBe('NULL');
        });

        it('Should render null', () => {
            expect(convertValuesForReactRender(false))
                .toBe('FALSE');
        });

        it('Should render null', () => {
            expect(convertValuesForReactRender(true))
                .toBe('TRUE');
        });

        it('Should render null', () => {
            expect(convertValuesForReactRender(undefined))
                .toBe('UNDEFINED');
        });

        it('Should render {}', () => {
            expect(() => {
                convertValuesForReactRender({ name: 'Air France' });
            }).toThrow('Object has passed to render');
        });
    });
});