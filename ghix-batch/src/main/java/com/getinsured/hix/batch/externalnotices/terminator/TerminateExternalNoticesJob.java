package com.getinsured.hix.batch.externalnotices.terminator;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;


public class TerminateExternalNoticesJob implements Tasklet{

	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) 
	{
		return RepeatStatus.FINISHED;
	}

}
