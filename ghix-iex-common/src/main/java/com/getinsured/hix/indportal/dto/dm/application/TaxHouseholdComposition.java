package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;
import java.util.ArrayList;

public class TaxHouseholdComposition {
	 private String taxHouseHoldStatus;
	 private String taxHouseHoldStatusReason;
	 private ArrayList < BigDecimal > taxHouseholdMemberIdentifiers;


	 // Getter Methods 

	 public String getTaxHouseHoldStatus() {
	  return taxHouseHoldStatus;
	 }

	 public String getTaxHouseHoldStatusReason() {
	  return taxHouseHoldStatusReason;
	 }

	 // Setter Methods 

	 public void setTaxHouseHoldStatus(String taxHouseHoldStatus) {
	  this.taxHouseHoldStatus = taxHouseHoldStatus;
	 }

	 public void setTaxHouseHoldStatusReason(String taxHouseHoldStatusReason) {
	  this.taxHouseHoldStatusReason = taxHouseHoldStatusReason;
	 }

	public ArrayList<BigDecimal> getTaxHouseholdMemberIdentifiers() {
		return taxHouseholdMemberIdentifiers;
	}

	public void setTaxHouseholdMemberIdentifiers(ArrayList<BigDecimal> taxHouseholdMemberIdentifiers) {
		this.taxHouseholdMemberIdentifiers = taxHouseholdMemberIdentifiers;
	}
	}