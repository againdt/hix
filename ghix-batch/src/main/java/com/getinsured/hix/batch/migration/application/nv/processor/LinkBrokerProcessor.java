package com.getinsured.hix.batch.migration.application.nv.processor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.getinsured.batch.util.JsonUtil;
import com.getinsured.hix.batch.migration.application.nv.mapper.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationDTO;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationJSONDTO;
import com.getinsured.hix.indportal.dto.dm.application.Assister;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.result.BrokerDesignationData;
import com.getinsured.iex.household.repository.HouseholdRepository;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;

public class LinkBrokerProcessor implements ItemProcessor<String, BrokerDesignationData>, ApplicationContextAware {
	private final static Logger LOGGER = Logger.getLogger(LinkBrokerProcessor.class);
	
	private String doNotOverwrite;
	private static ApplicationContext applicationContext;
	private Gson mapper = new GsonBuilder().disableHtmlEscaping().create();

	private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	private static final String LOGGERSTMT1 ="Reading Application JSON :";
	private static final String JSON_START = "{\"application\":{";
	private static final String JSON_END = "}}";
	private static final String APPLICATION_START = "\"applications\":{";
	private String waitInterval;
	private static final int WAIT_COUNT = 3;	
	private HouseholdRepository householdRepository; 
	private static final String SORT_BY_VALUE = "creationTimestamp";
	
	private SsapApplicationRepository ssapApplicationRepository;
	
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, false);

		} catch (JAXBException e) {
			LOGGER.error("Error while initializing JAXBContext", e);
		}

	}

	public BrokerDesignationData process(String result) throws Exception {
		BrokerDesignationData data = new BrokerDesignationData();
		householdRepository = (HouseholdRepository) applicationContext.getBean("householdRepository");
		data.setJsonSource(result);
		if (!StringUtils.isEmpty(result)) {
			
			if(StringUtils.startsWithIgnoreCase(result, APPLICATION_START)) {
				result = result.substring(APPLICATION_START.length(), result.length()-1);
			}
			else if (StringUtils.endsWithIgnoreCase(result, ",")) {
				result = result.substring(0, result.length() - 1);
			}
			result = JSON_START + result + JSON_END;
		}
		try {
			LOGGER.info(LOGGERSTMT1+result);
			JsonReader jsonReader = mapper.newJsonReader(new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
			ApplicationJSONDTO applicationDTO = mapper.fromJson(jsonReader, ApplicationJSONDTO.class);
			ApplicationDTO application=applicationDTO.getApplication().values().iterator().next();
				
			ArrayList<Assister> assisters = application.getResult().getAttestations().getApplication().getApplicationAssistors();
			//if(!CollectionUtils.isEmpty(assisters) && "AGENT_BROKER".equals(assisters.get(0).getApplicationAssistorType()))
			//{
				LOGGER.debug("Starting broker linking processor for application : "+application.getResult().getInsuranceApplicationIdentifier().toString()); 
				long waitingTime = getWaitingTimeInterval();
				if(waitingTime!=0) {
					//Map<String,String> conditionMap = new HashMap<String, String>();
					//conditionMap.put("CASEID", application.getResult().getInsuranceApplicationIdentifier().toString());
					//conditionMap.put("EMAIL", application.getResult().getAttestations().getApplication().getContactInformation().getEmail());
					waitForATCompletion(application.getResult().getInsuranceApplicationIdentifier().toString(),waitingTime);
				}
				ssapApplicationRepository = (SsapApplicationRepository) applicationContext.getBean("ssapApplicationRepository");
				PageRequest pageRequest = new PageRequest(0, 2, Sort.Direction.DESC, SORT_BY_VALUE);
				LOGGER.debug("Going to fetch ssapApplication by ExternalAppId");
				List<SsapApplication> applicationList = ssapApplicationRepository.findLatestByExternalAppId(application.getResult().getInsuranceApplicationIdentifier().toString(), pageRequest);
				LOGGER.debug("ssapApplication fetch complete");
				boolean processApplication = true;
				if ("true".equalsIgnoreCase(doNotOverwrite) && applicationList != null && applicationList.size() > 1) {
					processApplication = false;
				}
				if (processApplication) {
					boolean brokerChanged = false;
					if (applicationList != null && applicationList.size() > 1) {
						SsapApplication ssapApplication = applicationList.get(1);
						final SingleStreamlinedApplication applicationData = JsonUtil.getSingleStreamlinedApplicationFromJson(ssapApplication.getApplicationData());
						if (applicationData.getBrokerChanged() != null && applicationData.getBrokerChanged() == true) {
							brokerChanged = true;
						}
					}
					if (!brokerChanged) {
						
						String contactMemberIdentifier = application.getResult().getAttestations().getApplication().getContactMemberIdentifier();
						AttestationsMember member = application.getResult().getAttestations().getMembers().get(contactMemberIdentifier);
						if(!CollectionUtils.isEmpty(assisters) && "AGENT_BROKER".equals(assisters.get(0).getApplicationAssistorType())){
							data.setBrokerNPN(assisters.get(0).getAssistorNationalProducerNumber());
						}
						data.setHouseholdEmail(application.getResult().getAttestations().getApplication().getContactInformation().getEmail());
						data.setExtHouseholdCaseId(application.getResult().getInsuranceApplicationIdentifier().toString());
						if (member != null)
						{
							if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getFirstName())){
								data.setEsignFirstName(member.getDemographic().getName().getFirstName());
							}
							if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getLastName())){
								data.setEsignLastName(member.getDemographic().getName().getLastName());
							}
						}
					}
				}
				
				LOGGER.debug("Broker linking processor end for application : "+application.getResult().getInsuranceApplicationIdentifier().toString());
			//}
			
		} catch (Exception e) {
			data.setErrorMessage(ExceptionUtils.getFullStackTrace(e).replaceAll(System.lineSeparator(), " "));
			LOGGER.error("Unable to read next JSON object" + e);
			//throw new ParseException("Unable to read next JSON object", e);
		}
		return data;
	}
	
	private long getWaitingTimeInterval() {
		
		long waitTime=0;
		
		if(!StringUtils.isEmpty(waitInterval)) {
			try {
				waitTime = Long.parseLong(waitInterval);
			}catch(Exception e) {
				LOGGER.error("Error in parsing wait Interval : " + e);
			}
		}
		
		return waitTime;
	}
	
	private void waitForATCompletion(String caseId,long waitingTime) {

		LOGGER.debug("waitForATCompletion : START");
		int sleepCount = WAIT_COUNT;
		Predicate<String> householdExists = m -> {
			List<Integer> houseHoldIds = householdRepository.findByCaseIdOrderByIdDesc(caseId);
			return (houseHoldIds!=null && houseHoldIds.size()>0);
		};

		try {

			while (householdExists.negate().test(caseId) && sleepCount > 0) {
				Thread.sleep(waitingTime);
				sleepCount--;
			}

		} catch (InterruptedException ie) {
			LOGGER.error("Got interrupted while waiting for AT push completion" + ie);
		} catch (Exception e) {
			LOGGER.error("Exception while checking for AT push completion" + e);
			throw e;
		}
		
		LOGGER.debug("waitForATCompletion : END");

	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		applicationContext = context;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setWaitInterval(String waitInterval) {
		this.waitInterval = waitInterval;
	}

	public void setDoNotOverwrite(String doNotOverwrite) {
		this.doNotOverwrite = doNotOverwrite;
	}
}
