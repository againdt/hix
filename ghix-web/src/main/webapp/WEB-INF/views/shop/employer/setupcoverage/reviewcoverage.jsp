<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/datepicker.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript">
function doActionOnly(actionType){
		var validateUrl = '<c:url value="/shop/employer/setupcoverage/reviewcoverageredirect">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';

	 if(actionType != 'confirm'){
		 window.location.href='<c:url value="/shop/employer/dashboard"/>';
	 }else{
		 $.ajax({
				url: validateUrl, 
				type: "POST", 
				async: false,
				cache : false,
				data : ({
					eSignName : $('#esign').val(), eSignDate: $('#esignDate').val()
                }),
				success: function(response,xhr){ 
				      if(isInvalidCSRFToken(xhr))             
					        return;
					if(response=='confirm'){
						alert("confirmed successfully");
					}
				},
				
		 });
	 }
     
}

function changePage(){
	if ($("#reviewconfirmform").validate().form()){
		doActionOnly("confirm");
		$('#reviewConfirm').hide();
		$('#reviewCongrats').show();
		$('#sidebar .nav li.active').replaceWith('<li><a href="#"><i class="icon-ok"></i> Review and Confirm</a></li>');
		$('#rightpanel .header h4').replaceWith('<h4>Congratulations</h4>');
		$('#rightpanel .header a, .form-actions').hide();
		$('#reviewCongrats .form-actions').show();
		$('.reviewConfirmActionNext').hide();
		$('.reviewConfirmActionSave').hide();
	}
}

function openFindBrokerModal(){
		$('#findbroker').modal('show');
}
 $(function() {
	 
	 $('#findbroker').click(function(e){
			e.preventDefault();
			var href = $(this).attr('href');
			if (href.indexOf('#') != 0)
				openFindBrokerDialog(href);
		});
	 
     $('#addempdiv :a').attr('disabled', 'disabled'); 
});
 
 $('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});

$(document).keyup(function(e) {
	if (e.keyCode == 27) {
		$('#modal').remove();
	}
});

$(document).ready(function(){
	$('.date-picker').datepicker({
		autoclose:true,
		forceParse: false
	});


	
	$('.info').tooltip();
});
</script>
<div class="gutter10">  	
	<div class="row-fluid margin30-t">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage Setup</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/shop/employer/setupcoverage/gettingstarted' />"><i class="icon-ok"></i> Getting Started</a></li>
	      		<li><a href="<c:url value='/shop/employer/setupcoverage/healthcoverage' />"><i class="icon-ok"></i> Health Coverage Setup</a></li>
	      	<c:if test="${isDentalAvailable=='YES' }">	<li><a href="<c:url value='/shop/employer/setupcoverage/dentalcoverage' />"><i class="icon-ok"></i> Dental Coverage Setup</a></li></c:if>
	      		<li class="active"><a href="#">4 Review and Confirm</a></li>
	      	</ul>

	      	<div class="header margin30-t">
	      		<h4>Quick Links</h4>
	      	</div>
	      	<ul class="nav nav-list">
	      		<li><a href="<c:url value='/broker/search' />" id="findbroker"><i class="icon-search"></i> Find an Agent</a></li>
	      	</ul>
	    </div>
    
	    <div class="span9" id="rightpanel">
	    	<div class="header">
	    		<h4 class="pull-left">Review and Confirm</h4>
	    		<a href="#" class="btn btn-small btn-primary pull-right reviewConfirmActionNext"  onclick="changePage();">Confirm</a>
	    		<a href="#" class="btn btn-small pull-right reviewConfirmActionSave"  onclick="doActionOnly('saveConfirm');">Save, and Confirm Later</a>
	    	</div>

	    	<div class="gutter10">
		    	<form class="form-horizontal" id="reviewconfirmform" name="reviewconfirmform">

					<div class="gutter10" id="reviewConfirm">
						<div class="margin20-b">
							<p>Please review your choices and provide your eSignature if everything is correct.</p>
						</div><!-- .row-fluid -->

						<div class="healthCoverageResults margin30-tb border-custom">
							<div class="header">
								<h4 class="pull-left">Health Coverage Selections</h4>
								<a href="healthcoverage" class="pull-right btn btn-small">Change</a>
							</div>
							<dl class="row-fluid margin20-t">
								<dt class="span5">Coverage Start Date</dt>
								<dd class="span7">
									<span id="coverageEffectiveDate">${coverageEffectiveDate}</span>
									<span id="openEnrollmentDates"><small>Open Enrollment:</small> ${openEnrollmentDates}</span>
									<span id="firstPaymentDue"><small>First Payment Due:</small> ${firstPaymentDue}</span>
								</dd>
							</dl>
							<dl class="row-fluid margin30-t">
								<dt class="span5">Contribution %</dt>
								<dd class="span7">
									<span id="contributionTowardsEmployees">${employeeHealthContribution}% for Employees</span> 
									<span id="contributionTowardsDependents">${dependentHealthContribution}% for Dependents</span>
								</dd>
							</dl>
							<dl class="row-fluid margin30-t">
								<dt class="span5">Employee Plan Choice</dt>
								<dd class="span7">
									<span id="employeePlanChoice">${tierPlanCount} ${tierName} Plans</span> 
								</dd>
							</dl>
							<dl class="row-fluid margin30-t">
								<dt class="span5">Cost Basis Plan</dt>
								<dd class="span7">
									<span id="costBasisPlan">${selectedPlanPos} ${tierName} Plan</span> 
								</dd>
							</dl>
						</div>
						<c:if test="${isDentalAvailable=='YES' }">
						<div class="dentalCoverageResults margin30-b border-custom">
							<div class="header">
								<h4 class="pull-left">Dental Coverage Selections</h4>
								<a href="dentalcoverage" class="pull-right btn btn-small">Change</a>
							</div>
							<dl class="row-fluid margin20-t">
								<dt class="span5">Monthly Contribution Amounts</dt>
								<dd class="span7"> 
									<span id="contributionTowardsEmployeesDental">$${employeeDentalContribution} per Employee</span>
									<span id="contributionTowardsDependentsDental">$${dependentDentalContribution} per Dependent</span>
								</dd>
							</dl>
						</div>
						</c:if>
						
						<div class="totalCostResults margin30-b border-custom">
							<div class="header">
								<h4 class="pull-left">Your Total Cost</h4>
							</div>
							<dl class="row-fluid margin20-t">
								<dt class="span5">Contribution toward Employees</dt>
								<dd class="span7"> 
									<span id="">${employerContributionEmployee} per month <small>(based on ${totalEmp} employees)</small></span>
								</dd>
							</dl>
							<dl class="row-fluid">
								<dt class="span5">Contribution toward Dependents</dt>
								<dd class="span7"> 
									<span id="yourCostAmount">${employerContributionDependent} per month <small>(based on ${totalDependents} dependents)</small></span>
								</dd>
							</dl>
							<dl class="row-fluid">
								<dt class="span5">Total Contributions</dt>
								<dd class="span7"> 
									<span id="yourCostAmount">${employerContributionTotalMonth} per month <small>(${employerContributionTotalYear} per year)</small></span>
									<!-- <span><small>Our total contributions will be 6% higher than they are currently.</small></span> -->
								</dd>
							</dl>
						</div>

						<div class="taxBenefitResults margin30-b border-custom">
							<div class="header">
								<h4 class="pull-left">Tax Benefit</h4>
							</div>
							<dl class="row-fluid margin20-t">
								<dt class="span5">Estimated Tax Credit</dt>
								<dd class="span7"> 
									<span id="taxCreditAmount">${taxCredit} per month <small>(${taxCreditYear} per year)</small></span>
									<span><small>Tax credit provided here is only an estimate. Contact your tax advisor for more information about your specific situation.</small></span>
								</dd>
							</dl>
						</div>

						<div class="eSignatureResults border-custom">
							<div class="header">
								<h4 class="pull-left">eSignature</h4>
							</div>
							<div class="gutter10 eSignature margin10-t">
								<p>I understand that the company must meet any participation requirements and pay the initial invoice on time for coverage to take effect.</p>
								<div class="row-fluid margin20-t margin20-b">
									<div class="span8">
										<span>eSignature:</span>
										<input type="text" name="esign" id="esign" class="input-xlarge" placeholder="Your Name" /><br/>
										<span class="signature-text">${employerName}</span>
									</div>
									<div class="span4">
										<span>Date:</span>
										<c:set var="now" value="<%=new java.util.Date()%>" />
										<input type="text" name="esignDate" class="input-medium"  id="esignDate"  value="<fmt:formatDate value="${now}" type="date" dateStyle="long" pattern="MM/dd/yyyy" />" readonly/>				                  												
									</div>
									<div id="esign_error"></div>
									<div id="esignDate_error"></div>
								</div>
							</div>
						</div>
					</div>

					<div class="gutter10" id="reviewCongrats" style="display:none">
						<div class="margin20-b">
							<p>Now that you've made your selections and locked in your costs as an employer, you're ready to start the open enrollment process.  During open enrollment, your employees will verify their personal information and choose the specific healthcare plans to enroll in.</p>
						</div>
						<div class="form-actions">
							<a href="<c:url value='/shop/employer/dashboard' />" class="btn btn-primary btn-large margin75-l">Done</a>
						</div>
					</div>

					<div class="form-actions">
						<a href="#" class="btn btn-large reviewConfirmActionSave" onclick="doActionOnly('saveConfirm');" >Save, and Confirm Later</a>
						<a href="#" class="btn btn-primary btn-large reviewConfirmActionNext" onclick="changePage();" >Confirm</a>
					</div>
				</form>
	    	</div><!-- .gutter10 -->
		</div><!-- .span9 #rightpanel -->
	</div><!-- .row-fluid -->
</div><!-- .gutter10 -->

<script type="text/javascript">

jQuery.validator.addMethod("esigncheck", function(value, element, param) {
	var empname = '${employerName}'.replace('&amp;','&');

	var esign = $("#esign").val();
	if( esign.toLowerCase() == empname.toLowerCase() ){
		return true ;
	}
	return false;
});

var validator = $("#reviewconfirmform").validate({ 
	onkeyup: false,
	onclick: false,
	onfocusout: false,
	onSubmit: true,
	rules : {
		esign :{ required: true, esigncheck :true},
		esignDate : { required:true}      
	},
	messages : {
		esign: { required : "<span> <em class='excl'>!</em>Please Add Esignature</span>" ,
				   esigncheck : "<span> <em class='excl'>!</em>Esignature should match your first name and lastname</span>"},
		esignDate: {required: "<span> <em class='excl'>!</em>Please Select Date</span>"}	
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});
</script>