package com.getinsured.hix.batch.ssap.renewal.service;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.batch.core.UnexpectedJobExecutionException;

import com.getinsured.iex.ssap.model.RenewalApplication;

/**
 * Interface is used to provide services for Auto Enroll.
 */
public interface AutoEnrollService {

	String AUTO_ENROLL_JOB_NAME = "SsapRedeterminationAutoEnrollJob";

	void saveAndThrowsErrorLog(String errorMessage) throws UnexpectedJobExecutionException;

	Integer logToGIMonitor(Exception e, int errorCode, String caseNumber);
	
	Integer logToGIMonitor(String errorMessage, String errorCode);

	List<Long> getAutoRenewalApplIdListByCoverageYear(boolean processErrorRecordFlag, Long renewalYear, long batchSize);

	List<RenewalApplication> getAutoRenewalApplListByIds(List<Long> applicationIdList);

	List<Long> getAutoRenewalApplIdListByServerName(boolean processErrorRecordFlag, Long renewalYear, int serverCount,
			int serverName, long batchSize);

	AtomicBoolean processAutoEnrollData(RenewalApplication renewalApplication, AtomicBoolean processErrorRecordFlag,
			AtomicLong renewalYear, int serverName) throws Exception;
}
