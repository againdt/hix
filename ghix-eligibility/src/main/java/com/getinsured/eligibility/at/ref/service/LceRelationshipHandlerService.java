package com.getinsured.eligibility.at.ref.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;

/**
 * @author chopra_s
 * 
 */
@Component("lceRelationshipHandlerService")
@Scope("singleton")
public class LceRelationshipHandlerService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceRelationshipHandlerService.class);

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceRelationshipHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		createSepEvent(currentApplication);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		updateAllowEnrollment(currentApplication, Y);
		// the below notice EE057 will not be triggered in case of MN, hence check if state is MN dont trigger this notice 
		// rest of the states this notice will be sent 
		String stateCode = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (!stateCode.equalsIgnoreCase("MN")) {
			triggerChangeActionEmail(currentApplication,lceProcessRequestDTO,true);	
		}		
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
		LOGGER.info("LceRelationshipHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createSepEvent(SsapApplication currentApplication) {
		LOGGER.info("Create Relationship event - " + currentApplication.getId());
		ssapApplicationEventService.createRelationshipApplicationEvent(currentApplication);
	}
}
