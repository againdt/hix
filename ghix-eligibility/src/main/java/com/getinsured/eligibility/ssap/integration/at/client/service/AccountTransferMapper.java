package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityCodeType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferActivityType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.TransferHeaderType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.InformationExchangeSystemType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.AssisterType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationAssisterAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.MedicaidHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.PhysicalHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FamilyRelationshipHIPAACodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.PathNotFoundException;

/**
 * @author Ahtesham
 *
 */
@Component(value="accountTransferMapper")
@DependsOn("dynamicPropertiesUtil")
public class AccountTransferMapper {
	
	@Autowired	private VerificationMetadataWrapper verificationMetadataWrapper;
	@Autowired	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private InsuranceApplicationMapper insuranceApplicationMapper;
	@Autowired private PersonTypeMapper personTypeMapper;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private MedicaidHouseholdMapper medicaidHouseholdMapper;
	private static Logger lOGGER = Logger.getLogger(AccountTransferMapper.class);
	private int coverageSeekingCount = 0;
	
	/**
	 * Transforms appDataJson to AccountTransfer JAXB object.
	 * 
	 * @param ssap JSON and applicants verifications
	 * @return AccountTransferRequestPayloadType
	 * @throws ParseException
	 * @throws DatatypeConfigurationException
	 */
	@SuppressWarnings({ "rawtypes" })
	public AccountTransferRequestPayloadType populateAccountTransferRequest(String applicantsVerification, String appDataJson)
			throws PathNotFoundException, GIException {
		
		JSONObject singleStreamlinedApplication = (JSONObject)JsonPath.read(appDataJson,"singleStreamlinedApplication");
		AccountTransferRequestPayloadType returnObj = AccountTransferUtil.mainFactory.createAccountTransferRequestPayloadType();
	    returnObj.setAtVersionText(AccountTransferUtil.AT_VERSION_TXT);
		lOGGER.debug("appDataJson" + appDataJson);
	    Long ssapId = Long.valueOf((String) JsonPath.read(appDataJson,"$.singleStreamlinedApplication.ssapApplicationId"));
	    SsapApplication ssapAp = ssapApplicationRepository.findOne(ssapId);
		List<JSONObject> houseHolds = JsonPath.read(appDataJson,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
		JSONObject primaryHouseHold = houseHolds.get(0); 
		// Get the verification metadata for applicants in the household
		Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas = verificationMetadataWrapper.createVerificationMetadata(ssapId);
		
         // Add all verification metadata
        for (Map<String, VerificationMetadataType> personVerificationMetadatas : verificationMetadatas.values()) {
            for (VerificationMetadataType verificationMetadata : personVerificationMetadatas.values()) {
                returnObj.getVerificationMetadata().add(verificationMetadata);
            }
        }
		// number of applicants seeking coverage
		setCoverageSeekingCount(houseHolds);
		// transfer header
		returnObj.setTransferHeader(createTransferHeader(ssapAp.getCaseNumber()));
		// Sender
		returnObj.getSender().add(createSender());
		returnObj.getSender().get(0).setId("SBM1"); 
		// Receiver
		returnObj.getReceiver().add(createReceiver());
		// InsuranceApplication
		if (AccountTransferUtil.checkBoolean(singleStreamlinedApplication.get("getHelpIndicator"))){
			AssisterType assisterType = new AssisterMapper().createAssister(singleStreamlinedApplication);  
			if(assisterType != null){
				returnObj.setAssister(assisterType);
			}
		}
		returnObj.setInsuranceApplication(insuranceApplicationMapper.mapInsuranceApplication(appDataJson, houseHolds, verificationMetadatas));
		
		if(returnObj.getAssister() != null){
			ReferenceType assisterReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			assisterReferenceType.setRef(returnObj.getAssister());
			// link assister
			InsuranceApplicationAssisterAssociationType insuranceApplicationAssisterAssociationType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicationAssisterAssociationType();
			insuranceApplicationAssisterAssociationType.setInsuranceApplicationAssisterReference(assisterReferenceType);
			returnObj.getInsuranceApplication().setInsuranceApplicationAssisterAssociation(insuranceApplicationAssisterAssociationType);
		}
		//medicaidHousehold element
		for (JSONObject houseHold : houseHolds) {
			returnObj.getMedicaidHousehold().add(medicaidHouseholdMapper.createMedicaidHouseholdType(ssapId, houseHold, houseHolds.size()));
		}
		// Person element
		for (JSONObject houseHold : houseHolds) {
			// add person
			returnObj.getPerson().add(personTypeMapper.createPerson(houseHold,primaryHouseHold, verificationMetadatas,ssapId,houseHolds,returnObj));
			// Reference linking person to applicant
			ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			
			// Reference linking person to SSFsigner
			returnObj.getInsuranceApplication().getSSFSigner().setRoleOfPersonReference(referenceType);
			returnObj.getInsuranceApplication().getSSFPrimaryContact().setRoleOfPersonReference(referenceType);
		}
		//TODO: authorizedRepresentativeIndicator is null if agent is not designated so we need to change below condition.
		if(singleStreamlinedApplication.get("authorizedRepresentativeIndicator") != null){
			if((java.lang.Boolean)singleStreamlinedApplication.get("authorizedRepresentativeIndicator")){
				returnObj.setAuthorizedRepresentative(new AuthorizedRepresentativeMapper().createAuthorizedRep((JSONObject)singleStreamlinedApplication.get("authorizedRepresentative")));
		    }
		}
				
		
		Long coverageYear = ssapAp.getCoverageYear();
		// TaxReturn
		JSONObject application =  (JSONObject)JsonPath.read(appDataJson,"singleStreamlinedApplication");
		Object primaryTaxFilerPersonId = ((JSONObject)((JSONArray) application.get("taxHousehold")).get(0)).get("primaryTaxFilerPersonId"); 
	    if(primaryTaxFilerPersonId != null && !primaryTaxFilerPersonId.toString().equals("0") ){
	    	TaxHouseHoldMapper taxHouseHoldMapper = new TaxHouseHoldMapper();
	    	taxHouseHoldMapper.setGhixRestTemplate(ghixRestTemplate);
			returnObj.getTaxReturn().add(taxHouseHoldMapper.createTaxReturns(appDataJson,coverageYear , returnObj, verificationMetadatas, ssapId));
	    }
		
		returnObj = createReferences(returnObj, appDataJson);
		lOGGER.debug(" created JAXB object from the given JSON : "+returnObj );
		return returnObj;
	}
	
	private AccountTransferRequestPayloadType createReferences(
			AccountTransferRequestPayloadType returnObj, String appDataJson) {
		List<JSONObject> houseHolds = JsonPath.read(appDataJson,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
		
		int headCount = houseHolds.size();
		int skip = 0;
		for (int i = 0; i < headCount; i++) {
			// link toApplicants
			if ((java.lang.Boolean) houseHolds.get(i).get("applyingForCoverageIndicator")) {
				ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
				referenceType.setRef(returnObj.getPerson().get(i));
				returnObj.getInsuranceApplication().getInsuranceApplicant().get(skip).setRoleOfPersonReference(referenceType);
				skip = skip + 1;
			}
		}
		// link to PhysicalHouseHold
		// who lives with Primary

		PhysicalHouseholdType primaryPhysicalHouseHold = AccountTransferUtil.insuranceApplicationObjFactory
				.createPhysicalHouseholdType();

		for (int i = 0; i < headCount; i++) {
			ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			referenceType.setRef(returnObj.getPerson().get(i));
			if ((java.lang.Boolean) houseHolds.get(i).get("livesWithHouseholdContactIndicator")) {
				primaryPhysicalHouseHold.getHouseholdMemberReference().add(referenceType);
			} else {
				PhysicalHouseholdType separatePhysicalHouseHold = AccountTransferUtil.insuranceApplicationObjFactory.createPhysicalHouseholdType();
				separatePhysicalHouseHold.getHouseholdMemberReference().add(referenceType);
				returnObj.getPhysicalHousehold().add(separatePhysicalHouseHold);
			}
		}
		
		List<MedicaidHouseholdType> medicaidHouseholdTypeList = returnObj.getMedicaidHousehold();
		if(medicaidHouseholdTypeList.size() > 0){
			for (MedicaidHouseholdType medicaidHouseholdType : medicaidHouseholdTypeList) {
				for (int i = 0; i < headCount; i++) {
					ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
					referenceType.setRef(returnObj.getPerson().get(i));
					medicaidHouseholdType.getHouseholdMemberReference().add(referenceType);
				}
			}
		}

		returnObj.getPhysicalHousehold().add(primaryPhysicalHouseHold);

		// createPersonAssociation under for each person under any person.
		AccountTransferRequestPayloadType returnObjwithoutRelations = returnObj; 
		returnObj = createPersonRelationships(appDataJson, returnObjwithoutRelations);
		
		// SSF referneces
		ReferenceType primaryReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
		primaryReferenceType.setRef(returnObj.getPerson().get(0));
		returnObj.getInsuranceApplication().getSSFSigner().setRoleOfPersonReference(primaryReferenceType);

		returnObj.getInsuranceApplication().getSSFPrimaryContact().setRoleOfPersonReference(primaryReferenceType);

		for(InsuranceApplicantType  insuranceApplicant: returnObj.getInsuranceApplication().getInsuranceApplicant()){
			ReferenceType reciverReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			reciverReferenceType.setRef(returnObj.getReceiver().get(0));
			insuranceApplicant.getReferralActivity().setReferralActivityReceiverReference(reciverReferenceType);
			ReferenceType senderReferenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
			senderReferenceType.setRef(returnObj.getSender().get(0));
			insuranceApplicant.getReferralActivity().setReferralActivitySenderReference(senderReferenceType);
		}
		
		
		return returnObj;
	}
	/**
	 * 
	 * @param appDataJson
	 * @param returnObj
	 * @return
	 */
	private AccountTransferRequestPayloadType createPersonRelationships(String appDataJson, AccountTransferRequestPayloadType returnObj){
		
		List<JSONArray> relations = JsonPath.read(appDataJson,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*].bloodRelationship");
		List<JSONObject> relationJson = new ArrayList<JSONObject>();
		for (int i = 0; i < relations.size(); i++) {
			JSONArray relationJsonList = relations.get(i);
			for (int j = 0; j < relationJsonList.size(); j++) {
				if(relationJsonList.get(j) != null){
					relationJson.add((JSONObject) relationJsonList.get(j));
				}
			}
		}
			
		int numOfperson = returnObj.getPerson().size();
		for(int i=0; i< numOfperson; i++){
			for (int k = 0; k < numOfperson; k++) {
				if (i != k) {
				ReferenceType referenceType = AccountTransferUtil.niemStructFactory.createReferenceType();
				referenceType.setRef(returnObj.getPerson().get(k));
				PersonAssociationType personAssociationType = AccountTransferUtil.hixCoreFactory.createPersonAssociationType();
				personAssociationType.getPersonReference().add(referenceType);
				for(JSONObject relation : relationJson ){
					lOGGER.debug(" relation : " + (String)relation.get("individualPersonId"));
					if(Integer.parseInt((String)relation.get("relatedPersonId")) == i+1 
							&& Integer.parseInt((String)relation.get("individualPersonId")) == k+1){
						
						FamilyRelationshipHIPAACodeType familyRelationshipHIPAACodeType = AccountTransferUtil.hixTypeFactory.createFamilyRelationshipHIPAACodeType();
						familyRelationshipHIPAACodeType.setValue((java.lang.String)relation.get("relation"));
						personAssociationType.setFamilyRelationshipCode(familyRelationshipHIPAACodeType);
						
						/*DateType associationBeginDateType = AccountTransferUtil.niemCoreFactory.createDateType();
						com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date associationBeginDate = AccountTransferUtil.basicFactory.createDate();
						associationBeginDateType.setDate(associationBeginDate);
						personAssociationType.setAssociationBeginDate(associationBeginDateType);*/
						
						returnObj.getPerson().get(i).getPersonAugmentation().getPersonAssociation().add(personAssociationType);
					}
				}
			}
		  }
		}
		return returnObj;
	}
	

	/**
	 * creates the TransferHeader object
	 * @param caseNumber 
	 * 
	 * @return
	 */
	private TransferHeaderType createTransferHeader(String caseNumber) throws GIException {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ObjectFactory neimCore2fctry = new com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ObjectFactory();

		TransferHeaderType transferHeaderType = AccountTransferUtil.mainFactory.createTransferHeaderType();
		TransferActivityType transferActivityType = AccountTransferUtil.mainFactory.createTransferActivityType();
		IdentificationType identificationType = neimCore2fctry.createIdentificationType();
		identificationType.setIdentificationID(AccountTransferUtil.createString(caseNumber));
		transferActivityType.setActivityIdentification(identificationType);

		// time
		DateType activityDate = neimCore2fctry.createDateType();
		// activity Date
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime dateTime = AccountTransferUtil.basicFactory.createDateTime();
		dateTime.setValue(AccountTransferUtil.getSystemDateTime());
		transferActivityType.setActivityDate(activityDate);
		transferActivityType.getActivityDate().setDateTime(dateTime);
		
		// no of Referrals
		QuantityType noOfReferrals = neimCore2fctry.createQuantityType();

		lOGGER.debug("coverageSeekingCount :" + coverageSeekingCount);
		noOfReferrals.setValue(new BigDecimal(this.coverageSeekingCount));

		transferActivityType.setTransferActivityReferralQuantity(noOfReferrals);
		// state
		transferActivityType.setRecipientTransferActivityStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		// recipient
		TransferActivityCodeType transferActivityCodeType = AccountTransferUtil.mainFactory.createTransferActivityCodeType();
		transferActivityCodeType.setValue(TransferActivityCodeSimpleType.MEDICAID_CHIP); // because Exchange is sender
		transferActivityType.setRecipientTransferActivityCode(transferActivityCodeType);
		transferHeaderType.setTransferActivity(transferActivityType);

		return transferHeaderType;
	}
	
	private InformationExchangeSystemType createSender() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		InformationExchangeSystemType informationExchangeSystemType = AccountTransferUtil.hixCoreFactory.createInformationExchangeSystemType();
		InformationExchangeSystemCategoryCodeType informationExchangeSystemCategoryCodeType = AccountTransferUtil.hixTypeFactory.createInformationExchangeSystemCategoryCodeType();
		informationExchangeSystemCategoryCodeType.setValue(InformationExchangeSystemCategoryCodeSimpleType.EXCHANGE);
		informationExchangeSystemType.setInformationExchangeSystemCategoryCode(informationExchangeSystemCategoryCodeType);
		informationExchangeSystemType.setInformationExchangeSystemStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		return informationExchangeSystemType;
	}


	private int setCoverageSeekingCount(List<JSONObject> houseHolds) {
		coverageSeekingCount = 0;
		for (JSONObject houseHold : houseHolds) {
			if ((java.lang.Boolean) houseHold.get("applyingForCoverageIndicator")) {
				coverageSeekingCount = coverageSeekingCount + 1;
			}
		}
		return coverageSeekingCount;
	}
	
	private InformationExchangeSystemType createReceiver(){
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		InformationExchangeSystemType recieverInformationExchangeSystemType = AccountTransferUtil.hixCoreFactory.createInformationExchangeSystemType();
		InformationExchangeSystemCategoryCodeType informationExchangeSystemCategoryCodeType = AccountTransferUtil.hixTypeFactory.createInformationExchangeSystemCategoryCodeType();
		recieverInformationExchangeSystemType.setId("reciever1");
		informationExchangeSystemCategoryCodeType.setValue(InformationExchangeSystemCategoryCodeSimpleType.MEDICAID_AGENCY);
		recieverInformationExchangeSystemType.setInformationExchangeSystemCategoryCode(informationExchangeSystemCategoryCodeType);
		recieverInformationExchangeSystemType.setInformationExchangeSystemStateCode(AccountTransferUtil.createUSStateCodeType(stateCode));
		return recieverInformationExchangeSystemType;
	}
	
}
