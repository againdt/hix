package com.getinsured.hix.indportal.dto.dm.application;

public class AssociatedMemberIdentifier {
	 private String memberIdentifier;


	 // Getter Methods 

	 public String getMemberIdentifier() {
	  return memberIdentifier;
	 }

	 // Setter Methods 

	 public void setMemberIdentifier(String memberIdentifier) {
	  this.memberIdentifier = memberIdentifier;
	 }
	}