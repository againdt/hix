package com.getinsured.iex.hub.ifsv;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.ifsv.prefixmappers.IFSVNamespaceMapper;
import com.getinsured.iex.hub.platform.HubServiceBridge;

/** This class is a test class for IFSV services
* 
* HIX-30605
* 
* @author Nikhil Talreja
* @since 27-Feb-2014
*
*/
public final class VerifyIFS {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(VerifyIFS.class);
	
	private VerifyIFS(){
		
	}
	
	public static void main(String[] args) {
		
		try{
			HubServiceBridge ifsvBridge = HubServiceBridge.getHubServiceBridge("IFSVRequest");
			ifsvBridge.getServiceHandler().setJsonInput("{\"taxHousehold\":[{\"householdMember\":[{\"personId\":1,\"name\":{\"firstName\":\"carl\",\"middleName\":\"\",\"lastName\":\"Jose\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"212-305-9875\"},\"taxFilerCategoryCode\":\"PRIMARY\"}]}],\"requestID\":\"111\"}");
			Object payloadElement = ifsvBridge.getServiceHandler().getRequestpayLoad();
			
			if(payloadElement != null){
				JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.exchange._1");
				Marshaller m = jc.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new IFSVNamespaceMapper());
				//m.marshal(payloadElement, System.out);
			}
		}catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}

	}
	
}
