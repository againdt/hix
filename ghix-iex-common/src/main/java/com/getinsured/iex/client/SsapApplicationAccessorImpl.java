package com.getinsured.iex.client;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.iex.dto.SsapApplicationResource;

@Component
public class SsapApplicationAccessorImpl implements SsapApplicationAccessor {
	@Autowired private RestTemplate restTemplate;
	@Autowired private UserService userService;
	
	private static Logger LOGGER = LoggerFactory.getLogger(SsapApplicationAccessorImpl.class);
	
	@Override
	public List<SsapApplicationResource> getSsapApplicationsForCoverageYear(long cmrHouseholdId, long coverageYear) 
			throws JsonParseException, JsonMappingException, IOException {
		
		//String url = GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + "search/findByCmrHouseoldIdAndCoverageYear?cmrHouseoldId={cmrHouseoldId}&coverageYear={coverageYear}";
		String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/applications/coverageyearandcmrhousehold?cmrHouseoldId=" + cmrHouseholdId + "&coverageYear=" + coverageYear;
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
		List<SsapApplicationResource> applications = new ArrayList<SsapApplicationResource>();
		
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicationResource.class);
		
		JSONArray jsonArray = null;
		
		try {
			ResponseEntity<String> str = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			
			if(str == null || "".equals(str) || str.getBody() == null || "".equals(str.getBody())) {
				return applications;
			}
			
			jsonArray = new JSONArray(str.getBody().toString());
			for(int i=0; i<jsonArray.length(); i++) {
				org.json.JSONObject json_data = jsonArray.getJSONObject(i);
				if(json_data != null) {
					SsapApplicationResource ssapApplicationResource = reader.readValue(json_data.toString() );
					applications.add(ssapApplicationResource);
				}
			}
		}
		catch (Exception e) {
			LOGGER.error("Error in accessing the application for cmrHouseholdId:" + cmrHouseholdId + " for coverage year::" + coverageYear, e);
			return applications;
		}
		return applications;
	}
	
	@Override
	public List<SsapApplicationResource> getSsapApplicationsForCoverageYearNoAppData(long cmrHouseholdId, long coverageYear) throws JsonParseException, JsonMappingException, IOException {
		
		String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/applications/coverageyearandcmrhouseholdnoapp?cmrHouseoldId=" + cmrHouseholdId + "&coverageYear=" + coverageYear;
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
		List<SsapApplicationResource> applications = new ArrayList<SsapApplicationResource>();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(SsapApplicationResource.class);
		JSONArray jsonArray = null;
		
		try {
			ResponseEntity<String> str = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			
			if(str == null || "".equals(str) || str.getBody() == null || "".equals(str.getBody())) {
				return applications;
			}
			
			jsonArray = new JSONArray(str.getBody().toString());
			for(int i=0; i<jsonArray.length(); i++) {
				org.json.JSONObject json_data = jsonArray.getJSONObject(i);
				if(json_data != null) {
					SsapApplicationResource ssapApplicationResource = reader.readValue(json_data.toString() );
					applications.add(ssapApplicationResource);
				}
			}
		}
		catch (Exception e) {
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("Error in accessing the application for cmrHouseholdId:" + cmrHouseholdId + " for coverage year::" + coverageYear+" Call Response Message:"+e.getMessage());
			}
			return applications;
		}
		return applications;
	}
	@Override
	public List<SsapApplicationResource> getSsapApplications(long cmrHouseholdId) throws JsonParseException, JsonMappingException, IOException {
	
		String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/applications/cmrHousehold?cmrHouseoldId=" + cmrHouseholdId;
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
		List<SsapApplicationResource> applications = new ArrayList<SsapApplicationResource>();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class);
		JSONArray jsonArray = null;
		
		try {
			ResponseEntity<String> str = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			
			if(str == null || "".equals(str) || str.getBody() == null || "".equals(str.getBody())) {
				return applications;
			}
			
			jsonArray = new JSONArray(str.getBody().toString());
			for(int i=0; i<jsonArray.length(); i++) {
				org.json.JSONObject json_data = jsonArray.getJSONObject(i);
				if(json_data != null) {
					SsapApplicationResource ssapApplicationResource = reader.readValue(json_data.toString());
					applications.add(ssapApplicationResource);
				}
			}
		}
		catch (Exception e) {
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("Error in accessing the application for cmrHouseholdId:" + cmrHouseholdId +" Call Response Message:"+ e.getMessage());
			}
			return applications;
		}
		return applications;
	}
	
	@Override
	public SsapApplicationResource getSsapApplications(String caseNumber)  throws Exception {
		BigDecimal cmrHouseholdId = new BigDecimal(userService.getLoggedInUser().getActiveModuleId());
		
		String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/applications/casenumber?cmrHouseoldId=" + cmrHouseholdId + "&caseNumber=" + caseNumber;
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
		SsapApplicationResource application = new SsapApplicationResource();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class);
	
		try {
			ResponseEntity<String> str = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			
			if(str == null || "".equals(str) || str.getBody() == null || "".equals(str.getBody())) {
				return null;
			}
			application = reader.readValue(str.getBody().toString());
			
		}
		catch (Exception e) {
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("Exception i getting the application for case number:" + caseNumber+ " Call Response Message:"+e.getMessage());
			}
			return null;
		}
		return application;
	}
	
	@Override
	public SsapApplicationResource getSsapApplicationsByCaseNumberId(String caseNumber)  throws Exception {
		
		String url = GhixEndPoints.ELIGIBILITY_URL +  "/application/applications/casenumberid?caseNumber=" + caseNumber;
		HttpHeaders requestHeaders = new HttpHeaders();
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.ALL);
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<?> requestEntity = new HttpEntity<Object>(requestHeaders);
		SsapApplicationResource application = new SsapApplicationResource();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType( SsapApplicationResource.class );
	
		try {
			ResponseEntity<String> str = restTemplate.exchange(url, HttpMethod.GET, requestEntity, String.class);
			
			if(str == null || "".equals(str) || str.getBody() == null || "".equals(str.getBody())) {
				return null;
			}
			application = reader.readValue(str.getBody().toString());
			
		}
		catch (Exception e) {
			if(LOGGER.isWarnEnabled()){
				LOGGER.warn("Exception i getting the application for case number:" + caseNumber + " Call Response message:"+ e.getMessage());
			}
			return null;
		}
		return application;
	}
		
}
