package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 */
public class TaxHousehold {

  @Valid
  @Expose
  private List<HouseholdMember> householdMember = new ArrayList<>();
  @Expose
  private Double houseHoldIncome;
  @Expose
  private Boolean applyingForCashBenefitsIndicator = false;

  private List<HouseholdMember> deletedHouseholdMember = new ArrayList<>();

  /**
   * Holds primary tax filer person id for this tax household.
   */
  @Expose
  private int primaryTaxFilerPersonId = 0;

  /**
   * Boolean indicates if person reconciled their last years APTC.
   * Default value is true as per HIX-117784 requirements.
   */
  private boolean reconciledAptc = true;

  /**
   * Holds other {@link Address}'s that are used for personId=>addressId matrix.
   */
  @Expose
  private List<Address> otherAddresses = new ArrayList<>();

  /**
   * Boolean value indicates if primary tax household failed to reconcile last years APTC,
   * this is going to be set by the RRV when response comes back from the IRS.
   */
  private boolean failureToReconcile;

  /**
   * Stores flag if member is signed a consent to cooperate with Medicaid to collect
   * support from one of the absent parents.
   * <p>
   * Default value is {@code null} because UI needs to force user to pick Yes/No.
   */
  private Boolean medicaidConsent = null;

  @Expose
  private List<String> requiredVerifications = new ArrayList<>();


  public List<String> getRequiredVerifications() {
    return requiredVerifications;
  }

  public void setRequiredVerifications(List<String> requiredVerifications) {
    this.requiredVerifications = requiredVerifications;
  }

  public List<HouseholdMember> getDeletedHouseholdMember() {
    return deletedHouseholdMember;
  }

  public void setDeletedHouseholdMember(List<HouseholdMember> deletedHouseholdMember) {
    this.deletedHouseholdMember = deletedHouseholdMember;
  }

  public List<HouseholdMember> getHouseholdMember() {
    return householdMember;
  }

  public void setHouseholdMember(List<HouseholdMember> householdMember) {
    this.householdMember = householdMember;
  }

  public Double getHouseHoldIncome() {
    return houseHoldIncome;
  }

  public void setHouseHoldIncome(Double houseHoldIncome) {
    this.houseHoldIncome = houseHoldIncome;
  }

  public Boolean getApplyingForCashBenefitsIndicator() {
    return applyingForCashBenefitsIndicator;
  }

  public void setApplyingForCashBenefitsIndicator(Boolean applyingForCashBenefitsIndicator) {
    this.applyingForCashBenefitsIndicator = applyingForCashBenefitsIndicator;
  }

  /**
   * Returns primary tax filer person id for this tax household.
   *
   * @return primary tax filer person id for this tax household.
   */
  public int getPrimaryTaxFilerPersonId() {
    return primaryTaxFilerPersonId;
  }

  /**
   * Sets primary tax filer person id for this tax household.
   *
   * @param primaryTaxFilerPersonId primary tax filer person id for this tax household.
   */
  public void setPrimaryTaxFilerPersonId(int primaryTaxFilerPersonId) {
    this.primaryTaxFilerPersonId = primaryTaxFilerPersonId;
  }

  /**
   * Returns boolean that indicates if person reconciled their last years APTC.
   *
   * @return if person reconciled their APTC.
   */
  public boolean isReconciledAptc() {
    return reconciledAptc;
  }

  /**
   * Sets boolean that indicates if person reconciled their last years APTC.
   *
   * @param reconciledAptc boolean to set.
   */
  public void setReconciledAptc(boolean reconciledAptc) {
    this.reconciledAptc = reconciledAptc;
  }

  /**
   * Returns list of other {@link Address}'s used for personId=>addressId matrix.
   *
   * @return list of addresses
   */
  public List<Address> getOtherAddresses() {
    return otherAddresses;
  }

  /**
   * Sets list of other {@link Address}'s used for personId=>addressId matrix.
   *
   * @param otherAddresses list of other addresses.
   */
  public void setOtherAddresses(List<Address> otherAddresses) {
    this.otherAddresses = otherAddresses;
  }

  /**
   * Returns true if primary tax household failed to reconcile their last years APTC.
   *
   * @return true if failed, false if they did reconcile.
   */
  public boolean isFailureToReconcile() {
    return failureToReconcile;
  }

  /**
   * Sets boolean flag indicating if primary tax household failed to reconcile last years APTC.
   *
   * @param failureToReconcile boolean value to set.
   */
  public void setFailureToReconcile(boolean failureToReconcile) {
    this.failureToReconcile = failureToReconcile;
  }

  /**
   * Returns {@link Boolean} flag if member is signed a consent to cooperate with Medicaid to collect
   * support from one of the absent parents.
   * <p>
   * Default value is {@code null} because UI needs to force user to pick Yes/No.
   *
   * @return {@link Boolean#TRUE} if member consent to cooperation, {@link Boolean#FALSE} if did not,
   * {@code null} if user have not opted to anything yet.
   */
  public Boolean getMedicaidConsent() {
    return medicaidConsent;
  }

  /**
   * Sets {@link Boolean} flag to indicate if this member is consent to cooperation with Medicaid
   *
   * @param medicaidConsent {@link Boolean} flag to set.
   */
  public void setMedicaidConsent(Boolean medicaidConsent) {
    this.medicaidConsent = medicaidConsent;
  }
}
