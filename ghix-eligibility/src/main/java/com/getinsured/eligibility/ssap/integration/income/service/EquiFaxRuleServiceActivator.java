package com.getinsured.eligibility.ssap.integration.income.service;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.messaging.Message;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value = "equiFaxRuleServiceActivator")
public class EquiFaxRuleServiceActivator {
    private static final Logger LOGGER = Logger.getLogger(EquiFaxRuleServiceActivator.class);
    
    public Message<Map<String, String>> receive(Message<String> message) {
        String equiFaxResponse = message.getPayload();
        LOGGER.debug(">>>>>>>>>>>>>>>>>> Equifax Response: " + equiFaxResponse);
        String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
        Map<String, String> equiFaxRulePayload = new HashMap<String, String>();
        equiFaxRulePayload.put("ssapJson", ssapJSON);
        if(null != equiFaxResponse && equiFaxResponse.contains("ResponseMetadata")) {
            equiFaxRulePayload.put("hubResponse", equiFaxResponse);
        } else {
            equiFaxRulePayload.put("hubResponse", null);
        }
        Message<Map<String, String>> ruleMessage = MessageBuilder.withPayload(equiFaxRulePayload).setHeader("Content-Type", "application/x-www-form-urlencoded").build();
        return ruleMessage;
    }
    

}
