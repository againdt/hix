package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.IssuerServiceArea;

@Repository("iIssuerServiceAreaReposiroty")
public interface IIssuerServiceAreaReposiroty extends JpaRepository<IssuerServiceArea, Integer> {
		
	@Query("SELECT id FROM IssuerServiceArea WHERE serviceAreaName = :servicerAreaName and applicableYear = :applicableYear")
	Integer getServieAreaIdByName(@Param("servicerAreaName") String servicerAreaName, @Param("applicableYear") Integer applicableYear);
	
	@Query("SELECT serffServiceAreaId FROM IssuerServiceArea WHERE id = :id")
	String getSerfServieAreaIdById(@Param("id") Integer id);
					  
}

