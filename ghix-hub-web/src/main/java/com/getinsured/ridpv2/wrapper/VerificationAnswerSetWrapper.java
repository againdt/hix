package com.getinsured.ridpv2.wrapper;

import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.ridpv2.cms.dsh.ridp.extension._1.VerificationAnswerSetType;

public class VerificationAnswerSetWrapper {
	private VerificationAnswerSetType answerSetType;
	private ObjectFactory factory = new ObjectFactory();
	
	public VerificationAnswerSetWrapper(){
		answerSetType = factory.createVerificationAnswerSetType();
	}
	
	public void addVerificationAnswer(VerificationAnswerWrapper verificationAnswer){
		this.answerSetType.getVerificationAnswers().add(verificationAnswer.getSystemRepresentation());
	}
	
	public VerificationAnswerSetType getSystemRepresentation(){
		return this.answerSetType;
	}
}
