package com.getinsured.hix.batch.enrollment.service;

import static com.getinsured.hix.enrollment.util.EnrollmentUtils.isNotNullAndEmpty;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.enrollment.util.EnrlReconSnapshotGenJobThread;
import com.getinsured.hix.dto.enrollment.ReconAdditionalSubInfoDTO;
import com.getinsured.hix.dto.enrollment.ReconEnrollmentDTO;
import com.getinsured.hix.dto.enrollment.ReconMemberDTO;
import com.getinsured.hix.dto.enrollment.ReconMonthlyPremiumDTO;
import com.getinsured.hix.enrollment.repository.IEnrlReconDataRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconErrorRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSnapshotRepository;
import com.getinsured.hix.enrollment.repository.IEnrlReconSummaryRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeAudRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRelationshipRepository;
import com.getinsured.hix.enrollment.repository.IEnrolleeRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentPremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrollmentRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.enrollment.EnrlReconData;
import com.getinsured.hix.model.enrollment.EnrlReconError;
import com.getinsured.hix.model.enrollment.EnrlReconSnapshot;
import com.getinsured.hix.model.enrollment.EnrlReconSummary;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeRelationship;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentPremium;
import com.getinsured.hix.model.enrollment.ReconDetailCsvDto;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("enrlReconSnapshotGenerationService")
public class EnrlReconSnapshotGenerationServiceImpl implements EnrlReconSnapshotGenerationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpServiceImpl.class);
	
	@Autowired
	private IEnrlReconDataRepository enrlReconDataRepository;
	
	@Autowired
	private IEnrollmentRepository enrollmentRepository;
	
	@Autowired
	private IEnrolleeRepository enrolleeRepository;
	
	@Autowired
	private IEnrolleeAudRepository enrolleeAudRespository;
	
	@Autowired
	private IEnrolleeRelationshipRepository enrolleeRelationshipRepository;
	
	@Autowired
	private IEnrlReconSummaryRepository enrlReconSummaryRepository;
	
	@Autowired
	private IEnrlReconSnapshotRepository enrlReconSnapshotRepository;
	
	@Autowired
	private IEnrollmentPremiumRepository enrollmentPremiumRepository;
	
	@Autowired
	EnrollmentReconciliationServiceImpl enrollmentReconciliationServiceImpl;
	@Autowired
	private JobService jobService;
	@Autowired
	private IEnrlReconErrorRepository enrlReconErrorRepository;
	
	public static final String ENROLLMENT_STATUS_CANCEL = "CANCEL";
	
	@Override
	public void serveEnrlReconSnapshotGenerationJob(Integer fileId, long jobExecutionId) throws Exception{
		ExecutorService executor = null;
		List<Long> failedPolicyIds= new ArrayList<>();
		
		if(fileId!=null){
			EnrlReconSummary summary= enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
			List<Long> enrollmentIds= enrlReconDataRepository.getEnrollmentIdsByFileId(fileId);
			List<Integer> missingInFile = enrlReconDataRepository.getEnrollmentsMissingInFile(fileId, getIssuerCutOffDate(summary), summary.getHiosIssuerId(), String.valueOf(summary.getCoverageYear()));
			if(null != missingInFile && !missingInFile.isEmpty()) {
				missingInFile.stream().filter(p -> null != p).forEach(e -> enrollmentIds.add(e.longValue()));
			}
			if(enrollmentIds!=null && enrollmentIds.size()>0 && summary!=null){
				
				
				String threadPoolSize =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SNAPSHOTGEN_THREAD_POOLSIZE);
				if(threadPoolSize!=null){
					executor=Executors.newFixedThreadPool(Integer.parseInt(threadPoolSize.trim()));
				}else{
					executor=Executors.newFixedThreadPool(EnrollmentConstants.TEN);
				}
				
				Integer threadSize=EnrollmentConstants.HUNDRED;
				if(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SNAPSHOTGEN_THREAD_SIZE)!=null){
					threadSize=Integer.parseInt(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SNAPSHOTGEN_THREAD_SIZE).trim());
				}
				List<List<Long>> splittedPolicyIds=splitList(enrollmentIds, threadSize);
				
				for(List<Long> subList: splittedPolicyIds){
					
					executor.submit(new EnrlReconSnapshotGenJobThread(subList, this, fileId, failedPolicyIds, summary, jobExecutionId));
				}
				
				executor.shutdown();
				
				try {
					Integer threadTimeOut =null;
	    			String strInboundThreadTimeOut =DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_SNAPSHOTGEN_THREAD_TIMEOUT);
	    			if(strInboundThreadTimeOut != null){
	    			 threadTimeOut = Integer.parseInt(strInboundThreadTimeOut.trim());
	    			}
	    			if(threadTimeOut!=null){
	    				executor.awaitTermination(threadTimeOut, TimeUnit.HOURS);
	    			}else{
	    				executor.awaitTermination(EnrollmentConstants.DEFAULT_THREAD_WAIT, TimeUnit.HOURS);

	    			}
				} catch (Exception e) {
					LOGGER.error("Error Occurred in serveEnrlReconSnapshotGenerationJob(): ", e);
					throw new Exception("Error while serving the reconciliationsnapshot generation job "+e.getMessage());
				}
				
			}
		}
	}
	
	private long getEnrollmentCountInHix(EnrlReconSummary summary)throws GIException{
		long count=0;
		 if(summary!=null && summary.getHiosIssuerId()!=null && summary.getCoverageYear()!=null){
				//Deduct cutoff Date from latest FileDate
			count= enrollmentRepository.enrollmentCountInHIX(summary.getHiosIssuerId(),summary.getCoverageYear()+"", getIssuerCutOffDate(summary));
		 }
		return count;
		
	}
			 
	private Date getIssuerCutOffDate(EnrlReconSummary summary)throws GIException{
			Date issuerCutoffDate=summary.getIssuerExtractDate();
				if(issuerCutoffDate ==null){
					issuerCutoffDate=EnrollmentUtils.getMonthStartDate(new Date());
				}
				 String cutOffDays=DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.RECON_MISSING_ENROLLMENT_LOOKBACK_DAYS);
				 if(EnrollmentUtils.isNotNullAndEmpty(cutOffDays) && StringUtils.isNumeric(cutOffDays.trim())){
					 issuerCutoffDate=EnrollmentUtils.removeTimeFromDate(EnrollmentUtils.getBeforeDayDate(issuerCutoffDate, Integer.parseInt(cutOffDays.trim())-1));
				 }
		return issuerCutoffDate;
	}
	
	@Override
	public void populateEnrlReconSnapshot(List<Long> enrollmentIds, Integer fileId, List<Long> failedPolicyIds, EnrlReconSummary summary, long jobExecutionId){
		if(enrollmentIds!=null && enrollmentIds.size()>0){
			//code to populate snapshot data
			for(Long enrollmentId: enrollmentIds){
				List<EnrlReconData> enrlReconDataList=null;
				try{
					
					String batchJobStatus=null;
	    			
	    			if(jobService != null && jobExecutionId != -1){
	    				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
	    			}
	    			
	    			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
	    										batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
	    				break;
	    			}
					EnrlReconSnapshot snapshot= new EnrlReconSnapshot();
					snapshot.setFileId(fileId);
//					enrlReconDataList=enrlReconDataRepository.getReconDataByIssuerPolicyId(issuerPolicyId, fileId);
					enrlReconDataList=enrlReconDataRepository.getReconDataByEnrollmentId(enrollmentId, fileId);
					prepareIssuerdata(enrollmentId, snapshot, summary, enrlReconDataList);
					prepareGhixData(enrollmentId, snapshot, summary.getHiosIssuerId(), getIssuerCutOffDate(summary));
					enrlReconSnapshotRepository.saveAndFlush(snapshot);
					enrlReconDataRepository.updateReconDataToSuccess(enrollmentId, fileId);
				}catch(Exception e){
					//LOGGER.error("Failed for enrollment Id "+ enrollmentId+" File ID"+ fileId +" :: "+e.getMessage());
					//e.printStackTrace();
					failedPolicyIds.add(enrollmentId);
					String errorMsg=EnrollmentUtils.shortStackTrace(e, EnrollmentConstants.TWO_NINETY_NINE);
					enrlReconDataRepository.updateReconDataToFailure(enrollmentId, errorMsg, fileId);
					if(enrlReconDataList!=null && enrlReconDataList.size()>0){
						List<EnrlReconError> errors= new ArrayList<>();
						for(EnrlReconData data: enrlReconDataList){
							EnrlReconError error=new EnrlReconError();
							error.setEnrollmentId(data.getEnrollmentId());
							error.setSubscriberId(data.getExchgAssignedSubId());
							error.setFileId(data.getFileId());
							error.setInData(data.getInData());
							error.setErrorMessage(errorMsg);
							error.setCreationTimestamp(new Date());
							errors.add(error);
						}
						if(errors.size()>0){
							enrlReconErrorRepository.save(errors);
						}
					}
				}
				
				
			}
			
		}
		
	}
	
	private List<EnrlReconData> prepareIssuerdata(Long enrollmentId, EnrlReconSnapshot snapshot, EnrlReconSummary summary, List<EnrlReconData> reconDataRecords) throws Exception{
		ReconEnrollmentDTO issuerData=null;
		//List<EnrlReconData> reconDataRecords=null;
		if(enrollmentId!=null && snapshot!=null){
			//reconDataRecords=enrlReconDataRepository.getReconDataByIssuerPolicyId(issuerPolicyId, snapshot.getFileId());
			if(reconDataRecords!=null && reconDataRecords.size()>0){
				issuerData= new ReconEnrollmentDTO();
				//EnrlReconSummary summary= enrlReconSummaryRepository.getReconSummaryByFileId(reconDataRecords.get(0).getFileId());
				List<ReconDetailCsvDto> csvDTOs= new ArrayList<>();
				List<ReconDetailCsvDto> subscCsvDTOs = new ArrayList<>();;
				prepareMemberData(reconDataRecords, issuerData, csvDTOs, subscCsvDTOs);
				prepareEnrollmentData(subscCsvDTOs, issuerData, summary, snapshot);
				prepareMonthlyPremiumData(issuerData, subscCsvDTOs, csvDTOs);
				if(EnrollmentUtils.isNotNullAndEmpty(subscCsvDTOs.get(0).getBenefitStartDate())) {
					snapshot.setBenefitEffectiveDate(DateUtil.StringToDate(subscCsvDTOs.get(0).getBenefitStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
				}
				snapshot.setSubscriberName(getfullName(subscCsvDTOs.get(0).getQiFirstName(),
						subscCsvDTOs.get(0).getQiMiddleName(), subscCsvDTOs.get(0).getQiLastName()));
				
				//snapshot.setIssuerData(platformGson.toJson(issuerData));
				snapshot.setIssuerData(JacksonUtils.getJacksonObjectWriterForJavaType(ReconEnrollmentDTO.class).writeValueAsString(issuerData));
			}
		}
		return reconDataRecords;
	}
	
	
	private void prepareEnrollmentData(List<ReconDetailCsvDto> csvDTOs,ReconEnrollmentDTO issuerData, EnrlReconSummary summary, EnrlReconSnapshot snapshot){
		if(csvDTOs!=null && issuerData!=null && summary!=null && snapshot!=null){
			if(isNotNullAndEmpty(csvDTOs.get(0).getAgentBrokerName())){
				issuerData.setAgentBrokerName(csvDTOs.get(0).getAgentBrokerName().trim());
			}
			issuerData.setAgentBrokerNpn(csvDTOs.get(0).getAgentBrokerNpn());
			//issuerData.setBenefitStartDate(csvDTOs.get(0).getBenefitStartDate());
			//issuerData.setBenefitEndDate(csvDTOs.get(0).getBenefitEndDate());
			issuerData.setCoverageYear(csvDTOs.get(0).getCoverageYear());
			snapshot.setCoverageYear(csvDTOs.get(0).getCoverageYear());
			issuerData.setEoyTerminationIndicator(csvDTOs.get(0).getEndOfYearTerminationIndicator());
			issuerData.setHiosId(csvDTOs.get(0).getHiosId() != null ? csvDTOs.get(0).getHiosId() : summary.getHiosIssuerId());
			issuerData.setInitialPremiumPaidStatus(csvDTOs.get(0).getInitialPremiumPaidStatus());
			if(EnrollmentUtils.isNotNullAndEmpty(issuerData.getInitialPremiumPaidStatus()) ){
				if( issuerData.getInitialPremiumPaidStatus().equalsIgnoreCase("C")){
					issuerData.setEnrollmentStatus(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL);
				}else if(issuerData.getInitialPremiumPaidStatus().equalsIgnoreCase("Y")){
					issuerData.setEnrollmentStatus(EnrollmentConstants.ENROLLMENT_STATUS_CONFIRM);
				}else if(issuerData.getInitialPremiumPaidStatus().equalsIgnoreCase("N")){
					issuerData.setEnrollmentStatus(EnrollmentConstants.ENROLLMENT_STATUS_PENDING);
				}
			}
			
			issuerData.setIssuerAssignedRecordTraceNumber(csvDTOs.get(0).getIssuerAssignedRecordTraceNumber());
			issuerData.setIssuerExtractDate(csvDTOs.get(0).getIssuerExtractDate());
			issuerData.setIssuerExtractTime(csvDTOs.get(0).getIssuerExtractTime());
			issuerData.setPaidThroughDate(csvDTOs.get(0).getPaidThroughDate());
			issuerData.setIssuerAssignedPolicyId(csvDTOs.get(0).getIssuerAssignedPolicyId());
			issuerData.setExchangeAssignedPolicyid(csvDTOs.get(0).getExchangeAssignedPolicyNumber());
			snapshot.setIssuerAssignedPolicyId(csvDTOs.get(0).getIssuerAssignedPolicyId());
			if(csvDTOs.get(0).getExchangeAssignedPolicyNumber()!=null){
				snapshot.setHixEnrollmentId(Long.valueOf(csvDTOs.get(0).getExchangeAssignedPolicyNumber().trim()));
			}
			if(csvDTOs.get(0).getExchangeAssignedSubscriberId()!=null){
				snapshot.setHixSubscriberId(Long.valueOf(csvDTOs.get(0).getExchangeAssignedSubscriberId()));
			}
			
			if(csvDTOs.get(0).getQhpIdentifier()!=null){
				issuerData.setQhpId(csvDTOs.get(0).getQhpIdentifier());
				issuerData.setPartialQhpPlanId(csvDTOs.get(0).getQhpIdentifier().substring(0, 10));
			}
			
			
			issuerData.setRecordCode(csvDTOs.get(0).getRecordCode());
			issuerData.setSpoeId(csvDTOs.get(0).getSpoeId());
			issuerData.setTenantId(csvDTOs.get(0).getTenantId());
			issuerData.setTradingPartnerId(csvDTOs.get(0).getTradingPartnerId());
		}
	}
	
	private void prepareMonthlyPremiumData(ReconEnrollmentDTO issuerData, List<ReconDetailCsvDto> subscCsvDTOs,  List<ReconDetailCsvDto> uniqueEnrolleeDTOs) throws Exception{
		if(issuerData!=null && issuerData.getAdditionalSubInfo()!=null && subscCsvDTOs.size()>0 ){
			List<ReconMonthlyPremiumDTO> premiums=prepareEmptyMonthlyPremiums(subscCsvDTOs.get(0).getCoverageStartDate(), subscCsvDTOs.get(0).getCoverageEndDate(), uniqueEnrolleeDTOs);
			if(premiums!=null && premiums.size()>0){
				Collections.sort(premiums);
				issuerData.setMonthlyPremiums(premiums);
				for(ReconMonthlyPremiumDTO prem:  premiums){
					Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(prem.getCoverageMonth()-1,prem.getCoverageYear())); //Month Start Date
					Date monthEndDate = EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(prem.getCoverageMonth()-1,prem.getCoverageYear())); //Month End Date
					populateTotalPrmAmount(prem, subscCsvDTOs, monthStartDate, monthEndDate);
					populateAPTCPrmAmount(prem, subscCsvDTOs, monthStartDate, monthEndDate);
					populateCSRAmount(prem, subscCsvDTOs, monthStartDate, monthEndDate);
				}
			}
			
			
		}
		
	}
	
	private void populateCSRAmount(ReconMonthlyPremiumDTO prem, List<ReconDetailCsvDto> subscCsvDTOs, Date  monthStartDate, Date monthEndDate)throws Exception{
		//Date lastPremEffDate=null;
		Float totalPremium=null;
		for (ReconDetailCsvDto subscDTO : subscCsvDTOs){
			if (subscDTO.getCsrAmount() != null && subscDTO.getCsrEffectiveDate() != null
					&& subscDTO.getCsrEndDate() != null 
					&& !subscDTO.getCsrEffectiveDate().trim().isEmpty()
					&& !subscDTO.getCsrEndDate().trim().isEmpty())
			{
				
				Date premEffDate= DateUtil.StringToDate(subscDTO.getCsrEffectiveDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				Date premEffEndDate= DateUtil.StringToDate(subscDTO.getCsrEndDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				if(premEffDate.before(monthEndDate) &&  premEffEndDate.after(monthStartDate) ){
					Float premiumAmount=Float.valueOf(subscDTO.getCsrAmount().trim());
					if(totalPremium!=null){
						totalPremium=totalPremium+premiumAmount;
					}else{
						totalPremium=premiumAmount;
					}
					/*if(lastPremEffDate==null || premEffDate.after(lastPremEffDate)) {
						prem.setCsrAmt(premiumAmount);
						lastPremEffDate=premEffDate;
					}*/
				}
			}
		}
		if(totalPremium!=null){
			prem.setCsrAmt(totalPremium);
		}
	}
	
	
	
	
	private void populateAPTCPrmAmount(ReconMonthlyPremiumDTO prem, List<ReconDetailCsvDto> subscCsvDTOs, Date  monthStartDate, Date monthEndDate)throws Exception{
		//Date lastPremEffDate=null;
		Float totalPremium=null;
		for (ReconDetailCsvDto subscDTO : subscCsvDTOs){
			if (subscDTO.getAppliedAptcAmount() != null && subscDTO.getAppliedAptcEffectiveDate() != null
					&& subscDTO.getAppliedAptcEndDate() != null
					&& !subscDTO.getAppliedAptcEffectiveDate().trim().isEmpty()
					&& !subscDTO.getAppliedAptcEndDate().trim().isEmpty())
			{
				Date premEffDate= DateUtil.StringToDate(subscDTO.getAppliedAptcEffectiveDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				Date premEffEndDate= DateUtil.StringToDate(subscDTO.getAppliedAptcEndDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				if(premEffDate.before(monthEndDate) &&  premEffEndDate.after(monthStartDate) ){
					Float premiumAmount=Float.valueOf(subscDTO.getAppliedAptcAmount().trim());
					if(totalPremium!=null){
						totalPremium=totalPremium+premiumAmount;
					}else{
						totalPremium=premiumAmount;
					}
						/*if(lastPremEffDate==null || premEffDate.after(lastPremEffDate)){
							prem.setAptcAmt(premiumAmount);
							lastPremEffDate=premEffDate;
						}*/
				}
			}
		}
		if(totalPremium!=null && totalPremium.compareTo(0f) != 0){
			prem.setAptcAmt(totalPremium);
		}else{
			prem.setAptcAmt(0f);
		}
	}
	
	private void populateTotalPrmAmount(ReconMonthlyPremiumDTO prem, List<ReconDetailCsvDto> subscCsvDTOs, Date  monthStartDate, Date monthEndDate)throws Exception{
		Date lastPremEffDate=null;
		Float totalPremium=null;
		for (ReconDetailCsvDto subscDTO : subscCsvDTOs){
			if (subscDTO.getTotalPremiumAmount() != null && subscDTO.getTotalPremiumEffectiveDate() != null
					&& subscDTO.getTotalPremiumEndDate() != null
					&& !subscDTO.getTotalPremiumEffectiveDate().trim().isEmpty()
					&& !subscDTO.getTotalPremiumEndDate().trim().isEmpty())
			{
				
				Date premEffDate= DateUtil.StringToDate(subscDTO.getTotalPremiumEffectiveDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				Date premEffEndDate= DateUtil.StringToDate(subscDTO.getTotalPremiumEndDate().trim(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD);
				if(premEffDate.before(monthEndDate) &&  premEffEndDate.after(monthStartDate) ){
					Float premiumAmount=Float.valueOf(subscDTO.getTotalPremiumAmount());
					if(lastPremEffDate==null || premEffDate.after(lastPremEffDate)){
						prem.setRatingArea(subscDTO.getRatingArea());
						prem.setIssuerpremium(premiumAmount);
						lastPremEffDate=premEffDate;
						
					}
						
					
					/*
					 * Commented out becaus eproration logic has been discarded as part of HIX-100290
					 * Float proratedAmount=calculateProratedAmount(premEffDate, premEffEndDate, monthStartDate, monthEndDate, premiumAmount);
					if(totalPremium!=null){
						totalPremium=totalPremium+proratedAmount;
					}else{
						totalPremium=proratedAmount;
					}*/
					if(totalPremium!=null){
						totalPremium=totalPremium+premiumAmount;
					}else{
						totalPremium=premiumAmount;
					}
					
				}
			}
			
			
		}
		
		if(totalPremium!=null){
			prem.setGrossPremium(totalPremium);
		}
		
	}
	
	/*private Float calculateProratedAmount(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate, Float premAmount ) throws GIException {
		Float proratedAmount=0.f;
		if((null != premAmount && 0 != premAmount.compareTo(0f)) ){

			int daysActive=calculateActiveDays(coverageStartDate, coverageEndDate,monthStartDate, monthEndDate);				
			proratedAmount = premAmount / EnrollmentUtils.daysInMonth(monthStartDate) * daysActive;	
		}
		return proratedAmount;
	}*/
	
	private void prepareMemberData(List<EnrlReconData> reconDataRecords, ReconEnrollmentDTO issuerData, List<ReconDetailCsvDto> csvDTOs, List<ReconDetailCsvDto> subscCsvDTOs ) throws Exception {
		List<String> memberIds= new ArrayList<>();
		List<ReconMemberDTO> members= new ArrayList<ReconMemberDTO>();
		for (EnrlReconData reconData: reconDataRecords){
			ReconDetailCsvDto inDataDto= enrollmentReconciliationServiceImpl.convertStringToreconDetailCsvDto(reconData.getInData());
			
			inDataDto.setCoverageStartDate(reconData.getBenefitStartDate());
			inDataDto.setCoverageEndDate(reconData.getBenefitEndDate());
			csvDTOs.add(inDataDto);
			if(inDataDto.getSubscriberIndicator()!=null && EnrollmentConstants.SUBSCRIBER_FLAG_YES.equalsIgnoreCase(inDataDto.getSubscriberIndicator().trim())){
				subscCsvDTOs.add(inDataDto);
			}
			if( !(inDataDto.getSubscriberIndicator()!=null && EnrollmentConstants.SUBSCRIBER_FLAG_YES.equalsIgnoreCase(inDataDto.getSubscriberIndicator().trim())&& memberIds.contains(inDataDto.getExchangeAssignedMemberId()))){
				
				ReconMemberDTO member= new ReconMemberDTO();
				memberIds.add(inDataDto.getExchangeAssignedMemberId());
				member.setExchangeAssignedMemberId(inDataDto.getExchangeAssignedMemberId());
				member.setExchangeAssignedSubscriberId(inDataDto.getExchangeAssignedSubscriberId());
				if(reconData.getBenefitStartDate()!=null){
					member.setMemberStartDate(EnrollmentUtils.truncateTime(reconData.getBenefitStartDate()));
				}
				
				member.setMemberBeginDate(inDataDto.getBenefitStartDate());
				member.setMemberEndDate(inDataDto.getBenefitEndDate());
				
				member.setFirstName(inDataDto.getQiFirstName());
				member.setMiddleName(inDataDto.getQiMiddleName());
				member.setLastName(inDataDto.getQiLastName());
				member.setBirthDate(inDataDto.getQiBirthDate());
				member.setHomeAddress1(inDataDto.getResidentialAddressLine1());
				member.setHomeAddress2(inDataDto.getResidentialAddressLine2());
				member.setHomeCity(inDataDto.getResidentialCityName());
				member.setHomeCountyCode(inDataDto.getResidentialCountyCode());
				member.setHomeState(inDataDto.getResidentialStateCode());
				member.setHomeZip(inDataDto.getResidentialZipCode());
				if(inDataDto.getIndividualPremiumAmount()!=null){
					member.setIndividualPremium(Float.valueOf(inDataDto.getIndividualPremiumAmount().trim()));
				}
				
				
				//relationToSubscriber
				
				member.setIndividualRelationshipCode(inDataDto.getIndividualRelationshipCode());
				member.setMailingAddress1(inDataDto.getMailingAddressLine1());
				member.setMailingAddress2(inDataDto.getMailingAddressLine2());
				member.setMailingCity(inDataDto.getMailingAddressCity());
				member.setMailingState(inDataDto.getMailingAddressStateCode());
				member.setMailingZip(inDataDto.getMailingAddressZipCode());
				
				
				member.setIssuerAssignedMemberId(inDataDto.getIssuerAssignedMemberId());
				member.setIssuerAssignedSubscriberId(inDataDto.getIssuerAssignedSubscriberId());
				
				
				member.setSsn(inDataDto.getQiSocialSecurityNumber());
				member.setSubscriberIndicator(inDataDto.getSubscriberIndicator());
				
				member.setTelephoneNumber(inDataDto.getTelephoneNumber());
				member.setTobacco(inDataDto.getTobaccoUseCode());
				member.setGender(inDataDto.getQiGender());
				
				members.add(member);
			}
			
			
		}
		if(members!=null && members.size()>0){
			Collections.sort(members);
			issuerData.setMembers(members);
		}
		
		if(subscCsvDTOs.size()>0){
			prepareAdditionalSubscriberInfo(issuerData, subscCsvDTOs);
		}else{
			throw new Exception(EnrollmentConstants.NO_SUBSCRIBER_FROM_ISSUER);
		}
	}

	private void prepareAdditionalSubscriberInfo( ReconEnrollmentDTO issuerData, List<ReconDetailCsvDto> subscRecords){
		List<ReconAdditionalSubInfoDTO> addSubscInfos= new ArrayList<>();
		int rowId=1;
		if(issuerData!=null && subscRecords!=null && subscRecords.size()>0){
			for(ReconDetailCsvDto reconSubsc :subscRecords){
				ReconAdditionalSubInfoDTO subscDTO= new ReconAdditionalSubInfoDTO();
				subscDTO.setRow(rowId);
				if(NumberUtils.isNumber(reconSubsc.getAppliedAptcAmount())){
					subscDTO.setAptcAmt(Float.valueOf(reconSubsc.getAppliedAptcAmount().trim()));
				}
				subscDTO.setAptcEffectiveDate(reconSubsc.getAppliedAptcEffectiveDate());
				subscDTO.setAptcEndDate(reconSubsc.getAppliedAptcEndDate());
				subscDTO.setBenefitEndDate(reconSubsc.getBenefitEndDate());
				subscDTO.setBenefitStartDate(reconSubsc.getBenefitStartDate());
				if(NumberUtils.isNumber(reconSubsc.getCsrAmount())){
					subscDTO.setCsrAmt(Float.valueOf(reconSubsc.getCsrAmount().trim()));
				}
				subscDTO.setCsrEffectiveDate(reconSubsc.getCsrEffectiveDate());
				subscDTO.setCsrEndDate(reconSubsc.getCsrEndDate());
				subscDTO.setExchangeAssignedMemberId(reconSubsc.getExchangeAssignedMemberId());
				subscDTO.setExchangeAssignedSubscriberId(reconSubsc.getExchangeAssignedSubscriberId());
				if(NumberUtils.isNumber(reconSubsc.getTotalPremiumAmount())){
					subscDTO.setGrossPremium(Float.valueOf(reconSubsc.getTotalPremiumAmount().trim()));
				}
				subscDTO.setGrossPremiumEffectiveDate(reconSubsc.getTotalPremiumEffectiveDate());
				subscDTO.setGrossPremiumEndDate(reconSubsc.getTotalPremiumEndDate());
				subscDTO.setIssuerAssignedMemberId(reconSubsc.getIssuerAssignedMemberId());
				subscDTO.setIssuerAssignedPolicyId(reconSubsc.getIssuerAssignedPolicyId());
				rowId++;
				addSubscInfos.add(subscDTO);
			}
			
		}
		issuerData.setAdditionalSubInfo(addSubscInfos);
	}
	
	private void prepareGhixData(Long enrollmentId, EnrlReconSnapshot snapshot, String hiosIssuerId, Date issuerCutOffDate) throws Exception{
		ReconEnrollmentDTO ghixData=null;
		if(enrollmentId!=null){
//			Enrollment enr=enrollmentRepository.findById(enrollmentId);
			Enrollment enr=enrollmentRepository.getEnrollmentByIdAndHiosIssuerId(enrollmentId.intValue(), hiosIssuerId, issuerCutOffDate);
			if(enr!=null){
				enr.setEnrollees(enrolleeRepository.findEnrolleesByEnrollmentID(enrollmentId.intValue()));
				ghixData= new ReconEnrollmentDTO();
				fillEnrollmentInfo(enr, ghixData, hiosIssuerId);
				fillMemberInfo(enr, ghixData);
				fillPremiumInfo(enr, ghixData);
				if(null != enr.getBenefitEffectiveDate()) {
					snapshot.setBenefitEffectiveDate(enr.getBenefitEffectiveDate());
				}
				Enrollee subscriber = enr.getSubscriberForEnrollment();
				if(null != subscriber && EnrollmentUtils.isNotNullAndEmpty(getfullName(subscriber.getFirstName(), subscriber.getMiddleName(), subscriber.getLastName()))) {
					snapshot.setSubscriberName(getfullName(subscriber.getFirstName(), subscriber.getMiddleName(), subscriber.getLastName()));
				}
				if(EnrollmentUtils.isNullOrEmpty(snapshot.getIssuerData())) {
					snapshot.setHixEnrollmentId(enrollmentId);
					snapshot.setHixSubscriberId(Long.valueOf(enr.getExchgSubscriberIdentifier()));
					snapshot.setIssuerAssignedPolicyId(enr.getIssuerAssignPolicyNo());
					snapshot.setCoverageYear(DateUtil.dateToString(enr.getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYY));
				}
				//snapshot.setHixData(platformGson.toJson(ghixData));
				snapshot.setHixData(JacksonUtils.getJacksonObjectWriterForJavaType(ReconEnrollmentDTO.class).writeValueAsString(ghixData));
			}
		}
		//return ghixData;
	}
	
	private void fillEnrollmentInfo(Enrollment enr,ReconEnrollmentDTO ghixData, String hiosIssuerId){
		if(enr!=null && ghixData!=null){
			ghixData.setAgentBrokerName(enr.getAgentBrokerName());
			ghixData.setAgentBrokerNpn(enr.getBrokerTPAAccountNumber1());//to be confirmed
			//ghixData.setBenefitEndDate(DateUtil.dateToString(enr.getBenefitEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			//ghixData.setBenefitStartDate(DateUtil.dateToString(enr.getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			ghixData.setCoverageYear(DateUtil.dateToString(enr.getBenefitEffectiveDate(), EnrollmentConstants.DATE_FORMAT_YYYY));
			ghixData.setHiosId(enr.getHiosIssuerId() != null ? enr.getHiosIssuerId():hiosIssuerId);
			ghixData.setQhpId(enr.getCMSPlanID());
			ghixData.setIssuerAssignedPolicyId(enr.getIssuerAssignPolicyNo());
			ghixData.setExchangeAssignedPolicyid(enr.getId()+"");
			if(enr.getEnrollmentStatusLkp()!=null ){
				ghixData.setEnrollmentStatus(enr.getEnrollmentStatusLkp().getLookupValueCode());
			}
			if(enr.getEnrollmentConfirmationDate()!=null){
				ghixData.setEnrollmentconfirmationDate(DateUtil.dateToString(enr.getEnrollmentConfirmationDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			}
			if(enr.getInsuranceTypeLkp()!=null){
				ghixData.setInsuranceTypeCode(enr.getInsuranceTypeLkp().getLookupValueCode());
			}
		}
		
	}
	private void fillPremiumInfo(Enrollment enr, ReconEnrollmentDTO ghixData) throws Exception{
		if(enr!=null && ghixData!=null){
			List<EnrollmentPremium> enrollmentPremiums= enrollmentPremiumRepository.findByEnrollmentId(enr.getId());
			if(enrollmentPremiums!=null && enrollmentPremiums.size()>0){
				 Collections.sort(enrollmentPremiums);
				List<Enrollee> enrollees=enr.getEnrolleesAndSubscriber();
				Date coverageStartDate= enr.getBenefitEffectiveDate();
				Date coverageEndDate= enr.getBenefitEndDate();
				List<ReconMonthlyPremiumDTO> enrollmentPremiumList= new ArrayList<>();
				for(EnrollmentPremium pre : enrollmentPremiums){
					if(pre.getGrossPremiumAmount()!=null){
						Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(pre.getMonth()-1,pre.getYear())); //Month Start Date
						Date monthEndDate = EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(pre.getMonth()-1,pre.getYear())); //Month End Date
						ReconMonthlyPremiumDTO prem= new ReconMonthlyPremiumDTO();
						enrollmentPremiumList.add(prem);
						if(null != pre.getAptcAmount() && pre.getAptcAmount().compareTo(0f) != 0){
						prem.setAptcAmt(pre.getAptcAmount());
						}else{
							prem.setAptcAmt(0f);
						}
						prem.setCoverageMonth(pre.getMonth());
						prem.setCoverageYear(pre.getYear());
						//HIX-101050 prem.setCsrAmt(enrollmentAudRepository.getLatestCSRBeforeDate(enr.getId(), monthEndDate));
						if(null != enr.getCsrMultiplier() && enr.getCsrMultiplier().compareTo(0.0f) != 0){
						prem.setCsrAmt(EnrollmentUtils.roundFloat(pre.getGrossPremiumAmount() * enr.getCsrMultiplier(), 2));
						}
						prem.setGrossPremium(pre.getGrossPremiumAmount());
						//prem.setIndividualPremium(pre.getNetPremiumAmount());
						
						//prem.setIssuerpremium(pre.);//not for GHIX data
						
						prem.setMemberCount(calculateActiveEnrolleeCount(enrollees, monthStartDate, monthEndDate ));
						if( (DateUtils.isSameDay(coverageEndDate,monthStartDate) ||coverageEndDate.after(monthStartDate)) && coverageEndDate.before(monthEndDate)){
							prem.setMonthEnddate( DateUtil.dateToString(coverageEndDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
						}else{
							prem.setMonthEnddate(DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(monthEndDate), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
						}
						if(coverageStartDate.after(monthStartDate) && coverageStartDate.before(monthEndDate)){
							prem.setMonthStartdate(DateUtil.dateToString(coverageStartDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
						}else{
							prem.setMonthStartdate(DateUtil.dateToString(monthStartDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
						}
						
						
						prem.setRatingArea(EnrollmentUtils.getFormatedRatingArea(enrolleeAudRespository.getSubscRatingByCovDate(enr.getId(), monthEndDate)));
						
					}
					
				}
				if(enrollmentPremiumList.size()>0){
					Collections.sort(enrollmentPremiumList);
					ghixData.setMonthlyPremiums(enrollmentPremiumList);
				}
			}
			
		}
	}
	
	
	/* *
	 * Send only enrollees and one subscribers record
	 */
	private int calculateActiveMemberCount(List<ReconDetailCsvDto> uniqueEnrolleeDTOs, Date monthStartDate, Date monthEndDate){
		Set<String> uniqueMemberIds= new java.util.HashSet<String>();
		
		if(uniqueEnrolleeDTOs!=null && uniqueEnrolleeDTOs.size()>0 && monthEndDate!=null && monthEndDate!=null){
			
			for (ReconDetailCsvDto enrolleeDTO : uniqueEnrolleeDTOs){
				if(enrolleeDTO.getCoverageStartDate().before(monthEndDate) && (enrolleeDTO.getCoverageEndDate() == null || DateUtils.isSameDay(enrolleeDTO.getCoverageEndDate(), monthStartDate) ||enrolleeDTO.getCoverageEndDate().after(monthStartDate) )){
					uniqueMemberIds.add(enrolleeDTO.getExchangeAssignedMemberId());
				}
				
			}
			
		}
		return uniqueMemberIds.size();
	}
	
	/* *
	 * Send only enrollees and subscribers
	 */
	private int calculateActiveEnrolleeCount(List<Enrollee> enrollees, Date monthStartDate, Date monthEndDate){
		Set<String> uniqueMemberIds= new java.util.HashSet<String>();
		
		if(enrollees!=null && enrollees.size()>0 && monthEndDate!=null && monthEndDate!=null){
			
			for (Enrollee enrollee : enrollees){
				if(enrollee.getEffectiveStartDate().before(monthEndDate) && (enrollee.getEffectiveEndDate() == null || DateUtils.isSameDay(monthStartDate, enrollee.getEffectiveEndDate()) ||enrollee.getEffectiveEndDate().after(monthStartDate) )){
					uniqueMemberIds.add(enrollee.getExchgIndivIdentifier());
				}
				
			}
			
		}
		return uniqueMemberIds.size();
	}
	private void fillMemberInfo(Enrollment enr, ReconEnrollmentDTO ghixData){
		
		if (enr != null && ghixData != null) {
			  // List<String> memberIds= new ArrayList<>();
			  List<Enrollee> enrollees = new ArrayList<>();
			  List<Enrollee> allEnrollees = enr.getEnrolleesAndSubscriber();
			  Set<String> set = new HashSet<String>();

			  for (Enrollee enrollee : allEnrollees) {
			    if (!enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL)) {
			      String individualIdentifier = enrollee.getExchgIndivIdentifier();
			      set.add(individualIdentifier);
			      enrollees.add(enrollee);
			    }
			  }

			  for (Enrollee enrollee : allEnrollees) {
			    if (enrollee.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(ENROLLMENT_STATUS_CANCEL)) {
			      String individualIdentifier = enrollee.getExchgIndivIdentifier();
			      if (!set.contains(individualIdentifier)) {
			        enrollees.add(enrollee);
			      }
			    }
			  }
			List<ReconMemberDTO> members= new ArrayList<ReconMemberDTO>();
			Enrollee subscriber= enr.getSubscriberForEnrollment();
			if(subscriber!=null && subscriber.getLastPremiumPaidDate()!=null){
				ghixData.setPaidThroughDate(DateUtil.dateToString(subscriber.getLastPremiumPaidDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
			}
			for(Enrollee enrollee: enrollees){
				//if(!memberIds.contains(enrollee.getExchgIndivIdentifier())){
					ReconMemberDTO member= new ReconMemberDTO();
					if(enrollee.getBirthDate()!=null){
						member.setBirthDate(DateUtil.dateToString(enrollee.getBirthDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
					}
					member.setEnrolleeId(enrollee.getId());
					member.setExchangeAssignedMemberId(enrollee.getExchgIndivIdentifier());
					member.setExchangeAssignedSubscriberId(enr.getExchgSubscriberIdentifier());
					member.setFirstName(enrollee.getFirstName());
					member.setMiddleName(enrollee.getMiddleName());
					member.setLastName(enrollee.getLastName());
					if(enrollee.getEffectiveStartDate()!=null){
						member.setMemberBeginDate(DateUtil.dateToString(enrollee.getEffectiveStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
						member.setMemberStartDate(EnrollmentUtils.truncateTime(enrollee.getEffectiveStartDate()));
					}
					if(enrollee.getEffectiveEndDate()!=null){
						member.setMemberEndDate(DateUtil.dateToString(enrollee.getEffectiveEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
					}
					member.setIndividualPremium(enrollee.getTotalIndvResponsibilityAmt());
					if(enrollee.getEnrolleeLkpValue()!=null){
						member.setEnrolleeStatus(enrollee.getEnrolleeLkpValue().getLookupValueCode());
					}
					
					Location homeAddress=enrollee.getHomeAddressid();
					if(homeAddress!=null){
						member.setHomeAddress1(homeAddress.getAddress1());
						member.setHomeAddress2(homeAddress.getAddress2());
						member.setHomeCity(homeAddress.getCity());
						member.setHomeCountyCode(homeAddress.getCountycode());
						member.setHomeState(homeAddress.getState());
						member.setHomeZip(homeAddress.getZip());
					}
					
					//relationToSubscriber
					EnrolleeRelationship rel=enrolleeRelationshipRepository.getRelationshipBySourceEndTargetId(enrollee.getId(), subscriber.getId());

					if(rel!=null && rel.getRelationshipLkp()!=null){
						member.setIndividualRelationshipCode(rel.getRelationshipLkp().getLookupValueCode());
					}
					
					
					member.setIssuerAssignedMemberId(enrollee.getIssuerIndivIdentifier());
					member.setIssuerAssignedSubscriberId(enr.getIssuerSubscriberIdentifier());
					Location mailAddress=enrollee.getMailingAddressId();
					if(mailAddress!=null){
						member.setMailingAddress1(mailAddress.getAddress1());
						member.setMailingAddress2(mailAddress.getAddress2());
						member.setMailingCity(mailAddress.getCity());
						member.setMailingState(mailAddress.getState());
						member.setMailingZip(mailAddress.getZip());
					}
					
					member.setSsn(enrollee.getTaxIdNumber());
					if(enrollee.getPersonTypeLkp()!=null && enrollee.getPersonTypeLkp().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.PERSON_TYPE_SUBSCRIBER)){
						member.setSubscriberIndicator(EnrollmentConstants.SUBSCRIBER_FLAG_YES);
					}else{
						member.setSubscriberIndicator(EnrollmentConstants.SUBSCRIBER_FLAG_NO);
					}
					member.setTelephoneNumber(enrollee.getPrimaryPhoneNo());
					if(enrollee.getTobaccoUsageLkp()!=null) {
						member.setTobacco(EnrollmentConstants.TobacoUsage.getValue(enrollee.getTobaccoUsageLkp().getLookupValueCode()));
					}
					if(enrollee.getGenderLkp()!=null) {
						member.setGender(enrollee.getGenderLkp().getLookupValueCode());
					}
					members.add(member);
				//}
				
				if(members.size()>0){
					Collections.sort(members);
					ghixData.setMembers(members);
				}
			}
		}
	}
	
	/**
	 * Method to split List
	 * @param list
	 * @param size
	 * @return
	 */
	private List<List<Long>> splitList(List<Long> list, Integer size){
		List<List<Long>> splittedList=null;
		if(list!=null && !list.isEmpty()){
			splittedList= new ArrayList<List<Long>>();
			int totalCount=list.size();
			for(int i=0; i<totalCount; i+=size){
				splittedList.add(new ArrayList<Long>(list.subList(i, Math.min(totalCount, i+size))));
			}
		}
		
		return splittedList;
	}

	private List<ReconMonthlyPremiumDTO>  prepareEmptyMonthlyPremiums(Date covStartDate, Date covEndDate,  List<ReconDetailCsvDto> uniqueEnrolleeDTOs)throws Exception{
		List<ReconMonthlyPremiumDTO> enrollmentPremiumList= null;
		if(covStartDate!=null && covEndDate!=null && EnrollmentUtils.getDaysBetween(covStartDate, covEndDate) != 0){
			enrollmentPremiumList= new ArrayList<ReconMonthlyPremiumDTO>();
			//boolean isShop=false;
			Calendar covSartCal = Calendar.getInstance();
			Calendar covEndCal = Calendar.getInstance();
			covSartCal.setTime(covStartDate);
			covEndCal.setTime(covEndDate);
			int startMonth=covSartCal.get(Calendar.MONTH)+1;
			int endMonth=covEndCal.get(Calendar.MONTH)+1;
			int startYear=covSartCal.get(Calendar.YEAR);
			for(int i=startMonth;i<=endMonth;i++){
				enrollmentPremiumList.add(prepareEmptyMonthlyPremium( i, startYear, covStartDate, covEndDate, uniqueEnrolleeDTOs));
			}
		}
		return enrollmentPremiumList;
	}
	
	private ReconMonthlyPremiumDTO prepareEmptyMonthlyPremium( int month, int year, Date coverageStartDate, Date coverageEndDate, List<ReconDetailCsvDto> uniqueEnrolleeDTOs) throws Exception{
		ReconMonthlyPremiumDTO prem= new ReconMonthlyPremiumDTO();
		prem.setCoverageMonth(month);
		prem.setCoverageYear(year);
		
		Date monthStartDate =  EnrollmentUtils.truncateTime(EnrollmentUtils.getMonthStartDate(month-1,year)); //Month Start Date
		Date monthEndDate = EnrollmentUtils.truncateTime(EnrollmentUtils.getStartDateForNextMonthIndividualReport(month-1,year)); //Month End Date
		
		prem.setMemberCount(calculateActiveMemberCount(uniqueEnrolleeDTOs, monthStartDate, monthEndDate));
		prem.setActiveDays(calculateActiveDays(coverageStartDate, coverageEndDate, monthStartDate, monthEndDate));
		if(( DateUtils.isSameDay(coverageEndDate, monthStartDate)|| coverageEndDate.after(monthStartDate) ) && coverageEndDate.before(monthEndDate)){
			prem.setMonthEnddate(DateUtil.dateToString(coverageEndDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}else{
			prem.setMonthEnddate(DateUtil.dateToString(EnrollmentUtils.getBeforeDayDate(monthEndDate), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		if(coverageStartDate.after(monthStartDate) && coverageStartDate.before(monthEndDate)){
			prem.setMonthStartdate(DateUtil.dateToString(coverageStartDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}else{
			prem.setMonthStartdate(DateUtil.dateToString(monthStartDate, EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
		}
		
		
		return prem;
	}

	
	private int calculateActiveDays(Date coverageStartDate, Date coverageEndDate,Date monthStartDate, Date monthEndDate) throws GIException{
		int daysActive=0;
		coverageStartDate = EnrollmentUtils.truncateTime(coverageStartDate); 
		coverageEndDate = EnrollmentUtils.truncateTime(coverageEndDate); 
		if(coverageStartDate != null && coverageEndDate !=null ){ 
			boolean isCoverageAfterMonthStart = coverageStartDate.after(monthStartDate);
			//					boolean isCoverageBeforeMonthEnd = coverageStartDate.before(endDate);
			boolean isCoverageTermBeforeMonthEnd = coverageEndDate.before(monthEndDate);
			//					boolean isCoverageTermAfterMonthStart = coverageEndDate.after(startDate);
			daysActive = EnrollmentUtils.daysInMonth(monthStartDate) ;

			if(isCoverageAfterMonthStart && isCoverageTermBeforeMonthEnd){
				// Calculate number of days between coverage start date and coverage end date
				daysActive = EnrollmentUtils.getDaysBetween(coverageStartDate, coverageEndDate) + 1;

			}else if(isCoverageAfterMonthStart){
				// Calculate number of days between coverage start date and month end date
				daysActive = EnrollmentUtils.getDaysBetween(coverageStartDate, monthEndDate);
			}else if(isCoverageTermBeforeMonthEnd){
				// Calculate number of days between month start date and coverage term date
				daysActive = EnrollmentUtils.getDaysBetween(monthStartDate, coverageEndDate) + 1;
			}
		}
		return daysActive;
	}
	
	
	private void triggerBatchJobWithParams(Integer fileId,  Job job,JobLauncher jobLauncher) {
		
				LOGGER.info("Triggering Snapshot Comparator Job");
				//Trigger batch Job
				try {
					JobParameters jobParameters = new JobParametersBuilder()
													.addString("fileId", fileId+"")
													.addLong("EXECUTION_DATE", new Date().getTime())
													.toJobParameters();
					
					jobLauncher.run(job,jobParameters);
					
					LOGGER.info("Wait till job completes");
					/*while(jobExecution.isRunning()){
					}
					LOGGER.info("Job complete with exit status : " + jobExecution.getExitStatus());*/
				} catch (Exception e) {
					LOGGER.error("Error in triggering batch job from inbound code", e);
				}
	}

	@Override
	public void updateSummary(Integer fileId, Job job, JobLauncher jobLauncher, long jobExecutionId) throws Exception {
		if(fileId!=null){
			EnrlReconSummary summary= enrlReconSummaryRepository.getReconSummaryByFileId(fileId);
			summary.setStatus(EnrlReconSummary.SummaryStatus.TRANSLATED.toString());
			long enrollmentInHixCount= getEnrollmentCountInHix(summary);
			summary.setTotalEnrlCntInHix((int)enrollmentInHixCount);
			Integer enrollmentErrorCount= enrlReconErrorRepository.getEnrollmentCountByFileId(fileId);
			summary.setRecCntFailed(enrollmentErrorCount);
			enrlReconSummaryRepository.saveAndFlush(summary);

			String batchJobStatus=null;

			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}

			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
					batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}

			String jobChaining = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.
					EnrollmentConfigurationEnum.
					ENRL_RECON_BATCH_JOB_CHAINING);

			if(null != jobChaining && "false".equalsIgnoreCase(jobChaining)){

				LOGGER.info(" NOT Triggering SNAPSHOT COMPARATOR JOB  for batch File_ID(S) : " + fileId);
				LOGGER.info(" ENRL_RECON_BATCH_JOB_CHAINING : "+ jobChaining);

			}else{
				triggerBatchJobWithParams( fileId,  job,  jobLauncher);
			}
		}
	}
	
	/**
	 * Get full name from first, middle and last names
	 * @param firstName
	 * @param middleName
	 * @param lastName
	 * @return
	 */
	private String getfullName(String firstName, String middleName, String lastName){
		StringBuilder enrolleeName = new StringBuilder();
		if(isNotNullAndEmpty(firstName)){
			enrolleeName.append(firstName);
			enrolleeName.append(" ");
		}
		if(isNotNullAndEmpty(middleName)){
			enrolleeName.append(middleName);
			enrolleeName.append(" ");	
		}
		if(isNotNullAndEmpty(lastName)){
			enrolleeName.append(lastName);
		}
		return enrolleeName.toString().trim();
	}


}
