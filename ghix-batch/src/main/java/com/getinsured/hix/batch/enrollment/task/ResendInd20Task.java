package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.service.ResendInd20Service;
import com.getinsured.hix.platform.util.exception.GIException;

public class ResendInd20Task implements Tasklet{

	private static final Logger LOGGER = LoggerFactory.getLogger(ResendInd20Task.class);
	private ResendInd20Service resendInd20Service;

	@Override
	public RepeatStatus execute(StepContribution arg0, ChunkContext arg1)throws Exception {
		LOGGER.info("ClearCarrierSendFlagTask.execute : START");
		try {
			long jobExecutionId=-1;
			jobExecutionId=arg1.getStepContext().getStepExecution().getJobExecutionId();
			resendInd20Service.sendIndvPSDetails(jobExecutionId);
		} catch (Exception e) {
			throw e;
		}
		return RepeatStatus.FINISHED;
	}

	public ResendInd20Service getResendInd20Service() {
		return resendInd20Service;
	}

	public void setResendInd20Service(ResendInd20Service resendInd20Service) {
		this.resendInd20Service = resendInd20Service;
	}
}
