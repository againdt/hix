<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap-datepicker-new.js" />"></script>
<script type="text/javascript">
$(document).ready(function(){
	var alertMessage = '${alertMessage}';
	 if(alertMessage){
		alert(alertMessage);
		window.location.reload(true);   
		
	}; 
	
});
</script>

<!--DELETE EMPLOYEE LIGHTBOX  -->
			<div id="deleteEmployee" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="DeleteEmployee" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
					<h3 id="deleteEmpLabel"><spring:message  code='label.emprdeleteemp'/></h3>
				</div>
				<div class="modal-body" id="deleteEmpBody">
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" aria-hidden="true" id="delEmpCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" id="deleteEmployeeButton"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			<!--TERMINATE EMOPLOYEE LIGHTBOX  -->
			<div id="terminateEmployee" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="ternimateEmployee" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
					<h3 id="ternimateEmpLabel"><spring:message  code='label.terminateemployment'/></h3>
				</div>
				<form id="frmterminateEmployee" name="frmterminateEmployee">
					<df:csrfToken/>
					<div class="modal-body">
						<span id="terminateEmpBodySpan"><spring:message  code='label.employeelastdayemplmt'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></span><br/>
						
					<!-- <div class="controls">
						<div class="input-append date ceddate-picker" data-date="" data-date-format="mm/dd/yyyy" id="terminationDateDiv">
                  			<input class="span10" type="text" name="terminationDate" id="terminationDate">
                 		 	<span class="add-on"><i class="icon-calendar"></i></span>
                 		</div>
                 		<span id="terminationDate_error"></span>
                 		<div><spring:message code= "label.note"/> <spring:message code = "label.terminationNote"/></div>
					</div>-->
					<div class="controls">
					<div class="input-append date ceddate-picker" data-date="" data-date-format="mm/dd/yyyy" id="terminationDateDiv">
                  			<input type="text" name="terminationDate" id="terminationDate" aria-required="true" aria-label="terminationDate " tabindex="0" readonly class="datePicker">
                 		 	<span class="add-on"><i class="icon-calendar"></i></span>
                 	</div>
                 	<span id="terminationDate_error"></span>
					<div><spring:message code= "label.note"/> <spring:message code = "label.terminationNote"/></div>
					</div>
					</div>
				</form>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" id="termEmpCancel"><spring:message  code='label.cancel'/></button>
					<%-- <button class="btn btn-primary" id="terminateEmployeeButton"><spring:message  code='label.ok'/></button> --%>
					<button class="btn btn-primary" data-dismiss="modal"  id="terminateEmployeeButton"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			<!--TERMINATE EMOPLOYEE CONFIRM LIGHTBOX  -->
			<div id="terminateEmployeeConfirm" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="terminateEmployeeConfirm" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3><spring:message  code='label.terminateemploymentconfirm'/></h3>
				</div>
				 <div class="modal-body">
				 <span id="terminateEmpBodySpan">
				 <spring:message  code='label.terminateemploymentconfirmmsg'/>
				 </span>
				 </div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" id="termEmpConfirmCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" data-dismiss="modal"  id="terminateEmployeeButtonConfirm"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			<!--UPDATE WORKSITE LIGHTBOX  -->
			<div id="updateWorksite" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="updateWorksite" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3 id="updateWorksiteLabel"><spring:message  code='label.updateworksite'/></h3>
				</div>
				<div class="modal-body" id="updateWorksiteBody">
					<p><strong><spring:message  code='label.validateworksite'/></strong></p>
					<c:forEach items="${worksites}" var="worksite">
						<div class="control-group">
								<label class="radio"> <input type="radio" name="worksites" id="worksites" value="${worksite.id}" />
                  			${worksite.location.address1}, ${worksite.location.city}, ${worksite.location.state}</label>
						</div>
					</c:forEach>
				</div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" id="updWkCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" id="updateWorksiteButton" onclick="updateWorkSiteAddr();"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			<!--DEACTIVATE EMOPLOYEE CONFIRM LIGHTBOX  -->
			<div id="deactivateEmployeeConfirm" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="deactivateEmployeeConfirm" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3><spring:message  code='label.deactivateemployee'/></h3>
				</div>
				 <div class="modal-body">
				 <span id="deactivateEmpBodySpan">
				 
				 </span>
				 </div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" id="deactivateEmpConfirmCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" data-dismiss="modal"  id="deactivateEmployeeButtonConfirm"><spring:message  code='label.ok'/></button>
				</div>
			</div>
			
			<!--REACTIVATE EMOPLOYEE CONFIRM LIGHTBOX  -->
			<div id="reactivateEmployeeConfirm" class="modal hide fade"   tabindex="-1" role="dialog" aria-labelledby="reactivateEmployeeConfirm" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">�</button>
					<h3><spring:message  code='label.reactivateemployee'/></h3>
				</div>
				 <div class="modal-body">
				 <span id="reactivateEmpBodySpan">
				 	
				 </span>
				 </div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal" id="reactivateEmpConfirmCancel"><spring:message  code='label.cancel'/></button>
					<button class="btn btn-primary" data-dismiss="modal"  id="reactivateEmployeeButtonConfirm"><spring:message  code='label.ok'/></button>
				</div>
			</div>			
<div class="gutter10">
<div class="row-fluid">
<ul class="page-breadcrumb">
  <li><a href="<c:url value="/shop/employer/" />" > &lt; Back</a> <span class="divider">|</span></li>
  <li><a href="<c:url value="/shop/employer/" />">Employees</a> <span class="divider">/</span></li>
  <li class="active">Manage List</li>
</ul>
    <div class="row-fluid">
      <div class="span8">
        <h3 id="skip"><spring:message  code='label.Employee'/> <small> ${empCount} <spring:message  code='label.emprroasteraddemph2'/></small></h3>
      </div>
      <div class="pull-right gutter10">
		<div class="dropdown gutter10 bulk-action">
			<span><small>(<span id="selected_items">0</span> <spring:message  code='label.itemsselected'/>) </small> &nbsp;</span> 
				<a class="dropdown-toggle btn btn-primary btn-small" data-toggle="dropdown" href="#" alt="dropdown toggle">
					<spring:message  code='label.bulkaction'/> <i class="icon-cog icon-white"></i></a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
            <li><a href="#updateWorksite" onclick="openWorkSiteModal();"><i class="icon-refresh"></i> <spring:message  code='label.updateworksite'/></a></li>
          </ul>
         </div>
		</div>
    </div>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
      	<div class="header">
        	<h4><spring:message  code='label.refineresults'/></h4>
        </div>
        <div class="gray graybg">
          <form class="form-vertical gutter10" id="frmsrchemp" name="frmsrchemp" action="<c:url value="/shop/employer/manage/list" />" method="POST">
			<df:csrfToken/>
            <div class="control-group">
              <label class="control-label" for="Name"><spring:message  code='label.emprname'/></label>
              <div class="controls">
                <input type="text" id="Name" name="name" placeholder="Name" value="${searchCriteria.name}" class="span11">
              </div>
            </div>
            <div class="control-group">
              <label class="control-label" for="Status"><spring:message  code='label.status'/></label>
              <div class="controls">
                <select name="employeeStatus" id="employeeStatus"  class="span11">
                  <option value="" <c:if test="${searchCriteria.employeeStatus == ''}"> selected="selected" </c:if>>Any</option>	
                  <option value="ELIGIBLE" <c:if test="${searchCriteria.employeeStatus == 'ELIGIBLE'}"> selected="selected" </c:if>>Eligible</option>
                  <option value="NOT_ELIGIBLE" <c:if test="${searchCriteria.employeeStatus == 'NOT_ELIGIBLE'}"> selected="selected" </c:if>>Not Eligible</option>
                  <option value="TERMINATED" <c:if test="${searchCriteria.employeeStatus == 'TERMINATED'}"> selected="selected" </c:if>>Terminated</option>
                </select>
              </div>
              <label class="control-label" for="Enrollment Status"><spring:message  code='label.enrollmentStatus'/></label>
              <div class="controls">
                <select name="enrollmentStatus" id="enrollmentStatus"  class="span11">
                  <option value="" <c:if test="${searchCriteria.enrollmentStatus == ''}"> selected="selected" </c:if>>Any</option>	
                  <option value="ENROLLED" <c:if test="${searchCriteria.enrollmentStatus == 'ENROLLED'}"> selected="selected" </c:if>>Enrolled</option>
                  <option value="NOT_ENROLLED" <c:if test="${searchCriteria.enrollmentStatus == 'NOT_ENROLLED'}"> selected="selected" </c:if>>Not Enrolled</option>
                  <option value="WAIVED" <c:if test="${searchCriteria.enrollmentStatus == 'WAIVED'}"> selected="selected" </c:if>>Waived</option>
                  <option value="CANCELED" <c:if test="${searchCriteria.enrollmentStatus == 'CANCELED'}"> selected="selected" </c:if>>Canceled</option>
                </select>
              </div>
            </div>
             <div class="control-group">
             <!-- <div class="controls">
                <label class="checkbox">
                  <input type="hidden" name="hideTerminatedValue" id="hideTerminatedValue" value="${fn:contains(searchCriteria.hideTerminated, 'YES')? 'YES' : 'NO'}" />
                  <input type="checkbox" name="hideTerminated" value='YES' id="hideTerminated" ${fn:contains(searchCriteria.hideTerminated, 'YES') ? 'checked="checked"' : ''} onclick="update_hideTerminated(this.id)">
                  <spring:message  code='label.hideterminated'/> </label>
              </div> -->
            </div>
             <c:choose>
            	<c:when test="${worksiteCount >1}">
            		<input type="hidden" value="0" name="worksiteLocations">
            		<p><strong>Worksite</strong></p>
		            <c:forEach items="${worksites}" var="worksite" varStatus="wsStatus">
		            <div class="control-group" <c:if test="${wsStatus.count > 2}"> name="wss" style="display:none;" </c:if> >
		              <div class="controls">
		                <label class="checkbox">
		                  <input type="checkbox" name="worksiteLocations" id="worksites_${worksite.id}" value="${worksite.id}"  ${fn:contains(searchCriteria.worksite, worksite.id) ? 'checked="checked"' : '' }>
		                  ${worksite.location.address1} ${worksite.location.city} ${worksite.location.state}  </label>
		              </div>
		            </div>
		            </c:forEach>
		            <c:if test="${worksiteCount >2}">
		            <div class="pull-right">
		            <a href="#" id="linkMore" onclick="showHideAllWS('More');">More...</a>
		            <a href="#" id="linkLess" style="display:none;" onclick="showHideAllWS('Less');">Less...</a>
		            </div>
		            </c:if>
            	</c:when>
            	<c:otherwise>
            		<input type="hidden" value="${worksites[0].id}" name="worksiteLocations">
            	</c:otherwise>
            </c:choose>
            <br/>
            <div class="txt-center">
            	<a href="#" class="btn" onclick="formSubmit();"> <spring:message  code='label.go'/></a>
           	</div>
          </form>
        </div>
        <br>
        <input type="button" name="submitButton" id="submitButton"
						onClick="javascript:window.location.href ='addemployee' " class="btn btn-primary span11 btn-block"
						value="<spring:message  code='label.addnewemp'/>" /> <br>
				</div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="">
        <c:choose>
			<c:when test="${fn:length(employeelist) > 0}">
				<table class="table table-condensed table-border-none table-striped">
					<thead>
						<tr class="header"> 
							<th style="width: 20px;">
								<div class="control-group">
                    			<div class="controls">
                      					<label class="checkbox"><input type="checkbox" name="AllEmpCheck" id="AllEmpCheck" onclick="checkAll()"></label>
                    			</div>
                  				</div>
                  			</th>
						     <th class="sortable" scope="col" style="width: 190px;"><dl:sort title="Name" sortBy="name" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Dependents" sortBy="dependentCount" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <%-- <th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Hour/Wk" sortBy="hrsPerWeek" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th> --%>
						     <th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Employment Status" sortBy="employeeStatus" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Enrollment Status" sortBy="enrollmentStatus" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     <c:if test="${worksiteCount >1}">
						     <th class="sortable" scope="col" style="width: 120px;"><dl:sort title="Work Site" sortBy="employerLocation.location.address1" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
						     </c:if>
							 <th></th>
						</tr>
					</thead>
					<c:forEach items="${employeelist}" var="empList">
							<tr>
								<td> 
									<div class="control-group">
									<div class="controls"> 
                      					<label class="checkbox">
                        					<input type="checkbox" name="empCheck" id="emp_${empList.id}" value="${empList.id}" onclick="update_selection(this.id)">
                      					</label>
                    				</div>
                  					</div> 
                  				</td>
								<td id="name_${empList.id}">  ${empList.name} </td>
								<td align="center"> ${empList.dependentCount} </td>
								<%-- <td align="center"> ${empList.hrsPerWeek} </td> --%>
								<td id="employeeStatus_${empList.id}"> ${fn:replace(empList["employeeStatus"],'_',' ')} </td>
								<td id="enrollmentStatus_${empList.id}"> ${fn:replace(empList["enrollmentStatus"],'_',' ')} </td>
								<c:if test="${worksiteCount >1}">
									<td> ${empList.locationAddress}, ${empList.locationCity}, ${empList.locationState}</td>
								</c:if>
								<td>
									<div class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" alt="dropdown toggle"><i class="icon-cog"></i><i class="caret"></i></a>
				                    <ul class="right dropdown-menu " role="menu" aria-labelledby="dLabel">
				                      <li><a href="<c:url value="/shop/employer/manage/employeeinfo/${empList.id}?appId=${empList.empappId}" />"><i class="icon-eye-open"></i> <spring:message  code='label.viewdetails'/></a></li>
				                      <li><a href="<c:url value="/shop/employer/manage/dependents/${empList.id}?appId=${empList.empappId}" />"><i class="icon-eye-open"></i> <spring:message  code='label.viewdependentdetails'/></a></li>
				                     <c:if test="${empList.employeeStatus != 'DELETED'}">
				                      	<li><a href="<c:url value="/shop/employer/manage/editemployee?id=${empList.id}&appId=${empList.empappId}" />"><i class="icon-pencil"></i> <spring:message  code='label.empredit'/></a></li>
				                      </c:if>
				                      <c:if test="${empActiveEnrollmentSize > 0 && empList.employeeStatus != 'TERMINATED' && empList.employeeStatus != 'NOT_ELIGIBLE'}">
				                      	<li><a href="<c:url value="/shop/employer/manage/employeeinfo/generateactivationlink/${empList.id}" />"><i class="icon-eye-open"></i><spring:message code='label.employeeactivationlink'/></a></li>
				                      </c:if>
				                       
				                      <c:if test="${empList.employeeStatus != 'TERMINATED'}">
				                      <li><a href="#terminateEmployee" class="gutter10" data-toggle="modal" onclick="manageEmpTerminate('${empList.id}');"><i class="icon-remove-circle"></i> <spring:message  code='label.terminateemployment'/></a></li>
				                      </c:if>
				                      <c:if test="${(empList.employeeStatus == 'ELIGIBLE')}">
				                      <li><a href="#deactivateEmployeeConfirm" class="gutter10" data-toggle="modal" onclick="deactivateEmployee('${empList.id}','${empList.name}');">  <spring:message  code='label.deactivateemployee'/></a></li>
				                      </c:if>
				                      <c:if test="${empList.employeeStatus == 'NOT_ELIGIBLE' || empList.employeeStatus == 'TERMINATED'}">
				                      <li><a href="#" class="gutter10" data-toggle="modal" onclick="reactivateEmployee('${empList.id}','${empList.name}');">  <spring:message  code='label.reactivateemployeemenu'/></a></li>
				                      </c:if>				                      
				                    </ul>
				                  </div>
				                </td>
							</tr>
						</c:forEach>
				</table>
				 <div class="center">
				<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
				 </div>
			</c:when>
			<c:otherwise>
				<hr />
				<div class="alert alert-info"><spring:message  code='label.nomatchingrecd'/></div>
			</c:otherwise>
		</c:choose>
        </div> 
      <!--gutter10--> 
    </div>
    <!--rightpanel--> 
  </div>
  <!--row-fluid--> 
</div>
</div> <!--gutter10--> 

<script type="text/javascript">

if (!('indexOf' in Array.prototype)) {
    Array.prototype.indexOf= function(find, i /*opt*/) {
        if (i===undefined) i= 0;
        if (i<0) i+= this.length;
        if (i<0) i= 0;
        for (var n= this.length; i<n; i++)
            if (i in this && this[i]===find)
                return i;
        return -1;
    };
}

var selrecs=new Array();

function update_hideTerminated(elem){
	if(document.getElementById(elem).checked){
		$('#hideTerminatedValue').val('YES');
	}else{
		$('#hideTerminatedValue').val('NO');
	}
}

function update_selection(elem){
	
	var selCount = parseInt($('#selected_items').html());
	
	if(document.getElementById(elem).checked){
		selCount++;
		selrecs.push(elem.replace("emp_",""));
		if(selCount=='${resultSize}')
			document.getElementById('AllEmpCheck').checked=true;
		else
			document.getElementById('AllEmpCheck').checked=false;
	}else{
		selCount--;
		var elemIndex = selrecs.indexOf(elem.replace("emp_",""));
		if(elemIndex >=0)
		selrecs.splice(elemIndex,1);
		
		document.getElementById('AllEmpCheck').checked=false;
	}
	if(selCount < 0) selCount=0;
	
	$('#selected_items').html(selCount);
	$('#selEmps').val(selrecs);
}
function checkAll(){
	 $(':checkbox[name="empCheck"]').each(function() 
	 {
		 if(document.getElementById('AllEmpCheck').checked == false){
         	this.checked = false;
         	$('#selected_items').html(0);
         	
         	var elemIndex = selrecs.indexOf(this.id.replace("emp_",""));
         	if(elemIndex >=0)
         		selrecs.splice(elemIndex,1);
		 }else{
			 this.checked = true;
			 selrecs.push(this.id.replace("emp_",""));
			 $('#selected_items').html('${resultSize}');
		 }	 
     });
	 $('#selEmps').val(selrecs);
	 
}

function manageEmpDelete(eid, ename){
	
	$('#deleteEmpBody').html("<spring:message code='label.empdeleteconfirmmsg' javaScriptEscape='true'/> "+ename + " ?");
	$('#deleteEmployeeButton').bind('click', function() {
		window.location.href ='deleteemployee/'+eid;
    });
}
function manageEmpTerminate(eid){
	
	$('#terminationDate_error').html('');
	$('#terminateEmpBodySpan').html("<spring:message code='label.employeelastdayemplmt' javaScriptEscape='true'/> <img src='<c:url value='/resources/images/requiredAsterix.png' />' width='10' height='10' alt='Required!' />");
	
	$('#terminateEmployeeButton').bind('click', function() {
		if( $("#frmterminateEmployee").validate().form() ) {
			$('#terminateEmployeeConfirm').modal('show');
		}else{
			return false;
		}
    }); 
	
	$('#terminateEmployeeButtonConfirm').bind('click', function() {
			window.location.href ='terminateemployee/'+eid+'&'+($('#terminationDate').val().replace("/","-").replace("/","-"));
    }); 
	 $('#frmterminateEmployee').trigger("reset");
}

function deactivateEmployee(eid,ename){
	var selRec = new Array();
	selRec.push(eid);
	
	var lastDayOfMonth = '${lastDayOfMonth}';
	$('#deactivateEmpBodySpan').html(ename + " <spring:message code='label.deactivateemployeeconfirmmsg1' javaScriptEscape='true'/> " + lastDayOfMonth + " <spring:message code='label.deactivateemployeeconfirmmsg2' javaScriptEscape='true'/> <br> <br> <spring:message code='label.deactivateemployeeconfirmmsg3' javaScriptEscape='true'/>");
	
	
	$('#deactivateEmployeeButtonConfirm').bind('click', function() {
			window.location.href ='deactivateemployee/'+eid;
    }); 
	
}

function reactivateEmployee(eid,ename){
	var emprenrollment = '${emprenrollment}';
	if(emprenrollment == 'none')
	{
		window.location.href ='reactivateemployee/'+eid+'/covdate/none';
	}else
	{
		var selRec = new Array();
		selRec.push(eid);
		var optionsstring = '';
		
		<c:forEach var="eachDate" items="${coverageDateList}" varStatus="loop">
		<fmt:formatDate pattern="MM/dd/yyyy" value="${eachDate}" var="optionValueDate"/>
		<fmt:formatDate type="date" value="${eachDate}" var="optionDisplayDate"/>	         			
			var date1 = '${optionDisplayDate}';
			optionsstring +=  "<option value='" + date1 +"'>" + date1 + "</option>";
		</c:forEach>
		
		
		/* var date1 = '${coverageeffdate1}';
		var date2 = '${coverageeffdate2}';
		
		optionsstring = optionsstring + "<option value='" + date1 +"'>" + date1 + "</option>";
		optionsstring = optionsstring + "<option value='" + date2 +"'>" + date2 + "</option>"; */
		
		$('#reactivateEmpBodySpan').html(" <spring:message code='label.reactivatecoverage' javaScriptEscape='true'/> " + ename +"<br><br><select name='reactivateCoveargeDate' id='reactivateCoveargeDate'>" + optionsstring + "</select> <br> <br> <spring:message code='label.reactivatesendemailmsg' javaScriptEscape='true'/>");		
		$('#reactivateEmployeeConfirm').modal('show');		
		
		$('#reactivateEmployeeButtonConfirm').bind('click', function() {
			reacivateCoverageDate = $("#reactivateCoveargeDate").val();
			window.location.href ='reactivateemployee/'+eid+'/covdate/'+reacivateCoverageDate;
	    }); 
	}
	
}
function openWorkSiteModal(){
	if(selrecs.length>0){
		$('#updateWorksite').modal('show');
	}else{
		alert("<spring:message code='label.validateselectanyemp' javaScriptEscape='true'/>");
	}
}

function updateWorkSiteAddr(){
	var selectedWorkSite="";
	 $(':radio[name="worksites"]').each(function() {
			if(this.checked)	 
				selectedWorkSite = this.value;
		 });
	 if(!selectedWorkSite){
		 alert("<spring:message code='label.validateAddress' javaScriptEscape='true'/>");
		 return false;
	 }
	 
	window.location.href ='updateworksite/'+selrecs.toString()+'&'+selectedWorkSite;
}

function formSubmit(){
	$('#frmsrchemp').submit();
}

function showHideAllWS(colmn){
	if(colmn=='More'){
		$('div[name=wss]').each(function() {
			$(this).show();
		});
		//$('#wss').show();
		$('#linkLess').show();
		$('#linkMore').hide();
	}
	if(colmn=='Less'){
		$('div[name=wss]').each(function() {
			$(this).hide();
		});
		//$('#wss').hide();
		$('#linkLess').hide();
		$('#linkMore').show();
	}
}

var validator2 = $('#frmterminateEmployee').validate({ 
	rules : {
		terminationDate: {required : true, date: true}
	},
	messages : {
		terminationDate: {required : "<span> <em class='excl'>!</em><spring:message code='label.validateterminationdate' javaScriptEscape='true'/></span>", 
			date: "<span> <em class='excl'>!</em><spring:message code='label.validatevterminationdate' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
		$('#terminationDate').unbind('focus');
	} 
});


$(document).ready(function(){
	var ctx = "${pageContext.request.contextPath}";
	var imgpath = ctx+'/resources/images/calendar.gif';
	
	var maxExpirationDays = '${maxExpirationDays}';
	var expEndDate = '+' + maxExpirationDays +'d';
	
	
	$('.add-on').click(function(){
		$('#terminationDate').bind('focus');
	});
	
	$('#terminationDateDiv').datepicker({
		endDate:expEndDate,
		format:'mm/dd/yyyy'
	}).on('changeDate', function(ev) {
		$(this).datepicker('hide');
	});
	
	$(document).keypress(function(e){
		if (e.keyCode === 27){
			$('.datePicker').datepicker('hide');
		}
	});
});
</script>
