<%@ include file="include.jsp" %>
<div id="sidebar" class="span3 margin0">
		<div class="header">
			<h4>Steps</h4>
		</div>
	 	<ul class="borderTop-sidebar nav nav-list">
	         <li id="tab1"><a href="#"><spring:message code="ssap.menu.startApp" text="Start your application"/></a></li>
	         <li id="tab2"><a href="#" ><spring:message code="ssap.menu.familyAndHousehold" text="Family and Household"/></a></li>
	         <li id="tab3" class="non_financial_hide"><a href="#"><spring:message code="ssap.menu.income" text="Income"/></a></li>
	         <li id="tab4" class="non_financial_hide"><a href="#"><spring:message code="ssap.menu.additionalQuestion" text="Additional Questions"/></a></li>
	         <li id="tab5"><a href="#" ><spring:message code="ssap.menu.reviewSign"/></a></li>
	         <!-- <li id="tab5"><a href="#" onClick="openJsonTab();">Show JSON</a></li> -->
	   	</ul>
   </div>