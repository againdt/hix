package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component("enrollmentAdminEffectuationProcessor")
public class EnrollmentAdminEffectuationProcessor implements ItemProcessor< String, String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAdminEffectuationProcessor.class);
	@Override
	public String process(String fileName) throws Exception {
		LOGGER.info("Processing File Name : " + fileName);
		return fileName;
	}

}
