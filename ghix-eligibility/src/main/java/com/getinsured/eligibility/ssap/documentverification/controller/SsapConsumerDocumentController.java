package com.getinsured.eligibility.ssap.documentverification.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.ssap.documentverification.exceptions.SsapDocumentsServiceException;
import com.getinsured.eligibility.ssap.documentverification.service.SsapApplicantDocumentService;
import com.getinsured.eligibility.util.SsapConstants;
import com.getinsured.hix.model.ConsumerDocument;




@Controller
@RequestMapping("/ssapdocuments")
public class SsapConsumerDocumentController {

	private static final Logger LOGGER = Logger.getLogger(SsapConsumerDocumentController.class);
	
	@Autowired private SsapApplicantDocumentService ssapApplicantDocumentService;
	
	
	@RequestMapping(value = "/insertSsapDocument", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<ConsumerDocument> insertSsapDocument(@RequestBody ConsumerDocument consumerDocument) {
		LOGGER.info("Inside insertSsapDocument");
		
		/** Step 1 : validate consumerDocument object */
		if (consumerDocument == null){
			/** return error response */
			LOGGER.warn(SsapConstants.CONSUMER_DOCUMENT_OBJECT_CANNOT_BE_NULL);
			return new ResponseEntity<ConsumerDocument>(new ConsumerDocument(), HttpStatus.PRECONDITION_FAILED);
		}

		/** Step 2 : main logic */
		ConsumerDocument responseConsumerDocumentObj;
		try{
			/** Perform Service - Save Operation */
			responseConsumerDocumentObj = ssapApplicantDocumentService.saveSsapApplicantDocument(consumerDocument);
		}catch(SsapDocumentsServiceException consumerServiceException){
			/** return error response */
			LOGGER.error(consumerServiceException.getMessage());
			return new ResponseEntity<ConsumerDocument>(new ConsumerDocument(), HttpStatus.METHOD_FAILURE);
		}

		/** Step 3 : Form Success Response */
		return new ResponseEntity<ConsumerDocument>(responseConsumerDocumentObj, HttpStatus.OK);

	}
	
	@RequestMapping(value = "/insertSsapDocumenttest", method = RequestMethod.GET)
	public @ResponseBody String insertSsapDocumenttest() {
		
		return "TEST_SUCCESS";

	}
}
