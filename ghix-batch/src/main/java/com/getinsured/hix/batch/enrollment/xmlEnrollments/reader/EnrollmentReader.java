package com.getinsured.hix.batch.enrollment.xmlEnrollments.reader;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.batch.item.xml.StaxEventItemReader;

import com.getinsured.hix.batch.enrollment.xmlEnrollments.EnrollmentFileResource;
import com.getinsured.hix.batch.enrollment.xmlEnrollments.exceptions.InvalidRecordException;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.platform.util.exception.GIException;

public class EnrollmentReader extends StaxEventItemReader<Enrollment> implements ItemStream{
	private final Logger logger = LoggerFactory.getLogger(EnrollmentFileResource.class);
	private EnrollmentFileResource enrollmentFileResource;
	private EnrollmentBatchService enrollmentBatchService ;
	private Long jobExecutionId;
	private JobService jobService;
	private String fileName;
	private String timeStamp;
	private String inbound834XMLPath;
	
	@Override
	public void open(ExecutionContext executionContext) throws ItemStreamException {
		try {
			doOpen();
		} catch (Exception e) {
			logger.error("Exception in EnrollmentReader open : " + e.getMessage());
			throw new ItemStreamException("Error in opening XML file for reading : " + e.getMessage());
		}
	}
  
	@Override
    protected Enrollment doRead() throws Exception {
        if(enrollmentBatchService == null){
			throw new GIException("enrollmentBatchService object in Reader is not set");
		}
		Enrollment enrollment = super.doRead();
        
        if(enrollment !=  null) {
			validateEnrollmentRecord(enrollment);
			this.enrollmentFileResource.incrementReadCount();
			
		}
		return enrollment;
    }
	
	private void validateEnrollmentRecord(Enrollment enrollment) throws Exception{
		
		String batchJobStatus=null;
		
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
										batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		try {
				boolean validEnrollment = enrollmentBatchService.updateEnrollmentByXML2(enrollment, fileName, timeStamp, jobExecutionId);
				
				if(!validEnrollment){
					this.enrollmentFileResource.incrementSkipCount();
					this.enrollmentFileResource.setBadResource(true);
					throw new InvalidRecordException("Invalid Enrollment Record, Enrollment Id : " + enrollment.getId() + " of file : " + enrollmentFileResource.getFilepathURL().toString());
			}
		} catch (GIException e) {
			this.enrollmentFileResource.incrementSkipCount();
			this.enrollmentFileResource.setBadResource(true);
			throw new InvalidRecordException(e.getMessage() + ", Enrollment Id : " + enrollment.getId() + " of file : " + enrollmentFileResource.getFilepathURL().toString() + " : ", e);
		}
	}
	
   private String getDateTime(Date date) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd-HHmmss");
		return df.format(date);
	}
	   
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		ExecutionContext ec = stepExecution.getExecutionContext();
        String filePath = ec.getString("fileResource");
        jobExecutionId = stepExecution.getJobExecutionId();
        logger.debug("Reading file : " + filePath);
        
        timeStamp =  getDateTime(stepExecution.getJobExecution().getCreateTime());
    	File xmlFile = new File(filePath);
    	
    	this.fileName = xmlFile.getName();
    	
    	try {
			URI uri = new URI(filePath);
			enrollmentFileResource = new EnrollmentFileResource(uri);	 
		} catch (URISyntaxException e) {
			logger.error(e.getMessage());
		}
		
		inbound834XMLPath =  ec.getString("inboundDir");
    }
	
	@Override
	public void doClose() throws Exception {
		String succesFolderPath=null;
		String failureFolderPath=null;
		
		if(inbound834XMLPath!=null){
			succesFolderPath= inbound834XMLPath +File.separator+ EnrollmentConstants.SUCCESS_FOLDER_NAME + File.separator + timeStamp;
			failureFolderPath = inbound834XMLPath + File.separator + EnrollmentConstants.FAILURE_FOLDER_NAME +File.separator+ timeStamp;
			
			URI fileURI = enrollmentFileResource.getFilepathURL();
			File file = new File(fileURI.getPath());
			File targetFolder = null;
		
			if(enrollmentFileResource.isBadResource()){
				targetFolder = new File(failureFolderPath);
				if (!(targetFolder.exists())) {
					targetFolder.mkdirs();
				}
	    		file.renameTo(new File(failureFolderPath + File.separator + file.getName()));
			}else{
				targetFolder = new File(succesFolderPath);
				if (!(targetFolder.exists())) {
					targetFolder.mkdirs();
	            }
				file.renameTo(new File(succesFolderPath + File.separator + file.getName()));
			}
		}
				
		super.doClose();
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
	
	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}

	public void setEnrollmentBatchService(EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}
}