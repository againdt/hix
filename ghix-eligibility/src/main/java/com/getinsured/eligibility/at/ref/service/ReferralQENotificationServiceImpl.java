package com.getinsured.eligibility.at.ref.service;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 *
 * @author pravin wadkar
 */

@Component("referralQENotificationService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralQENotificationServiceImpl implements ReferralQENotificationService {

	@Autowired
	private NoticeService noticeService;
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Autowired private PreferencesService preferencesService;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private ILocationRepository iLocationRepository;
	
	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired 
	private NoticeTypeRepository noticeTypeRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralQENotificationServiceImpl.class);

	@Override
	public String generateQEWithAutoLinkingNotice(String caseNumber) throws Exception {
		List<SsapApplication> applications = ssapApplicationRepository.findByCaseNumberOne(caseNumber);
		if (applications != null && applications.size() > 0) {
			return generate(applications.get(0).getId(), "QEWithAutoLinking");
		}

		return null;

	}

	@Override
	public String generateQEWithAutoLinkingNotice(long appId) throws Exception {
		/* LCE01DemographicChange */
		return generate(appId, "QEWithAutoLinking");
	}

	@Override
	public String generateOEWithAutoLinkingNotice(String caseNumber) throws Exception {
		List<SsapApplication> applications = ssapApplicationRepository.findByCaseNumberOne(caseNumber);
		if (applications != null && applications.size() > 0) {
			return generate(applications.get(0).getId(), "OEWithAutoLinking");
		}

		return null;

	}

	@Override
	public String generateOEWithAutoLinkingNotice(long appId) throws Exception {
		return generate(appId, "OEWithAutoLinking");
	}

	private String generate(long appId, String noticeTemplateName) throws NoticeServiceException,Exception {
		
		String ecmId = null;
		SsapApplication ssapApplication = getSSAPApplicationData(appId);
		// send EE061LCEDocumentRequiredNotice if status is PENDING
		if (ssapApplication.getValidationStatus() == ApplicationValidationStatus.PENDING){
			
			ecmId = referralLceNotificationService.generateLceDocumentRequiredNotice(ssapApplication.getCaseNumber());
			
		}else{
			int moduleId = extractHouseholdId(ssapApplication);
	
			String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
			String fullName = getName(ssapApplication);
			
			PreferencesDTO preferencesDTO = preferencesService.getPreferences(moduleId, false);
			Location location = getLocation(preferencesDTO);
			//Location location = getLocation(ssapApplication);
			
			String relativePath = "cmr/" + moduleId + "/";
			String ecmFileName = noticeTemplateName + "_" + moduleId + (new TSDate().getTime()) + ".pdf";
	
			//String emailId = getEmailId(ssapApplication);
			String emailId = getEmailId(preferencesDTO);
			
			List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
			
			GhixNoticeCommunicationMethod noticeCommunicationPref = getCommunicationMethod(preferencesDTO);
			
	        try {
	           noticeTypeRepository.findByEmailClassAndLanguage(noticeTemplateName, GhixLanguage.US_EN);
	           Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(ssapApplication, noticeTemplateName), relativePath, ecmFileName, moduleName, moduleId, validEmails,
				        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), fullName, location, noticeCommunicationPref);
	           ecmId = notice.getEcmId();
	        } catch (final NotificationTypeNotFound e) {
	        	if(LOGGER.isWarnEnabled()){
		        	LOGGER.warn(e.getMessage());
	        	}
	        }
		}
		return ecmId;
	}
	
	private GhixNoticeCommunicationMethod getCommunicationMethod(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getPrefCommunication() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getPrefCommunication() is null");
		}
		
		return preferencesDTO.getPrefCommunication();
	}
	
	private String getEmailId(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null){
			throw new GIRuntimeException("preferencesDTO is null");
		}
		
		return !StringUtils.isEmpty(preferencesDTO.getEmailAddress()) ? preferencesDTO.getEmailAddress() : null;
	}

	private Location getLocation(PreferencesDTO preferencesDTO) {
		if (preferencesDTO == null || preferencesDTO.getLocationDto() == null){
			throw new GIRuntimeException("preferencesDTO or preferencesDTO.getLocationDto() is null");
		}
		
		LocationDTO locationDto = preferencesDTO.getLocationDto();
		
		if (StringUtils.isEmpty(locationDto.getAddressLine1()) || StringUtils.isEmpty(locationDto.getCity()) || 
				StringUtils.isEmpty(locationDto.getState()) || StringUtils.isEmpty(locationDto.getZipcode())){
			throw new GIRuntimeException("preferencesDTO.getLocationDto() address fields missing");
		}
		
		Location location = new Location();
		location.setAddress1(locationDto.getAddressLine1());
		location.setAddress2(locationDto.getAddressLine2());
		location.setCity(locationDto.getCity());
		location.setState(locationDto.getState());
		location.setZip(location.getZip());
		
		return location;
	}
	
	private String generate(long appId, String noticeTemplateName, String fileName) throws NoticeServiceException {
		SsapApplication ssapApplication = getSSAPApplicationData(appId);

		int moduleId = extractHouseholdId(ssapApplication);

		String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
		String fullName = getName(ssapApplication);
		Location location = getLocation(ssapApplication);
		
		String relativePath = "cmr/" + moduleId + "/";
		String ecmFileName = fileName + "_" + moduleId + (new TSDate().getTime()) + ".pdf";

		String emailId = getEmailId(ssapApplication);

		List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;
		String ecmId = null;
		try {
	           noticeTypeRepository.findByEmailClassAndLanguage(noticeTemplateName, GhixLanguage.US_EN);
	           Notice notice = noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, getReplaceableObjectData(ssapApplication, noticeTemplateName), relativePath, ecmFileName, moduleName, moduleId, validEmails,
	   		        DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), fullName, location, GhixNoticeCommunicationMethod.Mail);
	           ecmId = notice.getEcmId();
	        } catch (final NotificationTypeNotFound e) {
	        	LOGGER.error(e.getMessage());
	        }
		
		return ecmId;
	}

	private int extractHouseholdId(SsapApplication ssapApplication) {

		if (ssapApplication.getCmrHouseoldId() != null && ssapApplication.getCmrHouseoldId().intValue() != 0) {
			return ssapApplication.getCmrHouseoldId().intValue();
		} else {
			throw new GIRuntimeException("Cmr Household Id ");
		}

	}

	private Location getLocation(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				if (ssapApplicant.getMailiingLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
				} else if (ssapApplicant.getOtherLocationId() != null) {
					return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
				}
			}
		}

		return null;
	}

	private String getName(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getFirstName() + " " + ssapApplicant.getLastName();
			}
		}

		return null;
	}

	private SsapApplication getSSAPApplicationData(long AppId) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByAppId(AppId);
		if (ssapApplications != null && ssapApplications.size() > 0) {
			return ssapApplications.get(0);
		}
		throw new GIRuntimeException("SSAP Id is null.Unable to fetch household id. Email cannot be triggered for" + AppId);
	}

	private String getUserName(int householdId) {

		return cmrHouseholdRepository.findUserNameByCmrId(householdId);
	}

	private Map<String, Object> getReplaceableObjectData(SsapApplication ssapApplication, String noticeTemplateName) throws NoticeServiceException {
		Map<String, Object> tokens = new HashMap<String, Object>();
		String oeStartDate = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.OPEN_ENROLLMENT_START_DATE);
		String name = getName(ssapApplication);
		tokens.put("userAccountName", name);
		tokens.put("userName", getUserName(ssapApplication.getCmrHouseoldId().intValue()));
		tokens.put("primaryApplicantName", name);
		tokens.put("caseNumber", ssapApplication.getCaseNumber());
		tokens.put("OEStartDate", DateUtil.dateToString(DateUtil.addToDate(DateUtil.StringToDate(oeStartDate, "MM/dd/yyyy"), 365), "MMMM dd, YYYY"));

		SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
		tokens.put("spanishDate", formatter.format(new TSDate()));
		tokens.put("todaysDate", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
		tokens.put(TemplateTokens.EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		tokens.put("exgName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		tokens.put(TemplateTokens.EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
		tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
		tokens.put("exgCityName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
		tokens.put("exgStateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
		tokens.put("zip", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
		tokens.put(TemplateTokens.EXCHANGE_URL, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		tokens.put("exchangeFax", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
		tokens.put("exchangeAddressEmail", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
		tokens.put("ssapApplicationId", ssapApplication.getId());

		return tokens;
	}

	private String getEmailId(SsapApplication ssapApplication) {
		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();

		for (SsapApplicant ssapApplicant : applicants) {
			if (ssapApplicant.getPersonId() == 1) {
				return ssapApplicant.getEmailAddress();
			}
		}

		return null;
	}

	@Override
	public String generateAutoRenewalNotice(String caseNumber, String noticeName, String fileName) throws NoticeServiceException {
		List<SsapApplication> applications = ssapApplicationRepository.findByCaseNumberOne(caseNumber);
		if (applications != null && applications.size() > 0) {
			return generate(applications.get(0).getId(), noticeName, fileName);
		}

		return null;
	}
}
