package com.getinsured.ssap.model.eligibility;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Defines in which mode we are running.
 *
 * @author Yevgen Golubenko
 * @since 8/9/19
 */
public enum RunMode {
  /**
   * Eligibility request is coming from the SSAP Web
   */
  ONLINE(0),

  /**
   * Eligibility request is coming from Batch/back-office
   */
  BATCH(1),

  /**
   * Eligibility request is coming from SSAP Web during Pre-eligibility
   * call.
   */
  PRE_ELIGIBILITY(2);

  private int mode;

  RunMode(int mode) {
    this.mode = mode;
  }

  /**
   * Returns mode number that will be used when invoking eligibility engine.
   *
   * @return numeric mode value.
   */
  @JsonValue
  public int getMode() {
    return mode;
  }
}
