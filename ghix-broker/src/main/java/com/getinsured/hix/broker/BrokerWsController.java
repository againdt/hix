package com.getinsured.hix.broker;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.DesignateService;
import com.getinsured.hix.broker.util.BrokerConstants;
import com.getinsured.hix.broker.util.BrokerUtils;
import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.broker.BrokerInformationDTO;
import com.getinsured.hix.dto.broker.DesignateBrokerDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixUtils;
import com.thoughtworks.xstream.XStream;
import com.wordnik.swagger.annotations.ApiOperation;

/**
 * Intercepts Rest URL calls made to ghix-broker module.
 */
@Controller
@RequestMapping(value = "/broker")
public class BrokerWsController {

	private static final Logger LOGGER = Logger.getLogger(BrokerWsController.class);

	@Autowired
	private BrokerService brokerService;

	@Autowired
	private UserService userService;
	
	@Autowired
	private DesignateService designateService;

	@ApiOperation(value = "View Agent/Broker module active status.", notes = "This is the welcome page for Agent/Broker module. This is internal method call to verify whether broker service is up or not.")
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome(){
		
		LOGGER.info("welcome : START");
		
		LOGGER.info("Welcome to GHIX-BROKER Service module");
		
		LOGGER.info("welcome : END");
		return "Welcome to GHIX-BROKER Service module";
	}

	/**
	 * This method intercepts a rest call from Enrollment module. Based on the broker id received in
	 * the request, broker details are retrieved and returned in the response.
	 * 
	 * @param id
	 *            broker Id
	 * @return String representation of Broker Response. This contains brokerId, Full name, Federal
	 *         EIN and License number
	 */
	@ApiOperation(value = "View Agent/Broker detail.", notes = "This shows the Agent/Broker details for a Broker identified by Id. It is used by Enrollment module.")
	@RequestMapping(value = "/getBrokerDetail/{id}", method = RequestMethod.GET)
	@ResponseBody
	public String getBrokerDetail(@PathVariable String id) {
		
		LOGGER.info("getBrokerDetail : START");

		EntityResponseDTO brokerResponse = new EntityResponseDTO();

		if (!"null".equals(id) && !BrokerUtils.isEmpty(id)) {

			LOGGER.debug("Received Rest call from Enrollment.");

			Integer brokerId = Integer.parseInt(id);
			Broker broker = brokerService.findById(brokerId);

			if (broker != null) {

				Integer userId = broker.getUser().getId();
				LOGGER.debug("Looking up User Account");

				// Retrieving corresponding AccountUser from DB
				AccountUser accountUser = userService.findById(userId);

				// Populating BrokerResponse object
				brokerResponse.setBrokerId(brokerId);
				brokerResponse.setStateLicenseNumber(broker.getLicenseNumber());
				brokerResponse.setStateEINNumber(broker.getFederalEIN());
				if (broker.getNpn() != null) {
					brokerResponse.setAgentNPN(broker.getNpn());
				}

				if (accountUser != null) {
					brokerResponse.setBrokerName(accountUser.getFullName());
				}

				brokerResponse.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
				brokerResponse.setResponseDescription("Success");

				LOGGER.debug("Populated Broker Response object");
			}
			else {

				LOGGER.error("Broker Id does not exist");
				brokerResponse.setResponseCode(BrokerConstants.BROKERDOESNOTEXIST);
				brokerResponse.setResponseDescription("Broker does not exist in system");
			}
		}
		else {

			LOGGER.error("Broker Id in request is null");
			brokerResponse.setResponseCode(BrokerConstants.RESPONSECODEINVALIDINPUT);
			brokerResponse.setResponseDescription("Broker id is null");
		}

		// Marshalling BrokerResponse before responding to ghix-enrollment
		String brokerDetailResponse = marshal(brokerResponse);

		LOGGER.debug("Retrieved Broker Information successfully. Sending the response to enrollment");
		
		LOGGER.info("getBrokerDetail : END");
		
		return brokerDetailResponse;
	}
	
	/**
	 * Handles the POST request for designating employer when Agent tries to add Employer on behalf of Employer.
	 *
	 * @param request
	 * @param employerId
	 * @param response
	 */
	@ApiOperation(value = "Designate an Employer.", notes = "This designates an Employer by a Agent/Broker.")
	@RequestMapping(value = "/designateEmployer", method = RequestMethod.POST)
	@ResponseBody
	public String designateEmployer(@RequestBody String employerRequestStr) {
		LOGGER.info("designateEmployer: START ");		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		DesignateBrokerDTO designateBrokerDTO = (DesignateBrokerDTO)xstream.fromXML(employerRequestStr);	
		EntityResponseDTO designateResponse = new EntityResponseDTO();
		try {
			DesignateBroker designateBroker = designateService.designateEmployerByBroker(designateBrokerDTO.getBrokerId(),designateBrokerDTO.getEmployerId());
			if(designateBroker.getId() != 0)
			{
				designateResponse.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
				designateResponse.setResponseDescription("Success");
			}
			else
			{
				designateResponse.setResponseCode(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER);
				designateResponse.setResponseDescription("Failure");
			}
		} catch (Exception e) {
			LOGGER.debug("Error while designating an agent", e);
		}
		
		// Marshalling designateResponse before responding to ghix-web
		String designateEmployerResponse = marshal(designateResponse);

		LOGGER.debug("Retrieved Broker Information successfully. Sending the following response :"
				+ designateEmployerResponse);
		
		LOGGER.info("designateEmployer: END ");
		
		return designateEmployerResponse;
		
	}

	/**
	 * Marshal and form XML response.
	 * 
	 * @param response
	 * @return XML String
	 */
	private String marshal(EntityResponseDTO brokerResponse) {
		
		return GhixUtils.getXStreamStaxObject().toXML(brokerResponse);
	}
	
	/**
	 * Handles the request for fetching the Agent/Broker information by EmployerID
	 *
	 * @param employerId
	 * @return brokerObj
	 */
	@ApiOperation(value = "Retrieve the designated Agent/Broker information.", notes = "This retrieves the designated Agent/Broker information for an Employer identified by Id.")
	@RequestMapping(value = "/brokerinformation", method = RequestMethod.POST)
    @ResponseBody
    public String brokerInformation(@RequestBody String employerRequestStr) {
           LOGGER.info("brokerInformation: START ");
           XStream xstream = GhixUtils.getXStreamStaxObject();
           BrokerDTO designateResponse = new BrokerDTO();
           
           BrokerInformationDTO brokerInformationDTO = (BrokerInformationDTO)xstream.fromXML(employerRequestStr);
           
           try {
        	   DesignateBroker designateBroker = designateService.findBrokerByEmployerIdAndStatus(brokerInformationDTO.getEmployerId(),DesignateBroker.Status.Active);
	   			if(designateBroker!= null && designateBroker.getId() != 0)
	   			{
	   				Broker brokerObj = brokerService.getBrokerDetail(designateBroker.getBrokerId());
	   				
	   				designateResponse.setId(brokerObj.getId());
	   				designateResponse.setCompanyName(brokerObj.getCompanyName());
	   				designateResponse.setCertificationStatus(brokerObj.getCertificationStatus());
	   				
	   				AccountUser brokerUser = brokerObj.getUser();
	   				
	   				if(null != brokerUser) {
		   				designateResponse.setFirstName(brokerUser.getFirstName());
		   				designateResponse.setLastName(brokerUser.getLastName());
		   				designateResponse.setEmail(brokerUser.getEmail());
	   				}
	   				
	   				designateResponse.setResponseCode(BrokerConstants.RESPONSECODESUCCESS);
	   				designateResponse.setResponseDescription("Success");
	   			}
	   			else
	   			{
	   				designateResponse.setResponseCode(BrokerConstants.RESPONSECODENOAGENTDESIGNATIONRECORDFOREMPLOYER);
	   				designateResponse.setResponseDescription("Failure");
	   			}
	   		} catch (Exception e) {
	   			LOGGER.debug("Error in brokerInformation", e);
	   		}
          
           LOGGER.info("brokerInformation: END ");
           return GhixUtils.xStreamHibernateXmlMashaling().toXML(designateResponse);
	}
	
	@ApiOperation(value = "View Agent/Assister detail.", notes = "This shows the Agent/Assister details for an Individual identified by Id. It is used by Enrollment module.")
	@RequestMapping(value = "/getBrokerAssisterDetail/{id}", method = RequestMethod.GET)
	@ResponseBody
	public EntityResponseDTO getBrokerAndAssisterDetail(@PathVariable String id) {
		
		LOGGER.info("getBrokerAssisterDetail : START");

		EntityResponseDTO response = new EntityResponseDTO();

		if (!"null".equals(id) && !BrokerUtils.isEmpty(id)) {
			LOGGER.debug("Received Rest call from Enrollment for Individual : "+id);
			response = brokerService.getBrokerAssisterDetailsByHouseholdId(Integer.parseInt(id));
		} else {
			LOGGER.error("Individual Id in request is null");
			response.setResponseCode(BrokerConstants.RESPONSECODEINVALIDINPUT);
			response.setResponseDescription("Broker id is null");
		}

		LOGGER.debug("Retrieved Broker/Assister Information successfully. Sending the response to enrollment");
		
		LOGGER.info("getBrokerAssisterDetail : END");
		
		return response;
	}
}
