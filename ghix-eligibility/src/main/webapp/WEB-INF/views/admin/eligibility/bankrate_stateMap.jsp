<%@ include file="datasource.jsp" %>
<%@ page import = "java.util.*" %>
<%@ page import="org.springframework.context.support.ClassPathXmlApplicationContext"%>
<%@ page import="org.apache.commons.dbcp.BasicDataSource"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<%
	String whereClause = request.getParameter("whereClause");
	if( whereClause == null || whereClause == ""){
		whereClause = "CREATION_TIMESTAMP <= systimestamp ";
		
	}
%>

<sql:query dataSource="jdbc/ghixDS" var="result1">
select zipcode as zip, count(zipcode) as total
from elig_lead
where zipcode IS NOT NULL
and AFFILIATE_ID = (select max(id) from aff_affiliate where upper(COMPANY_NAME) = 'Bank Rate')
and <%=whereClause%>
group by zipcode
order by total desc
</sql:query> 


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>
<link rel="stylesheet" href="/resources/demos/style.css" />

<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Bank Rate Requests</title>
  
<style>
  table#table1 {
  	border: 1px solid #666;
  	width: 80%;
  	margin: 20px 0 20px 0 !important;
  	font-size: 14px;
  }
  
  th,td {
  	padding: 2px 4px 2px 4px !important;
  	text-align: left;
  	vertical-align: top;	
  }
  
  thead tr {
  	background-color: #fc0;
  }
  
  th.sorted {
  	background-color: orange;
  }
  
  th a,th a:visited {
  	color: black;
  }
  
  th a:hover {
  	text-decoration: underline;
  	color: black;
  }
  
  th.sorted a,th.sortable a {
  	background-position: right;
  	display: block;
  	width: 100%;
  }
  
  
  tr.odd {
  	background-color: #fff
  }
  
  tr.tableRowEven,tr.even {
  	background-color: #fea
  }
  
  div.exportlinks {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	margin: 2px 0 10px 0;
  	width: 79%;
  }
  
  span.export {
  	padding: 0 4px 1px 20px;
  	display: inline;
  	display: inline-block;
  	cursor: pointer;
  }
  
  span.pagebanner {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	width: 79%;
  	margin-top: 10px;
  	display: block;
  	border-bottom: none;
  }
  
  span.pagelinks {
  	background-color: #eee;
  	border: 1px dotted #999;
  	padding: 2px 4px 2px 4px;
  	width: 79%;
  	display: block;
  	border-top: none;
  	margin-bottom: -5px;
  }
  
  
  .group-1 {
      font-weight:bold;
      padding-bottom:10px;
      border-top:1px solid black;
  }
  .group-2 {
      font-style:italic;
      border-top: 1px solid black;
  
  }
  .subtotal-sum, .grandtotal-sum {
      font-weight:bold;
      text-align:right;
  }
  .subtotal-header {
      padding-bottom: 0px;
      border-top: 1px solid white;
  }
  .subtotal-label, .grandtotal-label {
      border-top: 1px solid white;
      font-weight: bold;
  }
  .grouped-table tr.even {
      background-color: #fff;
  }
  .grouped-table tr.odd {
      background-color: #fff;
  }
  .grandtotal-row {
      border-top: 2px solid black;
  }
  
  </style>

  
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type="text/javascript">
    google.load('visualization', '1', {packages: ['geochart']});

    function drawVisualization() {
      var data = google.visualization.arrayToDataTable([    
        ['State',   'Requests'],        
        <c:forEach var="row" items="${result1.rows}">
		['<c:out value="${row.zip}"/>',<c:out value="${row.total}"/>],
	</c:forEach>

      ]);
      
      var geochart = new google.visualization.GeoChart(
          document.getElementById('visualization'));
     	  geochart.draw(data, {height: 500,width: 800, region: 'US', resolution: 'metros'});
    }
    

    google.setOnLoadCallback(drawVisualization);
</script>
<script type="text/javascript">
	
$(function() {
	$( "#date1" ).datepicker().attr( 'readOnly' , 'true' );
});

$(function() {
	$( "#date2" ).datepicker().attr( 'readOnly' , 'true' );;
});

function setWhereClause(type){
	
	var whereClause = '';
	var filterStatement = '';
	
	//Step 1: Get the Dates
	var date1 = document.getElementById('date1').value;
	var date2 = document.getElementById('date2').value;

	if (date1 && date2) {

		if (whereClause) {
			whereClause = whereClause
					+ "and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
			

		} else {
			whereClause = whereClause
					+ " CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and to_timestamp('"
					+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
		}
		
		filterStatement = filterStatement + ' Start Date : ' + date1
		+ ' End date : ' + date2;
		
	} else {
		if (date1 && !date2) {
			if (whereClause) {

				whereClause = whereClause
					+ " and CREATION_TIMESTAMP between to_timestamp('"
					+ date1
					+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				
			} else {
				whereClause = whereClause
						+ " CREATION_TIMESTAMP between to_timestamp('"
						+ date1
						+ " 00:00:00' ,'mm/dd/yyyy HH24:MI:SS') and systimestamp ";
				+ date1 + "'";
				
			}
			
			filterStatement = filterStatement + ' Start Date : ' + date1;
			
		} else {

			if (!date1 && date2) {
				if (whereClause) {
					whereClause = whereClause
							+ " and CREATION_TIMESTAMP <= to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				} else {
					whereClause = whereClause
							+ " CREATION_TIMESTAMP <=  to_timestamp('"
									+ date2 + " 23:59:59','mm/dd/yyyy HH24:MI:SS')";
					
				}

			}
			
			filterStatement = filterStatement + ' End Date : ' + date2;
		}

	}
	if (!whereClause) {
		whereClause = "CREATION_TIMESTAMP <= systimestamp";
	}
	
	document.getElementById('whereClause').value = whereClause;
	document.getElementById('filterStatement').value = filterStatement;
}

</script>
</head>
<body style="font-family: Arial;border: 0 none;">
<h4>Visitor's Data - Shows data based on the zip code for which bank rate users are trying to get quotes</h4>
<form method="post" action="stateMap">
	<table border="1" style="border-color:#C0C0C0">
		<tbody>
		<tr><td>
				Date Start:	<input type="text" id="date1" name="date1" size="10" maxlength="10"/> 
				End: <input type="text" id="date2" name="date2" size="10" maxlength="10"/>
			</td>
			<td>
				<input type="submit" value="Search" name="B4" title="Filter requests based on field value" onclick="setWhereClause();"/>
				<input id="whereClause" name="whereClause" type="hidden"/>
				<input id="filterStatement" name="filterStatement" type="hidden"/>
			</td>
		</tr>
		</tbody>
	</table>
</form>
<br>
<%
	if (request.getParameter("filterStatement") != null) {
		out.println("Search Criteria : " + request.getParameter("filterStatement"));
	}
%>
<br><br>
<b>Total Requests (Map)</b>
<div id="visualization"></div>
<br><br>
<b>Total Requests (Table)</b>
<br>

<display:table name="${result1.rows}"  requestURI="bankrate_stateMap" id="table1" export="true" >
	<display:column property="zip" title="State" sortable="false" />
	<display:column property="total" title="Total" sortable="false" />
</display:table>

</body>
</html>

