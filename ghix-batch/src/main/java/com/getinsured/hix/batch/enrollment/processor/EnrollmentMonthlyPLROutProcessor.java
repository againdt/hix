package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component("enrollmentMonthlyPLROutProcessor")
public class EnrollmentMonthlyPLROutProcessor  implements ItemProcessor< String, String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentMonthlyPLROutProcessor.class);

	@Override
	public String process(String householdCaseId) throws Exception {
		return householdCaseId;
	}
}
