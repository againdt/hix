<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
        <li><a href="<c:url value="/planmgmt/issuer/account/profile/issuerprofile"/>">&lt; <spring:message  code="label.back"/></a></li>
        <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
        <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
        <li><spring:message  code="pgheader.financialInformation"/></li>
    </ul><!--page-breadcrumb ends-->
    
  <%--   <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
  <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/issuerprofile" />"><spring:message  code="pgheader.issuerProfile"/></a></li>
                    <c:if test="${STATE_CODE != 'ID' && STATE_CODE != 'MN'}">
                    	<li class="active"><a href="#"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/company/info" />"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/individual/info" />"><spring:message  code="pgheader.update.inidividualMarketProfile"/></a></li>
                    <%--  <c:if test="${STATE_CODE != 'ID'}">
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/market/shop/info" />"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    </c:if> --%>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/accreditationdocument/view" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/certification/status" />"><spring:message  code="pgheader.certificationStatus"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/account/profile/viewhistory" />"><spring:message  code="pgheader.issuerHistory"/></a></li>
                    <li><a href="<c:url value="/planmgmt/issuer/crossWalkStatus" />"><spring:message  code="pgheader.planIdCrosswalk"/></a></li>
                    <c:if test="${STATE_CODE == 'ID'}">
                    	<li><a href="<c:url value="/planmgmt/issuer/displayNetworkTransparencyData" />"><spring:message code="pgheader.networkTransparencyStatus"/></a></li>
                    </c:if>
                </ul>
                <!-- end of side bar -->
                <a href='<c:url value="/planmgmt/issuer/account/profile/financial/add"/>' class="btn btn-primary  btn-addpayment"><i class="icon-plus icon-white"></i> <spring:message  code="label.addPaymentMethod"/></a>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
             	<c:choose>
						<c:when test="${fn:length(paymentMethodsObj) > 0}">
							<div class="header margin5-b">
								<h4 class="pull-left">Financial Information</h4>	
							</div>
							<table class="table table-condensed table-border-none table-striped">
								<thead>
									<tr class="header">							
								 		<th scope="col" class="sortable"><dl:sort title="Payment Method Name" sortBy="paymentMethodName" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="Bank Name" sortBy="financialInfo.bankInfo.bankName" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="Last Updated" sortBy="financialInfo.updated" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="Status" sortBy="status" sortOrder="${sortOrder}"></dl:sort></th>
										<th scope="col" class="sortable"><dl:sort title="Default" sortBy="isDefault" sortOrder="${sortOrder}"></dl:sort></th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								<c:forEach items="${paymentMethodsObj}" var="paymentMethodsObj" varStatus="vs">
									<c:set var="encPaymentId" ><encryptor:enc value="${paymentMethodsObj.id}" isurl="true"/> </c:set>
									<tr>
										<td  id="name_${paymentMethodsObj.id}"><a href="<c:url value="/planmgmt/issuer/account/profile/financial/info/${encPaymentId}" />">${paymentMethodsObj.paymentMethodName}</a></td>
										<td>${paymentMethodsObj.financialInfo.bankInfo.bankName} </td>
										<td><fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${paymentMethodsObj.financialInfo.bankInfo.updated}" timeZone="${timezone}" /></td>
										<td id="status_${vs.index}">${paymentMethodsObj.status}</td>
										<td id="defaulticon_${vs.index}">
												<c:choose>
											        <c:when test="${'Y' == paymentMethodsObj.isDefault}">											                
														<i class="icon-ok-sign"></i> <!-- this icon will indicate whether or not the account is default -->														
													</c:when>
													<c:otherwise>
													<span></span>
												 	</c:otherwise>
												</c:choose>												
										</td>
										<td>
											<div class="dropdown">
													<a href="#" data-target="#"
														data-toggle="dropdown" role="button" id="dLabel"
														class="dropdown-toggle"><i
														class="icon-cog"></i><b class="caret"></b>
													</a>
													<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
														class="dropdown-menu pull-right"><!-- class removed "action-align-l" -->	
														<li>
														<a href="#" onClick="updatePaymentMethod(this,'edit');" id="edit_${vs.index}"><i class="icon-pencil"></i> Edit</a></li>													
														<c:if test="${paymentMethodsObj.status == 'Active'}">
															<li><a href="#" onClick="updatePaymentMethod(this,'InActive');" id="changestatus_${vs.index}"><i class="icon-remove"></i> Inactive</a></li>
														</c:if>
														<c:if test="${paymentMethodsObj.status == 'InActive'}">
															<li><a href="#" onClick="updatePaymentMethod(this,'Active');" id="changestatus_${vs.index}"><i class="icon-ok"></i> Active</a></li>
														</c:if>
														<c:if test="${'N' == paymentMethodsObj.isDefault && 'Active' == paymentMethodsObj.status}">
															<li><a href="#" onClick="updatePaymentMethod(this,'makedefault');" id="lidefault_${vs.index}"><i class="icon-ok-sign"></i> Make Default</a></li>
														</c:if>												
													</ul>
												</div>
											<input type="hidden" name="paymentid" id="paymentid_${vs.index}"  value="${encPaymentId}">
											 <input type="hidden" name="default" id="default_${vs.index}"  value="${paymentMethodsObj.isDefault}">
											
											<c:if test="${'Y' == paymentMethodsObj.isDefault}">
												 <input type="hidden" name="defaultpaymentid_${vs.index}" id="defaultpaymentid" value="${paymentMethodsObj.id}">
											</c:if>											
											<input type="hidden" value="" name="existingpaymentid" id="existingpaymentid" />
										</td>
									</tr>
								
								</c:forEach>
							</table>
							<div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info">No matching records found.</div>
					</c:otherwise>
				</c:choose>
           </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<df:csrfToken/>
<script type="text/javascript">
	$('.ttclass').tooltip();
	var csrValue = $("#csrftoken").val();
	function updatePaymentMethod(e,test) {		
		if (test == 'makedefault') {
			var didConfirm = confirm("This will make current payment method as default and remove default setting of other payment method. Is it ok?");
			if (didConfirm == true) {

			} else {
				return false;
			}
		}
		if (test != 'edit') {
			var validateUrl = '';
			var parameter = '';
			
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);
			
			var selectedstatus = '';
			if (test == 'makedefault') {
				validateUrl = 'makePaymentDefault';				
				parameter = {					
					id : $("#paymentid" + index).val(),	
					existingDefaultId : ($("#defaultpaymentid").val() != null) ? $("#defaultpaymentid").val() : 0,
					csrftoken:csrValue
				};
			} else {
				validateUrl = 'changePaymentStatus';
				selectedstatus = test;
				parameter = {
					id : $("#paymentid" + index).val(),
					status : selectedstatus,
					csrftoken:csrValue
				};
				if(selectedstatus=='InActive' && ('Y'==($('#default' + index).val()))){
					alert("Default Payment cannot be made inactive, kindly make other payment as default, before making this payment as inactive and then deactivate this payment method");
					return false;
				}
			}

			$
					.ajax({
						url : validateUrl,
						type : "POST",
						data : parameter,
						success : function(response) {							
							if (response != null || response != 'null') {
								if (selectedstatus != '') {
									
									
									$('#status' + index).text(response);
									
									$('#changestatus' + index).remove();
									
									if (response == 'InActive') {
									  $('#action'+ index ).append(
										    $('<li>').append(
										        $('<a>').attr({'href': '#',
										           'onClick': 'updatePaymentMethod(this,\'Active\')',
										           'id': 'changestatus'+index}).append(
										                   $('<i>').attr('class', 'icon-ok')).append('Active')
										                   
							          )); 
									  
									  if ('N'==($('#default' + index).val())){
										  $('#lidefault' + index).remove();
									  }
									}
									 else if (response == 'Active') {
										 $('#action'+ index ).append(
										 $('<li>').append(
											        $('<a>').attr({'href': '#',
											           'onClick': 'updatePaymentMethod(this,\'InActive\')',
											           'id': 'changestatus'+index}).append(
											                   $('<i>').attr('class', 'icon-remove')).append('InActive')
								         )); 
										 
										 if ('N'==($('#default' + index).val())){
											 $('#action'+ index ).append(
													 $('<li>').append(
														        $('<a>').attr({'href': '#',
														           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
														           'id': 'lidefault'+index}).append(
														                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
											         )); 
										 }
									}
									
								} else {
									if (response == 'Y') {
										$('#status' + index).text(
												$('#status' + index).text());
										$('#defaulticon' + index).text('');
										$('#defaulticon' + index).append($('<i>').attr('class', 'icon-ok-sign'));
										$('#default' + index).val('Y');
										$('#lidefault' + index).remove();
										
										if ($('#defaultpaymentid').length)
										{
												$("#defaultpaymentid").val(
														$("#paymentid" + index).val());
												
												var previousdefaultname = $(
														"#defaultpaymentid").attr(
														'name');
												var newstartindex = previousdefaultname
														.indexOf("_");
												var newindex = (previousdefaultname)
														.substring(
																newstartindex,
																(previousdefaultname).length);
												$('#default' + newindex).val('N');
												$('#defaulticon' + newindex).text('');
												if('Active' == $('#status'+newindex).text()){
												$('#action'+ newindex ).append(
														 $('<li>').append(
															        $('<a>').attr({'href': '#',
															           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
															           'id': 'lidefault'+newindex}).append(
															                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
												         )); 
												}
												$("#defaultpaymentid").attr('name','defaultpaymentid' + index);
										}
										else{
											$('<input>').attr({
											    type: 'hidden',
											    id: 'defaultpaymentid',
											    name: 'defaultpaymentid'+index,
											    value:$("#paymentid" + index).val()
											}).appendTo('form');
										}
										
									}
								}
							}
						},
					});
		} else {
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);				
			location.href= "<c:url value='/planmgmt/issuer/account/profile/financial/edit/"+ $("#paymentid" + index).val() +"'/>";;			
		}
		
	}	
	
</script>

