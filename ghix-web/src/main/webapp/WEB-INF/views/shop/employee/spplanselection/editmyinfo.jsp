<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/employee-dashboard-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="/hix/resources/css/bootstrap-responsive.css">
<link href="/hix/resources/css/portal-global.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/emp-portal.css" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ajaxfileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>

<script type="text/javascript">
	function shiftbox(element, nextelement) {
		maxlength = parseInt(element.getAttribute('maxlength'));
		if (element.value.length == maxlength) {
			nextelement = document.getElementById(nextelement);
			nextelement.focus();
		}
	}

	/* function validateHomeAddress() {
		var enteredAddress = $("#address1").val() + "," + $("#address2").val() + "," + $("#city").val() + 
							 "," + $("#state").val() + "," + $("#zip").val() + ", " +  $("#lat").val() + "," + $("#lon").val();
		var idsText = 'address1~address2~city~state~zip~lat~lon';
		$.ajax({
					url : "<c:url value='/platform/validateaddress'/>",
					data : {
						enteredAddress : enteredAddress,
						ids : idsText
					},
					success : function(data) {
						var href = "<c:url value='/platform/address/viewvalidaddress'/>";
						$('#addressIFrame').remove(); //remove any present modal 
						if (data == "SUCCESS") {
							$(
									'<div class="modal" id="addressIFrame" tabindex="-1" role="dialog"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="'
											+ href
											+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"></div></div>')
									.modal({
										backdrop : false
									});
							$('#iFrameClose').bind("click",
									submitFrmEditEmployee);
						} else {
							submitFrmEditEmployee();
						}
					}
				});
	} */
	
	function submitFrmEditEmployee() {
		$("#frmeditmyinfo").submit();
	}
	
	function validateForm() {
		if ($("#frmeditmyinfo").validate().form()) {
			var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
			$("#contactNumber").val(phone);
			submitFrmEditEmployee();
		}
	}
	
	function postPopulateIndex(zipId,zipCodeValue){
		//console.log(indexValue, zipCodeValue, zipId);
		//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
		jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
		getCountyList( zipCodeValue, '' );
	}
	

	
	function getCountyList(zip, county) {
		var subUrl = '<c:url value="/shop/employee/addCounties">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';

		$.ajax({
			type : "POST",
			url : subUrl,
			data : {zipCode : zip}, 
			dataType:'json',
			success : function(response,xhr) {
			if(isInvalidCSRFToken(xhr))	    				
				return;
			populateCounties(response, county);
			},error : function(e) {
				alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
			}
		});
	}

	function populateCounties(response, county) {
        var optionsstring = '<option value="">Select County...</option>';
			var i =0;
			$.each(response, function(key, value) {
				var selected = (county == key) ? 'selected' : '';
				var optionVal = key+'#'+value;
				var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
				optionsstring = optionsstring + options;
				i++;
			});
			$('#county').html(optionsstring);
	} // end populateCounties
</script>


<div class="gutter10">
  <h1>Welcome Back,&nbsp;<span id="user-name">${employeeApplication.name}</span></h1>
  <div class="row-fluid">
    <div class="span3" id="sidebar">
      <util:myStuff employeeApplication="${employeeApplication}"></util:myStuff>
      <util:quickLinks employeeApplication="${employeeApplication}"></util:quickLinks>
    </div><!--/span3-->
    <div class="span9" id="rightpanel">      
      <div class="header">
      	<h4 class="pull-left">My Information</h4>
        	<button type="submit" id="save" class="btn btn-small btn-primary pull-right" onClick="javascript:validateForm();" aria-describedby="description-save">Save</button>
        	<a href="<c:url value='/shop/employee/spplanselection/myinfo/${employeeApplication.id}'/>" id="cancel" class="btn btn-small pull-right" aria-describedby="description-cancel">Cancel</a>
      </div>   	
        <div class="gutter10">
          <div class="row-fluid">
            <p>The following information was entered by your employer and verified by you.</p>
            
			<form class="form-horizontal addressValidator"  id="frmeditmyinfo" name="frmeditmyinfo" action="<c:url value='/shop/employee/spplanselection/editmyinfo'/>" method="post" >
              <df:csrfToken/>
				 <div class="addressBlock">
				 		<div class="row-fluid">
					<input type="hidden" name="contactNumber" id="contactNumber"/>
                	<input type="hidden" name="id" id="id" value="${empDetailsId}" />
                	<input type="hidden" name="location.id" id="location_id" value="${empDetailsApplication.location.id}" />
                	<input type="hidden" value="0.0" id="lat" name="lat" tabindex="0">
					<input type="hidden" value="0.0" id="lon" name="lon" tabindex="0">
					<input type="hidden" value="0.0" id="rdi" name="rdi" tabindex="0">
					<input type='hidden' name='applicationId' value='${employeeApplication.id}'/>
					<input type="hidden" name="frtDocument" id="frtDocument"/>
					  <div class="control-group">                     
                    	<label class="control-label" for="firstName"><spring:message  code='label.emplFirstName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                      	<div class="controls">
                       	<input type="text" name="firstName" id="firstName" tabindex="0" value="${empDetailsApplication.firstName}">
                       	<span id="firstName_error"></span>
                          <input type="hidden" name="firstName_hidden" id="firstName_hidden" tabindex="0"> 
                    	</div>
	                 </div>
	                 <div class="control-group">                     
                    	<label class="control-label" for="lastName"><spring:message  code='label.emplLastName'/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                      	<div class="controls">
                       	<input type="text" name="lastName" id="lastName" tabindex="0" value="${empDetailsApplication.lastName}">
                       	<span id="lastName_error"></span>
                          <input type="hidden" name="lastName_hidden" id="lastName_hidden" tabindex="0"> 
                    	</div>
	                 </div>
	                 <div class="control-group"> 
						   <label for="suffix" class="control-label"><spring:message  code="label.suffix"/></label>                 
							<div class="controls">
								<select class="input-medium" name="suffix" id="suffix">
									<option value=""></option>
									<c:forEach var="suffix" items="${suffixlist}">
										<option id="${suffix}" <c:if test="${suffix == empDetailsApplication.suffix}"> SELECTED </c:if>
										value="${suffix}">${suffix}</option>
									</c:forEach>
								</select>
								 <span id="suffix_error"></span>
							</div>
						</div>
                      <div class="control-group">                     
                        	<label class="control-label" for="address1"><spring:message  code="label.homeAddress1"/><img width="" height="" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
                          	<div class="controls">
	                          	<input type="text" placeholder="Address Line 1" name="location.address1" id="address1" tabindex="0" value="${empDetailsApplication.location.address1}">
	                          	<span id="address1_error"></span>
	                             <input type="hidden" name="address1_hidden" id="address1_hidden" tabindex="0"> 
                        	</div>
                      </div>
                      <div class="control-group">
                        <label class="control-label"  for="address2"><spring:message  code="label.homeAddress2"/> </label>
                        <div class="controls">
                          <input type="text" placeholder="Address Line 2" name="location.address2" id="address2" tabindex="0" value="${empDetailsApplication.location.address2}">
                          <span id="address2_error"></span>
                            <input type="hidden" name="address2_hidden" id="address2_hidden" tabindex="0"> 
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label" for="city"><spring:message  code='label.emplCity'/>  <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
                        <div class="controls">
                          <input type="text" placeholder="City" name="location.city" id="city" tabindex="0" aria-required="true" value="${empDetailsApplication.location.city}">
                          <span id="city_error"></span>
                           <input type="hidden" name="city_hidden" id="city_hidden" tabindex="0"> 
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label" for="state"><spring:message  code='label.emplState'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!"/> </label>
                        <div class="controls">
                          <select id="state" name="location.state" class="input-medium" tabindex="0" aria-required="true">
								 <option value="">Select</option>
							<c:forEach var="state" items="${statelist}">
							<option id="${state.code}" <c:if test="${state.code == empDetailsApplication.location.state}"> SELECTED </c:if>
								value="${state.code}">${state.name}</option>
							</c:forEach>
  						 </select>
							<span id="state_error"></span>
							<input type="hidden" name="state_hidden" id="state_hidden" tabindex="0"> 
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label" for="zip"><spring:message  code='label.zip'/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
                        <div class="controls">
                            <input type="text" maxlength="5" id="zip" name="location.zip" value="${empDetailsApplication.location.zip}" placeholder="Zip Code" class="input-small zipCode" tabindex="0" aria-required="true">
                      	    <span id="zip_error"></span>
                      	     <input type="hidden" name="zip_hidden" id="zip_hidden" value="0.0" tabindex="0"> 
                        </div>
                      </div>
                      <div class="control-group">
                        <label class="control-label" for="county"><spring:message  code='label.county'/><img  src="<c:url  value="/resources/images/requiredAsterix.png"  />"  width="10"  height="10"  alt="Required!"/> </label>
                        <div class="controls">
                            <select size="1" class="input-large" name="location.county"  id="county"></select>
                            <span id="county_error"></span>
                        </div>
                      </div>
                      
                     <div class="control-group">
                    	<label class="control-label" for="gender"><spring:message  code="label.gender"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                   		<div id="gender" class="controls">
                      		<label class="radio inline"><input aria-required="true" type="radio" id="genderMale" name="gender" ${fn:contains(empDetailsApplication.gender, 'MALE') ? 'checked="checked"' : ''} value="MALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.male"/> </label>
			                <label class="radio inline"><input aria-required="true" type="radio" id="genderFemale" name="gender" ${fn:contains(empDetailsApplication.gender, 'FEMALE') ? 'checked="checked"' : ''}  value="FEMALE" /><span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.female"/> </label>
			                <span id="genderMale_error"></span> 
			                <span id="genderFemale_error"></span><br>
						</div>
                    </div>
                    <div class="control-group">
                    	<label class="control-label" for="marital_status"><spring:message  code="label.martialstatus"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                    	<div class="controls">
                     		<select class="input-xlarge" name="maritalStatus" id="maritalStatus">
								<option value="">Select</option>
								<c:forEach var="martialstatus" items="${martialstatuslist}">
									<option id="${martialstatus.code}" <c:if test="${martialstatus.code == empDetailsApplication.maritalStatus}"> SELECTED </c:if>
									value="${martialstatus.code}">${martialstatus.name}</option>
								</c:forEach>
							</select>
	                    	<span id="maritalStatus_error"></span>
	                    </div>
                    </div>
                    </div><!-- .row-fluid -->
                   
                    <dl class="dl-horizontal">
						<dt>Date of Birth</dt>
						<dd id="d-o-b">
							<c:choose>
			               	  	<c:when test="${empDetailsApplication.dob == null || empDetailsApplication.dob ==''}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
									<fmt:formatDate pattern="MM/dd/yyyy" value="${empDetailsApplication.dob}" var="dob" />
			               			<strong>${dob}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<p></p>
						<dt>Tax ID Number</dt>
						<dd id="tax-id">
							<c:choose>
			               	  	<c:when test="${empDetailsApplication.ssn == null || empDetailsApplication.ssn ==''}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
									<c:set var="ssnParts" value="${fn:split(empDetailsApplication.ssn,'-')}" />
									***-**-${ssnParts[2]}
			               		</c:otherwise>
							</c:choose>
						</dd>
					</dl>
					<div class="control-group">
                        <label class="control-label" for="email">Email Address <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
                        <div class="controls">
                            <input type="text" id="email" name="email" value="${empDetailsApplication.email}" class="input-xlarge" tabindex="0" aria-required="true">
                      	    <span id="email_error"></span>
                      	     <input type="hidden" name="email_hidden" id="email_hidden"  tabindex="0"> 
                        </div>
                    </div>
                    
                    
				 	<div class="control-group">
                        <label class="control-label" for="phone">Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> </label>
                        <div class="controls">
                        	<c:choose>
							<c:when
								test="${empDetailsApplication.contactNumber != null && empDetailsApplication.contactNumber != ''}">
								<c:set var="phone" value="${empDetailsApplication.contactNumber}" />
								<c:set var="phoneParts" value="${fn:split(phone,'-')}" />

								<c:set var="phone1" value="${phoneParts[0]}" />
								<c:set var="phone2" value="${phoneParts[1]}" />
								<c:set var="phone3" value="${phoneParts[2]}" />
							</c:when>
							</c:choose>
							<input type="text" class="span2 inline" value="${phone1}" name="phone1" id="phone1" maxlength="3" onkeyup="shiftbox(this, 'phone2')">
                        	<input type="text" class="span2 inline" value="${phone2}" name="phone2" id="phone2" maxlength="3" onkeyup="shiftbox(this, 'phone3')">
                        	<input type="text" class="span2 inline" value="${phone3}" name="phone3" id="phone3" maxlength="4">
                            <!-- <input type="text" id="phone" name="phone" class="input-xlarge" tabindex="0" aria-required="true"> -->
                      	    <span id="phone3_error"></span>
                        </div>
                    </div>
                    
                    <dl class="dl-horizontal">
	                    <dt>Tobacco User</dt>
						<dd id="Tobacco-user">
							<c:choose>
			               	  	<c:when test="${empDetailsApplication.smoker == null}">
									<weak>N/A</weak>
								</c:when>
								<c:otherwise>
			               			<strong>${fn:toUpperCase(fn:substring(empDetailsApplication.smoker, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.smoker, 1, -1))}</strong>
			               		</c:otherwise>
							</c:choose>
						</dd>
						<dt><spring:message  code="label.tobaccoCessationPrg"/></dt>
						<dd>
						<c:choose>
						    <c:when test="${empDetailsApplication.tobaccoCessPrg == null || empDetailsApplication.tobaccoCessPrg == ''}">
						       <weak>N/A</weak>
						    </c:when>
						    <c:otherwise>
						       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.tobaccoCessPrg, 1, -1))}</strong>
						    </c:otherwise>
					    </c:choose>
						</dd>
						<dt style="white-space: normal;">Member of Federally Recognized Tribe</dt>
						<dd id="fedral-tribe" style="height: 40px;">
							<c:choose>
							    <c:when test="${empDetailsApplication.nativeAmr == null}">
							       <weak>N/A</weak>
							    </c:when>
							    <c:otherwise>
							       <strong>${fn:toUpperCase(fn:substring(empDetailsApplication.nativeAmr, 0, 1))}${fn:toLowerCase(fn:substring(empDetailsApplication.nativeAmr, 1, -1))}</strong>
							    </c:otherwise>
						</c:choose>
						</dd>
					
						
						<dt>Worksite</dt>
						<dd id="worksite_myinfo"><address>
                  			 ${employee.employerLocation.location.address1} <br>
                  			 ${employee.employerLocation.location.address2} ${(empty employee.employerLocation.location.address2) ? '' : '<br>'} 
			                 ${employee.employerLocation.location.city}, 
			                 ${employee.employerLocation.location.state} &nbsp;
			                 ${employee.employerLocation.location.zip}<br>
			                 ${fn:toUpperCase(fn:substring(employee.employerLocation.location.county, 0, 1))}${fn:toLowerCase(fn:substring(employee.employerLocation.location.county, 1, -1))}
                  		</address></dd>
						<%-- <dt>Estimated Annual Salary</dt>
						<dd id="annum-sal">
							<fmt:formatNumber value="${employee.yearlyIncome}" var="incomeNumber" type="number"  groupingUsed="false" maxFractionDigits="2"/>
                			<fmt:formatNumber value="${incomeNumber}" var="incomeNumber" type="currency" currencySymbol="$"/>
							${incomeNumber}
						</dd>  --%>
					</dl>
                    
				 </div>
				 		<div class="control-group"> 
			  <h4 id="file_upload_container_msg" style="display:none;"><spring:message  code="label.uploadsupportingdocument"/></h4>
			  <%-- <img id = "imgDiv" style="display:none;" src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /> --%>           
				<div id="file_upload_container_box" style="display:none;">
            	  <div id="file_upload_container">
						<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;	
				  </div>
					 
					<div id="file_upload_error" class="error" style="display:none;">File could not be uploaded. Selected file name already exists on the server.</div>
					<div id="uploaddiv" style="display:none;">Uploading..... Please wait</div>
      					<div id="deletediv" style="display:none;">Deleting..... Please wait</div>
      					<span id="frtDocument_error" style="display:none;"></span>
	                <!-- The table listing the files available for attaching/removing -->
    							<div id="" class="files" >
    								<table id="upload_files_container"></table>
    							</div>
			</div>
			</div>	
			</form>
			
		  </div>
				
          </div>
    </div><!--/rightpanel--> 
  </div><!--/row-fluid-->        
</div><!--/main .gutter10-->

<c:if test="${isEnrollmentEnded }">
<jsp:include page="../terminateemployercoverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
	<jsp:param name="nativeAmr" value="${nativeAmr}" />
</jsp:include>  
</c:if>
<c:if test="${!isEnrollmentEnded }">
<jsp:include page="../waive_employer_coverage.jsp" >
	<jsp:param name="employeeApplicationId" value="${employeeApplication.id}" />
</jsp:include>
</c:if>  

<script>

var uploadedDocId;
var input = $('#fileupload');
var fileDetails;
var rowno=0;
var responseText="";

function setAjaxPlugin(){
	$('#fileupload').ajaxfileupload({
		
	    'action': '<c:url value="/shop/employee/spplanselection/uploadFRTDocument"/>',
	    'onComplete': function(response) {
	    	responseText=response;
	    	 if(response.status == false){       
	    	      alert(response.message);
    	      }
	    	 else if(responseText.indexOf("Error") != -1){
	    		 errorDetails=responseText.split('#');
	    		 alert(errorDetails[1]);
	    	 }
	    	 else if(responseText){
	    		fileDetails=responseText.split('#');
	    		if(fileDetails.length == 2){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	$('#frtDocument').val(uploadedDocId);
	            	rowno++;
	            	appendFileDetails();	
	            	$( "#fileupload" ).hide();
	            	$("#frtDocument_error").hide();
	            	$("#file_upload_error").hide();
	        	}
	        	else{
	        		$('#file_upload_error').show();
	        	}
	    	}
	    	else{
	    		$('#file_upload_error').show();
	    	}
	    	
	    	$('#uploaddiv').hide();
	    },
	    'onStart': function() {
	    	$('#uploaddiv').show();
	    	$('#file_upload_error').hide();
	    },
	    'onCancel': function() {
	      
	    },
	    'valid_extensions' : ['gif','png','jpg','jpeg','pdf']
	  });
}

	function showFrtDocument(){
		var frtdocumentdetails = '${frtdocumentdetails}';
		
		fileDetails=frtdocumentdetails.split('#');
		if(fileDetails.length == 2){
    		uploadedDocId=fileDetails[0];
        	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
        	$('#frtDocument').val(uploadedDocId);
        	rowno++;
        	appendFileDetails();		
        	$( "#fileupload" ).hide();
    	}
    	 
	}

	function deleteFile(fileId,rowid){
		$('#file_upload_error').hide();
		$('#deletediv').show();
		var subUrl = '<c:url value="/shop/employee/spplanselection/removeFRTDocument">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {uploadedDocId : fileId}, 
			dataType:'text',
			success : function(response,xhr) {
				if(isInvalidCSRFToken(xhr))	    				
					return;
				var newText = '<input type="file" class="input-file" id="fileupload" onchange=""  name="files" />&nbsp;';
				$('#file_upload_container').html('');
				$('#file_upload_container').html(newText);
				 //$('#fileupload').replaceWith($('#fileupload').clone(true).val(''));
				 setAjaxPlugin();
				$('#frtDocument').val(null);
				$('#deletediv').hide();
				$('#upload_files_container').html("");
			},error : function(e) {
				alert('error in deleting document.');
				$('#deletediv').hide();
			}
		});
	}
	
	function appendFileDetails(){
		orgHtml=$('#upload_files_container').html();
		//strHtml=orgHtml+'<tr id="' +rowno  +'">'+
		strHtml='<tr id="' +rowno  +'">'+
		'<td colspan="1">'+fileDetails[1]+'</td>'+
		'<td colspan="1"></td>'+
		'<td colspan="1"><a class="btn" href="javascript:downloadFile(\''+uploadedDocId+'\')">download</a></td>'+
		'<td colspan="1"><a class="btn btn-danger" href="javascript:deleteFile(\''+uploadedDocId+'\','+rowno+')">delete</a></td>'
		+'</tr>';
		$('#upload_files_container').html(strHtml);
		 
	}
	
	function downloadFile(fileId){
		var  contextPath =  "<%=request.getContextPath()%>";

		var url = contextPath+"/shop/employee/spplanselection/downloadDocument?udId="+fileId;
		var subUrl =  '<c:url value="'+url+'"/>';
		window.open(subUrl,"Supported Document");
	}
	
	function show_file_upload_container(frtDocumentRequired){
		  $("#file_upload_container_msg").show(); 
		  $("#file_upload_container_box").show();		
		  /* if(frtDocumentRequired == 'YES'){
			  $("#imgDiv").show();
		  } else{
			  $("#imgDiv").hide();
		  } */
	}
	
$(".inactive").click(function(e) {
	e.preventDefault();
});

$(document).ready(function(){
	$('.info').tooltip();
	var zip = '${empDetailsApplication.location.zip}';
	var county = '${empDetailsApplication.location.county}';
	
	getCountyList(zip, county);
	setAjaxPlugin();
	showFrtDocument();
	var isfrt = '${empDetailsApplication.nativeAmr}';
	var frtDocumentRequired = '${frtDocumentRequired}';
	if(isfrt == 'YES'){
		show_file_upload_container(frtDocumentRequired);
	} 
});


jQuery.validator.addMethod("namecheck", function(value, element, param) {
  	if(namecheck(value) == false){
	  	return false;	
	}
	return true;
});

	jQuery.validator.addMethod("frtdoccheck", function(value, element, param) {
		$('#frtDocument_error').html("");
		var fileName = $('#frtDocument').val();
		var isfrt = '${empDetailsApplication.nativeAmr}';
		var frtDocumentRequired = '${frtDocumentRequired}';
		if(isfrt == 'YES' && frtDocumentRequired == 'YES' && (fileName === undefined || fileName  == '' ) ) {
			$('#frtDocument_error').show();
			return false; 
		}
		return true;
	});

var validator = $("#frmeditmyinfo")
.validate(
		{
			ignore: ':hidden:not("#frtDocument")',
			onkeyup : false,
			onclick : false,
			onfocusout : false,
			onSubmit : true,
			rules : {
				firstName : { required : true,namecheck : true},
				lastName : { required : true,namecheck : true},
				email : { required : true, email : true },
				phone3 : { phonecheck : true },
				'location.address1' : { required : true , address1Check:true},
				'location.address2' :{address2Check:true, address2Match:true},
				'location.city' : { required : true },
				'location.state' : { required : true },
				'location.zip' : { required : true, zipcheck : true },
				'location.county' : { required : true },
				gender : {required: true},
				frtDocument : {frtdoccheck : true}
			},
			messages : {
				firstName : {
					required: "<span><em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>",
					namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>"	
				},
				lastName : {
					required : "<span><em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>",
					namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>"	
				},
				email : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateemailadr' javaScriptEscape='true'/></span>",
					email : "<span> <em class='excl'>!</em><spring:message code='label.validateemail' javaScriptEscape='true'/></span>"
				},
				phone3 : {
					phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvtelephone' javaScriptEscape='true'/></span>"
				},
				'location.address1' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>",
					address1Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>"
				},
				'location.address2' :  {
					address2Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>",
					address2Match: "<span> <em class='excl'>!</em><spring:message code='label.validateaddressline12' javaScriptEscape='true'/></span>"
				},
				'location.city' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"
				},
				'location.state' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"
				},
				'location.zip' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
					zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
				},
				'location.county' : {
					required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
				},
				gender : {
					required : "<span><em class='excl'>!</em><spring:message code='label.validategender' javaScriptEscape='true'/></span>"
				},
				frtDocument :{
					frtdoccheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefile' javaScriptEscape='true'/></span>"
				}
			},
			errorClass : "error",
			errorPlacement : function(error, element) {
				var elementId = element.attr('id');
				error.appendTo($("#" + elementId + "_error"));
				$("#" + elementId + "_error").attr('class', 'error span10');
			}
		});
		
/*focus out event for the text input to call the lightbox*/
jQuery.validator.addMethod("phonecheck",function(value, element, param) {
	phone1 = $("#phone1").val();
	phone2 = $("#phone2").val();
	phone3 = $("#phone3").val();

	if ((phone1 == "" || phone2 == "" || phone3 == "") || 
		(isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3)) || 
		(phone1.length != 3 || phone2.length != 3 || phone3.length != 4)) 
	{
		return false;
	}
	var phone = $("#phone1").val() + "-" + $("#phone2").val() + "-" + $("#phone3").val();
	$("#contactnumber").val(phone);
	return true;
});

jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
  	var numzip = new Number($("#"+elementId).val());
    var filter = /^[0-9]+$/;
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) || numzip<=0 || (!filter.test($("#"+elementId).val()))){
  		return false;	
  	}
	return true;
});
jQuery.validator.addMethod("address1Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
	if(!regex.test($("#"+elementId).val()) ){
  		return false;	
  	}
	return true;
});
jQuery.validator.addMethod("address2Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
  	if($("#"+elementId).val()!=""){
		if(!regex.test($("#"+elementId).val()) ){
	  		return false;	
	  	}
	}
	return true;
});
jQuery.validator.addMethod("address2Match", function(value, element, param) {
  	elementId = element.id;	  	
  	if($.trim($("#"+elementId).val())!="" && $.trim($("#address1").val())!=""){	  		  	
  		if($("#"+elementId).val() == $("#address1").val()){
	  		return false;	
		}
  	}
	return true;
});	
/* $('.zipCode').focusout(function(e) {
	
	var address1_e='address1'; var address2_e='address2'; var city_e= 'city'; var state_e='state'; var zip_e='zip';
	var lat_e='lat';var lon_e='lon'; var rdi_e='rdi'; var county_e='county';
	
	var model_address1 = address1_e + '_hidden' ;
	var model_address2 = address2_e + '_hidden' ;
	var model_city = city_e + '_hidden' ;
	var model_state = state_e + '_hidden' ;
	var model_zip = zip_e + '_hidden' ;
	getCountyList($('#'+ zip_e).val(), '');
	
	var idsText=address1_e+'~'+address2_e+'~'+city_e+'~'+state_e+'~'+zip_e+'~'+lat_e+'~'+lon_e+'~'+rdi_e+'~'+county_e;
	if(($('#'+ address1_e).val() != "Address Line 1")&&($('#'+ city_e).val() != "City")&&($('#'+ state_e).val() !="State")&&($('#'+ zip_e).val() != "Zip Code")){
		if(($('#'+ address2_e).val())==="Address Line 2"){
			$('#'+ address2_e).val('');
		}	
		viewValidAddressListNew($('#'+ address1_e).val(),$('#'+ address2_e).val(),$('#'+ city_e).val(),$('#'+ state_e).val(),$('#'+ zip_e).val(), 
			$('#'+ model_address1).val(),$('#'+ model_address2).val(),$('#'+ model_city).val(),$('#'+ model_state).val(),$('#'+ model_zip).val(),	
			idsText);	
	}
});
 */
</script>