<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!-- #sidebar -->     
<c:set var="employeeID" value="${sessionScope.employeeObjectForLeftNav.id}"/>

 <div class="span3" id="sidebar">  
	<div class="header"><h4><spring:message code="label.employeeDetails.AboutThisEmployee"/></h4></div>					
			<c:choose>
			<c:when test="${sessionScope.employeeObjectForLeftNav != null}">
			<ul class="nav nav-list">				
				<li id="emplSummary"><a href="/hix/broker/employeedetails/${employeeID}"><spring:message code="label.employeeDetails.Summary"/></a></li>								
				<li id="emplEnrollment"><a href="/hix/broker/employeeenrollment/${employeeID}"><spring:message code="label.employeeDetails.Enrollment"/></a></li>				
				<li id="emplComment"><a href="<c:url value="/broker/comments/${designatedBrokerProfile.id}/${employeeID}"/>"><spring:message code="label.employeeDetails.Comments"/></a></li>
			</ul>
				<br>
				
			<div class="header"><h4><i class="icon-gear icon-white"></i> <spring:message code="label.employeeDetails.Actions"/></h4></div>
			<ul class="nav nav-list">
			
			
			<c:choose>
				<c:when test="${objEmployee.status == 'DELETED'}">
					<li id="emplViewEmployeeAccount" class="navmenu"><a href="#employeeinactivedeletedpopup" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.employeeDetails.ViewEmployeeAccount"/></a></li>
				</c:when>
											
				<c:otherwise>
					<c:if test="${checkDilog == null}">  
              			<li id="emplViewEmployeeAccount" class="navmenu"><a href="#employeeViewDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.employeeDetails.ViewEmployeeAccount"/></a></li>
              		</c:if>
                       
              		<c:if test="${checkDilog != null}">   
                     
              			<c:forEach var="memeber" items="${listEmployeeDetails}">
							<c:if test="${memeber.type == 'EMPLOYEE'}" >
								<li id="emplViewEmployeeAccount"  class="navmenu"><a href="<%=request.getContextPath()%>/broker/employee/dashboard?switchToModuleName=employee&switchToModuleId=${employeeID}&switchToResourceName=${memeber.firstName}" role="button" data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.employeeDetails.ViewEmployeeAccount"/> </a></li>
							</c:if>
						</c:forEach>
                               
              		</c:if>
				</c:otherwise>
			</c:choose>
			  
			  
                
               <c:if test="${checkDilog == null}">  
               		<li id="emplViewEmployerAccount" class="navmenu"><a href="#employerSwitchDialog" role="button" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.employeeDetails.ViewEmployerAccount"/></a></li>
               </c:if>
               <c:if test="${checkDilog != null}">  
               		<li id="emplViewEmployerAccount" class="navmenu"><a href="<c:url value="/broker/employer/dashboard?switchToModuleName=employer&switchToModuleId=${employeeID}&switchToResourceName=${broker.user.firstName}"/>" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.employeeDetails.ViewEmployerAccount"/></a></li>
                </c:if>                           
				<li id="emplNewcomment"><a name="addComment" href="#" data-rel="<c:url value="/broker/employee/newcomment?target_id=${employeeID}&target_name=EMPLOYEE&employeeName=${sessionScope.employeeObjectForLeftNav.name}"/>" id="addComment" class="newcommentiframe"> 
						<i class="icon-comment"></i>
						<spring:message code="label.employeeDetails.NewComments"/>
				    </a>
				</li>
			</ul>		
			</c:when>
			<c:otherwise>				
			</c:otherwise>
		</c:choose>		
</div>



<div id="employeeinactivedeletedpopup" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="employeeinactivedeletedpopup" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"><spring:message code="label.employee.viewemployeeaccount"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  	<div class="modal-body">
    	<div class="control-group">	
				<div class="controls">
					 <spring:message code="label.employeeDetails.deleted.employee.message.part.one"/> ${exchangeName}
					 <spring:message code="label.employeeDetails.deleted.employee.message.part.two"/> ${exchangePhone}
					 <spring:message code="label.employeeDetails.deleted.employee.message.part.three"/>
				</div>
		</div>
  	</div>
  	<div class="modal-footer clearfix">
  		
    	<button class="btn btn" data-dismiss="modal" aria-hidden="true"> OK  </button>
  	</div>
</div>



<script type="text/javascript">

  $(document).ready(function(){
    $(".newcommentiframe").click(function(e){
        //console.log(e, $(this).attr('data-rel'));
        
    	e.preventDefault();
    	e.stopPropagation();
          var href= $(this).attr('data-rel');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal();
      }
    });
  });
  
  function setPopupValue() {
		if(document.getElementById("frmPopup").elements['empchkshowpopup'].checked) {
			document.frmPopup.showPopupInFuture.value = "N";
		}
  }
    
  
  function closeIFrame() {	  
	  $("#newcommentiframe, .modal-backdrop").remove();	 
	  $("#confirmdeletepopup").remove();	
  }
  function closeCommentBox()
  {	  	  
	  $("#newcommentiframe, .modal-backdrop").remove();	
	  var url = '<%=request.getContextPath()%>/broker/comments/${designatedBrokerProfile.id}/${employeeID}';
	  window.location.assign(url);
	  
  }
</script>