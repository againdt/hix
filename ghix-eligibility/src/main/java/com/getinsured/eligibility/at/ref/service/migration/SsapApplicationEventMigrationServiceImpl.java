package com.getinsured.eligibility.at.ref.service.migration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.getinsured.eligibility.at.ref.service.ReferralSEPService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeSimpleType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.EventPrecedenceIndicator;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.util.TSDate;

@Service("ssapApplicationEventMigrationService")
public class SsapApplicationEventMigrationServiceImpl implements SsapApplicationEventMigrationService {
	
	private static final Logger LOGGER = Logger.getLogger(SsapApplicationEventMigrationServiceImpl.class);

	private static final String CHANGE_PLAN = "Y";
	private static final String SEP_REMOVE_OTHER = "OTHER_ELIGIBILITY_CHANGE-HEALTH_STATUS_CHANGE";
	private static final String SEP_OTHER_EVENT = "OTHER";
	private static final String KEEP_ONLY_FALSE = "N";
	private static final String KEEP_ONLY_TRUE = CHANGE_PLAN;
	private static final String RELATIONSHIP_CHANGE = "RELATIONSHIP_CHANGE";
	private static final String AUTO_REMOVAL_WITHOUT_EVENT = "AUTO_REMOVAL_WITHOUT_EVENT";
	private static final String AUTO_CHANGE_IN_CSR_WITHOUT_EVENT = "AUTO_CHANGE_IN_CSR_WITHOUT_EVENT";
	private static final String INCOME_CHANGE_CSR_EVENT = "INCOME_CHANGE_CSR";

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralSEPService")
	private ReferralSEPService referralSEPService;

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private static Map<String, SepEvents> sepEventsOther;
	private static Map<String, SepEvents> sepRemoveEvents;
	private static Map<String, SepEvents> sepAddEvents;

	private static int enrollmentGracePeriod;

	private static int numberOfenrollmentDays = 60;

	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;
	@Autowired
	private MigrationUtil migrationUtil;
	
	public static Map<String, SepEvents> getSepRemoveEvents() {
		return sepRemoveEvents;
	}
	
	public static Map<String, SepEvents> getSepAddEvents() {
		return sepAddEvents;
	}
	
	public static Map<String, SepEvents> getSepOtherEvents() {
		return sepEventsOther;
	}

	@PostConstruct
	public void doPost() {
		sepEventsOther = sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
		sepRemoveEvents = sepEventsService.findFinancialSepEventsByChangeType(Events.REMOVE.name());
		sepAddEvents = sepEventsService.findFinancialSepEventsByChangeType(Events.ADD.name());
		enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (!StringUtils.isEmpty(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
	}

	@Override
	public SsapApplicationEvent setChangePlan(Long ssapApplicationId) {
		SsapApplicationEvent ssapApplicationEvent = getSsapApplicationEventsByApplicationId(ssapApplicationId);
		ssapApplicationEvent.setChangePlan(CHANGE_PLAN);
		return ssapApplicationEventRepository.save(ssapApplicationEvent);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = today with time as 00:00:00 and 3. Enrollment End Date = today + (60 + buffer) days with time as 23:59:59 3. Keep Only to Y
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today Enrollment Start Date as today Enrollment End Date = today + (60 + buffer) days
	 */
	@Override
	public SsapApplicationEvent createDoBApplicationEvent(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		
		Timestamp todayTimestamp;
		DateTime endDate;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplicationId);
		if(activityDate!=null){
			todayTimestamp = activityDate;
			endDate =  new DateTime(calculateEndDate(new Date(activityDate.getTime())));
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
			endDate = new DateTime(calculateEndDate(new TSDate())).withTime(23, 59, 59, 999);
		}

		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			// if (ReferralConstants.DOB_CHANGED.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) || ReferralConstants.NO_EFFECT.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
			if (!ReferralConstants.NON_DEMO_DOB_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvent.setEnrollmentStartDate(todayTimestamp);
				applicantEvent.setEnrollmentEndDate(calculateEndDate(new Date(activityDate.getTime())));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = null and 3. Enrollment End Date = null 3. Keep Only to N
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today Enrollment Start Date as today Enrollment End Date = today + (60 + buffer) days
	 */
	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEvent(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		Date todayDate;
		Timestamp todayTimestamp;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplicationId);
		if(activityDate!=null){
			todayTimestamp = activityDate;
			todayDate = todayTimestamp;
		} else {
			todayDate = new TSDate();
			todayTimestamp = new Timestamp(new TSDate().getTime());
		}

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if ("NONE".equals(applicant.getEligibilityStatus())) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvent.setEnrollmentStartDate(todayTimestamp);
				applicantEvent.setEnrollmentEndDate(calculateEndDate(todayDate));

				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createDuplicateOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId) {
		return createDemoOnlyApplicationEvent(ssapApplicationId, enrolledApplicationId);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Copy Enrollment Start Date and Enrollment End Date from previous application 3. Keep Only to N
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today
	 */
	@Override
	public SsapApplicationEvent createDemoOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		if (enrolledApplicationId == null || enrolledApplicationId == 0) {
			throw new GIRuntimeException("enrolledApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}
		final SsapApplicationEvent enrolledEvent = getSsapApplicationEventsByApplicationId(enrolledApplicationId);
		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		ssapApplicationEvent.setCoverageStartDate(null);
		ssapApplicationEvent.setEnrollmentStartDate(enrolledEvent != null ? enrolledEvent.getEnrollmentStartDate() : null);
		ssapApplicationEvent.setEnrollmentEndDate(enrolledEvent != null ? enrolledEvent.getEnrollmentEndDate() : null);
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		Timestamp todayTimestamp;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplicationId);
		if(activityDate!=null){
			todayTimestamp = activityDate;
		} else {
			todayTimestamp = new Timestamp(new TSDate().getTime());
		}
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) || ReferralConstants.NO_EFFECT.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent getSsapApplicationEventsByApplicationId(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplicationEvent> applicationEvents = ssapApplicationEventRepository.findEventBySsapApplicationId(ssapApplicationId);
		return (applicationEvents != null && applicationEvents.size() > 0) ? applicationEvents.get(0) : null;
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = today with time as 00:00:00 and 3. Enrollment End Date = passed sepEndDate with time as 23:59:59 4. Keep Only to N
	 * 
	 * 
	 * Create Events for all members based on applicantEventMap
	 */
	@Override
	public SsapApplicationEvent createApplication_Applicant_SepEvents(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date eventDate, Date sepEndDate, Integer createdBy, boolean keepOnly) {

		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(ssapApplicationId, applicantEventMap, eventDate, sepEndDate, createdBy, SsapApplicationEventTypeEnum.SEP, keepOnly);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createApplication_Applicant_QepEvents(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date eventDate, Date sepEndDate, Integer createdBy) {

		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(ssapApplicationId, applicantEventMap, eventDate, sepEndDate, createdBy, SsapApplicationEventTypeEnum.QEP, false);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createAptcOnlyApplicationEvent(SsapApplication ssapApplication, SepEvents event, Date applicantEventDate, Date applicantReportDate) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		DateTime endDate = new DateTime(applicantReportDate.getTime()).withTime(23, 59, 59, 999);

		ssapApplicationEvent.setEnrollmentStartDate(new Timestamp(applicantReportDate.getTime()));
		ssapApplicationEvent.setEnrollmentEndDate(calculateEndDate(new Date(endDate.getMillis())));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_APTC_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(event);
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(new Timestamp(applicantReportDate.getTime()));
				applicantEvent.setEventDate(new Timestamp(applicantEventDate.getTime()));
				applicantEvent.setEnrollmentStartDate(new Timestamp(applicantReportDate.getTime()));
				applicantEvent.setEnrollmentEndDate(calculateEndDate(new Date(endDate.getMillis())));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	private SsapApplicationEvent createApplicationAndApplicantEvent(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date eventDate, Date sepEndDate, Integer createdBy, SsapApplicationEventTypeEnum eventType, boolean keepOnly) {
		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(createdBy);
		
		
		DateTime endDate = new DateTime(sepEndDate.getTime()).withTime(23, 59, 59, 999);
		ssapApplicationEvent.setEnrollmentStartDate(new Timestamp(eventDate.getTime()));
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(eventType);
		ssapApplicationEvent.setKeepOnly(keepOnly ? KEEP_ONLY_TRUE : KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = applicantEventMap.get(applicant.getApplicantGuid());
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			applicantEvent.setCreatedBy(createdBy);

			applicantEvents.add(applicantEvent);
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEvent;

	}

	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEvent(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantEventMap) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ApplicantEvent payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplication.getId());

		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantEventMap.get(applicant.getApplicantGuid());
			SepEvents sepEvent = null;
			if (ReferralConstants.Y.equalsIgnoreCase(applicant.getOnApplication())) {
				if (null == payloadEvent) {
					sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
					if(activityDate!=null){
						applicantEvent.setEventDate(activityDate);
					} else {
						applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
					}
				} else {
					sepEvent = sepRemoveEvents.get(ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.getCode()).name());
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
			} else {
				if (null == payloadEvent) {
					sepEvent = sepRemoveEvents.get(SEP_REMOVE_OTHER);
					if(activityDate!=null){
						applicantEvent.setEventDate(activityDate);
					} else {
						applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
					}
				} else {
					sepEvent = sepRemoveEvents.get(ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.getCode()).name());
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
			}
			applicantEvent.setSepEvents(sepEvent);
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate()));
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		
		DateTime eventDate;
		Timestamp eventTimestamp;
		if(activityDate!=null){
			eventTimestamp = activityDate;
			eventDate = new DateTime(new Date(activityDate.getTime()));
		} else {
			eventDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			eventTimestamp = new Timestamp(eventDate.getMillis());
		}
		
		ssapApplicationEvent.setEnrollmentStartDate(eventTimestamp);
		Timestamp enrollmentEndDate = calculateEndDate(eventDate.withTime(23, 59, 59, 999).toDate());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEventForMixedAndNone(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantEventMap) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		
		Timestamp eventTimestamp;
		DateTime eventDate;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplication.getId());
		if(activityDate!=null){
			eventTimestamp = activityDate;
			eventDate = new DateTime(new Date(activityDate.getTime()));
		} else {
			eventDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			eventTimestamp = new Timestamp(eventDate.getMillis());
		}

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);

			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			if(activityDate!=null){
				applicantEvent.setEventDate(activityDate);
			} else {
				applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
			}
			applicantEvent.setSepEvents(sepEvent);
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate()));
			applicantEvents.add(applicantEvent);
		}

		ssapApplicationEvent.setEnrollmentStartDate(eventTimestamp);
		Timestamp enrollmentEndDate = calculateEndDate(eventDate.withTime(23, 59, 59, 999).toDate());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	private Timestamp calculateEndDate(Date currDate) {
		DateTime endDate = new DateTime(currDate.getTime());
		endDate = endDate.plusDays(enrollmentGracePeriod + numberOfenrollmentDays);
		return new Timestamp(endDate.getMillis());
	}

	@Override
	public SsapApplicationEvent createCitizenshipApplicationEvent(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantExtensionEvent) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		Timestamp todayTimestamp;
		DateTime endDate;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplication.getId());
		if(activityDate!=null){
			todayTimestamp = activityDate;
			endDate = new DateTime(calculateEndDate(new Date(activityDate.getTime())));
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
			endDate = new DateTime(calculateEndDate(new TSDate())).withTime(23, 59, 59, 999);
		}

		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setChangePlan(ReferralConstants.N);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		ApplicantEvent payloadEvent;
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_CITIZEN_SHIP_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				if (ReferralConstants.CITIZEN_SHIP_CHANGED.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
					payloadEvent = applicantExtensionEvent.get(applicant.getApplicantGuid());
				} else {
					payloadEvent = null;
				}
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				if (null == payloadEvent) {
					if(activityDate!=null){
						applicantEvent.setReportStartDate(activityDate);
						applicantEvent.setEventDate(activityDate);
					} else {
						applicantEvent.setReportStartDate(new Timestamp(new TSDate().getTime()));
						applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
					}
				} else {
					applicantEvent.setReportStartDate(new Timestamp(payloadEvent.getReportDate().getTime()));
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				if(activityDate!=null){
					applicantEvent.setEnrollmentStartDate(activityDate);
					applicantEvent.setEnrollmentEndDate(calculateEndDate(activityDate));
				} else {
					applicantEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
					applicantEvent.setEnrollmentEndDate(calculateEndDate(new TSDate()));
				}
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createRelationshipApplicationEvent(SsapApplication ssapApplication) {

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		Timestamp todayTimestamp;
		DateTime endDate;
		Timestamp activityDate = migrationUtil.getActivityDate(ssapApplication.getId());
		if(activityDate!=null){
			todayTimestamp = activityDate;
			endDate = new DateTime(calculateEndDate(new Date(activityDate.getTime())));
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
			endDate = new DateTime(calculateEndDate(new TSDate())).withTime(23, 59, 59, 999);
		}
		
		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_RELATIONSHIP_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(RELATIONSHIP_CHANGE));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvent.setEnrollmentStartDate(todayTimestamp);
				if(activityDate!=null){
					applicantEvent.setEnrollmentEndDate(calculateEndDate(activityDate));
				} else {
					applicantEvent.setEnrollmentEndDate(calculateEndDate(new TSDate()));
				}
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean autoRemoveEvents) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		boolean blnKeepOnlyDontAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			
			Date eventDate;
			Date reportDate;
			if(activityDate!=null){
				eventDate = activityDate;
				reportDate = activityDate;
			} else {
				eventDate = new TSDate();
				reportDate = new TSDate();
			}
			
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}

			if (!blnKeepOnlyDontAllow && ReferralConstants.DONT_ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyDontAllow = true;
			}
			
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (nonQHPEventName != null) {
							eventCode = nonQHPEventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}

					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							reportDate = payloadEvent.get(0).getReportDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (autoRemoveEvents){
						if (SEP_OTHER_EVENT.equals(eventCode)){
							eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
						}
					}
					
					sepEvent = sepRemoveEvents.get(eventCode);
				} else {
					sepEvent = sepRemoveEvents.get(AUTO_REMOVAL_WITHOUT_EVENT);
				}
			}

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate()));
			applicationSepEnrollmentStartDates.add(applicantEvent.getEventDate());
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		Timestamp enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		Timestamp enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		if (blnKeepOnlyAllow && !blnKeepOnlyDontAllow) {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		} else {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		}
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		
	}
	
	@Override
	public SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		return createAutoApplicationEvents(currentApplication, applicantExtensionEvents, false);
	}

	private SsapApplicationEvent createAutoApplicationEvents(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean autoRemoveEvents) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		boolean blnKeepOnlyDontAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			Date eventDate;
			Date reportDate;
			if(activityDate!=null){
				eventDate = activityDate;
				reportDate = activityDate;
			} else {
				eventDate = new TSDate();
				reportDate = new TSDate();
			}
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}

			if (!blnKeepOnlyDontAllow && ReferralConstants.DONT_ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyDontAllow = true;
			}

			if (ReferralUtil.listSize(payloadEvent) != ReferralConstants.NONE && ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(statusEnum)) {
				ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
				eventCode = eventName.name();
				eventDate = payloadEvent.get(0).getEventDate();
				reportDate = payloadEvent.get(0).getReportDate();
				sepEvent = sepAddEvents.get(eventCode);
			} else if (ReferralUtil.listSize(payloadEvent) != 0 && ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)) {
				try {
					ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (nonQHPEventName != null) {
						eventCode = nonQHPEventName.name();
						eventDate = payloadEvent.get(0).getEventDate();
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
				}

				try {
					ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (eventName != null) {
						eventCode = eventName.name();
						eventDate = payloadEvent.get(0).getEventDate();
						reportDate = payloadEvent.get(0).getReportDate();
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
				}

				// TODO: handle AUTO_REMOVE_EVENT scenario
				if (autoRemoveEvents){
					if (SEP_OTHER_EVENT.equals(eventCode)){
						eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
					}
				}
				
				sepEvent = sepRemoveEvents.get(eventCode);
			}
			// For citizenship and other events
			else if (ReferralUtil.listSize(payloadEvent) != 0) {
				/* Handle custom logic for ChangeInAddress, 
				 * 		try to read from AT 
				 * or 	if fails continue with existing flow and create OTHER event */
				if (statusEnum == ApplicantStatusEnum.UPDATED_ZIP_COUNTY){
					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null && eventName == ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS) {
							eventCode = eventName.name();
							sepEvent = sepEventsOther.get(eventCode);
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}
				}
				eventDate = payloadEvent.get(0).getEventDate();
				reportDate = payloadEvent.get(0).getReportDate();
			}

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate()));
			applicationSepEnrollmentStartDates.add(applicantEvent.getEventDate());
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		Timestamp enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		Timestamp enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		if (blnKeepOnlyAllow && !blnKeepOnlyDontAllow) {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		} else {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		}
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createAndUpdateApplicationEventForNonFinancialQE(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan) {
		SsapApplicationEvent applicationEvent = null;

		applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getId());
		applicationEvent = applicationEvent != null ? updateSsapApplicationEvent(currentApplication, applicationEvent, eventType, changePlan) : createSsapApplicationEventForNonFinancial(currentApplication, eventType, changePlan);

		return applicationEvent;
	}

	@Override
	public SsapApplicationEvent updateSsapApplicationEventForNonFinancial(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan) {

		SsapApplicationEvent applicationEvent = null;
		SepEvents sepEvent = sepEventsOther.get("OTHER");
		
		Timestamp todayTimestamp;
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());
		if(activityDate!=null){
			todayTimestamp = activityDate;
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
		}
		
		applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getId());
		applicationEvent.setCreatedBy(null);
		applicationEvent.setEnrollmentStartDate(todayTimestamp);
		applicationEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp));
		applicationEvent.setEventType(eventType);
		applicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		applicationEvent.setChangePlan(changePlan);
		applicationEvent.setSsapApplication(currentApplication);
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
			ssapApplicantEvent.setEventDate(todayTimestamp);
			ssapApplicantEvent.setReportStartDate(todayTimestamp);
			ssapApplicantEvent.setSepEvents(sepEvent);
			ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
			ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp));
			ssapApplicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			ssapApplicantEvent.setSsapApplicant(applicant);
			ssapApplicantEvent.setSsapAplicationEvent(applicationEvent);
			applicantEvents.add(ssapApplicantEvent);
			ssapApplicantEventRepository.save(ssapApplicantEvent);
		}
		applicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(applicationEvent);
	}

	private SsapApplicationEvent createSsapApplicationEventForNonFinancial(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan) {

		Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();
		SepEvents sepEvent = sepEventsOther.get(INCOME_CHANGE_CSR_EVENT);
		
		Timestamp todayTimestamp;
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());
		if(activityDate!=null){
			todayTimestamp = activityDate;
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
		}
		
		SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
			ssapApplicantEvent.setEventDate(todayTimestamp);
			ssapApplicantEvent.setSepEvents(sepEvent);
			ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
			ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp));

			applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

			finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
		}
		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(currentApplication.getId(), finalApplicantEvents, null, applicationSepEndDates.last(), null, eventType, false);
		ssapApplicationEvent.setChangePlan(changePlan);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	private SsapApplicationEvent updateSsapApplicationEvent(SsapApplication currentApplication, SsapApplicationEvent applicationEvent, SsapApplicationEventTypeEnum eventType, String changePlan) {

		Timestamp todayTimestamp;
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());
		if(activityDate!=null){
			todayTimestamp = activityDate;
		} else {
			DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
			todayTimestamp = new Timestamp(todayDate.getMillis());
		}		SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();
		List<SsapApplicantEvent> applicantEvents = applicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent applicantEvent : applicantEvents) {
			SepEvents sepEvent = sepEventsOther.get(INCOME_CHANGE_CSR_EVENT);
			applicantEvent.setSepEvents(sepEvent);

			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			applicantEvent.setEventDate(todayTimestamp);
			applicantEvent.setEnrollmentStartDate(todayTimestamp);
			applicantEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp));
			applicationSepEndDates.add(applicantEvent.getEnrollmentEndDate());
		}

		DateTime endDate = new DateTime(applicationSepEndDates.last().getTime()).withTime(23, 59, 59, 999);
		applicationEvent.setEnrollmentStartDate(todayTimestamp);
		applicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		applicationEvent.setEventType(eventType);
		applicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		applicationEvent.setChangePlan(changePlan);
		applicationEvent.setSsapApplication(currentApplication);
		return ssapApplicationEventRepository.saveAndFlush(applicationEvent);

	}

	@Override
	public SsapApplicationEvent createApplicationEventForCSChanges(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean pureCsChange) {
		
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		
		Timestamp activityDate = migrationUtil.getActivityDate(currentApplication.getId());

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			Date eventDate;
			Date reportDate;
			if(activityDate!=null){
				eventDate = activityDate;
				reportDate = activityDate;
			} else {
				eventDate = new TSDate();
				reportDate = new TSDate();
			}
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}
			
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (nonQHPEventName != null) {
							eventCode = nonQHPEventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}

					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							reportDate = payloadEvent.get(0).getReportDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (SEP_OTHER_EVENT.equals(eventCode)){
						eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
					}
					
					sepEvent = sepRemoveEvents.get(eventCode);
				} else {
					sepEvent = sepRemoveEvents.get(AUTO_REMOVAL_WITHOUT_EVENT);
				}
			} else if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							reportDate = payloadEvent.get(0).getReportDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (SEP_OTHER_EVENT.equals(eventCode)){
						LOGGER.error("should not reach here. no valid add event found. go back!!");
						return null;
					}
					
					sepEvent = sepAddEvents.get(eventCode);
					
					if (sepEvent == null){
						LOGGER.error("should not reach here. incoming add events doesnot match with system info. go back!!");
						return null;
					}
					
				} else {
					LOGGER.error("should not reach here. no add event found. go back!!");
					return null;
				}
			} else if (statusEnum == ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL){
				if (pureCsChange){
					sepEvent = sepEventsOther.get(INCOME_CHANGE_CSR_EVENT); /* create new event */
				} else {
					sepEvent = sepEventsOther.get(AUTO_CHANGE_IN_CSR_WITHOUT_EVENT); /* create auto event */
				}
			} 

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate()));
			applicationSepEnrollmentStartDates.add(applicantEvent.getEventDate());
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		Timestamp enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		Timestamp enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		
		
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		
	}
}
