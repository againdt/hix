package com.getinsured.hix.util;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.account.repository.ILocationRepository;
import com.getinsured.hix.broker.PasswordRecoveryEmail;
import com.getinsured.hix.consumer.repository.ISSAPApplicationRepository;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.CAPConfiguration.CAPConfigurationEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEncryptorUtil;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component
public class ConsumerResetPasswordUtil {

	private static final String SUPERVISOR = "SUPERVISOR";
	private static final String ALTERNATE_EMAIL = "alternateEmail";
	private static final String EMAIL_ID = "EMAIL_ID";
	private static final String OVERRIDE_EMAIL = "OVERRIDE_EMAIL";
	private static final String ERROR = "ERROR";
	private static final String ERROR_MESSAGE = "ERROR_MESSAGE";
	private static final String QUESTIONS = "QUESTIONS";
	public static final String QUESTIONSANSWERS = "QUESTIONSANSWERS";
	private static final String STATUS = "STATUS";
	public static final String ALLOW_RETRY = "ALLOW_RETRY";
	public static final String ACCOUNT_LOCKED = "ACCOUNT_LOCKED";
	public static final String OK = "OK";
	public static final String USER_OBJECT = "user";
	private static final Logger LOGGER = Logger
			.getLogger(ConsumerResetPasswordUtil.class);
	private static final int THIRTY_TWO = 32;
	private static final int noOfRandomQs = 2;
	private static final String REMAINING_RETRIES = "REMAINING_RETRIES";
	
	@Autowired private UserService userService;
	@Autowired private ISSAPApplicationRepository applicationRepository;
	@Autowired private PasswordRecoveryEmail passwordRecoveryEmail;
	@Autowired private ILocationRepository iLocationRepository;
	@Autowired private ConsumerEventsUtil consumerEventUtil;	

	

		private Integer getRandomInteger(int aStart, int aEnd, Random aRandom) {
		if (aStart > aEnd) {
			throw new IllegalArgumentException("Start cannot exceed End.");
		}
		long range = (long) aEnd - (long) aStart;
		long fraction = (long) (range * aRandom.nextDouble());
		int randomNumber = (int) (fraction + aStart);
		return randomNumber;
	}
	
	private Map<String,String> createResetPasswordQuestions(Household household){
		Map<String,String> resetPasswordQsMap = new HashMap<String,String>();
		Map<String,String> randomQuestionsMap = new HashMap<String,String>();
		
		if(null!=household.getUser())
		{
			resetPasswordQsMap.put("firstName" ,household.getFirstName());
			resetPasswordQsMap.put("lastName" ,household.getLastName());
			resetPasswordQsMap.put("phoneNo" ,household.getPhoneNumber());
			//HIX-101747 In case household phone no is empty, override it with user phone no.
			//Phone no should always be present as it belongs to Fixed set of Qs.
			if(StringUtils.isEmpty(household.getPhoneNumber())){
				resetPasswordQsMap.put("phoneNo" ,household.getUser().getPhone());	
			}
			
			
		}
		if(null != household.getPrefContactLocation()) { // If location is not null means user has filled details on 2nd screen.
			Location location = household.getPrefContactLocation();
			if(!StringUtils.isEmpty(location.getZip())){
				randomQuestionsMap.put("zipCode" ,location.getZip());	
			}
			if(!StringUtils.isEmpty(location.getCounty())){
				randomQuestionsMap.put("county" ,location.getCounty());	
			}
			if(household.getBirthDate() != null && !StringUtils.isEmpty(household.getBirthDate().toString())){
				randomQuestionsMap.put("dob", this.getFormattedDOB(household.getBirthDate()));	
			}
			
		}
		this.addApplicantLevelQuestions(household, randomQuestionsMap);
		this.addRandomQuestions(resetPasswordQsMap, randomQuestionsMap);
		return resetPasswordQsMap;
	}

	private void addRandomQuestions(Map<String,String> resetPasswordQsMap,Map<String, String> randomQuestionsMap){
		if(!randomQuestionsMap.isEmpty()){
			List<String> randomQuestionKeys = new ArrayList<String>(randomQuestionsMap.keySet());
			String value="";
			String key="";
			Random random = new Random();
			try{
				if(randomQuestionKeys.size() >= noOfRandomQs){
					for (int idx = 0; idx < noOfRandomQs; idx++) {
						int randomInt = -1;
						do{
							randomInt = getRandomInteger(0, randomQuestionKeys.size(), random);
							key = randomQuestionKeys.get(randomInt);
							value = randomQuestionsMap.get(key);
						}
						while(StringUtils.isEmpty(value) || resetPasswordQsMap.containsKey(key));
						resetPasswordQsMap.put(key, value);	
					}
				}
			} catch (Exception ex) {
				LOGGER.error("ERR: WHILE GENERATING RANDOM QST LST: ", ex);
			}
		}
		
	}
	private void addApplicantLevelQuestions(Household household, Map<String, String> randomQuestionsMap) {
		List<SsapApplication> ssapApplicationsList = applicationRepository.findLatestSsapApplicationByHouseholdId(household.getId());
		SsapApplicant primaryApplicant = null;
		List<SsapApplicant> ssapApplicants = null;
		if(!ssapApplicationsList.isEmpty()){
			ssapApplicants = ssapApplicationsList.get(0).getSsapApplicants();
			if(!ssapApplicants.isEmpty()){
				for (SsapApplicant ssapApplicant : ssapApplicants) {
					if(ssapApplicant.getPersonId() == 1){
						primaryApplicant = ssapApplicant;
						break;
					}
				}
			}
		}
		
		if(primaryApplicant != null){
			randomQuestionsMap.put("sizeOfHousehold", String.valueOf(ssapApplicants.size()));
			if(!StringUtils.isEmpty(primaryApplicant.getGender())){
				randomQuestionsMap.put("gender", primaryApplicant.getGender());	
			}
			if(!StringUtils.isEmpty(primaryApplicant.getSsn())){
				randomQuestionsMap.put("ssn", primaryApplicant.getSsn().substring(primaryApplicant.getSsn().length()-4,primaryApplicant.getSsn().length()));	
			}
		
			/*
			 * Override zipcode/county and dob in case application is found.
			 * */
			Location location = this.getLocation(primaryApplicant);
			if(!StringUtils.isEmpty(location.getZip())){
				randomQuestionsMap.put("zipCode" ,location.getZip());	
			}
			if(!StringUtils.isEmpty(location.getCounty())){
				randomQuestionsMap.put("county" ,location.getCounty());	
			}
			if(primaryApplicant.getBirthDate() != null && !StringUtils.isEmpty(primaryApplicant.getBirthDate().toString())){
				randomQuestionsMap.put("dob", this.getFormattedDOB(primaryApplicant.getBirthDate()));	
			}
		}
	}
	
	public Map<String,Object> validateResetPasswordQuestions(Household household,Map<String,Object> requestParams,Map<String,String> dbQuestionAnswerMap){
		Map<String,Object> responseMap = new HashMap<String,Object>();
		LOGGER.info("******** validateResetPasswordQuestions METHOD STARTS ***********");
		int noOfRetries = getResetPasswordRetryLimit();
		Boolean isValid = Boolean.TRUE;
		for (Map.Entry<String, String> eachEntry : dbQuestionAnswerMap.entrySet()) {
			String ansFromRequest = StringUtils.defaultString((String)requestParams.get(eachEntry.getKey())).trim();
			if(eachEntry.getKey().equalsIgnoreCase("phoneNo")){
				ansFromRequest = this.formatPhoneNo(ansFromRequest);
			}
			if(StringUtils.isEmpty(ansFromRequest) ||  !ansFromRequest.equalsIgnoreCase(eachEntry.getValue())){
				isValid = Boolean.FALSE;
				break;
			}
		}
		
		if(isValid == Boolean.FALSE){
			AccountUser user = household.getUser();
			if(user != null ){
				int passwordResetQsCnt = user.getSecQueRetryCount();
				if(passwordResetQsCnt < noOfRetries){
					passwordResetQsCnt++;
					if(passwordResetQsCnt == noOfRetries){// after allowed no. of  attempts lock the account and update count
						userService.updateConfirmedAndSecQueRetryCount(user, 0, passwordResetQsCnt);
						responseMap.put(STATUS, ACCOUNT_LOCKED);
						responseMap.put(QUESTIONSANSWERS, null);
						
					}
					else{// allow only allowed no. of  attempts and track using sec q retry count
						userService.updateSecQueRetryCount(user, passwordResetQsCnt);
						responseMap.put(STATUS, ALLOW_RETRY);
						responseMap.put(REMAINING_RETRIES, noOfRetries - passwordResetQsCnt);
						Map<String,String> newResetQsMap = this.createResetPasswordQuestions(household);
						responseMap.put(QUESTIONS, newResetQsMap.keySet());
						responseMap.put(QUESTIONSANSWERS, newResetQsMap);
					}
				}
			}
		}
		else{
			this.handleSendResetPasswordEmail(household, responseMap);
			responseMap.put(QUESTIONSANSWERS, null);
			}
		
		LOGGER.info("******** validateResetPasswordQuestions METHOD ENDS ***********");
		return responseMap;
	}

	private Map<String, Object> handleSendResetPasswordEmail(Household household, Map<String, Object> responseMap) {
		// if questions are valid send reset password email.
			AccountUser user = null;
			
			// case 1: Login is of L2 CSR
			try {
				user =  userService.getLoggedInUser();
			}catch (InvalidUserException e) {
				LOGGER.error("Unable to get loggedin user: ");
				responseMap.put(STATUS,ERROR);
				responseMap.put(ERROR_MESSAGE, "Unable to get logged in User");	
				return responseMap;
			}
			boolean isEmailOverrideAllowed = this.isEmailOverrideAllowed(user);
			
			//if(isEmailOverrideAllowed && featureFlag.isEnabled(user, "OVERRIDE_EMAIL", Boolean.FALSE)){
			if(isEmailOverrideAllowed){
				// if feature is ON and L2 Supervisor login - communicate to UI - display alternate email modal.
				responseMap.put(STATUS,OK);
				responseMap.put(OVERRIDE_EMAIL, Boolean.TRUE);	
			}
			else{
				// if feature is OFF or if L2 CSR is logged in - send email directly.
				String sendEmailResponse = this.processResetPasswordEmail(household.getUser(),null);
				
				if(!StringUtils.isEmpty(sendEmailResponse)){
					responseMap.put(STATUS,ERROR);
					responseMap.put(ERROR_MESSAGE, sendEmailResponse);	
				}
				else{
					responseMap.put(EMAIL_ID,household.getUser().getEmail());
					responseMap.put(STATUS,OK);
				}
			}
			return responseMap;
	}

	private int getResetPasswordRetryLimit() {
		String passwordResetRetryLimit = DynamicPropertiesUtil.getPropertyValue(CAPConfigurationEnum.CAP_CSR_PASSWORD_RESET_RETRIES_LIMIT.getValue());
		int noOfRetries = 0;
		if(StringUtils.isNumeric(passwordResetRetryLimit)){
			noOfRetries = Integer.parseInt(passwordResetRetryLimit);
		}
		else{
			LOGGER.info("******** passwordResetRetryLimit IS EITHER NOT CONFIGURED OR NOT NUMERIC ***********");
		}
		return noOfRetries;
	}
	
	private Boolean isOverrideEmailFeatureEnabled() {
		String featureProperty = DynamicPropertiesUtil.getPropertyValue(CAPConfigurationEnum.CAP_CSR__SUPERVISOR_OVERRIDE_EMAIL.getValue());
		Boolean isFeatureEnabled = Boolean.FALSE;
		if(StringUtils.isNotEmpty(featureProperty)){
			isFeatureEnabled = featureProperty.equalsIgnoreCase("true") ? Boolean.TRUE : Boolean.FALSE;
		}
		else{
			LOGGER.info("******** CAP_CSR__SUPERVISOR_OVERRIDE_EMAIL IS NOT DEFINED  ***********");
		}
		return isFeatureEnabled;
	}
	
	public boolean verifyIfAccountLocked(Household household){
		Boolean isAccountLocked = Boolean.FALSE;
		if(household.getUser().getConfirmed() == 0 || household.getUser().getSecQueRetryCount() >= getResetPasswordRetryLimit()){
			isAccountLocked = Boolean.TRUE;
		}
		return isAccountLocked;
	}
	
	@GiAudit(transactionName = "Consumer Reset Password Email", eventType = EventTypeEnum.PASSWORD_RESET, eventName = EventNameEnum.PII_READ)
	private String processResetPasswordEmail(AccountUser user, String alternateEmailId){
		String randomToken = GhixEncryptorUtil.generateUUIDHexString(THIRTY_TWO);
		// HIX-101902 - Set sec Q retry count to 0 as password reset email is being send after validation.
		userService.updateSecQueRetryCount(user, 0);
		user = userService.savePasswordRecoveryDetails(user.getEmail(), randomToken);
		String errorMSg=StringUtils.EMPTY;
		if (user == null) {
			LOGGER.info("Failed to save the password recovery details");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error Sending Password Recovery Email"));
			errorMSg = "Password recovery email can not be sent, Please contact the administrator";
		} else {
			// initiate email sending process...
			LOGGER.info("Sending password recovery e-mail...");
			try {
				this.generateAndSendResetPasswordEmail(user,alternateEmailId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Password Recovery Email Sent"));
			} catch (Exception e) {
				LOGGER.error(e.getMessage());
				LOGGER.error("Password recovery e-mail cannot be sent - ", e);
				errorMSg = "Password recovery e-mail cannot be sent. Please contact the administrator.";
			}
		}
		
		return errorMSg;
	}
	
	/**
	 * Sends email for password recovery
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void generateAndSendResetPasswordEmail(AccountUser userObj,String alternateEmailId) throws NotificationTypeNotFound {
		LOGGER.trace("CONSUMER SEND PASSWORD RESET EMAIL : START");

		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", userObj);
		Map<String, String> tokens = passwordRecoveryEmail.getTokens(notificationContext);
		Map<String, String> emailData = passwordRecoveryEmail.getEmailData(notificationContext);
		if(!StringUtils.isEmpty(alternateEmailId)){
			emailData.put("To", alternateEmailId); // overwrite To field to handle override email flow. The calling method needs to set right emailId in the parameter.	
		}
		
		Location location = null; 
		
		Notice noticeObj = passwordRecoveryEmail.generateEmail(passwordRecoveryEmail.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+passwordRecoveryEmail.getClass().getSimpleName());
		}
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug(noticeObj.getEmailBody());
		}
		passwordRecoveryEmail.sendEmail(noticeObj);
		
		LOGGER.trace("CONSUMER SEND PASSWORD RESET EMAIL : END");
	}
	
	public Map<String,Object> fetchResetPasswordQuestions(Household household){
		Map<String,Object> responseMap = new HashMap<String,Object>();
		if(household != null){
			if(StringUtils.isEmpty(household.getEmail())){
				responseMap.put(STATUS, ConsumerResetPasswordUtil.ERROR);
				responseMap.put(ERROR_MESSAGE, "The Household has no email associated with it.");
			}
			else if(household.getUser() != null){
				if("Inactive".equalsIgnoreCase(household.getUser().getStatus())){
					responseMap.put(STATUS, ConsumerResetPasswordUtil.ERROR);
					responseMap.put(ERROR_MESSAGE, "This user account is inactive. To reset the password, please re-activate the user account.");
				}
				else{
			if(this.verifyIfAccountLocked(household)){
						responseMap.put(STATUS, ConsumerResetPasswordUtil.ERROR);
						responseMap.put(ERROR_MESSAGE, "This user account is locked. To reset the password, please unlock the user account.");
			}
			else{
				responseMap.put(STATUS, ConsumerResetPasswordUtil.OK);
				Map<String, String> resetQs = this.createResetPasswordQuestions(household);
				responseMap.put(QUESTIONS, resetQs.keySet());
				responseMap.put(QUESTIONSANSWERS, resetQs);
			}			
		}
			}
			else{
				responseMap.put(STATUS, ConsumerResetPasswordUtil.ERROR);
				responseMap.put(ERROR_MESSAGE, "No user found for this household.");
			}	
		}
		else{
			responseMap.put(STATUS, ConsumerResetPasswordUtil.ERROR);
			responseMap.put(ERROR_MESSAGE, "No household found.");
		}

		
		return responseMap;
	}
	
	private Boolean isEmailOverrideAllowed(AccountUser user){
		Boolean isPermitted =  Boolean.FALSE;
		if(user != null && user.getActiveModuleName() != null){
			isPermitted = user.getActiveModuleName().equalsIgnoreCase(SUPERVISOR)	&& this.isOverrideEmailFeatureEnabled();
		}
		return isPermitted;
	}
	
	public Map<String, Object> handleResetPasswordToAlternateEmail(Household household,Map<String,Object> requestParams) {
		Map<String,Object> responseMap = new HashMap<String,Object>();
		String alternateEmailId = StringUtils.defaultString((String)requestParams.get("email")) ;
		String emailType =  StringUtils.defaultString((String)requestParams.get("emailType")) ;
		String comments =  StringUtils.defaultString((String)requestParams.get("reason")) ;
		String sendEmailResponse = StringUtils.EMPTY;
		
		// send email to alternate emailid.
		if(ALTERNATE_EMAIL.equalsIgnoreCase(emailType)){
			if(StringUtils.isEmpty(alternateEmailId)){
				responseMap.put(STATUS,ERROR);
				responseMap.put(ERROR_MESSAGE, "ALTERNATE EMAIL IS NOT MENTIONED");
			}
			else{
				sendEmailResponse = this.processResetPasswordEmail(household.getUser(), alternateEmailId);
				responseMap.put(EMAIL_ID,alternateEmailId);	
			}
		}
		//send email to hix email id
		else{
			this.processResetPasswordEmail(household.getUser(), null);	
			responseMap.put(EMAIL_ID,household.getUser().getEmail());
		}
		
		if(StringUtils.isNotEmpty(sendEmailResponse)){
			responseMap.put(STATUS,ERROR);
			responseMap.put(ERROR_MESSAGE, sendEmailResponse);	
		}
		else{
			responseMap.put(STATUS,OK);
			if(ALTERNATE_EMAIL.equalsIgnoreCase(emailType) && StringUtils.isNotEmpty(comments)){
			requestParams.put("household_id", household.getId());
			saveAlternateEmailDetailsInCapHistory(requestParams);
		}
		}
		return responseMap;
	}
	
	private Location getLocation(SsapApplicant ssapApplicant) {
		Location location = null;
		
		if (ssapApplicant.getOtherLocationId() != null) {
			location = iLocationRepository.findOne(ssapApplicant
					.getOtherLocationId().intValue());
		}
		
		return location;
	}
	

	private String getFormattedDOB(Date dob){
		String DATE_FORMAT_NOW = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT_NOW);
		String stringDate = sdf.format(dob );
		return stringDate;
	}
	
	private String formatPhoneNo(String phoneNo){
		if(StringUtils.isNotEmpty(phoneNo)){
			phoneNo = StringUtils.deleteWhitespace(phoneNo).replace("(","").replace(")","").replace("-","");
		}
		return phoneNo;
	}
	
	private void saveAlternateEmailDetailsInCapHistory(Map<String,Object> requestParams){
		consumerEventUtil.saveResetPasswordAlternateEmailDtls(requestParams);	
	}
	
}
