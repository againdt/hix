package com.getinsured.iex.hub.smec.wrapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.DatePeriodType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.MECVerificationCodeType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.MedicaidVerificationCodeType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.PregOnlyVerificationCodeType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.RecertificationDateType;
import com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov.SourceInformationType;

public class SourceInformationTypeWrapper implements JSONAware {
	private String recertificationDate;
	private String mecVerificationCode;
	private List<DatePeriodTypeWrapper> mecEligibilityPeriodWrapper;
 
	private String medicaidVerificationCode;
    private List<DatePeriodTypeWrapper> medicaidEligibilityPeriodWrapper;
    private String pregOnlyVerificationCode;
    private List<DatePeriodTypeWrapper> pregOnlyEligibilityPeriodWrapper;
    
	public SourceInformationTypeWrapper(SourceInformationType sourceInformation) throws HubServiceException{
		if(sourceInformation != null){
		try {
				RecertificationDateType recertificationDateType = sourceInformation.getRecertificationDate();
			
				if(recertificationDateType != null){
					PlatformServiceUtil.xmlDateToString(recertificationDateType.getStartDate(), "MM/dd/yyyy");
				}
				
				MECVerificationCodeType mecVerificationCodeType = sourceInformation.getMECVerificationCode();
				
				if(mecVerificationCodeType != null){
					mecVerificationCode = mecVerificationCodeType.value();
				}
				
				List<DatePeriodType> mecEligibilityPeriod = sourceInformation.getMECEligibilityPeriod();
				
				if(!mecEligibilityPeriod.isEmpty()){
					mecEligibilityPeriodWrapper = new ArrayList<DatePeriodTypeWrapper>();
				}
				for(DatePeriodType datePeriodType:  mecEligibilityPeriod){
					DatePeriodTypeWrapper datePeriodTypeWrapper;
					
						datePeriodTypeWrapper = new DatePeriodTypeWrapper(datePeriodType);
					
					mecEligibilityPeriodWrapper.add(datePeriodTypeWrapper);
				}
				
				MedicaidVerificationCodeType medicaidVerificationCodeType = sourceInformation.getMedicaidVerificationCode();
				
				if(medicaidVerificationCodeType != null){
					medicaidVerificationCode = medicaidVerificationCodeType.value();
				}
				
				PregOnlyVerificationCodeType pregOnlyVerificationCodeType = sourceInformation.getPregOnlyVerificationCode();
				
				if(pregOnlyVerificationCodeType != null){
					pregOnlyVerificationCode = pregOnlyVerificationCodeType.value();
				}
				
				List<DatePeriodType> medicaidEligibilityPeriod = sourceInformation.getMedicaidEligibilityPeriod();
				
				if(!medicaidEligibilityPeriod.isEmpty()){
					medicaidEligibilityPeriodWrapper = new ArrayList<DatePeriodTypeWrapper>();
				}
				for(DatePeriodType datePeriodType:  medicaidEligibilityPeriod){
					DatePeriodTypeWrapper datePeriodTypeWrapper = new DatePeriodTypeWrapper(datePeriodType);
					medicaidEligibilityPeriodWrapper.add(datePeriodTypeWrapper);
				}
				
				List<DatePeriodType> pregOnlyEligibilityPeriod = sourceInformation.getPregOnlyEligibilityPeriod();
				
				if(!pregOnlyEligibilityPeriod.isEmpty()){
					pregOnlyEligibilityPeriodWrapper = new ArrayList<DatePeriodTypeWrapper>();
				}
				for(DatePeriodType datePeriodType:  pregOnlyEligibilityPeriod){
					DatePeriodTypeWrapper datePeriodTypeWrapper = new DatePeriodTypeWrapper(datePeriodType);
					pregOnlyEligibilityPeriodWrapper.add(datePeriodTypeWrapper);
				}
				
			}
			catch (ParseException | DatatypeConfigurationException e) {
				 throw new HubServiceException(e.getMessage(), e);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		obj.put("RecertificationDate", this.recertificationDate);
		obj.put("MECVerificationCode", this.mecVerificationCode);
			
		JSONArray mecEligPeriod = null;
		if(mecEligibilityPeriodWrapper != null && !mecEligibilityPeriodWrapper.isEmpty()){
			mecEligPeriod = new JSONArray();
			
			for(DatePeriodTypeWrapper datePeriodTypeWrapper : this.mecEligibilityPeriodWrapper){
				mecEligPeriod.add(datePeriodTypeWrapper);
			}
			obj.put("MECEligibilityPeriod", mecEligPeriod);
		}
		obj.put("MedicaidVerificationCode", this.medicaidVerificationCode);
	
		JSONArray medicaidEligPeriod = null;
		if(medicaidEligibilityPeriodWrapper != null && !medicaidEligibilityPeriodWrapper.isEmpty()){
			medicaidEligPeriod = new JSONArray();
			
			for(DatePeriodTypeWrapper datePeriodTypeWrapper : this.medicaidEligibilityPeriodWrapper){
				medicaidEligPeriod.add(datePeriodTypeWrapper);
			}
			obj.put("MedicaidEligibilityPeriod", medicaidEligPeriod);
		}
		
		obj.put("PregOnlyVerificationCode", this.pregOnlyVerificationCode);
		
		JSONArray pregOnlyEligPeriod = null;
		if(pregOnlyEligibilityPeriodWrapper != null && !pregOnlyEligibilityPeriodWrapper.isEmpty()){
			pregOnlyEligPeriod = new JSONArray();
			
			for(DatePeriodTypeWrapper datePeriodTypeWrapper : this.pregOnlyEligibilityPeriodWrapper){
				pregOnlyEligPeriod.add(datePeriodTypeWrapper);
			}
			obj.put("PregOnlyEligibilityPeriod", pregOnlyEligPeriod);
		}
		
		return obj.toJSONString();
	}
}
