var row ='<tr id="person1">';
var SavingData = false;
function saveData53(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData53Json();
	var state = $('#home_primary_state').val();
	var county =  $('#home_primary_county').val();
	/*
    $.ajax({
            type : "POST",
            url : theUrlForCountyCodeDetails,
            data: { state: state, county: county},
            success : function(response) {
            	    webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].householdContact.homeAddress.primaryAddressCountyFipsCode = response.toString();

            },
            error : function(e) {
                    alert("Failed to get countycodedetails Response  ------->"+e);
            }
    });*/
}

function backData53(){
	setValuesToappscr53JSON();
}

function loadData54()
{
	setValuesToappscr54JSON();
	next();
}

function saveData54(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData54Json();
}

function backData54(){
	setValuesToappscr54JSON();
}

function loadData55()
{
	setData55();
	next();
}
function setData55(){
	setHouseHoldContact();
	setValuesToappscr55JSON();
}

function setHouseHoldContact()
{
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	$('#houseHold_Contact_span1').html('');
	if($('#middleName').val() == "Middle Name")
		$('#middleName').val('');

	if($('#lastName').val() == "Last Name")
		$('#lastName').val('');

	var name = getNameByPersonId(1);
	$('#houseHold_Contact_span1').html(name);
	$('#houseHold_Contact_span2').html(name);
	$('#houseHold_Contact_span3').html(name);
}

function saveData55(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	saveData55Json();
}

function backData55(){
	setHouseHoldContact();
	setValuesToappscr55JSON();
	var firstIndex;
	var secondIndex;
	if($('#1ApplyingForhouseHoldOther').html() != undefined)
		firstIndex = $('#1ApplyingForhouseHoldOther').index();
	else if($('#1ApplyingForhouseHoldMember').html() != undefined)
		firstIndex = $('#1ApplyingForhouseHoldMember').index();
	else
		firstIndex = $('#1ApplyingForhouseHoldOnly').index();

	if($('#1page55radio9').html() != undefined)
		secondIndex = $('#1page55radio9').index();
	else
		secondIndex = $('#1page55radio10').index();
}


function saveData57(){
	//Validation Checking
	$('#back_button_div').show();
	if(currentPage == 'appscr57'){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	}
	addHouseHoldDetail();
}

function backData57(){

	$('#back_button_div').hide();
	var firstIndex = $('#1appscr57FirstName').index();
	var secondIndex;
	if($('#1appscr57SexNo').html() != undefined)
		secondIndex = $('#1appscr57SexNo').index();
	else
		secondIndex = $('#1appscr57SexYes').index();
	var personCount = 1;
	//setValuesToappscr57JSON();
	return;
}

function continueData60(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData60Json(currentID);
	return;
}

function backData60(){
	//
}

function continueData61(){

	//HIX-53221 SSN is not updated: need to call parsleyPageValidation() twice
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false && parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	var currentID = getCurrentApplicantID();
	continueData61Json(currentID);
	return;
}

function backData61(){
	var ApplicantID = getCurrentApplicantID();
	fillAppscr61(ApplicantID);
}

function continueData62Part1(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	//continueData62Part1Json();// Suneel 05/09/2014
	continueData62Part1Json(currentID);
	return;
}

function backData62Part1(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr61FromJSON(currentID);
	return;
}

function continueData62Part2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData62Part2Json();
}

function backData62Part2(){
	//
}

function continueData63(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData63Json(currentID);
	return;
}

function backData63(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr63FromJSON(currentID);
	return;
}

function continueData64(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData64Json(currentID);
	return;
}

function backData64(){
	//
}

function continueData65(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	continueData65Json();
	return;
}

function backData65(){
	appscr65FromMongodb();
}

function continueData66(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	continueData66Json(); return; //below code not needed
}

function backData66(){
	populateRelationship();
	populateDataToRelations();
}

function continueData69(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData69Json(currentID);
	return;
}

function backData69(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		appscr69JSON(currentID);
		//appscr74EditControllerJSON(currentID);
	}
	return;
}

function continueData70(){
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData70Json(currentID);
}

function backData70(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		appscr69FromMongodbJSON(currentID);
		//appscr74EditControllerJSON(currentID);
	}
}

function continueData71(){

	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}

	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData71Json(currentID);
	return;
}

function backData71(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	if(useJSON == true){
		//appscr69JSON(currentID);
		appscr74EditControllerJSON(currentID);
	}
}

function continueData72(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData72Json(currentID);
	return;
}

function backData72(){
	//
}

function continueData73(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData73Json(currentID);
	return; //don't need following code
}

function backData73(){
	//
}

function continueData76P1(){
	//Validation Checking
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){
		return;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData76P1Json(currentID);
	return;
}

function backData76P1(){
	//
}

function continueData76P2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData76P2Json(currentID);
}

function backData76P2(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr76FromMongodbJSON(currentID);
	return;
}

function continueData77Part1(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData77Part1Json(currentID);
}

function backData77Part1(){
	//
}

function continueData77Part2(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	continueData77Part2Json(currentID);
}

function backData77Part2(){
	//
}

function continueData78(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	//var currentID = getCurrentApplicantID();
}

function backData78(){
	//
}

function continueData79(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function backData79(){
	//
}

function continueData81A(){
	//Validation Checking
	validationCheck = true;
	if(currentPage != "appscr64" && validation(currentPage)==false) {
		validationCheck = false;
		return false;
	}
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function backData81A(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
	appscr76FromMongodbJSON(currentID);
	return;
}

function saveData(){
	SavingData = true;
	if(currentPage == "appscr53"){
		saveData53();
	}
	if(currentPage == "appscr54"){
		saveData54();
	}
	if(currentPage == "appscr55"){
		saveData55();
	}
	if(currentPage == "appscr57"){
		$('#contBtn').show();
		$('#contBtn').text('Continue');
		$('#countinue_button_div').addClass("Continue_button");
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");

		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr58"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr59A"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr59B"){
		generateArray57();
		saveData57();
	}
	if(currentPage == "appscr60"){
		continueData60();
	}
	if(currentPage == "appscr61"){
		continueData61();
	}
	if(currentPage == "appscr62Part1"){
		continueData62Part1();
	}
	if(currentPage == "appscr62Part2"){
		continueData62Part2();
	}
	if(currentPage == "appscr63"){
		continueData63();
	}
	if(currentPage == "appscr64"){
		continueData64();
	}
	if(currentPage == "appscr65"){
		continueData65();
	}
	if(currentPage == "appscr66"){
		continueData66();
	}
	if(currentPage == "appscr69"){
		continueData69();
	}
	if(currentPage == "appscr70"){
		continueData70();
	}
	if(currentPage == "appscr71"){
		continueData71();
	}
	if(currentPage == "appscr72"){
		continueData72();
	}
	if(currentPage == "appscr73"){
		continueData73();
	}
	if(currentPage == "appscr76P1"){
		continueData76P1();
	}
	if(currentPage == "appscr76P2"){
		continueData76P2();
	}
	if(currentPage == "appscr77Part1"){
		continueData77Part1();
	}
	if(currentPage == "appscr77Part2"){
		continueData77Part2();
	}
	if(currentPage == "appscr78"){
		continueData78();
	}
	if(currentPage == "appscr79"){
		continueData79();
	}
	if(currentPage == "appscr81A"){
		continueData81A();
	}
	if(currentPage == "appscr53" && editFlag){
		addAppscr58Detail(); //reload summary data
		goToPageById("appscr58");
		enableOtherFields();
		editFlag = false;
		$('#contBtn').text('Continue');
		$('#countinue_button_div').removeClass("ContinueForUpdate_button");
		$('#countinue_button_div').addClass("Continue_button");
		$('#cancelButtonCancel').hide();
		$('#back_button_div').show();
		$('#contBtn').show();
	}
	//saveBloodRelationData();
	/*if(submitForAutoContinue==true) {
		saveDataSubmit();
	}*/
	if(mode=="") {
		saveDataSubmit();

		/*if(currentPage=="appscr82") {
			testRidp();
		}*/
	}
	SavingData = false;
}
var array53 = new Array();
function createArray53(){

	array53[0] = 'firstName';
	array53[1]= 'middleName';
	array53[2] = 'lastName';
	array53[3] = 'appscr53Suffix';
	array53[4] = 'dateOfBirth';
	array53[5] = 'emailAddress';
	array53[6] = 'home_addressLine1';
	array53[7] = 'home_addressLine2';
	array53[8] = 'home_primary_city';
	array53[9] = 'home_primary_zip';
	array53[10] = 'home_primary_state';
	array53[11] = 'home_primary_county';
	array53[12] = 'mailingAddressIndicator';
	array53[13] = 'mailing_addressLine1';
	array53[14] = 'mailing_addressLine2';
	array53[15] = 'mailing_primary_city';
	array53[16] = 'mailing_primary_zip';
	array53[17] = 'mailing_primary_state';
	array53[18] = 'mailing_primary_county';
	array53[19] = 'first_phoneNo';
	array53[20] = 'first_ext';
	array53[21] = 'first_phoneType';
	array53[22] = 'second_phoneNo';
	array53[23] = 'second_ext';
	array53[24] = 'second_phoneType';
	array53[25] = 'preffegred_spoken_language';
	array53[26] = 'preffered_written_language';
}

var array54 = new Array();
function createArray54(){

	array54[0] = 'brokerName54';
	array54[1]= 'brokerID54';
	array54[2] = 'guideName54';
	array54[3] = 'guideID54';
	if($('input[name=authorizedRepresentativeIndicator]:radio:checked').val()== 'yes'){
		array54[4] = 'displayAuthRepresentativeInfo';
	}
	else{
		array54[4] = 'hideAuthRepresentativeInfo';
	}
	array54[5] = 'authorizedFirstName';
	array54[6] = 'authorizedMiddleName';
	array54[7] = 'authorizedLastName';
	array54[8] = 'authorizedSuffix';
	array54[9] = 'authorizedEmailAddress';
	array54[10] = 'authorizedAddress1';
	array54[11] = 'authorizedAddress2';
	array54[12] = 'authorizedCity';
	array54[13] = 'authorizedState';
	array54[14] = 'authorizedZip';
	array54[15] = 'appscr54_phoneNumber';
	array54[16] = 'appscr54_ext';
	array54[17] = 'authorizedPhoneType';
	if($('input[name=isPersonHelpingyou4ApplingHealthInsurance]:radio:checked').val()== 'Yes'){
		array54[18] = 'cmpnyNameAndOrgRadio54_1';
	}
	else{
		array54[18] = 'cmpnyNameAndOrgRadio54_2';
	}
	array54[19] = 'authorizeCompanyName';
	array54[20] = 'authorizeOrganizationId';
	if($('input[name=makeOtherizedRepresentative]:radio:checked').val()== 'Signature'){
		array54[21] = 'makeOtherizedRepresentativeSignature';
	}
	else{
		array54[21] = 'makeOtherizedRepresentativeLater';
	}
}

var array55 = new Array();
function createArray55(){

	if($('input[name=ApplyingForhouseHold]:radio:checked').val()== 'houseHoldContactOnly'){
		array55[0] = 'ApplyingForhouseHoldOnly';
	}
	else if($('input[name=ApplyingForhouseHold]:radio:checked').val()== 'otherFamilyMember'){
		array55[0] = 'ApplyingForhouseHoldMember';
	}
	else{
		array55[0] = 'ApplyingForhouseHoldOther';
	}
	if($('input[name=wantToGetHelpPayingHealthInsurance]:radio:checked').val()== 'Yes'){
		array55[1]= 'wantToGetHelpPayingHealthInsurance_id1';
	}
	else{
		array55[1]= 'wantToGetHelpPayingHealthInsurance_id2';
	}
	if($('input[name=appscr55paragraph]:radio:checked').val()== 'yes'){
		array55[2] = 'page55radio6';
	}
	else if($('input[name=appscr55paragraph]:radio:checked').val()== 'no'){
		array55[2] = 'page55radio7';
	}
	else{
		array55[2] = 'page55radio8';
	}

	array55[3] = 'noOfPeople53';

	if($('input[name=checkTypeOfHelp4HealthInsurance]:radio:checked').val()== 'yes'){
		array55[4]= 'page55radio9';
	}
	else{
		array55[4]= 'page55radio10';
	}

}

var array57 = new Array();
function generateArray57(){
		var noOfPeople=getNoOfHouseHolds (); //$('#noOfPeople53').val();
		var count = 0;
		for(var index=1; index<=noOfPeople;index++){
			array57[count++] = 'appscr57FirstName';
			array57[count++]= 'appscr57MiddleName';
			array57[count++] = 'appscr57LastName';
			array57[count++] = 'identifire';
			array57[count++] = 'appscr57Suffix';
			array57[count++] = 'appscr57DOB';
			array57[count++] = 'appscr57DOBVerificationStatus';
			if($('input[name=appscr57Tobacco'+(index)+']:radio:checked').val()== 'yes'){
				array57[count++]= 'appscr57SexYes';
			}
			else{
				array57[count++]= 'appscr57SexNo';
			}

		}
}
////////////////////////// Method generating Ajax Request start////////

function saveDataSubmit(){
	$('#modalCSRApplicationType').modal('hide');
	$('#ssapSubmitModal').modal();
	$('#waiting').show();
	$('#contBtn').attr('disabled','disabled');
	if(currentPage == "appscr87"){
		//$('#generateXMLFormLast').submit();
		//$('#waiting').hide();
		//return;
	}
	var idname="";
	var caseindividual = [];
	var count = 0;

	var result = "";
	for ( var i = 0; i < caseindividual.length; i++) {
		result += caseindividual[i];
	}
	var dataFD = getFDData();
	var casei;
	var CaseIdentifier = 1;//$('#cohbeApplicationId').val();
	var HeadOFHousehold = 1;//$('#cbmsApplicationId').val();

	casei = 'CaseIdentifier=' + CaseIdentifier + '&HeadOFHousehold='
			+ HeadOFHousehold;

	var relationship = [];
	$('#appscrBloodRelTableBody tr').each(
			function() {

					var Indiv = $(this).find(".Indiv").html();
					var rel = $(this).find(".rel").html();
					var relt = $(this).find(".relt").html();
					var relat = $(this).find(".relat").html();
					var pct = $(this).find(".pct").html();
					var tax = $(this).find(".tax").html();
					relationship.push(Indiv + '&' + rel + '&' + relt + '&'
							+ relat + '&' + pct + '&' + tax + '&');

			});


	var result1 = "";
	for ( var i = 0; i < relationship.length; i++) {
		result1 += relationship[i];
	}

	useJSONstring="No";
	if(useJSON == true){
		useJSONstring="Yes";
	}
	webJson.singleStreamlinedApplication.currentApplicantId = getCurrentApplicantID();
	webJsonstring = JSON.stringify(webJson);
	$.ajax({
		type : 'POST',
		url : theUrlForSaveJsonData,
		dataType : 'json',
		data : {
			case1 : casei,
			case2 : result,
			case3 : result1,
			case4 : idname,
			data : dataFD,
			currentPage : currentPage,
			webJson:webJsonstring,
			useJSON:useJSONstring,
			applicationSource:$('input[name=optCSRApplicationType]:radio:checked').val()==undefined?"ON":$('input[name=optCSRApplicationType]:radio:checked').val(),
			csrftoken: csrftoken
		},
		//async : true,
		success : function(msg,xhr) {

			if(isInvalidCSRFToken(msg))	{
				applicantGuidToBeRemoved = "";
				return;
			}
			/*if(msg!='' && msg.status == 'SSAC_SUCCESS' ){

				if(msg.object == 'N'){
					alert('Cannot Proceed ');
				}
			}*/

			if(msg!='' && msg != null) {
				webJson = msg;
			}

			if(webJson.singleStreamlinedApplication.mecCheckStatus!=null && webJson.singleStreamlinedApplication.mecCheckStatus!=undefined && "Y"==webJson.singleStreamlinedApplication.mecCheckStatus){
				$('#mecCheckAlert').modal();
			}

			$('#ssapSubmitModal').modal('hide');
			$('#waiting').hide();
			$('#contBtn').removeAttr('disabled');
			//$('#generateJSONForm').submit();

		},
		error :function(XMLHttpRequest, textStatus, errorThrown) {
			applicantGuidToBeRemoved = "";
			$('#waiting').hide();
			$('#ssapSubmitModal').modal('hide');
			alert("Please try Again !");
			//$('#contBtn').removeAttr('disabled');
		}
	});
}

function saveFDRelation(){
	//var currentID = $('#houseHoldTRNo').val().charAt(9);
	var currentID = getCurrentApplicantID();
}

function getFDData(){
//
}

function getFrequencyData(HouseHoldRow)
{
	var dataIndex = 0;
	var frequencyData='';
	for (;dataIndex < financial_DetailsArray.length ; dataIndex++) {
		var data = financial_DetailsArray[dataIndex];
		if(dataIndex!=0 && dataIndex!=2 && dataIndex!=6){
			frequencyData+=HouseHoldRow.find("."+data+"Frequency").html();
			frequencyData+='#';
		}
	}
	return frequencyData;
}

function otherInsuranceCheckbox(i){
	if($("#appscr78OtherStateFHBP"+i).is(":checked")){
		$("#appscr78OtherStateOrFederalHealthPlan"+i).show();
	}else{
		$("#appscr78OtherStateOrFederalHealthPlan"+i).hide();
	}
}