
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for DHSIDType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DHSIDType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I327DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I551DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I571DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I766DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}CertOfCitDocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}NatrOfCertDocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}MacReadI551DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}TempI551DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I94DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I94UnexpForeignPassportDocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}UnexpForeignPassportDocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I20DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DS2019DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}OtherCase1DocumentID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}OtherCase2DocumentID"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DHSIDType", propOrder = {
    "i327DocumentID",
    "i551DocumentID",
    "i571DocumentID",
    "i766DocumentID",
    "certOfCitDocumentID",
    "natrOfCertDocumentID",
    "macReadI551DocumentID",
    "tempI551DocumentID",
    "i94DocumentID",
    "i94UnexpForeignPassportDocumentID",
    "unexpForeignPassportDocumentID",
    "i20DocumentID",
    "ds2019DocumentID",
    "otherCase1DocumentID",
    "otherCase2DocumentID"
})
public class DHSIDType {

    @XmlElement(name = "I327DocumentID")
    protected I327DocumentID3Type i327DocumentID;
    @XmlElement(name = "I551DocumentID")
    protected I551DocumentID4Type i551DocumentID;
    @XmlElement(name = "I571DocumentID")
    protected I571DocumentID5Type i571DocumentID;
    @XmlElement(name = "I766DocumentID")
    protected I766DocumentID9Type i766DocumentID;
    @XmlElement(name = "CertOfCitDocumentID")
    protected CertOfCitDocumentID23Type certOfCitDocumentID;
    @XmlElement(name = "NatrOfCertDocumentID")
    protected NatrOfCertDocumentID20Type natrOfCertDocumentID;
    @XmlElement(name = "MacReadI551DocumentID")
    protected MacReadI551DocumentID22Type macReadI551DocumentID;
    @XmlElement(name = "TempI551DocumentID")
    protected TempI551DocumentID21Type tempI551DocumentID;
    @XmlElement(name = "I94DocumentID")
    protected I94DocumentID2Type i94DocumentID;
    @XmlElement(name = "I94UnexpForeignPassportDocumentID")
    protected I94UnexpForeignPassportDocumentID10Type i94UnexpForeignPassportDocumentID;
    @XmlElement(name = "UnexpForeignPassportDocumentID")
    protected UnexpForeignPassportDocumentID30Type unexpForeignPassportDocumentID;
    @XmlElement(name = "I20DocumentID")
    protected I20DocumentID26Type i20DocumentID;
    @XmlElement(name = "DS2019DocumentID")
    protected DS2019DocumentID27Type ds2019DocumentID;
    @XmlElement(name = "OtherCase1DocumentID")
    protected OtherCase1DocumentID1Type otherCase1DocumentID;
    @XmlElement(name = "OtherCase2DocumentID")
    protected OtherCase2DocumentID1Type otherCase2DocumentID;

    /**
     * Gets the value of the i327DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I327DocumentID3Type }
     *     
     */
    public I327DocumentID3Type getI327DocumentID() {
        return i327DocumentID;
    }

    /**
     * Sets the value of the i327DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I327DocumentID3Type }
     *     
     */
    public void setI327DocumentID(I327DocumentID3Type value) {
        this.i327DocumentID = value;
    }

    /**
     * Gets the value of the i551DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I551DocumentID4Type }
     *     
     */
    public I551DocumentID4Type getI551DocumentID() {
        return i551DocumentID;
    }

    /**
     * Sets the value of the i551DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I551DocumentID4Type }
     *     
     */
    public void setI551DocumentID(I551DocumentID4Type value) {
        this.i551DocumentID = value;
    }

    /**
     * Gets the value of the i571DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I571DocumentID5Type }
     *     
     */
    public I571DocumentID5Type getI571DocumentID() {
        return i571DocumentID;
    }

    /**
     * Sets the value of the i571DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I571DocumentID5Type }
     *     
     */
    public void setI571DocumentID(I571DocumentID5Type value) {
        this.i571DocumentID = value;
    }

    /**
     * Gets the value of the i766DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I766DocumentID9Type }
     *     
     */
    public I766DocumentID9Type getI766DocumentID() {
        return i766DocumentID;
    }

    /**
     * Sets the value of the i766DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I766DocumentID9Type }
     *     
     */
    public void setI766DocumentID(I766DocumentID9Type value) {
        this.i766DocumentID = value;
    }

    /**
     * Gets the value of the certOfCitDocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link CertOfCitDocumentID23Type }
     *     
     */
    public CertOfCitDocumentID23Type getCertOfCitDocumentID() {
        return certOfCitDocumentID;
    }

    /**
     * Sets the value of the certOfCitDocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link CertOfCitDocumentID23Type }
     *     
     */
    public void setCertOfCitDocumentID(CertOfCitDocumentID23Type value) {
        this.certOfCitDocumentID = value;
    }

    /**
     * Gets the value of the natrOfCertDocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link NatrOfCertDocumentID20Type }
     *     
     */
    public NatrOfCertDocumentID20Type getNatrOfCertDocumentID() {
        return natrOfCertDocumentID;
    }

    /**
     * Sets the value of the natrOfCertDocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link NatrOfCertDocumentID20Type }
     *     
     */
    public void setNatrOfCertDocumentID(NatrOfCertDocumentID20Type value) {
        this.natrOfCertDocumentID = value;
    }

    /**
     * Gets the value of the macReadI551DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link MacReadI551DocumentID22Type }
     *     
     */
    public MacReadI551DocumentID22Type getMacReadI551DocumentID() {
        return macReadI551DocumentID;
    }

    /**
     * Sets the value of the macReadI551DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link MacReadI551DocumentID22Type }
     *     
     */
    public void setMacReadI551DocumentID(MacReadI551DocumentID22Type value) {
        this.macReadI551DocumentID = value;
    }

    /**
     * Gets the value of the tempI551DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link TempI551DocumentID21Type }
     *     
     */
    public TempI551DocumentID21Type getTempI551DocumentID() {
        return tempI551DocumentID;
    }

    /**
     * Sets the value of the tempI551DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link TempI551DocumentID21Type }
     *     
     */
    public void setTempI551DocumentID(TempI551DocumentID21Type value) {
        this.tempI551DocumentID = value;
    }

    /**
     * Gets the value of the i94DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I94DocumentID2Type }
     *     
     */
    public I94DocumentID2Type getI94DocumentID() {
        return i94DocumentID;
    }

    /**
     * Sets the value of the i94DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I94DocumentID2Type }
     *     
     */
    public void setI94DocumentID(I94DocumentID2Type value) {
        this.i94DocumentID = value;
    }

    /**
     * Gets the value of the i94UnexpForeignPassportDocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I94UnexpForeignPassportDocumentID10Type }
     *     
     */
    public I94UnexpForeignPassportDocumentID10Type getI94UnexpForeignPassportDocumentID() {
        return i94UnexpForeignPassportDocumentID;
    }

    /**
     * Sets the value of the i94UnexpForeignPassportDocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I94UnexpForeignPassportDocumentID10Type }
     *     
     */
    public void setI94UnexpForeignPassportDocumentID(I94UnexpForeignPassportDocumentID10Type value) {
        this.i94UnexpForeignPassportDocumentID = value;
    }

    /**
     * Gets the value of the unexpForeignPassportDocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link UnexpForeignPassportDocumentID30Type }
     *     
     */
    public UnexpForeignPassportDocumentID30Type getUnexpForeignPassportDocumentID() {
        return unexpForeignPassportDocumentID;
    }

    /**
     * Sets the value of the unexpForeignPassportDocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnexpForeignPassportDocumentID30Type }
     *     
     */
    public void setUnexpForeignPassportDocumentID(UnexpForeignPassportDocumentID30Type value) {
        this.unexpForeignPassportDocumentID = value;
    }

    /**
     * Gets the value of the i20DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link I20DocumentID26Type }
     *     
     */
    public I20DocumentID26Type getI20DocumentID() {
        return i20DocumentID;
    }

    /**
     * Sets the value of the i20DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link I20DocumentID26Type }
     *     
     */
    public void setI20DocumentID(I20DocumentID26Type value) {
        this.i20DocumentID = value;
    }

    /**
     * Gets the value of the ds2019DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link DS2019DocumentID27Type }
     *     
     */
    public DS2019DocumentID27Type getDS2019DocumentID() {
        return ds2019DocumentID;
    }

    /**
     * Sets the value of the ds2019DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link DS2019DocumentID27Type }
     *     
     */
    public void setDS2019DocumentID(DS2019DocumentID27Type value) {
        this.ds2019DocumentID = value;
    }

    /**
     * Gets the value of the otherCase1DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link OtherCase1DocumentID1Type }
     *     
     */
    public OtherCase1DocumentID1Type getOtherCase1DocumentID() {
        return otherCase1DocumentID;
    }

    /**
     * Sets the value of the otherCase1DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherCase1DocumentID1Type }
     *     
     */
    public void setOtherCase1DocumentID(OtherCase1DocumentID1Type value) {
        this.otherCase1DocumentID = value;
    }

    /**
     * Gets the value of the otherCase2DocumentID property.
     * 
     * @return
     *     possible object is
     *     {@link OtherCase2DocumentID1Type }
     *     
     */
    public OtherCase2DocumentID1Type getOtherCase2DocumentID() {
        return otherCase2DocumentID;
    }

    /**
     * Sets the value of the otherCase2DocumentID property.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherCase2DocumentID1Type }
     *     
     */
    public void setOtherCase2DocumentID(OtherCase2DocumentID1Type value) {
        this.otherCase2DocumentID = value;
    }

}
