package com.getinsured.iex.hub.vci.wrapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.NumericType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.OrganizationType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class IncomeTypeWrapper implements JSONAware{

	
	private List<BigDecimal> incomeAmount;
	private List<BigDecimal> incomeHoursPerWeekMeasure;
	private List<String> incomeDate;
	private List<BigDecimal> incomeNetPaymentAmount;
	
	private static final String AMT_KEY = "IncomeAmount";
	private static final String HOURS_KEY = "IncomeHoursPerWeekMeasure";
	private static final String DATE_KEY = "IncomeDate";
	private static final String NET_PAY_KEY = "IncomeNetPaymentAmount";
	
	public IncomeTypeWrapper(IncomeType incomeType) {

		if (incomeType == null) {
			return;
		}
		
		for(AmountType amount : incomeType.getIncomeAmount()){
			getIncomeAmount().add(amount.getValue());
		}
		
		for(NumericType hours : incomeType.getIncomeHoursPerWeekMeasure()){
			getIncomeHoursPerWeekMeasure().add(hours.getValue());
		}
		
		for(DateType date : incomeType.getIncomeDate()){
			if(date!= null
	    			&& date.getDateRepresentation() != null 
	    			&& date.getDateRepresentation().getValue() != null){
	    		Date incDate = (Date)date.getDateRepresentation().getValue();
	    		getIncomeDate().add(incDate.getValue().toString());
			}
		}
		
		for(AmountType amount : incomeType.getIncomeNetPaymentAmount()){
			getIncomeNetPaymentAmount().add(amount.getValue());
		}

	}
	
	public List<BigDecimal> getIncomeAmount() {
        if (incomeAmount == null) {
            incomeAmount = new ArrayList<BigDecimal>();
        }
        return this.incomeAmount;
    }

    public List<BigDecimal> getIncomeHoursPerWeekMeasure() {
        if (incomeHoursPerWeekMeasure == null) {
            incomeHoursPerWeekMeasure = new ArrayList<BigDecimal>();
        }
        return this.incomeHoursPerWeekMeasure;
    }

    public List<String> getIncomeDate() {
        if (incomeDate == null) {
            incomeDate = new ArrayList<String>();
        }
        return this.incomeDate;
    }

    public List<BigDecimal> getIncomeNetPaymentAmount() {
        if (incomeNetPaymentAmount == null) {
            incomeNetPaymentAmount = new ArrayList<BigDecimal>();
        }
        return this.incomeNetPaymentAmount;
    }
    
    @SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		JSONArray array = new JSONArray();
		if(this.incomeAmount != null){
			for(BigDecimal amount : this.incomeAmount){
				array.add(amount);
			}
		}
		obj.put(AMT_KEY, array);
		
		array = new JSONArray();
		if(this.incomeHoursPerWeekMeasure != null){
			for(BigDecimal amount : this.incomeHoursPerWeekMeasure){
				array.add(amount);
			}
		}
		obj.put(HOURS_KEY, array);
		
		array = new JSONArray();
		if(this.incomeDate != null){
			for(String date : this.incomeDate){
				array.add(date);
			}
		}
		obj.put(DATE_KEY, array);
		
		array = new JSONArray();
		if(this.incomeNetPaymentAmount != null){
			for(BigDecimal amount : this.incomeNetPaymentAmount){
				array.add(amount);
			}
		}
		obj.put(NET_PAY_KEY, array);
		
		return obj.toJSONString();
	}
	

}
