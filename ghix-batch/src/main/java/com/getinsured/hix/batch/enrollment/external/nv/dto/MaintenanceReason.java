package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


public class MaintenanceReason {

    private String maintenanceType;
    private String maintenanceTransactionType;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    public String getMaintenanceType() {
        return maintenanceType;
    }

    public void setMaintenanceType(String maintenanceType) {
        this.maintenanceType = maintenanceType;
    }

    public String getMaintenanceTransactionType() {
        return maintenanceTransactionType;
    }

    public void setMaintenanceTransactionType(String maintenanceTransactionType) {
        this.maintenanceTransactionType = maintenanceTransactionType;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
