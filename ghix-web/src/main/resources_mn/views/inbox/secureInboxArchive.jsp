<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table id="inbox" class="table table-condensed table-border-none">
	<thead>
		<tr class="header">
			<%-- <th class="graydrkbg">
				<div class="control-group">
					<div class="controls">
						<label class="checkbox"><input type="checkbox" class="checkall" style="padding-left:18px;"></label>
					</div>
				</div>
			</th> --%>
			<th><i class="icon-envelope"></i></th>
			<th scope="col" class="sortable"><dl:sort title="From" sortBy="fromUserName" sortOrder="${sortOrder}"></dl:sort></th>
			<th scope="col" class="sortable"><dl:sort title="Subject" sortBy="msgSub" sortOrder="${sortOrder}"></dl:sort></th>
			<th scope="col" class="sortable"><dl:sort title="Date" sortBy="dateCreated" sortOrder="${sortOrder}"></dl:sort></th>
			<th scope="col" ><i class="icon-paper-clip"></i></th>
		</tr>
	</thead>
	<tbody class="msg">
		<c:choose>
			<c:when test="${inboxMsgList != null}">
				<c:forEach var="inboxMsg" items="${inboxMsgList}">
					<tr class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
						<%-- <td>
							<div class="control-group">
								<div class="controls">
									<label class="checkbox"><input type="checkbox" class="checkbox" value="${inboxMsg.id}"></label>
								</div>
							</div>
						</td> --%>
						<td><i class="${inboxMsg.status == 'N' ? 'icon-envelope-unread' : 'icon-envelope'}"></i></td>
						<td class="${inboxMsg.status == 'N' ? 'unread' : 'read'}">
							<a href="javascript: doReadSubmit(${inboxMsg.id});"><i class="icon-user"></i>${inboxMsg.fromUserName}</a>
						</td>
						<td><a href="javascript: doReadSubmit(${inboxMsg.id});">${inboxMsg.msgSub}</a></td>
						<td>
							<jsp:useBean id="now" class="java.util.Date"/>
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="MMM dd, yyyy" var="dateCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="MMM dd, yyyy" var="dateNow" />
							<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="yyyy" var="yearCreated" />
							<fmt:formatDate type="DATE" value="${now}" pattern="yyyy" var="yearNow" />
							<c:choose>
								<c:when test="${dateCreated eq dateNow}">
									<fmt:formatDate type="TIME" value="${inboxMsg.dateCreated}" pattern="hh:mm a"/>
								</c:when>
								<c:when test="${yearCreated eq yearNow}">
									<fmt:formatDate type="DATE" value="${inboxMsg.dateCreated}" pattern="EEE MMM dd"/>
								</c:when>
								<c:otherwise>
									${dateCreated}
								</c:otherwise>
							</c:choose>
						</td>
						<td>
							<i class="${inboxMsg.messageDocs == null || empty inboxMsg.messageDocs ? '': 'icon-paper-clip'}"></i>
						</td>
					</tr>
				</c:forEach>
			</c:when>
			<c:otherwise>
				<tr class="read">
					<td colspan="5"><spring:message code='label.norecords' /></td>
				</tr>
			</c:otherwise>
		</c:choose>
	</tbody>
</table>
<div>
	<dl:paginate resultSize="${totalRecords + 0}" pageSize="${pageSize + 0}" firstAndLastView="true" hideTotal="true" />
</div>