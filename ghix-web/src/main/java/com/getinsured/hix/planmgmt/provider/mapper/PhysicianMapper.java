package com.getinsured.hix.planmgmt.provider.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.Physician.AcceptingNewPatients;
import com.getinsured.hix.model.Physician.PhysicianGender;
import com.getinsured.hix.model.Physician.PhysicianType;
import com.getinsured.hix.model.PhysicianAddress;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.Provider.AccessibilityCode;
import com.getinsured.hix.model.Provider.FSSProvider;
import com.getinsured.hix.model.Provider.LanguagesLocation;
import com.getinsured.hix.model.Provider.ProviderStatus;
import com.getinsured.hix.model.Provider.SanctionStatus;
import com.getinsured.hix.platform.file.mapper.Mapper;
import com.getinsured.hix.platform.util.DateUtil;

public class PhysicianMapper extends Mapper<List> {
	
	private static Map<String, Integer> columnKeys = null;
	private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(PhysicianMapper.class);
	
	/* Physician Table Column Entries */
	
	private static final String TYPE = "provider_type_indicator";
	private static final String FIRST_NAME = "first_name";
	private static final String MIDDLE_NAME = "middle_name";
	private static final String LAST_NAME = "last_name";
	private static final String SUFFIX = "suffix";
	private static final String GENDER = "gender";
	private static final String DEGREE = "Degree";
	private static final String ADDRESS_1 = "prac_addr_1";
	private static final String ADDRESS_2 = "prac_addr_2";
	private static final String CITY = "city";
	private static final String STATE = "state";
	private static final String ZIP = "zip";	
	private static final String LAT = "prac_addr_latitude";
	private static final String LONG = "prac_addr_longitude";
	private static final String PHONE = "prac_phone";
	private static final String FAX = "prac_fax";
	private static final String EMAIL = "practice_email_address";
	private static final String SPECIALITY = "Speciality";
	private static final String BOARD_CERTIFICATION = "board_certified";
	private static final String ACCEPTING_NEW_PATIENTS = "accepting_new_patients";
	private static final String STATUS = "status";
	private static final String MEDICAL_GROUP = "medical_group_affiliation";
	private static final String AFFILIATED_HOSPITAL = "hospital_affiliation";
	private static final String EDUCATION = "medical_school";	
	private static final String GRADUATION_YEAR = "medical_school_graduation_year";	
	private static final String RESIDENCY = "medical_school_residency";
	private static final String INTERNSHIP = "medical_school_internship";	
	private static final String YEARS_OF_EXP = "years_of_experience";
	private static final String OFFICE_HOURS = "office_hours";
	private static final String DOB = "DOB";
	private static final String DEA_NUMBER = "dea_number";
	private static final String LICENSE_NUMBER = "state_license_number";
	private static final String MEDICARE_PROVIDER_ID = "medicare_provider_id";
	private static final String TAX_ID = "Tax Id";
	private static final String NPI_NUMBER = "npi";
	private static final String LANGUAGES = "languages_spoken";
	private static final String LAST_UPDATED = "date_last_updated";
	private static final String DATASOURCE = "data_source";
	private static final String GROUP_KEY = "group_key";
	private static final String UPI_NUMBER = "upin";
	private static final String TITLE = "professional_title";
	private static final String SSN = "SSN";
	private static final String TAXONOMY_CODE = "taxonomy_code";
	private static final String COUNTY = "fips_county_code";
	private static final String LANGUAGES_LOCATION = "Language Location Provider";
	private static final String LICENSING_STATE = "licensing_state";
	private static final String MEDICAL_PROVIDER_ID = "medi-cal_provider_id";
	private static final String AHA_ID = "aha_id";
	private static final String FFS_PROVIDER = "ffs_provider";
	private static final String FFS_PROVIDER_COUNTY = "ffs_provider_county";
	private static final String PCP_ID = "pcp_id";
	private static final String ACCESSIBILITY_CODE = "accessibility_codes"; 
	private static final String LOCATION_ID ="location_id";
	private static final String CREATED_DATE ="date_created";
	private static final String SANCTION_STATUS ="sanction_status";
	private static final String SPECIALITY_NAME = "specialty_name";
	private static final String SPECIALITY_CODE = "specialty_code";
	private static final String FACILITY_TYPE_INDICATOR = "facility_type_indicator";
	private static final String NETWORK_ID = "network_id";
	private static final String FACILITY_NAME = "facility_name";
	private static final String FACILITY_TYPE = "facility_type";
	private static final String NETWORK_TIER_ID = "network_tier_id";
	private static final String INDIAN_HEALTH_SERVICE_PROVIDER = "indian_health_service_provider";
	
		
	/* Physician Table Column Entry key's */
	
	private static final int TYPE_VAL = 1;	
	private static final int FIRST_NAME_VAL = 2;
	private static final int MIDDLE_NAME_VAL = 3;
	private static final int LAST_NAME_VAL = 4;
	private static final int SUFFIX_VAL = 5;
	private static final int GENDER_VAL = 6;
	private static final int DEGREE_VAL = 7;
	private static final int ADDRESS_1_VAL = 8;
	private static final int ADDRESS_2_VAL = 9;
	private static final int CITY_VAL = 10;
	private static final int STATE_VAL = 11;
	private static final int ZIP_VAL = 12;
	private static final int LAT_VAL = 13;
	private static final int LONG_VAL = 14;
	private static final int PHONE_VAL = 15;
	private static final int FAX_VAL = 16;
	private static final int EMAIL_VAL = 17;
	private static final int SPECIALITY_VAL = 18;
	private static final int BOARD_CERTIFICATION_VAL = 19;
	private static final int ACCEPTING_NEW_PATIENTS_VAL = 20;
	private static final int STATUS_VAL = 21;
	private static final int MEDICAL_GROUP_VAL = 22;
	private static final int AFFILIATED_HOSPITAL_VAL = 23;
	private static final int EDUCATION_VAL = 24;
	private static final int GRADUATION_YEAR_VAL = 25;
	private static final int RESIDENCY_VAL = 26;
	private static final int INTERNSHIP_VAL = 27;
	private static final int YEARS_OF_EXP_VAL = 28;
	private static final int OFFICE_HOURS_VAL = 29;
	private static final int DOB_VAL = 30;
	private static final int DEA_NUMBER_VAL = 31;
	private static final int LICENSE_NUMBER_VAL = 32;
	private static final int MEDICARE_RPOVIDER_ID_VAL = 33;
	private static final int TAX_ID_VAL = 34;
	private static final int NPI_NUMBER_VAL = 35;
	private static final int LANGUAGES_VAL = 36;
	private static final int LAST_UPDATED_VAL = 37;
	private static final int DATASOURCE_VAL = 38;
	private static final int UPI_NUMBER_VAL = 39;
	private static final int TITLE_VAL = 40;
	private static final int SSN_VAL = 41;
	private static final int TAXONOMY_CODE_VAL = 42;
	private static final int COUNTY_VAL = 43;
	private static final int LANGUAGES_LOCATION_VAL = 44;
	private static final int LICENSING_STATE_VAL = 45;
	private static final int MEDICAL_PROVIDER_ID_VAL = 46;
	private static final int AHA_ID_VAL = 48;
	private static final int FFS_PROVIDER_VAL = 49;
	private static final int FFS_PROVIDER_COUNTY_VAL = 50;
	private static final int PCP_ID_VAL = 51;
	private static final int ACCESSIBILITY_CODE_VAL = 52;
	private static final int LOCATION_ID_VAL = 53;
	private static final int CREATED_DATE_VAL = 54;
	private static final int SANCTION_STATUS_VAL = 55;
	private static final int SPECIALITY_NAME_VAL = 57;
	private static final int SPECIALITY_CODE_VAL = 58;
	private static final int GROUP_KEY_VAL = 59;
	private static final int FACILITY_TYPE_INDICATOR_VAL = 60;
	private static final int NETWORK_ID_VAL = 61;
	private static final int FACILITY_NAME_VAL = 62;
	private static final int FACILITY_TYPE_VAL = 63;
	private static final int NETWORK_TIER_ID_VAL = 64;
	private static final int INDIAN_HEALTH_SERVICE_PROVIDER_VAL = 65;
	
	private static final int PHYSICIAN_INDEX = 0;
	private static final int NETWORK_ID_INDEX = 1;
	private static final int NETWORK_TIER_INDEX = 2;
	private static final int GROUP_KEY_INDEX = 3;
	private static final int SPECIALITY_CODE_INDEX = 4;
	
	static 	{
		columnKeys = new HashMap<String, Integer>();		
		columnKeys.put(TYPE.toLowerCase(), TYPE_VAL);
		columnKeys.put(FIRST_NAME.toLowerCase(), FIRST_NAME_VAL);			
		columnKeys.put(MIDDLE_NAME.toLowerCase(), MIDDLE_NAME_VAL);
		columnKeys.put(LAST_NAME.toLowerCase(), LAST_NAME_VAL);
		columnKeys.put(SUFFIX.toLowerCase(), SUFFIX_VAL);
		columnKeys.put(GENDER.toLowerCase(), GENDER_VAL);		
		columnKeys.put(DEGREE.toLowerCase(), DEGREE_VAL);	
		columnKeys.put(ADDRESS_1.toLowerCase(), ADDRESS_1_VAL);	
		columnKeys.put(ADDRESS_2.toLowerCase(), ADDRESS_2_VAL);	
		columnKeys.put(CITY.toLowerCase(), CITY_VAL);
		columnKeys.put(STATE.toLowerCase(), STATE_VAL);
		columnKeys.put(ZIP.toLowerCase(), ZIP_VAL);
		columnKeys.put(LAT.toLowerCase(), LAT_VAL);
		columnKeys.put(LONG.toLowerCase(), LONG_VAL);
		columnKeys.put(PHONE.toLowerCase(), PHONE_VAL);
		columnKeys.put(FAX.toLowerCase(), FAX_VAL);
		columnKeys.put(EMAIL.toLowerCase(), EMAIL_VAL);
		columnKeys.put(SPECIALITY.toLowerCase(), SPECIALITY_VAL);
		columnKeys.put(BOARD_CERTIFICATION.toLowerCase(), BOARD_CERTIFICATION_VAL);
		columnKeys.put(ACCEPTING_NEW_PATIENTS.toLowerCase(), ACCEPTING_NEW_PATIENTS_VAL);
		columnKeys.put(STATUS.toLowerCase(), STATUS_VAL);
		columnKeys.put(MEDICAL_GROUP.toLowerCase(), MEDICAL_GROUP_VAL);
		columnKeys.put(AFFILIATED_HOSPITAL.toLowerCase(), AFFILIATED_HOSPITAL_VAL);
		columnKeys.put(EDUCATION.toLowerCase(), EDUCATION_VAL);
		columnKeys.put(GRADUATION_YEAR.toLowerCase(), GRADUATION_YEAR_VAL);
		columnKeys.put(RESIDENCY.toLowerCase(), RESIDENCY_VAL);
		columnKeys.put(INTERNSHIP.toLowerCase(), INTERNSHIP_VAL);
		columnKeys.put(YEARS_OF_EXP.toLowerCase(), YEARS_OF_EXP_VAL);
		columnKeys.put(OFFICE_HOURS.toLowerCase(), OFFICE_HOURS_VAL);
		columnKeys.put(DOB.toLowerCase(), DOB_VAL);
		columnKeys.put(DEA_NUMBER.toLowerCase(), DEA_NUMBER_VAL);
		columnKeys.put(LICENSE_NUMBER.toLowerCase(), LICENSE_NUMBER_VAL);
		columnKeys.put(MEDICARE_PROVIDER_ID.toLowerCase(), MEDICARE_RPOVIDER_ID_VAL);
		columnKeys.put(TAX_ID.toLowerCase(), TAX_ID_VAL);
		columnKeys.put(NPI_NUMBER.toLowerCase(), NPI_NUMBER_VAL);
		columnKeys.put(LANGUAGES.toLowerCase(), LANGUAGES_VAL);
		columnKeys.put(LAST_UPDATED.toLowerCase(), LAST_UPDATED_VAL);
		columnKeys.put(DATASOURCE.toLowerCase(), DATASOURCE_VAL);
		columnKeys.put(TITLE.toLowerCase(), TITLE_VAL);
		columnKeys.put(UPI_NUMBER.toLowerCase(), UPI_NUMBER_VAL);
		columnKeys.put(SSN.toLowerCase(), SSN_VAL);
		columnKeys.put(TAXONOMY_CODE.toLowerCase(), TAXONOMY_CODE_VAL);
		columnKeys.put(COUNTY.toLowerCase(), COUNTY_VAL);
		columnKeys.put(LANGUAGES_LOCATION.toLowerCase(), LANGUAGES_LOCATION_VAL);
		columnKeys.put(LICENSING_STATE.toLowerCase(), LICENSING_STATE_VAL);
		columnKeys.put(MEDICAL_PROVIDER_ID.toLowerCase(), MEDICAL_PROVIDER_ID_VAL);
		columnKeys.put(AHA_ID.toLowerCase(), AHA_ID_VAL);
		columnKeys.put(FFS_PROVIDER.toLowerCase(), FFS_PROVIDER_VAL);
		columnKeys.put(FFS_PROVIDER_COUNTY.toLowerCase(), FFS_PROVIDER_COUNTY_VAL);
		columnKeys.put(PCP_ID.toLowerCase(), PCP_ID_VAL);
		columnKeys.put(ACCESSIBILITY_CODE.toLowerCase(), ACCESSIBILITY_CODE_VAL);
		columnKeys.put(LOCATION_ID.toLowerCase(), LOCATION_ID_VAL);
		columnKeys.put(CREATED_DATE.toLowerCase(), CREATED_DATE_VAL);
		columnKeys.put(SANCTION_STATUS.toLowerCase(), SANCTION_STATUS_VAL);
		columnKeys.put(SPECIALITY_NAME.toLowerCase(), SPECIALITY_NAME_VAL);
		columnKeys.put(SPECIALITY_CODE.toLowerCase(), SPECIALITY_CODE_VAL);
		columnKeys.put(GROUP_KEY.toLowerCase(), GROUP_KEY_VAL);
		columnKeys.put(FACILITY_TYPE_INDICATOR.toLowerCase(), FACILITY_TYPE_INDICATOR_VAL);
		columnKeys.put(NETWORK_ID.toLowerCase(), NETWORK_ID_VAL);
		columnKeys.put(FACILITY_NAME.toLowerCase(), FACILITY_NAME_VAL);
		columnKeys.put(FACILITY_TYPE.toLowerCase(), FACILITY_TYPE_VAL);
		columnKeys.put(NETWORK_TIER_ID.toLowerCase(), NETWORK_TIER_ID_VAL);
		columnKeys.put(INDIAN_HEALTH_SERVICE_PROVIDER.toLowerCase(), INDIAN_HEALTH_SERVICE_PROVIDER_VAL);
		
	}
	
	
	@Override
	@SuppressWarnings("unchecked")
	public List mapData(final List<String> data, final List<String> columns) 
	{
		Physician physician = new Physician();
		Provider provider = new Provider();
		PhysicianAddress physicianAddress = new PhysicianAddress();
		StringBuilder specialityCode = new StringBuilder();
		StringBuilder networkId = new StringBuilder("");
		StringBuilder networkTier = new StringBuilder("");;
		StringBuilder groupKey = new StringBuilder("");
		
		int index = 0;
		for (index=0;index<data.size();index++)
		{
			try{
			mapData(data.get(index), columns.get(index).toLowerCase(), physician, provider, physicianAddress, specialityCode, networkId, networkTier, groupKey);
			}catch(Exception e){
				LOGGER.error("ERROR in Physician mapper, Column Name:  "+columns.get(index)+", Available data value:  "+data.get(index)+"  , Message  :"+e.getMessage());
			}
		}
		
		physician.setProviderObj(provider); 
		physician.setPhysicianAddressObj(physicianAddress);
		@SuppressWarnings("rawtypes")
		List physicianDataList  = new ArrayList();
		physicianDataList.add(PHYSICIAN_INDEX, physician);
		physicianDataList.add(NETWORK_ID_INDEX, networkId);
		physicianDataList.add(NETWORK_TIER_INDEX, networkTier);
		physicianDataList.add(GROUP_KEY_INDEX, groupKey);
		physicianDataList.add(SPECIALITY_CODE_INDEX,specialityCode);
		return physicianDataList;
	}
	
	private void mapData(String data, String column, Physician physician, Provider provider, PhysicianAddress physicianAddress, StringBuilder specialityCode, StringBuilder networkId, StringBuilder networkTier, StringBuilder groupKey)
	{ 
		int	colVal = columnKeys.get(column.replace("\"", ""));
		String trimData = "";
		if(StringUtils.isNotBlank(data)){
			trimData = data.trim();	
		}
		try{
		switch (colVal)
		{
			case FIRST_NAME_VAL : 
				physician.setFirst_name(trimData);
			break;		
			case MIDDLE_NAME_VAL:
				physician.setMiddle_name(trimData);
			break;	
			case LAST_NAME_VAL:
				physician.setLast_name(trimData);
			break;
			case SUFFIX_VAL:
				if(trimData.length()>10){
					trimData = trimData.substring(0, 9);	
				}				
				physician.setSuffix(trimData);
			break;
			case GENDER_VAL:
				if(trimData.equalsIgnoreCase(PhysicianGender.M.toString()) || "male".equalsIgnoreCase(trimData)){
					physician.setGender(PhysicianGender.M);
				}
				else if(trimData.equalsIgnoreCase(PhysicianGender.F.toString()) || "female".equalsIgnoreCase(trimData)){
					physician.setGender(PhysicianGender.F);
				}
			break;
			case DEGREE_VAL:
				physician.setTitle(trimData);
			break;			
			case NPI_NUMBER_VAL:
				provider.setNpiNumber(trimData);
			break;
			case LICENSE_NUMBER_VAL:
				provider.setLicenseNumber(trimData);
			break;
			case MEDICARE_RPOVIDER_ID_VAL:
				provider.setMedicareProviderId(trimData);
			break;
			case TAX_ID_VAL:
				provider.setTaxId(trimData);
			break;
			case OFFICE_HOURS_VAL:
				provider.setOfficeHours(trimData);
			break;
			case DATASOURCE_VAL:
				provider.setDatasource(trimData);
			break;
			case TYPE_VAL:
				if(trimData.equalsIgnoreCase("m")){ /*Medical*/
					physician.setType(PhysicianType.DOCTOR);
				}else if(trimData.equalsIgnoreCase("d")){  /*Dental*/
					physician.setType(PhysicianType.DENTIST);
				}else if(trimData.equalsIgnoreCase("v")){  /*Vision*/
					physician.setType(PhysicianType.VISION);
				}
			break;		
			case STATUS_VAL:
				for(ProviderStatus status : ProviderStatus.values()){
					if(status.toString().equalsIgnoreCase(trimData)){
						provider.setStatus(status);
					 break;
					}
				 }
			break;	
			case UPI_NUMBER_VAL:
				provider.setUpiNumber(trimData);
			break;
			case ADDRESS_1_VAL : 
				physicianAddress.setAddress1(trimData);
			break;	
			case ADDRESS_2_VAL : 
				physicianAddress.setAddress2(trimData);
			break;	
			case CITY_VAL : 
				physicianAddress.setCity(trimData);
			break;
			case STATE_VAL : 
				physicianAddress.setState(trimData);
			break;
			case COUNTY_VAL: 
				physicianAddress.setCounty(trimData);
			break;
			case ZIP_VAL : 
				physicianAddress.setZip(trimData);
			break;
			case LAT_VAL : 
				try {
					physicianAddress.setLattitude(Float.parseFloat(trimData));
				} catch (Exception e) { LOGGER.error("Error while converting lattitude string to float "+e.getMessage()); }
			break;			
			case LONG_VAL : 
				try {
					physicianAddress.setLongitude(Float.parseFloat(trimData));
				} catch (Exception e) { LOGGER.error("Error while converting longitude string to float "+e.getMessage()); }
				
			break;
			case PHONE_VAL : 
				physicianAddress.setPhoneNumber(getValidPhoneOrFax(trimData));
			break;
			case FAX_VAL :
				
				physicianAddress.setFax(getValidPhoneOrFax(trimData));
			break;
			case EMAIL_VAL : 
				physician.setEmailAddress(trimData);
			break;
			case GRADUATION_YEAR_VAL:
				physician.setGraduationYear(trimData);
			break;	
			case ACCEPTING_NEW_PATIENTS_VAL:
				if(AcceptingNewPatients.YES.toString().equalsIgnoreCase(trimData)){
					physician.setAcceptingNewPatients(AcceptingNewPatients.YES);
				}else if(AcceptingNewPatients.NO.toString().equalsIgnoreCase(trimData)){
					physician.setAcceptingNewPatients(AcceptingNewPatients.NO);
				}else if(AcceptingNewPatients.U.toString().equalsIgnoreCase(trimData)){
					physician.setAcceptingNewPatients(AcceptingNewPatients.U);
				}
			break;
			case INTERNSHIP_VAL:
				physician.setInternship(trimData);
			break;
			case RESIDENCY_VAL:
				physician.setResidency(trimData);
			break;
			case DEA_NUMBER_VAL:
				physician.setDeaNumber(trimData);
			break;			
			case EDUCATION_VAL:
				physician.setEducation(trimData);
			break;
			case BOARD_CERTIFICATION_VAL:
				if(StringUtils.isNotBlank(trimData)){
					if(trimData.toUpperCase().charAt(0)=='Y'){
						trimData="YES";
					}else if(trimData.toUpperCase().charAt(0)=='N'){
						trimData="NO";
					}else{
						trimData = "UNDEFINED";
					}
				}
				physician.setBoardCertification(trimData);				
			break;
			case AFFILIATED_HOSPITAL_VAL:
				physician.setAffiliatedHospital(trimData);
			break;
			case MEDICAL_GROUP_VAL:
				physician.setMedicalGroup(trimData);
			break;			
			case LANGUAGES_VAL:
				StringBuilder providerLanguage = new StringBuilder("");
				if(trimData.contains("|")){
					String[] languagesSpoken = trimData.split("\\|");
					for(int i=0;i<languagesSpoken.length;i++){
						String[] languageAndLocation = languagesSpoken[i].split("-");
						if(languageAndLocation.length<=2){
							providerLanguage.append(languageAndLocation[0]);
						}else{
							for(int j =0;j<languageAndLocation.length-1;j++){
								providerLanguage.append(languageAndLocation[j]);
							}
						}
						providerLanguage.append(",");
					}
				}else{
					String[] languageAndLocation = trimData.split("-");
					if(languageAndLocation.length<=2){
						providerLanguage.append(languageAndLocation[0]);
					}else{
						for(int j =0;j<languageAndLocation.length-1;j++){
							providerLanguage.append(languageAndLocation[j]);
						}
					}
					providerLanguage.append(",");
				}
				if(providerLanguage.length()>0){
					providerLanguage.setLength(providerLanguage.length()-1);
				}
				physician.setLanguages(providerLanguage.toString());
			break;			
			case YEARS_OF_EXP_VAL :
				physician.setYearsOfExp(trimData);
			break;
			case DOB_VAL :
				if(StringUtils.isNotBlank(trimData)){
					physician.setBirthDay(DateUtil.StringToDate(trimData,"MM/dd/yyyy"));
				}
			break;
			case LAST_UPDATED_VAL : 
				/*
				 * Using system generated dates to avoid exception coming due to wrong date from enclarity file
				 * if(StringUtils.isNotBlank(trimData)){
					provider.setLastUpdateDate(DateUtil.StringToDate(trimData,"MM/dd/yyyy"));
				}*/
			break;
			case TITLE_VAL:
				physician.setTitle(trimData);
			break;
			case SSN_VAL:
				try {
					provider.setSSN(Integer.parseInt(trimData));
				}catch (Exception e) {LOGGER.error("Error while converting string to integer");}
				
			break;
			case LANGUAGES_LOCATION_VAL:
				if(LanguagesLocation.L.toString().equalsIgnoreCase(trimData)){
					provider.setLanguages_location(LanguagesLocation.L);
				}else if(LanguagesLocation.B.toString().equalsIgnoreCase(trimData)){
					provider.setLanguages_location(LanguagesLocation.B);
				}else if(LanguagesLocation.P.toString().equalsIgnoreCase(trimData)){
					provider.setLanguages_location(LanguagesLocation.P);
				}
			break;
			case LICENSING_STATE_VAL:
				provider.setLicensing_state(trimData);
			break;
			case MEDICAL_PROVIDER_ID_VAL:
				provider.setMedical_provider_id(trimData);
			break;
			case FFS_PROVIDER_VAL:
				if(FSSProvider.Y.toString().equalsIgnoreCase(trimData)){
					provider.setFfs_provider(FSSProvider.Y);
				}else if(FSSProvider.N.toString().equalsIgnoreCase(trimData)){
					provider.setFfs_provider(FSSProvider.N);
				}
			break;
			case FFS_PROVIDER_COUNTY_VAL:
				provider.setFfs_provider_country(trimData);
			break;
			case PCP_ID_VAL:
				provider.setPcp_id(trimData);
			break;
			case ACCESSIBILITY_CODE_VAL:
				if(AccessibilityCode.B.toString().equalsIgnoreCase(trimData)){
					provider.setAccessibility_code(AccessibilityCode.B);
				}else if (AccessibilityCode.E.toString().equalsIgnoreCase(trimData)) {
					provider.setAccessibility_code(AccessibilityCode.E);
				}else if (AccessibilityCode.P.toString().equalsIgnoreCase(trimData)) {
					provider.setAccessibility_code(AccessibilityCode.P);
				}else if (AccessibilityCode.R.toString().equalsIgnoreCase(trimData)) {
					provider.setAccessibility_code(AccessibilityCode.R);
				}else if (AccessibilityCode.T.toString().equalsIgnoreCase(trimData)) {
					provider.setAccessibility_code(AccessibilityCode.T);
				}else if (AccessibilityCode.W.toString().equalsIgnoreCase(trimData)) {
					provider.setAccessibility_code(AccessibilityCode.W);
				}
			break;
			case LOCATION_ID_VAL:
				provider.setLocation_id(trimData);
			break;
			case CREATED_DATE_VAL:
				/*
				 * Using system generated dates to avoid exception coming due to wrong date from enclarity file
				 * if(StringUtils.isNotBlank(trimData)){
					provider.setCreationTimestamp(DateUtil.StringToDate(trimData,"MM/dd/yyyy"));
				}*/
			break;
			case SANCTION_STATUS_VAL:
				if (SanctionStatus.Y.toString().equalsIgnoreCase(trimData)) {
					provider.setSanction_status(SanctionStatus.Y);
				}else if (SanctionStatus.N.toString().equalsIgnoreCase(trimData)) {
					provider.setSanction_status(SanctionStatus.N);
				}
			break;
			case SPECIALITY_NAME_VAL:
			//	speciality.setName(trimData);
			break;
			case TAXONOMY_CODE_VAL:
				if(isValidSpecialty(trimData)){ 				 /*Will be use as specialty*/
					specialityCode.append(trimData);	
				}
			break;
			case SPECIALITY_CODE_VAL:
				if(isValidSpecialty(trimData)){ 				 /*Will be use as specialty*/
					specialityCode.append(trimData);	
				}
			break;
			case GROUP_KEY_VAL:
				groupKey.append(trimData);
			break;
			case NETWORK_ID_VAL:
				networkId.append(trimData.toUpperCase());
				break;
			case NETWORK_TIER_ID_VAL:
				networkTier.append(trimData.toUpperCase());
				break;
			case INDIAN_HEALTH_SERVICE_PROVIDER_VAL:
				break;
			default:
				break;
		}
		}catch(Exception e){
			LOGGER.info("Error in data");
		}
	}
	
	private boolean isValidSpecialty(String trimData){
		boolean returnVal = true;
		if(StringUtils.isNotBlank(trimData)){
			if(trimData.contains("|")){
				String[] specialityCodeArray = trimData.split("\\|");
				for(int specialityCodeCount = 0;specialityCodeCount<specialityCodeArray.length;specialityCodeCount++){
					if(specialityCodeArray[specialityCodeCount].length()>3){
						returnVal = false;
					}
				}
			}else{
				if(trimData.length()>2){
					returnVal =false;
				}
			}
		}else{
			returnVal = false;
		}
		return returnVal;
	}
	
	private String getValidPhoneOrFax(String phoneNumber){
		if(StringUtils.isNotBlank(phoneNumber)&&phoneNumber.length()>20){
			/*
			 * TODO :  uncomment code if needed:
			 * if(phoneNumber.contains("\\|")){
				String[] phNumbers = phoneNumber.split("\\|");
				for(int i = 0;i<phNumbers.length;i++){
					if(StringUtils.isNotBlank(phNumbers[i])){
						if(phNumbers[i].contains("~")){
							String [] nextSplit = phNumbers[i].split("~");
							for(int j = 0;j<nextSplit.length;j++){
								if(StringUtils.isNotBlank(nextSplit[j])){
									phoneNumber = nextSplit[j];
									break;
								}
							}
						}else{
							phoneNumber = phNumbers[i];
							break;
						}
					}
				}
			}else if(phoneNumber.contains("~")){
				String[] phNumbers = phoneNumber.split("~");
				for(int i = 0;i<phNumbers.length;i++){
					if(phNumbers[i].contains("\\|")){
						String [] nextSplit = phNumbers[i].split("\\|");
						for(int j = 0;j<nextSplit.length;j++){
							if(StringUtils.isNotBlank(nextSplit[j])){
								phoneNumber = nextSplit[j];
								break;
							}
						}
					}else{
						phoneNumber = phNumbers[i];
						break;
					}

				}
			}else{*/
				phoneNumber = phoneNumber.substring(0, 19);
			}
		/*}*/
		return phoneNumber;
	}
}
