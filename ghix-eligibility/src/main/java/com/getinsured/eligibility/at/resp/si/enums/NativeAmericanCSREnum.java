package com.getinsured.eligibility.at.resp.si.enums;



public enum NativeAmericanCSREnum {
	
	CS2("OpenToIndiansBelow300PercentFPL"),
	CS3("OpenToIndiansAbove300PercentFPL");
	
	private String s = null;
    private NativeAmericanCSREnum(String s)
    {
    	this.s = s;
    }
    
    public String value() {
        return s;
    }

    public static String value(String v) {
		if (v != null) {
			final NativeAmericanCSREnum data = readValue(v);
			if (data != null) {
				return data.value();
			}
		}
		return null;
	}

	public static NativeAmericanCSREnum fromValue(String v) {
		if (v != null) {
			return readValue(v);
		}
		return null;
	}

	private static NativeAmericanCSREnum readValue(String v) {
		NativeAmericanCSREnum data = null;
		for (NativeAmericanCSREnum c : NativeAmericanCSREnum.class.getEnumConstants()) {
			if (c.value().equalsIgnoreCase(v)) {
				data = c;
				break;
			}
		}
		return data;
	}

}
