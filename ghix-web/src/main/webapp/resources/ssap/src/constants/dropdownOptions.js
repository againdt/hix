
// Constants

import {BLOOD_RELATIONSHIP_CODES} from './bloodRelationshipCodesList';
import CONFIG from '../.env';
import {EXPENSE_TYPES, INCOME_TYPES, OTHER_INCOME_TYPES} from './incomeAndExpenceTypes';
import {FREQUENCY_TYPES} from './frequencyTypes';
import STATES_LIST from './statesList';


/****************************************************

 Options for dropdowns in alphabetic order

 ****************************************************/


const AGREE_DISAGREE = [
    {
        value: true,
        text: 'I agree'
    },
    {
        value: false,
        text: 'I disagree'
    }
];

const AGREE_DISAGREE_STMT = [
    {
        value: true,
        text: 'I agree with this statement'
    },
    {
        value: false,
        text: 'I disagree with this statement'
    }
];

const AUTHORIZED_REPRESENTATIVES = [
    {
        value: true,
        text: 'Someone is helping me'
    },
    {
        value: false,
        text: 'I am filling out this application for myself and/or my family'
    }
];

const BLOOD_RELATIONSHIP = [
    {
        value: 'default',
        text: 'Relationship'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.SPOUSE,
        text: 'Spouse'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.PARENT_OF_CHILD,
        text: 'Parent of child'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.PARENT_OF_ADOPTED_CHILD,
        text: 'Parent of adopted child'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.ADOPTED_CHILD,
        text: 'Adopted child (son or daughter)'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.WARD_OF_COURT_APPOINTED_GUARDIAN,
        text: 'Ward of court-appointed guardian'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.STEPPARENT,
        text: 'Stepparent (stepfather or stepmother)'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.STEPCHILD,
        text: 'Stepchild (stepson or step daughter)'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.CHILD,
        text: 'Child (son or daughter)'
    },
    {
        value: BLOOD_RELATIONSHIP_CODES.COURT_APPOINTED_GUARDIAN,
        text: 'Court-appointed guardian'
    }
];

const COMMUNICATION_METHOD = [
    {
        value: 'Email',
        text: 'Email'
    },
    {
        value: 'Mail',
        text: 'Postal Mail'
    },
    {
        value: 'None',
        text: 'None'
    }
];

export const COMMUNICATION_METHODS_LIST = Object.assign({},
    ...COMMUNICATION_METHOD.map(({ value, text }) => ({
        [value]: text
    })));

export const DEFAULT_COMMUNICATION_METHOD = 'Email';

const COUNTIES = (counties, state) => {
    if(CONFIG.ENV.DEVELOPMENT) {
        return [
            {
                value: 'default',
                text: 'County'
            },
            {
                value: 'Santa Clara',
                text: 'Santa Clara'
            },
            {
                value: 'Lake',
                text: 'Lake'
            },
        ];
    } else {
        return [
            {
                value: 'default',
                text: 'County'
            },
            ...(counties[state] ? counties[state].map(county => {
                const [countyCode, countyName] = Object.entries(county)[0];
                return {
                    value: countyCode,
                    text: countyName
                };
            }) : [])
        ];
    }
};


const COUNTRIES = countries => {
    if(CONFIG.ENV.DEVELOPMENT) {
        return [
            {
                value: 'default',
                text: 'County'
            },
            {
                value: 'BLR',
                text: 'Belarus'
            },
            {
                value: 'FR',
                text: 'France'
            },
        ];
    } else {
        return [
            {
                value: 'default',
                text: 'Country'
            },
            ...countries.map(country => ({
                value: country.countryCode,
                text: country.countryName
            }))
        ];
    }
};

const DEDUCTION_SOURCES = [
    {
        value: 'default',
        text: 'Deduction Source'
    },
    {
        value: EXPENSE_TYPES.ALIMONY,
        text: 'Alimony'
    },
    {
        value: EXPENSE_TYPES.STUDENT_LOAN_INTEREST,
        text: 'Student loan interest'
    },
    {
        value: EXPENSE_TYPES.OTHER,
        text: 'Other deductions'
    }
];

export const DEDUCTION_SOURCES_LIST = Object.assign(
    {},
    ...DEDUCTION_SOURCES.map(({ value, text }) => ({
        [value]: text
    })));

const GENDER = [
    {
        value: 'male',
        text: 'Male'
    },
    {
        value: 'female',
        text: 'Female'
    }
];

const INCOME_SOURCES = [
    {
        value: 'default',
        text: 'Income Source'
    },
    {
        value: INCOME_TYPES.ALIMONY,
        text: 'Alimony received'
    },
    {
        value: INCOME_TYPES.CAPITAL_GAIN,
        text: 'Capital Gains'
    },
    {
        value: INCOME_TYPES.FARMING_FISHING,
        text: 'Farming or Fishing'
    },
    {
        value: INCOME_TYPES.INVESTMENT,
        text: 'Investment'
    },
    {
        value: INCOME_TYPES.JOB,
        text: 'Job'
    },
    {
        value: INCOME_TYPES.OTHER,
        text: 'Other Income'
    },
    {
        value: INCOME_TYPES.PENSION,
        text: 'Pension'
    },
    {
        value: INCOME_TYPES.RENTAL_ROYALTY,
        text: 'Rental or Royalty'
    },
    {
        value: INCOME_TYPES.RETIREMENT,
        text: 'Retirement'
    },
    {
        value: INCOME_TYPES.SCHOLARSHIP,
        text: 'Scholarship'
    },
    {
        value: INCOME_TYPES.SELF_EMPLOYMENT,
        text: 'Self Employment'
    },
    {
        value: INCOME_TYPES.SOCIAL_SECURITY_DISABILITY,
        text: 'Social Security Benefits'
    },
    {
        value: INCOME_TYPES.UNEMPLOYMENT,
        text: 'Unemployment'
    }
];

export const INCOME_SOURCES_LIST = Object.assign(
    {},
    ...INCOME_SOURCES.map(({ value, text }) => ({
        [value]: text

    })));

export const INCOME_SOURCES_TRIBAL_INCOME = [
    {
        value: INCOME_TYPES.AI_AN,
        text: 'American Indian / Alaska Native income'
    }
];

export const OTHER_INCOME_SUBTYPES = [
    {
        value: 'default',
        text: 'Income Type'
    },
    {
        value: OTHER_INCOME_TYPES.CANCELED_DEBT,
        text: 'Canceled debts'
    },
    {
        value: OTHER_INCOME_TYPES.CASH_SUPPORT,
        text: 'Cash Support'
    },
    {
        value: OTHER_INCOME_TYPES.COURT_AWARD,
        text: 'Court Awards'
    },
    {
        value: OTHER_INCOME_TYPES.GAMBLING_PRIZE_AWARD,
        text: 'Gambling, prizes, or awards'
    },
    {
        value: OTHER_INCOME_TYPES.JURY_DUTY_PAY,
        text: 'Jury Duty Pay'
    },
    {
        value: OTHER_INCOME_TYPES.OTHER_INCOME,
        text: 'Other'
    },
];

export const FLAGS_OPTIONS = [
    {
        value: 'default',
        text: 'Flag'
    },
    {
        value: true,
        text: 'True'
    },
    {
        value: null,
        text: 'Null'
    },
    {
        value: false,
        text: 'false'
    }
];

const FLOW_TYPE = [
    {
        value: false,
        text: 'No. (You will pay full cost for Exchange health coverage.)'
    },
    {
        value: true,
        text: 'Yes. (You will have to provide income information to see what you may qualify for.)'
    }
];

const FREQUENCY = [
    {
        value: 'default',
        text: 'Frequency'
    },
    {
        value: FREQUENCY_TYPES.HOURLY,
        text: 'Hourly'
    },
    {
        value: FREQUENCY_TYPES.DAILY,
        text: 'Daily'
    },
    {
        value: FREQUENCY_TYPES.WEEKLY,
        text: 'Weekly'
    },
    {
        value: FREQUENCY_TYPES.BIWEEKLY,
        text: 'Every 2 weeks'
    },
    {
        value: FREQUENCY_TYPES.BIMONTHLY,
        text: 'Twice a month'
    },
    {
        value: FREQUENCY_TYPES.MONTHLY,
        text: 'Monthly'
    },
    {
        value: FREQUENCY_TYPES.QUARTERLY,
        text: 'Quarterly'
    },
    {
        value: FREQUENCY_TYPES.YEARLY,
        text: 'Yearly'
    },
    {
        value: FREQUENCY_TYPES.ONCE,
        text: 'One time only'
    },
];

export const FREQUENCY_LIST = Object.assign(
    {},
    ...FREQUENCY.map(({ value, text }) => ({
        [value]: text
    }))
);


const LANGUAGE = [
    {
        value: 'default',
        text: 'Language'
    },
    {
        value: 'English',
        text: 'English'
    },
    {
        value: 'Spanish',
        text: 'Spanish'
    }
];

const NUMBER_OF_YEARS = [
    {
        value: 1,
        text: '1 year'
    },
    {
        value: 2,
        text: '2 years'
    },
    {
        value: 3,
        text: '3 years'
    },
    {
        value: 4,
        text: '4 years'
    },
    {
        value: 5,
        text: '5 years'
    },
    {
        value: 0,
        text: 'Do Not Renew'
    }
];

const QUALIFYING_EVENT = [
    {
        value: 'default',
        text: 'Event'
    },
    {
        value: 'CHANGE_IN_LEGAL_PRESENCE',
        text: 'Change in Immigration Status'
    },
    {
        value: 'CHANGE_IN_INCARCERATION',
        text: 'Change in Incarceration Status'
    },
    {
        value: 'CHANGE_IN_INCOME',
        text: 'Change in Income'
    },
    {
        value: 'BIRTH_ADOPTION',
        text: 'Change in Your Dependants'
    },
    {
        value: 'MOVED_INTO_STATE',
        text: 'Change of Address'
    },
    {
        value: 'DEATH',
        text: 'Death'
    },
    {
        value: 'DIVORCE',
        text: 'Divorce'
    },
    {
        value: 'LOST_OTHER_MIN_ESSENTIAL_COVERAGE',
        text: 'Loss of Minimum Essential Coverage'
    },
    {
        value: 'MARRIAGE',
        text: 'Marriage'
    },
    {
        value: 'CHANGE_IN_AMERICAN INDIAN / ALSAKA NATIVE STATUS',
        text: 'Native Americans'
    },
];

const PROFIT_LOSS = [
    {
        value: 'default',
        text: 'Select'
    },
    {
        value: true,
        text: 'Profit'
    },
    {
        value: false,
        text: 'Loss'
    }
];

export const RECONCILE_YES_NO = [
    {
        value: true,
        text: 'Yes, I have reconciled premium tax credits OR have never received premium tax credits in past years'
    },
    {
        value: false,
        text: 'No, I did not reconcile premium tax credits for past years'
    }
];

const REASONABLE_EXPLANATIONS_FOR_NO_SSN = [
    {
        value: 'default',
        text: 'Explanation'
    },
    {
        value: 'RELIGIOUS_EXCEPTION',
        text: 'Religious Exception'
    },
    {
        value: 'JUST_APPLIED',
        text: 'Just Applied'
    },
    {
        value: 'ILLNESS_EXCEPTION',
        text: 'Illness Exception'
    },
    {
        value: 'CITIZEN_EXCEPTION',
        text: 'Citizen Exception'
    }
];

export const REASONABLE_EXPLANATIONS_LIST = Object.assign(
    {},
    ...REASONABLE_EXPLANATIONS_FOR_NO_SSN.map(({ value, text}) => ({
        [value]: text
    }))
);

const SIGNATURE = [
    {
        value: false,
        text: 'I am not able to complete an electronic signature. ' +
            '(If you select this option, you will need to upload a document to prove ' +
            'that there is a legal reason why you cannot sign.)\n'
    },
    {
        value: true,
        text: 'Signature'
    }
];

const STATE = [                         // Check if no usages and delete
    ...STATES_LIST.map(state => ({
        text: state.stateName,
        value: state.stateCode
    }))
];

const STATES_FOR_TRIBES = tribesData => {
    if(CONFIG.ENV.DEVELOPMENT) {
        return [
            {
                value: 'default',
                text: 'State'
            },
            ...STATES_LIST.filter(s => s.stateCode === 'AL').map(state => ({
                text: state.stateName,
                value: state.stateCode
            }))
        ];
    } else {
        return [
            {
                value: 'default',
                text: 'State'
            },
            ...tribesData.map(state => ({
                text: state.name,
                value: state.code
            }))
        ];
    }
};


export const getOptionsForHardcodedState = stateCode => {
    const state = STATES_LIST.find(state => state.stateCode === stateCode);
    return [{
        text: state.stateName,
        value: state.stateCode
    }];
};

export const getDynamicDropdownNumberOption = (inputNumber, numberLimitForDropdown) => {
    const setLimit = (inputNumber <= numberLimitForDropdown ? inputNumber : numberLimitForDropdown);
    const populatedList = Array(setLimit).fill(0).map((e,i)=>({'text':i+1, 'value': i+1}));
    return [
        {
            value: 'default',
            text: 'Age'
        },
        ...populatedList
    ];
};

const SPOUSE_ON_APPLICATION = [
    {
        value: true,
        text: 'Someone already on the application'
    },
    {
        value: false,
        text: 'Someone else who isn\'t applying for health coverage'
    }
];


const SUFFIX = [
    {
        value: 'default',
        text: 'Suffix'
    },
    {
        value: 'Jr.',
        text: 'Jr.'
    },
    {
        value: 'Sr.',
        text: 'Sr.'
    },
    {
        value: 'III',
        text: 'III'
    },
    {
        value: 'IV',
        text: 'IV'
    }
];

const TRIBES = (tribes, stateCode) => {
    if(CONFIG.ENV.DEVELOPMENT) {
        return [
            {
                value: 'default',
                text: 'Tribe'
            },
            {
                value: 'AL010001',
                text: 'Poarch Band of Creek Indians of Alabama'
            },
        ];
    } else {
        const neededState = tribes.find(state => state.code === stateCode);
        return [
            {
                value: 'default',
                text: 'Tribe Name'
            },
            ...(neededState ? neededState.tribes.map(tribe => ({
                value: tribe.tribeCode,
                text: tribe.tribeName
            })) : [])
        ];
    }
};


export const YES_NO = [
    {
        value: true,
        text: 'Yes'
    },
    {
        value: false,
        text: 'No'
    }
];

export const FILING_JOINTLY_YES_NO = [
    {
        value: 'jointly',
        text: 'Yes'
    },
    {
        value: 'separately',
        text: 'No'
    }
];


export default {
    AGREE_DISAGREE,
    AGREE_DISAGREE_STMT,
    AUTHORIZED_REPRESENTATIVES,
    BLOOD_RELATIONSHIP,
    COMMUNICATION_METHOD,
    COUNTIES,
    COUNTRIES,
    DEDUCTION_SOURCES,
    GENDER,
    INCOME_SOURCES,
    INCOME_SOURCES_TRIBAL_INCOME,
    FILING_JOINTLY_YES_NO,
    FLAGS_OPTIONS,
    FLOW_TYPE,
    FREQUENCY,
    LANGUAGE,
    NUMBER_OF_YEARS,
    OTHER_INCOME_SUBTYPES,
    PROFIT_LOSS,
    REASONABLE_EXPLANATIONS_FOR_NO_SSN,
    QUALIFYING_EVENT,
    RECONCILE_YES_NO,
    SIGNATURE,
    SPOUSE_ON_APPLICATION,
    STATE,
    STATES_FOR_TRIBES,
    SUFFIX,
    TRIBES,
    YES_NO
};
