import React from 'react';
import { FormattedMessage } from 'react-intl';

function FurtherAction ({data, onClickHandler}) {

    const { btnText, processedParagraphText } = data;

    return (data &&
        <div className="ds-u-margin--2 ds-u-border--2 ds-u-fill--gray-lightest" id="auto-further-action">
            <div className="ds-l-row ds-u-margin--1">
                <p className="ds-u-font-weight--semibold" id="further-action-header">
                    <FormattedMessage id='further_action.paragraph_header' />
                </p>
            </div>
            <div className="ds-l-row ds-u-margin--1">
                <p className="ds-text" id="further-action-txt">
                    { processedParagraphText }
                </p>
            </div>
            {btnText &&
            <div className="ds-u-clearfix">
                <div className="ds-u-lg-float--right">
                    <div className="ds-l-row ds-u-margin--1">
                        <button
                            type="button"
                            id="further-action-btn"
                            className="btn btn-primary col-xs-12"
                            onClick={ () => onClickHandler({data}) }
                        >
                            <FormattedMessage id={btnText} />
                        </button>
                    </div>
                </div>
            </div>
            }
        </div>
    )
}

export default FurtherAction;