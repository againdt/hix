<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<!-- File upload scripts -->

<style>
	#titlebar:after {
		border-bottom: none;
	}
</style>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/verifydependent.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/modal-zipcode-utils.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ajaxfileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<script type="text/javascript"> window.history.forward(); </script>


<script type="text/javascript">
var ctx = "${pageContext.request.contextPath}";
var calenderimagepath = ctx+'/resources/images/calendar.gif';
var locationJSON = ${locationJSON};
var locationString = locationJSON['address']+", "+locationJSON['city'];
var homeLocation = "";
var dataCount;
var employeeId = ${employee.id};
var dependentCount = ${employee.dependentCount};
var isMarried = '${employee.isMarried}';
var empGender = '${employeeDetailsSelf.gender}';
var selectedLocation = "homeAddress";
//var orgSSNvalAssigned=false;
var tempFileName = '';
var wardDocumentRequired = '${wardDocumentRequired}';
var isNewDepe = false;
var hasEditedSsn = false;
function editSsn(){
	$("#showEncrSsn").hide();
	$("#ssn1").show();
	$("#ssn2").show();
	$('#ssn3').show();
	hasEditedSsn = true;
}

jQuery.validator.addMethod("warddoccheck", function(value, element,param ){
	$('#wardDocument_error').html("");	
	var fileName = $('#wardDocument').val();
    if($('#type').val()=='WARD' && wardDocumentRequired == 'YES' && (fileName === undefined || fileName == null || fileName == 'none' || fileName == "")){
		 $('#wardDocument_error').show();
		 return false;
	}
	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
	var today=new Date();
	var dob = $("#dob").val();
    dob_arr = dob.split(/\//);
	dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() <= birthDate.getTime()){ return false; }
	return true;
});

jQuery.validator.addMethod("namecheck", function(value, element, param) {
  	if(namecheck(value) == false){
	  	return false;	
	}
	return true;
});

jQuery.validator.addMethod("duplicateName", function(value, element, param) {
	currentName=$.trim($("#firstName").val())+$.trim($("#lastName").val())+$.trim($("#suffix").val());
	currentId=$.trim($("#id").val());
		 for(var i=1;i<=dataCount;i++){
	         if($("#dependent_"+i+"_id").val() != null && $("#dependent_"+i+"_id").val() != currentId){
	        	 existingFirstName=$.trim($("#dependent_"+i+"_firstName").val());
	        	 existingLastName=$.trim($("#dependent_"+i+"_lastName").val());
	        	 existingSuffix=$.trim($("#dependent_"+i+"_suffix").val());
	        	 if(existingFirstName.length >0 && existingLastName.length >0 ){
	        		 existingName=existingFirstName+existingLastName+existingSuffix;
	        		 if(currentName.toLowerCase() == existingName.toLowerCase()){
						return false;
					 }
	        	 } 
	        	}
	         }
		return true;
	});
	
jQuery.validator.addMethod("duplicateSSN", function(value, element, param) {
	dependentId=$.trim($("#id").val());
	var currentSSN="";
	ssn1 = $.trim($("#ssn1").val()) ; 
	ssn2 = $.trim($("#ssn2").val()) ; 
	ssn3 = $.trim($("#ssn3").val()) ;
	
	/* if(!orgSSNvalAssigned){
		ssn1 = $.trim($("#org_ssn1").val());
		ssn2 = $.trim($("#org_ssn2").val());
	} */
	currentSSN=ssn1+'-'+ssn2+'-'+ssn3;
	csrfParameter ='<c:url value="">
				<c:param name="${df:csrfTokenParameter()}"> 
					<df:csrfToken plainToken="true" />
				</c:param> 
			</c:url>';
			
	return isUniqueSSN(currentSSN,employeeId,dependentId,'dependent',csrfParameter);
	
	});
	
jQuery.validator.addMethod("ssn1check", function(value, element, param) {
	ssn1 = $.trim($("#ssn1").val()) ;
	/* if(!orgSSNvalAssigned){
		ssn1 = $.trim($("#org_ssn1").val());
	} */
	if ((ssn1 == "" || (isNaN(ssn1)))){
		return true;
	}
	else{
		return areaCodeCheck(ssn1);	
	}
	 
	
 });

jQuery.validator.addMethod("invalidssn1", function(value, element, param) {
		ssn1 = $.trim($("#ssn1").val()) ; 
		ssn2 = $.trim($("#ssn2").val()) ; 
		ssn3 = $.trim($("#ssn3").val()) ;
		
		/* if(!orgSSNvalAssigned){
			ssn1 = $.trim($("#org_ssn1").val());
			ssn2 = $.trim($("#org_ssn2").val());
		} */
		if ((ssn1 == "" && ssn2 == "" && ssn3 == "") || ((isNaN(ssn1)) && (isNaN(ssn2)) && (isNaN(ssn3))))
		{
			return true;
		}
		else{
			return notAllowedSSN1(ssn1,ssn2,ssn3);
		}
	 	
	 });
	 
jQuery.validator.addMethod("invalidssn2", function(value, element, param) {
	ssn1 = $.trim($("#ssn1").val()) ; 
	ssn2 = $.trim($("#ssn2").val()) ; 
	ssn3 = $.trim($("#ssn3").val()) ;
	
	/* if(!orgSSNvalAssigned){
		ssn1 = $.trim($("#org_ssn1").val());
		ssn2 = $.trim($("#org_ssn2").val());
	} */
	if ((ssn1 == "" && ssn2 == "" && ssn3 == "") || ((isNaN(ssn1)) && (isNaN(ssn2)) && (isNaN(ssn3))))
	{
		return true;
	}
	else{
		return notAllowedSSN2(ssn1,ssn2,ssn3);
	}
 	
 });
 
jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	ssn1 = $.trim($("#ssn1").val()) ; 
	ssn2 = $.trim($("#ssn2").val()) ; 
	ssn3 = $.trim($("#ssn3").val()) ;
	
	/* if(!orgSSNvalAssigned){
		ssn1 = $.trim($("#org_ssn1").val());
		ssn2 = $.trim($("#org_ssn2").val());
	} */
	var flag=false;
	if ((ssn1 == "" && ssn2 == "" && ssn3 == "") || ((isNaN(ssn1)) && (isNaN(ssn2)) && (isNaN(ssn3))))
	{
		flag=true;
	}
	else{
		flag=validateSSN(ssn1,ssn2,ssn3);
	}
		
 	return flag;
});

function getCurrentDate(){
	var currentDate = new Date();
   var year = currentDate.getFullYear();
   var currMonth = currentDate.getMonth()+1;
   var month = (currMonth > 9 ) ? currMonth : '0' + currMonth; 
   var day = currentDate.getDate();

   var today = month + '/' + day + '/' + year ;
   
   return today;
}
 
 function shiftbox(element, nextelement) {
	maxlength = parseInt(element.getAttribute('maxlength'));
	if (element.value.length == maxlength) {
		nextelement = document.getElementById(nextelement);
		nextelement.focus();
	}
}

function validateEmployeeDetails(){
	if( $("#frmemployeedetails").validate().form() ) {

		var isSsnHidden =  $("#ssn1").is(':hidden');
		if(!isSsnHidden){
			var ssn = $.trim($("#ssn1").val()) + "-" + $.trim($("#ssn2").val()) + "-" + $.trim($("#ssn3").val());
			
			 if(hasEditedSsn || isNewDepe ){
				hasEditedSsn = false;
				isNewDepe = false;
			}  
			$("#editedSsn").val("true");
			$('#ssn').val(ssn);
		} else{
			var ssn = document.getElementById('showEncrSsn').innerHTML;
			$('#ssn').val(ssn);
		}
		//orgSSNvalAssigned=false;
		submitEmployeeDetails(null,null);
	}
	
}
 
/* function validateHomeAddressWS(){
	var enteredAddress = $("#address1").val()+","+$("#address2").val()+","+$("#city").val()+","+$("#state").val()+","+$("#zip").val()+ ", " +  $("#lat").val() + "," + $("#lon").val();
	var idsText='address1~address2~city~state~zip~lat~lon';
	$.ajax({
		url: "/hix/platform/validateaddress",
	    data: {enteredAddress: enteredAddress,
	    		ids: idsText},
	    success: function(data){
	   		var href = '/hix/platform/address/viewvalidaddress';
	   		$('#addressIFrame').remove(); //remove any present modal 
	   		if(data=="SUCCESS"){
	   			$('<div class="modal-backdrop fade in addressErrModal"></div><div class="modal" id="addressIFrame" tabindex="-1" role="dialog"><div class="modal-header" style="border-bottom:0;"></div><div class="modal-body"><iframe id="modalData" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:380px;"></iframe></div><div class="modal-footer"><input type="button" value="Close" class="btn" id="iFrameClose" aria-hidden="true"></div></div>').modal();
	   			$('#iFrameClose').bind("click", submitLocation);
	   		}else{
	   			submitLocation();
	   		}
	    }
	});
} */

function submitLocation(){

	//var countyTmp = $('#county').val().split('#');
  var subUrl = '<c:url value="/shop/employee/addHomeAddress">
                  <c:param name="${df:csrfTokenParameter()}"> 
                    <df:csrfToken plainToken="true" />
                  </c:param> 
                </c:url>';
	
	$.ajax(
			{
        		type: "POST",
       			url: subUrl,
        		//data: "id=&address1="+$("#address1").val()+"&address2="+$("#address2").val()+"&city="+$("#city").val()+"&state="+$("#state").val()+"&zip="+$("#zip").val()+"&county="+countyTmp[0]+"&countycode="+countyTmp[1] ,
          		data: "id=&address1="+$("#address1").val()+"&address2="+$("#address2").val()+"&city="+$("#city").val()+"&state="+$("#state").val()+"&zip="+$("#zip").val()+"&county="+$("#county").val() ,
        		dataType:'json',
        		success: function(response, xhr){
			        if(isInvalidCSRFToken(xhr))
			          return;

        			if(response['error'] != undefined){
        				alert(response['error']);
        			}else{
        				locationsObj[response['id']] = response['address']+", "+response['city'];
        				submitEmployeeDetails(response['id'],$("#address1").val() + ", " + $("#city").val());
        			}
        		},
       			error: function(e){
        			alert("<spring:message code='label.failedtoaddnewaddr' javaScriptEscape='true'/>");
        		}
        });
}

function submitEmployeeDetails(locId,locName){
	var locId = $('#location_id').val();
	var locName = $('#address1').val()+' '+$('#city').val();
	
	$('input[id=isEmployeeAddressYes]').val('Y');
	$('input[id=isEmployeeAddressNo]').val('N');
	$('input[id=genderMale]').val('MALE');
	$('input[id=genderFemale]').val('FEMALE');
	$('input[id=smokerYes]').val('YES');
	$('input[id=smokerNo]').val('NO');	
	$('input[id=tobaccoCessPrgYes]').val('YES');		
	$('input[id=tobaccoCessPrgNo]').val('NO');
	$('input[id=nativeAmrYes]').val('YES');
	$('input[id=nativeAmrNo]').val('NO');		

  var subUrl = '<c:url value="/shop/employee/editEmployeeDetails/${employeeApplication.id}">
                  <c:param name="${df:csrfTokenParameter()}"> 
                    <df:csrfToken plainToken="true" />
                  </c:param> 
                </c:url>';

	$.ajax(
			{
        		type: "POST",
       			url: subUrl,
        		data: $("#frmemployeedetails").serialize(),
        		dataType:'json',
    			beforeSend: function( e ) {
    				$('#submitbutton').attr("disabled", true);
    			},
        		success: function(response,xhr){
			        if(isInvalidCSRFToken(xhr))
			          return;
        			$('#editdependent').modal('hide');
        			$("#submitbutton").attr("disabled", false);
        			if($("#id").val() == 0 && $("#CN").val() == 0){
        					var temp =$('#county').val().split('#');
        					if(tempFileName!=''){
        						Dependent.add_dependent(response['id'],$("#type").val(), calenderimagepath, $("#firstName").val(), $("#lastName").val(), $("#dob").val(), response['locId'], locName, $('input[name=gender]:radio:checked').val(), $("#ssn").val(),$('input[name=smoker]:radio:checked').val(),$('input[name=tobaccoCessPrg]:radio:checked').val() ,$('input[name=nativeAmr]:radio:checked').val(),$("#middleInitial").val(),$('#address1').val(),$('#address2').val(),$('#city').val(),$('#state').val(),temp[0],$('#zip').val(),$('input[name=isEmployeeAddress]:radio:checked').val(),$('#suffix').val(), $('#relationship').val(),$('#wardDocument').val()+'#'+tempFileName,wardDocumentRequired);
        						tempFileName='';
        					}else{
        						Dependent.add_dependent(response['id'],$("#type").val(), calenderimagepath, $("#firstName").val(), $("#lastName").val(), $("#dob").val(), response['locId'], locName, $('input[name=gender]:radio:checked').val(), $("#ssn").val(),$('input[name=smoker]:radio:checked').val(),$('input[name=tobaccoCessPrg]:radio:checked').val() ,$('input[name=nativeAmr]:radio:checked').val(),$("#middleInitial").val(),$('#address1').val(),$('#address2').val(),$('#city').val(),$('#state').val(),temp[0],$('#zip').val(),$('input[name=isEmployeeAddress]:radio:checked').val(),$('#suffix').val(), $('#relationship').val(),$('#wardDocument').val(),wardDocumentRequired);
        					}
        						
	      			}else{
        				Dependent.edit_dependentUI($("#CN").val(),response['id'],response['locId'],locName);	
        			}        		
        			$("#editedSsn").val("false");
        			$('#county').html('<option value="">Select County...</option>');
        			$('.modal-backdrop').remove();
        		},
       			error: function(e){
        			$("#submitbutton").attr("disabled", false);       				
        			alert("<spring:message code='label.failedtoaddemployer' javaScriptEscape='true'/>");
        			$("#editedSsn").val("false");
        		}
        });
}

function deleteEmployeeDetails(){
	var subUrl = '<c:url value="/shop/employee/deleteEmployeeDetails/${employeeApplication.id}">
                  <c:param name="${df:csrfTokenParameter()}"> 
                    <df:csrfToken plainToken="true" />
                  </c:param> 
                </c:url>';
	$.ajax(
			{
        		type: "POST",
       			url: subUrl,
        		data: $("#frmemployeedetails").serialize(),
        		dataType:'json',
        		success: function(response,xhr){
			        if(isInvalidCSRFToken(xhr))
			          return;
        			$('#deletedependent').modal('hide');
        			$('#deleteEmployeeButton').unbind('click');
        			if(response['status'] != null){
        				Dependent.delete_dependentUI($("#CN").val());
        				if(($("#type").val() == 'SPOUSE') || ($("#type").val() == 'LIFE_PARTNER')){
        					Dependent.add_spouseBtn();	
        				}
        			}else{
        				if(response['error'] != null){
        					alert(response['error']);
        				}
        			}
        		
        		},
       			error: function(e){
        			alert("<spring:message code='label.failedtodeleteemployee' javaScriptEscape='true'/>");
        		}
        });
}

function addressCheck(eleValue){
    var regex = /^[0-9a-zA-Z\.\s\-'#,\\]*$/;
    if(!regex.test(eleValue) ){
           return false; 
    }
    return true;
}

function address2Check(eleValue){
    var regex =  /^[0-9a-zA-Z\.\s\-'#,\\]*$/;
   if(eleValue != ""){
           if(!regex.test(eleValue) ){
                  return false; 
           }
    }
    return true;
}

function checkMissingDependents(){
    var missingNames = "";
    
    for(var i=1;i<=dataCount;i++){
           if($("#dependent_"+i+"_id").val() != null){
                  
                  if(($("#dependent_"+i+"_gender").val() == '' || $("#dependent_"+i+"_gender").val() == 'N/A') 
                  || ($("#dependent_"+i+"_smoker").val() == '' || $("#dependent_"+i+"_smoker").val() == 'N/A') 
                  || ($("#dependent_"+i+"_nativeAmr").val() == '' || $("#dependent_"+i+"_nativeAmr").val() == 'N/A')
                  || !(namecheck($("#dependent_"+i+"_firstName").val()))
                  || !(namecheck($("#dependent_"+i+"_lastName").val()))
                  || !(addressCheck($("#dependent_"+i+"_address1").val()))
                  || !(address2Check($("#dependent_"+i+"_address2").val()))
                  || !(address2Check($("#dependent_"+i+"_city").val()))
                  ){
                        missingNames = missingNames + $("#dependent_"+i+"_NameSpan").html() + ", ";
                  }
           }
    }
    if(missingNames.length > 0){
           missingNames = missingNames.substring(0,missingNames.length-2);
           $("#missingnames").html(missingNames);
           $("#missinginfo").modal('show');
    }else{
    	window.location.assign('<c:url value="/shop/employee/employeeselectplan/${employeeApplication.id}" />'); 
    }
}

function postPopulateIndex(zipCodeValue, zipId){
	//console.log(zipCodeValue, zipId);
	//alert(" called parent's getCounty with values " + indexValue + " "+zipCodeValue); 
	jQuery('#'+zipId).parents('.control-group').next().find('select').focus();
	getCountyList( zipCodeValue, '' );
}


function getCountyList(zip, county) {
  var subUrl = '<c:url value="/shop/employee/addCounties">
                  <c:param name="${df:csrfTokenParameter()}"> 
                    <df:csrfToken plainToken="true" />
                  </c:param> 
                </c:url>';

	$.ajax({
		type : "POST",
		url : subUrl,
		data : {zipCode : zip}, 
		dataType:'json',
		success : function(response,xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			populateCounties(response, county);
		},error : function(e) {
			alert("<spring:message code='label.failedtoaddcounty' javaScriptEscape='true'/>");
		}
	});
}

function populateCounties(response, county) {
    var optionsstring = '<option value="">Select County...</option>';
		var i =0;
		$.each(response, function(key, value) {
			var selected = (county == key) ? 'selected' : '';
			var optionVal = key+'#'+value;
			var options = '<option value="'+optionVal+'" '+ selected +'>'+ key +'</option>';
			optionsstring = optionsstring + options;
			i++;
		});
		$('#county').html(optionsstring);
} // end populateCounties

function isAgeLess(param) {
	var dob = $("#dob").val();
    dob_arr = dob.split(/\//);
    dob_mm = dob_arr[0]; dob_dd = dob_arr[1]; dob_yy = dob_arr[2];
	var today=new Date();
	var birthDate=new Date();
	year = parseInt(dob_yy)+ parseInt(param);
	birthDate.setFullYear(year ,dob_mm - 1,dob_dd);
	if(today.getTime() <= birthDate.getTime()){ return false; }
	return true;
};

function checkChildSsnValidity(ssn1 , ssn2, ssn3){
	var filter = /^[0-9]+$/;
	if(ssn1 != "" || ssn2 != "" || ssn3 != ""){
		if ( (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3)) || (ssn1.length != 3 || ssn2.length != 2 || ssn3.length != 4)
				|| (!filter.test(ssn1) || !filter.test(ssn2) || !filter.test(ssn3))) {
			return false;
		}else{
				return true;
			} 
	}else{
		return true;				
	}
}

function changeType(val){
	if(val !='' && (val=='SPOUSE' || val=='LIFE_PARTNER')){
		$('#type').val(val);
	}
}

jQuery.validator.addMethod("zipcheck", function(value, element, param) {
  	elementId = element.id; 
	if( $("#"+elementId).val().length != 5 ||  isNaN( $("#"+elementId).val() ) ){
  		return false;	
  	}return true;
});

jQuery.validator.addMethod("address1Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
	if(!regex.test($("#"+elementId).val()) ){
  		return false;	
  	}
	return true;
});
jQuery.validator.addMethod("address2Check", function(value, element, param) {
	var regex = /^[0-9a-zA-Z\s\#\-\,\\\.]+$/;
  	elementId = element.id; 
  	
  	if($("#"+elementId).val()!=""){
		if(!regex.test($("#"+elementId).val()) ){
	  		return false;	
	  	}
	}
	return true;
});
jQuery.validator.addMethod("address2Match", function(value, element, param) {
  	elementId = element.id;	  	
  	if($.trim($("#"+elementId).val())!="" && $.trim($("#address1").val())!=""){	  		  	
  		if($("#"+elementId).val() == $("#address1").val()){
	  		return false;	
		}
  	}
	return true;
});
jQuery.validator.addMethod("agecheck", function(value, element, param) {
	if($('#type').val()=='CHILD'){
		var dob = $("#dob").val();
		var today = getCurrentDate();
		var age = Math.floor((new Date(today) - new Date(dob))/(3600*24*365*1000));
		if(age>=26){
			return false;
		}
	}
	return true;
});

jQuery.validator.addMethod("wardAgeCheck", function(value, element, param) {
	if($('#type').val()=='WARD'){
		var dob = $("#dob").val();
		var today = getCurrentDate();
		var age = Math.floor((new Date(today) - new Date(dob))/(3600*24*365*1000));
		if(age<26){
			return false;
		}
	}
	return true;
});

jQuery.validator.addMethod("relationCheck", function(value, element, param) {
	if($('#type').val()=='SPOUSE'){
		if($('#relationship').val()==''){
			return false;
		}
	}
	return true;
});
</script>

<div class="gutter10">
	<div class="topnav">
  		<ul class="inline">
    		<li><a href="#">Verify Information</a></li>
    		<li class="active"><a href="#">Update Dependents</a></li>
    		<li><a href="#">Select Plans</a></li>
    		<li><a href="#" class="last">Enroll</a></li>
  		</ul>
	</div> 
  	<div class="row-fluid" id="titlebar">
          <h1 class="offset3"><spring:message  code="label.verifydependentinfo"/></h1>
	</div>
	
      <div class="row-fluid">
        <div class="span3" id="sidebar"></div>
        <!-- /.sidebar-->
        
        <div class="span9" id="rightpanel">
          <div class="row-fluid">
              <p><spring:message  code="label.dependentinfo"/></p>
              <form class="form-horizontal" id="frmverifydependents" name="frmverifydependents" action="editdependentsSubmit" method="post">
              <df:csrfToken/>
                <table id="employeeSpouseTable" class="table">
                  <tbody>
                    <tr class="header">
                      <td style="width: 120px;"><strong><spring:message  code="label.emprname"/></strong></td>
                      <td style="width: 135px;"><strong><spring:message  code="label.emplRelationship"/></strong></td>
                      <td style="width: 70px;"><strong><spring:message  code="label.gender"/></strong></td>
                      <td style="width: 110px;"><strong><spring:message  code="label.dob"/></strong></td>
                      <td style="width: 110px;"><strong><spring:message  code="label.verifydeptaxidSSN"/></strong></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td style="width:84px">&nbsp;</td>
                    </tr>
				</tbody>
                </table>
                
                <br/>
                
                <table id="employeeChildTable" class="table">
                  <tbody>
                    <tr class="header">
                      <td style="width: 120px;"><strong><spring:message  code="label.emprname"/></strong></td>
                      <td style="width: 135px;"><strong><spring:message  code="label.emplRelationship"/></strong></td>
                      <td style="width: 70px;"><strong><spring:message  code="label.gender"/></strong></td>
                      <td style="width: 110px;"><strong><spring:message  code="label.dob"/></strong></td>
                      <td style="width: 110px;"><strong><spring:message  code="label.verifydeptaxidSSN"/></strong></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td style="width:84px">&nbsp;</td>
                    </tr>
                  </tbody>
                  <tfoot>
                  <tr>
                      <td><a tabindex="0" class="btn btn-mini btn-primary" onclick="javascript:Dependent.edit_dependent('0','CHILD','true', wardDocumentRequired)" title="Add Child" data-toggle="modal"><i class="icon-plus-sign"></i> <spring:message  code="label.addchild"/></a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                       <td>&nbsp;</td>
                  </tr>
                  <tr>
                      <td><a tabindex="0" class="btn btn-mini btn-primary" onclick="javascript:Dependent.edit_dependent('0','WARD','true', wardDocumentRequired)" title="A ward is a disabled dependent above the age of 26" data-toggle="modal"><i class="icon-plus-sign"></i> <spring:message  code="label.addward"/></a></td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </tfoot>
                </table>
                </form>
                <!-- modal edit dependent-->
                <div id="editdependent" class="modal hide fade" tabindex="-1" role="dialog"  aria-labelledby="Addanewaworksite" aria-hidden="true">
				<form class="form-horizontal margin0 addressValidator"  id="frmemployeedetails" name="frmemployeedetails">
	              <df:csrfToken/>
				<div id="form-input-data" class="addressBlock">
					<input type="hidden" name="id" id="id" />
					<input type="hidden" name="type" id="type" />
					<input type="hidden" name="ssn" id="ssn"  />
					<input type="hidden" name="CN" id="CN" /> 
					<input type="hidden" name="employee.id" id="employee_id" />
					<input type="hidden" name="location.id" id="location_id" />
					<input type="hidden" name="wardDocument" id="wardDocument" value="" >
					<input type="hidden" name="editedSsn" id="editedSsn" value="false" >
	                  <div class="modal-header">
	                    <button type="button" class="removeModal close clearForm" data-dismiss="modal"
										aria-hidden="true" onClick="javascript:clearvars();" >�</button>
	                    <h3 id="modalTitle"></h3>
	                  </div>
	                  <div class="modal-body">
	                    <div class="control-group">
	                      <label for="firstName" class="control-label"><spring:message  code="label.emplFirstName"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                      <div class="controls">
	                        <input type="text" id="firstName" name="firstName" class="input-medium error" tabindex="0">
								<div id="firstName_error" class="error help-inline"></div>	
	                      </div>
	                    </div>
	                    
	                    <div class="control-group">
	                      <label for="middleInitial" class="control-label"><spring:message  code="label.emplMiddleInitial"/></label>
	                      <div class="controls">
	                        <input type="text" id="middleInitial" name="middleInitial" class="input-medium" tabindex="0" size="1" maxlength="1">
	                        <div id="middleInitial_error" class="error help-inline"></div>
	                      </div>
	                    </div>
	                    
	                    <div class="control-group">
	                      <label for="lastName" class="control-label"><spring:message  code="label.emplLastName"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                      <div class="controls">
	                        <input type="text" id="lastName" name="lastName" class="input-medium error" tabindex="0">
							<div id="lastName_error" class="error help-inline"></div>	
	                      </div>
	                    </div>
	                    <div class="control-group">
	                      <label for="suffix" class="control-label"><spring:message  code="label.suffix"/> </label>
	                      <div class="controls">
	                        <select class="input-medium" name="suffix" id="suffix">
								<option value=""></option>
								<c:forEach var="suffix" items="${suffixlist}">
									<option value="${suffix}">${suffix}</option>
								</c:forEach>
							</select>
							<div id="suffix_error" class="error help-inline"></div>	
	                      </div>
	                    </div>
	                    <div class="control-group" id="relationshipDiv">
	                      <label for="relationship" class="control-label"><spring:message  code="label.emplRelationship"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                      <div class="controls">
	                        <select id="relationship" name="relationship" onchange="changeType(this.value);">
	                        	<option value="">Select...</option>
								<option value="SPOUSE">Spouse</option>
								<option value="LIFE_PARTNER">Life Partner</option>	                        
	                        </select>
							<div id="relationship_error" class="error help-inline"></div>	
	                      </div>
	                    </div>
	                    <div class="control-group">
	                      <label for="employeeAddress" class="control-label"><spring:message  code="label.emplAddress"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                      <div class="controls">
	                        <input type="radio" id="isEmployeeAddressYes" name="isEmployeeAddress" class="input-medium error" tabindex="0" value="Y" onclick="showHideAddressDiv('Y');">YES
	                        <input type="radio" id="isEmployeeAddressNo" name="isEmployeeAddress" class="input-medium error" tabindex="0" value="N" onclick="showHideAddressDiv('N');" checked="checked">NO
							<div id="employeeAddress_error" class="error help-inline"></div>	
	                      </div>
	                    </div>
	<!-- Address Section Start 	 -->
					<div id="addressDiv">
	                    <input type="hidden" value="0.0" id="lat" name="lat" tabindex="0">
						<input type="hidden" value="0.0" id="lon" name="lon" tabindex="0">
						<input type="hidden" value="" id="rdi" name="rdi" tabindex="0">
						<div class="control-group">
						<label for="address1" class="control-label"><spring:message  code="label.homeAddress1"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                        <div class="controls">
	                          <input type="text" name="location.address1" id="address1" tabindex="0">
	                          <span id="address1_error"></span>
	                             <input type="hidden" name="address1_hidden" id="address1_hidden" tabindex="0">
	                        </div>
	                      </div>
	                      
	                      <div class="control-group">
	                      <label for="address2" class="control-label"><spring:message  code="label.homeAddress2"/> </label>
	                        <div class="controls">
	                          <input type="text" name="location.address2" id="address2" tabindex="0">
	                           <span id="address2_error"></span>
	                           <input type="hidden" name="address2_hidden" id="address2_hidden" tabindex="0">
	                        </div>
	                      </div>
	                      
	                      <div class="control-group">
	                      <label for="city" class="control-label"><spring:message  code="label.emplCity"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                        <div class="controls">
	                          <input type="text" name="location.city" id="city" tabindex="0">
	                          <span id="city_error"></span>
	                          <input type="hidden" name="city_hidden" id="city_hidden" tabindex="0">
	                        </div>
	                      </div>
	                      
	                      <div class="control-group">
	                      <label for="state" class="control-label"><spring:message  code="label.emplState"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                        <div class="controls">
	                      <select class="input-medium" name="location.state" id="state">
							<option value="">Select</option>
							<c:forEach var="state" items="${statelist}">
								<option id="${state.code}" <c:if test="${state.code == empDetails.location.state}"> SELECTED </c:if>
								value="${state.code}">${state.name}</option>
							</c:forEach>
							</select>
		                    <span id="state_error"></span>
								<input type="hidden" name="state_hidden" id="state_hidden" tabindex="0">
	                        </div>
	                      </div>
	                      
	                      <div class="control-group">
	                      <label for="zip" class="control-label"><spring:message  code="label.zip"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                        <div class="controls">
	                            <input type="text" maxlength="5" id="zip" name="location.zip" class="input-small zipCode" tabindex="0">
	                      	    <span id="zip_error"></span>
	                      	     <input type="hidden" name="zip_hidden" id="zip_hidden" value="0.0" tabindex="0">
	                        </div>
	                      </div>
	                      
	                      <div class="control-group">
	                      	<label for="county" class="control-label"><spring:message  code="label.county"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                        <div class="controls">
	                             <select class="input-large" name="location.county"  id="county">
	                             	<option value="">Select County...</option>
	                             </select>
	                             <span id="county_error"></span>
	                        </div>
	                      </div>
	                  </div>    
	<!-- Address Section End 	 -->                    
	                    <!-- /.new-address -->
	                    <div class="control-group">
	                      <label class="control-label"><spring:message  code="label.gender"/> <img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"/></label>
	                      <div class="controls">
	                        <label class="radio inline" for="male">
	                          <input type="radio" value="MALE" id="genderMale" name="gender" tabindex="0" class="error">
	                          <span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.male"/> <span class="aria-hidden"><spring:message  code="label.required"/></span></label>
	                        <label class="radio inline" for="female">
	                          <input type="radio" value="FEMALE" id="genderFemale" name="gender" tabindex="0" class="error">
	                           <span class="aria-hidden"><spring:message  code="label.gender"/></span> <spring:message  code="label.female"/> <span class="aria-hidden"><spring:message  code="label.required"/></span></label> 
	                           <span id="genderMale_error" class="error help-inline"></span>
	                           <span id="genderFemale_error" class="error help-inline"></span>
	                      </div>
	                    </div>
	                    <div class="control-group">
	                      <label class="control-label" for="dob"><spring:message  code="label.dob"/> <img width="10" height="10" src="/hix/resources/images/requiredAsterix.png" alt="Required!" aria-hidden="true"/></label>
	                      <div class="controls">
	                          <div data-date-format="mm/dd/yyyy" data-date="" class="input-append date date-picker">
										<input type="text" id="dob" name="dob" class="span10 error" tabindex="0"> <span class="add-on"><i class="icon-calendar"></i></span>
							  </div>
	                          <span id="dob_error" class="error help-inline"></span>
	                      </div>
	                    </div> 
	                    <div class="control-group">
						<label class="control-label"><spring:message  code="label.taxIdSSN"/><!-- <img width="10" height="10" src="/hix/resources/images/requiredAsterix.png" alt="Required!" aria-hidden="true"/> --><a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.taxidforincometax"/>" class="info"><i class="icon-question-sign"></i> </a>
						</label>
						<div class="controls">
							<span id="showEncrSsn" style="display:none;"></span>
							<label class="aria-hidden" for="ssn1"><spring:message  code="label.taxIdSSN"/> <spring:message  code="label.firstSSN"/> <a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.taxidforincometax"/>" class="info"><i class="icon-question-sign"></i> </a></label>
							<input type="text" tabindex="0" class="span2 inline valid" name="ssn1" id="ssn1" maxlength="3"  style="display:none;">
							<label class="aria-hidden" for="ssn2"><spring:message  code="label.taxIdSSN"/> <spring:message  code="label.secondSSN"/> <a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.taxidforincometax"/>" class="info"><i class="icon-question-sign"></i> </a></label>
							<input type="text" tabindex="0" class="span2 inline valid" name="ssn2" id="ssn2" maxlength="2"  style="display:none;">
							<label class="aria-hidden" for="ssn3"><spring:message  code="label.taxIdSSN"/> <spring:message  code="label.thirdSSN"/> <a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message  code="label.taxidforincometax"/>" class="info"><i class="icon-question-sign"></i> </a></label>
							<input type="text" tabindex="0" class="span2 inline error" name="ssn3" id="ssn3" maxlength="4" style="display:none;">
							<input type="button" class="btn btn-small pull-right" id="editSsnButton" name="editSsnButton" onclick="javascript:editSsn();" value='<spring:message  code="label.edit"/> <spring:message  code="label.taxidnumber"/>' style="display:none;"/>
							<span id="ssn1_error" class="error help-inline"></span> 
							<span id="ssn3_error" class="error help-inline"></span>
						</div>
						</div>
						<div class="control-group">
	                      <label class="control-label"><spring:message  code="label.tobacoUser"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
	                      <a href="javascript:void(0)" rel="tooltip" data-placement="top" data-original-title="<spring:message  code='label.tobaccousertooltip'/>" 
								class="info"><i class="icon-question-sign"></i>
						  </a>
	                      </label>
	                      <div class="controls">
	                        <label class="radio inline" for="smokerYes"><input type="radio" id="smokerYes" name="smoker" tabindex="0"  value="YES" class="error"><span class="aria-hidden"><spring:message  code="label.tobacoUser"/> <spring:message  code="label.required"/></span> <spring:message  code="label.yes"/> </label>
	                        <label class="radio inline" for="smokerNo"><input type="radio" id="smokerNo" name="smoker" tabindex="0"   value="NO" class="error"><span class="aria-hidden"><spring:message  code="label.tobacoUser"/> <spring:message  code="label.required"/></span> <spring:message  code="label.no"/> </label>
								<div id="smokerYes_error"></div>
								<div id="smokerNo_error"></div>
	                      </div>
	                    </div>
	                    
	                    <div class="control-group" id = "tobaccoCessPrgDiv" style="display:none;">
	                      <label class="control-label"><spring:message  code="label.tobaccoCessationPrg"/></label>
	                      <div class="controls">
	                        <label class="radio inline" for="tobaccoCessPrgYes"><input type="radio" id="tobaccoCessPrgYes" name="tobaccoCessPrg" tabindex="0"  value="YES" class="error"><span class="aria-hidden"> </span> <spring:message  code="label.yes"/> </label>
	                        <label class="radio inline" for="tobaccoCessPrgNo"><input type="radio" id="tobaccoCessPrgNo" name="tobaccoCessPrg" tabindex="0"   value="NO"  class="error" ><span class="aria-hidden"> </span> <spring:message  code="label.no"/> </label>
								<div id="tobaccoCessPrgYes_error"></div>
								<div id="tobaccoCessPrgNo_error"></div>
	                      </div>
	                    </div>
	                    
						<div class="control-group">
	                      <label class="control-label"><spring:message  code="label.memberoffed"/><img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png"></label>
	                      <div class="controls">
	                        <label class="radio inline" for="nativeAmrYes"><input type="radio" id="nativeAmrYes" name="nativeAmr" tabindex="0"  value="YES" class="error"><span class="aria-hidden"><spring:message  code="label.memberoffed"/> <spring:message  code="label.required"/></span> <spring:message  code="label.yes"/> </label>
	                        <label class="radio inline" for="nativeAmrNo"><input type="radio" id="nativeAmrNo" name="nativeAmr" tabindex="0"  value="NO" class="error"><span class="aria-hidden"><spring:message  code="label.memberoffed"/> <spring:message  code="label.required"/></span> <spring:message  code="label.no"/> </label>
								<div id="nativeAmrYes_error"></div>
								<div id="nativeAmrNo_error"></div>
	                      </div>
	                    </div>
	                    </form>
	                     <div class="control-group" id="uploadWardDoc" style='display:none;'>
	                      <label class="control-label"><spring:message  code="label.uploadDoc"/></label>
	                      <img id = "imgDiv" style="display:none;" src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
	                      <div class="controls">
	                        	<div id="file_upload_container">
										<input type="file" class="input-file" id="fileupload" onchange="" value=""  name="wardDoc" />&nbsp;	
								</div>
								<div id="fileupload_error" class="error" style="display:none;">File could not be uploaded. Selected file name already exists on the server.</div>
								<div id="uploaddiv" style="display:none;">Uploading..... Please wait</div>
			       				<div id="deletediv" style="display:none;">Deleting..... Please wait</div>
			       				 <span id="wardDocument_error" style="display:none;"></span>
							    <!-- The table listing the files available for attaching/removing -->
			       				<div id="" class="files" >
			       					<table id="upload_files_container"></table>
			       				</div>
	                      </div>
	                    </div> 
	                    <!-- /.edit dependent address --> 
	                  </div>
	                  <div class="modal-footer">
	                    <input type="button" value="Cancel" class="btn btn-secondary removeModal clearForm" data-dismiss="modal" aria-hidden="true" onclick="javascript:clearvars();">
	                    <input type="button" name="submitbutton" id="submitbutton" onClick="javascript:validateEmployeeDetails();" class="btn btn-primary" value="<spring:message  code="label.ok"/>" />
	                  </div>
                  </div>
                  
                </div>
                <!-- /modal --> 
                
                <!-- modal delete dependent -->
                <div id="deletedependent" class="modal hide fade" tabindex="-1"  role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="$('#deleteEmployeeButton').unbind('click');">�</button>
                    <h3 id="deletedependentinfo"><spring:message  code="label.deletedependenttitle"/></h3>
                  </div>
                  <div class="modal-body" id="deleteEmpBody"><span style="font-size:18px"><spring:message  code="label.deletedependent"/></span></div>
                  <div class="modal-footer">  
                  	<input type="button" data-dismiss="modal" name="submitbutton" id="" class="btn" onclick="$('#deleteEmployeeButton').unbind('click');" value="<spring:message  code="label.no"/>" />          
                    <input type="button" name="submitbutton" id="deleteEmployeeButton" class="btn btn-primary" value="<spring:message  code="label.yes"/>" />
                  </div>
                </div>
                <!-- /modal --> 
                
                <!-- modal missing information -->
                <div id="missinginfo" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="childhomeaddress" aria-hidden="true">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">�</button>
                    <h3 id="deletedependentinfo0"><spring:message  code="label.deletedependent"/></h3>
                  </div>
                  <div class="modal-body">
                    <p><spring:message  code="label.dependentmissinginfo"/></p>
                    <h4 id="missingnames"><spring:message  code="label.child"/> 2</h4>
                    <p><spring:message  code="label.clickedit"/> <i class="icon-pencil"></i> <spring:message  code="label.addmisinginfo"/></p>
                    <p><spring:message  code="label.notcovdependent"/> <i class="icon-trash"></i> <spring:message  code="label.toremoveperson"/></p>
                  </div>
                  <div class="modal-footer">
                    <button class="btn btn-primary" data-dismiss="modal"><spring:message  code="label.ok"/></button>
                  </div>
                </div>
                <!-- /modal -->
                
             
              <div class="form-actions margin-top50">
                <a href="<c:url value="/shop/employee/verifyemployee/${employeeApplication.id}"/>" class="btn pull-left"> <spring:message  code="label.newhireback"/> </a> 
                <input type="button" name="nextbutton" id="nextbutton" class="btn btn-primary pull-right" onclick="checkMissingDependents();" value="<spring:message  code="label.next"/>" />
              </div>
        </div>
        <!--rightpanel--> 
      </div>
      <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<script type="text/javascript">
var uploadedDocId;
var uploadedDocDtls='';
var input = $('#fileupload');
var fileDetails;
var rowno=0;
var responseText="";

var locationsObj =  new Array();
var empDetailsJSON = ${empDetailsJSON};

for(var i=0;i<empDetailsJSON.length;i++){
    var obj = empDetailsJSON[i];
    var locationid = obj['locationid'];
    var locationName = obj['location'];
    
    locationsObj[locationid] = locationName;
}

locationsObj[locationJSON['id']] = locationString;

var validator2 = $('#frmemployeedetails').validate({ 
	ignore: ':hidden:not("#wardDocument")',
	onkeyup: false,
	onclick: false,
	onSubmit: true,
	onfocusout: false,
	rules : {
		firstName : { required : true,namecheck : true, duplicateName: true},
		lastName : { required : true,namecheck : true, duplicateName: true},
		middleInitial : {namecheck : true},
		gender : {required: true},
		dob : {required: true,  date: true, dobcheck: true, agecheck:true,wardAgeCheck:true},
		ssn3 : { ssncheck : true ,ssn1check : true ,invalidssn1 :true,invalidssn2 :true, duplicateSSN: true},
		'location.address1' : { required : true, address1Check:true},
		'location.address2' :{address2Check : true, address2Match:true},
		'location.city' :  {required: true},
		'location.state' :  {required: true},
		'location.zip' : { required : true, zipcheck : true },
		'location.county' : { required : true },
		smoker : { required : true},
		nativeAmr : { required : true},
		isEmployeeAddress : { required : true},
		relationship : { relationCheck : true},
		wardDocument : { warddoccheck : true}
	},
	messages : {
		firstName : {required: "<span><em class='excl'>!</em><spring:message code='label.validateempfirstname' javaScriptEscape='true'/></span>" ,
			namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstnamechar' javaScriptEscape='true'/></span>"	,
			duplicateName: "<span> <em class='excl'>!</em><spring:message code='label.validateduplicatename' javaScriptEscape='true'/></span>"	
		},
		lastName : {required : "<span><em class='excl'>!</em><spring:message code='label.validateemplastname' javaScriptEscape='true'/></span>",
			namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatelastnamechar' javaScriptEscape='true'/></span>",
			duplicateName: "<span> <em class='excl'>!</em><spring:message code='label.validateduplicatename' javaScriptEscape='true'/></span>"
		},
		middleInitial : {
			namecheck : "<span> <em class='excl'>!</em><spring:message code='label.validatemiddlenamechar' javaScriptEscape='true'/></span>"	
		},
		gender : {required : "<span><em class='excl'>!</em><spring:message code='label.validategender' javaScriptEscape='true'/></span>"},
		dob : {  required: "<span><em class='excl'>!</em><spring:message code='label.validateempdob' javaScriptEscape='true'/></span>",
			date:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
	        dobcheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempvdob' javaScriptEscape='true'/></span>",
	        agecheck:  "<span> <em class='excl'>!</em><spring:message code='label.validateempage' javaScriptEscape='true'/></span>" ,
	        wardAgeCheck: "<span> <em class='excl'>!</em><spring:message code='label.validatewardage' javaScriptEscape='true'/></span>" },
	        ssn3 : { ssncheck : "<span> <em class='excl'>!</em><spring:message code='label.validate9digitnumber' javaScriptEscape='true'/></span>",
	        	ssn1check : "<span> <em class='excl'>!</em><spring:message code='label.validatefirstthreedigit' javaScriptEscape='true'/></span>",
	        	invalidssn1 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn1' javaScriptEscape='true'/></span>",
	        	invalidssn2 : "<span> <em class='excl'>!</em><spring:message code='label.invalidssn2' javaScriptEscape='true'/></span>",
	        	duplicateSSN : "<span> <em class='excl'>!</em><spring:message code='label.validateduplicatessn' javaScriptEscape='true'/></span>"
	        	},
	   		'location.address1' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddr' javaScriptEscape='true'/></span>",
									address1Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>"},
			'location.address2' :  {address2Check: "<span> <em class='excl'>!</em><spring:message code='label.validatehomeaddress' javaScriptEscape='true'/></span>",
									address2Match: "<span> <em class='excl'>!</em><spring:message code='label.validateaddressline12' javaScriptEscape='true'/></span>"},
			'location.city' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatecity' javaScriptEscape='true'/></span>"},
			'location.state' :  {required: "<span> <em class='excl'>!</em><spring:message code='label.validatestate' javaScriptEscape='true'/></span>"},
			'location.zip' : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validateempzip' javaScriptEscape='true'/></span>",
				zipcheck : "<span> <em class='excl'>!</em><spring:message code='label.validateempvzip' javaScriptEscape='true'/></span>"
			},
			'location.county' : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validatecounty' javaScriptEscape='true'/></span>"
			},
			nativeAmr : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validatenativeamerican' javaScriptEscape='true'/></span>"
			},
			smoker : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validatetobaccouser' javaScriptEscape='true'/></span>"
			},
			isEmployeeAddress : {
				required : "<span> <em class='excl'>!</em><spring:message code='label.validateisemployeeaddress' javaScriptEscape='true'/></span>"
			},
			relationship : {
				relationCheck : "<span> <em class='excl'>!</em><spring:message code='label.validaterelationship' javaScriptEscape='true'/></span>"
			},
			wardDocument : { 
				warddoccheck : "<span> <em class='excl'>!</em><spring:message code='label.validatefile' javaScriptEscape='true'/></span>" 
			}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
});

$(document).ready(function(){
	$('.info').tooltip();
	$('.new-address').hide();
	setAjaxPlugin();
});

$('#smokerNo').click(function() {
	  $('input:radio[name=tobaccoCessPrg]').attr('checked',false);
	  
	   if($('#smokerNo').is(':checked')) {
		  $('#tobaccoCessPrgDiv').hide();
	   }
});

$('#smokerYes').click(function() {
	if(!$('input:radio[name="tobaccoCessPrg"]').is(':checked')) {
    	  $('input:radio[name=tobaccoCessPrg]').attr('checked',false);
      }
	   if($('#smokerYes').is(':checked')) {
		  $('input:radio[name=tobaccoCessPrg]').attr('checked',false); 
		  $('#tobaccoCessPrgDiv').show();
	   }
});

function showHideAddressDiv(val){
	if(val=='Y'){
		$('#addressDiv').hide();
		$('#address1').val(locationJSON['address']);
		$('#address2').val(locationJSON['address2']);
		$('#city').val(locationJSON['city']);
		$('#state').val(locationJSON['state']);
		$('#zip').val(locationJSON['zip']);
		getCountyList(locationJSON['zip'], locationJSON['county']);
		$('#location_id').val(locationJSON['id']);
	}
	else{
		$('#addressDiv input[type="text"]').val('');
		$('#state').prop('selectedIndex', 0);
		$('#county').prop('selectedIndex', 0);
		if($('#CN').val()!='' && $('#CN').val()!='0'){
			if(locationJSON['id'] != $('#dependent_'+$('#CN').val()+'_location_id').val() ){
				$('#location_id').val($('#dependent_'+$('#CN').val()+'_location_id').val());
	            $('#address1').val($('#dependent_'+$('#CN').val()+'_address1').attr('value'));
	            $('#address2').val($('#dependent_'+$('#CN').val()+'_address2').attr('value'));
	            $('#city').val($('#dependent_'+$('#CN').val()+'_city').attr('value'));
	            $('#state').val($('#dependent_'+$('#CN').val()+'_state').attr('value'));
	            $('#zip').val($('#dependent_'+$('#CN').val()+'_zip').attr('value'));
	            getCountyList($('#dependent_'+$('#CN').val()+'_zip').attr('value'), $('#dependent_'+$('#CN').val()+'_county').attr('value'));
			}else{
				$('#location_id').val(0);
			}
		}
		else{
			$('#location_id').val(0);
		}
		
		$('#addressDiv').show();
	}
}

populateDependentData(empDetailsJSON);


function populateDependentData(empDetailsJSON){
	var empChildCount = 0;
	var isSpouseAdded = false;
	 var obj;
	 var type;
	 var id;
	 var firstName;
     var lastName;
     var dob;
     var locationid;
     var location;
     var gender;
     var ssn;
     var smoker;
     var tobaccoCessPrg;
     var nativeAmr;
     var middleInitial;
     var address1;
     var address2;
     var city;
     var state;
     var county;
     var zip;
     var isEmployeeAddress;
     var suffix;
     var relationship;
     var wardDoc;
     
    dependentCount = empDetailsJSON.length;
    
	  for(var i=0;i<dependentCount;i++){
		  firstName = null;
		  lastName = null;
		  if(i < empDetailsJSON.length){
				obj = empDetailsJSON[i];
				type = obj['type'];
				id = obj['id'];
				firstName = obj['firstName'];
				lastName = obj['lastName'];
				dob = obj['dob'];
				locationid = obj['locationid'];
				location = obj['location'];
				gender = obj['gender'];
				ssn = obj['ssn'];
				smoker = obj['smoker'];
				tobaccoCessPrg = obj['tobaccoCessPrg'];
				nativeAmr = obj['nativeAmr'];
				middleInitial = obj['middleInitial'];
				address1= obj['address1'];
			    address2= obj['address2'];
			    city= obj['city'];
			    state= obj['state'];
			    county= obj['county'];
			    zip = obj['zip'];
			    if(obj['isEmployeeAddress']==''||obj['isEmployeeAddress']==null){
			    	obj['isEmployeeAddress']='N';
			    }
			    
			    isEmployeeAddress = obj['isEmployeeAddress'];
			    suffix = obj['suffix'];
			    relationship = obj['relationship'];
			    wardDoc = obj['wardDoc'];
		  }else{
			  obj = new Object();
			  if(isMarried=='YES' && !isSpouseAdded){
				  type = 'SPOUSE';
				  isSpouseAdded=true;
			  }else{
				  type = 'CHILD';
			  }
			  
			    id=-1;
				dob = 'N/A';
				locationid = 0;
				location = 'N/A';
				gender = 'N/A';
				ssn = 'N/A';
				smoker='N/A';
				tobaccoCessPrg = 'N/A';
				nativeAmr = 'N/A';
				address1= '';
			    address2= '';
			    city= '';
			    state= '';
			    county= '';
			    zip='';
			    isEmployeeAddress= 'N';
			    suffix = '';
			    relationship = '';
			    wardDoc = null ;
		  }		    
		    if(type == 'SPOUSE' || type == 'LIFE_PARTNER'){
		    	isSpouseAdded = true;
		    	if(firstName == null || firstName == ''){
		    		firstName = 'SPOUSE';
		    	}
		    }else{
		    	empChildCount++;
		    	
		    	if(firstName == null || firstName == ''){
		    		firstName = 'CHILD ' + empChildCount;
		    	}
		    }
		    
		    if(lastName == null){
		    	lastName = '';
	    	}
		    if(middleInitial == null){
		    	middleInitial='';
		    }
		    
		    Dependent.add_dependent(id, type, calenderimagepath, firstName, lastName, dob, locationid, location, gender, ssn, smoker, tobaccoCessPrg, nativeAmr, middleInitial,address1, address2, city, state, county, zip, isEmployeeAddress, suffix, relationship,wardDoc,wardDocumentRequired);
	  }
	  
	  if(!isSpouseAdded){
		  Dependent.add_spouseBtn();
	  }
}
function clearvars(){
	if($('#wardDocument').val() && $('#wardDocument').val().indexOf("workspace") != -1 && $('#id').val()==0){
		deleteFileOnCancel();	
	}
	//orgSSNvalAssigned=false;
}
function deleteFileOnCancel(){
	var subUrl = '<c:url value="/shop/employee/removeSupportDocument">
                  <c:param name="${df:csrfTokenParameter()}"> 
                    <df:csrfToken plainToken="true" />
                  </c:param> 
                </c:url>';

	$.ajax({
		type : "POST",
		url : subUrl,
		data : {uploadedDocId : $('#wardDocument').val()}, 
		dataType:'text',
		success : function(response,xhr) {
	        if(isInvalidCSRFToken(xhr))
	          return;
			$('#wardDocument').val(null);
			var newText = '<input type="file" class="input-file" id="fileupload" onchange=""  name="wardDoc" />&nbsp;';
			$('#file_upload_container').html('');
			$('#file_upload_container').html(newText);
			 setAjaxPlugin();
		},error : function(e) {
		}
	});
} 

function setAjaxPlugin(){
	var subUrl =  '<c:url value="/shop/employee/uploadSupportDocument"/>';
	$('#fileupload').ajaxfileupload({
		
	    'action': subUrl,
	    'onComplete': function(response) {
	    	responseText=response;
	    	if(response.status == false){       
	    	   alert(response.message);
  	      	}
	    	else if(responseText.indexOf("Error") != -1){
	    		 errorDetails=responseText.split('#');
	    		 alert(errorDetails[1]);
	    	}
	    	else if(responseText){
	    		fileDetails=responseText.split('#');
	    		
	    		if(fileDetails.length == 2){
	        		uploadedDocId=fileDetails[0];
	            	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
	            	$('#wardDocument').val(uploadedDocId);
	            	tempFileName=fileDetails[1];
	            	rowno++;
	            	appendFileDetails();	
	            	$( "#fileupload" ).hide();
	            	$('#fileupload_error').hide();
	            	$("#wardDocument_error").hide();
	        	}
	        	else{
	        		$('#fileupload_error').show();
	        	}
	    	}
	    	else{
	    		$('#fileupload_error').show();
	    	}
	    	
	    	$('#uploaddiv').hide();
	    },
	    'onStart': function() {
	    	$('#uploaddiv').show();
	    	$('#fileupload_error').hide();
	    },
	    'onCancel': function() {
	      
	    },
	    'valid_extensions' : ['gif','png','jpg','jpeg','pdf']
	  });
}

	function showWardDocument(wardDoc){
		
		fileDetails=wardDoc.split('#');
		if(fileDetails.length == 2){
    		uploadedDocId=fileDetails[0];
        	uploadedDocId=uploadedDocId.substring(uploadedDocId.indexOf("workspace"),uploadedDocId.length);
        	$('#wardDocument').val(uploadedDocId);
        	rowno++;
        	appendFileDetails();		
        	$( "#fileupload" ).hide();
    	}
	}

	function deleteFile(fileId,rowid){
		$('#fileupload_error').hide();
		$('#deletediv').show();
		var subUrl = '<c:url value="/shop/employee/removeSupportDocument">
	                  <c:param name="${df:csrfTokenParameter()}"> 
	                    <df:csrfToken plainToken="true" />
	                  </c:param> 
	                </c:url>';
		
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {uploadedDocId : fileId}, 
			dataType:'text',
			success : function(response,xhr) {
		        if(isInvalidCSRFToken(xhr))
		          return;
				var newText = '<input type="file" class="input-file" id="fileupload" onchange=""  name="wardDoc" />&nbsp;';
				$('#file_upload_container').html('');
				$('#file_upload_container').html(newText);
				 //$('#fileupload').replaceWith($('#fileupload').clone(true).val(''));
				 setAjaxPlugin();
				$('#wardDocument').val(null);
				$('#deletediv').hide();
				$('#upload_files_container').html("");
			},error : function(e) {
				alert('error in deleting document.');
				$('#deletediv').hide();
			}
		});
	}

	function appendFileDetails(){
		orgHtml=$('#upload_files_container').html();
		//strHtml=orgHtml+'<tr id="' +rowno  +'">'+
		strHtml='<tr id="' +rowno  +'">'+
		'<td colspan="1">'+fileDetails[1]+'</td>'+
		'<td colspan="1"></td>'+
		'<td colspan="1"><a class="btn" href="javascript:downloadFile(\''+uploadedDocId+'\')">download</a></td>'+
		'<td colspan="1"><a class="btn btn-danger" href="javascript:deleteFile(\''+uploadedDocId+'\','+rowno+')">delete</a></td>'
		+'</tr>';
		$('#upload_files_container').html(strHtml);
		 
	}
	
	function downloadFile(fileId){
		var  contextPath =  "<%=request.getContextPath()%>";

		var url = contextPath+"/shop/employee/downloadDocument?udId="+fileId;
		var subUrl =  '<c:url value="'+url+'"/>';
		window.open(subUrl,"Supported Document");
	}

$('.icon-calendar').click(function(){
	$('.date-picker').datepicker();
	$('#datepickerDiv').attr('data-date', $('#dob').val());
    $(".datepicker, info").css("z-index", "3001");
}); 
$('i.icon-ok').parents('li').css('list-style','none');
/*  $('#crossClose').live('click', function(){
	 $('<div class="modal-backdrop"></div>').appendTo(document.body);
 });
  */
/*  $('#zip').focusout(function(e) {
 	 if($('#zip').val().length >= 5){
 	  getCountyList(0, $('#zip').val(), '');
 	 }   
 }); */
  
$('#editdependent, #deletedependent, #missinginfo').on('shown', function () {
	$('.modal-body').scrollTop(0);
});	
 

</script>