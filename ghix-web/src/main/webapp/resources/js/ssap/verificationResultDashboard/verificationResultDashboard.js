(function(){
	'use strict';
	
	//createDynamicTabs();createDocumentListInsideTabs();
	//var webJson;
	var userActiveRoleName;
	var subUrl = $('#subUrl').val();
	var documentUploadAjaxUrl = $('#documentUploadAjaxUrl').val();
	var csrftoken = $('#csrftoken').val();
	var globaldocumentCategory;
	var globalapplicantID;
	var locationI;
	var locationJ;
	

	function isInvalidCSRFToken(xhr) {
	    var rv = false;
	    if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {                   
	    	   alert($('Session is invalid').text());
	           rv = true;
	    }
	    return rv;
	}

	function actualverificationDocCategory(){
		var overrideCommentsvalue =  $("#overrideComments").val();
		if(overrideCommentsvalue == "") {                   
			alert("Please Enter Reason.");
			return false;
		}
		$('#adminModal').modal('hide');

		if ($('#verifying'+locationI+''+locationJ+'').is(':hidden')) {
			 $('#verifying'+locationI+''+locationJ+'').show();
		} 
		$.ajax({
			type : "POST",
			url : subUrl,
			data : {
				applicantID : globalapplicantID,
				documentCategory : globaldocumentCategory,
				overrideComments : overrideCommentsvalue ,
				csrftoken : '<df:csrfToken plainToken="true"/>'
			},
			success : function(response) {

				if(isInvalidCSRFToken(response))                                  
	                return; 
	            
				$('#verifying'+locationI+''+locationJ+'').hide('slow', 'linear');
				if(response){
					$("#verifystatus"+locationI+""+locationJ+"").html("<small>Verified</small> <i class='icon-ok-sign'></i>");
					$("#override"+locationI+""+locationJ+"").attr("disabled", true) ;
			    	$("#override"+locationI+""+locationJ+"").attr('onclick',null);
				}else{

					alert("Could not verify Document Category");

				}
				
				// populateCounties(response,''); FIX ME
			},
			error : function(e) {
				alert("Failed to verify ApplicantDocCategory");
			}
		});
	}

	function verifyApplicantDocCategory( documentCategory,applicantID,i,j){
		globaldocumentCategory = documentCategory;
		globalapplicantID = applicantID;
		locationI = i;
		locationJ = j;

		$("#overrideComments").val('');
		$('#adminModal').modal();

		
		
		
		/*  */
	}

	function createDynamicTabs() {
		var webJson = JSON.parse($('#webJson').val()); 
		var noOfApplicants = webJson.mainApplicantList.length;
		var applicantsTotal = document.getElementById("applicantsTotal"); 
		var tabs =''; 
		var classopt = "";

		if(!webJson.authorizedRepresentativeIndicator){
			 $('#authorizedRepresentativeIndicator').hide();
		}else{
			if(webJson.consumerDocuments.length>0){
				$('#authorizedRepresentativeIndicator').hide();
				var trHTML = '<table><tr><td>Authorized Representative Authorization</td></tr></table><table border="1" class="table margin20-t"><tr><td><b>Document Name</b></td><td><b>Status</b></td></tr>';
			    $.each(webJson.consumerDocuments, function(i, item) {
			    	    trHTML += '<tr><td>' + item.documentName + '</td><td>' + item.accepted+ '</td></tr>';
			    });
			    trHTML +='</table>';
			    $('#authorizedRepresentativeDocs').append(trHTML);
			}
		}

		

		for(var i=1;i<=noOfApplicants;i++){
			var objhouseholdMember = webJson.mainApplicantList[i-1];
			if(i==1){
				classopt = "active";
			}

			if(objhouseholdMember.isDocumentCategoryVerified){
				tabs = tabs + '<li class="'+classopt+'" id="applicant'+i+'"><a href="#"> '+objhouseholdMember.applicantDisplayName+' <i class="icon-ok-sign"></i></a></li>';
			}else{
				tabs = tabs + '<li class="'+classopt+'" id="applicant'+i+'"><a href="#"> '+objhouseholdMember.applicantDisplayName+' <i class="icon-exclamation-sign"></i></a></li>';
			}
			
			
		}

		applicantsTotal.innerHTML  = tabs;
		
		
		
		
		
		
		
		
		
		var applicantListHtml = '';		
		for(var i=1; i<=noOfApplicants; i++){
			
			var householdMember = webJson.mainApplicantList[i-1];
			
			var thisClass = i === 1? 'active':'';
			var thisIcon = householdMember.isDocumentCategoryVerified? '<i class="icon-ok-sign"></i>':'<i class="icon-exclamation-sign"></i>';
			
			applicantListHtml += '<li class="' + thisClass + '" id="applicant' + i + '"><a data-toggle="tab" href="#applicantDetails' + i + '">' + householdMember.applicantDisplayName + ' ' + thisIcon + '</li>';
			
		}
		
		$('#applicantList').html(applicantListHtml);
		
		
		
		

		
		
		
		
		
		
		
		
		
	}



	function createDocumentListInsideTabs(){
		userActiveRoleName = $('#userActiveRoleName').val();
		var webJson = JSON.parse($('#webJson').val());
		var noOfApplicants = webJson.mainApplicantList.length;
		var applicantContentHtml = '';
		
		if(noOfApplicants > 0){
			$('#case_numberforAR').val(webJson.caseId);
			$('#applicantIDforAR').val(webJson.mainApplicantList[0].applicantID);
			$('#applicantionIDforAR').val(webJson.applicationID);	
		}
		
		for(var i = 1; i <= noOfApplicants; i++){
			var applicant = webJson.mainApplicantList[i-1];
			var thisClass = i === 1? 'active':'';
			applicantContentHtml += '<div class="tab-pane ' + thisClass + '" id="applicantDetails' + i + '">'
			applicantContentHtml += '<div class="row-fluid gutter10 applicantDetails"><div class="accordion margin20-t" id="accordion' + i + '">';
			
			var j=0;
			
			for(var key in applicant.documentCategoryStatusMap){
				var tempKey = key.replace(/\s+/g, ""); //remove space
				j++;
				applicantContentHtml += '<div class="accordion-group margin10-b"><div class="accordion-heading"> <a class="accordion-toggle pull-left" data-toggle="collapse" data-parent="#accordion' + i + '" href="#collapse' + i + j+ '">' + key + '</a>'
				applicantContentHtml += '<a id="verifying' + i + j + '" class="txt-center" style="display:none"><img src="/hix/resources/img/loader_greendots.gif" width="10%" alt="loader dots"/></a>"';
				
				
				
				if(userActiveRoleName=='supervisor' || userActiveRoleName=='admin' || userActiveRoleName=='l2_csr'){
					applicantContentHtml += '<a href="#" class="btn btn-small btn-primary pull-right hide" onClick="verifyApplicantDocCategory("' + tempkey + '","' + applicant.applicantID + '","' + i + '","' + j + '")"><i class="icon-ok-sign"></i>Verify</a>';
					applicantContentHtml += '<a href="" data-toggle="modal" class="btn btn-small btn-danger pull-right" id="override' + i + j '"';
					
					if(applicant.documentCategoryStatusMap[key].isDocumentCategoryVerified){
						applicantContentHtml += 'disabled = true';
					}else{
						applicantContentHtml += 'onClick=verifyApplicantDocCategory("' + tempkey + '","' + applicant.applicantID + '","' + i + '","' + j + '")';
					}
					
					applicantContentHtml += '><i class="icon-edit-sign"></i> Override</a>';
				};
			}
			
			
			
			
			
			
			
			
			
			
			
			
			$.each(applicant, function(outerKey, outerValue){
				if(outerKey=='documentCategoryStatusMap'){
				    $.each(outerValue, function(key, valuetest){
				    					   if(true){
				    					       applicantContentHtml += " <div class='accordion-group margin10-b'><div class='accordion-heading'> <a class='accordion-toggle pull-left' data-toggle='collapse' data-parent='#accordion"+j+"' href='#collapse"+i+""+j+"'>"+key+"</a> ";
				    					       applicantContentHtml += "<a id='verifying"+i+""+j+"' class='txt-center' style='display:none'><img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></a>";
				    					       
				    							var tempkey = key.replace(/\s+/g, "");
				    						  
				    					     	var isDocumentVerified = false; 
				    					    

				    					       if(userActiveRoleName=='supervisor' || userActiveRoleName=='admin' || userActiveRoleName=='l2_csr'){

				    							    applicantContentHtml += "<a href='#' class='btn btn-small btn-primary pull-right hide' onClick=javascript:verifyApplicantDocCategory('"+tempkey+"','"+applicant.applicantID+"','"+i+"','"+j+"');><i class='icon-ok-sign'></i> Verify</a> <a href='#' data-toggle='modal' class='btn btn-small btn-danger pull-right'  id='override"+i+""+j+"'  ";
													if(valuetest.isDocumentCategoryVerified){
														applicantContentHtml += "disabled = true";
													}else{
														applicantContentHtml += "onClick=javascript:verifyApplicantDocCategory('"+tempkey+"','"+applicant.applicantID+"','"+i+"','"+j+"');";
						    						}
													applicantContentHtml += "><i class='icon-edit-sign'></i> Override</a> ";
				    							   
				    							}

				    					       if(valuetest.isDocumentCategoryVerified){
				    					    	   applicantContentHtml += "<span class='documentStatus pull-right'><div id='verifystatus"+i+""+j+"'><small>Verified</small> <i class='icon-ok-sign'></i></div></span> ";
				    					    	   isDocumentVerified = true;
					    					    }else{
				    					    	  
					    							   applicantContentHtml += "<span class='documentStatus pull-right'><div id='verifystatus"+i+""+j+"'><small>Not Verified</small> <i class='icon-exclamation-sign'></i></div></span> ";
				    						   }

				    					       applicantContentHtml += "</div> <div id='collapse"+i+""+j+"' class='accordion-body collapse'><div id='uploadeddocumentlist"+i+""+j+"' class='margin10-b gutter10'>";

				    					       applicantContentHtml += "<div id='loading"+i+""+j+"' class='txt-center' style='display:none'><label class='green' class='gutter10'>Uploading Document ....</label><img  src='/hix/resources/img/loader_greendots.gif' width='15%' alt='loader dots'/></div>";

				    					      // console.log(valuetest);
				    							var uploadedformlength = valuetest.documentCategoryDtlList.length;
												var isRejected = false;
				    							if(uploadedformlength>0){
				    								applicantContentHtml += "<table class='table table-condensed margin20-t'><thead><tr><th>Document Name</th><th>Document Status</th></tr><thead><tbody>";
				    							}else{
				    								isRejected = true;
					    						}
				    							
				    							 for(var k=0;k<uploadedformlength;k++){
				    								var uploadedform = valuetest.documentCategoryDtlList[k];
													if('REJECTED'==uploadedform.consumerDocument.accepted){
														isRejected = true;
													}
				    								
													applicantContentHtml += "<tr> <td> "+ uploadedform.consumerDocument.documentName+"  </td><td> "+ uploadedform.consumerDocument.accepted+"</td></tr>";
				    							}  //documentCategory

				    							if(uploadedformlength>0){
				    								applicantContentHtml += "</tbody></table>";
				    							}
				    							applicantContentHtml += "</div>";
				    							if(!valuetest.isDocumentCategoryVerified){
					    							if(isRejected){
					    								applicantContentHtml += "<form  class='form-horizontal' id='uploadDocumentModal"+i+""+j+"' enctype='multipart/form-data' name='uploadDocumentModal"+i+""+j+"' method='POST' action='/hix/ssapdocumentmgmt/documents/uploadDocumentAttachmentsAjax'><div class='accordion-inner'>  <div class='cloneMe border-bottom'> <div class='control-group margin10-t'> <label for='documentTye' class='control-label'>Select Document Type</label> <div class='controls'> <select class='input-xlarge' name='documentType'> ";
						    							
					    								var documentTypeListlength = valuetest.documentTypeList.length;
					    								for(var z=0;z<documentTypeListlength;z++){
					    									var documentType = valuetest.documentTypeList[z];
					    									applicantContentHtml += "<option>"+documentType+"</option>";
					    								}
					    								
					    								applicantContentHtml += "</select> </div> </div> <div class='control-group'> <label for='' class='required control-label'>Upload Selected Document</label> <div class='controls'> <input type='file' class='custom-file-input' multiple='' id='fileUpload' name='files[]' > <a class='pull-right'><input type='submit' class='btn btn-small btn-primary' value='Submit'/></a></div> </div>  <div class='cloneRemove hide'> <a href='#' class='btn btn-small btn-danger pull-right'><i class='icon-remove-sign'></i> Remove</a> </div> </div>  </div> <input type='hidden' id='ssapApplicationID' name='ssapApplicationID' value='1050'> <input type='hidden' id='caseNumber' name='caseNumber' value='yogesh123'>  <input type='hidden' id='documentCategory' name='documentCategory' value='"+valuetest.documentCategoryName+"'><input type='hidden' id='case_number' name='case_number' value='"+webJson.caseId+"'><input type='hidden' id='applicantID' name='applicantID' value='"+applicant.applicantID+"'><input type='hidden' id='applicantionID' name='applicantionID' value='"+webJson.applicationID+"'><input type='hidden' name='csrftoken' id='csrftoken' value='"+csrftoken+"' /></form>";

						    						}
				    							}
				    							applicantContentHtml += "</div> </div>";
				    							
				    							
				    							
				    							j++;
				    						    
				    						} 
				    					  
				    						   

				    				    });
				}

			});
			
		}
		
		applicantContentHtml += " </div></div></div></div> ";
		$('#applicantContent').html(applicantContentHtml); 
		
		
		
		
		
		
		
		
		
		
		
		
		
		var applicantDetails = document.getElementById('applicantDetails');
		var htmlcode = "";
		applicantDetails.innerHTML= "<div class='row-fluid gutter10'>No results to display.</div>"; 
		for(var i=1;i<=noOfApplicants;i++){
			var objhouseholdMember = webJson.mainApplicantList[i-1];

			if(i==1){
				$('#case_numberforAR').val(webJson.caseId);
				$('#applicantIDforAR').val(objhouseholdMember.applicantID);
				$('#applicantionIDforAR').val(webJson.applicationID);
				
			}
			htmlcode = htmlcode + " <div class='row-fluid gutter10 applicantDetails' id='applicant"+i+"Details'><div class='accordion margin20-t' id='accordion"+i+"'>";
				
				var j=1;
				$.each(objhouseholdMember, function(outerKey, outerValue){
					if(outerKey=='documentCategoryStatusMap'){
					    $.each(outerValue, function(key, valuetest){
					    					   if(true){
					    					       htmlcode=  htmlcode + " <div class='accordion-group margin10-b'><div class='accordion-heading'> <a class='accordion-toggle pull-left' data-toggle='collapse' data-parent='#accordion"+j+"' href='#collapse"+i+""+j+"'>"+key+"</a> ";
					    					       htmlcode=  htmlcode + "<a id='verifying"+i+""+j+"' class='txt-center' style='display:none'><img  src='/hix/resources/img/loader_greendots.gif' width='10%' alt='loader dots'/></a>";
					    					       
					    							var tempkey = key.replace(/\s+/g, "");
					    						  
					    					     	var isDocumentVerified = false; 
					    					    

					    					       if(userActiveRoleName=='supervisor' || userActiveRoleName=='admin' || userActiveRoleName=='l2_csr'){

					    							    htmlcode=  htmlcode + "<a href='#' class='btn btn-small btn-primary pull-right hide' onClick=javascript:verifyApplicantDocCategory('"+tempkey+"','"+objhouseholdMember.applicantID+"','"+i+"','"+j+"');><i class='icon-ok-sign'></i> Verify</a> <a href='#' data-toggle='modal' class='btn btn-small btn-danger pull-right'  id='override"+i+""+j+"'  ";
														if(valuetest.isDocumentCategoryVerified){
															htmlcode+="disabled = true";
														}else{
															htmlcode+="onClick=javascript:verifyApplicantDocCategory('"+tempkey+"','"+objhouseholdMember.applicantID+"','"+i+"','"+j+"');";
							    						}
					    							   htmlcode+="><i class='icon-edit-sign'></i> Override</a> ";
					    							   
					    							}

					    					       if(valuetest.isDocumentCategoryVerified){
					    					    	   htmlcode=  htmlcode + "<span class='documentStatus pull-right'><div id='verifystatus"+i+""+j+"'><small>Verified</small> <i class='icon-ok-sign'></i></div></span> ";
					    					    	   isDocumentVerified = true;
						    					    }else{
					    					    	  
						    							   htmlcode=  htmlcode + "<span class='documentStatus pull-right'><div id='verifystatus"+i+""+j+"'><small>Not Verified</small> <i class='icon-exclamation-sign'></i></div></span> ";
					    						   }

					    					       htmlcode=  htmlcode + "</div> <div id='collapse"+i+""+j+"' class='accordion-body collapse'><div id='uploadeddocumentlist"+i+""+j+"' class='margin10-b gutter10'>";

					    					       htmlcode=  htmlcode + "<div id='loading"+i+""+j+"' class='txt-center' style='display:none'><label class='green' class='gutter10'>Uploading Document ....</label><img  src='/hix/resources/img/loader_greendots.gif' width='15%' alt='loader dots'/></div>";

					    					      // console.log(valuetest);
					    							var uploadedformlength = valuetest.documentCategoryDtlList.length;
													var isRejected = false;
					    							if(uploadedformlength>0){
					    								 htmlcode = htmlcode + "<table class='table table-condensed margin20-t'><thead><tr><th>Document Name</th><th>Document Status</th></tr><thead><tbody>";
					    							}else{
					    								isRejected = true;
						    						}
					    							
					    							 for(var k=0;k<uploadedformlength;k++){
					    								var uploadedform = valuetest.documentCategoryDtlList[k];
														if('REJECTED'==uploadedform.consumerDocument.accepted){
															isRejected = true;
														}
					    								
					    								htmlcode = htmlcode + "<tr> <td> "+ uploadedform.consumerDocument.documentName+"  </td><td> "+ uploadedform.consumerDocument.accepted+"</td></tr>";
					    							}  //documentCategory

					    							if(uploadedformlength>0){
					    								htmlcode = htmlcode + "</tbody></table>";
					    							}
					    							htmlcode = htmlcode + "</div>";
					    							if(!valuetest.isDocumentCategoryVerified){
						    							if(isRejected){
						    								htmlcode = htmlcode + "<form  class='form-horizontal' id='uploadDocumentModal"+i+""+j+"' enctype='multipart/form-data' name='uploadDocumentModal"+i+""+j+"' method='POST' action='/hix/ssapdocumentmgmt/documents/uploadDocumentAttachmentsAjax'><div class='accordion-inner'>  <div class='cloneMe border-bottom'> <div class='control-group margin10-t'> <label for='documentTye' class='control-label'>Select Document Type</label> <div class='controls'> <select class='input-xlarge' name='documentType'> ";
							    							
						    								var documentTypeListlength = valuetest.documentTypeList.length;
						    								for(var z=0;z<documentTypeListlength;z++){
						    									var documentType = valuetest.documentTypeList[z];
						    									htmlcode = htmlcode + "<option>"+documentType+"</option>";
						    								}
						    								
						    								htmlcode = htmlcode +"</select> </div> </div> <div class='control-group'> <label for='' class='required control-label'>Upload Selected Document</label> <div class='controls'> <input type='file' class='custom-file-input' multiple='' id='fileUpload' name='files[]' > <a class='pull-right'><input type='submit' class='btn btn-small btn-primary' value='Submit'/></a></div> </div>  <div class='cloneRemove hide'> <a href='#' class='btn btn-small btn-danger pull-right'><i class='icon-remove-sign'></i> Remove</a> </div> </div>  </div> <input type='hidden' id='ssapApplicationID' name='ssapApplicationID' value='1050'> <input type='hidden' id='caseNumber' name='caseNumber' value='yogesh123'>  <input type='hidden' id='documentCategory' name='documentCategory' value='"+valuetest.documentCategoryName+"'><input type='hidden' id='case_number' name='case_number' value='"+webJson.caseId+"'><input type='hidden' id='applicantID' name='applicantID' value='"+objhouseholdMember.applicantID+"'><input type='hidden' id='applicantionID' name='applicantionID' value='"+webJson.applicationID+"'><input type='hidden' name='csrftoken' id='csrftoken' value='"+csrftoken+"' /></form>";

							    						}
					    							}
					    							htmlcode = htmlcode + "</div> </div>";
					    							
					    							
					    							
					    							j++;
					    						    
					    						} 
					    					  
					    						   

					    				    });
					}

				}); 

			htmlcode = htmlcode + " </div></div></div> ";
			applicantDetails.innerHTML=htmlcode; 



			}	

			
		

		

	    //TO KEEP OPEN MORE THAN ONE ACCORDION OPEN
	    $('.accordion-body').collapse({
	        toggle: false
	    });

	    
	    //CLONING SECTIONS
	    $('.cloneMachine').on('click', function(event){
	        event.preventDefault();
	        var cloneFactory = $(this).siblings('.cloneMe');
	        $(cloneFactory).clone().removeClass('cloneMe').addClass('removable').insertAfter(cloneFactory);
	        $('.removable .cloneRemove').removeClass('hide');
	        $('.cloneRemove a').on('click', function(event){
	            event.preventDefault();
	            var cloneRemove = $(this).closest('.removable');
	            $(cloneRemove).slideUp('normal', function(){ 
	                $(this).remove();
	            });
	        });
	    });

	    


	    //SHOW-HIDE APPLICANTS
	    $('.applicantDetails').hide();
	    $('#applicant1Details').show();

	    $('#applicantsTotal li').removeClass('active');
	    $('#applicant1').addClass('active');
	    
	    /* $('#applicant1').on('click', function(event){
	        event.preventDefault();
	        $('#applicantsTotal li').removeClass('active');
	        $(this).addClass('active');
	        $('.applicantDetails').hide();
	        $('#applicant1Details').show();

	    });

	    $('#applicantsTotal li#applicant2').removeClass('active');
	    $('#applicant2').on('click', function(event){
	        event.preventDefault();
	        $('#applicantsTotal li').removeClass('active');
	        $(this).addClass('active');
	        $('.applicantDetails').hide();
	        $('#applicant2Details').show();
	    });

	    $('#applicant3').on('click', function(event){
	        event.preventDefault();
	        $('#applicantsTotal li').removeClass('active');
	        $(this).addClass('active');
	        $('.applicantDetails').hide();
	        $('#applicant3Details').show();
	    }); */



	    //TEXT SWAP for Accordion btns
	    $('.swapText').on("click", function() {
	            var el = $(this);
	            if (el.text() === el.data("text-swap")) {
	                el.text(el.data("text-original"));
	            } else {
	                el.data("text-original", el.text());
	                el.text(el.data("text-swap"));
	        }
	    });


		for(var i=1;i<=noOfApplicants;i++){
			var objhouseholdMember = webJson.mainApplicantList[i-1];
			var j=1;
			$.each(objhouseholdMember, function(outerKey, outerValue){
				 $.each(outerValue, function(key, valuetest){


					 $('#uploadDocumentModal'+i+''+j).submit(function(e)
								{

								   var formObj = $(this);
								   var formURL = formObj.attr("action");
								   var loading = formObj.attr("name");
								   loading = loading.replace('uploadDocumentModal','#loading');
								   if ($(loading).is(':hidden')) {
										$(loading).show('slow', 'linear');
									}
								   if(window.FormData !== undefined)  // for HTML5 browsers
								   {
									   var formData = new FormData(this);
								       $.ajax({
								           url: documentUploadAjaxUrl,
								           type: 'POST',
								           data:  formData,
								           mimeType:"multipart/form-data",
								           contentType: false,
								           cache: false,
								           processData:false,
								           success: function(data, textStatus, xhr)
								           {

								        	   if(isInvalidCSRFToken(data))	    				
								       			return;	    
								       			
								        	   $(loading).hide('slow', 'linear');
									           var n = data.indexOf("File could not be uploaded")
									           if(n==0){

									        	   alert(data);
										        }else{

										        	jsonresponse =  JSON.parse(data);
													var showtable = formObj.attr("name");
													showtable = showtable.replace('uploadDocumentModal','#uploadeddocumentlist');
													if(jsonresponse.length>0){
														
														$(showtable).html('');
														 var trHTML = '<table border="1" class="table margin20-t"><tr><td><b>Document Name</b></td><td><b>Status</b></td></tr>';
												            $.each(jsonresponse, function(i, item) {
												            	    trHTML += '<tr><td>' + item.documentName + '</td><td>' + item.accepted+ '</td></tr>';
												            });
												            trHTML += '</table>';
												            $(showtable).append(trHTML);
												            var showtable1 = formObj.attr("name");
															showtable1 = showtable1.replace('uploadDocumentModal','#uploadDocumentModal');
															$(showtable1).hide();
													}else{
														alert('Failed to upload document');
														}
													
											    }
												
										    	
								           },
								           error: function(jqXHR, textStatus, errorThrown) 
								           {
									           //console.log(errorThrown);
								        	   alert('Document Upload failed :'+errorThrown);
								           }           
								      });
								       e.preventDefault();
								      
								  }
								     
								});
					j++;
					 
				 });
			});

			 $('#applicant'+i).on('click', function(event){
			        event.preventDefault();
			        $('.applicantDetails').hide();
			        var showdiv = '#'+event.currentTarget.id+'Details';
			        $(showdiv).show();
			        $('#applicant'+i+'Details').show();
				    $('#applicantsTotal li').removeClass('active');
			        $(this).addClass('active');
			       

			    });
		}
		
	}


	var taskClaimed=true;
	var jsonresponse;
	function submit(){
		 var fup = document.getElementById('fileuploadaction');
		 var fileName = fup.value
		 if(fileName==""){
			alert("Please select file to upload")
		 }
		 else{
			 
			 document.forms['uploadDocumentModel'].submit();
		 }
	}

	function getDoc(frame) {
	    var doc = null;

	    // IE8 cascading access check
	    try {
	        if (frame.contentWindow) {
	            doc = frame.contentWindow.document;
	        }
	    } catch(err) {
	    }

	    if (doc) { // successful getting content
	        return doc;
	    }

	    try { // simply checking may throw in ie8 under ssl or mismatched protocol
	        doc = frame.contentDocument ? frame.contentDocument : frame.document;
	    } catch(err) {
	        // last attempt
	        doc = frame.document;
	    }
	    return doc;
	}
	$("#multiform").submit(function(e)
	{

	   var formObj = $(this);
	   var formURL = formObj.attr("action");

	   if(window.FormData !== undefined)  // for HTML5 browsers
	   {
		
	       var formData = new FormData(this);
	       $.ajax({
	           url: documentUploadAjaxUrl,
	           type: 'POST',
	           data:  formData,
	           mimeType:"multipart/form-data",
	           contentType: false,
	           cache: false,
	           processData:false,
	           success: function(data, textStatus, jqXHR)
	           {
					jsonresponse =  JSON.parse(data);
					
					  $(function() {
						 var trHTML = '<table><tr><td>Authorized Representative Authorization</td></tr></table><table border="1" class="table margin20-t"><tr><td><b>Document Name</b></td><td><b>Status</b></td></tr>';
				            $.each(jsonresponse, function(i, item) {
				            	    trHTML += '<tr><td>' + item.documentName + '</td><td>' + item.accepted+ '</td></tr>';
				            });
				            trHTML +='</table>';
				            $('#authorizedRepresentativeDocs').html('');
				            $('#authorizedRepresentativeDocs').append(trHTML);
				        }); 

					  $('#authorizedRepresentativeIndicator').hide();
			    	
	           },
	           error: function(jqXHR, textStatus, errorThrown) 
	           {
	        	   alert('fail '+textStatus);
	           }           
	      });
	       e.preventDefault();
	      // e.unbind();
	  }
	  else  //for olden browsers
	   {
		 
	       //generate a random id
	       var  iframeId = 'unique' + (new Date().getTime());

	       //create an empty iframe
	       var iframe = $('<iframe src="javascript:false;" name="'+iframeId+'" />');

	       //hide it
	       iframe.hide();

	       //set form target to iframe
	       formObj.attr('target',iframeId);

	       //Add iframe to body
	       iframe.appendTo('body');
	       iframe.load(function(e)
	       {
	           var doc = getDoc(iframe[0]);
	           var docRoot = doc.body ? doc.body : doc.documentElement;
	           var data = docRoot.innerHTML;
	           //data is returned from server.

	       });

	   }     
	});

	$(document).ready(function() {
		createDynamicTabs();
		createDocumentListInsideTabs();
		
		$('.tabbable-content').on('click', 'li', function(){
			$(this).closest('.nav.nav-tabs').find('li').removeClass('active');
			$(this).addClass('active');
			
			var thisId = $(this).find('a').attr('href').replace('#','');
			$(this).closest('.tabbable').find('.tab-pane').removeClass('active');		
			$('#' + thisId).addClass('active');
		});
	})

	$('#overrideComments').keyup(function(){
		if ($(this).val()){
			$('#btnSubmtDocVerification').removeAttr('disabled');
		}else{
			$('#btnSubmtDocVerification').attr('disabled','disabled');
		}
	});
})();
