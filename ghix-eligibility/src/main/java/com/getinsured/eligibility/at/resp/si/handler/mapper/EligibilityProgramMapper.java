package com.getinsured.eligibility.at.resp.si.handler.mapper;

import com.getinsured.eligibility.at.resp.si.dto.EligibilityProgram;
import com.getinsured.iex.ssap.model.SsapApplicant;

public final class EligibilityProgramMapper {

	private EligibilityProgramMapper() { }

	public static com.getinsured.eligibility.model.EligibilityProgram map(EligibilityProgram eligibilityProgram, SsapApplicant ssapApplicantDO, Long id){

		com.getinsured.eligibility.model.EligibilityProgram eligibilityProgramDO = new com.getinsured.eligibility.model.EligibilityProgram();

		eligibilityProgramDO.setId(eligibilityProgram.getId());
		eligibilityProgramDO.setEligibilityType(eligibilityProgram.getEligibilityType());
		eligibilityProgramDO.setEligibilityIndicator(eligibilityProgram.getEligibilityIndicator());
		eligibilityProgramDO.setEligibilityStartDate(eligibilityProgram.getEligibilityStartDate());
		eligibilityProgramDO.setEligibilityEndDate(eligibilityProgram.getEligibilityEndDate());
		eligibilityProgramDO.setEligibilityDeterminationDate(eligibilityProgram.getEligibilityDeterminationDate());
		eligibilityProgramDO.setIneligibleReason(eligibilityProgram.getIneligibleReason());
		eligibilityProgramDO.setEstablishingCategoryCode(eligibilityProgram.getEstablishingCategoryCode());
		eligibilityProgramDO.setEstablishingCountyName(eligibilityProgram.getEstablishingCountyName());
		eligibilityProgramDO.setEstablishingStateCode(eligibilityProgram.getEstablishingStateCode());
		//eligibilityProgramDO.setSsapApplicantId(eligibilityProgram.getSsapApplicantId());
		//eligibilityDO.setSsapApplicantId(ssapApplicantId);
		eligibilityProgramDO.setSsapApplicant(ssapApplicantDO);
		eligibilityProgramDO.setId(id);

		return eligibilityProgramDO;

	}

}
