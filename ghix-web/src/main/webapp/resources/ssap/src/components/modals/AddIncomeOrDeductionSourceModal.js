import React, { Fragment, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import _ from 'lodash';

// Components

import InstructionalText from '../notifications/InstructionalText';
import FormForModal from '../../components/pages/FormForModal';
import UniversalInput from '../../lib/UniversalInput';

// ActionCreators

import { changeData, hideModal } from '../../actions/actionCreators';

// Utils

import incomeCalculator from '../../utils/incomeCalculator';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { DEDUCTIONS_SOURCES, FREQUENCY_OPTIONS_TYPES, INCOME_SOURCES } from '../../constants/incomeSourcesList';
import { INCOME_TYPES, OTHER_INCOME_TYPES } from '../../constants/incomeAndExpenceTypes';
import { FREQUENCY_TYPES } from '../../constants/frequencyTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';



const AddIncomeOrDeductionSourceModal = props => {

    const {
        contexts,
        data,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        householdMembers,
        modal: {
            modalOwner,
            data: [
                {
                    amount = null,
                    frequency = null,
                    sourceName = null,
                    type = null,
                    subType = INCOME_TYPES.UNSPECIFIED,
                    relatedExpense = null,
                    cyclesPerFrequency = null,
                    tribalAmount = null
                },
                index,
                parameter,
                pageToReturn,
                option = undefined
            ]
        },
    } = props;

    const {
        hhmIndex : member,
        currentHhm: currentHouseholdMember
    } = props;

    const {
        name
    } = currentHouseholdMember;

    const [ localState, setLocalState ] = useState({
        amount,
        frequency,
        sourceName,
        type: option ? option : type,
        profit: amount > 0,
        subType,
        relatedExpense,
        cyclesPerFrequency,
        tribalAmount
    });

    const [ areChangesApplied, changeStatus ] = useState(false);

    const handleSaveClick = () => {

        if(!areChangesApplied) {
            props.actions.hideModal(modalOwner);
        }

        const updateData = () => {
            const newState = { ...data };

            const dataForSave = _.omit(localState, ['profit']);

            if(index === null) {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[member][parameter]
                    .push(dataForSave);
            } else {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[member][parameter][index] = dataForSave;
            }

            const {
                incomes,
                expenses
            } = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member];

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member]
                .annualTaxIncome
                .reportedIncome = incomeCalculator.getTotalIncome(incomes, expenses);

            props.actions.changeData(newState);
        };

        contexts.FormForModal.onFormSubmitForValidationOnly([updateData]);
    };

    const valuesForWatch = householdMembers[member][parameter].length === 0 ?
        'no values' :
        householdMembers[member][parameter].map(value => Object.values(value).toString()).toString();

    useEffect(() => {
        if(valuesForWatch !== 'no values' && areChangesApplied) {
            changeStatus(false);
            contexts.FormForModal.onFormSubmit(pageToReturn,
                [() => props.actions.hideModal(modalOwner)]
            );
        }
    }, [valuesForWatch]);

    const onFieldChange = newData => {

        const [ key, value ] = Object.entries(newData)[0];

        if(key === 'type' && value !== localState.type) {
            setLocalState({
                amount: null,
                frequency: null,
                sourceName: null,
                type: newData.type,
                profit: null,
                subType: value === INCOME_TYPES.OTHER ? localState.subType : INCOME_TYPES.UNSPECIFIED,
                relatedExpense: null,
                tribalAmount: null
            });
        } else if(key === 'subType' && value !== localState.subType) {
            setLocalState({
                amount: null,
                frequency: null,
                sourceName: null,
                type: localState.type,
                profit: null,
                subType: newData.subType,
                relatedExpense: null,
                tribalAmount: null
            });
        } else if (key === 'amount') {
            setLocalState(state => ({
                ...{
                    ...state,
                    profit: value > 0
                },
                ...newData
            }));
        } else if (key === 'profit') {

            setLocalState(state => {
                return {
                    ...{
                        ...state,
                        amount: value === null ? state.amount : (value ? 1 : -1) * Math.abs(state.amount)
                    },
                    ...newData
                };
            });
        } else if (key === 'cyclesPerFrequency') {
            setLocalState(state => {
                return {
                    ...state,
                    cyclesPerFrequency: Number(value)
                };
            });
        } else if (key === 'frequency') {
            setLocalState(state => {
                return {
                    ...state,
                    frequency: value,
                    cyclesPerFrequency: null
                };
            });
        } else {
            setLocalState(state => ({
                ...state,
                ...newData
            }));
        }
        areChangesApplied === false && changeStatus(true);
    };

    const adjustQuestionText = text => {
        return text
            .replace('[Coverage Year]', coverageYear)
            .replace('[Full Name]', normalizeNameOfPerson(name));
    };

    const getAdditionalInputByType = inputsData => {
        switch(inputsData.additionalInput.inputType) {
        case INPUT_TYPES.DROPDOWN:
            return getDropdownInput(inputsData);
        case INPUT_TYPES.MONEY_INPUT:
            return getMoneyInput(inputsData);
        case INPUT_TYPES.REGULAR_INPUT:
            return getRegularInput(inputsData);
        default:
            return <p>{'Input\'s been lost'}</p>;
        }
    };

    const getRegularInput = inputsData => {
        const {
            additionalInput: {
                additionalInputText,
                required
            }
        } = inputsData;

        const propName = 'sourceName';

        return (
            <UniversalInput
                label={{
                    text: additionalInputText
                }}
                inputData={{
                    isInputRequired: required,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: propName,
                    value: localState[propName],
                    placeholder: additionalInputText,
                    validationType: VALIDATION_TYPES.NAME,
                    currentData: { [propName]: localState[propName] },
                    fields: []
                }}
                inputActions={{
                    onFieldChange
                }}
            />
        );
    };

    console.log(localState);

    const getMoneyInput = inputsData => {
        const {
            additionalInput: {
                additionalInputText,
                required,
                isOnlyPositiveAvailable,
            }
        } = inputsData;

        return (
            <UniversalInput
                label={{
                    text: additionalInputText,
                    labelClasses: ['usa-width-one-third']
                }}
                inputData={{
                    isInputRequired: required,
                    type: INPUT_TYPES.MONEY_INPUT,
                    name: 'relatedExpense',
                    value: localState.relatedExpense,
                    inputClasses: ['usa-width-two-thirds'],
                    isOnlyPositiveAvailable: !!isOnlyPositiveAvailable,
                    placeholder: additionalInputText,
                    validationType: VALIDATION_TYPES.MONEY,
                    currentData: { relatedExpense: localState.relatedExpense },
                    fields: []
                }}
                inputActions={{
                    onFieldChange
                }}
            />
        );
    };

    const getDropdownInput = inputsData => {
        const {
            additionalInput: {
                additionalInputText,
                required
            }
        } = inputsData;

        const propName = localState.type === INCOME_TYPES.OTHER ? 'subType' : 'profit';
        const options = localState.type === INCOME_TYPES.OTHER ? DROPDOWN_OPTIONS.OTHER_INCOME_SUBTYPES : DROPDOWN_OPTIONS.PROFIT_LOSS;

        return (
            <Fragment>
                <UniversalInput
                    label={{
                        text: additionalInputText
                    }}
                    inputData={{
                        isInputRequired: required,
                        type: INPUT_TYPES.DROPDOWN,
                        name: propName,
                        value: localState[propName],
                        returnAsBoolean: true,
                        placeholder: additionalInputText,
                        options: options,
                        validationType: VALIDATION_TYPES.CHOSEN,
                        currentData: { [propName]: localState[propName] },
                        fields: []
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                />
                {
                    localState.subType !== INCOME_TYPES.UNSPECIFIED &&

                        <InstructionalText
                            Component={
                                <p className="usa-grid" data-instructions={`source_${localState.subType}`}/>
                            }
                            styleType="MODAL"
                        />
                }
            </Fragment>
        );
    };


    const getRestOfInputs = () => {
        const inputsData = parameter === 'incomes' ?
            INCOME_SOURCES.find(source => source.type === localState.type) :
            DEDUCTIONS_SOURCES.find(source => source.type === localState.type);

        const mockInputsData = {
            isOnlyPositiveAvailable: false,
            additionalInput: {
                positionOnPage: null,
            },
            notice: {
                positionOnPage: null,
                text: ''
            },
            questionText: '',
            frequencyOptions: 'type_0'
        };

        const {
            isOnlyPositiveAvailable,
            additionalInput: {
                positionOnPage: inputPosition
            },
            notice: {
                positionOnPage: noticePosition,
                text: noticeText
            },
            questionText,
            frequencyOptions
        } = inputsData ? inputsData : mockInputsData;

        const getFrequencyOptions = () => {
            return DROPDOWN_OPTIONS
                .FREQUENCY
                .filter(fr => FREQUENCY_OPTIONS_TYPES[frequencyOptions].includes(fr.value));
        };


        return inputsData ?
            <Fragment>
                {
                    noticePosition === 0 &&
                    <div className='usa-grid'>
                        <p className="usa-width-one-whole controls"><strong>
                            { adjustQuestionText(noticeText) }
                        </strong></p>
                    </div>
                }

                {
                    inputPosition === 1 &&
                    getAdditionalInputByType(inputsData)
                }

                {
                    noticePosition === 1 && localState.subType === OTHER_INCOME_TYPES.OTHER_INCOME &&
                    <div className='usa-grid'>
                        <p className="usa-width-one-whole controls"><strong>
                            { adjustQuestionText(noticeText) }
                        </strong></p>
                    </div>
                }

                <div className='usa-grid'>
                    <p className="usa-width-one-whole controls"><strong>
                        { adjustQuestionText(questionText) }
                    </strong></p>
                </div>

                {
                    noticePosition === 2 &&
                    <div className='usa-grid'>
                        <p className="usa-width-one-whole controls"><strong>
                            { adjustQuestionText(noticeText) }
                        </strong></p>
                    </div>
                }

                <UniversalInput
                    label={{
                        text: 'Amount',
                        labelClasses: ['usa-width-one-third']
                    }}
                    inputData={{
                        isInputRequired: true,
                        type: INPUT_TYPES.MONEY_INPUT,
                        name: 'amount',
                        value: localState.amount,
                        inputClasses: ['usa-width-two-thirds'],
                        isOnlyPositiveAvailable: !!isOnlyPositiveAvailable,
                        placeholder: 'Amount',
                        validationType: VALIDATION_TYPES.MONEY,
                        currentData: { amount: localState.amount },
                        fields: []
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                />

                {
                    inputPosition === 2 &&
                    getAdditionalInputByType(inputsData)
                }

                <UniversalInput
                    label={{
                        text: 'How often?',
                        isLabelRequired: false
                    }}
                    inputData={{
                        isInputRequired: true,
                        type: INPUT_TYPES.DROPDOWN,
                        name: 'frequency',
                        value: localState.frequency,
                        options: getFrequencyOptions(),
                        validationType: VALIDATION_TYPES.CHOSEN,
                        currentData: { frequency: localState.frequency },
                        fields: []

                    }}
                    inputActions={{
                        onFieldChange
                    }}
                />

                {
                    (localState.frequency === FREQUENCY_TYPES.HOURLY ||
                        localState.frequency === FREQUENCY_TYPES.DAILY) &&
                        localState.type === INCOME_TYPES.JOB &&

                        <Fragment>
                            <p className="usa-width-one-whole controls">
                                <strong>
                                    { adjustQuestionText('How much does [Full Name] usually work per week at this job?' ) }
                                </strong>
                            </p>
                            <UniversalInput
                                label={{
                                    text: localState.frequency === FREQUENCY_TYPES.HOURLY ?
                                        'Hours per week:' : 'Days per week:'
                                }}
                                inputData={{
                                    isInputRequired: true,
                                    type: INPUT_TYPES.REGULAR_INPUT,
                                    name: 'cyclesPerFrequency',
                                    value: localState.cyclesPerFrequency,
                                    placeholder: 'Provide qty',
                                    validationType: VALIDATION_TYPES.RANGE,
                                    valuesToMatch: localState.frequency === FREQUENCY_TYPES.HOURLY ? [1, 168] : [1, 7],
                                    validationErrorMessage: localState.frequency === FREQUENCY_TYPES.HOURLY ?
                                        VALIDATION_ERROR_MESSAGES.HOURS_PER_WEEK :
                                        VALIDATION_ERROR_MESSAGES.DAYS_PER_WEEK,
                                    currentData: { cyclesPerFrequency: localState.cyclesPerFrequency },
                                    fields: []
                                }}
                                inputActions={{
                                    onFieldChange
                                }}
                            />
                        </Fragment>
                }

                {
                    noticePosition === 3 &&
                    <div className='usa-grid-full'>
                        <p className="usa-width-one-whole controls"><strong>
                            { adjustQuestionText(noticeText) }
                        </strong></p>
                    </div>
                }

                {
                    inputPosition === 3 &&
                    getAdditionalInputByType(inputsData)
                }

            </Fragment> :
            null;
    };

    const getOptions = (parameter, option) => {
        switch(parameter) {
        case 'incomes':
            return option ?
                DROPDOWN_OPTIONS.INCOME_SOURCES_TRIBAL_INCOME : index !== null ?
                    [DROPDOWN_OPTIONS.INCOME_SOURCES.find(option => option.value === type)] :
                    DROPDOWN_OPTIONS.INCOME_SOURCES;
        case 'expenses' :
            return index !== null ?
                [DROPDOWN_OPTIONS.DEDUCTION_SOURCES.find(option => option.value === type)] :
                DROPDOWN_OPTIONS.DEDUCTION_SOURCES;
        }
    };

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };
    return (
        <Fragment>
            <div className="modal__header" id="modal__validate__address">
                <h2>
                    {
                        parameter === 'incomes' ?
                            `Add Income for ${normalizeNameOfPerson(name)}` :
                            `Add Deduction for ${normalizeNameOfPerson(name)}`
                    }
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div id="iiAddIncomeOrDeductionModal" className="usa-grid">
                <div className="modal__body">
                    <FormForModal>
                        <UniversalInput
                            label={{
                                text: `What type of ${parameter ? 'income' : 'deduction'} would you like to add?`,
                                isLabelRequired: false,
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.DROPDOWN,
                                name: 'type',
                                value: localState.type,
                                options: getOptions(parameter, option),
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: { type: localState.type },
                                fields: []

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />

                        {
                            (localState.type !== null && localState.type !== INCOME_TYPES.OTHER) && parameter === 'incomes' &&

                            <InstructionalText
                                Component={
                                    <p className="usa-grid" data-instructions={`source_${localState.type}`}/>
                                }
                                styleType="MODAL"
                            />
                        }

                        {
                            localState.type && getRestOfInputs()
                        }
                    </FormForModal>
                </div>
            </div>
            <div className="modal__footer">
                <div className='usa-grid controls'>
                    <div className='usa-offset-one-third txt-left'>
                        <Button
                            className='modal-cancel-btn usa-button-secondary'
                            onClick={handleHideClick}
                        >
                            Cancel
                        </Button>
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleSaveClick}
                            disabled={!localState.type}
                        >
                            Save
                        </Button>
                    </div>
                </div>
            </div>

        </Fragment>
    );
};

const mapStateToProps = state => ({
    contexts: state.contexts,
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        hideModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AddIncomeOrDeductionSourceModal);
