<script type="text/ng-template" id="maritalstatus-ssn.jsp">
  <div class="gutter10" ng-controller="maritalStatusSSN" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
    <h3><spring:message code="label.lce.ssn"/></h3>
    <form class="form-horizontal" id="maritalStatusSSNForm" name="maritalStatusSSNForm" novalidate>

    	<div>
    		<p class="alert alert-info">
  				
          
          <spring:message code="label.lce.ssn.p1"/>
  	
          <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
          <spring:message code="label.lce.ssn.p2"/>
           <!-- <span ng-switch on="householdMember.gender" class="ssngGender">
              <span ng-switch-when="male"> <spring:message code="label.lce.paragraphhim"/> </span>
              <span ng-switch-when="female"> <spring:message code="label.lce.paragraphher"/> </span>
              <span ng-switch-default></span>
           </span> -->
  				<spring:message code="label.lce.ssn.p3"/>
    		</p>
    	</div>

    		<!-- Spouse have a SSN? -->
        <div id="ssnNumber">
          <div>
            <spring:message code="label.lce.does"/>
            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
            <spring:message code="label.lce.ssn.p8"/>
            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
          </div>
          <div class="margin10-l">
            <label class="radio-inline" for="snn4spouseT">
              <input type="radio" id="snn4spouseT" name="snn4spouse" required ng-value="true" value="true" ng-model="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-click="householdMember.socialSecurityCard.reasonableExplanationForNoSSN='';">
              <spring:message code="label.lce.yes"/>
            </label>
            <label class="radio-inline" for="snn4spouseF">
              <input type="radio" id="snn4spouseF" name="snn4spouse" onclick='removeSSN();' required ng-value="false" value="false" ng-model="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator">
              <spring:message code="label.lce.no"/>
            </label>
          </div><!-- controls -->
          <div class="margin30-l margin10-t">
            <span class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.snn4spouse.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
          </div>
        </div><!-- Spouse have a SSN? -->

        <!-- Spouse has an SSN-->
        <div class="row-fluid control-group ssnum margin10-t" ng-show="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator">
          <div class="control-group">
            <label class="control-label" for="socialSecurityNumber">
              <spring:message code="label.lce.ssn"/>
              <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
            </label>
			<label class="aria-hidden" for="ssn1">SSN1</label>
			<label class="aria-hidden" for="ssn2">SSN2</label>
			<label class="aria-hidden" for="ssn3">SSN3</label>
            <div class="controls">
          	 <input type="text" id="socialSecurityNumber" name="socialSecurityNumber" ng-model="householdMember.socialSecurityCard.socialSecurityNumber" style="display:none" ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-pattern="/^([1-57-8][0-9]{2}|0([1-9][0-9]|[0-9][1-9])|6([0-57-9][0-9]|[0-9][0-57-9]))([1-9][0-9]|[0-9][1-9])([1-9]\d{3}|\d[1-9]\d{2}|\d{2}[1-9]\d|\d{3}[1-9])$/" ng-minlength="9" ng-maxlength="9" />
              <input type="text" placeholder="XXX" class="span2" id="ssn1" name="ssn1"  minlength="3" maxlength="3" ng-model="ssn1" onkeyup="ssnMask('ssn1')">
              <input type="text" placeholder="XX" class="span2" id="ssn2" name="ssn2"   minlength="2" maxlength="2" ng-model="ssn2" onkeyup="ssnMask('ssn2')">
              <input type="text" placeholder="XXXX" class="span2" id="ssn3" name="ssn3" minlength="4" maxlength="4" ng-model="ssn3" onkeyup="ssnMask('ssn3')">
            </div>
            <div class="controls margin10-t">
              <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.socialSecurityNumber.$error.required"><spring:message code="label.lce.pleaseenterssn"/></div>
              <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.socialSecurityNumber.$error.pattern "><spring:message code="label.lce.pleaseentervalidssn"/></div>
            </div>
          </div>

          <div id="isthenamethesameasssn">
    			  <div>
              <spring:message code="label.lce.is"/>
              <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
              <spring:message code="label.lce.thesamenamethatappearsonssn"/>
             
                <spring:message code="label.lce.paragraphhis"/>/<spring:message code="label.lce.paragraphher"/> 

              <spring:message code="label.lce.paragraphsocialseccard"/>
              <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
            </div>
    			  <div class="margin10-l">
    			    <label class="radio-inline" for="samenameT">
    			      <input type="radio" id="samenameT" name="samename" ng-value="true" onclick='updateNameOnSsnCard();' ng-model="householdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator"> <spring:message code="label.lce.yes"/>
    	          </label>
    	        <label class="radio-inline" for="samenameF">
    			      <input type="radio" id="samenameF" name="samename" ng-value="false"  ng-model="householdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator"> <spring:message code="label.lce.no"/>
    			    </label>
    	      </div>
    	      <!-- Error -->
            <div class="margin30-l margin10-t">
              <span class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.samename.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
            </div>
          </div><!-- Spouse has a SSN -->

          <!-- //This displays when the spouses's name is not the same on the SSN -->
          <div class="ssnName margin30-t" ng-show="householdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false">

          <h4>
            <spring:message code="label.lce.enterthesamename"/>
            <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
            <spring:message code="label.lce.sssncard"/>
          </h4>

          <div class="control-group margin10-t">
            <label class="control-label" for="firstNameOnSSNCard">
              <spring:message code="label.lce.firstname"/>
              <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
            </label>
            <div class="controls">
              <input type="text" id="firstNameOnSSNCard" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" ng-model="householdMember.socialSecurityCard.firstNameOnSSNCard" class="input-large span6" ng-trim ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator && householdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
            </div>
  	        <!-- Error -->
  	        <div class="controls margin5-t">
  	          <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.firstNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenterafirstname"/></div>
  			      <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.firstNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></div>
  	        </div>
          </div><!-- First Name -->
          <div class="control-group margin10-t">
            <label class="control-label" for="middleNameOnSSNCard"><spring:message code="label.lce.middlename"/> </label>
            <div class="controls">
              <input type="text" id="middleNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" name="middleNameOnSSNCard" ng-model="householdMember.socialSecurityCard.middleNameOnSSNCard" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
            </div>
          </div><!-- Middle Name -->
          <div class="control-group margin10-t">
            <label class="control-label" for="lastNameOnSSNCard">
              <spring:message code="label.lce.lastname"/>
              <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
            </label>
            <div class="controls">
              <input type="text" id="lastNameOnSSNCard" name="lastNameOnSSNCard" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="householdMember.socialSecurityCard.lastNameOnSSNCard" class="input-large span6" ng-trim ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator && householdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
            </div>
  	        <!-- Error -->
  	        <div class="controls margin5-t">
  	          <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.lastNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenteralastname"/></div>
  		        <div class="validation-error-text-container" ng-if="submitted && (maritalStatusSSNForm.lastNameOnSSNCard.$error.pattern || maritalStatusSSNForm.lastNameOnSSNCard.$error.maxlength)"><spring:message code="label.lce.entervalidlastname"/></div>
  	        </div>
          </div><!-- Last Name -->
          <div class="control-group margin10-t">
            <label class="control-label" for="suffixOnSSNCard">
              <spring:message code="label.lce.suffix"/>
            </label>
            <div class="controls">
              <select class="input-large" id="suffixOnSSNCard" name="suffixOnSSNCard" ng-model="householdMember.socialSecurityCard.suffixOnSSNCard" ng-options="s for s in suffix" >
                <option value=""><spring:message code="label.lce.suffix"/></option>
              </select>
            </div>
          </div><!-- Suffix -->
        </div><!-- SSN Name the Same -->
      </div><!-- SSN -->

      <h4 ng-if="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" class="margin30-t">
        <spring:message code="label.lce.ifnossn"/>
      </h4>
    	<!-- This will display if spouse does not have a SSN -->
      <div class="noSSN" ng-if="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
        <div>
         <div>
			<label class="aria-hidden" for="noSSNReason">SSN Explanation</label>
           <select id="noSSNReason" class="margin10-l" name="noSSNReason" ng-model="householdMember.socialSecurityCard.reasonableExplanationForNoSSN" ng-required="householdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
             <option value="">Select Explanation</option>
             <option value="RELIGIOUS_EXCEPTION"><spring:message code="label.lce.religiousexemption"/></option>
             <option value="JUST_APPLIED"><spring:message code="label.lce.justapplied"/></option>
             <option value="ILLNESS_EXCEPTION"><spring:message code="label.lce.illnessexemption"/></option>
             <option value="CITIZEN_EXCEPTION"><spring:message code="label.lce.citizenexemption"/></option>
           </select>
         <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
         </div>
  		   <!-- Error -->
  		   <div class="margin10-l">
    	     <span class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.noSSNReason.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
         </div>
        </div>

        <div class="noSSN">
    	    <div class="control-group hidden">
    	      <label class="control-label" for="hidFirstLastNameLabel">
              <spring:message code="label.lce.does"/>
              <strong>{{householdMember.name.firstName | capitalize:true}} {{householdMember.name.middleName | capitalize:true}} {{householdMember.name.lastName | capitalize:true}}</strong>
              <spring:message code="label.lce.haveanindividualmandare"/>
				<input type="hidden" id="hidFirstLastNameLabel" />
            </label>
    	      <div class="controls margin20-t">
              <label class="radio inline" for="exemptionT">
                <input type="radio" id="exemptionT" name="exemption" value="true" class="exemptionYes" ng-model="householdMember.socialSecurityCard.individualMandateExceptionIndicator"> <spring:message code="label.lce.yes"/>
    	        </label>
              <label class="radio inline" for="exemptionF">
    		        <input type="radio" id="exemptionF" name="exemption"   value="false" class="exemptionNo" ng-model="householdMember.socialSecurityCard.individualMandateExceptionIndicator" "> <spring:message code="label.lce.no"/>
    	        </label>
  	        </div><!-- controls -->
    			  <!-- Error -->
    	      <div class="controls margin5-t">
              <div class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.exemption.$error.required">
                <spring:message code="label.lce.thisvalueisrequired"/>
              </div>
      	    </div>
    	    </div><!-- control-group -->

            <div ng-if="householdMember.socialSecurityCard.individualMandateExceptionIndicator=='true'">
               <div class="control-group" >
                 <label class="control-label" for="individualManadateExceptionID">
                  <spring:message code="label.lce.exemptionid"/>
                  <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
                </label>
                <div class="controls">
                  <input type="text" class="span5" id="individualManadateExceptionID" name="individualManadateExceptionID" ng-model="householdMember.socialSecurityCard.individualManadateExceptionID" ng-trim  placeholder='<spring:message code="label.lce.exemptionid"/>'></input>
  			        </div>
    		        <!-- Error -->
    		        <div class="controls margin5-t">
    		          <span class="validation-error-text-container" ng-if="submitted && maritalStatusSSNForm.individualManadateExceptionID.$error.required"><spring:message code="label.lce.exemptioniderror"/></span>
    		        </div>
              </div>
            </div>
          </div><!-- SSN -->
        </div>
      <!-- form-horizontal -->


      <!-- BACK & NEXT BUTTON -->
      <dl class="dl-horizontal pull-right">
        <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
        <a role="button" tabindex="0" ng-if="maritalStatusSSNForm.$valid" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
        <a role="button" tabindex="0" ng-if="!maritalStatusSSNForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true" ><spring:message code="label.lce.next"/></a>
        <!-- {{maritalStatusSSNForm.$error}} -->
      </dl>

    </form>
  </div><!-- gutter10 pull-right -->
</script>

<script>
var hiddenSsn1="";
var hiddenSsn2="";
var hiddenSsn3="";
var ssnMask = function(ssnID) {
	$("input[maxlength=3],input[maxlength=4],input[maxlength=2]").bind("keyup",function(event){
	    var keyCode = event.keyCode;
	    if( keyCode == 16 || keyCode == 9 || keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40){
	      return;
	    }
	    var maxLength = parseInt($(this).attr('maxlength'));
	    var nextElement = $(this).nextAll( "input[maxlength]:first");
	    if($(this).val().length == maxLength && nextElement !== undefined){
	      nextElement.focus();
	      nextElement.select();
	    }
	});
	if ( $('#ssn1').val().split("").length === 3 && 'ssn1' === ssnID) {
		if($('#ssn1').val().indexOf('***') == -1){
			hiddenSsn1=$('#ssn1').val();
		}
		$('#ssn1').val("***")
	}
	if ( $('#ssn2').val().split("").length === 2 && 'ssn2' === ssnID) {
		if($('#ssn2').val().indexOf('**') == -1){
			hiddenSsn2=$('#ssn2').val();
		}
		$('#ssn2').val("**")
	}
	if( 'ssn3' === ssnID){
		hiddenSsn3=$('#ssn3').val();
	}
	$('#socialSecurityNumber').val(hiddenSsn1 +hiddenSsn2+hiddenSsn3).change();
	$('#socialSecurityNumber').triggerHandler("change");

};

var updateNameOnSsnCard = function(){
	//$sope.householdMember.
	var scope = angular.element($("#firstNameOnSSNCard")).scope();
	$('#firstNameOnSSNCard').val(scope.householdMember.name.firstName).change();
	$('#firstNameOnSSNCard').triggerHandler("change");

	$('#middleNameOnSSNCard').val(scope.householdMember.name.middleName).change();
	$('#middleNameOnSSNCard').triggerHandler("change");

	$('#lastNameOnSSNCard').val(scope.householdMember.name.lastName).change();
	$('#lastNameOnSSNCard').triggerHandler("change");

	$('#suffixOnSSNCard').val(scope.householdMember.name.suffix).change();
	$('#suffixOnSSNCard').triggerHandler("change");

};

var removeSSN = function(){
	$('#socialSecurityNumber').val('').change();
	$('#socialSecurityNumber').triggerHandler("change");
	$('#ssn1').val('').change();
	$('#ssn1').triggerHandler("change");
	$('#ssn2').val('').change();
	$('#ssn2').triggerHandler("change");
	$('#ssn3').val('').change();
	$('#ssn3').triggerHandler("change");

	$('#firstNameOnSSNCard').val('').change();
	$('#firstNameOnSSNCard').triggerHandler("change");

	$('#middleNameOnSSNCard').val('').change();
	$('#middleNameOnSSNCard').triggerHandler("change");

	$('#lastNameOnSSNCard').val('').change();
	$('#lastNameOnSSNCard').triggerHandler("change");

	$('#suffixOnSSNCard').val('').change();
	$('#suffixOnSSNCard').triggerHandler("change");
};
</script>