package com.getinsured.hix.batch.enrollment.reader;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.enrollment.skip.Enrollment1095Out;

public class EnrollmentAnnual1095OutReader implements ItemReader<Integer> {
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIRSOutReader.class);
	List<Integer> enrollmentIds;		
	int partition;
	int startIndex;
	int endIndex;
	int loopCount;
	private Enrollment1095Out enrollment1095Out;
	

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");
		
		ExecutionContext ec = stepExecution.getExecutionContext();
		if(ec != null){
			partition =ec.getInt("partition");
			startIndex=ec.getInt("startIndex");
			endIndex=ec.getInt("endIndex");
			if(enrollment1095Out!=null && enrollment1095Out.getEnrollmentIdList()!=null && !enrollment1095Out.getEnrollmentIdList().isEmpty()) {
				if(startIndex<enrollment1095Out.getEnrollmentIdList().size() && endIndex<=enrollment1095Out.getEnrollmentIdList().size()){
					enrollmentIds= new ArrayList<Integer>(enrollment1095Out.getEnrollmentIdList().subList(startIndex, endIndex));
				}
				LOGGER.info("======= Enrollment_1095_PDF :: Thread Name: "+Thread.currentThread().getName() +" StartIndex: "+startIndex+" EndIndex: "+ endIndex  +" No Of enrollmentID: " +enrollmentIds);
			}
		}
	}
	
	@Override
	public Integer read() throws Exception, UnexpectedInputException,
			ParseException, NonTransientResourceException {
		
		if(enrollmentIds!=null && !enrollmentIds.isEmpty()){
			if(loopCount<enrollmentIds.size()){
				loopCount++;
				return enrollmentIds.get(loopCount-1);
				
			}else{
				return null;
			}
			
		}
			return null;
	}

	public List<Integer> getEnrollmentIds() {
		return enrollmentIds;
	}

	public void setEnrollmentIds(List<Integer> enrollmentIds) {
		this.enrollmentIds = enrollmentIds;
	}

	public Enrollment1095Out getEnrollment1095Out() {
		return enrollment1095Out;
	}

	public void setEnrollment1095Out(Enrollment1095Out enrollment1095Out) {
		this.enrollment1095Out = enrollment1095Out;
	}

	
	
}
