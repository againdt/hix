package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BooleanValueSetProcessor implements ProviderDataFieldProcessor {

    private ValidationContext context;
    private int index;
    private Logger logger = LoggerFactory.getLogger(BooleanValueSetProcessor.class);

    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated) throws ProviderValidationException, InvalidOperationException {
        String[] values = {"true", "false"};
        boolean valid = false;
        String validatedData = null;
        if (null != objToBeValidated) {
            validatedData = objToBeValidated.toString().trim();
            for (String s : values) {
                if (s.equalsIgnoreCase(validatedData)) {
                    valid = true;
                    break;
                }
            }
        }
        if(!valid){
            throw new ProviderValidationException("Validation failed for value:"+objToBeValidated+" One of the following was expected: true, false");
        }
        return validatedData;
    }
}
