package com.getinsured.hix.serff.service.networkTransparency;

import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.COUNTY;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.NETWORK_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.PLAN_HIOS_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.RATING;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.HIOS_ISSUER_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.ISSUER_NAME;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.PLAN_YEAR;
import static com.getinsured.hix.util.PlanMgmtConstants.SPACE;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.model.serff.SerffDocument;
import com.getinsured.hix.model.serff.SerffPlanMgmt;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.serff.service.SerffService;
import com.getinsured.hix.serff.service.excel.ExcelReader;
import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyVO.NetworkTransparencyDataVO;
import com.getinsured.hix.util.PlanMgmtConstants;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 *
 * Class is used to read data from Network Transparency Excel.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
@Service("networkTransparencyReader")
public class NetworkTransparencyReader extends ExcelReader {

	private static final Logger LOGGER = Logger.getLogger(NetworkTransparencyReader.class);
	private static final String MSG_ERROR_NW_TRANS_PLANS = "Failed to upload Network Transparency file.";
	private static final String MSG_ERROR_NO_TRACKING_RECORD = "No tracking record found.";
	private static final int SHEET_1 = 0;

	@Autowired private SerffService serffService;
	@Autowired private ContentManagementService ecmService;
	@Autowired private PlanMgmtUtil pmUtil;

	public NetworkTransparencyVO loadExcel(InputStream inputStream, String fileName, SerffPlanMgmt trackingRecord) {

		LOGGER.info("loadExcel() Start");
		NetworkTransparencyVO networkTransMainVO = new NetworkTransparencyVO();
		StringBuilder errorMessages = new StringBuilder();

		try {

			if (!validateInputParams(inputStream, fileName, trackingRecord, errorMessages)) {
				errorMessages.append(MSG_ERROR_NW_TRANS_PLANS);
				return networkTransMainVO;
			}
			// Upload Excel File to ECM
			SerffDocument attachment = addExcelInECM(errorMessages, trackingRecord, fileName, inputStream);

			if (null != attachment) {

				byte[] content = ecmService.getContentDataById(attachment.getEcmDocId());
				InputStream myInputStream = new ByteArrayInputStream(content);
				// Load Excel File
				loadFile(myInputStream);
				// Get first sheet from the workbook
				XSSFSheet networkTransSheet = getSheet(SHEET_1);

				if (null != networkTransSheet) {
					populateNetworkTransparencyData(networkTransMainVO, networkTransSheet, errorMessages);
				}
				else {
					errorMessages.append("Invalid sheet of Network Transparency Excel.");
				}
			}
		}
		catch (Exception ex) {
			errorMessages.append(MSG_ERROR_NW_TRANS_PLANS);
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (StringUtils.isNotBlank(errorMessages.toString())) {
				LOGGER.error(errorMessages);
				networkTransMainVO.setErrorMessages(errorMessages.toString());
				networkTransMainVO.setNotValid(true);
			}
			LOGGER.info("loadExcel() End");
		}
		return networkTransMainVO;
	}

	private void populateNetworkTransparencyData(NetworkTransparencyVO networkTransMainVO, XSSFSheet networkTransSheet, StringBuilder errorMessages) {

		LOGGER.debug("populateNetworkTransparencyData() Start");
		boolean commonDataFound = false;
		boolean headerFound = false;

		try {
			// Iterate through each rows from first sheet
			Iterator<Row> rowIterator = networkTransSheet.iterator();

			while (rowIterator.hasNext()) {
				Row excelRow = null;

				if (!commonDataFound) {
					commonDataFound = true;

					excelRow = rowIterator.next(); // Get Plan Year
					if (excelRow != null) {
						networkTransMainVO.setPlanYear(getCellValueTrim(excelRow.getCell(PLAN_YEAR.getRowIndex()), true));
					}

					excelRow = rowIterator.next();	// Get Issuer Name
					if (excelRow != null) {
						networkTransMainVO.setIssuerName(getCellValueTrim(excelRow.getCell(ISSUER_NAME.getRowIndex())));
					}

					excelRow = rowIterator.next();	// Get HIOS Issuer ID
					if (excelRow != null) {
						networkTransMainVO.setHiosIssuerID(getCellValueTrim(excelRow.getCell(HIOS_ISSUER_ID.getRowIndex()), true));
					}
				}
				excelRow = rowIterator.next();	// Start reading Network Transparency record

				if (!headerFound) {

					if (NETWORK_ID.getColumnName().equalsIgnoreCase(getCellValueTrim(excelRow.getCell(NETWORK_ID.getColumnIndex())))) {
						headerFound = true;
					}
					continue;
				}

				if (StringUtils.isBlank(getCellValueTrim(excelRow.getCell(NETWORK_ID.getColumnIndex())))
						&& StringUtils.isBlank(getCellValueTrim(excelRow.getCell(PLAN_HIOS_ID.getColumnIndex())))
						&& StringUtils.isBlank(getCellValueTrim(excelRow.getCell(COUNTY.getColumnIndex())))
						&& StringUtils.isBlank(getCellValueTrim(excelRow.getCell(RATING.getColumnIndex())))) {
					LOGGER.warn("Due to Blank row, stop reading from Excel.");
					break;
				}
				setNetworkTransparencyData(excelRow, networkTransMainVO);
			}
		}
		finally {

			if (!headerFound) {
				errorMessages.append("Network Transparency Template is invalid.");
			}
			LOGGER.debug("populateNetworkTransparencyData() End");
		}
	}

	/**
	 * Method is used to set Network Transparency Data.
	 */
	private void setNetworkTransparencyData(Row excelRow, NetworkTransparencyVO networkTransMainVO) {
		NetworkTransparencyDataVO networkTransDataVO = networkTransMainVO.getNetworkTransDataVO();
		networkTransDataVO.setNetworkID(getCellValueTrim(excelRow.getCell(NETWORK_ID.getColumnIndex())));
		networkTransDataVO.setPlanHiosID(getCellValueTrim(excelRow.getCell(PLAN_HIOS_ID.getColumnIndex())));
		networkTransDataVO.setCounty(getCellValueTrim(excelRow.getCell(COUNTY.getColumnIndex())));
		networkTransDataVO.setRating(getCellValueTrim(excelRow.getCell(RATING.getColumnIndex())));

		networkTransMainVO.setNetworkTransDataVOInList(networkTransDataVO);	// Set Network Transparency Child Data
	}

	/**
	 * This method add uploaded excel to ECM.
	 */
	private SerffDocument addExcelInECM(StringBuilder message, SerffPlanMgmt trackingRecord, String fileName,
			InputStream inputStream) throws IOException {

		LOGGER.debug("addExcelInECM() Start");
		SerffDocument attachment = null;

		try {
			String ecmFolderName = trackingRecord.getSerffReqId() + PlanMgmtConstants.FTP_NW_TRANS_FILE + TSCalendar.getInstance().getTimeInMillis();
			String basePath = PlanMgmtConstants.NW_TRANS_ECM_BASE_PATH + ecmFolderName + PlanMgmtConstants.PATH_SEPERATOR;

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Uploading File Name with path to ECM:" + basePath + fileName);
			}
			// Uploading Network Transparency Excel at ECM.
			attachment = pmUtil.addAttachmentInECM(ecmService, trackingRecord, basePath, fileName, IOUtils.toByteArray(inputStream));
			serffService.saveSerffDocument(attachment);
		}
		catch (Exception ex) {
			message.append(MSG_ERROR_NW_TRANS_PLANS);
			trackingRecord.setRequestStateDesc(ex.getMessage());
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {
			LOGGER.debug("addExcelInECM() End");
		}
		return attachment;
	}

	/**
	 * Method is used to validate input parameters.
	 */
	private boolean validateInputParams(InputStream inputStream, String fileName, SerffPlanMgmt trackingRecord, StringBuilder errorMessage) {

		LOGGER.debug("validateInputParams() Start");
		boolean isValid = true;

		try {
			List<String> errorList = new ArrayList<>();

			if (null == inputStream) {
				errorList.add("Network Transparency File");
			}

			if (StringUtils.isBlank(fileName)) {
				errorList.add("File Name");
			}

			if (null == trackingRecord) {
				LOGGER.error(MSG_ERROR_NO_TRACKING_RECORD);
				errorList.add("Tracking Record");
			}

			if (!CollectionUtils.isEmpty(errorList)) {
				isValid = false;
				errorMessage.append(MSG_ERROR_NW_TRANS_PLANS);
				errorMessage.append(SPACE);
				errorMessage.append("Invalid Input Parameters: ");
				errorMessage.append(errorList.toString());
			}
		}
		finally {

			if (!isValid) {
				LOGGER.error(errorMessage.toString());

				if (null != trackingRecord) {
					trackingRecord.setRequestStateDesc(errorMessage.toString());
				}
			}
			LOGGER.debug("validateInputParams() End");
		}
		return isValid;
	}
}
