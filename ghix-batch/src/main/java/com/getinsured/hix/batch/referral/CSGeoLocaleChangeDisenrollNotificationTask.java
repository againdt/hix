/**
 * 
 */
package com.getinsured.hix.batch.referral;

import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.referral.service.CSChangeDisenrollService;

/**
 * @author Biswakesh
 *
 */
public class CSGeoLocaleChangeDisenrollNotificationTask extends
		BaseLCEReminderNotificationTask {
	
	private static final Logger LOGGER = Logger.getLogger(CSGeoLocaleChangeDisenrollNotificationTask.class);
	
	private CSChangeDisenrollService csChangeDisenrollService;

	/* (non-Javadoc)
	 * @see org.springframework.batch.core.step.tasklet.Tasklet#execute(org.springframework.batch.core.StepContribution, org.springframework.batch.core.scope.context.ChunkContext)
	 */
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		try {	
			csChangeDisenrollService.processCSGeoLocaleChange();
		} catch(Exception ex) {
			LOGGER.error("ERR: WHILE EXECUTING CS PLAN CHANGE BATCH: ",ex);
		}
		
		return RepeatStatus.FINISHED;
	}

	public CSChangeDisenrollService getCsChangeDisenrollService() {
		return csChangeDisenrollService;
	}

	public void setCsChangeDisenrollService(
			CSChangeDisenrollService csChangeDisenrollService) {
		this.csChangeDisenrollService = csChangeDisenrollService;
	}
}
