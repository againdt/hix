package com.getinsured.hix.indportal.dto.dm.application;

public class AnnualTaxIncome {
	 private Float incomeAmount;
	 private Boolean unknownIncomeIndicator;
	 private Boolean variableIncomeIndicator;


	 // Getter Methods 

	 public Float getIncomeAmount() {
	  return incomeAmount;
	 }

	 public Boolean getUnknownIncomeIndicator() {
	  return unknownIncomeIndicator;
	 }

	 public Boolean getVariableIncomeIndicator() {
	  return variableIncomeIndicator;
	 }

	 // Setter Methods 

	 public void setIncomeAmount(Float incomeAmount) {
	  this.incomeAmount = incomeAmount;
	 }

	 public void setUnknownIncomeIndicator(Boolean unknownIncomeIndicator) {
	  this.unknownIncomeIndicator = unknownIncomeIndicator;
	 }

	 public void setVariableIncomeIndicator(Boolean variableIncomeIndicator) {
	  this.variableIncomeIndicator = variableIncomeIndicator;
	 }
	}