<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.9.2.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />

<style>
.line {
border-right: 1px solid #e3e3e3 !important;
}

#contribution_error label{margin: -12px 0 30px 142px;}

.display{display: block !important;}
</style>

<script type="text/javascript">
function validateForm(){
	$("#frmcontribution").submit();	
}

function clearNavigation(){
	$('li.navmenu').each(function(index, element) {
		var index = index+1;
		if(index > ${pageNo}){
			var link = $(this).find($('a')).attr('href');
			var finalHtml = $(this).html().replace('<i class="icon-ok"></i>', '<span>' + index +'. </span>').replace(link, '#');
	        $(this).html(finalHtml);
		}
	});
}

function populateEmployeeRange(){
	if( $("#frmcontribution").validate().form() ) {
		var validateUrl = '<c:url value="/shop/employer/plandisplay/getEmployeePremiumRange">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
       $.ajax({
			url: validateUrl, 
			type: "POST", 
			dataType:'json',
			data: {coverageEmployee: $("#employeeContribution").val(),coverageDependent: $("#dependentContribution").val()}, 
			success: function(response,xhr)
			{
		        if(isInvalidCSRFToken(xhr))
		          return;
				var obj = response;
				$("#employee_range").html(obj['employeeRange'] + ' / <spring:message code = "label.month"/>');
				$("#dependent_range").html(obj['dependentRange'] + ' / <spring:message code = "label.month"/>');
				$("#taxCreditRange").html(obj['taxCreditRange'] + ' / <spring:message code = "label.month"/>');
				$("#total").html("<b>" + obj['total'] + ' / <spring:message code = "label.month"/>' + "</b>");
				clearNavigation();
			},
       });
    }
}
</script>

<div class="gutter10">
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<div class="accordion graysidebar" id="accordion2">
			<util:nav pageName="${page_name}"></util:nav>
		</div>
		<c:if test="${errorMsg == '' }">
			<util:selection pageName="${page_name}"></util:selection>
		</c:if>
	</div>
	<!--sidebar-->
	<div class="span9" id="rightpanel">
	<h4 class="graydrkbg gutter10" id="skip">
			<c:choose>
			<c:when test="${customEffectiveDate == 'YES' }">
				<a href="<c:url value="/shop/employer/plandisplay/coveragestart"/>" class="btn"> <spring:message code = "label.back"/></a>
			</c:when>
			<c:otherwise>
				<a href="<c:url value="/shop/employer/plandisplay/selectcoverage"/>" class="btn"> <spring:message code = "label.back"/></a>
			</c:otherwise>
			</c:choose>
            <span class="offset1"><spring:message code = "label.proposeContribution"/></span>
			<a tabindex="0" type="button" id="submitButton" onClick="javascript:validateForm();" class="btn btn-primary pull-right"><spring:message  code="label.next"/></a>
	</h4>		
		<c:if test="${errorMsg != ''}">
			<p></p>
			<div class="row-fluid"> 
				<div style="font-size: 15px; color: red">
						&nbsp;<p><c:out value="${errorMsg}"></c:out><p/>
						<script type="text/javascript">$('#nextbutton').hide(); </script>
					<br>
				</div>
			</div>
		</c:if>
<c:if test="${errorMsg == '' }">
		<!--titlebar-->
		<form id="frmcontribution" name="frmcontribution" action="contribution" method="post">
			<df:csrfToken/>
		<input type="hidden" id="employeeContribution" name="employeeContribution" />
		<input type="hidden" id="dependentContribution" name="dependentContribution" />
			<div class="gutter10">
				<!-- <p>To help you narrow down to the right choices for your
					business, start by entering the percentage you think you'd like to
					contribute towards the premiums for your employees. Don't worry, you 
					can change these percentages later as
					you narrow down your choices further.</p>-->
					<p><spring:message code = "label.slideBarInfo"/></p>
					
				<div class="well">
			        <div style="margin:15px 0;">
			              <p class="pull-left paddingT5" style="width:105px;"><strong><spring:message code = "label.employees"/></strong> (${totalEmp})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="employeeSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		            </div>
		            <div id="contribution_error"></div>
		            <div style="margin:30px 0; clear:both;">
			              <p class="pull-left paddingT5" style="width:105px;"><strong><spring:message code = "label.dependent"/> </strong> (${totalDependents})</p>
			              <div class="pull-left paddingT5 gutter10-lr">0%</div>
			              <div id="dependentSlider" class="slider" style="width:70%;float:left; height:30px"></div>
			              <div class="pull-left paddingT5 marginL10">100%</div>
			              <div class="clearfix"></div>
		            </div>
					<table class="table table-border-none table-condensed costs">
						<tr>
							<td colspan="2" class="line"><strong><spring:message code = "label.emplCosts"/></strong> 
							<a href="#" rel="tooltip" data-placement="top" data-original-title="<spring:message code = "label.monthlyCosts"/>" class="info"><i
									class="icon-question-sign"></i></a></td>
							<td>&nbsp;</td>
							<td><strong><spring:message code = "label.potentialTaxCredit"/></strong> <a href="#"
								rel="tooltip" data-placement="top"
								data-original-title="<spring:message code = "label.taxAdvice"/>" class="info"><i class="icon-question-sign"></i></a></td>
						</tr>
						<tr>
							<td style="width: 27%;"><spring:message code = "label.costFor"/> ${totalEmp} <spring:message code = "label.employees"/></td>
							<td id="employee_range"  class="line">${employeeRange} / <spring:message code = "label.month"/></td>
							<td>&nbsp;</td>
							<td id="taxCreditRange">${taxCreditRange} / <spring:message code = "label.month"/></td>
						</tr>
						<tr>
							<td><spring:message code = "label.costFor"/> ${totalDependents} <spring:message code = "label.dependent"/></td>
							<td id="dependent_range" class="line">${dependentRange} / <spring:message code = "label.month"/></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td><b><spring:message code = "label.total"/></b></td>
							<td id="total"><b>${total} / <spring:message code = "label.month"/></b></td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</div>
			</div>
		</form>
</c:if>
		<!--gutter10-->
	</div>
	<!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->

<script type="text/javascript">
$('.info').tooltip();
$(document).ready(function(){
	empMinContribution = ${empMinContribution};
	stateName = '${stateName}';
	var employeeTooltip = function(event, ui) {
		if(ui.value == undefined){ 
			ui.value = ${employerIndvContribution};
		}
        if(ui.value < empMinContribution){
        	ui.value = ${employerIndvContribution};
        	//$('#contribution_error').html("<label class='error' generated='true'><span><em class='excl'>!</em>here Employer contribution should be 50% or more than that.</span></label>");
            return false;
        }
		$('#employee').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em; width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='employee'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#employee').remove();
		 	
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#employeeContribution").val(ui.value);
	};
	var stopSlider = function(event, ui){
		if(ui.value <= empMinContribution){
        	ui.value = ${employerIndvContribution};
        	$('#contribution_error').html("<label class='error' generated='true'><span><em class='excl'>!</em>In "+stateName+", <spring:message code = "label.mincontribution"/> "+empMinContribution+"%.</span></label>");
        	$('#contribution_error').find('label.error').addClass('display').fadeOut(3000, "linear", complete);
        }
		else{
			$('#contribution_error label').html("").hide();
		}
		
		function complete() {
			$('#contribution_error label').removeClass('display')
		  }
	};
	
	
	var dependentTooltip = function(event, ui) {
		
		if(ui.value == undefined){ 
			ui.value = ${employerDepeContribution};
		}
		$('#dependent').remove();
		$(this).find(".ui-slider-range-min").html("<span id='employer'><spring:message code = "label.emplPays"/></span>");
		$(this).find("a.ui-slider-handle").html("<span style='float: left; display: block; margin-top: -20px; margin-left: -8px; color:#555; font-size:0.7em; width:35px;'>"+ui.value + "% </span>");
		$(this).find("a.ui-slider-handle").after("<span id='dependent'><spring:message code = "label.employeePays"/></span>");
		if(ui.value == 100){
			//$(this).find("a.ui-slider-handle").find('span').remove();
			$('#dependent').remove();
		}else if(ui.value < 1){
			$(this).find(".ui-slider-range-min").find('span').remove();	
		}
		$( "#sliderdiv" ).html( ui.value + "%" );
        $( "#dependentContribution").val(ui.value);
	};
	
	$( "#employeeSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: ${employerIndvContribution} ,
	    create: employeeTooltip,
		slide:  employeeTooltip,
		change: populateEmployeeRange,
		stop: stopSlider
	});
	$( "#dependentSlider" ).slider({ orientation: "horizontal",
		range: "min",
		min: 0,
		max: 100,
		step: 5,
		value: ${employerDepeContribution} ,
	    create: dependentTooltip,
		slide:  dependentTooltip,
		change: populateEmployeeRange
	});
	
});
</script>