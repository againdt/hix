# Washingtonfull

Last updated by: Jesse Wu
Last updated on: June 2019

Jesse's Note: I am not the original creator of this project. I picked it up after the creator had already left. Many of the instructions in this README are discovered by me through trial and error. If you discover a better way of doing things or if you notice any mistakes, please feel free to change the contents of this README.

## The Washington frontend project

The frontend of the Washington's project exists independently as an angular project housed in the folder "dst", while its backend, data, and some of its resources exist in other folders or repositories. It was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.0-rc.0.

## How to test Washington on a development server

Install Angular CLI here: https://angular.io/cli

In your command line terminal, navigate to folder 'dst' (located at iex/ghix-web/src/main/webapp/resources/angular/dst).
Run the command 'ng serve', then go to `http://localhost:4200/` on your browser to view the app. The app will automatically reload if you change any of the angular source files.

Note: Because you are running the angular project independently with 'ng serve', anything used by your project that lives outside of dst will not be available. For example, this includes the app's logo image and the data that auto-populates the app's search boxes.

## How to deploy Washington for production

Read this first: The development server and production server uses different files in your project. When deployed, a 'index.jsp' file outside of this project is served on the server. This is NOT the 'index.html' within this project which is used when testing with 'ng serve'. 'index.jsp' is configured to read in the angular compiled stuff in dist. Hence, it is useful be aware that some things such as stylesheet includes may be coming from that file. As of writing, this file is located at iex/ghix-web/src/main/webapp/WEB-INF/views/dst/index.jsp.

To deploy Washington onto a dev/live server, use `npm run prod` to build and compile the project for production. A folder 'dist' will be automagically generated from the stuff you have in 'src'. The files in both src and dist should be committed to source if anything changed. For example, 'main.bundle.js' will be changed if you modified any angular components; 'styles.bundle.css' will be changed if you modified styles.scss in src. The deployed app depends on the committed contents of dist and not src, but it is good practice to commit src in the same commit for bookkeeping purposes.

Once everything is committed and pushed to your branch, you can go to Jenkins and deploy your feature branch, or merge your branch to master then deploy it, just like for other projects.


## Package Manager
This project is managed by yarn package manager

## Project Structure

Please refer to app.module.ts. DO NOT FORMAT THE FILE. 

## i18n
Add {lang}.json under assets/i18n folder. The language will be automagically available based on the url parameter lang={lang}.

To understand the magic, https://github.com/ngx-translate/core

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

Jesse's Note: I have tried many times running a local build for Washington (for 'localhost:8080') but it doesn't seem to work. If anyone gets this to work please update this README.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.






