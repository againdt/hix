/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import com.getinsured.hix.platform.util.exception.GIException;

/**
 * @author negi_s
 *
 */
public interface Enrollment1095AInboundBatchService {
	/**
	 * Move and Process CMS and IRS response files
	 * @author negi_s
	 * @param jobExecutionId 
	 * @since 24/09/2015
	 * @throws GIException
	 */
	void processInboundResponse(Long jobExecutionId) throws GIException;
}
