package com.getinsured.hix.referral.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.referral.ui.dto.ReferralVerification;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.RoleAccessControl;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.referral.exception.ReferralServiceException;
import com.getinsured.hix.referral.service.ReferralActivationService;
import com.getinsured.hix.referral.util.WebReferralConstants;
import com.getinsured.hix.referral.util.WebReferralUtil;
import com.getinsured.hix.referral.validator.DynamicSecurityQuestionsDTO;
import com.getinsured.hix.referral.validator.DynamicSecurityQuestionsValidator;
import com.getinsured.hix.referral.validator.FileNameValidator;
import com.getinsured.hix.referral.validator.SecurityQuestionsValidator;
import com.getinsured.iex.referral.ReferralLinkingOverride;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

import ch.compile.recaptcha.ReCaptchaVerify;
import ch.compile.recaptcha.model.SiteVerifyResponse;

/**
 * @author chopra_s
 *
 */
@Controller
@RequestMapping("/referral")
public class ReferralSsapVerificationController {
	private static final String INDIVIDUAL = "INDIVIDUAL";
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralSsapVerificationController.class);
	private static final String REDIRECT_ACCOUNT_USER_LOGINFAILED = "redirect:/account/user/loginfailed";
	private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
	private static final String APPLICATION_LINKED = "APPLICATION_LINKED";

	@Autowired
	private UserService userService;

	@Autowired
	private MessageSource messageSource;

	@Autowired
	@Qualifier("referralActivationService")
	private ReferralActivationService referralActivationService;

	@Autowired
	@Qualifier("securityQuestionsValidator")
	private SecurityQuestionsValidator securityQuestionsValidator;

	@Autowired
	private ReCaptchaVerify reCaptchaV2;
	
	@Autowired
	private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;

	@Autowired
	@Qualifier("fileNameValidator")
	private FileNameValidator fileNameValidator;

	@Autowired
	private RoleAccessControl roleAccessControl;

	@Autowired
	private AccountActivationService accountActivationService;

	@Autowired
	private AppEventService appEventService;
	@Autowired
	private LookupService lookupService;

	@Autowired
	@Qualifier("dynamicSecurityQuestionsValidator")
	private DynamicSecurityQuestionsValidator dynamicSecurityQuestionsValidator;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	
	private boolean isAllowed(Model model, HttpServletRequest request) {
		AccountUser user = null;
		HashMap<String, Object> errorMsg = new HashMap<String, Object>();

		try {
			user = userService.getLoggedInUser();
			if (user == null) {
				errorMsg.put("message", "User not authorized.");
				model.addAttribute("authfailed", errorMsg.get("message"));
				request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
				return false;
			}
		} catch (InvalidUserException ex) {
			LOGGER.error("User not logged in");
			errorMsg.put("message", "User not logged in.");
			model.addAttribute("authfailed", errorMsg.get("message"));
			request.getSession().setAttribute("SPRING_SECURITY_LAST_EXCEPTION", errorMsg);
			return false;
		}
		return true;
	}

	@GiAudit(transactionName = "SoftLinking", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/activation/binduser", "/activation/binduser/" }, method = RequestMethod.GET)
	public String referralActivationSsapAndUser(Model model, HttpServletRequest request) {

		try {
			LOGGER.debug("Referral Activation of User and SsapApplication Starts ");
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final HttpSession httpSession = request.getSession();

			if (httpSession.getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL) == null || !httpSession.getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL).equals("Y")) {
				LOGGER.warn("Login not from State Referral ");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Login not from State Referral"));

				return "redirect:/indportal";
			}

			if (httpSession.getAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID) == null) {
				LOGGER.warn("No ssap application in session ");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "No ssap application in session "));
				return "redirect:/indportal";
			}
			String ssapReferralAppId = (String) httpSession.getAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID);
			httpSession.removeAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID);
			httpSession.removeAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL);

			String referralLinkingFlow = (String) request.getSession().getAttribute(ReferralConstants.REFERRAL_LINKING_FLOW);
			String redirectUrl = "redirect:/indportal";
			if ("true".equalsIgnoreCase(referralLinkingFlow)) {
				boolean isMigrationFlow =true;
				String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

				if("NV".equals(stateCode))
				{
					SsapApplication application = ssapApplicationRepository.findOne(Long.valueOf(ssapReferralAppId));
					if(application != null && application.getCmrHouseoldId() != null)
					{
						isMigrationFlow = false;
						if(ApplicationStatus.UNCLAIMED.getApplicationStatusCode().equals(application.getApplicationStatus()))
						{
							application.setApplicationStatus(ApplicationStatus.OPEN.getApplicationStatusCode());
							ssapApplicationRepository.save(application);
						}
					}
				}
				if(isMigrationFlow)
				{
				String referralId = (ghixJasyptEncrytorUtil.encryptStringByJasypt((String) request.getSession().getAttribute(ReferralConstants.REFERRAL_ID)));
				StringBuilder redirectUrlNew = new StringBuilder("redirect:/referral/dynamicsecurityquestions/verify/").append(referralId);
				redirectUrl = redirectUrlNew.toString();
				}
				request.getSession().removeAttribute(ReferralConstants.REFERRAL_LINKING_FLOW);
				request.getSession().removeAttribute(ReferralConstants.REFERRAL_ID);
				request.getSession().removeAttribute(ReferralConstants.SSAP_ID);
			}
			return redirectUrl;
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ referralActivationSsapAndUser : bindUserCmrToReferralActivation " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.USER_CMR_TO_REFERRAL_BINDERROR.getId(), e, null, Severity.HIGH);
		}

	}

	@GiAudit(transactionName = "Security Questions", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/security/verify/{referralActivationId}" }, method = RequestMethod.GET)
	public String securityVerifyMain(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) throws InvalidUserException {
		try {
			HttpSession session = request.getSession();

			session.setAttribute("ActiveAppPresent", "N");
			session.setAttribute("enrolledAppSess", "N");

			final int referralId = toDecrypt(referralActivationId);
			model.addAttribute(ReferralConstants.REFERRAL_ID, referralId);

			LOGGER.debug("Referral Security Verify Main Starts for Id - " + referralId);
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation));

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			if (!referralActivation.isSecurityPhase()) {
				LOGGER.warn("Data not proper. Workflow Status not valid");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data not proper. Workflow Status not valid"));
				return "redirect:/referral/activation/status/" + referralActivationId;
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

			model.addAttribute("referralVerification", referralActivationService.populateReferralVerification(referralActivation));

			/* Check Non-Fin applications */
			long appCount = referralActivationService.getActiveApplicationCountForCMR(referralActivation.getCmrHouseholdId(), referralActivation.getSsapApplicationId());

			if (appCount > 0) {
				session.setAttribute("ActiveAppPresent", "Y");
				return "referral/verify";
			}

			/* Check Fin applications */
			/*
			 * String isEnrolledStatus = referralActivationService.checkforEnrolledApplication(referralActivation.getCmrHouseholdId(), referralActivation.getSsapApplicationId()); if ("Y".equalsIgnoreCase(isEnrolledStatus)) { session.setAttribute("enrolledAppSess", "Y"); return "referral/verify"; }
			 */

			LOGGER.debug("Referral Security Verify Main  Ends ");

			return "referral/verify";
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ securityVerifyMain : fetchReferralActivationObject " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.SECURITY_QUESTION_RETRIEVAL_ERROR.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Activation Status", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/activation/status/{referralActivationId}" }, method = RequestMethod.GET)
	public String activationStatus(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) throws InvalidUserException {
		try {
			final int referralId = toDecrypt(referralActivationId);
			LOGGER.debug("Activation Status Fetch Starts for Id - " + referralId);
			if (!this.isAllowed(model, request)) {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				LOGGER.warn("User not authorized");
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation));

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			model.addAttribute("referralActivation", referralActivation);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

			LOGGER.debug("Activation Status Fetch Starts Ends ");

			return "referral/invalidphase";
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ activationStatus : fetchReferralActivationObject " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.INVALID_APPLICATION_STATUS.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Security Questions", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/security/verify/{referralActivationId}" }, method = RequestMethod.POST)
	public String verifySubmit(@ModelAttribute("referralVerification") ReferralVerification referralVerification, Model model, HttpServletRequest request, RedirectAttributes redirectAttributes, BindingResult bindingResult) {
		try {
			LOGGER.debug("Referral Verify Submission Starts ");
			final HttpSession httpSession = request.getSession();

			if (httpSession.getAttribute("isEnrolledStatus") != null) {
				httpSession.removeAttribute("isEnrolledStatus");
			}

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivationId", referralVerification.getReferralActivation().getId()));
			securityQuestionsValidator.validate(referralVerification, bindingResult);

			if (bindingResult.hasErrors()) {
				LOGGER.debug("Referral Verify Submission has errors " + bindingResult.getFieldErrors());
				httpSession.setAttribute("fieldErrors", bindingResult.getFieldErrors());
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral Verify Submission has errors " + bindingResult.getFieldErrors()));
				return "redirect:/referral/security/verify/" + toEncrypt(Integer.valueOf((String.valueOf(referralVerification.getReferralActivation().getId()))));
			}

			if (httpSession.getAttribute(WebReferralConstants.REFERRAL_FROM_CSR) != null && httpSession.getAttribute(WebReferralConstants.REFERRAL_FROM_CSR).equals("Y")) {
				referralVerification.setSecurityCheck(false);
			}

			final int status = referralActivationService.validateSecurityQuestions(referralVerification);

			final long referralActivationId = referralVerification.getReferralActivation().getId();
			if (status == WebReferralConstants.FAILURE) {
				LOGGER.warn("Referral Verify failed Validation ");
				redirectAttributes.addFlashAttribute("failure", "1");
				httpSession.setAttribute("fieldErrors", null);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral Verify failed Validation "));
				return "redirect:/referral/security/verify/" + toEncrypt((int) referralActivationId);
			}

			if (status == WebReferralConstants.FAILURE_AGAIN) {
				LOGGER.warn("Referral Verify failed Validation 2 Times ");
				redirectAttributes.addFlashAttribute("failure", "1");
				referralActivationService.sendAdditionalVerificationRequiredNotification(referralActivationId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral Verify failed Validation 2 Times "));
				return "redirect:/referral/document/upload/" + toEncrypt((int) referralActivationId);
			}

			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			LOGGER.debug("Referral Verify Submission ends Success ");
			return "redirect:/referral/ridp/processcheck/" + toEncrypt((int) referralActivationId);
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ verifySubmit : validateSecurityQuestions " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.SECURITY_QUESTION_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Document Upload", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/document/upload/{referralActivationId}" }, method = RequestMethod.GET)
	public String documentUploadMain(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) {
		try {
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final int referralId = toDecrypt(referralActivationId);
			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation));

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			if (!referralActivation.isSecurityFailed() && !referralActivation.isCsrVerificationPending()) {
				LOGGER.warn("Data not proper. Workflow Status not valid");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data not proper. Workflow Status not valid"));
				return "redirect:/referral/activation/status/" + referralActivationId;
			}

			final Locale locale = LocaleContextHolder.getLocale();
			final List<String> documentTypes = WebReferralUtil.convertStringTokenToList(readValue("prop.referral.activation.document.type", locale), ",");
			final List<ConsumerDocument> documents = referralActivationService.fetchConsumerDocuments(new Long(toDecrypt(referralActivationId)));

			model.addAttribute("documentUploaded", referralActivation.isCsrVerificationPending());
			model.addAttribute("documentTypes", documentTypes);
			model.addAttribute("documents", documents);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return "referral/document/upload";
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ documentUploadMain : fetchConsumerDocuments " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.DOCUMENT_FETCH_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Document Upload", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/document/upload/{referralActivationId}" }, method = RequestMethod.POST)
	public String documentSubmit(@PathVariable("referralActivationId") String referralActivationId, @RequestParam(value = "documentFile", required = false) CommonsMultipartFile documentFile, MultipartHttpServletRequest mrequest,
	        RedirectAttributes redirectAttributes) throws GIException {
		try {
			LOGGER.debug("Document Submit Starts");

			final HttpSession httpSession = mrequest.getSession();

			if (!fileNameValidator.validate(documentFile.getOriginalFilename())) {
				httpSession.setAttribute("fieldErrors", "msg.referral.document.filetype.error");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "File type not proper"));
				return "redirect:/referral/document/upload/" + referralActivationId;
			}

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivationId", referralActivationId));
			final int status = referralActivationService.uploadDocumentForVerification(documentFile, mrequest.getParameter("documentType"), toDecrypt(referralActivationId));
			if (status != WebReferralConstants.SUCCESS) {
				redirectAttributes.addFlashAttribute("failure", "Y");
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			LOGGER.debug("Document Submit Ends");
			return "redirect:/referral/document/upload/" + referralActivationId;
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ documentSubmit : uploadDocumentForVerification " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.DOCUMENT_UPLOAD_EXCEPTION.getId(), e, null, Severity.HIGH);
		}

	}

	@RequestMapping(value = { "/coverageyear/{referralActivationId}" }, method = RequestMethod.GET)
	public String coverageYearMain(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) {
		try {
			LOGGER.debug("Inside coverageYearMain");
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}
			final int referralId = toDecrypt(referralActivationId);
			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				return "redirect:/indportal";
			}

			if (!referralActivation.isCoverageYearPending()) {
				LOGGER.warn("Data not proper. Workflow Status not valid");
				return "redirect:/referral/activation/status/" + referralActivationId;
			}

			final List<Integer> coverageYears = referralActivationService.fetchCoverageYear();
			model.addAttribute("coverageYears", coverageYears);

			return "referral/coverageyear";

		} catch (Exception e) {
			LOGGER.error("Error_Msg @ coverageYearMain  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.COVERAGE_YEAR_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@RequestMapping(value = { "/coverageyear/{referralActivationId}" }, method = RequestMethod.POST)
	public String coverageYearSubmit(@PathVariable("referralActivationId") String referralActivationId, @RequestParam(value = "coverageYear") Integer coverageYear, Model model, HttpServletRequest request) throws GIException {
		try {
			LOGGER.debug("Coverage Year Submit Starts");
			final int referralId = toDecrypt(referralActivationId);

			final HttpSession httpSession = request.getSession();

			if (!(validateCoverageYear(coverageYear))) {
				httpSession.setAttribute("fieldErrors", "msg.referral.coverageyear.error");
				return "redirect:/referral/coverageyear/" + toEncrypt(referralId);
			}

			final int status = referralActivationService.persistCoverageYear(referralId, coverageYear);

			if (status == WebReferralConstants.FAILURE) {
				LOGGER.warn("Coverage Year Submit Failure ");
				return "redirect:/referral/coverageyear/" + toEncrypt(referralId);
			}

			LOGGER.debug("Coverage Year Submit Ends");

			return "redirect:/referral/ridp/processcheck/" + toEncrypt(referralId);
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ coverageYearSubmit  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.COVERAGE_YEAR_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Ridp Check", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/ridp/processcheck/{referralActivationId}" }, method = RequestMethod.GET)
	public String processRidpCheck(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) {
		try {
			LOGGER.debug("Inside processRidpCheck");
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			// final AccountUser user = userService.getLoggedInUser();

			final int referralId = toDecrypt(referralActivationId);

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivationId", referralId));

			// final int status = referralActivationService.processRidpCheck(referralId, user.getId());

			return "redirect:/referral/activation/status/" + referralActivationId;
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ processRidpCheck  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.RIDP_CHECK_EXCEPTION.getId(), e, null, Severity.HIGH);
		}

	}

	@GiAudit(transactionName = "Final Referral Linking", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/ridp/link/{referralActivationId}" }, method = RequestMethod.GET)
	public String processFinalLinking(@PathVariable("referralActivationId") String referralActivationId, @RequestParam(value = "isOverride", required = false) boolean isOverride,Model model, HttpServletRequest request) {
		try {
			LOGGER.debug("Inside processRidpCheck");

			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}
			HttpSession session = request.getSession();

			session.removeAttribute(ReferralConstants.ACTIVE_APP_PRESENT);
			session.removeAttribute(ReferralConstants.ENROLLED_APP_PRESENT);

			final AccountUser user = userService.getLoggedInUser();

			final int referralId = toDecrypt(referralActivationId);

			final int householdId = ((Integer) request.getSession().getAttribute("householdId"));
			GiAuditParameterUtil.add(new GiAuditParameter("referralActivationId", referralId), new GiAuditParameter(ReferralConstants.HOUSEHOLD_ID, householdId));

			Map<String, String> result = referralActivationService.processRidpLinkCheck(referralId, user.getId(), householdId);
			int status = 0;
			if (result != null && result.get(ReferralConstants.FLOW) != null) {
				status = Integer.valueOf(result.get(ReferralConstants.FLOW));
			}

			GiAuditParameterUtil.add(new GiAuditParameter("status", status));
			
			if( null == result.get("REASON")) {
				//HIX-108543 - REASON is a key for SSN-Conflict case. 
				//ReferralWorkFlowServiceImpl#processRidpAndLinking
				request.getSession().removeAttribute(ReferralConstants.SSAP_MAP);
			}
			
			return navigateTo(status, referralActivationId,isOverride, model, result, householdId);
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ processRidpCheck  " + e.getMessage());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error during ridp and linking"));
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.RIDP_CHECK_EXCEPTION.getId(), e, null, Severity.HIGH);
		}

	}

	private String navigateTo(int status, String referralActivationId, boolean isOverride,Model model, Map<String, String> result, int householdId) {
		final int referralId = toDecrypt(referralActivationId);
		if (status == ReferralConstants.ERROR || status == ReferralConstants.STATUS_NOT_VALID) {
			LOGGER.warn("Data not proper. Workflow Status not valid");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occured in RIDP Process"));
			if( null != result.get("REASON")) {
				model.addAttribute("ssnORdObConflict", "Y");
				return "referral/errorpage";
			}
			
			return "redirect:/referral/activation/status/" + referralActivationId;
		}
		GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
		if (status == ReferralConstants.RIDP_MANUAL) {
			LOGGER.debug("Run the Manual Ridp Verification Process for Id - " + referralActivationId);
			model.addAttribute(ReferralConstants.REFERRAL_ID, referralId);
			return "referral/ridp/start";
		}

		if (status == ReferralConstants.LCE_FLOW) {
			LOGGER.debug("Process this referral as an LCE Id - " + referralActivationId);
			return "redirect:/referral/lce/processcheck/" + referralActivationId;
		}

		createApplicationLinkedLog(householdId, result);

		referralActivationService.triggerDesignation(householdId);

		LOGGER.debug("processRidpCheck ends");
		return "redirect:/referral/success/" + referralActivationId+"/"+isOverride;
	}

	private void createApplicationLinkedLog(int householdId, Map<String, String> result) {

		Map<String, String> mapEventParam = new HashMap<>();
		mapEventParam.put("ApplicationId", result.get(ReferralConstants.APPLICATION_ID));
		mapEventParam.put("MedicaidId", result.get(ReferralConstants.MEDICAID_ID));
		mapEventParam.put("HouseholdId", String.valueOf(householdId));

		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APPEVENT_APPLICATION, APPLICATION_LINKED);
		EventInfoDto eventInfoDto = new EventInfoDto();
		eventInfoDto.setEventLookupValue(lookupValue);
		eventInfoDto.setModuleId(householdId);
		eventInfoDto.setModuleName(INDIVIDUAL);
		appEventService.record(eventInfoDto, mapEventParam);

	}

	@GiAudit(transactionName = "LCE Check", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/lce/processcheck/{referralActivationId}" }, method = RequestMethod.GET)
	public String processLceCheck(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) {
		try {
			final int referralId = toDecrypt(referralActivationId);
			LOGGER.debug("Inside processLceCheck");
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}
			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);
			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation));
			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			if (!referralActivation.isLcePending()) {
				LOGGER.warn("Data not proper. Workflow Status not valid");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data not proper. Workflow Status not valid"));
				return "redirect:/referral/activation/status/" + referralActivationId;
			}
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));
			return "referral/lce/start";
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ processLceCheck  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.LCE_CHECK_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Linking Success", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/success/{referralActivationId}/{isOverride}" }, method = RequestMethod.GET)
	public String summary(@PathVariable("referralActivationId") String referralActivationId,@PathVariable("isOverride") boolean isOverride, Model model, HttpServletRequest request) {
		try {
			LOGGER.debug("Inside summary");
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			GiAuditParameterUtil.add(new GiAuditParameter("referralActivationId", toDecrypt(referralActivationId)));
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

			final String switchToNonDefaultURL = "/account/user/switchNonDefRole/individual";
			model.addAttribute("switchToNonDefaultURL", switchToNonDefaultURL);
			model.addAttribute("currentRoleName", roleAccessControl.getCurrentUser().getDefRole().getName());
			model.addAttribute("householdId", String.valueOf(request.getSession().getAttribute("householdId")));
			model.addAttribute("firstName", request.getSession().getAttribute("firstName"));
			model.addAttribute("lastName", request.getSession().getAttribute("lastName"));
			model.addAttribute("override", isOverride?"Y":"N");
			request.getSession().removeAttribute("householdId");
			request.getSession().removeAttribute("firstName");
			request.getSession().removeAttribute("lastName");
			clearDataFromSessionForAccessCode(request);
			LOGGER.debug("summary ends");
			return "referral/success";
		} catch (Exception e) {
			LOGGER.error("Error while displaying success page " + e.getMessage());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while displaying success page"));
			throw new GIRuntimeException("Error while displaying success page", e);
		}
	}

	@GiAudit(transactionName = "Portal Pending Referral", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/portalflow/{referralActivationId}" }, method = RequestMethod.GET)
	public String navigatePendingReferral(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) throws InvalidUserException {

		try {
			final int referralId = toDecrypt(referralActivationId);
			LOGGER.debug("Navigate Pending Referral Id - " + referralId);
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);
			GiAuditParameterUtil.add(new GiAuditParameter("referralActivation", referralActivation));

			final int cmrHouseholdId = ((AccountUser) request.getSession().getAttribute("sUser")).getActiveModuleId();

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			if (referralActivation.isActivationComplete() || referralActivation.isCancelled()) {
				LOGGER.warn("Data not proper. Workflow Status not valid");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Data not proper. Workflow Status not valid"));
				return "redirect:/referral/activation/status/" + referralActivationId;
			}

			if (referralActivation.getCmrHouseholdId() != cmrHouseholdId) {
				LOGGER.warn("Cmr household data not proper for Id - " + referralActivation.getId());
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Cmr household data not proper for Id - " + referralActivation.getId()));
				return "redirect:/indportal";
			}

			final String encryptedRefId = toEncrypt(referralId);
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

			final String reDirectUrl = WebReferralUtil.navigateTo(referralActivation, encryptedRefId);

			LOGGER.debug("Navigate Pending Referral Ends ");

			return reDirectUrl;
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ navigatePendingReferral  " + e.getMessage());
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.PENDING_REFERRAL_EXCEPTION.getId(), e, null, Severity.HIGH);
		}
	}

	@GiAudit(transactionName = "Verify referral access code", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/verifyaccesscode" }, method = RequestMethod.POST)
	public String validateAccessCode(Model model, HttpServletRequest request, 
			@RequestParam(value = "g-recaptcha-response", required = false) String gRecaptchaResponse) {
		boolean isValidCaptcha = false;

		String response = null;
		String accessCode = null;
		String errorMsg = null;
		String redirectUrl = "referral/errorpage";
		String status = null;

		clearDataFromSessionForAccessCode(request);

		Map<Integer, String> ssapMap = null;


		try {
			if (null != userService.getLoggedInUser()) {

				Role role = userService.getLoggedInUser().getDefRole();
				if (role != null && role.getName() != null) {
					request.getSession().setAttribute(ReferralConstants.ROLE, role.getName());
				}
				isValidCaptcha = true;
			} else {
				try {
					SiteVerifyResponse siteVerifyResponse = reCaptchaV2.verify(gRecaptchaResponse);
					if ( siteVerifyResponse.isSuccess() ) {
						isValidCaptcha = true;
					} else {
						isValidCaptcha = false;
					}
				} catch (IOException e) {
					isValidCaptcha = true;//setting to true incase of captcha issue.
					LOGGER.error("Error happened while validating recaptcha",e);
				}
				
			}

			isCsrDefaultRole(request);
			
		
			if(userService.getLoggedInUser()!=null && StringUtils.equalsIgnoreCase(userService.getLoggedInUser().getActiveModuleName(), "INDIVIDUAL")){
				request.getSession().setAttribute(ReferralConstants.MODULE_NAME, "INDIVIDUAL");
			}

			model.addAttribute("role", request.getSession().getAttribute(ReferralConstants.ROLE));
			
			if (!isValidCaptcha) {
				errorMsg = "Incorrect captcha. Please try again.";
				model.addAttribute("invalidCaptcha", "true");
				status = "INVALID_CAPTCHA";
			} else {
				accessCode = request.getParameter(ReferralConstants.ACCESS_CODE);

				if (StringUtils.isNotEmpty(accessCode)) {
					ssapMap = referralActivationService.getSsapApplicantfromHome(accessCode, isCsrDefaultRole(request));
					response = ssapMap.get(ReferralConstants.REFERRAL_STATUS_KEY);

					if (StringUtils.isNotEmpty(response)) {

						GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("Access Code", accessCode), new GiAuditParameter("status", response));

						if (ReferralConstants.VALID.equalsIgnoreCase(response)) {
							if ("BIRTH_DATE_MISMATCH".equalsIgnoreCase( ssapMap.get("9001"))   && (userService.getLoggedInUser() == null || !StringUtils.equalsIgnoreCase(userService.getLoggedInUser().getActiveModuleName(), "INDIVIDUAL")) ) {
								
								Map<String, String> mapEventParam = new HashMap<>();
								mapEventParam.put("DoB in Account Transfer",ssapMap.get("3") );// This key has DoB
								logIndividualAppEvent("HH_LINKING","DoB_MISMATCH",  mapEventParam, Integer.parseInt(ssapMap.get("9002"))   );
								
								model.addAttribute("ssnORdObConflict", "Y");
								status = "BIRTH_DATE_MISMATCH";
								GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, status));
							}else {
								processReferralData(request, ssapMap);
								redirectUrl = "redirect:/referral/dynamicsecurityquestions/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(accessCode);
							}
						} else if (ReferralConstants.INVALID.equalsIgnoreCase(response)) {
							errorMsg = messageSource.getMessage("msg.referral.accessCode.invalidAccessCode", null, LocaleContextHolder.getLocale());
							status = "ACCESSCODE_INVALID";
							model.addAttribute("invalidAccessCode", "true");
						} else if (ReferralConstants.MULTIPLE_HOUSEHOLDS.equalsIgnoreCase(response)) {
							errorMsg = messageSource.getMessage("msg.referral.accessCode.multipleHouseholds", null, LocaleContextHolder.getLocale());
							status = "MULTIPLE_HOUSEHOLD";
							model.addAttribute("invalidAccessCode", "true");
						} else if (ReferralConstants.REFERRAL_LOCKED.equalsIgnoreCase(response)) {
							errorMsg = messageSource.getMessage("msg.referral.accessCode.lockMessage", null, LocaleContextHolder.getLocale());
							status = "ACCESSCODE_LOCKED";
							model.addAttribute("locked", "true");
						}
					} else {
						errorMsg = messageSource.getMessage("msg.referral.accessCode.internalServerError", null, LocaleContextHolder.getLocale());
						throw new GIRuntimeException(errorMsg);
					}
				} else {
					errorMsg = messageSource.getMessage("msg.referral.accessCode.emptyAccessCode", null, LocaleContextHolder.getLocale());
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR:  WHILE VALIDATING CODE: ", ex);
			errorMsg = "Internal server error. We could not process the access code. Please call support for further help.";
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while validating AccessCode" + accessCode));
			throw new GIRuntimeException(errorMsg);
		}

		if (StringUtils.isNotEmpty(errorMsg)) {
			model.addAttribute("errorMsg", errorMsg);
			model.addAttribute("verifyAccess", "true");
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, status));
		}

		return redirectUrl;
	}

	@SuppressWarnings("unchecked")
	@GiAudit(transactionName = "Populate referral security questions", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_READ)
	@RequestMapping(value = { "/dynamicsecurityquestions/{accessCode}" }, method = RequestMethod.GET)
	public String getDynamicSecurityQuestions(@PathVariable(ReferralConstants.ACCESS_CODE) String encAccessCode, Model model, HttpServletRequest request) {
		String redirectUrl = "referral/dynamicsecurityquestions";
		String accessCode = null;
		String errorMsg = null;
		String response = null;
		String status = null;

		List<Integer> randomList = null;
		Map<Integer, String> ssapMap = null;

		clearDataFromSessionForAccessCode(request);

		try {

			if (null != userService.getLoggedInUser()) {
				Role role = userService.getLoggedInUser().getDefRole();
				if (role != null && role.getName() != null) {
					request.getSession().setAttribute(ReferralConstants.ROLE, role.getName());
				}
			}

			isCsrDefaultRole(request);

			//In real time it will not execute this if loop. Added for HIX-77048
			if(userService.getLoggedInUser()!=null && StringUtils.equalsIgnoreCase(userService.getLoggedInUser().getActiveModuleName(), "INDIVIDUAL")){
				request.getSession().setAttribute(ReferralConstants.MODULE_NAME, "INDIVIDUAL");
			}
			
			model.addAttribute("role", request.getSession().getAttribute(ReferralConstants.ROLE));

			accessCode = ghixJasyptEncrytorUtil.decryptStringByJasypt(encAccessCode);
			ssapMap = (Map<Integer, String>) request.getSession().getAttribute(ReferralConstants.SSAP_MAP);

			if (null == ssapMap || ssapMap.isEmpty() || ssapMap.size() < ReferralConstants.REFERRAL_ACCESS_CODE_MAP_SIZE) {
				ssapMap = referralActivationService.getSsapApplicant(accessCode, isCsrDefaultRole(request));
				response = ssapMap.get(ReferralConstants.REFERRAL_STATUS_KEY);

				if (StringUtils.isNotEmpty(response)) {

					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("Access Code", accessCode), new GiAuditParameter("status", response));
					if (ReferralConstants.VALID.equalsIgnoreCase(response)) {
						 if ("BIRTH_DATE_MISMATCH".equalsIgnoreCase(ssapMap.get("9001"))    && (userService.getLoggedInUser() == null || !StringUtils.equalsIgnoreCase(userService.getLoggedInUser().getActiveModuleName(), "INDIVIDUAL"))   ) {
							 	
							 	Map<String, String> mapEventParam = new HashMap<>();
								mapEventParam.put("DoB in Account Transfer",ssapMap.get("3") );// This key has DoB
								logIndividualAppEvent("HH_LINKING","DoB_MISMATCH",  mapEventParam, Integer.parseInt( ssapMap.get("9002")));
								
								model.addAttribute("ssnORdObConflict", "Y");
								status = "BIRTH_DATE_MISMATCH";
								errorMsg = "BIRTH_DATE_MISMATCH" ;
						 }else {
							 processReferralData(request, ssapMap);
						 }
						
					} else if (ReferralConstants.INVALID.equalsIgnoreCase(response)) {
						errorMsg = messageSource.getMessage("msg.referral.accessCode.invalidAccessCode", null, LocaleContextHolder.getLocale());
						model.addAttribute("invalidAccessCode", "true");
						status = "ACCESSCODE_INVALID";
					} else if (ReferralConstants.MULTIPLE_HOUSEHOLDS.equalsIgnoreCase(response)) {
						errorMsg = messageSource.getMessage("msg.referral.accessCode.multipleHouseholds", null, LocaleContextHolder.getLocale());
						model.addAttribute("invalidAccessCode", "true");
						status = "MULTIPLE_HOUSEHOLD";
					} else if (ReferralConstants.REFERRAL_LOCKED.equalsIgnoreCase(response)) {
						errorMsg = messageSource.getMessage("msg.referral.accessCode.lockMessage", null, LocaleContextHolder.getLocale());
						model.addAttribute("locked", "true");
						status = "ACCESSCODE_LOCKED";
					}
				} else {
					errorMsg = messageSource.getMessage("msg.referral.accessCode.internalServerError", null, LocaleContextHolder.getLocale());
					throw new GIRuntimeException(errorMsg);
				}
			}

			if (StringUtils.isEmpty(errorMsg)) {
				String householdId = ssapMap.get(ReferralConstants.HOUSEHOLD_ID_KEY);
				if (StringUtils.isNotEmpty(householdId)) {
					request.getSession().setAttribute(ReferralConstants.HOUSEHOLD_ID, householdId);
				}
				GiAuditParameterUtil.add(new GiAuditParameter("ReferralID", ssapMap.get(ReferralConstants.REFERRAL_ID_KEY)));
				Set<Integer> randomSet = ssapMap.keySet();
				randomSet.remove(ReferralConstants.REFERRAL_STATUS_KEY);
				randomSet.remove(ReferralConstants.SSAP_APPLICATION_KEY);
				randomSet.remove(ReferralConstants.HOUSEHOLD_ID_KEY);
				randomSet.remove(ReferralConstants.REFERRAL_ID_KEY);
				if (null != randomSet && !randomSet.isEmpty()) {
					randomList = new ArrayList<Integer>();
					randomList.addAll(randomSet);
					model.addAttribute("randomList", randomList);

				} else {
					errorMsg = messageSource.getMessage("msg.referral.securityQuestions.internalServerError", null, LocaleContextHolder.getLocale());
					throw new GIRuntimeException(errorMsg);
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING RANDOM LIST: ", ex);
			errorMsg = messageSource.getMessage("msg.referral.securityQuestions.internalServerError", null, LocaleContextHolder.getLocale());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error while generating questions" + errorMsg));
			throw new GIRuntimeException(errorMsg);
		}

		if (StringUtils.isNotEmpty(errorMsg)) {
			redirectUrl = "referral/errorpage";
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, status));
			if(!"BIRTH_DATE_MISMATCH".equalsIgnoreCase(errorMsg)) {
				model.addAttribute("errorMsg", errorMsg);
				model.addAttribute("verifyAccess", "true");
			}
		}

		request.getSession().setAttribute(ReferralConstants.ENC_REF_ACCESS_CODE, encAccessCode);
		return redirectUrl;
	}

	private void clearDataFromSessionForAccessCode(HttpServletRequest request) {
		HttpSession session = request.getSession();

		session.removeAttribute(ReferralConstants.SSAP_MAP);
		session.removeAttribute(ReferralConstants.SSAP_ID);
		session.removeAttribute(ReferralConstants.REFERRAL_ID);
		session.removeAttribute(ReferralConstants.HOUSEHOLD_ID);
		session.removeAttribute(ReferralConstants.ENC_REF_ACCESS_CODE);
		session.removeAttribute(ReferralConstants.REFERRAL_LINKING_FLOW);
		session.removeAttribute(ReferralConstants.ACCOUNT_ACTIVATION_FLOW);
		session.removeAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING);
		session.removeAttribute(ReferralConstants.FIRST_NAME);
		session.removeAttribute(ReferralConstants.LAST_NAME);
		session.removeAttribute(ReferralConstants.EMAIL_ID);
		session.removeAttribute(ReferralConstants.PHONE);
		session.removeAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL);
		session.removeAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID);
		session.removeAttribute(ReferralConstants.COVERAGE_YEAR);
		session.removeAttribute(ReferralConstants.ROLE);
		session.removeAttribute(ReferralConstants.MODULE_NAME);

	}
	

	/**
	 * @param request
	 * 
	 */

	private String isCsrDefaultRole(HttpServletRequest request) {
		String isCsr = "false";
		AccountUser accountUser;
		accountUser = roleAccessControl.getCurrentUser();
		if (null != accountUser) {
			Set<Role> roles = userService.getAllRolesOfUser(accountUser);
			for (Role role : roles) {
				if (role.getPrivileged() == 1) {
					isCsr = "true";
					request.getSession().setAttribute(ReferralConstants.ROLE, ReferralConstants.CSR);
					break;
				}
			}

		}

		return isCsr;
	}

	@SuppressWarnings("unchecked")
	@GiAudit(transactionName = "Validate referral security questions", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/dynamicsecurityquestions" }, method = RequestMethod.POST)
	public String validateDynamicSecurityQuestions(@ModelAttribute("dynamicSecurityQuestions") DynamicSecurityQuestionsDTO dynamicSecurityQuestionsDTO, Model model, HttpServletRequest request,
			@RequestParam(value = "g-recaptcha-response", required = false) String gRecaptchaResponse, BindingResult bindingResult, RedirectAttributes redirectAttributes) {
		boolean isValidCaptcha = false;
		String remoteAddress = null;
		String redirectUrl = "referral/errorpage";
		String errorMsg = null;
		String status = null;

		Map<String, String> ssapMap = null;
		boolean isSecurityAuthSuccessful = true;

		AccountUser accountUser = null;
		AccountActivation accountActivation = null;
		ActivationJson jsonObject = null;

		
		try {
			if (null != userService.getLoggedInUser()) {
				Role role = userService.getLoggedInUser().getDefRole();
				if (role != null && role.getName() != null) {
					model.addAttribute("role", request.getSession().getAttribute(ReferralConstants.ROLE));
				}
				isValidCaptcha = true;
			} else {
				try {
					SiteVerifyResponse siteVerifyResponse = reCaptchaV2.verify(gRecaptchaResponse);
					if ( siteVerifyResponse.isSuccess() ) {
						isValidCaptcha = true;
					} else {
						isValidCaptcha = false;
					}
				} catch (IOException e) {
					isValidCaptcha = true;//setting to true incase of captcha issue.
					LOGGER.error("Error happened while validating recaptcha",e);
				}
			}

			if (!isValidCaptcha) {
				model.addAttribute("invalidCaptcha", "true");
				status = "INVALID_CAPTCHA";
				errorMsg = "Incorrect captcha. Please try again.";
			}else if(null ==  request.getSession().getAttribute(ReferralConstants.SSAP_MAP)) {
				status = "EMPTY_QUESTION_MAP";
				errorMsg = "Something went wrong. Please try again.";
				
				if(userService.getLoggedInUser() != null && userService.getLoggedInUser().getActiveModuleName() != null) {
					model.addAttribute("currentRoleName",userService.getLoggedInUser().getActiveModuleName().toUpperCase() );
				}
				
				model.addAttribute("errorMsg", errorMsg);
				
				if (!("Y".equalsIgnoreCase((String) request.getSession().getAttribute("enrolledAppSess")) || "Y".equalsIgnoreCase((String)request.getSession().getAttribute("ActiveAppPresent")))  ){
					model.addAttribute("emptyQuestionMap","Y");
				}
				
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, status));
				return redirectUrl;
			} else {
				dynamicSecurityQuestionsValidator.validate(dynamicSecurityQuestionsDTO, bindingResult);
				if (bindingResult.hasErrors()) {
					LOGGER.debug("Referral Verify Submission has errors " + bindingResult.getFieldErrors());
					request.getSession().setAttribute("fieldErrors", bindingResult.getFieldErrors());
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
					        new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Referral Activation Verify Submission has errors " + bindingResult.getFieldErrors()));
					return "redirect:/referral/dynamicsecurityquestions/" + request.getSession().getAttribute(ReferralConstants.ENC_REF_ACCESS_CODE).toString();
				}
				ssapMap = (Map<String, String>) request.getSession().getAttribute(ReferralConstants.SSAP_MAP);
				isSecurityAuthSuccessful = validateSecurityAnswers(request, ssapMap, isSecurityAuthSuccessful);
				accountActivation = accountActivationService.getAccountActivationByCreatedObjectId(Integer.valueOf((String) request.getSession().getAttribute(ReferralConstants.SSAP_ID)), ActivationJson.OBJECTTYPE.INDIVIDUAL_REFERRAL.name());

				if(accountActivation != null) {
					jsonObject = accountActivationService.getActivationJson(accountActivation.getJsonString());

					GiAuditParameterUtil.add(new GiAuditParameter("Valid Security Answers", isSecurityAuthSuccessful));

					if (!isSecurityAuthSuccessful) {
						String referralId = (String) request.getSession().getAttribute(ReferralConstants.REFERRAL_ID);

						if ((null == roleAccessControl.getCurrentUser()) || /** no logged in user */
						/** any user other than CSR */
						(null != roleAccessControl.getCurrentUser() && !StringUtils.equalsIgnoreCase((String) request.getSession().getAttribute(ReferralConstants.ROLE), ReferralConstants.CSR))) {
							int count = referralActivationService.updateReferralActivationCount(referralId);

							GiAuditParameterUtil.add(new GiAuditParameter("Retry Count", count));

							if (count < 4) {
								errorMsg = messageSource.getMessage("msg.referral.securityQuestions.validationFailed", null, LocaleContextHolder.getLocale());
								model.addAttribute("invalidAnswers", "true");
								model.addAttribute("encAccessCode", request.getSession().getAttribute("encAccessCode"));
								status = "INVALID_ANSWERS";
							} else {
								errorMsg = messageSource.getMessage("msg.referral.accessCode.lockMessage", null, LocaleContextHolder.getLocale());
								model.addAttribute("locked", "true");
								status = "ACCOUNT_LOCKED";
							}
						} else {
							// this should reach only in case of CSR flow..
							errorMsg = messageSource.getMessage("msg.referral.securityQuestions.validationFailed", null, LocaleContextHolder.getLocale());
							model.addAttribute("invalidAnswers", "true");
							model.addAttribute("encAccessCode", request.getSession().getAttribute("encAccessCode"));
							status = "INVALID_ANSWERS";
						}

					} else {
						String referralId = (String) request.getSession().getAttribute(ReferralConstants.REFERRAL_ID);
						referralActivationService.updateReferralActivationSuccessCount(referralId);
						request.getSession().setAttribute("firstName", ssapMap.get("1"));
						request.getSession().setAttribute("lastName", ssapMap.get("2"));
						//request.getSession().removeAttribute(ReferralConstants.SSAP_MAP);//This has been commented under HIX-108543
						accountUser = userService.getLoggedInUser();

						GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS));

						if (null == accountUser) {
							populateSignupSession(request, ssapMap, accountActivation, jsonObject);
							LOGGER.info("Setting redirect attributes");
							redirectAttributes.addFlashAttribute("bypassPreventAddConsumerConfig", "Y");
							redirectUrl = "redirect:/account/signup/" + ghixJasyptEncrytorUtil.encryptStringByJasypt(INDIVIDUAL);
						} else {
							redirectUrl = "redirect:/referral/dynamicsecurityquestions/verify/" + ghixJasyptEncrytorUtil.encryptStringByJasypt((String) request.getSession().getAttribute(ReferralConstants.REFERRAL_ID));
						}
					}
				}else {
					//HIX-108268- In case there is no account-activation record we will show error message to user.
					errorMsg = "Application can't be link with account. Please contact support." ;
					LOGGER.error("ERR: WHILE FETCHING ACCOUNT_ACTIVATION:NULL ");
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING RANDOM LIST: ", ex);
			errorMsg = "Security question validation failed. Internal server error. Please contact support";
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
			throw new GIRuntimeException(errorMsg);
		}

		model.addAttribute("errorMsg", errorMsg);
		if (StringUtils.isNotEmpty(errorMsg)) {
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, status));
		}
		model.addAttribute("securityQuestions", true);
		return redirectUrl;
	}

	@GiAudit(transactionName = "Pre-Referral Linking", eventType = EventTypeEnum.REFERRAL_ACTIVATION, eventName = EventNameEnum.PII_WRITE)
	@RequestMapping(value = { "/dynamicsecurityquestions/verify/{referralActivationId}" }, method = RequestMethod.GET)
	public String dynamicSecurityVerify(@PathVariable("referralActivationId") String referralActivationId, Model model, HttpServletRequest request) throws InvalidUserException {
		//boolean isHouseholdAndPrimaryEqual = false;
		ReferralLinkingOverride referralLinkingOverride = null;
		String redirectUrl = null;
		int householdId = 0;
		String errorMsg = null;

		HttpSession session = request.getSession();
		session.setAttribute(ReferralConstants.ACTIVE_APP_PRESENT, "N");
		session.setAttribute(ReferralConstants.ENROLLED_APP_PRESENT, "N");

		try {
			LOGGER.info("Referral Security Verify Main Starts for Id - " + ghixJasyptEncrytorUtil.decryptStringByJasypt(referralActivationId));
			final int referralId = Integer.valueOf(ghixJasyptEncrytorUtil.decryptStringByJasypt(referralActivationId));
			if (!this.isAllowed(model, request)) {
				LOGGER.warn("User not authorized");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "User not authorized"));
				return REDIRECT_ACCOUNT_USER_LOGINFAILED;
			}

			final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);

			if (referralActivation == null) {
				LOGGER.warn("Not data found for Referral Activation Id " + referralId);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Not data found for Referral Activation Id " + referralId));
				return "redirect:/indportal";
			}

			householdId = getHouseholdId(request, referralActivation);

			if (householdId > 0) {
				request.getSession().setAttribute("householdId", householdId);
				/* Check Non-Fin applications */
				long appCount = referralActivationService.getActiveApplicationCountForCMR(householdId, referralActivation.getSsapApplicationId());

				Map<String, String> enrolledStatus = referralActivationService.checkforEnrolledApplication(householdId, referralActivation.getSsapApplicationId());

				GiAuditParameterUtil.add(new GiAuditParameter("Nonfinancial App Count", appCount), new GiAuditParameter("Enrolled App", enrolledStatus.get("CMR_ENROLLED_FLG")));

				if (appCount == 0 && enrolledStatus != null && StringUtils.equalsIgnoreCase(enrolledStatus.get("CMR_ENROLLED_FLG"), ReferralConstants.N)) {
					referralLinkingOverride = referralActivationService.compareHouseholdAndPrimary(String.valueOf(referralActivation.getSsapApplicationId()), String.valueOf(householdId));
					String firstTimeLinking = (String) session.getAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS), new GiAuditParameter("HouseholdId", householdId), new GiAuditParameter("isHouseholdAndPrimaryEqual", referralLinkingOverride.isOverride()),
					        new GiAuditParameter("firstTimeLinking", firstTimeLinking));
					if (referralLinkingOverride.isSameHH() || (!referralLinkingOverride.isSameHH() && referralLinkingOverride.isOverride())) {
						session.removeAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING);
						redirectUrl = "redirect:/referral/ridp/link/" + referralActivationId +"?isOverride="+referralLinkingOverride.isOverride();
					}else {
						//When referralLinkingOverride.isOverride() is false, we will not override.
						redirectUrl = "referral/errorpage";
						populateErrorData(model, request, session, appCount, enrolledStatus);
					}
					
					/* else {
						model.addAttribute("referralActivationId", referralActivationId);
						model.addAttribute("referralLinkingOverride", referralLinkingOverride);
						redirectUrl = "referral/householdOverride";
					}*/
				} else {
					redirectUrl = "referral/errorpage";
					populateErrorData(model, request, session, appCount, enrolledStatus);
				}
			} else {
				errorMsg = "Internal server error. No household found. Please contact support";
				throw new GIRuntimeException(errorMsg);
			}
		} catch (Exception e) {
			LOGGER.error("Error_Msg @ securityVerifyMain  " + e.getMessage());
			GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE), new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
			throw new GIRuntimeException(ReferralServiceException.ReferralErrors.SECURITY_QUESTION_RETRIEVAL_ERROR.getId(), e, null, Severity.HIGH);

		}

		return redirectUrl;
	}

	private void populateErrorData(Model model, HttpServletRequest request, HttpSession session, long appCount, Map<String, String> enrolledStatus) throws InvalidUserException {
		session.setAttribute("coverageYear", enrolledStatus.get("COVERAGE_YEAR"));
		final String switchToNonDefaultURL = "/account/user/switchNonDefRole/individual";
		model.addAttribute("switchToNonDefaultURL", switchToNonDefaultURL);
		
		model.addAttribute("currentRoleName",userService.getLoggedInUser().getActiveModuleName().toUpperCase());// roleAccessControl.getCurrentUser().getDefRole().getName()
		model.addAttribute("householdId", String.valueOf(request.getSession().getAttribute("householdId")));
		model.addAttribute("firstName", request.getSession().getAttribute("firstName"));
		model.addAttribute("lastName", request.getSession().getAttribute("lastName"));
		request.getSession().removeAttribute("householdId");
		request.getSession().removeAttribute("firstName");
		request.getSession().removeAttribute("lastName");
		if (appCount > 0) {
			session.setAttribute("ActiveAppPresent", "Y");
		} else if ("Y".equalsIgnoreCase(enrolledStatus.get("CMR_ENROLLED_FLG"))) {
			session.setAttribute(ReferralConstants.ENROLLED_APP_PRESENT, "Y");
		}
		clearDataFromSessionForAccessCode(request);
	}

	/**
	 * @param request
	 * @param referralActivation
	 * @return
	 */
	private int getHouseholdId(HttpServletRequest request, final ReferralActivation referralActivation) {
		AccountUser accountUser = null;
		int householdId = 0;

		try {
			accountUser = userService.getLoggedInUser();
			if (null != accountUser) {
				String roleName = roleAccessControl.getCurrentUserRoleName();
				if ("INDIVIDUAL".equals(roleName)) {
					householdId = accountUser.getActiveModuleId();
				} else {
					if (referralActivation.getSsapApplicationId() != null)
					{
						SsapApplication application = ssapApplicationRepository.findOne(referralActivation.getSsapApplicationId().longValue());
						if (application != null && application.getCmrHouseoldId() != null )
						{
							householdId = application.getCmrHouseoldId().intValue();
						}
					}
					if(householdId <=0) {
						householdId = referralActivationService.checkForExistingHouseholdFromSsnDoB(String.valueOf(referralActivation.getSsapApplicationId()));
					}
					if (householdId == 0) {
						throw new GIRuntimeException("household not found for referral linking flow");
					}

				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE GETTING HOUSEHOLD: ", ex);
		}

		return householdId;
	}

	/**
	 * @param request
	 * @param ssapMap
	 * @throws NumberFormatException
	 */
	private void processReferralData(HttpServletRequest request, Map<Integer, String> ssapMap) throws NumberFormatException {
		String referralId = null;

		request.getSession().setAttribute(ReferralConstants.SSAP_ID, ssapMap.get(ReferralConstants.SSAP_APPLICATION_KEY));
		request.getSession().setAttribute(ReferralConstants.SSAP_MAP, ssapMap);

		if (LOGGER.isInfoEnabled()) {
			LOGGER.info("SSAP APP ID: " + (String) request.getSession().getAttribute(ReferralConstants.SSAP_ID));
		}

		referralId = ssapMap.get(ReferralConstants.REFERRAL_ID_KEY);
		request.getSession().setAttribute(ReferralConstants.REFERRAL_ID, referralId);

	}

	/**
	 * @param request
	 * @param ssapMap
	 * @param isSecurityAuthSuccessful
	 * @return
	 * @throws NumberFormatException
	 * @throws ParseException
	 * @throws Exception
	 */
	private boolean validateSecurityAnswers(HttpServletRequest request, Map<String, String> ssapMap, boolean isSecurityAuthSuccessful) throws NumberFormatException, ParseException, Exception {

		List<String> randomList = new ArrayList<String>();
		
		randomList.addAll(ssapMap.keySet());
		randomList.remove("9001");
		randomList.remove("9002");
		randomList.remove("9003");
		
		for (String idx : randomList) {
			String randomAnswerIdx = request.getParameter(ReferralConstants.randomQuestionsMap.get(Integer.valueOf(idx)));
			String randomAnswerDb = ssapMap.get(idx);

			// Parsing DOB in UI and DB to
			if (idx.equals(ReferralConstants.BIRTH_DATE_KEY)) {
				if (0 != compareDates(randomAnswerIdx, randomAnswerDb)) {
					isSecurityAuthSuccessful = false;
					break;
				}
			} else if (idx.equals(ReferralConstants.SSN_KEY)) {
				randomAnswerIdx = randomAnswerIdx.replaceAll("-", "");
			} else if (idx.equals(ReferralConstants.PHONE_NUMBER_KEY)) {
				randomAnswerIdx = randomAnswerIdx.replaceAll("\\s", "");
				randomAnswerIdx = randomAnswerIdx.replaceAll("-", "");
				randomAnswerIdx = randomAnswerIdx.replaceAll("\\(", "");
				randomAnswerIdx = randomAnswerIdx.replaceAll("\\)", "");
			}

			if (!idx.equalsIgnoreCase(ReferralConstants.BIRTH_DATE_KEY) && !randomAnswerIdx.equalsIgnoreCase(randomAnswerDb)) {
				isSecurityAuthSuccessful = false;
				break;
			}
		}
		return isSecurityAuthSuccessful;
	}

	/**
	 * 
	 * @param request
	 * @param ssapMap
	 * @param jsonObject
	 * @throws NumberFormatException
	 */
	private void populateSignupSession(HttpServletRequest request, Map<String, String> ssapMap, AccountActivation accountActivation, ActivationJson jsonObject) throws NumberFormatException {
		request.getSession().setAttribute(ReferralConstants.REFERRAL_LINKING_FLOW, "true");
		request.getSession().setAttribute(ReferralConstants.ACCOUNT_ACTIVATION_FLOW, "true");
		request.getSession().setAttribute(ReferralConstants.ACTIVATION_ID, String.valueOf(accountActivation.getId()));
		request.getSession().setAttribute(ReferralConstants.FIRST_NAME, ssapMap.get("1"));
		request.getSession().setAttribute(ReferralConstants.LAST_NAME, ssapMap.get("2"));
		request.getSession().setAttribute(ReferralConstants.EMAIL_ID, jsonObject.getCreatedObject().getEmailId());
		
		
		int referralId = Integer.valueOf((String) request.getSession().getAttribute(ReferralConstants.REFERRAL_ID));
		
		final ReferralActivation referralActivation = referralActivationService.fetchReferralActivationObject(referralId);
		int householdId = 0;
		if (referralActivation.getSsapApplicationId() != null)
		{
			SsapApplication application = ssapApplicationRepository.findOne(referralActivation.getSsapApplicationId().longValue());
			if (application != null && application.getCmrHouseoldId() != null )
			{
				householdId = application.getCmrHouseoldId().intValue();
			}
		}
		if(householdId <=0) {
		 householdId = referralActivationService.checkForExistingHouseholdFromSsnDoB(String.valueOf(referralActivation.getSsapApplicationId()));
		}
		
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("populateSignupSession referral householdid = {}",householdId);
		}
		request.getSession().setAttribute(ReferralConstants.HOUSEHOLD_ID,String.valueOf(householdId));
		if (ReferralUtil.listSize(jsonObject.getCreatedObject().getPhoneNumbers()) > 0) {
			request.getSession().setAttribute(ReferralConstants.PHONE, jsonObject.getCreatedObject().getPhoneNumbers().get(0));
		}
		request.getSession().setAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL, "Y");
		request.getSession().setAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID, String.valueOf(accountActivation.getCreatedObjectId()));
	}

	/**
	 * @param randomAnswerIdx
	 * @param randomAnswerDb
	 * @throws ParseException
	 */
	private int compareDates(String randomAnswerIdx, String randomAnswerDb) throws ParseException, Exception {
		DateFormat df = null;

		Date uiDate = null;
		Date dbDate = null;

		try {
			df = new SimpleDateFormat(ReferralConstants.UI_DATE_FORMAT);
			uiDate = df.parse(randomAnswerIdx);
			df = new SimpleDateFormat(ReferralConstants.DB_DATE_FORMAT);
			dbDate = df.parse(randomAnswerDb);
		} catch (ParseException ex) {
			LOGGER.error("ERR: WHILE CMPRNG DATE: ", ex);
			throw ex;
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE CMPRNG DATE: ", ex);
			throw ex;
		}

		return uiDate.compareTo(dbDate);
	}

	private boolean validateCoverageYear(Integer coverageYear) {
		if (coverageYear == null || coverageYear.intValue() == 0) {
			return false;
		} else {
			return true;
		}
	}

	private String readValue(final String key, final Locale locale) {
		return messageSource.getMessage(key, null, locale);
	}

	private String toEncrypt(final int idToEncrypt) {
		return ghixJasyptEncrytorUtil.encryptStringByJasypt("" + idToEncrypt);
	}

	private int toDecrypt(String strToDecrypt) {
		return Integer.parseInt(ghixJasyptEncrytorUtil.decryptStringByJasypt(strToDecrypt));
	}
	
	private void logIndividualAppEvent(String eventName, String eventType, Map<String, String> mapEventParam,Integer cmrId ){

		try {
			 
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();

			eventInfoDto.setModuleId(cmrId );
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);

		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}
	
}