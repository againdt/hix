<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<%@ page isELIgnored="false"%>

<div class="clearfix">
	
	
	<div class="control-group pull-left margin5-lr">
		<label for="" class="control-label"><spring:message code="lbl_tkt.assignee"/></label>
		<div class="controls">
			<input class="input-medium" name="assignee" type="text" id="assignee" onfocus="javascript:fetchAssigneeList();" value="${searchCriteria.assignee}" />
 	 	 	<input type ="hidden" name="assigneeId" id="assigneeId" value="${searchCriteria.assigneeId}" /> 
		</div>
		<div id="assignee_error"></div>
	</div>
	
	
	
	<%-- <c:set var="selectedStatusList" value="${searchCriteria.Queues}"/> --%>
	<div class="control-group pull-left margin5-lr">
		<label for="planLevel" class="control-label"><spring:message code="lbl_searchTkt.queue"/></label>
		<div class="controls">
			<select id="queueName" name="queueName" class="input-medium">
				<option value="" <c:if test="${searchCriteria.queueName == ''}"> selected="selected" </c:if>><spring:message code="option_tkt.any"/></option>
				<c:forEach var="assigned" items="${queueName}">
				<option value="<encryptor:enc value = "${assigned.name}"/>" <c:if test="${searchCriteria.queueName == assigned.name}"> selected="selected" </c:if>>${assigned.name}</option>
			</c:forEach>
			</select>
		</div>
	</div>


	
	<div class="control-group">
                            <label for="openCount" class="control-label">Open tickets</label>
                            <div class="controls">
                                <select  name="openCount" id="openCount" class="input-medium">
                                    <!-- <option value="">SELECT</option> -->
                                    <option value="" <c:if test="${searchCriteria.openCount == ''}"> selected="selected" </c:if>><spring:message code="option_tkt.any"/></option>
                                    <option ${searchCriteria.openCount == '0-5' ? 'selected="selected"' : ''} value="0-5">0-5</option>
                                    <option ${searchCriteria.openCount == '6-10' ? 'selected="selected"' : ''} value="6-10">6-10</option>
                                    <option ${searchCriteria.openCount == '11-20' ? 'selected="selected"' : ''} value="11-20">11-20</option>
                                    <option ${searchCriteria.openCount == '21-30' ? 'selected="selected"' : ''} value=21-30>21-30</option>
                                    <option ${searchCriteria.openCount == '30+' ? 'selected="selected"' : ''} value="30+">30+</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->


</div><!--first row ends-->

<div class="clearfix">
	
	
	
						<div class="control-group">
                            <label for="dueIn" class="control-label">Due In</label>
                            <div class="controls">
                                <select  name="dueIn" id="dueIn" class="input-medium">
                                    <!-- <option value="">SELECT</option> -->
                                    <option value="" <c:if test="${searchCriteria.dueIn == ''}"> selected="selected" </c:if>><spring:message code="option_tkt.any"/></option>
                                    <option ${searchCriteria.dueIn == 'OVERDUE' ? 'selected="selected"' : ''} value="OVERDUE">OVERDUE</option>
                                    <option ${searchCriteria.dueIn == 'TODAY' ? 'selected="selected"' : ''} value="TODAY">TODAY</option>
                                    <option ${searchCriteria.dueIn == 'TOMORROW' ? 'selected="selected"' : ''} value="TOMORROW">TOMORROW</option>
                                    <option ${searchCriteria.dueIn == 'ZERO_TO_SEVEN_DAYS' ? 'selected="selected"' : ''} value="ZERO_TO_SEVEN_DAYS">0-7 DAYS</option>
                                    <option ${searchCriteria.dueIn == 'EIGHT_TO_FOURTEEN_DAYS' ? 'selected="selected"' : ''} value="EIGHT_TO_FOURTEEN_DAYS">8-14 DAYS</option>
                                </select>
                            </div> <!-- end of controls-->
                        </div><!-- end of control-group -->
	

	
	
	
	
	<div class="control-group pull-left margin5-lr">
		<label for="" class="control-label">&nbsp;</label>
     	<div class="controls">		
        	<input type="submit" class="btn btn-primary input-medium" value="Search" title="Click to Search">
		</div>
   	</div>
</div><!-- second row ends -->
