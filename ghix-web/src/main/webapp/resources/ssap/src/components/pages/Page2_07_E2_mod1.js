import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import AnotherParentQuestionBlock from './pageIncludes/thcBlocks/AnotherParentQuestionBlock';
import ConfirmationBlock from './pageIncludes/thcBlocks/ConfirmationBlock';
import DependentsBlock from './pageIncludes/thcBlocks/DependentsBlock';
import ExceptionsBlock from './pageIncludes/thcBlocks/ExceptionsBlock';
import FilersBlock from './pageIncludes/thcBlocks/FilersBlock';
import Form from './Form';
import PairsBlock from './pageIncludes/thcBlocks/PairsBlock';
import PrimaryFilerBlock from './pageIncludes/thcBlocks/PrimaryFilerBlock';
import TaxHouseholdCompositionSummary from '../summary/TaxHouseholdCompositionSummary';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData, clearMethods, hideModal, setMethods, showModal } from '../../actions/actionCreators';

// Utils

import bitWiseTools from '../../utils/bitWiseOperatorTools';
import { analyzeFamily } from '../../utils/analyzeFamily';

// Constants

import EMPTY_OBJECTS from '../../constants/emptyObjects';
import MASKS, { THC_TYPES } from '../../constants/masksConstants';
import MODAL_TYPES from '../../constants/modalTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';



const Page2_07_E2_mod1 = props => {

    const {
        buttonContainer: ButtonContainer,
        data,
        data: {
            singleStreamlinedApplication: {
                coverageYear
            }
        },
        form: {
            Form: {
                isEligiblyForSave,
                isFormSubmitted,
                isFormOnTopLevelValid
            }
        },
        isFirstVisit,
        householdMembers,
        loadedRelations,
        taxHousehold: {
            primaryTaxFilerPersonId
        }
    } = props;

    const {
        flagBucket: {
            taxHouseholdCompositionFlags
        }
    } = householdMembers[0];

    const {
        taxHouseholdComposition: taxHouseholdCompositionMasks,
        taxHouseholdComposition: {
            anyChangesFlags,
            livesWithAnotherParentFlags,
            moreInformationFlags
        }
    } = MASKS;

    const {
        anyChanges,
        moreInformation
    } = bitWiseTools
        .convertFlagsToValues(taxHouseholdCompositionFlags, taxHouseholdCompositionMasks);

    const [ isFirstRender, toggleFirstRender ] = useState(true);

    const [ noneOfAbove, toggleNoneOfAbove ] = useState(isFirstVisit ? null : householdMembers.every(hhm => {
        const {
            taxHouseholdComposition: {
                taxFilingStatus,
                taxRelationship
            }
        } = hhm;
        return !(taxFilingStatus === THC_TYPES.FILING_JOINTLY ?
            true : (taxRelationship === THC_TYPES.FILER || taxRelationship === THC_TYPES.FILER_DEPENDENT));
    }));

    const family = analyzeFamily(householdMembers, loadedRelations);

    useEffect(() => {
        props.actions.setMethods('Page2_07_E2_mod1', { handleSaveClick });
        return () => {
            props.actions.clearMethods('Page2_07_E2_mod1');
        };
    }, [
        family.filers.map(f => f.isFilingTaxes).toString(),
        family.dependents.map(d => d.isDependent).toString(),
        primaryTaxFilerPersonId,
        isEligiblyForSave
    ]);

    useEffect(() => {

        if(!isFirstRender) {

            const newState = { ...data };

            if(family.filersQty === 1) {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .primaryTaxFilerPersonId = family.filers.find(filer => filer.isFilingTaxes).filerPersonId;
            } else {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .primaryTaxFilerPersonId = 0;
            }

            newState
                .singleStreamlinedApplication
                .taxHousehold[0].householdMember = householdMembers.map(hhm => {
                    const {
                        personId,
                        taxHouseholdComposition: {
                            taxRelationship,
                            taxFilingStatus
                        }
                    } = hhm;

                    switch(taxRelationship) {
                    case THC_TYPES.FILER:
                        return analyzeFamily.updateHhm(hhm, {
                            taxFilingStatus: analyzeFamily.isPersonMarried(personId) ? taxFilingStatus : THC_TYPES.SINGLE,
                            claimerIds: []
                        });
                    case THC_TYPES.FILER_DEPENDENT:
                        return analyzeFamily.updateHhm(hhm, {
                            taxFilingStatus: analyzeFamily.isPersonMarried(personId) ? taxFilingStatus : THC_TYPES.SINGLE,
                            taxRelationship: THC_TYPES.FILER,
                            claimerIds: []
                        });
                    default:
                        return analyzeFamily.updateHhm(hhm, {
                            taxFilingStatus: THC_TYPES.UNSPECIFIED,
                            taxRelationship: THC_TYPES.UNSPECIFIED,
                            claimerIds: []
                        });
                    }
                });

            props.actions.changeData(newState);
        }
    }, [ family.filersQty ]);

    const watchedValues = [
        family.filteredCombinedFilers
            .reduce((res, filer) => { return res + Array.isArray(filer.filerIndex) ? 2 : 1; }, 0),

        family.dependents
            .filter(d => d.isDependent).length
    ];

    useEffect(() => {

        const newState = { ...data };

        if(family.filteredCombinedFilers.length !== 0) {

            // In case of any changes in filers' structure we set householdId to 0 before recalculating
            // Back-end handles only 0 as default. In case of any change warn back-end developer!

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember = newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember.map(hhm => ({
                        ...hhm,
                        householdId: 0
                    }));



            let counter = -1;

            family.filteredCombinedFilers.forEach(filer => {
                if(Array.isArray(filer.filerIndex) && filer.taxFilingStatus === THC_TYPES.FILING_JOINTLY ) {

                    counter++;

                    filer.filerIndex.forEach(idx => {
                        newState
                            .singleStreamlinedApplication
                            .taxHousehold[0]
                            .householdMember[idx]
                            .householdId = counter;
                    });
                } else {
                    newState
                        .singleStreamlinedApplication
                        .taxHousehold[0]
                        .householdMember[filer.filerIndex]
                        .householdId = ++counter;
                }
            });
        }

        family.dependents
            .filter(d => d.isDependent)
            .forEach(d => {
                const  {
                    claimerIds,
                    dependentIndex
                } = d;

                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[dependentIndex]
                    .householdId = householdMembers[claimerIds[0] - 1].householdId;
            });

        props.actions.changeData(newState);

    }, [ ...watchedValues ]);

    const clearBitWiseFlags = (newState, [ additionalFlags = undefined , index = undefined]) => {

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember.map((hhm, i) => {

                    const {
                        flagBucket: {
                            taxHouseholdCompositionFlags
                        }
                    } = hhm;

                    return {
                        ...hhm,
                        flagBucket: {
                            ...hhm.flagBucket,
                            taxHouseholdCompositionFlags: bitWiseTools
                                .getUpdatedFlags(taxHouseholdCompositionFlags,
                                    [
                                        [ livesWithAnotherParentFlags, null ],
                                        [ moreInformationFlags, null ],
                                        i === index ? additionalFlags : []
                                    ]
                                )
                        }
                    };
                });
        return newState;
    };

    const onFieldChange = (...args) => {
        const [ , indexInArray, , [ name , value, disabled ] ] = args;

        if(disabled) {
            return null;
        }

        const newState = { ...data };

        const hhmIndex = Array.isArray(indexInArray) ? indexInArray[0] : indexInArray;

        const hhm = newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[hhmIndex];

        switch(name) {

        case 'isTaxFiler':

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[hhmIndex] = analyzeFamily.updateHhm(hhm, {
                    taxRelationship:  value ? THC_TYPES.FILER : THC_TYPES.UNSPECIFIED
                });

            if(analyzeFamily.isPersonMarried(String(hhmIndex + 1))) {

                const spouseIndex = analyzeFamily.getSpouseIndex(hhmIndex);

                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[spouseIndex] = analyzeFamily.updateHhm(analyzeFamily.getHhmByPersonId(spouseIndex + 1, householdMembers), {
                        taxFilingStatus:  THC_TYPES.UNSPECIFIED
                    });
            }

            if(newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember.some(hhm => {
                    const {
                        taxHouseholdComposition: {
                            taxRelationship
                        }
                    } = hhm;
                    return (taxRelationship === THC_TYPES.FILER || taxRelationship === THC_TYPES.FILER_DEPENDENT);
                })) {
                toggleNoneOfAbove(false);
            }

            toggleFirstRender(false);
            break;

        case 'noneOfAbove':

            if(!value) {
                toggleNoneOfAbove(state => !state);
            } else {
                toggleNoneOfAbove(state => !state);
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember = householdMembers.map(hhm => ({
                        ...hhm,
                        taxHouseholdComposition: {
                            ...hhm.taxHouseholdComposition,
                            ...EMPTY_OBJECTS.HOUSEHOLD_COMPOSITION
                        }
                    }));
            }
            break;

        case 'spousesChoice':

            (() => {
                if(value === 'jointly') {
                    newState
                        .singleStreamlinedApplication
                        .taxHousehold[0].primaryTaxFilerPersonId = 0;
                }

                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember = newState
                        .singleStreamlinedApplication
                        .taxHousehold[0]
                        .householdMember.map(hhm => {
                            const {
                                taxHouseholdComposition: {
                                    taxRelationship
                                }
                            } = hhm;

                            const getNewTaxRelationship = taxRelationship => {
                                switch(taxRelationship) {
                                case THC_TYPES.FILER:
                                case THC_TYPES.FILER_DEPENDENT:
                                    return THC_TYPES.FILER;
                                case THC_TYPES.DEPENDENT:
                                    return THC_TYPES.UNSPECIFIED;
                                default:
                                    return THC_TYPES.UNSPECIFIED;
                                }
                            };

                            return {
                                ...hhm,
                                taxHouseholdComposition: {
                                    ...hhm.taxHouseholdComposition,
                                    taxRelationship: getNewTaxRelationship(taxRelationship),
                                    claimerIds: []
                                }
                            };
                        });

                if(analyzeFamily.isPersonMarried(String(hhmIndex + 1))) {

                    family.pairs[hhmIndex].spouseIndexes.forEach(spouseIndex => {

                        const {
                            taxRelationship: spouseTaxRelationship
                        } = analyzeFamily
                            .getHhmByPersonId(spouseIndex + 1, householdMembers)
                            .taxHouseholdComposition;

                        newState
                            .singleStreamlinedApplication
                            .taxHousehold[0]
                            .householdMember[spouseIndex] = analyzeFamily.updateHhm(analyzeFamily.getHhmByPersonId(spouseIndex + 1, householdMembers), {
                                taxRelationship:  value === 'jointly' ? THC_TYPES.FILER : spouseTaxRelationship
                            });
                    });

                }

                const updatedHhm = index => newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember[index];

                family.pairs[hhmIndex].spouseIndexes.forEach(index => {
                    newState
                        .singleStreamlinedApplication
                        .taxHousehold[0]
                        .householdMember[index] = analyzeFamily.updateHhm(updatedHhm(index),{
                            taxFilingStatus:  value === 'jointly' ?
                                THC_TYPES.FILING_JOINTLY :
                                (updatedHhm(index).taxHouseholdComposition.taxRelationship === THC_TYPES.FILER ||
                                updatedHhm(index).taxHouseholdComposition.taxRelationship === THC_TYPES.FILER_DEPENDENT) ?
                                    THC_TYPES.FILING_SEPARATELY : THC_TYPES.UNSPECIFIED,
                        });
                });
            })();
            toggleFirstRender(false);
            break;

        case 'isPrimaryTaxFiler':
            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .primaryTaxFilerPersonId = hhmIndex + 1;
            break;

        case 'isDependent':

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[hhmIndex] = analyzeFamily.updateHhm(hhm, {
                    taxRelationship:
                        value ?
                            (r => r === THC_TYPES.FILER ? THC_TYPES.FILER_DEPENDENT :  THC_TYPES.DEPENDENT)(analyzeFamily.getIndicatorsValues(hhmIndex + 1).taxRelationship) :
                            (r => r === THC_TYPES.FILER_DEPENDENT ? THC_TYPES.FILER :  THC_TYPES.UNSPECIFIED)(analyzeFamily.getIndicatorsValues(hhmIndex + 1).taxRelationship)
                });

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[indexInArray[0]]
                .taxHouseholdComposition
                .claimerIds = value ? (Array.isArray(indexInArray[1]) ? indexInArray[1] : [indexInArray[1]]) : [];
            break;

        case 'anyChanges':

            if(value) {
                newState
                    .singleStreamlinedApplication
                    .taxHousehold[0]
                    .householdMember = householdMembers.map(hhm => ({
                        ...hhm,
                        householdId: 0,
                        taxHouseholdComposition: {
                            ...hhm.taxHouseholdComposition,
                            ...EMPTY_OBJECTS.HOUSEHOLD_COMPOSITION
                        }
                    }));
                clearBitWiseFlags(newState, [[ anyChangesFlags, value ], 0]);
            }
            else {
                clearBitWiseFlags(newState, [[ anyChangesFlags, value ], 0]);
            }
            break;

        case 'moreInformation':

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[hhmIndex]
                .flagBucket
                .taxHouseholdCompositionFlags = bitWiseTools
                    .getUpdatedFlags(taxHouseholdCompositionFlags, [[ moreInformationFlags, value ]]);
            break;

        case 'livesWithAnotherParent':

            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[hhmIndex]
                .flagBucket
                .taxHouseholdCompositionFlags = bitWiseTools
                    .getUpdatedFlags(taxHouseholdCompositionFlags, [[ livesWithAnotherParentFlags, value ]]);

        }

        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember = newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember.map(hhm => {
                    const {
                        personId,
                        taxHouseholdComposition: {
                            claimerIds,
                            taxFilingStatus
                        }
                    } = hhm;

                    const exemptions = [
                        THC_TYPES.FILING_JOINTLY,
                        THC_TYPES.FILING_SEPARATELY,
                        THC_TYPES.UNSPECIFIED
                    ];

                    if(!exemptions.some(e => e === taxFilingStatus)) {
                        return analyzeFamily.updateHhm(hhm, {
                            taxFilingStatus: analyzeFamily.hasDependents(personId, claimerIds) ?
                                THC_TYPES.HEAD_OF_HOUSEHOLD : THC_TYPES.SINGLE
                        });
                    } else {
                        return hhm;
                    }
                });

        switch(name) {
        case 'isTaxFiler':
        case 'noneOfAbove':
        case 'spousesChoice':
        case 'isDependent':
            clearBitWiseFlags(newState, []); break;
        }

        props.actions.changeData(newState);
    };

    const filteredException1 = family.dependents
        .filter(d => d.isDependent && d.isClaimedByNotParentOrSpouse);

    const filteredException2 = family.dependents
        .filter(d => d.isDependent && d.childClaimedByFilingSeparatelyParents);

    const filteredException3 = family.dependents
        .filter(d => d.isDependent && d.isChildLivesWithParentWhoDoesntClaim);

    const listOfClaimedBySeparatelyLivingParent = family.dependents
        .filter(d => d.isDependent && d.isChildClaimedBySeparatelyLivingParent);

    const theMatched = [filteredException1, filteredException2, filteredException3]
        .reduce((names, list) => names.concat(list.map(d => d.dependentFullName)), []);
    const uniqueMatched = [...new Set(theMatched)];

    const noExceptions = [filteredException1, filteredException2, filteredException3]
        .every(e => e.length === 0);

    const isFilerQuestionResolved = anyChanges === false && (noneOfAbove === true || family.filers.some(f => f.isFilingTaxes));

    const checkTopLevelValidation = () => {
        return anyChanges === false
            && isFilerQuestionResolved
            && ((noExceptions || moreInformation === false) || (moreInformation === null))
            && (family.filersQty > 1 ? primaryTaxFilerPersonId !== 0 : true);
    };

    const handleSaveClick = callback => {

        const isMultiTaxHousehold = (() => {
            return [ ...new Set(householdMembers.map(hhm => hhm.householdId))].length !== 1;
        })();

        if(isEligiblyForSave) {
            if(isMultiTaxHousehold){
                props.actions.showModal(
                    MODAL_TYPES.MULTI_TAX_HOUSEHOLD_MODAL,
                    MODAL_TYPES.MULTI_TAX_HOUSEHOLD_MODAL,
                    [
                        family.filers,
                        primaryTaxFilerPersonId
                    ],
                    [
                        callback,
                        () => props.actions.hideModal(MODAL_TYPES.MULTI_TAX_HOUSEHOLD_MODAL)
                    ],
                );
            } else {
                callback();
            }
        } else {
            callback();
        }
    };

    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>
                <div className="usa-alert usa-alert-info alert--info">
                    <p>
                        Based on information provided so far, below are all the household members and their living arrangement:
                    </p>
                    <TaxHouseholdCompositionSummary {...{ analyzeFamily, family, householdMembers }}/>
                </div>
                <ValidationErrorMessage
                    isVisible={isFormSubmitted && !isFormOnTopLevelValid && anyChanges}
                    messageText={VALIDATION_ERROR_MESSAGES.UPDATE_INFORMATION}
                    errorClasses={['margin-l-20-ve', 'margin-b-10']}
                />
                <ConfirmationBlock {...{
                    anyChanges,
                    ButtonContainer,
                    onFieldChange,
                    currentData: {}
                }}/>

                {
                    anyChanges === false &&

                        <Fragment>

                            <ValidationErrorMessage
                                isVisible={isFormSubmitted && !isFormOnTopLevelValid && !isFilerQuestionResolved}
                                messageText={VALIDATION_ERROR_MESSAGES.DEFINE_TAX_FILERS}
                                errorClasses={['margin-l-20-ve', 'margin-b-10']}
                            />

                            <FilersBlock {...{
                                analyzeFamily,
                                coverageYear,
                                noneOfAbove,
                                householdMembers,
                                onFieldChange,
                                currentData: {}
                            }}/>

                            <PairsBlock {...{
                                coverageYear,
                                family,
                                onFieldChange,
                                currentData: {}
                            }}/>



                            <PrimaryFilerBlock {...{
                                family,
                                householdMembers,
                                isFormSubmitted,
                                isFormOnTopLevelValid,
                                onFieldChange,
                                primaryTaxFilerPersonId,
                                currentData: {}
                            }}/>

                            <DependentsBlock {...{
                                analyzeFamily,
                                family,
                                onFieldChange,
                                currentData: {}
                            }}/>

                            <AnotherParentQuestionBlock {...{
                                listOfClaimedBySeparatelyLivingParent,
                                householdMembers,
                                onFieldChange,
                                currentData: {}
                            }}/>

                            <ExceptionsBlock {...{
                                ButtonContainer,
                                checkTopLevelValidation,
                                family,
                                filteredException1,
                                filteredException2,
                                filteredException3,
                                isFormSubmitted,
                                moreInformation,
                                noExceptions,
                                onFieldChange,
                                uniqueMatched,
                                currentData: {}
                            }}/>
                        </Fragment>
                }
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    loadedRelations: state
        .relations
        .loadedRelations,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember,

    taxHousehold: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        clearMethods,
        hideModal,
        setMethods,
        showModal
    }, dispatch)
});

Page2_07_E2_mod1.defaultProps = {
    name: 'Page2_07_E2_mod1'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_07_E2_mod1);
