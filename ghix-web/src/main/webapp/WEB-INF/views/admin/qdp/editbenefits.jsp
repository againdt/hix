<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${planId}" isurl="true"/> </c:set>
<div class="row-fluid">
	<ul class="page-breadcrumb">
        <li><a href="#">&lt; <spring:message code="label.back"/></a></li>
        <li><a href="#"><spring:message code="label.plans"/></a></li>
        <li><a href="#"><spring:message code="label.linkManageQDP"/></a></li>
        <li><spring:message code='label.linkDentalPlanBenefits'/></li>
    </ul><!--page-breadcrumb ends-->
    <h1 class="span10 margin0"><a name="skip"></a>${planName}</h1>>
</div>
<div class="row-fluid">
	<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.aboutPlan'/></h4>
       			</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}" />"><spring:message  code='label.linkDentalPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpbenefits/${encPlanId}"/>"><spring:message  code='label.linkDentalPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdprates/${encPlanId}" />"><spring:message  code='label.linkDentalPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqdpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkDentalProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
			   <br><br>
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0">Actions</h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqdpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqdpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>

			
        </div><!-- end of span3 -->

	<div class="span9" id="rightpanel">
		<div class="header">
			<h4 class="pull-left"><spring:message code='label.linkDentalPlanBenefits' /></h4>
			<a class="btn btn-small" title="<spring:message code='label.btnCancel'/>" href="<c:url value="/admin/planmgmt/viewqdpbenefits/${encPlanId}"></c:url>"><spring:message code='label.btnCancel'/></a> 
			<a class="btn btn-primary btn-small" title="<spring:message  code='label.btnSave'/>" href="#" onclick="formSubmit();"><spring:message  code='label.btnSave'/></a>
		</div>
			<table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><spring:message code='label.issuerName' /></td>
						<td><strong>${issuerName}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planName' /></td>
						<td><strong>${planName}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message code='label.planNumber' /></td>
						<td><strong>${planNumber}</strong></td>
					</tr>

				</tbody>
			</table>

		<form class="form-horizontal form-bottom gutter10" id="frmBenefits"
			name="frmBenefits" action="<c:url value="/planmgmt/uploadData" />"
			method="post" enctype="multipart/form-data">
			<df:csrfToken/>
			<table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><spring:message code='label.titlePlanBenefits' /></td>
						<td>
							<input type="file" id="benefits" name="benefits" class="input-file"> 
							&nbsp;&nbsp;<button type="submit" class="btn" onclick="validCsv('#benefits','#frmBenefits','#benefitFile_error')" ><spring:message code='label.upload'/></button>
							<input type="hidden" name="fileType" value="benefits" />
							<div id="benefitFile_error" class="">${benefitsFileError}</div>
							<div id="benefitsFileName" class="help">${benefitsFileName}</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		
		<form class="form-horizontal form-bottom gutter10" id="frmBrochure"
			name="frmBrochure" action="<c:url value="/planmgmt/uploadData" />"
			method="post" enctype="multipart/form-data">
			<df:csrfToken/>
			<table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><spring:message code='label.planBrochure' /></td>
						<td>
							<input type="file" id="brochure" name="brochure" class="input-file"> 
							&nbsp;&nbsp;<button type="submit" class="btn"><spring:message code='label.upload'/></button>
							<input type="hidden" name="fileType" value="brochure" />
							<div id="brochure_error" class="">${brochureFileError}</div>
							<div id="brochureFileName" class="help">${brochureFileName}</div>
						</td>
					</tr>
				</tbody>
			</table>
		</form>
		<form class="form-horizontal gutter10" id="frmUploadBenefits" name="frmUploadBenefits" action="<c:url value="/admin/planmgmt/editqdpbenefits" />" method="POST">
			<df:csrfToken/>
			<input type="hidden" id="id" name="id" value="<encryptor:enc value="${planId}"></encryptor:enc>"/>
			<%-- <input type="hidden" id="id" name="id" value="${planId}" /> --%>
			<input type="hidden" id="benefitFile" name="benefitFile" />
			<input type="hidden" id="brochureFile" name="brochureFile" />
			
			<table class="table table-border-none">
				<tbody>
					<tr>
						<td class="span4 txt-right"><spring:message code='label.benefits'/> <spring:message code='label.effectiveDate'/></td>
						<td>
							<input class="input-small datepick" type="text" readonly="readonly" name="benefitStartDate" id="benefitStartDate" placeholder="mm/dd/yyyy" size="35">
							<div id="benefitStartDate_error" class="help-inline"></div>	
						</td>
					</tr>
					<tr>
						<td class="txt-right vertical-align-top"><spring:message code='label.changeJustification' /></td>
						<td><textarea class="input-xlarge" name="comments" id="comments" rows="4" cols="40" style="resize: none;" 
							maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>	                       
	                       	<div id="chars_left"><spring:message code="label.charLeft"/> <b><spring:message code="label.charLeftValue"/></b> </div>	
						</td>
					</tr>
				</tbody>
			</table>
			
		</form>
	</div>
	<!-- end of span9 -->


</div>
<!--  end of row-fluid -->
</div><!--  end of .gutter -->

<script type="text/javascript">
var commentMaxLen = 500;

$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})

//Update remaining characters for comments
function updateCharCount(){	
	var currentLen = $.trim(document.getElementById("comments").value).length;
	var charLeft = commentMaxLen - currentLen;	
	$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
}

function formSubmit() {
	$("#benefitFile_error").html("");
	$("#brochure_error").html("");
	$("#benefitStartDate_error").html("");
	$("#frmUploadBenefits").submit();
}

function validCsv(fileElementId, formElementId, errorElementId) {
	var ext = $(fileElementId).val().split('.').pop().toLowerCase();
	var allow = new Array('csv');
	if (jQuery.inArray(ext, allow) == -1) {
		$(errorElementId).html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileTypeError'/></span></label>");
		return false;
	} else {
		$(formElementId).submit();
	}
}



$(document).ready(function() {
	$.ajaxSetup({ cache: false }); // prevent Ajax caching in JQuery
	var BenefitsUpload = {
		//target:        '#certiName',   // target element(s) to be updated with server response 
		beforeSubmit : submitBenefitsRequest, // pre-submit callback 
		success : submitBenefitsResponse
	// post-submit callback 
	};
	$('#frmBenefits').ajaxForm(BenefitsUpload);
	
	var brocureUpload = {
		beforeSubmit : submitBrocureRequest, // pre-submit callback 
		success : submitBrocureResponse
	};
	$('#frmBrochure').ajaxForm(brocureUpload);
});

function submitBrocureResponse(responseText, statusText, xhr, $form) {
	var val = responseText.split(",");
	if (val[0] != 'Error') {
		$("#brochureFileName").text(val[1]);
		$("#brochureFile").val(val[2]);
	} else {
		$("#brochure_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}

}

function submitBrocureRequest(formData, jqForm, options) {
	var fileName = $('#brochure').val();
	if (fileName.length > 0) {
		return true;
	}
	return false;
}

function submitBenefitsResponse(responseText, statusText, xhr, $form) {
	var val = responseText.split(",");
	if (val[0] != 'Error') {
		$("#benefitsFileName").text(val[1]);
		$("#benefitFile").val(val[2]);
		$('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true,
				minDate : 0,
				onSelect: function() {$("#benefitStartDate_error").html("");}
			});
		});
	} else {
		$("#benefitFile_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}

}

function submitBenefitsRequest(formData, jqForm, options) {
	var fileName = $('#benefits').val();
	if (fileName.length > 0) {
		return true;
	}
	return false;
}

var validator = $("#frmUploadBenefits")
		.validate(
				{
					ignore : "",
					rules : {
						benefitFile : { requierd : false},
						benefitStartDate : {
							required : {
								depends : function() {
									return ($("#benefitFile").val() != "");
								}
							}
						}
					},
					messages : {
						benefitFile : {required : "<span> <em class='excl'>!</em><spring:message code='error.benefitFileRequired'/></span>"},
						benefitStartDate : {required : "<span> <em class='excl'>!</em><spring:message code='error.benefitEfectiveDateReq'/></span>"}
					},
					errorClass : "error",
					errorPlacement : function(error, element) {
						var elementId = element.attr('id');
						error.appendTo($("#" + elementId + "_error"));
						$("#" + elementId + "_error").attr('class', 'error help-block');
					}
				});
jQuery("#frmBenefits").validate({});
</script>
