package com.getinsured.ssap.model.hub.vlp;

import com.getinsured.ssap.model.hub.vlp.response.*;

/**
 * VLP Action Results.
 *
 * @see VlpAction
 * @author Yevgen Golubenko
 * @since 6/4/19
 */
public enum VlpActionResult {
  NONE,

  /**
   * Validation failed.
   */
  VALIDATION_FAILED,

  /**
   * Means we need to call close case method.
   * @deprecated Not going to use it on our side.
   */
  CLOSE_CASE,

  /**
   * Additional steps beyond original verification were invoked
   * by the hub, and we just need to check the result of those
   * additional steps, so periodically invoke check case resolution
   * call.
   */
  CHECK_CASE_RESOLUTION,

  /**
   * Validation Succeeded.
   */
  VALIDATION_SUCCESS,

  /**
   * We need to retry the same call again.
   * Due to communication error, server exception, etc.
   */
  RETRY,

  /**
   * Means we are calling step 1a (Additional Verification)
   */
  REVERIFY,

  /**
   * Means we are calling step 1b (Additional Verification),
   * because original call did not have SEVIS id present and it
   * is required to complete the validation process.
   */
  RESUBMIT_WITH_SEVISID,

  /**
   * Means we need to issue verify step again.
   */
  VERIFY;


  /**
   * Returns next step for given {@link VlpResponse}.
   *
   * @param response {@link VlpResponse} object.
   * @return {@link VlpAction} enum.
   */
  public static VlpActionResult get(VlpResponse response) {
    if (response.getResponseCode() == ResponseCode.SUCCESS) {
      if (!response.getIndividualDetails().isEmpty()) {
        // HubIntegration does VLP Requests for each HH Member separately. vlpRequestMap.forEach(...)
        IndividualDetails individualDetails = response.getIndividualDetails().get(0);

        if(individualDetails.getLawfulPresenceVerifiedCode() == MultiCode.Y ||
            individualDetails.getLawfulPresenceVerifiedCode() == MultiCode.X) {
          return VlpActionResult.VALIDATION_SUCCESS;
        }

        int responseCode = individualDetails.getEligibilityStatementCode();
        InvalidDataTypeCode invalidDataTypeCode = individualDetails.getIavTypeCode();
        switch(responseCode) {
          case 5:
            return VlpActionResult.CHECK_CASE_RESOLUTION;
            // TODO: Ask Reshma again regarding: eligibilityStatementCode:5 text: Institute Additional Verification
          // VlpIntegrationImplTest->verifyI327_fromSoapUI_ResubmitWithSEVIS
          /*
          {"IntialVerificationResponseTypeResponseSet":{"NonCitEntryDate":"2000-09-22","EligStatementCd":5,
          "USCitizenCode":"P","NonCitLastName":"Aiden","NonCitCountryCitCd":"UKR","WebServSftwrVer":"1.0.0.1",
          "QualifiedNonCitizenCode":"P","NonCitCountryBirthCd":"UKR","FiveYearBarMetCode":"P","LawfulPresenceVerifiedCode":"P",
          "NonCitMiddleName":"Aiden","CaseNumber":"9902000000158PJ","NonCitBirthDate":"1974-10-08",
          "EligStatementTxt":"Institute Additional Verification","NonCitFirstName":"Rose","FiveYearBarApplyCode":"P"},
          "ResponseMetadata":{"ResponseCode":"HS000000","ResponseDescriptionText":"Successful.","TDSResponseDescriptionText":"Successful."}}
           */
          case 32:
          case 37:
          case 40:
            switch(responseCode) {
              case 37:
              case 40:
                if(invalidDataTypeCode != InvalidDataTypeCode.RNF) {
                  return VlpActionResult.VALIDATION_FAILED;
                }

                return VlpActionResult.REVERIFY;
              default: // 32
                return VlpActionResult.RESUBMIT_WITH_SEVISID;
            }
          case 119:
            return VlpActionResult.VERIFY;
          default:
            return VlpActionResult.VALIDATION_FAILED;
        }
      }
    } else if (response.getResponseCode() == ResponseCode.COMMUNICATION_ERROR ||
        response.getResponseCode() == ResponseCode.NONE) {
      return VlpActionResult.RETRY;
    } else if(response.getResponseCode() == ResponseCode.CASE_RESULT_NOT_AVAILABLE) {
      // This is hit when we do step 2 case resolution, if verification results are not available, we redo
      // check case resolution.
      return VlpActionResult.CHECK_CASE_RESOLUTION;
    }

    return VlpActionResult.VALIDATION_FAILED;
  }
}
