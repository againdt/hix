export const VALIDATION_ERROR_MESSAGES = {
    UPDATE_INFORMATION: 'Please update information or select "No"',
    PRIMARY_TAX_FILER: 'Please select Primary Tax Filer',
    FIRST_NAME: 'Please enter a valid first name.',
    MIDDLE_NAME: 'Please enter a valid middle name.',
    LAST_NAME: 'Please enter a valid last name.',
    COUNTRY_OF_ISSUANCE: 'Please select the country from which your passport was issued.',
    ANY_VALUE: 'Please answer this question to continue.',
    NATURALIZATION_NUMBER: 'Please enter your naturalization number. The Naturalization Certificate number is between 6 and 12 characters alphanumeric.',
    CITIZENSHIP_NUMBER: ' Please enter your Citizenship number. This will contain 6-12 digits.',
    DAYS_PER_WEEK: 'Please enter number of days between 1 and 7.',
    HOURS_PER_WEEK: 'Please enter number of hours per week between 1 and 168',
    NOT_UNIQUE_SSN: 'This SSN has already been entered for a member of your household. Please provide a different SSN',
    DEFINE_TAX_FILERS: 'Please define tax filers or select \'None of Above\'',
    PROVIDE_SPOUSE: 'Please add your spouse'
};