package com.getinsured.hix.batch.enrollment.xmlEnrollments.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;
import org.springframework.stereotype.Component;

import com.getinsured.hix.model.enrollment.Enrollment;

@Component("EnrollmentsWriter")
public class EnrollmentsWriter implements ItemWriter<Enrollment>{
	
	private Logger logger = LoggerFactory.getLogger(EnrollmentsWriter.class);
	
	@Override
	public void write(List<? extends Enrollment> items) throws Exception {
		logger.debug("Writing ..." + items);
		
	}

}
