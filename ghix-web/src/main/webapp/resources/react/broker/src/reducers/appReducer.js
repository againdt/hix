import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import spinner from './spinnerReducer';
import onDemandReducer from './onDemandReducer';
import brokerConnectReducer from './brokerConnectReducer';
import brokerAvailabilityReducer from './brokerAvailabilityReducer';
import alertReducer from './alertReducer';
import messages from './messagesReducer';

export default combineReducers({
  spinner,
  onDemandReducer,
  brokerConnectReducer,
  brokerAvailabilityReducer,
  form: formReducer,
  alert: alertReducer,
  messages
});
