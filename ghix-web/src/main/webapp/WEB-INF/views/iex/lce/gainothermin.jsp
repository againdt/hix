<script type="text/ng-template" id="gainothermin.jsp">
 <div class="gutter20" ng-controller="gainMinimumCoverageController">
    <div class="alert alert-info">
        <p>
            <spring:message code="label.lce.gainIntro"/> 
        </p>
        <ul>
            <li><spring:message code="label.lce.planYHI"/></li>
            <li><spring:message code="label.lce.employeeSponsoredPlan"/></li>
            <li><spring:message code="label.lce.retireePlan"/></li>
            <li><spring:message code="label.lce.medicare"/></li>
            <li><spring:message code="label.lce.medicaid"/></li>
            <li><spring:message code="label.lce.CHIPLong"/></li>
            <li><spring:message code="label.lce.TRICARELong"/></li>
            <li><spring:message code="label.lce.veteranPlans"/></li>
            <li><spring:message code="label.lce.peacecorpsPlan"/></li>
            <li><spring:message code="label.lce.selfFundedPlan"/></li>
			<li><spring:message code="label.lce.privateOfferedPlans"/></li> 
        </ul>
        </p>
    </div>
    <div>
        <h4>
            <spring:message code="label.lce.famHouseholdSum"/>
        </h4>
        <div>
            <table id="" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold" role="presentation">
                <tbody>
                    <tr>
                        <td>
                            <spring:message code="label.lce.firstname"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.lastname"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.relationship"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.seekingcoverage"/>
                        </td>
                        <td>
                            <spring:message code="label.lce.selectoteditmec"/>
                        </td>
                    </tr>
                    <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
                        <td>{{householdMember.name.firstName}}</td>
                        <td>{{householdMember.name.lastName}}</td>
                        <td>
                            {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
                        </td>
                        <td>
                            <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
                            <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
                        </td>
                        <td style="text-align:center">
							<div ng-if="!householdMember.applyingForCoverageIndicator">--</div>
                            <a class="btn btn-primary btn-small" ng-if="householdMember.applyingForCoverageIndicator" ng-click='addChangedApplicants(householdMember.personId);toggleModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember));'>
                                <spring:message code="label.lce.edit"/>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- MODAL THAT POPS UP WHEN USER WANT TO EDIT  -->
            <modal-dialog show='modalShown' ng-model="modalHouseholdMember">
                <div class="gutter20">
                    <p class="alert alert-info">
                        <spring:message code="label.lce.youhavereported"/> {{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.lastName}} <spring:message code="label.lce.gainmec.p1"/>
                    </p>
                    <form class="form-horizontal ng-scope ng-pristine ng-valid" name="gainOtherForm">
                        <label class="control-label" for="dateOfChange">
                        <spring:message code="label.lce.dateMECObtained"/>:
                        <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                        </label>
                        <div class="controls margin20-t">
                            <div id="gainCalenderDiv" name="gainChangeDate"  required="true" class="input-append ng-pristine ng-valid ng-scope" datetimez="" ng-model="eventInfoArrayTemp[originalIndex].changeInGainDate" required >
                                <input type="text" id="dateOfChange" name="gainChangeDateText"  data-format="MM/dd/yyyy" ng-pattern="/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/"  ng-model="eventInfoArrayTemp[originalIndex].changeInGainDate" class="span9 ng-pristine ng-valid" placeholder='MM/DD/YYYY' check-after="59">
                                <span class="add-on" ng-click='appalyZindex()'>
                                <i class="icon-calendar"></i>
                                </span>
                            </div>
                            <div>
                                <span class="validation-error-text-container" ng-if="submitted && (gainOtherForm.gainChangeDate.$error.required)"><spring:message code="label.lce.plsEnterEventDate"/></span>
                                <span class="validation-error-text-container" ng-if="submitted && (gainOtherForm.gainChangeDateText.$error.pattern )"><spring:message code="label.lce.dateCorrectForm"/></span>
								<span class="validation-error-text-container" ng-if="submitted &&  (gainOtherForm.gainChangeDateText.$error.checkAfter)"><spring:message code="label.lce.futureDateAllowed60"/></span>
                            </div>
                        </div>
                        <div class="control-group margin40-t">
                            <div class="margin20-l">
                                <div>
                                    <label for="opt1">
                                    <input type="radio" name="gainOther" value="{{gainMinEventArray[0].name}}" ng-model="eventInfoArrayTemp[originalIndex].gainEventSubCategory" id="opt1" required>
                                    {{gainMinEventArray[0].label}}
                                    </label>
                                </div>
                                <div>
                                    <label for="opt2">
                                    <input type="radio" name="gainOther" value="{{gainMinEventArray[1].name}}" ng-model="eventInfoArrayTemp[originalIndex].gainEventSubCategory" id="opt2" required>
                                    {{gainMinEventArray[1].label}}
                                    </label>
                                </div>
                                <span class="validation-error-text-container" ng-if="submitted && (gainOtherForm.gainOther.$error.required)"><spring:message code="label.lce.plsSelectOne"/></span>
                            </div>
                            <p class="margin40-t alert alert-info"><spring:message code="label.lce.thxAndPlsCall"/></p>
                            <div>
                                <dl class="dl-horizontal pull-right ">
                                    <a class="btn btn-secondary" ng-click="removeChangedApplicants(modalHouseholdMember.personId);closeModal()"><spring:message code="lce.cancelbutton"/></a>
                                    <a class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex);closeModal();" ng-if="gainOtherForm.$valid"><spring:message code="label.lce.continue"/></a>
                                    <a ng-if="!gainOtherForm.$valid" class="btn btn-secondary" ng-click="$parent.submitted=true"><spring:message code="label.lce.continue"/></a>
                                </dl>
                            </div>
                        </div>
                    </form>
                </div>
            </modal-dialog>
        </div>
    </div>
    <dl class="dl-horizontal">
        <!--<a href="/#" class="btn btn-secondary pull-left" ng-if="!(lifeEventCounter >= 0 || counter >= 1)">
            Go Back to Dashboard
            </a>-->
        <a class="btn btn-primary pull-right" ng-click="next()">
        <spring:message code="label.lce.continue"/>
        </a>
    </dl>
</div>
</script>
