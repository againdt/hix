(function (views) {
	views.SortView = Backbone.View.extend({
		
		events : {
			'click a.sort_indvPremium_asc': 'sort',
			'click a.sort_indvPremium_desc': 'sort',
			'click a.sort_issuer': 'sort'
		},
		
		el : '#sort',
		initialize : function () {
			_.bindAll (this, 'render');
			var self = this;

			self.tmpl = _.template($('#tmpSort').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self;
            self = this;

			var html = this.tmpl(self.collection.info());
			$(this.el).html(html);
		},
		
		sort: function (e) {
			e.preventDefault();
			var self = this;
			var currentSort = e.target.id;

			// if sorting is by Insurer
			if(currentSort =='issuerText'){ 
					if(this.collection.sortDirection == 'asc' && this.collection.lastSortColumn==currentSort){
						this.collection.setSort(currentSort, 'desc');
						$('#'+currentSort+'Order').html(" (desc)");
					}else if(this.collection.sortDirection == 'desc' && this.collection.lastSortColumn==currentSort){
						this.collection.setSort(currentSort, 'asc');
						$('#'+currentSort+'Order').html(" (desc)");
					}
					else{
						this.collection.setSort(currentSort, 'asc');
						$('#'+currentSort+'Order').html(" (asc)");
					}
					
			}else if(currentSort !='issuerText'){ // if sorting is by Premium ASC OR DESC
					this.collection.setSort(currentSort.substr(0,11), currentSort.substr(12));
			}
			
			self.collection.preserveOtherOptions();
		}
		
	});
})(App.views);