package com.getinsured.hix.platform.file.reader;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TimeShifterUtil;

/**
 * Reads the CSV file line by line and forms the object {@code <T>} 
 * based on the line data and the header of file (FIrst line of the file).
 */
public class CsvReader<T> extends FileReader<T> 
{
	private static final Logger LOGGER = LoggerFactory.getLogger(CsvReader.class);	
	private char delimiter = ',';
	private char quoteCharacter = '"';
	private List<String> columns;

	public void setDelimiter(char separator) 
	{
		this.delimiter = separator;
	}

	/**
	 * Reads the CSV file line by line and forms the object {@code <T>} 
	 * based on the line data and the header of file (FIrst line of the file).
	 */
	@SuppressWarnings("resource")
	public List<T> readData() 
	{
		//LOGGER.info("Reading data for " + fileName + " started.");
		FileInputStream fstream = null;
		List<T> list = new ArrayList<T>();
		long startTime = TimeShifterUtil.currentTimeMillis();
		BufferedReader br = null;
		DataInputStream in = null;
		try 
		{
			//HIX-71378 - added path validation check before accessing file system.
			if(GhixUtils.isGhixValidPath(fileName)){
				try{
				fstream = new FileInputStream(fileName);
				}catch(Exception e){
					LOGGER.info("ERROR while reading input file: "+fileName);
				}
			}else{
		    	throw new GIRuntimeException("Application trying to reach a blacklisted folder or filename or extension type.");
			}
	        in = new DataInputStream(fstream);
	        br = new BufferedReader ( new InputStreamReader (in));
	        String strLine ;
	        columns = new ArrayList<String>();
	        List<String> data = null;
	        boolean isHeader = true; 
	        int lineNumber = 0;
	        while( (strLine = br.readLine()) != null)
            {
	        	try
	        	{
        		data = doTokenize(strLine);
        		if(isHeader)
        		{
        			//LOGGER.info("Reading headers : " + fileName);
        			//LOGGER.debug("Columns : " + data);
        			columns.addAll(data);
	        		isHeader = false;
        		}
        		else
	        	{
        			lineNumber++;
        			//LOGGER.info("Reading data : " + fileName + " line : " +lineNumber);
        			//LOGGER.debug("Data : " + data);
        			T obj = mapper.mapData(data, columns);
	        		list.add(obj);
	        	}
	        	}
	        	catch(Exception e)
	        	{
	    			LOGGER.error("Error while reading CSV file Data of line : " +lineNumber);
	        	}
            }
        }
		catch (FileNotFoundException e) 
		{
			LOGGER.error("Error while reading CSV file Data. Description : " +e.getMessage());
		} 
		catch (IOException e)
		{
			LOGGER.error("Error while reading CSV file Data. Description : " +e.getMessage());
		}finally{
			IOUtils.closeQuietly(br);
			IOUtils.closeQuietly(in);
			IOUtils.closeQuietly(fstream);
		}
		
		long endTime = TimeShifterUtil.currentTimeMillis();
		if(LOGGER.isDebugEnabled()){
			LOGGER.debug("Reading data for file is completed.");
			LOGGER.debug("Total rows read " + list.size());
			LOGGER.debug("Processing data completed in  : " + (endTime - startTime));
		}	
		return list;
	}
	
	protected List<String> doTokenize(String line) 
	{
		List<String> tokens = new ArrayList<String>();
		char[] chars = line.toCharArray();
		boolean inQuoted = false;
		int lastCut = 0;
		int length = chars.length;

		for (int i = 0; i < length; i++) {

			char currentChar = chars[i];
			boolean isEnd = (i == (length - 1));
			boolean isDelimiterCharacter = isDelimiterCharacter(currentChar);
			if ((isDelimiterCharacter && !inQuoted) || isEnd) {
				int endPosition = (isEnd ? (length - lastCut) : (i - lastCut));

				if (isEnd && isDelimiterCharacter) 
				{
					endPosition--;
				}

				String value = new String(chars, lastCut, endPosition);
				if(value.startsWith(String.valueOf(quoteCharacter)) && value.endsWith(String.valueOf(quoteCharacter)) )
				{
					value = value.substring(1);
					value = value.substring(0, value.length() - 1);
				}
				
				tokens.add(value);

				if (isEnd && isDelimiterCharacter) 
				{
					tokens.add("");
				}

				lastCut = i + 1;
			}
			else if (isQuoteCharacter(currentChar)) 
			{
				inQuoted = !inQuoted;
			}
		}
		return tokens;
	}
	
	private boolean isDelimiterCharacter(char c) 
	{
		return c == this.delimiter;
	}


	protected boolean isQuoteCharacter(char c) 
	{
		return c == quoteCharacter;
	}
	
	public void setQuoteCharacter(char quoteCharacter)
	{
		this.quoteCharacter = quoteCharacter;
	}

	public List<String> getColumns() 
	{
		return columns;
	}
}
