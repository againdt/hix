//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.04.29 at 08:34:31 AM IST 
//


package com.getinsured.iex.hub.vci.gov.cms.hix._0_1.hix_core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.DateType;


/**
 * A relationship A data type for a relationship between an employer and an employee.
 * 
 * <p>Java class for PersonEmploymentAssociationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonEmploymentAssociationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}PersonEmploymentAssociationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}EmploymentOriginalHireDate" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonEmploymentAssociationType", propOrder = {
    "employmentOriginalHireDate"
})
public class PersonEmploymentAssociationType
    extends com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.PersonEmploymentAssociationType
{

    @XmlElement(name = "EmploymentOriginalHireDate")
    protected DateType employmentOriginalHireDate;

    /**
     * Gets the value of the employmentOriginalHireDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getEmploymentOriginalHireDate() {
        return employmentOriginalHireDate;
    }

    /**
     * Sets the value of the employmentOriginalHireDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setEmploymentOriginalHireDate(DateType value) {
        this.employmentOriginalHireDate = value;
    }

}
