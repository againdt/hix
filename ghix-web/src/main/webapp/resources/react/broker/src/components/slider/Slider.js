import React, { Fragment, useState, useEffect } from 'react';
import Switch from 'react-switch';

const Slider = props => {
  const {
    id,
    name,
    defaultSwitchValue,
    error,
    onChange,
    switchClasses,
    ariaText,
    disabled
  } = props;

  const [checked, setChecked] = useState(false);

  useEffect(() => {
    setChecked(defaultSwitchValue);
  }, [defaultSwitchValue]);

  useEffect(() => {
    setChecked(defaultSwitchValue);
  }, [error]);

  const attrs = {};

  if (onChange) {
    attrs.onClick = onChange.bind(this);
  }

  const handleChange = value => {
    if (!error) {
      setChecked(value);
      if (attrs.onClick) {
        attrs.onClick(value);
      }
    }
  };

  return (
    <Fragment>
      <label>
        <Switch
          id={id}
          name={name}
          className={switchClasses}
          onChange={handleChange}
          checked={checked}
          aria-labelledby={ariaText}
          width={65}
          disabled={disabled}
          offColor="#b5b5b5"
          onColor="#007fae"
          uncheckedIcon={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                fontSize: 15,
                paddingRight: 2,

              }}
            >
              OFF
            </div>
          }
          checkedIcon={
            <div
              style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                height: '100%',
                fontSize: 15,
                paddingRight: 2,
                color: '#fff',
              }}
            >
              ON
            </div>
          }
        />
      </label>
    </Fragment>
  );
};

export default Slider;
