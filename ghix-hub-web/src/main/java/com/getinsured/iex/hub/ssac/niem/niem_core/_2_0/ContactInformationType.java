
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;



/**
 * A data type for how to contact a person or an organization.
 * 
 * <p>Java class for ContactInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ContactInformationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactMeans" minOccurs="0"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}ContactEntity" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ContactInformationType", propOrder = {
    "contactMeans",
    "contactEntity"
})
public class ContactInformationType
    extends ComplexObjectType
{

    @XmlElementRef(name = "ContactMeans", namespace = "http://niem.gov/niem/niem-core/2.0", type = JAXBElement.class)
    protected JAXBElement<?> contactMeans;
    @XmlElement(name = "ContactEntity", nillable = true)
    protected EntityType contactEntity;

    /**
     * Gets the value of the contactMeans property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     
     */
    public JAXBElement<?> getContactMeans() {
        return contactMeans;
    }

    /**
     * Sets the value of the contactMeans property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link TelephoneNumberType }{@code >}
     *     
     */
    public void setContactMeans(JAXBElement<?> value) {
        this.contactMeans = value;
    }

    /**
     * Gets the value of the contactEntity property.
     * 
     * @return
     *     possible object is
     *     {@link EntityType }
     *     
     */
    public EntityType getContactEntity() {
        return contactEntity;
    }

    /**
     * Sets the value of the contactEntity property.
     * 
     * @param value
     *     allowed object is
     *     {@link EntityType }
     *     
     */
    public void setContactEntity(EntityType value) {
        this.contactEntity = value;
    }

}
