package com.getinsured.iex.ssap.financial;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.financial.type.TaxRelationship;

/**
 * Describes tax household composition used
 * in various financial API's.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-07
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TaxHouseholdComposition {
  /**
   * If person is married. {@code null} value is
   * default because UI in case of unknown status will
   * follow different logic.
   */
  private Boolean married = null;

  /**
   * If person is a tax filer.
   * {@code null} is needed because UI will follow
   * different logic if tax filer status is unknown.
   */
  private Boolean taxFiler = null;

  /**
   * If person claims any dependents.
   * {@code null} is needed because UI will follow
   * different logic if tax filer status is unknown.
   */
  private Boolean claimingDependents = null;

  /**
   * If person is tax dependent of someone else.
   * {@code null} is needed because UI will follow
   * different logic if tax filer status is unknown.
   */
  private Boolean taxDependent = null;

  /**
   * Current person is claimed by someone outside of current household.
   * Default is {@code false}
   */
  private boolean claimedOutsideHousehold;

  /**
   * If this person lives with spouse at the same address.
   * {@code null} is needed because UI will follow
   * different logic if tax filer status is unknown.
   */
  private Boolean livesWithSpouse = null;

  /**
   * This person is claimed by these person id.
   */
  private Set<Integer> claimerIds = new HashSet<>();

  /**
   * Tax filing status of this person.
   * @see TaxFilingStatus
   */
  private TaxFilingStatus taxFilingStatus = TaxFilingStatus.UNSPECIFIED;

  private TaxRelationship taxRelationship = TaxRelationship.UNSPECIFIED;

  public Boolean getMarried() {
    return married;
  }

  public void setMarried(Boolean married) {
    this.married = married;
  }

  public Boolean getTaxFiler() {
    return taxFiler;
  }

  public void setTaxFiler(Boolean taxFiler) {
    this.taxFiler = taxFiler;
  }

  public Boolean getClaimingDependents() {
    return claimingDependents;
  }

  public void setClaimingDependents(Boolean claimingDependents) {
    this.claimingDependents = claimingDependents;
  }

  public Boolean getTaxDependent() {
    return taxDependent;
  }

  public void setTaxDependent(Boolean taxDependent) {
    this.taxDependent = taxDependent;
  }

  public boolean isClaimedOutsideHousehold() {
    return claimedOutsideHousehold;
  }

  public void setClaimedOutsideHousehold(boolean claimedOutsideHousehold) {
    this.claimedOutsideHousehold = claimedOutsideHousehold;
  }

  public Boolean getLivesWithSpouse() {
    return livesWithSpouse;
  }

  public void setLivesWithSpouse(Boolean livesWithSpouse) {
    this.livesWithSpouse = livesWithSpouse;
  }

  public TaxFilingStatus getTaxFilingStatus() {
    return taxFilingStatus;
  }

  public void setTaxFilingStatus(TaxFilingStatus taxFilingStatus) {
    this.taxFilingStatus = taxFilingStatus;
  }

  public TaxRelationship getTaxRelationship() {
    return taxRelationship;
  }

  public void setTaxRelationship(TaxRelationship taxRelationship) {
    this.taxRelationship = taxRelationship;
  }

  public Set<Integer> getClaimerIds() {
    return claimerIds;
  }

  public void setClaimerIds(Set<Integer> claimerIds) {
    this.claimerIds = claimerIds;
  }
}
