package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.RedetermineEligibilityService;
import com.getinsured.hix.batch.ssap.renewal.util.RedetermineEligibilityPartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.iex.ssap.model.SsapApplication;

public class RedetermineEligibilityReader implements ItemReader<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedetermineEligibilityReader.class);
	private List<SsapApplication> readerSsapApplicationList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private RedetermineEligibilityService redetermineEligibilityService;
	private RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : before Step execution for Reader ");
		
		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.info("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			List<Long> readerSsapApplicationIdList = null;
			if (redetermineEligibilityPartitionerParams != null && CollectionUtils.isNotEmpty(redetermineEligibilityPartitionerParams.getSsapApplicationIdList())) {

				if (startIndex < redetermineEligibilityPartitionerParams.getSsapApplicationIdList().size()
						&& endIndex <= redetermineEligibilityPartitionerParams.getSsapApplicationIdList().size()) {
					readerSsapApplicationIdList = new CopyOnWriteArrayList<Long>(redetermineEligibilityPartitionerParams.getSsapApplicationIdList().subList(startIndex, endIndex));
				}
			}
			
			if(CollectionUtils.isNotEmpty(readerSsapApplicationIdList))
			{
				readerSsapApplicationList = new CopyOnWriteArrayList<SsapApplication>();
				List<List<Long>>  chunkedList = RenewalUtils.chunkList(readerSsapApplicationIdList,RenewalUtils.INNER_QUERY_CHUNK_SIZE);
				
				for(List<Long> list : chunkedList)
				{
					List<SsapApplication> applicationList = redetermineEligibilityService.getSsapApplicationListByIds(list);
					readerSsapApplicationList.addAll(new CopyOnWriteArrayList<SsapApplication>(applicationList) );
				}
			}
		}
	}

	@Override
	public SsapApplication read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		SsapApplication readerSsapApplication = null;

		if (CollectionUtils.isNotEmpty(readerSsapApplicationList)) {

			if (loopCount < readerSsapApplicationList.size()) {
				loopCount++;
				readerSsapApplication = readerSsapApplicationList.get(loopCount - 1);
			}
		}
		else{
			LOGGER.error("No SsapApplications found in RedetermineEligibilityReader.read()");
		}
		
		return readerSsapApplication;
	}

	public RedetermineEligibilityService getRedetermineEligibilityService() {
		return redetermineEligibilityService;
	}

	public void setRedetermineEligibilityService(RedetermineEligibilityService redetermineEligibilityService) {
		this.redetermineEligibilityService = redetermineEligibilityService;
	}

	public RedetermineEligibilityPartitionerParams getRedetermineEligibilityPartitionerParams() {
		return redetermineEligibilityPartitionerParams;
	}

	public void setRedetermineEligibilityPartitionerParams(
			RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams) {
		this.redetermineEligibilityPartitionerParams = redetermineEligibilityPartitionerParams;
	}

	
}

