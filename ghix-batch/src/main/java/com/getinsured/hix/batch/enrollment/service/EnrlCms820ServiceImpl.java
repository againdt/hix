/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

import java.beans.PropertyDescriptor;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.batch.enrollment.util.EnrlCms820EnrollmentProcessingThread;
import com.getinsured.hix.batch.enrollment.util.EnrlCms820ProcessingThread;
import com.getinsured.hix.batch.enrollment.util.EnrlCms820SummaryProcessingThread;
import com.getinsured.hix.dto.enrollment.EnrlCms820DataCsvDTO;
import com.getinsured.hix.enrollment.repository.IEnrlCms820DataRepository;
import com.getinsured.hix.enrollment.repository.IEnrlCms820PremiumRepository;
import com.getinsured.hix.enrollment.repository.IEnrlCms820SummaryRepository;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.EnrlCms820Data;
import com.getinsured.hix.model.enrollment.EnrlCms820Premium;
import com.getinsured.hix.model.enrollment.EnrlCms820Summary;
import com.getinsured.hix.model.enrollment.EnrlCms820Summary.SummaryStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;
import com.opencsv.CSVReader;
import com.opencsv.bean.ColumnPositionMappingStrategy;
import com.opencsv.bean.CsvToBean;

/**
 * @author negi_s
 *
 */
@Service("enrlCms820Service")
@Transactional
public class EnrlCms820ServiceImpl implements EnrlCms820Service {

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlCms820ServiceImpl.class);

	@Autowired(required = true)
	private IEnrlCms820DataRepository enrlCms820DataRepository;

	@Autowired(required = true)
	private IEnrlCms820SummaryRepository enrlCms820SummaryRepository;

	@Autowired(required = true)
	private IEnrlCms820PremiumRepository enrlCms820PremiumRepository;

	public static final String WIP_FOLDER_NAME="WIP_820";

	private final Long defaultErrorKey = new Long(-1);

	private final String[] CMS_DATA_COLUMN = new String[] { "hiosIssuerId",
			"issuerAptcTotal", "issuerCsrTotal", "issuerUfTotal", "lastName",
			"firstName", "middleName", "namePrefix",
			"nameSuffix", "exchangeAssignedSubId", "exchangeAssignedQhpId", "exchangeAssignedPolicyId",
			"issuerAssignedPolicyId", "issuerAssignedSubId", "policyTotalPremiumAmount",
			"exchangePaymentType", "paymentAmount",
			"exchangeRelatedReportType", "exchangeReportDocCntrNum",
			"coveragePeriodStartDate", "coveragePeriodEndDate",
	};

	public static enum PAYMENT_TYPE {
		APTC, CSR, APTCMADJ, CSRMADJ
	}

	@Override
	public void loadCms820File(JobService jobService, StepExecution stepExecution) throws IOException, GIException, InterruptedException {
		Long jobId = stepExecution.getJobExecution().getJobId();
		LOGGER.info("Start CMS 820 file processing :: " + jobId+ " Start Time :: " + new Date());
		String wipFolderPath = moveAll820FilesToWipFolder();
		long fileCount = 0;
		boolean load820FilesInParallel = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
				EnrollmentConfiguration.EnrollmentConfigurationEnum.LOAD_CMS_820_FILE_IN_PARALLEL));
		ExecutorService executor = null;
		if(null != wipFolderPath){
			fileCount = Files.list(Paths.get(wipFolderPath)).count();
			if(fileCount > 1 && load820FilesInParallel){
				executor = Executors.newFixedThreadPool(EnrollmentConstants.FIVE);
			}else{
				executor = Executors.newFixedThreadPool(EnrollmentConstants.ONE);
			}
			File[] csvFiles = EnrollmentUtils.getFilesInAFolder(wipFolderPath, EnrollmentConstants.FILE_TYPE_OUT);
			File archivePath = createArchiveFolder(jobId, EnrollmentConstants.ARCHIVE_FOLDER);
			List<Callable<String>> taskList = new ArrayList<>();

			for (int index = 0; index < csvFiles.length; index++){
				taskList.add(new EnrlCms820ProcessingThread(csvFiles[index].getPath(), jobId, jobService, stepExecution, archivePath, this));
			}
			executor.invokeAll(taskList)
			.stream()
			.map(future -> { 
				try{
					return future.get();
				}catch(Exception ex){
					LOGGER.error("Exception occurred in processCms820File: ", ex);
				}
				return null;
			} )
			.filter(p -> p != null)
			.collect(Collectors.toCollection(ArrayList<String> :: new));
			executor.shutdown();
		}else{
			LOGGER.info("No CMS 820 Files to load.");
		}
	}
	
	/**
	 * Moves CMS 820 files to the WIP folder.
	 * @return String WIP folder path
	 */
	private String moveAll820FilesToWipFolder() {
		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN);
		String inboundDir = reconciliationFileNameBuilder.toString();
		File dir = new File(inboundDir);
		String wipFolderPath = inboundDir + File.separator + WIP_FOLDER_NAME;
		File wipFolder = new File(wipFolderPath);
		boolean wipPathValid = true;

		if (dir != null && !(wipFolder.exists())) {
			if(!wipFolder.mkdirs()){
				wipPathValid = false;
			}
		}
		if(wipPathValid){
			//Move files from inboundDir to WIP folder
			if (dir.isDirectory()) {
				File[] files = dir.listFiles(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(EnrollmentConstants.FILE_TYPE_OUT) && name.contains(CmsSftpServiceImpl.CMS_INBOUND_FUNC_CODE.SI820.toString());
					}
				});
				for (File file : files) {
					LOGGER.info("File received :: "+ file.getName());
					if(file.isFile()){
						File wipFile = new File(wipFolderPath + File.separator+ file.getName());
						file.renameTo(wipFile);
					}
				}
			}
			if(wipFolder.list()!= null && wipFolder.list().length > 0){
				return wipFolderPath;
			}else{
				return null;
			}
		}
		return null;
	}
	
	/**
	 * Creates archive folder.
	 * @param jobId
	 * @param directoryName
	 * @return String Archive folder path
	 */
	private File createArchiveFolder(Long jobId, String directoryName) {
		StringBuilder reconciliationFileNameBuilder =  EnrollmentUtils.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.CMS.toString(),
				EnrollmentConstants.TRANSFER_DIRECTION_IN);

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		String batchDate = sdf.format(new Date()).replace('|', '-').replace(':', '-')+"-"+jobId;

		String inboundDir = reconciliationFileNameBuilder.toString();
		File dir = new File(inboundDir);

		String archiveFolderPath = inboundDir + File.separator + directoryName + File.separator + "CMS820_"+ batchDate;
		File archiveFolder = new File(archiveFolderPath);
		boolean archivePathValid = true;
		if (dir != null && !(archiveFolder.exists())) {
			if (!archiveFolder.mkdirs()) {
				archivePathValid = false;
			}
		}
		if(archivePathValid){
			return archiveFolder;
		}else{
			return null;
		}
	}

	@Override
	public String populateCms820Data(String fileName, Long batchExecutionId, JobService jobService, StepExecution stepExecution, File archivePath) throws Exception {

		EnrlCms820Summary summary =  createSummary(fileName, batchExecutionId);
		Integer counter = 0;

		Map<Long, String> errorMap = new ConcurrentHashMap<>();

		String batchJobStatus=null;
		if(jobService != null){
			batchJobStatus = jobService.getJobExecution(stepExecution.getJobExecutionId()).getStatus().name();
		}

		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
				batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			updateSummaryForJobFailure(summary, "Job Stop by using JobKill Functionality  [ JobExecutionId : "+ stepExecution.getJobExecutionId()+" ] ");
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}

		List<EnrlCms820DataCsvDTO> csvLineList = new ArrayList<>();
		try {
			if(summary.getId() != 0 ){
				counter = Files.lines(Paths.get(fileName)) .map(line -> line.trim())
						.filter(line -> line.length() > 0)
						.map(line -> convertToBeanAndSave(line, summary, csvLineList, errorMap))
						.reduce(0, Integer::sum);

				counter = counter + saveEnrlCms820Data(csvLineList, summary.getId(), errorMap);
				moveBadEnrollmentFromDataTable(summary.getId(), errorMap);
				updateSummary(summary, errorMap);
			}
		} catch (IOException e) {
			LOGGER.error("EnrlCms820Service SERVICE : IOException while reading : "+fileName);
			updateSummaryForJobFailure(summary,"IOException while reading : "+fileName);
			throw new GIException("EnrlCms820Service SERVICE : IOException while reading + "+fileName, e);
		}
		moveFileToArchiveOrFailure(archivePath, fileName);
		return Integer.toString(summary.getId());
	}

	/**
	 * Updates summary table with data from the CSV file.
	 * @param summary
	 * @param errorMap
	 */
	private void updateSummary(EnrlCms820Summary summary, Map<Long, String> errorMap) {
		//Get information from data table
		List<EnrlCms820Data> data = enrlCms820DataRepository.find820DataBySummaryId(summary.getId(), new PageRequest(0, 1));
		if(null != data && !data.isEmpty()) {
			summary.setTotalIssuerAptc(data.get(0).getIssuerAptcTotal());
			summary.setTotalIssuerCsr(data.get(0).getIssuerCsrTotal());
			summary.setTotalIssuerUf(data.get(0).getIssuerUfTotal());
			summary.setHiosIssuerId(data.get(0).getHiosIssuerId());
			//Processing Month
			Calendar time = Calendar.getInstance();
			time.setTime(new Date());
			summary.setProcessingMonth(time.get(Calendar.MONTH) + 1);
		}
		summary.setStatus(SummaryStatus.LOADED.toString());
		if(!errorMap.isEmpty()) {
			summary.setErrorData(errorMap.toString());
		}
		enrlCms820SummaryRepository.saveAndFlush(summary);
	}
	
	/**
	 * Purges bad or incomplete enrollment CMS 820 data from the data table.
	 * @param summaryId
	 * @param errorMap
	 */
	private void moveBadEnrollmentFromDataTable(Integer summaryId, Map<Long, String> errorMap) {
		StringBuilder errorBuilder = null;
		for(Long exchgAssignedPolicyId : errorMap.keySet()) {
			if( defaultErrorKey != exchgAssignedPolicyId) {
				List<EnrlCms820Data> errorDataList = enrlCms820DataRepository.findByExchgAssignedPolicyId(exchgAssignedPolicyId, summaryId);
				errorBuilder = new StringBuilder();
				for(EnrlCms820Data data : errorDataList) {
					errorBuilder.append(data.toCsv('|')).append("\n");
				}
				errorBuilder.append(errorMap.get(exchgAssignedPolicyId));
				errorMap.put(exchgAssignedPolicyId, errorBuilder.toString());
				enrlCms820DataRepository.delete(errorDataList);
			}
		}
	}

	/**
	 * Updates summary record status incase of job failure.
	 * @param summary
	 * @param errorMessage
	 */
	private void updateSummaryForJobFailure(EnrlCms820Summary summary, String errorMessage) {
		summary.setErrorData(errorMessage);
		summary.setStatus(SummaryStatus.LOADING_ERROR.toString());
		enrlCms820SummaryRepository.saveAndFlush(summary);
	}
	
	/**
	 * Creates summary record for the CMS 820 file received.
	 * @param fileName
	 * @param batchExecutionId
	 * @return {@link EnrlCms820Summary}
	 */
	private EnrlCms820Summary createSummary(String fileName, Long batchExecutionId) {
		EnrlCms820Summary summary = new EnrlCms820Summary();
		summary.setFileName(Paths.get(fileName).getFileName().toString());
		summary.setStatus(SummaryStatus.RECEIVED.toString());
		summary = enrlCms820SummaryRepository.saveAndFlush(summary);
		return summary;
	}

	/**
	 * Converts a CSV line to a bean and inserts the record in the 820 data table
	 * @param line
	 * @param fileId
	 * @param csvLineList
	 * @param errorMap
	 * @return
	 */
	private int convertToBeanAndSave(String line, EnrlCms820Summary summary,List<EnrlCms820DataCsvDTO> csvLineList, Map<Long, String> errorMap ) {
		List<EnrlCms820DataCsvDTO> cms820DataList = null;
		int internalCounter = 0;
		CsvToBean<EnrlCms820DataCsvDTO> csvToBeanDetail = new CsvToBean<EnrlCms820DataCsvDTO>() {
			protected Object convertValue(String value, PropertyDescriptor prop) throws InstantiationException, IllegalAccessException {
				if(StringUtils.isEmpty(value)) {
					value = null;
				}else {
					value = value.trim();
				}
				return super.convertValue(value, prop);
			}
		};;

		if(null != line && NumberUtils.isNumber(line.substring(0, line.indexOf('|')))){
			cms820DataList =  csvToBeanDetail.parse(getCms820DataColumnMapping(), new CSVReader(new StringReader(line), '|'));

			if(null != cms820DataList && !cms820DataList.isEmpty()){
				if(validateCms820DataCsv(cms820DataList.get(0)) ){
					csvLineList.add(cms820DataList.get(0));
				}else{
					if(null != cms820DataList.get(0) && NumberUtils.isNumber(cms820DataList.get(0).getExchangeAssignedPolicyId())) {
						Long enrollmentId = Long.parseLong(cms820DataList.get(0).getExchangeAssignedPolicyId());
						if(errorMap.containsKey(enrollmentId)) {
							errorMap.put(enrollmentId, errorMap.get(enrollmentId)+"\n"+line);
						}else {
							errorMap.put(Long.parseLong(cms820DataList.get(0).getExchangeAssignedPolicyId()), line);
						}
					}else {
						if(errorMap.containsKey(defaultErrorKey)) {
							errorMap.put(defaultErrorKey, errorMap.get(defaultErrorKey)+"\n"+line);
						}else {
							errorMap.put(defaultErrorKey, line);
						}
					}
					LOGGER.error("Invalid record :" + line);
					return ++internalCounter;
				}

				if (csvLineList.size() >= 1000) {
					List<EnrlCms820DataCsvDTO> csvLineSaveList = csvLineList.stream().collect(Collectors.toList());
					internalCounter = saveEnrlCms820Data(csvLineSaveList,summary.getId(),errorMap);
					csvLineList.clear();
					return internalCounter;
				}
			}else{
				if(errorMap.containsKey(defaultErrorKey)) {
					errorMap.put(defaultErrorKey, errorMap.get(defaultErrorKey)+"\n"+line);
				}else {
					errorMap.put(defaultErrorKey, line);
				}
				LOGGER.error("Parsing error at line ::" + line);
				return ++internalCounter;
			}
		}else if(null != line){
			String[] header = line.split("[|]");
			if(header.length > 1) {
				String type = header[EnrollmentConstants.ZERO];
				String value =  header[EnrollmentConstants.ONE];
				switch(type) {
				case "Policy-Based Transition Month" :
					LOGGER.info("Policy-Based Transition Month :: " + value);
					if(DateUtil.isValidDate(value, "yyyyMM")) {
						summary.setTransitionYearMonth(value);
						summary.setCoverageYear(Integer.valueOf(value.substring(EnrollmentConstants.ZERO, EnrollmentConstants.FOUR)));
					}
					break;
				case "Total Payment($)" :
					LOGGER.info("Total Payment($)" + value);
					summary.setTotalPayment(getBigDecimalValue(value));
					break;
				case "Payment Method Code" :
					LOGGER.info("Payment Method Code" + value);
					summary.setPaymentMethodCode(StringUtils.isNotBlank(value) && value.length() < EnrollmentConstants.FIVE ? value : null);
					break;
				case "State":
					LOGGER.info("State" + value);
					summary.setState(StringUtils.isNotBlank(value) && value.length() == EnrollmentConstants.TWO ? value : null);
					break;
				case "Transaction Set Control Number" :
					LOGGER.info("Transaction Set Control Number" + value);
					summary.setTxnControlNumber(value);
					break;
				case "Run Date":
					LOGGER.info("Run Date" + value);
					summary.setRptGenerationDate(DateUtil.StringToDate(value, GhixConstants.DISPLAY_DATE_FORMAT1));
					break;
				default: 
					LOGGER.info("Skipping header information");
				}
			}
		}
		return internalCounter;
	}
	
	/**
	 * Validates the {@link EnrlCms820DataCsvDTO} record formed
	 * @param enrlCms820DataCsvDTO
	 * @return {@link boolean}
	 */
	private boolean validateCms820DataCsv(EnrlCms820DataCsvDTO enrlCms820DataCsvDTO) {
		if(null != enrlCms820DataCsvDTO) {
			if(//StringUtils.isNotBlank(enrlCms820DataCsvDTO.getExchangeAssignedPolicyId()) &&
					DateUtil.isValidDate(enrlCms820DataCsvDTO.getCoveragePeriodStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD)
					&& DateUtil.isValidDate(enrlCms820DataCsvDTO.getCoveragePeriodEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD)
					&& StringUtils.isNotBlank(enrlCms820DataCsvDTO.getHiosIssuerId())
					&& StringUtils.isNotBlank(enrlCms820DataCsvDTO.getExchangePaymentType())
					&& NumberUtils.isNumber(enrlCms820DataCsvDTO.getPaymentAmount())
					) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Saves 820 data records in the database.
	 * @param csvLineSaveList
	 * @param summaryId
	 * @param errorMap
	 * @return
	 */
	private int saveEnrlCms820Data(List<EnrlCms820DataCsvDTO> csvLineSaveList, Integer summaryId, Map<Long, String> errorMap) {
		List<EnrlCms820Data> enrlCms820DataList = new ArrayList<>();
		EnrlCms820Data enrlCms820Data = null;
		for(EnrlCms820DataCsvDTO dto : csvLineSaveList) {
			try {
				enrlCms820Data = new EnrlCms820Data();
				enrlCms820Data.setSummaryId(summaryId);
				enrlCms820Data.setExchangeAssignedPolicyId(getLongValue(dto.getExchangeAssignedPolicyId()));
				enrlCms820Data.setHiosIssuerId(dto.getHiosIssuerId());
				enrlCms820Data.setIssuerAptcTotal(getBigDecimalValue(dto.getIssuerAptcTotal()));
				enrlCms820Data.setIssuerCsrTotal(getBigDecimalValue(dto.getIssuerCsrTotal()));
				enrlCms820Data.setIssuerUfTotal(getBigDecimalValue(dto.getIssuerUfTotal()));
				enrlCms820Data.setLastName(dto.getLastName());
				enrlCms820Data.setFirstName(dto.getFirstName());
				enrlCms820Data.setMiddleName(dto.getMiddleName());
				enrlCms820Data.setNamePrefix(dto.getNamePrefix());
				enrlCms820Data.setNameSuffix(dto.getNameSuffix());
				enrlCms820Data.setExchangeAssignedSubId(dto.getExchangeAssignedSubId());
				enrlCms820Data.setExchangeAssignedQhpId(dto.getExchangeAssignedQhpId());
				enrlCms820Data.setIssuerAssignedPolicyId(dto.getIssuerAssignedPolicyId());
				enrlCms820Data.setIssuerAssignedSubId(dto.getIssuerAssignedSubId());
				enrlCms820Data.setPolicyTotalPremiumAmount(getBigDecimalValue(dto.getPolicyTotalPremiumAmount()));
				enrlCms820Data.setExchangePaymentType(dto.getExchangePaymentType());
				enrlCms820Data.setPaymentAmount(getBigDecimalValue(dto.getPaymentAmount()));
				enrlCms820Data.setExchangeRelatedReportType(dto.getExchangeRelatedReportType());
				enrlCms820Data.setExchangeReportDocCntrNum(dto.getExchangeReportDocCntrNum());
				enrlCms820Data.setCoveragePeriodStartDate(DateUtil.StringToDate(dto.getCoveragePeriodStartDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
				enrlCms820Data.setCoveragePeriodEndDate(DateUtil.StringToDate(dto.getCoveragePeriodEndDate(), EnrollmentConstants.DATE_FORMAT_YYYYMMDD));
				enrlCms820DataList.add(enrlCms820Data);
			}catch(Exception e) {
				LOGGER.error("Error in parsing data" , e);
				if(null != enrlCms820Data.getExchangeAssignedPolicyId()) {
					if(errorMap.containsKey(enrlCms820Data.getExchangeAssignedPolicyId())) {
						errorMap.put(enrlCms820Data.getExchangeAssignedPolicyId(), errorMap.get(enrlCms820Data.getExchangeAssignedPolicyId())+"\n"+dto.toCsv('|'));
					}else {
						errorMap.put(enrlCms820Data.getExchangeAssignedPolicyId(), dto.toCsv('|'));
					}
				}else {
					if(errorMap.containsKey(defaultErrorKey)) {
						errorMap.put(defaultErrorKey, errorMap.get(defaultErrorKey)+"\n"+dto.toCsv('|'));
					}
					else {
						errorMap.put(defaultErrorKey,dto.toCsv('|'));
					}
				}
			}
		}
		enrlCms820DataList.stream().forEach(new Consumer<EnrlCms820Data>(){
			@Override
			public void accept(EnrlCms820Data enrlCms820Data) {
				try{
					enrlCms820DataRepository.saveAndFlush(enrlCms820Data);
				}catch(Exception ex){
					LOGGER.error("Error in saving data, data ID :"+ enrlCms820Data.getId() , ex);
				}
			}
		});
		return csvLineSaveList.size();
	}

	private Long getLongValue(String str) {
		return StringUtils.isNoneBlank(str) ? Long.parseLong(str) : null;
	}

	private BigDecimal getBigDecimalValue(String str) {
		return NumberUtils.isNumber(str)? new BigDecimal(str) : null;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private ColumnPositionMappingStrategy<EnrlCms820DataCsvDTO> getCms820DataColumnMapping() {
		ColumnPositionMappingStrategy<EnrlCms820DataCsvDTO> strategy = new ColumnPositionMappingStrategy();
		strategy.setType(EnrlCms820DataCsvDTO.class);
		strategy.setColumnMapping(CMS_DATA_COLUMN);
		return strategy;
	}
	
	/**
	 * Moves CMS 820 files in the archive location.
	 * @param filePath
	 * @param fileName
	 */
	private void moveFileToArchiveOrFailure(File filePath, String fileName) {
		if(filePath != null){
			try {
				EnrollmentUtils.moveFile(Paths.get(fileName).getParent().toString(), filePath.toString(), new File(fileName));
			}catch (GIException e) {
				LOGGER.error("Exception occurred while Moving CMS820 files: ", e);
			}
		}
	}

	@Override
	public void processCms820(JobService jobService, StepExecution stepExecution) throws Exception {
		//Get summary IDs to be processed
		List<Integer> summaryIdList = enrlCms820SummaryRepository.getCms820ForProcessing();
		LOGGER.debug("Summaries picked up fro processing :: " + summaryIdList);
		if(null != summaryIdList && !summaryIdList.isEmpty()) {
			boolean process820FilesInParallel = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(
					EnrollmentConfiguration.EnrollmentConfigurationEnum.PROCESS_CMS_820_FILE_IN_PARALLEL)); 
			ExecutorService executor = null;
			if(summaryIdList.size() > 1 && process820FilesInParallel){
				executor = Executors.newFixedThreadPool(EnrollmentConstants.FIVE);
			}else{
				executor = Executors.newFixedThreadPool(EnrollmentConstants.ONE);
			}
			String batchJobStatus=null;
			if(jobService != null){
				batchJobStatus = jobService.getJobExecution(stepExecution.getJobExecutionId()).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
					batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			List<Callable<String>> taskList = new ArrayList<>();
			for(Integer summaryId : summaryIdList) {
				taskList.add(new EnrlCms820SummaryProcessingThread(summaryId, jobService, stepExecution, this));
			}
			executor.invokeAll(taskList)
			.stream()
			.map(future -> { 
				try{
					return future.get();
				}catch(Exception ex){
					LOGGER.error("Exception occurred in processCms820: ", ex);
				}
				return null;
			} )
			.filter(p -> p != null)
			.collect(Collectors.toCollection(ArrayList<String> :: new));
			executor.shutdown();
		}else {
			LOGGER.debug("No CMS 820 files to be processed");
		}
	}

	@Override
	public String processLoadedData(Integer summaryId, JobService jobService, StepExecution stepExecution) throws Exception {
		//Get distinct enrollment IDs
		LOGGER.info("Processing summary :: " + summaryId );
		String batchJobStatus=null;
		if(jobService != null){
			batchJobStatus = jobService.getJobExecution(stepExecution.getJobExecutionId()).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||
				batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		enrlCms820SummaryRepository.updateSummaryStatus(summaryId, SummaryStatus.PROCESSING.toString());
		List<Long> enrollmentIdList = enrlCms820DataRepository.getDistinctEnrollmentIds(summaryId);
		int threadPoolSize = 5;
		String enrlCms820ThreadPool = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_CMS_820_THREAD_POOL);
		if(enrlCms820ThreadPool!=null && !"".equals(enrlCms820ThreadPool.trim())){
			threadPoolSize= Integer.parseInt(enrlCms820ThreadPool);
		}
		Integer maxIdPerSublist = 1000;
		String maxIdPerSublistStr = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENTS_PER_CMS_820_THREAD);
		if(null != maxIdPerSublistStr && NumberUtils.isNumber(maxIdPerSublistStr.trim())){
			maxIdPerSublist=Integer.valueOf(maxIdPerSublistStr.trim());
		}
	
		List<List<Long>> enrollmentChunkList = getSubList(enrollmentIdList, maxIdPerSublist);
		List<Callable<Integer>> taskList = new ArrayList<>();
		for(List<Long> chunk : enrollmentChunkList) {
			taskList.add(new EnrlCms820EnrollmentProcessingThread(chunk, summaryId, jobService, stepExecution, this));
		}
		
		ExecutorService executor = Executors.newFixedThreadPool(threadPoolSize);
		executor.invokeAll(taskList)
		.stream()
		.map(future -> { 
			try{
				return future.get();
			}catch(Exception ex){
				LOGGER.error("Exception occurred in processLoadedData: ", ex);
			}
			return null;
		} )
		.filter(p -> p != null)
		.collect(Collectors.toCollection(ArrayList<Integer> :: new));
		executor.shutdown();
		ExecutorService summaryUpdateExecService = Executors.newFixedThreadPool(EnrollmentConstants.ONE);
		try {
			summaryUpdateExecService.submit(new  Callable<Integer>() {
				@Override
				public Integer call() throws Exception {
					EnrlCms820Summary summary = enrlCms820SummaryRepository.getCms820SummaryBySummaryId(summaryId);
					summary.setStatus(SummaryStatus.COMPLETED.toString());
					enrlCms820SummaryRepository.saveAndFlush(summary);
					return 1;
				}
			});		
		}catch(Exception e) {
			LOGGER.error("Error in updating summary table", e);
		}
		summaryUpdateExecService.shutdown();
		summaryUpdateExecService.awaitTermination(EnrollmentConstants.ONE, TimeUnit.HOURS);
		return "1";
	}

	@Override
	public Integer processEnrollmentIdList(List<Long> enrollmentIdList, Integer summaryId, JobService jobService, StepExecution stepExecution) {
		for(Long enrollmentId : enrollmentIdList) {
			LOGGER.debug("Processing enrollment ID : " + enrollmentId);
			List<EnrlCms820Data> cms820DataList = enrlCms820DataRepository.findByExchgAssignedPolicyId(enrollmentId, summaryId);
			///Check existence of Premium info
			List<EnrlCms820Premium> cms820PremiumList = enrlCms820PremiumRepository.findByEnrollmentId(enrollmentId);
			if(null == cms820PremiumList || cms820PremiumList.isEmpty()) {
				cms820PremiumList = createEmptyCms820PremiumStructure(enrollmentId);
			}
			Map<Integer, EnrlCms820Premium> premiumMap = getPremiumMonthMap(cms820PremiumList);
			Calendar periodStart = Calendar.getInstance();
			Calendar periodEnd = Calendar.getInstance();
			BigDecimal aptcAmt = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
			BigDecimal csrAmt = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
			try {
				for(EnrlCms820Data rowData : cms820DataList) {
					periodStart.setTime(rowData.getCoveragePeriodStartDate());
					periodEnd.setTime(rowData.getCoveragePeriodEndDate());
					for(int i = periodStart.get(Calendar.MONTH) + 1; i<= periodEnd.get(Calendar.MONTH) + 1; i++) {
						EnrlCms820Premium ep = premiumMap.get(i);
						if(PAYMENT_TYPE.APTC.toString().equalsIgnoreCase(rowData.getExchangePaymentType())){
							aptcAmt = rowData.getPaymentAmount();
							if(null != ep.getAptcAmt()) {
								aptcAmt =   ep.getAptcAmt().add(rowData.getPaymentAmount());
							}
							ep.setAptcAmt(aptcAmt);
						}else if(PAYMENT_TYPE.CSR.toString().equalsIgnoreCase(rowData.getExchangePaymentType())) {
							csrAmt = rowData.getPaymentAmount();
							if(null != ep.getCsrAmt()) {
								csrAmt = ep.getCsrAmt().add(rowData.getPaymentAmount());
							}
							ep.setCsrAmt(csrAmt);
						}
					}
				}
			}catch(Exception e){
				LOGGER.error("Exception in processing enrollment ID : " + enrollmentId, e);
			}
			try {
				enrlCms820PremiumRepository.save(cms820PremiumList);	
			}catch(Exception e) {
				LOGGER.error("Error saving premium list", e);
			}
		}
		return enrollmentIdList.size();
	}

	private Map<Integer, EnrlCms820Premium> getPremiumMonthMap(List<EnrlCms820Premium> cms820PremiumList) {
		Map<Integer, EnrlCms820Premium> premiumMap = new ConcurrentHashMap<>();
		for(EnrlCms820Premium ep : cms820PremiumList) {
			premiumMap.put(ep.getMonth(), ep);
		}
		return premiumMap;
	}

	private List<EnrlCms820Premium> createEmptyCms820PremiumStructure(Long enrollmentId) {
		List<EnrlCms820Premium> cms820PremiumList = new ArrayList<>();
		EnrlCms820Premium premium = null;
		for(int i = 1 ; i<=12 ; i++) {
			premium = new EnrlCms820Premium();
			premium.setMonth(i);
			premium.setEnrollmentId(enrollmentId);
			cms820PremiumList.add(premium);
		}
		return cms820PremiumList;
	}

	private List<List<Long>> getSubList(List<Long> enrollmentIdList, int maxIdPerSublist) {
		List<List<Long>> subListList = new ArrayList<>();
		final int n = enrollmentIdList.size();

		for (int i = 0; i < n; i += maxIdPerSublist) {
			subListList.add(new ArrayList<Long>(enrollmentIdList.subList(i, Math.min(n, i + maxIdPerSublist))));
		}
		return subListList;
	}
}
