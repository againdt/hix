package com.getinsured.iex.hub.hubc.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.ObjectFactory;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.ResponseMetadataType;


/**
 * Wrapper com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.hubc.ResponseMetadataType
 * 
 * @author Nikhil Talreja
 * @since 05-Jun-2014
 * 
 */
public class ResponseMetadataWrapper implements JSONAware{
	
	private ResponseMetadataType responseMetadataType;
	
    public ResponseMetadataWrapper(ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	ObjectFactory factory = new ObjectFactory();
    	this.responseMetadataType = factory.createResponseMetadataType();
    	this.responseMetadataType.setResponseCode(responseMetadataType.getResponseCode());
    	this.responseMetadataType.setResponseDescriptionText(responseMetadataType.getResponseDescriptionText());
    }
    
    public ResponseMetadataType getResponseMetadataType(){
    	return responseMetadataType;
    }
    
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseMetadataType.getResponseCode());
		obj.put("ResponseDescriptionText", this.responseMetadataType.getResponseDescriptionText());
		return obj.toJSONString();
	}
	
}
