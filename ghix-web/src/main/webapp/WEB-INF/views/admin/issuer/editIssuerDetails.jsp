<!-- 
 @author raja : editIssuerDetails.jsp
 @UserStory : HIX-1807
 @Jira-id : HIX-4190
 -->
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" ></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/manageissuer"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
        	<li><spring:message  code="pgheader.issuerDetails"/></li>
        </ul><!--page-breadcrumb ends-->
	</div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="issuerDetails"/>
				</jsp:include>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.issuerInfo"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">
				<form class="form-horizontal" id="frmEditIssuerDetails" name="frmEditIssuerDetails" action="<c:url value="/admin/issuer/details/save" />" method="POST">
					<df:csrfToken/>
					<input type="hidden" id="id" name="id" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
					<div class="profile">
						<table class="table table-border-none">	
							<tbody>						
								<tr>
									<td class="txt-right span4"><label for="name"><spring:message  code="label.name"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
									<td>
										<input type="text" name="name" id="name" class="input-large" value="${issuerObj.name}" maxlength="75"/>
	                            		<div id="name_error" class=""></div>
									</td>
								</tr>	
								<tr>
									<td class="txt-right span4"><label for="shortName"><spring:message  code="label.shortName"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
									<td>
										<input type="text" name="shortName" id="shortName" class="input-large" value="${issuerObj.shortName}" maxlength="50"/>
	                            		<div id="shortName_error" class=""></div>
									</td>
								</tr>		
										<tr>
									<td class="txt-right"><label for="naicCompanyCode"><spring:message  code="label.NAICCompanyCode"/> <c:if test="${exchangeState != 'CA'}"><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></c:if></label></td>
											<td>
											  	<input type="text" name="naicCompanyCode" id="naicCompanyCode" class="input-large" value="${issuerObj.naicCompanyCode}" maxlength="25"/>
			                            		<div id="naicCompanyCode_error" class=""></div>
											</td>
										</tr>
								
										<tr>
									<td class="txt-right"><label for="naicGroupCode"><spring:message  code="label.NAICGroupCode"/> <c:if test="${exchangeState != 'CA'}"><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></c:if></label></td>
											<td>
												<input type="text" name="naicGroupCode" id="naicGroupCode" class="input-large" value="${issuerObj.naicGroupCode}" maxlength="25"/>
			                            		<div id="naicGroupCode_error" class=""></div>
											</td>
										</tr>
										
								<tr>
									<td class="txt-right"><label for="federalEin"><spring:message  code="label.FederalEmployerId"/></label></td>
									<td>
										<input type="text" name="federalEin" id="federalEin" class="input-large" value="${issuerObj.federalEin}" maxlength="9"/>
	                            		<div id="federalEin_error" class=""></div>
									</td>
								</tr>
								<tr>
									<td class="txt-right"><label for="hiosIssuerId"><spring:message  code="label.HIOSIssuerId"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
									<td>
										<input type="text" name="hiosIssuerId" id="hiosIssuerId" class="input-large" value="${issuerObj.hiosIssuerId}" maxlength="5"/>
	                            		<div id="hiosIssuerId_error" class=""></div>
									</td>
								</tr>
								<%-- <tr>
									<td class="txt-right span4"><label for="txn834Version"><spring:message  code="label.txn834Version"/></label></td>
									<td>
										<input type="text" name="txn834Version" id="txn834Version" class="input-large" value="${issuerObj.txn834Version}"/>
	                            		<div id="txn834Versionname_error" class=""></div>
									</td>
								</tr>
								<tr>
									<td class="txt-right span4"><label for="txn820Version"><spring:message  code="label.txn820Version"/></label></td>
									<td>
										<input type="text" name="txn820Version" id="txn820Version" class="input-large" value="${issuerObj.txn820Version}"/>
	                            		<div id="txn820Version_name_error" class=""></div>
									</td>
								</tr> --%>
							</tbody>
						</table><br />
                        <div class="header">
                            <h4><spring:message  code="pgheader.issuerAddress"/></h4>
                        </div>
                        <table class="table table-border-none">
                            <tr>
                                <td class="txt-right span4"><label for="addressLine1"><spring:message  code="label.addressLine1"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
                                <td>
	                                <input type="text" name="addressLine1" id="addressLine1" class="input-large" value="${issuerObj.addressLine1}" maxlength="100"/>
		                            <div id="addressLine1_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right span4"><label for="addressLine2"><spring:message  code="label.addressLine2"/></label></td>
                                <td>
                                	<input type="text" name="addressLine2" id="addressLine2" class="input-large" value="${issuerObj.addressLine2}" maxlength="100"/>
		                            <div id="addressLine2_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="city"><spring:message  code="label.city"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
                                <td>
                                 	<input type="text" name="city" id="city" class="input-large" value="${issuerObj.city}" maxlength="30"/>
		                            <div id="city_error" class=""></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right">
                                	<label for="state"><spring:message  code="label.state"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
                                </td>
                                <td>
                                	<label for="state" class="hide">State</label>
                                	<select id="state" name="state" path="statelist">
                                     <option value="">Select</option>
                                     <c:forEach var="state" items="${statelist}">
                                        <option <c:if test="${state.code == issuerObj.state}"> SELECTED </c:if> value="${state.code}">${state.name}</option>
                                    </c:forEach>
                                </select>
                                <div id="state_error"></div>
                                </td>
                            </tr>
                            <tr>
                                <td class="txt-right"><label for="zip"><spring:message  code="label.zipCode"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label></td>
                                <td>
                                	<input type="text" name="zip" id="zip" class="input-large" value="${issuerObj.zip}" maxlength="5"/>
		                            <div id="zip_error" class=""></div>
                                </td>
                            </tr>
                       	</table>
					</div>
					<div class="form-actions">
						<a class="btn cancel-btn" href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a>
						<a class="btn btn-primary" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
						</div>
					</div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div>
<script type="text/javascript">

var exchangeState = '${exchangeState}';

var flagValue = true;
if(exchangeState == 'CA'){	
	flagValue = false;
}else {	
	flagValue = true;
}

var duplicateError = '${duplicateHOISId}';
var hoisId = '${HOISId}';
if(duplicateError == 'true'){
	$('#hiosIssuerId_error').html("<label class='error' for='hiosIssuerId' generated='true'><span><em class='excl'>!</em> <spring:message  code='err.duplicateHios' javaScriptEscape='true'/></span></label>");
}

$.validator.addMethod("alphanumericRegex", function(value, element) {
    return this.optional(element) || /^[a-zA-Z0-9]+$/i.test(value);
});

function submitForm(){
	 $("#frmEditIssuerDetails").submit(); 
} 
 var validator = $("#frmEditIssuerDetails").validate({ 
		rules : {
			name : {required : true},
			shortName : {required : true},
			naicCompanyCode : {required : flagValue, alphanumericRegex:true},
			naicGroupCode : {required : flagValue, alphanumericRegex:true},
			federalEin : { digits : true, minlength : 9, maxlength : 9},
			hiosIssuerId : { required : true, number : true, minlength : 5, maxlength : 5},
			addressLine1 : { required : true},
			city : { required : true},
			state : {required : true},
			zip : { required : true, number : true, minlength : 5, maxlength : 5}		
		},
		messages : {
			name : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerName' javaScriptEscape='true'/></span>"},
			shortName : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerShortName' javaScriptEscape='true'/></span>"},
			naicCompanyCode: { required : "<span><em class='excl'>!</em><spring:message  code='err.naicCompanyCode' javaScriptEscape='true'/></span>",
							   alphanumericRegex : "<span><em class='excl'>!</em><spring:message  code='err.validNaicCompanyCode' javaScriptEscape='true'/></span>"
							 },		
			naicGroupCode: { required : "<span><em class='excl'>!</em><spring:message  code='err.naicGroupCode' javaScriptEscape='true'/></span>",
							alphanumericRegex : "<span><em class='excl'>!</em><spring:message  code='err.validNaicGroupCode' javaScriptEscape='true'/></span>"
						   },
			federalEin: { digits : "<span><em class='excl'>!</em><spring:message  code='err.validFederalEmployerId' javaScriptEscape='true'/></span>",
				minlength : "<span><em class='excl'>!</em><spring:message  code='err.federalEmployerId' javaScriptEscape='true'/></span>"},
				
			hiosIssuerId: { 
				required : "<span><em class='excl'>!</em><spring:message  code='err.hiosIssuerId' javaScriptEscape='true'/></span>",
				number:"<span><em class='excl'>!</em><spring:message  code='err.validHios' javaScriptEscape='true'/></span>",
				minlength : "<span><em class='excl'>!</em><spring:message  code='err.hiosIdLength' javaScriptEscape='true'/></span>",
				maxlength : "<span><em class='excl'>!</em><spring:message  code='err.hiosIdLength' javaScriptEscape='true'/></span>"
			},		
			addressLine1 : { required : "<span><em class='excl'>!</em><spring:message  code='err.addressLine1' javaScriptEscape='true'/></span>"},
			city : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerCity' javaScriptEscape='true'/></span>"},
			state : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerState' javaScriptEscape='true'/></span>"},
			zip : { required : "<span><em class='excl'>!</em><spring:message  code='err.issuerZip' javaScriptEscape='true'/></span>",
		        number : "<span><em class='excl'>!</em><spring:message  code='err.issuervalidZip' javaScriptEscape='true'/></span>",
		        minlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>",
		        maxlength : "<span><em class='excl'>!</em><spring:message  code='err.issuerZipLength' javaScriptEscape='true'/></span>"
	          }			
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});

</script>		