//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.28 at 11:06:29 AM IST 
//


package com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResubmitResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ResubmitResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}ResubmitResponseSet" minOccurs="0"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *         &lt;element ref="{http://vclpsav.ee.sim.dsh.cms.hhs.gov}ArrayOfErrorResponseMetadata" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResubmitResponseType", propOrder = {
    "resubmitResponseSet",
    "responseMetadata",
    "arrayOfErrorResponseMetadata"
})
public class ResubmitResponseType {

    @XmlElement(name = "ResubmitResponseSet")
    protected ResubmitResponseSetType resubmitResponseSet;
    @XmlElement(name = "ResponseMetadata", required = true)
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "ArrayOfErrorResponseMetadata")
    protected ArrayOfErrorResponseMetadataType arrayOfErrorResponseMetadata;

    /**
     * Gets the value of the resubmitResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link ResubmitResponseSetType }
     *     
     */
    public ResubmitResponseSetType getResubmitResponseSet() {
        return resubmitResponseSet;
    }

    /**
     * Sets the value of the resubmitResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResubmitResponseSetType }
     *     
     */
    public void setResubmitResponseSet(ResubmitResponseSetType value) {
        this.resubmitResponseSet = value;
    }

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the arrayOfErrorResponseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfErrorResponseMetadataType }
     *     
     */
    public ArrayOfErrorResponseMetadataType getArrayOfErrorResponseMetadata() {
        return arrayOfErrorResponseMetadata;
    }

    /**
     * Sets the value of the arrayOfErrorResponseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfErrorResponseMetadataType }
     *     
     */
    public void setArrayOfErrorResponseMetadata(ArrayOfErrorResponseMetadataType value) {
        this.arrayOfErrorResponseMetadata = value;
    }

}
