/**
 * 
 */
package com.getinsured.hix.web.security;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Servlet context listener that marks cookie secure if enviornment variable
 * GHIX_SECURE_COOKIE
 * is present and set to 'true'.
 * 
 * This is related to Jira issue: HIX-37109
 * 
 * @author Yevgen Golubenko
 */
public class SSLCookieListener implements ServletContextListener
{
	private static final String SECURE_COOKIE_ENV_NAME = "GHIX_SECURE_COOKIE";
	private ServletContext      context;

	/**
	 * Called when context is initialized.
	 * 
	 * @param contextEvent context event.
	 */
	@Override
	public void contextInitialized(final ServletContextEvent contextEvent)
	{
		System.out.println("*** SSLCookieListener initialized ***");
		context = contextEvent.getServletContext();
		final Boolean secure = Boolean.valueOf(System.getProperty(SECURE_COOKIE_ENV_NAME, "false"));
		context.getSessionCookieConfig().setSecure(secure);
		System.out.println("Setting HTTP Cookie to secure: " + secure);
	}

	/**
	 * Called when context is destroyed.
	 * 
	 * @param contextEvent context event.
	 */
	@Override
	public void contextDestroyed(final ServletContextEvent contextEvent)
	{
		System.out.println("*** SSLCookieListener is destroyed ***");
	}
}
