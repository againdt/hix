package com.getinsured.iex.im.wso2.wrappers;

import com.getinsured.iex.im.wso2.carbon.um.ws.service.IsExistingUserResponse;

public class IsExistingUserResponseWrapper {
	private boolean existingUser;
	public IsExistingUserResponseWrapper(IsExistingUserResponse result){
		this.existingUser = result.isReturn();
	}
	public boolean isExistingUser() {
		return this.existingUser;
	}
	
}
