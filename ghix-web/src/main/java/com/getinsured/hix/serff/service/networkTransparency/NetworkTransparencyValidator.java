package com.getinsured.hix.serff.service.networkTransparency;

import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.COUNTY;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.NETWORK_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.PLAN_HIOS_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelColumnEnum.RATING;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.HIOS_ISSUER_ID;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.ISSUER_NAME;
import static com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyExcel.NetworkTransparencyExcelRowEnum.PLAN_YEAR;
import static com.getinsured.hix.util.PlanMgmtConstants.COMMA;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.getinsured.hix.serff.service.networkTransparency.NetworkTransparencyVO.NetworkTransparencyDataVO;

/**
 *-----------------------------------------------------------------------------
 * HIX-84043 Load and persist Network transparency rating.
 *-----------------------------------------------------------------------------
 *
 * Class is used to validate Network Transparency Excel data.
 * @author Bhavin Parmar
 * @since July 25, 2016
 */
@Component
public class NetworkTransparencyValidator {

	private static final Logger LOGGER = Logger.getLogger(NetworkTransparencyValidator.class);

	private static final String NEW_LINE = "\n";
	private static final String OPEN_BRACKET = "[";
	private static final String CLOSE_BRACKET = "]";
	private static final String EMSG_EMPTY = " Value should not be empty." + NEW_LINE;
	private static final String EMSG_TEXT_LENGTH_EXCEEDS = " Text length is greater than maximum length allowed." + NEW_LINE;
	private static final String EMSG_INVALID_COUNTY_ID = " Mismatched County Name with County Fips in database." + NEW_LINE;
	private static final String EMSG_INVALID_NETWORK_ID = " Invalid Network ID." + NEW_LINE;
	private static final String EMSG_INVALID_STATE_IN_NETWORK_ID = " Invalid State in Network ID." + NEW_LINE;
	private static final String EMSG_INVALID_STATE_IN_PLAN_ID = " Invalid State in Plan HIOS ID." + NEW_LINE;
	private static final String EMSG_INVALID_PLAN_ID = " Invalid Plan HIOS ID." + NEW_LINE;
	private static final String EMSG_INVALID_NUMERIC_VALUE = "Value must be Numeric" + NEW_LINE;
	private static final String EMSG_INVALID_NETWORK_TRANS_DATA = "Invalid Network Transparency Rating data due to validations error:" + NEW_LINE;
	private static final String EMSG_INVALID_NETWORK_TRANS = "Network Transparency Template is invalid.";

	private static final int LEN_NETWORK_ID = 6;
	private static final int LEN_PLAN_HIOS_ID = 14;
	private static final String PATTERN_NETWORK_ID = "(^[A-Z]{2}[N])([0-9]{3})$";
	private static final String PATTERN_PLAN_ID = "(^[0-9]{5})([A-Z]{2})([0-9]{7})$";
	private static final List<String> RATING_LIST = Arrays.asList(new String[] {"Basic","Standard","Broad"});

	/**
	 * Method is used to validate require data from validateLifeExcelVO object before persistence.
	 * 
	 * @param networkTransMainVO - NetworkTransparencyVO instance
	 * @return - Whether NetworkTransparencyVO having valid data to persist
	 */
	public boolean validateNetworkTransparencyVO(NetworkTransparencyVO networkTransMainVO, String applicableYear, String stateCode, String hiosIssuerID) {

		LOGGER.info("validateNetworkTransparencyVO() Start");
		boolean isValid = true;

		try {

			if (null == networkTransMainVO || CollectionUtils.isEmpty(networkTransMainVO.getNetworkTransData())) {
				LOGGER.error(EMSG_INVALID_NETWORK_TRANS);

				if (null != networkTransMainVO) {
					networkTransMainVO.setErrorMessages(EMSG_INVALID_NETWORK_TRANS);
					networkTransMainVO.setNotValid(true);
				}
				return false;
			}

			StringBuilder errorMessages = new StringBuilder();
			validatePlanYear(networkTransMainVO.getPlanYear(), networkTransMainVO, applicableYear, errorMessages, PLAN_YEAR.getRowName());
			validateBlankData(networkTransMainVO.getIssuerName(), networkTransMainVO, errorMessages, ISSUER_NAME.getRowName());
			validateHiosIssuerID(networkTransMainVO.getHiosIssuerID(), hiosIssuerID, networkTransMainVO, errorMessages, HIOS_ISSUER_ID.getRowName());

			List<String> planIDAndCountyList = new ArrayList<String>();
			Map<String, String> planIDAndNetworkIDMap = new HashMap<String, String>();

			for (NetworkTransparencyDataVO networkTransData : networkTransMainVO.getNetworkTransData()) {

				validateNetworkID(networkTransData.getNetworkID(), stateCode, networkTransMainVO, errorMessages, NETWORK_ID.getColumnName(), LEN_NETWORK_ID);
				validatePlanHiosID(networkTransData.getPlanHiosID(), stateCode, hiosIssuerID, networkTransMainVO, errorMessages, PLAN_HIOS_ID.getColumnName(), LEN_PLAN_HIOS_ID);
				validateCounty(networkTransData.getCounty(), networkTransData.getCountyFips(), networkTransMainVO, errorMessages, COUNTY.getColumnName());
				validateRating(networkTransData.getRating(), networkTransMainVO, networkTransData.getPlanHiosID(), errorMessages, RATING.getColumnName());

				if (!networkTransMainVO.isNotValid()) {

					if (planIDAndCountyList.contains(networkTransData.getPlanHiosID() + networkTransData.getCounty().toLowerCase())) {
						setErrorForDuplicatePlanRecord(networkTransData.getPlanHiosID(), networkTransMainVO, errorMessages);
						break;
					}
					// Combination of HIOS Plan ID and County
					planIDAndCountyList.add(networkTransData.getPlanHiosID() + networkTransData.getCounty().toLowerCase());
	
					if (null != planIDAndNetworkIDMap.get(networkTransData.getPlanHiosID())
							&& !planIDAndNetworkIDMap.get(networkTransData.getPlanHiosID()).equals(networkTransData.getNetworkID())) {
						setErrorForMultiNetworksForOnePlan(networkTransData.getPlanHiosID(), networkTransMainVO, errorMessages);
						break;
					}
					// Map of Plan ID and Network ID
					planIDAndNetworkIDMap.put(networkTransData.getPlanHiosID(), networkTransData.getNetworkID());
				}
			}

			if (networkTransMainVO.isNotValid()) {
				isValid = false;
				networkTransMainVO.setErrorMessages(EMSG_INVALID_NETWORK_TRANS_DATA + errorMessages.toString());
				LOGGER.error(networkTransMainVO.getErrorMessages());
			}
		}
		finally {
			LOGGER.info("validateNetworkTransparencyVO() End");
		}
		return isValid;
	}

	private void setErrorForDuplicatePlanRecord(String hiosPlanNumber, NetworkTransparencyVO networkTransMainVO, StringBuilder errorMessages) {

		errorMessages.append(" Duplicate entries for ");
		errorMessages.append(hiosPlanNumber);
		errorMessages.append(NEW_LINE);

		if (!networkTransMainVO.isNotValid()) {
			networkTransMainVO.setNotValid(true);
		}
	}

	private void setErrorForMultiNetworksForOnePlan(String hiosPlanNumber, NetworkTransparencyVO networkTransMainVO, StringBuilder errorMessages) {

		errorMessages.append(" Multiple networks for ");
		errorMessages.append(hiosPlanNumber);
		errorMessages.append(NEW_LINE);

		if (!networkTransMainVO.isNotValid()) {
			networkTransMainVO.setNotValid(true);
		}
	}

	/**
	 * Method is used to validate Plan Year from Cell Value.
	 */
	private void validatePlanYear(String cellValue, NetworkTransparencyVO networkTransMainVO, String applicableYear,
			StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateNumeric() Start");
		if (!StringUtils.isNumeric(cellValue)) {

			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_NUMERIC_VALUE);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!cellValue.equals(applicableYear)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(" is not matched with Selected Plan Year [");
			errorMessages.append(applicableYear);
			errorMessages.append("].");
			errorMessages.append(NEW_LINE);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateNumeric() End");
	}

	/**
	 * Method is used to validate Blank Data from Cell Value.
	 */
	private void validateBlankData(String cellValue, NetworkTransparencyVO networkTransMainVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateBlankData() Start");

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateBlankData() End");
	}

	/**
	 * Method is used to validate County from Cell Value.
	 */
	private void validateCounty(String county, String countyFips, NetworkTransparencyVO networkTransMainVO, StringBuilder errorMessages, String cellHeader) {
		LOGGER.debug("validateCounty() Start");

		if (StringUtils.isBlank(county)) {
			errorMessages.append(cellHeader);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (StringUtils.isBlank(countyFips)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(county);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_COUNTY_ID);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateCounty() End");
	}

	/**
	 * Method is used to validate HIOS Issuer ID from Cell Value.
	 */
	private void validateHiosIssuerID(String cellValue, String hiosIssuerID, NetworkTransparencyVO networkTransMainVO,
			StringBuilder errorMessages, String cellHeader) {

		LOGGER.debug("validateHiosIssuerID() Start");

		if (!StringUtils.isNumeric(cellValue)) {

			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_NUMERIC_VALUE);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!cellValue.equals(hiosIssuerID)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(" is not matched with Current HIOS Issuer ID [");
			errorMessages.append(hiosIssuerID);
			errorMessages.append("].");
			errorMessages.append(NEW_LINE);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateHiosIssuerID() End");
	}

	/**
	 * Method is used to validate Plan HIOS ID from Cell Value.
	 */
	private void validatePlanHiosID(String cellValue, String stateCode, String hiosIssuerID, NetworkTransparencyVO networkTransMainVO,
			StringBuilder errorMessages, String cellHeader, int maxLength) {

		LOGGER.debug("validatePlanHiosID() Start");

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (cellValue.length() > maxLength) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_TEXT_LENGTH_EXCEEDS);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!cellValue.startsWith(hiosIssuerID)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(" Invalid Current HIOS Issuer ID  [");
			errorMessages.append(hiosIssuerID);
			errorMessages.append("] in this Plan.");
			errorMessages.append(NEW_LINE);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!cellValue.substring(5, 7).equals(stateCode)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_STATE_IN_PLAN_ID);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!isValidPattern(PATTERN_PLAN_ID, cellValue)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_PLAN_ID);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validatePlanHiosID() End");
	}

	/**
	 * Method is used to validate Network ID from Cell Value.
	 */
	private void validateNetworkID(String cellValue, String stateCode, NetworkTransparencyVO networkTransMainVO,
			StringBuilder errorMessages, String cellHeader, int maxLength) {

		LOGGER.debug("validateNetworkID() Start");

		if (StringUtils.isBlank(cellValue)) {
			errorMessages.append(cellHeader);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_EMPTY);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (cellValue.length() > maxLength) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_TEXT_LENGTH_EXCEEDS);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!cellValue.startsWith(stateCode)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_STATE_IN_NETWORK_ID);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		else if (!isValidPattern(PATTERN_NETWORK_ID, cellValue)) {
			errorMessages.append(cellHeader);
			errorMessages.append(OPEN_BRACKET);
			errorMessages.append(cellValue);
			errorMessages.append(CLOSE_BRACKET);
			errorMessages.append(COMMA);
			errorMessages.append(EMSG_INVALID_NETWORK_ID);

			if (!networkTransMainVO.isNotValid()) {
				networkTransMainVO.setNotValid(true);
			}
		}
		LOGGER.debug("validateNetworkID() End");
	}

	/**
	 * Method is used to validate Rating from Cell Value.
	 * Rating must be Basic, Standard or Broad.
	 */
	private void validateRating(String cellValue, NetworkTransparencyVO networkTransMainVO, String hiosPlanID, StringBuilder errorMessages, String cellHeader) {
		
		LOGGER.debug("validateRating() Start");

		if (StringUtils.isNotEmpty(cellValue)) {

			if (!RATING_LIST.contains(cellValue)) {
				errorMessages.append(cellHeader);
				errorMessages.append(OPEN_BRACKET);
				errorMessages.append(cellValue);
				errorMessages.append(CLOSE_BRACKET);
				errorMessages.append(COMMA);
				errorMessages.append(" Network Rating for ");
				errorMessages.append(hiosPlanID);
				errorMessages.append(" not acceptable, Correct value should be Basic or Standard or Broad.");
				errorMessages.append(NEW_LINE);

				if (!networkTransMainVO.isNotValid()) {
					networkTransMainVO.setNotValid(Boolean.TRUE);
				}
			}
		}
		LOGGER.debug("validateRating() End");
	}

	/**
	 * Method is used to validate given value with passed argument pattern.
	 * @param pattern
	 * @return True for Valid otherwise False.
	 */
	private boolean isValidPattern(final String pattern, final String givenValue) {

		boolean isValid = false;

		if (StringUtils.isNotEmpty(pattern)
				&& StringUtils.isNotEmpty(givenValue)) {

			// Assigning the regular expression
			return givenValue.matches(pattern);
		}
		return isValid;
	}
}
