<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/employer-registration-util.tld" prefix="util"%>

<!--titlebar-->
<div class="gutter10">
<div class="row-fluid">
	<div class="span9">
		<h1 id="skip">
			<spring:message  code="label.emprroasteraddemph1"/> <small> ${empCount} <spring:message  code="label.emprroasteraddemph2"/>,
				${dependentCnt} <spring:message  code="label.emprreviewdepend"/></small>
		</h1>
	</div>
</div>
<div class="row-fluid">
	<div class="span3" id="sidebar">
		<util:nav pageName="Review Employees" navStepNo="2"></util:nav>
	</div>
	<!--sidebar-->

	<div class="span9" id="rightpanel">
		<div class="header">
			<h4><spring:message  code="label.emprreviewemp"/></h4>
		</div>
		<!--titlebar-->
		<div class="gutter10">
			<p><spring:message  code="label.emprreviewemptitle"/></p>

			<!-- CHANGE STARTS HERE  -- TABLE  -->
			<div id="emplyeeslist">
				<c:choose>
					<c:when test="${fn:length(employeelist) > 0}">
						<table class="table table-border-none table-striped">
							<thead>
								<tr class="header">
									<th scope="col" class="sortable"><dl:sort title="Name"
											sortBy="name" sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
									<th scope="col" class="txt-center sortable"><dl:sort
											title="Dependents" sortBy="dependentCount"
											sortOrder="${searchCriteria.sortOrder}"></dl:sort></th>
									<th scope="col" class="txt-right"><input type="button" name="submitButtonAddEmp" id="submitButtonAddEmp" onclick="javascript:window.location.href ='addemployee' " class="btn btn-primary btn-small" value="<spring:message  code="label.addnewemp"/>"></th>
								</tr>
							</thead>
							<c:forEach items="${employeelist}" var="employee">
								<tr class="odd">
									<td>${employee.name}</td>
									<td class="txt-center">${employee.dependentCount}</td>
									<td><c:if
											test="${(employee.status != 'DELETED')}">
											<div class="dropdown pull-right">
												<a class="dropdown-toggle" data-toggle="dropdown" href="#"><i
													class="icon-cog" alt="cog"></i><i class="caret"></i></a>
												<ul class="dropdown-menu" aria-labelledby="menu">
													<li><a
														href="/hix/shop/employer/roster/editemployee?id=${employee.id}"><i
															class="icon-pencil"></i><spring:message  code="label.empredit"/></a></li>
													<li><a href="#deleteEmployee" class="gutter10"
														data-toggle="modal"
														onclick="manageEmpDelete('${employee.id}','${employee.name}');"><i
															class="icon-remove"></i> <spring:message  code="label.emprdelete"/></a></li>
												</ul>
											</div>
										</c:if></td>
								</tr>
							</c:forEach>
						</table>
						<dl:paginate resultSize="${resultSize + 0}"
							pageSize="${pageSize + 0}" />
					</c:when>
					<c:otherwise>
						<hr />
						<div class="alert alert-info"><spring:message  code="label.norecordfound"/></div>
					</c:otherwise>
				</c:choose>
			</div>
			<!-- end of employees list -->

			<!-- TABLE END -->
			<!-- modal -->
			<div id="deleteEmployee" class="modal hide fade" tabindex="-1"
				role="dialog" aria-labelledby="DeleteEmployee" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal"
						aria-hidden="true">�</button>
					<h3><spring:message  code="label.emprdeleteemp"/></h3>
				</div>
				<div class="modal-body" id="deleteEmpBody"></div>
				<div class="modal-footer">
					<button class="btn" data-dismiss="modal"><spring:message  code="label.cancel"/></button>
					<button class="btn btn-primary" id="deleteEmployeeButton"><spring:message  code="label.ok"/></button>
				</div>
			</div>
			<!-- /modal -->

			<div class="center-text">
				<div class="form-actions">
					<input type="button" name="submitButtonBack" id="submitButtonBack" onclick="javascript:window.location.href ='addemployee' " class="btn" value="<spring:message  code="label.newhireback"/>">
					<a href="<c:url value="/shop/employer/application/attestation"/>" class="btn btn-primary"><spring:message  code="label.next"/></a>
				</div>
			</div>
		</div>
		<!--gutter10-->
	</div>
	<!-- rightpanel -->
</div>
<!-- row-fluid -->
</div>

<script type="text/javascript">

function manageEmpDelete(eid, ename){	
	$('.modal-body').html("<spring:message code='label.emprdelete' javaScriptEscape='true'/> " + ename + " ?");
	$('#deleteEmployeeButton').bind('click', function() {
		window.location.href ='deleteemployee?id='+eid;
    });
}

</script>

