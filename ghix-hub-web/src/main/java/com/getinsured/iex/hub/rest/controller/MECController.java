package com.getinsured.iex.hub.rest.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/**
 * Controller class to handle synchronous request to MEC module
 * 
 * @author Vishaka Tharani
 * @since 24-Mar-2014
 * 
 */
@Controller
public class MECController extends MessageProcessor {

	
	@Autowired
	private WebServiceTemplate mecApplicantServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MECController.class);

	@RequestMapping(value = "/invokeMEC", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeMEC(@RequestBody String mecRequestJSON){
		
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		Object tmp = null;
		try{
			HubServiceBridge esiMecBridge = HubServiceBridge.getHubServiceBridge("MECApplicantRequest");
			handler = esiMecBridge.getServiceHandler();
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(mecRequestJSON);
			tmp = objInputJson.get("payload");
			if(tmp == null){
				throw new HubMappingException("No Payload found in input data");
			}
			String mecInputJSON = ((JSONObject)tmp).toJSONString();
			handler.setJsonInput(mecInputJSON);
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("MEC");
			requestRecord.setEndpointOperationName("MEC Eligibility");
			tmp = objInputJson.get("clientIp");
			if(tmp != null){
				requestRecord.setAccessIp((String)tmp);
			}
			requestRecord.setStatus("PENDING");
			tmp = objInputJson.get("applicationId");
			if(tmp ==  null){
				throw new HubServiceException("Application Id is mandatory");
			}
			if(objInputJson.get("applicationId") != null){
				requestRecord.setSsapApplicationId((Long) objInputJson.get("applicationId"));
			}
			response = this.executeRequest(requestRecord, handler, mecApplicantServiceTemplate);
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
}
