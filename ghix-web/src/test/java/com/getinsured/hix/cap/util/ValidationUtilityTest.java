package com.getinsured.hix.cap.util;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ValidationUtilityTest {

	@Autowired	ValidationUtility m_validationUtility;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(ValidationUtilityTest.class);
	
	
	@Test
	public void testPhoneValidity()
	{
		assertTrue(ValidationUtility.isValidPhoneNumber("1231231232"));
		assertFalse(ValidationUtility.isValidPhoneNumber("ssdkfj"));

		//corner case:
		ValidationUtility.isValidPhoneNumber(null);
		ValidationUtility.isValidPhoneNumber("");
}
	@Test
	public void testEmailValidity()
	{
		assertTrue(ValidationUtility.isValidEmail("ravi@getinsured.com"));
		assertFalse(ValidationUtility.isValidEmail("alskdfj"));
		
		//corner case:
		ValidationUtility.isValidEmail(null);
		ValidationUtility.isValidEmail("");
	}
	

	
}
