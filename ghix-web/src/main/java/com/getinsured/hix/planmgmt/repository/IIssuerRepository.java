package com.getinsured.hix.planmgmt.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Issuer;

@Repository("iIssuerRepository")
public interface IIssuerRepository extends JpaRepository<Issuer, Integer>, RevisionRepository<Issuer, Integer, Integer> {
	//Issuer findByUser(AccountUser user);

	@Query("SELECT issuer.id, issuer.name FROM Issuer issuer order by UPPER(issuer.name)")
	List<Object[]> getAllIssuerNames();

	@Query("SELECT issuer.id, issuer.name FROM Issuer issuer WHERE issuer.id IN ( select distinct p.issuer from Plan p where isDeleted='N' AND insurance_type = :insuranceType) order by UPPER(issuer.name)")
	List<Issuer> getAllIssuerNamesByInsuranceType(@Param("insuranceType") String insuranceType);
	
	@Query("SELECT issuer.id, issuer.name FROM Issuer issuer WHERE (issuer.certificationStatus='CERTIFIED' OR issuer.certificationStatus='RECERTIFIED') order by UPPER(issuer.name)")
	List<Object[]> findAllCertifiedIssuerNames();

	@Query("SELECT i FROM Issuer i WHERE (i.name = :issuerName) AND (i.licenseNumber = :issuerLicenseNumber) AND (i.state = :issuerState)")
	Issuer findIssuer(@Param("issuerName") String issuerName, @Param("issuerLicenseNumber") String issuerLicenseNumber, @Param("issuerState") String issuerState);

	@Query("SELECT i FROM Issuer i WHERE (i.creationTimestamp BETWEEN :startdatetime AND :enddatetime) OR (i.lastUpdateTimestamp between :startdatetime AND :enddatetime) ORDER BY i.id")
	List<Issuer> getAddedOrUpdatedIssuers(@Param("startdatetime") Date startdatetime, @Param("enddatetime") Date enddatetime);

	@Query("SELECT ir.issuer FROM IssuerRepresentative ir WHERE ir.userRecord.id = :userId")
	Issuer getIssuerByRepUserId(@Param("userId") Integer userId);
	
	/*//@Query("SELECT u FROM AccountUser u , Issuer i, ModuleUser mu, UserRole ur, Role r where i.id=:issuerId AND r.name=:roleName AND mu.user.id=u.id AND mu.moduleId=i.id AND mu.user.id=ur.user.id AND ur.role.id=r.id")
	@Query("SELECT U FROM AccountUser U , Issuer I, ModuleUser MU, UserRole UR, Role R, IssuerRepresentative IR " +
			"WHERE IR.issuer.id = :issuerId AND IR.issuer.id = I.id " +
			"AND R.name = :roleName AND R.id = UR.role.id AND UR.user.id = MU.user.id AND MU.user.id = U.id AND U.id = IR.issuer.id")
	List<AccountUser> getRepListForIssuer(@Param("issuerId") Integer issuerId, @Param("roleName") String roleName);*/
	
	@Query("SELECT u FROM AccountUser u , " +
            "Issuer i, " +
            "IssuerRepresentative ir, " +
            "ModuleUser mu, " +
            "UserRole ur, " +
            "Role r " +
           " where " +
	         "mu.user.id=u.id AND " +
	         "mu.moduleId=i.id AND " +
	         "mu.user.id=ur.user.id AND " +
	         "u.id=ir.userRecord.id AND " +
	         "i.id=ir.issuer.id AND " +
	         "ur.role.id=r.id AND " +
	         "mu.moduleName=:moduleName AND " +
	         "r.name=:roleName AND " +
	         "i.id=:issuerId AND " +
	         "((lower(u.firstName) like '%'|| lower(:firstName) || '%') OR (lower(u.lastName) like '%'|| lower(:lastName) || '%')) AND " +
	         "(((LOWER(:title) IS NOT NULL) AND (LOWER(u.title) like '%'|| lower(:title) || '%')) OR (((LOWER(:title) IS NULL) AND ((u.title IS NULL) OR (u.title IS NOT NULL))))) AND " +
	         "u.confirmed in (:status)")
	Page<AccountUser> getRepListForIssuer(@Param("moduleName") String moduleName, @Param("roleName") String roleName,  @Param("issuerId") Integer issuerId, @Param("firstName") String firstName,  @Param("lastName") String lastName, @Param("title") String title, @Param("status") List<Integer> status, Pageable pageable);
	
	@Query("SELECT i FROM Issuer i WHERE ('ALL' =:status OR UPPER(i.certificationStatus) = UPPER(:status) ) AND ('ALL' =:name OR UPPER(i.name) LIKE '%' || UPPER(:name) || '%') ")
	Page<Issuer> getAllIssuers(@Param("name") String name, @Param("status") String status, Pageable pageable);
	
	@Query("SELECT i FROM Issuer i WHERE (i.hiosIssuerId = :hiosIssuerID)")
	Issuer getIssuerByHiosID(@Param("hiosIssuerID") String hiosIssuerID);
	
	@Query("SELECT i FROM Issuer i WHERE i.name = :issuerName")
	Issuer getIssuerByName(@Param("issuerName") String issuerName);	
	
	@Query("SELECT new com.getinsured.hix.serff.service.IssuerDropDownDTO(issuer.hiosIssuerId, issuer.shortName, issuer.name) from Issuer as issuer order by issuer.hiosIssuerId")
	List<com.getinsured.hix.serff.service.IssuerDropDownDTO> getIssuerNameList();
	
	@Query("SELECT i.certificationStatus FROM Issuer i WHERE i.id = :isserId")
	String getIssuerStatus(@Param("isserId") Integer isserId);	
	
	@Query("SELECT issuer.paymentUrl FROM Issuer issuer  where issuer.id = :id")
	String findPaymentUrlById(@Param("id") Integer id);
	
	@Query("SELECT i FROM Issuer i WHERE i.id = :id")
	Issuer findById(@Param("id") Integer id);
	
	/**
	 * 
	 * @param issuerId
	 * @return
	 */
	@Query("SELECT iss.companyLogo FROM Issuer iss WHERE iss.id = :issuerId")
	String getIssuerLogoByIssuerId(@Param("issuerId")Integer issuerId);
}
