<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>

<div class="row-fluid">
	<div class="txt-center margin100">
		<p>For your security, we have timed out your session. Please log in or try again.</p>
	</div>
</div>