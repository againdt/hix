<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.util.GhixPlatformConstants"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
 <script type="text/javascript">
 var RecaptchaOptions = {
    theme : 'custom',
    custom_theme_widget: 'recaptcha_widget'
 };
 </script>
 
 <%@page import="java.util.Properties"%>
	<%@page import="org.springframework.context.ApplicationContext"%>
	<%!private static ApplicationContext ctx = null;%>
	<%!private static Properties prop = null;%>
	<%
	        if (ctx == null) {
	                ctx = org.springframework.web.servlet.support.RequestContextUtils.getWebApplicationContext(request, application);
	        }
	        if (prop == null) {
	                prop = (Properties) ctx.getBean("configProp");
	        }
	%>
 	 <script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div class="form-wrapper">
	<div class="row-fluid" id="titlebar">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<h1><a name="skip"></a>Reset your password</h1>
	<script>
		document.title = 'Reset your password / ' + $('#exchangeName').val();
	</script>
	</div>
			</div>
	<div class="row-fluid">
		<div class="box-loose padd_none" id="rightpanel">
			<form class="form-horizontal gutter20" id="forgotpassword" name="forgotpassword" action="forgotpassword" method="POST" onsubmit="formSubmit()">
				<df:csrfToken/>
				<div class="form-group">
					<label for="j_username" class="col-xs-12 col-sm-5 col-md-5 col-lg-5 bs3-control-label"><spring:message code="label.userEmail"/></label>
						<div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
							<input type="text" name="j_username" id="j_username" class="input-large" size="30">
							<div class="error help-inline" id="j_username_error"></div>
														
							<c:if test="${!empty errorMsg}">
								<div class="alert alert-error alert-error-custom">
									<c:out value="${errorMsg}"></c:out>
								</div>
							</c:if>	
							
							<c:if test="${!empty idalinkUser}">
								<%
									String idalinkForgotPasswordUrl = GhixPlatformConstants.IDALINK_FORGOT_PASSWORD_URL+"?p_p_id=58&_58_struts_action=%2Flogin%2Fforgot_password&_58_emailAddress="+request.getParameter("j_username");
									String exchangefullName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME);
								%>
								<div class="alert alert-error alert-error-custom">
								<p>We are unable to reset your password at <%=exchangefullName%>. Please visit <a onclick="openInSameTab('<%=idalinkForgotPasswordUrl%>')">idalink</a> to reset your password and come back later.</p>
								</div>
							</c:if>
						
						</div>
				</div>
                <c:if test="${hideCaptcha != 'true'}">
					<!--custom captcha starts-->
					
					<div class="control-group">
						<div class="controls">
							<div class="g-recaptcha" data-sitekey="<%=prop.getProperty("Recaptcha.siteKey")%>" data-callback="reCapchaFilled"></div>      
								
							<input type="hidden" name="hiddenRecaptcha" id="hiddenRecaptcha">
							<div class="error help-inline" id="hiddenRecaptcha_error"></div>
							
							
							<div id="securityCode_error" class="error help-inline" style="color: red;height:30px;">
								${captchaError}
							</div> 
						</div>
						
					</div>
						
				</c:if>				
				<div class="form-group">
					<div class="col-xs-12 col-sm-7 col-sm-offset-5 col-md-7 col-md-offset-5 col-lg-7 col-lg-offset-5">
						<input type="submit" id="continue" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-fullwidth-xs margin10-t" title="<spring:message  code='label.brkContinue'/>">
					</div>
				</div>		
			</form>
		</div>
	</div>
</div>
<script type="text/javascript">
function validateEmail(sEmail) {
	var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
	if (filter.test(sEmail)) {
	    return true;
	}
	else {
	    return false;
	}
}

jQuery.validator.addMethod("validateLogin", function(value,
			element, param) {
		 var sEmail = $('#j_username').val();
		 if (!validateEmail(sEmail)){
			 return false;
		 }
		 return true;
	});

var validator = $("#forgotpassword").validate({ 
	ignore: ".ignore",
	rules : {
		j_username: { 
			required : true,
			validateLogin:true	
		},
		hiddenRecaptcha: {
            required: function () {
                if(grecaptcha.getResponse() === '') {
                	return true;
                }else {
                	return false;
                }
            }
        }
	},
	messages : {
		j_username: { 
			required : "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Email address is required.</span>",
			validateLogin:"<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please enter valid email address.</span>"
			},
		hiddenRecaptcha: {
	        required: "<span> <em class='excl'>!<span class='aria-hidden'>Error: Invalid Entry </span></em>Please confirm that you are not a robot.</span>"
	    }
	},
	onkeyup: false,
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	}
});


function reCapchaFilled() {
	$('#hiddenRecaptcha_error, #securityCode_error').hide();
}

function formSubmit() {
	$('#securityCode_error').hide();
}

</script>	