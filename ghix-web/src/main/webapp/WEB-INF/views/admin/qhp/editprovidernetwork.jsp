<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${planId}" isurl="true"/> </c:set>
	<div class="row-fluid">
			<ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
                <li><spring:message code="label.linkProviderNetwork"/></li>
            </ul><!--page-breadcrumb ends-->
            <h1><a name="skip"></a><issuerLogo:view issuerName="${issuerName}" />${planName}</h1>
		</div>
	
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
					<c:if test="${displayProviderNetwork == 'YES'}">
						<li><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message code='label.action'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqhpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
        </div><!-- end of span3 -->
        
		<div class="span9" id="rightpanel"> 
			
			<div class="header">
				<h4 class="pull-left"><spring:message code='label.QHPRates'/></h4>
				<a class="btn btn-small" title="<spring:message  code="label.btnCancel"/>" href="#" onclick="Javascript:location.href='<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />'"><spring:message code='label.btnCancel'/></a> 
				<a class="btn btn-primary btn-small" title="<spring:message  code="label.btnSave"/>" href="#"  onclick="javascript:document.frmUpdtCert.submit();"><spring:message  code='label.btnSave'/></a>
			</div>
			<form class="form-horizontal" id="frmUpdtCert" name="frmUpdtCert"
				action='<c:url value="/admin/planmgmt/updateqhpprovidernetwork"/>'
				method="post">
				<df:csrfToken/>
				<%-- <input type="hidden" id="planId" name="planId" value="${planId}" /> --%>
				<input type="hidden" id="planId" name="planId" value="<encryptor:enc value="${planId}"></encryptor:enc>"/>

				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span4 txt-right"><spring:message code='label.issuerName'/></td>
							<td><strong>${issuerName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message  code="label.planName"/></td>
							<td><strong>${planName}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message  code="label.planNumber"/></td>
							<td><strong>${planNumber}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message  code="label.currentStatus"/></td>
							<td><strong>${currentStatus}</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><label for="providerNetwork"><spring:message  code="label.linkProviderNetwork"/></label></td>
							<td> 
	                            <select id="providerNetwork" name="providerNetwork">
									 <c:forEach var="network" items="${networkList}">
										<option <c:if test="${network.id == selNetwork}"> selected='selected' </c:if> value="${network.id}">
											${network.name} - ( <c:if test="${fn:toUpperCase(network.type) == 'HMO'}"><spring:message code='label.hmo' javaScriptEscape='true'/></c:if>	
											<c:if test="${fn:toUpperCase(network.type) == 'PPO'}"><spring:message code='label.ppo' javaScriptEscape='true'/></c:if> )</option>
									 </c:forEach>	
								</select>
			                 </td>
						</tr>
					</tbody>
				</table>
			</form>

		</div>
		<!-- end of .span9 -->
	</div>
</div>
<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	})
})
</script>
