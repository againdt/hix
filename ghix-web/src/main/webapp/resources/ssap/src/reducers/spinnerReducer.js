import { spinnerActionTypes } from '../constants/actionTypes';

export const initialState = {
    activeSpinnersQty: 0,
    message: null
};

export default (state = initialState, action) => {
    switch(action.type) {
    case spinnerActionTypes.START_SPINNER:
        return {
            activeSpinnersQty: state.activeSpinnersQty + 1,
            message: action.message
        };
    case spinnerActionTypes.STOP_SPINNER:
        if(state.activeSpinnersQty === 0) {
            throw new Error('Stop spinner invoked before start spinner');
        }
        return {
            activeSpinnersQty: state.activeSpinnersQty - 1,
            message: state.activeSpinnersQty === 1 ? null : state.message
        };
    case spinnerActionTypes.CLEAR_STATE:
        return initialState;
    default:
        return state;
    }
};