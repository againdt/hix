package com.getinsured.hix.entity.enrollmententity.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.ws.http.HTTPException;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.dto.finance.PaymentMethodRequestDTO;
import com.getinsured.hix.dto.finance.PaymentMethodResponse;
import com.getinsured.hix.entity.admin.service.EntityIndTriggerService;
import com.getinsured.hix.entity.assister.service.AssisterDesignateService;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.enrollmententity.service.EnrollmentEntityService;
import com.getinsured.hix.entity.enrollmententity.service.PopulationEthnicityService;
import com.getinsured.hix.entity.enrollmententity.service.PopulationIndustryService;
import com.getinsured.hix.entity.enrollmententity.service.PopulationLanguageService;
import com.getinsured.hix.entity.enrollmententity.service.SiteLangaugesService;
import com.getinsured.hix.entity.enrollmententity.service.SiteLocationHoursService;
import com.getinsured.hix.entity.enrollmententity.service.SiteService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.PaymentMethods.PaymentStatus;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityLocation;
import com.getinsured.hix.model.entity.PopulationEthnicity;
import com.getinsured.hix.model.entity.PopulationIndustry;
import com.getinsured.hix.model.entity.PopulationLanguage;
import com.getinsured.hix.model.entity.PopulationServedWrapper;
import com.getinsured.hix.model.entity.Site;
import com.getinsured.hix.model.entity.SiteLanguages;
import com.getinsured.hix.model.entity.SiteLocationHours;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.FinanceServiceEndPoints;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.thoughtworks.xstream.XStream;

/**
 * Handles requests for the enrollment entity pages.
 */
@SuppressWarnings("unused")
@Controller
@RequestMapping(value = "/entity/enrollmentEntity")
public class EnrollmentEntityController {

	private static final String HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY = "hasPermission(#model, 'ENTITY_EDIT_ENTITY')";

	private static final String HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY = "hasPermission(#model, 'ENTITY_VIEW_ENTITY')";

	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentEntityController.class);

	@Autowired
	private EnrollmentEntityService enrollmentEntityService;
	@Autowired
	EntityIndTriggerService entityIndTriggerService;
	@Autowired
	private SiteService siteService;
	@Autowired
	private SiteLangaugesService siteLanguageService;
	@Autowired
	private SiteLocationHoursService siteLocationHoursService;
	@Autowired
 	private GhixRestServiceInvoker ghixRestServiceInvoker;
 	@Autowired
 	private GhixRestTemplate restTemplate;
	@Autowired
	private PopulationLanguageService populationLanguageService;
	@Autowired
	private PopulationEthnicityService populationEthnicityService;
	@Autowired
	private PopulationIndustryService populationIndustryService;
	@Autowired
	private LookupService lookupService;
	@Autowired
	private AssisterService assisterService;
	@Autowired
	private ZipCodeService zipCodeService;
	@Autowired
	AssisterDesignateService assisterDesignateService;
	
	private static final int TWO_HUNDRED_ONE = 201;
	private static final String PRIMARY = "PRIMARY";
	private static final String ENTITY_INFO = "ENTITY_INFO";
	private static final String POPULATION_SERVED = "POPULATION_SERVED";
	private static final String LOCATION_HOURS = "LOCATION_HOURS";
	private static final String ENTITY_CONTACT = "ENTITY_CONTACT";
	private static final String ASSISTER = "ASSISTER";
	private static final String PAYMENT_INFO = "PAYMENT_INFO";
	private static final String DOCUMENT_UPLOAD = "DOCUMENT_UPLOAD";
	private static final String ASSISTERS = "assisters";
	private static final String TOTAL_RECORDS = "totalRecords";
	
	/**
	 * To test {@link EnrollmentEntityController}
	 * 
	 * @return welcome message
	 * @throws HTTPException
	 * @throws Exception
	 */
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {

		LOGGER.info("welcome: START");

		LOGGER.info("Welcome to Enrollment Entity Service module");

		LOGGER.info("welcome: END");

		return "Welcome to Enrollment Entity Service module";
	}

	/**
	 * Retrieves EnrollmentEntity object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getenrollmententitydetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEnrollmentEntityInfo(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getEnrollmentEntityInfo: START");
		String responseJSON = null;
		LOGGER.info("Retrieving Enrollment Entity Information");
		try {
			EnrollmentEntityResponseDTO enrollmentEntityDTO = getEnrollmentEntityByUserOrRecordId(enrollmentEntityRequestDTO);
			// Marshaling the response before it is sent out to ghix-web
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString(enrollmentEntityDTO);
		} catch (Exception exception) {
			 LOGGER.error("Exception occured in getEnrollmentEntityInfo in ghix-entity." , exception);
			 throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getEnrollmentEntityInfo: END");

		return responseJSON;
	}

	/**
	 * Saves EnrollmentEntity object
	 * 
	 * @param enrollmentEntityRequesttring
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/saveenrollmententitydetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEnrollmentEntity(@RequestBody String enrollmentEntityRequesttring) {

		LOGGER.info("saveEnrollmentEntity: START");
		String responseJSON = null;
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
			EnrollmentEntity enrollEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
			EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
			EnrollmentEntity savedentityObject=enrollmentEntityService.saveEnrollmentEntity(enrollEntity);
			
			if (savedentityObject != null) {
				enrollmentEntityDTO.setEnrollmentEntity(savedentityObject);
			}
			
			responseJSON = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString(enrollmentEntityDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveEnrollmentEntity in ghix-entity." , exception);
			 throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		 
		}
				
		LOGGER.info("saveEnrollmentEntity: END");		
		return responseJSON ; 
	}

	private EnrollmentEntityResponseDTO getEnrollmentEntityByUserId(
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getEnrollmentEntityByUserId: START");

		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		int userId = enrollmentEntityRequestDTO.getUserId();
		EnrollmentEntity enrollEntity = enrollmentEntityService.findEnrollmentEntityByUserId(userId);
		if (enrollEntity != null) {
			enrollmentEntityDTO.setEnrollmentEntity(enrollEntity);
		}

		LOGGER.info("getEnrollmentEntityByUserId: END");

		return enrollmentEntityDTO;
	}

	private EnrollmentEntityResponseDTO getEnrollmentEntityByUserOrRecordId(
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getEnrollmentEntityByUserOrRecordId: START");

		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		int userId = enrollmentEntityRequestDTO.getUserId();
		EnrollmentEntity enrollEntity = enrollmentEntityService.findEnrollmentEntityByUserId(userId);

		if (enrollEntity != null) {
			LOGGER.debug("Existing Enrollment Entity record");
			enrollmentEntityDTO.setEnrollmentEntity(enrollEntity);
		} else {

			// Could be AHBX-GI redirection/First time signup
			LOGGER.debug("Finding Enrollment Entity on the basis of Record Id");

			String recordId = enrollmentEntityRequestDTO.getRecordId();

			// In case of AHBX-GI redirection to Entity Information Page
			if (StringUtils.isNotBlank(recordId)) {

				LOGGER.debug("AHBX-GI redirection to Entity Information Page");

				Integer intRecordId = recordId != null ? Integer.parseInt(recordId) : null;
				
				if(null == intRecordId) {
					LOGGER.warn("No record identifier found in the request.");
				}
				else {
					// Retrieve Enrollment Entity based on the recordId
					enrollEntity = enrollmentEntityService.findEnrollmentEntityById(intRecordId);
				}

				LOGGER.debug("Setting Enrollment Entity in Response DTO");
				enrollmentEntityDTO.setEnrollmentEntity(enrollEntity);
			} else {

				// First time sign up
				LOGGER.debug("First time Enrollment Entity sign up");
				enrollmentEntityDTO.setEnrollmentEntity(null);
			}
		}

		LOGGER.info("getEnrollmentEntityByUserOrRecordId: END");

		return enrollmentEntityDTO;
	}

	/**
	 * Retrieves Site Details for Logged in Entity
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getsitedetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String siteDetail(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("siteDetail: START");

		LOGGER.info("Inside siteDetail method for getting list of subsite details");
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON = null;
		try {

			EnrollmentEntity enrollmentEntity = enrollmentEntityService
					.findEnrollmentEntityByUserId(enrollmentEntityRequestDTO.getUserId());
			if (enrollmentEntity != null) {

				List<Site> sites = siteService.findSiteByEntity(enrollmentEntity);
				if (!sites.isEmpty()) {
					List<Site> a = new ArrayList<Site>();
					for (Site siteCounter : sites) { 
						if(siteCounter != null) {
							if(siteCounter.getPhysicalLocation() == null)
								{
									siteCounter.setPhysicalLocation(new EntityLocation());
								}
							// Iterate through a list
															// of sites
							// retrieves SiteLanguages and Location Hrs only is
							// siteType is Primary
							if (PRIMARY.equalsIgnoreCase(siteCounter.getSiteType())) {
	
								// set siteLanguage for primary site in response
								// object
								enrollmentEntityDTO.setSiteLanguages(siteLanguageService
										.findSiteLanguagesBySite(siteCounter));
								// set siteLocationHours for primary site in
								// response object
								enrollmentEntityDTO.setSiteLocationHoursList(siteLocationHoursService
										.findSiteLocationHoursBySite(siteCounter));
							}
							a.add(siteCounter); // Add Subsites to arrayList so that
												// that they can be shown in the
												// subsite list table
						}
					}
					enrollmentEntityDTO.setSiteList(a);
					enrollmentEntityDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
					enrollmentEntityDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
				} else {
					// setting response object in case no site returned for an
					// enrollment entity
					enrollmentEntityDTO.setResponseCode(TWO_HUNDRED_ONE);
					enrollmentEntityDTO.setResponseDescription("No Site Available");
				}
			} else {
				LOGGER.error("Invalid Input");
				enrollmentEntityDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				enrollmentEntityDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
			
		} catch (Exception exception) {
			enrollmentEntityDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			enrollmentEntityDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Error Occured while DB transaction", exception);
		}
		// Marshaling the response before it is sent out to ghix-web
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in siteDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("siteDetail: END");

		return responseJSON;  
	}

	/**
	 * Saves Site Details for Logged in Entity
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String
	 */
	@RequestMapping(value = "/savesitedetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String postsiteDetail(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("postsiteDetail: START");

		LOGGER.info("Inside postsiteDetail method for saving site details");
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		
		Site siteReceived = enrollmentEntityRequestDTO.getSite();
		EnrollmentEntity entity = null;
		boolean isPrimarySiteExist = false;
		int id=enrollmentEntityRequestDTO.getUserId();
		
		if (siteReceived != null) {
				entity = enrollmentEntityService
					.findEnrollmentEntityByUserId(id);
			siteReceived.setEntity(entity);
			//for validating if primary site already exist
			//check if request of primary site add
			
			if(!isPrimarySiteExist(siteReceived, entity)){
				if(checkEntityLocation(siteReceived.getMailingLocation())){
					ZipCode zipCode=zipCodeService.findByZipCode(siteReceived.getMailingLocation().getZip().toString());
					if(zipCode!=null){
						siteReceived.getMailingLocation().setLat(zipCode.getLat());
						siteReceived.getMailingLocation().setLon(zipCode.getLon());
					}
				}
				if(checkEntityLocation(siteReceived.getPhysicalLocation())){
					ZipCode zipCode=zipCodeService.findByZipCode(siteReceived.getPhysicalLocation().getZip().toString());
					if(zipCode!=null){
						siteReceived.getPhysicalLocation().setLat(zipCode.getLat());
						siteReceived.getPhysicalLocation().setLon(zipCode.getLon());
					}
				}
				
				//Added for HIX-23621
				//Retrieve the current site if update request
				if(siteReceived.getId() !=0){
					Site siteFromDB =  siteService.findByEESiteId(siteReceived.getId());
					if(siteFromDB != null){
						siteReceived.setCreatedBy(siteFromDB.getCreatedBy());
						siteReceived.setCreatedOn(siteFromDB.getCreatedOn());
					}
				}
				//HIX-23621 ends
				// saves the siteDetail
				Site siteSaved =saveSiteLocationAndHours(enrollmentEntityRequestDTO, siteReceived);
				
				
				//Trigger for IND35 when Primary Site is updated
				if(entity!=null && !EntityConstants.STATUS_INCOMPLETE.equalsIgnoreCase(entity.getRegistrationStatus()) && siteSaved!=null && Site.site_type.PRIMARY.toString().equals(siteSaved.getSiteType())){
					//enrollmentEntityService.sendEnrollmentEntityInfo(entity);
					entityIndTriggerService.triggerInd35(entity);
				}
					
				enrollmentEntityResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				enrollmentEntityResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
				enrollmentEntityResponseDTO.setEnrollmentEntity(entity);
			}else{
				enrollmentEntityResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				enrollmentEntityResponseDTO.setResponseDescription("PrimarySiteExist");
			}

		} else {
			LOGGER.error("Invalid Input");
			enrollmentEntityResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			enrollmentEntityResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in postsiteDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("postsiteDetail: END");

		return responseJSON;
	}

	/**
	 * @param enrollmentEntityRequestDTO
	 * @param siteReceived
	 */
	private Site saveSiteLocationAndHours( EnrollmentEntityRequestDTO enrollmentEntityRequestDTO, Site siteReceived) {
		SiteLanguages siteLanguagesRetrieved;
		SiteLanguages siteLanguages;
		Site siteSaved;
		List<SiteLocationHours> siteLocationHoursList;
		siteSaved = siteService.saveSite(siteReceived);
		if (enrollmentEntityRequestDTO.getSiteLanguages() != null) {
			siteLanguagesRetrieved = enrollmentEntityRequestDTO.getSiteLanguages();
			// set Site in siteLanguage object
			siteLanguagesRetrieved.setSite(siteSaved);
			siteLanguages = siteLanguageService.saveSiteLanguage(siteLanguagesRetrieved);
		}
		siteLocationHoursList = enrollmentEntityRequestDTO.getSiteLocationHoursList();
		// iterate through received list of SiteLocationHours
		if (siteLocationHoursList != null) {
			for (SiteLocationHours siteLocationHours : siteLocationHoursList) {
				// set Site in siteLocationHours object
				siteLocationHours.setSite(siteSaved);
			}
			// save siteLocationHours
			siteLocationHoursService.saveSiteLocationHours(siteSaved, siteLocationHoursList);
		}
		return siteSaved;
	}

	/**
	 * @param entityLocation
	 * @return
	 */
	private boolean checkEntityLocation(EntityLocation entityLocation) {
		return (entityLocation!=null && (entityLocation.getLat() == 0.0) &&(entityLocation.getLon() == 0.0) && entityLocation.getZip()!=null);
	}

	/**
	 * @param siteReceived
	 * @param entity
	 * @return
	 */
	private boolean isPrimarySiteExist(Site siteReceived,
			EnrollmentEntity entity) {
		boolean isPrimarySiteExist = false;
		if(siteReceived.getId() ==0 && siteReceived.getSiteType().equalsIgnoreCase(PRIMARY)){
			//request to save for primary site
			//find existing primary site for entity
			List<Site> siteList = siteService.findSiteByEntity(entity);
			for(Site site : siteList){
				if(site.getSiteType().equalsIgnoreCase(PRIMARY)){
					isPrimarySiteExist = true;
					break;
				}
			}
		}
		return isPrimarySiteExist;
	}

	/**
	 * Retrieve Site Details for Logged in Entity by id
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String
	 */
	@RequestMapping(value = "/getsitedetailbyId", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String siteDetailById(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("siteDetailById: START");

		LOGGER.info("Getting Site Details for a specific site Id");
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		List<SiteLocationHours> siteLocationHoursList = null;
		SiteLanguages siteLanguages = null;
		Site retrievedSite = null;
		Site site = enrollmentEntityRequestDTO.getSite();
		if (site != null) {
			try {
				// retrieving site details by id
				retrievedSite = siteService.findByEESiteId(site.getId());
				if (retrievedSite != null && retrievedSite.getPhysicalLocation() == null)
				{
					retrievedSite.setPhysicalLocation(new EntityLocation());
				}
				enrollmentEntityResponseDTO.setSite(retrievedSite);

				// find siteLanguages by site sent from web layer
				siteLanguages = siteLanguageService.findSiteLanguagesBySite(retrievedSite);
				enrollmentEntityResponseDTO.setSiteLanguages(siteLanguages);

				// retrieves siteLocation hours list from site sent from web
				// layer
				siteLocationHoursList = siteLocationHoursService.findSiteLocationHoursBySite(retrievedSite);
				enrollmentEntityResponseDTO.setSiteLocationHoursList(siteLocationHoursList);
				enrollmentEntityResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				enrollmentEntityResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {
				enrollmentEntityResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				enrollmentEntityResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);

				LOGGER.error("Error Occured while DB transaction", exception);

			}
		} else {
			LOGGER.error("Invalid Input");
			enrollmentEntityResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			enrollmentEntityResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in siteDetailById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("Marshalling EnrollmentEntity Response DTO" + enrollmentEntityResponseDTO);

		LOGGER.info("siteDetailById: END");

		return responseJSON ; // marshalling response DTO
														// into an XML string
	}

	/**
	 * Retrieve Site Details for Logged in Entity by Type
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String
	 */
	@RequestMapping(value = "/getsitebytype", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getSiteByType(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getSiteByType: START");

		LOGGER.info("Entering getSiteByType method");
		String siteType = null;
		Site site = null;
		List<Site> sitesList = null;
		List<SiteLocationHours> siteLocationHoursList = null;
		SiteLanguages siteLanguages = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		try {
			EnrollmentEntity enrollmentEntity = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO)
					.getEnrollmentEntity();
			siteType = enrollmentEntityRequestDTO.getSiteType();
			// retrieves siteList by enrollment entity and site type
			sitesList = siteService.findByEnrollmentEntityAndSiteType(enrollmentEntity.getId(), siteType);
			if (!sitesList.isEmpty()) {
				site = sitesList.get(0);

				// set enrollmententityResponse DTO with site object
				enrollmentEntityResponseDTO.setSite(site);
				if (site != null) {
					siteLanguages = siteLanguageService.findSiteLanguagesBySite(site);
					if (siteLanguages != null) {
						enrollmentEntityResponseDTO.setSiteLanguages(siteLanguages);
					}

					siteLocationHoursList = siteLocationHoursService.findSiteLocationHoursBySite(site);
					if (!siteLocationHoursList.isEmpty()) {
						enrollmentEntityResponseDTO.setSiteLocationHoursList(siteLocationHoursList);
					}
					
					LOGGER.debug("Site Retrieved is" + site.getSiteLocationName());
					LOGGER.debug("Site Type" + site.getSiteType());
				}

				enrollmentEntityResponseDTO.setErrCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				enrollmentEntityResponseDTO.setErrMsg(EntityConstants.RESPONSE_DESC_SUCCESS);
			}
		} catch (Exception exception) {
			LOGGER.error("Error occcured in db transaction", exception);
			enrollmentEntityResponseDTO.setErrCode(EntityConstants.RESPONSE_CODE_FAILURE);
			enrollmentEntityResponseDTO.setErrMsg(EntityConstants.RESPONSE_DESC_FAILURE);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getSiteByType in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting getSiteByType method");

		LOGGER.info("getSiteByType: END");

		return responseJSON;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getpaymentdetails", method = RequestMethod.POST)
	@ResponseBody
	public String retrievePaymentDetails(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrievePaymentDetails: START");

		LOGGER.info("inside retrieve population served info");
		Integer moduleId = enrollmentEntityRequestDTO.getModuleId();
		String entity = enrollmentEntityRequestDTO.getEntityType();
		PaymentMethods paymentMethods = enrollmentEntityService.findPaymentMethodDetailsForEntity(moduleId, entity);
		if(paymentMethods != null && paymentMethods.getPaymentType() == PaymentMethods.PaymentType.EFT){
	 	 	String paymentMethodsString = restTemplate.getForObject(FinanceServiceEndPoints.FIND_PAYMENT_METHOD_PCI+paymentMethods.getId(), String.class);
	 	 	paymentMethods = getPaymentMethods(paymentMethodsString);
 	 	}
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityDTO.setPaymentMethods(paymentMethods);

		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrievePaymentDetails in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("retrievePaymentDetails: END");

		return responseJSON;
	}

	private  PaymentMethods getPaymentMethods(String paymentMethodsString){
	 	XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
	 	PaymentMethodResponse  paymentMethodResponse = (PaymentMethodResponse) xstream.fromXML(paymentMethodsString);
	 	return paymentMethodResponse.getPaymentMethods();
 	}
	
	/**
	 * Retrieves PaymentMethod object by paymentId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getpaymentdetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String retrievePaymentDetailsById(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrievePaymentDetailsById: START");

		LOGGER.info("inside retrieve PaymentDetailsById served info");
		PaymentMethods inputPaymentMethods = enrollmentEntityRequestDTO.getPaymentMethods();
		LOGGER.info("Retrieving Payment Detail REST Call Starts");
 	 	String paymentMethodsStr = ghixRestServiceInvoker.invokeRestService(inputPaymentMethods.getId(),FinanceServiceEndPoints.PAYMENTMETHODS_BY_ID, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
 	 	LOGGER.info("Retrieving Payment Detail REST Call Ends");
 	 	
 	 	PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) GhixUtils.xStreamHibernateXmlMashaling().fromXML(paymentMethodsStr);
 	 	PaymentMethods paymentMethods = null;
 	 	Map<String, Object> paymentMethodMap = paymentMethodResponse.getPaymentMethodMap();
 	 	if(paymentMethodMap != null && paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY) != null){
	 	 	List<PaymentMethods> paymentMethodList = (List<PaymentMethods>)paymentMethodMap.get(GhixConstants.FinancePaymentMethodResponse.PAYMENT_METHOD_LIST_KEY);
	 	 	paymentMethods =  paymentMethodList != null ? paymentMethodList.get(0) : null;
 	 	}
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityDTO.setPaymentMethods(paymentMethods);
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrievePaymentDetailsById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("retrievePaymentDetailsById: END");

		return responseJSON;
	}

	/**
	 * save PaymentMethod object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 * @throws GIException 
	 */
	@RequestMapping(value = "/savepaymentdetails", method = RequestMethod.POST)
	@ResponseBody
	public String savePaymentDetails(@RequestBody String enrollmentEntityRequesttring) throws GIException {

		LOGGER.info("savePaymentDetails: START");

		LOGGER.info("inside SavePaymentDetails served info");
		PaymentMethods paymentMethodsNew = null;
		PaymentMethods paymentMethods = null;
		EnrollmentEntity enrollmentEntityNew = null;
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();

		LOGGER.info("inside SavePaymentDetails served info");
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
		} catch ( Exception exception) {
			LOGGER.error("Exception occured while demarsheling object in savePaymentDetails in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}  
		paymentMethods = (PaymentMethods) enrollmentEntityRequestDTO.getPaymentMethods();
		EnrollmentEntity enrollmentEntity = (EnrollmentEntity) enrollmentEntityRequestDTO.getEnrollmentEntity();

		if ("1".equals(enrollmentEntity.getReceivedPayments())) {
			try {
 	 	 	// Exception handled for new PCI flow.
 	 	 	LOGGER.info("Retrieving Save Payment Detail REST Call Starts");
 	 	 	String paymentMethodsStr = ghixRestServiceInvoker.invokeRestService(paymentMethods,FinanceServiceEndPoints.SAVE_PAYMENT_METHOD, HttpMethod.POST, GIRuntimeException.Component.AEE.toString());
 	 	 	LOGGER.info("Retrieving Save Payment Detail REST Call Ends");
 	 	 	
 	 	 	PaymentMethodResponse paymentMethodResponse = (PaymentMethodResponse) GhixUtils.xStreamHibernateXmlMashaling().fromXML(paymentMethodsStr);
 	 	 	if(paymentMethodResponse != null) {
 	 	 		if(paymentMethodResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)){
 	 	 			throw new GIException(201, paymentMethodResponse.getErrMsg(), "HIGH");
 	 	 		}
 	 	 		
 	 	 		paymentMethodsNew = paymentMethodResponse.getPaymentMethods();
 	 	 	}
 	 	 	} catch (Exception e) {
	 	 	 	LOGGER.error("Exception caught: ", e);
	 	 	 	enrollmentEntityDTO.setErrCode(201);
	 	 	 	enrollmentEntityDTO.setErrMsg(e.getMessage());
	 	 	 	enrollmentEntityDTO.setStatus(GhixConstants.RESPONSE_FAILURE);
	 	 	 	try {
					return JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString(enrollmentEntityDTO);
				} catch (JsonProcessingException exception) {
					LOGGER.error("Exception occured in savePaymentDetails in ghix-entity." , exception);
					throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
			
				}
 	 	 	}
			enrollmentEntityNew = enrollmentEntityService.saveEnrollmentEntity(enrollmentEntity);
		}

		if ("Incomplete".equals(enrollmentEntity.getRegistrationStatus())) {
			enrollmentEntity.setRegistrationStatus("Pending");
			enrollmentEntity.setStatusChangeDate(new TSDate());
			enrollmentEntityNew = enrollmentEntityService.saveEnrollmentEntity(enrollmentEntity);
		}

		enrollmentEntityDTO.setPaymentMethods(paymentMethodsNew);
		enrollmentEntityDTO.setEnrollmentEntity(enrollmentEntityNew);

		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in savePaymentDetails in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("savePaymentDetails: END");

		return responseJSON ;

	}

	/**
	 * save EnrollmentEntity object with changed Status
	 * 
	 * @param requestXML
	 * @return String response DTO String with EnrollmentEntity object
	 */
	@RequestMapping(value = "/saveenrollmententityStatus", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public void saveEnrollmentEntityStatus(@RequestBody String enrollmentEntityRequesttring) {

		LOGGER.info("saveEnrollmentEntityStatus: START");

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
		} catch ( Exception exception) {
			LOGGER.error("Exception occured in saveEnrollmentEntityStatus in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		EnrollmentEntity enrollEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();

		LOGGER.info("saveEnrollmentEntityStatus: END");

		enrollmentEntityService.saveEnrollmentEntity(enrollEntity);

	}

	/**
	 * Retrieves EnrollmentEntity object by UserId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with EnrollmentEntity object
	 */
	@RequestMapping(value = "/getentitydetailsbyuserid", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEnrollmentEntityByUserId(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrieveEnrollmentEntityByUserId: START");

		LOGGER.info("inside retrieve EnrollmentEntityByUserId served info");
		Integer userId = enrollmentEntityRequestDTO.getModuleId();
		EnrollmentEntity enrollmentEntity = enrollmentEntityService.findEnrollmentEntityByUserId(userId);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityDTO.setEnrollmentEntity(enrollmentEntity);

		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrieveEnrollmentEntityByUserId in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("retrieveEnrollmentEntityByUserId: END");

		return responseJSON;  
	}

	/**
	 * Retrieves EnrollmentEntity History object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with List<Map<String,object>> object
	 */
	@RequestMapping(value = "/getenrollmententityhistory", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEnrollmentEntityHistory(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrieveEnrollmentEntityHistory: START");

		LOGGER.info("inside retrieve EnrollmentEntityHistory served info");
		Integer userId = (Integer) enrollmentEntityRequestDTO.getModuleId();
		List<Map<String, Object>> data = enrollmentEntityService.loadEntityrStatusHistory(userId);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityDTO.setEnrollmentEntityHistory(data);

		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrieveEnrollmentEntityHistory in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("retrieveEnrollmentEntityHistory: END");

		return responseJSON;
	}

	@RequestMapping(value = "/getenrollmententitybyid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEnrollmEntentityById(@RequestBody Integer entityId) {

		LOGGER.info("getEnrollmEntentityById: START");

		LOGGER.info("inside retrieve population served info");
		String response = null;
		try {
			
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
			EnrollmentEntity enrollmentEntity = enrollmentEntityService.findEnrollmentEntityById(entityId);
			enrollmentEntityResponseDTO.setEnrollmentEntity(enrollmentEntity);
			
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class);
			response = writer.writeValueAsString(enrollmentEntityResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception while retrieving enrollment entity : ", exception);
	 	}

		LOGGER.info("getEnrollmEntentityById: END");
		return response;
	}

	@RequestMapping(value = "/getpopulationserveddetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getPopulationServedDetail(@RequestBody String strEnrollmentEntityRequestDTO) {

		LOGGER.info("getPopulationServedDetail: START");

		LOGGER.info("Retrieve population served information");
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(strEnrollmentEntityRequestDTO);
		} catch ( Exception exception) {
			LOGGER.error("Exception occured in getPopulationServedDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		try {

			// 1. Get Enrollment Entity for logged in user
			EnrollmentEntity enrollmentEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
			if (enrollmentEntity == null || enrollmentEntity.getId() == 0) {
				enrollmentEntityResponseDTO = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO);
				enrollmentEntity = enrollmentEntityResponseDTO.getEnrollmentEntity();
			}

			// 2. Get associated population served data if any and set response
			// DTO
			enrollmentEntityResponseDTO = getAssociatedPopulationServedInformation(enrollmentEntity);

		} catch (Exception e) {
			if(null != enrollmentEntityResponseDTO) {
				LOGGER.error(
					"Exception while retrieving enrollment entity : " + enrollmentEntityResponseDTO.getResponseCode()
							+ " : ", e);
			}
		}
		LOGGER.info("Returning population served information");

		// Marshaling the response before it is sent out to ghix-web
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getPopulationServedDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getPopulationServedDetail: END");

		return responseJSON;
	}

	private EnrollmentEntityResponseDTO getAssociatedPopulationServedInformation(EnrollmentEntity enrollmentEntity) {

		LOGGER.info("getAssociatedPopulationServedInformation: START");

		List<PopulationLanguage> associatedLanguages = populationLanguageService.findByEntityId(enrollmentEntity
				.getId());
		List<PopulationEthnicity> associatedEthnicities = populationEthnicityService.findByEntityId(enrollmentEntity
				.getId());
		List<PopulationIndustry> associatedIndustries = populationIndustryService.findByEntityId(enrollmentEntity
				.getId());

		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityResponseDTO.setEnrollmentEntity(enrollmentEntity);
		setAssociatedPopulationServedMap(associatedLanguages, associatedEthnicities, associatedIndustries,
				enrollmentEntityResponseDTO);

		LOGGER.info("getAssociatedPopulationServedInformation: END");

		return enrollmentEntityResponseDTO;
	}

	@RequestMapping(value = "/savepopulationserveddetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String savePopulationServedDetail(@RequestBody String strEnrollmentEntityRequestDTO) {

		LOGGER.info("savePopulationServedDetail: START");
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		String responseJSON=null;
		LOGGER.info("inside retrieve population served info");
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(strEnrollmentEntityRequestDTO);
			PopulationServedWrapper populationServedWrapper = enrollmentEntityRequestDTO.getPopulationServedWrapper();
			
			enrollmentEntityResponseDTO = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO);
			EnrollmentEntity enrollmentEntity = enrollmentEntityResponseDTO.getEnrollmentEntity();

			if (enrollmentEntity != null) {
				if (populationServedWrapper != null) {
					// 1. Save languages
					Map<String, PopulationLanguage> populationLanguagesMap = populationServedWrapper
							.getPopulationLanguages();
					List<PopulationLanguage> populationLanguagesList = (populationLanguagesMap != null && !populationLanguagesMap
							.isEmpty()) ? new ArrayList<PopulationLanguage>(populationLanguagesMap.values())
							: new ArrayList<PopulationLanguage>();
					populationLanguageService.saveAssociatedLanguages(enrollmentEntity, populationLanguagesList);

					// 2. Save ethnicities
					Map<String, PopulationEthnicity> populationEthnicitiesMap = populationServedWrapper
							.getPopulationEthnicities();
					List<PopulationEthnicity> populationEthnicitiesList = (populationEthnicitiesMap != null && !populationEthnicitiesMap
							.isEmpty()) ? new ArrayList<PopulationEthnicity>(populationEthnicitiesMap.values())
							: new ArrayList<PopulationEthnicity>();
					populationEthnicityService.saveAssociatedEthnicities(enrollmentEntity, populationEthnicitiesList);

					// 3. Save industries
					Map<String, PopulationIndustry> populationIndustriesMap = populationServedWrapper.getPopulationIndustries();
					List<PopulationIndustry> populationIndustriesList = (populationIndustriesMap != null && !populationIndustriesMap
							.isEmpty()) ? new ArrayList<PopulationIndustry>(populationIndustriesMap.values())
							: new ArrayList<PopulationIndustry>();
					populationIndustryService.saveAssociatedIndustries(enrollmentEntity, populationIndustriesList);
				}
			}	
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in savePopulationServedDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("savePopulationServedDetail: END");

		return responseJSON;
	}

	private void setAssociatedPopulationServedMap(List<PopulationLanguage> associatedLanguages,
			List<PopulationEthnicity> associatedEthnicities, List<PopulationIndustry> associatedIndustries,
			EnrollmentEntityResponseDTO enrollmentEntityResponseDTO) {

		LOGGER.info("setAssociatedPopulationServedMap: START");

		List<LookupValue> lookupLanguages = (List<LookupValue>) lookupService.getLookupValueList("ENTITY_LANGUAGE");

		List<LookupValue> lookupEthnicities = (List<LookupValue>) lookupService.getLookupValueList("ENTITY_ETHNICITY");

		List<LookupValue> lookupIndustries = (List<LookupValue>) lookupService.getLookupValueList("ENTITY_INDUSTRY");

		Map<String, PopulationLanguage> associatedLanguagesMap = getAssociatedLanguagesMap(associatedLanguages, lookupLanguages);
		Map<String, PopulationEthnicity> associatedEthnicitiesMap = getAssociatedEthnicitiesMap(associatedEthnicities, lookupEthnicities);
		Map<String, PopulationIndustry> associatedIndustriesMap = getAssociatedIndustriesMap(associatedIndustries, lookupIndustries);

		PopulationServedWrapper populationServedWrapper = new PopulationServedWrapper();
		populationServedWrapper.setPopulationLanguages(associatedLanguagesMap != null
				&& !associatedLanguagesMap.isEmpty() ? associatedLanguagesMap : null);
		populationServedWrapper.setPopulationEthnicities(associatedEthnicitiesMap != null
				&& !associatedEthnicitiesMap.isEmpty() ? associatedEthnicitiesMap : null);
		populationServedWrapper.setPopulationIndustries(associatedIndustriesMap != null
				&& !associatedIndustriesMap.isEmpty() ? associatedIndustriesMap : null);

		enrollmentEntityResponseDTO.setPopulationServedWrapper(populationServedWrapper);

		LOGGER.info("setAssociatedPopulationServedMap: END");
	}

	/**
	 * @param associatedLanguages
	 * @param lookupLanguages
	 * @return
	 */
	private Map<String, PopulationLanguage> getAssociatedLanguagesMap(
			List<PopulationLanguage> associatedLanguages,
			List<LookupValue> lookupLanguages) {
		Map<String, PopulationLanguage> associatedLanguagesMap = new HashMap<String, PopulationLanguage>();

		for (LookupValue lookupLangVal : lookupLanguages) {
			for (PopulationLanguage lang : associatedLanguages) {
				if (lookupLangVal.getLookupValueLabel().equalsIgnoreCase(lang.getLanguage())) {
					associatedLanguagesMap.put(lookupLangVal.getLookupValueLabel(), lang);
				} else {
					associatedLanguagesMap.put(lang.getLanguage(), lang);
				}
			}
		}
		return associatedLanguagesMap;
	}

	/**
	 * @param associatedEthnicities
	 * @param lookupEthnicities
	 * @return
	 */
	private Map<String, PopulationEthnicity> getAssociatedEthnicitiesMap(
			List<PopulationEthnicity> associatedEthnicities,
			List<LookupValue> lookupEthnicities) {
		Map<String, PopulationEthnicity> associatedEthnicitiesMap = new HashMap<String, PopulationEthnicity>();

		for (LookupValue lookupEthnicityVal : lookupEthnicities) {
			for (PopulationEthnicity ethnicity : associatedEthnicities) {
				if (lookupEthnicityVal.getLookupValueLabel().equalsIgnoreCase(ethnicity.getEthnicity())) {
					associatedEthnicitiesMap.put(lookupEthnicityVal.getLookupValueLabel(), ethnicity);
				} else {
					associatedEthnicitiesMap.put(ethnicity.getEthnicity(), ethnicity);
				}
			}
		}
		return associatedEthnicitiesMap;
	}

	/**
	 * @param associatedIndustries
	 * @param lookupIndustries
	 * @return
	 */
	private Map<String, PopulationIndustry> getAssociatedIndustriesMap(List<PopulationIndustry> associatedIndustries,
			List<LookupValue> lookupIndustries) {
		Map<String, PopulationIndustry> associatedIndustriesMap = new HashMap<String, PopulationIndustry>();

		for (LookupValue lookupIndustryVal : lookupIndustries) {
			for (PopulationIndustry industry : associatedIndustries) {
				if (lookupIndustryVal.getLookupValueLabel().equalsIgnoreCase(industry.getIndustry())) {
					associatedIndustriesMap.put(lookupIndustryVal.getLookupValueLabel(), industry);
				} else {
					associatedIndustriesMap.put(industry.getIndustry(), industry);
				}
			}
		}
		return associatedIndustriesMap;
	}

	@RequestMapping(value = "/getEnrollmentEntitySites", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEnrollmentEntitySites(@RequestBody String requestJSON) {

		LOGGER.info("getEnrollmentEntitySites: START");

		LOGGER.debug("Get List of Enrollment Entity Sites Starts: Received Rest call from ghix-web.");
		EnrollmentEntityResponseDTO responseDTO = new EnrollmentEntityResponseDTO();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(requestJSON);
		}  catch ( Exception exception) {
			LOGGER.error("Exception occured in getEnrollmentEntitySites in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
	
		}
		EnrollmentEntity enrollmentEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();

		if (enrollmentEntity != null) {
			try {
				List<Site> sites = siteService.findSiteByEntity(enrollmentEntityRequestDTO.getEnrollmentEntity());
				responseDTO.setSiteList(sites);
				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				responseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {
				LOGGER.error(
						"Get List of Enrollment Entity Sites : Exception occurred while getting Enrollment Entity Sites : ",
						exception);

				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				responseDTO.setResponseDescription(exception.getMessage());
			}
		} else {
			LOGGER.error("Get List of Enrollment Entity Sites : Invalid input. Unable to get Enrollment Entity Sites");

			responseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			responseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		LOGGER.debug("Get List of Enrollment Entity Sites Ends.");
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( responseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getEnrollmentEntitySites in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getEnrollmentEntitySites: END");

		return responseJSON;
	}

	@RequestMapping(value = "/getEnrollmentEntitySitesbysitetype", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEnrollmentEntitySitesBySiteType(@RequestBody String requestJSON) {

		LOGGER.info("getEnrollmentEntitySitesBySiteType: START");

		LOGGER.debug("Get List of Enrollment Entity Sites By Site Type Starts : Received Rest call from ghix-web.");
		EnrollmentEntityResponseDTO responseDTO = new EnrollmentEntityResponseDTO();
		List<Site> sites = null;

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(requestJSON);
		} catch ( Exception exception) {
			LOGGER.error("Exception occured in getEnrollmentEntitySitesBySiteType in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		EnrollmentEntity enrollmentEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();

		if (enrollmentEntity != null) {
			try {
				sites = siteService.findByEnrollmentEntityAndSiteType(enrollmentEntity.getId(),
						enrollmentEntityRequestDTO.getSiteType());
				responseDTO.setSiteList(sites);
				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				responseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {
				LOGGER.error(
						"Get List of Enrollment Entity Sites By Site Type : Exception occurred while getting Enrollment Entity Sites : ",
						exception);

				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				responseDTO.setResponseDescription(exception.getMessage());
			}
		} else {
			LOGGER.error("Get List of Enrollment Entity Sites By Site Type : Invalid input. Unable to get Enrollment Entity Sites");

			responseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			responseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		LOGGER.debug("Get List of Enrollment Entity Sites By Site Type Ends :");
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( responseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in siteDetailById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getEnrollmentEntitySitesBySiteType: END");

		return responseJSON ;
	}

	@RequestMapping(value = "/populatenavigationmenumap", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String populateNavigationMenuMap(@RequestBody String requestJSON) {

		LOGGER.info("populateNavigationMenuMap: START");

		LOGGER.debug("Populate Left Navigation Map Starts : Received Rest call from ghix-web.");
		EnrollmentEntityResponseDTO responseDTO = new EnrollmentEntityResponseDTO();
		List<Site> listOfSites = null;
		
		List<Assister> listOfAssisters;
		List<String> sortedListOfMapKeys = new ArrayList<String>();
		Map<String, Boolean> navigationMenuMap = new HashMap<String, Boolean>();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(requestJSON);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in populateNavigationMenuMap in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		EnrollmentEntity enrollmentEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
		
		// navigationmenumap populated only when entity record contains
		// entityname
		// and hence is not a partial record populated by IND-65
		if (enrollmentEntity != null && enrollmentEntity.getEntityName() != null) {
			navigationMenuMap.put(ENTITY_INFO, true);

			// IND65 will pre-populate some of the information
			// Do not populate this in map if entity name is null
			if (!EntityUtils.isEmpty(enrollmentEntity.getEntityName())) {
				sortedListOfMapKeys.add(ENTITY_INFO);
			}

			List<PopulationLanguage> populationLanguage = populationLanguageService.findByEntityId(enrollmentEntity.getId());
			List<PopulationEthnicity> ethinicity = populationEthnicityService.findByEntityId(enrollmentEntity.getId());
			
			if (checkIfEmpty(populationLanguage)|| checkIfEmpty(ethinicity)){
				navigationMenuMap.put(POPULATION_SERVED, true);
				sortedListOfMapKeys.add(POPULATION_SERVED);
			}

			listOfSites = siteService.findSiteByEntity(enrollmentEntity);

			updateNavigationMapForSite(listOfSites, sortedListOfMapKeys,
					navigationMenuMap);

			if (enrollmentEntity.getFinContactName() != null) {
				navigationMenuMap.put(ENTITY_CONTACT, true);
				sortedListOfMapKeys.add(ENTITY_CONTACT);
			}

			listOfAssisters = assisterService.findByEnrollmentEntity(enrollmentEntity);
			if (checkIfEmpty(listOfAssisters)) {
				navigationMenuMap.put(ASSISTER, true);
				sortedListOfMapKeys.add(ASSISTER);
			}

			PaymentMethods paymentMethods = enrollmentEntityService.findPaymentMethodDetailsForEntity(
					enrollmentEntity.getId(), ModuleUserService.ENROLLMENTENTITY_MODULE);
			if (paymentMethods != null) {
				navigationMenuMap.put(PAYMENT_INFO, true);
				sortedListOfMapKeys.add(PAYMENT_INFO);
			}
			List<Map<String, Object>> data = enrollmentEntityService.loadEntityrDocumentHistory(enrollmentEntity
					.getId());
			if (data != null) {
				navigationMenuMap.put(DOCUMENT_UPLOAD, true);
				sortedListOfMapKeys.add(DOCUMENT_UPLOAD);
			}
			responseDTO.setNavigationMenuMap(navigationMenuMap);
			responseDTO.setSortedListOfMapKeys(sortedListOfMapKeys);
			responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			responseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

		} else {
			LOGGER.error("Populate Left Navigation Map : Invalid input. Unable to Populate Left Navigation Map");

			responseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			responseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( responseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in populateNavigationMenuMap in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("Populate Left Navigation Map Ends.");

		LOGGER.info("populateNavigationMenuMap: END");

		return responseJSON ; 
	}

	/**
	 * @param listOfSites
	 * @param sortedListOfMapKeys
	 * @param navigationMenuMap
	 */
	private void updateNavigationMapForSite(List<Site> listOfSites,
			List<String> sortedListOfMapKeys,
			Map<String, Boolean> navigationMenuMap) {
		List<SiteLocationHours> listOfSiteLocationHours;
		if (checkIfEmpty(listOfSites)) {
			navigationMenuMap.put("SITE", true);
			listOfSiteLocationHours = siteLocationHoursService.findSiteLocationHoursBySite(listOfSites.get(0));
			if (checkIfEmpty(listOfSiteLocationHours)) {
				navigationMenuMap.put(LOCATION_HOURS, true);
				sortedListOfMapKeys.add(PRIMARY);
			}					
			sortedListOfMapKeys.add("SUBSITE");
		}
	}

	/**
	 * @param temp
	 * @return
	 */
	private <E> boolean checkIfEmpty(List<E> temp) {
		return temp != null && !temp.isEmpty();
	}

	@RequestMapping(value = "/getsitebyid", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getSiteById(@RequestBody String requestJSON) {

		LOGGER.info("getSiteById: START");

		LOGGER.debug("Get Site By ID Starts : Received Rest call from ghix-web.");
		EnrollmentEntityResponseDTO responseDTO = new EnrollmentEntityResponseDTO();

		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(requestJSON);
		}  catch (Exception exception) {
			LOGGER.error("Exception occured in getSiteById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		Site site = enrollmentEntityRequestDTO.getSite();

		if (site != null) {
			try {
				site = siteService.findByEESiteId(site.getId());
				responseDTO.setSite(site);
				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				responseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			} catch (Exception exception) {
				LOGGER.error("Get Site By ID : Exception occurred while getting Site By ID : ", exception);

				responseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
				responseDTO.setResponseDescription(exception.getMessage());
			}
		} else {
			LOGGER.error("Get Site By ID : Invalid input. Unable to get Site By ID");

			responseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
			responseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( responseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getSiteById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.debug("Get Site By ID Ends.");

		LOGGER.info("getSiteById: END");

		return responseJSON;
	}

	/**
	 * Handles Get for Enrollment Entity Contact object
	 * 
	 * @param enrollmentEntityRequesttring
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getenrollmententitycontactdetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getEnrollmentEntityContactDetail(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getEnrollmentEntityContactDetail: START");

		LOGGER.info("Get the Entity's Contact Details");
		EnrollmentEntityResponseDTO enrollmentEntityDTO = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO);

		LOGGER.info("getEnrollmentEntityContactDetail: END");

		// Marshaling the response before it is sent out to ghix-web
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getEnrollmentEntityContactDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		return responseJSON;
	}

	/**
	 * Saves Enrollment Entity's Contact object
	 * 
	 * @param enrollmentEntityRequesttring
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/saveenrollmententitycontactdetail", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEnrollmentEntityContactDetail(@RequestBody String enrollmentEntityRequesttring) {

		LOGGER.info("saveEnrollmentEntityContactDetail: START");

		LOGGER.info("Save the Entity Contact Details");
		EnrollmentEntity enrollEntity = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO;
		try {
			enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
		}catch (Exception exception) {
			LOGGER.error("Exception occured in saveEnrollmentEntityContactDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		enrollEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
		enrollEntity = enrollmentEntityService.saveEnrollmentEntity(enrollEntity);
		// enrollmentEntityRequestDTO.setEnrollmentEntity(enrollmentEntityNew);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityDTO.setEnrollmentEntity(enrollEntity);
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveEnrollmentEntityContactDetail in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Saved the Entity Contact Details");

		LOGGER.info("saveEnrollmentEntityContactDetail: END");

		return responseJSON; 
	}


	/*
	 * Method to fetch all sites ,sitelanguages and sitelocationhours associated
	 * with the logged in entity
	 */
	@RequestMapping(value = "/getAllSites", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_VIEW_ENTITY)
	public String getSitesForEnrollmentEntity(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getSitesForEnrollmentEntity: START");

		EnrollmentEntityResponseDTO response = new EnrollmentEntityResponseDTO();
		EnrollmentEntity enrollmentEntity = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO)
				.getEnrollmentEntity();
		
		Map<Integer, List<SiteLocationHours>> siteLocationHrsMap = new HashMap<Integer, List<SiteLocationHours>>();
		Map<Integer, SiteLanguages> siteLanguagesMap = new HashMap<Integer, SiteLanguages>();

		try {
			// retrieves list of sites by enrollmententity
			List<Site> siteList = siteService.findSiteByEntity(enrollmentEntity);
			response.setSiteList(siteList);

			// loop through siteList
			for (Site site : siteList) {
				// retrieve siteLanguages by site
				SiteLanguages sitelanguage = siteLanguageService.findSiteLanguagesBySite(site);

				String languageSpoken = sitelanguage.getSpokenLanguages();
				List<String> languageSpokenList = EntityUtils.splitStringValues(languageSpoken, ",");
				if(languageSpokenList.contains("null")){
					languageSpokenList.remove("null");
				}
				StringBuilder sb = new StringBuilder();
				for (String languageSpokenStr : languageSpokenList) {
					sb.append(languageSpokenStr);
					sb.append(", ");
				}
				languageSpoken = sb.substring(0, sb.length() - 2);
				sitelanguage.setSpokenLanguages(languageSpoken);

				String languageWritten = sitelanguage.getWrittenLanguages();
				List<String> languageWrittenList = EntityUtils.splitStringValues(languageWritten, ",");
				if(languageWrittenList.contains("null")){
					languageWrittenList.remove("null");
				}
				StringBuilder strBufferWritten = new StringBuilder();
				for (String languageWrittenStr : languageWrittenList) {
					strBufferWritten.append(languageWrittenStr);
					strBufferWritten.append(", ");
				}
				languageWritten = strBufferWritten.substring(0, strBufferWritten.length() - 2);
				sitelanguage.setWrittenLanguages(languageWritten);
				// create a siteLanguageMap indexed by siteId
				siteLanguagesMap.put(site.getId(), sitelanguage);
				List<SiteLocationHours> siteLocationHr = siteLocationHoursService.findSiteLocationHoursBySite(site);
				Collections.sort(siteLocationHr);
				siteLocationHrsMap.put(site.getId(), siteLocationHr);
			}
			// set siteLanguagesMap and siteLocationHrsMap in response object
			response.setSiteLanguagesMap(siteLanguagesMap);
			response.setSiteLocationHoursMap(siteLocationHrsMap);

		} catch (Exception exception) {
			LOGGER.error("Get Site By ID : Exception occurred while getting Site By ID : ", exception);
			response.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			response.setResponseDescription(exception.getMessage());

		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( response)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getSitesForEnrollmentEntity in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("getSitesForEnrollmentEntity: END");

		return responseJSON;
	}

	/**
	 * Saves EnrollmentEntity object
	 * 
	 * @param enrollmentEntityRequesttring
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/saveenrollmententitywithdocument", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize(HAS_PERMISSION_MODEL_ENTITY_EDIT_ENTITY)
	public String saveEnrollmentEntityWithDocumentId(@RequestBody String enrollmentEntityRequesttring) {
		String responseJSON =null;
		LOGGER.info("saveEnrollmentEntityWithDocumentId: START");
		try {
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
			EnrollmentEntity enrollEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
			enrollmentEntityService.saveEnrollmentEntity(enrollEntity);
			enrollmentEntityRequestDTO.setUserId(enrollEntity.getUser().getId());
			EnrollmentEntityResponseDTO enrollmentEntityDTO = getEnrollmentEntityByUserId(enrollmentEntityRequestDTO);

			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveEnrollmentEntityWithDocumentId in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("saveEnrollmentEntityWithDocumentId: END");

		return responseJSON;
	}

	/**
	 * Retrieves EnrollmentEntity History object
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with List<Map<String,object>> object
	 */
	@RequestMapping(value = "/getenrollmententitydocumenthistory", method = RequestMethod.POST)
	@ResponseBody
	public String retrieveEnrollmentEntityDocumentHistory(
			@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("retrieveEnrollmentEntityDocumentHistory: START");

		LOGGER.info("inside retrieve EnrollmentEntityHistory served info");
		Integer id = (Integer) enrollmentEntityRequestDTO.getModuleId();
		List<Map<String, Object>> data = enrollmentEntityService.loadEntityrDocumentHistory(id);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		if (data != null) {
			enrollmentEntityDTO.setEnrollmentEntityHistory(data);
		}
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrieveEnrollmentEntityDocumentHistory in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		LOGGER.info("retrieveEnrollmentEntityDocumentHistory: END");

		return responseJSON;
	}

	/**
	 * delete document from broker document
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with List<Map<String,object>> object
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletedocument", method = RequestMethod.POST)
	@ResponseBody
	public String deleteDocument(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) throws ContentManagementServiceException {

		LOGGER.info("deleteDocument: START");

		LOGGER.info("inside retrieve EnrollmentEntityHistory served info");
		Integer id = (Integer) enrollmentEntityRequestDTO.getModuleId();
		enrollmentEntityService.deleteDocument(id);
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in deleteDocument in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}	
		LOGGER.info("deleteDocument: END");

		return responseJSON;
	}

	/**
	 * 
	 * 
	 * @param enrollmentEntityRequesttring
	 */
	@RequestMapping(value = "/updateReceivedPayment", method = RequestMethod.POST)
	@ResponseBody
	@PreAuthorize("hasPermission(#model, 'ENTITY_EDIT_ASISTER')")
	public String updateEnrollmentEntity(@RequestBody String enrollmentEntityRequesttring) {
		String responseJSON=null;
		LOGGER.info("updateEnrollmentEntity: START");
		try {
			
			EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
			EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReaderForJavaType(EnrollmentEntityRequestDTO.class).readValue(enrollmentEntityRequesttring);
			EnrollmentEntity enrollEntity = enrollmentEntityRequestDTO.getEnrollmentEntity();
			enrollEntity = enrollmentEntityService.saveEnrollmentEntity(enrollEntity);

			enrollmentEntityDTO.setEnrollmentEntity(enrollEntity);

			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in updateEnrollmentEntity in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("updateEnrollmentEntity: END");

		return responseJSON;

	}

	/**
	 * Delete payment method by module id
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value = "/deletepaymentmethod", method = RequestMethod.POST)
	@ResponseBody
	public String deletePaymentMethod(@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO)throws GIException{

		LOGGER.info("deletePaymentMethod: START");

		LOGGER.info("Inside Delete Payment Method");
		Integer moduleId = (Integer) enrollmentEntityRequestDTO.getModuleId();
		EnrollmentEntityResponseDTO enrollmentEntityDTO = new EnrollmentEntityResponseDTO();
		
		String entity = enrollmentEntityRequestDTO.getEntityType();
	 	PaymentMethods payment = enrollmentEntityService.findPaymentMethodDetailsForEntity(moduleId, entity);
		if(payment != null) {
			PaymentMethodRequestDTO paymentMethodRequestDTO = new PaymentMethodRequestDTO();
			paymentMethodRequestDTO.setId(payment.getId());
	 	 	paymentMethodRequestDTO.setPaymentMethodStatus(PaymentStatus.InActive);
	 	 	String paymentMethodsString = restTemplate.postForObject(FinanceServiceEndPoints.MODIFY_PAYMENT_STATUS, paymentMethodRequestDTO, String.class);
 	 	}

		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in deletePaymentMethod in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("deletePaymentMethod: END");

		return responseJSON ;
	}

	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getindividuallistforenrollmententity", method = RequestMethod.POST)
	@ResponseBody
	public String getIndividualListForEnrollmentEntity(
			@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getIndividualListForEnrollmentEntity: START");

		LOGGER.info("Inside getIndividualListForEnrollmentEntity method");
		Map<String, Object> searchCriteria = enrollmentEntityRequestDTO.getSearchCriteria();
		String desigStatus = enrollmentEntityRequestDTO.getDesigStatus();
		Integer startRecord = enrollmentEntityRequestDTO.getStartRecord();
		Integer pageSize = enrollmentEntityRequestDTO.getPageSize();
		EnrollmentEntityResponseDTO enrollmentEntityDTO = enrollmentEntityService.searchIndividualForEnrollmentEntity(
				searchCriteria, desigStatus, startRecord, pageSize);
		
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentEntityResponseDTO.class).writeValueAsString( enrollmentEntityDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in siteDetailById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Returning EE list to ghix-web");

		LOGGER.info("getIndividualListForEnrollmentEntity: END");

		// Marshaling the response before it is sent out to ghix-web
		return responseJSON;
	}
	
	
	/**
	 * Retrieves Individual List
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/getListOfCEC", method = RequestMethod.POST)
	@ResponseBody
	public String getCECForEnrollmentEntity(
			@RequestBody EnrollmentEntityRequestDTO enrollmentEntityRequestDTO) {

		LOGGER.info("getCECList: START");
		Map<String,Object> output = enrollmentEntityService.searchCECToReassign(enrollmentEntityRequestDTO);
		List<Assister> assisters = (List<Assister>) output.get(ASSISTERS);
		int totalRecords = (Integer)output.get(TOTAL_RECORDS);
		LOGGER.info("Returning EE list to ghix-web");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		assisterResponseDTO.setListOfAssisters(assisters);
		assisterResponseDTO.setTotalAssisters(totalRecords);
		LOGGER.info("getCECList: END");
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class).writeValueAsString( assisterResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getCECForEnrollmentEntity in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		// Marshaling the response before it is sent out to ghix-web
		return responseJSON;		
	}
	
	/**
	 * Re-assigning the individuals to CEC
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String in XML format
	 */
	@RequestMapping(value = "/reassignindividuals", method = RequestMethod.POST)
	@ResponseBody
	public String reassigningIndividualsToCEC(
			@RequestBody AssisterRequestDTO assisterRequestDTO) {

		LOGGER.info("reassigningIndividualsToCEC: START");
		//Call service to reassign selected individuals to CEC.
		// First Check if selected individuals are already assigned to CEC.If yes then do not assign those indiduals to CEC otherwise
		// assign them.
		// Need to check all the individuals one by one and then designate to that CEC accordingly.
		List<Household> individualIdsList = new ArrayList<Household>();	
		DesignateAssister designateAssister = null;
		int existingAssisterId;
		individualIdsList = assisterRequestDTO.getListOfIndividuals();
		EntityResponseDTO entityResponseDTO =  new EntityResponseDTO();
		int countIndividualsTransfered = 0;
		for(int individualId:assisterRequestDTO.getListOfIndividualsToReassign()) {
			designateAssister = assisterDesignateService.findAssisterByIndividualId(individualId);
			
			existingAssisterId = designateAssister.getAssisterId();
			// First Check if selected individuals are already assigned to CEC.If yes then do not assign those individuals to CEC
			// otherwise assign them.
			if(designateAssister.getAssisterId() != assisterRequestDTO.getId())
			{	
				designateAssister.setIndividualId(individualId);
				designateAssister.setAssisterId(assisterRequestDTO.getId());
				designateAssister.setEntityId(assisterRequestDTO.getEnrollmentEntityId());
				designateAssister.setStatus(DesignateAssister.Status.Active);
				designateAssister.setEsignBy(assisterRequestDTO.getEsignName());
				designateAssister.setEsignDate(new TSDate());
				designateAssister.setShow_switch_role_popup('Y');
				entityResponseDTO = assisterDesignateService.saveDesignateAssister(designateAssister, existingAssisterId);
				countIndividualsTransfered++;
			}
		}
		
		entityResponseDTO.setCount(countIndividualsTransfered);	
				
		String responseJSON=null;
		try {
			responseJSON =JacksonUtils.getJacksonObjectWriterForJavaType(EntityResponseDTO.class).writeValueAsString( entityResponseDTO)  ;
		} catch (Exception exception) {
			LOGGER.error("Exception occured in siteDetailById in ghix-entity." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}	
		
		LOGGER.info("reassigningIndividualsToCEC: END");
		// Marshaling the response before it is sent out to ghix-web
		return responseJSON;		
	}


}
