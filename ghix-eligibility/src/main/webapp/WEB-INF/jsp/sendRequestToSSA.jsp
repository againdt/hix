<html>

<head>
<meta http-equiv="Content-Language" content="en-us">
<meta http-equiv="Content-Type" content="text/html; charset=windows-1252">
<meta name="GENERATOR" content="Microsoft FrontPage 4.0">
<meta name="ProgId" content="FrontPage.Editor.Document">
<title>Submit Application to State</title>
</head>

<body>
<b>Submit JSON Application to SSA</b>
<form method="POST" action="--WEBBOT-SELF--">
<table border="0" width="100%">
  <tr>
    <td width="100%">
    <p><textarea rows="30" name="S1" cols="80"></textarea></p>
    </td>
  </tr>
  <tr>
    <td width="100%"><select size="1" name="D1">
        <option value="localhost" selected>localhost:8080</option>
        <option value="state">123.123.123.123:8080</option>

      </select></td>
  </tr>
  <tr>
    <td width="100%"><input type="reset" value="Reset" name="B2"> <input type="submit" value="Submit to SSA" name="B1"></td>
  </tr>
</table>
</form>

<p><font face="Courier New" size="2">Note: SSA accepts JSON Application as input
and create entry in all respective tables. SSA submits application to state. Also
updates response in GI_WS_PAYLOAD table.</font></p>

<p><font face="Courier New" size="2">1. Helps to test SSAS (Single Streamline
Application Service) 
<br>2. Helps to test ERP (Eligibility Result Processor) 
<br>3. Helps to test State integration
<br>4. This saves time from filling application through UI and going through HUB/Verifications.</font></p>

</body>

</html>
