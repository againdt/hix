package com.getinsured.hix.batch.externalnotices.service;

import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.hix.batch.repository.BatchNoticeRepository;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.timeshift.util.TSDate;
import com.opencsv.CSVReader;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("noticeBatchService")
@Transactional
public class NoticeBatchServiceImpl implements NoticeBatchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(NoticeBatchService.class);
    private static final String INDIVIDUAL = "INDIVIDUAL";
    private static final int CONTACT_PREFERENCE_EMAIL = 1781;
    private static final int CONTACT_PREFERENCE_EMAIL_AND_POSTAL = 6772;
    private static final int CONTACT_PREFERENCE_POSTAL = 1780;
    private static final int CONTACT_PREFERENCE_NONE = 6773;
    private static final int CONTACT_PREFERENCE = CONTACT_PREFERENCE_EMAIL;
    private static final String UNDELIVERABLE_MAIL_TEMPLATE = "UndeliverableMailNotification";
    private static final String COMMA = ",";
    private static final String EMPTY = "";

    @PersistenceUnit
    private EntityManagerFactory emf;

    @Autowired
    private BatchNoticeRepository noticeRepository;

    @Autowired
    private NoticeService noticeService;

    @Autowired
    private CmrHouseholdRepository cmrHouseholdRepository;

    @Autowired
    private PreferencesService preferencesService;

    @Override
    public void processUndeliverableMail(String filePathName) throws GIRuntimeException {
        LOGGER.debug("processUndeliverableMail::filePathName = {}", filePathName);
        List<Integer> noticeIdList = new ArrayList<>();
        List<String> fileList = new ArrayList<>();

        try {
            try {
                if (Files.isDirectory(new File(filePathName).toPath())) {
                    Stream<Path> walk = Files.walk(Paths.get(filePathName));
                    fileList.addAll(walk.filter(Files::isRegularFile)
                            .map(Path::toString).collect(Collectors.toList()));
                } else {
                    fileList.add(filePathName);
                }
            } catch (Exception ex) {
                LOGGER.error("processUndeliverableMail::error getting paths in directory", ex);
            }

            fileList.forEach(filePath -> {
                try {
                    FileReader filereader = new FileReader(filePath);
                    CSVReader csvReader = new CSVReader(filereader);
                    String[] nextRecord;

                    // Reading data line by line assuming each barcode is on a line
                    while ((nextRecord = csvReader.readNext()) != null) {
                        for (String cell : nextRecord) {
                            try {
                                if (cell.length() != 13 || !validateEAN13Checksum(cell)) {
                                    LOGGER.error("processUndeliverableMail::input number invalid {}", cell);
                                } else {
                                    noticeIdList.add(Integer.valueOf(cell.substring(0, cell.length() - 1)));
                                }
                            } catch (Exception ex) {
                                LOGGER.error("processUndeliverableMail::error validating EAN13 line {}", cell, ex);
                            }
                        }
                    }

                    File file = new File(filePath);
                    if(file.delete()) {
                        if(LOGGER.isDebugEnabled()) {
                            LOGGER.debug("processUndeliverableMail::deletion of file {} was successful", filePath);
                        }
                    } else {
                        LOGGER.error("processUndeliverableMail::deletion of file {} was not successful", filePath);
                    }
                } catch (Exception ex) {
                    LOGGER.error("processUndeliverableMail::error reading CSV File for path {}", filePath, ex);
                }
            });

            if (noticeIdList.size() > 0) {
                LOGGER.debug("processUndeliverableMail::number notice ids: {}", noticeIdList.size());
                setInformationPreferenceToType(noticeIdList.stream()
                        .distinct()
                        .collect(Collectors.toList()), CONTACT_PREFERENCE);
            } else {
                LOGGER.debug("processUndeliverableMail::number notice ids: {}", noticeIdList.size());
                throw new Exception("processUndeliverableMail::availableBrokers object was null");
            }
        } catch (Exception ex) {
            LOGGER.error("processUndeliverableMail::Exception occurred setting broker availability", ex);
            throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), GIRuntimeException.Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
        }
    }

    // ZXing's UPCEANReader validations for checksum
    private boolean validateEAN13Checksum(String barcode) {
        int length = barcode.length();
        if (length == 0) {
            return false;
        }
        int check = Character.digit(barcode.charAt(length - 1), 10);

        LOGGER.debug("validateEAN13Checksum::validating checksum value {} of barcode {}", check, barcode);
        return getStandardChecksum(barcode.subSequence(0, length - 1)) == check;
    }

    // ZXing's UPCEANReader checksum calculator
    private int getStandardChecksum(CharSequence s) throws GIRuntimeException {
        int length = s.length();
        int sum = 0;
        for (int i = length - 1; i >= 0; i -= 2) {
            int digit = s.charAt(i) - '0';
            if (digit < 0 || digit > 9) {
                throw new GIRuntimeException("getStandardChecksum::digit not correct format odd");
            }
            sum += digit;
        }
        sum *= 3;
        for (int i = length - 2; i >= 0; i -= 2) {
            int digit = s.charAt(i) - '0';
            if (digit < 0 || digit > 9) {
                throw new GIRuntimeException("getStandardChecksum::digit not correct format even");
            }
            sum += digit;
        }
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("getStandardChecksum::checksum should be {}", (1000 - sum) % 10);
        }
        return (1000 - sum) % 10;
    }

    private void setInformationPreferenceToType(List<Integer> noticeIdList, int CONTACT_PREFERENCE) {
        try {
            List<Household> cmrHouseholdList = new ArrayList<>();
            if (noticeIdList != null) {
                List<Notice> noticeList = noticeRepository.findNoticesByIdList(noticeIdList);
                if(noticeList != null && noticeList.size() > 0) {
                    for(Notice notice : noticeList) {
                        try {
                            if(notice != null && StringUtils.isNotBlank(notice.getKeyName())) {
                                int keyId = notice.getKeyId();
                                String keyName = notice.getKeyName();
                                if(keyName.equals(INDIVIDUAL)) {
                                    Household cmrHousehold = cmrHouseholdRepository.findOne(keyId);
                                    if (cmrHousehold != null && !cmrHousehold.getPrefContactMethod().equals(CONTACT_PREFERENCE)) {
                                        String noticeId = null;
                                        try {
                                            LOGGER.debug("setInformationPreferenceToType::attempting to send notification to secure inbox");
                                            noticeId = generateNoticeForTemplate(UNDELIVERABLE_MAIL_TEMPLATE, cmrHousehold);
                                        } catch (Exception ex) {
                                            LOGGER.error("setInformationPreferenceToType::error sending notification for household {}", cmrHousehold.getId(), ex);
                                        }

                                        if (noticeId != null) {
                                            LOGGER.debug("setInformationPreferenceToType::notice {} was successful, setting preference", noticeId);
                                            cmrHousehold.setPrefContactMethod(CONTACT_PREFERENCE);
                                            cmrHouseholdList.add(cmrHousehold);
                                        }
                                    }
                                }
                            }
                        } catch (Exception ex) {
                            LOGGER.error("setInformationPreferenceToType::error getting household", ex);
                        }
                    }

                    cmrHouseholdRepository.save(cmrHouseholdList);
                }
            }
        } catch (Exception ex) {
            LOGGER.error("setInformationPreferenceToType::error", ex);
        }
    }

    public String generateNoticeForTemplate(String noticeTemplateName, Household cmrHousehold) throws NoticeServiceException {
        String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
        String fullName = getName(cmrHousehold);
        PreferencesDTO preferencesDTO  = preferencesService.getPreferences(cmrHousehold.getId(), false);
        Location location = getLocationFromDTO(preferencesDTO.getLocationDto());
        String relativePath = "cmr/" + cmrHousehold.getId() + "/";
        String ecmFileName = noticeTemplateName + "_" + cmrHousehold.getId()
                + (new TSDate().getTime()) + ".pdf";

        String emailId = preferencesDTO.getEmailAddress();

        List<String> validEmails = emailId != null ? Arrays.asList(emailId) : null;

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("generateNoticeForTemplate::creating notice for {} using template {}", cmrHousehold.getId(), noticeTemplateName);
        }

        Notice notice = noticeService
                .createModuleNotice(
                        noticeTemplateName,
                        GhixLanguage.US_EN,
                        getReplaceableObjectData(cmrHousehold, location),
                        relativePath,
                        ecmFileName,
                        moduleName,
                        cmrHousehold.getId(),
                        validEmails,
                        DynamicPropertiesUtil
                                .getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME),
                        fullName, location, preferencesDTO.getPrefCommunication());

        return notice.getEcmId();
    }

    private String getName(Household cmrHousehold) {
        String userFullName = "";
        if(StringUtils.isNotBlank(cmrHousehold.getFirstName())) {
            userFullName += cmrHousehold.getFirstName();
        }

        if(StringUtils.isNotBlank(cmrHousehold.getMiddleName())) {
            userFullName += " " + cmrHousehold.getMiddleName();
        }

        if(StringUtils.isNotBlank(cmrHousehold.getLastName())) {
            userFullName += " " + cmrHousehold.getLastName();
        }

        return userFullName;
    }

    private Map<String, Object> getReplaceableObjectData(Household cmrHousehold, Location location) {
        Map<String, Object> tokens = new HashMap<>();
        tokens.put("userFullName", getName(cmrHousehold));
        tokens.put("date", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
        tokens.put(TemplateTokens.EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

        if(StringUtils.isNotBlank(cmrHousehold.getFirstName())) {
            tokens.put("individualFN", cmrHousehold.getFirstName());
        }

        if(StringUtils.isNotBlank(cmrHousehold.getMiddleName())) {
            tokens.put("individualMN", cmrHousehold.getMiddleName());
        }

        if(StringUtils.isNotBlank(cmrHousehold.getLastName())) {
            tokens.put("individualLN", cmrHousehold.getLastName());
        }

        tokens.put(TemplateTokens.ADDRESS_LINE_1, (null== location.getAddress1())?EMPTY:location.getAddress1());
        tokens.put(TemplateTokens.ADDRESS_LINE_2, (null== location.getAddress2())?EMPTY:location.getAddress2());
        tokens.put(TemplateTokens.CITY_NAME, (null== location.getCity())?EMPTY:location.getCity());
        tokens.put(TemplateTokens.STATE_CODE, (null== location.getState())?EMPTY:location.getState());
        tokens.put("zipCode", (null == location.getZip())?EMPTY:location.getZip());

        return tokens;
    }

    public Location getLocationFromDTO(LocationDTO locationDto){
        if (locationDto == null){
            return null;
        }
        Location l = new Location();
        l.setAddress1(locationDto.getAddressLine1());
        l.setAddress2(locationDto.getAddressLine2());
        l.setCity(locationDto.getCity());
        l.setState(locationDto.getState());
        l.setZip(locationDto.getZipcode());
        l.setCounty(locationDto.getCountyName());
        l.setCountycode(locationDto.getCountyCode());
        return l;
    }
}
