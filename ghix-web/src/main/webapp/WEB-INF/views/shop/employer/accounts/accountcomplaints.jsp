<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

 <div class="gutter10">
  <div class="row-fluid">
    <h1 id="skip">${employerName}</h1>
    <div class="row-fluid">
      <div class="span3" id="sidebar">
<!--       	<div class="header"> -->
<!--         	<h4>Account</h4> -->
<!--         </div> -->
<!--          <ul class="nav nav-list"> -->
<!--           <li><a href="#">Company Information</a></li> -->
<!--           <li><a href="#">Authorized Representatives</a></li> -->
<!--           <li><a href="#">Appeals</a></li> -->
<!--           <li class="active"><a href="#">Complaints</a></li> -->
<!--         </ul> -->
      </div>
      <!-- /.sidebar-->
      <div class="span9" id="rightpanel">
        <div class="row-fluid">
        <div class="header">
          <h4>Complaints</h4>
          </div>
          <div class="gutter10">
          <spring:message code = "label.toFileComplaint"/> <spring:message code = "label.otherDetails"/> 
          </div>
          <!--gutter10--> 
        </div>
        <!--row-fluid--> 
      </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
  <!--main--> 
</div>
<!--gutter10-->