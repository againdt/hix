package com.getinsured.hix.indportal.dto.dashboard;

import java.io.Serializable;

public class DashboardSepEventDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private Boolean isIdentified;
	private Boolean isVerified;
	private Boolean isGatedNeedsVerification;

	public Boolean getIsIdentified() {
		return isIdentified;
	}

	public void setIsIdentified(Boolean isIdentified) {
		this.isIdentified = isIdentified;
	}

	public Boolean getIsVerified() {
		return isVerified;
	}

	public void setIsVerified(Boolean isVerified) {
		this.isVerified = isVerified;
	}

	public Boolean getIsGatedNeedsVerification() {
		return isGatedNeedsVerification;
	}

	public void setIsGatedNeedsVerification(Boolean isGatedNeedsVerification) {
		this.isGatedNeedsVerification = isGatedNeedsVerification;
	}

}
