<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>

<%
Integer pageSizeInt = (Integer)request.getAttribute("pageSize");
String reqURI = (String)request.getAttribute("reqURI");
Integer totalResultCount = (Integer)request.getAttribute("resultSize"); 
if(totalResultCount == null){
	totalResultCount = 0;
}
Integer currentPage = (Integer)request.getAttribute("currentPage");
int noOfPage = 0;
 
if(totalResultCount>0){
	if((totalResultCount%pageSizeInt)>0){
		noOfPage = (totalResultCount/pageSizeInt)+1;
	}
	else{
		noOfPage = (totalResultCount/pageSizeInt);
	}
}

%>

	<script>
	    function validate()
	    {
	      var confirmMessage="Are you sure to approve.";

	      if(confirm(confirmMessage)==false)
	      {
	          return false;
	      }
	    }
	</script>
	<div class="row-fluid">
		
		 <!-- 	<div class="span3">
		 		<div class="gutter10">
			 	    <ul class="nav nav-list">
					    <li class="nav-header">Broker Dashboard</li>
					    <li><a href="#">Enroll New Members</a></li>
					    <li class="active"><a href="#">Applications in Progress</a></li>
					    <li><a href="#">Current Members</a></li>
					    <li><a href="#">My Member Trends</a></li>
				    </ul>
			    </div>
		 	</div> -->

		<div class="span12">
			<form class="form-vertical gutter10" id="pendingrequests" name="pendingrequests" action="pendingrequests" method="POST">
				<c:set var="encryptedId" ><encryptor:enc value="${broker.employerId}" isurl="true"/> </c:set>
				<div class="page-header">
				<!-- 	<h1><a name="skip" class="skip">Applications in Pending</a></h1> -->
				</div><!-- end of page-header -->
				<div id="brokerlist">
					<input type="hidden" name="id" id="id" value="">
					<c:choose>
						<c:when test="${fn:length(brokerslist) > 0}">
								<table class="table table-striped">
									<thead>
										<tr class="header">
											<th scope="col"><spring:message code="label.brkemployername"/></th>
											<th scope="col" class="txt-center"><spring:message code="label.brkusertype"/></th>
											<th scope="col" class="txt-center"><spring:message code="label.brkAction"/></th>
										</tr>
									</thead>
									<c:forEach items="${brokerslist}" var="broker">
										<tr>
										    <td ><a href="<c:url value="/shop/employer/viewemployerdetail/${broker.employerId}"/>" id =  "emp${broker.employerId}" class = "empdetail">${broker.employerName}</a></td>
											<!--  <td>${broker.employerName}</td>-->
											<td class="txt-center"><spring:message code="label.employer"/></td>
										   <td class="txt-center"><a class="btn btn-small" href="<c:url value="/broker/dedesignate/${encryptedId}"/>"><spring:message code="label.employer"/><spring:message code="label.brkdecline"/></a> <a href="<c:url value="/broker/approve/${encryptedId}"/>" class="btn btn-primary btn-small"  onclick = "return validate()"><spring:message code="label.brkaccept"/></a></td>
										<!--   <td><a href="<c:url value="/broker/pendingrequests/${broker.employerId}"/>" > Approve</a></td>-->
										

										</tr>
									</c:forEach>
								</table>
							<div class="pagination">
								<ul>
									<% if(noOfPage>1) { %>
										<% if(noOfPage>1 && currentPage>1) { %>
											<li><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=(currentPage-1)%>"><spring:message code="label.brkprev"/></a></li>
										<% }
										for(int iCount=1;iCount<=noOfPage;iCount++) { %>
											<li <% if (currentPage == iCount){ %> class="active" <%} %>><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=iCount%>"><%=iCount%></a></li>
										<% }
										if(noOfPage>1 && currentPage<noOfPage){ %> 
											<li><a href="<%=reqURI%>?usesession=yes&pageNumber=<%=(currentPage+1)%>"><spring:message code="label.brkNext"/></a></li>
										<% } %>
									<% } %>
								</ul>
							</div>
						</c:when>
						<c:otherwise>
							<hr class="nmhide" />
							<div class="alert alert-info"><spring:message code="label.brkapprovalalert"/></div>
						</c:otherwise>
					</c:choose>
				</div>
			</form>
		</div><!-- end of .span12 -->
	</div><!-- end of .row-fluid -->

	<script>
		$(function() {
			$('.datepick').each(function() {
				$(this).datepicker({
					showOn : "button",
					buttonImage : "../resources/images/calendar.gif",
					buttonImageOnly : true
				});
			});
			$(".empdetail").click(function(e){
		        e.preventDefault();
		        var href = $(this).attr('href');
				
		        if (href.indexOf('#') != 0) {
		           $('<div class="modal"><div class="modal-header" style="border-bottom:0;"><button type="button" title="x" class="close" data-dismiss="modal" aria-hidden="true">�</button></div><div class="modal-body"><iframe src="' + href + '" style="overflow:hidden;width:100%;border:0;margin:0;padding:0;height:400px;"></iframe></div><div class="modal-footer"><button class="btn" data-dismiss="modal" aria-hidden="true">Close</button></div></div>').modal({backdrop:false});
				}
			});
		});
		function onUpdate() {
			
		}
	</script>
