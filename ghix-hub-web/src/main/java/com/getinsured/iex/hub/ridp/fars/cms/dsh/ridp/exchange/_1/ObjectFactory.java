
package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.exchange._1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.RequestPayloadType;
import com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1.ResponsePayloadType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.dsh.ridp.exchange._1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Request_QNAME = new QName("http://ridp.dsh.cms.gov/exchange/1.0", "Request");
    private final static QName _Response_QNAME = new QName("http://ridp.dsh.cms.gov/exchange/1.0", "Response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.dsh.ridp.exchange._1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestPayloadType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ridp.dsh.cms.gov/exchange/1.0", name = "Request")
    public JAXBElement<RequestPayloadType> createRequest(RequestPayloadType value) {
        return new JAXBElement<RequestPayloadType>(_Request_QNAME, RequestPayloadType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponsePayloadType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ridp.dsh.cms.gov/exchange/1.0", name = "Response")
    public JAXBElement<ResponsePayloadType> createResponse(ResponsePayloadType value) {
        return new JAXBElement<ResponsePayloadType>(_Response_QNAME, ResponsePayloadType.class, null, value);
    }

}
