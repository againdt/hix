import React, { Fragment, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import moment from 'moment';

// Components

import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import cloneObject from '../../utils/cloneObject';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';

// Constants

import EMPTY_OBJECTS from '../../constants/emptyObjects';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../constants/validationErrorMessages';



/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Preganant Details ' Page
 *
 */
const Page2_22= props => {

    const {
        data,
        householdMembers,
        isFirstVisit,
        form: {
            Form: {
                isEligiblyForSave,
                isFormSubmitted
            }
        }
    } = props;


    // Local State of Functional Component

    const [localState, setLocalState] = useState({
        members: householdMembers
            .map((member, i) => ({
                name: `${member.name.firstName}_${i}`,
                [`isHouseholdMember${i}Checked`]: member.specialCircumstances.pregnantIndicator,
                [`householdMember${i}NumberBabiesExpectedInPregnancy`]: member.specialCircumstances.numberBabiesExpectedInPregnancy,
                [`householdMember${i}Gender`]: member.gender,
                indexInArray: i
            })),
        noneOfTheAbove: isFirstVisit ? false : householdMembers.every(member => !member.specialCircumstances.pregnantIndicator)
    });

    const { members } = localState;

    //const [ count, setCount ] = useState(0);
    const onFieldChange_member = (newData, indexInArray) => {
        setLocalState(state => {
            const updatedMembers = state.members.map((member, i) => i === indexInArray ? newData : member);
            return {
                noneOfTheAbove: !updatedMembers.indexOf((member, i) => member[`isHouseholdMember${i}Checked`]),
                members: updatedMembers
            };
        });
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray].specialCircumstances.pregnantIndicator = newData[`isHouseholdMember${indexInArray}Checked`];
        props.actions.changeData(newState);
    };

    const onFieldChange_number = (newData, indexInArray) => {
        // setLocalState(state => {
        //     const updatedMembers = state.members.map((member, i) => i === indexInArray ? newData : member);
        //     return {
        //         members: updatedMembers
        //     };
        // });
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[indexInArray] = newData;
        props.actions.changeData(newState);
    };

    // Updating status of 'None of Above' checkbox and if it is checked:
    // a) uncheck all household members
    // b) clear all <americanIndianAlaskaNative> objects in household members
    // c) save changes to Redux Store

    const onFieldChange_none_of_above = newData => {

        setLocalState(state => {
            const newState = {
                members: state.members.map((member, i) => ({
                    ...member,
                    [`isHouseholdMember${i}Checked`]: false
                })),
                noneOfTheAbove: newData.noneOfTheAbove // Actually this is <noneOfAbove> prop
            };
            return newState;
        });

        const newState = {...data };

        const updatedHouseholdMembers =
            householdMembers
                .map(member => {
                    member.specialCircumstances = {
                        ...member.specialCircumstances,
                        ...cloneObject(EMPTY_OBJECTS.SPECIAL_CIRCUMSTANCES)
                    };
                    return member;
                });
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember = updatedHouseholdMembers;
        props.actions.changeData(newState);
    };

    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const getHouseholdMembers = (householdMembers) => {
        const combinedMembers = [];
        const { members, noneOfTheAbove } = localState;

        householdMembers.forEach((member, i) => {
            const {
                name
            } = member;
            const memberName = `${normalizeNameOfPerson(name)}`;
            let age = moment().diff(member.dateOfBirth, 'years');
            combinedMembers.push(
                <Fragment key={`${name.firstName}_${i}`}>
                    <ul className="usa-unstyled-list">
                        {localState.members[i][`householdMember${i}Gender`] && localState.members[i][`householdMember${i}Gender`].toUpperCase() === 'FEMALE' && (age >= 9 && age <= 66) &&
                        <li>
                            <UniversalInput
                                label={{
                                    text: `${memberName}`
                                }}
                                inputData={{
                                    isInputRequired: false,
                                    type: INPUT_TYPES.CHECKBOX,
                                    name: `isHouseholdMember${i}Checked`,
                                    classes:[''],
                                    checked: members[i][`isHouseholdMember${i}Checked`],
                                    value: members[i][`isHouseholdMember${i}Checked`],
                                    currentData: members[i],
                                    fields: [],
                                }}
                                inputActions={{
                                    onFieldChange: onFieldChange_member
                                }}
                                indexInArray={i}
                                errorClasses={['margin-l-0']}
                            />
                        </li>
                        }
                        {
                            localState.members[i][`isHouseholdMember${i}Checked`] &&
                            (localState.members[i][`householdMember${i}Gender`] && localState.members[i][`householdMember${i}Gender`].toUpperCase() === 'FEMALE') &&
                            <Fragment>
                                <p className="required-true">How many babies are expected in this pregnancy?</p>
                                <UniversalInput
                                    label={{
                                        text: '',
                                        labelClasses: ['required-false']
                                    }}
                                    button={{
                                        btnLId: 'decrement',
                                        btnRId: 'increment',
                                        rBtnText: '+',
                                        lBtnText: '-',
                                        minCount: 1,
                                        maxCount: 19,
                                        btnClasses: ['countBtn']
                                    }}
                                    inputData={{
                                        isInputRequired: true,
                                        type: INPUT_TYPES.INPUT_LR_BUTTONS,
                                        name: 'numberBabiesExpectedInPregnancy',
                                        outerClass: ['usa-width-one-whole'],
                                        value: !Number(member.specialCircumstances.numberBabiesExpectedInPregnancy)? 1 :  Number(member.specialCircumstances.numberBabiesExpectedInPregnancy),
                                        placeholder: '',
                                        validationType: VALIDATION_TYPES.ALL_DIGITS,
                                        currentData: member,
                                        fields: ['specialCircumstances']
                                    }}
                                    inputActions={{
                                        onFieldChange: onFieldChange_number
                                    }}
                                    indexInArray={i}
                                />

                            </Fragment>
                        }
                    </ul>
                </Fragment>
            );

        });

        combinedMembers.push(
            <UniversalInput
                key={`None_of_above_${combinedMembers.length + 1}`}
                label={{
                    text: 'None of the Above',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.CHECKBOX,
                    name: 'noneOfTheAbove',
                    checked: noneOfTheAbove,
                    value: noneOfTheAbove,
                    currentData: Object.assign({}, localState),
                    fields: [],
                }}
                inputActions={{
                    onFieldChange: onFieldChange_none_of_above
                }}
            />
        );
        return combinedMembers;
    };

    const checkTopLevelValidation = () => {
        return true || !!members.find((member, i) => member[`isHouseholdMember${i}Checked`]);
    };

    return (
        <Fragment>
            <div id="fhMoreInfo" className="fade-in">
                <Form topLevelValidity={checkTopLevelValidation()}>
                    <div className="subsection">
                        <ValidationErrorMessage
                            isVisible={isFormSubmitted && !isEligiblyForSave}
                            messageText={VALIDATION_ERROR_MESSAGES.ANY_VALUE}
                            errorClasses={['margin-l-20-ve', 'margin-b-10-ve']}
                        />
                        <p className="required-false">
                            Are any of these people pregnant?
                        </p>
                        {
                            getHouseholdMembers(householdMembers)
                        }
                    </div>
                </Form>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page2_22.defaultProps = {
    name: 'Page2_22'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page2_22);
