
package com.getinsured.ssap.model.financial.nmec;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import org.apache.commons.lang.builder.ToStringBuilder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "householdMember"
})
public class NonEsiMecTaxHousehold implements Serializable
{

    @JsonProperty("householdMember")
    private List<NonEsiMecHouseholdMember> householdMember = new ArrayList<NonEsiMecHouseholdMember>();
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    private final static long serialVersionUID = -6011797079287638517L;

    @JsonProperty("householdMember")
    public List<NonEsiMecHouseholdMember> getHouseholdMember() {
        return householdMember;
    }

    @JsonProperty("householdMember")
    public void setHouseholdMember(List<NonEsiMecHouseholdMember> householdMember) {
        this.householdMember = householdMember;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("householdMember", householdMember).append("additionalProperties", additionalProperties).toString();
    }

}
