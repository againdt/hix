package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.IssuerQualityRating;

@Repository("iIssuerQualityRatingRepository")
public interface IIssuerQualityRatingRepository extends JpaRepository<IssuerQualityRating, Integer>{
	
	/* Returns selected issuer quality rating */
	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.issuerId=:issuerId and TO_DATE(to_char(CURRENT_TIMESTAMP(),'DD-MM-YYYY'), 'DD-MM-YYYY') BETWEEN TO_DATE(to_char(iqr.effectiveDate,'DD-MM-YYYY'), 'DD-MM-YYYY') AND TO_DATE(to_char(iqr.effectiveEndDate,'DD-MM-YYYY'), 'DD-MM-YYYY') AND isDeleted='N' ORDER BY iqr.id DESC")
	List<IssuerQualityRating> findByIssuerId(@Param("issuerId") int issuerId);
	
	/*  check issuers Quality Rating already exist in table  */
	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.issuerId=:issuerId AND iqr.planHiosId=:planNumber AND iqr.effectiveDate= TO_DATE(:effectiveStartDate,'MM-DD-YYYY') AND iqr.effectiveEndDate=TO_DATE(:effectiveEndDate,'MM-DD-YYYY') AND isDeleted='N' ")
	IssuerQualityRating getIssuerQualityRatingByIssuerId(@Param("issuerId") int issuerId, @Param("planNumber") String planNumber,@Param("effectiveStartDate") String effectiveStartDate,@Param("effectiveEndDate") String effectiveEndDate);

	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.issuerId=:issuerId AND iqr.planHiosId=:planNumber AND iqr.effectiveEndDate<TO_DATE(:effectiveStartDate,'MM-DD-YYYY')  AND isDeleted='N' ")
	List<IssuerQualityRating> getIssuersEffectiveEndDates(@Param("issuerId") int issuerId, @Param("planNumber") String planNumber, @Param("effectiveStartDate") String effectiveStartDate);
	
	@Query("SELECT iqr from IssuerQualityRating iqr where iqr.issuerId=:issuerId and iqr.planHiosId=:planNumber  and TO_DATE(:effectiveStartDate,'MM-DD-YYYY') between TO_DATE(to_char(iqr.effectiveDate,'MM-DD-YYYY'),'MM-DD-YYYY') + 1 AND TO_DATE(to_char(iqr.effectiveEndDate,'MM-DD-YYYY'),'MM-DD-YYYY') AND isDeleted='N' ")
	IssuerQualityRating checkEffectiveDateBetweenStartAndEndDate(@Param("issuerId") int issuerId, @Param("planNumber") String planNumber, @Param("effectiveStartDate") String effectiveStartDate);
	
	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.issuerId=:issuerId AND isDeleted='N'")
	IssuerQualityRating getQualitRatingForIssuer(@Param("issuerId") int issuerId);
	
	@Query("SELECT iqr FROM IssuerQualityRating iqr WHERE iqr.issuerId=:issuerId AND iqr.planHiosId=:planNumber AND isDeleted='N'")
	IssuerQualityRating getQualitRatingByIssuerAndPlanNumber(@Param("issuerId") int issuerId,@Param("planNumber") String planNumber);
	
	@Modifying
	@Transactional
	@Query("UPDATE IssuerQualityRating iqr SET iqr.isDeleted='Y', iqr.lastUpdatedBy=:userId, iqr.lastUpdateTimestamp=current_timestamp WHERE iqr.issuerId=:issuerId AND iqr.planHiosId=:planNumber AND iqr.effectiveDate >= TO_DATE(:effectiveStartDate,'MM-DD-YYYY') AND iqr.isDeleted='N' ")
	void deleteFutureEffectiveDateRecord(@Param("issuerId") int issuerId,@Param("planNumber") String planNumber, @Param("effectiveStartDate") String effectiveStartDate, @Param("userId") int userId);

}
