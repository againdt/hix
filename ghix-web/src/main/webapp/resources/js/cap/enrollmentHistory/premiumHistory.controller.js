(function() {
	"use strict";
	angular.module("enrollmentHistoryApp").controller('PremiumHistoryCtrl', PremiumHistoryCtrl);
	
	PremiumHistoryCtrl.$inject = ['$http', '$state', '$stateParams', '$scope', '$anchorScroll', 'enrollmentHistoryService', '$window'];
	
	function PremiumHistoryCtrl($http, $state, $stateParams, $scope, $anchorScroll, enrollmentHistoryService, $window){
		var vm = this;
		
		vm.fromPage = $stateParams.fromPage;
		vm.isEditMode = $stateParams.isEditMode === undefined ? false:  $stateParams.isEditMode;
		vm.isEnrollmentSaved = $stateParams.isEnrollmentSaved === undefined ? false : $stateParams.isEnrollmentSaved;
		
		if(vm.fromPage === 'addOrChangeEnrollmentTxn' && vm.isEditMode === false){
			//from cancel, discard new changes
			vm.enrollment = angular.copy($stateParams.newEnrollment);
			vm.enrollmentCopy = angular.copy(vm.enrollment);
			if(vm.isEnrollmentSaved === true){
				showModal(200, 'Your change has been successfully updated.');
			}
			vm.fromPage = undefined;
		}else if(vm.fromPage === 'addOrChangeEnrollmentTxn' && vm.isEditMode === true){
			//from back, use new changes and keep original one
			vm.enrollmentCopy = angular.copy($stateParams.newEnrollment);
			vm.enrollment = angular.copy($stateParams.oldEnrollment);
		}else{
			vm.enrollment = angular.copy($stateParams.newEnrollment);
			vm.enrollmentCopy = angular.copy(vm.enrollment);
		}
		
		vm.goToEnrollmentHistoryPage = goToEnrollmentHistoryPage;
		vm.editPremium = editPremium;
		vm.validateAndSaveEnrollmentData = validateAndSaveEnrollmentData;
		vm.savePremiumUpdate = savePremiumUpdate;
		vm.emptyPremiumOverrideReason = emptyPremiumOverrideReason;
		vm.cancelPremiumUpdate = cancelPremiumUpdate;
		vm.updateAptc = updateAptc;
        vm.updateStateSubsidy = updateStateSubsidy;
		vm.updateApplicationType = updateApplicationType;
		vm.updateEnrollmentDate = updateEnrollmentDate;
		vm.relationshipOrderBy = enrollmentHistoryService.relationshipOrderBy;
		vm.showCancelEnrollmentModal = showCancelEnrollmentModal;
		vm.cancelActiveEnrollment = cancelActiveEnrollment;
		vm.emptyCancelEnrollmentReason = emptyCancelEnrollmentReason;
		vm.goToAddOrChangeControlEnrollmentPage = goToAddOrChangeControlEnrollmentPage;
		
		vm.isUpdatedSuccess = true;
		
		//For role based show / hide columns - end
		
		if(vm.enrollmentCopy !== undefined){
			_init();
		}
		
		function _init(){
			$scope.isEnrollmentEditAllowed = $('#isEnrollmentEditAllowed').val();
			
			
			vm.enrollmentBenefitEffectiveStartMonth = parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI.substring(0,2));
			vm.enrollmentBenefitEffectiveEndMonth =  parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI.substring(0,2));
			
			vm.originalEnrollmentBenefitEffectiveStartMonth = parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI.substring(0,2));
			vm.originalEnrollmentBenefitEffectiveEndMonth =  parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI.substring(0,2));
		}
		
		
		$scope.memberPrmHistActions = $('#memberPrmHistActions').val() == "true" ? true: false;
		$scope.memberPrmHistChange = $('#memberPrmHistChange').val() == "true" ? true: false;
		$scope.memberPrmHistCancel = $('#memberPrmHistCancel').val() == "true" ? true: false;
		
		
		$scope.memberEnrlmntStrtenddt = $('#memberEnrlmntStrtenddt').val() == "true" ? true: false;
		$scope.memberEnrlmntMembrName = $('#memberEnrlmntMembrName').val() == "true" ? true: false;
		$scope.memberEnrlmntMembrGender = $('#memberEnrlmntMembrGender').val() == "true" ? true: false;
		$scope.memberEnrlmntMembrSsn = $('#memberEnrlmntMembrSsn').val() == "true" ? true: false;
		
		$scope.memberEnrlmntDpndtStrtEndDt = $('#memberEnrlmntDpndtStrtEndDt').val() == "true" ? true: false;
		$scope.memberEnrlmntAptc = $('#memberEnrlmntAptc').val() == "true" ? true: false;
		$scope.memberEnrlmntNetprmum = $('#memberEnrlmntNetprmum').val() === "true" ? true: false;
		$scope.memberEnrlmntEhbprmum = $('#memberEnrlmntEhbprmum').val() === "true" ? true: false;
		$scope.memberEnrlmntGrpMaxPrmum = $('#memberEnrlmntGrpMaxPrmum').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrNmEditable = $('#memberEnrlmntMembrNmEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrGndrEditable = $('#memberEnrlmntMembrGndrEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrSsnEditable = $('#memberEnrlmntMembrSsnEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntAptcEditable = $('#memberEnrlmntAptcEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntNetPrmumEditable = $('#memberEnrlmntNetPrmumEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntEhbPrmumEditable = $('#memberEnrlmntEhbPrmumEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntGrpmaxPrmumEditable = $('#memberEnrlmntGrpmaxPrmumEditable').val() === "true" ? true: false;
		
		$scope.memberEnrlmntMembrDpndntNm = $('#memberEnrlmntMembrDpndntNm').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrDpndntGndr = $('#memberEnrlmntMembrDpndntGndr').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrDpndntSsn = $('#memberEnrlmntMembrDpndntSsn').val() === "true" ? true: false;

		$scope.memberEnrlmntMembrDpndntNmEditable = $('#memberEnrlmntMembrDpndntNmEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrDpndntGndrEditable = $('#memberEnrlmntMembrDpndntGndrEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntMembrDpndntSsnEditable = $('#memberEnrlmntMembrDpndntSsnEditable').val() === "true" ? true: false;

		$scope.memberEnrlmntStrtenddtEditable = $('#memberEnrlmntStrtenddtEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntDpndtStrtEndDtEditable = $('#memberEnrlmntDpndtStrtEndDtEditable').val() === "true" ? true: false;
		$scope.memberEnrlmntAppType = $('#memberEnrlmntAppType').val() === "true" ? true: false;
		
		$scope.memberEnrlmntGroupMaxAptc = $('#memberEnrlmntGroupMaxAptc').val() == "true" ? true: false;
		$scope.memberEnrlmntGroupMaxAptcEditable = $('#memberEnrlmntGroupMaxAptcEditable').val() == "true" ? true: false;

		$scope.stateSubsidyEnabled = $('#stateSubsidyEnabled').val() === "Y" ? true : false;
		
		function goToEnrollmentHistoryPage(){
			if($('#enrollmentDetail').val() === ''){
				$state.go('enrollmentHistory');
			}else{
				//$window.location.href = '/hix/enrollmentreconciliation/enrollmentmonthlyactivity/' + $('#encodedEnrollmentID').val();
				$window.location.href = '/hix/enrollmentreconciliation/enrollmentmonthlyactivity#/search';
			}
		}
		
		function goToAddOrChangeControlEnrollmentPage(){
			$state.go('addOrChangeEnrollment', {newEnrollment: vm.enrollmentCopy, oldEnrollment: vm.enrollment, isEditMode: vm.isEditMode});
		}
		
		function editPremium(){
			vm.enrollmentCopy = angular.copy(vm.enrollment);
			vm.isEditMode = true;
			$scope.premiumHistoryForm.$pristine = true;
			$anchorScroll();
		}
		
		function validateAndSaveEnrollmentData(){
			var subscriberBenefitStartDates = '';
			var subscriberBenefitEndDates = '';
			var validationErrorCode = 0;
			var isValidDate = true;
			var enrollmentStartDate = new Date(vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI);
			var enrollmentEndDate = new Date(vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI);
			
			angular.forEach(vm.enrollmentCopy.enrolleeDataDtoList, function(value, key){
				var enrolleeStartDate = new Date(value.enrolleeEffectiveStartDateUI);
				var enrolleeEndDate =  new Date(value.enrolleeEffectiveEndDateUI);
				if(isValidDate){
					if(enrollmentStartDate.getFullYear() != enrollmentEndDate.getFullYear()){
						isValidDate = false;
						validationErrorCode = 215
					}
					else if(enrolleeStartDate.getFullYear() != enrolleeEndDate.getFullYear()){
						isValidDate = false;
						validationErrorCode = 215
					}
					else if(enrollmentEndDate < enrollmentStartDate){
						isValidDate = false;
						validationErrorCode = 1003;
					}
					else if((enrolleeStartDate < enrollmentStartDate) &&
							value.enrolleeEffectiveStartDateUI != value.enrolleeEffectiveEndDateUI){	
						isValidDate = false;
						validationErrorCode = 207;
					}
					else if((enrolleeEndDate > enrollmentEndDate) &&
							value.enrolleeEffectiveStartDateUI != value.enrolleeEffectiveEndDateUI){
						isValidDate = false;
						validationErrorCode = 207;
					}
					else if(enrolleeStartDate > enrolleeEndDate &&
							value.enrolleeEffectiveStartDateUI != value.enrolleeEffectiveEndDateUI){
						isValidDate = false;
						validationErrorCode = 217;
					}
				}
				if(value.relationshipToSubscriber.toUpperCase() === 'SELF'){
					subscriberBenefitStartDates = enrolleeStartDate;
					subscriberBenefitEndDates = enrolleeEndDate;
				}
			});
			
			if(validationErrorCode !== 0 && !isValidDate){
				vm.isUpdatedSuccess = false;
				showModal(validationErrorCode, '');
			}
			else{
				if(subscriberBenefitStartDates.toString() === enrollmentStartDate.toString()
						&& subscriberBenefitEndDates.toString() === enrollmentEndDate.toString()){
					goToAddOrChangeControlEnrollmentPage();
				}
			}
		}
		
		function cancelActiveEnrollment(){
			$scope.loader = true;
			if(vm.cancelEnrollmentReason == "59"){
				vm.isSendIn834 = false;
			}
			var url = '/hix/crm/member/cancelenrollment';
			var data = {
					'id': vm.enrollmentCopy.enrollmentId,
					'householdId': vm.enrollmentCopy.houseHoldCaseId,
					'comment': vm.cancelEnrollmentComment,
					'send834': vm.isSendIn834,
					'cancellationReasonCode' : vm.cancelEnrollmentReason
			};
			var promise = $http.post(url, data);
			promise.success(function(response){
				$scope.loader = false;
				if(response.errCode === 200){
					getUpdatedEnrollmentDetail(vm.enrollmentCopy.enrollmentId);

				}else{
					vm.isUpdatedSuccess = false;
					
					showModal(response.errCode, response.errMsg);
				}
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				
				showModal(response.errCode, response.errMsg);
			});
			
		}
		
		function showPremiumOverrideModal(){
			$('#premiumOverrideReasonModal').modal({
				keyboard: false,
				backdrop: 'static'
			});
		}
		
		function showCancelEnrollmentModal(){
			$('#cancelEnrollmentModal').modal({
				keyboard: false,
				backdrop: 'static'
			});
		}
		
		function savePremiumUpdate(){
			$scope.loader = true;
			
			var url = '/hix/crm/member/updateEnrollmentdetails';
			var data = {
				'id': vm.enrollmentCopy.enrollmentId,
				'householdId': vm.enrollmentCopy.houseHoldCaseId,
				'benefitEffectiveDate': null,
				'benefitEndDate': null,
				'benefitEffectiveDateStr': vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI,
				'benefitEndDateStr': vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI,
				'healthCoveragePolicyNo': null,
				'maintainenceReasonCodeStr': null,
				'action': null,
				'confirmIfNotConfirmed': false,
				'comment': vm.premiumHistoryComment,
				'members': []
			};
			
			angular.forEach(vm.enrollmentCopy.enrolleeDataDtoList, function(value, key){
				var genderLookupCode = '';
				
				if(value.genderLookupLabel.toUpperCase() === 'MALE'){
					genderLookupCode = 'M';
				}else if(value.genderLookupLabel.toUpperCase() === 'FEMALE'){
					genderLookupCode = 'F';
				}else{
					genderLookupCode = 'U';
				}
				
				
				data.members.push({
					'exchgIndivIdentifier': value.memberId, 
					'effectiveStartDate': null, 
					'effectiveEndDate': null, 
					'effectiveStartDateStr': value.enrolleeEffectiveStartDateUI, 
					'effectiveEndDateStr': value.enrolleeEffectiveEndDateUI, 
					'firstName': value.firstName, 
					'lastName': value.lastName, 
					'middleName': value.middleName, 
					'ssn': value.taxIdentificationNumber, 
					'gender': genderLookupCode,
					'enrolleeId': value.enrolleeId
				});
			
				
			});
			
			data.enrollmentPremiumDtoList = angular.copy(vm.enrollmentCopy.enrollmentPremiumDtoList);
			
			var promise = $http.post(url, data);
			
			promise.success(function(response){
				$scope.loader = false;
				if(response.errCode === 200){
					getUpdatedEnrollmentDetail(vm.enrollmentCopy.enrollmentId);

				}else{
					vm.isUpdatedSuccess = false;
					
					showModal(response.errCode, response.errMsg);
				}
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				
				showModal(response.errCode, response.errMsg);
			});
			
		}
		
		function getUpdatedEnrollmentDetail(enrollmentId){

			var data={
				encHouseholdId:$('#encHouseHoldId').val()
			};
			
			var url = '/hix/crm/member/enrollmentandenrolleedetails';

			var promise = $http.post(url, $.param(data), {
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				transformResponse: [function(data){return JSON.parse(data);}]
			});
			
			promise.success(function(response){
				
				if(response.errCode === 200){
					
					vm.isUpdatedSuccess = true;
					vm.isEditMode = false;
					$scope.enrollmentEditToolAction();
					$anchorScroll();
					
				for (var key in response.enrollmentDataDtoListMap) {
					for(var i = 0; i < response.enrollmentDataDtoListMap[key].length; i++){
							if(enrollmentId === response.enrollmentDataDtoListMap[key][i].enrollmentId){
							
							vm.enrollment = response.enrollmentDataDtoListMap[key][i];
							
							vm.enrollment.enrollmentBenefitEffectiveStartDateUI = enrollmentHistoryService.getUIDateFormat(vm.enrollment.enrollmentBenefitEffectiveStartDate);
							vm.enrollment.enrollmentBenefitEffectiveEndDateUI = enrollmentHistoryService.getUIDateFormat(vm.enrollment.enrollmentBenefitEffectiveEndDate);
							vm.enrollment.enrollmentStatus = vm.enrollment.enrollmentStatus;
							angular.forEach(vm.enrollment.enrolleeDataDtoList, function(value, key){
								value.enrolleeEffectiveStartDateUI = enrollmentHistoryService.getUIDateFormat(value.enrolleeEffectiveStartDate);
								value.enrolleeEffectiveEndDateUI = enrollmentHistoryService.getUIDateFormat(value.enrolleeEffectiveEndDate);
	            			});
							
							
							angular.forEach(vm.enrollment.enrollmentPremiumDtoList, function(value, key){
	            				if(value.isFinancial === false){
	            					value.aptcAmount = 0;
	            				}
	            			});
							
						}
					}
				}
				vm.enrollmentCopy = angular.copy(vm.enrollment);
					_init();
				}else{
					vm.isUpdatedSuccess = false;
				}
				showModal(response.errCode, response.errMsg);
			});
			
			promise.error(function(response){
				$scope.loader = false;
				vm.isUpdatedSuccess = false;
				showModal(response.errCode, response.errMsg);
			});
		}
		
		function showModal(errCode, errMsg){
				emptyPremiumOverrideReason();
				emptyCancelEnrollmentReason();
			vm.premiumOverrideResultMessage = enrollmentHistoryService.getEnrollmentEditToolErrorMessage(errCode, errMsg);
				$('#premiumOverrideResultModal').modal();
			
			$scope.loader = false;
		}
		
		function emptyPremiumOverrideReason(){
			vm.premiumHistoryComment = '';
		}
		
		function emptyCancelEnrollmentReason(){
			vm.cancelEnrollmentComment = '';
			vm.cancelEnrollmentReason = "AI";
			vm.isSendIn834 = 'true';
		}
		
		function cancelPremiumUpdate(){
			vm.enrollmentCopy = angular.copy(vm.enrollment);
			_init();
			vm.isEditMode = false;
			vm.fromPage ='';
		}
		
	
		
		function updateAptc(premium, premiumSubForm, ehbPercent){
			if(ehbPercent != null && ehbPercent != 0){
				if((parseFloat(premium.grossPremiumAmount)*parseFloat(ehbPercent)) < parseFloat(premium.aptcAmount)){
					premiumSubForm.aptcAmount.$setValidity('ehbPremium', false);
				}
				else{
					premiumSubForm.aptcAmount.$setValidity('ehbPremium', true);
				}
			}
			else{
			if(parseFloat(premium.grossPremiumAmount) < parseFloat(premium.aptcAmount)){
				premiumSubForm.aptcAmount.$setValidity('ehbPremium', false);
			}else{
				premiumSubForm.aptcAmount.$setValidity('ehbPremium', true);
			}
			}
			if(premium.maxAptc !== undefined && parseFloat(premium.aptcAmount) > parseFloat(premium.maxAptc)) {
				premiumSubForm.aptcAmount.$setValidity('highAptc', false);
			} else {
				premiumSubForm.aptcAmount.$setValidity('highAptc', true);
			}
			if(premiumSubForm.aptcAmount.$valid){
				premium.netPremiumAmount = parseFloat(premium.grossPremiumAmount);

				if(premium.aptcAmount !== undefined){
				    if(parseFloat(premium.netPremiumAmount) - parseFloat(premium.aptcAmount) < parseFloat(premium.minimumPremium)){
                        premium.aptcAmount = parseFloat(premium.grossPremiumAmount) - parseFloat(premium.minimumPremium);
                        premium.netPremiumAmount = parseFloat(premium.minimumPremium);
                    }
				    else{
				        premium.netPremiumAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.aptcAmount);
                    }
                }

                if(premium.stateSubsidyAmount !== undefined) {
                    if(premium.netPremiumAmount > premium.minimumPremium){
                        if(parseFloat(premium.netPremiumAmount) - parseFloat(premium.stateSubsidyAmount) < parseFloat(premium.minimumPremium)){
                            premium.stateSubsidyAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.minimumPremium);
                            premium.netPremiumAmount = parseFloat(premium.minimumPremium);
                        }
                        else{
                            //premium.stateSubsidyAmount = premium.maxStateSubsidy;
                            premium.netPremiumAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.stateSubsidyAmount);
                        }
                    }
                    else{
                        premium.stateSubsidyAmount = 0;
                    }
                }
			}else{
				premium.netPremiumAmount = parseFloat(premium.grossPremiumAmount);
			}
		}

        function updateStateSubsidy(premium, premiumSubForm, ehbPercent){
            if(ehbPercent != null && ehbPercent != 0){
                if((parseFloat(premium.grossPremiumAmount)*parseFloat(ehbPercent)) < parseFloat(premium.stateSubsidyAmount)){
                    premiumSubForm.stateSubsidyAmount.$setValidity('ehbPremium', false);
                }
                else{
                    premiumSubForm.stateSubsidyAmount.$setValidity('ehbPremium', true);
                }
            }
            else{
                if(parseFloat(premium.grossPremiumAmount) < parseFloat(premium.stateSubsidyAmount)){
                    premiumSubForm.stateSubsidyAmount.$setValidity('ehbPremium', false);
                }else{
                    premiumSubForm.stateSubsidyAmount.$setValidity('ehbPremium', true);
                }
            }

            //if(premium.stateSubsidyAmount !== undefined) {
                if(premium.maxStateSubsidy !== undefined && parseFloat(premium.stateSubsidyAmount) > parseFloat(premium.maxStateSubsidy)) {
                    premiumSubForm.stateSubsidyAmount.$setValidity('highStateSubsidy', false);
                } else {
                    premiumSubForm.stateSubsidyAmount.$setValidity('highStateSubsidy', true);
                }
            //}

            if(premiumSubForm.stateSubsidyAmount.$valid){
                premium.netPremiumAmount = parseFloat(premium.grossPremiumAmount);

                if(premium.aptcAmount !== undefined){
                    if(parseFloat(premium.netPremiumAmount) - parseFloat(premium.aptcAmount) < parseFloat(premium.minimumPremium)){
                        premium.aptcAmount = parseFloat(premium.grossPremiumAmount) - parseFloat(premium.minimumPremium);
                        premium.netPremiumAmount = parseFloat(premium.minimumPremium);
                    }
                    else{
                        premium.netPremiumAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.aptcAmount);
                    }
                }

                if(premium.stateSubsidyAmount !== undefined) {
                    if(premium.netPremiumAmount > premium.minimumPremium){
                        if(parseFloat(premium.netPremiumAmount) - parseFloat(premium.stateSubsidyAmount) < parseFloat(premium.minimumPremium)){
                            premium.stateSubsidyAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.minimumPremium);
                            premium.netPremiumAmount = parseFloat(premium.minimumPremium);
                        }
                        else{
                            //premium.stateSubsidyAmount = premium.maxStateSubsidy;
                            premium.netPremiumAmount = parseFloat(premium.netPremiumAmount) - parseFloat(premium.stateSubsidyAmount);
                        }
                    }
                    else{
                        premium.stateSubsidyAmount = 0;
                    }
                }
            }else{
                premium.netPremiumAmount = parseFloat(premium.grossPremiumAmount);
            }
        }
		
		function updateApplicationType(premium){
			if(!premium.isFinancial){
				premium.aptcAmount = 0;
				premium.netPremiumAmount = parseFloat(premium.grossPremiumAmount);
			}
		}
		
		function validateCancelFromChangeRequest(startDate, endDate){
			if($scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$valid &&
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$valid &&
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$viewValue == 
						$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$viewValue){
				if(vm.enrollment.enrollmentBenefitEffectiveEndDateUI != endDate){
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$setValidity('cancelNotAllowed', false);
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$setValidity('cancelNotAllowed', true);
				}
				else if(vm.enrollment.enrollmentBenefitEffectiveStartDateUI != startDate){
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$setValidity('cancelNotAllowed', true);
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$setValidity('cancelNotAllowed', false);
				}
			}
			else{
				$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$setValidity('cancelNotAllowed', true);
				$scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$setValidity('cancelNotAllowed', true);
			}
		}
		
		function validateDateOfBirth(startDateStr, dateOfBirth){
				var startDate = stringToDate(startDateStr);
			if(null != startDate && new Date(dateOfBirth) > startDate){
					$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$setValidity('birthDateBeforStartDate', false);					
				} else {
				$scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$setValidity('birthDateBeforStartDate', true);	
				}
		}

		function stringToDate(_date){
			if(_date != undefined){	
				var month = parseInt(_date.substring(0,2))-1;
				var day = _date.substring(2,4)
			var year = _date.substring(4,8);
				return new Date(year,month,day);
			}
			return null;
		}		
		
		function updateEnrollmentDate(type){
			var subscriberBirthDateUI
			var startDateStr = vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI;
			vm.enrollmentCopy.enrolleeDataDtoList.forEach(function(enrollee){
				var startDateUI = vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI				
				if(enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'){
					subscriberBirthDateUI = enrollee.birthDate;
				}
			})
		
			if(type==='start' && $scope.premiumHistoryForm.enrollmentBenefitEffectiveStartDateUI.$valid){
				vm.enrollmentBenefitEffectiveStartMonth = parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI.substring(0,2));
				vm.enrollmentCopy.enrolleeDataDtoList.forEach(function(enrollee){
					var startDateUI = vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI				
					if(enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'){
						enrollee.enrolleeEffectiveStartDateUI = startDateUI;
					}
				})
				
			}else if(type==='end' && $scope.premiumHistoryForm.enrollmentBenefitEffectiveEndDateUI.$valid){
				vm.enrollmentBenefitEffectiveEndMonth =  parseInt(vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI.substring(0,2));
				vm.enrollmentCopy.enrolleeDataDtoList.forEach(function(enrollee){
					var endDateUI = vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI
					if(enrollee.relationshipToSubscriber.toUpperCase() === 'SELF'){
						enrollee.enrolleeEffectiveEndDateUI = endDateUI;
					}
				})
			}
			validateCancelFromChangeRequest(vm.enrollmentCopy.enrollmentBenefitEffectiveStartDateUI,  vm.enrollmentCopy.enrollmentBenefitEffectiveEndDateUI);
			validateDateOfBirth(startDateStr, subscriberBirthDateUI);
		}
		
		$scope.enrollmentEditToolAction = function(){
			$('.editToolButtonDiv').slideToggle();
		};
	}
	
})();
