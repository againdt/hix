package com.getinsured.hix.entity.admin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.history.RevisionRepository;

import com.getinsured.hix.model.BrokerDocuments;

/**
 * Spring-JPA repository that encapsulates methods that interact with the BrokerDocuments
 * table
 */
public interface IDocumentRepository extends RevisionRepository<BrokerDocuments, Integer, Integer>,
		JpaRepository<BrokerDocuments, Integer> {

}
