(function() {
'use strict';

angular.module('agencyApp')
.config(RoutesConfig)
.run(RoutesRootScope);


RoutesConfig.$inject = ['$stateProvider'];
function RoutesConfig ($stateProvider) {
        $stateProvider
            .state('accsetup', {
                url: '',
                abstract: true,
                templateUrl: '/hix/resources/html/agency/template.html',
                controller: 'AppController',
                controllerAs: 'vm',
                bindToController: true
            })
            .state('accsetup.agencyInformation', {
                url: '/agencyInformation',
                templateUrl: '/hix/resources/html/agency/agencyInformation.html',
                controller: 'AgencyInformationCtrl',
                controllerAs: 'vm',
            })
            .state('accsetup.locationHour', {
                url: '/locationHour',
                templateUrl: '/hix/resources/html/agency/agencyLocation.html',
                controller: 'LocationHourCtrl',
                controllerAs: 'vm',
            })
            .state('accsetup.upload', {
                url: '/documentupload',
                templateUrl: '/hix/resources/html/agency/documentUpload.html',
                controller: 'DocumentUploadCtrl',
                controllerAs: 'vm'
            })
            .state('accsetup.certificationstatus', {
                url: '/certificationstatus',
                templateUrl: '/hix/resources/html/agency/certificationStatus.html',
                controller: 'CertificationStatusCtrl',
                controllerAs: 'vm',
                params: {refreshFlag: undefined}
            })
            .state('adminstaff', {
                url: '',
                abstract: true,
                templateUrl: '/hix/resources/html/agency/staffTemplate.html',
                controller: 'adminStaffAppController',
                controllerAs: 'vm',
                bindToController: true
            })
            .state('adminstaff.addstaff', {
              url: '/addadminstaff',
              templateUrl: '/hix/resources/html/agency/addAdminStaff.html',
              controller: 'AdminStaffCtrl',
              controllerAs: 'vm',
              params: {
                  assistantId: undefined,
                  registrationComplete: undefined,
                  viewAdminStaffFlow: undefined
              }
            })
            .state('adminstaff.approvalstatus', {
                url: '/adminstaffapprovalstatus',
                templateUrl: '/hix/resources/html/agency/adminStaffApprovalStatus.html',
                controller: 'AdminStaffApprovalCtrl',
                controllerAs: 'vm',
                params: {
                    assistantId: undefined,
                    registrationComplete: undefined
                }
            })
            .state('adminstaff.status', {
              url: '/adminstaffstatus',
              templateUrl: '/hix/resources/html/agency/adminStaffStatus.html',
              controller: 'AdminStaffStatusCtrl',
              controllerAs: 'vm',
            })
            .state('viewadminstafflist', {
              url: '/viewadminstafflist',
              templateUrl: '/hix/resources/html/agency/viewAdminStaffList.html',
              controller: 'ViewAdminStaffCtrl',
              controllerAs: 'vm'
            })
            .state('viewagentlist', {
                url: '/viewagentlist',
                templateUrl: '/hix/resources/html/agency/viewagentlist.html',
                controller: 'ViewAgentListController',
                controllerAs: 'vm',
            })
            .state('agencyDelegation', {
                url: '/agencyDelegation',
                templateUrl: '/hix/resources/html/agency/agencyDelegation.html',
                controller: 'AgencyDelegationCtrl',
                controllerAs: 'vm',
            })
            .state('agencyBOB', {
                url: '/agencyBOB',
                templateUrl: '/hix/resources/html/agency/agencyBOB.html',
                controller: 'AgencyBOBCtrl',
                controllerAs: 'vm',
            })
            .state('transferConsumerDelegations', {
                url: '/transferConsumerDelegations',
                templateUrl: '/hix/resources/html/agency/transferConsumerDelegations.html',
                controller: 'TransferConsumerDelegationsCtrl',
                controllerAs: 'vm',
                params: {
                	delegatedIndividuals: undefined,
                	delegatedAgent: undefined,
                	flow: undefined
                },
                resolve: {
                	checkDelegatedIndividuals: function($stateParams, $state, $timeout, $location){
                        if($location.search().newApplication) {
                            $state.go('transferConsumerDelegations');
                        }
                		if((!$location.search().newApplication || $location.search().newApplication === undefined) && $stateParams.flow === undefined){
                			$timeout(function() {
                                $state.go('agencyBOB');
            				}, 0);
                		}
                	}
                }
            })

}


RoutesRootScope.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$timeout', 'StatusService', '$translate','dataStorageService'];
function RoutesRootScope ($rootScope, $state, $stateParams, $window, $timeout, StatusService, $translate, dataStorageService) {
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams, $state, $stateParams) {
            if (toState.external) {
                event.preventDefault();
                $window.open(toState.url, '_self');
            }
    });
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    if(StatusService.CertificationStatus === "INCOMPLETE" || !StatusService.CertificationStatus){
    	$state.transitionTo('accsetup.agencyInformation', { reload: false, inherit: false, notify: true });
    }else{
    	$state.transitionTo('viewagentlist', { reload: false, inherit: false, notify: true });
    }

}

})();
