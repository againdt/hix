package com.getinsured.eligibility.prescreen.data;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;

/**
 * This program returns premium for all 50 states for parent and child ages.
 * 
 * @author polimetla_b / Nikhil
 * @since 29May2013
 */
@Component
public class PremiumData {

	// public static final String states50 =
	// "Alabama;AL;Montgomery;36101#Alaska ;AK;Juneau;99801#Arizona ;AZ;Phoenix;85001#Arkansas ;AR;Little Rock;72201#California ;CA;Sacramento;94203#Colorado ;CO;Denver;80201#Connecticut ;CT;Hartford;06101#Delaware ;DE;Dover;19901#Florida ;FL;Tallahassee;32301#Georgia ;GA;Atlanta;30301#Hawaii ;HI;Honolulu;96801#Idaho ;ID;Boise;83701#Illinois ;IL;Springfield;62701#Indiana ;IN;Indianapolis;46201#Iowa ;IA;Des Moines;50301#Kansas ;KS;Topeka;66601#Kentucky ;KY;Frankfort;40601#Louisiana ;LA;Baton Rouge;70801#Maine ;ME;Augusta;04330#Maryland ;MD;Annapolis;21401#Massachusetts ;MA;Boston;02108#Michigan ;MI;Lansing;48901#Minnesota ;MN;St. Paul;55101#Mississippi ;MS;Jackson;39201#Missouri ;MO;Jefferson City;65101#Montana ;MT;Helena;59601#Nebraska ;NE;Lincoln;68501#Nevada ;NV;Carson City;89701#New Hampshire ;NH;Concord;03301#New Jersey ;NJ;Trenton;08601#New Mexico ;NM;Santa Fe;87501#New York ;NY;Albany;12201#North Carolina;NC;Raleigh;27601#North Dakota ;ND;Bismarck;58501#Ohio ;OH;Columbus;43201#Oklahoma ;OK;Oklahoma City;73101#Oregon ;OR;Salem;97301#Pennsylvania ;PA;Harrisburg;17101#Rhode Island ;RI;Providence;02901#South Carolina;SC;Columbia;29201#South Dakota ;SD;Pierre;57501#Tennessee ;TN;Nashville;37201#Texas ;TX;Austin;73301#Utah ;UT;Salt Lake City;84101#Vermont ;VT;Montpelier;05601#Virginia ;VA;Richmond;23218#Washington ;WA;Olympia;98501#West Virginia ;WV;Charleston;25301#Wisconsin ;WI;Madison;53701#Wyoming ;WY;Cheyenne;82001#";
	
	private static final Logger LOGGER = Logger.getLogger(PremiumData.class);
	
	public static final String STATES_50 = "99801;Alaska;AK;JUNEAU#36101;Alabama;AL;MONTGOMERY#72201;Arkansas;AR;PULASKI#85001;Arizona;AZ;MARICOPA#94203;California;CA;SACRAMENTO#80201;Colorado;CO;DENVER#06101;Connecticut;CT;HARTFORD#19901;Delaware;DE;KENT#32301;Florida;FL;LEON#30301;Georgia;GA;FULTON#96801;Hawaii;HI;HONOLULU#50301;Iowa;IA;POLK#83701;Idaho;ID;ADA#62701;Illinois;IL;SANGAMON#46201;Indiana;IN;MARION#66601;Kansas;KS;SHAWNEE#40601;Kentucky;KY;FRANKLIN#70801;Louisiana;LA;EAST BATON ROUGE#02108;Massachusetts;MA;SUFFOLK#21401;Maryland;MD;ANNE ARUNDEL#04330;Maine;ME;KENNEBEC#48901;Michigan;MI;INGHAM#55101;Minnesota;MN;RAMSEY#65101;Missouri;MO;COLE#39201;Mississippi;MS;HINDS#59601;Montana;MT;LEWIS AND CLARK#27601;North Carolina;NC;WAKE#58501;North Dakota;ND;BURLEIGH#68501;Nebraska;NE;LANCASTER#03301;New Hampshire;NH;MERRIMACK#08601;New Jersey;NJ;MERCER#87501;New Mexico;NM;SANTA FE#89701;Nevada;NV;CARSON CITY#12201;New York;NY;ALBANY#43201;Ohio;OH;FRANKLIN#73101;Oklahoma;OK;OKLAHOMA#97301;Oregon;OR;MARION#17101;Pennsylvania;PA;DAUPHIN#02901;Rhode Island;RI;PROVIDENCE#29201;South Carolina;SC;RICHLAND#57501;South Dakota;SD;HUGHES#37201;Tennessee;TN;DAVIDSON#76001;Texas;TX;TARRANT#84101;Utah;UT;SALT LAKE#23218;Virginia;VA;RICHMOND CITY#05601;Vermont;VT;WASHINGTON#98501;Washington;WA;THURSTON#53701;Wisconsin;WI;DANE#25301;West Virginia;WV;KANAWHA#82001;Wyoming;WY;LARAMIE#20004;WashingtonDC;DC;DISTRICT OF COLUMBIA#";

	public static final String URL = "http://ghixpilot.com/ghix-planmgmt/premiumRestService/houseHoldReq";
	// public static final String URL =
	// "http://localhost:8080/ghix-planmgmt/premiumRestService/houseHoldReq";

	public static final String POST_CONTENT = "{\"appID\":\"Prescreen\",\"appPassword\":null,\"loggedInUserName\":null,\"requestTime\":1369782693561,\"members\":[{\"memberType\":\"primary\",\"gender\":\"MALE\","
			+ "\"age\":AGE1,\"dateOfBirth\":DOB1,\"student\":false,\"tobaccoUser\":false},"
			+ "{\"memberType\":\"child\",\"gender\":\"FEMALE\",\"age\":AGE2,\"dateOfBirth\":DOB2,\"student\":true,\"tobaccoUser\":false}],"
			+ "\"state\":\"STATE_CD\",\"zipCode\":\"ZIP_CD\",\"county\":\"COUNTY_NAME\",\"effectiveDate\":EFFDATE}";

	private HttpPost httppost = new HttpPost(URL);
	private HttpClient httpclient = new DefaultHttpClient();

	private StringBuffer errorList = new StringBuffer();
	
	private static final int THREE = 3;
	private static final int NINETY_NINE = 99;
	
	public static void main(String[] args) {

		String adultDob = "01/01/1985";
		String childDob = "01/01/1999";
		String effectiveStartDate = "08/01/2013";
		PremiumData pd = new PremiumData();
		pd.processUsers(adultDob, childDob,
				effectiveStartDate);
		LOGGER.debug("" + pd.getErrorList());
		//System.out.println("" + pd.getErrorList());
		// printList(list);
	}

	public String getErrorList() {
		return errorList.toString();
	}

	public static void printList(List<UserRec> list) {
		for (UserRec userRec : list) {
			LOGGER.debug("final==>" + userRec.getData());
		}
	}

	private void intializeConnections() {
		httpclient = new DefaultHttpClient();
	}

	private void closeConnections() {
		if (httpclient != null) {
			try {
				httpclient.getConnectionManager().shutdown();
				httpclient = null;
			} catch (Exception ex) {
				LOGGER.error(ex.getMessage(),ex);
			}
		}
	}

	private List<UserRec> preimumList = new ArrayList<UserRec>();
	private String adultDobOld, childDobOld, effectiveStartDateOld;
	
	public List<UserRec> processUsers(String adultDob, String childDob,
			String effectiveStartDate) {
		
		errorList = new StringBuffer();
		if (null == adultDob || null == childDob || null == effectiveStartDate) {
			errorList.append("Parent or Child or Effective Date are empty;");
			return null;
		}

		if (adultDob.equals(adultDobOld) && childDob.equals(childDobOld)
				&& effectiveStartDate.equals(effectiveStartDateOld)) {
			return preimumList;
		} else {
			preimumList = new ArrayList<UserRec>();
			adultDobOld = adultDob;
			childDobOld = childDob;
			effectiveStartDateOld = effectiveStartDate;
		}

		intializeConnections();

		try {
			int adult = getAgeFromDob(adultDob);
			int child = getAgeFromDob(childDob);

			long adultMillis = getMillisFromDob(adultDob);
			long childMillis = getMillisFromDob(childDob);
			long effectiveStartDateMillis = getMillisFromDob(effectiveStartDate);

			if (adult < 0 || adult > NINETY_NINE || child < 0 || child > NINETY_NINE) {
				return null;
			}

			String rec[] = STATES_50.split("#");
			for (String rec1 : rec) {
				String fields[] = rec1.split(";");
				// (int age1,int age2, String sn,String sc,String zip,String cn)
				// 99801;Alaska;AK;JUNEAU
				UserRec userRec = new UserRec(adult, child, fields[1],
						fields[2], fields[0], fields[THREE]);
				
				String postData = POST_CONTENT;
				postData = postData.replaceAll("AGE1", "" + adult);
				postData = postData.replaceAll("AGE2", "" + child);
				postData = postData.replaceAll("DOB1", "" + adultMillis);
				postData = postData.replaceAll("DOB2", "" + childMillis);
				postData = postData.replaceAll("STATE_CD", fields[2]);
				postData = postData.replaceAll("ZIP_CD", fields[0]);
				postData = postData.replaceAll("COUNTY_NAME", fields[THREE]);
				// postData = postData.replaceAll("EFFDATE",
				// ""+TSCalendar.getInstance().getTimeInMillis());

				// postData = postData.replaceAll("EFFDATE",
				// ""+getNextMonthFirst());
				postData = postData.replaceAll("EFFDATE", ""
							+ effectiveStartDateMillis);

				// System.out.println("post data==>"+postData);
				userRec.startCall();
				String result = getDataFromURL(postData);
				LOGGER.debug("result=>" + result);

				if (result.contains("503 Service Temporarily Unavailable")) {
						errorList.append("503 Service Temporarily Unavailable\n");
						break;

				}

				JSONParser parser = new JSONParser();

				Object obj = parser.parse(result);
				JSONObject jsonObject = (JSONObject) obj;

				Long errCode = (Long) jsonObject.get("errCode");
				// System.out.println(errCode);
				userRec.setErrorCode(errCode);

				String errMsg = (String) jsonObject.get("errMsg");
				userRec.setErrMsg(errMsg);

				Double premium = (Double) jsonObject.get("premium");
				// System.out.println(premium);
				userRec.setPremium(premium);
				userRec.endCall();
				preimumList.add(userRec);
				
				LOGGER.debug("final==>" + userRec.getData());
				// Enable break for testing purpose.
				// break;
			}// End of for loop

		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			errorList.append(e.getMessage()).append("\n");
		}

		closeConnections();
		return preimumList;
	}

	public static int getAgeFromDob(String dob) {

		if (dob == null || dob.trim().length() == 0) {
			return -1;
		}

		int age = 0;
		int factor = 0;
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		Date today;
		try {
			today = df.parse(df.format(new TSDate()));
		} catch (java.text.ParseException e) {
			return -1;
		}

		Calendar cal1 = new GregorianCalendar();
		Calendar cal2 = new GregorianCalendar();

		Date dobDate = DateUtil.StringToDate(dob,
				GhixConstants.REQUIRED_DATE_FORMAT);
		cal1.setTime(dobDate);
		cal2.setTime(today);

		if (cal2.get(Calendar.DAY_OF_YEAR) < cal1.get(Calendar.DAY_OF_YEAR)) {
			factor = -1;
		}
		age = cal2.get(Calendar.YEAR) - cal1.get(Calendar.YEAR) + factor;

		return age;
	}

	public static long getMillisFromDob(String dob) {

		if (dob == null || dob.trim().length() == 0) {
			return 0;
		}

		Date dobDate = DateUtil.StringToDate(dob,
				GhixConstants.REQUIRED_DATE_FORMAT);
		Calendar cal = new GregorianCalendar();
		cal.setTime(dobDate);

		return cal.getTimeInMillis();
	}

	/**
	 * Returns result for given content.
	 * 
	 * @param content
	 * @return result as String
	 */
	private String getDataFromURL(String content) {

		StringEntity entity = new StringEntity(content, ContentType.create(
				"application/json", "UTF-8"));
		httppost.setEntity(entity);

		try {

			ResponseHandler<byte[]> handler = new ResponseHandler<byte[]>() {
				public byte[] handleResponse(HttpResponse response)
						throws IOException {
					HttpEntity entity = response.getEntity();
					if (entity != null) {
						return EntityUtils.toByteArray(entity);
					} else {
						return null;
					}
				}
			};

			byte[] response = httpclient.execute(httppost, handler);

			if (null == response){
				return null;
			}
			else{
				return new String(response);
			}

		} catch (Exception ex) {
			LOGGER.error(ex.getMessage(),ex);
			errorList.append(ex.getMessage()).append("\n");
		}
		return null;
	}

}
