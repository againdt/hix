package com.getinsured.hix.provider.search;

import java.util.Iterator;

import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

public class ProviderDetailsResult implements JSONAware {

	private long totalHits;
	private long currentStart;
	private int nextStart;
	private int pageSize;
	private QueryResponse solrResponse;
	private String message;

	public ProviderDetailsResult(QueryResponse solrResponse){
		this.solrResponse = solrResponse;
		this.solrResponse = solrResponse;
		this.totalHits = solrResponse.getResults().getNumFound();
		this.currentStart = solrResponse.getResults().getStart();
		this.pageSize = solrResponse.getResults().size();
	}
	
	public ProviderDetailsResult(String message){
		this.totalHits = 0;
		this.currentStart = 0;
		this.pageSize = 0;
		this.nextStart = 0;
		this.message = message;
	}
	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("total_hits", new Long(this.totalHits));
		obj.put("current_start", new Long(this.currentStart));
		obj.put("next_start",(this.currentStart+this.pageSize));
		obj.put("page_size", new Integer(this.pageSize));
		if(this.solrResponse != null){
			Iterator<SolrDocument> cursor = solrResponse.getResults().iterator();
			Provider firstProvider = null;
			while(cursor.hasNext()){
				if(firstProvider == null){
					firstProvider = new Provider(cursor.next());
				}else{
					firstProvider.mergeProviderDocument(cursor.next());
				}
			}
			obj.put("networkId", firstProvider.getNetworkId());
			obj.put("provider", firstProvider);
		}
		if(this.message != null){
			obj.put("message", this.message);
		}
		return obj.toJSONString();
	}
	
	public int getPageSize() {
		return this.pageSize;
	}
	public long getTotalHits() {
		return totalHits;
	}
	public long getCurrentStart() {
		return currentStart;
	}
	public int getNextStart() {
		return nextStart;
	}
	
	public void setCurrentStart(long start){
		this.currentStart = start;
	}
	
	public void setPageSize(int pageSize){
		this.pageSize = pageSize;
	}

}
