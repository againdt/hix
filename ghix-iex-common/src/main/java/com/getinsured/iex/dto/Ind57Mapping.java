package com.getinsured.iex.dto;

import java.util.List;

public class Ind57Mapping {
	private String caseNumber;
	private String userName;
    private Long updatedMailingAddressLocationID;
    private Boolean mailingAddressChanged;
	private List<String> memberGuids;

	/**
	 * @return the caseNumber
	 */
	public String getCaseNumber() {
		return caseNumber;
	}

	/**
	 * @param caseNumber
	 *            the caseNumber to set
	 */
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName
	 *            the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Long getUpdatedMailingAddressLocationID() {
		return updatedMailingAddressLocationID;
	}

	public void setUpdatedMailingAddressLocationID(Long updatedMailingAddressLocationID) {
		this.updatedMailingAddressLocationID = updatedMailingAddressLocationID;
	}

	public Boolean getMailingAddressChanged() {
		return mailingAddressChanged;
	}

	public void setMailingAddressChanged(Boolean mailingAddressChanged) {
		this.mailingAddressChanged = mailingAddressChanged;
	}

	public List<String> getMemberGuids() {
		return memberGuids;
	}

	public void setMemberGuids(List<String> memberGuids) {
		this.memberGuids = memberGuids;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Ind57Mapping [caseNumber=");
		builder.append(caseNumber);
		builder.append(", userName=");
		builder.append(userName);		
		builder.append(", updatedMailingAddressLocationID=");
		builder.append(updatedMailingAddressLocationID);
		builder.append(", mailingAddressChanged=");
		builder.append(mailingAddressChanged);
		builder.append("]");
		return builder.toString();
	}

}
