<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!-- Getting Started 1: previously NOT ENROLLED  -->
<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.openEnrollmentEnd}" var="enollEndDate" />
<fmt:formatDate pattern="MM/dd/yyyy" value="${empEnrollment.coverageDateStart}" var="coverageEffDt" />
           
<p><spring:message  code="label.toenrollin"/> ${employerName} <spring:message  code="label.sponsoredcovmessage"/> ${enollEndDate}</p>
<p><spring:message  code="label.coveffective"/> ${coverageEffDt}</p>
<div class="form-actions margin-top50">
	<a href="verifyemployee" class="btn btn-primary pull-right"> <spring:message  code="label.next"/> </a>
</div>