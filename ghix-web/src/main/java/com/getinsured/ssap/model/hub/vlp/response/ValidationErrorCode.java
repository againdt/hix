package com.getinsured.ssap.model.hub.vlp.response;

/**
 * Error response metadata.
 * <p>
 *   This contains more detailed information about error response. ({@link ResponseCode#VALIDATION_FAILED}.
 * </p>
 * @see ResponseCode
 * @author Yevgen Golubenko
 * @since 2/17/19
 */
public enum ValidationErrorCode
{
  NONE("NONE", 0, "No validation error code"),
  BENEFITS_LIST_REQUIRED("HE001022", 1022, "Benefits list is required"),
  ALIEN_NUMBER_REQUIRED("HE001023", 1023, "Alien number is required"),
  I94_NUMBER_REQUIRED("HE001024", 1024, "I-94 Number is required"),
  CASE_NUMBER_REQUIRED("HE001025", 1025, "Case Number is required"),
  DOCTYPE_ID_REQUIRED("HE001026", 1026, "Document Type ID is required"),
  LAST_NAME_REQUIRED("HE001029", 1029, "Last Name is required"),
  FIRST_NAME_REQUIRED("HE001030", 1030, "First Name is required"),
  DATE_OF_BIRTH_REQUIRED("HE001032", 1032, "Date of Birth is required"),
  ALIEN_NUMBER_INVALID_FORMAT("HE001040", 1040, "Alien number must contain only 9 digits"),
  I94_NUMBER_INVALID_FORMAT("HE001041", 1041, "I-94 Number must contain only 11 digits"),
  CASE_NUMBER_INVALID_FORMAT("HE001042", 1042, "Case Number must contain only 15 digits OR 13 alphanumerical followed by 2 alphabetic characters"),
  LAST_NAME_INVALID_FORMAT("HE001045", 1045, "Last Name must contain only: letters, spaces, hyphens and apostrophes, between 1 and 40 characters"),
  FIRST_NAME_INVALID_FORMAT("HE001046", 1046, "Fist Name must contain only: letters, spaces, hyphens and apostrophes, between 1 and 25 characters"),
  BIRTH_DATE_INVALID("HE001125", 1125, "Birth Date cannot be greater than current date"),
  SOFTWARE_VERSION_INVALID_FORMAT("HE001051",  1051, "Software version must be between 1 and 30 characters"),
  ALIAS_NAME_INVALID("HE001053", 1053, "Alias name cannot be more than 40 letters, or must only contain: letters, spaces, hyphens and apostrophes"),
  COMMENTS_INVALID_FORMAT("HE001054", 1054, "Comments must be between 1 and 400 characters"),
  RECEIPT_NUMBER_INVALID_FORMAT("HE001103", 1103, "Receipt Number must be 13 characters, 3 alpha-numeric followed by 10 numeric"),
  NATURALIZATION_NUMBER_INVALID_FORMAT("HE001119", 1119, "Naturalization Number must be alpha-numeric, between 6 and 12 characters"),
  CITIZENSHIP_NUMBER_INVALID_FORMAT("HE001120", 1120, "Citizenship Number must be betwee 6 and 12 digits, commas not allowed"),
  SEVISID_REQUIRED_OR_INVALID_FORMAT("HE001118", 1118, "SEVISID must be 10 digits, or SEVISID is required"),
  VALIDATION_FAILED("HE003000", -1, "Validation failed, please check that all required fields are present and in correct format"),
  EXCEPTION("EXCEPTION", -2, "Exception occurred while calling HUB service")
  ;

  private String name;
  private int code;
  private String message;
  private String exceptionDetails;

  /**
   * Constructs new {@code ResponseCode} with given code name/number.
   *
   * @param name name of this code.
   * @param code integer representation of this code.
   * @param message optional message text for this code.
   */
  ValidationErrorCode(String name, int code, String message)
  {
    this.name = name;
    this.code = code;
    this.message = message;
  }

  public String getName()
  {
    return name;
  }

  public void setName(final String name)
  {
    this.name = name;
  }

  public int getCode()
  {
    return code;
  }

  public void setCode(final int code)
  {
    this.code = code;
  }

  public String getMessage()
  {
    return message;
  }

  public void setMessage(final String message)
  {
    this.message = message;
  }

  public String getExceptionDetails() {
    return exceptionDetails;
  }

  public void setExceptionDetails(String exceptionDetails) {
    this.exceptionDetails = exceptionDetails;
  }

  public static ValidationErrorCode fromResponseCode(ResponseCode responseCode) {
    for(ValidationErrorCode vec : ValidationErrorCode.values()) {
      if(vec.getName().equals(responseCode.getName())) {
        return vec;
      }
    }

    return ValidationErrorCode.NONE;
  }

  @Override
  public String toString() {
    return "ValidationErrorCode{" +
        "name='" + name + '\'' +
        ", code=" + code +
        ", message='" + message + '\'' +
        ", exceptionDetails='" + exceptionDetails + '\'' +
        '}';
  }}
