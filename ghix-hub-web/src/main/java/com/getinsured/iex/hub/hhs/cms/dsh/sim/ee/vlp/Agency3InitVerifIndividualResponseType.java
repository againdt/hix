
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Agency3InitVerifIndividualResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Agency3InitVerifIndividualResponseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}ResponseMetadata"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}LawfulPresenceVerifiedCode"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}Agency3InitVerifIndividualResponseSet"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency3InitVerifIndividualResponseType", propOrder = {
    "responseMetadata",
    "lawfulPresenceVerifiedCode",
    "agency3InitVerifIndividualResponseSet"
})
public class Agency3InitVerifIndividualResponseType {

    @XmlElement(name = "ResponseMetadata", required = true)
    protected ResponseMetadataType responseMetadata;
    @XmlElement(name = "LawfulPresenceVerifiedCode", required = true)
    protected String lawfulPresenceVerifiedCode;
    @XmlElement(name = "Agency3InitVerifIndividualResponseSet", required = true)
    protected Agency3InitVerifIndividualResponseSetType agency3InitVerifIndividualResponseSet;

    /**
     * Gets the value of the responseMetadata property.
     * 
     * @return
     *     possible object is
     *     {@link ResponseMetadataType }
     *     
     */
    public ResponseMetadataType getResponseMetadata() {
        return responseMetadata;
    }

    /**
     * Sets the value of the responseMetadata property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResponseMetadataType }
     *     
     */
    public void setResponseMetadata(ResponseMetadataType value) {
        this.responseMetadata = value;
    }

    /**
     * Gets the value of the lawfulPresenceVerifiedCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLawfulPresenceVerifiedCode() {
        return lawfulPresenceVerifiedCode;
    }

    /**
     * Sets the value of the lawfulPresenceVerifiedCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLawfulPresenceVerifiedCode(String value) {
        this.lawfulPresenceVerifiedCode = value;
    }

    /**
     * Gets the value of the agency3InitVerifIndividualResponseSet property.
     * 
     * @return
     *     possible object is
     *     {@link Agency3InitVerifIndividualResponseSetType }
     *     
     */
    public Agency3InitVerifIndividualResponseSetType getAgency3InitVerifIndividualResponseSet() {
        return agency3InitVerifIndividualResponseSet;
    }

    /**
     * Sets the value of the agency3InitVerifIndividualResponseSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link Agency3InitVerifIndividualResponseSetType }
     *     
     */
    public void setAgency3InitVerifIndividualResponseSet(Agency3InitVerifIndividualResponseSetType value) {
        this.agency3InitVerifIndividualResponseSet = value;
    }

}
