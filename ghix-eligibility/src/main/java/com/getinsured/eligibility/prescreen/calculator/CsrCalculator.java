package com.getinsured.eligibility.prescreen.calculator;

import org.springframework.stereotype.Component;

/**
 * 
 * This class contains the implementation of the CSR caluclator which bases its
 * calculation on the FPL percentage.
 * 
 * @author Sunil Desu
 * @since April 17 2013
 * 
 */
@Component
public class CsrCalculator {

	private String csr = "";
	private static final String CS2 = "CS2";
	private static final String CS3 = "CS3";
	private static final String CS6 = "CS6";
	private static final String CS5 = "CS5";
	private static final String CS4 = "CS4";
	private static final String CS1 = "CS1";

	private static final double CS6_LOWER_LIMIT_INCL = 100.0;
	private static final double CS6_UPPER_LIMIT_INCL = 150.0;

	private static final double CS5_LOWER_LIMIT_EXCL = 150.0;
	private static final double CS5_UPPER_LIMIT_INCL = 200.0;

	private static final double CS4_LOWER_LIMIT_EXCL = 200.0;
	private static final double CS4_UPPER_LIMIT_INCL = 250.0;

	private static final double CS1_LOWER_LIMIT_EXCL = 250.0;
	private static final double CS2_LOWER_LIMIT_INCL = 300.0;
	private static final double CS3_LOWER_LIMIT_EXCL = 300.0;

	private static final String MEDICAID_LEVEL = "Medicaid";

	/**
	 * This method is exposed for services to invoke by passing the FPL
	 * percentage. The corresponding CSR value is returned.
	 * 
	 * @author Sunil Desu
	 * @since April 17 2013
	 * @param fpl
	 *            percentage for which the CSR has to be evaluated
	 * @return csr
	 */
	public String computeCSR(double fpl) {

		if (fpl < CS6_LOWER_LIMIT_INCL) {
			return MEDICAID_LEVEL;
		}

		if (fpl > CS1_LOWER_LIMIT_EXCL) {
			csr = CS1;
		} else if (fpl > CS4_LOWER_LIMIT_EXCL && fpl <= CS4_UPPER_LIMIT_INCL) {
			csr = CS4;
		} else if (fpl > CS5_LOWER_LIMIT_EXCL && fpl <= CS5_UPPER_LIMIT_INCL) {
			csr = CS5;
		} else if (fpl >= CS6_LOWER_LIMIT_INCL && fpl <= CS6_UPPER_LIMIT_INCL) {
			csr = CS6;
		}

		return csr;
	}
	public String computeCSR(double fpl, boolean isNativeAmericanHousehold) {
		if (fpl < CS6_LOWER_LIMIT_INCL) {
			return MEDICAID_LEVEL;
		}
		if (isNativeAmericanHousehold && fpl >=CS6_LOWER_LIMIT_INCL && fpl<=CS2_LOWER_LIMIT_INCL)
		{
			return CS2;
		}
		if (isNativeAmericanHousehold && fpl>CS3_LOWER_LIMIT_EXCL)
		{
			return CS3;
		}
		if (fpl > CS1_LOWER_LIMIT_EXCL) {
			csr = CS1;
		} else if (fpl > CS4_LOWER_LIMIT_EXCL && fpl <= CS4_UPPER_LIMIT_INCL) {
			csr = CS4;
		} else if (fpl > CS5_LOWER_LIMIT_EXCL && fpl <= CS5_UPPER_LIMIT_INCL) {
			csr = CS5;
		} else if (fpl >= CS6_LOWER_LIMIT_INCL && fpl <= CS6_UPPER_LIMIT_INCL) {
			csr = CS6;
		}
		return csr;
	}
}
