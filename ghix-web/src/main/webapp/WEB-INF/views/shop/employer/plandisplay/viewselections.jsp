<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css"  href="<c:url value="/resources/css/shopstyles.css"/>" />
<style>
  .dialogCSS{background-color: white}
</style>

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<div class="gutter10">
  <div class="row-fluid">
		<c:if test="${renewalExist=='Y'}">
	    <div class="span3" id="sidebar">
	    	<div class="header">
	      		<h4>Coverage for Employees</h4>
	      	</div>
	      	<p>The coverage selections in the <br/> two tabs to the right display what <br/> you selected for your employees</p>
	    </div><!-- #sidebar -->	
		</c:if>

      <div class="span9" id="rightpanel">
          
    <!--titlebar-->
		<input type="hidden" name="currentEnrollmentVal"  id="currentEnrollmentVal" value="${currentEnrollment}" />
		<input type="hidden" name="upcomingEnrollmentVal" id="upcomingEnrollmentVal" value="${upcomingEnrollment}" />
    <div class="gutter10">
      <c:if test="${renewalExist=='Y'}">
        <div class="row-fluid bubble-nav-shop">
           <ul class="nav nav-tabs">
            <li id="currentEnrollmentTab" class="active" ><a href="#" onclick="loadData('current');">Current Coverage</a></li>
            <li id="upcomingEnrollmentTab"><a href="#"  onclick="loadData('upcoming');">Upcoming Coverage</a></li>
          </ul>
          <div class="alert">
            <span class="hide" id="currentEnrollmentAlert">The information below is related to your current coverage which ends on <strong><span id="currentCoverageEndDate"></span></strong>.</span>
            <span class="" id="upcomingEnrollmentAlert">The information below is related to your upcoming coverage which begins on <strong><span id="upcomingCoverageEndDate"></span></strong>.<span>
          </div>
        </div>
      </c:if>
      <h3 id="skip"><spring:message code = "label.viewyourSelections"/></h3>
			<p></p>
			<div class="row-fluid" id="errorMsgDiv" style="display:none;"> 
				<div style="font-size: 15px; color: red">
						<!--&nbsp;<p><c:out value="${errorMsg}"></c:out><p/>-->
						&nbsp;<p><spring:message code = "label.noPlansFound"/></p>
					<br>
				</div>
			</div>
		
		<div class="row-fluid" id="dataDiv">
          <p><spring:message code= "label.yourCovEffectiveDt"/> <span id="coverageStartDate">${coverageStartDate}</span> </p>
          <div class="header">
          	<h4><spring:message code= "label.yourSelections"/></h4>
          </div>
          <table class="table table-border-none">
	          <tr>
	            <td class="span5" scope="row"><spring:message code="label.contri"/> %</td>
	            <td><strong><spring:message code= "label.employees"/></strong></td>
	            <td><strong><span id="employerIndvContribution">${employerIndvContribution}</span>%</strong></td>
	            
	          </tr>
	          <tr>
	            <td class="span5" scope="row">&nbsp;</td>
	            <td><strong><spring:message code= "label.dependent"/></strong></td>
	            <td><strong><span id="employerDepeContribution">${employerDepeContribution}</span>%</strong></td>
	            
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.planLevel"/></td>
		          <td><strong><span id="tierType">${tierType}</span></strong></td>
		          <td>&nbsp;</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.refPlan"/></td>
		          <td><strong><span id="benchmarkPlanName">${benchmarkPlanName}</span></strong></td>
		          <td>&nbsp;</td>
	          </tr>
          </table>
          <div class="header">
          	<h4><spring:message code = "label.monthlyContriCost"/></h4>
          </div>
          <table class="table table-border-none">
	          <tr>
	            <td class="span5" scope="row"><spring:message code = "label.contriForEmp"/></td>
	            <td><strong><span id="employerBenchmarkIndvPremium">${employerBenchmarkIndvPremium}</span> /<spring:message code = "label.month"/> </strong> (<spring:message code = "label.basedOnRoster"/> <strong><span id="totalEmployeesCount">${totalEmployeesCount}</span></strong> <spring:message code = "label.emp"/>)
				</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.contriForDependent"/></td>
		          <td><strong><span id="employerBenchmarkDeptPremium">${employerBenchmarkDeptPremium}</span> / <spring:message code = "label.month"/></strong> (<spring:message code = "label.basedOnRoster"/> <strong><span id="totalDependentsCount">${totalDependentsCount}</span></strong> <spring:message code = "label.dependnt"/>)</td>
	          </tr>
	          <tr>
		          <td scope="row"><spring:message code = "label.totContri"/></td>
		          <td><strong><span id="totalBenchMarkPremium">${totalBenchMarkPremium}</span> / <spring:message code = "label.month"/></strong></td>
	          </tr>
          </table>
       
       	  <div class="header">
          	<h4><spring:message code = "label.potTaxImpact"/></h4>
          </div>
          <table class="table table-border-none">
            <tr>
			 <td class="span5" scope="row"><spring:message code = "label.estTaxCredit"/></td>
             <td><strong><span id="taxCredit">${taxCredit}</span> / <spring:message code = "label.month"/></strong></td>
			</tr>
          </table>
          <div class="gutter10"><spring:message code = "label.estTaxPenaltyAmt"/> </div>
          </div>
        </div>
        <!--gutter10--> 
        </div>
      <!--rightpanel--> 
    </div>
    <!--row-fluid--> 
  </div>
<!--main-->

<div id="waitDiv" class="modal hide fade"  tabindex="-1" role="dialog" aria-labelledby="waitingDiv" aria-hidden="true">
     <div class="modal-header"></div>
     <div class="modal-body">
       <h3 class="green"><spring:message code = "label.processingMsg"/></h3>
<img src="<c:url value="/resources/img/loader_greendots.gif"/>" alt="loader dots">
     </div>
     <div class="modal-footer"></div>
</div>

<script>
$(document).ready(function () {
    $('.info').tooltip();
    
    loadData('current');
});

function isInvalidCSRFToken = function(xhr) {
  var rv = false;
  if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {      
    alert($('Session is invalid').text());
    rv = true;
  }
  return rv;
}

function loadData(val){
	
	var enrollmentVal="";
	if(val=="current"){
		enrollmentVal=$('#currentEnrollmentVal').val();
		

			$('#currentEnrollment').addClass('active');
			$('#upcomingEnrollment').removeClass('active');	
			$('#currentEnrollmentAlert').show();
			$('#upcomingEnrollmentAlert').hide();

	}else if(val=="upcoming"){
		enrollmentVal=$('#upcomingEnrollmentVal').val();
		

			$('#currentEnrollment').removeClass('active');
			$('#upcomingEnrollment').addClass('active');
			$('#currentEnrollmentAlert').hide();
			$('#upcomingEnrollmentAlert').show();

	}
	$('#waitDiv').modal('show');
	var subUrl = '<c:url value="/shop/employer/plandisplay/viewselectiondata">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
		type : "POST",
		url : subUrl,
		dataType:'json',
		data: {enrollmentValue: enrollmentVal}, 
		success : function(response,xhr) {
		      if(isInvalidCSRFToken(xhr))             
		        return;
			if(response.errorMsg!=''){
				$('#errorMsgDiv').show();
				$('#dataDiv').hide();
			}	 
			else{
				$('#errorMsgDiv').hide();
				$('#dataDiv').show();
				
				var obj = response;
				$('#employerIndvContribution').html(obj['employerIndvContribution']);
				$('#employerDepeContribution').html(obj['employerDepeContribution']);
				$('#tierType').html(obj['tierType']);
				$('#benchmarkPlanName').html(obj['benchmarkPlanName']);
				$('#employerBenchmarkIndvPremium').html(obj['employerBenchmarkIndvPremium']);
				$('#employerBenchmarkDeptPremium').html(obj['employerBenchmarkDeptPremium']);
				$('#totalBenchMarkPremium').html(obj['totalBenchMarkPremium']);
				$('#taxCredit').html(obj['taxCredit']);
				$('#totalEmployeesCount').html(obj['totalEmployeesCount']);
				$('#totalDependentsCount').html(obj['totalDependentsCount']);
				$('#coverageStartDate').html(obj['coverageStartDate']);
				$('#stateCode').html(obj['stateCode']);
				if(val=='current'){
					$('#currentCoverageEndDate').html(obj['coverageEndDate']);
				}else if(val=='upcoming'){
					$('#upcomingCoverageEndDate').html(obj['coverageEndDate']);
				}
			}
			$('#waitDiv').modal('hide');
		},
		error : function(e) {
			$('#waitDiv').modal('hide');
			alert("Failed to get View-Selections Data");
		}
	});
}  
</script>