package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanRate;


public interface IPlanRateRepository extends JpaRepository<PlanRate, Integer> {		
	@Query("SELECT PR FROM PlanRate PR WHERE PR.plan.id= :planid")
	List<PlanRate> getPlanRates(@Param("planid") int planid);
	 
}

