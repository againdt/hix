//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ActivityType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;


/**
 * A data type for an
 * 				activity that enrolls a person with an insurance policy (starting
 * 				that person in the member role).
 * 
 * <p>Java class for EnrollmentActivityType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnrollmentActivityType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://hix.cms.gov/0.1/hix-core}ActivityType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}EnrollmentActivityReasonText" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnrollmentActivityType", propOrder = {
    "enrollmentActivityReasonText"
})
public class EnrollmentActivityType
    extends ActivityType
{

    @XmlElement(name = "EnrollmentActivityReasonText")
    protected TextType enrollmentActivityReasonText;

    /**
     * Gets the value of the enrollmentActivityReasonText property.
     * 
     * @return
     *     possible object is
     *     {@link TextType }
     *     
     */
    public TextType getEnrollmentActivityReasonText() {
        return enrollmentActivityReasonText;
    }

    /**
     * Sets the value of the enrollmentActivityReasonText property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextType }
     *     
     */
    public void setEnrollmentActivityReasonText(TextType value) {
        this.enrollmentActivityReasonText = value;
    }

}
