<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>

<script type="text/javascript">
	function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
            String.prototype.trim = function() {
            	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
            };
		}
	}
	</script>

<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
	       <li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
	       <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
           <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
	       <li><spring:message  code="pgheader.financialInformation"/></li>
        </ul><!--page-breadcrumb ends-->
    </div>
	<div class="row-fluid issuer_info">
		<div class="centered_all">
			<div class="span3">
				<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
			</div>
			<div class="span9">
				<h1>${issuerObj.name}</h1>
			</div>
		</div>
	</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="financialInfo"/>
				</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.financialInformation"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" id="save-btn" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">
					<p><spring:message  code="label.verifyBankInfo"/></p>
			
				<form class="form-horizontal" id="frmIssuerEditFinInfo" name="frmIssuerEditFinInfo" action="<c:url value="/admin/issuer/financial/info/save" />" method="POST">
                    <df:csrfToken/>
                     <input type="hidden" id="paymentId" name="paymentId" value="<encryptor:enc value="${paymentMethodsObj.id}"></encryptor:enc>"/>
                     <input type="hidden" id="issuerId" name="issuerId" value="${encIssuerId}"/>
                     <input type="hidden" id="currentPaymentId" name="currentPaymentId" value="<encryptor:enc value="${curent_payment_id}"></encryptor:enc>"/>
                     <input type="hidden" id="currentBankAccountNumber" name="currentBankAccountNumber" value="<encryptor:enc value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}"></encryptor:enc>"/>
                     <input type="hidden" id="accountNumChanged" name="accountNumChanged" value="<encryptor:enc value="false"></encryptor:enc>"/>
                      
                    <div class="control-group">
						<label class="control-label required" for="paymentMethodName"><spring:message  code="label.paymentMethodName"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="paymentMethodName" id="paymentMethodName" class="input-large" value="${paymentMethodsObj.paymentMethodName}"/>
                            <div id="paymentMethodName_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    <div class="control-group">
						<label class="control-label required" for="bankName"><spring:message  code="label.bankName"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.bankName" id="bankName" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.bankName}"/>
                            <div id="bankName_error" class=""></div>
						</div>
					</div><!-- end of control-group--> 
					
					<div class="control-group">
						<label class="control-label required" for="bankABARoutingNumber"><spring:message  code="label.abaRoutingNumber"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<c:choose>
							    <c:when test="${not empty routing_number && isPaymentGateway eq true}">
							        <input type="text"  name="financialInfo.bankInfo.routingNumber" id="bankABARoutingNumber" class="input-large" value="${routing_number}" maxlength="9"/>
							    </c:when>
							    <c:otherwise>
							        <input type="text"  name="financialInfo.bankInfo.routingNumber" id="bankABARoutingNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.routingNumber}" maxlength="9"/>
							    </c:otherwise>
							</c:choose>
                            <div id="bankABARoutingNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->                  
                    
                    <div class="control-group">
						<label class="control-label required" for="bankAccountNumber"><spring:message  code="label.bankAcctNumber"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /><a class="code" rel= "popover" data-img="/hix/resources/img/routing.png"  data-toggle="popover" href="#"><i class="icon-question-sign"></i></a></label>
						<div class="controls">
							<c:choose>
							    <c:when test="${isPaymentGateway eq true}">
									<%if(request.getParameter("paymentId")!=null){%>
										<input type="text" name="financialInfo.bankInfo.accountNumber" id="bankAccountNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}" maxlength="40" disabled/>
											<c:if test="${flow != null && flow == 'edit'}">
												<input type="button" class="btn btn-primary" name="editAccountNo" value="Click To Edit" onclick="javascript:clearAccountNoField();"/>
												<div id="bankAccountNumber_error" class=""></div>
											</c:if>
									<%}else{%>
										<input type="text" name="financialInfo.bankInfo.accountNumber" id="bankAccountNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}" maxlength="40"/>
									<%} %>
							    </c:when>
							    <c:otherwise>
							        <input type="text" name="financialInfo.bankInfo.accountNumber" id="bankAccountNumber" class="input-large" value="${paymentMethodsObj.financialInfo.bankInfo.accountNumber}" maxlength="40"/>
							    </c:otherwise>
							</c:choose>
                            <div id="bankAccountNumber_error" class=""></div>
						</div>
					</div><!-- end of control-group-->                    
                    
                    <div class="control-group">
						<label for="nameOnAccount" class="control-label required"><spring:message code="label.nameOnAccount" /> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<input type="text" name="financialInfo.bankInfo.nameOnAccount"
								value="${paymentMethodsObj.financialInfo.bankInfo.nameOnAccount}"
								id="nameOnAccount" maxlength="20" class="input-xlarge" size="30" placeholder="">
							<div id="nameOnAccount_error"></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label for="accountType" class="control-label required"><spring:message code="label.bankAcctType"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="controls">
							<select id="accountType"
								name="financialInfo.bankInfo.accountType">
								<option value=""><spring:message code="label.selectUpperCase"/></option>
								<option
									<c:if test="${'C' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="C"><spring:message code="label.bankAcctTypeChecking"/></option>
								<option
									<c:if test="${'S' == paymentMethodsObj.financialInfo.bankInfo.accountType}"> SELECTED </c:if>
									value="S"><spring:message code="label.bankAcctTypeSaving"/></option>
							</select>
							<div id="accountType_error"></div>
						</div>						
						<!-- end of controls-->
					</div>
					<div class="controls" id="autoPayCheckBox">
						
							<label class="checkbox"> <input type="checkbox" name="chkboxAutoPay" id="chkboxAutoPay" <c:if test="${paymentMethodsObj.isDefault == 'Y' || curent_payment_id == 0 }">checked="checked" disabled="disabled"</c:if> <c:if test="${paymentMethodsObj.status == 'InActive'}">disabled="disabled"</c:if>> 						
								<spring:message  code="label.setAsDefaultPaymentMethod"/>
							</label>
					
					</div><!-- end of control-group-->
                    <div class="form-actions paddingLR20">
                    	
                   	</div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>

<div id="dialog-modal" style="display: none;">
	<p>
		<c:if test="${not empty invalid_routing_number && isPaymentGateway eq true}">
	    	${invalid_routing_number} <br/>
		</c:if>
	</p>
  	<p>
        <c:if test="${not empty payment_error && isPaymentGateway eq true}">
            ${payment_error} <br/>
        </c:if>
    </p>
</div>
<div class="modal hide confirmModal">
    <div class="modal-header"><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div>
    <div class="modal-body">
	    <p><spring:message  code='paymentMethod.defaultChange' javaScriptEscape='true'/></p>
	    <button class="btn btn-primary paymentDefaultConfirm">Yes</button>
	    <a href="javascript:void(0)" data-dismiss="modal" class="btn" style="float:none;">No</a>
    </div>
</div>
<script type="text/javascript">
function submitForm(){	
	/* if($("#paymentMethodName").val() != "" && $("#bankName").val() && $("#bankABARoutingNumber").val() && $("#bankAccountNumber").val() && $("#nameOnAccount").val() && $("#accountType").val()){
		if($("#chkboxAutoPay").is(':checked')){
			var isDefault = '${paymentMethodsObj.isDefault}';		
			var currPaymentId = '${curent_payment_id}';
			if( isDefault != 'Y' && currPaymentId > 0 ){ //if it's not a default payment method and not adding payment methofd fopr first time, then ask for confirmation 
				$(".confirmModal").modal();
			}
			else{		
				$("#frmIssuerEditFinInfo").submit(); //validateRoutingNumber();
			}
		}
		else{		
			$("#frmIssuerEditFinInfo").submit(); //validateRoutingNumber();
		}		
	}else{
		return false;
	} */
	
	if($("#chkboxAutoPay").is(':checked')){
		var isDefault = '${paymentMethodsObj.isDefault}';		
		var currPaymentId = '${curent_payment_id}';
		if( isDefault != 'Y' && currPaymentId > 0 ){ //if it's not a default payment method and not adding payment methofd for first time, then ask for confirmation 
			$(".confirmModal").modal();
		}
		else{		
			$("#frmIssuerEditFinInfo").submit(); //validateRoutingNumber();
		}
	}else{	
		$("#frmIssuerEditFinInfo").submit();
	}	
}	
$('.ttclass').tooltip();
</script>

<script type="text/javascript">
	var csrValue = $("#csrftoken").val();
	jQuery.validator.addMethod("nonNegative", function(value, element, param) {
		if(value <= 0){
			return false; 
		}
		return true;
	});

	<%-- <%if(request.getParameter("paymentId")!=null){%> --%>
		var validator = $("#frmIssuerEditFinInfo").validate({ 		
				rules : {
					'paymentMethodName' : {required : true},
					'financialInfo.bankInfo.bankName' : {required : true},
					'financialInfo.bankInfo.accountNumber' : {required : true, number : true},
					'financialInfo.bankInfo.nameOnAccount' : {required : true},
					'financialInfo.bankInfo.routingNumber' : {required : true, number : true, minlength : 9, maxlength :9},
					'financialInfo.bankInfo.accountType': {required : true}
				},
				messages : {
					'paymentMethodName' : { required : "<span><em class='excl'>!</em><spring:message  code='err.paymentMethodName' javaScriptEscape='true'/></span>"},
					'financialInfo.bankInfo.bankName': { required : "<span><em class='excl'>!</em><spring:message  code='err.bankName' javaScriptEscape='true'/></span>"},				
					'financialInfo.bankInfo.accountNumber' : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumber' javaScriptEscape='true'/></span>"},
						//nonNegative :"<span><em class='excl'>!</em><spring:message  code='err.bankAcctNumberValid' javaScriptEscape='true'/></span>"},
					'financialInfo.bankInfo.nameOnAccount' : { required : "<span> <em class='excl'>!</em><spring:message code='err.nameOnAccount' javaScriptEscape='true'/></span>"},
					'financialInfo.bankInfo.routingNumber' : { required : "<span><em class='excl'>!</em><spring:message  code='err.bankABARoutingNumber' javaScriptEscape='true'/></span>",
						RoutingNumberCheck : "<span> <em class='excl'>!</em><spring:message  code='err.bankInvalidRoutingNumber' javaScriptEscape='true'/></span>",
						RoutingNumberCheckDigits:  "<span> <em class='excl'>!</em><spring:message code='err.routingNumberDigitCheck' javaScriptEscape='true'/></span>",
						minlength : "<span> <em class='excl'>!</em><spring:message code='err.routingNumberDigitCheck' javaScriptEscape='true'/></span>"},
						//nonNegative :"<span><em class='excl'>!</em><spring:message  code='err.bankInvalidRoutingNumber' javaScriptEscape='true'/></span>"},
					'financialInfo.bankInfo.accountType' : { required : "<span> <em class='excl'>!</em><spring:message code='err.bankAcctType' javaScriptEscape='true'/></span>"}
					},
				errorClass: "error",
				errorPlacement: function(error, element) {
					var elementId = element.attr('id');
					error.appendTo( $("#" + elementId + "_error"));
					$("#" + elementId + "_error").attr('class','error');
				} 
			});
		<%-- <%}else{%>
			alert("ehere");
			var validator = $("#frmIssuerEditFinInfo").validate({ 		
				rules : {
					'financialInfo.bankInfo.bankName' : {required : true}
					
				},
				messages : {
					'financialInfo.bankInfo.bankName': { required : "<span><em class='excl'>!</em><spring:message  code='err.bankName' javaScriptEscape='true'/></span>"}
					
					},
				errorClass: "error",
				errorPlacement: function(error, element) {
					var elementId = element.attr('id');
					error.appendTo( $("#" + elementId + "_error"));
					$("#" + elementId + "_error").attr('class','error');
				} 
			});
		<%}%> --%>
		
		$('.code').popover({ html: true,
			  trigger: 'hover',
			  placement: 'top',
			  content: function(){return '<img src="'+$(this).data('img') + '" />';}
		 });
		
		/* function validateRoutingNumber()
		{
			var validateUrl = "<c:url value='/admin/issuer/financial/info/validateroutingnumber'/>";
			$.ajax({
				url : validateUrl,
				type : "POST",
				data : {
					routingNumber : $("#bankABARoutingNumber").val(),
					csrftoken:csrValue
				},
				success: function(response)
				{
					if(response == 'INVALID')
					{
						$("#bankABARoutingNumber_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='err.bankInvalidRoutingNumber' javaScriptEscape='true'/></span></label>");
						$("#bankABARoutingNumber_error").attr('class','error help-inline');
						return false;
					}
					else{
						 $("#frmIssuerEditFinInfo").submit();  
					}
				},
				
			});
		} */

jQuery.validator.addMethod("RoutingNumberCheck", function(value, element, param) {
			
			ie8Trim();
			zip = $('#bankABARoutingNumber').val().trim();
			if((zip == "")  || (isNaN(zip) ) ){
				return false; 
			}
			return true;
		});
		
		jQuery.validator.addMethod("RoutingNumberCheckDigits", function(value, element, param) {
		
			ie8Trim();
			zip = $('#bankABARoutingNumber').val().trim();
			if((zip == "")  || (isNaN(zip) || (zip.length < 9 ) )){
				return false; 
			}
			return true;
		});

</script>

<script type="text/javascript">
$(document).ready(function()
{	
	if('${invalid_routing_number}' != null && '${invalid_routing_number}' != '' && '${invalid_routing_number}' != null && '${invalid_routing_number}' != '')
	{
		 $( "#dialog-modal" ).dialog({
		     modal: true,
			 title: "Invalid Information",
			 buttons: {
			   Ok: function() {
			    $( this ).dialog( "close" );
			   }
			   }
		    });
	}
	
    if('${payment_error}' != null && '${payment_error}' != '')
    {
         $( "#dialog-modal" ).dialog({
             modal: true,
             title: "Invalid Information",
             buttons: {
               Ok: function() {
                $( this ).dialog( "close" );
               }
               }
            });
    }
    $(".paymentDefaultConfirm").click(function(){
        $("#frmIssuerEditFinInfo").submit(); 
        $(".confirmModal").modal('hide');
    });
    
});

function clearAccountNoField()
{
	document.getElementById('bankAccountNumber').value = "";
	$('#bankAccountNumber').attr("disabled", false);
	$('#accountNumChanged').val("true");
}

</script>