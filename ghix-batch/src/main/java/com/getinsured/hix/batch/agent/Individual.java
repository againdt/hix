package com.getinsured.hix.batch.agent;

import java.util.List;

public class Individual {
	
	private String firstName;
	private String lastName;
	
	private String emailAddress;
	
	private long consumerId;
	private String status;
	
	private String phoneNumber;
	private String address1;
	private String address2; 
	private String city;
	private String state;
	private String zip;
	private String noticeSerialNumber;
	private String noticeName;
	private String noticeEmailSubject;
	private long serialNo;	
	private List<Notices> noticeList;	

	public List<Notices> getNoticeList() {
		return noticeList;
	}

	public void setNoticeList(List<Notices> noticeList) {
		this.noticeList = noticeList;
	}

	public long getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(long serialNo) {
		this.serialNo = serialNo;
	}

	public String getNoticeSerialNumber() {
		return noticeSerialNumber;
	}

	public void setNoticeSerialNumber(String noticeSerialNumber) {
		this.noticeSerialNumber = noticeSerialNumber;
	}

	public String getNoticeName() {
		return noticeName;
	}

	public void setNoticeName(String noticeName) {
		this.noticeName = noticeName;
	}

	public String getNoticeEmailSubject() {
		return noticeEmailSubject;
	}

	public void setNoticeEmailSubject(String noticeEmailSubject) {
		this.noticeEmailSubject = noticeEmailSubject;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public long getConsumerId() {
		return consumerId;
	}

	public void setConsumerId(long consumerId) {
		this.consumerId = consumerId;
	}

	
	
	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	
	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getZip() {
		return zip;
	}

	public void setZip(String zip) {
		this.zip = zip;
	}

	
}
