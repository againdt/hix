package com.getinsured.eligibility.rrv.service;

import com.getinsured.eligibility.model.RrvSubmission;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface RrvService {

    Page<Long> findAllEligibleSsapApplication(long coverageYear, String applicationStatus, Pageable pageable);

    String process(List<Long> applicationIdList, long batchId, long coverageYear);

    List<RrvSubmission> rrvBatchProcess(List<? extends Long> ssapApplications, long batchId, long coverageYear) throws Exception;

    void saveRrvSubmissions(List<? extends RrvSubmission> rrvSubmissions);

    long getBatchId() throws Exception;

    void checkStatusOfPendingSubmissions(long coverageYear);

    void getVerificationResult(List<? extends RrvSubmission> rrvSubmissionList) throws Exception;

}
