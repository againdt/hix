package com.getinsured.eligibility.at.ref.service.nonfinancial;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;

/**
 * @author chopra_s
 * 
 */
@Component("referralNFConversionPostService")
@Scope("singleton")
public class ReferralNFConversionPostServiceImpl extends ReferralNFConversionServiceImpl implements ReferralNonFinancialService {
	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralNFConversionPostServiceImpl.class);

	@Override
	@ReferralTransactionAnno
	public boolean execute(NFProcessDTO nfProcessRequestDTO) throws Exception {
		LOGGER.info("ReferralNFConversionPostServiceImpl starts for - " + nfProcessRequestDTO);
		boolean blnComplete =  invokeInd71(nfProcessRequestDTO.getCompareMainDTO(), 
				nfProcessRequestDTO.getCurrentApplicationId(), nfProcessRequestDTO.getNfApplicationId());
		LOGGER.info("ReferralNFConversionPostServiceImpl ends for - " + nfProcessRequestDTO);
		return blnComplete;
	}
	
	private boolean invokeInd71(CompareMainDTO compareMainDTO, Long currentApplicationId,
			Long nonFinancialApplicationId) {
		boolean status = executeIND71(currentApplicationId, compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getHealthEnrollees(),
				compareMainDTO.getEnrolledApplication().getEnrolledApplicationAttributes().getDentalEnrollees());
		
		if (status) {
			LOGGER.info("IND71 update successful for  " + currentApplicationId);
			status = processCSSuccess(currentApplicationId, nonFinancialApplicationId);
			triggerAutomationCSNotice(compareMainDTO.getCurrentApplication().getCaseNumber());
		}
		return status;
	}

	
}
