  // Create the module and name it newMexico
  var newMexico = angular.module('newMexico', ['ngRoute']);

  // Configure our routes
  newMexico.config(function($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', { templateUrl : '../../resources/js/lce/pages/home.html', controller  : 'lifeChangeCtrl' })
      .when('/next', { templateUrl : '../../resources/js/lce/pages/next.html', controller  : 'nextCtrl' })
  });

  
  // Factory
  newMexico.factory('myService', function() {
    // Declare variable in the entire scope
    var lifeEventSelection;
    // Factory returns the get function, which
    return {
      getLifeEventSelection: function() {
        return lifeEventSelection;
      },
      setLifeEventSelection: function (cat) {
        lifeEventSelection = cat;
      }
    };
  });

  // Controller 1
  newMexico.controller('lifeChangeCtrl', ['$scope', '$http','$location', 'myService',
    function($scope, $http, $location, myService) {
      debugger;
    // This is an array of the life events; JSON object
    $scope.lifeEvents = [
      {eventName:"Marital Status", value: false},
      {eventName:"Number of Dependents", value: false},
      {eventName:"Loss of Minimum Essential Coverage", value: false},
      {eventName:"Incarceration", value: false},
      {eventName:"Lawful Presence", value: false},
      {eventName:"Change in Residence or Mailing Address", value: false},
      {eventName:"Income", value: false},
      {eventName:"Gain Other Minimum Essential Coverage", value: false},
      {eventName:"Other", value: false}
    ];
    $scope.next = function(path) {
      myService.setLifeEventSelection($scope.lifeEvents);
      $location.path(path);
    }
  }]);

  // Controller 2
  newMexico.controller('nextCtrl', ['$scope', '$http','$location', 'myService',
    function($scope, $http, $location, myService) {
    $scope.msg = myService.getLifeEventSelection();
  }])

  // Directives
  newMexico.directive('dateOfChange', function () {
    return {
      restrict: 'EA',
      template: 'Date of Change'
    };
  });

  newMexico.directive('blah', function () {
    return {
      restrict: 'EA',
      template: '<h1>Add details of your partner</h1><hr>'+'<h3>Household Information</h3>'+'<form class="form-horizontal">'+'<div class="control-group">'+'<label class="control-label">First Name: </label>' +'<div class="controls">' +'<input type="text" ng-model="household.firstName" placeholder="Fred" class="input-large span6" id="firstName" name="firstName">' +'</div>'+'</div>'+'<div class="control-group">'+'<label class="control-label">Last Name: </label>'+'<div class="controls">'+'<input type="text" placeholder="Jones" ng-model="household.lastName" class="input-large span6" id="lastName" name="lastName">'+'</div>'+'</div>' + '</form>' + '<a class="btn btn-primary" onClick="saveDataToJson();">Add Name to JSON</a>'
    };
  });



   function Ctrl($scope) {
   $scope.templates =
     [ { name: 'incarceration', url: '../../resources/js/lce/pages/incarceration.html'},
       { name: 'maritalstatus', url: 'maritalstatus'} ];
   $scope.template = $scope.templates[0];
 }