{
   "householdMember": [
      {
         "personId": "1",
         "name": {
            "firstName": "Rachel",
            "middleName": "L",
            "lastName": "Turner"
         },
         "dateOfBirth": "1966-08-10",
         "socialSecurityCard": {
            "socialSecurityNumber": "222222465"
         },
         "citizenshipImmigrationStatus": {
            "citizenshipAsAttestedIndicator": "true"
         },
         "incarcerationStatus": {
            "incarcerationAsAttestedIndicator": "false"
         }
      }
   ]
}