package com.getinsured.eligibility.ind19.client;

import java.util.HashSet;
import java.util.Set;

import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ui.enums.ApplicantEligibilityStatus;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Component
public final class Ind19ClientConstants {

	/**
	 * Making it a singleton
	 */
	private Ind19ClientConstants(){}
	
	public static final String IND19_WSDL_URL = GhixEndPoints.PLANDISPLAY_URL+"webservice/plandisplay/endpoints/IndividualInformationService.wsdl";
	
	public static final String IND19_GENERIC_ERROR_CODE = "19000000";
	public static final String IND19_INVALID_REQUEST_ERROR_CODE = "19000001";

	public static final String IND19_GENERIC_ERROR_MESSAGE = "Exception while consuming IND19. ";
	public static final String IND19_INVALID_REQUEST_ERROR_MESSAGE = "Invalid Request to the Ind19Client.";

	public static final Set<ApplicantEligibilityStatus> ELIGIBLE_STATUS_SET = new HashSet<ApplicantEligibilityStatus>();
	
	static{
		ELIGIBLE_STATUS_SET.add(ApplicantEligibilityStatus.APTC);
		ELIGIBLE_STATUS_SET.add(ApplicantEligibilityStatus.CSR);
		ELIGIBLE_STATUS_SET.add(ApplicantEligibilityStatus.QHP);
	}
	
	public static final int TYPE_0_EVENT = 0;
	public static final int TYPE_1_EVENT = 1;
	public static final int TYPE_2_EVENT = 2;
	public static final int TYPE_3_EVENT = 3;
	public static final int TYPE_4_EVENT = 4;
	
	public static final int HARDSHIP_EXEMPTION_AGE = 30;
	public static final String  ENRL_END_DATE_IS_BEFORE_COVG_DATE = "This action is not permitted as your coverage through this plan is ending soon.";


}
