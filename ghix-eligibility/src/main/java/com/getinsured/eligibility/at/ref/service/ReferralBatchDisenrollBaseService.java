package com.getinsured.eligibility.at.ref.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.getinsured.eligibility.at.ref.common.ReferralBatchDisenrollDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.plan.service.BatchPlanAvailabilityAdapter;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapterBatch;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
public abstract class ReferralBatchDisenrollBaseService extends LceProcessHandlerBaseService implements ReferralBatchDisenrollService {
	private static final Logger LOGGER = Logger.getLogger(ReferralBatchDisenrollBaseService.class);
	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;

	@Autowired
	@Qualifier("batchPlanAvailabilityAdapter")
	private BatchPlanAvailabilityAdapter batchPlanAvailabilityAdapter;

	@Autowired
	@Qualifier("enrollmentUpdateAdapterBatch")
	private EnrollmentUpdateAdapterBatch enrollmentUpdateAdapterBatch;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	void closeSsapApplicationById(long ssapApplicationId) {
		LOGGER.info("Close Application id " + ssapApplicationId);
		SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
		ssapApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		ssapApplicationRepository.save(ssapApplication);
	}
	
	ReferralBatchDisenrollDTO validateAndPopulateDTO(String[] arguments) {
		final long currentApplicationId = Long.valueOf(arguments[0]);
		final long enrolledApplicationId = Long.valueOf(arguments[1]);
		final String terminationDateStr = arguments[2];

		if (currentApplicationId == ReferralConstants.NONE || enrolledApplicationId == ReferralConstants.NONE) {
			throw new GIRuntimeException("Curent application id or Enrolled application id is not correct" + currentApplicationId + " " + enrolledApplicationId);
		} else {
			final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
			final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
			final Date terminationDate = ReferralUtil.convertStringToDate(terminationDateStr, ReferralConstants.DB_DATE_FORMAT);
			if (terminationDate == null) {
				throw new GIRuntimeException("Termination date is not valid for " + currentApplicationId);
			}

			if (currentApplication == null || enrolledApplication == null || currentApplication.getCmrHouseoldId() == null || enrolledApplication.getCmrHouseoldId() == null) {
				throw new GIRuntimeException("Curent application or Enrolled application not found for" + currentApplicationId + " " + enrolledApplicationId);
			}

			if (currentApplication.getCmrHouseoldId().compareTo(enrolledApplication.getCmrHouseoldId()) != 0) {
				throw new GIRuntimeException("Households are not same for newSsapApplicationId and enrolledSsapApplicationId");
			}

			if (enrolledApplication.getCoverageYear() != currentApplication.getCoverageYear()) {
				throw new GIRuntimeException("Coverage year is not same for newSsapApplicationId and enrolledSsapApplicationId");
			}

			final ReferralBatchDisenrollDTO referralBatchDisenrollDTO = new ReferralBatchDisenrollDTO();
			referralBatchDisenrollDTO.setCurrentApplicationId(currentApplicationId);
			referralBatchDisenrollDTO.setEnrolledApplicationId(enrolledApplicationId);
			referralBatchDisenrollDTO.setCurrentApplication(currentApplication);
			referralBatchDisenrollDTO.setEnrolledApplication(enrolledApplication);
			referralBatchDisenrollDTO.setTerminationDate(terminationDate);
			referralBatchDisenrollDTO.setTerminationDateStr(terminationDateStr);
			return referralBatchDisenrollDTO;
		}

	}

	void populateEnrollment(ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		final Map<String, Object> enrollmentDetails = ssapEnrolleeHandler.fetchEnrollmentID(referralBatchDisenrollDTO.getEnrolledApplicationId());
		if (enrollmentDetails == null) {
			throw new GIRuntimeException("No Enrollment Details found for - " + referralBatchDisenrollDTO.getEnrolledApplicationId());
		}

		if (enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO) != null && enrollmentDetails.get(ReferralConstants.HEALTH) != null) {
			referralBatchDisenrollDTO.setHealthEnrolled(true);
			referralBatchDisenrollDTO.setHealthEnrollmentId(ReferralUtil.convertToLong(enrollmentDetails.get(ReferralConstants.HEALTH)));
			referralBatchDisenrollDTO.setHealthEnrollees(fetchEnrollees(referralBatchDisenrollDTO.getEnrolledApplication(), (EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO)));
			referralBatchDisenrollDTO.setHealthTerminationFuture(isTerminationFuture(referralBatchDisenrollDTO.getTerminationDate(), ((EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO)).getEnrolleeShopDTOList().get(0)
			        .getEffectiveEndDate()));
		}

		if (enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO) != null && enrollmentDetails.get(ReferralConstants.DENTAL) != null) {
			referralBatchDisenrollDTO.setDentalEnrolled(true);
			referralBatchDisenrollDTO.setDentalEnrollmentId(ReferralUtil.convertToLong(enrollmentDetails.get(ReferralConstants.DENTAL)));
			referralBatchDisenrollDTO.setDentalEnrollees(fetchEnrollees(referralBatchDisenrollDTO.getEnrolledApplication(), (EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO)));
			referralBatchDisenrollDTO.setDentalTerminationFuture(isTerminationFuture(referralBatchDisenrollDTO.getTerminationDate(), ((EnrollmentShopDTO) enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO)).getEnrolleeShopDTOList().get(0)
			        .getEffectiveEndDate()));
		}
	}

	private boolean isTerminationFuture(Date terminationDate, Date effectiveEndDate) {
		DateTime coverageDate = new DateTime(terminationDate.getTime());
		DateTime enrollmentDate = new DateTime(effectiveEndDate.getTime());
		return coverageDate.isBefore(enrollmentDate);
	}

	private List<String> fetchEnrollees(SsapApplication enrolledApplication, EnrollmentShopDTO enrollmentShopDTO) {
		List<String> enrollees = new ArrayList<>();
		for (EnrolleeShopDTO enrolleeMembers : enrollmentShopDTO.getEnrolleeShopDTOList()) {
			for (SsapApplicant enrolledApplicant : enrolledApplication.getSsapApplicants()) {
				if (enrolleeMembers.getExchgIndivIdentifier().equals(enrolledApplicant.getApplicantGuid())) {
					enrollees.add(enrolledApplicant.getApplicantGuid());
					break;
				}
			}
		}
		return enrollees;
	}

	
	
	PlanAvailabilityAdapterResponse executePlanAvailability(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {
		PlanAvailabilityAdapterResponse response = null;
		try {
			response = batchPlanAvailabilityAdapter.execute(planAvailabilityAdapterRequest);
			if (response == null) {
				throw new GIRuntimeException("No Response returned from batchPlanAvailabilityAdapter for application - " + planAvailabilityAdapterRequest.getEnrolledSsapApplicationId());
			}
			return response;
		} catch (Exception e) {
			throw e;
		}

	}

	boolean executeEnrollment(long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees, PlanAvailabilityAdapterResponse planResponse) {
		String enrollmentResponse = GhixConstants.RESPONSE_FAILURE;
		try {
			enrollmentResponse = enrollmentUpdateAdapterBatch.execute(ssapApplicationId, healthEnrollees, dentalEnrollees, planResponse);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		boolean status = false;
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse)) {
			status = true;
		}
		return status;
	}
}
