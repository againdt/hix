package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.InformationExchangeSystemType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.InformationExchangeSystemCategoryCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.DateTime;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType;


@Component(value="verificationMetadataWrapperBatch")
public class VerificationMetadataWrapperBatch {
    
    private static final String YES = "Y";
    
    private final String[] verificationCategoryCodes = {
            "SSN", 
            "Citizenship", 
            "EligibleImmigrationStatus", 
            "CurrentIncome", 
            "AnnualIncome", 
            "IncarcerationStatus",
            "ESI-MEC",
            "NonESI-MEC"}; 
    
    /**
     * returns Map<Integer, Map<String, VerificationMetaData>>
     * @param ssapId
     * @return
     */
    public Map<Long, Map<String, VerificationMetadataType>> createVerificationMetadata(Long ssapId) {
        Map<Long, Map<String, VerificationMetadataType>> personToVerificationMetaDataMapping = new HashMap<Long, Map<String, VerificationMetadataType>>();
        
        // Loop through all the applicants for the household and create verification metadata 
        // if applicant is applying for coverage 
        /*for(SsapApplicant applicantn : applicants) {
            
            Map<String, VerificationMetadataType> verificationMetadatas = new HashMap<String, VerificationMetadataType>();
            
            // If it is the primary applicant, then create the annual income verification metadata.
            if(applicant.getPersonId() == 1) {
                verificationMetadatas.put(VerificationCategoryCodeSimpleType.ANNUAL_INCOME.value(), 
                        createVerificationMetadataFor(VerificationCategoryCodeSimpleType.ANNUAL_INCOME, applicant.getIncomeVerificationStatus(), applicant.getPersonId()));
            }
            
            String applyingForCoverage = applicant.getApplyingForCoverage();
            if (null != applyingForCoverage && applyingForCoverage.equals(YES)) {
                
                // Loop through all the different verification category codes and create
                // verification metadata
                for (String verificationCode : verificationCategoryCodes) {
                    switch (verificationCode) {
                        case "SSN":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.SSN, applicant.getSsnVerificationStatus(), applicant.getPersonId()));
                            break;
                        case "CurrentIncome":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.CURRENT_INCOME, SSAPIntegrationConstants.NOT_VERIFIED, applicant.getPersonId()));
                            break;
                        case "EligibleImmigrationStatus":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.ELIGIBLE_IMMIGRATION_STATUS, applicant.getVlpVerificationStatus(), applicant.getPersonId()));
                            break;
                        case "Citizenship":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.CITIZENSHIP, applicant.getCitizenshipImmigrationStatus(), applicant.getPersonId()));
                            break;
                        case "IncarcerationStatus":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.INCARCERATION_STATUS, applicant.getIncarcerationStatus(), applicant.getPersonId()));
                            break;
                        case "NonESI-MEC":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.NON_ESI_MEC, applicant.getMecVerificationStatus(), applicant.getPersonId()));
                            break;
                        case "ESI-MEC":
                            verificationMetadatas.put(verificationCode, 
                                    createVerificationMetadataFor(VerificationCategoryCodeSimpleType.ESI_MEC, applicant.getMecVerificationStatus(), applicant.getPersonId()));
                            break;
                    }
                }
            }

            if(!verificationMetadatas.isEmpty()) {
            	personToVerificationMetaDataMapping.put(applicant.getPersonId(), verificationMetadatas);
            }
        }*/
        return personToVerificationMetaDataMapping;
    }
    
    private VerificationMetadataType createVerificationMetadataFor(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType verificationCategoryCodeTypeValue, String verificationStatus, Long personId) {
        VerificationMetadataType verificationMetadataType = AccountTransferUtil.hixCoreFactory.createVerificationMetadataType();
        
        // Set the verification indicator (true/false)
        com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean verificationStatusValue = AccountTransferUtil.basicFactory.createBoolean();
        if(null != verificationStatus && verificationStatus.equals("VERIFIED")) {
            verificationStatusValue.setValue(true);
        } else {
            verificationStatusValue.setValue(false);
        }
        verificationMetadataType.setVerificationIndicator(verificationStatusValue);
        
        // Set the verification category code (SSN/Incarceration/Citizenship/CurrentIncome/Non-ESI-MEC etc)
        VerificationCategoryCodeType verificationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createVerificationCategoryCodeType();
        verificationCategoryCodeType.setValue(verificationCategoryCodeTypeValue);
        verificationMetadataType.getVerificationCategoryCode().add(verificationCategoryCodeType);
        
        // Set the verification date time 
        // TODO: Set verification date time from SSAP_VERIFICATIONS when rules start populating it.
        DateType verificationDateType = AccountTransferUtil.niemCoreFactory.createDateType();
        DateTime verificationDoneDateTime = AccountTransferUtil.basicFactory.createDateTime();
        //verificationDoneDateTime.setValue(AccountTransferMapper.getSystemDateTime());
        verificationDateType.setDateTime(verificationDoneDateTime);
        verificationMetadataType.setVerificationDate(verificationDateType);
        
        // Set the verification requesting system i.e Exchange and state code.
        InformationExchangeSystemType verificationRequestingSystemType = AccountTransferUtil.hixCoreFactory.createInformationExchangeSystemType();
        InformationExchangeSystemCategoryCodeType informationExchangeSystemCategoryCodeType = AccountTransferUtil.hixTypeFactory.createInformationExchangeSystemCategoryCodeType();
        informationExchangeSystemCategoryCodeType.setValue(InformationExchangeSystemCategoryCodeSimpleType.EXCHANGE);
        verificationRequestingSystemType.setInformationExchangeSystemCategoryCode(informationExchangeSystemCategoryCodeType);
        USStateCodeType usStateCodeType = AccountTransferUtil.statesObjFactory.createUSStateCodeType();
        usStateCodeType.setValue(USStateCodeSimpleType.valueOf("NV"));
        verificationRequestingSystemType.setInformationExchangeSystemStateCode(usStateCodeType);
        verificationMetadataType.setVerificationRequestingSystem(verificationRequestingSystemType);
        
        // Set the Id which will be linked with the relevant nodes later on
        verificationMetadataType.setId(verificationCategoryCodeTypeValue.toString() + "P" + personId);
        TextType verificationAuthorityName = AccountTransferUtil.niemCoreFactory.createTextType();
        verificationAuthorityName.setValue(InformationExchangeSystemCategoryCodeSimpleType.EXCHANGE.toString());
        verificationMetadataType.setVerificationAuthorityName(verificationAuthorityName);
        return verificationMetadataType;
    }
    
    public static void setMetadata(Map<String, VerificationMetadataType> personVerificationMetadata, ComplexObjectType targetObject, VerificationCategoryCodeSimpleType metadataCategory) {
        if(personVerificationMetadata != null) {
            VerificationMetadataType metadataToAdd = personVerificationMetadata.get(metadataCategory.value());
            if(metadataToAdd != null) {
                targetObject.getMetadata().add(metadataToAdd);
            }
        }
    }
}
