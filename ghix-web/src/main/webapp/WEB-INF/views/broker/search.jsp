<%@ page import="com.getinsured.hix.model.AccountUser"%>
<%@ page import="com.getinsured.hix.model.Location"%>
<%@ page import="com.getinsured.hix.model.Broker"%>
<%@ page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@ page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%
       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
       request.setAttribute("trackingCode",trackingCode);
%>
<link href="/hix/resources/css/assister.css" media="screen" rel="stylesheet" type="text/css" />

<link href="/hix/resources/css/chosen.css" rel="stylesheet" type="text/css" media="screen,print">
<script src="/hix/resources/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="/hix/resources/js/bootstrap-typeahead.js" type="text/javascript"></script>
<link href="/hix/resources/css/assister.css" media="screen" rel="stylesheet" type="text/css" />
<link href="/hix/resources/css/accessibility.css" media="screen" rel="stylesheet" type="text/css" />
<c:if test="${CA_STATE_CODE}">
	<link href="<c:url value="/resources/css/broker-search.css" />" media="screen" rel="stylesheet" type="text/css" />
</c:if>
<script type="text/javascript"	src='<c:url value="/resources/js/jquery.validate.min.js" />'></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>

<!-- Google Analytics -->
<c:if test="${not empty fn:trim(trackingCode)}">
<script>
var googleAnalyticsTrackingCodes = '${trackingCode}';
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', googleAnalyticsTrackingCodes, 'auto');
ga('send', 'pageview');
</script>
</c:if>
<!-- End Google Analytics -->
</script>

<script type="text/javascript">

$(document).ready(function(){
         var config = {
                     '.chosen-select'           : {},
                     '.chosen-select-deselect'  : {allow_single_deselect:true},
                     '.chosen-select-no-single' : {disable_search_threshold:10},
                     '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
                     '.chosen-select-width'     : {width:"218px"}
                   }
                   for (var selector in config) {
                     $(selector).chosen(config[selector]);
                   }
 });

</script>
<style>
	.bottomBorder {
		border-bottom: 1px solid #999;
	}
</style>

<c:choose>
	<c:when test="${CA_STATE_CODE}">
		<c:set value="col-xs-hidden col-sm-5 col-md-5 col-lg-5" var="cssClass"></c:set>
	</c:when>
	<c:otherwise>
		<c:set value="span5" var="cssClass"></c:set>
	</c:otherwise>
</c:choose>
<div class="container-fluid" id="search_page">
	<div class="row">
		<div class="titlebar">
			<c:if test="${CA_STATE_CODE}">
				<h1><spring:message code="label.locate.assistance.modal.1"/></h1>
				<!-- <div class="bottomBorder"></div> -->
				<h3><spring:message code="label.brksearchinfo"/></h3>
			</c:if>
			<c:if test="${!CA_STATE_CODE}">
				<h1><spring:message code="label.brksearchinfo"/></h1>
			</c:if>
		</div>
	</div>

	<div class="row-fluid" id="search_container">
		<div class="${cssClass} well" id="location_search">
			<form class="form-vertical" id="frmbrokersearch" name="frmbrokersearch" action="<c:url value="/broker/searchlist"/>">
				<h4 class="search_title_small"><spring:message code="label.searchbylocation"/></h4>
				<div class="gutter10">
					<div class="control-group">
						<label for="zipcode" class="control-label"><spring:message code="label.brkZipCode"/>
							<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
						</label>
						<div class="controls">
							<input type="text" id="zipcode"
								value="${sessionScope.brokersearchparam.location.zip!=0?sessionScope.brokersearchparam.location.zip:0}"
								name="location.zip" maxlength="5" />
						</div>
						<div id="zipcode_error"></div>
						<!-- end of controls -->
					</div><!-- end of control-group -->

					<div class="control-group">
						<c:if test="${CA_STATE_CODE}">
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistanceinmiles"/></label>
						</c:if>
						<c:if test="${!CA_STATE_CODE}">
							<label for="Distancer" class="control-label"><spring:message code="label.brkdistance"/> <span class="aria-hidden">miles</span></label>
						</c:if>
						<div class="controls">
							<select id="Distancer" name="distance" value="${sessionScope.brokermaxdis}">
								<option value="5"
									<c:if test="${sessionScope.brokermaxdis == '5'}"> selected="selected"</c:if>>5</option>
								<option value="10"
									<c:if test="${sessionScope.brokermaxdis == '10'}"> selected="selected"</c:if>>10</option>
								<option value="15"
									<c:if test="${sessionScope.brokermaxdis == '15'}"> selected="selected"</c:if>>15</option>
								<option value="20"
									<c:if test="${sessionScope.brokermaxdis == '20'}"> selected="selected"</c:if>>20</option>
								<option value="30"
									<c:if test="${sessionScope.brokermaxdis == '30'}"> selected="selected"</c:if>>30</option>
								<option value="50"
									<c:if test="${sessionScope.brokermaxdis == '50'}"> selected="selected"</c:if>>50</option>
								<option value="4000"
									<c:if test="${sessionScope.brokermaxdis == '4000'}"> selected="selected"</c:if>>Any</option>
							</select>
							<c:if test="${!CA_STATE_CODE}">
								<span class="distance-unit"><spring:message code="label.miles"/></span>
							</c:if>
						</div><!-- end of controls -->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="languagesSpoken" class="control-label"><spring:message code="label.brkLanguagesSpoken"/></label>
						<div class="controls">
							<select id="languagesSpoken" name="languagesSpoken"  data-placeholder="<spring:message code='label.SelectAnOption'/>" class="chosen-select" multiple style=""></select>
						 <div id="languagesSpoken_error"></div>
						</div>	<!-- end of controls-->
					</div><!-- end of control-group -->
					<div class="control-group">
						<div class="controls">
							<input type="submit" name="submit" id="submit1"
								value="<spring:message  code='label.search'/>"
								class="btn btn-primary"
								title="<spring:message  code='label.search'/>"/>
						</div><!-- end of control-group -->
					</div><!-- end of control-group -->
				</div><!-- end of gutter10 -->
			</form>
		</div><!-- end of location -->
		<!-- OR button -->

		<c:choose>
			<c:when test="${CA_STATE_CODE}">
				<div class="hr-line"></div>
				<div class="col-xs-hidden col-sm-2 col-md-2 col-lg-2 center" id="search_center"><h6 class="col-xs-hidden"> <strong><spring:message code="label.assister.or"/></strong></h6> </div>
			</c:when>
			<c:otherwise>
				<div class="col-xs-hidden span1 center" id="search_center"><h6> <strong><spring:message code="label.assister.or"/></strong></h6> </div>
			</c:otherwise>
		</c:choose>

		<div class="${cssClass} well" id="name_search">
			<form class="form-vertical" id="frmbrokersearchvalues" name="frmbrokersearchvalues" action="<c:url value="/broker/searchlist"/>">
				<h4 class="search_title_small"><spring:message code="label.searchbyname"/></h4>
				<div class="gutter10">
					<div class="control-group">
						<label for="firstName" class=""><spring:message code="label.brkFirstName"/></label>
							<!-- <div class="controls"> -->
							<input type="text" name="user.firstName" id="firstName" value="${sessionScope.brokersearchparam.user.firstName}" size="30"/>
							<!-- </div> -->
					</div> <!-- end of control-group -->

					<div class="control-group cntrolgrp-lastname">
						<label for="lastName" class=""><spring:message code="label.brkLastName"/></label>
						<input type="text" name="user.lastName" id="lastName" value="${sessionScope.brokersearchparam.user.lastName}" size="30"/> <!-- Removed style="cursor:pointer" for HIX=102793-->
							<!-- </div> -->
					</div><!-- end of control-group -->

					<div class="control-group">
						<label for="companyName" class=""><spring:message code="label.searchCompanyName"/></label>
						<input type="text" name="companyName" id="companyName" value="${sessionScope.brokersearchparam.companyName}" size="30"/>
					</div><!-- end of control-group -->
					<div class="control-group">
						<div class="controls">
							<input type="submit" name="submit" id="submit"
								value="<spring:message  code='label.search'/>"
								class="btn btn-primary"
								title="<spring:message  code='label.search'/>"/>
						</div>
					</div>
				</div><!-- end of control-group -->
			</form>
		</div>
		<input type="hidden" name="languagesSpokenCheck" id="languagesSpokenCheck" value="1" />
	</div>
<c:if test="${CA_STATE_CODE}">
	<div class="accordion" id="search_agent">
		<div class="accordion-group">
		    <div class="accordion-heading">
		      <a class="accordion-toggle" data-toggle="collapse" data-parent="#search_agent" href="#collapseOne">
		        <h4 class="search_title_small col-xs-10"><spring:message code="label.searchbylocation"/></h4>
		        <i class="icon-chevron-right col-xs-2 pull-right"></i>
		      </a>
		    </div>
		    <div id="collapseOne" class="accordion-body collapse">
		      	<div class="accordion-inner">
		        	<form class="form-vertical" id="frmBrokerSearchAccordion" name="frmBrokerSearchAccordion" action="<c:url value="/broker/searchlist"/>">
						<div class="gutter10">
							<div class="control-group">
								<label for="zipcodeAccordion" class="control-label"><spring:message code="label.brkZipCode"/>
									<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png">
								</label>
								<div class="controls">
									<input type="text" id="zipcodeAccordion"
										value="${sessionScope.brokersearchparam.location.zip!=0?sessionScope.brokersearchparam.location.zip:0}"
										name="location.zip" maxlength="5" />
								</div>
								<div id="zipcodeAccordion_error"></div>
								<!-- end of controls -->
							</div><!-- end of control-group -->

							<div class="control-group">
								<label for="Distancer" class="control-label"><spring:message code="label.brkdistanceinmiles"/></label>
								<div class="controls">
									<select id="Distancer" name="distance" value="${sessionScope.brokermaxdis}">
										<option value="5"
											<c:if test="${sessionScope.brokermaxdis == '5'}"> selected="selected"</c:if>>5</option>
										<option value="10"
											<c:if test="${sessionScope.brokermaxdis == '10'}"> selected="selected"</c:if>>10</option>
										<option value="15"
											<c:if test="${sessionScope.brokermaxdis == '15'}"> selected="selected"</c:if>>15</option>
										<option value="20"
											<c:if test="${sessionScope.brokermaxdis == '20'}"> selected="selected"</c:if>>20</option>
										<option value="30"
											<c:if test="${sessionScope.brokermaxdis == '30'}"> selected="selected"</c:if>>30</option>
										<option value="50"
											<c:if test="${sessionScope.brokermaxdis == '50'}"> selected="selected"</c:if>>50</option>
										<option value="4000"
											<c:if test="${sessionScope.brokermaxdis == '4000'}"> selected="selected"</c:if>>Any</option>
									</select>
									<c:if test="${!CA_STATE_CODE}">
										<span class="distance-unit"><spring:message code="label.miles"/></span>
									</c:if>
								</div><!-- end of controls -->
							</div><!-- end of control-group -->

							<div class="control-group">
								<label for="languagesSpokenAccordion" class="control-label"><spring:message code="label.brkLanguagesSpoken"/></label>
								<div class="controls">
									<select id="languagesSpokenAccordion" name="languagesSpokenAccordion"  data-placeholder="<spring:message code='label.SelectAnOption'/>" class="chosen-select" multiple style=""></select>
								 <div id="languagesSpokenAccordion_error"></div>
								</div>	<!-- end of controls-->
							</div><!-- end of control-group -->
							<div class="control-group">
								<div class="controls">
									<input type="submit" name="submit" id="submit1"
										value="<spring:message  code='label.search'/>"
										class="btn btn-primary"
										title="<spring:message  code='label.search'/>"/>
								</div><!-- end of control-group -->
							</div><!-- end of control-group -->
						</div><!-- end of gutter10 -->
					</form>
					<input type="hidden" name="languagesSpokenAccordionCheck" id="languagesSpokenCheck" value="1" />
		      	</div>
		    </div>
		</div><!-- end of accordion-group -->
	    <div class="accordion-group">
	    	<div class="accordion-heading">
	      		<a class="accordion-toggle" data-toggle="collapse" data-parent="#search_agent" href="#collapseTwo">
	        		<h4 class="search_title_small col-xs-10"><spring:message code="label.searchbyname"/></h4>
	        		<i class="icon-chevron-right col-xs-2 pull-right"></i>
	      		</a>
    		</div>
	    	<div id="collapseTwo" class="accordion-body collapse">
	      		<div class="accordion-inner">
	        		<form class="form-vertical" id="frmbrokersearchvalues" name="frmbrokersearchvalues" action="<c:url value="/broker/searchlist"/>">
						<div class="gutter10">
							<div class="control-group">
								<label for="firstName" class=""><spring:message code="label.brkFirstName"/></label>
									<!-- <div class="controls"> -->
									<input type="text" name="user.firstName" id="firstName" value="${sessionScope.brokersearchparam.user.firstName}" size="30"/>
									<!-- </div> -->
							</div> <!-- end of control-group -->

							<div class="control-group cntrolgrp-lastname">
								<label for="lastName" class=""><spring:message code="label.brkLastName"/></label>
								<input type="text" name="user.lastName" id="lastName" value="${sessionScope.brokersearchparam.user.lastName}" size="30"/> <!-- Removed style="cursor:pointer" for HIX=102793-->
									<!-- </div> -->
							</div><!-- end of control-group -->

							<div class="control-group">
								<label for="companyName" class=""><spring:message code="label.searchCompanyName"/></label>
								<input type="text" name="companyName" id="companyName" value="${sessionScope.brokersearchparam.companyName}" size="30"/>
							</div><!-- end of control-group -->
							<div class="control-group">
								<div class="controls">
									<input type="submit" name="submit" id="submit"
										value="<spring:message  code='label.search'/>"
										class="btn btn-primary"
										title="<spring:message  code='label.search'/>"/>
								</div>
							</div>
						</div><!-- end of control-group -->
					</form>
	      		</div>
	    	</div>
	 	</div>
	</div>
</c:if>
</div>

<script type="text/javascript">

function validateForm() {
	return {
		rules : {
		"location.zip" : { required : true,
	    	    number : true,
	        	minlength : 5,
	        	maxlength : 5,
	        	ZipCodeCheck : true

      		},
	      	languagesSpoken : {required : false, number:false, languagesSpokenCheck:true},
	      	languagesSpokenAccordion : {required : false, number:false, languagesSpokenCheck:true}
		},
		messages : {
			"location.zip" : { required : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcode.aee' javaScriptEscape='true'/></span>",
					number : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber.aee' javaScriptEscape='true'/></span>",
		  			minlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>",
		  			maxlength : "<span> <em class='excl'>!</em><spring:message  code='label.validateZipcodeMaxlength.aee' javaScriptEscape='true'/></span>",
		  			ZipCodeCheck : "<span> <em class='excl'>!</em><spring:message  code='label.validateIsZipcodeNumber.aee' javaScriptEscape='true'/></span>",
		  			/* languagesSpoken : { languagesSpokenCheck: "<span> <em class='excl'>!</em>Please enter language spoken from drop down.</span>"} */
		  		}
		},
		errorClass: "error",
		onkeyup: false,
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-inline');
		}
	}
}

var frmbrokersearchValidator = $("#frmbrokersearch").validate(validateForm());
var frmBrokerSearchAccordionValidator = $("#frmBrokerSearchAccordion").validate(validateForm());

function zipcodeCheckMtd(value, element, param) {
	 var test=/^[0-9]*$/.test(value);
	 if((value == '00000')||(!test)){
	  return false;
	 }
	return true;
}

jQuery.validator.addMethod("ZipCodeCheck", zipcodeCheckMtd);

function languagesSpokenCheckMtd(value, element, param) {
	if(value == '' || value == 'undefined') {
		return true;
	}
	var ID = element;
	//checkLanguageSpoken(ID);
	return $("#"+ID+"Check").val() == 1 ? false : true;
}


jQuery.validator.addMethod("languagesSpokenCheck", languagesSpokenCheckMtd);
jQuery.validator.addMethod("languagesSpokenCheckAccordion", languagesSpokenCheckMtd);

/*-- Commented checkLanguageSpoken function for HIX-106506 --*/
 // function checkLanguageSpoken(ID){
	// $("#"+ ID + "_error").hide();
	// $.get('<c:url value="/broker/buildProfile/checkLanguagesSpoken" />',
	// {languagesSpoken: $("#"+ ID).val()},
	//             function(response){
	//                     if(response == true){
	//                             $("#"+ ID+"Check").val(0);
	// 							$("#"+ ID+"_error").hide();
	//                     }else{
	//                             $("#"+ ID+"Check").val(1);
	// 							$("#"+ ID+"_error").show();
	//                     }
	//      	    }
	//         );
	// }

 //load the jquery chosen plugin
	function loadLanguages() {
		$("#languagesSpoken").html('');
		$("#languagesSpokenAccordion").html('');
		var respData = $.parseJSON('${languagesList}');
		var counties='${languagesList}';

	    for ( var key in respData) {
	    	var isSelected = false;
		     if(counties!=null){
			      isSelected = checkLanguage(respData[key]);
		      }
		      if(isSelected){
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		    	  $('#languagesSpokenAccordion').append("<option value='"+respData[key]+"' selected='selected'>"+ respData[key] + "</option>");
		      } else {
		    	  $('#languagesSpoken').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		    	  $('#languagesSpokenAccordion').append("<option value='"+respData[key]+"'>"+ respData[key] + "</option>");
		      }
		 }

	     $('#languagesSpoken').trigger("liszt:updated");
	     $('#languagesSpokenAccordion').trigger("liszt:updated");
	   }

	function checkLanguage(county){
	     var counties='${languagesList}';
	     var countiesArray=counties.split(',');
	     var found = false;
		     for (var i = 0; i < countiesArray.length && !found; i++) {
			     var countyTocompare=countiesArray[i];
			     if (countyTocompare.toLowerCase() == county.toLowerCase()) {
			    	 found = true;

			     }
			 }
		   return found;
    }

	$(document).ready(function() {
		loadLanguages();
		$('input[type="text"]:first').focus();
		$('#submit1').click(function() {
		 	$("#firstName").val("");
		 	$("#companyName").val("");
			 $("#lastName").val("");
			 triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Search by name', 1]);
		});

		$('#submit').click(function() {
		 	$("#zipcode").val("");
		 	$("#languagesSpoken").val("");
		 	$(".chosen-select").val("").trigger('liszt:updated');
			$("#zipcode").rules("remove");
		 	/* $("languagesSpoken").rules("remove"); */
			$("#frmbrokersearch").valid();
			triggerGoogleTrackingEvent(['_trackEvent', 'Get Help Events', 'Click', 'Search by name', 1]);
		});
	/*$("#frmbrokersearch").on("keypress",function(event){
		$("input[type='text']").each(function (){
		if ((event.which == 13 || event.keyCode == 13) && $(this).is(":focus") && $(this).val() != "" ) {
			parent = $(this).parents(".span6").attr("id");
			if(parent =="name_search") $("#submit").trigger("click");
		}
		});
	});*/
	});

</script>
