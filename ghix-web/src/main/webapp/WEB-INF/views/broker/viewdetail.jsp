<%@ taglib prefix="audit" uri="/WEB-INF/tld/audit-view.tld"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 

<%@ page isELIgnored="false"%>


<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/assister.css" />" media="screen"/>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/accessibility.css" />" media="screen"/>
<!-- <body id="broker-detail"> -->
	<form class="form-horizontal" id="frmviewdetail" name="frmviewdetail">
	<c:set var="encBrokerId" ><encryptor:enc value="${broker.id}" isurl="true"/> </c:set>
		<div class="gutter10">
			<c:choose>
				<c:when test="${fn:contains(ShowDesignate, 'Y')}">
					<a class="btn btn-info pull-right" href="../broker/designate/${encBrokerId}">Designate</a> 
					<div class="alert alert-info pull-left">
				<strong>Agent Designation</strong><br />
				Designating a Agent as your representative allows them to&#58; Access your account, see your information, and make changes on your behalf.
			</div>
				</c:when>
			</c:choose>
			
		</div> 
		
		<div class="row-fluid">
			<div class="span12">
				<div class="gutter10">
					<div class="row-fluid">
					
						<c:choose>
							<c:when test="${!empty broker}">
								<spring:url value="/broker/photo/${encBrokerId}" var="photoUrl"/>
								<div class="span2">
									<img src="${photoUrl}" alt="Agent thumbnail photo" class="profilephoto thumbnail" />
								</div>

								<div class="page-header span10">
									<h1 class="span7">
										<a name="skip" class="skip">${broker.user.firstName}&nbsp;${broker.user.lastName}</a>
									</h1>
									<div class="span5">
										<div class="pull-right">
											<a class="btn" href="<c:url value="/admin/broker/manage/list?usesession=yes&pageNumber=${currentPage}"/>" href="<c:url value="/admin/broker/manage/list?usesession=yes&pageNumber=${currentPage}"/>">Back to Results</a>
											<a class="btn btn-primary" href="<c:url value="/admin/broker/manage"/>">Search Again</a>
										</div>
									</div>
								</div>
								<div class="span10">
									<p>${broker.location.address1}, 
									<c:if test="${broker.location.address2!=null && broker.location.address2!=\"\"}">
										${broker.location.address2}, 
									</c:if>
									${broker.location.city}, ${broker.location.state}, ${broker.location.zip}<br/>
										${broker.contactNumber}<br/>
										${broker.user.email}
									</p>
								</div>
								<div class="row-fluid">
									<div class="span"></div>
								</div>
								<div class="row-fluid">

									<table class="table table-border-none span7">
										<tbody>
											<tr>
												<td class="txt-right"><spring:message  code="label.brkRegionServiced"/></td>
												<td><strong>${broker.areaServed}</strong></td>
											</tr>
											<tr>
												<td class="txt-right"><spring:message  code="label.brkProductExpertises"/></td>
												<td><strong>${broker.productExpertise}</strong></td>
											</tr>
											<tr>
												<td class="txt-right"><spring:message  code="label.brkLanguageSpoken"/></td>
												<td><strong>${broker.languagesSpoken}</strong></td>
											</tr>
											<tr>
												<td class="txt-right"><spring:message  code="label.brkStateLicenseNumber"/></td>
												<td><strong>${broker.licenseNumber}</strong></td>
											</tr>
										</tbody>
									</table>

									<div class="span5">
										<iframe width="250" height="250" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail pull-right" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${broker.location.address1}+${broker.location.city},+${broker.location.state}+${broker.location.zip}&amp;gl=us&amp;t=m&amp;ll=${broker.location.lat},${broker.location.lon}&amp;z=13&amp;iwloc=near&amp;output=embed"></iframe>
									</div>
								</div>

							</c:when>
							<c:otherwise>
								<b>No matching record found.</b>
							</c:otherwise>
						</c:choose>
						
						<h2><a>Agent Audit Information</a></h2>
						<div class="audit_info">
							<!-- Display Audit information - Starts -->
							<audit:view 
								entityId="${broker.id}" 
								className="com.getinsured.hix.model.Agent"
								repoBeanName="iBrokerRepository">
							</audit:view>
							<!-- Display Audit information - Ends -->
						</div>
					</div>
				</div><!-- .gutter10 -->
			</div>
		</div><!-- end of .row-fluid -->
	</form>
	<script type="text/javascript">
		$(document).ready(function(){
			var laClasses = new Array();
			$('.audit_info th').each(function(){
				if ($(this).attr('class') == "LicenseNumber" || $(this).attr('class') == "CertificationStatus" || $(this).attr('class') == "LanguagesSpoken" || $(this).attr('class') == "ContactNumber") {
					$(this).addClass('txt-center')
				}
				laClasses[$(this).index()] = $(this).attr('class');
			});
			$('.audit_info td').each(function(){
				$(this).addClass(laClasses[$(this).index()]);
			});
		})
	</script>

