package com.getinsured.hix.batch.ssap.integration.task;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.regex.Pattern;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.context.annotation.DependsOn;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.getinsured.hix.batch.ssap.service.SsapAgeOutDisnenrollService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * Spring batch Tasklet to Disenroll Age out batch job 
 * 
 * @author Vijay Patel
 * 
 *  
 */
@DependsOn("dynamicPropertiesUtil")
public class DisenrollAgeOutDependentsNonFinancialTask extends StepExecutionListenerSupport implements Tasklet {
	

	private static final String APPLICATION_ID = "APPLICATION_ID";
	private static final Logger LOGGER = Logger.getLogger(DisenrollAgeOutDependentsNonFinancialTask.class);
	private SsapApplicationRepository ssapApplicationRepository;
	private ThreadPoolTaskExecutor taskExecutor;
	private GIMonitorService giMonitorService;
	private SsapAgeOutDisnenrollService ssapAgeOutDisnenrollService;
	 
	
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		
		//get current coverage year 
		Calendar calendar = Calendar.getInstance();
		Long currentCoverageYear = (long)calendar.get(Calendar.YEAR); 
		//get current month
		long month = calendar.get(Calendar.MONTH);
		if(month == 11){
			return RepeatStatus.FINISHED;
		}
		//Long currentCoverageYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		//Long batchSize = new Long(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE));
		
		String applicationIds =  chunkContext.getStepContext().getStepExecution().getJobParameters().getString(APPLICATION_ID);
		List<String> applicationList = null;
		List<Long> ageOutApplications = null;
		try {
			if(applicationIds !=null && !applicationIds.isEmpty()){
				applicationList = Arrays.asList(applicationIds.split(Pattern.quote("|")));
			}
			if(applicationList!=null && !applicationList.isEmpty()){
				ageOutApplications = new ArrayList<Long>();
				for(String applicationId : applicationList){
						ageOutApplications.add(Long.parseLong(applicationId));
				}
				
			}else{
				ageOutApplications =   ssapAgeOutDisnenrollService.getSsapAppForAgeOut(currentCoverageYear,"N","EN");
			}
		} catch (Exception giExceotion) {
			giMonitorService.saveOrUpdateErrorLog(null, new Date(), this.getClass().getName(), giExceotion.getLocalizedMessage(), null, giExceotion.getStackTrace().toString(), GIRuntimeException.Component.BATCH.getComponent(), null);
		}
		
		if(ageOutApplications == null || ageOutApplications.size() == 0) {
			giMonitorService.saveOrUpdateErrorLog(null, new Date(), this.getClass().getName(), "No  application found to AgeOut.", null, "No cloned application found to renew.", GIRuntimeException.Component.BATCH.getComponent(), null);
			return RepeatStatus.FINISHED;
		}
		LOGGER.info("Total applications to be processed:"+ageOutApplications.size());
		Set<Future<Map<String,String>>> tasks = new HashSet<Future<Map<String,String>>>(ageOutApplications.size());
		for (Long applicationId : ageOutApplications) {
			LOGGER.info("Processing Application:" + applicationId);
			try {
				tasks.add(taskExecutor.submit(new DisnenrollAgeOutProcessor(applicationId, currentCoverageYear)));
			} catch (Exception giException) {
				giMonitorService.saveOrUpdateErrorLog(null, new Date(), this.getClass().getName(), ExceptionUtils.getStackTrace(giException), null,null, GIRuntimeException.Component.BATCH.getComponent(), null);
			}
		}
		
		waitForAllTaskToComplete(tasks);
		
		return RepeatStatus.FINISHED;

	}


	private void waitForAllTaskToComplete(Set<Future<Map<String,String>>> tasks) {
		boolean batchIsNotCompleted = true; 
		long currentNanoTime = System.nanoTime();
		long elapsedNanoTime = System.nanoTime();
		
		while(batchIsNotCompleted || (elapsedNanoTime/1000000000 >  60 * 15)) {
			boolean isAllTaskCompleted = true;
			for (Future<Map<String,String>> future : tasks) {
				if(!future.isDone()) {
					isAllTaskCompleted = false;
					break;
				}
			}
			batchIsNotCompleted = !isAllTaskCompleted;
			elapsedNanoTime = System.nanoTime() - currentNanoTime;
			if(isAllTaskCompleted){
				ssapAgeOutDisnenrollService.logData(tasks);
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				LOGGER.error("Thread interrupted from sleep.");
			}
		}
	}
	
	private class DisnenrollAgeOutProcessor implements Callable<Map<String,String>> {
		
		private Long applicationId= null;
		Long renewalYear = null;

		
		public DisnenrollAgeOutProcessor(Long applicationId, Long renewalYear) {
			this.applicationId = applicationId;
			this.renewalYear = renewalYear;
		}


		@Override
		public Map<String,String> call() throws Exception {
			return ssapAgeOutDisnenrollService.disenrollQHP(applicationId);
		}
		
		
	}

	public SsapApplicationRepository getSsapApplicationRepository() {
		return ssapApplicationRepository;
	}

	public void setSsapApplicationRepository(
			SsapApplicationRepository ssapApplicationRepository) {
		this.ssapApplicationRepository = ssapApplicationRepository;
	}

	
	public ThreadPoolTaskExecutor getTaskExecutor() {
		return taskExecutor;
	}


	public void setTaskExecutor(ThreadPoolTaskExecutor taskExecutor) {
		this.taskExecutor = taskExecutor;
	}


	public GIMonitorService getGiMonitorService() {
		return giMonitorService;
	}

	public void setGiMonitorService(GIMonitorService giMonitorService) {
		this.giMonitorService = giMonitorService;
	}

	public SsapAgeOutDisnenrollService getSsapAgeOutDisnenrollService() {
		return ssapAgeOutDisnenrollService;
	}

	public void setSsapAgeOutDisnenrollService(
			SsapAgeOutDisnenrollService ssapAgeOutDisnenrollService) {
		this.ssapAgeOutDisnenrollService = ssapAgeOutDisnenrollService;
	}
	
	


	
}
