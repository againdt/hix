package com.getinsured.iex.hub.qac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.ObjectFactory;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.VerifyRequestPayloadType;

public class VerifyRequestPayloadWrapper implements JSONAware {
	private static ObjectFactory extnFactory = new ObjectFactory();
	private VerifyRequestPayloadType requestType = null;
	private InsuranceApplicantRequestWrapper applicant;

	public VerifyRequestPayloadWrapper(){
		this.requestType = extnFactory.createVerifyRequestPayloadType(); 
	}
	
	public void setApplicant(InsuranceApplicantRequestWrapper applicant){
		this.applicant = applicant;
		this.requestType.setRequestApplicant(applicant.getSystemRepresentation());
	}
	
	public VerifyRequestPayloadType getSystemRepresentation(){
		return requestType;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("RequestApplicant", this.applicant);
		return obj.toJSONString();
	}

}
