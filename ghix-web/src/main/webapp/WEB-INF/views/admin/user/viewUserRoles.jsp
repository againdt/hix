<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>

<style>
	#roleChanges table{
		width:70%;
		margin-left: auto;
    margin-right: auto;
	}
	
	#roleChanges table th{
		padding-left:29px;
	}

</style>
<div class="row-fluid">
	<div class="gutter10">
			<h1 id="skip">Manage User Account</h1>
	</div>
</div>
<c:set var="userId" ><encryptor:enc value="${user.id}" isurl="true"/> </c:set>
<div class="row-fluid">
	<div class="span3" id="sidebar">
              <div class="gray">
                  <h4 class="margin0">Accounts</h4>
              </div>
              
              <ul class="nav nav-list">
                      <li><a href="<c:url value="/admin/manage/user/details?userId=${userId}"/>">User Details</a></li>
                      <li class="active"><a href="<c:url value="/admin/manage/user/roles?userId=${userId}"/>">User Roles</a></li>
              </ul>
              
              <div class="margin30-t alert alert-info">
              	<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="information"> The default role of a user cannot be removed. <br>
              	<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="information"> Role management is only applicable to Active Users.  <br>   
              	<c:if test="${logedUser.id==user.id}">
              	<img src="<c:url value="/resources/img/requiredAsterisk.png"/>" alt="information"> A user can't manage his/her own user information or roles. 
              	</c:if>        
              </div>
              
	</div>
	<!-- end of span3 -->
	
	<div class="span9" id="rightpanel">
          	<div class="header">
                  <h4 class="span10">Manage User Roles</h4>
             </div>
             
             <div class="gutter10">
             	<div class="control-group">
             		<div class="controls">
             			User - ${user.email}
             		</div>
             	</div>
             	<form action="<c:url value="/admin/manage/user/roles/edit" />" method="POST" id="roleChanges">
	             	<df:csrfToken/>
	             	
	             	<!-- START :: Privileged user's role listing  -->
	             	<!-- HIX-48505 :: if the user's default role is privileged then allow to add / remove only privileged roles -->
	             	
	             		<table class="table">	
		             		<thead>  
		             			<tr><th>User Roles</th><th><input type="hidden" id="id" name="id" value="<encryptor:enc value="${user.id}"/>"></th></tr> 
		             		</thead>    
		             		<tbody>     	
			             		<tr><td><label class="checkbox" for="${defaultRole.label}">${defaultRole.label}(default)</label></td><td><input type="checkbox" id="${defaultRole.label}" name="${defaultRole.name}" checked disabled></td></tr> 
			             		<c:forEach var="available" items="${mangeUserRoles}">    
			             			<c:set var="encRoleName" ><encryptor:enc value="${available.role.name}"/> </c:set>
			             			
			             			<c:choose>
		             					<c:when test="${available.isActiveUserRole()==true}">
		             						<c:if test="${user.confirmed==1 && logedUser.id!=user.id}">
<%-- 			             						<tr><td><label class="checkbox" for="${available.role.name}">${available.role.label}</label></td><td><input type="checkbox" id="${available.role.name}" name="${available.role.name}" checked></td></tr> --%>
			             						<tr><td><label class="checkbox" for="${available.role.label}">${available.role.label}</label></td>
			             						<td><input type="checkbox" id="${available.role.label}" name="${encRoleName}" checked></td></tr>
			             					</c:if>
			             					<c:if test="${user.confirmed==0 || logedUser.id==user.id}">
			             						<tr><td><label class="checkbox" for="${available.role.label}">${available.role.label}</label></td>
			             						<td><input type="checkbox" id="${available.role.label}" name="${encRoleName}" checked disabled></td></tr>
			             					</c:if>
			             				</c:when>
		             					<c:otherwise>
		             						<c:if test="${user.confirmed==1 && logedUser.id!=user.id}">
		             							<tr><td><label class="checkbox" for="${available.role.label}">${available.role.label}</label></td>
		             							<td><input type="checkbox" id="${available.role.label}" name="${encRoleName}"></td></tr>
		             						</c:if>
		             						<c:if test="${user.confirmed==0 || logedUser.id==user.id}">
		             							<tr><td><label class="checkbox" for="${available.role.label}">${available.role.label}</label></td>
		             							<td><input type="checkbox" id="${available.role.label}" name="${encRoleName}" disabled></td></tr>
		             						</c:if>
		             					</c:otherwise>
				             		</c:choose>
			             		</c:forEach>
		             		</tbody>
						</table>
					<%-- 	<option value="<encryptor:enc value="${role.id}"/>"> ${role.label} </option> --%>
	
					<div class="form-actions center">
						<a href="<c:url value="/admin/manage/users"/>" id="cancelEditBtn" class="btn btn-medium"><spring:message  code="button.cancel"/></a>
						<input type="submit" name="submit" id="submit" value="Save" class="btn btn-primary" title="Save">
					</div>
	             				
             	</form>
             
             </div>
		<div class="gutter10">

	</div>
</div><!--  end of span9 -->
</div><!-- end row-fluid -->

<script>
$(document).ready(function(){
	$( "#roleChanges tr:even" ).css( "background-color", "#F9F9F9" );	
});
</script>