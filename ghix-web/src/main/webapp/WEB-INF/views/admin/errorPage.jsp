<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page isErrorPage="true" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" 
	  xmlns:th="http://www.thymeleaf.org">
<head>


<style type="text/css">
   .application-error-container h1{font-size: 48px !important;color: #333;}
   .application-error-container h2{font-size: 24px;font-weight: normal;}
   .application-error-container .btn{font-family: Arial;}
   .application-error-container p{font-size: 18px;}
   .gutter10 {padding-bottom: 0 !important;}
</style>


<script type="text/javascript">
$(document).ready(function () {
    $("#showStackTrace").click(
    function () {
      var tempdis = $("#stackTraceDiv").css( "display");
      if("none"==tempdis){
    	  $("#stackTraceDiv").css( "display","block" );
          $("#stackTraceDetailDiv").css( "display","block" );
      }else{
    	  $("#stackTraceDiv").css( "display","none" );
          $("#stackTraceDetailDiv").css( "display","none" );
      }
    
    });
  });
</script>




</head>
<body>

<div class="row-fluid txt-center margin20-t application-error-container">
	<div class="row">
		<c:choose>
			<c:when test="${GIMONITOR.exception == 'org.springframework.security.access.AccessDeniedException' || 
							GIMONITOR.exception == 'org.springframework.security.access.AccessDecisionManager'}">
					<h3>Application Security Error : No permission to access this page.	</h3>
			</c:when>
			<c:otherwise>
					<h1 class="margin20-t span12 gutter5-t headerText">
						<c:choose>
							<c:when test="${'404' == pageContext.errorData.statusCode}">
		  						${pageContext.errorData.statusCode}: Page Not Found
		  					</c:when>
							<c:when test="${'405' == pageContext.errorData.statusCode}">
		  						${pageContext.errorData.statusCode}: Method Not Allowed
		  					</c:when>
							<c:when test="${'500' == pageContext.errorData.statusCode}">
		  						${pageContext.errorData.statusCode}: Internal Server Error
		  					</c:when>
		  					<c:otherwise>
		  						500: Internal Server Error
		  					</c:otherwise>
						</c:choose>
					</h1>
			</c:otherwise>
		</c:choose>
	</div>
	<div class="row">
		  <h2 class="margin0 span12" style="">
		  <c:choose>
		  	<c:when test="${'404' == pageContext.errorData.statusCode}">
		  		Hmm... We can't find that page right now.
		  	</c:when>
		  	<c:when test="${'405' == pageContext.errorData.statusCode}">
		  		That method is not allowed.
		  	</c:when>
		  	<c:when test="${'500' == pageContext.errorData.statusCode}">
		  		The page cannot be displayed.
		  	</c:when>
		  	<c:otherwise>
		  		The page cannot be displayed.
		  	</c:otherwise>
		  </c:choose>
		  </h2>
	</div>
	<div class="clearfix margin20-t"></div>
	<div class="row margin30-tb gutter5-t">
		   <a href="javascript:history.back()" class="btn btn-primary margin10-r" title="Go Back">Go Back</a>
	</div>

			</div>
</body>
</html>