package com.getinsured.iex.hub.vlp37.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.envers.Audited;
import org.hibernate.envers.NotAudited;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;


@Audited
@Entity
@Table(name="VLP37_CASE_LOG")
public class VLP37CaseLog implements Serializable, JSONAware {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VLP37_CASE_LOG_SEQ")
	@SequenceGenerator(name = "VLP37_CASE_LOG_SEQ", sequenceName = "VLP37_CASE_LOG_SEQ", allocationSize = 1)
	@Column(name="id")
	private long id;
		
	@Column(name="CASE_NUMBER")
	private String caseNumber;
	
	@Column(name="ESC_CODE")
	private int escCode;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="CASE_RESOLUTION")
	private String caseResolution;
	
	@Column (name="LAST_UPDATED")
	private Timestamp lastUpdated;
	
	@Column(name="CREATED_ON")
	private Timestamp createdOn;
	
	@NotAudited
	@Column(name="LOCKED")
	private int locked;
	
	@NotAudited
	@Column(name="LOCKED_AT")
	private Timestamp locketAt;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCaseNumber() {
		return caseNumber;
	}

	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getCaseResolution() {
		return caseResolution;
	}

	public void setCaseResolution(String caseResolution) {
		this.caseResolution = caseResolution;
	}

	public Timestamp getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Timestamp lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public Timestamp getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}
	
	@PreUpdate
	public void setUpdatedTime() {
		this.setLastUpdated(new Timestamp(System.currentTimeMillis()));
	}
	
	@PrePersist
	public void setCreatedTime() {
		this.setCreatedOn(new Timestamp(System.currentTimeMillis()));
		this.setLastUpdated(new Timestamp(System.currentTimeMillis()));
	}
	
	public int getEscCode() {
		return escCode;
	}

	public void setEscCode(int escCode) {
		this.escCode = escCode;
	}

	public int getLocked() {
		return locked;
	}

	public void setLocked(int locked) {
		this.locked = locked;
	}

	public Timestamp getLocketAt() {
		return locketAt;
	}

	public void setLocketAt(Timestamp locketAt) {
		this.locketAt = locketAt;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((caseNumber == null) ? 0 : caseNumber.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VLP37CaseLog other = (VLP37CaseLog) obj;
		if (caseNumber == null) {
			if (other.caseNumber != null)
				return false;
		} else if (!caseNumber.equals(other.caseNumber))
			return false;
		if (id != other.id)
			return false;
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("case_number", this.getCaseNumber());
		obj.put("case_status", this.getStatus());
		obj.put("resolution", this.getCaseResolution());
		obj.put("esc_code", this.getEscCode());
		obj.put("last_updated", this.getLastUpdated());
		obj.put("created", this.getCreatedOn());
		return obj.toJSONString();
	}
}
