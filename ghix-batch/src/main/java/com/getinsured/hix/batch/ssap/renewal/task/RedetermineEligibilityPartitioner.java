package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.RedetermineEligibilityService;
import com.getinsured.hix.batch.ssap.renewal.util.RedetermineEligibilityPartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.batch.service.BatchJobExecutionService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;

@Component
@Scope("step")
public class RedetermineEligibilityPartitioner implements Partitioner {

	private static final Logger LOGGER = LoggerFactory.getLogger(RedetermineEligibilityPartitioner.class);
	private static final String RENEWAL_REDETERMINE_ELIGIBILITY_JOB = "redetermineEligibilityJob";
	private Long batchSize;
	private String redetermineEligibilityCommitInterval;
	private RedetermineEligibilityService redetermineEligibilityService;
	private BatchJobExecutionService batchJobExecutionService;
	private RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams;
	private String outboundAt;
	
	
	@Override
	public Map<String, ExecutionContext> partition(int gridSize) {

		Long renewalYear = new Long(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR));
		
		LOGGER.info("Renewal Redetermine Eligibility Batch job to Process Applications with batchSize: {}, redetermineEligibilityCommitInterval: {}, renewalYear: {}", batchSize, redetermineEligibilityCommitInterval, renewalYear);
		
		Map<String, ExecutionContext> partitionMap = null;
		StringBuffer errorMessage = new StringBuffer();
		String errorCode = "";
		try 
		{
			if (!hasRunningBatchSizeOne()) {
				errorMessage.append(RenewalUtils.EMSG_RUNNING_BATCH);
				return partitionMap;
			}
			
			String defaultBatchSize = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.SSAP_AUTO_RENEWAL_BATCHSIZE);
			if(batchSize == null || batchSize == 0L){
				batchSize = Long.valueOf(defaultBatchSize);
			}
			if (!validateParams(errorMessage)) {
				return partitionMap;
			}

			List<Long> ssapApplicationIdsList = null;
			if(StringUtils.isNotBlank(outboundAt) && "Y".equalsIgnoreCase(outboundAt))
			{
				redetermineEligibilityPartitionerParams.setOutboundAt(outboundAt);
			}

			ssapApplicationIdsList = redetermineEligibilityService.getSsapAppIdsForRenewalWithSgStatus(renewalYear, batchSize);
			

			if (CollectionUtils.isNotEmpty(ssapApplicationIdsList)) 
			{
				LOGGER.info("Number of ssapApplicationIds for renewal : {}", ssapApplicationIdsList.size());
				partitionMap = new HashMap<String, ExecutionContext>();
				redetermineEligibilityPartitionerParams.clearSsapApplicationIdList();
				redetermineEligibilityPartitionerParams.addAllToSsapApplicationIdList(ssapApplicationIdsList);

				int maxCommitInterval = 1;
				int size = ssapApplicationIdsList.size();

				if (StringUtils.isNumeric(redetermineEligibilityCommitInterval)) {
					maxCommitInterval = Integer.valueOf(StringUtils.trim(redetermineEligibilityCommitInterval));
				}

				int numberOfApplicationIdToCommit = size / maxCommitInterval;
				if (size % maxCommitInterval != 0) {
					numberOfApplicationIdToCommit++;
				}

				int firstIndex = 0;
				int lastIndex = 0;

				for (int i = 0; i < numberOfApplicationIdToCommit; i++) {
					firstIndex = i * maxCommitInterval;
					lastIndex = (i + 1) * maxCommitInterval;

					if (lastIndex > size) {
						lastIndex = size;
					}
					ExecutionContext value = new ExecutionContext();
					value.putInt("startIndex", firstIndex);
					value.putInt("endIndex", lastIndex);
					value.putInt("partition", i);
					partitionMap.put("partition - " + i, value);
				}
			}
			else {
				errorMessage.append("No application found to renew for year : " + renewalYear);
				errorCode = "RENEWALBATCH_60008";
			}
		}
		catch (Exception ex) {
			errorMessage.append("RedetermineEligibilityPartitioner failed to execute : ").append(ex.getMessage());
			errorCode = "RENEWALBATCH_60009";
			LOGGER.error(errorMessage.toString(), ex);
		} finally {
			if (StringUtils.isNotBlank(errorMessage)) {
				redetermineEligibilityService.saveAndThrowsErrorLog(errorMessage.toString(),errorCode);
			}
		}
		
		return partitionMap;
	}

	private boolean validateParams(StringBuffer errorMessage) {

		boolean hasValidParams = true;

		if (null == batchSize || 0 == batchSize) {
			errorMessage.append("Invalid batch size : ");
			errorMessage.append(batchSize);
			hasValidParams = false;
		}

		if (!hasValidParams) {
			LOGGER.error(errorMessage.toString());
		}
		return hasValidParams;
	}

	/**
	 * Method is used to get Running Batch List.
	 */
	private boolean hasRunningBatchSizeOne() {

		boolean hasRunningBatchSizeOne = false;

		List<BatchJobExecution> batchExecutionList = batchJobExecutionService.findRunningJob(RENEWAL_REDETERMINE_ELIGIBILITY_JOB);
		if (batchExecutionList != null && batchExecutionList.size() == 1) {
			hasRunningBatchSizeOne = true;
		}
		return hasRunningBatchSizeOne;
	}

	public Long getBatchSize() {
		return batchSize;
	}

	public void setBatchSize(Long batchSize) {
		this.batchSize = batchSize;
	}

	public String getredetermineEligibilityCommitInterval() {
		return redetermineEligibilityCommitInterval;
	}

	public void setredetermineEligibilityCommitInterval(String redetermineEligibilityCommitInterval) {
		this.redetermineEligibilityCommitInterval = redetermineEligibilityCommitInterval;
	}

	public BatchJobExecutionService getBatchJobExecutionService() {
		return batchJobExecutionService;
	}

	public void setBatchJobExecutionService(BatchJobExecutionService batchJobExecutionService) {
		this.batchJobExecutionService = batchJobExecutionService;
	}

	public RedetermineEligibilityPartitionerParams getRedetermineEligibilityPartitionerParams() {
		return redetermineEligibilityPartitionerParams;
	}

	public void setRedetermineEligibilityPartitionerParams(
			RedetermineEligibilityPartitionerParams redetermineEligibilityPartitionerParams) {
		this.redetermineEligibilityPartitionerParams = redetermineEligibilityPartitionerParams;
	}

	public String getOutboundAt() {
		return outboundAt;
	}

	public void setOutboundAt(String outboundAt) {
		this.outboundAt = outboundAt;
	}

	public String getRedetermineEligibilityCommitInterval() {
		return redetermineEligibilityCommitInterval;
	}

	public void setRedetermineEligibilityCommitInterval(String redetermineEligibilityCommitInterval) {
		this.redetermineEligibilityCommitInterval = redetermineEligibilityCommitInterval;
	}

	public RedetermineEligibilityService getRedetermineEligibilityService() {
		return redetermineEligibilityService;
	}

	public void setRedetermineEligibilityService(RedetermineEligibilityService redetermineEligibilityService) {
		this.redetermineEligibilityService = redetermineEligibilityService;
	}
	
}

