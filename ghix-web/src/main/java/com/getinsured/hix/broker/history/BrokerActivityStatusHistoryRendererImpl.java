package com.getinsured.hix.broker.history;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Comment;
import com.getinsured.hix.platform.audit.service.HistoryRendererService;
import com.getinsured.hix.platform.comment.service.CommentService;

@Service
public class BrokerActivityStatusHistoryRendererImpl implements HistoryRendererService {

	private static final String NEW_LINE_SEP = "#newLine#";

	public static final String COMMENTS_ID = "commentsId";

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerActivityStatusHistoryRendererImpl.class);

	public static final int UPDATE_DATE = 1;
	public static final int PREVIOUS_STATUS = 2;
	public static final int NEW_STATUS = 3;
	public static final int COMMENTS = 4;
	public static final String TIMESTAMP_PATTERN1 = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String UPDATED_DATE_COL = "updated";
	public static final String COMMENT_COL = "activityComments";
	public static final String ACTIVITY_STATUS_COL = "status";
	public static final String PREVIOUS_STATUS_COL = "previousActivityStatus";
	public static final String NEW_STATUS_COL = "newActivityStatus";
	public static final String NEW_ACTIVITY_STATUS = "New Status";
	public static final String VIEW_COMMENT = "View Comment";
	public static final String REPOSITORY_NAME = "IBrokerRepository";
	public static final String MODEL_NAME = "com.getinsured.hix.model.Broker";
	public static final String APOSTROPHE_STRING = "apostrophes";
	public static final String APOSTROPHE = "'";
	public static final String NO_COMMENTS = "No Comments";
	private static final String COMMENT_TEXT_PRE = "<a onclick=\"getComment('";
	private static final String COMMENT_TEXT_POST = "');\" href='#modalBox' data-toggle='modal'>View Comment</a>";
	public static final String HASH_SEP = "#";
	public static final String LINE_SEP = "line.separator";
	
	@Autowired
	CommentService commentService;

	@Override
	public List<Map<String, Object>> processData(List<Map<String, String>> data, List<String> compareColumns,
			List<Integer> columnsToDisplay) {
		
		LOGGER.info("processData: START");
		
		List<Map<String, String>> orderedData = new ArrayList<Map<String, String>>(data);
		Collections.reverse(orderedData);
		int size = orderedData.size();
		Map<String, String> firstElement = null;
		Map<String, String> secondElement = null;
		List<Map<String, Object>> processedData = new ArrayList<Map<String, Object>>();
		firstElement = orderedData.get(0);
		for (int i = 0; i < size - 1; i++) {
			firstElement = orderedData.get(i);
			secondElement = orderedData.get(i + 1);
			for (String keyColumn : compareColumns) {
				if (!isEqual(firstElement, secondElement, keyColumn)) {
					processedData.add(formMap(firstElement, secondElement, columnsToDisplay));
					break;
				} 
			}
		}
		
		if (size == 1 || processedData.isEmpty()) {
			processedData.add(formMap(firstElement, firstElement, columnsToDisplay));
			return processedData;
		}
		
		
		LOGGER.info("processData: END");
		
		return processedData;
	}

	/**
	 * Compares values of maps firstElement and secondElement for key keyColumn.
	 */
	private boolean isEqual(Map<String, String> firstElement, Map<String, String> secondElement, String keyColumn) {
		String valueFirstElement = firstElement.get(keyColumn);
		String valueSecondElement = secondElement.get(keyColumn);		
		
		//to skip empty comment with the same status
		if(keyColumn.equals(COMMENTS_ID) &&  StringUtils.isBlank(valueFirstElement)) {
			return true;
		}
		
		if((valueFirstElement == null && valueSecondElement != null) || 
		   (valueFirstElement != null && valueSecondElement == null) ||
		   (valueFirstElement != null && valueSecondElement != null && !valueFirstElement.equals(valueSecondElement))){
			return false;
		}
	
		return true;		
		
	}
	/**
	 * Forms map for given key. The newly formed map contains key-value pairs
	 * based on the list provided as columnsToDisplay.
	 */
	private Map<String, Object> formMap(Map<String, String> firstMap, Map<String, String> secondMap,
			List<Integer> columnsToDisplay) {
		
		LOGGER.info("formMap: START");
		
		Map<String, Object> values = new HashMap<String, Object>();
		for (int colId : columnsToDisplay) {
			switch (colId) {
			case UPDATE_DATE:
				String dt = firstMap.get(UPDATED_DATE_COL);
				SimpleDateFormat sdf = new SimpleDateFormat();
				sdf.applyPattern(TIMESTAMP_PATTERN1);
				try {
					if(null != dt){
						values.put(UPDATED_DATE_COL, sdf.parse(dt));
					}
					
				} catch (ParseException e) {
					LOGGER.error("Unable to parse Updated Date " + dt + " " , e);
				}
				break;

			case PREVIOUS_STATUS:
				String previousStatus = secondMap.get(ACTIVITY_STATUS_COL);
				if(StringUtils.isBlank(previousStatus)) {
					values.put(PREVIOUS_STATUS_COL, "InActive");
				}else {
					values.put(PREVIOUS_STATUS_COL, previousStatus);
				}
				
				break;

			case NEW_STATUS:
				String newStatus = firstMap.get(ACTIVITY_STATUS_COL);
				values.put(NEW_STATUS_COL, newStatus);
				break;

			case COMMENTS:
				String commentId = firstMap.get(COMMENTS_ID);
				
				if (!StringUtils.isBlank(commentId)) {
					Comment comment = commentService.findCommentById(Integer.parseInt(commentId));
					if(comment != null &&  comment.getComment() !=null) {
						String commentStr = comment.getComment();
						commentStr = commentStr.replaceAll("\r", "");
						commentStr = commentStr.replaceAll("\n", NEW_LINE_SEP);
						String cmnt = commentStr.replace(APOSTROPHE, APOSTROPHE_STRING);
						cmnt = StringEscapeUtils.escapeHtml(cmnt);
						values.put(COMMENT_COL, COMMENT_TEXT_PRE + cmnt + COMMENT_TEXT_POST);
					}else {
						values.put(COMMENT_COL, NO_COMMENTS);
					}
				}
				else {
					values.put(COMMENT_COL, NO_COMMENTS);
				}
				break;

			}

		}
		
		LOGGER.info("formMap: END");

		return values;
	}

}
