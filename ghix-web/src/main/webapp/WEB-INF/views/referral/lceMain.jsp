<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Iterator"%>
<%@page import="org.springframework.validation.FieldError"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="java.io.*,java.util.*, javax.servlet.*"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%
        String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
%>
<c:set var="stateCode" value="<%=stateCode%>" />
<script type="text/javascript"
	src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>


<div class="gutter10">
	<style type="text/css">
.square,.well-step {
	border-radius: 0px !important;
}
#qualifyEventSelected{padding: 5px 6px;}

.checkboxlce {
	margin: 4px !important;
}

.nomargindate {
	border: 1px solid #ddd;
	margin: -21px 0px 0px 0px;
}

.eligibility-wrapper #sidebar li:before {
	content: none;
}

ul.nav.nav-list>li {
	margin: 0px !important;
}

.square,.well-step {
	border-radius: 0px !important;
}

.checkboxlce,.checkboxlcenone {
	margin: 4px !important;
}

.nomargindate {
	border: 1px solid #ddd;
	margin: -21px 0px 0px 0px;
}

h4.square {
	border: 1px solid #ddd !important;
}

.lceeventdate0 {
	border-top: 0px;
	/*position: relative;
  	top: 10px;*/
}

h4.square {
	margin-top: 0px;
}
</style>
	<div class="gutter10" >
		<div id="titlebar">
			<c:choose>
				<c:when test="${stateCode != 'CA'}">
					<h1><spring:message code="label.sep.heading"/></h1>
					<h1><spring:message code="label.lce.mainjsp.alertp3qualifyinglifeevent"/></h1>
				</c:when>
				<c:otherwise>
					<h1><spring:message code="label.sep.heading"/></h1>
				</c:otherwise>
			</c:choose>
		</div>
		<div class="row-fluid margin30-t eligibility-wrapper"
			>
			<div id="sidebar" class="span3">
				<div class="header">
					<h4>
						<spring:message code="label.sep.help"/>
					</h4>
				</div>
					<ul class="nav nav-list">
	      		<li>
                    <c:choose>
                        <c:when test="${stateCode == 'CA'}">
                            <a href="<c:url value="https://www.coveredca.com/find-help/contact/"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
                        </c:when>
                        <c:when test="${stateCode == 'ID'}">
                            <a href="<c:url value="https://www.yourhealthidaho.org/contact-us/"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
						</c:when>
						<c:when test="${stateCode == 'MN'}">
                            <i class="icon-phone"></i><spring:message code="label.sep.contact.mn"/>
                        </c:when>
                        <c:otherwise>
                            <a href="<c:url value="#"/>"><i class="icon-phone"></i><spring:message code="label.contact.us"/></a>
                        </c:otherwise>
                    </c:choose>
                </li>
	      	</ul>
			</div>
			<!-- ngView:  -->
			<div class="span9" id="rightpanel">

				<div>
					
					<div class="gutter20-lr">
					    <% 
			 Object fieldErrorObj = session.getAttribute("lceAllErrors");
			 if(fieldErrorObj != null)
	         {
	        	 %>
	        	 <c:forEach items="${lceAllErrors}" var="msg">
						<font color="red">${msg.code}</font><br/>
				</c:forEach>
				<%         	 
	         	session.removeAttribute("lceAllErrors");
	         }
	         %>
						<p class="alert">
								<strong><spring:message code="label.sep.important.heading"/></strong><spring:message code="labl.sep.important.text"/>
							</p>
							
						<form:form id="lceStart" method="POST" modelAttribute="lceActivity">
							<form:hidden path="ssapApplicationId" />
							<form:hidden path="userId" />

							<df:csrfToken />

							<div>
								<c:if test="${fn:length(lceActivity.addedMembersMap) != 0 or fn:length(lceActivity.removedMembersMap) != 0 or fn:length(lceActivity.householdChangeMembersMap) != 0 }">
									<div class="margin20-b">
									<c:if test="${fn:length(lceActivity.addedMembersMap) > 0 or  fn:length(lceActivity.removedMembersMap) > 0 }">
										<div class="header">
											<h4><spring:message code="label.sep.heading.household.eligibility1"/></h4>
										</div>
										<h5><spring:message code="label.sep.heading.household.eligibility2"/></h5>
									</c:if>
									<c:if test="${fn:length(lceActivity.addedMembersMap) > 0}">
										<div id="add" class="lce margin20-t">
											<h5><spring:message code="label.sep.heading.household.add"/></h5>
											<c:forEach items="${lceActivity.addedMembersMap}" var="event">
												<div class="row-fluid">
													<div class="span4">
														<form:label path="addedMembersMap[${event.key}].applicantName" class="control-label">${event.value.applicantName}
														<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
														</form:label>
														
														<form:hidden
															path="addedMembersMap[${event.key}].applicantId" />
															<form:hidden path="addedMembersMap[${event.key}].applicantName"/>
													</div>
													<div class="span4">
														<form:select path="addedMembersMap[${event.key}].name" required="required" id="addedMembersMap[${event.key}].name" onchange="validateEvent(this)">
															<form:option value="" label="--- Select ---" />
															<c:forEach items="${sepEventsAdd}" var="addEvent">
																<form:option value="${addEvent.name}"
																	label="${addEvent.label}" data-af="${addEvent.allowFutureEvent}" data-ig="${addEvent.gated}"/>
															</c:forEach>
														</form:select>
													</div>
													<input type="hidden" name="addedMembersMap[${event.key}].allowFutureEvent" id="addedMembersMap[${event.key}].allowFutureEvent" />
													<input type="hidden" name="addedMembersMap[${event.key}].isGated" id="addedMembersMap[${event.key}].isGated" />
													<div class="span4">
														<div data-provide="datepicker"
															class="input-append date date-picker margin10-l">
															<form:label path="addedMembersMap[${event.key}].eventDate" class="aria-hidden"><spring:message code="label.sep.eventDate"/></form:label>
															<form:input path="addedMembersMap[${event.key}].eventDate"
																aria-label=" Sample text.  M M/ D D/ Y Y Y Y"
																class="input-medium span8 datepicker" maxlength="10"
																autocomplete="off" required="required" id="addedMembersMap[${event.key}].eventDate" onchange="validateDate(this)"/>
															<span class="add-on dateSpan"><i
																class="icon-calendar"></i> </span>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
										
									</c:if>

									<c:if test="${fn:length(lceActivity.removedMembersMap) > 0}">
										<div id="remove" class="lce margin20-t">
											<h5><spring:message code="label.sep.heading.household.remove"/></h5>
											<c:forEach items="${lceActivity.removedMembersMap}"
												var="event">
												<div class="row-fluid">
													<div class="span4">
														<form:label path="removedMembersMap[${event.key}].applicantName" class="control-label">${event.value.applicantName}
														<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
														</form:label>
														
														<form:hidden
															path="removedMembersMap[${event.key}].applicantId" />
															<form:hidden path="removedMembersMap[${event.key}].applicantName"/>
													</div>
													<div class="span4">
														<form:select path="removedMembersMap[${event.key}].name" required="required" id="removedMembersMap[${event.key}].name" onchange="validateEvent(this)">
															<form:option value="" label="--- Select ---" />
															<c:forEach items="${sepEventsRemove}" var="removeEvent">
																<form:option value="${removeEvent.name}"
																	label="${removeEvent.label}" data-af="${removeEvent.allowFutureEvent}" data-ig="${addEvent.gated}"/>
															</c:forEach>
														</form:select>													
													</div>
													<input type="hidden" name="removedMembersMap[${event.key}].allowFutureEvent" id="removedMembersMap[${event.key}].allowFutureEvent" />
													<input type="hidden" name="removedMembersMap[${event.key}].isGated" id="removedMembersMap[${event.key}].isGated" />
													<div class="span4">
														<div data-provide="datepicker"
															class="input-append date date-picker margin10-l">
															<form:label path="removedMembersMap[${event.key}].eventDate" class="aria-hidden"><spring:message code="label.sep.eventDate"/></form:label>
															<form:input
																path="removedMembersMap[${event.key}].eventDate"
																aria-label=" Sample text.  M M/ D D/ Y Y Y Y"
																class="input-medium span8 datepicker" maxlength="10"
																autocomplete="off" required="required" id="removedMembersMap[${event.key}].eventDate" onchange="validateDate(this)"/>
															<span class="add-on dateSpan"><i
																class="icon-calendar"></i> </span>
														</div>
													</div>
												</div>
											</c:forEach>
										</div>
									</c:if>

									<c:if test="${fn:length(lceActivity.addedMembersMap) == 0 and fn:length(lceActivity.removedMembersMap) == 0 and fn:length(lceActivity.householdChangeMembersMap) != 0 }">				
										<div class="header">
											<h4><spring:message code="label.sep.heading.household.eligibility3"/></h4>
										</div>
										<h5><spring:message code="label.sep.heading.household.eligibility.loss"/></h5>
										<div class="row-fluid">
											<div class="span4"><spring:message code="label.sep.heading.household.eligibility.entire"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></div>
											
											<div class="span4">
												<form:select path="householdChangeReason" required="required">
													<form:option value="" label="--- Select ---" />
													<c:forEach items="${sepEventsOther}" var="otherEvent">
													<c:if test="${'CHANGE_IN_ADDRESS' ne otherEvent.name and 'AUTO_APTC_UPDATE' ne otherEvent.name}">
													<!-- Not to display change in address and aptc auto update in drop down -->
														<form:option value="${otherEvent.name}"
															label="${otherEvent.label}" />
															</c:if>
													</c:forEach>
												</form:select>
											</div>
											<div class="span4">
												<div data-provide="datepicker"
													class="input-append date date-picker margin10-l">
													<form:label path="householdChangeEventDate" class="aria-hidden"><spring:message code="label.sep.eventDate"/></form:label>
													<form:input path="householdChangeEventDate"
														aria-label=" Sample text.  M M/ D D/ Y Y Y Y"
														class="input-medium span8 datepicker" maxlength="10"
														autocomplete="off" required="required" id="householdChangeEventDate" onchange="validateDateForAddressHHold(this)"/>
													<span class="add-on dateSpan"><i
														class="icon-calendar"></i> </span>

												</div>
											</div>
											<form:hidden path="householdDisplayed"/>
										</div>
									</c:if>  	
									<c:if test="${ fn:length(lceActivity.householdChangeMembersMap) != 0}">
										<c:forEach items="${lceActivity.householdChangeMembersMap}"
											var="event">
											<form:hidden
												path="householdChangeMembersMap[${event.key}].applicantId" />
										</c:forEach>
									</c:if>
							</div>
						</c:if>
						<c:if test="${fn:length(lceActivity.changeOfAddressMembersMap)>0}">
							<div id="address" class="lce">
								<div class="header">
									<h4><spring:message code="label.sep.heading.household.address"/></h4>
									</div>

								<div class="date">
									<h5><spring:message code="label.sep.heading.household.address1"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
									</h5>
											<div data-provide="datepicker"
										class="input-append date date-picker margin30-l">
										<form:label  path="zipCountyChangeEventDate" class="aria-hidden"><spring:message code="label.sep.eventDate"/></form:label>
										<form:input path="zipCountyChangeEventDate"
											aria-label=" Sample text.  M M/ D D/ Y Y Y Y"
											class="input-medium datepicker" maxlength="10"
											autocomplete="off" required="required" id="zipCountyChangeEventDate" onchange="validateDateForAddressHHold(this)"/>
										<span class="add-on dateSpan"><i
											class="icon-calendar"></i> </span>

									</div>
								</div>


							</div>
							<c:forEach items="${lceActivity.changeOfAddressMembersMap}"
								var="event">
								<form:hidden
									path="changeOfAddressMembersMap[${event.key}].applicantId"/>
							</c:forEach>
						</c:if>
						</div>
						<div>
							<c:forEach items="${lceActivity.changeOfDobMembersMap}" var="event">
								<form:hidden
									path="changeOfDobMembersMap[${event.key}].applicantId" />
							</c:forEach>
						</div>
						<div>
							<c:forEach items="${lceActivity.noChangeMembersMap}" var="event">
								<form:hidden
									path="noChangeMembersMap[${event.key}].applicantId" />
							</c:forEach>
						</div>

						<!-- Qualifying Events -->
						<c:if test="${fn:length(lceActivity.qualifyingEventMap)>0}">
							<div class="header">
								<h4><spring:message code="label.sep.qualifying.life.event.select"/></h4>
							</div>
										
							<div class="row-fluid">
								<div class="span3">
									<c:choose>
										<c:when test="${stateCode != 'CA'}">
											<spring:message code="label.sep.qualifying.life.event"/>
										</c:when>
										<c:otherwise>
											<spring:message code="label.sep.qualifying.event"/>
										</c:otherwise>
									</c:choose>
									
								<img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></div>
								
								<div class="span6">
									<form:select path="qualifyEventSelected" class="span12" required="required" id="qualifyEvent" onchange="validateQualifyEvent(this)">
										<form:option value="" label="--- Select ---" />
										<c:forEach items="${qualifyEvents}" var="qualifyEvent">
											<form:option value="${qualifyEvent.name}"
												label="${qualifyEvent.label}" data-af="${qualifyEvent.allowFutureEvent}"/>																			
										</c:forEach>
									</form:select>												
								</div>

								<input type="hidden" name="qualifyEventAllowFuture" id="qualifyEventAllowFuture" />
																		
								<div class="span3">
									<div data-provide="datepicker"
										class="input-append date date-picker margin10-l">
										<form:label path="qualifyEventDate" class="aria-hidden"><spring:message code="label.sep.eventDate"/></form:label>
										<form:input path="qualifyEventDate"
											aria-label=" Sample text.  M M/ D D/ Y Y Y Y"
											class="input-medium span8 datepicker" maxlength="10"
											autocomplete="off" required="required" id="qualifyEventDate" onchange="validateQualifyDate(this)"/>
										<span class="add-on dateSpan"><i
											class="icon-calendar"></i> </span>

									</div>
								</div>
								
							</div>									
							<c:forEach items="${lceActivity.qualifyingEventMap}"
								var="event">
								<form:hidden
									path="qualifyingEventMap[${event.key}].applicantId" />
							</c:forEach>
						</c:if>								
						<!-- end of qualifying events-->

						<c:if test="${fn:length(lceActivity.qualifyingEventMap)==0}">	
							<p class="alert alert-info margin20-t">
								<strong class="gutter5-r"><spring:message code="label.sep.note.heading"/></strong><spring:message code="label.sep.note"/>
							</p>
						</c:if>

                            <div id="termsDiv">
                                <fieldset>
                                    <legend class="aria-hidden">Agree to Attestation</legend>
                                    <div class="form-group">
                                        <div class="help-inline hide" id="check-attestation_error">
                                            <label class="error">
									                <span> <em class="excl">!</em>
                                                        <spring:message
                                                                code="label.attestationError" />
                                                    </span>
                                            </label>
                                        </div>
                                        <div class="span12">
                                            <label class="checkbox" for="terms" title="Please check this box to continue"> <input type="checkbox" required="required" value="Accept Terms" name="terms" id="terms">
                                                <spring:message code="label.attestation" />
                                            </label>
                                        </div>
                                    </div>
                                </fieldset>
                            </div>

							<a id="backToDashboard" href="<c:url value='/indportal' />" class="btn btn-secondary"><spring:message code="label.backto.dashboard"/></a>
							<input id="continueBtn" type="submit" value="<spring:message code='label.continue'/>"
								class="btn btn-small btn-primary pull-right">
							
							<input type="hidden" name="message" id="message" />
							<input type="hidden" name="elementId" id="elementId" />
							<div id="modalConfirm" class="modal hide fade modelcntr">	  
							  <div class="modal-body" id="modal_body" align="center">
							   		
							  </div>
							  <div class="modal-footer txt-center">
							    <a href="#" id="modalBtnOk" class="btn " data-dismiss="modal">Ok</a>							    
							  </div>
							</div>
							<!-- future date beyond 60 days confirmation modal -->
							<div id="futureDateModalConfirm" class="modal hide fade modelcntr">	  
							  <div class="modal-body" id="future_date_modal_body" align="center">
							   		
							  </div>
							  <div class="modal-footer txt-center">
								<a href="#" id="modalBtnOk" class="btn " data-dismiss="modal"><spring:message code="label.event.date.change"/></a>
							    <a href="#" id="changeEventDateSubmit" class="btn " data-dismiss="modal"><spring:message code="label.confirm"/></a>							    
							  </div>
							</div>
							<script type="text/javascript">

								$(document).ready(function() {
	
									$('#modalBtnOk').on('click', function(evt)
									{
										var id = $("#elementId").val();										
										document.getElementById(id).value = '';
									    document.getElementById(id).focus();
										return;
									});
								});
								function validateQualifyEvent(e)
								{
									var selectedEvent = $("[id='" + e.id + "'] option:selected").val();								

									var allowFutureYN = $("[id='" + e.id + "'] option:selected").attr("data-af") ;										
									document.getElementById("qualifyEventAllowFuture").value = allowFutureYN;									

									var selectedEventDate =  document.getElementById("qualifyEventDate").value;	
									if(selectedEventDate != null || selectedEventDate != '')
									{
										validateQualifyDate(e);
									}
																
								}
								function validateAppStatus() {
									if (window.localStorage.ssapApplicationId) {
										var pathURL = '/hix/indportal/getapplicationstatusbyid/' + window.localStorage.ssapApplicationId;
										$.get(pathURL, function(resp) {
											var jsonStr = JSON.parse(resp);
											if (jsonStr && jsonStr.applicationStatus && jsonStr.applicationStatus === 'CL') {
												$('#app-status-closed-warning-light-box').removeClass("hide").modal({backdrop:"static",keyboard:"false"});
											} else {
												$('input[type=submit]').parents('form').submit();
											}
										});
									} else {
										$('input[type=submit]').parents('form').submit();
									}
								}
								function validateQualifyDate(e) 
								{
									var selectedEvent =  document.getElementById("qualifyEvent").value;								   
								    var selectedEventDate = document.getElementById("qualifyEventDate").value;																   
								    var serverDate = "${serverDate}";								
									
								    if(selectedEventDate == null || selectedEventDate == '')
									{
										return;	
									}

								   
								    var qualifyDateVal = selectedEventDate.split('/');
									var serverDateVal = serverDate.split('/');

									//server date format - MM/DD/YYYY
									var toDate =  new Date();
									toDate.setFullYear(serverDateVal[2],(serverDateVal[0] - 1 ),serverDateVal[1]);
								    	
									//event date format - MM/DD/YYYY
									var qualifyEventDate = new Date();
									qualifyEventDate.setFullYear(qualifyDateVal[2],(qualifyDateVal[0] - 1 ),qualifyDateVal[1]);
									
								    if(selectedEvent != null && selectedEvent != '')
									{	
								    	var qId = "qualifyEvent";
									    var allowYN = $("[id='" +qId + "'] option:selected").attr("data-af") ;										
																							
										var selectedEventText = $("[id='" + qId + "'] option:selected").text();
									    if("N" == allowYN && qualifyEventDate>toDate)
									    {
									    	$("#message").val('Future date is not allowed for selected event : '+selectedEventText);									    	 
									    	$("#modal_body").text($('#message').val());
									    	$("#elementId").val('qualifyEventDate');	
									    	$("#modalConfirm").modal({
												show:true,
												backdrop:"static"
											});										        
										  	return;										    
										} else if (e == null) {
											var numDaysBetweenDates = datediff(toDate, qualifyEventDate);
											$("#message").val('Please confirm that date ' + selectedEventDate + ' for event "'+selectedEventText + '" is correct');
											$("#future_date_modal_body").text($('#message').val());
											$("#elementId").val('qualifyEventDate');	
											$("#futureDateModalConfirm").modal({
												show:true,
												backdrop:"static"
											});
											return false;
										}
									}								    								    
								    $('input[type="submit"]').removeAttr('disabled');
									return true;			    
								 }
								 
								function validateEvent(e)
								{
									var endindex = (e.id).indexOf("]");
									var indexKey =  (e.id).substring(0, endindex+1);
									
									var selectedEvent = document.getElementById(e.id).value;								
									
									var allowFutureYN = $("[id='" + e.id + "'] option:selected").attr("data-af") ;
									var isGatedYN = $("[id='" + e.id + "'] option:selected").attr("data-ig") ;
									document.getElementById(indexKey+'.allowFutureEvent').value = allowFutureYN;
									document.getElementById(indexKey+'.isGated').value = isGatedYN;
									
									var selectedEventDate = document.getElementById(indexKey+'.eventDate').value;
									if(selectedEventDate != null || selectedEventDate != '')
									{
										validateDate(e);
									}
								}
								function validateDate(e) 
								{
									var endindex = (e.id).indexOf("]");
									var indexKey =  (e.id).substring(0, endindex+1);
									
									var selectedEvent = document.getElementById(indexKey+'.name').value;								   
								    
									var selectedEventDate = document.getElementById(indexKey+'.eventDate').value;																   
								    var serverDate = "${serverDate}";								
									
								    checkFutureDate(indexKey, selectedEventDate, serverDate, selectedEvent);								   								    
								 }
								
								function checkFutureDate(indexKey, eventDate, serverDate, selectedEvent)
								{
									var dateId = indexKey+'.eventDate';
									var eventId = indexKey+'.name';
									
									if(eventDate == null || eventDate == '')
									{
										return;	
									}								
									
									
								   	var serverDate = "${serverDate}";								
									var eventDate = document.getElementById(dateId).value;
									var qualifyDateVal = eventDate.split('/');
									var serverDateVal = serverDate.split('/');

									//server date format - MM/DD/YYYY
									var toDate =  new Date();
									toDate.setFullYear(serverDateVal[2],(serverDateVal[0] - 1 ),serverDateVal[1]);

									//event date format - MM/DD/YYYY
									var selectedEventDate = new Date();
									selectedEventDate.setFullYear(qualifyDateVal[2],(qualifyDateVal[0] - 1 ),qualifyDateVal[1]);    
									
								    if(selectedEvent != null && selectedEvent != '')
									{	
								    	var allowYN = $("[id='" +eventId + "'] option:selected").attr("data-af") ;																									   
									    if("N" == allowYN && selectedEventDate>toDate)
									    {
										    
									    	var selectedEventText = $("[id='" + eventId + "'] option:selected").text() ;
									    	$("#message").val('Future date is not allowed for selected event : '+selectedEventText);
									    	$("#elementId").val(dateId);									    	 
									    	$("#modal_body").text($('#message').val());
									    	
									    	$("#modalConfirm").modal({
												show:true,
												backdrop:"static"
											});											    
										    
										   
										  	return;										    
										}
									}								    								    
								    $('input[type="submit"]').removeAttr('disabled');	
								}
								
								function datediff(first, second) {
									// Take the difference between the dates and divide by milliseconds per day.
									// Round to nearest whole number to deal with DST.
									return Math.round((second-first)/(1000*60*60*24));
								}
								function validateDateForAddressHHold(e) 
								{
									var serverDate = "${serverDate}";								
									var eventDate = $('#'+e.id).val();

									
								    var qualifyDateVal = eventDate.split('/');
									var serverDateVal = serverDate.split('/');

									//server date format - MM/DD/YYYY
									var toDate =  new Date();
									toDate.setFullYear(serverDateVal[2],(serverDateVal[0] - 1 ),serverDateVal[1]);
								    	

									//event date format - MM/DD/YYYY
									var selectedEventDate = new Date();
									selectedEventDate.setFullYear(qualifyDateVal[2],(qualifyDateVal[0] - 1 ),qualifyDateVal[1]);
									
								 	if(selectedEventDate>toDate)
									{
								 		$("#message").val('Future date is not allowed.');
								    	$("#elementId").val(e.id);									    	 
								    	$("#modal_body").text($('#message').val());
								    	
								    	$("#modalConfirm").modal({
											show:true,
											backdrop:"static"
										});											
								    								    									    										  	
									  	return;	
									}
								 	$('input[type="submit"]').removeAttr('disabled');
								 }
								   

								
									$('input[type=submit]').click(function() {
                                        if(!document.getElementById("terms").checked){
                                            $('#check-attestation_error').removeClass('hide').addClass('show');

                                            return false;
                                        }

										if (!validateQualifyDate(null)) {
											return false;
										}
										$(this).attr('disabled', 'disabled');
										validateAppStatus();
									});
									
									$('#changeEventDateSubmit').click(function(){
										validateAppStatus();
									});
							</script>
						    <% 
						    Map<String, String> fieldSelectedEventsObj = (Map<String, String>)session.getAttribute("selectedEvents");
							   
							 if(fieldSelectedEventsObj != null)
					         {	
								 for (String addedElement : fieldSelectedEventsObj.keySet()) {
								 System.out.println("JSP - Select Values"+fieldSelectedEventsObj.get(addedElement));
					         %>
				        	 	<script type="text/javascript">
				        	 	$('select[name="<%= addedElement %>"]').find('option[value="<%= fieldSelectedEventsObj.get(addedElement) %>"]').attr("selected",true);
								</script>
								<%         
								 }
					         	session.removeAttribute("selectedEvents");
					         }
							 Map<String, String> fieldSelectedEventDatesObj = (Map<String, String>)session.getAttribute("selectedEventDates");
							 if(fieldSelectedEventDatesObj != null)
					         {	
								 for (String addedElement : fieldSelectedEventDatesObj.keySet()) {
									 
					        	 %>
					        	 	<script type="text/javascript">
									$('input[name="<%= addedElement %>"]').val('<%= fieldSelectedEventDatesObj.get(addedElement) %>');
									</script>
								<%         
								 }
					         	session.removeAttribute("selectedEventDates");
					         }
							 
							 Map<String, String> fieldAllowFutureEventObj = (Map<String, String>)session.getAttribute("allowFutureDate");

							 if(fieldAllowFutureEventObj != null)
					         {	
								 for (String addedElement : fieldAllowFutureEventObj.keySet()) {
									 
					        	 %>
					        	 	<script type="text/javascript">
					        	 	$('input[id="<%= addedElement %>"]').val('<%= fieldAllowFutureEventObj.get(addedElement) %>');							
									</script>
								<%         
								 }
					         	session.removeAttribute("allowFutureDate");
					         }
					         %>
						</form:form>
					</div>


				</div>
			</div>
		</div>
	</div>

</div>
