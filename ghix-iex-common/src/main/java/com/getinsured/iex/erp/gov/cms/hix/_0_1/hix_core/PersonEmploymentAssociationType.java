//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ESIAugmentationType;


/**
 * A relationship A data type for a relationship between an employer and an employee.
 * 
 * <p>Java class for PersonEmploymentAssociationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonEmploymentAssociationType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}PersonEmploymentAssociationType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}ESIAugmentation" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}Employer" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}EmploymentStatus" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonEmploymentAssociationType", propOrder = {
    "esiAugmentation",
    "employer",
    "employmentStatus"
})
public class PersonEmploymentAssociationType
    extends com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonEmploymentAssociationType
{

    @XmlElement(name = "ESIAugmentation", namespace = "http://hix.cms.gov/0.1/hix-ee")
    protected ESIAugmentationType esiAugmentation;
    @XmlElement(name = "Employer")
    protected OrganizationType employer;
    @XmlElement(name = "EmploymentStatus")
    protected EmploymentStatusType employmentStatus;

    /**
     * Gets the value of the esiAugmentation property.
     * 
     * @return
     *     possible object is
     *     {@link ESIAugmentationType }
     *     
     */
    public ESIAugmentationType getESIAugmentation() {
        return esiAugmentation;
    }

    /**
     * Sets the value of the esiAugmentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ESIAugmentationType }
     *     
     */
    public void setESIAugmentation(ESIAugmentationType value) {
        this.esiAugmentation = value;
    }

    /**
     * Gets the value of the employer property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationType }
     *     
     */
    public OrganizationType getEmployer() {
        return employer;
    }

    /**
     * Sets the value of the employer property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationType }
     *     
     */
    public void setEmployer(OrganizationType value) {
        this.employer = value;
    }

    /**
     * Gets the value of the employmentStatus property.
     * 
     * @return
     *     possible object is
     *     {@link EmploymentStatusType }
     *     
     */
    public EmploymentStatusType getEmploymentStatus() {
        return employmentStatus;
    }

    /**
     * Sets the value of the employmentStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmploymentStatusType }
     *     
     */
    public void setEmploymentStatus(EmploymentStatusType value) {
        this.employmentStatus = value;
    }

}
