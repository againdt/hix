<%@page session="true" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<style>
.tall {
	min-height: 300px;
	margin-top: 50px;
}
</style>

<script>
	$(document).ready(function() {
		document.title = "Covered California | Frequently Asked Questions";
	});
</script>

<div class="row-fluid">
	<h1 class="gutter10"><a name="skip"></a>Broker Administrator FAQs</h1>
	<!--<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right">FAQ Home</a></small>-->
</div>
<div class="tall">
	<!--  end of row-fluid -->
	<div class="txt-center alert alert-info">
		<h1>Page Under Construction</h1>
	</div>
</div>

<script>
$(document).ready(function() {
	$('.txt-center').removeClass('titlebar');
});

</script>