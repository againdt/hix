<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %> 
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script> 
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<div class="gutter10">
    <div class="row-fluid">
        <ul class="page-breadcrumb">
           <li><a href="<c:url value="/admin/issuer/details/${issuerObj.id}"/>">&lt; <spring:message  code="label.back"/></a></li>
           <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
           <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
           <li><spring:message  code="pgheader.financialInformation"/></li>
        </ul><!--page-breadcrumb ends-->

    </div>
    <div class="row-fluid issuer_info">
        <div class="centered_all">
            <div class="span3">
                <img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
            </div>
            <div class="span9">
                <h1>${issuerObj.name} </h1>
            </div>
        </div>
    </div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
			<div class="header">
                <h4><spring:message  code="pgheader.issuerAbout"/></h4>
            </div>
             
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
        				<jsp:param name="pageName" value="financialInfo"/>
				</jsp:include>
                <!-- end of side bar -->
                <br />
                <a href='<c:url value="/admin/issuer/financial/info/add/${encIssuerId}"/>' class="btn btn-primary"><i class="icon-plus icon-white"></i>  <spring:message  code="label.addPaymentMethod"/></a>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
				<div class="header margin5-b">
					<h4 class="pull-left"><spring:message  code="pgheader.financialInformation"/></h4>	
				</div>
             	<c:choose>
						<c:when test="${fn:length(paymentMethodsObj) > 0}">
							<table class="table table-condensed table-border-none table-striped">
								<thead>
									<tr class="header">															 	
										<spring:message code="label.paymentMethodName" var="paymentMethodName" />
										<spring:message code="label.bankName" var="bankName" />
										<spring:message code="label.lastUpdated" var="lastUpdated" />
										<spring:message code="label.status.plan" var="statusValue" />
										<spring:message code="label.default" var="defaultValue" />
								 		<th scope="col" class="sortable" style="width: 160px;"><dl:sort title="${paymentMethodName}" sortBy="paymentMethodName" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${bankName}" sortBy="financialInfo.bankInfo.bankName" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${lastUpdated}" sortBy="financialInfo.updated" sortOrder="${sortOrder}"></dl:sort></th>
								     	<th scope="col" class="sortable"><dl:sort title="${statusValue}" sortBy="status" sortOrder="${sortOrder}"></dl:sort></th>
										<th scope="col" class="sortable txt-center"><dl:sort title="${defaultValue}" sortBy="isDefault" sortOrder="${sortOrder}"></dl:sort></th>
										<th>&nbsp;</th>
									</tr>
								</thead>
								
								<c:forEach items="${paymentMethodsObj}" var="paymentMethodsObj" varStatus="vs">
								<c:set var="encPaymentId" ><encryptor:enc value="${paymentMethodsObj.id}" isurl="true"/> </c:set>
									<tr>
										<td id="name_${paymentMethodsObj.id}"><a class="margin10-l" href="<c:url value="/admin/issuer/financial/info/${encIssuerId}?paymentId=${encPaymentId}"/>">${paymentMethodsObj.paymentMethodName}</a></td>
										<td align="center">${paymentMethodsObj.financialInfo.bankInfo.bankName} </td>
										<td align="center"><fmt:formatDate type="both" pattern="MMM dd, yyyy" value="${paymentMethodsObj.financialInfo.bankInfo.updated}" timeZone="${timezone}" /></td>
										<td align="center" id="status_${vs.index}">${paymentMethodsObj.status}</td>
										<td align="center" class="txt-center" id="defaulticon_${vs.index}">
												<c:choose>
											        <c:when test="${'Y' == paymentMethodsObj.isDefault}">											                
														<i class="icon-ok-sign"></i> <!-- this icon will indicate whether or not the account is default -->														
													</c:when>
													<c:otherwise>
													<span></span>
												 	</c:otherwise>
												</c:choose>												
										</td>
										<td class="txt-center">
												<div class="">
													<div class="dropdown">
														<a href="/page.html" data-target="#" data-toggle="dropdown" role="button" id="dLabel" class="dropdown-toggle">
															<i class="icon-cog"></i>
															<i class="icon-caret-down"></i>
														</a>
														<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right"><!-- class removed "action-align-l" -->	
															<li>
															<a href="#" onClick="updatePaymentMethod(this,'edit');" id="edit_${vs.index}"><i class="icon-pencil"></i> Edit</a></li>													
															<c:if test="${paymentMethodsObj.status == 'Active'}">
																<li><a href="#" onClick="updatePaymentMethod(this,'InActive');" id="changestatus_${vs.index}"><i class="icon-remove"></i> Inactive</a></li>
															</c:if>
															<c:if test="${paymentMethodsObj.status == 'InActive'}">
																<li><a href="#" onClick="updatePaymentMethod(this,'Active');" id="changestatus_${vs.index}"><i class="icon-ok"></i> Active</a></li>
															</c:if>
															<c:if test="${'N' == paymentMethodsObj.isDefault && 'Active' == paymentMethodsObj.status}">
																<li><a href="#" onClick="updatePaymentMethod(this,'makedefault');" id="lidefault_${vs.index}"><i class="icon-ok-sign"></i> Make Default</a></li>
															</c:if>
															
																													
														</ul>
													</div>
												</div>
											<input type="hidden" id="paymentid_${vs.index}" name="paymentid"  value="${paymentMethodsObj.id}">
											<input type="hidden" id="encPaymentId_${vs.index}" name="encPaymentId"  value="${encPaymentId}">
                     						<input type="hidden" id="default_${vs.index}" name="default"  value="${paymentMethodsObj.isDefault}">
                     						
											<c:if test="${'Y' == paymentMethodsObj.isDefault}">
												<input type="hidden" id="defaultpaymentid" name="defaultpaymentid_${vs.index}"  value="${paymentMethodsObj.id}">
											</c:if>											
											<input type="hidden" id="existingpaymentid" name="existingpaymentid" value="<encryptor:enc value="${existingpaymentid}"></encryptor:enc>"/>
										</td>
									</tr>
								
								</c:forEach>
							</table>
							<input type="hidden" id="issuerid" name="issuerid" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
							<div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info"><spring:message code="label.recordsNotFound"/></div>
					</c:otherwise>
				</c:choose>
           </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<df:csrfToken/>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<script type="text/javascript">
	$('.ttclass').tooltip();
	var csrValue = $("#csrftoken").val();
	function updatePaymentMethod(e,test) {		
		if (test == 'makedefault') {
			var didConfirm = confirm("<spring:message code='paymentMethod.defaultChange'/>");
			if (didConfirm == true) {

			} else {
				return false;
			}
		}
		if (test != 'edit') {
			var validateUrl = '';
			var parameter = '';
			
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);
			
			var selectedstatus = '';
			if (test == 'makedefault') {
				validateUrl = "<c:url value='/admin/issuer/financial/info/makepaymentdefault'/>";				
				parameter = {					
					id : $("#paymentid" + index).val(),	
					existingDefaultId : ($("#defaultpaymentid").val() != null) ? $("#defaultpaymentid").val() : 0,
					csrftoken:csrValue
				};
			} else {
				validateUrl = "<c:url value='/admin/issuer/financial/info/changepaymentptatus'/>";
				selectedstatus = test;
				parameter = {
					id : $("#paymentid" + index).val(),
					status : selectedstatus,
					csrftoken:csrValue
				};
								
				if(selectedstatus=='InActive' && ('Y'==($('#default' + index).val()))){
					alert("<spring:message code='label.defaultPaymentInfo'/>");
					return false;
				}
			}

			$
					.ajax({
						url : validateUrl,
						type : "POST",
						data : parameter,
						success : function(response) {							
							if (response != null || response != 'null') {
								if (selectedstatus != '') {
									
									
									$('#status' + index).text(response);
									
									$('#changestatus' + index).remove();
									
									if (response == 'InActive') {
									  $('#action'+ index ).append(
										    $('<li>').append(
										        $('<a>').attr({'href': '#',
										           'onClick': 'updatePaymentMethod(this,\'Active\')',
										           'id': 'changestatus'+index}).append(
										                   $('<i>').attr('class', 'icon-ok')).append('Active')
										                   
							          )); 
									  
									  if ('N'==($('#default' + index).val())){
										  $('#lidefault' + index).remove();
									  }
									}
									 else if (response == 'Active') {
										 $('#action'+ index ).append(
										 $('<li>').append(
											        $('<a>').attr({'href': '#',
											           'onClick': 'updatePaymentMethod(this,\'InActive\')',
											           'id': 'changestatus'+index}).append(
											                   $('<i>').attr('class', 'icon-remove')).append('InActive')
								         )); 
										 
										 if ('N'==($('#default' + index).val())){
											 $('#action'+ index ).append(
													 $('<li>').append(
														        $('<a>').attr({'href': '#',
														           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
														           'id': 'lidefault'+index}).append(
														                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
											         )); 
										 }
									}else if(response == 'EXCEPTION'){
										alert("Exception occured while updating financial information");
									}
									
								} else {
									if (response == 'Y') {
										$('#status' + index).text(
												$('#status' + index).text());
										$('#defaulticon' + index).text('');
										$('#defaulticon' + index).append($('<i>').attr('class', 'icon-ok-sign'));
										$('#default' + index).val('Y');
										$('#lidefault' + index).remove();
										
										if ($('#defaultpaymentid').length)
										{
												$("#defaultpaymentid").val(
														$("#paymentid" + index).val());
												
												var previousdefaultname = $(
														"#defaultpaymentid").attr(
														'name');
												var newstartindex = previousdefaultname
														.indexOf("_");
												var newindex = (previousdefaultname)
														.substring(
																newstartindex,
																(previousdefaultname).length);
												$('#default' + newindex).val('N');
												$('#defaulticon' + newindex).text('');
												if('Active' == $('#status'+newindex).text()){
												$('#action'+ newindex ).append(
														 $('<li>').append(
															        $('<a>').attr({'href': '#',
															           'onClick': 'updatePaymentMethod(this,\'makedefault\')',
															           'id': 'lidefault'+newindex}).append(
															                   $('<i>').attr('class', 'icon-ok-sign')).append('Make Default')
												         )); 
												}
												$("#defaultpaymentid").attr('name','defaultpaymentid' + index);
										}
										else{
											$('<input>').attr({
											    type: 'hidden',
											    id: 'defaultpaymentid',
											    name: 'defaultpaymentid'+index,
											    value:$("#paymentid" + index).val()
											}).appendTo('form');
										}
										
									}else if(response == 'EXCEPTION'){
										alert("Exception occured while updating aaa  financial information");
									}
								}
							}
						},
					});
		} else {			
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);	
			var encPaymId =  $("#encPaymentId" + index).val();
			var encIssId = "${encIssuerId}";
			location.href= "<c:url value='/admin/issuer/financial/info/edit/"+encIssId +"?paymentId="+ encPaymId +"'/>";	
		}
		
	}	
</script>
