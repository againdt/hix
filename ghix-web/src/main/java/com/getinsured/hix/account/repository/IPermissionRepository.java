package com.getinsured.hix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Permission;

public interface IPermissionRepository extends JpaRepository<Permission, Integer> {
	public Permission findIdByName(String name);
	
}
