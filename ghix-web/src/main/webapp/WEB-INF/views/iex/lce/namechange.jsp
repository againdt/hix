<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<style type="text/css">
  .bootstrap-datetimepicker-widget {
    position: absolute;
    z-index: 400000;
  }

  .ssngGender > div {
    display: inline-block;
  }
</style>
<script type="text/ng-template" id="namechange.jsp">
  <div class="gutter20" ng-controller="nameChangeController">
    <%--h3><spring:message code="label.lce.entereventdetails"/></h4>
    <form class="form-horizontal" name="demographic">
      <div class="control-group">
        <span class="control-label margin5-t">
          <spring:message code="label.lce.whatInfoUpdate"/>
        </span>
        <div class="controls">
          <label for="demoName" class="radio-inline">
            <input type="radio" name="demographic" id="demoName" value="{{demographics[0].name}}" ng-model="eventInfo.eventSubCategory"  required ng-change="resetValues();">
            <spring:message code="label.lce.namechange"/>
          </label>
        </div>
        <div class="controls">
          <label for="demoDOB" class="radio-inline">
            <input type="radio" name="demographic" id="demoDOB" value="{{demographics[3].name}}" ng-model="eventInfo.eventSubCategory" required ng-change="resetValues();">
            <spring:message code="label.lce.dob2"/>
          </label>
        </div>
        <div class="controls">
          <label for="demoSSN" class="radio-inline">
            <input type="radio" name="demographic" id="demoSSN" value="{{demographics[1].name}}" ng-model="eventInfo.eventSubCategory" required ng-change="resetValues();">
            <spring:message code="label.lce.ssn2"/>
          </label>
        </div>
  		  <div class="controls ">
      		<span class="validation-error-text-container marginL20" ng-show="demographicSubmitted && demographic.demographic.$error.required"><spring:message code="label.lce.plsSelectEvent"/></span>
     		</div>
      </div>
    </form--%>
    <div class="margin30-t">
      <h4><spring:message code="label.lce.famHouseholdSum"/></h4>
      <table id="" role="presentation" class="table table-striped"  ng-repeat="taxHousehold in jsonObject.singleStreamlinedApplication.taxHousehold">
        <tbody>
         <tr>
            <td><spring:message code="label.lce.firstname"/></td>
            <td><spring:message code="label.lce.lastname"/></td>
            <td><spring:message code="label.lce.relationship"/></td>
            <td><spring:message code="label.lce.seekingcoverage"/></td>
            <td><spring:message code="label.lce.takeAction"/></td>
          </tr>
          <tr ng-repeat="(fIndex, householdMember)  in taxHousehold.householdMember | filter: activeDeathFilter track by $index">
            <td>{{householdMember.name.firstName}}</td>
            <td>{{householdMember.name.lastName}}</td>
            <td>
              {{getRelationShipWithPrimaryHousehold(householdMember.personId)}}
            </td>
            <td>
              <span ng-if="householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.yes"/></span>
              <span ng-if="!householdMember.applyingForCoverageIndicator"><spring:message code="label.lce.no"/></span>
  					</td>
            <td <%--ng-if="!eventInfo.eventSubCategory == ''"--%>>
              <%--a style="width:115px" class="btn btn-secondary btn-small" ng-if="eventInfo.eventSubCategory == ''"><spring:message code="label.lce.update"/></a>
              <a style="width:115px" class="btn btn-primary btn-small" ng-click='showModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember),"name");' ng-if="eventInfo.eventSubCategory ==demographics[0].name"><spring:message code="label.lce.update"/></a>
              <a style="width:115px" class="btn btn-primary btn-small" ng-click='showModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember),"dob");' ng-if="eventInfo.eventSubCategory ==demographics[3].name"><spring:message code="label.lce.update"/></a>
              <a style="width:115px" class="btn btn-primary btn-small" ng-click='householdMember.socialSecurityCard.socialSecurityCardHolderIndicator= true ; householdMember.socialSecurityCard.reasonableExplanationForNoSSN="" ; showModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember),"ssn")' ng-if="eventInfo.eventSubCategory == demographics[1].name"><spring:message code="label.lce.update"/></a--%>
			  <a style="width:115px" class="btn btn-primary btn-small" ng-click='showModal(householdMember,getOriginalIndex( householdMember,taxHousehold.householdMember),"tab")'><spring:message code="label.lce.update"/></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>

	  <!-- Tab MODAL  -->
    <modal-dialog-static-backdrop show='modalShownTab' ng-model="modalHouseholdMember" modal-event-type="modalEventType" id="tabDiv" width="55%">
	<tabset>
	 <tab heading="<spring:message code="label.lce.basicinfo"/>" active="tabs.namechangeform">
      <div class="gutter20">
       <h4><spring:message code="label.lce.pleaseupdate"/> {{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}<spring:message code="label.lce.sname"/></h4>
		<form class="form-horizontal" name="namechangeform">
        <div class="control-group">
            <label class="control-label" for="eventDateNameChange"><spring:message code="label.lce.eventdate"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="nameChangeCalenderDiv" name="nameChangeEventDate" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate" check-after="0">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateNameChange" ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate" name="eventDateNameChange" class="span8"  required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>

            </div>
			<div class="controls ">
  	      		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.eventDateNameChange.$error.pattern"><spring:message code="label.lce.plsEnterValidDate"/></span>
   		  	</div>
			<div class="controls ">
  	      		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.eventDateNameChange.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
				<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.nameChangeEventDate.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
   		  	</div>
			
          </div>


          <div class="control-group">
            <label class="control-label" for="firstName"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <input type="text" id="firstName" name= "firstName" required ng-model="modalHouseholdMember.name.firstName" class="input-large span6" ng-pattern='/^[a-zA-Z]{1,45}$/'>
            </div>
            <div class="controls">
              <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.firstName.$error.required"><spring:message code="label.lce.plsEnter1stName"/></span>
            </div>
			       <div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.firstName.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
        	   </div>
          </div>

          <div class="control-group">
            <label class="control-label" for="middleName"><spring:message code="label.lce.middlename"/></label>
            <div class="controls">
              <input type="text" id="middleName" name="middleName" ng-pattern='/^[a-zA-Z]{1,45}$/' ng-model="modalHouseholdMember.name.middleName" class="input-large span6" >
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.middleName.$error.pattern"><spring:message code="label.lce.entervalidmiddlename"/></span>
        	</div>
          </div>

          <div class="control-group">
            <label class="control-label" for="lastName"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <input type="text" id="lastName" name="lastName" required ng-model="modalHouseholdMember.name.lastName" class="input-large span6" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" >
            </div>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.lastName.$error.required" ><spring:message code="label.lce.plsEnterLastame"/></span>
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && (namechangeform.lastName.$error.pattern || namechangeform.lastName.$error.maxlength)"><spring:message code="label.lce.entervalidlastname"/></span>
        	</div>
          </div>

          <div class="control-group">
            <label class="control-label" for="suffix"><spring:message code="label.lce.suffix"/> </label>
            <div class="controls">
              <select class="span6" id="suffix" name="suffix" ng-model="modalHouseholdMember.name.suffix" ng-options="s for s in suffix">
                <option value=""><spring:message code="label.lce.suffix"/></option>
			  </select>
            </div>
          </div>
			
		  <!-- Gender -->
      <div class="control-group">
        <label class="control-label" for="sexgender">
          <input type="hidden" id="sexgender">
          <spring:message code="label.lce.sex"/>
          <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="<spring:message code='label.lce.required'/>" aria-hidden="true">
        </label>
        <div class="controls">
  	      <label class="radio inline" for="female">
  	        <input type="radio" id="female" name="gender" value="female" ng-model="modalHouseholdMember.gender" required/>
  	        <spring:message code="label.lce.female"/>
  	      </label>
  	      <label class="radio inline" for="male">
  	      	<input type="radio" id="male" name="gender" value="male" ng-model="modalHouseholdMember.gender" required />
						<spring:message code="label.lce.male"/>
  	      </label>
        </div>
        <div class="controls">
          <div class="validation-error-text-container" ng-if="formValidations.namechangeform && namechangeform.gender.$error.required">
            <spring:message code="label.lce.pleaseselectagender"/>
          </div>
        </div>
      </div>

	<div class="control-group" ng-if="modalHouseholdMember.personId==1">
            <label class="control-label" for="email"><spring:message code="label.lce.email"/></label>
            <div class="controls">
			  <input type="text" id="email" name="email" class="input-medium" ng-model="modalHouseholdMember.householdContact.contactPreferences.emailAddress" ng-pattern='/^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/' placeholder="<spring:message code="label.lce.email"/>"  required/>
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.email.$error.pattern"><spring:message code="label.lce.entervalidemail"/></span>
				<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.email.$error.required"><spring:message code="label.lce.pleaseenteremail"/></span>
        	</div>
    </div>	
  
	<div class="control-group" ng-if="modalHouseholdMember.personId==1">
            <label class="control-label" for="middleName"><spring:message code="label.lce.cellPhone"/></label>
            <div class="controls">
			  <input type="text" id="phoneNo" name="phoneNo" class="input-medium" ng-model="modalHouseholdMember.householdContact.phone.phoneNumber" placeholder="(xxx) xxx-xxxx" ui-mask="(999) 999-9999" init-mask />
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.phoneNo.$error.pattern"><spring:message code="label.lce.phoneNumValid"/></span>
        	</div>
      </div>

	  <div class="control-group" ng-if="modalHouseholdMember.personId==1">
            <label class="control-label" for="middleName"><spring:message code="label.lce.homePhone"/></label>
            <div class="controls">
				  <div class="pull-left">
				 	 <input type="text" id="homePhoneNo" name="homePhoneNo" class="input-medium" ng-model="modalHouseholdMember.householdContact.otherPhone.phoneNumber" placeholder="(xxx) xxx-xxxx" ui-mask="(999) 999-9999" init-mask />
				  </div>				
				  <div class="pull-left margin5-l">
						<input type="text" id="homeExt" name="homeExt" class="input-mini span6" ng-model="modalHouseholdMember.householdContact.otherPhone.phoneExtension" placeholder='<spring:message code="label.lce.phoneExt" />' ng-pattern="/^\d{1,4}$/" />
				  </div>
            </div>
			
			<div class="controls margin5-t">
				<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.homePhoneNo.$error.pattern"><spring:message code="label.lce.phoneNumValid"/></span>
   		  		<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.homeExt.$error.pattern"><spring:message code="label.lce.phoneNumExtValid"/></span>
			</div>
		</div>

        <div class="control-group margin30-t" ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator">
            <div id="isthenamethesameasssn">
              <h4>
                <spring:message code="label.lce.is"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.sameName"/>
                <div ng-switch on="modalHouseholdMember.gender" class="ssngGender">
                  <div ng-switch-when="male"> <spring:message code="label.lce.paragraphhis"/> </div>
                  <div ng-switch-when="female"> <spring:message code="label.lce.paragraphher"/> </div>
                  <div ng-switch-default></div>
                  Social Security Card? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                </div>
              </h4>
            </div>
            <div class="margin10-l">
              <label class="radio-inline" for="samenameT">
                <input type="radio" id="samenameT" name="samename" ng-value="true"  ng-click='updateNameOnSSNCard(modalHouseholdMember);' ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" /> <spring:message code="label.lce.yes"/>
                </label>
              <label class="radio-inline" for="samenameF">
                <input type="radio" id="samenameF" name="samename" ng-value="false"  ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-click="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.suffixOnSSNCard='';" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" > <spring:message code="label.lce.no"/>
              </label>
            </div>
            <!-- Error -->
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.samename.$error.required"><spring:message code="label.lce.pleaseselectif"/> {{householdMember.name.firstName}}<spring:message code="label.lce.nameisthesameonssn"/></span>
            </div>
          </div><!-- Spouse has a SSN -->

          <!-- This displays when the spouses's name is not the same on the SSN -->
          <div class="ssnName" ng-show="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false">
            <div class="control-group">
              <label class="control-label" for="firstNameOnSSNCard"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
              <div class="controls">
                <input type="text" id="firstNameOnSSNCard" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" required ng-model="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
              </div>
              <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.firstNameOnSSNCard.$error.required"><spring:message code="label.lce.plsEnter1stName"/></span>
				<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.firstNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
              </div>
            </div><!-- First Name -->
            <div class="control-group">
              <label class="control-label" for="middleNameOnSSNCard"><spring:message code="label.lce.middlename"/> </label>
              <div class="controls">
                <input type="text" id="middleNameOnSSNCard" ng-pattern='/^[a-zA-Z]{1,45}$/' name="middleNameOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
              </div>
			  <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.middleNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidmiddlename"/></span>
              </div>
            </div><!-- Middle Name -->
            <div class="control-group">
              <label class="control-label" for="lastNameOnSSNCard"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
              <div class="controls">
                <input type="text" id="lastNameOnSSNCard" name="lastNameOnSSNCard" required ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
              </div>
              <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.lastNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenteralastname"/></span>
				<span class="validation-error-text-container" ng-show="formValidations.namechangeform && namechangeform.lastNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidlastname"/></span>
               </div>
            </div><!-- Last Name -->
            <div class="control-group">
              <label class="control-label" for="suffixOnSSNCard"><spring:message code="label.lce.suffix"/> </label>
              <div class="controls">
                <select class="span6" id="suffixOnSSNCard" name="suffixOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.suffixOnSSNCard" ng-options="s for s in suffix" >
                  <option value=""><spring:message code="label.lce.suffix"/></option>
                </select>
              </div>
            </div><!-- Suffix -->
          </div><!-- SSN Name the Same -->
        </form>
        <%--dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('name');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'name')" ng-if="namechangeform.$valid"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!namechangeform.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl--%>
      </div>
	</tab>
	<tab heading="<spring:message code="label.lce.dob2"/>" active="tabs.dobchange">
	<div class="gutter20" ng-if="modalShownTab" >
       <h4><spring:message code="label.lce.pleaseupdate"/> <strong>{{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}'s</strong> <spring:message code="label.lce.dob"/></h4>
       	 <form class="form-horizontal" name="dobchange">
          <div class="control-group">
            <label class="control-label" for="eventDateDOB"><spring:message code="label.lce.eventdate"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="dobChangeCalenderDiv" name="dobChangeEventDate" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].dobChangeDate" check-after="0">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateDOB" ng-model="eventInfoArrayTemp[originalIndex].dobChangeDate" name="eventDateDOB" class="span12" required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>
       			<div >
              		<span class="validation-error-text-container" ng-show="formValidations.dobchange && dobchange.eventDateDOB.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
					<span class="validation-error-text-container" ng-show="formValidations.dobchange && dobchange.dobChangeEventDate.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
            	</div>
            </div>
          </div>


          <!-- Date of Birth -->
          <div class="control-group">
            <label class="control-label" for="dateOfBirth"><spring:message code="label.lce.dob"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <div id="date" class="input-append" name="dateOfBirthDiv" ng-model="modalHouseholdMember.dateOfBirth" datetimez  required >
            	<input type="text" id="dateOfBirth" name="dateOfBirth" date-formatter date="modalHouseholdMember.dateOfBirth" class="span12"  placeholder="MM/DD/YYYY"></input>
            	<span class="add-on">
              		<i class="icon-calendar"></i>
            	</span>
          	  </div>

             </div><!-- Controls -->
            <!-- Error -->
            <label class="control-label"></label>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="formValidations.dobchange && dobchange.dateOfBirthDiv.$error.pattern "><spring:message code="label.lce.entervalidedob"/></span>
            </div>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="formValidations.dobchange && dobchange.dateOfBirthDiv.$error.required"><spring:message code="label.lce.pleaseenteradob"/></span>
            </div><!-- Controls -->
          </div><!-- Date of Birth -->
        </form>
        <%--dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('dob');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="showModal(modalHouseholdMember,originalIndex,'relChange')" ng-if="dobchange.$valid && checkIf26YearOld(modalHouseholdMember) && modalHouseholdMember.applyingForCoverageIndicator "><spring:message code="label.lce.ok"/></a>
			<a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'dob')" ng-if="dobchange.$valid && ((!checkIf26YearOld(modalHouseholdMember)) || (!modalHouseholdMember.applyingForCoverageIndicator))"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!dobchange.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl--%>
      </div>
	</tab>
	<tab heading="<spring:message code="label.lce.ssn2"/>" active="tabs.maritalStatusSSNForm">
		<div class="gutter20">
       <h4><spring:message code="label.lce.pleaseupdate"/> <strong>{{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}'s</strong> <spring:message code="label.lce.ssn"/></h4>
          <form class="form-horizontal" id="maritalStatusSSNForm" name="maritalStatusSSNForm" novalidate>
          <div class="control-group">
            <label class="control-label" for="eventDateSSN"><spring:message code="label.lce.eventdate"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="ssnChangeCalenderDiv" name="ssnChangeEventDate" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].ssnChangeDate" check-after="0">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateSSN" ng-model="eventInfoArrayTemp[originalIndex].ssnChangeDate"  name="eventDateSSN" class="span12" required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>
             	<div >
              		<span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.eventDateSSN.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
					<span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.ssnChangeEventDate.$error.checkAfter"><spring:message code="label.lce.futureNotAllowed"/></span>
            	</div>

            </div>
          </div>




            <div>
              <label class="alert alert-info" for="hidDummyAlertField">
                <spring:message code="label.lce.ssn.p1"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong>
                <spring:message code="label.lce.ssn.p2"/>
                <spring:message code="label.lce.ssn.p3"/>
                <input type="hidden" id="hidDummyAlertField" />
              </label>
            </div>

            <!-- Spouse have a SSN? -->
            <div id="ssnNumber">
              <span>
                <spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong><spring:message code="label.lce.apoS"/> <spring:message code="label.lce.ssn.p8"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
              </span>
              <div class="margin10-l">
                <label class="radio-inline" for="snn4spouseT">
                  <input type="radio" id="snn4spouseT" name="snn4spouse" required ng-value="true" value="true" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-click="modalHouseholdMember.socialSecurityCard.reasonableExplanationForNoSSN='';"> <spring:message code="label.lce.yes"/>
                </label>
                <label class="radio-inline" for="snn4spouseF">
                  <input type="radio" id="snn4spouseF" name="snn4spouse" ng-click='removeSSN(modalHouseholdMember);' required ng-value="false" value="false" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator"  > <spring:message code="label.lce.no"/>
                </label>
              </div> <!--controls -->

            <!-- Error -->
            <div class="margin30-l margin10-t">
              <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.snn4spouse.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
            </div>

            </div><!-- Spouse have a SSN? -->

            <!-- Spouse has an SSN-->
            <div class="row-fluid control-group ssnum margin10-t" ng-if="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator">
              <div class="control-group">
                <label class="control-label" for="socialSecurityNumber"><spring:message code="label.lce.ssn"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                <div class="controls">
                 <input type="text" id="socialSecurityNumber" name="socialSecurityNumber" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityNumber" style="display:none" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-pattern="/^([1-57-8][0-9]{2}|0([1-9][0-9]|[0-9][1-9])|6([0-57-9][0-9]|[0-9][0-57-9]))([1-9][0-9]|[0-9][1-9])([1-9]\d{3}|\d[1-9]\d{2}|\d{2}[1-9]\d|\d{3}[1-9])$/" ng-minlength="9" ng-maxlength="9" />
					<label for="ssn1" class="aria-hidden">SSN 1</label>
					<label for="ssn2" class="aria-hidden">SSN 2</label>
					<label for="ssn3" class="aria-hidden">SSN 3</label>
                  <input type="text" placeholder="XXX" class="span2" id="ssn1" name="ssn1"  minlength="3" maxlength="3" ng-model="ssn1"  onkeyup="ssnMask('ssn1')">
                  <input type="text" placeholder="XX" class="span2" id="ssn2" name="ssn2"   minlength="2" maxlength="2" ng-model="ssn2"  onkeyup="ssnMask('ssn2')">
                  <input type="text" placeholder="XXXX" class="span2" id="ssn3" name="ssn3" minlength="4" maxlength="4" ng-model="ssn3"  onkeyup="ssnMask('ssn3')">
                </div>
                <!-- Error -->
                <div class="controls margin10-t">
                  <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.socialSecurityNumber.$error.required"><spring:message code="label.lce.pleaseenterssn"/></span>
                  <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.socialSecurityNumber.$error.pattern "><spring:message code="label.lce.pleaseentervalidssn"/></span>
                </div>
              </div>

              <div id="isthenamethesameasssn2">
                <span><spring:message code="label.lce.is"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.thesamenamethatappearsonssn"/>
                <div ng-switch on="modalHouseholdMember.gender" class="ssngGender">
                  <div ng-switch-when="male"> <spring:message code="label.lce.paragraphhis"/> </div>
                  <div ng-switch-when="female"> <spring:message code="label.lce.paragraphher"/> </div>
                  <div ng-switch-default></div>
               </div>
                Social Security Card? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></span>
                <div class="margin10-l">
                  <label class="radio-inline" for="samenameT2">
                    <input type="radio" id="samenameT2" name="samename" ng-value="true" ng-click='updateNameOnSsnCard();' ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator"  > <spring:message code="label.lce.yes"/>
                    </label>
                  <label class="radio-inline" for="samenameF2">
                    <input type="radio" id="samenameF2" name="samename" ng-value="false"  ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" > <spring:message code="label.lce.no"/>
                  </label>
                </div>
                <!-- Error -->
                <div class="margin30-l margin10-t">
                  <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.samename.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
                </div>
              </div><!-- Spouse has a SSN -->

              <!-- This displays when the spouses's name is not the same on the SSN -->
              <div class="ssnName margin30-t" ng-show="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false">

              <h4>
              <spring:message code="label.lce.enterthesamename"/><strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong><spring:message code="label.lce.sssncard"/>
              </h4>

                <div class="control-group">
                  <label class="control-label" for="firstNameOnSSNCard2"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                  <div class="controls">
                    <input type="text" id="firstNameOnSSNCard2" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" ng-model="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
                  </div>
                  <!-- Error -->
                  <div class="controls margin5-t">
                    <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.firstNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenterafirstname"/></span>
              <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.firstNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
                  </div>
                </div><!-- First Name -->
                <div class="control-group">
                  <label class="control-label" for="middleNameOnSSNCard2"><spring:message code="label.lce.middlename"/> </label>
                  <div class="controls">
                    <input type="text" id="middleNameOnSSNCard2" ng-pattern="/^[a-zA-Z]{1,45}$/" name="middleNameOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
                  </div>
                </div><!-- Middle Name -->
                <div class="control-group">
                  <label class="control-label" for="lastNameOnSSNCard2"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                  <div class="controls">
                    <input type="text" id="lastNameOnSSNCard2" name="lastNameOnSSNCard" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
                  </div>
                  <!-- Error -->
                  <div class="controls margin5-t">
                    <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.lastNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenteralastname"/></span>
              <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && (maritalStatusSSNForm.lastNameOnSSNCard.$error.pattern || maritalStatusSSNForm.lastNameOnSSNCard.$error.maxlength)"><spring:message code="label.lce.entervalidlastname"/></span>
                  </div>
                </div><!-- Last Name -->
                <div class="control-group">
                  <label class="control-label" for="suffixOnSSNCard2"><spring:message code="label.lce.suffix"/> </label>
                  <div class="controls">
                    <select class="span6" id="suffixOnSSNCard2" name="suffixOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.suffixOnSSNCard" ng-options="s for s in suffix" >
                      <option value=""><spring:message code="label.lce.suffix"/></option>
                    </select>
                  </div>
                </div><!-- Suffix -->
              </div><!-- SSN Name the Same -->
            </div><!-- SSN -->

          <h4 ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" class="margin30-t">
            <spring:message code="label.lce.ifnossn"/>
          </h4>
          <!-- This will display if spouse does not have a SSN -->
          <div class="noSSN" ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
            <div>
             <div>
				<label for="noSSNReason" class="aria-hidden">Select Explanation for SSN</label>
               <select id="noSSNReason" class="margin10-l" name="noSSNReason" ng-model="modalHouseholdMember.socialSecurityCard.reasonableExplanationForNoSSN" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
                 <!-- *NEW FIELD-->
                 <option value="">Select Explanation</option>
             	 <option value="RELIGIOUS_EXCEPTION"><spring:message code="label.lce.religiousexemption"/></option>
             	 <option value="JUST_APPLIED"><spring:message code="label.lce.justapplied"/></option>
             	 <option value="ILLNESS_EXCEPTION"><spring:message code="label.lce.illnessexemption"/></option>
             	 <option value="CITIZEN_EXCEPTION"><spring:message code="label.lce.citizenexemption"/></option>
               </select>
             <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
             </div>
             <!-- Error -->
             <div class="margin10-l margin10-t">
               <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.noSSNReason.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
             </div>
            </div>

            <div class="noSSN">
              <div class="control-group hidden">
                <span class="control-label"><spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.haveanindividualmandare"/></span>
                <div class="controls margin20-t">
                <label class="radio inline" for="exemptionT">
                  <input type="radio" id="exemptionT" name="exemption" value="true" class="exemptionYes" ng-model="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator"> <spring:message code="label.lce.yes"/>
                   <!-- ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" -->
                </label>
                  <label class="radio inline" for="exemptionF">
                    <input type="radio" id="exemptionF" name="exemption"   value="false" class="exemptionNo" ng-model="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator" "> <spring:message code="label.lce.no"/>
                       <!-- ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" -->
                  </label>
                </div><!-- controls -->

                <!-- Error -->
                <div class="controls margin5-t">
                  <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.exemption.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
                </div>
              </div><!-- control-group -->

                <div ng-show="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator=='true'">
                   <div class="control-group" >
                     <label class="control-label" for="individualManadateExceptionID"><spring:message code="label.lce.exemptionid"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                     <div class="controls">
                       <input type="text" class="span5" id="individualManadateExceptionID" name="individualManadateExceptionID" ng-model="modalHouseholdMember.socialSecurityCard.individualManadateExceptionID" ng-trim  placeholder='<spring:message code="label.lce.exemptionid"/>'></input>
                       <!--   ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false && modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator=='true'"       -->
                     </div>
                     <!-- Error -->
                     <div class="controls margin5-t">
                       <span class="validation-error-text-container" ng-show="formValidations.maritalStatusSSNForm && maritalStatusSSNForm.individualManadateExceptionID.$error.required"><spring:message code="label.lce.exemptioniderror"/></span>
                     </div>
                   </div>
                 </div>
              </div><!-- SSN -->
            </div>
          <!-- form-horizontal -->


        </form>
        <%--dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('ssn');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'ssn')" ng-if="maritalStatusSSNForm.$valid"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!maritalStatusSSNForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl--%>
      </div>
	</tab>
	
        <tab heading="<spring:message code="label.lce.change.ethnicity"/>" active="tabs.ethnicityRaceChangeForm">
            <div class="gutter20" ng-if="modalShownTab"  >
              <h4><spring:message code="label.lce.pleaseupdate"/> {{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}'s <spring:message code="label.lce.ethnicityandrace"/></h4>
				<form name="ethnicityRaceChangeForm" class="form-horizontal" novalidate>
                    <div class="control-group">
                        <label class="control-label" for="eventDateEthnicity">
                            <spring:message code="label.lce.eventdate"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                        </label>
                        <div class="controls">
                            <div  class="input-append" name="ethnicityRaceChangeDate" datetimez ng-model="eventInfoArrayTemp[originalIndex].ethnicityRaceChangeDate" check-after="0" >
                                <input data-format="MM/dd/yyyy" type="text" id="eventDateEthnicity" ng-model="eventInfoArrayTemp[originalIndex].ethnicityRaceChangeDate"  name="eventDateEthnicity" class="span12" required placeholder="MM/DD/YYYY"></input>
                                <span class="add-on">
                                <i class="icon-calendar"></i>
                                </span>
                            </div>
                            <div >
                                <span class="validation-error-text-container" ng-show="formValidations.ethnicityRaceChangeForm && ethnicityRaceChangeForm.eventDateEthnicity.$error.required">
                                    <spring:message code="label.lce.plsEnterEventDate"/>
                                </span>
								<span class="validation-error-text-container" ng-show="formValidations.ethnicityRaceChangeForm && ethnicityRaceChangeForm.ethnicityRaceChangeDate.$error.checkAfter">
                                    <spring:message code="label.lce.futureNotAllowed"/>
								</span>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-info margin20-t">
                        <p>
                            <spring:message code="label.lce.ethp1"/>
                        </p>
                    </div>
                    <div id="hispaniclatinospanish">
                        <div>
                            <spring:message code="label.lce.is"/>
                            <strong>{{modalHouseholdMember.name.firstName | capitalize:true}} {{modalHouseholdMember.name.middleName | capitalize:true}} {{modalHouseholdMember.name.lastName | capitalize:true}}</strong>
                            <spring:message code="label.lce.ofhispanic"/>
                        </div>
                        <div class="margin10-l">
                            <label class="radio-inline" for="ethnicityYes">
                                <input type="radio" id="ethnicityYes" name="ethnicityRadioBtn"  value="Yes" ng-value="true" ng-model="modalHouseholdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator"> 
                                <spring:message code="label.lce.yes"/>
                            </label>
                            <label class="radio-inline" for="ethnicityNo">
                                <input type="radio" id="ethnicityNo" name="ethnicityRadioBtn"  value="No" ng-value="false" ng-model="modalHouseholdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator" ng-click="uncheckAllEthnicities();"> 
                                <spring:message code="label.lce.no"/>
                            </label>
                        </div>
                        <div class="margin10-l">
                            <span class="validation-error-text-container" ng-if="submitted && ethnicityRaceChangeForm.ethnicityRadioBtn.$error.required">
                                <spring:message code="label.lce.selectvalidoption"/>
                            </span>
                        </div>
                    </div>
                    <!-- Is spouse Hispanic, Latino, or Spanish origin?  -->
                    <!-- Ethnicity -->
                    <div ng-if="modalHouseholdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator" id="ethnicity" class="margin30-t">
                        <div>
                            <spring:message code="label.lce.ethnicity"/>
                            <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                            <spring:message code="label.lce.checkallthatapply"/>
                        </div>
                        <div class="margin10-l" ng-repeat="ethnicity in ethnicities">
                            <label class="checkbox inline">
                            <input type="checkbox" id="ETHNICITY{{$index}}" name="ethincities" class="native" ng-checked="ethnicity.checked" ng-required="modalHouseholdMember.ethnicityAndRace.hispanicLatinoSpanishOriginIndicator && modalHouseholdMember.ethnicityAndRace.ethnicity.length==0" ng-click="updateEthnicity(ethnicity)" ng-model="ethnicity.value"  ng-true-value="{{ethnicity.label}}"/> {{ethnicity.label}}
                            </label>
                        </div>
                        <div class="margin10-l margin10-t" ng-if="ethnicities[3].value == 'Other'">
                            <label for="otherEthnicityText">
                            <span class="aria-hidden">otherEthnicityText text field</span>
                            <input type="text" id="otherEthnicityText" name="otherEthnicityText" ng-model="tempEthnicityHolder" ng-required="ethnicities[3].value == 'Other'" class="span4" placeholder="<spring:message code='label.lce.enterotherethnicity'/>" onchange="updateOtherEthnicity();">
                            </label>
                        </div>
                        <!-- Error -->
                        <div class="margin30-l margin10-t">
                            <div class="validation-error-text-container" ng-if="formValidations.ethnicityRaceChangeForm && ethnicityRaceChangeForm.ethincities.$error.required ">
                                <spring:message code="label.lce.thisvalueisrequired"/>
                            </div>
                            <div class="validation-error-text-container" ng-if="formValidations.ethnicityRaceChangeForm && ethnicityRaceChangeForm.otherEthnicityText.$error.required ">
                                <spring:message code="label.lce.error1"/>
                            </div>
                        </div>
                    </div>
                    <!--END Ethnicity -->
                    <!-- Race -->
                    <div id="race" class="margin30-t">
                        <div>Race (check all that apply)</div>
                        <div class="margin10-l" ng-repeat="race in races" >
                            <label class="checkbox inline">
                            <input type="checkbox"  name="{{$index}}"  id="RACE{{$index}}" ng-checked="race.checked"  ng-click="updateRace(race)" ng-model="race.value" ng-true-value="{{race.label}}"/> {{race.label}}
                            </label>
                        </div>
                        <div class="margin10-l margin10-t" ng-if="races[14].value == 'Other'">
                            <label for="otherRaceText">
                            <span class="aria-hidden">otherRaceText text field</span>
                            <input type="text" id="otherRaceText" ng-model="tempRaceHolder" name="otherRaceText" ng-required="races[14].value == 'Other'" onchange="updateOtherRace();" class="span4" placeholder="<spring:message code='label.lce.enterotherrace'/>">
                            </label>
                        </div>
                        <!-- Error -->
                        <div class="margin30-l margin10-t">
                            <div class="validation-error-text-container" ng-if="formValidations.ethnicityRaceChangeForm && ethnicityRaceChangeForm.otherRaceText.$error.required ">
                                <spring:message code='label.lce.error2'/>
                            </div>
                        </div>
                    </div>
                    <!-- Race -->
                </form>
            </div>
        </tab>
	</tabset>
		 <hr/>
		 <dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('tab');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="showModal(modalHouseholdMember,originalIndex,'relChange')" ng-if="dobchange.$valid && checkIf26YearOld(modalHouseholdMember) && modalHouseholdMember.applyingForCoverageIndicator "><spring:message code="label.lce.ok"/></a>
			<a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'tab')" ng-if="dobchange.$valid && ((!checkIf26YearOld(modalHouseholdMember)) || (!modalHouseholdMember.applyingForCoverageIndicator))"><spring:message code="label.lce.ok"/></a>
			
			<!--a class="btn btn-primary" ng-click="tabs[2].active=true;" ><spring:message code="label.lce.next"/>: <spring:message code="label.lce.dob2"/></a-->
            <a class="btn btn-primary" ng-click="$parent.submitted=true;modelOK(modalHouseholdMember,originalIndex,'tab');" >Save & close</a>
		</dl>
    </modal-dialog-static-backdrop>

    <!-- NAME MODAL  -->
    <%--modal-dialog-static-backdrop show='modalShown' ng-model="modalHouseholdMember" modal-event-type="modalEventType">
      <div class="gutter20">
       <h4><spring:message code="label.lce.pleaseupdate"/> {{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}<spring:message code="label.lce.sname"/></h4>
		<form class="form-horizontal" name="namechangeform">
        <div class="control-group">
            <label class="control-label" for="eventDateNameChange"><spring:message code="label.lce.eventdate"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="nameChangeCalenderDiv" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateNameChange" ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate" name="eventDateNameChange" class="span8"  required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>

            </div>
			<div class="controls ">
  	      		<span class="validation-error-text-container" ng-show="submitted && namechangeform.eventDateNameChange.$error.pattern"><spring:message code="label.lce.plsEnterValidDate"/></span>
   		  	</div>
			<div class="controls ">
  	      		<span class="validation-error-text-container" ng-show="submitted && namechangeform.eventDateNameChange.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
   		  	</div>
			
          </div>


          <div class="control-group">
            <label class="control-label" for="firstName"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <input type="text" id="firstName" name= "firstName" required ng-model="modalHouseholdMember.name.firstName" class="input-large span6" ng-pattern='/^[a-zA-Z]{1,45}$/'>
            </div>
            <div class="controls">
              <span class="validation-error-text-container" ng-show="submitted && namechangeform.firstName.$error.required"><spring:message code="label.lce.plsEnter1stName"/></span>
            </div>
			       <div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="submitted && namechangeform.firstName.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
        	   </div>
          </div>

          <div class="controls-group">
            <label class="control-label" for="middleName"><spring:message code="label.lce.middlename"/></label>
            <div class="controls">
              <input type="text" id="middleName" name="middleName" ng-pattern='/^[a-zA-Z]{1,45}$/' ng-model="modalHouseholdMember.name.middleName" class="input-large span6" >
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="submitted && namechangeform.middleName.$error.pattern"><spring:message code="label.lce.entervalidmiddlename"/></span>
        	</div>
          </div>

          <div class="controls-group margin20-t">
            <label class="control-label" for="lastName"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <input type="text" id="lastName" name="lastName" required ng-model="modalHouseholdMember.name.lastName" class="input-large span6" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" >
            </div>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="submitted && namechangeform.lastName.$error.required" ><spring:message code="label.lce.plsEnterLastame"/></span>
            </div>
			<div class="controls margin5-t">
          		<span class="validation-error-text-container" ng-show="submitted && (namechangeform.lastName.$error.pattern || namechangeform.lastName.$error.maxlength)"><spring:message code="label.lce.entervalidlastname"/></span>
        	</div>
          </div>

          <div class="controls-group margin20-t">
            <label class="control-label" for="suffix"><spring:message code="label.lce.suffix"/> </label>
            <div class="controls">
              <select class="span6" id="suffix" name="suffix" ng-model="modalHouseholdMember.name.suffix" ng-options="s for s in suffix">
                <option value=""><spring:message code="label.lce.suffix"/></option>
			  </select>
            </div>
          </div>

          <div class="control-group margin30-t" ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator">
            <div id="isthenamethesameasssn">
              <h4>
                <spring:message code="label.lce.is"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.sameName"/>
                <div ng-switch on="modalHouseholdMember.gender" class="ssngGender">
                  <div ng-switch-when="male"> <spring:message code="label.lce.paragraphhis"/> </div>
                  <div ng-switch-when="female"> <spring:message code="label.lce.paragraphher"/> </div>
                  <div ng-switch-default></div>
                  Social Security Card? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
                </div>
              </h4>
            </div>
            <div class="margin10-l">
              <label class="radio-inline" for="samenameT">
                <input type="radio" id="samenameT" name="samename" ng-value="true"  ng-click='updateNameOnSSNCard(modalHouseholdMember);' ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" /> <spring:message code="label.lce.yes"/>
                </label>
              <label class="radio-inline" for="samenameF">
                <input type="radio" id="samenameF" name="samename" ng-value="false"  ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-click="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard='';modalHouseholdMember.socialSecurityCard.suffixOnSSNCard='';" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" > <spring:message code="label.lce.no"/>
              </label>
            </div>
            <!-- Error -->
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="submitted && namechangeform.samename.$error.required"><spring:message code="label.lce.pleaseselectif"/> {{householdMember.name.firstName}}<spring:message code="label.lce.nameisthesameonssn"/></span>
            </div>
          </div><!-- Spouse has a SSN -->

          <!-- This displays when the spouses's name is not the same on the SSN -->
          <div class="ssnName" ng-show="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false">
            <div class="control-group">
              <label class="control-label" for="firstNameOnSSNCard"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
              <div class="controls">
                <input type="text" id="firstNameOnSSNCard" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" required ng-model="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
              </div>
              <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="submitted && namechangeform.firstNameOnSSNCard.$error.required"><spring:message code="label.lce.plsEnter1stName"/></span>
				<span class="validation-error-text-container" ng-show="submitted && namechangeform.firstNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
              </div>
            </div><!-- First Name -->
            <div class="control-group">
              <label class="control-label" for="middleNameOnSSNCard"><spring:message code="label.lce.middlename"/> </label>
              <div class="controls">
                <input type="text" id="middleNameOnSSNCard" ng-pattern='/^[a-zA-Z]{1,45}$/' name="middleNameOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
              </div>
			  <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="submitted && namechangeform.middleNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidmiddlename"/></span>
              </div>
            </div><!-- Middle Name -->
            <div class="control-group">
              <label class="control-label" for="lastNameOnSSNCard"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
              <div class="controls">
                <input type="text" id="lastNameOnSSNCard" name="lastNameOnSSNCard" required ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
              </div>
              <!-- Error -->
              <div class="controls margin5-t">
                <span class="validation-error-text-container" ng-show="submitted && namechangeform.lastNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenteralastname"/></span>
				<span class="validation-error-text-container" ng-show="submitted && namechangeform.lastNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidlastname"/></span>
               </div>
            </div><!-- Last Name -->
            <div class="control-group">
              <label class="control-label" for="suffixOnSSNCard"><spring:message code="label.lce.suffix"/> </label>
              <div class="controls">
                <select class="span6" id="suffixOnSSNCard" name="suffixOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.suffixOnSSNCard" ng-options="s for s in suffix" >
                  <option value=""><spring:message code="label.lce.suffix"/></option>
                </select>
              </div>
            </div><!-- Suffix -->
          </div><!-- SSN Name the Same -->
        </form>
        <dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('name');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'name')" ng-if="namechangeform.$valid"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!namechangeform.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl>
      </div>
    </modal-dialog-static-backdrop>

    <!-- DOB MODAL  -->
    <modal-dialog-static-backdrop show='modalShownDOB' ng-model="modalHouseholdMember" ng-if="modalShownDOB" modal-event-type="modalEventType">
      <div class="gutter20">
       <h4><spring:message code="label.lce.pleaseupdate"/> <strong>{{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}'s</strong> <spring:message code="label.lce.dob"/></h4>
       	 <form class="form-horizontal" name="dobchange">
          <div class="control-group">
            <label class="control-label" for="eventDateDOB"><spring:message code="label.lce.eventdateReport"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="dobChangeCalenderDiv" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateDOB" ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate" name="eventDateDOB" class="span12" required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>
       			<div >
              		<span class="validation-error-text-container" ng-show="submitted && dobchange.eventDateDOB.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
            	</div>
            </div>
          </div>


          <!-- Date of Birth -->
          <div class="control-group">
            <label class="control-label" for="dateOfBirth"><spring:message code="label.lce.dob"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">
              <div id="date" class="input-append" name="dateOfBirthDiv" ng-model="modalHouseholdMember.dateOfBirth" datetimez  required >
            	<input type="text" id="dateOfBirth" name="dateOfBirth" date-formatter date="modalHouseholdMember.dateOfBirth" class="span12"  placeholder="MM/DD/YYYY"></input>
            	<span class="add-on">
              		<i class="icon-calendar"></i>
            	</span>
          	  </div>

             </div><!-- Controls -->
            <!-- Error -->
            <label class="control-label"></label>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="submitted && dobchange.dateOfBirthDiv.$error.pattern "><spring:message code="label.lce.entervalidedob"/></span>
            </div>
            <div class="controls margin5-t">
              <span class="validation-error-text-container" ng-show="submitted && dobchange.dateOfBirthDiv.$error.required"><spring:message code="label.lce.pleaseenteradob"/></span>
            </div><!-- Controls -->
          </div><!-- Date of Birth -->
        </form>
        <dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('dob');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="showModal(modalHouseholdMember,originalIndex,'relChange')" ng-if="dobchange.$valid && checkIf26YearOld(modalHouseholdMember) && modalHouseholdMember.applyingForCoverageIndicator "><spring:message code="label.lce.ok"/></a>
			<a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'dob')" ng-if="dobchange.$valid && ((!checkIf26YearOld(modalHouseholdMember)) || (!modalHouseholdMember.applyingForCoverageIndicator))"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!dobchange.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl>
      </div>
    </modal-dialog-static-backdrop>

    <!-- SSN MODAL  -->
    <modal-dialog-static-backdrop show='modalShownSSN' ng-model="modalHouseholdMember" modal-event-type="modalEventType">
      <div class="gutter20">
       <h4><spring:message code="label.lce.pleaseupdate"/> <strong>{{modalHouseholdMember.name.firstName}} {{ modalHouseholdMember.name.lastName}}'s</strong> <spring:message code="label.lce.ssn"/></h4>
          <form class="form-horizontal" id="maritalStatusSSNForm" name="maritalStatusSSNForm" novalidate>
          <div class="control-group">
            <label class="control-label" for="eventDateSSN"><spring:message code="label.lce.dateReportChange"/><img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
            <div class="controls">

                <div id="ssnChangeCalenderDiv" class="input-append" datetimez ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate">
                  <input data-format="MM/dd/yyyy" type="text" id="eventDateSSN" ng-model="eventInfoArrayTemp[originalIndex].nameChangeDate"  name="eventDateSSN" class="span12" required placeholder="MM/DD/YYYY"></input>
                  <span class="add-on">
                    <i class="icon-calendar"></i>
                  </span>
                </div>
             	<div >
              		<span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.eventDateSSN.$error.required"><spring:message code="label.lce.plsEnterEventDate"/></span>
            	</div>

            </div>
          </div>




            <div>
              <label class="alert alert-info" for="hidDummyAlertField">
                <spring:message code="label.lce.ssn.p1"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong>
                <spring:message code="label.lce.ssn.p2"/>
                <spring:message code="label.lce.ssn.p3"/>
                <input type="hidden" id="hidDummyAlertField" />
              </label>
            </div>

            <!-- Spouse have a SSN? -->
            <div id="ssnNumber">
              <span>
                <spring:message code="label.lce.plsEnter"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong><spring:message code="label.lce.apoS"/> <spring:message code="label.lce.ssn"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
              </span>
              <!--<div class="margin10-l">
                <label class="radio-inline" for="snn4spouseT">
                  <input type="radio" id="snn4spouseT" name="snn4spouse" required ng-value="true" value="true" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-click="modalHouseholdMember.socialSecurityCard.reasonableExplanationForNoSSN='';"> <spring:message code="label.lce.yes"/>
                </label>
                <label class="radio-inline" for="snn4spouseF">
                  <input type="radio" id="snn4spouseF" name="snn4spouse" ng-click='removeSSN(modalHouseholdMember);' required ng-value="false" value="false" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator"  > <spring:message code="label.lce.no"/>
                </label>
              </div> controls -->

            <!-- Error -->
            <div class="margin30-l margin10-t">
              <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.snn4spouse.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
            </div>

            </div><!-- Spouse have a SSN? -->

            <!-- Spouse has an SSN-->
            <div class="row-fluid control-group ssnum margin10-t">
              <div class="control-group">
                <label class="control-label" for="socialSecurityNumber"><spring:message code="label.lce.ssn"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                <div class="controls">
                 <input type="text" id="socialSecurityNumber" name="socialSecurityNumber" ng-model="modalHouseholdMember.socialSecurityCard.socialSecurityNumber" style="display:none" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" ng-pattern="/^\d{3}\d{2}\d{4}$/" ng-minlength="9" ng-maxlength="9" />
					<label for="ssn1" class="aria-hidden">SSN 1</label>
					<label for="ssn2" class="aria-hidden">SSN 2</label>
					<label for="ssn3" class="aria-hidden">SSN 3</label>
                  <input type="text" placeholder="XXX" class="span2" id="ssn1" name="ssn1"  minlength="3" maxlength="3" ng-model="ssn1"  onkeyup="ssnMask('ssn1')">
                  <input type="text" placeholder="XX" class="span2" id="ssn2" name="ssn2"   minlength="2" maxlength="2" ng-model="ssn2"  onkeyup="ssnMask('ssn2')">
                  <input type="text" placeholder="XXXX" class="span2" id="ssn3" name="ssn3" minlength="4" maxlength="4" ng-model="ssn3"  onkeyup="ssnMask('ssn3')">
                </div>
                <!-- Error -->
                <div class="controls margin10-t">
                  <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.socialSecurityNumber.$error.required"><spring:message code="label.lce.pleaseenterssn"/></span>
                  <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.socialSecurityNumber.$error.pattern "><spring:message code="label.lce.pleaseentervalidssn"/></span>
                </div>
              </div>

              <div id="isthenamethesameasssn2">
                <span><spring:message code="label.lce.is"/> <strong>{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.thesamenamethatappearsonssn"/>
                <div ng-switch on="modalHouseholdMember.gender" class="ssngGender">
                  <div ng-switch-when="male"> <spring:message code="label.lce.paragraphhis"/> </div>
                  <div ng-switch-when="female"> <spring:message code="label.lce.paragraphher"/> </div>
                  <div ng-switch-default></div>
               </div>
                Social Security Card? <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></span>
                <div class="margin10-l">
                  <label class="radio-inline" for="samenameT2">
                    <input type="radio" id="samenameT2" name="samename" ng-value="true" ng-click='updateNameOnSsnCard();' ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator"  > <spring:message code="label.lce.yes"/>
                    </label>
                  <label class="radio-inline" for="samenameF2">
                    <input type="radio" id="samenameF2" name="samename" ng-value="false"  ng-model="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator" > <spring:message code="label.lce.no"/>
                  </label>
                </div>
                <!-- Error -->
                <div class="margin30-l margin10-t">
                  <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.samename.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
                </div>
              </div><!-- Spouse has a SSN -->

              <!-- This displays when the spouses's name is not the same on the SSN -->
              <div class="ssnName margin30-t" ng-show="modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false">

              <h4>
              <spring:message code="label.lce.enterthesamename"/><strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong><spring:message code="label.lce.sssncard"/>
              </h4>

                <div class="control-group">
                  <label class="control-label" for="firstNameOnSSNCard2"><spring:message code="label.lce.firstname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                  <div class="controls">
                    <input type="text" id="firstNameOnSSNCard2" name= "firstNameOnSSNCard" ng-pattern="/^[a-zA-Z]{1,45}$/" ng-model="modalHouseholdMember.socialSecurityCard.firstNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.firstname"/>'>
                  </div>
                  <!-- Error -->
                  <div class="controls margin5-t">
                    <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.firstNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenterafirstname"/></span>
              <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.firstNameOnSSNCard.$error.pattern"><spring:message code="label.lce.entervalidfirstname"/></span>
                  </div>
                </div><!-- First Name -->
                <div class="control-group">
                  <label class="control-label" for="middleNameOnSSNCard2"><spring:message code="label.lce.middlename"/> </label>
                  <div class="controls">
                    <input type="text" id="middleNameOnSSNCard2" ng-pattern="/^[a-zA-Z]{1,45}$/" name="middleNameOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.middleNameOnSSNCard" class="input-large span6" placeholder='<spring:message code="label.lce.middlename"/>'>
                  </div>
                </div><!-- Middle Name -->
                <div class="control-group">
                  <label class="control-label" for="lastNameOnSSNCard2"><spring:message code="label.lce.lastname"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                  <div class="controls">
                    <input type="text" id="lastNameOnSSNCard2" name="lastNameOnSSNCard" ng-maxlength="45" ng-pattern="/^(([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z]))+((\s|-)?([A-Za-z]|([O|L|D|o|l|d]'[A-Za-z])))*)$/" ng-model="modalHouseholdMember.socialSecurityCard.lastNameOnSSNCard" class="input-large span6" ng-trim ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator && modalHouseholdMember.socialSecurityCard.nameSameOnSSNCardIndicator==false" placeholder='<spring:message code="label.lce.lastname"/>'>
                  </div>
                  <!-- Error -->
                  <div class="controls margin5-t">
                    <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.lastNameOnSSNCard.$error.required"><spring:message code="label.lce.pleaseenteralastname"/></span>
              <span class="validation-error-text-container" ng-show="submitted && (maritalStatusSSNForm.lastNameOnSSNCard.$error.pattern || maritalStatusSSNForm.lastNameOnSSNCard.$error.maxlength)"><spring:message code="label.lce.entervalidlastname"/></span>
                  </div>
                </div><!-- Last Name -->
                <div class="control-group">
                  <label class="control-label" for="suffixOnSSNCard2"><spring:message code="label.lce.suffix"/> </label>
                  <div class="controls">
                    <select class="span6" id="suffixOnSSNCard2" name="suffixOnSSNCard" ng-model="modalHouseholdMember.socialSecurityCard.suffixOnSSNCard" ng-options="s for s in suffix" >
                      <option value=""><spring:message code="label.lce.suffix"/></option>
                    </select>
                  </div>
                </div><!-- Suffix -->
              </div><!-- SSN Name the Same -->
            </div><!-- SSN -->

          <h4 ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" class="margin30-t">
            <spring:message code="label.lce.ifnossn"/>
          </h4>
          <!-- This will display if spouse does not have a SSN -->
          <div class="noSSN" ng-show="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
            <div>
             <div>
				<label for="noSSNReason" class="aria-hidden">Select Explanation for SSN</label>
               <select id="noSSNReason" class="margin10-l" name="noSSNReason" ng-model="modalHouseholdMember.socialSecurityCard.reasonableExplanationForNoSSN" ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false">
                 <!-- *NEW FIELD-->
                 <option value="">Select Explanation</option>
             	 <option value="RELIGIOUS_EXCEPTION"><spring:message code="label.lce.religiousexemption"/></option>
             	 <option value="JUST_APPLIED"><spring:message code="label.lce.justapplied"/></option>
             	 <option value="ILLNESS_EXCEPTION"><spring:message code="label.lce.illnessexemption"/></option>
             	 <option value="CITIZEN_EXCEPTION"><spring:message code="label.lce.citizenexemption"/></option>
               </select>
             <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true">
             </div>
             <!-- Error -->
             <div class="margin10-l margin10-t">
               <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.noSSNReason.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
             </div>
            </div>

            <div class="noSSN">
              <div class="control-group hidden">
                <span class="control-label"><spring:message code="label.lce.does"/> <strong class="nameOfHouseHold">{{modalHouseholdMember.name.firstName}} {{modalHouseholdMember.name.middleName}} {{modalHouseholdMember.name.lastName}}</strong> <spring:message code="label.lce.haveanindividualmandare"/></span>
                <div class="controls margin20-t">
                <label class="radio inline" for="exemptionT">
                  <input type="radio" id="exemptionT" name="exemption" value="true" class="exemptionYes" ng-model="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator"> <spring:message code="label.lce.yes"/>
                   <!-- ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" -->
                </label>
                  <label class="radio inline" for="exemptionF">
                    <input type="radio" id="exemptionF" name="exemption"   value="false" class="exemptionNo" ng-model="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator" "> <spring:message code="label.lce.no"/>
                       <!-- ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false" -->
                  </label>
                </div><!-- controls -->

                <!-- Error -->
                <div class="controls margin5-t">
                  <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.exemption.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
                </div>
              </div><!-- control-group -->

                <div ng-show="modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator=='true'">
                   <div class="control-group" >
                     <label class="control-label" for="individualManadateExceptionID"><spring:message code="label.lce.exemptionid"/> <img src="../../resources/img/requiredAsterix.png" width="10" height="10" alt="Required!" aria-hidden="true"></label>
                     <div class="controls">
                       <input type="text" class="span5" id="individualManadateExceptionID" name="individualManadateExceptionID" ng-model="modalHouseholdMember.socialSecurityCard.individualManadateExceptionID" ng-trim  placeholder='<spring:message code="label.lce.exemptionid"/>'></input>
                       <!--   ng-required="modalHouseholdMember.socialSecurityCard.socialSecurityCardHolderIndicator==false && modalHouseholdMember.socialSecurityCard.individualMandateExceptionIndicator=='true'"       -->
                     </div>
                     <!-- Error -->
                     <div class="controls margin5-t">
                       <span class="validation-error-text-container" ng-show="submitted && maritalStatusSSNForm.individualManadateExceptionID.$error.required"><spring:message code="label.lce.exemptioniderror"/></span>
                     </div>
                   </div>
                 </div>
              </div><!-- SSN -->
            </div>
          <!-- form-horizontal -->


        </form>
        <dl class="dl-horizontal pull-right">
            <a  class="btn btn-secondary" ng-click="hideModal('ssn');"><spring:message code="lce.cancelbutton"/></a>
            <a  class="btn btn-primary" ng-click="modelOK(modalHouseholdMember,originalIndex,'ssn')" ng-if="maritalStatusSSNForm.$valid"><spring:message code="label.lce.ok"/></a>
            <a ng-if="!maritalStatusSSNForm.$valid" class="btn btn-primary" ng-click="$parent.submitted=true;" ><spring:message code="label.lce.ok"/></a>
        </dl>
      </div>
    </modal-dialog-static-backdrop--%>

    <dl class="dl-horizontal pull-right">
      <a class="btn btn-secondary" ng-click="back()" ng-if="!(lifeEventCounter >= 0 || counter >= 1)"><spring:message code="label.lce.back"/></a>
      <a class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
		<%--a class="btn btn-primary" ng-click="$parent.demographicSubmitted = true;"><spring:message code="label.lce.next"/></a--%>
    </dl>

	<modal-Tiny show='showWarningOnChange' ng-model="modalHouseholdMember">
           <div class="gutter20">
                   <div>
						<spring:message code="label.lce.changeEvent"/> <strong>{{previousEventName}}</strong> <spring:message code="label.lce.to"/>  <strong>{{currentEventName}}</strong>. <spring:message code="label.lce.yourChangesFor"/> <strong>{{previousEventName}}</strong> <spring:message code="label.lce.revertToOriginal"/>
                   </div>
				  <div class="pull-right">
                            <button class="btn btn-secondary" ng-click="hideModal('warningOnChange')">
                                <spring:message code="label.lce.ok"/>
                            </button>
    			  </div>
		</div>	
   </modal-Tiny>

	<modal-dialog-static-backdrop show='showRelChangeRadio' ng-model="modalHouseholdMember">
           <div class="alert" ><!-- Message for more than 26 year old dependent -->
         		<p>
                  <spring:message code="label.lce.dependentOverAge1"/>
           		</p>
                <p>
                  <spring:message code="label.lce.dependentOverAge2"/>
                </p>
                <p class="alert alert-info">
                  <spring:message code="label.lce.dependentOverAge3"/>
                </p>
			</div>
			<form role="form" name="relSelectForm" class="form-horizontal" >
				<div class="radio">
					<label class="radio-inline"><spring:message code="label.lce.keepRelation"/>'{{currentUserRelation}}'
						<input id="keepSame"  name="changeRelation" type="radio" value="keepSame" ng-model="$parent.relationAction" required  />
					</label>
				</div>
				<div class="radio">
					<label class="radio-inline"><spring:message code="label.lce.changeToParent"/>
						<input id="parentWard" name="changeRelation" type="radio" value="parentWard" ng-model="$parent.relationAction" required  />
					</label>
				</div>
				<div class="radio">
					<label class="radio-inline"><spring:message code="label.lce.changeToGuardian"/> 
						<input id="CourtAppointed" name="changeRelation" type="radio" value="CourtAppointed" ng-model="$parent.relationAction" required />
					</label>
				</div>
				<div class="margin10-l margin10-t">
               			<span class="validation-error-text-container" ng-show="submitted && relSelectForm.relationAction.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
             	</div>
				<div class="controls">
					<button class="btn btn-primary" ng-if="relSelectForm.$valid" ng-click="changeRelation($parent.relationAction,modalHouseholdMember)"><spring:message code="label.lce.ok"/></button>
					<button class="btn btn-primary" ng-if="!relSelectForm.$valid" ng-click="submitted=true;"><spring:message code="label.lce.ok"/></button>
				</div>
			</form>	
   </modal-dialog-static-backdrop>
	<modal-dialog-static-backdrop show='showRelChangeSelect' ng-model="modalHouseholdMember">
           <div class="alert" ><!-- Message for more than 26 year old dependent -->
         		<p>
                  <spring:message code="label.lce.dependentOverAge1"/>
           		</p>
                <p>
                  <spring:message code="label.lce.dependentOverAge2"/>
                </p>
                <p class="alert alert-info">
                  <spring:message code="label.lce.dependentOverAge3"/>
                </p>
		 	</div>
			<form role="form" name="relSelectForm"  class="form-horizontal" >
				<div class="control-group">
					<label class="control-label"> </label>
					<div class="controls">
						<select name="relationAction" ng-model="$parent.relationAction" required>
							<option value=''> <spring:message code="label.lce.selectRelation"/></option>
							<option value="keepSame" > {{currentUserRelation}} </option>
							<option value="parentWard" ><spring:message code="label.lce.parentOrCaretaker"/></option>
							<option value="CourtAppointed" ><spring:message code="label.lce.appointedGuardian"/></option>
						</select>
					</div>
					<div class="margin10-l margin10-t">
               			<span class="validation-error-text-container" ng-show="submitted && relSelectForm.relationAction.$error.required"><spring:message code="label.lce.thisvalueisrequired"/></span>
             		</div>
				</div>
				<div class="controls">
					<button class="btn btn-primary" ng-if="!relSelectForm.$valid" ng-click="$parent.submitted=true;"><spring:message code="label.lce.ok"/></button>
					<button class="btn btn-primary" ng-if="relSelectForm.$valid" ng-click="changeRelation($parent.relationAction,modalHouseholdMember)"><spring:message code="label.lce.ok"/></button>
				</div>
			</form>	
   </modal-dialog-static-backdrop>

  </div>



	
</script>
<%-- script>
var hiddenSsn1="";
var hiddenSsn2="";
var hiddenSsn3="";
var ssnMask = function(ssnID) {
	$("input[maxlength=3],input[maxlength=4],input[maxlength=2]").bind("keyup",function(event){
	    var keyCode = event.keyCode;
	    if( keyCode == 16 || keyCode == 9 || keyCode == 37 || keyCode == 38 || keyCode == 39 || keyCode == 40){
	      return;
	    }
	    var maxLength = parseInt($(this).attr('maxlength'));
	    var nextElement = $(this).nextAll( "input[maxlength]:first");
	    if($(this).val().length == maxLength && nextElement !== undefined){
	      nextElement.focus();
	      nextElement.select();
	    }
	});
	if ( $('#ssn1').val().split("").length === 3 && 'ssn1' === ssnID) {
		if($('#ssn1').val().indexOf('***') == -1){
			hiddenSsn1=$('#ssn1').val();
		}
		$('#ssn1').val("***")
	}
	if ( $('#ssn2').val().split("").length === 2 && 'ssn2' === ssnID) {
		if($('#ssn2').val().indexOf('**') == -1){
			hiddenSsn2=$('#ssn2').val();
		}
		$('#ssn2').val("**")
	}
	if( 'ssn3' === ssnID){
		hiddenSsn3=$('#ssn3').val();
	}
	$('#socialSecurityNumber').val(hiddenSsn1+hiddenSsn2+hiddenSsn3).change();
	$('#socialSecurityNumber').triggerHandler("change");

};

var updateNameOnSsnCard = function(){
	//$sope.householdMember.
	var scope = angular.element($("#firstNameOnSSNCard")).scope();
	$('#firstNameOnSSNCard').val(scope.householdMember.name.firstName).change();
	$('#firstNameOnSSNCard').triggerHandler("change");

	$('#middleNameOnSSNCard').val(scope.householdMember.name.middleName).change();
	$('#middleNameOnSSNCard').triggerHandler("change");

	$('#lastNameOnSSNCard').val(scope.householdMember.name.lastName).change();
	$('#lastNameOnSSNCard').triggerHandler("change");

	$('#suffixOnSSNCard').val(scope.householdMember.name.suffix).change();
	$('#suffixOnSSNCard').triggerHandler("change");

};

var removeSSN = function(){
	$('#socialSecurityNumber').val('').change();
	$('#socialSecurityNumber').triggerHandler("change");
	$('#ssn1').val('').change();
	$('#ssn1').triggerHandler("change");
	$('#ssn2').val('').change();
	$('#ssn2').triggerHandler("change");
	$('#ssn3').val('').change();
	$('#ssn3').triggerHandler("change");

	$('#firstNameOnSSNCard').val('').change();
	$('#firstNameOnSSNCard').triggerHandler("change");

	$('#middleNameOnSSNCard').val('').change();
	$('#middleNameOnSSNCard').triggerHandler("change");

	$('#lastNameOnSSNCard').val('').change();
	$('#lastNameOnSSNCard').triggerHandler("change");

	$('#suffixOnSSNCard').val('').change();
	$('#suffixOnSSNCard').triggerHandler("change");
};
</script>--%>
<script>
  $('a.btn-primary').attr('tabindex', '0');
  var updateOtherEthnicity = function() {
    var scope = angular.element($("#ethnicityYes")).scope();
    var loopVar=0;
     for(loopVar=0  ; loopVar <  scope.selectedEthnicities.length ; loopVar++){
       if(scope.selectedEthnicities[loopVar].code === '000-0'){
         scope.selectedEthnicities[loopVar].label=$('#otherEthnicityText').val();
       }
     }
  };
  var updateOtherRace= function() {
    var scope = angular.element($("#ethnicityYes")).scope();
    var loopVar=0;
     for(loopVar=0  ; loopVar <  scope.selectedraces.length ; loopVar++){
       if(scope.selectedraces[loopVar].code === '000-0'){
         scope.selectedraces[loopVar].label=$('#otherRaceText').val();
       }
     }
  };
</script>
