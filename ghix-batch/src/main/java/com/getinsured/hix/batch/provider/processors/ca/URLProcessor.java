package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class URLProcessor implements ProviderDataFieldProcessor {
    private ValidationContext context;
    private int index;
    private Logger logger = LoggerFactory.getLogger(EmailFormatProcessor.class);

    @Override
    public void setIndex(int idx) {
        this.index = idx;
    }

    @Override
    public int getIndex() {
        return this.index;
    }

    @Override
    public void setValidationContext(ValidationContext validationContext) {
        this.context = validationContext;
    }

    @Override
    public ValidationContext getValidationContext() {
        return this.context;
    }

    @Override
    public Object process(Object objToBeValidated) throws ProviderValidationException, InvalidOperationException {
        String maxObj = (String) context.getContextField("max");
        if (maxObj == null) {
            throw new ProviderValidationException("Max attribute required. Please check your metadata file.");
        }
        int max = Integer.parseInt(maxObj);
        String url = (String) objToBeValidated;
        if (url.length() > max) {
            throw new ProviderValidationException("Email field is longer than expected.");
        }

        Pattern pattern = Pattern.compile("^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");
        Matcher matcher =  pattern.matcher(url);
        if (!matcher.matches()) {
            throw new ProviderValidationException("URL validation failed. Pattern mismatch.");
        }
        return url;
    }
}
