package com.getinsured.iex.ssap.service;

import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS2;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS3;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS4;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS5;
import static com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing.CS6;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.TimeShifterUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.model.SepEvents.ChangeType;
import com.getinsured.eligibility.plan.service.ApplicantPlanDto;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutDependant;
import com.getinsured.eligibility.ssap.integration.notices.AgeOutNoticeDTO;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto;
import com.getinsured.hix.dto.enrollment.EnrolleeCoreDto.ENROLLEE_ACTION;
import com.getinsured.hix.dto.enrollment.EnrollmentCoreDto;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentUpdateRequest;
import com.getinsured.hix.dto.enrollment.LocationCoreDto;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemPersonResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdOrderItemResponse;
import com.getinsured.hix.dto.plandisplay.planavailability.PdPersonRequest;
import com.getinsured.hix.dto.plandisplay.planavailability.PdRequestPlanAvailability;
import com.getinsured.hix.dto.plandisplay.planavailability.PdResponsePlanAvailability;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.CostSharing;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.Relationship;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.ShoppingType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.PreferencesService;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.EthnicityAndRace;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.Race;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.enums.LanguageEnum;
import com.getinsured.iex.ssap.enums.MaritalStatusEnum;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.model.SsapIntegrationLog;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.repository.SsapIntegrationLogRepository;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Service
public class SsapAutoEnrollServiceImpl implements SsapAutoEnrollService{
	
	private static final String AGE_OUT_DISENROLL_NOTIFICATION = "AgeOutDisenrollNotification";
	private static final String DATE_FORMAT_MMMM_DD_YYYY = "MMMM dd, YYYY";
	private static final String DATE_FORMAT_MMMM_D_YYYY = "MMMM d, YYYY";
	private static final int PRIMARY_APPLICANT_PERSON_ID = 1;
	private static final String ELIGIBLE_DENTAL_ENROLLEE = "eligibleDentalEnrollee";
	private static final String ELIGIBLE_HEALTH_ENROLLEE = "eligibleHealthEnrollee";
	private static final String SUCCESS_RESPONSE_CODE = "200";
	private static final String RESPONSE = "Response - ";
	private static final String REQUEST = "Request - ";
	private static final String EXCEPTION = "Exception - ";
	private static final String DENTAL_ENROLLEE = "dentalEnrollee";
	private static final String HEALTH_ENROLLEE = "healthEnrollee";
	private static List<String> CHILD_RELATIONSHIP_CODES = Arrays.asList("05", "07", "09", "10", "17", "19");
	private static List<String> SPOUSE_RELATIONSHIP_CODES = Arrays.asList("01");
	private static final String PLAN_AVAILABILITY = "PLAN_AVAILABILITY";
	private static List<String> SELF_RELATIONSHIP_CODES = Arrays.asList("18");
	private static final String HEALTH_ENROLLMENT_DTO = "HealthEnrollmentDTO";
	private static final String DENTAL_ENROLLMENT_DTO = "DentalEnrollmentDTO";
	private static final String NEW_LINE = "\n";
	private static final String DISENROLL_FROM_DENTAL = "disenrollFromDental";
	private static final String DISENROLL_FROM_HEALTH = "disenrollFromHealth";
	private static final String DLT_NOT_RESPONSE_CODE = "104";
	private static final String HLT_NOT_RESPONSE_CODE = "103";
	private static final String HLT_DLT_NOT_RESPONSE_CODE = "102";
	private static final String IN_ACTIVE_HEALTH_ENROLLEE = "inActiveHealthEnrollee";
	private static final String IN_ACTIVE_DENTAL_ENROLLEE = "inActiveDentalEnrollee";
	private static final String SEPARATOR = " - ";
	private static final String PLAN_AVAILABILITY_RETURNED_ERROR_CODE = "planAvailability returned error code - ";
	private static final String AUTO_ENROLLMENT_UPDATE = "AUTO_ENROLLMENT_UPDATE";
	private static final String DATE_FORMAT_MM_DD_YYYY = "MM/dd/yyyy";
	
	private static final Logger LOGGER = Logger.getLogger(SsapAutoEnrollServiceImpl.class);
	private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);

	
	@Autowired
	CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired
	GhixRestTemplate ghixRestTemplate;
	@Autowired
	private SsapJsonBuilder ssapJsonBuilder;
	@Autowired
	BenchmarkPlanService benchmarkPlanService;
	@Autowired
	SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	SsapIntegrationLogRepository ssapIntegrationLogRepository;
	@Autowired
	SsapService ssapService;
	@Autowired
	NoticeService noticeService;
	@Autowired
	ILocationRepository iLocationRepository;
	@Autowired
	PreferencesService preferencesService;
	@Autowired
	CoverageStartDateService coverageStartDateService;
	
	@Override
	public String autoEnroll(long ssapApplicationId,Map<String, EnrollmentShopDTO> enrollmentIdMap,Map<String,Set<String>> enrollee,Map<String,Set<String>> disenrollMap){
		try{
			//load ssapApplication
			SsapApplication ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(ssapApplicationId);
			//trigger plan display adapter API
			PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest  = populatePlanAvailabilityAdapterRequest(enrollmentIdMap, enrollee, ssapApplication,disenrollMap);
			long dentalEnrollmentId = planAvailabilityAdapterRequest.getDentalEnrollmentId();
			long healthEnrollmentId = planAvailabilityAdapterRequest.getHealthEnrollmentId();
			PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(planAvailabilityAdapterRequest, enrollmentIdMap, ssapApplication);
			if((planAvailabilityAdapterResponse.getHealthPlanDisplayMap() == null || planAvailabilityAdapterResponse.getHealthPlanDisplayMap().isEmpty()) &&
					(planAvailabilityAdapterResponse.getDentalPlanDisplayMap() == null || planAvailabilityAdapterResponse.getDentalPlanDisplayMap().isEmpty())){
				throw new GIRuntimeException("Plan display adapter response map is empty case_number:"+ssapApplication.getCaseNumber());
			}
			//trigger enrollment API
			String response = automateEnrollment(ssapApplication,planAvailabilityAdapterResponse,enrollee,disenrollMap,healthEnrollmentId,dentalEnrollmentId);
			if(response.equals(GhixConstants.RESPONSE_SUCCESS)){
				//send notice
				triggerAgeOutNotice(ssapApplication, disenrollMap, enrollmentIdMap);
			}
			return response; 
		}catch(Exception ex){
			throw new GIRuntimeException(ex);
		}
		
		
	}
	
	
	private void triggerAgeOutNotice(SsapApplication ssapApplication,Map<String,Set<String>> disenrollMap,Map<String, EnrollmentShopDTO> enrollmentIdMap){
		AgeOutNoticeDTO ageOutNoticeDTO = new AgeOutNoticeDTO();
		List<AgeOutDependant> healthDependants = new ArrayList<AgeOutDependant>();
		List<AgeOutDependant> dentalDependants = new ArrayList<AgeOutDependant>();
		Set<String> disenrollFromHealth =  disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental =  disenrollMap.get(DISENROLL_FROM_DENTAL);
		EnrollmentShopDTO healthEnrollmentShopDTO  = enrollmentIdMap.get(HEALTH_ENROLLMENT_DTO);
		EnrollmentShopDTO dentalEnrollmentShopDTO  = enrollmentIdMap.get(DENTAL_ENROLLMENT_DTO);
		
		for(SsapApplicant ssapApplicant : ssapApplication.getSsapApplicants()){
			//disenroll from health
			if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())){
				createAgeOutDependent(healthDependants,healthEnrollmentShopDTO, ssapApplicant,26);
			}
			//disenroll from dental
			if(disenrollFromDental.contains(ssapApplicant.getApplicantGuid())){
				if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())){
					createAgeOutDependent(dentalDependants,dentalEnrollmentShopDTO, ssapApplicant,26);
				}else{
					createAgeOutDependent(dentalDependants,dentalEnrollmentShopDTO, ssapApplicant,19);
				}
			}
		}
		ageOutNoticeDTO.setApplicationID(ssapApplication.getCaseNumber());
		SsapApplicant primaryApplicant =  getPrimaryApplicantDB(ssapApplication);
		ageOutNoticeDTO.setPrimaryFirstName(primaryApplicant.getFirstName());
		ageOutNoticeDTO.setPrimaryLastName(primaryApplicant.getLastName());
		ageOutNoticeDTO.setEmailId(primaryApplicant.getEmailAddress());
		ageOutNoticeDTO.setsEPendDate(formatDate(getCoverageDate(),DATE_FORMAT_MMMM_DD_YYYY));
		//ageOutNoticeDTO.setHouseholdId(ssapApplication.getCmrHouseoldId().intValue());
		if(!(healthDependants.isEmpty())){
			ageOutNoticeDTO.setHealthDependants(healthDependants);
		}
		if(!(dentalDependants.isEmpty())){
			ageOutNoticeDTO.setDentalDependants(dentalDependants);
		}
		sendNotification(ageOutNoticeDTO, AGE_OUT_DISENROLL_NOTIFICATION, ssapApplication);
	}
	
	
	
	public void sendNotification(AgeOutNoticeDTO ageOutNoticeDTO,String templateName,SsapApplication ssapApplication){
		List<String> sendToEmailList = new LinkedList<String>();;
		Map<String, Object> tokens = new HashMap<String, Object>();
		Location location = null;
		try {
			tokens.put("primaryApplicantName", ageOutNoticeDTO.getPrimaryFirstName()+ " " +ageOutNoticeDTO.getPrimaryLastName());
			tokens.put("ApplicationID", ssapApplication.getCaseNumber());
			tokens.put("Date", DateUtil.dateToString(new TSDate(), "MMMM dd, YYYY"));
			tokens.put(TemplateTokens.EXCHANGE_FULL_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
			tokens.put(TemplateTokens.EXCHANGE_PHONE,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_1,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_1));
			tokens.put(TemplateTokens.EXCHANGE_ADDRESS_2,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_ADDRESS_2));
			tokens.put("exgCityName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_CITY));
			tokens.put("exgStateName",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_STATE));
			tokens.put("zip",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CALLCENTER_PINCODE));
			tokens.put(TemplateTokens.EXCHANGE_URL,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
			tokens.put("exchangeFax",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FAX));
			tokens.put("exchangeAddressEmail",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_EMAIL));
			tokens.put(TemplateTokens.EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
			tokens.put("ageOutDependents",ageOutNoticeDTO);
			SimpleDateFormat formatter=new SimpleDateFormat("MMMM dd, YYYY", new Locale("es", "ES"));
			tokens.put("spanishDate", formatter.format(new TSDate()));
			tokens.put("caseNumber", ssapApplication.getCaseNumber());
			tokens.put("ssapApplicationId", ssapApplication.getId());
			String moduleName = GhixRole.INDIVIDUAL.toString().toLowerCase();
			String fileName = templateName+"_" +String.valueOf(ssapApplication.getCmrHouseoldId())+ TimeShifterUtil.currentTimeMillis() + ".pdf";
			String relativePath = "cmr/" + String.valueOf(ssapApplication.getCmrHouseoldId()) + "/ssap/" + ssapApplication.getId() + "/notifications/"; 
			
			//preference communication logic
			PreferencesDTO preferencesDTO  = preferencesService.getPreferences(ssapApplication.getCmrHouseoldId().intValue(), false);
			location = getLocationFromDTO(preferencesDTO.getLocationDto());
			sendToEmailList.add(preferencesDTO.getEmailAddress());
			noticeService.createModuleNotice(templateName, GhixLanguage.US_EN, tokens, relativePath, fileName,
					moduleName, ssapApplication.getCmrHouseoldId().longValue(), sendToEmailList, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME), 
					ageOutNoticeDTO.getPrimaryFirstName()+" "+ageOutNoticeDTO.getPrimaryLastName(),location,preferencesDTO.getPrefCommunication());
		} catch(Exception exception){
			LOGGER.error("Error occurred while sending age out notification:"+ssapApplication.getId(),exception);
		}
		
		
	}

	private Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}

	private void createAgeOutDependent(List<AgeOutDependant> healthDependants,
				EnrollmentShopDTO healthEnrollmentShopDTO,SsapApplicant ssapApplicant,int age) {
		AgeOutDependant ageOutDependant= new AgeOutDependant();
		ageOutDependant.setFirstName(ssapApplicant.getFirstName());
		ageOutDependant.setLastName(ssapApplicant.getLastName());
		ageOutDependant.setPlanId(healthEnrollmentShopDTO.getPlanName());
		if ("MN".equalsIgnoreCase(STATE_CODE)) {
			ageOutDependant.setDropOutDate(formatDate(getCoverageDate(),DATE_FORMAT_MMMM_D_YYYY));
		}
		else {
			ageOutDependant.setDropOutDate(formatDate(getCoverageDate(),DATE_FORMAT_MMMM_DD_YYYY));
		}
		ageOutDependant.setDropOutReasonText(ssapApplicant.getFirstName()+ " will be "+age+" years old on %s", getAgeOutDate(ssapApplicant.getBirthDate(),age));
		healthDependants.add(ageOutDependant);
	}
	
	
	
	private String getAgeOutDate(Date birthDate,int age) {
		String dateFormat = DATE_FORMAT_MMMM_DD_YYYY;
	    if ("MN".equalsIgnoreCase(STATE_CODE)) {
	    	dateFormat = DATE_FORMAT_MMMM_D_YYYY;
	    }
		Calendar cal = TSCalendar.getInstance();
		int currentYear = cal.get(Calendar.YEAR);
		cal.setTime(birthDate);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		cal.add(Calendar.YEAR, age);
		//if birth year is leap year add 1 day
		if(!(new GregorianCalendar().isLeapYear(currentYear)) && day == 29 && month == 1){
			cal.add(Calendar.DAY_OF_MONTH, 1);
		}
		return formatDate(cal.getTime(), dateFormat);
	}

	private PlanAvailabilityAdapterRequest populatePlanAvailabilityAdapterRequest(Map<String, EnrollmentShopDTO> enrollmentIdMap,Map<String,Set<String>> enrollee,SsapApplication ssapApplication,Map<String,Set<String>> disenrollMap) {
		EnrollmentShopDTO healthEnrollmentShopDTO  = enrollmentIdMap.get(HEALTH_ENROLLMENT_DTO);
		EnrollmentShopDTO dentalEnrollmentShopDTO  = enrollmentIdMap.get(DENTAL_ENROLLMENT_DTO);
		List<String> healthEnrollee  = null;
		List<String> dentalEnrollee  = null;
		Set<String> disenrollFromHealth =  disenrollMap.get(DISENROLL_FROM_HEALTH);
		Set<String> disenrollFromDental =  disenrollMap.get(DISENROLL_FROM_DENTAL);
		long healthEnrollmentId = 0;
		long dentalEnrollmentId = 0;
		if(healthEnrollmentShopDTO != null){
			healthEnrollmentId = healthEnrollmentShopDTO.getEnrollmentId();
		}
		if(dentalEnrollmentShopDTO != null){
			dentalEnrollmentId = dentalEnrollmentShopDTO.getEnrollmentId();
		}
		healthEnrollee = removeAgeOutEnrollee(enrollee,disenrollFromHealth,HEALTH_ENROLLEE,IN_ACTIVE_HEALTH_ENROLLEE);
		dentalEnrollee = removeAgeOutEnrollee(enrollee,disenrollFromDental,DENTAL_ENROLLEE,IN_ACTIVE_DENTAL_ENROLLEE);
		disenrollMap.put(ELIGIBLE_HEALTH_ENROLLEE, new HashSet<String>(healthEnrollee));
		disenrollMap.put(ELIGIBLE_DENTAL_ENROLLEE, new HashSet<String>(dentalEnrollee));
		PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest  = new PlanAvailabilityAdapterRequest();
		planAvailabilityAdapterRequest.setDentalEnrollees(dentalEnrollee);
		planAvailabilityAdapterRequest.setDentalEnrollmentId(dentalEnrollmentId);
		planAvailabilityAdapterRequest.setEnrolledSsapApplicationId(ssapApplication.getParentSsapApplicationId().longValue());
		planAvailabilityAdapterRequest.setHealthEnrollees(healthEnrollee);
		planAvailabilityAdapterRequest.setHealthEnrollmentId(healthEnrollmentId);
		planAvailabilityAdapterRequest.setSsapApplicationId(ssapApplication.getId());
		return planAvailabilityAdapterRequest;
	}
	
	private List<String> removeAgeOutEnrollee(Map<String,Set<String>> enrollee,Set<String> disEnrollee,String enrolleeType,String inactiveEnrolleeType ){
		Set<String>  enrolleeSet = enrollee.get(enrolleeType);
		Set<String>  inActiveEnrolleeSet = enrollee.get(inactiveEnrolleeType);
		List<String> filteredEnrollee = new ArrayList<String>();
		for(String applicantGuid : enrolleeSet){
			if(!disEnrollee.contains(applicantGuid) && !inActiveEnrolleeSet.contains(applicantGuid)){
				filteredEnrollee.add(applicantGuid);
			}
		}
		return filteredEnrollee;
	}
	
	private PdResponsePlanAvailability invokePlanAvailabilityAPI(PdRequestPlanAvailability request, Long ssapApplicationId) {
		PdResponsePlanAvailability pdResponsePlanAvailability = null;
		String status = "FAILURE";
		StringBuilder finalPayload = new StringBuilder();
		long responseTime=0;
		try{
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdRequestPlanAvailability.class);
			String requestJson = writer.writeValueAsString(request);
			finalPayload.append(REQUEST).append(requestJson);
			long currentMillis = TimeShifterUtil.currentTimeMillis();
			ResponseEntity<PdResponsePlanAvailability> response = ghixRestTemplate.exchange(GhixEndPoints.PlandisplayEndpoints.FIND_PLAN_AVAILABILITY, ReferralConstants.EXADMIN_USERNAME, 
					HttpMethod.POST, MediaType.APPLICATION_JSON, PdResponsePlanAvailability.class, request);
			responseTime = (TimeShifterUtil.currentTimeMillis() - currentMillis);
			if (response == null || response.getBody() == null){
				throw new GIRuntimeException("Empty response from plan availability API");
			}
			
			pdResponsePlanAvailability = response.getBody();
			writer = JacksonUtils.getJacksonObjectWriterForJavaType(PdResponsePlanAvailability.class);
			String responseJson = writer.writeValueAsString(pdResponsePlanAvailability);
			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);
			//String responseJson = mapper.writeValueAsString(pdResponsePlanAvailability);
			if (!StringUtils.equals(pdResponsePlanAvailability.getErrCode(), SUCCESS_RESPONSE_CODE)
					/*&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_DLT_NOT_RESPONSE_CODE)
					&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), HLT_NOT_RESPONSE_CODE)
					&& !StringUtils.equals(pdResponsePlanAvailability.getErrCode(), DLT_NOT_RESPONSE_CODE)*/){
				// close the application 
				ssapService.updateSsapApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode(), ssapApplicationId, new Timestamp(new TSDate().getTime()));
				throw new GIRuntimeException(PLAN_AVAILABILITY_RETURNED_ERROR_CODE + pdResponsePlanAvailability.getErrCode() + SEPARATOR  + pdResponsePlanAvailability.getResponse());
			} else {
				status = "SUCCESS";
			}
			
		} catch(Exception e){
			finalPayload.append(NEW_LINE).append(EXCEPTION).append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		}finally{
			logData(ssapApplicationId, finalPayload.toString(), status,PLAN_AVAILABILITY,responseTime);
		}
		return pdResponsePlanAvailability;
	}
	
	
	public PlanAvailabilityAdapterResponse executePlanAvailability(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest,Map<String, EnrollmentShopDTO> enrollmentIdMap,SsapApplication ssapApplication) {

		PdRequestPlanAvailability request;
		try {
			//validatePlanAvailabilityAdapterRequest(planAvailabilityAdapterRequest);
			request = formRequest(ssapApplication,enrollmentIdMap,planAvailabilityAdapterRequest.getHealthEnrollees(), planAvailabilityAdapterRequest.getDentalEnrollees());
		} catch (Exception e) {
			throw new GIRuntimeException(e);
		}
		
		PdResponsePlanAvailability pdResponse = invokePlanAvailabilityAPI(request, planAvailabilityAdapterRequest.getSsapApplicationId());
		
		Long healthEnrollmentId = null;
		Long dentalEnrollmentId = null;
		for (PdPersonRequest person : request.getPdPersonCRDTOList()) {
			if (person.getHealthEnrollmentId() != null) {
				healthEnrollmentId = person.getHealthEnrollmentId();
			}
			if (person.getDentalEnrollmentId() != null) {
				dentalEnrollmentId = person.getDentalEnrollmentId();
			}
			if (healthEnrollmentId != null && dentalEnrollmentId != null) {
				break;
			}

		}
		return formResponse(pdResponse, request.getCoverageDate(),dentalEnrollmentId,healthEnrollmentId);
	}

	private PlanAvailabilityAdapterResponse formResponse(PdResponsePlanAvailability pdResponse, String coverageDate, Long dentalEnrollmentId, Long healthEnrollmentId) {
		
		Map<String, ApplicantPlanDto> healthPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.HEALTH);
		Map<String, ApplicantPlanDto> dentalPlanDisplayMap = map(pdResponse, PlanDisplayEnum.InsuranceType.DENTAL);
		
				
		PlanAvailabilityAdapterResponse response = new PlanAvailabilityAdapterResponse();
		response.setHealthPlanDisplayMap(healthPlanDisplayMap);
		response.setDentalPlanDisplayMap(dentalPlanDisplayMap);
		response.setCoverageStartDate(ReferralUtil.convertStringToDate(coverageDate));
		if(dentalEnrollmentId != null)
		{
			response.setDentalEnrollmentId(dentalEnrollmentId);
		}
		response.setHealthEnrollmentId(healthEnrollmentId);
		
		return response;
	}
	
	private Map<String, ApplicantPlanDto> map(PdResponsePlanAvailability pdResponse, InsuranceType type) {

		Map<String, ApplicantPlanDto> planDisplayMap = new HashMap<String, ApplicantPlanDto>();
		if (pdResponse != null && pdResponse.getPdOrderItemResponse() != null) {
			for (PdOrderItemResponse plandisplayresponse : pdResponse.getPdOrderItemResponse()) {
				if (type == plandisplayresponse.getInsuranceType()) {
					if (ReferralUtil.listSize(plandisplayresponse.getPdOrderItemPersonResponseList()) != 0) {
						for (PdOrderItemPersonResponse pdApplicantDetails : plandisplayresponse.getPdOrderItemPersonResponseList()){
							ApplicantPlanDto applicantPlanDto = populateApplicantPlanDto(pdApplicantDetails, plandisplayresponse);
							planDisplayMap.put(String.valueOf(pdApplicantDetails.getMemberGUID()), applicantPlanDto);
						}
					}
				}
			}
		}
		return planDisplayMap;
	}

	private ApplicantPlanDto populateApplicantPlanDto(PdOrderItemPersonResponse pdApplicantDetails, PdOrderItemResponse plandisplayresponse) {
			
		ApplicantPlanDto applicantPlanDto = new ApplicantPlanDto();
		applicantPlanDto.setMemberGUID(pdApplicantDetails.getMemberGUID());
		applicantPlanDto.setSubscriberFlag(pdApplicantDetails.getSubscriberFlag());
		applicantPlanDto.setPremium(pdApplicantDetails.getPremium());
		applicantPlanDto.setRegion(pdApplicantDetails.getRegion());
		applicantPlanDto.setInsuranceType(plandisplayresponse.getInsuranceType());
		applicantPlanDto.setPlanId(plandisplayresponse.getPlanId());
		applicantPlanDto.setPlanLevelMonthlySubsidy(plandisplayresponse.getMonthlySubsidy());
		applicantPlanDto.setPlanLevelPremiumAfterCredit(plandisplayresponse.getPremiumAfterCredit());
		applicantPlanDto.setPlanLevelPremiumBeforeCredit(plandisplayresponse.getPremiumBeforeCredit());
		
		return applicantPlanDto;
	}

	private PdRequestPlanAvailability formRequest(SsapApplication ssapApplication,Map<String,EnrollmentShopDTO> enrollmentIdMap,List<String> healthEnrollees,
			List<String> dentalEnrollees) throws GIException {
		/*Set<String> healthEnrollees  =  enrollee.get(HEALTH_ENROLLEE);
		Set<String> dentalEnrollees  =  enrollee.get(DENTAL_ENROLLEE);*/
		
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();
		
		SsapApplicant primaryApplicantDB = getPrimaryApplicantDB(ssapApplication);
		HouseholdMember primaryApplicantJson = getPrimaryApplicantJson(members, primaryApplicantDB);
		
		if (primaryApplicantDB == null || primaryApplicantJson == null){
			throw new GIRuntimeException("Primay applicant not found");
		}
		
		
		Map<String, SsapApplicant>  ssapApplicantMap = getSsapApplicantMap(ssapApplication);
		Map<String, HouseholdMember> householdJsonMap = getHouseholdJsonMap(members);
		
		List<BloodRelationship> relationshipList = getRelationships(primaryApplicantJson);

		Date coverageDate = getCoverageDate(ssapApplication,enrollmentIdMap); //Coverage date should end of the month
		
		/*EnrollmentShopDTO healthEnrollmentShopDTO = enrollmentIdMap.get("HealthEnrollmentDTO");
		
		if(healthEnrollmentShopDTO != null && ReferralUtil.isCoverageDatePastEnrollmentDate(coverageDate, healthEnrollmentShopDTO.getEnrolleeShopDTOList().get(0).getEffectiveEndDate())) {
			throw new GIRuntimeException(ReferralConstants.COVERAGE_DATE_IS_PAST_ENROLLMENT_DATE);        
		}*/
	
		PdRequestPlanAvailability request = populateRequest(ssapApplication, primaryApplicantDB, primaryApplicantJson,
				ssapApplicantMap, householdJsonMap,
				relationshipList, enrollmentIdMap, coverageDate, healthEnrollees, dentalEnrollees);
		
		return request;
	}
	
/*private Map<String, EnrollmentShopDTO> getEnrollmentDetails(SsapApplication currentApplication) {
		
		Map<String, EnrollmentShopDTO> enrollmentsByTypeMap = new HashMap<String, EnrollmentShopDTO>(0);
		try {
			enrollmentsByTypeMap = enrollmentsExtractionService.extractActiveMedicalAndDentalEnrollments(currentApplication.getId());
		} catch (GIException e) {
			throw new GIRuntimeException("Failed to get enrollment for " + currentApplication.getCaseNumber());
		}
		if(enrollmentsByTypeMap == null || enrollmentsByTypeMap.isEmpty() || null == enrollmentsByTypeMap.get(HEALTH)){
			throw new GIRuntimeException("Failed to get health enrollment for case number:"+ enrolledApplication.getCaseNumber());
		}
		return enrollmentsByTypeMap;
	}*/
	
	private Date getCoverageDate(SsapApplication ssapApplication,Map<String,EnrollmentShopDTO> enrollmentIdMap) throws GIException{
		Timestamp timestamp = coverageStartDateService.computeCoverageStartDate(new BigDecimal(ssapApplication.getId()), getMap(enrollmentIdMap), false, false);
		Calendar startDate = TSCalendar.getInstance();
		startDate.setTimeInMillis(timestamp.getTime());
		return startDate.getTime();
		
	}
	
	private Date getCoverageDate(){
		Calendar cal = TSCalendar.getInstance();
		cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
		return cal.getTime();
	}

	private Map<String, EnrollmentShopDTO> getMap(Map<String, EnrollmentShopDTO> enrollmentIdMap) {
		 Map<String, EnrollmentShopDTO> enrollmentMap = new HashMap<>();
		EnrollmentShopDTO health = enrollmentIdMap.get("HealthEnrollmentDTO");
		EnrollmentShopDTO dental = enrollmentIdMap.get("DentalEnrollmentDTO");
		if(health != null) {
			enrollmentMap.put("health", health);
		}
		if(dental != null) {
			enrollmentMap.put("dental", dental);
		}
		return enrollmentMap;
}


	private PdRequestPlanAvailability populateRequest(
			SsapApplication currentApplication,
			SsapApplicant primaryApplicantDB,
			HouseholdMember primaryApplicantJson,
			Map<String, SsapApplicant> ssapApplicantMap,
			Map<String, HouseholdMember> householdMemberJsonMap,
			List<BloodRelationship> relationshipList,
			Map<String, EnrollmentShopDTO> enrollmentsByTypeMap,
			Date coverageDate,
			List<String> healthEnrollees, List<String> dentalEnrollees
			) {
		
		PdRequestPlanAvailability request = new PdRequestPlanAvailability();
		request.setHouseholdGUID(currentApplication.getCmrHouseoldId().longValue());
		request.setCoverageDate(ReferralUtil.formatDate(coverageDate, ReferralConstants.DEFDATEPATTERN));
		request.setShoppingType(ShoppingType.INDIVIDUAL);
		request.setEnrollmentType(EnrollmentType.S);
		request.setCostSharing(currentApplication.getCsrLevel() != null ? mapCostSharing(currentApplication.getCsrLevel().toString()) : null);		
		request.setMaxSubsidy(currentApplication.getMaximumAPTC() != null ? currentApplication.getMaximumAPTC().floatValue() : null);

		LinkedList<PdPersonRequest> pdPersonCRDTOList = new LinkedList<PdPersonRequest>();
		//Set<Long> persons = new TreeSet<Long>(ssapApplicantMap.keySet());

		for (String applicantGuid : ssapApplicantMap.keySet()) {
			PdPersonRequest person = new PdPersonRequest();
			SsapApplicant applicantDBRow = ssapApplicantMap.get(applicantGuid);
			HouseholdMember applicantJsonRow = householdMemberJsonMap.get(applicantGuid);
			if((healthEnrollees.contains(applicantGuid) || dentalEnrollees.contains(applicantGuid))  && ExchangeEligibilityStatus.QHP.name().equals(applicantDBRow.getEligibilityStatus())
					&& applicantJsonRow.getApplyingForCoverageIndicator() && applicantDBRow.getOnApplication().equals("Y") ){
				person.setCountyCode(getCounty(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));
				person.setDob(ReferralUtil.formatDate(applicantJsonRow.getDateOfBirth(), ReferralConstants.DEFDATEPATTERN));
				person.setDentalEnrollmentId(getEnrollmentId(enrollmentsByTypeMap, applicantDBRow.getApplicantGuid(), dentalEnrollees, "DentalEnrollmentDTO"));
				person.setHealthEnrollmentId(getEnrollmentId(enrollmentsByTypeMap, applicantDBRow.getApplicantGuid(), healthEnrollees, "HealthEnrollmentDTO"));
				person.setMemberGUID(applicantDBRow.getApplicantGuid());
				person.setRelationship(getRelationShip(relationshipList, String.valueOf(applicantJsonRow.getPersonId())));
				person.setTobacco(StringUtils.equalsIgnoreCase(ReferralConstants.Y, applicantDBRow.getTobaccouser()) ? YorN.Y : YorN.N);
				person.setZipCode(getZipCode(applicantDBRow, applicantJsonRow, primaryApplicantDB, primaryApplicantJson));
				if(applicantDBRow.getPersonId() == PRIMARY_APPLICANT_PERSON_ID){
					pdPersonCRDTOList.addFirst(person);
				}else{
					pdPersonCRDTOList.add(person);
				}
			}
		}
		request.setPdPersonCRDTOList(pdPersonCRDTOList);
		return request;
	}
	
	private Long getEnrollmentId(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap, String applicantGuid, List<String> enrollees, String enrollmentType) {
		Long enrollmentId = null;
		if (enrollees != null && enrollees.size() > 0 && enrollees.contains(applicantGuid)){
			EnrollmentShopDTO enrollment = enrollmentsByTypeMap.get(enrollmentType);
			enrollmentId =  enrollment != null ? enrollment.getEnrollmentId().longValue() : null;
		}
		return enrollmentId;
		
	}

	private List<BloodRelationship> getRelationships(HouseholdMember primaryApplicantJson) {
		List<BloodRelationship> relationshipList = new ArrayList<BloodRelationship>();
		for(BloodRelationship relationship : primaryApplicantJson.getBloodRelationship()){
			if (String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID).equals(relationship.getRelatedPersonId())){
				relationshipList.add(relationship);
			}
		}
		
		return relationshipList;
	}

	private HouseholdMember getPrimaryApplicantJson(List<HouseholdMember> members, SsapApplicant primaryApplicantDB) {
		HouseholdMember primaryApplicantJson = null;
		for (HouseholdMember householdMember : members) {
			if (householdMember.getPersonId().compareTo((int) primaryApplicantDB.getPersonId()) == 0){
				primaryApplicantJson = householdMember;
				break;
			}
		}
		return primaryApplicantJson;
	}

	private SsapApplicant getPrimaryApplicantDB(SsapApplication currentApplication) {
		SsapApplicant primaryApplicantDB = null;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			if (applicant.getPersonId() == 1){
				primaryApplicantDB = applicant;
				break;
			}
		}
		return primaryApplicantDB;
	}

	/*private SsapApplication validate(Long ssapApplicationId) {
		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException(UNABLE_TO_FIND_APPLICATION_FOR_SSAP_APPLICATION_ID + ssapApplicationId);
		}

		SsapApplication currentApplication = ssapApplications.get(0);
		
		SsapApplicationEvent currentApplicationEvent = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplicationId);
		
		if (currentApplicationEvent == null){
			throw new GIRuntimeException(NO_APPLICATION_EVENTS_FOUND_FOR_APPLICATION);
		}
		return currentApplication;
	}
*/	
	private void validatePlanAvailabilityAdapterRequest(PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest) {
		if (planAvailabilityAdapterRequest.getSsapApplicationId() == 0){
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest ssap application id" + planAvailabilityAdapterRequest.getSsapApplicationId());
		}
		
		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() == 0){
			throw new GIRuntimeException("Invalid planAvailabilityAdapterRequest health enrollment id" + planAvailabilityAdapterRequest.getHealthEnrollmentId());
		}
		
		if (planAvailabilityAdapterRequest.getHealthEnrollmentId() != 0 
				&& (planAvailabilityAdapterRequest.getHealthEnrollees() == null || planAvailabilityAdapterRequest.getHealthEnrollees().size() == 0)){
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest health enrollee list for " + planAvailabilityAdapterRequest.getHealthEnrollmentId());
		}
		
		if (planAvailabilityAdapterRequest.getDentalEnrollmentId() != 0 
				&& (planAvailabilityAdapterRequest.getDentalEnrollees() == null || planAvailabilityAdapterRequest.getDentalEnrollees().size() == 0)){
			throw new GIRuntimeException("Missing planAvailabilityAdapterRequest dental enrollee list for " + planAvailabilityAdapterRequest.getDentalEnrollmentId());
		}
		
	}
	
	private CostSharing mapCostSharing(String csrLevel) {
		CostSharing csLevel = CostSharing.valueOf(csrLevel);
		switch (csrLevel) {
			case ReferralConstants.CS2:
				csLevel = CS2;
				break;
			case ReferralConstants.CS3:
				csLevel = CS3;
				break;
			case ReferralConstants.CS4:
				csLevel = CS4;
				break;
			case ReferralConstants.CS5:
				csLevel = CS5;
				break;
			case ReferralConstants.CS6:
				csLevel = CS6;
				break;
			default:
				break;
		}

		return csLevel;
	}
	
	private Relationship getRelationShip(List<BloodRelationship> relationshipList, String personId ) {
		Relationship relationShipCode = null;
		for (BloodRelationship bloodRelationship : relationshipList) {
			if (personId.equals(bloodRelationship.getIndividualPersonId())){
				if (SELF_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.SELF;
				} else if (SPOUSE_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.SPOUSE;
				} else if (CHILD_RELATIONSHIP_CODES.contains(bloodRelationship.getRelation())){
					relationShipCode = Relationship.CHILD;
				} else {
					relationShipCode = Relationship.DEPENDENT;
				}
			}
		}
		return relationShipCode;
	}

	private String getZipCode(SsapApplicant applicantDBRow,	HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String zipCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPostalCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				zipCode = applicantJsonRow.getOtherAddress().getAddress().getPostalCode(); 
			}
		}
		return zipCode;
	}

	private String getCounty(SsapApplicant applicantDBRow, HouseholdMember applicantJsonRow, SsapApplicant primaryApplicantDB, HouseholdMember primaryApplicantJson) {
		String countyCode = primaryApplicantJson.getHouseholdContact().getHomeAddress().getPrimaryAddressCountyFipsCode();
		if (applicantJsonRow.getLivesWithHouseholdContactIndicator() != null){ 
			if (!applicantJsonRow.getLivesWithHouseholdContactIndicator()){
				countyCode = applicantJsonRow.getOtherAddress().getAddress().getCounty(); 
			}
		}
		return countyCode;
	}
	
	private void logData(Long ssapApplicationId, String message, String status,String serviceName,long responseTime) {
		SsapIntegrationLog ssapIntegrationLog = new SsapIntegrationLog();
		ssapIntegrationLog.setCreatedDate(new TSDate());
		ssapIntegrationLog.setPayload(message);
		ssapIntegrationLog.setServiceName(serviceName);
		ssapIntegrationLog.setStatus(status);
		ssapIntegrationLog.setSsapApplicationId(ssapApplicationId);
		ssapIntegrationLog.setResponseTimeMillis(responseTime);
		ssapIntegrationLogRepository.save(ssapIntegrationLog);
	}

	//Enrollment Methods - Started
	public String automateEnrollment(SsapApplication ssapApplication,PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse,Map<String,Set<String>> enrollee,Map<String,Set<String>> disenrollMap,long healthEnrollmentId,long dentalEnrollmentId) {
		Map<String, ApplicantPlanDto> healthPlanDisplayMap = planAvailabilityAdapterResponse.getHealthPlanDisplayMap();
		Map<String, ApplicantPlanDto> dentalPlanDisplayMap = planAvailabilityAdapterResponse.getDentalPlanDisplayMap();
		StringBuilder finalPayload = new StringBuilder();
		GsonBuilder  gsonBuilder =  new GsonBuilder();
		gsonBuilder.setDateFormat("MMM dd, yyyy HH:mm:ss a");
		Gson gson = gsonBuilder.create();
		Set<String> healthEnrollees  =  enrollee.get(HEALTH_ENROLLEE);
		Set<String> dentalEnrollees  =  enrollee.get(DENTAL_ENROLLEE);
		long responseTime = 0;
		String status = GhixConstants.RESPONSE_FAILURE;

		EnrollmentUpdateRequest request;
		try {
			request = formRequest(ssapApplication, healthPlanDisplayMap, dentalPlanDisplayMap, healthEnrollees, dentalEnrollees, planAvailabilityAdapterResponse,disenrollMap,healthEnrollmentId,dentalEnrollmentId);

			String requestJson = gson.toJson(request);

			finalPayload.append(REQUEST).append(requestJson);
			long currentMillis = TimeShifterUtil.currentTimeMillis();
			ResponseEntity<String> response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.AUTOMATE_SPECIAL_ENROLLMENT_URL, ReferralConstants.EXADMIN_USERNAME, HttpMethod.POST, MediaType.APPLICATION_JSON,
			        String.class, requestJson);
			responseTime = (TimeShifterUtil.currentTimeMillis() - currentMillis);
			if (response == null || response.getBody() == null) {
				throw new GIRuntimeException("Empty Response from Automate Enrollment API.");
			}
			
			String responseJson = response.getBody();
			EnrollmentResponse enrollmentResponse = gson.fromJson(responseJson, EnrollmentResponse.class);

			finalPayload.append(NEW_LINE).append(RESPONSE).append(responseJson);

			if (enrollmentResponse != null && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(enrollmentResponse.getStatus())) {
				long count = ssapService.updateSsapApplicationStatus(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(), ssapApplication.getId(),new Timestamp(new TSDate().getTime()));
				if(count>0){
					status = GhixConstants.RESPONSE_SUCCESS;
				}else{
					status = GhixConstants.RESPONSE_FAILURE;
				}
			} else {
				//close the application
				ssapService.updateSsapApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode(), ssapApplication.getId(), new Timestamp(new TSDate().getTime()));
			}


		} catch (Exception e) {
			finalPayload.append(NEW_LINE).append("Exception").append(ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(e);
		} finally {
			logData(ssapApplication.getId(), finalPayload.toString(), status,AUTO_ENROLLMENT_UPDATE,responseTime);
		}

		return status;

	}

	private EnrollmentUpdateRequest formRequest(SsapApplication currentApplication, Map<String, ApplicantPlanDto> healthPlanDisplayMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap,
			Set<String> healthEnrollees, Set<String> dentalEnrollees,
			PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse,Map<String,Set<String>> disenrollMap,Long healthEnrollmentId,Long dentalEnrollmentId) {

		Set<String> disenrollFromHealth = null;
		Set<String> disenrollFromDental = null;
		List<EnrollmentCoreDto> enrollmentDto = new ArrayList<EnrollmentCoreDto>();
		EnrollmentUpdateRequest request = new EnrollmentUpdateRequest();
		SingleStreamlinedApplication applicationData = ssapJsonBuilder.transformFromJson(currentApplication.getApplicationData());
		List<HouseholdMember> members = applicationData.getTaxHousehold().get(0).getHouseholdMember();

		SsapApplicant primaryApplicantDB = getPrimaryApplicantDB(currentApplication);
		HouseholdMember primaryApplicantJson = getPrimaryApplicantJson(members, primaryApplicantDB);

		if (primaryApplicantDB == null || primaryApplicantJson == null) {
			throw new GIRuntimeException("Primary application not found");
		}

		

		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		
		// populate applicant event map to draw enrollment ADD, TERM, OTHER sections
		Map<String, SsapApplicantEvent> ssapApplicantEventMap = new HashMap<String, SsapApplicantEvent>();
		List<SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent ssapApplicantEvent : applicantEvents) {
			if(SsapApplicantEvent.EventPrecedenceIndicator.Y.equals(ssapApplicantEvent.getEventPrecedenceIndicator())){
				ssapApplicantEventMap.put(ssapApplicantEvent.getSsapApplicant().getApplicantGuid(), ssapApplicantEvent);
			}
		}
		
		Household household = cmrHouseholdRepository.findOne(currentApplication.getCmrHouseoldId().intValue());
		
		Map<String, SsapApplicant>  ssapApplicantMap = getSsapApplicantMap(currentApplication);
		Map<String, HouseholdMember> householdJsonMap = getHouseholdJsonMap(members);
		
		
		if(disenrollMap != null){
			disenrollFromHealth = disenrollMap.get(DISENROLL_FROM_HEALTH);
			disenrollFromDental = disenrollMap.get(DISENROLL_FROM_DENTAL);

			if(healthEnrollmentId !=null && healthEnrollmentId !=0){
				if(healthPlanDisplayMap == null){
					healthPlanDisplayMap =  new HashMap<String, ApplicantPlanDto>();
				}
				EnrollmentCoreDto healthEnrollment = populateEnrollment(ssapApplicantMap, householdJsonMap,healthPlanDisplayMap, primaryApplicantJson,
					planAvailabilityAdapterResponse, household, healthEnrollees, currentApplication,disenrollFromHealth,ssapApplicantEventMap);
					healthEnrollment.setId(healthEnrollmentId.intValue());
					enrollmentDto.add(healthEnrollment);
				
			}
			if(dentalEnrollmentId !=null && dentalEnrollmentId !=0){
				if(dentalPlanDisplayMap == null){
					dentalPlanDisplayMap =  new HashMap<String, ApplicantPlanDto>();
				}
				EnrollmentCoreDto dentalEnrollment = populateEnrollment(ssapApplicantMap, householdJsonMap,dentalPlanDisplayMap, primaryApplicantJson,
					planAvailabilityAdapterResponse, household, dentalEnrollees, currentApplication,disenrollFromDental,ssapApplicantEventMap);
				dentalEnrollment.setId(dentalEnrollmentId.intValue());
				enrollmentDto.add(dentalEnrollment);
			}
			request.setEnrollmentDto(enrollmentDto);
			return request;
		}
		
		Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.ADD.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (!ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.REMOVE.name().equals(applicantEvent.getSepEvents().getChangeType())) {

				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						newlyIneigibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				newlyIneligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		Map<String, SsapApplicant> changeEligibleApplicantDBRowMap = new HashMap<String, SsapApplicant>();
		Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap = new HashMap<String, HouseholdMember>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = ssapApplicantEventMap.get(applicant.getApplicantGuid());

			if (ExchangeEligibilityStatus.QHP.name().equals(applicant.getEligibilityStatus()) && ChangeType.OTHER.name().equals(applicantEvent.getSepEvents().getChangeType())) {
				for (HouseholdMember householdMember : members) {
					if (householdMember.getPersonId().compareTo((int) applicant.getPersonId()) == 0) {
						changeEligibleApplicantJsonRowMap.put(applicant.getApplicantGuid(), householdMember);
						break;
					}
				}
				changeEligibleApplicantDBRowMap.put(applicant.getApplicantGuid(), applicant);
			}
		}

		

		// HLT
		EnrollmentCoreDto healthEnrollment = populateHealthEnrollment(newlyEligibleApplicantDBRowMap, newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, newlyEligibleApplicantJsonRowMap, newlyIneigibleApplicantJsonRowMap,
		        changeEligibleApplicantJsonRowMap, healthPlanDisplayMap, primaryApplicantJson, ssapApplicantEventMap, planAvailabilityAdapterResponse, household, new ArrayList<String>(healthEnrollees), currentApplication);

		healthEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getHealthEnrollmentId())));
		healthEnrollment.setSsapApplicationid(currentApplication.getId());
		healthEnrollment.setCsrLevel(currentApplication.getCsrLevel());
		
		enrollmentDto.add(healthEnrollment);

		// DLT

		if (dentalPlanDisplayMap.size() > 0) {
			EnrollmentCoreDto dentalEnrollment = populateDentalEnrollment(newlyIneligibleApplicantDBRowMap, changeEligibleApplicantDBRowMap, newlyIneigibleApplicantJsonRowMap, changeEligibleApplicantJsonRowMap, dentalPlanDisplayMap,
			        primaryApplicantJson, ssapApplicantEventMap, planAvailabilityAdapterResponse, household, currentApplication, new ArrayList<String>(dentalEnrollees));

			dentalEnrollment.setId(Integer.valueOf(String.valueOf(planAvailabilityAdapterResponse.getDentalEnrollmentId())));
			dentalEnrollment.setSsapApplicationid(currentApplication.getId());
			dentalEnrollment.setCsrLevel(currentApplication.getCsrLevel());
		
			enrollmentDto.add(dentalEnrollment);
		}
		// add enrollments to request
		request.setEnrollmentDto(enrollmentDto);

		return request;
	}


	private Map<String, HouseholdMember> getHouseholdJsonMap(List<HouseholdMember> members) {
		Map<String, HouseholdMember> householdJsonMap = new HashMap<String, HouseholdMember>();
		//populate applicant json map
		for (HouseholdMember householdMember : members) {
			householdJsonMap.put(householdMember.getApplicantGuid(), householdMember);
		}
		return householdJsonMap;
	}


	private Map<String, SsapApplicant> getSsapApplicantMap(SsapApplication currentApplication) {
		Map<String, SsapApplicant>  ssapApplicantMap = new HashMap<String, SsapApplicant>();
		//populate applicant db map
		for (SsapApplicant ssapApplicant: currentApplication.getSsapApplicants()) {
			ssapApplicantMap.put(ssapApplicant.getApplicantGuid(), ssapApplicant);
		}
		return ssapApplicantMap;
	}
	
	
	private EnrollmentCoreDto populateEnrollment(Map<String, SsapApplicant> applicantMap,Map<String, HouseholdMember> applicantJsonMap,
	        Map<String, ApplicantPlanDto> healthPlanDisplayMap, HouseholdMember primaryApplicantJson,PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, 
	        Household household,Set<String> healthEnrollees, SsapApplication currentApplication,Set<String> disenrollFromHealth,Map<String, SsapApplicantEvent> applicantEventMap) {

		EnrollmentCoreDto healthEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> healthEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		ApplicantPlanDto memberPlan = null;
		EnrolleeCoreDto enrolleeCoreDto = null;
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);
		boolean isPremiumUpdated = false;
		for(SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()){
			if(healthEnrollees.contains(ssapApplicant.getApplicantGuid())){
				enrolleeCoreDto = new EnrolleeCoreDto();
				SsapApplicant memberDB = applicantMap.get(ssapApplicant.getApplicantGuid());
				HouseholdMember memberJson = applicantJsonMap.get(ssapApplicant.getApplicantGuid());
				memberPlan = healthPlanDisplayMap.get(ssapApplicant.getApplicantGuid());
				if (memberPlan != null && !isPremiumUpdated) {
					healthEnrollment.setAptcAmt(memberPlan.getPlanLevelMonthlySubsidy()==0?null:memberPlan.getPlanLevelMonthlySubsidy());
					healthEnrollment.setPlanId(memberPlan.getPlanId().intValue());
					healthEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
					healthEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());
					isPremiumUpdated = true;
				}
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(ssapApplicant.getApplicantGuid());
				ENROLLEE_ACTION enrolleeAction = null;
				if(disenrollFromHealth.contains(ssapApplicant.getApplicantGuid())){
					enrolleeAction= EnrolleeCoreDto.ENROLLEE_ACTION.TERM;
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
					DateTime terminationDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate());
					enrolleeCoreDto.setEffectiveEndDate(new TSDate(terminationDate.minusDays(1).getMillis()));
				}else{
					enrolleeAction = EnrolleeCoreDto.ENROLLEE_ACTION.OTHER;
					enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.OTHER);
					enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication,primaryApplicantJson));
				}
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant ,enrolleeAction);
				healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		
		

		healthEnrollment.setEnrolleeCoreDtoList(healthEnrolleeCoreDtoList);
		
		healthEnrollment.setSsapApplicationid(currentApplication.getId());
		healthEnrollment.setCsrLevel(currentApplication.getCsrLevel());
		healthEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());

		return healthEnrollment;
	}
	


	private EnrollmentCoreDto populateDentalEnrollment(Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap, Map<String, SsapApplicant> otherEligibleApplicantDBRowMap, Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap,
	        Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap, Map<String, ApplicantPlanDto> dentalPlanDisplayMap, HouseholdMember primaryApplicantJson, Map<String, SsapApplicantEvent> applicantEventMap,
	        PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, Household household, SsapApplication currentApplication, List<String> dentalEnrollees) {
		EnrollmentCoreDto dentalEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> dentalEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);

		ApplicantPlanDto memberPlan = null;
		// DO NOT automate add members to DENTAL

		// remove members
		for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
			if (dentalEnrollees.contains(newIneligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
	
				SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
				HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
	
				memberPlan = dentalPlanDisplayMap.get(newIneligibleApplicantGuid);
	
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				DateTime terminationDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate());
				if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {
					terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
				}
				enrolleeCoreDto.setEffectiveEndDate(new TSDate(terminationDate.getMillis()));
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		// other eligible members
		for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
			if (dentalEnrollees.contains(otherEligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
				SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
				HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
				memberPlan = dentalPlanDisplayMap.get(otherEligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(otherEligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getDentalPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication,primaryApplicantJson));
	
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
				dentalEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		if (memberPlan == null) {
			// this should never happen!
			throw new GIRuntimeException("No new dental plan information found!");
		}

		dentalEnrollment.setEnrolleeCoreDtoList(dentalEnrolleeCoreDtoList);
		dentalEnrollment.setAptcAmt(memberPlan.getPlanLevelMonthlySubsidy()==0?null:memberPlan.getPlanLevelMonthlySubsidy());
		dentalEnrollment.setPlanId(memberPlan.getPlanId().intValue());
		dentalEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
		dentalEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());
		dentalEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());

		return dentalEnrollment;
	}

	private EnrollmentCoreDto populateHealthEnrollment(Map<String, SsapApplicant> newlyEligibleApplicantDBRowMap, Map<String, SsapApplicant> newlyIneligibleApplicantDBRowMap, Map<String, SsapApplicant> otherEligibleApplicantDBRowMap,
	        Map<String, HouseholdMember> newlyEligibleApplicantJsonRowMap, Map<String, HouseholdMember> newlyIneigibleApplicantJsonRowMap, Map<String, HouseholdMember> changeEligibleApplicantJsonRowMap,
	        Map<String, ApplicantPlanDto> healthPlanDisplayMap, HouseholdMember primaryApplicantJson, Map<String, SsapApplicantEvent> applicantEventMap, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse, Household household,
	        List<String> healthEnrollees, SsapApplication currentApplication) {

		EnrollmentCoreDto healthEnrollment = new EnrollmentCoreDto();
		List<EnrolleeCoreDto> healthEnrolleeCoreDtoList = new ArrayList<EnrolleeCoreDto>();
		ApplicantPlanDto memberPlan = null;
		
		SsapApplicant primaryApplicant= getPrimaryApplicantDB(currentApplication);
		
		// add members
		for (String newApplicantGuid : newlyEligibleApplicantDBRowMap.keySet()) {
			EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();

			SsapApplicant memberDB = newlyEligibleApplicantDBRowMap.get(newApplicantGuid);
			HouseholdMember memberJson = newlyEligibleApplicantJsonRowMap.get(newApplicantGuid);
			memberPlan = healthPlanDisplayMap.get(newApplicantGuid);
			SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newApplicantGuid);

			populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.ADD);

			enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication ,primaryApplicantJson));

			enrolleeCoreDto.setEffectiveStartDate(planAvailabilityAdapterResponse.getCoverageStartDate());
			enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.ADD);

			healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
		}

		// remove members
		for (String newIneligibleApplicantGuid : newlyIneligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(newIneligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
	
				SsapApplicant memberDB = newlyIneligibleApplicantDBRowMap.get(newIneligibleApplicantGuid);
				HouseholdMember memberJson = newlyIneigibleApplicantJsonRowMap.get(newIneligibleApplicantGuid);
				memberPlan = healthPlanDisplayMap.get(newIneligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(newIneligibleApplicantGuid);
	
				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant ,EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
				DateTime terminationDate = new DateTime(planAvailabilityAdapterResponse.getCoverageStartDate());
				if (ReferralConstants.ONE != ssapApplicantEvent.getSepEvents().getType()) {	
					terminationDate = terminationDate.minusDays(ReferralConstants.ONE);
				}
				enrolleeCoreDto.setEffectiveEndDate(new TSDate(terminationDate.getMillis()));
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.TERM);
	
				healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		// other eligible members
		for (String otherEligibleApplicantGuid : otherEligibleApplicantDBRowMap.keySet()) {
			if (healthEnrollees.contains(otherEligibleApplicantGuid)){
				EnrolleeCoreDto enrolleeCoreDto = new EnrolleeCoreDto();
				SsapApplicant memberDB = otherEligibleApplicantDBRowMap.get(otherEligibleApplicantGuid);
				HouseholdMember memberJson = changeEligibleApplicantJsonRowMap.get(otherEligibleApplicantGuid);
				memberPlan = healthPlanDisplayMap.get(otherEligibleApplicantGuid);
				SsapApplicantEvent ssapApplicantEvent = applicantEventMap.get(otherEligibleApplicantGuid);

				populateEnrolleeCoreDto(enrolleeCoreDto, memberDB, memberJson, memberPlan, primaryApplicantJson, ssapApplicantEvent, household, currentApplication, planAvailabilityAdapterResponse.getHealthPlanDisplayMap(),primaryApplicant,EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				enrolleeCoreDto.setRelationshipList(populateRelationship(memberJson, "" + memberDB.getPersonId(), currentApplication,primaryApplicantJson));
	
				enrolleeCoreDto.setEnrolleeAction(EnrolleeCoreDto.ENROLLEE_ACTION.CHANGE);
	
				healthEnrolleeCoreDtoList.add(enrolleeCoreDto);
			}
		}

		if (memberPlan == null) {
			// this should never happen!
			throw new GIRuntimeException("No new health plan information found!");
		}

		healthEnrollment.setEnrolleeCoreDtoList(healthEnrolleeCoreDtoList);
		healthEnrollment.setAptcAmt(memberPlan.getPlanLevelMonthlySubsidy());
		healthEnrollment.setPlanId(memberPlan.getPlanId().intValue());
		healthEnrollment.setNetPremiumAmt(memberPlan.getPlanLevelPremiumAfterCredit());
		healthEnrollment.setGrossPremiumAmt(memberPlan.getPlanLevelPremiumBeforeCredit());
		healthEnrollment.setBenefitEffectiveDate(planAvailabilityAdapterResponse.getCoverageStartDate());

		return healthEnrollment;
	}

	private String populateSubscriberFlag(SsapApplicant memberDB, Map<String, ApplicantPlanDto> applicantPlanMap) {
		String subscriberFlag = "N";
		if (applicantPlanMap.containsKey(memberDB.getApplicantGuid())) {
			ApplicantPlanDto applicantPlanDto = applicantPlanMap.get(memberDB.getApplicantGuid());
			subscriberFlag = applicantPlanDto.getSubscriberFlag().name();
		}
		
		return subscriberFlag;
	}

	private void populateEnrolleeCoreDto(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB, HouseholdMember memberJson, ApplicantPlanDto memberPlan, HouseholdMember primaryApplicantJson, SsapApplicantEvent ssapApplicantEvent,
	        Household household, SsapApplication currentApplication, Map<String, ApplicantPlanDto> planMap, SsapApplicant primaryApplicant, ENROLLEE_ACTION action) {

		enrolleeCoreDto.setExchgIndivIdentifier(memberDB.getApplicantGuid());
		enrolleeCoreDto.setTaxIdNumber(memberDB.getSsn());
		
		enrolleeCoreDto.setLastName(memberDB.getLastName());
		enrolleeCoreDto.setFirstName(memberDB.getFirstName());
		enrolleeCoreDto.setMiddleName(memberDB.getMiddleName());
		enrolleeCoreDto.setSuffix(memberDB.getNameSuffix());
		enrolleeCoreDto.setBirthDate(memberDB.getBirthDate());

		enrolleeCoreDto.setMaintainenceReasonCode(ssapApplicantEvent.getSepEvents().getMrcCode());

		String primaryPhone = null, secondaryPhone = null;
		if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber()) && !StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
			secondaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getPhone().getPhoneNumber();
		} else if (!StringUtils.isEmpty(primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber())) {
			primaryPhone = primaryApplicantJson.getHouseholdContact().getOtherPhone().getPhoneNumber();
		}

		if (StringUtils.isEmpty(primaryPhone)) {
			primaryPhone = household.getPhoneNumber();
		}
		enrolleeCoreDto.setPrimaryPhoneNo(primaryPhone);
		enrolleeCoreDto.setSecondaryPhoneNo(secondaryPhone);

		if (EnrolleeCoreDto.ENROLLEE_ACTION.TERM != action) {

			if(memberPlan!=null){
				enrolleeCoreDto.setRatingArea(String.valueOf(memberPlan.getRegion()));
				enrolleeCoreDto.setTotalIndvResponsibilityAmt(memberPlan.getPremium());
			}

			enrolleeCoreDto.setRaceMap(populateRaceDetails(memberJson));

			String relationshipToHCPLkpStr = populateRelationshipToHCP(memberJson, "" + memberDB.getPersonId(), primaryApplicantJson);
			enrolleeCoreDto.setRelationToHCPLkpStr(relationshipToHCPLkpStr);

			populateLocationDetails(enrolleeCoreDto, memberJson);
			String languageWrittenLkpStr = languageWritten(memberJson);
			

			String languageSpokenLkpStr = languageSpoken(memberJson);
			enrolleeCoreDto.setLanguageSpokenLkpStr(languageSpokenLkpStr);
			enrolleeCoreDto.setLanguageWrittenLkpStr(languageWrittenLkpStr);
		}
		populateTobaccoUsageDetails(enrolleeCoreDto, memberDB);

		//boolean citizenshipIndicator = memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator() != null ? memberJson.getCitizenshipImmigrationStatus().getCitizenshipStatusIndicator().booleanValue() : false;
		
		// populateCitizenshipDetails(enrolleeCoreDto);

		populateMaritalStatus(enrolleeCoreDto, memberDB);

		populateGenderDetails(enrolleeCoreDto, memberDB);
		
		
		enrolleeCoreDto.setSubscriberNewFlag(populateSubscriberFlag(memberDB, planMap));
		
		
	}

	private void populateMaritalStatus(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String maritalStatusLkpStr = MaritalStatusEnum.value(memberDB.getMarried());
		if ("Yes".equalsIgnoreCase(memberDB.getMarried()) || "true".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "Married";
		}else if("No".equalsIgnoreCase(memberDB.getMarried()) || "false".equalsIgnoreCase(memberDB.getMarried())) {
			maritalStatusLkpStr = "Single";
		}
		enrolleeCoreDto.setMaritalStatusLkpStr(maritalStatusLkpStr);
	}

	private String populateRelationshipToHCP(HouseholdMember memberJson, String personId, HouseholdMember primaryApplicantJson) {
		String relatioshipToHCPStr = null;

		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId()) && String.valueOf(LifeChangeEventConstant.PRIMARY_APPLICANT_PERSON_ID).equalsIgnoreCase(bloodRelationship.getRelatedPersonId())) {
				relatioshipToHCPStr = bloodRelationship.getRelation();
			}
		}
		return relatioshipToHCPStr;
	}

	private String languageSpoken(HouseholdMember memberJson) {
		String languageSpoken = null;
		String languageSpokenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageSpoken = memberJson.getHouseholdContact().getContactPreferences().getPreferredSpokenLanguage();
		}
		if(StringUtils.isNotBlank(languageSpoken)) {
			languageSpokenCode = LanguageEnum.getCode(languageSpoken);								
		}
		return languageSpokenCode;
	}

	private String languageWritten(HouseholdMember memberJson) {
		String languageWritten = null;
		String languageWrittenCode = null;
		if (memberJson.getHouseholdContact() != null && memberJson.getHouseholdContact().getContactPreferences() != null) {
			languageWritten = memberJson.getHouseholdContact().getContactPreferences().getPreferredWrittenLanguage();
		}

		if(StringUtils.isNotBlank(languageWritten)) {
			languageWrittenCode = LanguageEnum.getCode(languageWritten);								
		}
		return languageWrittenCode;
	}

	private void populateGenderDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String genderLkpStr = "Male".equalsIgnoreCase(memberDB.getGender()) ? "M" : "F";
		enrolleeCoreDto.setGenderLkpStr(genderLkpStr);
	}

	private void populateLocationDetails(EnrolleeCoreDto enrolleeCoreDto, HouseholdMember memberJson) {

		Address homeAddress = memberJson.getHouseholdContact().getHomeAddress();

		Address mailingAddress = memberJson.getHouseholdContact().getMailingAddress();

		enrolleeCoreDto.setHomeAddressDto(populateAddress(homeAddress));
		enrolleeCoreDto.setMailingAddressDto(populateAddress(mailingAddress));
	}

	private void populateTobaccoUsageDetails(EnrolleeCoreDto enrolleeCoreDto, SsapApplicant memberDB) {
		String tobaccoUsage = "U";
		if (memberDB.getTobaccouser() != null) {
			tobaccoUsage = StringUtils.equalsIgnoreCase(ReferralConstants.Y, memberDB.getTobaccouser()) ? "T" : "N";
		}

		enrolleeCoreDto.setTobaccoUsageLkpStr(tobaccoUsage);
	}


	private LocationCoreDto populateAddress(Address address) {
		LocationCoreDto locationCoreDto = null;
		if (address != null) {
			locationCoreDto = new LocationCoreDto();
			locationCoreDto.setAddress1(address.getStreetAddress1());
			locationCoreDto.setAddress2(address.getStreetAddress2());
			locationCoreDto.setCity(address.getCity());
			locationCoreDto.setState(address.getState());
			locationCoreDto.setZip(address.getPostalCode());
			locationCoreDto.setCountycode(address.getCountyCode());

		}
		return locationCoreDto;
	}

	private Map<String, String> populateRaceDetails(HouseholdMember memberJson) {

		EthnicityAndRace ethnicityAndRace = memberJson.getEthnicityAndRace();
		Map<String, String> raceMap = null;
		if (ethnicityAndRace != null && ReferralUtil.listSize(ethnicityAndRace.getRace()) != 0) {

			raceMap = new HashMap<String, String>();
			for (Race race : ethnicityAndRace.getRace()) {
				raceMap.put(race.getCode(), race.getLabel() != null ? race.getLabel() : race.getOtherLabel());
			}

		}

		return raceMap;
	}

	private List<Map<String, String>> populateRelationship(HouseholdMember memberJson, String personId, SsapApplication currentApplication, HouseholdMember primaryApplicantJson) {
		List<Map<String, String>> relatioshipMapList = new ArrayList<Map<String, String>>();
		List<BloodRelationship> bloodRelatioshipList = primaryApplicantJson.getBloodRelationship();
		Map<String, String> applicantGuidMapwithPersonId = populateApplicantGuidWithPersonId(currentApplication.getSsapApplicants());

		for (BloodRelationship bloodRelationship : bloodRelatioshipList) {
			if (personId.equalsIgnoreCase(bloodRelationship.getIndividualPersonId())) {
				Map<String, String> relationshipMap = new HashMap<String, String>();
				relationshipMap.put("relationshipCode", bloodRelationship.getRelation());
				relationshipMap.put("memberId", applicantGuidMapwithPersonId.get(bloodRelationship.getRelatedPersonId()));
				relatioshipMapList.add(relationshipMap);
			}
		}

		return relatioshipMapList;
	}

	private Map<String, String> populateApplicantGuidWithPersonId(List<SsapApplicant> ssapApplicants) {
		Map<String, String> applicantGuidMapwithPersonId = new HashMap<String, String>();
		for (SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidMapwithPersonId.put(String.valueOf(ssapApplicant.getPersonId()), ssapApplicant.getApplicantGuid());
		}
		return applicantGuidMapwithPersonId;

	}

	

	
	private String formatDate(Date date){
		DateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT_MM_DD_YYYY);
		return dateFormat.format(date);
	}
	private String formatDate(Date date,String pattern){
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		return dateFormat.format(date);
	}
	
	private Location getLocation(SsapApplicant ssapApplicant) {
		if (ssapApplicant.getMailiingLocationId() != null){
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else if (ssapApplicant.getOtherLocationId() != null){
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		}
		return null;
	}


}

