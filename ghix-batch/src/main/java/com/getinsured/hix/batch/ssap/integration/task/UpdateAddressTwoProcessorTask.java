package com.getinsured.hix.batch.ssap.integration.task;

import com.getinsured.hix.batch.ssap.service.UpdateAddressTwoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.Timestamp;

public class UpdateAddressTwoProcessorTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(UpdateAddressTwoProcessorTask.class);
    private static volatile Boolean isBatchRunning = false;

    private String absPathWithNameAndExt;
    private String ssapAppIdsFilePath;
    private String jobId;
    private UpdateAddressTwoService updateAddressTwoService;

    public String getAbsPathWithNameAndExt() {
        return absPathWithNameAndExt;
    }

    public void setAbsPathWithNameAndExt(String absPathWithNameAndExt) {
        this.absPathWithNameAndExt = absPathWithNameAndExt;
    }

    public String getSsapAppIdsFilePath() {
        return ssapAppIdsFilePath;
    }

    public void setSsapAppIdsFilePath(String ssapAppIdsFilePath) {
        this.ssapAppIdsFilePath = ssapAppIdsFilePath;
    }

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public UpdateAddressTwoService getUpdateAddressTwoService() {
        return updateAddressTwoService;
    }

    public void setUpdateAddressTwoService(UpdateAddressTwoService updateAddressTwoService) {
        this.updateAddressTwoService = updateAddressTwoService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
            }

            if (!ssapAppIdsFilePath.isEmpty() && !absPathWithNameAndExt.isEmpty()) {
                updateAddressTwoService.processUpdateAddressTwo(ssapAppIdsFilePath, absPathWithNameAndExt);
            } else {
                LOGGER.debug("execute::input file paths not valid");
            }
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        }

        return RepeatStatus.FINISHED;
    }
}
