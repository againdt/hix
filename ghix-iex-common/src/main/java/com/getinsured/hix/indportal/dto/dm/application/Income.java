package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class Income {
	private ArrayList < Object > employer;
	 private String medicaidIncomeStatus;
	 private String chipIncomeStatus;
	 private String medicaidChipIncomeStatusReason;
	 private Float attestedMonthlyIndividualIncomeAmt;
	 private Float attestedAnnualizedAptcIndividualIncomeAmt;
	 private String irsDataStatus;
	 private String irsDataStatusReason;


	 // Getter Methods 

	 public String getMedicaidIncomeStatus() {
	  return medicaidIncomeStatus;
	 }

	 public String getChipIncomeStatus() {
	  return chipIncomeStatus;
	 }

	 public String getMedicaidChipIncomeStatusReason() {
	  return medicaidChipIncomeStatusReason;
	 }

	 public Float getAttestedMonthlyIndividualIncomeAmt() {
	  return attestedMonthlyIndividualIncomeAmt;
	 }

	 public Float getAttestedAnnualizedAptcIndividualIncomeAmt() {
	  return attestedAnnualizedAptcIndividualIncomeAmt;
	 }

	 public String getIrsDataStatus() {
	  return irsDataStatus;
	 }

	 public String getIrsDataStatusReason() {
	  return irsDataStatusReason;
	 }

	 // Setter Methods 

	 public void setMedicaidIncomeStatus(String medicaidIncomeStatus) {
	  this.medicaidIncomeStatus = medicaidIncomeStatus;
	 }

	 public void setChipIncomeStatus(String chipIncomeStatus) {
	  this.chipIncomeStatus = chipIncomeStatus;
	 }

	 public void setMedicaidChipIncomeStatusReason(String medicaidChipIncomeStatusReason) {
	  this.medicaidChipIncomeStatusReason = medicaidChipIncomeStatusReason;
	 }

	 public void setAttestedMonthlyIndividualIncomeAmt(Float attestedMonthlyIndividualIncomeAmt) {
	  this.attestedMonthlyIndividualIncomeAmt = attestedMonthlyIndividualIncomeAmt;
	 }

	 public void setAttestedAnnualizedAptcIndividualIncomeAmt(Float attestedAnnualizedAptcIndividualIncomeAmt) {
	  this.attestedAnnualizedAptcIndividualIncomeAmt = attestedAnnualizedAptcIndividualIncomeAmt;
	 }

	 public void setIrsDataStatus(String irsDataStatus) {
	  this.irsDataStatus = irsDataStatus;
	 }

	 public void setIrsDataStatusReason(String irsDataStatusReason) {
	  this.irsDataStatusReason = irsDataStatusReason;
	 }
	}
