package com.getinsured.ssap.model.financial;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Describes IFSV name of the household member {@link IfsvHouseholdMember}.
 * @author Yevgen Golubenko
 * @since 6/11/19
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Name {
  private String firstName;
  private String middleName;
  private String lastName;

  public Name(String firstName, String lastName) {
    this.firstName = firstName;
    this.lastName = lastName;
  }

  public Name(String firstName, String middleName, String lastName) {
    this.firstName = firstName;
    this.middleName = middleName;
    this.lastName = lastName;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

  public String getMiddleName() {
    return middleName;
  }

  public void setMiddleName(String middleName) {
    this.middleName = middleName;
  }

  @Override
  public String toString() {
    return "Name{" +
      "firstName='" + firstName + '\'' +
      ", middleName='" + middleName + '\'' +
      ", lastName='" + lastName + '\'' +
      '}';
  }
}
