/*
 * Custom script for Household Page
 */

$(function(){
	
	//Household page model
	PrescreenHouseholdMemberModel = Backbone.Model.extend({

		initialize : function() {
		},
	
		defaults: {
			memberDesc: "",
			dateOfBirth: "",
			pregnant : false,
			seekingCoverage : true,
			relationshipWithClaimer : ""
      	}
		
	});
	
	//Household members Collection
	PrescreenHouseholdCollection = Backbone.Collection.extend({
		model : PrescreenHouseholdMemberModel
	});
	
	//Household Model
	PrescreenHousehold = Backbone.Model.extend({
		
		initialize : function() {
		},
	
		defaults: {
			householdMembers : new PrescreenHouseholdCollection()
		}
	});
	
	//Model and View for claimant
	claimantModel = new PrescreenHouseholdMemberModel({
		relationshipWithClaimer : 'self'
	});
	
	var claimantBindings = {
			dateOfBirth: '#selfDob',
			pregnant : '[name=isSelfPregnant]',
			seekingCoverage : '[name=isSelfSeekingCoverage]'
	};
	
	claimantUpdateView = new UpdateView({
		el : $ ('#householdForm'),
		model : claimantModel,
		bindings : claimantBindings
	});
	
	//Creating Household Collection
	prescreenHouseholdMembers = new PrescreenHouseholdCollection();
	prescreenHouseholdDependents = new PrescreenHouseholdCollection();
	
});