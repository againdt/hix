<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<ul class="nav nav-list">
	<input type="hidden" id="currentPage" value='<%=request.getParameter("pageName")%>'>

    <li id="issuerDetailsTab"><a id="issuerDetailsLink" href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
    <li id="issuerRepTab"><a id="issuerRepLink" href="<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
    <c:if test="${exchangeType != 'PHIX' && exchangeState != 'ID' && exchangeState != 'MN' }">
            <li id="financialInfoTab"><a id="financialInfoLink" href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
    </c:if>
    <li id="companyProfileTab"><a id="companyProfileLink" href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
    <li id="individualProfileTab"><a id="individualProfileLink" href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
    <li id="accreditationTab"><a id="accreditationLink" href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
    <li id="certStatusTab"><a id="certStatusLink" href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
    <li id="issuerHistoryTab"><a id="issuerHistoryLink" href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
    <li id="crossWalkTab"><a id="crossWalkLink" href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
    <c:if test="${exchangeState == 'ID'}">
		<li id="netTransTab"><a id="netTransLink" href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
    </c:if>
    <c:if test="${activeRoleName == 'OPERATIONS' || activeRoleName == 'ISSUER_ADMIN' }">
            <li id="paymentInfoTab"><a id="paymentInfoLink" href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
    </c:if>
</ul>

<script type="text/javascript">
  $(document).ready(function(){
	$("#"+$("#currentPage").val()+"Tab").addClass("active");
	$("#"+$("#currentPage").val()+"Link").attr("href", "#");
  });
</script>