package com.getinsured.hix.batch.plandisplay.processor;

import org.apache.solr.common.SolrInputDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import com.getinsured.hix.batch.plandisplay.reader.PlanDisplayDentalBenefitMapper;

public class PlanDisplayDentalBenefitRecordProcessor implements ItemProcessor<PlanDisplayDentalBenefitMapper, SolrInputDocument>{
	private static Logger logger = LoggerFactory.getLogger(PlanDisplayDentalBenefitRecordProcessor.class);

	@Override
	public SolrInputDocument process(PlanDisplayDentalBenefitMapper fromReader) throws Exception {
		logger.info("In process ---> "+fromReader.getName());
		return prepareSolrDocument(fromReader);
	}
	
	private SolrInputDocument prepareSolrDocument(PlanDisplayDentalBenefitMapper providerData){
		
		SolrInputDocument solrInputDocument = providerData.getSolrInputDocument();
		logger.info("Processing Data completed : "+providerData.getPlanId());
		return solrInputDocument;
	}
}
