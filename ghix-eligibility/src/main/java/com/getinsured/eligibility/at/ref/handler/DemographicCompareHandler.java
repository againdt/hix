package com.getinsured.eligibility.at.ref.handler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.iex.ssap.Address;

/**
 * @author chopra_s
 * 
 */
@Component("demographicCompareHandler")
@Scope("singleton")
public class DemographicCompareHandler {

	public void processCompare(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> enrolledList = compareMainDTO.getEnrolledApplication().getApplicants();
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		CompareApplicantDTO currentApplicant = null;
		
		for (CompareApplicantDTO enrolledApplicant : enrolledList) {
			if (enrolledApplicant.isEnrolled()) {
				currentApplicant = findCompareApplicant(enrolledApplicant, currentList);
				if (currentApplicant != null && ApplicantStatusEnum.NO_CHANGE.value().equals(currentApplicant.getStatus())) {
					MatchHandler.handleCompareLogic(enrolledApplicant, currentApplicant, compareMainDTO.getEnrolledApplication().getMailingAddress());
				}
			}
		}
	}
	
	public void processNonFinancialCompare(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> enrolledList = compareMainDTO.getEnrolledApplication().getApplicants();
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();
		CompareApplicantDTO currentApplicant = null;
		
		for (CompareApplicantDTO enrolledApplicant : enrolledList) {
			if (enrolledApplicant.isEnrolled()) {
				currentApplicant = findCompareNFApplicant(enrolledApplicant, currentList);
				if (currentApplicant != null) {
					MatchHandler.handleCompareLogic(enrolledApplicant,currentApplicant,compareMainDTO.getEnrolledApplication().getMailingAddress());
				}
			}
		}
	}

	private CompareApplicantDTO findCompareApplicant(CompareApplicantDTO enrolledApplicant, List<CompareApplicantDTO> currentList) {
		for (CompareApplicantDTO currentApplicant : currentList) {
			if (((null!=currentApplicant.getExternalApplicantId() && null!=enrolledApplicant.getExternalApplicantId()) && StringUtils.equalsIgnoreCase(currentApplicant.getExternalApplicantId(), enrolledApplicant.getExternalApplicantId()))   ||  StringUtils.equalsIgnoreCase( currentApplicant.getApplicantGuid(),enrolledApplicant.getApplicantGuid())) {
				return currentApplicant;
			}
		}
		return null;
	}
	private CompareApplicantDTO findCompareNFApplicant(CompareApplicantDTO enrolledApplicant, List<CompareApplicantDTO> currentList) {
		for (CompareApplicantDTO currentApplicant : currentList) {
			if (currentApplicant.getApplicantGuid().equals(enrolledApplicant.getApplicantGuid())) {
				return currentApplicant;
			}
		}
		return null;
	}


	private static class MatchHandler {
		private static void handleCompareLogic(CompareApplicantDTO enrolledApplicant, CompareApplicantDTO currentApplicant,Address houseHoldMailingAddress) {
			enrolledApplicant.compareDemographic(currentApplicant,houseHoldMailingAddress);
		}
	}
}
