package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentIncomeInfoType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.CurrentHouseholdIncomeType
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class CurrentIncomeInfoTypeWrapper implements JSONAware {
	
	private ResponseMetadataWrapper responseMetadata;
	private PersonTypeWrapper employeeInformation;
	private CurrentHouseholdIncomeTypeWrapper currentIncomeInformation;
	
	private static final String METADATA_KEY = "ResponseMetadata";
	private static final String EMPLOYEE_KEY = "EmployeeInformation";
	private static final String CURRENT_INCOME_KEY = "CurrentIncomeInformation";
	
	public CurrentIncomeInfoTypeWrapper(CurrentIncomeInfoType currentIncomeInfoType){
		
		if(currentIncomeInfoType == null){
			return;
		}
		
		this.responseMetadata = new ResponseMetadataWrapper(currentIncomeInfoType.getResponseMetadata());
		this.employeeInformation = new PersonTypeWrapper(currentIncomeInfoType.getEmployeeInformation());
		this.currentIncomeInformation = new CurrentHouseholdIncomeTypeWrapper(currentIncomeInfoType.getCurrentIncomeInformation());
		
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public PersonTypeWrapper getEmployeeInformation() {
		return employeeInformation;
	}

	public CurrentHouseholdIncomeTypeWrapper getCurrentIncomeInformation() {
		return currentIncomeInformation;
	}
	

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(METADATA_KEY, this.responseMetadata);
		obj.put(EMPLOYEE_KEY, this.employeeInformation);
		obj.put(CURRENT_INCOME_KEY, this.currentIncomeInformation);
		return obj.toJSONString();
	}
	
	
}
