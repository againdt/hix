<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="gutter10">	
	<div class="row-fluid margin20-t">
	    <div class="span10 offset1" id="rightpanel">		    	
			<div class="alert alert-info margin20-t">
				<h1 class="margin20-tb margin20-l">Email change</h1>
				
				<p class="margin20-b margin20-l">
					<c:choose>
						<c:when test="${!empty errorMsg}">
							<c:out value="${errorMsg}"></c:out>
						</c:when>
						<c:otherwise>
							Your email has been successfully changed. Click on Login button to login with new email. <a href="<c:url value="/account/user/login"/>" class="btn btn-primary pull-right">Login</a>		
						</c:otherwise>	
					</c:choose>
					
					
				</p>
			</div>
		</div>
	</div>
</div>