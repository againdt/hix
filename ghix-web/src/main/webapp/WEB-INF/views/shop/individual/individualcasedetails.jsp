<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/secure-url.tld" prefix="surl"%>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%@ page import="java.util.HashMap" %>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<%-- <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" /> --%>

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<c:set var="individualUserId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
<c:set var="isIndividualUserExists" value="${isIndividualUserExists}" ></c:set>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<script type="text/javascript">
  $(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href = $(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:270px;" scrolling="no"></iframe></div></div>').modal();
      }
    });
  });
  
  function closeIFrame() {
    $("#newcommentiframe").modal('hide');
    window.location.href = '/hix/individual/individualcomments/'+'${individualUserId}';
  }
  
  function closeCommentBox() {
	  $("#newcommentiframe").modal('hide');	  
	  var url = '/hix/individual/individualcomments/'+'${individualUserId}';
	  window.location.assign(url);
  }
  
  function setPopupValue() {
		if(document.getElementById("frmPopup").elements['indchkshowpopup'].checked) {
			document.frmPopup.showPopupInFuture.value = "N";
		}
  }
</script>

<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/broker/individuals"/>"><spring:message
						code="label.individuals" /></a></li>
				<li><spring:message code="label.brkactive"/></li>
				<li>${individualUser.firstName} ${individualUser.lastName}</li>
				<li><spring:message code="label.employeeDetails.Summary"/></li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
  <div style="font-size: 14px; color: red">
    <c:if test="${errorMsg != 'false'}">
      <c:out value="${errorMsg}"></c:out>
    </c:if>
  </div>
</div>


<!--  Latest UI -->
	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>


	<h1>
    ${individualUser.firstName} ${individualUser.lastName}
	</h1>
  <div class="row-fluid">
	<c:set var="encryptedId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
    <div id="sidebar" class="span3">
        <h4 class="header"></h4>
        <ul class="nav nav-list">         
          <li class="active"><spring:message code="label.employeeDetails.Summary"/></li>
          <c:if test="${isCACall == 'FALSE'}">
          <li class="link"><a
            href="/hix/individual/individualincomeinfo/${individualUserId}"><spring:message code="label.indIncomeInformation"/></a></li>
            </c:if>
			<!--  <li class="link">
				<c:url value="/inbox/secureInboxSearch" var="searchURL">
				   <c:param name="searchText" value="${individualUser.firstName} ${individualUser.lastName}"/>
				</c:url>
				<a href="${searchURL}">Messages</a>
			</li>-->
          <li class="link"><a href="/hix/individual/individualcomments/${individualUserId}"><spring:message code="label.employeeDetails.Comments"/></a></li>
        </ul>
      <br>
        <h4 class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
        <ul class="nav nav-list">  
        <li><a name="addComment" href="<c:url value="/individual/newcomment?target_id=${individualUserId}&target_name=DESIGNATEBROKER&individualName=${individualUser.firstName} ${individualUser.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.employeeDetails.NewComments"/></a></li>
              
       <c:if test="${userActiveRoleName != 'admin' && userActiveRoleName != 'broker_admin' && userActiveRoleName != 'assisterenrollmententity'}">
        	<li>
		        <c:choose>
					<c:when test="${showSwitchRolePopup == \"N\"}">					
						<a href="#" onclick="document.frmPopup.submit();"  role="button" ><i class="icon-eye-open"></i> <spring:message code="label.indViewIndividualAccount"/></a>
					</c:when>
					<c:otherwise>					
						<c:if test="${showPopupInFuture == null}">												
							<a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.indViewIndividualAccount"/></a>  
						</c:if>
						<c:if test="${showPopupInFuture != null}">												
							<a href="#" onclick="document.frmPopup.submit();" role="button" class="" ><i class="icon-eye-open"></i><spring:message code="label.indViewIndividualAccount"/></a>
						</c:if>						
					</c:otherwise>
				</c:choose>
			</li>
		</c:if>
<!--           <li><a href="#"><i class="icon-eye-open"></i>View Individual Account</a></li> -->
          <!-- <li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"><i class="icon-envelope-unread"></i> Compose Message</a></li> -->
          <!--  <li><a href="#new-msg" class="btn span11 btn-block btn-primary" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">-->
          <c:if test="${isConsumerActivated == false}">
           		<li><a name="resendActivationLink" href="#" id ="resendActivationLink" class=""><i class="icon-envelope"></i><spring:message code="label.agent.individual.activationLink"/></a></li>
          </c:if>           
        </ul>
    </div>
    <!-- Modal -->
    
    <div id="rightpanel" class="span9">

      <form class="form-horizontal" id="" name="" action="" method="POST">
      	<div class="gray">
      		<h4 class="header"><spring:message code="label.employeeDetails.Summary"/>	
				<a class="btn btn-medium pull-right ind-edit-btn"><spring:message  code="label.edit"/></a>
			</h4>
      	</div>
        <table class="table table-border-none" role="presentation">
	          <tbody>
	            <tr>
	              <td class="span3 txt-right"><spring:message code="label.employeeDetails.PrimaryApplicant"/></td>
	              <td><strong>${individualUser.firstName} ${individualUser.lastName}</strong></td>
	            </tr>
	            <tr>
	              <td class="txt-right"><spring:message code="label.IndividualAddress"/></td>
	              <td><strong>
                  ${individualUser.address1} <br />
                  ${individualUser.address2} 
                  <c:if test="${individualUser.city !=null && individualUser.city!=\"\"}">
					${individualUser.city},
				  </c:if>
                  ${individualUser.state} 
                  ${individualUser.zip}
              	  </strong></td>
	            </tr>
	            <c:choose>
					<c:when test="${isCACall == 'TRUE'}">
	            <tr>
					<td class="txt-right"><spring:message code="label.employeeDetails.PhoneNumber"/></td>
					<td><strong>${individualUser.phone}</strong></td>
				</tr>
				<tr>
				<td class="txt-right"><spring:message code="label.employeeDetails.EmailAddress"/></td>
				<td><strong>${individualUser.email}</strong></td>
				</tr>
				<tr>
				<td class="txt-right"><spring:message code=""/><spring:message code="label.enrollmentStatus"/></td>
				<td><strong>${individualUser.enrollmentStatus}</strong></td>
				<tr>
				<tr>
				<td class="txt-right"><spring:message code="label.agent.employers.EligibilityStatus"/></td>
				<td><strong>${individualUser.eligibilityStatus}</strong></td>
	           </tr>
	           </c:when>
	           <c:otherwise>
	          
	          	<tr>
				<td class="txt-right"><spring:message code="label.employeeDetails.PhoneNumber"/></td>
					<td>
						<strong>
							<c:choose>
							<c:when test="${individualUser.phoneNumber!=null}">
							<c:set var="phoneNumber" value="${individualUser.phoneNumber}"></c:set>
							<c:set var="formattedNumber" value="(${fn:substring(phoneNumber,0,3)})${fn:substring(phoneNumber,3,6)}-${fn:substring(phoneNumber,6,10)}"></c:set>
							${formattedNumber}
							</c:when>
							<c:otherwise>
							${individualUser.phoneNumber}
							</c:otherwise>
							</c:choose>
						</strong>
					</td>
			</tr>
				<tr>
				<td class="txt-right"><spring:message code="label.entity.email"/></td>
				<td><strong>${individualUser.emailAddress}</strong></td>
				</tr>
				<tr>
				<td class="txt-right"><spring:message code="label.agent.employers.EligibilityStatus"/></td>
				<td><strong>${individualUser.eligibilityStatus}</strong></td>
				<tr>
				<tr>
				<td class="txt-right"><spring:message code="label.indApplicationStatus"/></td>
				<td><strong>${individualUser.applicationStatus}</strong></td>
	           </tr>
	             </c:otherwise>
	           </c:choose>
			</tbody>
		</table>
		<div>
	           <hr/>
	           <div class="txt-right">
		           <%-- <a class="btn btn-medium pull-right ind-edit-btn"><spring:message  code="label.edit"/></a> --%>
				</div>
		</div>
<%-- 		<c:if test="${isCACall == 'FALSE'}">
		<h4 class="lightgray">Dependents</h4>
		<table class="table table-border-none">
			<tbody>
            <tr>
              <td class="span3 txt-right">Spouse</td>
              <td><strong>N&#92;A</strong></td>
            </tr>
            <tr>
              <td class="txt-right">Daughter</td>
              <td><strong>Alice McFarson</strong> <span>&nbsp; Age <strong>15</strong></span></td>
            </tr>
            <tr>
              <td class="txt-right">Son</td>
              <td><strong>George McFarson</strong> <span>&nbsp; Age <strong>12</strong></span></td>
            </tr>
          </tbody>
        </table>
        </c:if> --%>
      </form>

      <!-- Modal -->
      <div id="viewindModal" class="modal hide fade" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">&times;</button>
          <h3 id="myModalLabel"><spring:message code="label.indViewIndividualAccount"/></h3>
        </div>
        <div class="modal-body">
          <p><spring:message code="label.indClickingIndividualView"/> ${individualUser.firstName} ${individualUser.lastName}.<spring:message code="label.IndThroughThisPortal"/>
          </p>
          <p><spring:message code="label.indProceedtoIndividualview"/></p>
        </div>
        
        <c:set var="idAsString">${individualUser.id}</c:set>
        
        <jsp:useBean id="moduleParamsMap" class="java.util.HashMap"/>
		<c:set target="${moduleParamsMap}" property="switchToModuleName" value="individual" />
		<c:set target="${moduleParamsMap}" property="switchToModuleId" value="${idAsString}" />
		<c:set target="${moduleParamsMap}" property="switchToResourceName" value="${individualUser.firstName} ${individualUser.lastName}" />
		<surl:secureUrl paramMap="${moduleParamsMap}" var="ecyptedModuleParamsMap"/>
		
        
        <form action="<c:url value='/account/user/switchUserRole'/>" id="frmPopup" name="frmPopup">
        	<div class="modal-footer clearfix">   
        			<label for="showPopupInFuture" class="aria-hidden">Show pop up in future checkbox</label> <!-- Accessibility Use -->    	
	 				<input class="pull-left"  type="checkbox" id="showPopupInFuture" name="showPopupInFuture" <c:if test="${showPopupInFuture != null}">checked</c:if>  />
	    			<div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
				    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
					
					<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="<encryptor:enc value="individual"/>" />
					<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="<encryptor:enc value="${individualUser.id}"/>" />
					<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="<encryptor:enc value="${individualUser.firstName} ${individualUser.lastName}"/>" />
				 
				 	<input type="hidden" name="ref" id="ref" value="${ecyptedModuleParamsMap}" />
				 
				   <button class="btn btn-primary" type="submit" ><spring:message code="label.indIndividualView"/></button >
			</div>			
		</form>
      </div>
      <!-- Modal end -->
    </div>
  </div>
  <!-- row-fluid -->
  </div>
  <!-- gutter10 -->
<!--  Latest UI -->
  <script type="text/javascript">
  function isInvalidCSRFToken(xhr) {
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
			rv = true;
		}
		return rv;
	}
  
$("document").ready(function (){
	$(".ind-edit-btn").click(function (){
		var varboolean = ${isIndividualUserExists}; 
		if(varboolean) {
			$(
					'<div id="ind-dialogue" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">'
						+ '<div class="modal-header">'
							+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
							+ '<h3 id="myModalLabel">Failure!</h3>'
						+ '</div>'
						+ '<div class="modal-body">'
							+ '<p> This action is only allowed for individual consumers who have not completed the sign up process. <br/>'
							+'${individualUser.firstName} has already completed the sign up process.</p>'
						+ '</div>'
						+'<div class="modal-footer txt-center">'
						+ '<button type="button" class="btn btn-primary" data-dismiss="modal" aria-hidden="true">OK</button>'
						+ '</div>'
						
					+ '</div>').modal();
			
		}
		else {
			$(this).attr("href", "<c:url value='/broker/editindividual/${encryptedId}'/>")
			$(this).click();
		}		
	});
	
	
	$("#resendActivationLink").click(function () {
		if(${isConsumerActivated}) {
		    $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
		       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		   }
		else if(${isIndividualUserExists}) {
		    $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
				       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
				   }
		   else {
			   	
			   $.post("<c:url value='/broker/sendconsumeractivationlink/${encryptedId}'><c:param name='${df:csrfTokenParameter()}'> <df:csrfToken plainToken='true'/> </c:param></c:url>", null,
				   function(data,status){
					   if(data.indexOf('Missing CSRF token') != -1){
							$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
							return false;
						}
						if(status === 'success') {
					    	 if(-1 == data.search("SUCCESS")) { 
					    		 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px; "><P>   <spring:message code="label.agent.individual.activationLink.message.success.negative.pre"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}<spring:message code="label.agent.individual.activationLink.message.success.negative.post"/>' + data + '.</P></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					    	 else {
					    		 $('<div class="modal popup-address" ><div class="modal-header " style="border-bottom:0; "><h3>Success!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p><spring:message code="label.agent.individual.activationLink.message.success.positive"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					     }
					     else {
					    	 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p>   <spring:message code="label.agent.individual.activationLink.message.failure"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					     }
					   });
		   } 
		
		
	});
});
</script>
