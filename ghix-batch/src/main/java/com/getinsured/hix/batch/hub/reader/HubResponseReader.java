package com.getinsured.hix.batch.hub.reader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemStream;
import org.springframework.batch.item.ItemStreamException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.hub.model.HubResponse;

/**
 * HIX-38366
 * Class to read PENDING CLOSE records from HUB_RESPONSES table
 * This is the reader class for the VLP Close Case Job
 * 
 * @author - Nikhil Talreja
 * @since - 22-May-2014
 * 
 */
@Component("HubResponseReader")
public class HubResponseReader implements ItemReader<HubResponse>, ItemStream{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(HubResponseReader.class);
	
	private static final int FETCH_SIZE = 100;
	
	@Autowired private DataSource dataSource;
	private ResultSet resultSet;
	private Connection conn;
	private Statement statement;
	
	private static final String QUERY = "SELECT "+ 
		"response.ID, response.HUB_CASE_NUMBER, response.STATUS, response.RETRY_COUNT"+
		" FROM "+
			"HUB_RESPONSES response"+
		" WHERE "+
			" response.STATUS = 'PENDING_CLOSE' AND response.HUB_CASE_NUMBER IS NOT NULL AND response.HUB_CASE_NUMBER <> 'null'";
	
	@Override
	public HubResponse read() {
		HubResponseReaderMapper mapper = new HubResponseReaderMapper();
		LOGGER.info("Reading record from HUB_RESPONSES table");
		try{
			if(this.resultSet.next()){
				return mapper.mapRow(this.resultSet,0);
			}
		}
		catch(SQLException e){
			LOGGER.error("Failed to get records from DB",e);
		}
		return null;
	}

	@Override
	public void open(ExecutionContext executionContext)	{
		try {
			this.conn = this.dataSource.getConnection();
			this.statement = conn.createStatement();
			this.statement.setFetchSize(FETCH_SIZE);
			this.resultSet = this.statement.executeQuery(QUERY);
		} catch (SQLException e) {
			throw new ItemStreamException(e.getMessage(),e);
		}
		LOGGER.info("Opened Item stream for reading HUB responses");
		
	}

	@Override
	public void update(ExecutionContext executionContext){
		LOGGER.debug("Update method not implemented");
		
	}

	@Override
	public void close() {
		LOGGER.info("Closing DB connection");
		if(this.conn != null){
			try {
				this.statement.close();
				this.conn.close();
			} catch (SQLException e) {
				LOGGER.error("Failed to close DB connection, Ignoring",e);
			}
		}
		
	}

}
