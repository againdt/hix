<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri ="/WEB-INF/tld/ghix-encryptor.tld" prefix="encryptor" %>

<jsp:include page="checkPreLinkApplData.jsp" />

<!-- SSAP Application Details JSP  -->
<div class="gutter10">
	<div class="row-fluid">
		<h1>${application.name}'s Application</h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="header">
				<h4>Individual Information</h4>
			</div>
			<dl>
  				<dt>Name</dt>
    				<dd>${application.name}</dd>
  				<dt>Zip Code</dt>
    				<dd>${application.zip}</dd>
    			<dt>Email Address</dt>
    				<dd>${application.emailAddress}</dd>
  				<dt>Phone Number</dt>
    				<dd>${application.phoneNumber}</dd>
    			<%-- <dt>DOB</dt>
    				<dd>${application.dob}</dd>	 --%>
			</dl>
			
			<div class="header margin20-t">
				<h4>Application Information</h4>
			</div>
			<dl>
  				<dt>Referral Id</dt>
    				<dd>${application.referralId}</dd>
  				<dt>Application Status</dt>
    				<dd>${application.status}</dd>
    			<dt>Created on</dt>
    			    <dd>${application.created}</dd>
    			    
			</dl>
		</div>

		<div class="span9" id="rightpanel">
			<c:if test="${errorMsg != null && errorMsg != ''}">
				<div class="alert alert-danger" id="applinkErrorDiv">
					<p><c:out value="${errorMsg}"></c:out></p>
					<c:remove var="errorMsg" />
				</div>
			</c:if>
			
			<c:choose>
			<c:when test="${resultSize > 0}">
				<div class="header">
					<h4 class="pull-left" id="pageTitle">Existing Record Matches (${resultSize})</h4>
				</div>
					<table class="table table-striped" id="cmr_table">
						<thead>
							<tr class="header">
								<th></th>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Phone Number</th>
								<th>Email Address</th>
								<th>Account ?</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${houseHoldList}" var="household"
								varStatus="count">
								<tr>
									<td><input type="radio" name="applRad"
										value="${count.count}"></td>
									<td>${household[0]}</td>
									<td>${household[1]}</td>
									<td>${household[2]}</td>
									<td>${household[3]}</td>
									<td class="hide"><encryptor:enc value="${household[4]}" isurl="true"/></td>	
									<%-- <td>${household[5]}</td> --%>
									<td>
									<c:choose>
									<c:when test="${household[5] != null}">	Yes</c:when>
									<c:otherwise>No</c:otherwise>	
									</c:choose>
									</td>
								</tr>
								
							</c:forEach>
						</tbody>
					</table>
				<div class="pagination txt-center">
					<dl:paginate resultSize="${resultSize + 0}"	pageSize="${pageSize + 0}" />
				</div>
				<div class="span10">
					<div style="font-size: 14px; color: red" id="linkCustErrorMsg"> </div>
				</div>
			</c:when>
			<c:otherwise>
					<h4 class="alert alert-info">
						<spring:message code='label.norecords' />
					</h4>
				</c:otherwise>
			</c:choose>
			<div>
			<div class="row-fluid">
				<div class="span4 pull-left">
					<%-- <a href="#" id="cancelButton" class="btn btn-small" onclick="window.history.go(-1); return false;">Back</a> --%>
					<a class="btn btn-small" href="<c:url value="/crm/member/searchapplicants"/>"><spring:message  code="label.cancel"/></a> 
				</div>
				<div class="span4">
					<c:if test="${resultSize > 0}">
					<a href="javascript:showlinkDialog()" class="btn btn-small btn-primary" name="btnLink" id="linkToCustumer">Link to Existing Account</a>
					</c:if>
				</div>
				<div class="span4 pull-right">
					<a onclick="javascript:AddNewIndividual()" href="#" class="btn btn-small pull-right">Create New Individual</a>
				</div>
			</div>

		</div><!-- #rightpanel -->

		<!-- Link Household to application dialog  -->
		<div id="confirmLinkCust" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmLinkCustDlg" aria-hidden="true" data-keyboard="false" data-backdrop="static">
			<div class="confirmLinkCustHeader">
				<div class="header">
					<div id="cancelerr" style="font-size: 14px; color: red"></div>
					<h4 class="pull-left">Confirm your action</h4>
					<button aria-hidden="true" data-dismiss="modal"	id="crossCloseCancel" class="dialogClose" title="x" type="button">
						<spring:message code="btn.crossClose" />
					</button>
				</div>
			</div>

			<div class="modal-body">
				<div class="control-group">
					<div class="controls">
						<label><b>You have selected:</b> </label>
					</div>
					<div class="controls" align="center">
						<label id="cmrName"></label> 
						<label id="cmrPhno"></label> 
						<label id="cmremail"></label>
					</div>
				</div>
			</div>
			<div class="modal-footer clearfix">
				<button class="btn pull-left" aria-hidden="true" data-dismiss="modal" id="CancellinkBtn"> <spring:message code="button.cancel" /></button>
				<button class="btn btn-primary"	id="submitBtn">Link</button>
			</div>
			<div id="errorMsg">
				<label id="errorLabel"></label>
			</div>
			<div id="ajaxloader"
				style="display: none; width: 80px; height: 80px; position: absolute; top: 50%; left: 50%; padding: 2px; margin-top: -52px; margin-left: -47px;">
				<img src="<c:url value="/resources/images/Eli-loader.gif" />" alt="Required!" width="80" height="80" />
			</div>
		</div>
		</div>
	</div>
	<input type="hidden" value="<encryptor:enc value="${application.id}" isurl="true"/>" name="encssapApplicationId" id="encssapApplicationId">
</div>
<form action="<c:url value="/cap/consumer/addconsumer" />" method="post" id="frmAddNewIndividual">
<df:csrfToken/>
<input type="hidden" id="applicationJson" name="applicationJson"  />
</form>

	<script type="text/javascript">
		function showlinkDialog() {
			var radVal = $('input[type="radio"]:checked').val();

			$("#linkCustErrorMsg").text("");
			if (radVal != undefined || radVal != null) {

				var table = document.getElementById("cmr_table");				
				var rowData = table.rows[radVal].getElementsByTagName("td");
				var cmrFName,cmrLName;
				
				if(rowData[1].innerHTML !=null && rowData[2].innerHTML !=null)
				{
					var cmrFName = rowData[1].firstChild.nodeValue;	
					var cmrLName = rowData[2].firstChild.nodeValue;
					$("#cmrName").text(cmrFName + " " + cmrLName); // FL & LN
				}
				
				
				
				//$("#cmrName").text(cmrFName + " " + cmrLName); // FL & LN
				if(rowData[3].innerHTML != ""){
					
					$("#cmrPhno").text(rowData[3].firstChild.nodeValue); // PHno
				} 
				
				
				if(rowData[4].innerHTML != ""){
				$("#cmremail").text(rowData[4].firstChild.nodeValue); // email
				} 

				$('#confirmLinkCust').modal('show');
				var ssapAppId = $("#encssapApplicationId").val();
				var cmrId = rowData[5].firstChild.nodeValue;
				
				$("#confirmLinkCust").find("#submitBtn").unbind("click",fetchAppData);
				$("#confirmLinkCust").find("#submitBtn").bind("click",{ssapAppId:ssapAppId,cmrId:cmrId},fetchAppData);
			} else {
				$("#linkCustErrorMsg").text("Please Select one of the options above to link this application to an existing account.")
			}
		};
		
		function fetchAppData(e) {
			$('#confirmLinkCust').modal('hide');
			checkPreLinkData(e.data.ssapAppId, e.data.cmrId);
		}
		function AddNewIndividual()
		{
			var dataJson=${applicationJson};
			dataJson.encssapApplicationId = $("#encssapApplicationId").val();
			dataJson.encssapApplicationId=dataJson.encssapApplicationId.toString().trim();
			dataJson=angular.toJson(dataJson);
			dataJson=escape(dataJson);
			$("#applicationJson").val(dataJson);
			$("#frmAddNewIndividual")[0].submit();
			
		};
	</script>