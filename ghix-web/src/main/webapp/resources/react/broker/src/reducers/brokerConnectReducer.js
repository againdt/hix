import { brokerConnectActionTypes } from '../constants/actionTypes';

const initialState = {
  isAvailable: false,
  phoneNumber: '',
  status: 'INACTIVE',
  brokerAvailabilities: []
};

export default (state = initialState, action) => {
  switch (action.type) {
    case brokerConnectActionTypes.SET_BROKER_CONNECT_DATA:
      console.log('Broker Action info: ' + JSON.stringify(action));
      return { ...state, ...action.data };
    case brokerConnectActionTypes.SUBMIT_BROKER_CONNECT_DATA:
      return { ...state, ...action.data };
    default:
      return state;
  }
};
