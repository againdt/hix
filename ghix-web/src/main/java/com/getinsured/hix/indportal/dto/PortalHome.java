package com.getinsured.hix.indportal.dto;

public class PortalHome {

	private String firstName;
	private String lastName;
	
	private int currentStage;
	
	private boolean preEligibility;
	private boolean preEligibilityMedicaid;
	private boolean preEligibilityChip;
	private boolean preEligibilityMedicare;
	private boolean favoritePlanSelected;

	private String aptc;
	private String csr;

	private String issuerLogo;
	private String planName;
	private String premium;
	private String premiumAfterAptc;
	private String planType;
	private String officeVisit;
	private String genericMedication;
	private String deductible;
	private String outOfPocketMax;
	private String planId;
	private String coverageYear;
	private String coverageDate;
	
	private boolean applicationCreated;
	private boolean applicationInProgress;
	private boolean applicationSigned;
	private boolean waitingForEligibilityResults;
	private boolean postEligibility;		
	private boolean enableEnrollment;
	private boolean enrollmentPlanSelected;
	
	private String caseNumber;
	private String stateCode;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public int getCurrentStage() {
		return currentStage;
	}
	public void setCurrentStage(int currentStage) {
		this.currentStage = currentStage;
	}
	public boolean isPreEligibility() {
		return preEligibility;
	}
	public void setPreEligibility(boolean preEligibility) {
		this.preEligibility = preEligibility;
	}
	public boolean isPreEligibilityMedicaid() {
		return preEligibilityMedicaid;
	}
	public void setPreEligibilityMedicaid(boolean preEligibilityMedicaid) {
		this.preEligibilityMedicaid = preEligibilityMedicaid;
	}
	public boolean isPreEligibilityChip() {
		return preEligibilityChip;
	}
	public void setPreEligibilityChip(boolean preEligibilityChip) {
		this.preEligibilityChip = preEligibilityChip;
	}
	public boolean isPreEligibilityMedicare() {
		return preEligibilityMedicare;
	}
	public void setPreEligibilityMedicare(boolean preEligibilityMedicare) {
		this.preEligibilityMedicare = preEligibilityMedicare;
	}
	public boolean isFavoritePlanSelected() {
		return favoritePlanSelected;
	}
	public void setFavoritePlanSelected(boolean favoritePlanSelected) {
		this.favoritePlanSelected = favoritePlanSelected;
	}
	public String getAptc() {
		return aptc;
	}
	public void setAptc(String aptc) {
		this.aptc = aptc;
	}
	public String getCsr() {
		return csr;
	}
	public void setCsr(String csr) {
		this.csr = csr;
	}
	public String getIssuerLogo() {
		return issuerLogo;
	}
	public void setIssuerLogo(String issuerLogo) {
		this.issuerLogo = issuerLogo;
	}
	public String getPlanName() {
		return planName;
	}
	public void setPlanName(String planName) {
		this.planName = planName;
	}
	public String getPremium() {
		return premium;
	}
	public void setPremium(String premium) {
		this.premium = premium;
	}
	public String getPremiumAfterAptc() {
		return premiumAfterAptc;
	}
	public void setPremiumAfterAptc(String premiumAfterAptc) {
		this.premiumAfterAptc = premiumAfterAptc;
	}
	public String getPlanType() {
		return planType;
	}
	public void setPlanType(String planType) {
		this.planType = planType;
	}
	public String getOfficeVisit() {
		return officeVisit;
	}
	public void setOfficeVisit(String officeVisit) {
		this.officeVisit = officeVisit;
	}
	public String getGenericMedication() {
		return genericMedication;
	}
	public void setGenericMedication(String genericMedication) {
		this.genericMedication = genericMedication;
	}
	public String getDeductible() {
		return deductible;
	}
	public void setDeductible(String deductible) {
		this.deductible = deductible;
	}
	public String getOutOfPocketMax() {
		return outOfPocketMax;
	}
	public void setOutOfPocketMax(String outOfPocketMax) {
		this.outOfPocketMax = outOfPocketMax;
	}
	public String getPlanId() {
		return planId;
	}
	public void setPlanId(String planId) {
		this.planId = planId;
	}
	public String getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(String coverageYear) {
		this.coverageYear = coverageYear;
	}
	public String getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(String coverageDate) {
		this.coverageDate = coverageDate;
	}
	public boolean isApplicationCreated() {
		return applicationCreated;
	}
	public void setApplicationCreated(boolean applicationCreated) {
		this.applicationCreated = applicationCreated;
	}
	public boolean isApplicationInProgress() {
		return applicationInProgress;
	}
	public void setApplicationInProgress(boolean applicationInProgress) {
		this.applicationInProgress = applicationInProgress;
	}
	public boolean isApplicationSigned() {
		return applicationSigned;
	}
	public void setApplicationSigned(boolean applicationSigned) {
		this.applicationSigned = applicationSigned;
	}
	public boolean isWaitingForEligibilityResults() {
		return waitingForEligibilityResults;
	}
	public void setWaitingForEligibilityResults(boolean waitingForEligibilityResults) {
		this.waitingForEligibilityResults = waitingForEligibilityResults;
	}
	public boolean isPostEligibility() {
		return postEligibility;
	}
	public void setPostEligibility(boolean postEligibility) {
		this.postEligibility = postEligibility;
	}
	public boolean isEnableEnrollment() {
		return enableEnrollment;
	}
	public void setEnableEnrollment(boolean enableEnrollment) {
		this.enableEnrollment = enableEnrollment;
	}
	public boolean isEnrollmentPlanSelected() {
		return enrollmentPlanSelected;
	}
	public void setEnrollmentPlanSelected(boolean enrollmentPlanSelected) {
		this.enrollmentPlanSelected = enrollmentPlanSelected;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getStateCode() {
		return stateCode;
	}
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("PortalHome [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", currentStage=");
		builder.append(currentStage);
		builder.append(", preEligibility=");
		builder.append(preEligibility);
		builder.append(", preEligibilityMedicaid=");
		builder.append(preEligibilityMedicaid);
		builder.append(", preEligibilityChip=");
		builder.append(preEligibilityChip);
		builder.append(", preEligibilityMedicare=");
		builder.append(preEligibilityMedicare);
		builder.append(", favoritePlanSelected=");
		builder.append(favoritePlanSelected);
		builder.append(", aptc=");
		builder.append(aptc);
		builder.append(", csr=");
		builder.append(csr);
		builder.append(", issuerLogo=");
		builder.append(issuerLogo);
		builder.append(", planName=");
		builder.append(planName);
		builder.append(", premium=");
		builder.append(premium);
		builder.append(", premiumAfterAptc=");
		builder.append(premiumAfterAptc);
		builder.append(", planType=");
		builder.append(planType);
		builder.append(", officeVisit=");
		builder.append(officeVisit);
		builder.append(", genericMedication=");
		builder.append(genericMedication);
		builder.append(", deductible=");
		builder.append(deductible);
		builder.append(", outOfPocketMax=");
		builder.append(outOfPocketMax);
		builder.append(", planId=");
		builder.append(planId);
		builder.append(", coverageYear=");
		builder.append(coverageYear);
		builder.append(", coverageDate=");
		builder.append(coverageDate);
		builder.append(", applicationCreated=");
		builder.append(applicationCreated);
		builder.append(", applicationInProgress=");
		builder.append(applicationInProgress);
		builder.append(", applicationSigned=");
		builder.append(applicationSigned);
		builder.append(", waitingForEligibilityResults=");
		builder.append(waitingForEligibilityResults);
		builder.append(", postEligibility=");
		builder.append(postEligibility);
		builder.append(", enableEnrollment=");
		builder.append(enableEnrollment);
		builder.append(", enrollmentPlanSelected=");
		builder.append(enrollmentPlanSelected);
		builder.append(", caseNumber=");
		builder.append(caseNumber);
		builder.append(", stateCode=");
		builder.append(stateCode);
		builder.append("]");
		return builder.toString();
	}
	

}
