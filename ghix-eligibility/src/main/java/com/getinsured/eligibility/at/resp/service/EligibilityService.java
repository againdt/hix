package com.getinsured.eligibility.at.resp.service;

import java.util.Map;

import com.getinsured.eligibility.ui.dto.SsapApplicantEligibilityDTO;
import com.getinsured.eligibility.ui.dto.SsapApplicationEligibilityDTO;

public interface EligibilityService {

	/**
	 * Returns Eligibilities with TRUE indicator for all covered members
	 * in household for a given application Id.
	 *
	 * This may return empty Map.
	 *
	 * @param applicationId
	 * @return Map<Long, SsapApplicantEligibilityDTO>
	 * @throws {@link GIRuntimeException}
	 */
	Map<Long, SsapApplicantEligibilityDTO> getApplicantsEligibilityForApplicationId(Long applicationId);


	SsapApplicationEligibilityDTO getApplicationDataApplicationId(Long applicationId);

	SsapApplicationEligibilityDTO getApplicationData(String caseNumber);

	boolean checkNotVerifiedStatus(String status);
}
