<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/ng-template" id="referrals">
<div class="row-fluid dashboard-rightpanel" ng-init="retrieveReferrals()">
  <div class="header ng-scope">
    <h4><spring:message code="indportal.referrals.pendingReferrals" javaScriptEscape="true"/></h4>
  </div>

  <div id="appealsHistoryIndPortal"  >
    <div class="dashboard-rightpanel" id="rightpanel">
      <div class="well">
        <div class="gutter20">
          <div ng-if="pendingReferrals.length===0">
            <spring:message code="indportal.referrals.noReferral" javaScriptEscape="true"/>
          </div>
          <table class="table table-striped" ng-if="pendingReferrals.length>0">
						<caption class="aria-hidde"><spring:message code="indportal.referrals.pendingReferrals" javaScriptEscape="true"/></caption>
            <thead>
              <tr class="header">
                <th scope="col"><spring:message code="indportal.referrals.reference" javaScriptEscape="true"/></th>
              </tr>
            </thead>
            <tbody ng-repeat="referral in pendingReferrals"  >
              <tr >
                <td>
                  <a class="referal" href="./referral/portalflow/{{referral.referralId}}">{{referral.caseNumber}}</a>

                  <a class="normal-case btn btn-primary pull-right" href="./referral/portalflow/{{referral.referralId}}"><spring:message code="indportal.referrals.linkReferral" javaScriptEscape="true"/></a>  
                 </td>
              </tr>
            </tbody>
          </table>
          <!--  well-steps ends -->
        </div>
      </div><!--  well ends -->
    </div>
  </div>
  
		  <!-- Modal for failing to retrieve referrals-->
		<div ng-show= "retrievalFail">
		      <div modal-show="retrievalFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="technicalIssues" aria-hidden="true">
		      <div class="modal-dialog">
		        <div class="modal-content">
		          <div class="modal-header">
		            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		            <h3 id="technicalIssue"><spring:message code="indportal.portalhome.technicalissue" javaScriptEscape="true"/></h3>
		          </div>
		          <div class="modal-body">
		          <p><spring:message code="indportal.portalhome.csrsubmissionfailcontent" javaScriptEscape="true"/></p>
		          </div>
		          <div class="modal-footer">
		            <button class="btn pull-left" data-dismiss="modal"><spring:message code="indportal.portalhome.close" javaScriptEscape="true"/></button>
		          </div>
		        </div><!-- /.modal-content-->
		      </div> <!-- /.modal-dialog-->
		     </div>
		</div>
		<!-- Modal for failing init-->
</div>

</script>
