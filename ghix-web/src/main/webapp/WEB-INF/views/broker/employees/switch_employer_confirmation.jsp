<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<form name="switchEmployerForm" id="switchEmployerForm" action='<c:url value="/broker/employer/dashboard"/>' novalidate="novalidate">
	<div id="employerSwitchDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="employerSwitchDialog" aria-hidden="true">
		<div class="markCompleteHeader">
	    	<div class="header">
	            <h4 class="margin0 pull-left padding0">View Employer Account</h4>
	            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
	        </div>
	    </div>
	  
	  <div class="modal-body pull-left">
	  <div class="control-group">	
			<div class="controls">
					Clicking "Employer View" will take you to the Employer's portal for ${employerName}.
					Through this portal you will be able to take actions on behalf of this employer, such as view and edit employee list, fill out
					employer elligibility, etc.<br/>
					Proceed?
			</div>
	  </div>
	  </div>
	  <div class="modal-footer clearfix">
	 	 <input class="pull-left"  type="checkbox" id="checkEmployerView" name="checkEmployerView"  > 
	     <div class="pull-left">&nbsp; Don't show this message again.</div>
	     <button class="btn btn" data-dismiss="modal" aria-hidden="true">Cancel</button>
	     <button class="btn btn-primary" type="submit">Employer View</button>
	     <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
	  	 <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employerID}" />
		 <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${brokerFirstName}" />
	  </div>
	</div>
</form>