package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Years;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ind19.util.service.CoverageStartDateService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.AptcRatioRequest;
import com.getinsured.hix.indportal.dto.AptcRatioResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.SignatureType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFAttestationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFPrimaryContactType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFSignerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactPreferenceCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactPreferenceCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ActivityType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.google.gson.Gson;
import com.jayway.jsonpath.JsonPath;

@Component
public class InsuranceApplicationMapper {
	private static final String SBM = "SBM";
	@Autowired private InsuranceApplicantMapper insuranceApplicantMapper;
	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
    @Autowired private Gson platformGson;
    @Autowired private CoverageStartDateService coverageStartDateService;
    @Autowired private GhixRestTemplate ghixRestTemplate;
    @Autowired private SsapApplicantRepository ssapApplicant;
    
    private static final String APTC_ELIGIBILITY_TYPE = "APTCEligibilityType";
    
    private static Logger lOGGER = Logger.getLogger(InsuranceApplicationMapper.class);
    
	
	public InsuranceApplicationType mapInsuranceApplication(String appDataJson,List<JSONObject> houseHolds,Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas) throws  GIException{
		Long ssapId = Long.valueOf((String) JsonPath.read(appDataJson,"$.singleStreamlinedApplication.ssapApplicationId"));
		SsapApplication  ssapApplication = ssapApplicationRepository.findOne(ssapId);
		String hhCaseId = null;
		if(ssapApplication.getCmrHouseoldId() != null) {
			hhCaseId = cmrHouseholdRepository.findHHCaseIdByHouseholdId(ssapApplication.getCmrHouseoldId().intValue());
		}
		JSONObject primaryHouseHold = houseHolds.get(0);
		JSONObject singleStreamlinedApplication = JsonPath.read(appDataJson,"singleStreamlinedApplication");
		
		String applicationSignatureDate =  (String)singleStreamlinedApplication.get("applicationSignatureDate");
		String applicationStartDate  = (String)singleStreamlinedApplication.get("applicationStartDate");
		
		InsuranceApplicationType insuranceApplicationType = AccountTransferUtil.insuranceApplicationObjFactory.createInsuranceApplicationType();

		IdentificationType appIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
		appIdentificationType.setIdentificationID(AccountTransferUtil.createString(hhCaseId));
		insuranceApplicationType.getApplicationIdentification().add(appIdentificationType);

		ActivityType applicationCreationActivity= AccountTransferUtil.niemCoreFactory.createActivityType();
		DateType applicationCreationDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date applicationCreationDate = AccountTransferUtil.basicFactory.createDate();
		
		applicationCreationDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(applicationStartDate));
		applicationCreationDateType.setDate(applicationCreationDate);
		applicationCreationActivity.setActivityDate(applicationCreationDateType);
		insuranceApplicationType.setApplicationCreation(applicationCreationActivity);
		
		ActivityType submisionActivity= AccountTransferUtil.niemCoreFactory.createActivityType();
		DateType applicationSubmisstionDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date submisstionDate = AccountTransferUtil.basicFactory.createDate();
		
		//TODO:need confirmation for applicationSignatureDate
		if(StringUtils.isNotBlank(applicationSignatureDate)){
			submisstionDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(applicationSignatureDate));
		}else{
			submisstionDate.setValue(AccountTransferUtil.getSystemDate());
		}
		applicationSubmisstionDateType.setDate(submisstionDate);
		submisionActivity.setActivityDate(applicationSubmisstionDateType);
		insuranceApplicationType.setApplicationSubmission(submisionActivity);			

		// applyingForFinancialAssistanceIndicator
		insuranceApplicationType.setInsuranceApplicationRequestingFinancialAssistanceIndicator(AccountTransferUtil.addBoolean(singleStreamlinedApplication.get("applyingForFinancialAssistanceIndicator")));
		AptcRatioResponse aptcRatioResponse = this.getMemberLevelAPTC(ssapId);
		for (JSONObject houseHold : houseHolds) {
			if ((java.lang.Boolean) houseHold.get("applyingForCoverageIndicator")) {
			    BigDecimal APTCamount = this.getAptcAmount((String) houseHold.get("applicantGuid"), aptcRatioResponse);
				insuranceApplicationType.getInsuranceApplicant().add(insuranceApplicantMapper.createInsuranceApplicants(houseHold, verificationMetadatas, ssapId, APTCamount));
			}
		}
		/*ApplicationExtensionType applicationExtensionType = AccountTransferUtil.insuranceApplicationObjFactory.createApplicationExtensionType();
		
		GYear gyear = AccountTransferUtil.basicFactory.createGYear();
		XMLGregorianCalendar coverageYear = AccountTransferUtil.getSystemDateYear();
		coverageYear.setYear(BigInteger.valueOf(ssapApplication.getCoverageYear()));
		gyear.setValue(coverageYear);
		
		applicationExtensionType.setCoverageYear(gyear);
		insuranceApplicationType.setApplicationExtension(applicationExtensionType);*/

		insuranceApplicationType.setSSFPrimaryContact(this.addSSFPrimaryContactType(primaryHouseHold));
		insuranceApplicationType.setSSFSigner(this.createSSFSignerType(primaryHouseHold, applicationSignatureDate, singleStreamlinedApplication));
		// InsuranceApplicationRequestingMedicaidIndicator
		insuranceApplicationType.setInsuranceApplicationRequestingMedicaidIndicator(AccountTransferUtil.addBoolean(true));
		//returnObj.setInsuranceApplication(insuranceApplicationType);
		insuranceApplicationType.setInsuranceApplicationTaxReturnAccessIndicator(AccountTransferUtil.addBoolean(true));
		QuantityType coverageRenewalYearQuantity =AccountTransferUtil.niemCoreFactory.createQuantityType();
		coverageRenewalYearQuantity.setValue(BigDecimal.ONE);
		insuranceApplicationType.setInsuranceApplicationCoverageRenewalYearQuantity(coverageRenewalYearQuantity);
		
		return insuranceApplicationType;
		
	}
	private BigDecimal getAptcAmount(String applicantGuid, AptcRatioResponse aptcRatioResponse){
		BigDecimal APTCAmount = null;
		if(aptcRatioResponse != null && aptcRatioResponse.getMemberList()!=null && !aptcRatioResponse.getMemberList().isEmpty()){
			for(AptcRatioResponse.Member member :aptcRatioResponse.getMemberList() )
			{
				if(member.getId().equals(applicantGuid)){
					APTCAmount =  member.getAptc();
					break;
				}
			}
		}
		return APTCAmount;
	}
	
	private SSFSignerType createSSFSignerType(JSONObject household, String applicationSignatureDate, JSONObject singleStreamlinedApplication) {
		SSFSignerType sSFSigner = AccountTransferUtil.insuranceApplicationObjFactory.createSSFSignerType();
		SignatureType signatureType = AccountTransferUtil.hixCoreFactory.createSignatureType();
		
		DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		//TODO:set default date as current date
		if(org.apache.commons.lang3.StringUtils.isNotBlank(applicationSignatureDate)){
			sigDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(applicationSignatureDate));
		}else{
			sigDate.setValue(AccountTransferUtil.getSystemDate());
		}
		sigDateType.setDate(sigDate);
		signatureType.setSignatureDate(sigDateType);
		sSFSigner.setSignature(signatureType);
		
		SSFAttestationType sSFAttestation = AccountTransferUtil.insuranceApplicationObjFactory.createSSFAttestationType();
		//TO:DO - we need to check this value in DB = setSSFAttestationNonPerjuryIndicator, setSSFAttestationCollectionsAgreementIndicator
		//sSFAttestation.setSSFAttestationMedicaidObligationsIndicator(AccountTransferUtil.addBoolean(applicationDTO.getResult().getAttestations().getApplication().getLegalAttestations().getMedicaidRequirementAgreementIndicator()));
		sSFAttestation.setSSFAttestationMedicaidObligationsIndicator(AccountTransferUtil.addBoolean(true));
		sSFAttestation.setSSFAttestationNonPerjuryIndicator(AccountTransferUtil.addBoolean(true));
		sSFAttestation.getSSFAttestationNotIncarceratedIndicator().add((AccountTransferUtil.addBoolean(((JSONObject) household.get("incarcerationStatus")).get("incarcerationStatusIndicator"))));
		sSFAttestation.setSSFAttestationPrivacyAgreementIndicator(AccountTransferUtil.addBoolean(singleStreamlinedApplication.get("acceptPrivacyIndicator")));
		sSFAttestation.setSSFAttestationPendingChargesIndicator(AccountTransferUtil.addBoolean(((JSONObject) household.get("incarcerationStatus")).get("incarcerationPendingDispositionIndicator")));
		sSFAttestation.setSSFAttestationInformationChangesIndicator(AccountTransferUtil.addBoolean(false));
		sSFSigner.setSSFAttestation(sSFAttestation);
		return sSFSigner;
	}
	
	private SSFPrimaryContactType addSSFPrimaryContactType(JSONObject primaryhousehold){
		SSFPrimaryContactType sSFPrimaryContactType = AccountTransferUtil.insuranceApplicationObjFactory.createSSFPrimaryContactType();
		ContactPreferenceCodeType contactPreferenceCodeType = AccountTransferUtil.hixTypeFactory.createContactPreferenceCodeType();
		JSONObject contactPref = (JSONObject) ((JSONObject) primaryhousehold.get("householdContact")).get("contactPreferences");
		String howToContact = (String) contactPref.get("preferredContactMethod");
		if (!StringUtils.isBlank(howToContact)) {
			if (howToContact.toLowerCase().contains("email")) {
				contactPreferenceCodeType.setValue(ContactPreferenceCodeSimpleType.fromValue("Email"));
			} else {
				contactPreferenceCodeType.setValue(ContactPreferenceCodeSimpleType.fromValue("Mail"));
			}
			sSFPrimaryContactType.setSSFPrimaryContactPreferenceCode(contactPreferenceCodeType);
		}
		return sSFPrimaryContactType;
	} 
	
	private AptcRatioResponse getMemberLevelAPTC(long ssapApplicationId){
		try {
			SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
			if(ssapApplication.getMaximumAPTC() != null && ssapApplication.getMaximumAPTC().compareTo(BigDecimal.ZERO) > 0){
				AptcRatioRequest aptcRatioRequest = new AptcRatioRequest();
				AptcRatioResponse aptcRatioResponse = new AptcRatioResponse(); 
				Timestamp coverageDate = null;
				coverageDate = coverageStartDateService.computeCoverageStartDateIncludingTermMembers(new BigDecimal(ssapApplicationId), false, false, null);
				
				List<AptcRatioRequest.Member> requestMemberList= new ArrayList<AptcRatioRequest.Member>();
				AptcRatioRequest.Member requestMember = null;
				
				List<String> eligibilityTypes = new ArrayList<String>();
				eligibilityTypes.add(APTC_ELIGIBILITY_TYPE);
				List<SsapApplicant> ssapApplicants = ssapApplicant.getEligibilitiesForApplicantsByAppId(ssapApplicationId, eligibilityTypes);
				if(ssapApplicants.size() > 0){
					aptcRatioRequest.setCoverageYear((int) ssapApplication.getCoverageYear());
					aptcRatioRequest.setMaxAptc(ssapApplication.getMaximumAPTC());
					
					for (SsapApplicant ssapApplicant : ssapApplicants) {
						requestMember = aptcRatioRequest.new Member();
						requestMember.setAge(getAgeFromDob(DateUtil.dateToString(ssapApplicant.getBirthDate(), GhixConstants.REQUIRED_DATE_FORMAT), coverageDate));
						requestMember.setId(ssapApplicant.getApplicantGuid());
						requestMemberList.add(requestMember);
					}
					aptcRatioRequest.setMemberList(requestMemberList);
					
					String aptcRatioRequestStr = platformGson.toJson(aptcRatioRequest);
					String aptcRatioResponseStr = ghixRestTemplate.postForObject(GhixEndPoints.EligibilityEndPoints.FETCH_APTC_RATIO, aptcRatioRequestStr, String.class);
					if(StringUtils.isNotBlank(aptcRatioResponseStr)) {
						aptcRatioResponse = platformGson.fromJson(aptcRatioResponseStr, AptcRatioResponse.class);
						return aptcRatioResponse;
					}else{
						return null;
					}
				}
			}else{
				return null;
			}
		} catch (Exception e) {
			lOGGER.error("Error while calling member level APTC amount API " + e.getStackTrace());
			return null;
		}
		return null;
	}
	
	
	private int getAgeFromDob(String dob, Date coverageStartDate) {

		if (StringUtils.isBlank(dob)) {
			return 0;
		}
		// Implementation change as per Joda Time
		LocalDate now = new LocalDate(coverageStartDate.getTime());

		Date dobDate = DateUtil.StringToDate(dob, GhixConstants.REQUIRED_DATE_FORMAT);
		LocalDate birthdate = new LocalDate(dobDate.getTime());

		Years years = Years.yearsBetween(birthdate, now);
		int age = years.getYears();
		return age;
	}
	
}
