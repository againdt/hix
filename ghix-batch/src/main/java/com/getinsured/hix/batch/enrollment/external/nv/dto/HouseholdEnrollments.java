package com.getinsured.hix.batch.enrollment.external.nv.dto;

import java.util.List;

import com.getinsured.hix.dto.enrollment.ExternalEnrollmentCsvDTO;

public class HouseholdEnrollments {
	private List<ExternalEnrollmentCsvDTO> enrollments = null;
	private String jsonLine = null;
	private List<String> parsingErrors = null;
	private boolean hasErrors;
	public List<ExternalEnrollmentCsvDTO> getEnrollments() {
		
		return enrollments;
	}

	public void setEnrollments(List<ExternalEnrollmentCsvDTO> enrollments) {
		this.enrollments = enrollments;
	}

	public String getJsonLine() {
		return jsonLine;
	}

	public void setJsonLine(String jsonLine) {
		this.jsonLine = jsonLine;
	}

	public boolean getHasErrors() {
		return hasErrors;
	}

	public void setHasErrors(boolean hasErrors) {
		this.hasErrors = hasErrors;
	}

	public List<String> getParsingErrors() {
		return parsingErrors;
	}

	public void setParsingErrors(List<String> parsingErrors) {
		this.parsingErrors = parsingErrors;
	}
}
