<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<div style="font: italic 14px/30px Georgia, serif;">
	<div id="errorMsgPayment" class="errorblock alert alert-error hide"></div>
</div>

<a id ="healthPaymentBtn" class="btn btn-primary"><spring:message code='enroll.orderConfirm.health.makePayment'/></a>
<a id ="dentalPaymentBtn" class="btn btn-primary"><spring:message code='enroll.orderConfirm.dental.makePayment'/></a>

<!-- Code Starts for Payment Redirection-->
<form id="postform" id="postform" name="postform" method="post" target="_blank">
<df:csrfToken/>
  <input type="hidden"  id="SAMLResponse" name="SAMLResponse"/>
</form> 
<script type="text/javascript">
var isRequestProcessed=false;
var isBtnClicked=null;
var paymentRedirectClick=function(e){
	if(isRequestProcessed){return;};
	isRequestProcessed=true;
	isBtnClicked=$(this);
	var defaultText = $.trim(isBtnClicked.text());
	//Change the text to loading and reset back in AJAX Success and Error handle
	isBtnClicked.text('<spring:message code='enroll.orderConfirm.loadingmsg'/>');
	var urlToServer=e.data;
		var validateUrl = '<c:url value="/finance/paymentRedirect"> <c:param name="${df:csrfTokenParameter()}"> <df:csrfToken plainToken="true" /> 	</c:param> </c:url>';
		  $.ajax({
			  type : "POST",
			  url : validateUrl,
			  data : urlToServer,
			  error: function()
			  {
				$("#errorMsgPayment").text('<spring:message code='enroll.orderConfirm.samlerrormsg'/>').show();
				isRequestProcessed=false;
			  },
			  success:function(data){
				isRequestProcessed=false;
				var result = JSON.parse(data);
				if(result.STATUS == 'SUCCESS')
				{
					var actionUrl = '<c:url value="' +result.ffmEndpointURL + '"/>';
					$.browser.ie = /trident/.test(navigator.userAgent.toLowerCase());
					var mWin=window.open(actionUrl,"_new","width:1px,height:1px");
					if(mWin==undefined)
					{
						$("#errorMsgPayment").text('<spring:message code='enroll.orderConfirm.popuperrormsg'/>').show();
						if($.browser.ie){
							$("#errorMsgPayment").text('<spring:message code='enroll.orderConfirm.iepopuperrormsg'/>').show();
						}
					}else{
						mWin.close();
						$("#errorMsgPayment").remove();
						$('#postform').attr('action',actionUrl);
						$('#SAMLResponse').attr('value', result.SAMLResponse);
						document.postform.submit();
						isBtnClicked.attr('disabled','disabled');
						isBtnClicked.unbind("click");
					}
				}
				else if(result.STATUS == 'FAILURE')
				{
					$("#errorMsgPayment").text('<spring:message code='enroll.orderConfirm.samlerrormsg'/>').show();
				}
				isBtnClicked.text(defaultText);
		  }});
		};

$(document).ready(function() {
	$("#healthPaymentBtn").bind("click",{finEnrlDataKey:"${finEnrlHltDataKey}"},paymentRedirectClick);
	$("#dentalPaymentBtn").bind("click",{finEnrlDataKey:"${finEnrlDntDataKey}"},paymentRedirectClick);
});

function isInvalidCSRFToken(xhr){
	var rv = false;
	if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
		alert($('Session is invalid').text());
		rv = true;
	}
	return rv;
}
</script>
<!-- Code End for Payment Redirection-->