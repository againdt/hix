package com.getinsured.hix.indportal.dto;

public class AppGroupRequest {
	private Long applicationId;
	private String coverageStartDate;
	private Boolean postAptcRedistributionFlag;

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getCoverageStartDate() {
		return coverageStartDate;
	}

	public void setCoverageStartDate(String coverageStartDate) {
		this.coverageStartDate = coverageStartDate;
	}

	public Boolean getPostAptcRedistributionFlag() {
		return postAptcRedistributionFlag;
	}

	public void setPostAptcRedistributionFlag(Boolean postAptcRedistributionFlag) {
		this.postAptcRedistributionFlag = postAptcRedistributionFlag;
	}
	
}
