package com.getinsured.hix.serff.service;

import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.serff.SerffPlanMgmtBatch;

/**
 * DTO class is used to upload SERFF Templates at FTP Server.
 */
public class SerffPlanTemplatesDTO {

	private String batchStatus;
	private String serffReqId;
	private String ftpStatus;
	private String issuerId;
	private String carrierName;
	private String state;
	private String exchangeType;
	private String defaultTenant;
	private SerffPlanMgmtBatch.OPERATION_TYPE operationType;
	private MultipartFile[] fileList;

	public SerffPlanTemplatesDTO() {
	}

	public String getBatchStatus() {
		return batchStatus;
	}

	public void setBatchStatus(String batchStatus) {
		this.batchStatus = batchStatus;
	}

	public String getSerffReqId() {
		return serffReqId;
	}

	public void setSerffReqId(String serffReqId) {
		this.serffReqId = serffReqId;
	}

	public String getFtpStatus() {
		return ftpStatus;
	}

	public void setFtpStatus(String ftpStatus) {
		this.ftpStatus = ftpStatus;
	}

	public String getIssuerId() {
		return issuerId;
	}

	public void setIssuerId(String issuerId) {
		this.issuerId = issuerId;
	}

	public String getCarrierName() {
		return carrierName;
	}

	public void setCarrierName(String carrierName) {
		this.carrierName = carrierName;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public String getExchangeType() {
		return exchangeType;
	}

	public void setExchangeType(String exchangeType) {
		this.exchangeType = exchangeType;
	}

	public String getDefaultTenant() {
		return defaultTenant;
	}

	public void setDefaultTenant(String defaultTenant) {
		this.defaultTenant = defaultTenant;
	}

	public SerffPlanMgmtBatch.OPERATION_TYPE getOperationType() {
		return operationType;
	}

	public void setOperationType(SerffPlanMgmtBatch.OPERATION_TYPE operationType) {
		this.operationType = operationType;
	}

	public MultipartFile[] getFileList() {
		return fileList;
	}

	public void setFileList(MultipartFile[] fileList) {
		this.fileList = fileList;
	}
}
