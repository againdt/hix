<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/employer-planselection-util.tld" prefix="util"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/ui-lightness/jquery-ui-1.8.22.custom.css"/>" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/jquery-ui-1.9.2.css"/>" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/shopstyles.css"/>" />
<script type="text/javascript">
function validateForm(){
	$("#frmdentalcontribution").submit();
}


function getEmpDentalContribution(){
	if( $("#frmdentalcontribution").validate().form() ) {
		var validateUrl = '<c:url value="/shop/employer/plandisplay/getEmployeeDentalContributionData">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		 var empDentalContribution = $("#empDentalContribution").val().replace('$','').replace(',','').replace('.','');
	       $.ajax({
			url: validateUrl, 
			type: "POST", 
			dataType:'json',
			async: false,
			cache : false,
			data: {empDentalCont: empDentalContribution}, 
			success: function(response,xhr)
			{
		        if(isInvalidCSRFToken(xhr))
		          return;
				 var empdentalcost = response;
				 $("#totalempdentalcost_hidden").val(empdentalcost);
				 $("#totalempdentalcost").html('$' + empdentalcost + ' / <spring:message code = "label.month"/>');	
				 
				 var totalcost = parseInt($("#totaldepedentalcost_hidden").val()) + parseInt($("#totalempdentalcost_hidden").val());
				 $("#totaldentalcost").html('$<strong>' + totalcost + ' / <spring:message code = "label.month"/></strong>');	
			},
      });
   }
}


function getDepeDentalContribution(){
	if( $("#frmdentalcontribution").validate() ) {
		var validateUrl = '<c:url value="/shop/employer/plandisplay/getDependentDentalContributionData">
							<c:param name="${df:csrfTokenParameter()}"> 
								<df:csrfToken plainToken="true" />
							</c:param> 
						</c:url>';
		 var depeDentalContribution = $("#depeDentalContribution").val().replace('$','').replace(',','').replace('.','');
		 
	       $.ajax({
			url: validateUrl, 
			type: "POST", 
			dataType:'json',
			async: false,
			cache : false,
			data: {depeDentalCont: depeDentalContribution}, 
			success: function(response,xhr)
			{
		        if(isInvalidCSRFToken(xhr))
		          return;
				 var depedentalcost = response;
				 $("#totaldepedentalcost_hidden").val(depedentalcost);
				 $("#totaldepedentalcost").html('$'+ depedentalcost + ' / <spring:message code = "label.month"/>');	
				 
				 var totalcost = parseInt($("#totaldepedentalcost_hidden").val()) + parseInt($("#totalempdentalcost_hidden").val());
				 $("#totaldentalcost").html('$<strong>' + totalcost + ' / <spring:message code = "label.month"/></strong>');	
			},
      });
   }
};

</script>

<div class="gutter10">
<div class="row-fluid">
	<div class="row-fluid">
		<div class="span3" id="sidebar">
			<div class="accordion graysidebar" id="accordion2">
				<util:nav pageName="${page_name}"></util:nav>
			</div>
			<util:selection pageName="${page_name}"></util:selection>
		</div>
		<!--sidebar-->
		<div class="span9" id="rightpanel">
				<h4 class="graydrkbg gutter10">
			 		<a href="<c:url value="/shop/employer/plandisplay/finalcontribution"/>" class="btn"> <spring:message code = "label.back"/> </a>
            		<span class="offset1"><spring:message code = "label.dentalCosts"/></span>
					<a href="#" class="btn btn-primary btn-small pull-right" id="submitButton" onClick="javascript:validateForm();"> <spring:message  code="label.next"/> </a>
				</h4>				
			<form id="frmdentalcontribution" name="frmdentalcontribution" action="dentalcontribution" method="post">
				<df:csrfToken/>
				<input type="hidden" name="totalempdentalcost_hidden" id="totalempdentalcost_hidden"/>
				<input type="hidden" name="totaldepedentalcost_hidden" id="totaldepedentalcost_hidden"/>
				<div class="gutter10">
					<p><spring:message code = "label.dentalContri"/></p>
					<div class="well">
					<c:choose>
						<c:when test="${dentalRateData != null}">
							<fmt:formatNumber value="${dentalRateData.maxRate + 0.5}" var="dentalMaxRate" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
							<fmt:formatNumber value="${dentalRateData.minRate + 0.5}" var="dentalMinRate" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
						</c:when>
						<c:otherwise>
							<fmt:formatNumber value="0" var="dentalMaxRate" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
							<fmt:formatNumber value="0" var="dentalMinRate" currencySymbol="$" maxFractionDigits="0" minFractionDigits="0" type="currency"/>
						</c:otherwise>
					</c:choose>
						<div>
			              Monthly premiums for dental plan available to your employees range from <strong>${dentalMinRate} - ${dentalMaxRate}</strong> per person.
		                </div>
		                <div class="gutter10">
		                	<div class="control-group">
		                		<label class="control-label span6">Monthly Contribution per employee </label>
		                		<div class="contorls">$ <input type="text" name="empDentalContribution" id="empDentalContribution" onblur="javascript:getEmpDentalContribution()" value="${empDentalCont}" class="input-small"/></div>
		                		<span id="empDentalContribution_error"></span>
		                	</div>
		                	<div class="control-group">
		                		<label class="control-label span6">Monthly Contribution per Dependent</label>
		                		<div class="contorls">$ <input type="text" name="depeDentalContribution" id="depeDentalContribution" onblur="javascript:getDepeDentalContribution()" value="${depeDentalCont}" class="input-small"/></div>
		                		<span id="depeDentalContribution_error"></span>
		                	</div>
		                	<h5>Potential employer Cost</h5>
		                	<div class="gutter10">
			                	<div class="control-group">
			                		<label class="control-label span4">cost for ${totalemployees} employee</label>
			                		<div class="contorls" id="totalempdentalcost" name="totalempdentalcost">$${empDentalTotalCont} / Month</div>
			                	</div>
			                	<div class="control-group">
			                		<label class="control-label span4">cost for ${totaldependents} dependent</label>
			                		<div class="contorls" id="totaldepedentalcost" name="totaldepedentalcost">$${depeDentalTotalCont} / Month</div>
			                	</div>
			                	<div class="control-group">
			                		<c:set var="totalcost" value="${empDentalTotalCont + depeDentalTotalCont}" />
			                		<label class="control-label span4"><strong>Total Costs</strong></label>
			                		<div class="contorls" id="totaldentalcost" name="totaldentalcost"><strong>$${totalcost} / Month</strong></div>
			                	</div>
			                </div>	
		                </div>
					</div>
				</div><!--gutter10-->
			</form>
		</div> <!--rightpanel-->
	</div> <!--row-fluid-->
</div> <!--main-->
</div>
<script type="text/javascript">
jQuery.validator.addMethod("currency", function(value, element, param) {
	var pattern = /^(\\$)?\d{1,3}(,?\d{3})?$/;
	return pattern.test(value); 
});

var validator = $('#frmdentalcontribution').validate({ 
	rules : {
		empDentalContribution : {currency : true},
		depeDentalContribution : {currency: true}
	},
	messages : {
		empDentalContribution : { currency : "<span> <em class='excl'>!</em>Enter a dollar amount.</span>"},
		depeDentalContribution : { currency : "<span> <em class='excl'>!</em>Enter a dollar amount.</span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-inline');
	} 
}); 
</script>