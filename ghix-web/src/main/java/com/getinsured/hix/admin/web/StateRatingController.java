package com.getinsured.hix.admin.web;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.AdminDocument;
import com.getinsured.hix.model.AdminDocument.DOCUMENT_TYPE;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.IssuerQualityRating;
import com.getinsured.hix.model.RatingArea;
import com.getinsured.hix.model.ZipCountyRatingArea;
import com.getinsured.hix.planmgmt.mapper.StateAreaRatingPojo;
import com.getinsured.hix.planmgmt.repository.RatingAreaRepository;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.planmgmt.service.RatingAreaService;
import com.getinsured.hix.planmgmt.service.ZipCountyRatingAreaService;
import com.getinsured.hix.planmgmt.util.PlanMgmtUtil;
import com.getinsured.hix.platform.comment.service.CommentService;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.file.reader.FileReader;
import com.getinsured.hix.platform.file.reader.ReaderFactory;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.util.PlanMgmtConstants;

@Controller
public class StateRatingController {
	private static final String STATEQUALITYRATING= "StateQualityRating;";
	private static final String STATEAREARATING_FOLDER = "stateAreaRating";
	private static final Logger LOGGER = LoggerFactory.getLogger(StateRatingController.class);
	@Autowired private ContentManagementService ecmService;
	@Autowired private RatingAreaRepository ratingAreaRepository;
	@Autowired private UserService userService;
	@Autowired private PlanMgmtService planMgmtService;
	@Autowired private IUserRepository iUserRepository;
	@Autowired private CommentService commentService;
	@Autowired private RatingAreaService ratingAreaService;
	@Autowired private ZipCountyRatingAreaService zipCountyRatingAreaService;
	@Autowired private PlanMgmtUtil planMgmtUtils;
	
	@Value("#{configProp['uploadPath']}")
	private String uploadPath;
	@Value("#{configProp['appUrl']}")
	private String appUrl;
	@Value("#{configProp['ecm.type']}")
	private String documentConfigurationType;

	private static final int COLUMN0 = 0;
	private static final int COLUMN1 = 1;
	private static final int COLUMN2 = 2;
	private static final int COLUMN3 = 3;
	private static final int COLUMN4 = 4;
	private static final int RATING_AREA_START = 1;
	private static final int RATING_AREA_END = 19;
	private static final int VALID_ZIP_CODE_LENGTH = 5;
	private static final String INCORRECT_DATA_FLAG = "1";
	private static final String CORRECT_DATA_FLAG = "0";
	private static final String REMOVED_STRING = "Rating Area ";
	private static final String ZIP_PATTERN = "\\d{5}";
	private static final String REQUIRED_DATE_FORMAT = "MM/dd/yyyy";
	private static final String PAGE_SIZE = "pageSize";
	
	@RequestMapping(value = "/admin/staterating/view", method = {RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'MANAGE_ISSUER')")
	public String viewStateRatingInfo(@ModelAttribute(STATEQUALITYRATING) IssuerQualityRating issuerQualityRating, Model model, HttpServletRequest request) {
		List<AdminDocument> documentList=zipCountyRatingAreaService.getAllAdminDocument(DOCUMENT_TYPE.STATE_RATING_DOCUMENT.toString());
		String commentText=null;
		AccountUser accountUser=null;
		List<HashMap<String,String>> documentFilesList = new ArrayList<HashMap<String,String>>();
		try{
			if(documentList.size()>0){
				for(AdminDocument adminDoc: documentList){
					HashMap<String,String> documentInfoHM= new HashMap<String,String>();
					documentInfoHM.put("uploadedFileLink",adminDoc.getDocumentName());
					String documentURL = "<a href='"+appUrl+"admin/document/filedownload?documentId="+adminDoc.getDocumentName()+"&fileName="+adminDoc.getFileName()+"'>"+ planMgmtService.getDocumentNameFromECMorUCM(documentConfigurationType, adminDoc.getDocumentName(),adminDoc.getFileName()) + "</a>";
					documentInfoHM.put("uploadedFileName",documentURL);
					
					if(adminDoc.getCreatedBy()!=null){
						accountUser = iUserRepository.findOne(Integer.parseInt(adminDoc.getCreatedBy().toString()));
						if(accountUser!=null && accountUser.getUsername()!=null){
							documentInfoHM.put("uploadedBy",accountUser.getUsername());
						}else{
							documentInfoHM.put("uploadedBy","-");
						}
					}else{
						documentInfoHM.put("uploadedBy","-");
					}
					
					documentInfoHM.put("uploadedDate",DateUtil.changeFormat(adminDoc.getCreationTimestamp().toString(), "yyyy-MM-dd", REQUIRED_DATE_FORMAT));
					if(adminDoc.getCommentID()>0){
						documentInfoHM.put("uploadComment",String.valueOf(adminDoc.getCommentID()));
						commentText=commentService.getCommentTextById(adminDoc.getCommentID());
						
						if(commentText!=null){
							String commentString=commentText.replace(" ", "_");
							String commentURL="<a href="+"#"+" id="+"test"+" onclick="+"showComment('"+commentString+"');>"+"Comment"+"</a>";
							documentInfoHM.put("uploadCommentText",commentURL);
						}else{
							documentInfoHM.put("uploadCommentText","No Comment Added");
						}
					}
					documentFilesList.add(documentInfoHM);
				}
				model.addAttribute("documentList", documentFilesList);
				model.addAttribute(PAGE_SIZE, GhixConstants.PAGE_SIZE);
				
			}
		}catch(Exception ex){
			LOGGER.error("ExceptionInQualityRating: " + ex.getMessage(),ex);
		}
		return "admin/staterating/view";
	}

	@RequestMapping(value = "/admin/staterating/upload", method = {RequestMethod.GET})
	@PreAuthorize("hasPermission(#model, 'MANAGE_ISSUER')")
	public String newStateRatingInfo(@ModelAttribute(STATEQUALITYRATING) IssuerQualityRating issuerQualityRating, Model model, HttpServletRequest request) {
		return "admin/staterating/upload";
	}
	
	@RequestMapping(value = "/admin/stateAreaRating/document/save", method = {RequestMethod.POST})
	public String saveStateRatingArea(Model model,@RequestParam(value = "stateAreaRatingFile", required = false) MultipartFile stateAreaRatingFile,@RequestParam(value = "hdnStateAreaRating", required = false) String hdnStateAreaRating,@RequestParam(value = "hdnCommentText", required = false) String hdnCommentText) {
		
		boolean saveResponse = false;
		
		LOGGER.info("Inside_Save_Method"); 
		
		if(stateAreaRatingFile.getOriginalFilename()!=null && stateAreaRatingFile.getOriginalFilename().length()>0 && hdnStateAreaRating!=null && hdnStateAreaRating.length()>0){
			String filePath = null;
			try{
				filePath = uploadPath + File.separatorChar + STATEAREARATING_FOLDER + File.separatorChar + stateAreaRatingFile.getOriginalFilename();
			}catch(Exception e){
				LOGGER.error("ERROR while fetching stored file message ex: " + e.getMessage(),e);
			}
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				List<List<StateAreaRatingPojo>> ratingModelsList = processStateAreaRatingCSV(filePath);
				List<StateAreaRatingPojo> invalidRatingAreaDataList = ratingModelsList.get(0);
				List<StateAreaRatingPojo> validRatingAreaDataList = ratingModelsList.get(1);
				
				LOGGER.debug("Valida Rating area data list size : " + SecurityUtil.sanitizeForLogging(String.valueOf(validRatingAreaDataList.size())));
				LOGGER.debug("In valid Rating area data list size : " + SecurityUtil.sanitizeForLogging(String.valueOf(invalidRatingAreaDataList.size())));
					
				if(invalidRatingAreaDataList.size() > 0)
				{
					String invalidZips = getInvalidZipCodes(invalidRatingAreaDataList);
					
					String invalidRatingArea = getInvalidRatingArea(invalidRatingAreaDataList);
					
					if(invalidZips!=null){
						LOGGER.debug("Invalid zips : " + SecurityUtil.sanitizeForLogging(String.valueOf(invalidZips)));
						return "redirect:/admin/staterating/upload?invalidZips="+invalidZips+"&invalidRatingArea="+invalidRatingArea;
					}
					
					if(invalidRatingArea!=null){
						LOGGER.debug("Invalid Rating Area : " + SecurityUtil.sanitizeForLogging(String.valueOf(invalidRatingArea)));
						return "redirect:/admin/staterating/upload?invalidZips="+invalidZips+"&invalidRatingArea="+invalidRatingArea;
					}
				}
				else
				{
					saveResponse = saveStateAreaRatingMapping(validRatingAreaDataList);
					if(saveResponse){
						LOGGER.info("Prparing for Saveing Document In Admin Table");
						saveDocumentInDocumentTable(hdnStateAreaRating,hdnCommentText,stateAreaRatingFile);;
					}
				}
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
			
		}
		return "redirect:/admin/staterating/view";
	}
	
	public boolean saveStateAreaRatingMapping(List<StateAreaRatingPojo> stateRatingAreaList){
		// save logic for persisting data into the PM_ZIP_COUNTY_RATING_AREA table
		boolean returningResponse = false;
		try{
			Integer countyRatingAreasListCount = zipCountyRatingAreaService.getAllCountyAreaRatingCount();
			
			if(countyRatingAreasListCount > 0){
				LOGGER.info("Remove All Contents of the Table");
				zipCountyRatingAreaService.removeAllContentsFromZipCounty();
			}
			for(StateAreaRatingPojo stateRatingArea : stateRatingAreaList){
				ZipCountyRatingArea zipCountyRatingArea = new ZipCountyRatingArea();
				RatingArea ratingArea = ratingAreaService.getByRatingAreaAndState(stateRatingArea.getRatingArea(),stateRatingArea.getState());
				zipCountyRatingArea.setRatingArea(ratingArea);
				zipCountyRatingArea.setZip(stateRatingArea.getZip());
				zipCountyRatingArea.setCounty(stateRatingArea.getCounty());
				zipCountyRatingArea.setCountyFips(stateRatingArea.getCountyFIPS());
				returningResponse = zipCountyRatingAreaService.saveZipCountyRatingArea(zipCountyRatingArea);
			}
		}catch(Exception e){
			LOGGER.error("Exception Occures while Insert: " + e.getMessage(),e);
		}
		if(returningResponse){
			return true;
		}else{
			return false;
		}
	}
	
	@RequestMapping(value ="/admin/statearearatingfile/upload", method ={RequestMethod.GET,RequestMethod.POST})
	@ResponseBody public String uploadStateAreaRatingFile(Model model,@RequestParam(value = "stateAreaRatingFile", required = false) MultipartFile stateAreaRatingFile) {
		return UploadFile("qualityRatingDocument",STATEAREARATING_FOLDER,stateAreaRatingFile);
	}
	
	@SuppressWarnings("unused")
	private String UploadFile(String fileToUpload,String folderName, MultipartFile file){
		String returnString = null;
		String documentId=null;
		try{
			String filePath = GhixConstants.ISSUER_DOC_PATH + GhixConstants.FRONT_SLASH + folderName;
			boolean isValidPath = planMgmtUtils.isValidPath(filePath);
			if(isValidPath){
				String modifiedFileName = FilenameUtils.getBaseName(file.getOriginalFilename()) + GhixConstants.UNDERSCORE + TimeShifterUtil.currentTimeMillis() + GhixConstants.DOT + FilenameUtils.getExtension(file.getOriginalFilename());
				try{
					documentId = ecmService.createContent(filePath, modifiedFileName, file.getBytes()
							, ECMConstants.Issuer.DOC_CATEGORY, ECMConstants.Issuer.DOC_SUB_CATEGORY_QRATING, null);
				}catch (ContentManagementServiceException e) {
					LOGGER.error("Error while reading File from DMS : "+e.getMessage());
				}
				
				returnString = fileToUpload + "|" + file.getOriginalFilename() + "|"  + documentId;
				
				String saveFilePath = uploadPath + GhixConstants.FRONT_SLASH + STATEAREARATING_FOLDER;
				boolean isValidSaveFilePath = planMgmtUtils.isValidPath(filePath);
				if(isValidSaveFilePath){
					String uploadResult=zipCountyRatingAreaService.saveFile(saveFilePath, file, file.getOriginalFilename());
				}else{
					LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
				}
				LOGGER.info("Upload state rating document Document");
			}else{
				LOGGER.error(PlanMgmtConstants.INVALID_VALID_PATH_ERROR);
			}
		}
		catch(Exception ex){
			LOGGER.error("Fail to upload issuer file. Ex: " + ex.getMessage(),ex);
		}
	 return returnString;
	}
	
	private List<List<StateAreaRatingPojo>>  processStateAreaRatingCSV(String filePath){
		List<List<StateAreaRatingPojo>> resultantList = new ArrayList<List<StateAreaRatingPojo>>(2);
		List<StateAreaRatingPojo> validDataList = new ArrayList<StateAreaRatingPojo>();
		List<StateAreaRatingPojo> invalidDataList = new ArrayList<StateAreaRatingPojo>();
		List<List<String>> data = null;
		
		try{
			FileReader<List<String>> qualityReader = ReaderFactory.<List<String>> getReader(filePath, ',', List.class);
			data = qualityReader.readData();
		}catch(Exception ex){
			LOGGER.error("Invalid File type or file read  error" + ex.getMessage() );
		}
		
		if(data!=null && data.size()>0){
			for(List<String> rowsList : data) {
				try{
						if(rowsList != null && rowsList.size() > 4){
							
							boolean isValidZipCode = isValidZipCode(rowsList.get(COLUMN0));
							
							boolean isValidRatingArea = isValidRatinArea(rowsList.get(COLUMN2));
							
							if(isValidZipCode && isValidRatingArea){
								StateAreaRatingPojo validRatingArea = new StateAreaRatingPojo();
								validRatingArea.setZip(rowsList.get(COLUMN0));
								validRatingArea.setCounty(rowsList.get(COLUMN1));
								validRatingArea.setRatingArea(rowsList.get(COLUMN2));
								validRatingArea.setState(rowsList.get(COLUMN3));
								validRatingArea.setCountyFIPS(rowsList.get(COLUMN4));
								validRatingArea.setErrorFlag(CORRECT_DATA_FLAG);
								validDataList.add(validRatingArea);
							}else{
								StateAreaRatingPojo invalidRatingArea = new StateAreaRatingPojo();
								if(!isValidZipCode){
									invalidRatingArea.setZip(rowsList.get(COLUMN0));
								}
								if(!isValidRatingArea){
									invalidRatingArea.setRatingArea(rowsList.get(COLUMN2));
								}
								invalidRatingArea.setErrorFlag(INCORRECT_DATA_FLAG);
								invalidDataList.add(invalidRatingArea);
								LOGGER.debug("Invalid rating area data list size : " + SecurityUtil.sanitizeForLogging(String.valueOf(invalidDataList.size())));
							}
						}
					}catch (Exception e) {
						LOGGER.error("Invalid Data Available In File ",e);
					}
				}
		}
		resultantList.add(invalidDataList);
		resultantList.add(validDataList);
		return resultantList;
	}
	
	private boolean isValidZipCode(String zipcode){
		boolean validZipCode=true;
		Pattern pattern = Pattern.compile(ZIP_PATTERN);
		if(zipcode!=null){
			Matcher matcher = pattern.matcher(zipcode);
			boolean zipMatcher = matcher.matches();
			if(zipMatcher==true){
				if(zipcode.length()!=VALID_ZIP_CODE_LENGTH){
					validZipCode=false;
					return validZipCode;
				}else{
					return validZipCode;
				}
			}else{
				validZipCode=false;
				return validZipCode;
			}
		}else{
			validZipCode=false;
			return validZipCode;
		}
	}
	
	private boolean isValidRatinArea(String ratingArea){
		boolean validRatingArea=false;
		if(ratingArea.contains(REMOVED_STRING)){
			for(int raIndex=RATING_AREA_START;raIndex<=RATING_AREA_END;raIndex++){
				String replacedData=ratingArea.replace(REMOVED_STRING, "");
				LOGGER.debug("replacedData: " + SecurityUtil.sanitizeForLogging(String.valueOf(replacedData)));
				if(replacedData.length()>0){
					Integer ratingAreaFromFile=Integer.parseInt(replacedData.trim());
					if(ratingAreaFromFile>=RATING_AREA_START && ratingAreaFromFile<=RATING_AREA_END){
						validRatingArea=true;
						return validRatingArea;
					}
				}
			}
		}
		return validRatingArea;
	}
	
	private String getInvalidZipCodes(List<StateAreaRatingPojo> invalidDataList){
		String invalidZipString=null;
		for(StateAreaRatingPojo stateAreaRatingPojo : invalidDataList){
			if(stateAreaRatingPojo.getZip()!=null && stateAreaRatingPojo.getZip().length()!=VALID_ZIP_CODE_LENGTH){
				if(invalidZipString!=null){
					invalidZipString=invalidZipString+","+stateAreaRatingPojo.getZip();
				}else{
					invalidZipString=stateAreaRatingPojo.getZip();
				}
			}
		}
		return invalidZipString;
	}
	
	private String getInvalidRatingArea(List<StateAreaRatingPojo> invalidDataList){
		String invalidRatingString = null;
		for(StateAreaRatingPojo stateAreaRatingPojo : invalidDataList){
			if(stateAreaRatingPojo.getRatingArea() != null){
				if(invalidRatingString != null){
					invalidRatingString = invalidRatingString+","+stateAreaRatingPojo.getRatingArea();
				}else{
					invalidRatingString = stateAreaRatingPojo.getRatingArea();
				}
			}
		}
		return invalidRatingString;
	}
	
	private void saveDocumentInDocumentTable(String stateAreaRating,String commentText,MultipartFile stateAreaRatingFile){
		AccountUser user=null;
		try{
			user = userService.getLoggedInUser();
			Integer commentid=planMgmtService.saveComment(user.getId(), commentText, TargetName.ADMIN_HOME, user.getId(), user.getUsername());
			
			if(user.getEmail()!=null){
				AdminDocument document = new AdminDocument();
				document.setDocumentType(DOCUMENT_TYPE.STATE_RATING_DOCUMENT.toString());
				document.setCreatedBy(user.getId());						
				document.setLastUpdatedBy(user.getId());
				document.setDocumentName(stateAreaRating);
				document.setUserID(user.getId());
				document.setCommentID(commentid);
				document.setFileName(stateAreaRatingFile.getOriginalFilename());
				document.setFileType(stateAreaRatingFile.getContentType());
				document.setFileSize(stateAreaRatingFile.getSize());
				
				document.setCreationTimestamp(DateUtil.StringToDate(getSpecificFormat(),REQUIRED_DATE_FORMAT));
				document.setLastUpdateTimestamp(DateUtil.StringToDate(getSpecificFormat(),REQUIRED_DATE_FORMAT));
				zipCountyRatingAreaService.saveDocument(document);
			}			
		}catch (InvalidUserException e) {
			LOGGER.error("Invalid User Loggerin from saveDocumentInDocumentTable: " + e.getMessage());
		}
	}
	
	private String getSpecificFormat(){
		SimpleDateFormat sdf = new SimpleDateFormat(REQUIRED_DATE_FORMAT);
		Date date = new TSDate();
		return(sdf.format(date));
	}
}
