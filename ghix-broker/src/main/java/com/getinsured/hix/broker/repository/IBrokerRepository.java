package com.getinsured.hix.broker.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


import com.getinsured.hix.model.Broker;

/**
 * Spring-JPA repository that encapsulates methods that interact with the Broker
 * table
 */
public interface IBrokerRepository extends JpaRepository<Broker, Integer> {

	/**
	 * Fetches the Broker record against the given broker Id
	 * 
	 * @param brokerId
	 *            the broker id
	 * @return the Broker record against the broker Id
	 */
	Broker findById(int brokerId);
	
	/**
	 * Finds the broker by given license no
	 * 
	 * @param licenseNo
	 *            the given license no
	 * @return the existing broker if found
	 */
	@Query("FROM Broker where licenseNumber = :licenseNo")
	Broker findByLicenseNo(@Param("licenseNo") String licenseNo);
	
	Broker findByUserId(int userId);
	
	Broker findByAgencyId(Long agencyId);

	@Query("From Broker where certification_status = 'Certified'")
	List<Broker> getBRokerList();
	
	
	@Query("select a from Broker a, UserRole u, Role r where a.user.id = u.user.id and r.name = 'AGENCY_MANAGER' and u.role.id = r.id and a.agencyId is not null and a.agencyId = :agencyId")
	List<Broker> findByAgencyManagerInfo(@Param("agencyId")long agencyId);
	
	
	@Query("select count(*) from Broker where agencyId=:agencyId and certification_status = 'Certified'")
	int getAgentCount(@Param("agencyId")long agencyId);
	
}