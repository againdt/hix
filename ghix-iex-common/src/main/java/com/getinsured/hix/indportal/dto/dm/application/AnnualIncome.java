package com.getinsured.hix.indportal.dto.dm.application;

public class AnnualIncome {
	 private Float attestedAnnualTaxHouseholdIncomeAmount;
	 private Float attestedAnnualTaxHouseholdIncomePercent;
	 private String annualIncomeStatus;
	 private String annualIncomeStatusReason;
	 private Boolean incomeExplanationRequiredIndicator;
	 private Float usedDocumentAnnualTaxIncomeAmount;
	 private String incomeExplanationRequiredReasonType;

	 // Getter Methods 

	 public Float getAttestedAnnualTaxHouseholdIncomeAmount() {
	  return attestedAnnualTaxHouseholdIncomeAmount;
	 }

	 public Float getAttestedAnnualTaxHouseholdIncomePercent() {
	  return attestedAnnualTaxHouseholdIncomePercent;
	 }

	 public String getAnnualIncomeStatus() {
	  return annualIncomeStatus;
	 }

	 public String getAnnualIncomeStatusReason() {
	  return annualIncomeStatusReason;
	 }

	 // Setter Methods 

	 public void setAttestedAnnualTaxHouseholdIncomeAmount(Float attestedAnnualTaxHouseholdIncomeAmount) {
	  this.attestedAnnualTaxHouseholdIncomeAmount = attestedAnnualTaxHouseholdIncomeAmount;
	 }

	 public void setAttestedAnnualTaxHouseholdIncomePercent(Float attestedAnnualTaxHouseholdIncomePercent) {
	  this.attestedAnnualTaxHouseholdIncomePercent = attestedAnnualTaxHouseholdIncomePercent;
	 }

	 public void setAnnualIncomeStatus(String annualIncomeStatus) {
	  this.annualIncomeStatus = annualIncomeStatus;
	 }

	 public void setAnnualIncomeStatusReason(String annualIncomeStatusReason) {
	  this.annualIncomeStatusReason = annualIncomeStatusReason;
	 }

	public Boolean getIncomeExplanationRequiredIndicator() {
		return incomeExplanationRequiredIndicator;
	}

	public void setIncomeExplanationRequiredIndicator(Boolean incomeExplanationRequiredIndicator) {
		this.incomeExplanationRequiredIndicator = incomeExplanationRequiredIndicator;
	}

	public String getIncomeExplanationRequiredReasonType() {
		return incomeExplanationRequiredReasonType;
	}

	public void setIncomeExplanationRequiredReasonType(String incomeExplanationRequiredReasonType) {
		this.incomeExplanationRequiredReasonType = incomeExplanationRequiredReasonType;
	}

	public Float getUsedDocumentAnnualTaxIncomeAmount() {
		return usedDocumentAnnualTaxIncomeAmount;
	}

	public void setUsedDocumentAnnualTaxIncomeAmount(Float usedDocumentAnnualTaxIncomeAmount) {
		this.usedDocumentAnnualTaxIncomeAmount = usedDocumentAnnualTaxIncomeAmount;
	}
	}
