<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<link href="<c:url value="/resources/css/broker.css" />" media="screen" rel="stylesheet" type="text/css" />

<script type="text/javascript">

$(document).ready(function() {
	$("#emplComment").addClass("active");
	$("#emplNewcomment").removeClass("link");
	$("#emplViewEmployerAccount").removeClass("link");
	$("#emplViewEmployeeAccount").removeClass("link");		
	$("#emplEnrollment").removeClass("link");
	$("#emplSummary").removeClass("link");
});

(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href =     "<%=request.getContextPath()%>/broker/employee/newcomment?target_id=${encryptedId}&target_name=${encModuleName}&employeeName=${objEmployee.name}" ;//$(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
      }
    });
  });
  
  function setPopupValue() {
		if(document.getElementById("frmPopup").elements['empchkshowpopup'].checked) {
			document.frmPopup.showPopupInFuture.value = "N";
		}
  }
  
  function closeIFrame() {
	  
	  $("#newcommentiframe").remove();
	  window.location.reload(true);
	 // window.location.href = '/hix/shop/employer/employercomments/'+${employer.id};TODO: After canceling where should it redirect
  }
  function closeCommentBox()
  {	  	  
	  $("#newcommentiframe").remove();	  
		 var url = '<%=request.getContextPath()%>/broker/comments/${objEmployee.id}';
		 window.location.assign(url); //TODO: After successfully saving comment page should redirect to comment page
  }
  
  $(document).ready(function() {
		
		function closeWindow()
		{
			window.location.reload(true);
			window.parent.closeIFrame();
			// $('#confirmdeletepopup').remove();
		} 
  })
</script>
<!--start page-breadcrumb -->
<div class="gutter10-lr">
<c:set var="encModuleName" ><encryptor:enc value="EMPLOYEE" isurl="true"/> </c:set>		
<c:set var="encryptedId" ><encryptor:enc value="${objEmployee.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message
						code="label.back" /></a></li>
			<li><a href="<c:url value="/broker/employers"/>"><spring:message code="label.employers"/></a></li>
			<li><spring:message code="label.brkactive"/></li>
			<li>Employee Name ${employer.name} Employee Name</li>
			<li><spring:message code="label.agent.employers.Summary"/></li>
		</ul>
	</div>
	<!--page-breadcrumb ends-->
</div>
<!-- gutter10 -->

	<div class="row-fluid">
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != 'false'}">
				<c:out value="${errorMsg}"></c:out>
			</c:if>
		</div>
	</div>


	<!--  Latest UI -->
	<c:if test="${message != null}">
		<div class="errorblock alert alert-info">
			<p>${message}</p>
		</div>
	</c:if>
    <div class="nav nav-list">
		<h1>${objEmployee.name} <small>${objEmployee.employer.name}</small></h1>
	</div> 
	<div class="row-fluid">
		<!-- #sidebar -->			
	    <jsp:include page="employeeLeftNavigationMenu.jsp" />
	   	<!-- #sidebar ENDS -->	
		
		<!-- Modal -->
		<c:set var="todaysDate" scope="page" value="<%=new java.util.Date()%>" />
		<div class="span9" id="rightpanel">
			<div class="graydrkaction margin0">
				<h4 class="span10">Comments</h4>
			</div>
			<div id="update_comment">
				<input type="hidden" name="commentId" id="commentId" value="" />
				<input type="hidden" name="existingHtml" id="existingHtml" value="" />
			</div>
			<div class="gutter10">
				<form class="form-vertical" id="commentForm" name="commentForm" action="" method="POST">
					<table class="table">
						<tbody>
							<tr>
								<td colspan="2" class="">					
									<comment:view targetId="${objEmployee.id}" targetName="EMPLOYEE">
									</comment:view>
									<jsp:include page="../../platform/comment/addcomments.jsp">
									<jsp:param value="" name=""/>
									</jsp:include>
									<input type="hidden" value="EMPLOYEE" id="target_name" name="target_name" /> 
									<input type="hidden" value="${objEmployee.id}" id="target_id" name="target_id" />
									
								</td>
							</tr>
						</tbody>
					</table>
					<input type="hidden" name="brokerName" id="brokerName" value="${broker.user.firstName}&nbsp;${broker.user.lastName}" />
				</form>
			</div>
		</div>
	</div>
	<!-- row-fluid -->

<!--  Latest UI -->
<!-- code for employee switch -->
<form name="employeeViewForm" id="employeeViewForm" action="<c:url value="/broker/employee/dashboard" />" novalidate="novalidate">
<div id="employeeViewDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="employeeViewDialog" aria-hidden="true">
	<div class="markCompleteHeader">
    	<div class="header">
            <h4 class="margin0 pull-left"><spring:message code="label.employee.viewemployeeaccount"/></h4>
            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="dialogClose" title="x" type="button">x</button>
        </div>
    </div>
  
  <div class="modal-body">
    <div class="control-group">	
				<div class="controls">
					Clicking "Employee view" will take you to the Employee's portal for ${objEmployee.name}.<br/>
					Through this portal you will be able to take actions on behalf of the Employee.<br/>
					Proceed to Employee view?
				</div>
			</div>
  </div>
  <div class="modal-footer clearfix">
  <input class="pull-left"  type="checkbox" id="checkAgentView" name="checkAgentView"  > 
    <div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/> </div>
    <button class="btn btn" data-dismiss="modal" aria-hidden="true">  <spring:message code="label.employee.cancle"/>  </button>
   <button class="btn btn-primary" type="submit">  <spring:message code="label.employee.employeeview"/>  </button>
   <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employee" />
	<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${objEmployee.id}" />
	
	<c:forEach var="memeber" items="${listEmployeeDetails}">
		<c:if test="${memeber.type == 'EMPLOYEE'}" >
		 	<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${memeber.firstName}" />
		</c:if>
	</c:forEach>
	
  </div>
</div>
</form>

<c:set var="employerName" scope="request" value="${objEmployee.employer.name}"/>
<c:set var="employerID" scope="request" value="${objEmployee.employer.id}"/>
<c:set var="brokerFirstName" value="${brokerFirstName}"/>

<jsp:include page="switch_employer_confirmation.jsp"/>	