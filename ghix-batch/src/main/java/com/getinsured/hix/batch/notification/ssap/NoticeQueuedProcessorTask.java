package com.getinsured.hix.batch.notification.ssap;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import org.apache.commons.lang.StringUtils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import java.sql.Timestamp;

public class NoticeQueuedProcessorTask extends StepExecutionListenerSupport implements Tasklet {
    private static final Logger LOGGER = LoggerFactory.getLogger(NoticeQueuedProcessorTask.class);
    private static volatile Boolean isBatchRunning = false;

    private String scheduledDate;
    private NoticeQueuedService noticeQueuedService;

    public String getScheduledDate() {
        return scheduledDate;
    }

    public void setScheduledDate(String scheduledDate) {
        this.scheduledDate = scheduledDate;
    }

    public NoticeQueuedService getNoticeQueuedService() {
        return noticeQueuedService;
    }
    public void setNoticeQueuedService(NoticeQueuedService noticeQueuedService) {
        this.noticeQueuedService = noticeQueuedService;
    }

    @Override
    public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext) throws Exception {
        synchronized (isBatchRunning) {
            if(isBatchRunning) {
                return RepeatStatus.FINISHED;
            } else {
                isBatchRunning = true;
            }
        }

        try {
            if(LOGGER.isDebugEnabled()) {
                LOGGER.debug(this.getClass().getName() + " started at " + new Timestamp(System.currentTimeMillis()));
            }

            noticeQueuedService.processNoticesQueued(scheduledDate);
        } catch(Exception ex) {
            LOGGER.error("execute::error while executing", ex);
        } finally{
            isBatchRunning = false;
        }

        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug(this.getClass().getName() + " finishing at " + new Timestamp(System.currentTimeMillis()));
        }

        return RepeatStatus.FINISHED;
    }
}
