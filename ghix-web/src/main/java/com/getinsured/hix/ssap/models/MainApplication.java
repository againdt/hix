package com.getinsured.hix.ssap.models;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.hix.model.ConsumerDocument;

public class MainApplication {

	private String caseId;
	private long applicationID;
	private boolean authorizedRepresentativeIndicator;

	List<ConsumerDocument> consumerDocuments = new ArrayList<ConsumerDocument>();
	private List<MainApplicationApplicant> mainApplicantList = new ArrayList<MainApplicationApplicant>();
	
	public String getCaseId() {
		return caseId;
	}
	public List<ConsumerDocument> getConsumerDocuments() {
		return consumerDocuments;
	}
	public void addConsumerDocuments(ConsumerDocument consumerDocument) {
		this.consumerDocuments.add(consumerDocument);
	}
	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}
	
	public List<MainApplicationApplicant> getMainApplicantList() {
		return mainApplicantList;
	}
	public void addMainApplicantToList(MainApplicationApplicant objMainApplicationApplicant) {
		this.mainApplicantList.add(objMainApplicationApplicant);
	}
	
	public long getApplicationID() {
		return applicationID;
	}
	public void setApplicationID(long applicationID) {
		this.applicationID = applicationID;
	}
	public boolean isAuthorizedRepresentativeIndicator() {
		return authorizedRepresentativeIndicator;
	}
	public void setAuthorizedRepresentativeIndicator(
			boolean authorizedRepresentativeIndicator) {
		this.authorizedRepresentativeIndicator = authorizedRepresentativeIndicator;
	}

}
