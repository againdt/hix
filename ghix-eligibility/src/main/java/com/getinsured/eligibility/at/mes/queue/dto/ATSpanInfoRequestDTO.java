package com.getinsured.eligibility.at.mes.queue.dto;

import org.joda.time.DateTime;

import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.AccountTransferQueueStatus;

public class ATSpanInfoRequestDTO {

	DateTime eligibilityStartDate;
	DateTime eligibilityEndDate;
	DateTime atEffectiveDate;
	int currentSpan;
	int totalSpans;
	Integer giWsPayloadId;
	AccountTransferProcessStatus processStatus;
	String externalApplicantId;
	long atSpanInfoId;
	long coverageYear;
	AccountTransferQueueStatus queueStatus;
	long ssapApplicationId;
	String errorMessage;
	int cmrHouseoldId;
	
	public DateTime getEligibilityStartDate() {
		return eligibilityStartDate;
	}
	public void setEligibilityStartDate(DateTime eligibilityStartDate) {
		this.eligibilityStartDate = eligibilityStartDate;
	}
	public DateTime getEligibilityEndDate() {
		return eligibilityEndDate;
	}
	public void setEligibilityEndDate(DateTime eligibilityEndDate) {
		this.eligibilityEndDate = eligibilityEndDate;
	}
	public DateTime getAtEffectiveDate() {
		return atEffectiveDate;
	}
	public void setAtEffectiveDate(DateTime atEffectiveDate) {
		this.atEffectiveDate = atEffectiveDate;
	}
	public int getCurrentSpan() {
		return currentSpan;
	}
	public void setCurrentSpan(int currentSpan) {
		this.currentSpan = currentSpan;
	}
	public int getTotalSpans() {
		return totalSpans;
	}
	public void setTotalSpans(int totalSpans) {
		this.totalSpans = totalSpans;
	}
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}
	public AccountTransferProcessStatus getProcessStatus() {
		return processStatus;
	}
	public void setProcessStatus(AccountTransferProcessStatus processStatus) {
		this.processStatus = processStatus;
	}
	public String getExternalApplicantId() {
		return externalApplicantId;
	}
	public void setExternalApplicantId(String externalApplicantId) {
		this.externalApplicantId = externalApplicantId;
	}
	public long getAtSpanInfoId() {
		return atSpanInfoId;
	}
	public void setAtSpanInfoId(long atSpanInfoId) {
		this.atSpanInfoId = atSpanInfoId;
	}
	public long getCoverageYear() {
		return coverageYear;
	}
	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}
	
	public AccountTransferQueueStatus getQueueStatus() {
		return queueStatus;
	}
	public void setQueueStatus(AccountTransferQueueStatus queueStatus) {
		this.queueStatus = queueStatus;
	}
	
	public long getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(long ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	
	public String getErrorMessage() {
		return errorMessage;
	}
	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public int getCmrHouseoldId() {
		return cmrHouseoldId;
	}
	public void setCmrHouseoldId(int cmrHouseoldId) {
		this.cmrHouseoldId = cmrHouseoldId;
	}
}
