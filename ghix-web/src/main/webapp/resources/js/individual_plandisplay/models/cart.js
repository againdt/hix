(function (models) {
	models.Cart = Backbone.Model.extend({
		
		urlRoot : "individualCartItems",
		
		url : function () {
			if(this.id)
				return 'individualCartItems/'+this.id+'?'+(Math.random()*10);
			else
				return 'individualCartItems?'+(Math.random()*10);
		},

		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"name"		: "",
			"level"		: "",
			"premium"		: "",
			"premiumBeforeCredit"	: "",
			"issuer" : "",
			"issuerText" : "",
			"result" : ""
		}
	});
})(App.models);