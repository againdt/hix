package com.getinsured.eligibility.at.ref.service;

import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;

/**
 * @author chopra_s
 * 
 */
public interface ReferralDemographicService {
	CompareMainDTO executeCompare(long enrolledApplicationId, long currentApplicationId) throws Exception;
}
