package com.getinsured.eligibility.at.resp.si.transform;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBElement;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.resp.si.dto.ERPResponse;
import com.getinsured.eligibility.at.resp.si.dto.TaxHouseholdMember;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantESIAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsurancePolicyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ComplexObjectType;
import com.getinsured.iex.util.ReferralUtil;

@Component
public class ERPTransformer {

	private static final String ERP_TRANSFORMER_ERROR = "ERPTransformer error - ";

	// private static final Logger LOGGER = Logger.getLogger(ERPTransformer.class);

	// public ERPResponse transform(AccountTransferRequestPayloadType request){
	public ERPResponse transform(AccountTransferRequestDTO request) {

		ERPResponse erpResponse = new ERPResponse();
		
		transformRequestAttributes(erpResponse, request);
		
		erpResponse.setTaxHouseholdMemberList(new ArrayList<TaxHouseholdMember>());

		headerInfo(erpResponse, request.getAccountTransferRequestPayloadType(), request.getCaseNumber());
		if(request.getAccountTransferRequestPayloadType() != null) {
			try {
				PersonTransformer.personInfo(erpResponse, request.getAccountTransferRequestPayloadType());
			} catch (Exception e) {
				// LOGGER.error(ERP_TRANSFORMER_ERROR + ExceptionUtils.getFullStackTrace(e));
				throw new GIRuntimeException(ERP_TRANSFORMER_ERROR + ExceptionUtils.getFullStackTrace(e));
			}
		}
		try {
			ApplicationExtensionTransformer.appExtensionInfo(erpResponse, request.getAccountTransferRequestPayloadType());
		} catch (Exception e) {
			// LOGGER.error(ERP_TRANSFORMER_ERROR + ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException(ERP_TRANSFORMER_ERROR + ExceptionUtils.getFullStackTrace(e));
		}
		erpResponse.setGiWsPayloadId(request.getGiwsPayloadId());
		erpResponse.setFullAssessment(!request.isFullDetermination());
		erpResponse.setAtResponseType(request.getAtResponseType());
		// System.out.println("erpResponse - " + erpResponse);

		return erpResponse;

	}

	private void transformRequestAttributes(ERPResponse erpResponse, AccountTransferRequestDTO request) {
		erpResponse.setCompareEnrolledApplicationId(request.getEnrolledApplicationId());
		erpResponse.setRenewalCompareToApplicationId(request.getRenewalCompareToApplicationId());
		erpResponse.setCompareToApplicationId(request.getCompareToApplicationId());
		erpResponse.setCmrAutoLinking(request.isCmrAutoLinking());
		erpResponse.setMultipleCmr(request.isMultipleCmr());
		erpResponse.setNonFinancialCmr(request.isNonFinancialCmr());
		erpResponse.setNonFinancialCmrId(request.getNonFinancialCmrId());
		erpResponse.setAccountTransferCategory(request.getAccountTransferCategory());
		erpResponse.setApplicationsWithSameId(request.getApplicationsWithSameId());
		BigDecimal slcspAmount = new BigDecimal(0);
		if(request.getAccountTransferRequestPayloadType() != null) {
			AccountTransferRequestPayloadType taxReturnType = request.getAccountTransferRequestPayloadType();
			String primaryTaxFilerId = taxReturnType != null ? extractPrimaryTaxFilerId(taxReturnType) : null;
			List<InsurancePolicyType> esPolicies = null ;
			List<InsuranceApplicantType> applicants = taxReturnType.getInsuranceApplication().getInsuranceApplicant();
			if (applicants != null && !applicants.isEmpty()) {
			for(InsuranceApplicantType applicant : applicants) {
				PersonType personType;
				personType = (PersonType) applicant.getRoleOfPersonReference().getRef();
				String roleOfPerson = personType.getId().toString();
				if(roleOfPerson.equalsIgnoreCase(primaryTaxFilerId)) {
					 esPolicies = applicant.getInsuranceApplicantNonESIPolicy() != null ? applicant.getInsuranceApplicantNonESIPolicy() : null;
				}
			}
			}
			//Handled null check to avoid NPE
			if(esPolicies != null  && !esPolicies.isEmpty() && esPolicies.get(0) != null && esPolicies.get(0).getInsurancePremium() != null && esPolicies.get(0).getInsurancePremium().getInsurancePremiumAmount() != null){
				slcspAmount = esPolicies.get(0).getInsurancePremium().getInsurancePremiumAmount().getValue();
			}
		}
		erpResponse.setSlcspAmount(slcspAmount);
		erpResponse.setRequester(request.getRequester());
	}

	private static void headerInfo(ERPResponse erpResponse, AccountTransferRequestPayloadType incomingRequest, String caseNumber) {

		// Application ID - check this with our DB to identify fullResponse or Referral
		if(incomingRequest != null) {
			String applicationID = incomingRequest.getInsuranceApplication().getApplicationIdentification().get(0).getIdentificationID().getValue();
	
			if (!StringUtils.isEmpty(caseNumber)) {
				applicationID = caseNumber;
			}
			erpResponse.setApplicationID(applicationID);
		}
	}
	
	@SuppressWarnings("unchecked")
	private String extractPrimaryTaxFilerId(AccountTransferRequestPayloadType source) {
		String primaryTaxFilerId="";
		final int size = ReferralUtil.listSize(source.getTaxReturn());
		if (size != 0) {
			final TaxReturnType taxReturnType = source.getTaxReturn().get(0);
			if (taxReturnType.getTaxHousehold() == null) {
				return "";
			}
			final TaxHouseholdType taxHouseholdType = taxReturnType.getTaxHousehold();


			final List<?> taxFilerList = taxHouseholdType.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent();
			final int tfSize = ReferralUtil.listSize(taxFilerList);
			JAXBElement<?> taxFilerType = null;

			// Populate Primary Taxfiler & Dependents
			for (int i = 0; i < tfSize; i++) {
				taxFilerType = (JAXBElement<?>) taxFilerList.get(i);
				if (taxFilerType != null) {
					if (taxFilerType.getName().getLocalPart().equals("PrimaryTaxFiler")) {
						JAXBElement<TaxFilerType> primaryTaxFiler = (JAXBElement<TaxFilerType>)taxFilerType;
						if (primaryTaxFiler.getValue() != null && primaryTaxFiler.getValue().getRoleOfPersonReference() != null) {
							primaryTaxFilerId = ((PersonType) primaryTaxFiler.getValue().getRoleOfPersonReference().getRef()).getId();	
						}
					}
				}
			}
		}
		return primaryTaxFilerId;
	}

}
