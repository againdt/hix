<div class="row-fluid">
    <div class="header margin20-tb">
      <h4>$enrolleeNames$</h4>
    </div>
</div>

<div class="row-fluid">
	<div class="span6 info-block">                    
	  <div class="gutter10">
	    <div class="plan-selected alert alert-success">
	        <div class="info-header txt-center">
	            <img src="$logoStr$" alt="$issuerName$" class="resize-img">
	            <p><a href="#">$planName$</a></p>
	            <h5 class="payment">$monthlyPremium$</h5>
	        </div>
	        <div class="data-display well">
	         <dl class="dl-horizontal">
	            <dt>Plan Type:</dt>
	            <dd id="plan-type">$planType$</dd>
	            <dt>Office Visit:</dt>
	            <dd id="off-visit">$officeVisit$</dd>
	            <dt>Generic Medications:</dt>
	            <dd id="generic-medic">$genericMedications$</dd>
	            <dt>Deductible:</dt>
	            <dd id="deductible">$deductible$</dd>
	            <dt>Max Out-of-Pocket:</dt>
	            <dd id="m-o-p">$oopMax$</dd>
	          </dl>
	        </div>
	        <div class="center">
       		<a href="$PlanUrl$" data-toggle="modal" id="viewBenifitDetails" class="btn" alt="View Benifit Details">
	       		View Benefit Details
	       	</a>

	      </div>
	      </div>
	      <div class="gutter10-tb"></div>  
	  </div>
	</div> <!-- .span6 -->
	
    <div class="span6 info-block">
      <div class="gutter10">
       	  <div class="sub-header"><h4>Policy Information</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Policy Number:</dt>
                <dd id="policy-number">$policyNumber$</dd>
                <dt>Enrollment Period:</dt>
                <dd id="enrollment-date">$coverageStartDate$ - $coverageEndDate$</dd>
              </dl>                   
          </div>
          <div class="sub-header"><h4>Payment Information</h4></div>
		  <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Monthly Premium:</dt>
                <dd id="monthly-premium">$monthlyPremium$</dd>
                <dt>Premium Tax Credit:</dt>
                <dd id="premium-tax-credit"><span>-</span>$premiumTaxCredit$</dd>
                <dt class="breakword"><strong>Monthly Payment:</strong></dt>
                <dd id="monthly-payment"><strong>$monthlyPayment$</strong></dd>
              </dl>                   
          </div>
          <div class="sub-header"><h4>Health Plan Contact</h4></div>              
          <div class="gutter10-lr">
            <dl class="dl-horizontal dl-lr-style">
                <dt>Customer Service:</dt>
                <dd id="cust-service">$issuerPhone$</dd>
                <dt>Web:</dt>
                <dd id="web-address">$issuerSite$</dd>
              </dl>                   
          </div>
        </div><!-- .gutter10-->
    </div> <!-- .span6 -->
    
 </div><!-- .row-fluid -->