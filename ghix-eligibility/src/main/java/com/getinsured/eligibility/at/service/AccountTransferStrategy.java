package com.getinsured.eligibility.at.service;

import java.util.List;
import java.util.Map;

import com.getinsured.eligibility.at.resp.si.dto.AtAdditionalParamsDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;



/**
 * Encapsulates service layer method calls for AccountTransfer.
 *
 * @author Ekram Ali Kazi
 */
public interface AccountTransferStrategy{

	
	/**
	 * 
	 * @param request: AT request 
	 * @param giwsPayloadId: gi_ws_payload identifier for AT request
	 * @param requester: requester of the method - AT_PROCESS or MES_BATCH_PROCESS
	 * @param additionalFieldsMapJson: fields required if requester is other than AT_PROCESS
	 */
	void process(AccountTransferRequestPayloadType request,AtAdditionalParamsDTO additionalParams);

	List<String> validate(AccountTransferRequestPayloadType request);
	
	void processEditApplication(String caseNumber);
}
