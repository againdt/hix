package com.getinsured.hix.indportal.dto;

public class MyAppealDetails {
	private String appealId;
	private String appealDescription;
	private String createdDate;
	private String currentStatus;
	private String appealClosedDate;
	
	public String getAppealId() {
		return appealId;
	}
	public void setAppealId(String appealId) {
		this.appealId = appealId;
	}
	public String getAppealDescription() {
		return appealDescription;
	}
	public void setAppealDescription(String appealDescription) {
		this.appealDescription = appealDescription;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	public String getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(String currentStatus) {
		this.currentStatus = currentStatus;
	}
	public String getAppealClosedDate() {
		return appealClosedDate;
	}
	public void setAppealClosedDate(String appealClosedDate) {
		this.appealClosedDate = appealClosedDate;
	}
	

}
