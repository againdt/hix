#!/bin/bash 
# this script is to be attached at the end of the deploy scripts such as DeployMaster.sh so that ghix-batch.war file can be unfolded
# without running this step, command line job runner will not run
	echo 'Unzipping ghix-batch.war...'
	sudo rm -rf  /home/jboss/default/standalone/deployments/ghix-batch
	sudo mkdir /home/jboss/default/standalone/deployments/ghix-batch
	sudo unzip -q /home/jboss/default/standalone/deployments/ghix-batch.war -d /home/jboss/default/standalone/deployments/ghix-batch
