package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("enrollment1095Out")
public class Enrollment1095Out {

	List<Integer> enrollmentIdList=null;

	int initialPdfCount;
	int correctedPdfCount;
	int voidPdfCount;
	int resendPdfCount;
	int skippedPdfCount;
	
	public Enrollment1095Out() {
		enrollmentIdList= new ArrayList<Integer>();
		initialPdfCount=0;
		correctedPdfCount=0;
		voidPdfCount=0;
		resendPdfCount=0;
		resendPdfCount=0;
		skippedPdfCount=0;
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}
	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}

	
	public synchronized void incrementInitialPdfCount(int incrementBy){
		initialPdfCount=initialPdfCount+incrementBy;
	}
	
	public int getInitialPdfCount(){
		return initialPdfCount;
	}
	
	public synchronized void resetInitialPdfCount(){
		initialPdfCount=0;
	}

	public synchronized void incrementCorrectedPdfCount(int incrementBy){
		correctedPdfCount=correctedPdfCount+incrementBy;
	}
	
	public int getCorrectedPdfCount(){
		return correctedPdfCount;
	}
	
	public synchronized void resetCorrectedPdfCount(){
		correctedPdfCount=0;
	}
	
	public synchronized void incrementVoidPdfCount(int incrementBy){
		voidPdfCount=voidPdfCount+incrementBy;
	}
	
	public int getVoidPdfCount(){
		return voidPdfCount;
	}
	
	public synchronized void resetVoidPdfCount(){
		voidPdfCount=0;
	}
	
	
	public synchronized void incrementResendPdfCount(int incrementBy){
		resendPdfCount=resendPdfCount+incrementBy;
	}
	
	public int getResendPdfCount(){
		return resendPdfCount;
	}
	
	public synchronized void resetResendPdfCount(){
		resendPdfCount=0;
	}
	
	public synchronized void incrementSkippedPdfCount(int incrementBy){
		skippedPdfCount=skippedPdfCount+incrementBy;
	}
	
	public int getSkippedPdfCount(){
		return skippedPdfCount;
	}
	
	public synchronized void resetSkippedPdfCount(){
		skippedPdfCount=0;
	}
}
