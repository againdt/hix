package com.getinsured.hix.batch.provider;

/**
 * Class to log provider error in CSV file
 * 
 *
 */
public class ProviderCsvLine {

	private int lineNo;
	private int column;
	private String fieldName;
	private String message;
	
	public int getLineNo() {
		return lineNo;
	}
	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}
	public int getColumn() {
		return column;
	}
	public void setColumn(int column) {
		this.column = column;
	}
	public String getFieldName() {
		return fieldName;
	}
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	} 
	
	
}
