package com.getinsured.hix.account.web;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpSession;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.platform.test.GhixBaseMvcTest;
import com.getinsured.hix.util.PlanMgmtConstants;

/**
 * @author liu_j
 * @Created 11-APR-2014  This is the first test written to use Mockmvc which uses URL/MVC model to test.
 * 
 * BaseTest needs to have these annotations
	@RunWith(SpringJUnit4ClassRunner.class)
	@WebAppConfiguration
	@ContextConfiguration({ 
	"classpath:/applicationSecurity-db-unittests.xml",
	"classpath:/applicationContext.xml"})
 *  
 *  */

public class UserControllerMockMvcTest  extends GhixBaseMvcTest{
	
	public static class MockSecurityContext implements SecurityContext {

        private static final long serialVersionUID = -1386535243513362694L;

        private Authentication authentication;

        public MockSecurityContext(Authentication authentication) {
            this.authentication = authentication;
        }

        @Override
        public Authentication getAuthentication() {
            return this.authentication;
        }

        @Override
        public void setAuthentication(Authentication authentication) {
            this.authentication = authentication;
        }
    }
	
	@Autowired UserController userController; 
	private MockMvc mockMvc;
	private MockHttpSession session = new MockHttpSession();

    @Resource
    private FilterChainProxy springSecurityFilterChain;
    
    @Autowired
    private WebApplicationContext webApplicationContext;
	
    @Before
    public void mySetUp() {

    	// setup mock MVC
        this.mockMvc = MockMvcBuilders
                .webAppContextSetup(this.webApplicationContext)
                .addFilters(this.springSecurityFilterChain)
                .build();
        
		UsernamePasswordAuthenticationToken principal = 
		userController.getPrincipal("exadmin@ghix.com");
		SecurityContextHolder.getContext().setAuthentication(principal);        

		session.setAttribute(
    		  HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, 
    		  new MockSecurityContext(principal));
        
    }
    
	@Test
    public void signedIn() throws Exception {
        mockMvc.perform(get("/account/user/login")
                .session(session))
        .andExpect(status().isOk());
	}
	
	@Test
	public void updatePartner() throws Exception {
		final String issuerId = "75605";
		final String partnerId = "94";
		final String redirectUrl = "ticketmgmt/ticket/ticketlist";	
		
		ExchgPartnerLookup partner = new ExchgPartnerLookup();
		partner.setInsurerName("Blue Cross Blue Shield of New Mexico");
		partner.setSt01("834");
		partner.setGs08("834");
		partner.setArchiveDir("/home/tibcouser/data/archive");
		partner.setDirection("Inbound");
		partner.setInprogressDir("home/tibcouser/data/inprocess");
		partner.setMarket("24");
		partner.setRoleId(110);
		partner.setSourceDir("/mnt/FTPnfs-mnt/75605/in");
		
		mockMvc.perform(post("/admin/issuer/partner/save")
				.flashAttr(PlanMgmtConstants.LIST_EXCHG_PARTNERS_OBJ, partner)
				.param(PlanMgmtConstants.ISSUER_ID, issuerId)
				.param(PlanMgmtConstants.PARTNER_ID, partnerId)
				.param(PlanMgmtConstants.REDIRECT_TO, redirectUrl)
				.session(session))
				.andExpect(model().attributeHasNoErrors(PlanMgmtConstants.LIST_EXCHG_PARTNERS_OBJ));
	}
	
	@Test
	public void listPartner() throws Exception {
		final String id = "75605";
		final String partnerId = "94";

		MvcResult result = mockMvc.perform(post("/admin/issuer/partner/edit/"+id)
				.param("id", id)
				.param(PlanMgmtConstants.PARTNER_ID, partnerId)
				.session(session))
				.andExpect(model()
				.attributeHasNoErrors(PlanMgmtConstants.LIST_EXCHG_PARTNERS_OBJ))
				.andReturn();
		String content = result.getResponse().getContentAsString();
		System.out.println(content);
	}
}