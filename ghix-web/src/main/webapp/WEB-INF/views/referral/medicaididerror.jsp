<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1>
			<spring:message code="label.referral.pageheader" />
		</h1>
	</div>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
				<h4>
					<spring:message code="label.referral.sideheader" />
				</h4>
			</div>
			<ul class="nav nav-list">
				<li class="active"><spring:message
						code="label.referral.linktoapplication" /></li>
			</ul>
		</div>
		<div id="rightpanel" class="span9 dashboard-rightpanel">
			<div class="header">
				<h4>Application Link Error</h4>
			</div>
			<div class="gutter10 center-text">
				<p> <spring:message code="msg.referral.medicaidid.error" /></p><br>
				<p><a href="<c:url value='/indportal' />" class="btn btn-primary" >Go To DashBoard</a></p>
			</div>
		</div>

	</div>
</div>
