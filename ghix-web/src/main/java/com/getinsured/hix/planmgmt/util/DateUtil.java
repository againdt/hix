/**
 * 
 */
package com.getinsured.hix.planmgmt.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.util.PlanMgmtConstants;


/**
 * @author gorai_k
 *
 */
public class DateUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(DateUtil.class);
	
	/**
	 * Converts the string to java.util.Date
	 * 
	 * @param dateStr
	 * @param format
	 * @return java.util.Date
	 */
	public static Date StringToDate(String dateStr, String format) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat(format);
	    dateFormat.setLenient(false);
	    dateFormat.setTimeZone(PlanMgmtConstants.getGlobalTimeZone());
	    Date convertedDate = null;
		try {
			convertedDate = dateFormat.parse(dateStr);
		} catch (ParseException e) {
			LOGGER.error("ParseException",e);
		}
	    return convertedDate;
	}
	
	/**
	 * Converts the java.util.Date to String with a user specified format
	 * 
	 * @param date (java.util.Date)
	 * @param requiredFormat
	 * @return
	 */
	public static String dateToString(java.util.Date date, String requiredFormat) {
		DateFormat formatter = new SimpleDateFormat(requiredFormat);
		formatter.setTimeZone(PlanMgmtConstants.getGlobalTimeZone());
		return  formatter.format(date);
	}
	
	 /**
     * Converts the date String of one format to date String of another format
     * 
     * @param dateStr
	 * @param currentformat
     * @param requiredFormat
     * @return
     */
    public static String changeFormat(String dateStr, String currentformat, String requiredFormat){
    	String coverageStartDate = "";
    	try{
    		SimpleDateFormat sdfSource = new SimpleDateFormat(currentformat);
    		Date date = sdfSource.parse(dateStr);
    		SimpleDateFormat sdfDestination = new SimpleDateFormat(requiredFormat);
    		coverageStartDate = sdfDestination.format(date);
    	}catch(ParseException e){
    		LOGGER.error("ParseException",e);
	    }
    	return coverageStartDate;
    }
    
    /**
     * 
     * Converts the date( To a String ) in a timezone specific way
     * 
     * @param dateStr
     * @param currentformat
     * @param requiredFormat
     * @return
     */
    public static String changeFormatWithTimezone(String dateStr, String currentformat, String requiredFormat){
    	String coverageStartDate = "";
    	try{
    		SimpleDateFormat sdfSource = new SimpleDateFormat(currentformat);
    		Date date = sdfSource.parse(dateStr);
    		SimpleDateFormat sdfDestination = new SimpleDateFormat(requiredFormat);
    		sdfDestination.setTimeZone(PlanMgmtConstants.getGlobalTimeZone());
    		coverageStartDate = sdfDestination.format(date);
    	}catch(ParseException e){
    		LOGGER.error("ParseException",e);
	    }
    	return coverageStartDate;
    }
}
