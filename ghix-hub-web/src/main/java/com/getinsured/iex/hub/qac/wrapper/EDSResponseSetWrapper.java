package com.getinsured.iex.hub.qac.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.EDSResponseSetType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.InsuranceApplicantType;

public class EDSResponseSetWrapper implements JSONAware{
	private InsuranceApplicantResponseWrapper insuranceApplicant;
	
	public EDSResponseSetWrapper(EDSResponseSetType response){
		if(response != null){
			InsuranceApplicantType x = response.getResponseApplicant();
			
			if(x != null){
				this.insuranceApplicant = new InsuranceApplicantResponseWrapper(x);
			}
		}
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseApplicant", this.insuranceApplicant);
		return obj.toJSONString();
	}

}
