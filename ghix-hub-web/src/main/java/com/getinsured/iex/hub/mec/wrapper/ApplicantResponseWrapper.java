package com.getinsured.iex.hub.mec.wrapper;



import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantMECInformationType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ApplicantResponseType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.InsuranceApplicantResponseType;
import com.getinsured.iex.hub.mec.cms.dsh.vesim.extension._1.ResponsePersonType;
import com.getinsured.iex.hub.mec.cms.hix._0_1.hix_core.ResponseMetadataType;



/**
 * Wrapper Class for ApplicantResponseWrapper
 * 
 * @author Vishaka Tharani
 * @since 24-Feb-2014
 */

public class ApplicantResponseWrapper implements JSONAware{

	private ResponsePersonWrapper personWrapper;
    private InsuranceApplicantResponseWrapper insuranceApplicantResponseWrapper;
    private Boolean inconsistencyIndicator;
  
	private String responseCode;
	private String responseDesc;
	private ApplicantMECInformationWrapper mecInfoWrapper;
    
 	public ApplicantResponseWrapper(ApplicantResponseType response){
 		
 		if(response != null)
 		{
 	 		ResponseMetadataType metadata = response.getResponseMetadata();
	 		
	 		if(metadata != null){
	 			this.responseCode = metadata.getResponseCode().getValue();
	 			this.responseDesc = metadata.getResponseDescriptionText().getValue();
	 		}
	 		
	 		ApplicantMECInformationType mecInfo = response.getApplicantMECInformation();
	 		
	 		if(mecInfo != null)
	 		{
	 			this.mecInfoWrapper = new ApplicantMECInformationWrapper(mecInfo);
	 			
	 			com.getinsured.iex.hub.mec.niem.niem.proxy.xsd._2.Boolean bIndicator =  mecInfo.getInconsistencyIndicator();
	 			
	 			if(bIndicator != null)
	 			{
	 				this.inconsistencyIndicator =  bIndicator.isValue();
	 			}
	 			
	 			InsuranceApplicantResponseType  insuranceApplicantResponseType = mecInfo.getInsuranceApplicantResponse();
	 			
	 			if(insuranceApplicantResponseType != null)
	 			{
	 				insuranceApplicantResponseWrapper = new InsuranceApplicantResponseWrapper(insuranceApplicantResponseType);
	 			}
	 		}
	 		ResponsePersonType personType = response.getResponsePerson();
	 		
	 		if(personType != null)
	 		{
	 			this.personWrapper = new ResponsePersonWrapper(personType);
	 		}
 		}
 	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseText", this.responseDesc);
		obj.put("Person", this.personWrapper);
		obj.put("MECInfo", this.mecInfoWrapper);
		obj.put("InsuranceApplicant", this.insuranceApplicantResponseWrapper);
		obj.put("InconsistencyIndicator", inconsistencyIndicator);
		return obj.toJSONString();
	}
}
