package com.getinsured.eligibility.at.ref.service;

import com.getinsured.iex.ssap.SingleStreamlinedApplication;

/**
 * @author chopra_s
 * 
 */
public interface ReferralApplicationCompareService {
	void executeCompare(SingleStreamlinedApplication singleStreamlinedApplication, long compareApplicationId);
}
