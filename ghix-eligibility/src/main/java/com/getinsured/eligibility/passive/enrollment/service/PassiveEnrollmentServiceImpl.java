package com.getinsured.eligibility.passive.enrollment.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.at.ref.service.ReferralProcessingService;
import com.getinsured.eligibility.at.ref.service.ReferralSsapApplicationService;
import com.getinsured.eligibility.at.resp.si.handler.helper.ApplicantEligibilityHelper;
import com.getinsured.eligibility.at.resp.si.handler.helper.NativeAmericanEligibilityHelper;
import com.getinsured.eligibility.benchmark.service.BenchmarkPlanService;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.eligibility.phix.service.PhixService;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.IEligibilityProgram;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

@Service("passiveEnrollmentService")
public class PassiveEnrollmentServiceImpl implements PassiveEnrollmentService {

	private static final String N = "N";

	private static final String Y = "Y";

	private static final String CONVERSION = "CONVERSION";

	private static final Logger LOGGER = Logger.getLogger(PassiveEnrollmentServiceImpl.class);

	private static final String COVERAGE_START_DATE_STR = "01/01/2015 00:00:00";

	private static final String COVERAGE_END_DATE_STR = "12/31/2015 23:59:59";

	private static final String DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";

	private static final Timestamp COVERAGE_END_DATE_TS;

	private static final Timestamp COVERAGE_START_DATE_TS;

	private static final Date COVERAGE_END_DATE;
	private static final Date COVERAGE_START_DATE;

	private static final String ELIBILITY_INDICATOR_TRUE = "TRUE";
    private static final String EXCHANGE_ELIGIBILITY_TYPE = "ExchangeEligibilityType";
    private static final String CSR_ELIGIBILITY_TYPE = "CSREligibilityType";
    private static final String ALLOW_ENROLLMENT = Y;

	static{
		try {
			COVERAGE_END_DATE_TS = new Timestamp((new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_END_DATE_STR)).getTime()));
			COVERAGE_START_DATE_TS = new Timestamp((new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_START_DATE_STR)).getTime()));

			COVERAGE_START_DATE = new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_START_DATE_STR));
			COVERAGE_END_DATE = new SimpleDateFormat(DATE_FORMAT).parse((COVERAGE_END_DATE_STR));
		} catch (ParseException e) {
			throw new GIRuntimeException(ReferralProcessingService.PARSE_EXCEPTION);
		}
	}

	@Autowired
	@Qualifier("cmrHouseholdRepository")
	private CmrHouseholdRepository cmrHouseholdRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private ILocationRepository iLocationRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private ReferralSsapApplicationService referralSsapApplicationService;

	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;

	@Autowired
    private IEligibilityProgram eligibilityProgramRepository;

    @Autowired
    private BenchmarkPlanService benchmarkPlanService;

	@Autowired
	private LookupService lookupService;

	@Value("#{configProp['nativeAmerican.default.cslevel'] != null ? configProp['nativeAmerican.default.cslevel'] : 'CS3'}")
    private String nativeAmericanHouseholdCSLevel;

    @Autowired
    private NativeAmericanEligibilityHelper nativeAmericanEligibilityHelper;
    
    @Autowired
    private ApplicantEligibilityHelper applicantEligibilityHelper;

    @Autowired private PhixService phixService;

	String RIDP_VERIFIED_YES = Y;

	String RIDP_VERIFIED_NO = N;

	int PRIMARY_PERSON = 1;

	@Override
	public Long execute(String ssapJson) {
		LOGGER.debug("--> Start Execute");
		SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapJson);
		SsapApplication ssapApplication = referralSsapApplicationService.executeCreate(ssapJson, singleStreamlinedApplication);
		ssapApplication = fetchCurrentApplication(ssapApplication.getCaseNumber());
		/* process member eligibility and update applicant info */
		processEligibility(ssapApplication);
		createSsapApplicationEvent(ssapApplication);
		Household household = createHH(ssapApplication);
		handlePost(household,ssapApplication);

		LOGGER.debug("--> End Execute");
		return ssapApplication.getId();
	}

	private void handlePost(Household household, SsapApplication ssapApplication) {
		LOGGER.debug("--> Start handlePost for application " + ssapApplication.getId());
		
		Date currentDate = new TSDate();
		
		BigDecimal householdId = new BigDecimal(household.getId());
		
		String hhNativeAmerican = setNativeamericanStatus(ssapApplication) ? Y : N;
		String csrLevel = null;
        if(Y.equals(hhNativeAmerican)) {
        	csrLevel = nativeAmericanHouseholdCSLevel;
        }

        String status = ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode();
        EligibilityStatus eligibilityStatus = EligibilityStatus.AE;
        ExchangeEligibilityStatus exchangeEligibilityStatus = ExchangeEligibilityStatus.QHP;
        String allowEnrollment = ALLOW_ENROLLMENT;
        Date eligibilityReceivedDate = currentDate;

        LOGGER.debug("--> In handlePost for application update " + ssapApplication.getId());
        ssapApplicationRepository.updatePassiveEnrollmentApplication(ssapApplication.getId(), householdId, 
        		hhNativeAmerican, csrLevel, status, 
        		eligibilityStatus, exchangeEligibilityStatus, 
        		allowEnrollment, eligibilityReceivedDate, new Timestamp(currentDate.getTime()));
        
        LOGGER.debug("--> End handlePost for application " + ssapApplication.getId());
	}

	private void processEligibility(SsapApplication ssapApplication) {
		LOGGER.debug("--> Start processEligibility for application " + ssapApplication.getId());

		Date currentDate = new TSDate();
		SsapApplicant primaryApplicant = null;
		
        boolean isHHNA = setNativeamericanStatus(ssapApplication);
        List<EligibilityProgram> eligibilityProgramList = new ArrayList<EligibilityProgram>();

		List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
		for(SsapApplicant applicant : ssapApplicants ) {
            EligibilityProgram eligibilityProgramDO = new EligibilityProgram();
            eligibilityProgramDO.setEligibilityType(isHHNA ? CSR_ELIGIBILITY_TYPE : EXCHANGE_ELIGIBILITY_TYPE);
            eligibilityProgramDO.setEligibilityIndicator(ELIBILITY_INDICATOR_TRUE);
            eligibilityProgramDO.setEligibilityStartDate(COVERAGE_START_DATE);
            eligibilityProgramDO.setEligibilityEndDate(COVERAGE_END_DATE);
            eligibilityProgramDO.setEligibilityDeterminationDate(currentDate);
            eligibilityProgramDO.setSsapApplicant(applicant);
            eligibilityProgramList.add(eligibilityProgramDO);
            eligibilityProgramRepository.save(eligibilityProgramDO);
            if(applicant.getPersonId() == 1L) {
            	primaryApplicant = applicant;
            }
            applicant.setEligibilityStatus(ExchangeEligibilityStatus.QHP.toString());
            
		}
        applicantEligibilityHelper.setSsapApplicationStartDate(ssapApplication, eligibilityProgramList);
		
		for (SsapApplicant applicant : ssapApplicants) {
			if(applicant.getPersonId() != 1L && primaryApplicant != null) {
				applicant.setMailiingLocationId(primaryApplicant.getMailiingLocationId());
				applicant.setOtherLocationId(primaryApplicant.getOtherLocationId());
			}
		}
		
		ssapApplicantRepository.save(ssapApplicants);
		LOGGER.debug("--> End processEligibility for application " + ssapApplication.getId());
	}


	private boolean setNativeamericanStatus(SsapApplication ssapApplication) {
		LOGGER.debug("--> Start setNativeamericanStatus for application " + ssapApplication.getId());

		
        boolean isHouseholdNativeAmerican = true;
        for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
            if (Y.equals(applicant.getApplyingForCoverage()) && !Y.equals(applicant.getNativeAmericanFlag())){
                isHouseholdNativeAmerican = false;
                break;
            }
        }

        LOGGER.debug("--> End setNativeamericanStatus for application " + ssapApplication.getId());
        return isHouseholdNativeAmerican;

    }

	private void createSsapApplicationEvent(SsapApplication ssapApplication) {
		LOGGER.debug("--> Start createSsapApplicationEvent for application " + ssapApplication.getId());

		Date date = new TSDate();
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedTimeStamp(new Timestamp(date.getTime()));
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ssapApplicationEvent.setCoverageStartDate(COVERAGE_START_DATE_TS);
		ssapApplicationEvent.setCoverageEndDate(COVERAGE_END_DATE_TS);
		ssapApplicationEvent.setEnrollmentStartDate(COVERAGE_START_DATE_TS);
		ssapApplicationEvent.setEnrollmentEndDate(COVERAGE_END_DATE_TS);
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.OE);
		ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		LOGGER.debug("--> End createSsapApplicationEvent for application " + ssapApplication.getId());
	}


	private Household createHH(SsapApplication ssapApplication){
		LOGGER.debug("--> Start createHH for application " + ssapApplication.getId());

		final SsapApplicant ssapApplicant = ssapApplicantRepository.findByPersonId(ssapApplication.getId(), PRIMARY_PERSON);

		EligLead eligLead = new EligLead();
		eligLead.setPhoneNumber(ssapApplicant.getPhoneNumber());
		eligLead.setStage(EligLead.STAGE.WELCOME);
		eligLead = phixService.saveEligLeadRecord(eligLead);

		final Location location = populateLocation(ssapApplicant);
		Household household = new Household();
		household.setFirstName(ssapApplicant.getFirstName());
		household.setLastName(ssapApplicant.getLastName());
		household.setBirthDate(ssapApplicant.getBirthDate());
		household.setPhoneNumber(ssapApplicant.getPhoneNumber());
		household.setRidpVerified(RIDP_VERIFIED_YES);
		household.setRidpDate(new TSDate());
		household.setRidpSource(CONVERSION);
		household.setEligLead(eligLead);
		household.setEmail(ssapApplicant.getEmailAddress());
		household = cmrHouseholdRepository.save(household);
		household.setLocation(location);
		LOGGER.debug("--> End createHH for application " + ssapApplication.getId());
		return cmrHouseholdRepository.save(household);
	}

	private Location populateLocation(SsapApplicant ssapApplicant) {
		// Other Location is the Primary Location
		if (ssapApplicant.getOtherLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getOtherLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getOtherLocationId().intValue());
		} else if (ssapApplicant.getMailiingLocationId() != null && ReferralUtil.isValidInt(ssapApplicant.getMailiingLocationId().intValue())) {
			return iLocationRepository.findOne(ssapApplicant.getMailiingLocationId().intValue());
		} else {
			return null;
		}
	}

	private SsapApplication fetchCurrentApplication(String caseNumber) {
		LOGGER.debug("--> Start fetchCurrentApplication for application " + caseNumber);
		List<SsapApplication> ssapApplicationList = ssapApplicationRepository.findByCaseNumber(caseNumber);

		if (ssapApplicationList.isEmpty()){
			throw new GIRuntimeException(EligibilityConstants.UNABLE_TO_FIND_SSAP_APPLICATION_IN_GI_TABLES);
		}
		SsapApplication currentApplication = ssapApplicationList.get(0);
		LOGGER.debug("--> End fetchCurrentApplication for application " + caseNumber);
		return currentApplication;
	}


}
