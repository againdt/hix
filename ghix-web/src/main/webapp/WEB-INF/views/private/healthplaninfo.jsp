<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<link rel="stylesheet" href="<c:url value="/resources/css/tiles.css" />" type="text/css">
<link media="screen,print" type="text/css" rel="stylesheet" href="/hix/resources/css/bootstrap-responsive.css">
<link media="screen,print" type="text/css" rel="stylesheet" href="/hix/resources/css/ghixcustom.css">

<%-- <script src="<c:url value="/resources/js/jquery-ui.1.10.2.js" />"></script> --%>

<script type="text/javascript">
  var plan_display = new Array();
  
  plan_display['pd.label.title.removeDentalPlan'] = "<spring:message code='pd.label.title.removeDentalPlan' javaScriptEscape='true'/>";
  plan_display['pd.label.title.showCompare'] = "<spring:message code='pd.label.title.showCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.hideCompare'] = "<spring:message code='pd.label.title.hideCompare' javaScriptEscape='true'/>";
  plan_display['pd.label.title.removeDental'] = "<spring:message code='pd.label.title.removeDental' javaScriptEscape='true'/>";  
  plan_display['pd.label.selectOneMember'] = "<spring:message code='pd.label.selectOneMember' javaScriptEscape='true'/>";
  plan_display['pd.label.adultDentalSelection'] = "<spring:message code='pd.label.adultDentalSelection' javaScriptEscape='true'/>";
  plan_display['pd.label.selectOnePlan'] = "<spring:message code='pd.label.selectOnePlan' javaScriptEscape='true'/>";
  plan_display['pd.label.dentalPlanAlreadyAdded'] = "<spring:message code='pd.label.dentalPlanAlreadyAdded' javaScriptEscape='true'/>";
  plan_display['pd.label.removeThisPlan'] = "<spring:message code='pd.label.removeThisPlan' javaScriptEscape='true'/>";
  
  plan_display['pd.label.selectPlanAlreadyInCart'] = "<spring:message code='pd.label.selectPlanAlreadyInCart' javaScriptEscape='true'/>";
  plan_display['pd.label.goToCart'] = "<spring:message code='pd.label.goToCart' javaScriptEscape='true'/>";
  plan_display['pd.label.title.addPlan'] = "<spring:message code='pd.label.title.addPlan' javaScriptEscape='true'/>";
</script>
<style>
#container-wrap {
	position: initial;
}
.table thead th strong a,
.table thead th a {
	font-weight: 400!important;
	font-size: 12px!important;
	border-bottom: 1px dotted #333;
}
.table thead th p {
	margin: 0;
}
.table thead th strong {
	font-weight: 400;
}
#benefits .span3 {
	padding-top: 8px;
}
td > p > small > a {
	margin: 0 0 0 50px;
}
.tooltip-inner {
    max-width: 24em;
    overflow: hidden;
}

.tooltip-inner div.qr-label {
	float: left;
	width: 220px;
	font-weight: bold;
}
.tooltip-inner div.qr-value {
	float: right;
}
</style>
 
<script>
  function resizeimage(imageObj){
	var maxWidth = 160;
	var maxHeight = 40;
    var width = imageObj.width;
    var height = imageObj.height;

    // Check if the current width is larger than the max
    if(width > maxWidth){
      ratio = maxWidth / width; // get ratio for scaling image
      imageObj.style.width =  maxWidth+'px'; // Set new width
      imageObj.style.height = (height * ratio)+'px'; // Scale height based on ratio
      height = height * ratio; // Reset height to match scaled image
      width = width * ratio; // Reset width to match scaled image
    }
    // Check if current height is larger than max
    if(height > maxHeight){                                     
      ratio = maxHeight / height; // get ratio for scaling image
      imageObj.style.height= maxHeight+'px'; // Set new height
      imageObj.style.width = (width * ratio) +'px'; // Scale width based on ratio                                        
    }
    imageObj.style.display = "block";
  }

  function displaySubToDeduct(appliesNetwk){
    var str = "";
    if(appliesNetwk && appliesNetwk != "" && appliesNetwk != "Not Applicable"){
      str += "<i class='has-deductible-"+appliesNetwk+"'></i>";
    } 
    else{
      str += "<p><spring:message code='pd.label.commontext.notAvailable' htmlEscape='true'/></small></p>";
    } 
    document.write(str);
  }
  
  function displayAdditionalInfo(benefitExplanation, netwkException) {
	  var str = "";	  
	  	  
	  if((benefitExplanation != null && benefitExplanation != '') && (netwkException != null && netwkException != '')) {
		  str += "<a href='#' rel='tooltip' data-placement='top' data-html='true' data-original-title='<spring:message arguments='"+benefitExplanation+","+netwkException+"' code='pd.tooltip.title.explanationAndExclusions' htmlEscape='false'/>'><spring:message code='pd.label.title.view' htmlEscape='true'/></a></small>";
	  }
	  else if((benefitExplanation == null || benefitExplanation == '') && (netwkException != null && netwkException != '')) {
		  str += "<a href='#' rel='tooltip' data-placement='top' data-html='true' data-original-title='<spring:message arguments='"+netwkException+"' code='pd.tooltip.title.limitsAndExclusions' htmlEscape='false'/>'><spring:message code='pd.label.title.view' htmlEscape='true'/></a></small>";
	  }
	  else if((benefitExplanation != null && benefitExplanation != '') && (netwkException == null || netwkException == '')) {
		  str += "<a href='#' rel='tooltip' data-placement='top' data-html='true' data-original-title='<spring:message arguments='"+benefitExplanation+"' code='pd.tooltip.title.benefitExplanation' htmlEscape='false'/>'><spring:message code='pd.label.title.view' htmlEscape='true'/></a></small>";
	  }
	  else {
		  str += "";
	  }	  
	  document.write(str);
  }
  
  function displayIndDeductibleAndMaxOOP(benefit) {
	  var str = "";
	  var deductible = buildObject(benefit);
	  
	  if((deductible.inNetworkInd != null && deductible.inNetworkInd != '') || (deductible.inNetworkTier2Ind != null && deductible.inNetworkTier2Ind != '') || (deductible.outNetworkInd != null && deductible.outNetworkInd != '') || (deductible.combinedInOutNetworkInd != null && deductible.combinedInOutNetworkInd != '')) {
		  if(deductible.inNetworkInd != null && deductible.inNetworkInd != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkInd+" (<spring:message code='pd.label.title.inNetwork' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.inNetworkTier2Ind != null && deductible.inNetworkTier2Ind != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkTier2Ind+" (<spring:message code='pd.label.title.tier2Network' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.outNetworkInd != null && deductible.outNetworkInd != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.outNetworkInd+" (<spring:message code='pd.label.title.outOfNetwork' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.combinedInOutNetworkInd != null && deductible.combinedInOutNetworkInd != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.combinedInOutNetworkInd+" (<spring:message code='pd.label.title.combinedAndOutOfNetwork' htmlEscape='true'/>)";
		  }
	  }
	  else {
		  str += "<spring:message code='pd.label.commontext.notApp' htmlEscape='true'/></small>";
	  }
	  document.write(str);
  }
  
  function displaySimplifiedInNetworkDeductible(benefit) {
	  var str = "";
	  var deductible = buildObject(benefit);
	  
	  if(deductible.inNetworkInd != null && deductible.inNetworkInd != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkInd+" (<spring:message code='pd.label.commontext.individual' htmlEscape='true'/>)<br/>";
	  }
	  if(deductible.combinedInOutNetworkInd != null && deductible.combinedInOutNetworkInd != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.combinedInOutNetworkInd+" (<spring:message code='pd.label.commontext.individual' htmlEscape='true'/> <spring:message code='pd.label.title.combinedAndOutOfNetwork' htmlEscape='true'/>)<br/>";
	  }
	  if(deductible.inNetworkFly != null && deductible.inNetworkFly != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkFly+" (<spring:message code='pd.label.commontext.family' htmlEscape='true'/>)<br/>";
	  }
	  if(deductible.combinedInOutNetworkFly != null && deductible.combinedInOutNetworkFly != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.combinedInOutNetworkFly+" (<spring:message code='pd.label.commontext.individual' htmlEscape='true'/> <spring:message code='pd.label.title.combinedAndOutOfNetwork' htmlEscape='true'/>)<br/>";
	  }
	 
	  document.write(str);
  }
  function displaySimplifiedOutNetworkDeductible(benefit) {
	  var str = "";
	  var deductible = buildObject(benefit);
	  
	  if(deductible.outNetworkInd != null && deductible.outNetworkInd != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.outNetworkInd+" (<spring:message code='pd.label.commontext.individual' htmlEscape='true'/>)<br/>";
	  }
	  if(deductible.outNetworkFly != null && deductible.outNetworkFly != '') {
		  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.outNetworkFly+" (<spring:message code='pd.label.commontext.family' htmlEscape='true'/>)<br/>";
	  }
	  document.write(str);
  }
  
  function displaySimplifiedOptionalDeductible(optionalDeductiblesStr){
		var str = "";
		var benefitAttr = optionalDeductiblesStr.split("=",1);
		var deductibleName = benefitAttr[0];
		var index = optionalDeductiblesStr.indexOf("=");  // Gets the first index where a space occours
		var text = optionalDeductiblesStr.substr(index + 1);
		var deductible = buildObject(text);
	  	str += "<span>" + deductibleName + " : </span>";
		if(deductible.inNetworkInd != null && deductible.inNetworkInd != ""){
		  str += "<span><spring:message code='pd.label.dollarSign'/>" + parseFloat(deductible.inNetworkInd).toFixed() + " </span>";
	   	} 	   	
		str += "<br><br>";
			
		document.write(str);
	  }
  
  function displayFlyDeductibleAndMaxOOP(benefit) {
	  var str = "";
	  var deductible = buildObject(benefit);
	  
	  if((deductible.inNetworkFly != null && deductible.inNetworkFly != '') || (deductible.inNetworkTier2Fly != null && deductible.inNetworkTier2Fly != '') || (deductible.outNetworkFly != null && deductible.outNetworkFly != '') || (deductible.combinedInOutNetworkFly != null && deductible.combinedInOutNetworkFly != '')) {
		  if(deductible.inNetworkFly != null && deductible.inNetworkFly != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkFly+" (<spring:message code='pd.label.title.inNetwork' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.inNetworkTier2Fly != null && deductible.inNetworkTier2Fly != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.inNetworkTier2Fly+" (<spring:message code='pd.label.title.tier2Network' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.outNetworkFly != null && deductible.outNetworkFly != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.outNetworkFly+" (<spring:message code='pd.label.title.outOfNetwork' htmlEscape='true'/>)<br/>";
		  }
		  if(deductible.combinedInOutNetworkFly != null && deductible.combinedInOutNetworkFly != '') {
			  str += "<spring:message code='pd.label.dollarSign'/>"+deductible.combinedInOutNetworkFly+" (<spring:message code='pd.label.title.combinedAndOutOfNetwork' htmlEscape='true'/>)";
		  }
	  }
	  else {
		  str += "<spring:message code='pd.label.commontext.notApp' htmlEscape='true'/></small>";
	  }
	  document.write(str);
  }
  
  function buildObject(benefit){
	var benefitObj = new Object();
    var benefitAttr = benefit.replace(/{|}/gi,"").split(", ");
    for (var benefitName in benefitAttr){
	   	var val = benefitAttr[benefitName].split("=");
    	benefitObj[val[0]] = val[1];
    }
    return benefitObj;
  }
  
  function displayOptionalDeductible(optionalDeductiblesStr){
	var str = "";
	var benefitAttr = optionalDeductiblesStr.split("=",1);
	var deductibleName = benefitAttr[0];
	var index = optionalDeductiblesStr.indexOf("=");  // Gets the first index where a space occours
	var text = optionalDeductiblesStr.substr(index + 1);
	var deductible = buildObject(text);
  	//var deductible = buildObject(optionalDeductiblesStr.replace(deductibleName));
  	str += "<span>" + deductibleName + " : </span>";
	if(deductible.inNetworkInd != null && deductible.inNetworkInd != ""){
	  str += "<span><spring:message code='pd.label.dollarSign'/>" + parseFloat(deductible.inNetworkInd).toFixed() + "  <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>";
   	} else if(deductible.combinedInOutNetworkInd != null && deductible.combinedInOutNetworkInd != ""){ 
   	  str += "<span><spring:message code='pd.label.dollarSign'/>" + parseFloat(deductible.combinedInOutNetworkInd).toFixed() + " <spring:message code='pd.label.commontext.individualSmall' htmlEscape='true'/></span>";
   	}
   	if(deductible.inNetworkInd != null && deductible.inNetworkInd != "" && deductible.inNetworkFly != null && deductible.inNetworkFly != ""){
      str += "<span>,&nbsp;</span>";
	} else if(deductible.combinedInOutNetworkInd != null && deductible.combinedInOutNetworkInd != "" && deductible.combinedInOutNetworkFly != null && deductible.combinedInOutNetworkFly != ""){
	  str += "<span>,&nbsp;</span>";
	}
	if(deductible.inNetworkFly != null && deductible.inNetworkFly != ""){
	  str += "<span><spring:message code='pd.label.dollarSign'/>" + parseFloat(deductible.inNetworkFly).toFixed() + " <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>";
    } else if(deductible.combinedInOutNetworkFly != null && deductible.combinedInOutNetworkFly != ""){
      str += "<span>$" + parseFloat(deductible.combinedInOutNetworkFly).toFixed() + " <spring:message code='pd.label.commontext.familySmall' htmlEscape='true'/></span>";
    }
	str += "<br><br>";
		
	document.write(str);
  }
  
</script>

<script type="text/javascript">
function issuerRatingsTooltip()
{
	var issuerQualityRating = '${individualPlan.issuerQualityRating}';
 	var data = '';
 	if ('${individualPlan.issuerQualityRating}'!=null &&(('${individualPlan.issuerQualityRating.SummaryClinicalQualityManagement}' != null && '${individualPlan.issuerQualityRating.SummaryClinicalQualityManagement}' != '') ||('${individualPlan.issuerQualityRating.SummaryEnrolleeExperience}' != null && '${individualPlan.issuerQualityRating.SummaryEnrolleeExperience}' != '') ||('${individualPlan.issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement}' != null && '${individualPlan.issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement}' != ''))) {
 		if('${individualPlan.issuerQualityRating.QualityRating}' != null && '${individualPlan.issuerQualityRating.QualityRating}' != ""){
			data = data + '<b><spring:message code="pd.label.tooltip.qualityRatings.1"/></b><br>';
			for(var j = 1; j <= 5; j++){ 
				if(j <= '${individualPlan.issuerQualityRating.QualityRating}'){  
					data = data + '<i class=\'icon icon-star\'></i>';
				} else if ('${individualPlan.issuerQualityRating.QualityRating}' % 1 != 0 && Math.ceil('${individualPlan.issuerQualityRating.QualityRating}') == j) {
					data = data+'<i class=\'icon-star-half-empty\'></i>' 
 				} else {
    				data = data+'<i class=\'icon icon-star-empty\'></i>';
    			} 
    		} 
    	}
		data = data + '<spring:message code="pd.label.tooltip.qualityRatings.2"/>';
		if('${individualPlan.issuerQualityRating.SummaryClinicalQualityManagement}' != null && '${individualPlan.issuerQualityRating.SummaryClinicalQualityManagement}' != ""){
			data = data + '<div class="qr-label"><spring:message code="pd.label.tooltip.qualityRatings.3"/></div>';
			data = data + '<div class="qr-value">' + getQualitySubscoreBars('${individualPlan.issuerQualityRating.SummaryClinicalQualityManagement}') + '</div>';
		}
		if('${individualPlan.issuerQualityRating.SummaryEnrolleeExperience}' != null && '${individualPlan.issuerQualityRating.SummaryEnrolleeExperience}' != ""){
			data = data + '<div class="qr-label"><spring:message code="pd.label.tooltip.qualityRatings.4"/></div>';
			data = data + '<div class="qr-value">' + getQualitySubscoreBars('${individualPlan.issuerQualityRating.SummaryEnrolleeExperience}') + '</div>';
		}
		if('${individualPlan.issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement}' != null && '${individualPlan.issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement}' != ""){
			data = data + '<div class="qr-label"><spring:message code="pd.label.tooltip.qualityRatings.5"/></div>';
			data = data + '<div class="qr-value">' + getQualitySubscoreBars('${individualPlan.issuerQualityRating.SummaryPlanEfficiencyAffordabilityAndManagement}') + '</div>';
		}
 	} else{
		data='';
 	} 
 	$("#qualityRatings").attr('data-original-title', data);
}


function getQualitySubscoreBars(subScore)
{
var subscoreData='';
if(subScore != null && subScore != ''){
	for(var j=1; j<=5; j++){ 
		if(j<=subScore){ 
			subscoreData=subscoreData+'<i class=\'icon icon-star\'></i>';
		} else if (subScore % 1 != 0 && Math.ceil(subScore) == j) {
			subscoreData=subscoreData+'<i class=\'icon-star-half-empty\'></i>' 
 		} else {
      		 subscoreData=subscoreData+'<i class=\'icon icon-star-empty\'></i>';
     	} 
     } 
}
return subscoreData;
}
</script>
<div class="row-fluid">  
    <h1>${individualPlan.issuer} &nbsp; - &nbsp; ${individualPlan.name}</h1>

    <div id="benefits" class="gutter10">
    
    <div class="table">
     
          <div class="row header">
          	<div class="cell header-name">
          		<p><strong><spring:message code="pd.label.title.informationFrom"/></strong></p>
          	</div>
          </div>
       
	      <div class="row-detail clearfix">
		       <div class="cell">
      <c:choose>
        <c:when test="${individualPlan.providerLink != null && fn:length(individualPlan.providerLink) != 0}">
		        	<a href="${individualPlan.providerLink}"><spring:message code="pd.label.link.providerDirectoryUrl"/></a>
        </c:when>
		        <c:otherwise><spring:message code="pd.label.link.providerDirectoryUrl"/> (<spring:message code="pd.label.commontext.notAvailable"/></small>)</c:otherwise>
      </c:choose>
		      	</div>
	      </div>
	      <div class="row-detail clearfix">
		      <div class="cell">
      <c:choose>
        <c:when test="${individualPlan.sbcUrl != null && fn:length(individualPlan.sbcUrl) != 0}">
		        	<a href="${individualPlan.sbcUrl}"><spring:message code="pd.label.link.sbcUrl"/></a>
        </c:when>
		        <c:otherwise><spring:message code="pd.label.link.sbcUrl"/> (<spring:message code="pd.label.commontext.notAvailable"/></small>)</c:otherwise>
      </c:choose>
		      	</div>
	      </div>
	      <div class="row-detail clearfix">
		      <div class="cell">
      <c:choose>
        <c:when test="${individualPlan.planBrochureUrl != null && fn:length(individualPlan.planBrochureUrl) != 0}">
		        	<a href="${individualPlan.planBrochureUrl}"><spring:message code="pd.label.link.planBrochureUrl"/></a>
        </c:when>
		        <c:otherwise><spring:message code="pd.label.link.planBrochureUrl"/> (<spring:message code="pd.label.commontext.notAvailable"/></small>)</c:otherwise>
      </c:choose>
		      </div>
	      </div>
	      <div class="row-detail clearfix">
				<div class="cell">
      <c:choose>
        <c:when test="${individualPlan.formularyUrl != null && fn:length(individualPlan.formularyUrl) != 0}">
					<a href="${individualPlan.formularyUrl}"><spring:message code="pd.label.link.formularyUrl"/></a>
        </c:when>
				<c:otherwise><spring:message code="pd.label.link.formularyUrl"/> (<spring:message code="pd.label.commontext.notAvailable"/></small>)</c:otherwise>
      </c:choose>
				</div>
	      </div>
     
	</div>
	
    <div class="table">    	
		<div class="row header">
		   	<div class="cell header-name">
		   		<p><strong><spring:message code="pd.label.title.summary"/></strong></p>
			</div>
		</div>
    	<div class="row-detail clearfix">
	       	<div class="cell">
	       		<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.summary.producttype" htmlEscape="true"/>"><spring:message code="pd.label.commontext.productType"/></a>
	       	</div>          
			<div class="details">
			
            <c:choose>
            <c:when test="${(individualPlan.networkType == 'PPO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.ppo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'HMO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hmo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'POS')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.pos" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'DHMO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dhmo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'DPPO')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.dppo" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:when test="${(individualPlan.networkType == 'HSA')}">
            	<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.script.networkType.hsa" htmlEscape="true"/>">${individualPlan.networkType}</a>
            </c:when>
            <c:otherwise>
            <c:choose>
	            <c:when test="${(individualPlan.networkType == null) || (individualPlan.networkType == '')}">
			  <div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div>
	            </c:when>
 		    </c:choose>
            </c:otherwise>
 		  </c:choose>
			</div>
       </div>  	
          <c:if test="${exchangeType != 'OFF' && PGRM_TYPE == 'Individual'}">
       <div class="row-detail clearfix">
			<div class="cell">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.discounts" htmlEscape="true"/>"><spring:message code="pd.label.summary.link6"/></a>
			</div>
	        <div class="details"><c:choose>
            	  <c:when test="${CSR != null && CSR != '' && CSR != 'CS1'}">
            	  	<c:choose>
            	  	  <c:when test="${(CSR == 'CS2' || CSR == 'CS3') && individualPlan.level != 'CATASTROPHIC'}">
            	  		<spring:message code="pd.label.script.specialSavings" htmlEscape="false"/>
            	  	  </c:when>
            	  	  <c:when test="${(CSR == 'CS4' || CSR == 'CS5' || CSR == 'CS6') && individualPlan.level == 'SILVER'}">
            	  	  	<spring:message code="pd.label.script.specialSavings" htmlEscape="false"/>
            	  	  </c:when>
            	  	  <c:otherwise>
	        	  		<spring:message code="pd.label.commontext.notAvailable" htmlEscape="false"/></small>
            	  	  </c:otherwise>
            	  	</c:choose>
            	  </c:when>
            	  <c:otherwise>
	        	  	<spring:message code="pd.label.commontext.notAvailable" htmlEscape="false"/></small>
            	  </c:otherwise>
            	</c:choose>
	        </div>
       </div></c:if>
       <div class="row-detail clearfix">
			<div class="cell">
				<a data-placement="right" rel="tooltip" href="#" data-original-title=""><spring:message code="pd.label.commontext.hsatype"/></a>
			</div>
			<div class="details">
           <c:choose>
            <c:when test="${(individualPlan.hsa != null) && (individualPlan.hsa != '')}">
              ${individualPlan.hsa}
            </c:when>
            </c:choose>
            </div>
 	 	</div>
 	 	<div class="row-detail clearfix"> 	
          <c:choose>
          <c:when test="${showQualityRating == 'YES'}">
           <div class="cell">
           		 <a data-placement="right" rel="tooltip" href="#" data-original-title=""><spring:message code="pd.label.title.qualityRatings"/></a>
           
          		<fmt:parseNumber var="rating" value="${individualPlan.qualityRating}"/>
                <fmt:parseNumber var="wholeRating" integerOnly="true" value="${rating}" />
           </div> 
           <div class="details">
				<a data-placement="top" rel="tooltip" href="#" id='qualityRatings' name='qualityRatings' data-html="true" data-original-title="">
					<c:choose>
						<c:when test="${(individualPlan.qualityRating != null) && (individualPlan.qualityRating != '')}">
							<c:forEach begin="1" end="5" var="val">
								<c:choose>
									<c:when test="${val <= rating}"><i class="icon icon-star"></i></c:when>
									<c:when test="${(rating % 1 != 0) && (wholeRating+1 == val)}"><i class="icon-star-half-empty"></i></c:when>
									<c:otherwise><i class="icon icon-star-empty"></i></c:otherwise>
								</c:choose>
							</c:forEach>
						</c:when>
						<c:otherwise><spring:message code="pd.label.quality.not.available"/></c:otherwise>
					</c:choose>
				</a>
            </div>
            </c:when></c:choose>
 	 	 </div>
     
    </div>
	
	
	<!-- Deductible & Out-of-Pocket -->
	<c:if test="${showSBCScenarios == 'ON'}">
	<div class="table">
     <div class="row header">
		   	<div class="cell header-name">
		   		<spring:message code="pd.label.title.SampleCareCosts"/>
			</div>
		</div>
      
        <div class="row-detail clearfix">
            <div class="cell"><spring:message code="pd.health.label.havingABaby"/></div>
            <div class="details">
            <c:choose>
               <c:when test="${(individualPlan.havingBabySBC != null) && (individualPlan.havingBabySBC >= 0)}">
               $${individualPlan.havingBabySBC}
               </c:when>
               <c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
               </c:choose>
	        </small>
	        </div>
        </div>
        <div class="row-detail clearfix">
            <div class="cell"><spring:message code="pd.health.label.havingDiabetes"/></div>
	        <div class="details">
            <c:choose>
               <c:when test="${(individualPlan.havingDiabetesSBC != null) && (individualPlan.havingDiabetesSBC >= 0)}">
               $${individualPlan.havingDiabetesSBC}
               </c:when>
               <c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
               </c:choose>
	        </small></div>
        </div>
        <div class="row-detail clearfix">
            <div class="cell"><spring:message code="pd.health.label.treatmentOfASimpleFracture"/></div>
	        <div class="details">
            <c:choose>
               <c:when test="${(individualPlan.simpleFractureSBC != null) && (individualPlan.simpleFractureSBC >= 0)}">
               $${individualPlan.simpleFractureSBC}
               </c:when>
               <c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
               </c:choose>
	        </small></div>
       </div>
     
    </div>
    </c:if>
	
	<c:choose>
    <c:when test="${simplifiedDeductible == 'ON' }">
	<div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.Deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/>
	   		</div>
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.inNetwork"/></a>
	   		</div>
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>"><spring:message code="pd.label.title.outOfNetwork"/></a>
	   		</div>
	   		<div class="u-table-cell cell u-table-cell--10">
	   		</div>

		</div>
      
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.Deductible"/></div>
			<div class="details u-table-cell cell">
				<c:choose>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG != null)}">
						<script>displaySimplifiedInNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG}")</script>
					</c:when>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
						<script>displaySimplifiedInNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_MEDICAL}")</script>
					</c:when>
				</c:choose>
			</div>
			<div class="details u-table-cell cell">
				<c:choose>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG != null)}">
						<script>displaySimplifiedOutNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG}")</script>
					</c:when>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
						<script>displaySimplifiedOutNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_MEDICAL}")</script>
					</c:when>
				</c:choose>
			</div>
			<div class="details u-table-cell cell u-table-cell--10">
			</div>
		</div>
		
		<c:if test="${individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_DRUG != null }">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.separate.drugdeductible"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_DRUG != null)}">
							<script>displaySimplifiedInNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_DRUG}")</script>
						</c:when>
					</c:choose>
				</div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_DRUG != null)}">
							<script>displaySimplifiedOutNetworkDeductible("${individualPlan.planCosts.DEDUCTIBLE_DRUG}")</script>
						</c:when>
					</c:choose>
				 </div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.deductible.oopmax"/></div>
			<div class="details u-table-cell cell">
				<c:choose>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG != null)}">
						<script>displaySimplifiedInNetworkDeductible("${individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG}")</script>
					</c:when>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
						<script>displaySimplifiedInNetworkDeductible("${individualPlan.planCosts.MAX_OOP_MEDICAL}")</script>
					</c:when>
				</c:choose>
			</div>
			<div class="details u-table-cell cell">
				<c:choose>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG != null)}">
						<script>displaySimplifiedOutNetworkDeductible("${individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG}")</script>
					</c:when>
					<c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
						<script>displaySimplifiedOutNetworkDeductible("${individualPlan.planCosts.MAX_OOP_MEDICAL}")</script>
					</c:when>
				</c:choose>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.maxCostPerPrescription"/></div>
			<div class="details u-table-cell cell">
				<c:if test="${(individualPlan.maxCoinseForSpecialtyDrugs != null && individualPlan.maxCoinseForSpecialtyDrugs != null)}">
					${individualPlan.maxCoinseForSpecialtyDrugs}
				</c:if>
			</div>                
		</div>
		</c:if>
		
		<c:if test="${fn:length(individualPlan.optionalDeductible) gt 0}">
			<div class="ps-detail__service" id="planDeductibleOtherDetail">
				<div class="u-table-cell cell"><a data-placement="right" rel="tooltipp" href="#"><spring:message code="pd.label.deductible.otherDeductibles"/></a></div>
				<div class="details u-table-cell cell">
					<c:forEach var="optionalDeductible" items="${individualPlan.optionalDeductible}">
						<script>displaySimplifiedOptionalDeductible("${optionalDeductible}")</script>
					</c:forEach>
				</div>            
			</div>
		</c:if>
    </div>
     
    </c:when>
    <c:otherwise>
    <div class="table">    
      	<div class="row header">
		   	<div class="cell header-name">
	          	<p><strong><spring:message code="pd.label.Deductible"/> &amp; <spring:message code="pd.label.icon.outOfPocket"/></strong></p>
	     	</div>
		</div>  
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link1indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
            	 (<spring:message code="pd.label.commontext.individual"/>)
          </div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG != null)}">
           		<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link2indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
           (<spring:message code="pd.label.commontext.individual"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG != null)}">
              	<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link1famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link1"/></a>
            	 (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG != null)}">
			  	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_INTG_MED_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link2famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link2"/></a>
          (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG != null)}">
              	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_INTG_MED_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><spring:message code="pd.label.deductible.link3"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
              	<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_MEDICAL}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link4indtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
           (<spring:message code="pd.label.commontext.individual"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_DRUG != null)}">
              	<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
              	<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_MEDICAL}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.individual"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_DRUG != null)}">
              	<div class="details"><script>displayIndDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link3famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link3"/></a>
          (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_MEDICAL != null)}">
              	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_MEDICAL}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><a data-original-title="<spring:message code="pd.label.deductible.link4famtooltip" htmlEscape="true"/>" data-html="true" rel="tooltip" data-placement="bottom" href="#"><spring:message code="pd.label.deductible.link4"/></a>
           (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.DEDUCTIBLE_DRUG != null)}">
	        	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.DEDUCTIBLE_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><spring:message code="pd.label.deductible.link5"/> (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_MEDICAL != null)}">
              	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_MEDICAL}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <div class="row-detail clearfix">
          <div class="cell"><spring:message code="pd.label.deductible.link6"/> (<spring:message code="pd.label.commontext.family"/>)</div>
          <c:choose>
            <c:when test="${(individualPlan.planCosts != null && individualPlan.planCosts.MAX_OOP_DRUG != null)}">
              	<div class="details"><script>displayFlyDeductibleAndMaxOOP("${individualPlan.planCosts.MAX_OOP_DRUG}")</script></div>
            </c:when>
            <c:otherwise><div class="details"><spring:message code="pd.label.commontext.notAvailable"/></small></div></c:otherwise>
          </c:choose>
        </div>
        <c:if test="${fn:length(individualPlan.optionalDeductible) gt 0}">
          <div class="row-detail clearfix" id="planDeductibleOtherDetail">
            <div class="cell"><a data-placement="right" rel="tooltip" href="#"><spring:message code="pd.label.deductible.linkOther"/></a></div>
            <div class="details">
            <c:forEach var="optionalDeductible" items="${individualPlan.optionalDeductible}">
            	<script>displayOptionalDeductible("${optionalDeductible}")</script>
            </c:forEach>
            </div>
          </div>
		</c:if>
      
    </div>
	</c:otherwise>
	</c:choose>
	
	
	<!-- Doctor Visit -->
	<div class="table">
		<div class="header ps-detail__sub-header">
			<div class="u-table-cell cell">
				<spring:message code="pd.label.icon.doctorVisit"/>
			</div>
			<div class="u-table-cell cell">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
					<spring:message code="pd.label.title.inNetwork"/>
				</a>
			</div>
			<div class="u-table-cell cell">
				<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
					<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
				
			</div>
			<div class="u-table-cell cell u-table-cell--10">
				<spring:message code="pd.label.title.additionalInfo"/>
			</div>
		</div>
     
       	<div class="ps-detail__service">
       		<div class="u-table-cell cell"><spring:message code="pd.label.doctorVisit.link1"/></div>
       		<div class="details u-table-cell cell">
				${individualPlan.planTier1.PRIMARY_VISIT.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.PRIMARY_VISIT.displayVal}
			</div>
			<div class="details u-table-cell cell u-table-cell--10"">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.PRIMARY_VISIT.displayVal}", "${individualPlan.netwkException.PRIMARY_VISIT.displayVal}")</script>
			</div>
       	</div>
       	
       	<div class="ps-detail__service">
       		<div class="u-table-cell cell"><spring:message code="pd.label.doctorVisit.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.SPECIAL_VISIT.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.SPECIAL_VISIT.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SPECIAL_VISIT.displayVal}", "${individualPlan.netwkException.SPECIAL_VISIT.displayVal}")</script>
			</div>
       	</div>
       	
       	<div class="ps-detail__service">
       		<div class="u-table-cell cell"><spring:message code="pd.label.doctorVisit.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.OTHER_PRACTITIONER_VISIT.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.OTHER_PRACTITIONER_VISIT.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.OTHER_PRACTITIONER_VISIT.displayVal}", "${individualPlan.netwkException.OTHER_PRACTITIONER_VISIT.displayVal}")</script>
			</div>
       	</div>
       	
       	<div class="ps-detail__service">
       		<div class="u-table-cell cell"><spring:message code="pd.label.doctorVisit.link4"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.PREVENT_SCREEN_IMMU.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.PREVENT_SCREEN_IMMU.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.PREVENT_SCREEN_IMMU.displayVal}", "${individualPlan.netwkException.PREVENT_SCREEN_IMMU.displayVal}")</script>
			</div>
       	</div>
      
    </div>
      
      
	<!-- Tests -->
	<div class="table">
		
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
				<spring:message code="pd.label.icon.tests"/>
	   		</div>
	
			<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  	
	  	<c:if test="${stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.tests.link1"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.LABORATORY_SERVICES.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.LABORATORY_SERVICES.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
	      	    	<script>displayAdditionalInfo("${individualPlan.benefitExplanation.LABORATORY_SERVICES.displayVal}", "${individualPlan.netwkException.LABORATORY_SERVICES.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.tests.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.IMAGING_XRAY.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.IMAGING_XRAY.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.IMAGING_XRAY.displayVal}", "${individualPlan.netwkException.IMAGING_XRAY.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.tests.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.IMAGING_SCAN.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.IMAGING_SCAN.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.IMAGING_SCAN.displayVal}", "${individualPlan.netwkException.IMAGING_SCAN.displayVal}")</script>
			</div>
		</div>
	</div>	
	
      
      
    <!-- Drugs --> 
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.drugs"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.drugs.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.GENERIC.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.GENERIC.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.GENERIC.displayVal}", "${individualPlan.netwkException.GENERIC.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.drugs.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.PREFERRED_BRAND.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.PREFERRED_BRAND.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
	      	    <script>displayAdditionalInfo("${individualPlan.benefitExplanation.PREFERRED_BRAND.displayVal}", "${individualPlan.netwkException.PREFERRED_BRAND.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.drugs.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.NON_PREFERRED_BRAND.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.NON_PREFERRED_BRAND.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.NON_PREFERRED_BRAND.displayVal}", "${individualPlan.netwkException.NON_PREFERRED_BRAND.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.drugs.link4"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.SPECIALTY_DRUGS.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.SPECIALTY_DRUGS.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SPECIALTY_DRUGS.displayVal}", "${individualPlan.netwkException.SPECIALTY_DRUGS.displayVal}")</script>
			</div>
		</div>		
	</div>
      
      
      
    <!-- Outpatient Services   -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.outpatient"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  	
	  	<c:if test="${stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.outpatient.link1"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.OUTPATIENT_FACILITY_FEE.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.OUTPATIENT_FACILITY_FEE.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.OUTPATIENT_FACILITY_FEE.displayVal}", "${individualPlan.netwkException.OUTPATIENT_FACILITY_FEE.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.outpatient.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.OUTPATIENT_SURGERY_SERVICES.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.OUTPATIENT_SURGERY_SERVICES.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.OUTPATIENT_SURGERY_SERVICES.displayVal}", "${individualPlan.netwkException.OUTPATIENT_SURGERY_SERVICES.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.outpatient.officeVisits"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planTier1.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal != null && individualPlan.planTier1.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal != ''}">
        					${individualPlan.planTier1.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal}
        				</c:when>
        				<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
				<div class="details u-table-cell cell">
					<c:choose>
				        <c:when test="${individualPlan.planOutNet.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal != null && individualPlan.planOutNet.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal != ''}">
				        	${individualPlan.planOutNet.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal}
				        </c:when>
				        <c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal}", "${individualPlan.netwkException.OUTPATIENT_SERVICES_OFFICE_VISIT.displayVal}")</script>
				</div>
			</div>
		</c:if>
	</div>
      
      
     <!-- Emergency & Urgent Care -->
     <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.urgentCare.link3"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.urgentCare.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.EMERGENCY_SERVICES.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.EMERGENCY_SERVICES.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.EMERGENCY_SERVICES.displayVal}", "${individualPlan.netwkException.EMERGENCY_SERVICES.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.urgentCare.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.EMERGENCY_TRANSPORTATION.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.EMERGENCY_TRANSPORTATION.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.EMERGENCY_TRANSPORTATION.displayVal}", "${individualPlan.netwkException.EMERGENCY_TRANSPORTATION.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.urgentCare.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.URGENT_CARE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.URGENT_CARE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.URGENT_CARE.displayVal}", "${individualPlan.netwkException.URGENT_CARE.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.urgentCare.professionalFee"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planTier1.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal != null && individualPlan.planTier1.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planTier1.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planOutNet.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal != null && individualPlan.planOutNet.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planOutNet.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal}", "${individualPlan.netwkException.EMERGENCY_ROOM_PROFESSIONAL_FEE.displayVal}")</script>
				</div>
			</div>
		</c:if>		
	</div>

      
    <!-- Hospital Services -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.hospital"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.hospital.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.INPATIENT_HOSPITAL_SERVICE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.INPATIENT_HOSPITAL_SERVICE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.INPATIENT_HOSPITAL_SERVICE.displayVal}", "${individualPlan.netwkException.INPATIENT_HOSPITAL_SERVICE.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.hospital.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.INPATIENT_PHY_SURGICAL_SERVICE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.INPATIENT_PHY_SURGICAL_SERVICE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.INPATIENT_PHY_SURGICAL_SERVICE.displayVal}", "${individualPlan.netwkException.INPATIENT_PHY_SURGICAL_SERVICE.displayVal}")</script>
			</div>
		</div>		
	</div>

    
    <!-- Mental / Behavioral Health -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.health"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.MENTAL_HEALTH_OUT.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.MENTAL_HEALTH_OUT.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.MENTAL_HEALTH_OUT.displayVal}", "${individualPlan.netwkException.MENTAL_HEALTH_OUT.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.MENTAL_HEALTH_IN.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.MENTAL_HEALTH_IN.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.MENTAL_HEALTH_IN.displayVal}", "${individualPlan.netwkException.MENTAL_HEALTH_IN.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.inpatientProfFee"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planTier1.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal != null && individualPlan.planTier1.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planTier1.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal}", "${individualPlan.netwkException.MENTAL_BEHAVIORAL_HEALTH_INPATIENT_PROFESSIONAL_FEE.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.SUBSTANCE_OUTPATIENT_USE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.SUBSTANCE_OUTPATIENT_USE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SUBSTANCE_OUTPATIENT_USE.displayVal}", "${individualPlan.netwkException.SUBSTANCE_OUTPATIENT_USE.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.link4"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.SUBSTANCE_INPATIENT_USE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.SUBSTANCE_INPATIENT_USE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SUBSTANCE_INPATIENT_USE.displayVal}", "${individualPlan.netwkException.SUBSTANCE_INPATIENT_USE.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.mentalHealth.subsDisorderInpProfFee"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planTier1.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal != null && individualPlan.planTier1.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planTier1.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planOutNet.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal != null && individualPlan.planOutNet.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planOutNet.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal}", "${individualPlan.netwkException.SUBSTANCE_USE_DISORDER_INPATIENT_PROFESSIONAL_FEE.displayVal}")</script>
				</div>
			</div>	
		</c:if>	
	</div>
    
    
    <!-- Pregnancy -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.pregnancy"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  	
	  	<c:if test="${stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.pregnancy.link1"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.PRENATAL_POSTNATAL.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.PRENATAL_POSTNATAL.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.PRENATAL_POSTNATAL.displayVal}", "${individualPlan.netwkException.PRENATAL_POSTNATAL.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.pregnancy.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.DELIVERY_IMP_MATERNITY_SERVICES.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.DELIVERY_IMP_MATERNITY_SERVICES.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.DELIVERY_IMP_MATERNITY_SERVICES.displayVal}", "${individualPlan.netwkException.DELIVERY_IMP_MATERNITY_SERVICES.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.pregnancy.inpatientProfFee"/></div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planTier1.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal != null && individualPlan.planTier1.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planTier1.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
						</c:choose>
				</div>
				<div class="details u-table-cell cell">
					<c:choose>
						<c:when test="${individualPlan.planOutNet.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal != null && individualPlan.planOutNet.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal != ''}">
							${individualPlan.planOutNet.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal}
						</c:when>
						<c:otherwise><spring:message code="pd.label.commontext.notAvailable"/></c:otherwise>
					</c:choose>
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal}", "${individualPlan.netwkException.DELIVERY_AND_ALL_INPATIENT_SERVICES_PROFESSIONAL_FEE.displayVal}")</script>
				</div>
			</div>
		</c:if>	
	</div>

    
    
    <!-- Other Services -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.otherSpecialNeeds"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.HOME_HEALTH_SERVICES.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.HOME_HEALTH_SERVICES.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.HOME_HEALTH_SERVICES.displayVal}", "${individualPlan.netwkException.HOME_HEALTH_SERVICES.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.OUTPATIENT_REHAB_SERVICES.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.OUTPATIENT_REHAB_SERVICES.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.OUTPATIENT_REHAB_SERVICES.displayVal}", "${individualPlan.netwkException.OUTPATIENT_REHAB_SERVICES.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.HABILITATION.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.HABILITATION.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.HABILITATION.displayVal}", "${individualPlan.netwkException.HABILITATION.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link4"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.SKILLED_NURSING_FACILITY.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.SKILLED_NURSING_FACILITY.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.SKILLED_NURSING_FACILITY.displayVal}", "${individualPlan.netwkException.SKILLED_NURSING_FACILITY.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link5"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.DURABLE_MEDICAL_EQUIP.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.DURABLE_MEDICAL_EQUIP.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.DURABLE_MEDICAL_EQUIP.displayVal}", "${individualPlan.netwkException.DURABLE_MEDICAL_EQUIP.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link6"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.HOSPICE_SERVICE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.HOSPICE_SERVICE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.HOSPICE_SERVICE.displayVal}", "${individualPlan.netwkException.HOSPICE_SERVICE.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link7"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.CHIROPRACTIC.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.CHIROPRACTIC.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.CHIROPRACTIC.displayVal}", "${individualPlan.netwkException.CHIROPRACTIC.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link8"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.ACUPUNCTURE.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.ACUPUNCTURE.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.ACUPUNCTURE.displayVal}", "${individualPlan.netwkException.ACUPUNCTURE.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link9"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.REHABILITATIVE_SPEECH_THERAPY.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.REHABILITATIVE_SPEECH_THERAPY.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.REHABILITATIVE_SPEECH_THERAPY.displayVal}", "${individualPlan.netwkException.REHABILITATIVE_SPEECH_THERAPY.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link10"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.REHABILITATIVE_PHYSICAL_THERAPY.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.REHABILITATIVE_PHYSICAL_THERAPY.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.REHABILITATIVE_PHYSICAL_THERAPY.displayVal}", "${individualPlan.netwkException.REHABILITATIVE_PHYSICAL_THERAPY.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link11"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.WELL_BABY.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.WELL_BABY.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.WELL_BABY.displayVal}", "${individualPlan.netwkException.WELL_BABY.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link12"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.ALLERGY_TESTING.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.ALLERGY_TESTING.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.ALLERGY_TESTING.displayVal}", "${individualPlan.netwkException.ALLERGY_TESTING.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link13"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.DIABETES_EDUCATION.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.DIABETES_EDUCATION.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.DIABETES_EDUCATION.displayVal}", "${individualPlan.netwkException.DIABETES_EDUCATION.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.otherSpecialNeeds.link14"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.NUTRITIONAL_COUNSELING.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.NUTRITIONAL_COUNSELING.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.NUTRITIONAL_COUNSELING.displayVal}", "${individualPlan.netwkException.NUTRITIONAL_COUNSELING.displayVal}")</script>
			</div>
		</div>		
	</div>
    
    <!-- Vision / Hearing Aids -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.children.vision"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.children.vision.link1"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.RTN_EYE_EXAM_CHILDREN.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.RTN_EYE_EXAM_CHILDREN.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.RTN_EYE_EXAM_CHILDREN.displayVal}", "${individualPlan.netwkException.RTN_EYE_EXAM_CHILDREN.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.children.vision.link2"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.GLASSES_CHILDREN.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.GLASSES_CHILDREN.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.GLASSES_CHILDREN.displayVal}", "${individualPlan.netwkException.GLASSES_CHILDREN.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode == 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.hearing.link1"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.HEARING_AIDS.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.HEARING_AIDS.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.HEARING_AIDS.displayVal}", "${individualPlan.netwkException.HEARING_AIDS.displayVal}")</script>
				</div>
			</div>
		</c:if>	
	</div>
	
    
    <!-- Dental -->
    <div class="table">
	
		<div class="header ps-detail__sub-header">
	   		<div class="u-table-cell cell">
	   			<spring:message code="pd.label.icon.dental"/>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.innetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.inNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell">
	   			<a data-placement="right" rel="tooltip" href="#" data-original-title="<spring:message code="pd.label.tooltip.outofnetwork" htmlEscape="false"/>">
	   				<spring:message code="pd.label.title.outOfNetwork"/>
				</a>
	   		</div>
	
	   		<div class="u-table-cell cell u-table-cell--10">
	   			<spring:message code="pd.label.title.additionalInfo"/>
	   		</div>
	
		</div>
	  	
	  	<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link1"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.ACCIDENTAL_DENTAL.displayVal}		
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.ACCIDENTAL_DENTAL.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.ACCIDENTAL_DENTAL.displayVal}", "${individualPlan.netwkException.ACCIDENTAL_DENTAL.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<c:if test="${stateCode != 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link2"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.BASIC_DENTAL_CARE_ADULT.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.BASIC_DENTAL_CARE_ADULT.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.BASIC_DENTAL_CARE_ADULT.displayVal}", "${individualPlan.netwkException.BASIC_DENTAL_CARE_ADULT.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link3"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.BASIC_DENTAL_CARE_CHILD.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.BASIC_DENTAL_CARE_CHILD.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.BASIC_DENTAL_CARE_CHILD.displayVal}", "${individualPlan.netwkException.BASIC_DENTAL_CARE_CHILD.displayVal}")</script>
			</div>
		</div>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link4"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.DENTAL_CHECKUP_CHILDREN.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.DENTAL_CHECKUP_CHILDREN.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.DENTAL_CHECKUP_CHILDREN.displayVal}", "${individualPlan.netwkException.DENTAL_CHECKUP_CHILDREN.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode != 'CA'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link5"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.MAJOR_DENTAL_CARE_ADULT.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.MAJOR_DENTAL_CARE_ADULT.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.MAJOR_DENTAL_CARE_ADULT.displayVal}", "${individualPlan.netwkException.MAJOR_DENTAL_CARE_ADULT.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<div class="ps-detail__service">
			<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link6"/></div>
			<div class="details u-table-cell cell">
				${individualPlan.planTier1.MAJOR_DENTAL_CARE_CHILD.displayVal}
			</div>
			<div class="details u-table-cell cell">
				${individualPlan.planOutNet.MAJOR_DENTAL_CARE_CHILD.displayVal}
			</div>
	
			<div class="details u-table-cell cell u-table-cell--10">
				<script>displayAdditionalInfo("${individualPlan.benefitExplanation.MAJOR_DENTAL_CARE_CHILD.displayVal}", "${individualPlan.netwkException.MAJOR_DENTAL_CARE_CHILD.displayVal}")</script>
			</div>
		</div>
		
		<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link7"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.ORTHODONTIA_ADULT.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.ORTHODONTIA_ADULT.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.ORTHODONTIA_ADULT.displayVal}", "${individualPlan.netwkException.ORTHODONTIA_ADULT.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<c:if test="${stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link8"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.ORTHODONTIA_CHILD.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.ORTHODONTIA_CHILD.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.ORTHODONTIA_CHILD.displayVal}", "${individualPlan.netwkException.ORTHODONTIA_CHILD.displayVal}")</script>
				</div>
			</div>
		</c:if>
		
		<c:if test="${stateCode != 'CA' && stateCode != 'MN'}">
			<div class="ps-detail__service">
				<div class="u-table-cell cell"><spring:message code="pd.label.children.dental.link9"/></div>
				<div class="details u-table-cell cell">
					${individualPlan.planTier1.RTN_DENTAL_ADULT.displayVal}
				</div>
				<div class="details u-table-cell cell">
					${individualPlan.planOutNet.RTN_DENTAL_ADULT.displayVal}
				</div>
		
				<div class="details u-table-cell cell u-table-cell--10">
					<script>displayAdditionalInfo("${individualPlan.benefitExplanation.RTN_DENTAL_ADULT.displayVal}", "${individualPlan.netwkException.RTN_DENTAL_ADULT.displayVal}")</script>
				</div>
			</div>
		</c:if>		
	</div>
  </div>
  <input type="hidden" name="orderItemId" id="orderItemId">
  <input type="hidden" id="csrfToken" value='<df:csrfToken plainToken="true"/>'>
  <!--gutter10--> 
  <div class="carrier-disclaimers hide">
	<h3>Carrier Notices</h3>

	<div id="carrierInfo"></div>
  </div> <!--.carrier-disclaimers  -->
</div> <!--.row-fluid  -->

<script type="text/javascript" src="<c:url value="/resources/js/tmpl.js" />"></script> 
<script type="text/javascript">
$(document).ready(function(){
	
	var hasno = $('td').find('i');	
	$(hasno).parent('td').addClass('txt-center');
	
	  $('a[rel=tooltip]').tooltip();

	$("input").each(function(i){ 
		$(this).attr('checked', false);
	});//input end

	$('a.btn').popover({
		placement:'left',
		
	});//popover 
	 
    $('span.icon-ok').parents('li').css('list-style', 'none');
	if('${showQualityRating}'=='YES')
	{
	    issuerRatingsTooltip();
	}
    $.ajax({
        type : "POST",
        url : "../../getDisclaimersInfo",
        data:{"issuersString": "${individualPlan.issuerId}", "exchangeType": "${individualPlan.exchangeType}","csrftoken": $('#csrfToken').val()},
        dataType:'json',
        success : function(response) {
               $('#carrierInfo').empty();
               $.each(response,function(index,value){
                      if(value != null && value != ""){
                         $('#carrierInfo').append('<div><h4>${individualPlan.issuer}</h4><p>'+value+'</p></div>');
                      }
               });
               if($('#carrierInfo').html() != ""){
               	$('.carrier-disclaimers').show();
               }
        },
 	});
});

</script>
