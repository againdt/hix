package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.history.RevisionRepository;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.enrollment.Enrollment;

public interface IPlanHealthRepository extends JpaRepository<PlanHealth, Integer>, RevisionRepository<PlanHealth, Integer, Integer> {

	@Query("SELECT planHealth.id,planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    Object[] getPlanHealthIdByParentId(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);
	
	@Query("SELECT planHealth.acturialValueCertificate, planHealth.benefitFile, planHealth.costSharing FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId)")
    List<Object[]> getPlanHealthDetails(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) and planHealth.costSharing = :costSharing ")
    List<Integer> getCostSharingPlanIds(@Param("parentPlanId") String parentPlanId,@Param("costSharing") String costSharing);

    @Query("SELECT planHealth.plan.id FROM PlanHealth planHealth WHERE LOWER(planHealth.parentPlanId) = LOWER(:parentPlanId) ")
    List<Integer> getChildPlanIds(@Param("parentPlanId") String parentPlanId);
    
    @Query("SELECT planHealth.id FROM PlanHealth planHealth WHERE planHealth.plan.id = :planId ")
    Integer getPlanHealthId(@Param("planId") Integer planId);
    
    @Query("SELECT ph FROM PlanHealth ph WHERE ph.plan.issuer.id=:issuerID and ph.plan.id = :planID and ph.costSharing=:csType")
    PlanHealth getRequiredPlanHealthObject(@Param("issuerID") int issuerID,@Param("planID") int planID,@Param("csType") String csType);
    
    @Modifying
    @Query("Update PlanHealth ph set ph.csrAdvPayAmt=:csrAdvPayAmt WHERE ph.plan.issuer.id=:issuerID and ph.plan.id = :planID and ph.costSharing=:csType")
    void getUpdateInfo(@Param("csrAdvPayAmt") Float csrAdvPayAmt,@Param("issuerID") int issuerID,@Param("planID") int planID,@Param("csType") String csType);
    
    @Query("SELECT ph FROM PlanHealth ph WHERE ph.plan.issuer.id=:issuerID and ph.plan.id = :planID and ph.planLevel=:planLevelString")
    List<PlanHealth> getPlanHealthInfo(@Param("issuerID") int issuerID,@Param("planID") int planID,@Param("planLevelString") String planLevelString);

    @Query("SELECT ph FROM PlanHealth ph WHERE ph.plan.id = :planID and ph.planLevel=:planLevelString")
    List<PlanHealth> getPlanHealthInfoList(@Param("planID") int planID,@Param("planLevelString") String planLevelString);

    @Query("SELECT e.planId FROM Enrollment e WHERE e.planId = :planID")
    List<String> getEnrollmentCount(@Param("planID") int planID);

}


