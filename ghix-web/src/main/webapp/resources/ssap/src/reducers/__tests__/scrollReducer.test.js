import scrollReducer, { initialState } from '../scrollReducer';
import { scrollActionTypes } from '../../constants/actionTypes';

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const clearElementAction = {
    type: scrollActionTypes.CLEAR_ELEMENT_FOR_SCROLLING
};

const clearStateAction = {
    type: scrollActionTypes.CLEAR_STATE
};

const defineElementForScrollingAction = {
    type: scrollActionTypes.DEFINE_ELEMENT_FOR_SCROLLING,
    element: 'element_to_scroll_to'
};

const wrongTypes = [
    5,
    [],
    {}
];

describe('Test for scrollReducer', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(
            scrollReducer(
                initialState,
                anotherAction
            )
        ).toEqual(initialState);
    });

    it('Should scroll to element', () => {
        expect(
            scrollReducer(
                initialState,
                defineElementForScrollingAction
            )
        ).toBe(defineElementForScrollingAction.element);
    });

    wrongTypes.forEach(wt => {
        it('Should throw an error', () => {
            expect(() => {
                scrollReducer(
                    initialState,
                    { ...defineElementForScrollingAction, element: wt }
                );
            }).toThrow('Not a string has been passed to scrollReducer');
        });
    });

    it('Should clear element', () => {
        expect(
            scrollReducer(
                'Some element',
                clearElementAction
            )
        ).toBe(initialState);
    });

    it('Should clear state', () => {
        expect(
            scrollReducer(
                'Some element2',
                clearStateAction
            )
        ).toBe(initialState);
    });

});