import React from 'react';

const Page3_15 = () => {

    return (
        <div id="rsReview" className="fade-in">
            <div className="subsection">
                <div className="usa-width-one-whole">
                    <div className="usa-alert-body" >
                        <p className="usa-alert-text">
                            <strong>{'Now it\'s time to review and sign your health insurance application.'}</strong>
                            <br/>
                                Please review all the detailed application information about
                                every household member who is applying for health insurance.
                                In a moment, you will finalize your application and provide your eSignature.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

Page3_15.defaultProps = {
    name: 'Page3_15'
};

export default Page3_15;
