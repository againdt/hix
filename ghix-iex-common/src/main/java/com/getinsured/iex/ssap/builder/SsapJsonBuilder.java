package com.getinsured.iex.ssap.builder;

import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.google.gson.JsonSyntaxException;

/**
 * @author chopra_s
 * 
 */
public interface SsapJsonBuilder {
	void buildForJson(SingleStreamlinedApplication singleStreamlinedApplication);

	String transformToJson(SingleStreamlinedApplication singleStreamlinedApplication);

	String modifyJsonForClonedApp(SsapApplication ssapApplication);
	
	SingleStreamlinedApplication transformFromJson(String json) throws JsonSyntaxException;
}
