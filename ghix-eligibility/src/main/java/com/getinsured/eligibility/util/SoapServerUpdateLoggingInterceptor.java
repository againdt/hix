package com.getinsured.eligibility.util;


import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.getinsured.timeshift.util.TSDate;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.ws.context.MessageContext;

import com.getinsured.eligibility.at.dto.AccountTransferResponseDTO;
import com.getinsured.eligibility.at.server.endpoint.MessageContextHolder;
import com.getinsured.eligibility.repository.outboundat.OutboundATApplicantRepository;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferResponsePayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FFEVerificationCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ReferralActivityStatusCodeType;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;
import com.getinsured.iex.ssap.model.OutboundATApplicant;
import com.getinsured.iex.ssap.model.OutboundATApplication;

/**
 *
 * SoapServerUpdateLoggingInterceptor to intercept
 * request/response for incoming SOAP Web Services for Eligibility AT.
 *
 *
 * @author EkramAli Kazi
 *
 */
public class SoapServerUpdateLoggingInterceptor implements MethodInterceptor{

	private static final String AT_VERSION = "2.3";

	private static final String METHOD_NAME = "prepareATResponse";

	private static final String INPUT_XML = "inputXml";

	private static final String ERROR_MSG = "Error recording request/response in SoapServerUpdateLoggingInterceptor --> ";

	private static final String EXECUTION_TIME_CORRELATION_ID = "execution time - correlationId - ";

	private static final String FAILURE = "FAILURE";

	private static final String SUCCESS = "SUCCESS";

	private static final Logger LOGGER = LoggerFactory.getLogger(SoapServerUpdateLoggingInterceptor.class);
	
	private static final String ERROR_LOADING_AND_CREATING_JAXB_XSD_VALIDATOR_FOR_ACCOUNT_TRANSFER = "Error loading and creating JAXB XSD validator for account transfer";
	
	private static final JAXBContext jc = initContext();
	
	private static final String ENDPOINT_FUNCTION_NAME = "ERP-TRANSFERACCOUNT";
	
	private static JAXBContext initContext() {
    	JAXBContext jc = null;
    	try {
    		jc = JAXBContext.newInstance(AccountTransferResponsePayloadType.class);
		} catch (Exception e) {
			LOGGER.error(ERROR_LOADING_AND_CREATING_JAXB_XSD_VALIDATOR_FOR_ACCOUNT_TRANSFER, e);
			throw new GIRuntimeException(ERROR_LOADING_AND_CREATING_JAXB_XSD_VALIDATOR_FOR_ACCOUNT_TRANSFER, e);
		}
    	return jc;
    }

	
	/*private static JAXBContext jc = null;
	private static Marshaller marshaller = null;
	
	static {
		try {
			jc = JAXBContext.newInstance("com.getinsured.iex.erp.gov.cms.dsh.at.extension._1");
			marshaller = jc.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		} catch (JAXBException e) {
			e.printStackTrace();
		}
	}
	*/
	
	
	
	

	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {

		Object result = null;

		String requestPayload = null;
		String responsePayload = null;
		String exceptionTrace = null;
		String status = SUCCESS;
		
		long startTime = System.nanoTime();	
		
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		MessageContext messageContext = MessageContextHolder.getMessageContext();

		Object[] arg = methodInvocation.getArguments();

		try {
			requestPayload = extractRequestXml(messageContext);
			result = methodInvocation.proceed();
			
		} catch (Exception e) {
			status = FAILURE;
			exceptionTrace = ExceptionUtils.getFullStackTrace(e);
			throw e;
		} finally {
			try{
				// update outbound for ssa
				AccountTransferRequestPayloadType accountTransferRequest = (AccountTransferRequestPayloadType) arg[0];
				String webServiceVersion = accountTransferRequest != null ?  accountTransferRequest.getAtVersionText() : AT_VERSION;
				String correlationId = extractCorrelationId(accountTransferRequest);
				
				String requestUrl = readRequestUrl(request);
				
				String responseCode = EligibilityConstants.ERROR;
				if (result != null){
					AccountTransferResponsePayloadType response = ((AccountTransferResponseDTO) result).getAccountTransferResponsePayloadType();
					responseCode = extractResponseCode(response);
					
					responseCode = StringUtils.upperCase(responseCode);
					if (!StringUtils.equalsIgnoreCase(EligibilityConstants.HS000000, responseCode) && !StringUtils.equalsIgnoreCase(EligibilityConstants.SUCCESS_WITH_WARNING_CODE, responseCode)) {
						status = FAILURE;
					}
					
					responsePayload = marshal(response);
				}
				// prepare giwsPayload object
				GIWSPayload giwsPayload = new GIWSPayload();
				giwsPayload.setCorrelationId(correlationId);
				giwsPayload.setCreatedTimestamp(new TSDate());
				giwsPayload.setCreatedUserId(null);
				giwsPayload.setEndpointFunction(ENDPOINT_FUNCTION_NAME);
				giwsPayload.setEndpointOperationName(METHOD_NAME);
				giwsPayload.setEndpointUrl(requestUrl);
				giwsPayload.setExceptionMessage(exceptionTrace);
				giwsPayload.setRequestPayload(requestPayload);
				giwsPayload.setResponseCode(responseCode);
				giwsPayload.setResponsePayload(responsePayload);
				giwsPayload.setStatus(status);
				giwsPayload.setClientNodeIp(request.getRemoteAddr());
				giwsPayload.setVersion(webServiceVersion);
				
				if (result != null){
					((AccountTransferResponseDTO) result).setGiwsPayload(giwsPayload);
					((AccountTransferResponseDTO) result).setGiWsPayloadId(giwsPayload.getId());
				}
				
				if (LOGGER.isInfoEnabled()){
					LOGGER.info(EXECUTION_TIME_CORRELATION_ID + correlationId  + " -- "+ (System.nanoTime() - startTime) + " ns");
				}

			} catch(Exception e){
				/** log and eat exception to give chance to get response back to the caller. Something gone wrong with SoapServerLoggingInterceptor recording*/
				String errorMessage = ERROR_MSG + ExceptionUtils.getFullStackTrace(e);
				LOGGER.error(errorMessage);
			}
		}
		return result;
	}


	private String readRequestUrl(HttpServletRequest request) {
		return request != null ? 
					request.getRequestURL() != null ? request.getRequestURL().toString() : StringUtils.EMPTY 
							: StringUtils.EMPTY;
	}


	private String extractResponseCode(AccountTransferResponsePayloadType response) {
		return response != null ? response.getResponseMetadata() != null ? 
				response.getResponseMetadata().getResponseCode() != null ? 
						response.getResponseMetadata().getResponseCode().getValue() : StringUtils.EMPTY : StringUtils.EMPTY : StringUtils.EMPTY;
	}

	private String extractCorrelationId(AccountTransferRequestPayloadType accountTransferRequest) {

		return accountTransferRequest!= null ? accountTransferRequest.getTransferHeader() != null ? 
				accountTransferRequest.getTransferHeader().getTransferActivity() != null ? 
						accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification() != null ? 
								accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID() != null ?
								accountTransferRequest.getTransferHeader().getTransferActivity().getActivityIdentification().getIdentificationID().getValue() 
				 : StringUtils.EMPTY  : StringUtils.EMPTY : StringUtils.EMPTY : StringUtils.EMPTY : StringUtils.EMPTY;

	}

	private String extractRequestXml(MessageContext messageContext) {
		return (String) messageContext.getProperty(INPUT_XML);
	}
	
	
	private static String marshal(AccountTransferResponsePayloadType response) throws JAXBException, PropertyException {
		Marshaller jaxbMarshaller = jc.createMarshaller();
		StringWriter sw = new StringWriter();
		jaxbMarshaller.marshal(response, sw);
		return sw.toString();
	}
}
