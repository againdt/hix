<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
<div class="gutter10">
    <div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
       		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
            <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
            <c:if test="${exchangeState != 'ID'}">
            	<li><spring:message  code="pgheader.update.shopMarketProfile"/></li>
            </c:if>
        </ul><!--page-breadcrumb ends-->
        <h1><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
    
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/representative/manage/${encIssuerId}"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
                    <c:if test="${exchangeState != 'ID' && exchangeState != 'MN'}">
                    	<li><a href="<c:url value="/admin/issuer/financial/info/list/${encIssuerId}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
                    </c:if>	
                    <li><a href="<c:url value="/admin/issuer/company/profile/${encIssuerId}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/market/individual/profile/${encIssuerId}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
                    <li class="active"><a href="#"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${encIssuerId}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/certification/status/${encIssuerId}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
    				<%-- <li><a href="<c:url value="/admin/issuer/partner/list/${issuerObj.id}"/>"><spring:message  code="pgheader.partners"/></a></li> --%>
                    <li><a href="<c:url value="/admin/issuer/history/${encIssuerId}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
                    <li><a href="<c:url value="/admin/issuer/crossWalkStatus/${encIssuerId}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
                    <c:if test="${exchangeState == 'ID'}">
                    <li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
                    </c:if>
                     <c:if test="${activeRoleName == 'OPERATIONS'}">
						<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
					</c:if>
                    
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
            	<div class="header">
                    <h4 class="pull-left"><spring:message  code="pgheader.update.shopMarketProfile"/></h4>
                    <a class="btn btn-primary btn-small pull-right margin5-lr" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                    <a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/market/shop/profile/${encIssuerId}"/>"><spring:message  code="label.cancel"/></a> 
                </div>
				<div class="gutter10">
		
				<form class="form-horizontal" id="frmIssuerProfMarktShopInfo" name="frmIssuerProfMarktShopInfo" action="<c:url value="/admin/issuer/market/profile/save" />" method="POST">
                    <df:csrfToken/>
                    <input type="hidden" id="market" name="market" value="shop"/>
                    <input type="hidden" id="id" name="id" value="<encryptor:enc value="${issuerObj.id}"></encryptor:enc>"/>
<%--                     <input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/> --%>
                    <div class="control-group">
						<label class="control-label" for="customerServicePhone"><spring:message  code="label.customerServicePhone"/></label>
						<div class="controls">
						  	<input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="input-mini"/> 
							<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-mini" /> 
                            <input type="hidden" name="customerServicePhone" id="customerServicePhone">
                            <label class="help-inline" for="customerServiceExt"><spring:message  code="label.phone.ext"/></label>
                            <input type="text" name="customerServiceExt" id="customerServiceExt" class="input-mini" value="${issuerObj.shopCustServicePhoneExt}" maxlength="11"/>
	                        <div id="phone3_error" class=""></div>
                            <div id="customerServiceExt_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="custServiceTollFreeNumber"><spring:message  code="label.customerServiceTollFreeNumber"/></label>
						<div class="controls">
							<input type="text" name="phone4" id="phone4" value="${phone4}" maxlength="3" class="input-mini"/> 
							<input type="text" name="phone5" id="phone5" value="${phone5}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone6" id="phone6" value="${phone6}" maxlength="4" class="input-mini" /> 
                            <input type="hidden" name="custServiceTollFreeNumber" id="custServiceTollFreeNumber">
                            <div id="phone6_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="custServiceTTY"><spring:message  code="label.customerServiceTTY"/></label>
						<div class="controls">
							<input type="text" name="phone7" id="phone7" value="${phone7}" maxlength="3" class="input-mini"/> 
							<input type="text" name="phone8" id="phone8" value="${phone8}" maxlength="3" class="input-mini" /> 
							<input type="text" name="phone9" id="phone9" value="${phone9}" maxlength="4" class="input-mini" /> 
                            <input type="hidden" name="custServiceTTY" id="custServiceTTY">
                            <div id="phone9_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                    
                    <div class="control-group">
						<label class="control-label" for="facingWebSite"><spring:message  code="label.consumerFacingWebSiteURL"/></label>
						<div class="controls">
							<input type="text" name="facingWebSite" id="facingWebSite" class="input-large" value="${issuerObj.shopSiteUrl}" maxlength="200"/>
                            <div id="facingWebSite_error" class=""></div>
						</div>
					</div><!-- end of control-group-->
                                        
                    <div class="form-actions paddingLR20">
                        
                    </div>
				</form>
			</div>
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
</div>
<script type="text/javascript">

function submitForm(){
	 $("#frmIssuerProfMarktShopInfo").submit(); 
}
var digitRegex = new RegExp("^[0-9]+$");
jQuery.validator.addMethod("custServicePhoneUS", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone1").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone2").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone3").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone1").val().trim();
		phoneNumber2 = $("#phone2").val().trim();
		phoneNumber3 = $("#phone3").val().trim();
	}
	var phoneNumber = phoneNumber1 + phoneNumber2 + phoneNumber3;
	if(phoneNumber != "" && !digitRegex.test(phoneNumber)){
		return false;
	}
	if(phoneNumber != ""){
		if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length == 10){
			$("#customerServicePhone").val(phoneNumber);
			return true;
		}else{
			return false;
		}	
	}else{
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("custServiceTollFreePhone", function(value, element, param) {
	if($.browser.msie==true && $.browser.version=='8.0'){
		phoneNumber1 = $("#phone4").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone5").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone6").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone4").val().trim();
		phoneNumber2 = $("#phone5").val().trim();
		phoneNumber3 = $("#phone6").val().trim();
	}
	var phoneNumber = phoneNumber1 + phoneNumber2 + phoneNumber3;
	if(phoneNumber != "" && !digitRegex.test(phoneNumber)){
		return false;
	}
	if(phoneNumber != ""){
		if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length == 10){
			$("#custServiceTollFreeNumber").val(phoneNumber);
			return true;
		}else{
			return false;
		}	
	}else{
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("custServiceTTYPhone", function(value, element, param) {
	if($.browser.msie == true && $.browser.version == '8.0'){
		phoneNumber1 = $("#phone7").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber2 = $("#phone8").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
		phoneNumber3 = $("#phone9").val().replace(/^\s\s*/, '').replace(/\s\s*$/, '');
	}else{
		phoneNumber1 = $("#phone7").val().trim();
		phoneNumber2 = $("#phone8").val().trim();
		phoneNumber3 = $("#phone9").val().trim();
	}
	var phoneNumber = phoneNumber1 + phoneNumber2 + phoneNumber3;
	if(phoneNumber != "" && !digitRegex.test(phoneNumber)){
		return false;
	}
	if(phoneNumber != ""){
		if(!isNaN(phoneNumber1) && !isNaN(phoneNumber2) && !isNaN(phoneNumber3) && phoneNumber.length == 10){
			$("#custServiceTTY").val(phoneNumber);
			return true;
		}else{
			return false;
		}	
	}else{
		return true;
	}
},"<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>");

jQuery.validator.addMethod("checkDepend", function(value, element, param) {
	var phoneNumber = $("#customerServicePhone").val();
	if(phoneNumber == "" && value != ""){
		return false;
	}
	return true;
});

jQuery.validator.addMethod("checkForValidUrl", function (value, element, param) {		
    var urlRegExp = new RegExp("(^((http|https):[/]{2}){1}(www[.])?([a-zA-Z0-9]|-)+([.][a-zA-Z0-9(-|/|=|@|&|#|$|%|_|+|?| )?]+)+$)|^$");
    if(value == '') {
		return true;
	}else{
		if((value.indexOf('.') == -1)){
			 return false;
		}
		if(urlRegExp.test(value)){
    		return (true);
    	}else{
    		return (false);
    	}   
    }   	    
});

var validator = $("#frmIssuerProfMarktShopInfo").validate({
	rules : {
		phone3 : {custServicePhoneUS : true },
		customerServiceExt : {required :false, number : true,checkDepend:true},
		phone6 : {custServiceTollFreePhone : true},
		phone9 : {custServiceTTYPhone : true},
		facingWebSite : {required : false, checkForValidUrl : true}	
	},
	messages : {
		phone3 : { custServicePhone : "<span><em class='excl'>!</em><spring:message  code='err.customerServicePhone' javaScriptEscape='true'/></span>" },
		customerServiceExt: {  required : "<span><em class='excl'>!</em><spring:message  code='err.customerServiceExt' javaScriptEscape='true'/></span>",
			number : "<span><em class='excl'>!</em><spring:message  code='err.customerServiceExt' javaScriptEscape='true'/></span>",
			checkDepend : "<span><em class='excl'>!</em><spring:message code='err.customerServicePhoneDepends' javaScriptEscape='true'/></span>"},
		phone6 : { custServiceTollFreePhone : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>" },
		phone9 : { custServiceTTYPhone : "<span><em class='excl'>!</em><spring:message  code='err.issuerPhoneInvalid' javaScriptEscape='true'/></span>" },
		facingWebSite : { checkForValidUrl : "<span><em class='excl'>!</em><spring:message  code='err.validfacingWebSite' javaScriptEscape='true'/></span>" }
	},
	errorClass: "error",
	errorPlacement: function(error, element) { 
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error');
	} 
});
</script>