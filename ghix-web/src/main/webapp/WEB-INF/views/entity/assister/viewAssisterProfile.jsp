<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld" %>

<div class="gutter10">
	<div class="row-fluid">
		<h1>${assister.firstName} ${assister.lastName}</h1>
	</div>
	<c:set var="encrytedAssisterIdforURL" ><encryptor:enc value="${assister.id}" isurl="true"/> </c:set>
		<form class="form-vertical" id="frmviewbrokerprofile" name="frmviewbrokerprofile" action="viewprofile" method="POST">
			<div class="row-fluid">
				<div class="span">			
					<table class="table table-border-none span7">
						<tbody>				
							<tr>
								<td>
								<spring:url value="/entity/assisteradmin/photo/${encrytedAssisterIdforURL}"  var="photoUrl"/>
									<img src="${photoUrl}" alt="Counselor thumbnail photo" class="profilephoto thumbnail" width="100px" height="100px" />
								</td>
								<td>
									<h4>${assister.firstName} ${assister.lastName}</h4>
								</td>
							</tr>
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.emailAddress"/></th>
									<td><strong>${assister.emailAddress}</strong></td>
								</tr>
								
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.PhoneNumber"/></th>
									<td>
										<strong> 
										<c:choose>
											<c:when test="${assister.primaryPhoneNumber!=null}">
												<c:set var="primaryPhoneNumber" value="${assister.primaryPhoneNumber}"></c:set>
												<c:set var="formattedPrimaryPhoneNumber" value="(${fn:substring(primaryPhoneNumber,0,3)}) ${fn:substring(primaryPhoneNumber,3,6)}-${fn:substring(primaryPhoneNumber,6,10)} "></c:set>
												${formattedPrimaryPhoneNumber}
											</c:when>
											<c:otherwise>
												${assister.primaryPhoneNumber}
											</c:otherwise>											
										</c:choose>																				
										</strong>
									</td>
								</tr>
								
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.secondaryPhoneNumber"/></th>
									<td>
									<strong> 
										<c:choose>
											<c:when test="${assister.secondaryPhoneNumber!=null}">
												<c:set var="secondaryPhoneNumber" value="${assister.secondaryPhoneNumber}"></c:set>
												<c:set var="formattedSecondaryPhoneNumber" value="(${fn:substring(secondaryPhoneNumber,0,3)}) ${fn:substring(secondaryPhoneNumber,3,6)}-${fn:substring(secondaryPhoneNumber,6,10)} "></c:set>
												${formattedSecondaryPhoneNumber}
											</c:when>
											<c:otherwise>
												${assister.secondaryPhoneNumber}
											</c:otherwise>
										</c:choose>
									</strong>
									</td>
								</tr>
	
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.preferredMethodofCommunication"/></th>
									<td><strong>${assister.communicationPreference}</strong></td>
								</tr>
								
								
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.isCertifiedAssister"/></th>
									<c:if test="${assister.certificationNumber==null || assister.certificationNumber == ''}">
										<td><strong><spring:message code="label.assister.No"/></strong></td>
									</c:if>
									<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
										<td><strong><spring:message code="label.assister.Yes"/></strong></td>
									</c:if>
								</tr>
								
								<c:if test="${assister.certificationNumber!=null && assister.certificationNumber != ''}">
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.assisterCertification"/> #</th>
									<td><strong>${assister.certificationNumber}</strong></td>
								</c:if>
	
								<c:if test="${assister.primaryAssisterSite!=null}">
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.assister.priAssisterSite"/></th>
										<td><strong>${assister.primaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>	
								<c:if test="${assister.secondaryAssisterSite!=null}">
									<tr>
										<th class="span4 txt-right" scope="row"><spring:message code="label.assister.secAssisterSite"/></th>
										<td><strong>${assister.secondaryAssisterSite.siteLocationName}</strong></td>
									</tr>
								</c:if>
															<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.assister.streetAddress"/></th>
								<td><strong>${assister.mailingLocation.address1}</strong></td>
							</tr>
								<tr>
									<th class="span4 txt-right" scope="row"><spring:message code="label.assister.suite"/></th>
									<td><strong>${assister.mailingLocation.address2}</strong></td>
								</tr>
							
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.assister.city"/></th>
								<td><strong>${assister.mailingLocation.city}</strong></td>
							</tr>
							
							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.assister.state"/></th>
								<td><strong>${assister.mailingLocation.state}</strong></td>
							</tr>

							<tr>
								<th class="span4 txt-right" scope="row"><spring:message code="label.assister.zipCode"/></th>
								<td><strong>${assister.mailingLocation.zip}</strong></td>
							</tr>
						</tbody>
					</table>
					<div class="span5 gutter10">
						<iframe width="270" height="270" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" class="thumbnail" src="//maps.google.com/maps?oe=utf-8&amp;client=firefox-a&amp;q=${assister.mailingLocation.address1}+${assister.mailingLocation.city},+${assister.mailingLocation.state}+${assister.mailingLocation.zip}+map&amp;ie=UTF8&amp;hq=&amp;hnear=${assister.mailingLocation.address1}+${assister.mailingLocation.city},+${assister.mailingLocation.state}+${assister.mailingLocation.zip}&amp;gl=us&amp;t=m&amp;ll=${assister.mailingLocation.lat},${assister.mailingLocation.lon}&amp;z=13&amp;iwloc=A&amp;output=embed"></iframe>
					</div>				
				</div>
			</div>
		</form>
</div>
	
