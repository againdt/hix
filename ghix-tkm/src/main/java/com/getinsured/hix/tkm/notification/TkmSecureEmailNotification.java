package com.getinsured.hix.tkm.notification;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notification.NotificationAgent;
import com.getinsured.hix.platform.util.GhixEndPoints;

@Component
@Scope("prototype")
public class TkmSecureEmailNotification extends NotificationAgent {
	private Map<String, String> singleData;
	
	/**
	 * 
	 * @param notificationData
	 */
	public void setNotificationData(Map<String, Object> notificationData){
		this.notificationData = notificationData;
	}
	
	/**
	 * 
	 * @return
	 */
	public Map<String, Object> getNotificationData(){
		return this.notificationData;
	}
	
	@Override
	public Map<String, String> getSingleData() {
		
		String exchangeName=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME);
		String exchangeUrl = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL);
		Map<String,String> bean = new HashMap<String, String>();

		bean.put("exchangeName", exchangeName );
		bean.put("exchangeUrl", exchangeUrl);
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
		bean.putAll(singleData);
		setTokens(bean);
		
		Map<String, String> data = new HashMap<String, String>();
		
		if (singleData != null)	{
			data.put("To", singleData.get("recipient"));
			data.put("Subject", singleData.get("subject"));
			
			data.putAll(singleData);
		}
		return data;
		
	}
	
	/**
	 * 
	 * @param singleData
	 */
	public void updateSingleData(Map<String, String> singleData)
	{
		this.singleData = singleData;
	}
}
