package com.getinsured.eligibility.at.resp.si.dto;

public class AtAdditionalParamsDTO {

	private Integer giWsPayloadId;
	private String requester;
	private String additionalFieldsMapXmlString;
	private long atSpanInfoId;
	
	
	public Integer getGiWsPayloadId() {
		return giWsPayloadId;
	}
	public void setGiWsPayloadId(Integer giWsPayloadId) {
		this.giWsPayloadId = giWsPayloadId;
	}
	public String getRequester() {
		return requester;
	}
	public void setRequester(String requester) {
		this.requester = requester;
	}
	public String getAdditionalFieldsMapXmlString() {
		return additionalFieldsMapXmlString;
	}
	public void setAdditionalFieldsMapXmlString(String additionalFieldsMapXmlString) {
		this.additionalFieldsMapXmlString = additionalFieldsMapXmlString;
	}
	public long getAtSpanInfoId() {
		return atSpanInfoId;
	}
	public void setAtSpanInfoId(long atSpanInfoId) {
		this.atSpanInfoId = atSpanInfoId;
	}
	
	
}
