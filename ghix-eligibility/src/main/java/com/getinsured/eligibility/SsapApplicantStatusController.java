package com.getinsured.eligibility;

import java.math.BigDecimal;
import java.sql.Timestamp;
import com.getinsured.timeshift.sql.TSTimestamp;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;

import com.getinsured.iex.ssap.model.SsapApplicant;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.iex.client.SsapApplicantRequest;
import com.getinsured.iex.client.SsapApplicationRequest;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;


@Controller
@RequestMapping("/ssapapplicant")
public class SsapApplicantStatusController {

	@Autowired SsapApplicantRepository ssapApplicantRepository;
	@Autowired SsapApplicationRepository ssapApplicationRepository;
	private static final Logger SsapApplicantStatusControllerLOGGER = LoggerFactory.getLogger(SsapAppController.class);

	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	@ResponseBody 
	public boolean updateSsapApplicationStatus(@RequestBody SsapApplicantRequest ssapApplicantRequest) {
		String tobaccoUsage = ssapApplicantRequest.getTobaccoUsage();
		String cessationProgram = ssapApplicantRequest.getIsEnrldCessPrgrm();
		BigDecimal maritalStatus = ssapApplicantRequest.getMaritialStatusId();
		long id = ssapApplicantRequest.getId();
		boolean isUpdateSuccess = true;
		try {
			ssapApplicantRepository.updateApplicantFlags(tobaccoUsage, cessationProgram, maritalStatus, id, new TSTimestamp());	
		}
		catch(Exception ex) {
			isUpdateSuccess = false;
			SsapApplicantStatusControllerLOGGER.error("Error in updating the ssap applicant Id:" + id, ex);
		}
		return isUpdateSuccess;
	}
	
	@RequestMapping(value = "/updateapplicantflags", method = RequestMethod.POST)
	@ResponseBody 
	public boolean updateApplicantFlags(@RequestBody SsapApplicantRequest ssapApplicantRequest) {
		String tobaccoUsage = ssapApplicantRequest.getTobaccoUsage();
		String cessationProgram = ssapApplicantRequest.getIsEnrldCessPrgrm();
		BigDecimal maritalStatus = ssapApplicantRequest.getMaritialStatusId();
		String applicantGuid = ssapApplicantRequest.getApplicantGuid();
		String ecnNumber = ssapApplicantRequest.getEcnNumber();
		String hardshipExempt = ssapApplicantRequest.getHardshipExempt();
		String caseNumber = ssapApplicantRequest.getCaseNumber();
		//FIXMIX Removed  as a part of new Rek in AH
		/*	String applyingForCoverage = ssapApplicantRequest.getApplyingForCoverage();*/
		boolean isUpdateSuccess = true;
		try {
			ssapApplicantRepository.updateApplicantFlagsForApplicant(tobaccoUsage, cessationProgram, maritalStatus, 
					applicantGuid,ecnNumber,hardshipExempt, new TSTimestamp(), caseNumber);	
		}
		catch(Exception ex) {
			isUpdateSuccess = false;
			SsapApplicantStatusControllerLOGGER.error("Error in updating the ssap applicant GUID:" + applicantGuid, ex);
		}
		return isUpdateSuccess;
	}
	
	@RequestMapping(value = "/updateHouseholdExemptflag", method = RequestMethod.POST)
	@ResponseBody 
	public boolean updateHouseholdExemptflag(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
		boolean isUpdateSuccess = true;
			
		String caseNumber =ssapApplicationRequest.getCaseNumber();
		String exemptHouseHold = ssapApplicationRequest.getExemptFlag();
		try {
			ssapApplicationRepository.updateSsapExempt(exemptHouseHold, caseNumber, new TSTimestamp());
		}
		catch(Exception ex) {
			isUpdateSuccess = false;
			SsapApplicantStatusControllerLOGGER.error("Error in updating the ssap applicant GUID:" , ex);
		}
		return isUpdateSuccess;
	}
	
	/*
	@RequestMapping(value = "/deleteSsapApplicant/{applicantGuid}", method = RequestMethod.POST)
	@ResponseBody 
	public int deleteSsapApplicant(@PathVariable("applicantGuid") String applicantGuid) {
		int recordsUpdated = 0;
			
		if(applicantGuid==null || (applicantGuid!=null && StringUtils.isEmpty(applicantGuid))){
			return recordsUpdated;
		}
		try {
			
			List<SsapApplicant> applicantList = ssapApplicantRepository.findBySsapApplicantGuid(applicantGuid);
			
			ssapApplicantRepository.delete(applicantList);
			recordsUpdated = applicantList.size();
		}
		catch(Exception ex) {
			SsapApplicantStatusControllerLOGGER.error("Error in deleting the ssap applicant GUID:"+applicantGuid , ex);
		}
		return recordsUpdated;
	}*/
}
