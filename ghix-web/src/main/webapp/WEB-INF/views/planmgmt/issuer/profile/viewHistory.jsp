<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<div class="gutter10">
<div class="row-fluid">
    	<ul class="page-breadcrumb">
           	<li><a href="<c:url value="/planmgmt/issuer/landing/info"/>">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="<c:url value="/issuer/dashboard"/>"><spring:message  code="pgheader.issuerHome"/></a></li>
            <li><a href="<c:url value="/planmgmt/issuer/landing/info"/>"><spring:message  code="pgheader.account"/></a></li>
            <li><spring:message  code="pgheader.issuerHistory"/></li>
        </ul><!--page-breadcrumb ends-->
        <%-- <h1><issuerLogo:view/> ${issuerObj.name}</h1> --%>
        <h1><img class="resize-img" src="<c:url value="/planmgmt/issuer/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"/>${issuerObj.name}</h1>
           
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message  code="pgheader.profile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <jsp:include page="issuerDetailsLeftNav.jsp">
 	 	 	 			<jsp:param name="pageName" value="issuerHistory"/>
 	 	 	 	</jsp:include>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
				<div class="header margin5-b">
					<h4 class="pull-left">Issuer History</h4>	
				</div>
             	<form class="form-horizontal" id="frmIssuerHistory" name="frmIssuerHistory" action="#" method="POST">
              
					<display:table name="history" pagesize="${pageSize}" list="rates" requestURI="" sort="list" class="table table-condensed table-border-none table-striped"  defaultsort="1" defaultorder="descending" >
			           
			           <display:column property="lastUpdateTimestamp" titleKey="label.date" format="{0,date,MMM dd, yyyy}" sortable="true" headerClass="graydrkbg txt-left" />
			           <display:column property="displayField" titleKey="label.fieldUpdated" sortable="true" headerClass="graydrkbg txt-left"/>
			           <display:column property="displayVal" titleKey="label.newValue" sortable="true" headerClass="graydrkbg txt-left" />
			           <display:column property="lastUpdatedBy" media="none" titleKey="label.userName" sortable="false" headerClass="graydrkbg txt-left"/>
			           <display:column property="commentId" media="none" titleKey="label.comment" sortable="false" headerClass="graydrkbg txt-left"/>
			           <display:setProperty name="paging.banner.placement" value="bottom" />
			           <display:setProperty name="paging.banner.some_items_found" value=''/>
			           <display:setProperty name="paging.banner.all_items_found" value=''/>
			           <display:setProperty name="paging.banner.group_size" value='50'/>
                       <display:setProperty name="paging.banner.onepage" value=''/>
			           <display:setProperty name="paging.banner.page.separator" value='</li><li>'/>
			           <display:setProperty name="paging.banner.page.selected" value='<li class="active"><a href="#">{0}</a></li>'/>
			           <display:setProperty name="paging.banner.first" value='<span class="pagelinks"><div class="pagination center"><ul><li></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
					   <display:setProperty name="paging.banner.last" value='<span class="pagelinks"><div class="pagination center"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li></li></ul></div></span>'/>
					   <display:setProperty name="paging.banner.full" value='<span class="pagelinks"><div class="pagination center"><ul><li><a href="{2}">&lt; Prev</a></li><li>{0}</li><li><a href="{3}">Next &gt;</a></li></ul></div></span>'/>
					
					</display:table>
                                      
				</form>
			
		</div><!--  end of span9 -->
		</div><!-- end row-fluid -->

 </div>
  <div id="modalBox" class="modal hide fade">
	<div class="modal-header">
		<a class="close" data-dismiss="modal" data-original-title="">x</a>
		<h3 id="myModalLabel"><spring:message code="label.viewComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
		<p></p>
	</div>
	<div class="modal-footer">
		<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.btnClose"/></a>
	</div>
</div>
  
<script type="text/javascript">
	$('.ttclass').tooltip();
	
	function getComment(commentId)
	{
		$('#commentDet').html("<p> Loading Comment...</p>");
		$.ajax({
			  type: 'GET',
			  url: "../getComment",
			  data:{"commentId":commentId},
			  success: function(data) {
				  $('#commentDet').html("<p> "+ data + "</p>");
	          }	  
			});
		
	}	
</script>
