package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.cms.dsh.vci.extension._1.PayPeriodInformationType;
import com.getinsured.iex.hub.vci.gov.niem.niem.proxy.xsd._2.Date;

public class PayPeriodInformationTypeWrapper implements JSONAware{
	
	private String payPeriodEndDate;
	private IncomeTypeWrapper incomeDetail;
	
	private static final String PERIOD_KEY = "PayPeriodEndDate";
	private static final String INCOME_KEY = "IncomeDetail";
	
	public PayPeriodInformationTypeWrapper(PayPeriodInformationType payPeriodInformationType) {

		if (payPeriodInformationType == null) {
			return;
		}
		
		if(payPeriodInformationType.getPayPeriodEndDate() != null
    			&& payPeriodInformationType.getPayPeriodEndDate().getDateRepresentation() != null 
    			&& payPeriodInformationType.getPayPeriodEndDate().getDateRepresentation().getValue() != null){
    		Date date = (Date)payPeriodInformationType.getPayPeriodEndDate().getDateRepresentation().getValue();
			this.payPeriodEndDate = date.getValue().toString();
		}
		
		this.incomeDetail = new IncomeTypeWrapper(payPeriodInformationType.getIncomeDetail());
		
	}

	public String getPayPeriodEndDate() {
		return payPeriodEndDate;
	}

	public IncomeTypeWrapper getIncomeDetail() {
		return incomeDetail;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(PERIOD_KEY, this.payPeriodEndDate);
		obj.put(INCOME_KEY, this.incomeDetail);
		return obj.toJSONString();
	}
	
	
}
