package com.getinsured.ssap.util;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.getinsured.ssap.model.hub.vlp.documents.DhsDocument;

/**
 * Custom serializer for DHS Documents.
 *
 * @author Yevgen Golubenko
 * @since 2/14/19
 */
public class DhsDocumentSerializer extends JsonSerializer<DhsDocument>
{
  private static final Logger log = LoggerFactory.getLogger(DhsDocumentSerializer.class);
  @Override
  public void serialize(final DhsDocument dhsDocument, final JsonGenerator jgen,
                        final SerializerProvider provider) throws IOException, JsonProcessingException
  {
    jgen.writeStartObject();

    jgen.writeObjectFieldStart("DHSID");

    String dhsId = "INVALID_DHS_ID";

    if(dhsDocument.getDocumentType() != null) {
      dhsId = dhsDocument.getDocumentType().getDhsId();
    } else {
      log.error("DHS Document doesn't have DocumenType set! [documentType = {}]",
          dhsDocument.getDocumentType());
    }

    jgen.writeObjectField(dhsId, dhsDocument);
    jgen.writeEndObject(); // DHSID
    jgen.writeEndObject();
  }
}
