//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.03.03 at 12:07:52 PM IST 
//


package com.getinsured.iex.hub.apc.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.getinsured.iex.hub.apc.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.hub.apc.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for an Advance Payment of Premium Tax Credit (APTC) calculation.
 * 
 * <p>Java class for APTCType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APTCType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}APTCMaximumAmount"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-ee}APTCRemainingBHCAmount"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APTCType", propOrder = {
    "aptcMaximumAmount",
    "aptcRemainingBHCAmount"
})
public class APTCType
    extends ComplexObjectType
{

    @XmlElement(name = "APTCMaximumAmount", required = true)
    protected AmountType aptcMaximumAmount;
    @XmlElement(name = "APTCRemainingBHCAmount", required = true)
    protected AmountType aptcRemainingBHCAmount;

    /**
     * Gets the value of the aptcMaximumAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAPTCMaximumAmount() {
        return aptcMaximumAmount;
    }

    /**
     * Sets the value of the aptcMaximumAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAPTCMaximumAmount(AmountType value) {
        this.aptcMaximumAmount = value;
    }

    /**
     * Gets the value of the aptcRemainingBHCAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountType }
     *     
     */
    public AmountType getAPTCRemainingBHCAmount() {
        return aptcRemainingBHCAmount;
    }

    /**
     * Sets the value of the aptcRemainingBHCAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountType }
     *     
     */
    public void setAPTCRemainingBHCAmount(AmountType value) {
        this.aptcRemainingBHCAmount = value;
    }

}
