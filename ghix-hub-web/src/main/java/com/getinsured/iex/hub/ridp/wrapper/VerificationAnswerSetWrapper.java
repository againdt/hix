package com.getinsured.iex.hub.ridp.wrapper;

import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationAnswerSetType;

public class VerificationAnswerSetWrapper {
	private VerificationAnswerSetType answerSetType;
	private ObjectFactory factory = new ObjectFactory();
	
	public VerificationAnswerSetWrapper(){
		answerSetType = factory.createVerificationAnswerSetType();
	}
	
	public void addVerificationAnswer(VerificationAnswerWrapper verificationAnswer){
		this.answerSetType.getVerificationAnswers().add(verificationAnswer.getSystemRepresentation());
	}
	
	public VerificationAnswerSetType getSystemRepresentation(){
		return this.answerSetType;
	}
}
