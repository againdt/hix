		<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
		<%@ taglib uri="/WEB-INF/tld/comments-view" prefix="comment" %>
		<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
		<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
		<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
		<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
		<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
		<link href="<c:url value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

		
			<div class="gutter10-lr">
			<jsp:include page="/WEB-INF/views/entity/activationLinkResult.jsp" />	
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#"><spring:message code="label.assister.back"/></a></li>
								<li><a href="<c:url value="/entity/entityadmin/displayassisters" />"><spring:message code="label.assister.assisters"/></a></li>
								<li><spring:message code="label.assister.manage"/></li>
								<li>${assister.firstName} ${assister.lastName}</li>
							</ul>
							<!--page-breadcrumb ends-->
						</div><!--  end of row-fluid -->
					</div><!--  end l-page-breadcrumb -->
					<div class="row-fluid">
						<h1><a id="skip"></a>${assister.firstName} ${assister.lastName}</h1>						
					</div>
					<c:set var="assisterId" ><encryptor:enc value="${assister.id}" isurl="true" /> </c:set>
					<c:set var="assisterEntityId" ><encryptor:enc value="${assister.entity.id}" isurl="true" /> </c:set>
					<div class="row-fluid">
						<div id="sidebar" class="span3">
							<div class="header"></div>
							<ul class="nav nav-list">
								<li><a href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.certifiedCounselor"/></a></li>
								<li><a href="<c:url value="/entity/entityadmin/assisterprofile?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.profile"/></a></li>
								<c:choose>	
									<c:when test="${loggedUser=='entityAdmin' }">
										<li><a href="<c:url value="/entity/assisteradmin/certificationstatus?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.certificationStatus"/></a></li>
									</c:when>
									</c:choose>
								<li class="active"><a href="#"><spring:message code="label.assister.status"/></a></li>
							</ul>
						</div><!-- end of span3 -->
						<%-- <jsp:include page="/WEB-INF/views/entity/leftNavigationMenu.jsp" /> --%>
						<div class="span9">
							<div class="header">
								<h4 class="pull-left"><spring:message code="label.assister.status"/></h4>
								<a class="btn btn-small pull-right" href="<c:url value="/entity/entityadmin/editassisterstatus?assisterId=${assisterId}&entityId=${assisterEntityId}" />"><spring:message code="label.assister.edit"/></a>
							</div>
							<form class="form-horizontal" id="frmcertificationstatus" name="frmcertificationstatus" action="" method="post">
							<df:csrfToken/>
								<input type="hidden" id="assisterId" name="assisterId" value="<encryptor:enc value="${assister.id}"/>">
								<input type="hidden" id="entityId" name="entityId" value="<encryptor:enc value="${assister.entity.id}"/>">
								<div class="gutter10-tb">
									<table class="table table-border-none table-condensed">
										<tbody>
											<tr>
												<td><strong><spring:message code="label.assister.status"/></strong></td>
												<td><strong>${assister.status}</strong></td>
												
											</tr>
										</tbody>
									</table>
								</div><!-- end of .gutter10 -->   
									<p><spring:message code="label.assister.viewTheStatusOfUrCertificationAppHereUCan"/></p>
                   						 <h4><spring:message code="label.assister.certificationHistory"/></h4>                   
                                  	<display:table id="assisterActivityStatusHistory" name="assisterActivityStatusHistory" list="assisterActivityStatusHistory" requestURI="" sort="list" class="table" >
                                             <display:column property="updatedOn" titleKey="label.entity.date"  format="{0,date,MM/dd/yyyy}" sortable="false" />
                                             <display:column property="previousActivityStatus" titleKey="label.entity.previousStatus" sortable="false"/>
                                             <display:column property="newActivityStatus" titleKey="label.NewStatus" sortable="false" />
                                              <display:column property="activityComments" titleKey="label.entity.vieComment" sortable="false" />
                                          </display:table>                                
							</form>
						</div><!-- end of span9 -->
					</div>
				
	<div id="modalBox" class="modal hide fade" role="dialog" aria-labelledby="viewCommentLabel" aria-hidden="true" tabindex="-1">
	<div class="modal-header">
		
		<button class="close" data-dismiss="modal" data-original-title="">&times;</button>
		<h3 id="viewCommentLabel"><spring:message code="label.assister.viewComment"/></h3>
	</div>
	<div id="commentDet" class="modal-body">
	<label id="commentlbl"></label>	
	</div>
	
		<div class="modal-footer">
			<a href="#" class="btn btn-primary" data-dismiss="modal" data-original-title=""><spring:message code="label.assister.close"/></a>
	  	</div>
    </div>
				</div>
	
<script type="text/javascript">
		
		function getComment(comments)
		{					
				
			    $("#commentlbl").html("<p>Loading Data...</p>");
				comments =comments.replace(/#/g,'<br/>'); //Added to remove new line break marker with HTML equivalent br				
				comments=comments.replace(/apostrophes/g,"'"); //Added to replace regex apostrophes with '
				$("#commentlbl").html("<p> "+ comments + "</p>");			
		}
		</script>
		<script type="text/javascript">
if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,88,new Date().getTime(),"","","","",""]);
		</script>
