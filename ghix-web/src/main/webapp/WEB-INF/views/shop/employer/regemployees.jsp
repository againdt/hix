<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<script type="text/javascript" src="<c:url value="/resources/js/shop/shoputils.js" />"></script>

<ul class="tabs">
	<li><a href="/shop/employer">Account</a></li>
	<li class="active"><a href="/shop/employer_index">Employee &amp; Dependents</a></li>
	<li><a href="/shop/employer_eligibility">Eligibility</a></li>
	<li><a href="/shop/employer_coverage">Coverage</a></li>
	<li><a href="/shop/employer_payments">Payments</a></li>
	<li><a href="/shop/employer_reports">Reports</a></li>
	<li><a href="/shop/employer_messages">Messages</a></li>
</ul>
<ul class="pills">
	<li><a href="/shop/employer_index/addremove">Manage Your List</a></li>
	<li class="active"><a href="/shop/employer_index/regemployees">Register</a></li>
	<li><a href="/shop/employer_index/validatessns">Validate SSN</a></li>
</ul>

<div class="row">
	<div class="span4">
		<h2>Register Employees</h2>
		<p>
			You now need to register your employees. You may register each
			individual employee or upload your registry using an excel file. To
			access the excel file template along with instructions, <a href="http://c822258.r58.cf2.rackcdn.com/Employee_list_template.xlsx">click here.</a> 
			<strong>NOTE:</strong> The total number of employees should not exceed 50.
		</p>

		<div class="well clearfix">
			<p>
				So far, you have successfully registered <span class="label important">2</span> employees of your company into the Exchange.
			</p>
		</div>
		<a class="btn large primary" type="submit" href="addremove">I&#39;m Done, <br />Preview My Employee List</a>
	</div>
	<div class="row span12">
		<div class="span6">
			<form class="form-stacked" id="frmcontact" name="frmcontact" action="regemployees" method="post">
				<df:csrfToken/>
				<fieldset>
					<legend>Register Individual Employee &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;OR</legend>
					<div class="profile">
						<div class="clearfix">
							<label for="firstName" class="required">First Name</label>
							<div class="input">
								<input type="text" name="firstName" id="firstName" value="" class="xlarge" size="60">
							</div>
						</div>
						<div class="clearfix">
							<label for="lastName" class="required">Last Name</label>
							<div class="input">
								<input type="text" name="lastName" id="lastName" value="" class="xlarge" size="60">
							</div>
						</div>
						<div class="clearfix">
							<label for="dob_mm" class="optional">Date of Birth <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="input">

								<input type="text" name="dob_mm" id="dob_mm" value="" size="2" maxlength="2" placeholder="MM" onKeyUp="shiftbox(this,&#39;dob_dd&#39;)"> 
								<input type="text" name="dob_dd" id="dob_dd" value="" size="2" maxlength="2" placeholder="DD" onKeyUp="shiftbox(this,&#39;dob_yy&#39;)"> 
								<input type="text" name="dob_yy" id="dob_yy" value="" size="4" maxlength="4" placeholder="YYYY">
							</div>
							<div id="dob_yy_error" class=""></div>
						</div>

						<div class="clearfix">
							<label for="gender" class="required">Gender</label>
							<div class="input">
								<select name="gender" id="gender" class="small">
									<option value="Male" label="Male">Male</option>
									<option value="Female" label="Female">Female</option>
								</select>
							</div>
						</div>
						<div class="clearfix">
							<label for="ssn1" class="required">Social Security Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="input">

								<input type="text" name="ssn1" id="ssn1" value="123" size="3" maxlength="3" onKeyUp="shiftbox(this,&#39;ssn2&#39;)">
								<input type="text" name="ssn2" id="ssn2" value="45" size="2" maxlength="2" onKeyUp="shiftbox(this,&#39;ssn3&#39;)">
								<input type="text" name="ssn3" id="ssn3" value="6789" size="4" maxlength="4">
							</div>
							<div id="ssn3_error" class=""></div>
						</div>
						<div class="clearfix">
							<label for="annual_salary" class="required">Annual Salary</label>
							<div class="input">
								$ <input type="text" name="annual_salary" id="annual_salary" value="" class="small">
							</div>
						</div>
						<div class="clearfix">
							<label for="email" class="required">Email Address</label>
							<div class="input">
								<input type="text" name="email" id="email" value="" class="xlarge" size="30">
							</div>
						</div>
						<div class="clearfix">
							<label for="phone1" class="required">Phone Number <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
							<div class="input">

								<input type="text" name="phone1" id="phone1" value="" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone2&#39;)"> 
								<input type="text" name="phone2" id="phone2" value="" size="3" maxlength="3" class="micro" onKeyUp="shiftbox(this,&#39;phone3&#39;)"> 
								<input type="text" name="phone3" id="phone3" value="" size="4" maxlength="4" class="micro">
							</div>
							<div id="phone3_error" class=""></div>
						</div>
						<div class="clearfix">
							<label for="address" class="required">Address</label>
							<div class="input">
								<input type="text" name="address" id="address" value="" class="xlarge" size="30">
							</div>
						</div>
						<div class="clearfix row">
							<div class="span3">
								<label for="city" class="required">City</label>
								<div class="input">
									<input type="text" name="city" id="city" value="St. Paul" class="medium" size="30">
								</div>
							</div>
							<div class="span2">
								<label for="zip" class="required">Zip Code</label>
								<div class="input">
									<input type="text" name="zip" id="zip" value="55155" class="mini" size="5" onBlur="populateData(this.value,false)" maxlength="5">
								</div>
							</div>
						</div>
						<div class="clearfix">
							<label for="hrs_wk" class="required">Hours per Week</label>
							<div class="input">
								<input type="text" name="hrs_wk" id="hrs_wk" value="" class="mini">
							</div>
						</div>
						<div class="clearfix">
							<dt id="native_amr-label">
								<label class="optional">Member of Native American Tribe?</label>
							</dt>
							<ul class="inputs-list">
								<li>
								<label for="native_amr-Yes"><input type="radio" name="native_amr" id="native_amr-Yes" value="Yes"> Yes</label>
								<label for="native_amr-No"><input type="radio" name="native_amr" id="native_amr-No" value="No"> No</label></li>
							</ul>
						</div>
						<div class="clearfix">
							<label for="work_address" class="required">Worksite Address</label>
							<div class="input">
								<input type="text" name="work_address" id="work_address" value="" class="xlarge" size="30">
							</div>
						</div>
						<div class="clearfix row">
							<div class="span3">
								<label for="work_city" class="required">City</label>
								<div class="input">
									<input type="text" name="work_city" id="work_city" value="St. Paul" class="medium" size="30">
								</div>
							</div>
							<div class="span2">
								<label for="work_zip" class="required">Zip Code</label>
								<div class="input">
									<input type="text" name="work_zip" id="work_zip" value="55155" class="mini" size="5" onBlur="populateData(this.value,true)" maxlength="5">
								</div>
							</div>
						</div>
					</div>
					<div class="span8">
						<div class="form-stacked">
							<div class="actions span8">
								<div class="clearfix">
									<input type="submit" name="continue" id="continue" value="Submit" title="Submit" class="btn large primary" style="width: 100px">
									&nbsp;&nbsp;
									<a class="btn large primary" type="submit" href="addremove"> I&#39;m Done, Preview My Employee List</a>
								</div>
							</div>
						</div>
					</div>
				</fieldset>
			</form>
		</div>
		<div class="span5">
			<form class="form-stacked" id="frmreg" name="frmreg" action="regemployees" method="post" enctype="multipart/form-data">
			<df:csrfToken/>
				<fieldset>
					<legend>Upload List of Employees</legend>
					<p>You may register employees by uploading their information in a Microsoft Excel file in the format provided. You may upload
						multiple lists. You can also come back and make corrections manually to this list when completed.</p>
					<div class="clearfix">
						<ul class="inputs-list">
							<li>
								<div class="clearfix">
									<a href="http://c822258.r58.cf2.rackcdn.com/Employee_list_template.xlsx" class="btn small">Download Excel Template &#40;CSV&#41;</a>
								</div>
							</li>
							<li>
								<div class="clearfix">
									<label for="fileInput">Fill out and Upload Your Excel
										Template</label>
									<div class="input span5">
										<dt id="fileName-label">
											<label for="fileName" class="optional">Select a file</label>
										</dt>
										<dd>
											<input type="hidden" name="MAX_FILE_SIZE" value="8388608" id="MAX_FILE_SIZE"> 
											<input type="file" name="fileName" id="fileName" class="xlarge" size="30">
										</dd>
									</div>
								</div>
								<div class="clearfix">

									<input type="submit" name="continue" id="continue" value="Upload" class="btn" title="Upload">
								</div>
							</li>
						</ul>
					</div>
				</fieldset>
			</form>
		</div>

	</div>

	<div class="notes" style="display: none">
		<div class="row">
			<div class="span">
				<p>The Employer may register each of their Employees individually or upload a list of Employees using an Excel template in the CSV format.</p>
				<br />
				<p>All fields are subject to format validation (e.g. a zip code must be a 5 digit number). A question mark icon opens a light-box
					with helpful information for the end user. The SSN number can be validated automatically with an external data source if an adequate
					web API is available.</p>
			</div>
		</div>
	</div>

</div>

<script type="text/javascript">
function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

function populateData(zipCode,work){
	var subUrl = '<c:url value="regemployees">
						<c:param name="${df:csrfTokenParameter()}"> 
							<df:csrfToken plainToken="true" />
						</c:param> 
					</c:url>';
	$.ajax({
	  url: subUrl,
	  type: "GET",
	  data: {op : 'GETZIPDATA', zipCode : zipCode},
	  success: function(data,xhr){
	        if(isInvalidCSRFToken(xhr))
	          return;
          if (work) {
        	  $('#work_city').val(data);
          } else {
		  	$('#city').val(data);
          }
	  }
	});
}

jQuery.validator.addMethod("ssncheck", function(value, element, param) {
	ssn1 = $("#ssn1").val(); ssn2 = $("#ssn2").val(); ssn3 = $("#ssn3").val();
  	if( (ssn1 == "" || ssn2 == "" || ssn3 == "")  || (isNaN(ssn1)) || (isNaN(ssn2)) || (isNaN(ssn3))  ){ 
  	  	return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("phonecheck", function(value, element, param) {
	phone1 = $("#phone1").val(); phone2 = $("#phone2").val(); phone3 = $("#phone3").val();
  	if( (phone1 == "" || phone2 == "" || phone3 == "")  || (isNaN(phone1)) || (isNaN(phone2)) || (isNaN(phone3))  ){ 
  	  	return false; 
  	}
  	return true;
});

jQuery.validator.addMethod("dobcheck", function(value, element, param) {
  	dob_mm = $("#dob_mm").val(); dob_dd = $("#dob_dd").val(); dob_yy = $("#dob_yy").val(); 
  
  	if( (dob_mm == "" || dob_dd == "" || dob_yy == "")  || (isNaN(dob_mm)) || (isNaN(dob_dd)) || (isNaN(dob_yy))  ){ return false; }
  		
	var today=new Date()
	var birthDate=new Date();
	birthDate.setFullYear(dob_yy ,dob_mm - 1,dob_dd);
	if( (today.getFullYear() - 100) >  birthDate.getFullYear() ) { return false; }
	if( (dob_dd != birthDate.getDate()) || (dob_mm - 1 != birthDate.getMonth()) || (dob_yy != birthDate.getFullYear()) ) { return false; }
	if(today.getTime() < birthDate.getTime()){ return false; }
	return true;
});

var validator = $("#frmcontact").validate({ 
	rules: { dob_yy:{ dobcheck: true }, ssn3:{ ssncheck: true  }, 
			 phone3:{ phonecheck: true }
	       },
    messages: { 
           dob_yy: "<span> <em class='excl'>!</em> Enter valid date of birth.</span>" ,
           ssn3: "<span> <em class='excl'>!</em> Enter valid SSN number.</span>" ,
           phone3: "<span> <em class='excl'>!</em> Enter valid phone number.</span>" ,
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
	}
});


$(function() {
	$('#zip').mask('99999', {placeholder:""});
	$('#phone1').mask('999', {placeholder:""});
	$('#phone2').mask('999',{placeholder:""});
	$('#phone3').mask('9999',{placeholder:""});
	$('#ssn1').mask('999',{placeholder:""});
	$('#ssn2').mask('99',{placeholder:""});
	$('#ssn3').mask('9999',{placeholder:""});
	$('#dob_mm').mask('99',{placeholder:""});
	$('#dob_dd').mask('99',{placeholder:""});
	$('#dob_yy').mask('9999',{placeholder:""});
});

</script>
