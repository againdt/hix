<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<style>
#container-wrap .alert {
	background: #d9edf7;
	border: 1px solid #bce8f1;
	color: #337ca3;
	width: 86%;
	margin-left: 7%;
	min-height: 250px;
	position: relative;
	top: 100px;
}

.alert {
	padding: 8px 35px 8px 14px;
	margin-bottom: 20px;
	text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
	background-color: #fcf8e3;
	border: 1px solid #fbeed5;
	-webkit-border-radius: 4px;
	-moz-border-radius: 4px;
	border-radius: 4px;
}

.alert-info {
	color: #3a87ad;
	background-color: #d9edf7;
	border-color: #bce8f1;
}

#container-wrap .alert:before {
	content: "\f085";
	font-family: FontAwesome;
	font-size: 110px;
	color: #337ca3;
	position: absolute;
	top: 190px;
	left: 72%;
}

#container-wrap .alert p {
	margin-left: 40px;
	margin-right: 40px;
	font-size: 1.05em;
	line-height: 1.7em;
	font-family: Arial;
}

#container-wrap .alert h3 {
	font-size: 1.6em;
	line-height: 1.3em;
	margin: 35px 40px 10px;
	font-family: "montserratregular", Arial, serif;
}
</style>

<div class="gutter10">
	<div class="alert alert-info">
		<h3>Please come back on Nov 15, 2014</h3>
		<p>
			Open Enrollment for 2015 coverage starts November 15, 2014. If you
			need coverage for the rest of 2014, please visit <b><a
				href="https://www.healthcare.gov">healthcare.gov</a></b>.
			If you would like to look at your options for 2015, click <b><a
				href="<c:url value='/preeligibility' />">here</a></b> to use our
			anonymous shopping tool.	
		</p>
	</div>
</div>