
import bloodRelationsReducer from '../bloodRelationsReducer';
import { bloodRelationshipsTypes } from '../../constants/actionTypes';

const initialState = {
    dropdownOptions: [],
    loadedRelations: [],
    selfOption: {},
    loadedRelationsWholeList: []
};

const anotherAction = {
    type: 'ANOTHER_ACTION',
    payload: []
};

const dropdownOptions = [{
    text: 'Relationship',
    value: 'default'
}];

const clearAction = {
    type: bloodRelationshipsTypes.CLEAR_STATE
};

const setAction = {
    type: bloodRelationshipsTypes.SET_RELATIONS,
    bloodRelationships: [
        {
            text: 'Self',
            type: 'SELF',
            code: '18',
            mirrorType: 'SELF'
        },
        {
            text: 'Uncle or Aunt',
            type: 'UNCLE_AUNT',
            code: '06',
            mirrorType: 'NEPHEW_NIECE'
        },
        {
            text: 'Ward',
            type: 'WARD',
            code: '15',
            mirrorType: 'COURT_APPOINTED_GUARDIAN'
        }
    ]
};

const setWholeRelationsAction = {
    type: bloodRelationshipsTypes.SET_RELATIONS_WHOLE_LIST,
    bloodRelationships: [
        {
            text: 'Self',
            type: 'SELF',
            code: '18',
            mirrorType: 'SELF'
        },
        {
            text: 'Uncle or Aunt',
            type: 'UNCLE_AUNT',
            code: '06',
            mirrorType: 'NEPHEW_NIECE'
        },
        {
            text: 'Ward',
            type: 'WARD',
            code: '15',
            mirrorType: 'COURT_APPOINTED_GUARDIAN'
        }
    ]
};

describe('Test for bloodRelationsReducer', () => {

    it('This reducer hasn\'t been invoked', () => {
        expect(bloodRelationsReducer(initialState, anotherAction)).toEqual(initialState);
    });

    it('Should set blood relationships', () => {
        expect(bloodRelationsReducer(initialState, setAction)).toEqual({
            dropdownOptions: [
                ...dropdownOptions,
                ...setAction
                    .bloodRelationships
                    .reduce((res, el) => el.type !== 'SELF' ? [ ...res, { text: el.text, value: el.code} ] : res, [])
            ],
            loadedRelations: setAction.bloodRelationships,
            selfOption: setAction.bloodRelationships.find(br => br.type === 'SELF'),
            loadedRelationsWholeList: initialState.loadedRelationsWholeList
        });
    });

    it('Should set whole list of blood relationships', () => {
        expect(bloodRelationsReducer(initialState, setWholeRelationsAction)).toEqual({
            ...initialState,
            loadedRelationsWholeList: setAction.bloodRelationships
        }) ;
    });

    it('Should clear state', () => {
        expect(bloodRelationsReducer({ welcomeText: 'Hi there!' }, clearAction)).toEqual(initialState);
    });

});