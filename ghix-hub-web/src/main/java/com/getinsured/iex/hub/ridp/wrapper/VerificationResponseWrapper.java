package com.getinsured.iex.hub.ridp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.FinalDecisionCodeSimpleType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationQuestionSetType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationQuestionType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationResponseType;


public class VerificationResponseWrapper implements JSONAware {
	private static final Logger logger = LoggerFactory.getLogger(VerificationResponseWrapper.class); 
	private String sessionIdentification;
	private List<VerificationQuestionWrapper> questions;
	private String dhsRefNumber;
	private String finalDecision;

	public VerificationResponseWrapper(VerificationResponseType resType) {
		this.dhsRefNumber = resType.getDSHReferenceNumber();
		this.sessionIdentification = resType.getSessionIdentification();
		FinalDecisionCodeSimpleType tmp = resType.getFinalDecisionCode();
		if(tmp != null){
				this.finalDecision = tmp.value();
		}
		VerificationQuestionSetType questionSet = resType
				.getVerificationQuestions();
		if(questionSet != null){
			List<VerificationQuestionType> questionTypes = questionSet
				.getVerificationQuestionSet();
			logger.info("Received "+questionTypes.size()+" RIDP questions");
			this.questions = extractQuestions(questionTypes);
		}else{
			logger.info("Failed to receive the question set");
		}
	}

	private List<VerificationQuestionWrapper> extractQuestions(
			List<VerificationQuestionType> questions) {
		int questionNo = 0;
		List<VerificationQuestionWrapper> returnVal = new ArrayList<VerificationQuestionWrapper>();
		if (questions != null && !questions.isEmpty()) {
			VerificationQuestionWrapper tmpQuestion;
			for (VerificationQuestionType question : questions) {
				questionNo++;
				logger.info("Creating Question Wrapper:"+question.getVerificationQuestionText());
				tmpQuestion = new VerificationQuestionWrapper(question);
				tmpQuestion.setQuestionNumber(questionNo);
				returnVal.add(tmpQuestion);
			}
		}
		return returnVal;
	}

	public String getSessionIdentifier() {
		return this.sessionIdentification;
	}

	public String getFinalDecision() {
		return finalDecision;
	}

	public String getSessionIdentification() {
		return sessionIdentification;
	}

	public List<VerificationQuestionWrapper> getQuestions() {
		return questions;
	}

	public String getType() {
		return dhsRefNumber;
	}

	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("dhsReferenceNumber", this.dhsRefNumber);
		obj.put("sessionIdentifier", this.sessionIdentification);
		obj.put("finalDecision", this.finalDecision);
		JSONArray questionSet = new JSONArray();
		if(this.questions != null && !questions.isEmpty()){
			for (VerificationQuestionWrapper quest : this.questions) {
				questionSet.add(quest);
			}
			obj.put("questions", questionSet);
		}
		return obj.toJSONString();
	}
}
