package com.getinsured.iex.hub.vlp.validator;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Validator for doc date of birth
 * 
 * @author Nikhil Talreja
 * @since 06-Feb-2014
 *
 */
public class DocDateOfBirthValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DocDateOfBirthValidator.class);
	
	/** 
	 * Validations for doc date of birth
	 * 1. Should be a valid date
	 * 2. Date format is YYYY-MM-DD
	 * 3. Cannot be a future date
	 * 
	 * Required : No
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		//Value is not mandatory
		if(value == null){
			return;
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		try{
			XMLGregorianCalendar docBirthDate = VLPServiceUtil.stringToXMLDate(value.toString(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
			if(docBirthDate.toGregorianCalendar().compareTo(Calendar.getInstance()) > 0){
				throw new InputDataValidationException(VLPServiceConstants.DATE_OF_BIRTH_FUTURE_ERROR_MESSAGE);
			}
		}
		catch(ParseException e){
			throw new InputDataValidationException(VLPServiceConstants.DATE_ERROR_MESSAGE+". Expected format is " + VLPServiceConstants.VLP_SERVICE_DATE_FORMAT.toUpperCase(), e);
		} 
		catch (DatatypeConfigurationException e) {
			throw new InputDataValidationException(VLPServiceConstants.DATE_ERROR_MESSAGE+". Expected format is " + VLPServiceConstants.VLP_SERVICE_DATE_FORMAT.toUpperCase(), e);
		}
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
