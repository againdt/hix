package com.getinsured.hix.batch.enrollment.skip;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component("enrollmentIRSOut")
public class EnrollmentIRSOut {

	private Map<String,String> skippedHouseholdMap=null;
	private String batchId;
	private String oldBatchId;
	private String action;
	boolean isGeneratedXMLsValid = Boolean.TRUE;
	
	List<String> householdCaseIds=null;
	public EnrollmentIRSOut() {
		skippedHouseholdMap= new HashMap<String, String>();
		householdCaseIds= new ArrayList<String>();
	}
	
	public synchronized void putToSkippedHouseholdMap(String key, String value){
		skippedHouseholdMap.put(key, value);
	}
	public synchronized void putAllToSkippedHouseholdMap(Map<String,String> failedHouseholdMap){
		this.skippedHouseholdMap.putAll(failedHouseholdMap);
	}	
	
	public Map<String,String> getSkippedHouseholdMap(){
		return skippedHouseholdMap;
	}
	
	public synchronized void clearSkippedHouseholdMap(){
		this.skippedHouseholdMap.clear();
	}
	
	public synchronized void addAllToHouseholdCaseIds(List<String> householdCaseIds){
		this.householdCaseIds.addAll(householdCaseIds);
	}
	
	public synchronized void addToHouseholdCaseIds(String householdCaseId){
		this.householdCaseIds.add(householdCaseId);
	}
	
	public List<String> getHouseholdCaseIds(){
		return householdCaseIds;
	}
	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public synchronized void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the batchId
	 */
	public String getBatchId() {
		return batchId;
	}

	/**
	 * @param batchId the batchId to set
	 */
	public synchronized void setBatchId() {
		this.batchId = createBatchId();
	}

	/**
	 * @return the oldBatchId
	 */
	public String getOldBatchId() {
		return oldBatchId;
	}

	/**
	 * @param oldBatchId the oldBatchId to set
	 */
	public synchronized void setOldBatchId(String oldBatchId) {
		this.oldBatchId = oldBatchId;
	}

	public synchronized void clearHouseholdCaseIds(){
		this.householdCaseIds.clear();
	}
	/**
	 * @return the isGeneratedXMLsValid
	 */
	public boolean isGeneratedXMLsValid() {
		return isGeneratedXMLsValid;
	}

	/**
	 * @param isGeneratedXMLsValid the isGeneratedXMLsValid to set
	 */
	public synchronized void setGeneratedXMLsValid(boolean isGeneratedXMLsValid) {
		this.isGeneratedXMLsValid = isGeneratedXMLsValid;
	}
	
	/**
	 * Get Batch Id 
	 * @return String batchId  yyyy-MM-dd'T'HH:mm:ss'Z'
	 */
	private String createBatchId() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());
		cal.set(Calendar.SECOND, cal.get(Calendar.SECOND));
		return sdf.format(cal.getTime()); 
	}
	
	
	public synchronized void resetAllFields(){
		this.skippedHouseholdMap.clear();
		this.householdCaseIds.clear();
		this.batchId = null;
		this.oldBatchId = null;
		this.action = null;
		this.isGeneratedXMLsValid = Boolean.TRUE;
	}
}
