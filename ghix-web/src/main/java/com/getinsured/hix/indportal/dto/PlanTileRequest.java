package com.getinsured.hix.indportal.dto;

public class PlanTileRequest {
	
	private String caseNumber;
	private String stage;
	
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getStage() {
		return stage;
	}
	public void setStage(String stage) {
		this.stage = stage;
	}
	
	

}
