package com.getinsured.hix.ssap.models;

import java.util.ArrayList;
import java.util.List;

public class DocumentCategory {
  private String documentCategoryName;
  private List<DocumentCategoryDtl> documentCategoryDtlList = new ArrayList<DocumentCategoryDtl>();
  private List<String> documentTypeList = new ArrayList<String>();
  private boolean isDocumentCategoryVerified;
  private String verificationStatus;

  public DocumentCategory() {

  }

  public DocumentCategory(String documentCategoryName, List<String> documentTypeList, boolean isDocumentCategoryVerified) {
    this.documentCategoryName = documentCategoryName;
    this.documentTypeList = documentTypeList;
    this.isDocumentCategoryVerified = isDocumentCategoryVerified;
  }

  public DocumentCategory(String documentCategoryName, List<String> documentTypeList, boolean isDocumentCategoryVerified, String verificationStatus) {
    this.documentCategoryName = documentCategoryName;
    this.documentTypeList = documentTypeList;
    this.isDocumentCategoryVerified = isDocumentCategoryVerified;
    this.verificationStatus = verificationStatus;
  }


  public String getDocumentCategoryName() {
    return documentCategoryName;
  }

  public void setDocumentCategoryName(String documentCategoryName) {
    this.documentCategoryName = documentCategoryName;
  }

  public List<DocumentCategoryDtl> getDocumentCategoryDtlList() {
    return documentCategoryDtlList;
  }

  public void setDocumentCategoryDtlToList(DocumentCategoryDtl objDocumentCategoryDtl) {
    this.documentCategoryDtlList.add(objDocumentCategoryDtl);
  }

  public List<String> getDocumentTypeList() {
    return documentTypeList;
  }

  public void setDocumentTypeList(List<String> documentTypeList) {
    this.documentTypeList = documentTypeList;
  }

  public boolean isDocumentCategoryVerified() {
    return isDocumentCategoryVerified;
  }

  public void setDocumentCategoryVerified(boolean isDocumentCategoryVerified) {
    this.isDocumentCategoryVerified = isDocumentCategoryVerified;
  }

  public String getVerificationStatus() {
    return verificationStatus;
  }

  public void setVerificationStatus(String verificationStatus) {
    this.verificationStatus = verificationStatus;
  }

  @Override
  public String toString() {
    return "DocumentCategory{" +
      "documentCategoryName='" + documentCategoryName + '\'' +
      ", documentCategoryDtlList=" + documentCategoryDtlList +
      ", documentTypeList=" + documentTypeList +
      ", isDocumentCategoryVerified=" + isDocumentCategoryVerified +
      ", verificationStatus='" + verificationStatus + '\'' +
      '}';
  }
}
