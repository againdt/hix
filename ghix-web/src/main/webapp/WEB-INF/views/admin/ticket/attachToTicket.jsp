<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<style type="text/css">
.modal .header{
	border-radius: 6px 6px 0 0 !important;
    padding: 10px !important;
}
.modal .dialogClose{
	margin: -4px !important;
    padding: 4px;
}
</style>
<input type="hidden" id="ticketId" name="ticketId" value="${param.ticketId}"> 

<!-- Ticket Upload  dialog  -->
<form  id="uploadDocumentModal" enctype="multipart/form-data" name="uploadDocumentModal" method="POST" action="<c:url value='/ticketmgmt/ticket/uploadTicketAttachments'/>">
<df:csrfToken/>
<div id="uploadDocumentDialog" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="uploadDocumentDialog" aria-hidden="true">
		<div class="uploadDialogHeader">
	    	<div class="header">
	            <h4 class="pull-left"><spring:message code="header.uploadAttachment" /></h4>
	            <button aria-hidden="true" data-dismiss="modal" id="crossClose" class="close" title="x" type="button"><spring:message code="btn.crossClose" /></button>
	        </div>
	    </div>
	  
	  <div class="modal-body">
	    <div class="control-group">	
					<div class="controls" id="controls">
					<div class="controls paddingT5">
					<input type="hidden" id="ticketId" name="ticketId" value="${param.ticketId}">
									<!-- File upload plugin starts -->
									<input type="file" class="input-file" id="fileuploadaction" name="files[]" accept=".gif,.jpeg,.jpg,.png,.pdf,.bmp">&nbsp; 
							                <!-- The table listing the files available for attaching/removing -->
			       							<div id="upload_files_container" class="files" ></div>
			       						<!-- File upload plugin ends -->     
					</div>
					</div>
				</div>
	  </div>
	  <div class="modal-footer clearfix">
	    <button class="btn pull-left" data-dismiss="modal" aria-hidden="true"><spring:message code="button.cancel" /></button>
	    <button class="btn btn-primary upload-submit-btn"><spring:message code="sp_button.submit" /></button>
	  </div>
	</div>
	
</form>

<c:if test="${not empty Upload_Failure}">
	<form class="upload-failure-area">
	<div>
	  <label class="error">
	        <span><em class="excl">!</em>
	    	${Upload_Failure}
	    	<c:remove var="Upload_Failure"/>
	    	<input type="hidden" id="ticketAttachment" name="ticketAttachment" value="yes">
			</span>
	  </label>
	</div>
	</form>
</c:if>	

<!-- Cancel ticket dialog ends -->
<script type="text/javascript">
var taskClaimed=true;
$("document").ready(function (){
	$(".upload-submit-btn").click(function(){
		 var fup = document.getElementById('fileuploadaction');
		 var fileName = fup.value;
		 if(fileName==""){
			alert("Please select file to upload");
			return false;
		 }
		 else{
			 
			 document.forms['uploadDocumentModal'].submit();
		 }
	});
	
});

 $(function(){
	 $('#fileuploadaction').change(function(){
			var fileObject=this.files[0];
			var excludeTypes = ['image/gif', 'image/jpeg', 'image/png','application/pdf','image/bmp'];
			if (fileObject && !excludeTypes.includes(fileObject.type)) {
				alert("Unknown file type. Please upload one of these file types: pdf,png,gif,jpg/jpeg,bmp");
				$("#fileuploadaction").val(null);
			}
		});
	});
</script>