package com.getinsured.hix.indportal.dto.dashboard;

import java.io.Serializable;

public class SepApplcantEventDTO implements Serializable{

	private static final long serialVersionUID = 1L;
	private String eventName;
	private String changeType;	
	
	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
}
