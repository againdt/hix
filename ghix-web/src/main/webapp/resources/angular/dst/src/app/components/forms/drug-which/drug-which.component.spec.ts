import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DrugWhichComponent } from './drug-which.component';

describe('DrugWhichComponent', () => {
  let component: DrugWhichComponent;
  let fixture: ComponentFixture<DrugWhichComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DrugWhichComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DrugWhichComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
