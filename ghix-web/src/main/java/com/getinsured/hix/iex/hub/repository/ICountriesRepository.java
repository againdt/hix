package com.getinsured.hix.iex.hub.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.iex.hub.Countries;

/**
 * Repository class for COUNTRIES table
 * 
 * HIX-34223
 * 
 * @author Nikhil Talreja
 * @since 11-Apr-2014
 *
 */
public interface ICountriesRepository extends JpaRepository<Countries, Long> {

}
