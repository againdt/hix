package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.BenefitsListArrayType;

/**
 * Wrapper Class for BenefitsListArrayType
 * 
 * @author Vishaka Tharani
 * @since  19-Feb-2014 
 * 
 */
public class BenefitsListArrayTypeWrapper implements JSONAware{
	
    private String name;
    private long code;
    
	private static final String NAME_KEY = "name";
	private static final String CODE_KEY = "code";
	
	public BenefitsListArrayTypeWrapper(BenefitsListArrayType benefitsListArrayType){
		if(benefitsListArrayType != null)
		{
			name = benefitsListArrayType.getName();
			code = benefitsListArrayType.getCode().longValue();
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(NAME_KEY,this.name);
		obj.put(CODE_KEY,this.code);
		return obj.toJSONString();
	}
	
}
