package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("enrollmentEHBUpdation")
public class EnrollmentEHBUpdation {

	List<Integer> enrollmentIdList=null;
	List<Integer> skippedEnrollmentIdList=null;

	public EnrollmentEHBUpdation() {
		enrollmentIdList= new ArrayList<Integer>();
		skippedEnrollmentIdList= new ArrayList<Integer>();
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}

	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}

	
	public synchronized void addToSkippedEnrollmentIdList(Integer enrollmentId){
		this.skippedEnrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getSkippedEnrollmentIdList(){
		return skippedEnrollmentIdList;
	}
	public synchronized void clearSkippedEnrollmentIdList(){
		this.skippedEnrollmentIdList.clear();
	}

	public synchronized void addAllToSkippedEnrollmentIdList(List<Integer> enrollmentIdList){
		this.skippedEnrollmentIdList.addAll(enrollmentIdList);
	}
}
