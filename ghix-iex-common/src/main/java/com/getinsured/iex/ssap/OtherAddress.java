
package com.getinsured.iex.ssap;

import javax.validation.Valid;

import com.google.gson.annotations.Expose;
/**
 * @author chopra_s
 * 
 */
public class OtherAddress {

    @Expose
    @Valid
    private Address address=new Address();
    @Expose
    private LivingOutsideofStateTemporarilyAddress livingOutsideofStateTemporarilyAddress=new LivingOutsideofStateTemporarilyAddress();
    @Expose
    private Boolean livingOutsideofStateTemporarilyIndicator=false;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LivingOutsideofStateTemporarilyAddress getLivingOutsideofStateTemporarilyAddress() {
        return livingOutsideofStateTemporarilyAddress;
    }

    public void setLivingOutsideofStateTemporarilyAddress(LivingOutsideofStateTemporarilyAddress livingOutsideofStateTemporarilyAddress) {
        this.livingOutsideofStateTemporarilyAddress = livingOutsideofStateTemporarilyAddress;
    }

    public Boolean getLivingOutsideofStateTemporarilyIndicator() {
        return livingOutsideofStateTemporarilyIndicator;
    }

    public void setLivingOutsideofStateTemporarilyIndicator(Boolean livingOutsideofStateTemporarilyIndicator) {
        this.livingOutsideofStateTemporarilyIndicator = livingOutsideofStateTemporarilyIndicator;
    }

}
