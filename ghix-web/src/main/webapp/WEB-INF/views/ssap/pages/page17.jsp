<%@ include file="../include.jsp" %>
<div id="appscr67" style='display: none;'>
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages">
				<spring:message code="ssap.page17.familyAndHouseholdSummary" text="Family & Household Summary" />
			</h4>
		</div>
		
		<form id="appscr67Form" class="form-horizontal">
			<div class="header">
				<h4><spring:message code="ssap.page17.header" /></h4>
			</div>
			
			<div class="alert alert-info"><p><spring:message code="ssap.page17.summaryInfo" /></p></div>
			
			
			<div class="gutter10">
				<div id="houseHoldContactSummaryDiv"></div>
			</div>
		</form>
	</div>
</div>

<!-- ApplyingForhouseHold -->
<div id="mecCheckAlert" class="modal hide fade" tabindex="-1" role="dialog">
  <div class="modal-header">
  </div>
  <div class="modal-body">
  <p><spring:message code="ssap.page17.mecCheckAlert"/> <p>
    <%-- <p class="hide PCNotEligible"><spring:message code="ssap.notSeekingCoverageAlert.PCNotEligible1"/> <strong class="PCName"></strong> <spring:message code="ssap.notSeekingCoverageAlert.PCNotEligible2"/><p>  	
    <div class="dependentNotEligible hide">
	    <p class="hide singular"><spring:message code="ssap.notSeekingCoverageAlert.singular"/><p>
	    <p class="hide plural"><spring:message code="ssap.notSeekingCoverageAlert.plural"/><p>
	    <div id="notSeekingCoverageList"></div>
	    <p class="margin10-t"><spring:message code="ssap.notSeekingCoverageAlert.message"/></p>
    </div> --%>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" onclick="goToDashboard()"><spring:message code="ssap.page17.mecCheckGoToDashboard"/></button>
  </div>
</div>