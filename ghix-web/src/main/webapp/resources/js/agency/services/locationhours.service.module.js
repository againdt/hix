(function(){
    'use strict';

    angular.module('locationHoursServiceModule',[])
    .service('LocationHoursService', LocationHoursService);

    LocationHoursService.$inject = ['$window'];
    function LocationHoursService($window) {
        var service = this;
        var items = [{}];
        service.displayPrimarySt = service.displaySubSiteSt = false;
        service.displayRemoveBtn = true;

        service.populateDays = function () {
            return {          
                "weekdays" : [
                    {"day": "Monday", "fromTime": "select",  "toTime": "select", "id": null},
                    {"day": "Tuesday","fromTime": "select", "toTime": "select", "id": null},
                    {"day": "Wednesday","fromTime": "select", "toTime": "select", "id": null},
                    {"day": "Thursday","fromTime": "select", "toTime": "select", "id": null},
                    {"day": "Friday","fromTime": "select", "toTime": "select", "id": null},
                    {"day": "Saturday", "fromTime": "closed", "toTime": "closed", "id": null},
                    {"day": "Sunday", "fromTime": "closed", "toTime": "closed", "id": null}
                ]
            };
        };

        service.nextPage = function() {
            $window.location.href = "/hix/broker/certificationapplication";
        };
    }


})();
