package com.getinsured.hix.batch.provider.processors.ca;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class NetworkTierIdProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;
	private static Logger logger = LoggerFactory.getLogger(NetworkTierIdProcessor.class);

	@Override
	public Object process(Object objToBeValidated) throws ProviderValidationException {

		String s = objToBeValidated.toString();
		s = s.replaceAll("\"", "");
		String[] networks = s.split("\\|");
		ArrayList<String> networkTierList = new ArrayList<String>();

		for(String networkId: networks){
			
			/* Existing Validation had the checked but new does not confirmed to this validation so, commented out.
			int underScaoreIndex = networkId.indexOf('_');
			if(underScaoreIndex == -1){
				throw new ProviderValidationException("Invalid network tier id field "+networkId+" Tier info needs to beperated using \"_\", none found");
			}
			int len = networkId.length();
			if(len <=9){
				throw new ProviderValidationException("Invalid imput provided for network id "+s);
			}
			//logger.info("Processing network id:"+networkId);
			
			String[] networkData = new String[3];
			networkData[0] = networkId.substring(0,5);
			networkData[1] = networkId.substring(5,underScaoreIndex);
			networkData[2] = networkId.substring(underScaoreIndex+1);
			networkTierList.add(networkData[0]+"-"+networkData[1]+"-"+networkData[2]);*/
			networkTierList.add(networkId);
		}

		Object tmp = this.context.getContextField("output_field");
		String outputField = (tmp == null)? null: (String)tmp;

		if(outputField == null){
			//Not available in metadata file, create a default one
			outputField = "network_tier_id";
			this.context.addContextInfo("output_field", outputField);
		}

		if(networkTierList.size() > 0){
			context.addContextInfo(outputField, networkTierList);
		}
		return s;

	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}
}
