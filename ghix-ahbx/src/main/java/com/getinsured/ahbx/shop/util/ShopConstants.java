package com.getinsured.ahbx.shop.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


/**
 * @author Deval
 *
 * In this class we can define constants for GHIX application.
 * These Constants can be
 * either direct (define a constant of type psf & directly assign any value to it)
 * or it can from config file. (define a constant of type psf & populate value from config file
 * by using 'Value' attribute of annotation)
 * 
 */


@Component
public final class ShopConstants {

	private ShopConstants() {
	}

	/*****************************************************************************************************/
	/*********************** AHBX Web Service  configuration properties *************************/
	/*****************************************************************************************************/
	public static String AHBX_GET_EMPLOYER_WSDL;
	public static String AHBX_SEND_CASEORDERID_WSDL;
	
	@Value("#{configProp['ahbx.GetEmployer.WSDL']}")
	public void setAhbxGetEmployerWsdl(String ahbxGetEmployerWsdl) {
		AHBX_GET_EMPLOYER_WSDL = ahbxGetEmployerWsdl;
	}
	
	@Value("#{configProp['ahbx.sendCaseOrderId.WSDL']}")
	public void setSendCaseOrderIdWsdl(String sendCaseOrderIdWsdl) {
		AHBX_SEND_CASEORDERID_WSDL = sendCaseOrderIdWsdl;
	}
	
}
