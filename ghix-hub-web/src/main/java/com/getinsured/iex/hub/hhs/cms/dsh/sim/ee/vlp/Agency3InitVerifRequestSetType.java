
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Agency3InitVerifRequestSetType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Agency3InitVerifRequestSetType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DHSID"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}FirstName"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}MiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}LastName"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}DateOfBirth"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}AKA" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}I94Number" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}Comments" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}RequestedCoverageStartDate"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}FiveYearBarApplicabilityIndicator"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}RequestSponsorDataIndicator"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}RequestGrantDateIndicator"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}RequesterCommentsForHub" minOccurs="0"/>
 *         &lt;element ref="{http://vlp.ee.sim.dsh.cms.hhs.gov}CategoryCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Agency3InitVerifRequestSetType", propOrder = {
    "dhsid",
    "firstName",
    "middleName",
    "lastName",
    "dateOfBirth",
    "aka",
    "i94Number",
    "comments",
    "requestedCoverageStartDate",
    "fiveYearBarApplicabilityIndicator",
    "requestSponsorDataIndicator",
    "requestGrantDateIndicator",
    "requesterCommentsForHub",
    "categoryCode"
})
public class Agency3InitVerifRequestSetType {

    @XmlElement(name = "DHSID", required = true)
    protected DHSIDType dhsid;
    @XmlElement(name = "FirstName", required = true)
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "LastName", required = true)
    protected String lastName;
    @XmlElement(name = "DateOfBirth", required = true)
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(name = "AKA")
    protected String aka;
    @XmlElement(name = "I94Number")
    protected String i94Number;
    @XmlElement(name = "Comments")
    protected String comments;
    @XmlElement(name = "RequestedCoverageStartDate", required = true)
    protected XMLGregorianCalendar requestedCoverageStartDate;
    @XmlElement(name = "FiveYearBarApplicabilityIndicator")
    protected boolean fiveYearBarApplicabilityIndicator;
    @XmlElement(name = "RequestSponsorDataIndicator")
    protected boolean requestSponsorDataIndicator;
    @XmlElement(name = "RequestGrantDateIndicator")
    protected boolean requestGrantDateIndicator;
    @XmlElement(name = "RequesterCommentsForHub")
    protected String requesterCommentsForHub;
    @XmlElement(name = "CategoryCode")
    protected String categoryCode;

    /**
     * Gets the value of the dhsid property.
     * 
     * @return
     *     possible object is
     *     {@link DHSIDType }
     *     
     */
    public DHSIDType getDHSID() {
        return dhsid;
    }

    /**
     * Sets the value of the dhsid property.
     * 
     * @param value
     *     allowed object is
     *     {@link DHSIDType }
     *     
     */
    public void setDHSID(DHSIDType value) {
        this.dhsid = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the aka property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAKA() {
        return aka;
    }

    /**
     * Sets the value of the aka property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAKA(String value) {
        this.aka = value;
    }

    /**
     * Gets the value of the i94Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI94Number() {
        return i94Number;
    }

    /**
     * Sets the value of the i94Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI94Number(String value) {
        this.i94Number = value;
    }

    /**
     * Gets the value of the comments property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComments() {
        return comments;
    }

    /**
     * Sets the value of the comments property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComments(String value) {
        this.comments = value;
    }

    /**
     * Gets the value of the requestedCoverageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedCoverageStartDate() {
        return requestedCoverageStartDate;
    }

    /**
     * Sets the value of the requestedCoverageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedCoverageStartDate(XMLGregorianCalendar value) {
        this.requestedCoverageStartDate = value;
    }

    /**
     * Gets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public boolean isFiveYearBarApplicabilityIndicator() {
        return fiveYearBarApplicabilityIndicator;
    }

    /**
     * Sets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public void setFiveYearBarApplicabilityIndicator(boolean value) {
        this.fiveYearBarApplicabilityIndicator = value;
    }

    /**
     * Gets the value of the requestSponsorDataIndicator property.
     * 
     */
    public boolean isRequestSponsorDataIndicator() {
        return requestSponsorDataIndicator;
    }

    /**
     * Sets the value of the requestSponsorDataIndicator property.
     * 
     */
    public void setRequestSponsorDataIndicator(boolean value) {
        this.requestSponsorDataIndicator = value;
    }

    /**
     * Gets the value of the requestGrantDateIndicator property.
     * 
     */
    public boolean isRequestGrantDateIndicator() {
        return requestGrantDateIndicator;
    }

    /**
     * Sets the value of the requestGrantDateIndicator property.
     * 
     */
    public void setRequestGrantDateIndicator(boolean value) {
        this.requestGrantDateIndicator = value;
    }

    /**
     * Gets the value of the requesterCommentsForHub property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequesterCommentsForHub() {
        return requesterCommentsForHub;
    }

    /**
     * Sets the value of the requesterCommentsForHub property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequesterCommentsForHub(String value) {
        this.requesterCommentsForHub = value;
    }

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryCode(String value) {
        this.categoryCode = value;
    }

}
