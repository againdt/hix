package com.getinsured.hix.entity.enrollmententity.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.enrollmententity.repository.IPopulationIndustryRepository;
import com.getinsured.hix.entity.enrollmententity.service.PopulationIndustryService;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationIndustry;

@Service("populationIndustryService")
@Transactional
public class PopulationIndustryServiceImpl implements PopulationIndustryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PopulationIndustryServiceImpl.class);
	
	@Autowired
	private IPopulationIndustryRepository populationIndustryRepository;

	/**
	 * @see PopulationIndustryService#findById(int)
	 */
	@Override
	public PopulationIndustry findById(int industryId) {
		
		LOGGER.info("findById: START");
		LOGGER.info("findById: END");
		
		return populationIndustryRepository.findById(industryId);
	}

	/**
	 * @see PopulationIndustryService#findByEntityId(int)
	 */
	@Override
	public List<PopulationIndustry> findByEntityId(int entityId) {
		
		LOGGER.info("findByEntityId: START");
		LOGGER.info("findByEntityId: END");
		
		return populationIndustryRepository.findByEntityId(entityId);
	}

	/**
	 * @see PopulationIndustryService#deleteAssociatedIndustries(int)
	 */
	@Override
	public int deleteAssociatedIndustries(int entityId) {
		
		LOGGER.info("deleteAssociatedIndustries: START");
		LOGGER.info("deleteAssociatedIndustries: END");
		
		return populationIndustryRepository
				.deleteAssociatedIndustries(entityId);
	}

	/**
	 * @see PopulationIndustryService#saveAssociatedIndustries(EnrollmentEntity,
	 *      List)
	 */
	@Override
	public void saveAssociatedIndustries(EnrollmentEntity enrollmentEntity,
			List<PopulationIndustry> populationIndustries) {
		
		LOGGER.info("saveAssociatedIndustries: START");

		populationIndustryRepository
				.deleteAssociatedIndustries(enrollmentEntity.getId());

		List<PopulationIndustry> updatedPopulationIndustries = new ArrayList<PopulationIndustry>();
		
		if (populationIndustries != null && !populationIndustries.isEmpty()) {
			for (PopulationIndustry industry : populationIndustries) {
				industry.setEntity(enrollmentEntity);
				updatedPopulationIndustries.add(industry);
			}
			
			populationIndustryRepository.save(updatedPopulationIndustries);
		}
		
		LOGGER.info("saveAssociatedIndustries: END");
	}

}
