package com.getinsured.eligibility.mec.controller;


import java.net.InetAddress;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import java.util.List;

import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.jayway.jsonpath.JsonPath;



/**
 * returns a interupt on screen if the state MEC is valid for the user.
 * @author choudhary_a
 *
 */
@Controller("stateMECController")
public class StateMECController {

	private static final Logger lOGGER = Logger.getLogger(StateMECController.class);

	@Value("#{configProp['ghixStateMECURL']}")
	private static String mECURL;// = "http://iexdev.getinsured.com/ghix/invokeSMEC";
	
	@Autowired	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired	private  org.springframework.web.client.RestTemplate restTemplate;

	@RequestMapping(value = "/checkmecinstate", method = RequestMethod.GET)
	@ResponseBody
	public String checkMECinState(@RequestParam("case_number") String caseNumber) {
		//TODO remove after unit testing String ssapJSON="{\"singleStreamlinedApplication\":{\"applyingForFinancialAssistanceIndicator\":true,\"ApplicationType\":\"OE\",\"acceptPrivacyIndicator\":true,\"authorizedRepresentativeIndicator\":\"no\",\"ssapApplicationId\":\"17150\",\"applicationDate\":\"Mar 13, 2014 1:25:47 PM\",\"IP\":\"\",\"applicationGuid\":\"003e5183-415e-4f16-bde4-5a9985a93f7a\",\"authorizedRepresentative\":{\"signatureisProofIndicator\":\"Signature\",\"phone\":{\"phoneExtension\":\"\",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"partOfOrganizationIndicator\":\"Yes\",\"address\":{\"postalCode\":\"\",\"streetAddress1\":\"\",\"streetAddress2\":\"\",\"state\":\"\",\"city\":\"\"},\"name\":{\"middleName\":\"\",\"lastName\":\"\",\"firstName\":\"\",\"suffix\":\"0\"},\"emailAddress\":\"\",\"organizationId\":\"\",\"companyName\":\"\"},\"clientIp\":\"0:0:0:0:0:0:0:1\",\"broker\":{\"brokerFederalTaxIdNumber\":\"\",\"brokerName\":\"\"},\"taxHousehold\":[{\"householdMember\":[{\"detailedIncome\":{\"partnershipAndSwarpIncomeIncomeProof\":\"VERIFIED\",\"capitalGains\":{\"annualNetCapitalGains\":null},\"unemploymentBenefit\":{\"incomeFrequency\":\"\",\"stateGovernmentName\":\"\",\"benefitAmount\":null,\"unemploymentDate\":\"\"},\"annuityIncomeProof\":\"VERIFIED\",\"scholarshipIncomeProof\":\"VERIFIED\",\"otherIncome\":{\"incomeAmount\":null,\"otherIncomeTypeDescription\":\"\"},\"receiveMAthrough1619\":true,\"taxExemptedIncomeProof\":\"VERIFIED\",\"wageIncomeProof\":\"VERIFIED\",\"dividendsIncomeProof\":\"VERIFIED\",\"farmFishingIncome\":{\"incomeFrequency\":\"\",\"netIncomeAmount\":null},\"rentalRoyaltyIncomeIndicator\":null,\"unemploymentBenefitIndicator\":null,\"capitalGainsIndicator\":null,\"jobIncome\":[{\"incomeFrequency\":\" \",\"employerName\":\"\",\"incomeAmountBeforeTaxes\":null,\"workHours\":\" \"}],\"alimonyReceivedIndicator\":null,\"unemploymentCompensationIncomeProof\":\"VERIFIED\",\"selfEmploymentTax\":{\"incomeAmount\":null},\"studentLoanInterest\":{\"incomeAmount\":null},\"selfEmploymentIncome\":{\"monthlyNetIncome\":null,\"typeOfWork\":\" \"},\"businessIncomeProof\":\"VERIFIED\",\"sellingBusinessProperty\":{\"incomeAmount\":null},\"retirementOrPension\":{\"taxableAmount\":null,\"incomeFrequency\":\"\"},\"royaltiesIncomeProof\":\"VERIFIED\",\"farmFishingIncomeIndictor\":null,\"investmentIncome\":{\"incomeFrequency\":\"\",\"incomeAmount\":null},\"farmIncom\":{\"incomeAmount\":null},\"deductions\":{\"alimonyDeductionAmount\":null,\"otherDeductionAmount\":null,\"alimonyDeductionFrequency\":\" \",\"studentLoanDeductionFrequency\":\" \",\"deductionFrequency\":\"MONTHLY\",\"studentLoanDeductionAmount\":null,\"deductionType\":[\"ALIMONY\",\"STUDENT_LOAN_INTEREST\",\"OTHER\"],\"otherDeductionFrequency\":\" \",\"deductionAmount\":null},\"rentalRoyaltyIncome\":{\"incomeFrequency\":\"\",\"netIncomeAmount\":null},\"receiveMAthroughSSI\":true,\"foreignEarnedIncomeProof\":\"VERIFIED\",\"socialSecurityBenefit\":{\"incomeFrequency\":\"\",\"benefitAmount\":null},\"foreignEarnedIncome\":{\"incomeAmount\":null},\"childNaTribe\":\"VERIFIED\",\"sellingBusinessPropertyIncomeProof\":\"VERIFIED\",\"alimonyReceived\":{\"amountReceived\":null,\"incomeFrequency\":\"\"},\"selfEmploymentTaxProof\":\"VERIFIED\",\"partnershipsCorporationsIncome\":{\"incomeAmount\":null},\"studentLoanInterestProof\":\"VERIFIED\",\"farmIncomeProof\":\"VERIFIED\",\"retirementOrPensionIndicator\":null,\"investmentIncomeIndicator\":null,\"rentalRealEstateIncome\":{\"incomeAmount\":null},\"otherIncomeIndicator\":null,\"pensionIncomeProof\":\"VERIFIED\",\"socialSecurityBenefitProof\":\"VERIFIED\",\"jobIncomeIndicator\":null,\"businessIncome\":{\"incomeAmount\":null},\"taxableInterestIncomeProof\":\"VERIFIED\",\"selfEmploymentIncomeIndicator\":null,\"rentalRealEstateIncomeProof\":\"VERIFIED\",\"discrepancies\":{\"hasWageOrSalaryBeenCutIndicator\":false,\"explanationForJobIncomeDiscrepancy\":\"There is a reason\",\"didPersonEverWorkForEmployerIndicator\":false,\"stopWorkingAtEmployerIndicator\":false,\"hasHoursDecreasedWithEmployerIndicator\":false,\"explanationForDependantDiscrepancy\":\"There is a reason\",\"seasonalWorkerIndicator\":false,\"hasChangedJobsIndicator\":false}},\"dateOfBirth\":\"Aug 08, 1978 00:00:00 AM\",\"parentCaretakerRelatives\":{\"relationship\":\"\",\"dateOfBirth\":\"\",\"mainCaretakerOfChildIndicator\":null,\"name\":{\"middleName\":\"\",\"lastName\":\"\",\"firstName\":\"\",\"suffix\":\"\"},\"liveWithAdoptiveParentsIndicator\":true,\"personId\":[],\"anotherChildIndicator\":false},\"specialCircumstances\":{\"ageWhenLeftFosterCare\":50,\"everInFosterCareIndicator\":false,\"fosterChild\":false,\"numberBabiesExpectedInPregnancy\":0,\"pregnantIndicator\":false,\"gettingHealthCareThroughStateMedicaidIndicator\":true,\"fosterCareState\":\"\",\"tribalManualVerificationIndicator\":true,\"americanIndianAlaskaNativeIndicator\":false},\"livesWithHouseholdContactIndicator\":false,\"socialSecurityCard\":{\"middleNameOnSSNCard\":\"\",\"deathManualVerificationIndicator\":false,\"socialSecurityNumber\":\"500-12-2334\",\"suffixOnSSNCard\":\"0\",\"individualMandateExceptionIndicator\":null,\"ssnManualVerificationIndicator\":true,\"deathIndicatorAsAttested\":false,\"nameSameOnSSNCardIndicator\":true,\"deathIndicator\":false,\"firstNameOnSSNCard\":\"\",\"socialSecurityCardHolderIndicator\":true,\"useSelfAttestedDeathIndicator\":true,\"lastNameOnSSNCard\":\"\",\"reasonableExplanationForNoSSN\":\"0\"},\"strikerIndicator\":false,\"birthCertificateType\":\"NO_CRETIFICATE\",\"citizenshipImmigrationStatus\":{\"honorablyDischargedOrActiveDutyMilitaryMemberIndicator\":false,\"citizenshipStatusIndicator\":true,\"eligibleImmigrationDocumentType\":[{\"DS2019Indicator\":false,\"UnexpiredForeignPassportIndicator\":false,\"TemporaryI551StampIndicator\":false,\"I551Indicator\":false,\"MachineReadableVisaIndicator\":false,\"OtherDocumentTypeIndicator\":false,\"I94InPassportIndicator\":false,\"I571Indicator\":false,\"I766Indicator\":false,\"I20Indicator\":false,\"I94Indicator\":false,\"I797Indicator\":false,\"I327Indicator\":false}],\"naturalizationCertificateIndicator\":true,\"citizenshipManualVerificationStatus\":true,\"citizenshipAsAttestedIndicator\":true,\"eligibleImmigrationStatusIndicator\":true,\"citizenshipDocument\":{\"SEVISId\":\" \",\"foreignPassportOrDocumentNumber\":\" \",\"sevisId\":\" \",\"alienNumber\":\" \",\"nameOnDocument\":{\"middleName\":\"\",\"lastName\":\"\",\"firstName\":\"\",\"suffix\":\"\"},\"documentDescription\":\" \",\"nameSameOnDocumentIndicator\":false,\"visaNumber\":\" \",\"documentExpirationDate\":\" \",\"i94Number\":\" \",\"foreignPassportCountryOfIssuance\":\"0\",\"cardNumber\":\" \"},\"lawfulPresenceIndicator\":true,\"naturalizationCertificateAlienNumber\":\" \",\"eligibleImmigrationDocumentSelected\":\"\",\"otherImmigrationDocumentType\":{\"CubanHaitianEntrantIndicator\":false,\"ORREligibilityLetterIndicator\":false,\"StayOfRemovalIndicator\":false,\"ORRCertificationIndicator\":false,\"AmericanSamoanIndicator\":false,\"WithholdingOfRemovalIndicator\":false},\"naturalizedCitizenshipIndicator\":false,\"livedIntheUSSince1996Indicator\":false,\"naturalizationCertificateNaturalizationNumber\":\" \"},\"medicalExpense\":null,\"specialEnrollmentPeriod\":{\"birthOrAdoptionEventIndicator\":false},\"marriedIndicator\":false,\"bloodRelationship\":[],\"drugFellowIndicator\":false,\"livingArrangementIndicator\":false,\"taxFiler\":{\"spouseHouseholdMemberId\":\"HouseHold2\",\"claimingDependantsOnFTRIndicator\":true,\"taxFilerDependants\":[{\"dependantHouseholdMemeberId\":\"amp\"}],\"liveWithSpouseIndicator\":false},\"shelterAndUtilityExpense\":null,\"tobaccoUserIndicator\":false,\"dependentCareExpense\":null,\"residencyVerificationIndicator\":false,\"taxFilerDependant\":{\"taxFilerDependantIndicator\":false},\"name\":{\"middleName\":\"\",\"lastName\":\"silly\",\"firstName\":\"ymom\",\"suffix\":null},\"ethnicityAndRace\":{\"ethnicities\":{\"ethnicity\":[]},\"races\":{\"race\":[]}},\"personId\":1,\"gender\":\"female\",\"SSIIndicator\":false,\"IPVIndicator\":false,\"householdContactIndicator\":true,\"americanIndianAlaskaNative\":{\"tribeName\":\"AmericanIndian\",\"state\":\"\",\"memberOfFederallyRecognizedTribeIndicator\":false},\"householdContact\":{\"homeAddressIndicator\":false,\"phone\":{\"phoneExtension\":\"\",\"phoneNumber\":\"\",\"phoneType\":\"CELL\"},\"homeAddress\":{\"postalCode\":\"87505\",\"county\":\"Santa Fe\",\"streetAddress1\":\"1896 Lorca Dr\",\"streetAddress2\":\"\",\"state\":\"NM\",\"city\":\"Santa Fe\"},\"stateResidencyProof\":\"\",\"otherPhone\":{\"phoneExtension\":\"\",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"contactPreferences\":{\"emailAddress\":\"silly@mailcatch.com\",\"preferredSpokenLanguage\":\"English\",\"preferredContactMethod\":\"EMAIL\",\"preferredWrittenLanguage\":\"English\"},\"mailingAddress\":{\"postalCode\":\"87505\",\"county\":\"NM\",\"streetAddress1\":\"1896 Lorca Dr\",\"streetAddress2\":\"\",\"state\":\"NM\",\"city\":\"Santa Fe\"},\"mailingAddressSameAsHomeAddressIndicator\":\"true\"},\"PIDVerificationIndicator\":false,\"applicantGuid\":\"6dda07a0-c978-4537-y863-2142cda3de95\",\"PIDIndicator\":false,\"applyingForCoverageIndicator\":true,\"planToFileFTRJontlyIndicator\":false,\"livesAtOtherAddressIndicator\":\"no\",\"healthCoverage\":{\"chpInsurance\":{\"reasonInsuranceEndedOther\":\" \",\"insuranceEndedDuringWaitingPeriodIndicator\":false},\"haveBeenUninsuredInLast6MonthsIndicator\":null,\"currentEmployer\":[{\"employer\":{\"phone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"employerContactPersonName\":\" \",\"employerContactEmailAddress\":\" \"},\"currentEmployerInsurance\":{\"currentLowestCostSelfOnlyPlanName\":\" \",\"willBeEnrolledInEmployerPlanIndicator\":false,\"memberEmployerPlanStartDate\":\"\",\"willBeEnrolledInEmployerPlanDate\":\"\",\"comingPlanMeetsMinimumStandardIndicator\":false,\"memberPlansToDropEmployerPlanIndicator\":true,\"employerWillOfferPlanIndicator\":true,\"expectedChangesToEmployerCoverageIndicator\":true,\"employerCoverageEndingDate\":\"\",\"memberPlansToEnrollInEmployerPlanIndicator\":true,\"employerPlanStartDate\":\"\",\"memberEmployerPlanEndingDate\":\"\",\"comingLowestCostSelfOnlyPlanName\":\" \",\"currentPlanMeetsMinimumStandardIndicator\":false,\"employerWillNotOfferCoverageIndicator\":false,\"isCurrentlyEnrolledInEmployerPlanIndicator\":false}}],\"formerEmployer\":[{\"phone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"CELL\"},\"employerName\":\" \",\"employerIdentificationNumber\":\" \",\"employerContactPersonName\":\" \",\"address\":{\"postalCode\":\"\",\"streetAddress1\":\" \",\"streetAddress2\":\" \",\"state\":\"CA\",\"city\":\" \"},\"employerContactPhone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"employerContactEmailAddress\":\" \"}],\"primaryCarePhysicianName\":\" \",\"otherInsuranceIndicator\":false,\"currentlyEnrolledInCobraIndicator\":false,\"currentlyHasHealthInsuranceIndicator\":false,\"employerWillOfferInsuranceIndicator\":null,\"currentlyEnrolledInRetireePlanIndicator\":false,\"currentOtherInsurance\":{\"peaceCorpsIndicator\":false,\"otherStateOrFederalProgramType\":\" \",\"tricareEligibleIndicator\":false,\"otherStateOrFederalProgramName\":\" \",\"medicareEligibleIndicator\":false,\"otherStateOrFederalProgramIndicator\":false},\"currentlyEnrolledInVeteransProgramIndicator\":false,\"hasPrimaryCarePhysicianIndicator\":false,\"employerWillOfferInsuranceCoverageStartDate\":\"\",\"willBeEnrolledInCobraIndicator\":false,\"willBeEnrolledInVeteransProgramIndicator\":false,\"medicaidInsurance\":{\"requestHelpPayingMedicalBillsLast3MonthsIndicator\":false},\"isOfferedHealthCoverageThroughJobIndicator\":null,\"willBeEnrolledInRetireePlanIndicator\":false},\"planToFileFTRIndicator\":false,\"otherAddress\":{\"address\":{\"postalCode\":\"\",\"county\":\"\",\"streetAddress1\":\"\",\"streetAddress2\":\" \",\"state\":\"\",\"city\":\"\"},\"livingOutsideofStateTemporarilyIndicator\":\"no\"},\"disabilityIndicator\":false,\"heatingCoolingindicator\":false,\"incarcerationStatus\":{\"incarcerationManualVerificationIndicator\":false,\"incarcerationStatusIndicator\":false,\"useSelfAttestedIncarceration\":true,\"incarcerationPendingDispositionIndicator\":false,\"incarcerationAsAttestedIndicator\":false},\"infantIndicator\":false,\"childSupportExpense\":null,\"expeditedIncome\":{\"irsReportedAnnualIncome\":null,\"expectedAnnualIncomeUnknownIndicator\":false,\"irsIncomeManualVerificationIndicator\":false,\"expectedAnnualIncome\":null},\"migrantFarmWorker\":true},{\"detailedIncome\":{\"partnershipAndSwarpIncomeIncomeProof\":\"VERIFIED\",\"capitalGains\":{\"annualNetCapitalGains\":\"\"},\"unemploymentBenefit\":{\"incomeFrequency\":\"SelectFrequency\",\"stateGovernmentName\":\"\",\"benefitAmount\":\"\",\"unemploymentDate\":\"\"},\"annuityIncomeProof\":\"VERIFIED\",\"scholarshipIncomeProof\":\"VERIFIED\",\"otherIncome\":{\"incomeAmount\":null,\"otherIncomeTypeDescription\":\"\"},\"receiveMAthrough1619\":true,\"taxExemptedIncomeProof\":\"VERIFIED\",\"wageIncomeProof\":\"VERIFIED\",\"dividendsIncomeProof\":\"VERIFIED\",\"farmFishingIncome\":{\"incomeFrequency\":\"SelectFrequency\",\"netIncomeAmount\":\"\"},\"rentalRoyaltyIncomeIndicator\":null,\"unemploymentBenefitIndicator\":null,\"capitalGainsIndicator\":null,\"jobIncome\":[{\"incomeFrequency\":\"SelectFrequency\",\"employerName\":\"\",\"incomeAmountBeforeTaxes\":\"\",\"workHours\":\"\"}],\"alimonyReceivedIndicator\":null,\"unemploymentCompensationIncomeProof\":\"VERIFIED\",\"selfEmploymentTax\":{\"incomeAmount\":null},\"studentLoanInterest\":{\"incomeAmount\":null},\"selfEmploymentIncome\":{\"monthlyNetIncome\":\"\",\"typeOfWork\":\"\"},\"businessIncomeProof\":\"VERIFIED\",\"sellingBusinessProperty\":{\"incomeAmount\":null},\"retirementOrPension\":{\"taxableAmount\":\"\",\"incomeFrequency\":\"SelectFrequency\"},\"royaltiesIncomeProof\":\"VERIFIED\",\"farmFishingIncomeIndictor\":null,\"investmentIncome\":{\"incomeFrequency\":\"SelectFrequency\",\"incomeAmount\":\"\"},\"farmIncom\":{\"incomeAmount\":null},\"deductions\":{\"alimonyDeductionAmount\":\"\",\"otherDeductionAmount\":\"\",\"alimonyDeductionFrequency\":\"SelectFrequency\",\"studentLoanDeductionFrequency\":\"SelectFrequency\",\"deductionFrequency\":\"MONTHLY\",\"studentLoanDeductionAmount\":\"\",\"deductionType\":[\"ALIMONY\",\"STUDENT_LOAN_INTEREST\",\"\"],\"otherDeductionFrequency\":\"SelectFrequency\",\"deductionAmount\":null},\"rentalRoyaltyIncome\":{\"incomeFrequency\":\"SelectFrequency\",\"netIncomeAmount\":\"\"},\"receiveMAthroughSSI\":true,\"foreignEarnedIncomeProof\":\"VERIFIED\",\"socialSecurityBenefit\":{\"incomeFrequency\":\"SelectFrequency\",\"benefitAmount\":\"\"},\"foreignEarnedIncome\":{\"incomeAmount\":null},\"childNaTribe\":\"VERIFIED\",\"sellingBusinessPropertyIncomeProof\":\"VERIFIED\",\"alimonyReceived\":{\"amountReceived\":\"\",\"incomeFrequency\":\"SelectFrequency\"},\"selfEmploymentTaxProof\":\"VERIFIED\",\"partnershipsCorporationsIncome\":{\"incomeAmount\":null},\"studentLoanInterestProof\":\"VERIFIED\",\"farmIncomeProof\":\"VERIFIED\",\"retirementOrPensionIndicator\":null,\"investmentIncomeIndicator\":null,\"rentalRealEstateIncome\":{\"incomeAmount\":null},\"otherIncomeIndicator\":null,\"pensionIncomeProof\":\"VERIFIED\",\"socialSecurityBenefitProof\":\"VERIFIED\",\"jobIncomeIndicator\":null,\"businessIncome\":{\"incomeAmount\":null},\"taxableInterestIncomeProof\":\"VERIFIED\",\"selfEmploymentIncomeIndicator\":null,\"rentalRealEstateIncomeProof\":\"VERIFIED\",\"discrepancies\":{\"hasWageOrSalaryBeenCutIndicator\":false,\"explanationForJobIncomeDiscrepancy\":\"There is a reason\",\"didPersonEverWorkForEmployerIndicator\":false,\"stopWorkingAtEmployerIndicator\":false,\"hasHoursDecreasedWithEmployerIndicator\":false,\"explanationForDependantDiscrepancy\":\"There is a reason\",\"seasonalWorkerIndicator\":false,\"hasChangedJobsIndicator\":false}},\"dateOfBirth\":\"Aug 08, 2000 00:00:00 AM\",\"parentCaretakerRelatives\":{\"relationship\":\"\",\"dateOfBirth\":\"\",\"mainCaretakerOfChildIndicator\":null,\"name\":{\"middleName\":\"\",\"lastName\":\"\",\"firstName\":\"\",\"suffix\":\"\"},\"liveWithAdoptiveParentsIndicator\":true,\"personId\":[],\"anotherChildIndicator\":false},\"specialCircumstances\":{\"ageWhenLeftFosterCare\":50,\"everInFosterCareIndicator\":false,\"fosterChild\":false,\"numberBabiesExpectedInPregnancy\":0,\"pregnantIndicator\":false,\"gettingHealthCareThroughStateMedicaidIndicator\":true,\"fosterCareState\":\"\",\"tribalManualVerificationIndicator\":true,\"americanIndianAlaskaNativeIndicator\":false},\"livesWithHouseholdContactIndicator\":false,\"socialSecurityCard\":{\"middleNameOnSSNCard\":\"\",\"deathManualVerificationIndicator\":false,\"socialSecurityNumber\":\"500-56-6778\",\"suffixOnSSNCard\":\"0\",\"individualMandateExceptionIndicator\":null,\"ssnManualVerificationIndicator\":true,\"deathIndicatorAsAttested\":false,\"nameSameOnSSNCardIndicator\":true,\"deathIndicator\":false,\"firstNameOnSSNCard\":\"\",\"socialSecurityCardHolderIndicator\":true,\"useSelfAttestedDeathIndicator\":true,\"lastNameOnSSNCard\":\"\",\"reasonableExplanationForNoSSN\":\"0\"},\"strikerIndicator\":false,\"birthCertificateType\":\"NO_CRETIFICATE\",\"citizenshipImmigrationStatus\":{\"honorablyDischargedOrActiveDutyMilitaryMemberIndicator\":false,\"citizenshipStatusIndicator\":true,\"eligibleImmigrationDocumentType\":[{\"DS2019Indicator\":false,\"UnexpiredForeignPassportIndicator\":false,\"TemporaryI551StampIndicator\":false,\"I551Indicator\":false,\"MachineReadableVisaIndicator\":false,\"OtherDocumentTypeIndicator\":true,\"I94InPassportIndicator\":false,\"I571Indicator\":false,\"I766Indicator\":false,\"I20Indicator\":false,\"I94Indicator\":false,\"I797Indicator\":false,\"I327Indicator\":false}],\"naturalizationCertificateIndicator\":true,\"citizenshipManualVerificationStatus\":true,\"citizenshipAsAttestedIndicator\":true,\"eligibleImmigrationStatusIndicator\":true,\"citizenshipDocument\":{\"SEVISId\":\"\",\"foreignPassportOrDocumentNumber\":\"\",\"sevisId\":\" \",\"alienNumber\":\"\",\"nameOnDocument\":{\"middleName\":\"\",\"lastName\":\"\",\"firstName\":\"\",\"suffix\":\"\"},\"documentDescription\":\"\",\"nameSameOnDocumentIndicator\":false,\"visaNumber\":\" \",\"documentExpirationDate\":\"\",\"i94Number\":\"\",\"foreignPassportCountryOfIssuance\":\"0\",\"cardNumber\":\" \"},\"lawfulPresenceIndicator\":true,\"naturalizationCertificateAlienNumber\":\" \",\"eligibleImmigrationDocumentSelected\":\"OtherCase1\",\"otherImmigrationDocumentType\":{\"CubanHaitianEntrantIndicator\":false,\"ORREligibilityLetterIndicator\":false,\"StayOfRemovalIndicator\":false,\"ORRCertificationIndicator\":false,\"AmericanSamoanIndicator\":false,\"WithholdingOfRemovalIndicator\":false},\"naturalizedCitizenshipIndicator\":true,\"livedIntheUSSince1996Indicator\":true,\"naturalizationCertificateNaturalizationNumber\":\" \"},\"medicalExpense\":null,\"specialEnrollmentPeriod\":{\"birthOrAdoptionEventIndicator\":false},\"marriedIndicator\":false,\"bloodRelationship\":[],\"drugFellowIndicator\":false,\"livingArrangementIndicator\":false,\"taxFiler\":{\"claimingDependantsOnFTRIndicator\":false,\"taxFilerDependants\":[{\"dependantHouseholdMemeberId\":\"amp\"}],\"liveWithSpouseIndicator\":false},\"shelterAndUtilityExpense\":null,\"tobaccoUserIndicator\":false,\"dependentCareExpense\":null,\"residencyVerificationIndicator\":false,\"taxFilerDependant\":{\"taxFilerDependantIndicator\":false},\"name\":{\"middleName\":\"\",\"lastName\":\"silly\",\"firstName\":\"ykid\",\"suffix\":\"Suffix\"},\"ethnicityAndRace\":{\"ethnicities\":{\"ethnicity\":[]},\"hispanicLatinoSpanishOriginIndicator\":true,\"races\":{\"race\":[]}},\"personId\":2,\"gender\":\"female\",\"SSIIndicator\":false,\"IPVIndicator\":false,\"householdContactIndicator\":true,\"americanIndianAlaskaNative\":{\"tribeName\":\"AmericanIndian\",\"state\":\"\",\"memberOfFederallyRecognizedTribeIndicator\":false},\"householdContact\":{\"homeAddressIndicator\":false,\"phone\":{\"phoneExtension\":\"\",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"homeAddress\":{\"postalCode\":\"\",\"county\":\"\",\"streetAddress1\":\"\",\"streetAddress2\":\" \",\"state\":\"\",\"city\":\"\"},\"stateResidencyProof\":\"\",\"otherPhone\":{\"phoneExtension\":\" \",\"phoneNumber\":\" \",\"phoneType\":\"HOME\"},\"contactPreferences\":{\"emailAddress\":\"\",\"preferredSpokenLanguage\":\"\",\"preferredContactMethod\":\"\",\"preferredWrittenLanguage\":\"\"},\"mailingAddress\":{\"postalCode\":\"\",\"county\":\"\",\"streetAddress1\":\"\",\"streetAddress2\":\" \",\"state\":\"\",\"city\":\"\"},\"mailingAddressSameAsHomeAddressIndicator\":true},\"PIDVerificationIndicator\":false,\"applicantGuid\":\"e7b36757-b735-4174-y358-c53c0b9bc67c\",\"PIDIndicator\":false,\"applyingForCoverageIndicator\":true,\"planToFileFTRJontlyIndicator\":false,\"livesAtOtherAddressIndicator\":\"no\",\"healthCoverage\":{\"chpInsurance\":{\"reasonInsuranceEndedOther\":\" \",\"insuranceEndedDuringWaitingPeriodIndicator\":false},\"haveBeenUninsuredInLast6MonthsIndicator\":null,\"currentEmployer\":[{\"employer\":{\"phone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"employerContactPersonName\":\" \",\"employerContactEmailAddress\":\" \"},\"currentEmployerInsurance\":{\"currentLowestCostSelfOnlyPlanName\":\" \",\"willBeEnrolledInEmployerPlanIndicator\":false,\"memberEmployerPlanStartDate\":\"\",\"willBeEnrolledInEmployerPlanDate\":\"\",\"comingPlanMeetsMinimumStandardIndicator\":false,\"memberPlansToDropEmployerPlanIndicator\":true,\"employerWillOfferPlanIndicator\":true,\"expectedChangesToEmployerCoverageIndicator\":true,\"employerCoverageEndingDate\":\"\",\"memberPlansToEnrollInEmployerPlanIndicator\":true,\"employerPlanStartDate\":\"\",\"memberEmployerPlanEndingDate\":\"\",\"comingLowestCostSelfOnlyPlanName\":\" \",\"currentPlanMeetsMinimumStandardIndicator\":false,\"employerWillNotOfferCoverageIndicator\":false,\"isCurrentlyEnrolledInEmployerPlanIndicator\":false}}],\"formerEmployer\":[{\"phone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"CELL\"},\"employerName\":\" \",\"employerIdentificationNumber\":\" \",\"employerContactPersonName\":\" \",\"address\":{\"postalCode\":\"\",\"streetAddress1\":\" \",\"streetAddress2\":\" \",\"state\":\"CA\",\"city\":\" \"},\"employerContactPhone\":{\"phoneExtension\":\" \",\"phoneNumber\":\"\",\"phoneType\":\"HOME\"},\"employerContactEmailAddress\":\" \"}],\"primaryCarePhysicianName\":\" \",\"otherInsuranceIndicator\":false,\"currentlyEnrolledInCobraIndicator\":false,\"currentlyHasHealthInsuranceIndicator\":false,\"employerWillOfferInsuranceIndicator\":null,\"currentlyEnrolledInRetireePlanIndicator\":false,\"currentOtherInsurance\":{\"peaceCorpsIndicator\":false,\"otherStateOrFederalProgramType\":\" \",\"tricareEligibleIndicator\":false,\"otherStateOrFederalProgramName\":\" \",\"medicareEligibleIndicator\":false,\"otherStateOrFederalProgramIndicator\":false},\"currentlyEnrolledInVeteransProgramIndicator\":false,\"hasPrimaryCarePhysicianIndicator\":false,\"employerWillOfferInsuranceCoverageStartDate\":\"\",\"willBeEnrolledInCobraIndicator\":false,\"willBeEnrolledInVeteransProgramIndicator\":false,\"medicaidInsurance\":{\"requestHelpPayingMedicalBillsLast3MonthsIndicator\":false},\"isOfferedHealthCoverageThroughJobIndicator\":null,\"willBeEnrolledInRetireePlanIndicator\":false},\"planToFileFTRIndicator\":false,\"otherAddress\":{\"address\":{\"postalCode\":\"\",\"county\":\"\",\"streetAddress1\":\"\",\"streetAddress2\":\" \",\"state\":\"\",\"city\":\"\"},\"livingOutsideofStateTemporarilyIndicator\":\"no\"},\"disabilityIndicator\":false,\"heatingCoolingindicator\":false,\"incarcerationStatus\":{\"incarcerationManualVerificationIndicator\":false,\"incarcerationStatusIndicator\":false,\"useSelfAttestedIncarceration\":true,\"incarcerationPendingDispositionIndicator\":false,\"incarcerationAsAttestedIndicator\":false},\"infantIndicator\":false,\"childSupportExpense\":null,\"expeditedIncome\":{\"irsReportedAnnualIncome\":null,\"expectedAnnualIncomeUnknownIndicator\":false,\"irsIncomeManualVerificationIndicator\":false,\"expectedAnnualIncome\":null},\"migrantFarmWorker\":true}],\"houseHoldIncome\":null,\"applyingForCashBenefitsIndicator\":false}],\"applyingForhouseHold\":\"otherFamilyMember\"}}";
		String interrupt = "N";
		List<SsapApplication> application  = ssapApplicationRepository.findByCaseNumber(caseNumber);
		if(application.isEmpty()){
			lOGGER.info(" no application found for this case number");
			return "N";
		}
		String ssapJSON = application.get(0).getApplicationData(); 
		String eventId = "";
		JSONObject singleStreamlinedApplication = (JSONObject)JsonPath.read(ssapJSON,"singleStreamlinedApplication");

		//Is Seeking financial coverage
/*		if(!AccountTransferUtil.checkBoolean(singleStreamlinedApplication.get("applyingForFinancialAssistanceIndicator"))){
				return "N";//not seeking financial coverage. 
			}else{

				String ssapApplicationID =(String)singleStreamlinedApplication.get("ssapApplicationId");
				if(StringUtils.isBlank(ssapApplicationID)){
					lOGGER.debug(" no ssap" + ssapJSON);
					return "N";
				}
				//get list of all the household
				List<JSONObject> houseHolds = JsonPath.read(ssapJSON,"$.singleStreamlinedApplication.taxHousehold[*].householdMember[*]");
				for(JSONObject houseHold : houseHolds){

					if((Boolean)houseHold.get("applyingForCoverageIndicator")){//individual seeking coverage

						String mecResponse = callStateMEC(houseHold,ssapApplicationID,eventId);
						lOGGER.debug("State MECs response "+ mecResponse);
						String mECFlag = (String)JsonPath.read(ssapJSON,"NonESIMECIndividualResponseType.MECVerificationCode");
						//TODO ask Linu where to get it from
						String coverageEndDate = "08/20/2014";
						if("Y".equalsIgnoreCase(mECFlag)){//MECFlag="y"
							if(hasValidCoverageEndDate(coverageEndDate)){//!coverageEndDate<60
								interrupt="Y";// individual is MEC
								continue;
							}
						}else{//MECFlag="N"; terminate MEC Check;household is not MEC
							interrupt="N";
							return interrupt;
						}
					}else{
						continue;
					}
				}
				return interrupt;
			}*/
			return "N";//TODO remove it after testing
		}
		boolean hasValidCoverageEndDate(String coverageDateStr){
			try{
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date coverageEndDate = sdf.parse(coverageDateStr);

				Calendar today = TSCalendar.getInstance();
				today.add(Calendar.DATE, 60);
				if(today.getTime().compareTo(coverageEndDate)<1){
					return true;
				}else{
					return false;
				}
			}catch(Exception e){
				lOGGER.error(" exception while converting coverageDateStr: "+ coverageDateStr,e);
				return false;
			}
		}
		boolean hasValidRecertificationDate(String recertificationDateStr){
			try{
				SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
				Date recertificationDate = sdf.parse(recertificationDateStr);

				Calendar today = TSCalendar.getInstance();
				today.add(Calendar.DATE, 60);
				return (today.getTime().compareTo(recertificationDate)>1);
			}catch(Exception e){
				lOGGER.error(" exception while converting coverageDateStr: "+ recertificationDateStr,e);
				return false;
			}
		}
		private String callStateMEC(JSONObject houseHold, String ssapApplicationID, String applicationEvenID){
			String mECFlag="Y";
			String dob = (String)houseHold.get("dateOfBirth");
			String firstName = (String)((JSONObject)houseHold.get("name")).get("firstName");
			String middleName = (String)((JSONObject)houseHold.get("name")).get("middleName");
			String lastName = (String)((JSONObject)houseHold.get("name")).get("lastName");
			String suffix = (String)((JSONObject)houseHold.get("name")).get("suffix");
			String ssn = (String)((JSONObject)houseHold.get("socialSecurityCard")).get("socialSecurityNumber");
			String gender = (String)houseHold.get("gender");

			String insurancePolicyEffectiveDate ="";
			String insurancePolicyExpirationDate = "";

			try {
				String ip = InetAddress.getLocalHost().getHostName();
				SsapApplicationEvent ssapApplicationEvent= ssapApplicationEventRepository.findEventForOpenEnrollment(new Long(ssapApplicationID));
				if(ssapApplicationEvent != null){
					insurancePolicyEffectiveDate = new SimpleDateFormat("MM/dd/yyyy").format(ssapApplicationEvent.getCoverageStartDate());
					insurancePolicyExpirationDate =new SimpleDateFormat("MM/dd/yyyy").format(ssapApplicationEvent.getCoverageEndDate());
				}else{
					return "N";
				}
				String mecRequestJSON="{\"applicationId\":%S, \"clientIp\":\"%S\", \"operation\":\"\", \"payload\":{ \"taxHousehold\":"
						+ "[{\"householdMember\":[{\"dateOfBirth\":\"%S\",\"name\":{\"firstName\":\"%S\",\"middleName\":\"%S\","
						+ "\"lastName\":\"%S\",\"personNameSuffixText\":\"%S\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"%S\"},"
						+ "\"gender\":\"%S\",\"insurance\":{\"insurancePolicyEffectiveDate\":\"%S\",\"insurancePolicyExpirationDate\":\"%S\"},"
						+ "\"organizationCode\":\"%S\"}]}]}}";
				String request = String.format(mecRequestJSON, ssapApplicationID,ip,dob,firstName,middleName,lastName,suffix,ssn,gender,insurancePolicyEffectiveDate,insurancePolicyExpirationDate,"GHIX");
				HttpHeaders headers = new HttpHeaders();
				headers.setContentType(MediaType.TEXT_PLAIN);
				HttpEntity<String> requestEntity = new HttpEntity<String>(request, headers);

				ResponseEntity<String> response = restTemplate.exchange(mECURL, HttpMethod.POST, requestEntity, String.class); 
				return response.getBody();
			} catch (Exception e) {
				lOGGER.error(e);
				return "N";
			}
		}
	}
