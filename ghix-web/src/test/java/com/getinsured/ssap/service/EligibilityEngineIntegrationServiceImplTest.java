package com.getinsured.ssap.service;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.eligibility.model.EligibilityProgram;
import com.getinsured.hix.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.enums.SsapApplicationTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.multitenant.resolver.filter.TenantContextHolder;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.erp.org.nmhix.ssa.person.GenderEnum;
import com.getinsured.iex.ssap.*;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.ssap.model.eligibility.EligibilityRequest;
import com.getinsured.ssap.model.eligibility.EligibilityResponse;
import com.getinsured.ssap.model.eligibility.HouseholdMemberRequest;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;
import com.getinsured.ssap.model.eligibility.ResponseStatus;
import com.getinsured.ssap.model.eligibility.RunMode;
import com.getinsured.ssap.repository.EligibilityProgramRepository;
import com.getinsured.ssap.util.BloodRelationshipCode;
import com.getinsured.ssap.util.FamilyBuilder;
import com.getinsured.ssap.util.HouseholdUtil;
import com.getinsured.ssap.util.JsonUtil;
import com.getinsured.ssap.util.SSLUtil;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;


/**
 * Unit test for EligibilityEngineIntegrationServiceImpl.
 *
 * @author Yevgen Golubenko
 * @since 4/22/19
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({DynamicPropertiesUtil.class, TenantContextHolder.class})
@PowerMockIgnore({"javax.net.ssl.*", "javax.security.*"})
public class EligibilityEngineIntegrationServiceImplTest {
  private static final Logger log = LoggerFactory.getLogger(EligibilityEngineIntegrationServiceImplTest.class);
  private static final String TEST_SERVER_URL = "https://nv2dev.eng.vimo.com/ghix-eligibility-engine/";

  @Mock
  private Properties configProp;

  @Mock
  private GhixRestTemplate ghixRestTemplate;

  @Mock
  private SsapApplicantRepository ssapApplicantRepository;

  @Mock
  private EligibilityProgramRepository eligibilityProgramRepository;

  @Mock
  private SsapApplicationRepository ssapApplicationRepository;

  @Mock
  private HubIntegration hubIntegration;

  @Mock
  private SsapNoticeService ssapNoticeService;

  @Mock
  private EventCreationService eventCreationService;
  
  @Mock
  private UserServiceImpl userService;

  @Mock
  private AptcHistoryRepository aptcHistoryRepository;

  private ExchangeEligibilityDeterminationService exchangeEligibilityDeterminationService;

  private ObjectMapper om = new ObjectMapper();

  private EligibilityEngineIntegrationServiceImpl eligibilityEngineIntegrationService;

  @Before
  public void setUp() throws Exception {
    MockitoAnnotations.initMocks(this);
    PowerMockito.mockStatic(DynamicPropertiesUtil.class);
    SSLUtil.turnOffSslChecking();

    when(ghixRestTemplate.getForObject(anyString(), any(Class.class))).thenAnswer(invocation -> {
      RestTemplate restTemplate = restTemplate();
      final String url = invocation.getArgument(0, String.class);
      final Class clazz = (Class) invocation.getArguments()[1];
      return restTemplate.getForObject(url, clazz);
    });

    when(ghixRestTemplate.postForObject(anyString(), any(EligibilityRequest.class), anyObject())).thenAnswer(invocation -> {
      RestTemplate restTemplate = new RestTemplate();

      final String url = invocation.getArgument(0, String.class);
      final EligibilityRequest request = invocation.getArgument(1, EligibilityRequest.class);
      final Class response = invocation.getArgument(2, Class.class);

      return restTemplate.postForObject(url, request, response);
    });


    when(configProp.getProperty("endPointEligibilityDetermination")).thenReturn(TEST_SERVER_URL);

    when(ssapApplicantRepository.findByApplicantGuid(anyString())).thenAnswer(invocation -> {

      String requestedGuid = invocation.getArgument(0, String.class);

      SsapApplicant applicant = new SsapApplicant();
      applicant.setId(new Random().nextLong());
      applicant.setApplicantGuid(requestedGuid);

      return applicant;
    });

    when(eligibilityProgramRepository.save(any(EligibilityProgram.class))).thenAnswer(invocation -> {
      EligibilityProgram eligibilityProgram = invocation.getArgument(0, EligibilityProgram.class);
      log.info("Saving eligibility program record into database: {}", eligibilityProgram);
      eligibilityProgram.setId(new Random().nextLong());
      return eligibilityProgram;
    });

    when(ssapApplicationRepository.save(any(SsapApplication.class))).thenAnswer(invocation -> {
      SsapApplication application = invocation.getArgument(0, SsapApplication.class);
      log.info("Saving SsapApplication: {}", application);
      return application;
    });

    when(ssapApplicationRepository.findOne(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      SsapApplication application = new SsapApplication();
      log.info("Returning empty SsapApplication for id: {}", applicationId);
      return application;
    });

    when(ssapApplicationRepository.getOne(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      SsapApplication application = getSampleApplication(applicationId);
      return application;
    });

    when(ssapApplicantRepository.save(any(SsapApplicant.class))).thenAnswer(invocation -> {
      SsapApplicant applicant = invocation.getArgument(0, SsapApplicant.class);
      log.info("Saving SSAP applicant: {}", applicant);
      return applicant;
    });

    when(ssapApplicantRepository.findBySsapApplicationId(anyLong())).thenAnswer(invocation -> {
      Long applicationId = invocation.getArgument(0, Long.class);
      List<SsapApplicant> applicantList = new ArrayList<>();
      log.info("{Mock ssapApplicantRepository.findBySsapApplicationId} Returning empty applicant list for ssap id: {}", applicationId);
      return applicantList;
    });


    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    // make OEP window: 1 month - TODAY + 1 month
    when(DynamicPropertiesUtil.getPropertyValue(any(IEXConfiguration.IEXConfigurationEnum.class))).then(invocation -> {
      final IEXConfiguration.IEXConfigurationEnum constant = invocation.getArgument(0, IEXConfiguration.IEXConfigurationEnum.class);
      if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE) {
        calendar.roll(Calendar.MONTH, -1);
        log.info("[base] Returning OEP start date: " + calendar.getTime());
      } else if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) {
        calendar.roll(Calendar.MONTH, 2);
        log.info("[base] Returning OEP end date: " + calendar.getTime());
      }

      return new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT).format(calendar.getTime());
    });

    exchangeEligibilityDeterminationService = new ExchangeEligibilityDeterminationService();
    om.enable(SerializationFeature.INDENT_OUTPUT);

    eligibilityEngineIntegrationService = new EligibilityEngineIntegrationServiceImpl(ghixRestTemplate,
        configProp, ssapApplicantRepository, eligibilityProgramRepository,
        ssapApplicationRepository, exchangeEligibilityDeterminationService,
        hubIntegration, eventCreationService, ssapNoticeService, userService, aptcHistoryRepository);
    eligibilityEngineIntegrationService.postConstruct();
  }

  @After
  public void tearDown() throws Exception {
  }

  private SsapApplication getSampleApplication(long applicationId) {
    SingleStreamlinedApplication application = sampleApplication(applicationId);

    SsapApplication ssapApplication = new SsapApplication();
    ssapApplication.setId(applicationId);

    JsonUtil.setApplicationData(ssapApplication, application);

    ssapApplication.setFinancialAssistanceFlag(application.getApplyingForFinancialAssistanceIndicator() == Boolean.TRUE ? "Y" : "N");

    return ssapApplication;
  }

  private SingleStreamlinedApplication sampleApplication(long applicationId) {
    SingleStreamlinedApplication app = new SingleStreamlinedApplication();
    app.setSsapApplicationId(String.valueOf(applicationId));
    app.setApplicationType(SsapApplicationTypeEnum.OE.name());
    List<TaxHousehold> taxHouseholds = new ArrayList<>(1);
    TaxHousehold taxHousehold = null;

    switch(Math.toIntExact(applicationId)) {
      case 1:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_H_Household());
        taxHouseholds.add(taxHousehold);
        break;
      case 2:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildTestCase_E_Household());
        taxHouseholds.add(taxHousehold);
        break;
      case 3:
        taxHousehold = new TaxHousehold();
        taxHousehold.setHouseholdMember(FamilyBuilder.buildHusbandWifeAndTwoKidsMarriedFilingJointly());
        taxHouseholds.add(taxHousehold);
        break;
      default:
        taxHouseholds = sampleTaxHousehold();
        break;
    }

    app.setTaxHousehold(taxHouseholds);
    app.setCoverageYear(2019);
    app.setPrimaryTaxFilerPersonId(1);
    app.setJointTaxFilerSpousePersonId(2);
    app.setRidpVerified(true);
    app.setApplyingForFinancialAssistanceIndicator(true);

    return app;
  }

  private List<TaxHousehold> sampleTaxHousehold() {
    List<TaxHousehold> taxHouseholds = new ArrayList<>(1);

    TaxHousehold tx1 = new TaxHousehold();

    HouseholdContact hhc = new HouseholdContact();
    HomeAddress homeAddress = new HomeAddress();
    homeAddress.setCity("Las Vegas");
    homeAddress.setState("NV");
    homeAddress.setPostalCode("89144");
    homeAddress.setStreetAddress1("9851 Canyon Run Dr");
    homeAddress.setCounty("Clark");
    homeAddress.setCountyCode("32003");
    hhc.setHomeAddress(homeAddress);

    // ------------------- HH 1 -------------------
    HouseholdMember hh1 = new HouseholdMember();

    hh1.setHouseholdContact(hhc);
    hh1.setPersonId(1);
    hh1.setApplicantGuid("10000001");

    Name name = new Name();
    name.setFirstName("Mark"); // members->0->firstName
    name.setLastName("William"); // members->0->lastName
    hh1.setApplyingForCoverageIndicator(true); // // members->0->seekingCoverage
    hh1.setDateOfBirth(getDate(1963, 1, 1)); // members->0->dateOfBirth
    hh1.setName(name);
    hh1.setGender(GenderEnum.MALE.name()); // members->0->gender
    hh1.setTobaccoUserIndicator(false); // members->0->tobaccoUser

    CitizenshipImmigrationStatus citizenshipImmigrationStatus = new CitizenshipImmigrationStatus();
    citizenshipImmigrationStatus.setCitizenshipStatusIndicator(true); // members->0->usCitizen
    citizenshipImmigrationStatus.setLivedIntheUSSince1996Indicator(false); // // members->0->firstName
    citizenshipImmigrationStatus.setLawfulPresenceIndicator(false); // members->0->legalAlien
    // citizenshipImmigrationStatus.setNaturalizationCertificateIndicator(false); // // members->0->legalAlien?
    hh1.setCitizenshipImmigrationStatus(citizenshipImmigrationStatus);

    IncarcerationStatus incarcerationStatus = new IncarcerationStatus();
    incarcerationStatus.setIncarcerationStatusIndicator(false); // members->0->inCarcerated TODO: Fix name for request
    hh1.setIncarcerationStatus(incarcerationStatus);

    hh1.setLongTermCare(true);
    hh1.setBlindOrDisabled(true);

    hh1.setDisabilityIndicator(hh1.isLongTermCare() || hh1.isBlindOrDisabled()); // members->0->disabled TODO: BlindOrDisabled also there

    hh1.setMedicaidChipDenial(false); // members->0->deniedMedicaidChip

    SpecialCircumstances specialCircumstances = new SpecialCircumstances();
    specialCircumstances.setAgeWhenLeftFosterCare(18);
    specialCircumstances.setPregnantIndicator(false); // // members->0->pregnant
    specialCircumstances.setNumberBabiesExpectedInPregnancy(0); // // members->0->noOfPregnancies
    hh1.setSpecialCircumstances(specialCircumstances);


    BloodRelationship bloodRelationship_1_0 = new BloodRelationship();
    bloodRelationship_1_0.setRelation("01"); // SPOUSE
    bloodRelationship_1_0.setIndividualPersonId("2");
    bloodRelationship_1_0.setRelatedPersonId("1");
    bloodRelationship_1_0.setTextDependency(false);

    BloodRelationship bloodRelationship_1_1 = new BloodRelationship();
    bloodRelationship_1_1.setRelation("18"); // SELF
    bloodRelationship_1_1.setIndividualPersonId("1");
    bloodRelationship_1_1.setRelatedPersonId("2");
    bloodRelationship_1_1.setTextDependency(false);

    BloodRelationship bloodRelationship_1_2 = new BloodRelationship();
    bloodRelationship_1_2.setRelation("03"); // PARENT OF CHILD
    bloodRelationship_1_2.setIndividualPersonId("1");
    bloodRelationship_1_2.setRelatedPersonId("3");
    bloodRelationship_1_2.setTextDependency(true);

    List<BloodRelationship> bloodRelationships1 = new ArrayList<>(
        Arrays.asList(bloodRelationship_1_0, bloodRelationship_1_1, bloodRelationship_1_2)
    );

    hh1.setBloodRelationship(bloodRelationships1);

    // ------------------- HH 2 -------------------
    HouseholdMember hh2 = new HouseholdMember();
    hh2.setPersonId(2);
    hh1.setApplicantGuid("10000002");
    hh2.setLivesWithHouseholdContactIndicator(true);
    hh2.setHouseholdContact(hhc);

    Name name2 = new Name();
    name2.setFirstName("Susan"); // members->0->firstName
    name2.setLastName("William"); // members->0->lastName
    hh2.setName(name2);

    hh2.setApplyingForCoverageIndicator(true); // // members->0->seekingCoverage
    hh2.setDateOfBirth(getDate(1967, 1, 1)); // members->0->dateOfBirth

    hh2.setGender(GenderEnum.FEMALE.name()); // members->0->gender
    hh2.setTobaccoUserIndicator(false); // members->0->tobaccoUser

    CitizenshipImmigrationStatus citizenshipImmigrationStatus2 = new CitizenshipImmigrationStatus();
    citizenshipImmigrationStatus2.setCitizenshipStatusIndicator(true); // members->0->usCitizen
    citizenshipImmigrationStatus2.setLivedIntheUSSince1996Indicator(false); // // members->0->firstName
    citizenshipImmigrationStatus2.setLawfulPresenceIndicator(false); // members->0->legalAlien
    // citizenshipImmigrationStatus.setNaturalizationCertificateIndicator(false); // // members->0->legalAlien?
    hh2.setCitizenshipImmigrationStatus(citizenshipImmigrationStatus2);

    IncarcerationStatus incarcerationStatus2 = new IncarcerationStatus();
    incarcerationStatus2.setIncarcerationStatusIndicator(false); // members->0->inCarcerated TODO: Fix name
    hh2.setIncarcerationStatus(incarcerationStatus);

    hh2.setLongTermCare(false);
    hh2.setBlindOrDisabled(false);
    hh2.setDisabilityIndicator(hh2.isLongTermCare() || hh2.isBlindOrDisabled()); // members->0->disabled TODO: BlindOrDisabled also there

    hh2.setMedicaidChipDenial(false); // members->0->deniedMedicaidChip

    SpecialCircumstances specialCircumstances2 = new SpecialCircumstances();
    specialCircumstances2.setAgeWhenLeftFosterCare(18);
    specialCircumstances2.setPregnantIndicator(false); // // members->0->pregnant
    specialCircumstances2.setNumberBabiesExpectedInPregnancy(0); // // members->0->noOfPregnancies
    hh2.setSpecialCircumstances(specialCircumstances2);

    BloodRelationship bloodRelationship_2_0 = new BloodRelationship();
    bloodRelationship_2_0.setRelation("01"); // SPOUSE
    bloodRelationship_2_0.setIndividualPersonId("2");
    bloodRelationship_2_0.setRelatedPersonId("1");
    bloodRelationship_2_0.setTextDependency(false);

    BloodRelationship bloodRelationship_2_1 = new BloodRelationship();
    bloodRelationship_2_1.setRelation("18"); // SELF
    bloodRelationship_2_1.setIndividualPersonId("2");
    bloodRelationship_2_1.setRelatedPersonId("2");
    bloodRelationship_2_1.setTextDependency(false);

    BloodRelationship bloodRelationship_2_2 = new BloodRelationship();
    bloodRelationship_2_2.setRelation("03"); // PARENT OF CHILD
    bloodRelationship_2_2.setIndividualPersonId("2");
    bloodRelationship_2_2.setRelatedPersonId("3");
    bloodRelationship_2_2.setTextDependency(true);

    List<BloodRelationship> bloodRelationships2 = new ArrayList<>(
        Arrays.asList(bloodRelationship_2_0, bloodRelationship_2_1, bloodRelationship_2_2)
    );

    hh2.setBloodRelationship(bloodRelationships2);

    // ------------------- HH 3 -------------------
    HouseholdMember hh3 = new HouseholdMember();
    hh3.setPersonId(3);
    hh1.setApplicantGuid("10000003");
    hh3.setLivesWithHouseholdContactIndicator(true);
    hh3.setHouseholdContact(hhc);

    Name name3 = new Name();
    name3.setFirstName("Reva"); // members->0->firstName
    name3.setLastName("William"); // members->0->lastName
    hh3.setName(name3);

    hh3.setApplyingForCoverageIndicator(true); // // members->0->seekingCoverage
    hh3.setDateOfBirth(getDate(2014, 1, 1)); // members->0->dateOfBirth

    hh3.setGender(GenderEnum.MALE.name()); // members->0->gender
    hh3.setTobaccoUserIndicator(false); // members->0->tobaccoUser

    CitizenshipImmigrationStatus citizenshipImmigrationStatus3 = new CitizenshipImmigrationStatus();
    citizenshipImmigrationStatus3.setCitizenshipStatusIndicator(true); // members->0->usCitizen
    citizenshipImmigrationStatus3.setLivedIntheUSSince1996Indicator(false); // // members->0->firstName
    citizenshipImmigrationStatus3.setLawfulPresenceIndicator(false); // members->0->legalAlien
    // citizenshipImmigrationStatus.setNaturalizationCertificateIndicator(false); // // members->0->legalAlien?
    hh3.setCitizenshipImmigrationStatus(citizenshipImmigrationStatus3);

    IncarcerationStatus incarcerationStatus3 = new IncarcerationStatus();
    incarcerationStatus3.setIncarcerationStatusIndicator(false); // members->0->inCarcerated TODO: Fix name
    hh3.setIncarcerationStatus(incarcerationStatus);

    hh3.setLongTermCare(false);
    hh3.setBlindOrDisabled(false);
    hh3.setDisabilityIndicator(hh3.isLongTermCare() || hh3.isBlindOrDisabled()); // members->0->disabled TODO: BlindOrDisabled also there

    hh3.setMedicaidChipDenial(false); // members->0->deniedMedicaidChip

    SpecialCircumstances specialCircumstances3 = new SpecialCircumstances();
    specialCircumstances3.setAgeWhenLeftFosterCare(18);
    specialCircumstances3.setPregnantIndicator(false); // // members->0->pregnant
    specialCircumstances3.setNumberBabiesExpectedInPregnancy(0); // // members->0->noOfPregnancies
    hh3.setSpecialCircumstances(specialCircumstances3);

    BloodRelationship bloodRelationship_3_0 = new BloodRelationship();
    bloodRelationship_3_0.setRelation("18"); // SELF
    bloodRelationship_3_0.setIndividualPersonId("3");
    bloodRelationship_3_0.setRelatedPersonId("3");
    bloodRelationship_3_0.setTextDependency(false);

    BloodRelationship bloodRelationship_3_1 = new BloodRelationship();
    bloodRelationship_3_1.setRelation("19"); // CHILD
    bloodRelationship_3_1.setIndividualPersonId("3");
    bloodRelationship_3_1.setRelatedPersonId("1");
    bloodRelationship_3_1.setTextDependency(false);

    BloodRelationship bloodRelationship_3_2 = new BloodRelationship();
    bloodRelationship_3_2.setRelation("19"); // CHILD
    bloodRelationship_3_2.setIndividualPersonId("3");
    bloodRelationship_3_2.setRelatedPersonId("2");
    bloodRelationship_3_2.setTextDependency(false);

    List<BloodRelationship> bloodRelationships3 = new ArrayList<>(
        Arrays.asList(bloodRelationship_3_0, bloodRelationship_3_1, bloodRelationship_3_2)
    );

    hh3.setBloodRelationship(bloodRelationships3);

    tx1.setHouseholdMember(Arrays.asList(hh1, hh2, hh3));
    taxHouseholds.add(tx1);
    return taxHouseholds;
  }

  private Date getDate(int year, int month, int day) {
    final ZoneId zoneId = ZoneId.of(ZoneId.SHORT_IDS.get("PST"));
    final LocalDate ld = LocalDate.of(year, month, day);
    return Date.from(ld.atStartOfDay(zoneId).toInstant());
  }

  private RestTemplate restTemplate() {
    RestTemplate restTemplate = new RestTemplate();
    return restTemplate;
  }

  @Test
  public void getEligibilityRequestFromApplication_ssap_4_tax_filers() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("sample-ssap-4-tax-filers.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Household size should be 1", 1, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Household relationship to primary should be SELF", BloodRelationshipCode.SELF, eligibilityRequest.getMembers().get(0).getRelationshipToPrimary());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix_117279() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117279.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertTrue("Household size should be > 0",  eligibilityRequest.getHouseholdSize() > 0);
    Assert.assertEquals("Household relationship to primary should be SELF", BloodRelationshipCode.SELF,
      eligibilityRequest.getMembers().get(0).getRelationshipToPrimary());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix_117569() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117569.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Household size should be 2", 2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Household relationship to primary should be SELF", BloodRelationshipCode.SELF,
      eligibilityRequest.getMembers().get(0).getRelationshipToPrimary());
  }

  @Test
  public void getEligibilityRequestFromApplication_hangingFruits() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117570_hanging_fruits.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    // All live together at the same address.
    // Jane Doe PTF                           => QHP + APTC
    // Jim Doe is domestic partner of Jane    => QHP - APTC
    // Baby Doe is child of Jane Doe          => QHP + APTC
    //                                        =  Jim Doe: QHP + APTC
    Assert.assertNotNull(eligibilityRequest);
  }

  @Test
  public void getEligibilityRequestFromApplication_hix117570() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117570-2.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Household size should be 2", 2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Household members list should be 3", 3, eligibilityRequest.getMembers().size());
  }

  @Test
  public void getEligibilityRequestFromApplication_mfsTwoPeople_117686() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("mfs-two-ppl-117686.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Household size should be 2", 2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Household members list should be 2", 2, eligibilityRequest.getMembers().size());
  }

  @Test
  public void getEligibilityRequestFromApplication_mfsTwoPeople_117686_2() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("mfs-two-ppl-117686-2.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Household size should be 2", 2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Household members list should be 2", 2, eligibilityRequest.getMembers().size());
  }

  @Test
  public void getEligibilityRequestFromApplication_nonFinancial_oneMember() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117813.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Eligibility request should have 1 member", 1, eligibilityRequest.getMembers().size());
    Assert.assertEquals("Eligibility household size should be 1", 1, eligibilityRequest.getHouseholdSize());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix117837() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117837.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Eligibility request should have 2 member2", 2, eligibilityRequest.getMembers().size());
    Assert.assertEquals("Eligibility household size should be 2", 2, eligibilityRequest.getHouseholdSize());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix117557() {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117557.json");
    List<HouseholdMember> primaryHHMembers = HouseholdUtil.getPrimaryTaxHouseholdMembers(singleStreamlinedApplication);
//    Assert.assertEquals(3, primaryHHMembers.size());
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("Should have household size of 2", 2,eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Should have 3 members in request", 3, eligibilityRequest.getMembers().size());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix117557_2() throws Exception {
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117557-2.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    log.info("Eligibility request: {}", om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);

    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(singleStreamlinedApplication, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityResponse);
    log.info("Eligibility response: {}", om.writeValueAsString(eligibilityResponse));

    eligibilityRequest.getMembers().forEach(hhm -> {
      HouseholdMemberResponse hhmr = eligibilityResponse.getMembers().stream().filter(erm -> erm.getApplicantId() == hhm.getApplicantId()).findFirst().orElse(null);
      if(hhmr != null) {
        try {
          log.info("Request: {}, Response: {}", om.writeValueAsString(hhm), om.writeValueAsString(hhmr));
        } catch (JsonProcessingException e) {
          log.error("Json Exception: {}", e.getMessage());
        }
      }
    });
    List<HouseholdMemberRequest> memberRequests = eligibilityRequest.getMembers();

    memberRequests.forEach(mr -> {
      if(mr.getFirstName().equals("Junior") && mr.getLastName().equals("Banfield")) {
        Assert.assertEquals(3, mr.getHouseholdSizeMedicaid());
        Assert.assertEquals(BloodRelationshipCode.CHILD, mr.getRelationshipToPrimary());
      }
    });

    Assert.assertNotNull(eligibilityResponse);
    log.info("Eligibility response: {}", om.writeValueAsString(eligibilityResponse));

    eligibilityRequest.getMembers().forEach(hhm -> {
      HouseholdMemberResponse hhmr = eligibilityResponse.getMembers().stream().filter(erm -> erm.getApplicantId() == hhm.getApplicantId()).findFirst().orElse(null);
      if(hhmr != null) {
        try {
          log.info("Request: {}, Response: {}", om.writeValueAsString(hhm), om.writeValueAsString(hhmr));
        } catch (JsonProcessingException e) {
          log.error("Json Exception: {}", e.getMessage());
        }
      }
    });
    // Curtis not filing taxes, nor tax dependent and not filer dependent.
    Assert.assertEquals("Should have household size of 2", 2,eligibilityRequest.getHouseholdSize());
    Assert.assertEquals("Should have 3 members in request", 3, eligibilityRequest.getMembers().size());
  }

  @Test
  public void getEligibilityRequestFromApplication_hix117890() throws Exception {
    log.info("Testing Husband & Wife both AI/AN");
    SingleStreamlinedApplication singleStreamlinedApplication = FamilyBuilder.sampleApplicationFromJson("hix-117890.json");
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(singleStreamlinedApplication, RunMode.ONLINE);
    System.out.println("Eligibility request: " + om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);

    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(singleStreamlinedApplication, RunMode.ONLINE);
    System.out.println("Eligibility Response: " + om.writeValueAsString(eligibilityResponse));
    Assert.assertNotNull(eligibilityResponse);
  }

  @Test
  public void hix20303() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("20303.json");
    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(application, RunMode.ONLINE);

    log.info("Eligibility response: {}", om.writeValueAsString(eligibilityResponse));

    Assert.assertNotNull(eligibilityResponse);
  }

  @Test
  public void testCaseC() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("testcase-c.json");

    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    System.out.println("Eligibility request: " + om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals(TaxFilingStatus.HEAD_OF_HOUSEHOLD, eligibilityRequest.getMembers().get(0).getTaxFilingStatus());
    Assert.assertEquals(TaxFilingStatus.UNSPECIFIED, eligibilityRequest.getMembers().get(1).getTaxFilingStatus());
    Assert.assertEquals(2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals(3, eligibilityRequest.getMembers().get(1).getHouseholdSizeMedicaid());
    Assert.assertEquals(2, eligibilityRequest.getMembers().get(0).getHouseholdSizeMedicaid());

    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(application, RunMode.ONLINE);

    log.info("Eligibility response: {}", om.writeValueAsString(eligibilityResponse));

    Assert.assertNotNull(eligibilityResponse);
  }

  @Test
  public void testResetSeeksQhp() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("testcase-g.json");

    application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm->hhm.getPersonId() == 3).forEach(hhm -> {
      Assert.assertFalse(hhm.isSeeksQhp());
    });

    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm->hhm.getPersonId() == 3).forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

  }

  @Test
  public void testCaseG() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("testcase-g.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().stream().filter(hhm->hhm.getPersonId() == 3).forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    System.out.format("Eligibility request: %s%n", om.writeValueAsString(eligibilityRequest));
    Assert.assertNotNull(eligibilityRequest);

    eligibilityRequest.getMembers().forEach(hhm->{
      Assert.assertTrue(hhm.isSeeksQhp());
    });
  }

  @Test
  public void testCaseD() throws Exception {
    // https://docs.google.com/spreadsheets/d/1Ycw4hsgDon12uGfRRXcte4_NABdPwfF1/edit#gid=284924950
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("testcase-d.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals(2, eligibilityRequest.getHouseholdSize());
    // Jane Doe: 48_720 (overridden from 39,520) - Primary Tax Filer
    // Jim Doe 17_780 (overridden from 22,880)   -
    // Baby: 0                                   - dependent of Jane Doe lives with both parents (Jim is domestic partner of Jane)
    Assert.assertEquals(2, eligibilityRequest.getHouseholdSize());
    Assert.assertEquals(48_720,  eligibilityRequest.getHouseholdIncome());

    HouseholdMemberRequest janeDoe = getMemberByName(eligibilityRequest, "Jane", "Doe");
    HouseholdMemberRequest jimDoe = getMemberByName(eligibilityRequest, "Jim", "Doe");
    HouseholdMemberRequest babyDoe = getMemberByName(eligibilityRequest, "Baby", "Doe");

    Assert.assertNotNull(janeDoe);
    Assert.assertEquals(48_720, janeDoe.getHouseholdIncomeMedicaid());
    Assert.assertEquals(TaxFilingStatus.HEAD_OF_HOUSEHOLD, janeDoe.getTaxFilingStatus());
    Assert.assertEquals(BloodRelationshipCode.SELF, janeDoe.getRelationshipToPrimary());
    Assert.assertEquals(2, janeDoe.getHouseholdSizeMedicaid());
    Assert.assertTrue(janeDoe.isPrimaryHousehold());
    Assert.assertTrue(janeDoe.isSeeksQhp());

    Assert.assertNotNull(jimDoe);
    Assert.assertEquals(17_780, jimDoe.getHouseholdIncomeMedicaid());
    Assert.assertEquals(BloodRelationshipCode.OTHER, jimDoe.getRelationshipToPrimary());
    Assert.assertEquals(1, jimDoe.getHouseholdSizeMedicaid());
    Assert.assertFalse("Jim Doe should not be part of the primary tax household", jimDoe.isPrimaryHousehold());
    Assert.assertTrue(jimDoe.isSeeksQhp());

    Assert.assertNotNull(babyDoe);
    Assert.assertEquals(48_720, babyDoe.getHouseholdIncomeMedicaid());
    Assert.assertEquals(2, babyDoe.getHouseholdSizeMedicaid());
    Assert.assertEquals(TaxFilingStatus.UNSPECIFIED, babyDoe.getTaxFilingStatus());
    Assert.assertEquals(BloodRelationshipCode.CHILD, babyDoe.getRelationshipToPrimary());
    Assert.assertTrue(babyDoe.isPrimaryHousehold());
    Assert.assertTrue(babyDoe.isSeeksQhp());

    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityResponse);
    log.info("Eligibility Response for test case D: {}", om.writeValueAsString(eligibilityResponse));
    Assert.assertEquals(ResponseStatus.success, eligibilityResponse.getStatus());

    Assert.assertTrue(eligibilityResponse.isAptcEligible());
    Assert.assertNotNull(eligibilityResponse.getAptcEligibilityStartDate());
    Assert.assertNotNull(eligibilityResponse.getAptcEligibilityEndDate());
    Assert.assertEquals(3, eligibilityResponse.getMembers().size());
  }

  @Test
  public void getLowestCSRLevel() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("testcase-d.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    EligibilityResponse eligibilityResponse = eligibilityEngineIntegrationService.callEligibilityEngine(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityResponse);
    log.info("Eligibility Response for test case D: {}", om.writeValueAsString(eligibilityResponse));
    Assert.assertEquals(ResponseStatus.success, eligibilityResponse.getStatus());

    AtomicInteger idx = new AtomicInteger();
    eligibilityResponse.getMembers().forEach(hhm -> {
      if(idx.get() == 0) {
        hhm.setCsrLevel("CS5");
      } else if(idx.get() == 1){
        hhm.setCsrLevel("CS5");
      } else {
        hhm.setCsrLevel("CS2");
      }
      idx.getAndIncrement();
    });

    String csrLevel = getLowestCSRLevel2(eligibilityResponse.getMembers());
    Assert.assertNotNull("CSR Level should not be null", csrLevel);
    Assert.assertEquals("CSR Level should CS5", csrLevel, "CS5");
  }

  private String getLowestCSRLevel2(List<HouseholdMemberResponse> members) {
    List<String> memberCsrs = members.stream().map(HouseholdMemberResponse::getCsrLevel).distinct().collect(Collectors.toList());
    return Stream.of("CS1", "CS4", "CS5", "CS6", "CS3", "CS2").filter(memberCsrs::contains).findFirst().orElse(null);
  }

  @Test
  public void oePeriod_ApplicationOE() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("sample-1.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    // make OEP window: 1 month - TODAY + 1 month
    when(DynamicPropertiesUtil.getPropertyValue(any(IEXConfiguration.IEXConfigurationEnum.class))).then(invocation -> {
      final IEXConfiguration.IEXConfigurationEnum constant = invocation.getArgument(0, IEXConfiguration.IEXConfigurationEnum.class);
      if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE) {
        calendar.roll(Calendar.MONTH, -1);
        log.info("Returning OEP start date: " + calendar.getTime());
      } else if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) {
        calendar.roll(Calendar.MONTH, 2);
        log.info("Returning OEP end date: " + calendar.getTime());
      }

      return new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT).format(calendar.getTime());
    });

    log.info("application type: {}", application.getApplicationType());
    Assert.assertEquals("Initial application type should be OE", SsapApplicationTypeEnum.OE.name(), application.getApplicationType());
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("type of application should be OE", eligibilityRequest.getTypeOfApplication(), SsapApplicationTypeEnum.OE);
  }

  @Test
  public void outsideOfOePeriod_ApplicationSEP() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("sample-1.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    // make OEP window: 1 month - TODAY + 1 month
    when(DynamicPropertiesUtil.getPropertyValue(any(IEXConfiguration.IEXConfigurationEnum.class))).then(invocation -> {
      final IEXConfiguration.IEXConfigurationEnum constant = invocation.getArgument(0, IEXConfiguration.IEXConfigurationEnum.class);
      if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE) {
        calendar.roll(Calendar.MONTH, -5);
        log.info("Returning OEP start date: " + calendar.getTime());
      } else if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) {
        calendar.roll(Calendar.MONTH, 2);
        log.info("Returning OEP end date: " + calendar.getTime());
      }

      return new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT).format(calendar.getTime());
    });

    application.setApplicationType(SsapApplicationTypeEnum.SEP.name());
    log.info("application type: {}", application.getApplicationType());
    Assert.assertEquals("Initial application type should be SEP", SsapApplicationTypeEnum.SEP.name(), application.getApplicationType());
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("type of application should be SEP", eligibilityRequest.getTypeOfApplication(), SsapApplicationTypeEnum.SEP);
  }

  @Test
  public void outsideOfOePeriod_ApplicationQEP() throws Exception {
    SingleStreamlinedApplication application = FamilyBuilder.sampleApplicationFromJson("sample-1.json");
    eligibilityEngineIntegrationService.resetSeeksQhpForPreEligibilityCall(application);

    application.getTaxHousehold().get(0).getHouseholdMember().forEach(hhm -> {
      Assert.assertTrue(hhm.isSeeksQhp());
    });

    Calendar calendar = Calendar.getInstance();
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    calendar.set(Calendar.MILLISECOND, 0);

    // make OEP window: 1 month - TODAY + 1 month
    when(DynamicPropertiesUtil.getPropertyValue(any(IEXConfiguration.IEXConfigurationEnum.class))).then(invocation -> {
      final IEXConfiguration.IEXConfigurationEnum constant = invocation.getArgument(0, IEXConfiguration.IEXConfigurationEnum.class);
      if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_START_DATE) {
        calendar.roll(Calendar.MONTH, -5);
        log.info("Returning OEP start date: " + calendar.getTime());
      } else if(constant == IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) {
        calendar.roll(Calendar.MONTH, 2);
        log.info("Returning OEP end date: " + calendar.getTime());
      }

      return new SimpleDateFormat(GhixConstants.REQUIRED_DATE_FORMAT).format(calendar.getTime());
    });

    application.setApplicationType(SsapApplicationTypeEnum.QEP.name());
    log.info("application type: {}", application.getApplicationType());
    Assert.assertEquals("Initial application type should be QEP", SsapApplicationTypeEnum.QEP.name(), application.getApplicationType());
    EligibilityRequest eligibilityRequest = eligibilityEngineIntegrationService.getEligibilityRequestFromApplication(application, RunMode.ONLINE);
    Assert.assertNotNull(eligibilityRequest);
    Assert.assertEquals("type of application should be QEP", eligibilityRequest.getTypeOfApplication(), SsapApplicationTypeEnum.QEP);
  }

  private HouseholdMember getMemberByName(SingleStreamlinedApplication application, String firstName, String lastName) {
    return
      application.getTaxHousehold().get(0)
        .getHouseholdMember().stream()
        .filter(hhm -> hhm.getName().getFirstName().equals(firstName) && hhm.getName().getLastName().equals(lastName)).findFirst().orElse(null);
  }

  private HouseholdMemberRequest getMemberByName(EligibilityRequest request, String firstName, String lastName) {
    return
      request.getMembers().stream()
        .filter(hhm -> hhm.getFirstName().equals(firstName) && hhm.getLastName().equals(lastName)).findFirst().orElse(null);
  }

}
