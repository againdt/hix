package com.getinsured.hix.planmgmt.service.jpa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.GhixNoticeCommunicationMethod;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.planmgmt.repository.IIssuerRepresentativeRepository;
import com.getinsured.hix.planmgmt.service.IssuerRepresentativeService;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.email.IssuerCertificationStatusChangeEmail;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.getinsured.timeshift.TimeShifterUtil;


@Service("issuerRepresentativeService")
@DependsOn("dynamicPropertiesUtil")
public class IssuerRepresentativeServiceImpl implements IssuerRepresentativeService{

	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerRepresentativeServiceImpl.class);
	
	@Autowired private IssuerService issuerService;
	@Autowired private IIssuerRepresentativeRepository iIssuerRepresentativeRepository;
	// Changes for HIX-26334
	@Autowired private IssuerCertificationStatusChangeEmail issuerCertificationStatusEmail;
	@Autowired private NoticeService noticeService;
	
	/**
	 * 
	 * @param newIssuerRepresentative
	 * @param redirectAttr
	 * @param issuerObj
	 * @param accountUser
	 */
	/* ##### implementation of sending delegation code  ##### */
	@Override
	public void updateDelegationInfo(IssuerRepresentative newIssuerRepresentative, RedirectAttributes redirectAttr, Issuer issuer, AccountUser accountUser) {
		try{
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			if (newIssuerRepresentative != null && stateCode.equalsIgnoreCase("CA")) {
				/**
				 * below code is added by kuldeep to update the delegation
				 * information received from AHBX integration
				 */
	
				DelegationRequest delegationRequest = new DelegationRequest();
	
				delegationRequest.setRecordId(newIssuerRepresentative.getId());
				delegationRequest.setContactFirstName((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getFirstName()
						: newIssuerRepresentative.getUserRecord().getFirstName());
				delegationRequest.setContactLastName((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getLastName()
						: newIssuerRepresentative.getUserRecord().getLastName());
				// Setting business legal name as Issuer Name
				delegationRequest.setBusinessLegalName(issuer.getName());
				delegationRequest.setAddressLineOne(issuer.getAddressLine1());
				delegationRequest.setAddressLineTwo(issuer.getAddressLine2());
				delegationRequest.setCity(issuer.getCity());
				delegationRequest.setState(issuer.getState());
				delegationRequest.setZipCode(issuer.getZip());
				delegationRequest.setCertificationStausCode(issuer.getCertificationStatus());

				if (ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString().equalsIgnoreCase(newIssuerRepresentative.getRole())) {
					delegationRequest.setRecordType("Issuer"); // Maps to Issuer Plan Representative
				}
				else if (ActivationJson.OBJECTTYPE.ISSUER_ENROLLMENT_REPRESENTATIVE.toString().equalsIgnoreCase(newIssuerRepresentative.getRole())) {
					delegationRequest.setRecordType("ISSUER_ENROLL_REP"); // Maps to Issuer Enrollment Representative
				}
				else {
					LOGGER.warn("Record Type is not set for the role {}", newIssuerRepresentative.getRole());
				}
				// delegationRequest.setHiosIssuerId(issuer.getHiosIssuerId());
	
				if (StringUtils.isNotBlank(issuer.getHiosIssuerId())
						&& StringUtils.isNumeric(issuer.getHiosIssuerId())) {
					delegationRequest.setHiosIssuerId(Integer.parseInt(issuer.getHiosIssuerId()));
				}
				try {
					delegationRequest.setPhoneNumber(Long.parseLong((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getPhone()
							: newIssuerRepresentative.getUserRecord().getPhone()));
				} catch (NumberFormatException nFE) {
					throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.DELEGATION_REQUEST_EXCEPTION, nFE, null, Severity.HIGH);
				}
				delegationRequest.setEmailId((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getEmail()
						: newIssuerRepresentative.getUserRecord().getEmail());
				// refer Jira HIX-7757 for functional identifier value range.
				delegationRequest.setFunctionalId(PlanMgmtConstants.FUNTIONAL_IDENTIFIER_VAL + newIssuerRepresentative.getId());
				DelegationResponse resp = issuerService.getAhbxDelegationResp(delegationRequest);
				/**
				 * Service failure track code is not yet written
				 */
				if (resp != null) {
					if (resp.getDelegationCode() != null && !resp.getDelegationCode().equals("0")
									&& resp.getResponseCode() != 0 && resp.getResponseDescription() != null) {
						issuerService.updateDelegateInfo(newIssuerRepresentative.getId(), resp.getDelegationCode(), resp.getResponseCode(), resp.getResponseDescription());
						redirectAttr.addFlashAttribute("sessionDelegationCode", resp.getDelegationCode());
						redirectAttr.addFlashAttribute("sessionIssuerName", issuer.getName());
						redirectAttr.addFlashAttribute("sessionRepresentativeName", newIssuerRepresentative.getUserRecord() != null ?  newIssuerRepresentative.getUserRecord().getFullName() : accountUser.getFullName());
					} else {
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("AHBX response: " + SecurityUtil.sanitizeForLogging(resp.getResponseDescription()));
						}
						redirectAttr.addFlashAttribute("sessionErrorInfo", resp.getResponseDescription());
					}
	
				} else {
					LOGGER.info("Received null response from AHBX Rest call.");
				}
			}
		}
		catch(Exception ex)
		{
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPDATE_DELEGATION_INFO_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}
	}
	
	
	
	
	/**
	 * 
	 * @param newIssuerRepresentative
	 * @param model
	 * @param issuerObj
	 * @param accountUser
	 */
	/* ##### implementation of sending delegation code  ##### */
	@Override
	public void updateIssuerDelegationInfo(IssuerRepresentative newIssuerRepresentative, Model model, Issuer issuerObj, AccountUser accountUser){
		try
		{

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE).toUpperCase();
			if (newIssuerRepresentative != null && stateCode.equalsIgnoreCase("CA")) {
				/**
				 * below code is added by Deepa to update the delegation information
				 * received from AHBX integration
				 */
				// Map<String, Object> criteria = new HashMap<String, Object>();
				/*
				 * criteria is set to CARIIER for issuer.
				 */
				/*criteria.put("recordType", "CARRIER");
				criteria.put("recordId", newIssuerRepresentative.getId());*/
				DelegationRequest delegationRequest = new DelegationRequest();
	
				delegationRequest.setRecordId(newIssuerRepresentative.getId());
				delegationRequest.setContactFirstName((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getFirstName()
						: newIssuerRepresentative.getUserRecord().getFirstName());
				delegationRequest.setContactLastName((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getLastName()
						: newIssuerRepresentative.getUserRecord().getLastName());
				// TODO setting business legal name as Issuer Name
				delegationRequest.setBusinessLegalName(issuerObj.getName());
				delegationRequest.setAddressLineOne(newIssuerRepresentative.getLocation().getAddress1());
				delegationRequest.setAddressLineTwo(newIssuerRepresentative.getLocation().getAddress2());
				delegationRequest.setCity(newIssuerRepresentative.getLocation().getCity());
				delegationRequest.setState(newIssuerRepresentative.getLocation().getState());
				delegationRequest.setZipCode((newIssuerRepresentative.getLocation().getZip()));
				delegationRequest.setCertificationStausCode(issuerObj.getCertificationStatus());

				if (ActivationJson.OBJECTTYPE.ISSUER_REPRESENTATIVE.toString().equalsIgnoreCase(newIssuerRepresentative.getRole())) {
					delegationRequest.setRecordType("Issuer"); // Maps to Issuer Plan Representative
				}
				else if (ActivationJson.OBJECTTYPE.ISSUER_ENROLLMENT_REPRESENTATIVE.toString().equalsIgnoreCase(newIssuerRepresentative.getRole())) {
					delegationRequest.setRecordType("ISSUER_ENROLL_REP"); // Maps to Issuer Enrollment Representative
				}
				else {
					LOGGER.warn("Record Type is not set for the role {}", newIssuerRepresentative.getRole());
				}
				// delegationRequest.setHiosIssuerId(issuerObj.getHiosIssuerId());
	
				if (StringUtils.isNotBlank(issuerObj.getHiosIssuerId())
						&& StringUtils.isNumeric(issuerObj.getHiosIssuerId())) {
					delegationRequest.setHiosIssuerId(Integer.parseInt(issuerObj.getHiosIssuerId()));
				}
	
				try {
					delegationRequest.setPhoneNumber(Long.parseLong((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getPhone()
							: newIssuerRepresentative.getUserRecord().getPhone()));
				} catch (NumberFormatException nFE) {
					LOGGER.error("AdminIssuerController :" + nFE);
				}
				delegationRequest.setEmailId((newIssuerRepresentative.getUserRecord() == null) ? accountUser.getEmail()
						: newIssuerRepresentative.getUserRecord().getEmail());
				// refer Jira HIX-7757 for functional identifier value range.
				delegationRequest.setFunctionalId(PlanMgmtConstants.FUNTIONAL_IDENTIFIER_VAL + newIssuerRepresentative.getId());
	
				DelegationResponse resp = issuerService.getAhbxDelegationResp(delegationRequest);
	
				/**
				 * Service failure track code is not yet written
				 */
				if (resp != null) {
					if (resp.getDelegationCode() != null && !resp.getDelegationCode().equals(PlanMgmtConstants.ZERO_STR) && resp.getResponseCode() != 0
							&& resp.getResponseDescription() != null) {
						issuerService.updateDelegateInfo(newIssuerRepresentative.getId(), resp.getDelegationCode(), resp.getResponseCode(), resp.getResponseDescription());
						model.addAttribute(PlanMgmtConstants.SESSION_DELEGATION_CODE, resp.getDelegationCode());
						model.addAttribute(PlanMgmtConstants.SESSION_REPRESENTATIVE_NAME, newIssuerRepresentative.getUserRecord() != null ? newIssuerRepresentative.getUserRecord().getFullName()
								: accountUser.getFullName());
					} else {
						LOGGER.debug("AHBX response: " +  SecurityUtil.sanitizeForLogging(resp.getResponseDescription()));
						model.addAttribute(PlanMgmtConstants.SESSION_ERROR_INFO, resp.getResponseDescription());
					}
				} else {
					LOGGER.info("Received null response from AHBX Rest call.");
				}
			}
		}
		catch(Exception ex)
		{
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.UPDATE_DELEGATION_INFO_EXCEPTION.getCode(), ex, null, Severity.HIGH);
		}	
	}
	
	
	
	/*
	 * Method for sending email to notification to Issuer Representative for certification status changes.
	 * Changes for HIX-26334
	 */
	/* ##### implementation of sending email to issuer representative  ##### */
	@Override
	public void sendNotificationEmailToIssuerRepresentative(Issuer issuerObj) {
		try {
			List<IssuerRepresentative> listIssuerRep = iIssuerRepresentativeRepository.findByIssuerId(issuerObj.getId());
			Map<String, Object> data = new HashMap<>();
			for (int i = 0; i < listIssuerRep.size(); i++) {
				if (listIssuerRep.get(i).getUserRecord() != null) {
					AccountUser accountUser = listIssuerRep.get(i).getUserRecord();
					if (accountUser != null && !StringUtils.isBlank(accountUser.getEmail()) && PlanMgmtConstants.ACTIVE.equalsIgnoreCase(accountUser.getStatus())) {
						List<String>  sendToEmailList = new ArrayList<String>();
						sendToEmailList.add(accountUser.getEmail());
						issuerCertificationStatusEmail.setIssuerObj(issuerObj);
						issuerCertificationStatusEmail.setIssuerRepObj(listIssuerRep.get(i));
						issuerCertificationStatusEmail.setUser(accountUser);
						data = issuerCertificationStatusEmail.getSingleData();
						String fileName = "IssuerCertificationStatusChange"+TimeShifterUtil.currentTimeMillis() + PlanMgmtConstants.PDF_EXT;
						noticeService.createNotice(PlanMgmtConstants.ISSUER_CERTIFICATION_STATUS_CHANGE_NOTIFICATION,  GhixLanguage.US_EN, data, PlanMgmtConstants.ISSUER_ECM_PDF_FILE_PATH, fileName, accountUser, null, GhixNoticeCommunicationMethod.Email);
						LOGGER.info("Message stored in inbox successfully");
					} else {
						LOGGER.info("Issuer Representative email address is blank or null");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Notification type \"Plan Confirmation Email\" not found. Email cannot be sent.");
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME+PlanMgmtErrorCodes.ErrorCode.SEND_NOTIFICATION_EAMIL_EXCEPTION, e, null, Severity.HIGH);
		}
	}
	
}
