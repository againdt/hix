package com.getinsured.hix.indportal.dto;

public class PendingReferralDetails {
	private String referralId;
	private String caseNumber;
	private String ssapApplicationId;
	private String workFlowStatus;
	
	
	public String getReferralId() {
		return referralId;
	}
	public void setReferralId(String referralId) {
		this.referralId = referralId;
	}
	public String getCaseNumber() {
		return caseNumber;
	}
	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}
	public String getSsapApplicationId() {
		return ssapApplicationId;
	}
	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}
	public String getWorkFlowStatus() {
		return workFlowStatus;
	}
	public void setWorkFlowStatus(String workFlowStatus) {
		this.workFlowStatus = workFlowStatus;
	}

}
