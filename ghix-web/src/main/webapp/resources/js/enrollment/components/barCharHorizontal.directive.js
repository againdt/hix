(function() {
    'use strict';

    angular
        .module('reconciliationWorkbenchApp')
        .directive('d3BarChartHorizontal', d3BarChartHorizontal);

    function d3BarChartHorizontal() {

        return {
            restrict: 'E',
            scope: {
                val: '=',
                grouped: '=',
                data: '='
            },
            link: function (scope, element, attrs) {
                var initStackedBarChart = {
                    draw: function(config) {
                        var me = this;
                        var domEle = config.element;
                        var stackKey = config.key;
                        var data = config.data;
                        var  margin = {top: 20, right: 20, bottom: 30, left: 100};
                        // parseDate = d3.timeParse("%m/%Y"),
                        var width = 400 - margin.left - margin.right;
                        var height = 200 - margin.top - margin.bottom;
                        var xScale = d3.scaleLinear().rangeRound([0, width]);
                        var yScale = d3.scaleBand().rangeRound([height, 0]).padding(0.1);
                        var color = d3.scaleOrdinal(d3.schemeCategory10);
                        var xAxis = d3.axisBottom(xScale);
                        var yAxis =  d3.axisLeft(yScale);
                        var svg = d3.select(element[0]).append("svg")
                            .attr("width", width + margin.left + margin.right)
                            .attr("height", height + margin.top + margin.bottom)
                            .append("g")
                            .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

                        var stack = d3.stack()
                            .keys(stackKey)
                            /*.order(d3.stackOrder)*/
                            .offset(d3.stackOffsetNone);

                        var layers= stack(data);
                        // data.sort(function(a, b) { return b.total - a.total; });
                        yScale.domain(data.map(function(d) { return (d.date); }));
                        xScale.domain([0, d3.max(layers[layers.length - 1], function(d) { return d[0]; }) ]).nice();

                        var div = d3.select(element[0]).append("div")
                            .attr("class", "tooltip")
                            .style("opacity", 0);

                        var layer = svg.selectAll(".layer")
                            .data(layers)
                            .enter().append("g")
                            .attr("class", "layer");
                        // .style("fill", function(d, i) { return color(i); });

                        layer.selectAll("rect")
                            .data(function(d) { return d; })
                            .enter().append("rect")
                            .attr("y", function(d) { return yScale((d.data.date)); })
                            .attr("x", function(d) { return xScale(d[0]); })
                            .attr("height", yScale.bandwidth())
                            .attr("width", function(d) { return xScale(d[1]) - xScale(d[0]) })
                            .style("fill", function(d, i) { return color(i); })
                            .on("mouseover", function(d) {
                                div.transition()
                                    .duration(200)
                                    .style("opacity", .9);
                                div.html("<h4>Trends</h4><h5> Date:  " + d.data.date + "</h5><div style='color: #1F77B4'> Missing:  " + d.data.missing + "</div> <div style='color: #FF7F0E'>Success:  " + d.data.success + "</div><div style='color: #2CA02C'>Success:  " + d.data.failure + "</div>")
                                    .style("left", (d3.event.pageX) + "px")
                                    .style("top", (d3.event.pageY - 28) + "px");
                            })
                            .on("mouseout", function(d) {
                                div.transition()
                                    .duration(500)
                                    .style("opacity", 0);
                            });

                        svg.append("g")
                            .attr("class", "axis axis--x")
                            .attr("transform", "translate(0," + (height+5) + ")")
                            .call(xAxis);

                        svg.append("g")
                            .attr("class", "axis axis--y")
                            .attr("transform", "translate(0,0)")
                            .call(yAxis);


                    }
                };

                var key = ["success", "failure"];
                initStackedBarChart.draw({
                    data: scope.data,
                    key: key,
                    element: d3.select(element[0])
                });
            }
        }
    }
})();