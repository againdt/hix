/**
 * 
 */
package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.UnexpectedJobExecutionException;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.ssap.renewal.service.AutoEnrollService;
import com.getinsured.hix.batch.ssap.renewal.util.AutoEnrollPartitionerParams;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.iex.ssap.model.RenewalApplication;

/**
 * Auto Enroll Writer class is used to process and write Auto Enroll data.
 * 
 * @since July 19, 2019
 */
public class AutoEnrollWriter implements ItemWriter<RenewalApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoEnrollWriter.class);
	private long jobExecutionId = -1;

	private JobService jobService;
	private AutoEnrollService autoEnrollService;
	private AutoEnrollPartitionerParams autoEnrollPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		jobExecutionId = stepExecution.getJobExecution().getId();
	}

	@Override
	public void write(List<? extends RenewalApplication> autoEnrollListToWrite) throws Exception {

		String batchJobStatus = null;
		if (jobService != null && jobExecutionId != -1) {
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}

		if (batchJobStatus != null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING)
				|| batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))) {
			throw new UnexpectedJobExecutionException(EnrollmentConstants.BATCH_STOP_MSG);
		}

		AtomicBoolean autoEnrollStatus = new AtomicBoolean(false);

		if (CollectionUtils.isNotEmpty(autoEnrollListToWrite)) {

			for (RenewalApplication renewalApplication : autoEnrollListToWrite) {

				try {
					autoEnrollStatus = autoEnrollService.processAutoEnrollData(renewalApplication,
							autoEnrollPartitionerParams.getProcessErrorRecordFlag(),
							autoEnrollPartitionerParams.getRenewalYear(), autoEnrollPartitionerParams.getServerName());
	
					if (!autoEnrollStatus.get()) {
						LOGGER.warn("Failed to process Auto enroll data for Renewal Application: {}", renewalApplication.getId());
					}
				}
				catch (Exception e) {
					autoEnrollService.logToGIMonitor(e, 50003, Long.toString(renewalApplication.getSsapApplicationId()));
				}
			}
		}
		LOGGER.info("Successfully execute Auto Enroll Job.");
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}

	public AutoEnrollService getAutoEnrollService() {
		return autoEnrollService;
	}

	public void setAutoEnrollService(AutoEnrollService autoEnrollService) {
		this.autoEnrollService = autoEnrollService;
	}

	public AutoEnrollPartitionerParams getAutoEnrollPartitionerParams() {
		return autoEnrollPartitionerParams;
	}

	public void setAutoEnrollPartitionerParams(AutoEnrollPartitionerParams autoEnrollPartitionerParams) {
		this.autoEnrollPartitionerParams = autoEnrollPartitionerParams;
	}
}
