package com.getinsured.eligibility.at.ref.si;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.common.ReferralResponse;
import com.getinsured.eligibility.at.ref.dto.NFProcessDTO;
import com.getinsured.eligibility.at.ref.service.ReferralEligibilityDecisionService;
import com.getinsured.eligibility.at.ref.service.nonfinancial.ReferralNonFinancialService;
import com.getinsured.eligibility.util.EligibilityUtils;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("nfReferralProcessingService")
@Scope("singleton")	
public class NFReferralProcessingService {
	private static List<Integer> nfProcessDTOList = Collections.synchronizedList(new LinkedList<Integer>());
	private static final Logger LOGGER = Logger.getLogger(NFReferralProcessingService.class);

	@Autowired
	@Qualifier("referralNFDeterminationService")
	private ReferralNonFinancialService referralNFDeterminationService;

	@Autowired
	@Qualifier("referralNFConversionService")
	private ReferralNonFinancialService referralNFConversionService;
	
	@Autowired
	@Qualifier("referralNFConversionPostService")
	private ReferralNonFinancialService referralNFConversionPostService;
	
	@Autowired
	@Qualifier("referralNFAPTCUpdateService")
	private ReferralNonFinancialService referralNFAPTCUpdateService;


	private String NF_CONV_DECISION_HEADER = "nfConversionFlow";
	private String NF_APTC_UPDATE_HEADER = "nfAPTCUpdateFlow";

	public Message<String> determineNonFinancialFlow(Message<String> message) {
		String headerValue = ReferralEligibilityDecisionService.ERROR_HANDLER;
		ReferralResponse referralResponse = new ReferralResponse();
		try {
			LOGGER.info("NFReferralProcessingService determineNonFinancialFlow starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			final NFProcessDTO nfProcessDTO = createNFProcessDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (boolean) input.getData().get(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE), (boolean) input
			        .getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR), (String) input.getData().get(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY),
			        (int) input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID),(boolean) input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));
			nfProcessDTO.setCaseNumber((String)input.getData().get(ReferralProcessingConstants.CASE_NUMBER));
			//Hashcode has been implemented to uniquely identify the DTO we are processing
			int dtoIdentifier = nfProcessDTO.hashCode();
			if(nfProcessDTOList.contains(dtoIdentifier)){
				LOGGER.error("Duplicate processing request detected for :"+nfProcessDTO.toString()+" Not sure how to handle this, letting this go, Ref: HIX-80835");
			}else{
				nfProcessDTOList.add(dtoIdentifier);
			}
			final boolean blnComplete = referralNFDeterminationService.execute(nfProcessDTO);
			headerValue = blnComplete ? ReferralEligibilityDecisionService.SUCCESS_HANDLER : ReferralEligibilityDecisionService.NF_CONVERSION_HANDLER;
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NF_MATCHING_CMR_ID, nfProcessDTO.getMatchingCmrId());
			referralResponse.getData().put(ReferralProcessingConstants.KEY_NF_APPLICATION_ID, nfProcessDTO.getNfApplicationId());

			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_NF_DETERMINATION + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_NF_DETERMINATION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_NF_DETERMINATION);
		}
		final String response = EligibilityUtils.marshal(referralResponse);
		if(nfProcessDTOList.size() > 1500){
			// We'll keep max 1500 references in memory for duplicate check
			nfProcessDTOList.remove(0);
		}
		LOGGER.info("NFReferralProcessingService determineNonFinancialFlow ends for " + response + ", headerValue - " + headerValue);
		return ReferralUtil.buildMessageWithHeader(response, NF_CONV_DECISION_HEADER, headerValue, message);
	}

	public Message<String> convertNonFinancialProcess(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		String headerValue = ReferralEligibilityDecisionService.ERROR_HANDLER;
		try {
			LOGGER.info("NFReferralProcessingService convertNonFinancialProcess starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			final NFProcessDTO nfProcessDTO = createNFProcessDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (boolean) input.getData().get(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE), (boolean) input
			        .getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR), (String) input.getData().get(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY),
			        (int) input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID), (int) input.getData().get(ReferralProcessingConstants.KEY_NF_MATCHING_CMR_ID),
			        (long) input.getData().get(ReferralProcessingConstants.KEY_NF_APPLICATION_ID),(boolean) input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));
	
			boolean blnComplete = referralNFConversionService.execute(nfProcessDTO);
			
			if (!blnComplete && nfProcessDTO.isCSChangeDetected() && nfProcessDTO.isValidationDone()){
				/* try calling ind 71 first */
				blnComplete = referralNFConversionPostService.execute(nfProcessDTO);
			}
			
			//headerValue = (blnComplete) ? ReferralEligibilityDecisionService.SUCCESS_HANDLER : ReferralEligibilityDecisionService.NF_APTC_UPDATE_HANDLER;
			
			headerValue = (!blnComplete && nfProcessDTO.isValidationDone()) ? ReferralEligibilityDecisionService.NF_APTC_UPDATE_HANDLER : ReferralEligibilityDecisionService.SUCCESS_HANDLER;
			
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_NF_CONVERSION + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_NF_CONVERSION).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_NF_CONVERSION);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("NFReferralProcessingService convertNonFinancialProcess ends for " + response + ", headerValue - " + headerValue);
		return ReferralUtil.buildMessageWithHeader(response, NF_APTC_UPDATE_HEADER, headerValue, message);

	}
	public String handleAPTCUpdateProcess(Message<String> message) {
		ReferralResponse referralResponse = new ReferralResponse();
		try {
			LOGGER.info("NFReferralProcessingService handleAPTCUpdateProcess starts ");
			final ReferralResponse input = (ReferralResponse) EligibilityUtils.unmarshal(message.getPayload());

			referralResponse.getData().putAll(input.getData());
			final NFProcessDTO nfProcessDTO = createNFProcessDTO((long) input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID), (boolean) input.getData().get(ReferralProcessingConstants.KEY_REFERRAL_CMR_MULTIPLE), (boolean) input
			        .getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR), (String) input.getData().get(ReferralProcessingConstants.KEY_ACCOUNT_TRANSFER_CATEGORY),
			        (int) input.getData().get(ReferralProcessingConstants.KEY_NON_FINANCIAL_CMR_ID), (int) input.getData().get(ReferralProcessingConstants.KEY_NF_MATCHING_CMR_ID),
			        (long) input.getData().get(ReferralProcessingConstants.KEY_NF_APPLICATION_ID),(boolean) input.getData().get(ReferralProcessingConstants.REFERRAL_AUTOLINKING));
	
			referralNFAPTCUpdateService.execute(nfProcessDTO);
					
			referralResponse.setErrorCode(ReferralResponse.NO_ERROR);
			referralResponse.setMessage(ReferralProcessingConstants.SUCCESS_EXECUTING_NF_APTC_UPDATE + input.getData().get(ReferralProcessingConstants.KEY_SSAP_APPLICATION_ID));
			referralResponse.setHeadermessage(ReferralResponse.REFERRAL_SUCCESS);

		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ReferralProcessingConstants.ERROR_EXECUTING_NF_APTC_UPDATE).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
			referralResponse.setMessage(errorReason.toString());
			referralResponse.setErrorCode(ReferralResponse.ERROR_NF_APTC_UPDATE);
		}
		final String response = EligibilityUtils.marshal(referralResponse);

		LOGGER.info("NFReferralProcessingService handleAPTCUpdateProcess ends - Response is - " + response);

		return response;
	}
	private NFProcessDTO createNFProcessDTO(long currentApplicationId, boolean multipleCmr, boolean nfCmr, String accountTransferCategory, int nfCmrId, boolean isAutoLink) {
		NFProcessDTO nfProcessDTO = new NFProcessDTO();
		nfProcessDTO.setCurrentApplicationId(currentApplicationId);
		nfProcessDTO.setMultipleCmr(multipleCmr);
		nfProcessDTO.setNonFinancialCmr(nfCmr);
		nfProcessDTO.setAccountTransferCategory(accountTransferCategory);
		nfProcessDTO.setNfCmrId(nfCmrId);
		nfProcessDTO.setAutoLink(isAutoLink);
		return nfProcessDTO;
	}

	private NFProcessDTO createNFProcessDTO(long currentApplicationId, boolean multipleCmr, boolean nfCmr, String accountTransferCategory, int nfCmrId, int matchingCmrId, long nfApplicationId, boolean isAutoLink) {
		NFProcessDTO nfProcessDTO = createNFProcessDTO(currentApplicationId, multipleCmr, nfCmr, accountTransferCategory, nfCmrId,isAutoLink);
		nfProcessDTO.setMatchingCmrId(matchingCmrId);
		nfProcessDTO.setNfApplicationId(nfApplicationId);
		return nfProcessDTO;
	}
}
