package com.getinsured.ssap.service;

import java.util.Map;
import java.util.Set;

import com.getinsured.iex.ssap.CitizenshipDocument;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.ssap.model.hub.vlp.ApplicantVerificationStatus;
import com.getinsured.ssap.model.hub.vlp.VlpIntegrationLog;
import com.getinsured.ssap.model.hub.vlp.documents.DhsDocument;

/**
 * Hub integration service definition.
 *
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
public interface HubIntegration
{
  /**
   * After application is complete, this start initial processing step.
   * <p>
   *   If you want to run verifications only for sub-set of applicants for given SSAP, use
   *   {@link #process(long, Set)}
   * </p>
   * @param applicationId ssap id.
   * @see #process(long, Set) 
   */
  void process(final long applicationId);

  /**
   * Starts initial verification for given SSAP id and filtered list of applicants
   * of that SSAP, this is useful if:
   * <ul>
   *   <li>Your SSAP has more then 1 applicant, and for 
   * some applicants verification call succeeded but for others some kind of failure occurred
   * and you only want to process records that resulted in failure.
   *   </li>
   *   <li>
   *     You want to re-run verification only for specific applicants
   *   </li>
   * </ul>
   * <p>
   *   If you want to run verifications for all applicants for given SSAP, use {@link #process(long)}
   * </p>
   * @param applicationId SSAP id.
   * @param applicantIds list of applicants for which you want to run verification.
   * @see #process(long)
   */
  void process(final long applicationId, final Set<Long> applicantIds);

  /**
   * Checks resolution for the case and updates status.
   * @param integrationLog {@link VlpIntegrationLog} original request log.
   * @return
   */
  Map<Long, ApplicantVerificationStatus> checkStepTwoCaseResolution(final VlpIntegrationLog integrationLog);

  /**
   * Resubmits new VLP with SavisId.
   * @param integrationLogId original vlp request log id.
   * @param citizenshipDocument updated citizenship document (sevis id)
   * @return boolean if call was completed.
   */
  Map<Long, ApplicantVerificationStatus> resubmitWithSevisId(final Long integrationLogId, final CitizenshipDocument citizenshipDocument);

  /**
   * Takes {@link HouseholdMember} and produces {@link DhsDocument} out of it's {@link CitizenshipDocument}.
   * @param householdMember {@link HouseholdMember} object.
   * @return {@link DhsDocument}
   */
  DhsDocument fillDocument(HouseholdMember householdMember);
}
