package com.getinsured.hix.batch.ssap.integration.task;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.joda.time.DateMidnight;
import org.joda.time.DateTime;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.repository.IAccountActivationRepository;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

public class ResendActivationNotificationTask implements Tasklet {

	private static final Logger lOGGER = Logger.getLogger(ResendActivationNotificationTask.class);
	private static final String REFERRAL_ACTIVATION_REMINDER_DAYS = "referral.activation.reminder.days";
	private AccountActivationService printEmailAccountActivationServiceImpl;
	private IAccountActivationRepository iAccountActivationRepository;
	private AccountActivationService accountActivationService;
	private ReferralActivationRepository referralActivationRepository;

	public void createStateContext() {
		accountActivationService = printEmailAccountActivationServiceImpl;
	}

	@Override
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {

		lOGGER.info("ResendActivationNotificationTask started on " + new Timestamp(System.currentTimeMillis()));
		Integer reminderDays = new Integer(DynamicPropertiesUtil.getPropertyValue(REFERRAL_ACTIVATION_REMINDER_DAYS));

		if (reminderDays.intValue() < 0){
			throw new GIRuntimeException("Reminder days not configured in gi_app_config");
		}

		DateTime datetime =  new org.joda.time.DateTime();
		DateMidnight today = datetime.toDateMidnight();
		Date tomorrow = today.plusDays(1).toDate();
		Date startDate = today.minusDays(reminderDays.intValue()).toDate();
		Date endDate = new org.joda.time.DateMidnight(startDate.getTime()).plusDays(1).toDate();
		Date nextCycle = today.plusDays(reminderDays.intValue()).toDate();
		try {
			lOGGER.info( "stDate :" + startDate+ " endDate:"+ endDate);

			List<ReferralActivation> referralActivations =  referralActivationRepository.findByWorkflowStatus(ReferralActivationEnum.INITIAL_PHASE);

			List<AccountActivation> accountActivations = iAccountActivationRepository.findByCreatedObjectTypeAndSentDateBetween(ActivationJson.OBJECTTYPE.INDIVIDUAL_REFERRAL.toString(), startDate, endDate);
			lOGGER.info(" total reminders to send :"+ accountActivations.size() );
			for(AccountActivation  accountActivation :accountActivations){

				for(ReferralActivation referralActivation: referralActivations){
					if(accountActivation.getCreatedObjectId().equals(referralActivation.getSsapApplicationId())){
						try{
							accountActivationService.sendActivationReminders(accountActivation);
							accountActivation.setSentDate(nextCycle);
						}catch(Exception e){
							lOGGER.info(getClass().getName()+"reminder failed for  :"+ accountActivation.getId()  );
							accountActivation.setSentDate(tomorrow);
						}finally {
							iAccountActivationRepository.save(accountActivation);
						}
					}
				}
			}
		} catch (Exception e) {
			lOGGER.info("Error in ResendActivationNotificationTask task"+ e.getMessage());
		}
		lOGGER.info("ResendActivationNotificationTask finished on " + new Timestamp(System.currentTimeMillis()));
		return RepeatStatus.FINISHED;
	}

	public IAccountActivationRepository getiAccountActivationRepository() {
		return iAccountActivationRepository;
	}

	public void setiAccountActivationRepository(
			IAccountActivationRepository iAccountActivationRepository) {
		this.iAccountActivationRepository = iAccountActivationRepository;
	}

	public AccountActivationService getPrintEmailAccountActivationServiceImpl() {
		return printEmailAccountActivationServiceImpl;
	}
	public void setPrintEmailAccountActivationServiceImpl(
			AccountActivationService printEmailAccountActivationServiceImpl) {
		this.printEmailAccountActivationServiceImpl = printEmailAccountActivationServiceImpl;
	}

	public AccountActivationService getAccountActivationService() {
		return accountActivationService;
	}

	public void setAccountActivationService(
			AccountActivationService accountActivationService) {
		this.accountActivationService = accountActivationService;
	}

	public ReferralActivationRepository getReferralActivationRepository() {
		return referralActivationRepository;
	}

	public void setReferralActivationRepository(
			ReferralActivationRepository referralActivationRepository) {
		this.referralActivationRepository = referralActivationRepository;
	}
}
