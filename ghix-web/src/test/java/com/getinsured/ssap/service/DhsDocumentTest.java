package com.getinsured.ssap.service;

import java.util.Calendar;
import java.util.Date;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.getinsured.ssap.model.hub.vlp.DocumentType;
import com.getinsured.ssap.model.hub.vlp.VlpPayload;
import com.getinsured.ssap.model.hub.vlp.VlpRequest;
import com.getinsured.ssap.model.hub.vlp.documents.*;

/**
 * Unit tests for DHS Documents and payloads.
 *
 * @author Yevgen Golubenko
 * @since 2/16/19
 */
@RunWith(MockitoJUnitRunner.class)
public class DhsDocumentTest
{
  private ObjectMapper om = new ObjectMapper();

  @Before
  public void setUp() {
    om.enable(SerializationFeature.INDENT_OUTPUT);
  }

  @After
  public void tearDown() {
    /* Nothing */
  }

  @Test
  public void testVlpRequestJson() throws Exception
  {
    I94Document document = new I94Document();
    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setDhsDocument(document);

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I327, document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I94 form string", json.contains("I94"));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
  }

  @Test
  public void testI327() throws Exception {
    I327Document document = new I327Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setAlienNumber(getSampleCorrectAlienNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I327 form string", json.contains(DocumentType.I327.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I327.getDhsId()));

    document.setAlienNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I327, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());
  }

  @Test
  public void testCertificateOfCitizenship() throws Exception {
    CertificateOfCitizenshipDocument document = new CertificateOfCitizenshipDocument();
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setCitizenshipNumber(getSampleCitizenshipNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Certificate of Citizenship form string", json.contains(DocumentType.CERTIFICATE_OF_CITIZENSHIP.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.CERTIFICATE_OF_CITIZENSHIP.getDhsId()));

    document.setAlienNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.CERTIFICATE_OF_CITIZENSHIP, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());
  }

  @Test
  public void testDs2019() throws Exception {
    Ds2019Document document = new Ds2019Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setI94Number(getSampleI94Number());
    document.setSevisId(getSampleSevisId());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain DS2019 form string", json.contains(DocumentType.DS2019.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.DS2019.getDhsId()));

    document.setSevisId(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.DS2019, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());
  }

  public void testI20() throws Exception {
    I20Document document = new I20Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setI94Number(getSampleI94Number());
    document.setSevisId(getSampleSevisId());
    document.setPassportCountry(getSamplePassportCountry());
    document.setPassportNumber(getSamplePassportNumber());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I20 form string", json.contains(DocumentType.I20.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I20.getDhsId()));

    document.setSevisId(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I20, document);
    Assert.assertTrue("DHS Document validation should pass", vlpRequest.getPayload().getDhsDocument().isValid());

    document.setPassportNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I20, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());

    document.setCountryOfIssuance(null);
    document.setPassportNumber(getSamplePassportNumber());
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I20, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());

    document.setCountryOfIssuance(null);
    document.setPassportCountry(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I20, document);
    Assert.assertTrue("DHS Document validation should pass", vlpRequest.getPayload().getDhsDocument().isValid());
  }

  @Test
  public void testI94() throws Exception {
    I94Document document = new I94Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setI94Number(getSampleI94Number());
    document.setSevisId(getSampleSevisId());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I94 form string", json.contains(DocumentType.I94.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I94.getDhsId()));

    document.setSevisId(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I94, document);
    Assert.assertTrue("DHS Document validation should pass", vlpRequest.getPayload().getDhsDocument().isValid());

    document.setI94Number(null);
    vlpRequest = createSampleVlpRequestWithPayload(DocumentType.I94, document);
    Assert.assertFalse("DHS Document validation should not pass", vlpRequest.getPayload().getDhsDocument().isValid());
  }

  @Test
  public void testI94InPassport() throws Exception {
    I94InPassportDocument document = new I94InPassportDocument();
    document.setI94Number(getSampleI94Number());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setPassportNumber(getSamplePassportNumber());
    document.setSevisId(getSampleSevisId());
    document.setVisaNumber(getSampleVisaNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I94 In Passport form string", json.contains(DocumentType.I94_IN_PASSPORT.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I94_IN_PASSPORT.getDhsId()));

    document.setI94Number(null);
    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertFalse("DHS Document shouldn't contain I94 number", json.contains(getSampleI94Number()));
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
  }

  @Test
  public void testI551() throws Exception {
    I551Document document = new I551Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setReceiptNumber(getSampleReceiptNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I551 In Passport form string", json.contains(DocumentType.I551.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I551.getDhsId()));

    document.setAlienNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertFalse("DHS Document shouldn't contain Alien number", json.contains(getSampleCorrectAlienNumber()));
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
  }

  @Test
  public void test571() throws Exception {
    I571Document document = new I571Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setAlienNumber(getSampleCorrectAlienNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I571 In Passport form string", json.contains(DocumentType.I571.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I571.getDhsId()));

    document.setDocExpirationDate(null);
    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    document.setAlienNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertFalse("DHS Document shouldn't contain Alien number", json.contains(getSampleCorrectAlienNumber()));
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
  }

  @Test
  public void test766() throws Exception {
    I766Document document = new I766Document();
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setReceiptNumber(getSampleReceiptNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain I766 In Passport form string", json.contains(DocumentType.I766.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.I766.getDhsId()));

    document.setAlienNumber(null);
    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertFalse("DHS Document shouldn't contain Alien number", json.contains(getSampleCorrectAlienNumber()));
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
  }

  @Test
  public void testMachineReadableImmigrantVisa() throws Exception {
    MachineReadableImmigrantVisaDocument document = new MachineReadableImmigrantVisaDocument();
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setDocExpirationDate(getDocumentExpirationDate());
    document.setPassportNumber(getSamplePassportNumber());
    document.setVisaNumber(getSampleVisaNumber());
    document.setAlienNumber(getSampleCorrectAlienNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Machine Readable Immigration Visa in form string", json.contains(DocumentType.MACHINE_READABLE_IMMIGRANT_VISA.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.MACHINE_READABLE_IMMIGRANT_VISA.getDhsId()));

    document.setDocExpirationDate(null);
    Assert.assertTrue("DHS Document validation should pass #1", document.isValid());

    document.setAlienNumber(null);
    Assert.assertFalse("DHS Document validation should not pass #2", document.isValid());
    document.setAlienNumber(getSampleCorrectAlienNumber());

    document.setPassportNumber(null);
    Assert.assertFalse("DHS Document validation should not pass #3", document.isValid());
    document.setPassportNumber(getSamplePassportNumber());

    document.setVisaNumber(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    document.setDocExpirationDate(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    document.setCountryOfIssuance(null);
    Assert.assertFalse("DHS Document validation should not pass #4", document.isValid());
    document.setPassportNumber(getSampleCountryOfIssuance());

    vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    json = om.writeValueAsString(vlpRequest);
    Assert.assertFalse("DHS should contain Visa Number", json.contains(getSampleVisaNumber()));
  }

  @Test
  public void testNaturalizationCertficiate() throws Exception {
    NaturalizationCertificateDocument document = new NaturalizationCertificateDocument();
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setNaturalizationNumber(getSampleNaturalizationNumber());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Naturalization Certificate in form string", json.contains(DocumentType.NATURALIZATION_CERTIFICATE.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.NATURALIZATION_CERTIFICATE.getDhsId()));
    Assert.assertTrue("Payload doesn't contain Alien Number", json.contains(getSampleCorrectAlienNumber()));

    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    document.setNaturalizationNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
    document.setNaturalizationNumber(getSampleNaturalizationNumber());

    document.setAlienNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
  }

  @Test
  public void testOtherAlienDocument() throws Exception {
    OtherAlienDocument document = new OtherAlienDocument();
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setPassportCountry(getSamplePassportCountry());
    document.setPassportNumber(getSamplePassportNumber());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setSevisId(getSampleSevisId());
    document.setDocDescReq(getSampleDocDescReq());
    document.setDocExpirationDate(getDocumentExpirationDate());

    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Other Alien information in form string", json.contains(DocumentType.OTHER_ALIEN.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.OTHER_ALIEN.getDhsId()));
    Assert.assertTrue("Payload doesn't contain Alien Number", json.contains(getSampleCorrectAlienNumber()));


    document.setPassportCountry(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    document.setPassportCountry(getSamplePassportCountry());

    document.setCountryOfIssuance(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(getSamplePassportNumber());
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
  }

  @Test
  public void testOtherI94() throws Exception {
    OtherI94Document document = new OtherI94Document();
    document.setI94Number(getSampleI94Number());
    document.setPassportCountry(getSamplePassportCountry());
    document.setPassportNumber(getSamplePassportNumber());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setSevisId(getSampleSevisId());
    document.setDocDescReq(getSampleDocDescReq());
    document.setDocExpirationDate(getDocumentExpirationDate());

    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Other I94 information in form string", json.contains(DocumentType.OTHER_I94.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.OTHER_I94.getDhsId()));
    Assert.assertTrue("Payload doesn't contain I94 Number", json.contains(getSampleI94Number()));


    document.setPassportCountry(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    document.setPassportCountry(getSamplePassportCountry());

    document.setCountryOfIssuance(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(getSamplePassportNumber());
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
  }

  @Test
  public void testTemporaryI551Stamp() throws Exception {
    TemporaryI551StampDocument document = new TemporaryI551StampDocument();
    document.setAlienNumber(getSampleCorrectAlienNumber());
    document.setPassportCountry(getSamplePassportCountry());
    document.setPassportNumber(getSamplePassportNumber());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setDocExpirationDate(getDocumentExpirationDate());

    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Temporary I-551 Stamp information in form string", json.contains(DocumentType.TEMPORARY_I551_STAMP.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.TEMPORARY_I551_STAMP.getDhsId()));
    Assert.assertTrue("Payload doesn't contain Alien Number", json.contains(getSampleCorrectAlienNumber()));


    document.setPassportCountry(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
    document.setPassportCountry(getSamplePassportCountry());

    document.setCountryOfIssuance(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());

    document.setPassportNumber(getSamplePassportNumber());
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
  }

  @Test
  public void testUnexpiredForeignPassport() throws Exception {
    UnexpiredForeignPassportDocument document = new UnexpiredForeignPassportDocument();
    document.setI94Number(getSampleI94Number());
    document.setPassportNumber(getSamplePassportNumber());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());
    document.setSevisId(getSampleSevisId());
    document.setDocExpirationDate(getDocumentExpirationDate());

    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    VlpRequest vlpRequest = createSampleVlpRequestWithPayload(document.getDocumentType(), document);
    String json = om.writeValueAsString(vlpRequest);

    Assert.assertTrue("Json doesn't contain FirstName", json.contains("FirstName"));
    Assert.assertTrue("Json doesn't contain Temporary Unexpired Foreign Passport information in form string", json.contains(DocumentType.UNEXPIRED_FOREIGN_PASSPORT.getName()));
    Assert.assertTrue("Payload doesn't contain DHSID", json.contains("DHSID"));
    Assert.assertTrue("Payload doesn't contain valid document type", json.contains(DocumentType.UNEXPIRED_FOREIGN_PASSPORT.getDhsId()));
    Assert.assertTrue("Payload doesn't contain I-94 Number", json.contains(getSampleI94Number()));


    document.setI94Number(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());

    document.setCountryOfIssuance(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
    document.setCountryOfIssuance(getSampleCountryOfIssuance());

    document.setPassportNumber(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
    document.setPassportNumber(getSamplePassportNumber());

    document.setDocExpirationDate(null);
    Assert.assertFalse("DHS Document validation should not pass", document.isValid());
    document.setDocExpirationDate(getDocumentExpirationDate());

    document.setSevisId(null);
    Assert.assertTrue("DHS Document validation should pass", document.isValid());
  }

  private VlpRequest createSampleVlpRequestWithPayload(DocumentType documentType) throws Exception {
    return createSampleVlpRequestWithPayload(documentType, null);
  }

  private VlpRequest createSampleVlpRequestWithPayload(DocumentType documentType, DhsDocument document) throws Exception
  {
    Calendar dateOfBirth = getSampleDate();

    VlpRequest vlpRequest = new VlpRequest();
    vlpRequest.setApplicationId(10214);
    vlpRequest.setDocumentType(documentType);
    vlpRequest.setClientIp("10.0.0.7");

    VlpPayload vlpPayload = new VlpPayload();
    vlpPayload.setDhsDocument(document);
    vlpPayload.setFirstName("Yevgen");
    vlpPayload.setLastName("Golubenko");
    vlpPayload.setMiddleName("O");
    vlpPayload.setAka("John Golubenko");
    vlpPayload.setDateOfBirth(dateOfBirth.getTime());

    vlpRequest.setPayload(vlpPayload);

    String json = om.writeValueAsString(vlpRequest);
    System.out.println("Returning json: \n" + json);

    return vlpRequest;
  }

  private Calendar getSampleDate()
  {
    Calendar dateOfBirth = Calendar.getInstance();
    dateOfBirth.set(Calendar.YEAR, 1980);
    dateOfBirth.set(Calendar.MONTH, Calendar.APRIL);
    dateOfBirth.set(Calendar.DAY_OF_MONTH, 24); // 0-based
    return dateOfBirth;
  }

  private Date getDocumentExpirationDate() {
    Calendar date = getSampleDate();
    date.roll(Calendar.YEAR, 50);
    return date.getTime();
  }

  private String getSampleCorrectAlienNumber() {
    return "738812313";
  }

  private String getSampleIncorrectAlienNumber() {
    return "00000";
  }

  private String getSampleCitizenshipNumber()
  {
    return "11111";
  }

  private String getSampleSevisId()
  {
    return "22222";
  }

  private String getSampleI94Number()
  {
    return "33333";
  }


  private String getSampleCountryOfIssuance()
  {
    return "RUS";
  }

  private String getSamplePassportNumber()
  {
    return "AC44444";
  }

  private String getSamplePassportCountry()
  {
    return "FRA";
  }

  private String getSampleVisaNumber()
  {
    return "VISA55555555";
  }

  private String getSampleReceiptNumber()
  {
    return "666666";
  }

  private String getSampleNaturalizationNumber() {
    return "77777";
  }

  private String getSampleDocDescReq()
  {
    return "88888";
  }
}
