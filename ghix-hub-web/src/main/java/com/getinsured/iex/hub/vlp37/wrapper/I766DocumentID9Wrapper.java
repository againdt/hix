package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.I766DocumentID9Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;


public class I766DocumentID9Wrapper implements JSONAware {
	
private static final Logger LOGGER = LoggerFactory.getLogger(I766DocumentID9Wrapper.class);
	
	private ObjectFactory factory;
	private I766DocumentID9Type i766DocumentID9Type;
	
	private String alienNumber;
	private String receiptNumber;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String RECEIPT_NUMBER_KEY = "ReceiptNumber";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	
	public I766DocumentID9Wrapper(){
		this.factory = new ObjectFactory();
		this.i766DocumentID9Type = factory.createI766DocumentID9Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.i766DocumentID9Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public String getReceiptNumber() {
		return receiptNumber;
	}

	public void setReceiptNumber(String receiptNumber) throws HubServiceException {
		
		if(receiptNumber == null){
			throw new HubServiceException("No Receipt number provided");
		}
		
		this.receiptNumber = receiptNumber;
		this.i766DocumentID9Type.setReceiptNumber(receiptNumber);
	}

	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i766DocumentID9Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I766 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}
	
	public I766DocumentID9Type getI766DocumentID9TypeType(){
		return this.i766DocumentID9Type;
	}
	
	/**
	 * Converts the JSON String to I766DocumentID9Wrapper object
	 * 
	 * @param jsonString - String representation for I766DocumentID9Wrapper
	 * @return I766DocumentID9Wrapper Object
	 */
	public static I766DocumentID9Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I766DocumentID9Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I766DocumentID9Wrapper wrapper = new I766DocumentID9Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.i766DocumentID9Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(RECEIPT_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.receiptNumber = (String)tmpObj;
			wrapper.i766DocumentID9Type.setReceiptNumber(wrapper.receiptNumber);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			wrapper.expiryDate = (String)tmpObj;
			try {
				wrapper.i766DocumentID9Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I766DocumentID9Wrapper from JSON, Invalid expiration date",e);
			}
		}
		
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I766DocumentID9Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I766DocumentID9Wrapper wrapper = new I766DocumentID9Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setReceiptNumber((String) jsonObj.get(RECEIPT_NUMBER_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(RECEIPT_NUMBER_KEY, this.receiptNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
}
