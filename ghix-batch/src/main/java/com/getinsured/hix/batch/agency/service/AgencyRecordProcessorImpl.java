package com.getinsured.hix.batch.agency.service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.batch.agency.repository.IAgencyRepository;
import com.getinsured.hix.batch.agency.repository.IAgencySiteRepository;
import com.getinsured.hix.batch.agency.repository.IBrokerRepository;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.model.agency.Agency;
import com.getinsured.hix.model.agency.Agency.CertificationStatus;
import com.getinsured.hix.model.agency.AgencySite;
import com.getinsured.hix.platform.security.repository.IModuleUserRepository;
import com.getinsured.hix.platform.security.repository.IUserRoleRepository;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserRoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.exception.GIException;

@Service
public class AgencyRecordProcessorImpl implements AgencyRecordProcessor {
	
	@Autowired
	IAgencyRepository iAgencyRepository;
	@Autowired
	IBrokerRepository iBrokerRepository;
	@Autowired
	ZipCodeService zipCodeService;
	@Autowired
	IAgencySiteRepository iAgencySiteRepository;
	@Autowired
	UserRoleService userRoleService;
	@Autowired
	RoleService roleService;
	@Autowired
	UserService userService;
	@Autowired
	IModuleUserRepository iModuleUserRepository;
	@Autowired
	IUserRoleRepository iUserRoleRepository;
	
	public static final DateTimeFormatter MM_DD_YYYY_FORMATTER = DateTimeFormatter.ofPattern("M/dd/yyyy");

	
	@Override
	public void process(String[] columns,int agencyColumnIndex,Long rowNo) throws Exception {
		StringBuilder errorMessages = new StringBuilder();
		if(columns.length < agencyColumnIndex+11){ 
			errorMessages.append("Expected columns as per start column ").append(agencyColumnIndex+1).append(" : ").append(agencyColumnIndex+11).append(", found - ").append(columns.length).append(System.lineSeparator());
		}
		String roleName =columns[agencyColumnIndex-1];
		if(StringUtils.isBlank(roleName)){
			errorMessages.append("No Agent Role found").append(System.lineSeparator());
		}
		String federalTaxId  = columns[agencyColumnIndex+2];
		if(StringUtils.isBlank(federalTaxId)){
			errorMessages.append("No Fedreal Tax Id found").append(System.lineSeparator());
		}
		String brokerIdStr = columns[0];
		if(StringUtils.isBlank(brokerIdStr) && NumberUtils.isNumber(brokerIdStr) ){
			errorMessages.append("Invalid agent id found.").append(System.lineSeparator());
		}
		if(errorMessages.length()>0){
			buildAndThrowException(null, brokerIdStr, rowNo,errorMessages.toString());
		}
		int brokerId = Integer.parseInt(brokerIdStr) ;	
		Agency agency = iAgencyRepository.findAgencyByFederalTaxId( federalTaxId);
		if(roleName.equalsIgnoreCase("Agency Manager")){
			
			//agency already exist
			if(agency != null){
				//check if agency already have agency manager.
				int count = iBrokerRepository.getAgencyManagerCount(agency.getId(), RoleService.AGENCY_MANAGER);
				if(count > 0){
					buildAndThrowException(agency.getId(), brokerIdStr, rowNo,"Agency Manager already linked with agency : "+agency.getId());
				}
				
			}else{
				// create new agency
				agency = createAgency(columns,agencyColumnIndex);
			}
			
			//link agency to agent
			Broker broker = linkAgencyToAgent(brokerId, agency);
			// update user role
			updateUserRole(broker);
			// update module user if needed
			updateModuleUser(broker);
		}else if(roleName.equalsIgnoreCase("Agent")){
			if(agency == null){
				// create new agency
				agency = createAgency(columns,agencyColumnIndex);
			}
			//link agency to agent
			linkAgencyToAgent(brokerId, agency);
		}else{
			buildAndThrowException((agency != null?agency.getId():null), brokerIdStr, rowNo,"Invalid Agent role : "+ roleName);
		}
	}




	private void updateModuleUser(Broker broker) throws GIException {
		ModuleUser moduleUser  = iModuleUserRepository.findModuleUser(broker.getId(),  ModuleUserService.BROKER_MODULE, broker.getUser().getId());
		if(moduleUser != null){
			moduleUser.setModuleName(ModuleUserService.AGENCY_MODULE);
			iModuleUserRepository.save(moduleUser);
		}else{
			userService.createModuleUser(broker.getId(), ModuleUserService.AGENCY_MODULE, broker.getUser(), true);
		}
		
	}




	private void updateUserRole(Broker broker) {
		Set<UserRole> userRoles = broker.getUser().getUserRole();
		UserRole brokerUserRole = null;
		if(userRoles != null){
			for(UserRole userRole : userRoles){
				if(userRole.getRole().getName().equalsIgnoreCase(RoleService.BROKER_ROLE)){
					brokerUserRole = userRole;
					break;
				}
			}
		}
		
		if(brokerUserRole != null){
			//fetch Agency Manager Role 
			Role agencyManagerRole = roleService.findRoleByName(RoleService.AGENCY_MANAGER);
			brokerUserRole.setRole(agencyManagerRole);
			iUserRoleRepository.save(brokerUserRole);
		}
		
	}




	private Broker linkAgencyToAgent(int brokerId, Agency agency) {
		Broker broker = iBrokerRepository.findById(brokerId);
		broker.setAgencyId(agency.getId());
		broker = iBrokerRepository.save(broker);
		return broker;
	}
	
	
	

	private Agency createAgency(String[] columns,int agencyColumnIndex) throws Exception {
		Agency agency = new Agency();
		agency.setAgencyName(columns[agencyColumnIndex]);
		agency.setBusinessLegalName(columns[agencyColumnIndex+1]);
		agency.setFederalTaxId(columns[agencyColumnIndex+2]);
		agency.setLicenseNumber(columns[agencyColumnIndex+3]);
		agency.setCertificationStatus(CertificationStatus.getCertificationStatus(columns[agencyColumnIndex+10]));
		agency.setCertificationDate(getCertificationDate(columns[agencyColumnIndex+11]));;
		agency = iAgencyRepository.save(agency);
		//save agency
		Location location = new Location();
		AgencySite primarySite = new AgencySite();
		primarySite.setSiteLocationName(getSiteLocationName(columns[agencyColumnIndex+4],columns[agencyColumnIndex+7]));
		primarySite.setSiteType(AgencySite.SiteType.PRIMARY.toString());
		primarySite.setAgency(agency);
		location.setAddress1(columns[agencyColumnIndex+5]);
		location.setAddress2(columns[agencyColumnIndex+6]);
		location.setCity(columns[agencyColumnIndex+7]);
		location.setState(columns[agencyColumnIndex+8]);
		location.setZip(columns[agencyColumnIndex+9]);
		ZipCode zipCode=zipCodeService.findByZipCode(location.getZip()); 
		if(zipCode!=null){
			location.setLat(zipCode.getLat());
			location.setLon(zipCode.getLon());
		}
		primarySite.setLocation(location);
		//save site
		iAgencySiteRepository.save(primarySite); 
		return agency;
	}
	
	private String getSiteLocationName(String locationName,String city) {
		String siteLocationName = locationName;
		if(StringUtils.isBlank(siteLocationName)){
			siteLocationName = city +" Location";
		}
		return siteLocationName;
	}




	private Timestamp getCertificationDate(String strCerificationDate) throws Exception {
		Timestamp certificationDate = null;
		LocalDate date= null;
		try {
			if(!StringUtils.isBlank(strCerificationDate)) {
				date = LocalDate.parse(strCerificationDate, MM_DD_YYYY_FORMATTER);
				certificationDate = Timestamp.valueOf(date.atStartOfDay());
			}
		}catch(DateTimeParseException  ex) {
			throw new Exception("Certification date not valid  : "+strCerificationDate,ex);
		}
		
		return certificationDate;
	}




	private void buildAndThrowException(Long agencyId,String agentId,Long rowNo,String message) throws AgencyMigrationException{
		throw new AgencyMigrationException(agencyId,agentId, rowNo,message);
	}
	
	

}
