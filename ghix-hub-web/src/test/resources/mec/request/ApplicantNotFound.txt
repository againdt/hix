{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload": {
    "taxHousehold": [
      {
        "householdMember": [
          {
            "dateOfBirth": "Apr 09, 1950 12:00:00 AM",
            "name": {
              "firstName": "Don",
              "middleName": "C",
              "lastName": "Johnson",
              "personNameSuffixText": ""
            },
            "socialSecurityCard": {
              "socialSecurityNumber": "215678812"
            },
            "gender": "F",
            "householdContact": {
              "homeAddress": {
                "state": "AL"
              }
            },
            "insurance": {
              "insurancePolicyEffectiveDate": "01/01/2013",
              "insurancePolicyExpirationDate": "12/31/2013"
            }
          }
        ]
      }
    ]
  }
}