package com.getinsured.hix.planmgmt.web;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestClientException;

import com.getinsured.hix.admin.web.AdminReportFormBean;
import com.getinsured.hix.admin.web.ProcessDataForReport;
import com.getinsured.hix.dto.enrollment.EnrollmentPlanResponseDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.MarketType;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.Plan.PlanLevel;
import com.getinsured.hix.model.PlanDental;
import com.getinsured.hix.model.PlanHealth;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.planmgmt.repository.IPlanDentalRepository;
import com.getinsured.hix.planmgmt.repository.IPlanHealthRepository;
import com.getinsured.hix.planmgmt.repository.RatingAreaRepository;
import com.getinsured.hix.planmgmt.service.IssuerService;
import com.getinsured.hix.planmgmt.service.PlanMgmtService;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.util.PlanMgmtConstants;
import com.getinsured.hix.util.PlanMgmtErrorCodes;
import com.thoughtworks.xstream.XStream;

@Controller
@DependsOn("dynamicPropertiesUtil")
public class IssuerEnrollmentReportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(IssuerEnrollmentReportController.class);
	@Autowired private IssuerService issuerService;
	@Autowired private RatingAreaRepository ratingAreaRepository;
	@Autowired private IPlanHealthRepository iPlanHealthRepository;
	@Autowired private IPlanDentalRepository iPlanDentalRepository;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private PlanMgmtService planMgmtService;
	
	private final ProcessDataForReport dataForReport = new ProcessDataForReport();
	public Integer timeSpanForGraph = 0;
	public final String splitStringForReport = ",";
	private static final String ENROLLMENT_STATUS_ENROLLED = "Enrolled";
	private static final String ENROLLMENT_STATUS_CONFIRMED = "CONFIRM";
	private static final String ENROLLMENT_STATUS_ORDER_ED = "ORDER_ED";
	private static final String ENROLLMENT_STATUS_ORDER_CONFIRMED = "ORDER_CONFIRMED";
	
	/**
	 * QHP Enrollment Report - Graph View
	 * 
	 * @param issuer
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/issuer/reports/qhpenrollmentreport/graph", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'VIEW_ISSUER_PROFILE')")
	public String qhpEnrollmentReportGraph(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
				
		try{
		if (issuerService.isIssuerRepLoggedIn()) {

			if (request.getQueryString() != null) {
				if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.PLATINUM_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.GOLD_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.SILVER_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.BRONZE_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);
				}
			}

			List<EnrollmentPlanResponseDTO> act_enrollListForQHPGraph = new ArrayList<EnrollmentPlanResponseDTO>();
			List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;
			List<String> goldListForQHPGraph = new ArrayList<String>();
			List<String> plantiumListForQHPGraph = new ArrayList<String>();
			List<String> silverListForQHPGraph = new ArrayList<String>();
			List<String> bronzeListForQHPGraph = new ArrayList<String>();
			List<String> expandedBronzeListForQHPGraph = new ArrayList<String>();
			List<String> catastrophicListForQHPGraph = new ArrayList<String>();

			String marketTypeConcatStringForQHPGraph = null;
			String planLevelStringForQHPGraph = null;
			String planNumberForQHP = null;
			String marketForQHP = null;
			String rNameForQHP = null;
			String periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
			String platinumCheckForQHPGraph = null;
			String goldCheckForQHPGraph = null;
			String silverCheckForQHPGraph = null;
			String bronzeCheckForQHPGraph = null;
			String expandedBronzeCheckForQHPGraph = null;
			String catastrophicCheckForQHPGraph = null;
			Integer ratingRegionAreaForQHPGraph = 0;

			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			if(null != issuerObj){
				List<Plan> planList = planMgmtService.getPlanList(issuerObj.getId(), PlanMgmtConstants.HEALTH);
				model.addAttribute(PlanMgmtConstants.PLAN_LIST, planList);
			}
			
			if (request.getParameter(PlanMgmtConstants.PLAN_ID) != null) {
				planNumberForQHP = request.getParameter(PlanMgmtConstants.PLAN_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP, planNumberForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP) != null) {
				planNumberForQHP = request.getSession().getAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
				marketForQHP = request.getParameter(PlanMgmtConstants.MARKET_ID);
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP) != null) {
				marketForQHP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
				rNameForQHP = request.getParameter(PlanMgmtConstants.REGION_NAME);
				request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP, rNameForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP) != null) {
				rNameForQHP = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP).toString();
			}

			if (rNameForQHP != null && !rNameForQHP.isEmpty()) {
				ratingRegionAreaForQHPGraph = dataForReport.getRatingRegionValue(rNameForQHP);
				LOGGER.debug("ratingRegionAreaForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForQHPGraph)));
			}

			if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
				periodIDForQHP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP) != null) {
				periodIDForQHP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP).toString();
			}

			if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
				String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
						platinumCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, platinumCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
						goldCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, goldCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
						silverCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, silverCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
						bronzeCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, bronzeCheckForQHPGraph);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
						expandedBronzeCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, expandedBronzeCheckForQHPGraph);
					}  else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
						catastrophicCheckForQHPGraph = planLevels[i];
						planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, catastrophicCheckForQHPGraph);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
			} else {
				if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP) != null) {
					String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);

					for (int i = 0; i < planLevels.length; i++) {
						if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
							platinumCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, platinumCheckForQHPGraph);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
							goldCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, goldCheckForQHPGraph);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
							silverCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, silverCheckForQHPGraph);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
							bronzeCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, bronzeCheckForQHPGraph);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
							expandedBronzeCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, expandedBronzeCheckForQHPGraph);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
							catastrophicCheckForQHPGraph = planLevels[i];
							planLevelStringForQHPGraph = getConcatedString(planLevelStringForQHPGraph, catastrophicCheckForQHPGraph);
						}
					}

					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);

				} else {
					planLevelStringForQHPGraph = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_QHP;
					model.addAttribute(PlanMgmtConstants.GOLD_CHECK, Plan.PlanLevel.GOLD);
					model.addAttribute(PlanMgmtConstants.PLATINUM_CHECK, Plan.PlanLevel.PLATINUM);
					model.addAttribute(PlanMgmtConstants.SILVER_CHECK, Plan.PlanLevel.SILVER);
					model.addAttribute(PlanMgmtConstants.BRONZE_CHECK, Plan.PlanLevel.BRONZE);
					model.addAttribute(PlanMgmtConstants.EXPANDED_BRONZE_CHECK, Plan.PlanLevel.EXPANDEDBRONZE);
					model.addAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK, Plan.PlanLevel.CATASTROPHIC);
					periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
					marketForQHP = null;
					request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
					request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);

					String[] planLevels = { PlanLevel.PLATINUM.toString().toLowerCase(), PlanLevel.GOLD.toString().toLowerCase(), PlanLevel.SILVER.toString().toLowerCase(), PlanLevel.BRONZE.toString().toLowerCase(), PlanLevel.EXPANDEDBRONZE.toString().toLowerCase(), PlanLevel.CATASTROPHIC.toString().toLowerCase()};
					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
				}
			}

			if (planLevelStringForQHPGraph != null) {

				LOGGER.debug("planLevelStringForQHPGraph for qhpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPGraph.contains(splitStringForReport))));
				if (planLevelStringForQHPGraph.contains(splitStringForReport)) {

					String[] planLevelArrForQHPGraph = planLevelStringForQHPGraph.split(splitStringForReport);
					for (int p = 0; p < planLevelArrForQHPGraph.length; p++) {

						List<String> returnList = new ArrayList<String>();
						EnrollmentRequest enrollmentRequestForQHPGraph = new EnrollmentRequest();
						Map<String, String> timeMap = new HashMap<String, String>();
						if(null != periodIDForQHP){
							timeMap = dataForReport.setTimePeriods(periodIDForQHP.toUpperCase());
						}

						if (marketForQHP != null && !(marketForQHP.isEmpty())) {
							if (marketTypeConcatStringForQHPGraph != null
									&& !(marketTypeConcatStringForQHPGraph.isEmpty())
									&& !(marketTypeConcatStringForQHPGraph.contains(marketForQHP))) {
								marketTypeConcatStringForQHPGraph = getConcatedString(marketTypeConcatStringForQHPGraph, marketForQHP);
								LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPGraph)));
							} else {
								marketTypeConcatStringForQHPGraph = marketForQHP;
								LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPGraph)));
							}
						} else {
							marketTypeConcatStringForQHPGraph = PlanMgmtConstants.ALL_MARKET_TYPES;
						}

						String marketTypeArray[] = marketTypeConcatStringForQHPGraph.split(splitStringForReport);
						LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));

						
							for (int market = 0; market < marketTypeArray.length; market++) {

								if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
									enrollmentRequestForQHPGraph.setPlanMarket(MarketType.INDIVIDUAL);
								} else {
									enrollmentRequestForQHPGraph.setPlanMarket(MarketType.SHOP);
								}

								if (planNumberForQHP != null) {
									enrollmentRequestForQHPGraph.setPlanNumber(planNumberForQHP);
								} else {
									enrollmentRequestForQHPGraph.setPlanNumber(null);
								}
								enrollmentRequestForQHPGraph.setPlanLevel(planLevelArrForQHPGraph[p].toUpperCase());

								if (ratingRegionAreaForQHPGraph != 0) {
									enrollmentRequestForQHPGraph.setRatingRegion(String.valueOf(ratingRegionAreaForQHPGraph));
								} else {
									enrollmentRequestForQHPGraph.setRatingRegion(null);
								}

								enrollmentRequestForQHPGraph.setStartDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
								enrollmentRequestForQHPGraph.setEndDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.CURRENT_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));

//								XStream xstreamForQHPGraph = GhixUtils.getXStreamStaxObject();
								XStream xstreamForQHPGraph = GhixUtils.getXStreamStaxObject();
								String xmlRequestForQHPGraph = xstreamForQHPGraph.toXML(enrollmentRequestForQHPGraph);
								LOGGER.debug("xmlRequestToSendForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForQHPGraph)));

								String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, xmlRequestForQHPGraph, String.class);
								LOGGER.debug("enrollmentListXML: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));

								temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();

								EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForQHPGraph.fromXML(enrollmentListXML);
								LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
								if (enrollmentResponse != null) {
									temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
									if (temp_enrollemntResponse != null
											&& temp_enrollemntResponse.size() > 0) {
										act_enrollListForQHPGraph.addAll(temp_enrollemntResponse);
									}
								}

								if (temp_enrollemntResponse != null
										&& temp_enrollemntResponse.size() > 0) {
									LOGGER.debug("act_enrollListForQHPGraph_test: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_enrollemntResponse.size())));
									if (periodIDForQHP != null
											&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.MONTHLY_DEFAULT_SPAN;
									} else if (periodIDForQHP != null
											&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.QUARTERLY_DEFAULT_SPAN;
									} else if (periodIDForQHP != null
											&& periodIDForQHP.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
										timeSpanForGraph = PlanMgmtConstants.YEAR_DEFAULT_SPAN;
									}

									if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
										plantiumListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									} else if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
										goldListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									} else if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
										silverListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									} else if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
										bronzeListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									}else if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
										expandedBronzeListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									}
									else if (planLevelArrForQHPGraph[p] != null
											&& planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
										catastrophicListForQHPGraph = setDataForGraph(planLevelArrForQHPGraph[p], timeSpanForGraph, periodIDForQHP, temp_enrollemntResponse, enrollmentRequestForQHPGraph.getStartDate(), enrollmentRequestForQHPGraph.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj, returnList);
									}

									if (periodIDForQHP != null
											&& act_enrollListForQHPGraph != null
											& planLevelArrForQHPGraph[p] != null) {
										dataForReport.setTimePeriodsAndValues(model, periodIDForQHP, act_enrollListForQHPGraph);
										if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, goldListForQHPGraph, planLevelArrForQHPGraph[p]);
										} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, plantiumListForQHPGraph, planLevelArrForQHPGraph[p]);
										} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, silverListForQHPGraph, planLevelArrForQHPGraph[p]);
										} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, bronzeListForQHPGraph, planLevelArrForQHPGraph[p]);
										} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, expandedBronzeListForQHPGraph, planLevelArrForQHPGraph[p]);
										} else if (planLevelArrForQHPGraph[p].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
											dataForReport.setTimePeriodsValues(model, periodIDForQHP, catastrophicListForQHPGraph, planLevelArrForQHPGraph[p]);
										}
									}

									LOGGER.debug("Enrollment List for QHP Graph: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForQHPGraph.size())));
								}
							}
						

					}
				}
			}

			model.addAttribute(PlanMgmtConstants.PLAN_ID, planNumberForQHP);
			model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForQHP);
			model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForQHP);
			model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForQHP);

			List<String> regionList = ratingAreaRepository.getAllRatingAreaName(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			if (regionList != null) {
				model.addAttribute(PlanMgmtConstants.REGION_LIST, regionList);
			}
		}
		}catch (Exception e) {
			LOGGER.error("Exception Comes in QHP Enrollment graph: " , e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME
					+ PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_GRAPH_EXCEPTION,
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return "issuer/reports/qhpenrollmentreport/graph";
	}

	/**
	 * QHP Enrollment Report - List View
	 * 
	 * @param issuer
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/issuer/reports/qhpenrollmentreport/list", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'VIEW_ISSUER_PROFILE')")
	public String qhpEnrollmentReportList(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		try{
		if (issuerService.isIssuerRepLoggedIn()) {

			if (request.getQueryString() != null) {
				if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.PLATINUM_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.GOLD_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.SILVER_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.BRONZE_CHECK_FOR_QHP);
					//request.getSession().removeAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP);
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);
				}
			}

			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			List<Plan> planList = planMgmtService.getPlanList(issuerObj.getId(), PlanMgmtConstants.HEALTH);

			List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
			ProcessDataForReport dataForReport = new ProcessDataForReport();
			List<EnrollmentPlanResponseDTO> act_enrollListForQHPList = new ArrayList<EnrollmentPlanResponseDTO>();
			List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;
			List<HashMap<String, String>> listPageMapData = new ArrayList<HashMap<String, String>>();

			String marketTypeConcatStringForQHPList = null;
			String planLevelStringForQHPList = null;
			String rNameForQHPList = null;
			Integer ratingRegionAreaForQHPList = 0;

			String planNumberForQHP = null;
			String marketForQHP = null;
			String periodIDForQHP = null;
			String platinumCheckForQHPList = null;
			String goldCheckForQHPList = null;
			String silverCheckForQHPList = null;
			String bronzeCheckForQHPList = null;
			String expandedBronzeCheckForQHPList = null;
			String catastrophicCheckForQHPList = null;

			if (request.getParameter(PlanMgmtConstants.PLAN_ID) != null) {
				planNumberForQHP = request.getParameter(PlanMgmtConstants.PLAN_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP, planNumberForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP) != null) {
				planNumberForQHP = request.getSession().getAttribute(PlanMgmtConstants.PLAN_NUMBER_FOR_QHP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
				marketForQHP = request.getParameter(PlanMgmtConstants.MARKET_ID);
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP) != null) {
				marketForQHP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
				periodIDForQHP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP) != null) {
				periodIDForQHP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP).toString();
			}

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			List<String> regionList = ratingAreaRepository.getAllRatingAreaName(stateCode);

			if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
				rNameForQHPList = request.getParameter(PlanMgmtConstants.REGION_NAME);
				request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP, rNameForQHPList);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP) != null) {
				rNameForQHPList = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_QHP).toString();
			}

			if (rNameForQHPList != null && !rNameForQHPList.isEmpty()) {
				ratingRegionAreaForQHPList = dataForReport.getRatingRegionValue(rNameForQHPList);
				LOGGER.debug("ratingRegionAreaForQHPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForQHPList)));
			}

			if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
				String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
						platinumCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, platinumCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
						goldCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, goldCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
						silverCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, silverCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
						bronzeCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, bronzeCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
						expandedBronzeCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, expandedBronzeCheckForQHPList);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
						catastrophicCheckForQHPList = planLevels[i];
						planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, catastrophicCheckForQHPList);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
			} else {
				if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP) != null) {
					String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP);

					for (int i = 0; i < planLevels.length; i++) {
						if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.PLATINUM.toString())) {
							platinumCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, platinumCheckForQHPList);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.GOLD.toString())) {
							goldCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, goldCheckForQHPList);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.SILVER.toString())) {
							silverCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, silverCheckForQHPList);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.BRONZE.toString())) {
							bronzeCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, bronzeCheckForQHPList);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.EXPANDEDBRONZE.toString())) {
							expandedBronzeCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, expandedBronzeCheckForQHPList);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevel.CATASTROPHIC.toString())) {
							catastrophicCheckForQHPList = planLevels[i];
							planLevelStringForQHPList = getConcatedString(planLevelStringForQHPList, catastrophicCheckForQHPList);
						}
					}

					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);

				} else {
					planLevelStringForQHPList = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_QHP;
					model.addAttribute(PlanMgmtConstants.GOLD_CHECK, Plan.PlanLevel.GOLD);
					model.addAttribute(PlanMgmtConstants.PLATINUM_CHECK, Plan.PlanLevel.PLATINUM);
					model.addAttribute(PlanMgmtConstants.SILVER_CHECK, Plan.PlanLevel.SILVER);
					model.addAttribute(PlanMgmtConstants.BRONZE_CHECK, Plan.PlanLevel.BRONZE);
					model.addAttribute(PlanMgmtConstants.EXPANDED_BRONZE_CHECK, Plan.PlanLevel.EXPANDEDBRONZE);
					model.addAttribute(PlanMgmtConstants.CATASTROPHIC_CHECK, Plan.PlanLevel.CATASTROPHIC);
					periodIDForQHP = PlanMgmtConstants.MONTHLY_PERIOD;
					marketForQHP = null;
					request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_QHP, marketForQHP);
					request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_QHP, periodIDForQHP);

					String[] planLevels = { PlanLevel.PLATINUM.toString().toLowerCase(), PlanLevel.GOLD.toString().toLowerCase(), PlanLevel.SILVER.toString().toLowerCase(), PlanLevel.BRONZE.toString().toLowerCase(), PlanLevel.EXPANDEDBRONZE.toString().toLowerCase(), PlanLevel.CATASTROPHIC.toString().toLowerCase()};
					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_QHP, planLevels);
				}

			}

			/*iteration depends on the number of plan levels*/
			if (planLevelStringForQHPList != null) {

				LOGGER.debug("planLevelStringForQHPList for qhpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForQHPList.contains(splitStringForReport))));

				if (planLevelStringForQHPList.contains(splitStringForReport)) {

					String[] planLevelArrForQHPList = planLevelStringForQHPList.split(splitStringForReport);
					if(null != planLevelArrForQHPList){
						
						for (int p = 0; p < planLevelArrForQHPList.length; p++) {
							EnrollmentRequest enrollmentRequestForQHPList = new EnrollmentRequest();
							Map<String, String> timeMap = new HashMap<String, String>();

							// Fixed HP-FOD issue
							if (StringUtils.isNotBlank(periodIDForQHP)) {
								timeMap = dataForReport.setTimePeriods(periodIDForQHP.toUpperCase());
							}

							if (marketForQHP != null && !(marketForQHP.isEmpty())) {
								if (marketTypeConcatStringForQHPList != null
										&& !(marketTypeConcatStringForQHPList.isEmpty())
										&& !(marketTypeConcatStringForQHPList.contains(marketForQHP))) {
									marketTypeConcatStringForQHPList = getConcatedString(marketTypeConcatStringForQHPList, marketForQHP);
									LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPList)));
								} else {
									marketTypeConcatStringForQHPList = marketForQHP;
									LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForQHPList)));
								}
							} else {
								marketTypeConcatStringForQHPList = PlanMgmtConstants.ALL_MARKET_TYPES;
							}
	
							String marketTypeArray[] = marketTypeConcatStringForQHPList.split(splitStringForReport);
							if(null != marketTypeArray){
								LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
								for (int market = 0; market < marketTypeArray.length; market++) {
	
									if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
										enrollmentRequestForQHPList.setPlanMarket(MarketType.INDIVIDUAL);
									} else {
										enrollmentRequestForQHPList.setPlanMarket(MarketType.SHOP);
									}
	
									if (planNumberForQHP != null) {
										enrollmentRequestForQHPList.setPlanNumber(planNumberForQHP);
									} else {
										enrollmentRequestForQHPList.setPlanNumber(null);
									}
									enrollmentRequestForQHPList.setPlanLevel((planLevelArrForQHPList[p].toUpperCase()));
									if (ratingRegionAreaForQHPList != 0) {
										enrollmentRequestForQHPList.setRatingRegion(String.valueOf(ratingRegionAreaForQHPList));
									} else {
										enrollmentRequestForQHPList.setRatingRegion(null);
									}
									// Fixed HP-FOD issue
									if (StringUtils.isNotBlank(periodIDForQHP)) {
										timeMap = dataForReport.setTimePeriods(periodIDForQHP.toUpperCase());
										enrollmentRequestForQHPList.setStartDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
										enrollmentRequestForQHPList.setEndDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.CURRENT_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
									}

//									XStream xstreamForQHPList = GhixUtils.getXStreamStaxObject();
									XStream xstreamForQHPList = GhixUtils.getXStreamStaxObject();
									String xmlRequestForQHPList = xstreamForQHPList.toXML(enrollmentRequestForQHPList);
									LOGGER.debug("enrollmentRequestForQHPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForQHPList)));
	
									String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, xmlRequestForQHPList, String.class);
									LOGGER.debug("enrollmentListXML: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
	
									temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
									EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForQHPList.fromXML(enrollmentListXML);
									LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
									if (enrollmentResponse != null) {
										temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
	
										if (temp_enrollemntResponse != null
												&& temp_enrollemntResponse.size() > 0) {
											act_enrollListForQHPList.addAll(temp_enrollemntResponse);
										}
									}
	
									if (act_enrollListForQHPList != null
											&& act_enrollListForQHPList.size() > 0) {
										listPageData = setDataForListPage(planLevelArrForQHPList[p], periodIDForQHP, act_enrollListForQHPList, enrollmentRequestForQHPList.getStartDate(), enrollmentRequestForQHPList.getEndDate(), PlanMgmtConstants.HEALTH, marketTypeArray[market], issuerObj);
	
										List<AdminReportFormBean> formBeans = new ArrayList<AdminReportFormBean>();
										formBeans.addAll(listPageData);
										LOGGER.debug("formBeans: " + SecurityUtil.sanitizeForLogging(String.valueOf(formBeans.size())));
	
										removeDuplicateInfo(listPageData);
										LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData.size())));
	
										if (listPageData.size() > 0) {
											for (AdminReportFormBean adminReportFormBean : listPageData) {
												HashMap<String, String> listPageMap = new HashMap<String, String>();
												Integer enrollcount = getEnrollmentCountForOneEnrollment(adminReportFormBean, formBeans);
												LOGGER.debug("enrollcount: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollcount)));
												listPageMap.put(PlanMgmtConstants.TIME_PERIOD, adminReportFormBean.getTimePeriod());
												listPageMap.put(PlanMgmtConstants.ISSUER_NAME, adminReportFormBean.getIssuerName());
												listPageMap.put(PlanMgmtConstants.PLAN_NAME, adminReportFormBean.getPlanName());
												listPageMap.put(PlanMgmtConstants.PLAN_NUMBER, adminReportFormBean.getPlanNumber());
												listPageMap.put(PlanMgmtConstants.PLAN_LEVEL, adminReportFormBean.getPlanLevel());
												listPageMap.put(PlanMgmtConstants.PLAN_MARKET, adminReportFormBean.getMarket());
												listPageMap.put(PlanMgmtConstants.ENROLLMENT, String.valueOf(enrollcount));
												listPageMapData.add(listPageMap);
											}
										}
									}
								}
							}
						}
					}
				}
			}
			model.addAttribute(PlanMgmtConstants.PLAN_LIST, planList);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planNumberForQHP);
			model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForQHP);
			model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForQHP);
			model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForQHPList);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_DATA, listPageMapData);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, PlanMgmtConstants.PAGE_SIZE);
			if (regionList != null) {
				model.addAttribute(PlanMgmtConstants.REGION_LIST, regionList);
			}
		}
		}catch (Exception e) {
			LOGGER.error("Exception Comes in QHP Enrollment List: ",e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "issuer/reports/qhpenrollmentreport/list";
	}

	/**
	 * SADP Enrollment Report - Graph View, This method generates the enrollment
	 * report for SADP plans
	 * 
	 * @param issuer - Issuer Id
	 * @param model - Model object
	 * @param request - HTTP request object
	 * @return
	 */
	@RequestMapping(value = "/issuer/reports/sadpenrollmentreport/graph", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'VIEW_ISSUER_PROFILE')")
	public String sadpEnrollmentReportGraph(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try{
		if (issuerService.isIssuerRepLoggedIn()) {

			if (request.getQueryString() != null) {
				if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.HIGH_CHECK_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.LOW_CHECK_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);
				}
			}

			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			List<Plan> planList = planMgmtService.getPlanList(issuerObj.getId(), PlanMgmtConstants.DENTAL);

			ProcessDataForReport dataForReport = new ProcessDataForReport();
			List<String> highListForSADPGraph = new ArrayList<String>();
			List<String> lowListForSADPGraph = new ArrayList<String>();
			List<EnrollmentPlanResponseDTO> act_enrollListForSADPGraph = new ArrayList<EnrollmentPlanResponseDTO>();
			List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;

			String planLevelStringForSADP = null;
			String marketTypeConcatStringForSADPGraph = null;
			String rNameForSADPGraph = null;
			String planNameForSADP = null;
			String marketForSADP = null;
			String periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
			String highCheckForSADP = null;
			String lowCheckForSADP = null;
			Integer ratingRegionAreaForSADPGraph = 0;

			if (request.getParameter(PlanMgmtConstants.PLAN_ID) != null) {
				planNameForSADP = request.getParameter(PlanMgmtConstants.PLAN_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP, planNameForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP) != null) {
				planNameForSADP = request.getSession().getAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
				marketForSADP = request.getParameter(PlanMgmtConstants.MARKET_ID);
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP, marketForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP) != null) {
				marketForSADP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
				periodIDForSADP = request.getParameter(PlanMgmtConstants.PERIOD_ID).toString();
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP) != null) {
				periodIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
				rNameForSADPGraph = request.getParameter(PlanMgmtConstants.REGION_NAME);
				request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP, rNameForSADPGraph);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP) != null) {
				rNameForSADPGraph = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP).toString();
			}

			if (rNameForSADPGraph != null && !rNameForSADPGraph.isEmpty()) {
				ratingRegionAreaForSADPGraph = dataForReport.getRatingRegionValue(rNameForSADPGraph);
				LOGGER.debug("ratingRegionAreaForSADPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForSADPGraph)));
			}

			if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
				String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
						highCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
						lowCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
			} else {
				if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP) != null) {
					String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);

					for (int i = 0; i < planLevels.length; i++) {
						if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
							highCheckForSADP = planLevels[i];
							planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
							lowCheckForSADP = planLevels[i];
							planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
						}
					}

					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);

				} else {
					planLevelStringForSADP = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_SADP;
					model.addAttribute(PlanMgmtConstants.HIGH_CHECK, Plan.PlanLevelDental.HIGH);
					model.addAttribute(PlanMgmtConstants.LOW_CHECK, Plan.PlanLevelDental.LOW);

					periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
					marketForSADP = null;
					String[] planLevels = { Plan.PlanLevelDental.HIGH.toString().toLowerCase(), Plan.PlanLevelDental.LOW.toString().toLowerCase() };
					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
				}
			}

			/*iteration depends on the number of plan levels*/
			if (planLevelStringForSADP != null) {
				LOGGER.debug("planLevelStringForSADP for qhpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForSADP.contains(splitStringForReport))));

				if (planLevelStringForSADP.contains(splitStringForReport)) {
					String[] planLevelArrForSADPGraph = planLevelStringForSADP.split(splitStringForReport);

					for (int p = 0; p < planLevelArrForSADPGraph.length; p++) {

						EnrollmentRequest enrollmentRequestForSADPGraph = new EnrollmentRequest();
						List<String> returnList = new ArrayList<String>();
						Map<String, String> timeMap = new HashMap<String, String>();
						timeMap = dataForReport.setTimePeriods(periodIDForSADP.toUpperCase());

						if (marketForSADP != null && !(marketForSADP.isEmpty())) {
							if (marketTypeConcatStringForSADPGraph != null
									&& !(marketTypeConcatStringForSADPGraph.isEmpty())
									&& !(marketTypeConcatStringForSADPGraph.contains(marketForSADP))) {
								marketTypeConcatStringForSADPGraph = getConcatedString(marketTypeConcatStringForSADPGraph, marketForSADP);
								LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPGraph)));
							} else {
								marketTypeConcatStringForSADPGraph = marketForSADP;
								LOGGER.debug("Market_Type_NOT_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPGraph)));
							}
						} else {
							marketTypeConcatStringForSADPGraph = "INDIVIDUAL,SHOP";
						}
						String marketTypeArray[] = marketTypeConcatStringForSADPGraph.split(splitStringForReport);
						LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));

							for (int market = 0; market < marketTypeArray.length; market++) {

								if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
									enrollmentRequestForSADPGraph.setPlanMarket(MarketType.INDIVIDUAL);
								} else {
									enrollmentRequestForSADPGraph.setPlanMarket(MarketType.SHOP);
								}

								if (planNameForSADP != null) {
									enrollmentRequestForSADPGraph.setPlanNumber(planNameForSADP);
								} else {
									enrollmentRequestForSADPGraph.setPlanNumber(null);
								}

								enrollmentRequestForSADPGraph.setPlanLevel((planLevelArrForSADPGraph[p].toUpperCase()));
								if (ratingRegionAreaForSADPGraph != 0) {
									enrollmentRequestForSADPGraph.setRatingRegion(String.valueOf(ratingRegionAreaForSADPGraph));
								} else {
									enrollmentRequestForSADPGraph.setRatingRegion(null);
								}

								enrollmentRequestForSADPGraph.setStartDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
								enrollmentRequestForSADPGraph.setEndDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.CURRENT_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));

//								XStream xstreamForSADPGraph = GhixUtils.getXStreamStaxObject();
								XStream xstreamForSADPGraph = GhixUtils.getXStreamStaxObject();
								String xmlRequestForSADPGraph = xstreamForSADPGraph.toXML(enrollmentRequestForSADPGraph);
								LOGGER.debug("enrollmentRequestForSADPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForSADPGraph)));

								String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, xmlRequestForSADPGraph, String.class);
								LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));

								if (enrollmentListXML != null) {
									temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
									EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForSADPGraph.fromXML(enrollmentListXML);
									LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));

									if (enrollmentResponse != null) {
										temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();

										if (temp_enrollemntResponse != null
												&& temp_enrollemntResponse.size() > 0) {
											act_enrollListForSADPGraph.addAll(temp_enrollemntResponse);
											LOGGER.debug("act_enrollListForSADPGraph_test: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPGraph.size())));
										}
									}

									if (temp_enrollemntResponse != null
											&& temp_enrollemntResponse.size() > 0) {
										if (periodIDForSADP != null
												&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
											timeSpanForGraph = PlanMgmtConstants.MONTHLY_DEFAULT_SPAN;
										} else if (periodIDForSADP != null
												&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
											timeSpanForGraph = PlanMgmtConstants.QUARTERLY_DEFAULT_SPAN;
										} else if (periodIDForSADP != null
												&& periodIDForSADP.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
											timeSpanForGraph = PlanMgmtConstants.YEAR_DEFAULT_SPAN;
										}

										if (planLevelArrForSADPGraph[p] != null
												&& planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
											lowListForSADPGraph = setDataForGraph(planLevelArrForSADPGraph[p], PlanMgmtConstants.MONTHLY_DEFAULT_SPAN, periodIDForSADP, temp_enrollemntResponse, enrollmentRequestForSADPGraph.getStartDate(), enrollmentRequestForSADPGraph.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market], issuerObj, returnList);
										} else if (planLevelArrForSADPGraph[p] != null
												&& planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
											highListForSADPGraph = setDataForGraph(planLevelArrForSADPGraph[p], PlanMgmtConstants.MONTHLY_DEFAULT_SPAN, periodIDForSADP, temp_enrollemntResponse, enrollmentRequestForSADPGraph.getStartDate(), enrollmentRequestForSADPGraph.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market], issuerObj, returnList);
										}

										if (periodIDForSADP != null
												&& act_enrollListForSADPGraph != null
												& planLevelArrForSADPGraph[p] != null) {
											dataForReport.setTimePeriodsAndValues(model, periodIDForSADP, act_enrollListForSADPGraph);

											if (planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
												dataForReport.setTimePeriodsValues(model, periodIDForSADP, highListForSADPGraph, planLevelArrForSADPGraph[p]);
											} else if (planLevelArrForSADPGraph[p].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
												dataForReport.setTimePeriodsValues(model, periodIDForSADP, lowListForSADPGraph, planLevelArrForSADPGraph[p]);
											}
										}
									}
									LOGGER.debug("Enrollment List for SADP Graph: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPGraph.size())));
								}
							}
					}
				}
			}
			model.addAttribute(PlanMgmtConstants.PLAN_LIST, planList);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planNameForSADP);
			model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForSADP);
			model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForSADP);
			model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForSADPGraph);

			List<String> regionList = ratingAreaRepository.getAllRatingAreaName(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE));

			if (regionList != null) {
				model.addAttribute(PlanMgmtConstants.REGION_LIST, regionList);
			}
		}
		}catch (Exception e) {
			LOGGER.error("Exception Comes in SADP Enrollment graph: ",e);
			throw new GIRuntimeException(
					PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "issuer/reports/sadpenrollmentreport/graph";
	}

	/**
	 * SADP Enrollment Report - List View
	 * 
	 * @param issuer
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/issuer/reports/sadpenrollmentreport/list", method = {
			RequestMethod.GET, RequestMethod.POST })
	@PreAuthorize("hasPermission(#model, 'VIEW_ISSUER_PROFILE')")
	public String sadpEnrollmentReportList(@ModelAttribute(PlanMgmtConstants.ISSUER) Issuer issuer, Model model, HttpServletRequest request) {
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}

		try{
		if (issuerService.isIssuerRepLoggedIn()) {

			if (request.getQueryString() != null) {
				if (request.getQueryString().contains(PlanMgmtConstants.RESET_QUERYSTRING)) {
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.HIGH_CHECK_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.LOW_CHECK_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP);
					request.getSession().removeAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);
				}
			}

			Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
			List<Plan> planList = planMgmtService.getPlanList(issuerObj.getId(), PlanMgmtConstants.DENTAL);

			ProcessDataForReport dataForReport = new ProcessDataForReport();

			List<HashMap<String, String>> listPageMapData = new ArrayList<HashMap<String, String>>();
			List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
			List<EnrollmentPlanResponseDTO> act_enrollListForSADPList = new ArrayList<EnrollmentPlanResponseDTO>();
			List<EnrollmentPlanResponseDTO> temp_enrollemntResponse = null;

			String planLevelStringForSADP = null;
			String marketTypeConcatStringForSADPList = null;
			String rNameForSADPList = null;
			String planNumberForSADP = null;
			String marketForSADP = null;
			String periodIDForSADP = null;
			String lowCheckForSADP = null;
			String highCheckForSADP = null;
			Integer ratingRegionAreaForSADPList = 0;

			if (request.getParameter(PlanMgmtConstants.PLAN_ID) != null) {
				planNumberForSADP = request.getParameter(PlanMgmtConstants.PLAN_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP, planNumberForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP) != null) {
				planNumberForSADP = request.getSession().getAttribute(PlanMgmtConstants.PLAN_NAME_FOR_SADP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.MARKET_ID) != null) {
				marketForSADP = request.getParameter(PlanMgmtConstants.MARKET_ID);
				request.getSession().setAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP, marketForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP) != null) {
				marketForSADP = request.getSession().getAttribute(PlanMgmtConstants.MARKET_NUMBER_FOR_SADP).toString();
			}

			if (request.getParameter(PlanMgmtConstants.PERIOD_ID) != null) {
				periodIDForSADP = request.getParameter(PlanMgmtConstants.PERIOD_ID);
				request.getSession().setAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP, periodIDForSADP);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP) != null) {
				periodIDForSADP = request.getSession().getAttribute(PlanMgmtConstants.PERIOD_ID_FOR_SADP).toString();
			}

			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			List<String> regionList = ratingAreaRepository.getAllRatingAreaName(stateCode);

			if (request.getParameter(PlanMgmtConstants.REGION_NAME) != null) {
				rNameForSADPList = request.getParameter(PlanMgmtConstants.REGION_NAME);
				request.getSession().setAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP, rNameForSADPList);
			} else if (request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP) != null) {
				rNameForSADPList = request.getSession().getAttribute(PlanMgmtConstants.REGION_NAME_FOR_SADP).toString();
			}

			if (rNameForSADPList != null && !rNameForSADPList.isEmpty()) {
				ratingRegionAreaForSADPList = dataForReport.getRatingRegionValue(rNameForSADPList);
				LOGGER.debug("ratingRegionAreaForQHPGraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(ratingRegionAreaForSADPList)));
			}

			if (request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL) != null) {
				String[] planLevels = request.getParameterValues(PlanMgmtConstants.PLAN_LEVEL);
				for (int i = 0; i < planLevels.length; i++) {
					if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
						highCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
					} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
						lowCheckForSADP = planLevels[i];
						planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
					}
				}

				model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
				request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
			} else {
				if (request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP) != null) {
					String[] planLevels = (String[]) request.getSession().getAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP);

					for (int i = 0; i < planLevels.length; i++) {
						if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.HIGH.toString())) {
							highCheckForSADP = planLevels[i];
							planLevelStringForSADP = getConcatedString(planLevelStringForSADP, highCheckForSADP);
						} else if (planLevels[i].equalsIgnoreCase(Plan.PlanLevelDental.LOW.toString())) {
							lowCheckForSADP = planLevels[i];
							planLevelStringForSADP = getConcatedString(planLevelStringForSADP, lowCheckForSADP);
						}
					}

					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);

				} else {
					planLevelStringForSADP = PlanMgmtConstants.ALL_PLAN_LEVELS_FOR_SADP;
					model.addAttribute(PlanMgmtConstants.HIGH_CHECK, Plan.PlanLevelDental.HIGH);
					model.addAttribute(PlanMgmtConstants.LOW_CHECK, Plan.PlanLevelDental.LOW);
					periodIDForSADP = PlanMgmtConstants.MONTHLY_PERIOD;
					marketForSADP = null;
					String[] planLevels = { Plan.PlanLevelDental.HIGH.toString().toLowerCase(), Plan.PlanLevelDental.LOW.toString().toLowerCase() };
					model.addAttribute(PlanMgmtConstants.PLAN_LEVEL, planLevels);
					request.getSession().setAttribute(PlanMgmtConstants.PLAN_LEVEL_FOR_SADP, planLevels);
				}
			}

			/*iteration depends on the number of plan levels*/
			if (planLevelStringForSADP != null) {
				LOGGER.debug("planLevelStringForSADPList for qhpgraph: " + SecurityUtil.sanitizeForLogging(String.valueOf(planLevelStringForSADP.contains(splitStringForReport))));
				boolean hasMultipleString = planLevelStringForSADP.contains(splitStringForReport);

				if (hasMultipleString) {
					String[] planLevelArrForSADPList = planLevelStringForSADP.split(splitStringForReport);
					if(null != planLevelArrForSADPList){
						for (int p = 0; p < planLevelArrForSADPList.length; p++) {
	
							EnrollmentRequest enrollmentRequestForSADPList = new EnrollmentRequest();
							Map<String, String> timeMap = new HashMap<String, String>();
							timeMap = dataForReport.setTimePeriods(periodIDForSADP.toUpperCase());
	
							if (marketForSADP != null && !(marketForSADP.isEmpty())) {
								if (marketTypeConcatStringForSADPList != null
										&& !(marketTypeConcatStringForSADPList.isEmpty())
										&& !(marketTypeConcatStringForSADPList.contains(marketForSADP))) {
									marketTypeConcatStringForSADPList = getConcatedString(marketTypeConcatStringForSADPList, marketForSADP);
									LOGGER.debug("Market_Type_Concated: " + null != marketTypeConcatStringForSADPList ? SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)) : null);
								}
								else {
									marketTypeConcatStringForSADPList = marketForSADP;
									LOGGER.debug("Market_Type_NOT_Concated: " + null != marketTypeConcatStringForSADPList ? SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)) : null);
								}
							} else {
								marketTypeConcatStringForSADPList = PlanMgmtConstants.ALL_MARKET_TYPES;
							}
							
							// Fixed HP-FOD issue
							if(null != marketTypeConcatStringForSADPList && null != marketTypeConcatStringForSADPList.split(splitStringForReport)){
								
								LOGGER.debug("Market_Type_Concated: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeConcatStringForSADPList)));
								String marketTypeArray[] = marketTypeConcatStringForSADPList.split(splitStringForReport);
								LOGGER.debug("marketTypeArray: " + SecurityUtil.sanitizeForLogging(String.valueOf(marketTypeArray.length)));
								
								for (int market = 0; market < marketTypeArray.length; market++) {
	
									if (marketTypeArray[market].equalsIgnoreCase(PlanMgmtConstants.INDIVIDUAL)) {
										enrollmentRequestForSADPList.setPlanMarket(MarketType.INDIVIDUAL);
									} else {
										enrollmentRequestForSADPList.setPlanMarket(MarketType.SHOP);
									}
	
									if (planNumberForSADP != null) {
										enrollmentRequestForSADPList.setPlanNumber(planNumberForSADP);
									} else {
										enrollmentRequestForSADPList.setPlanNumber(null);
									}
	
									if (ratingRegionAreaForSADPList != 0) {
										enrollmentRequestForSADPList.setRatingRegion(String.valueOf(ratingRegionAreaForSADPList));
									} else {
										enrollmentRequestForSADPList.setRatingRegion(null);
									}
	
									enrollmentRequestForSADPList.setPlanLevel((planLevelArrForSADPList[p].toUpperCase()));
									enrollmentRequestForSADPList.setStartDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
									enrollmentRequestForSADPList.setEndDate(DateUtil.changeFormat(timeMap.get(PlanMgmtConstants.CURRENT_DATE), PlanMgmtConstants.REQUIRED_DATE_FORMAT, PlanMgmtConstants.REQUIRED_DATE_FORMAT_FOR_REPORT));
	
//									XStream xstreamForSADPList = GhixUtils.getXStreamStaxObject();
									XStream xstreamForSADPList = GhixUtils.getXStreamStaxObject();
									String xmlRequestForSADPList = xstreamForSADPList.toXML(enrollmentRequestForSADPList);
									LOGGER.debug("enrollmentRequestForSADPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(xmlRequestForSADPList)));
	
									String enrollmentListXML = ghixRestTemplate.postForObject(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLMENTS_BY_PLAN_URL, xmlRequestForSADPList, String.class);
									LOGGER.debug("enrollmentList: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentListXML)));
	
									if (enrollmentListXML != null) {
	
										temp_enrollemntResponse = new ArrayList<EnrollmentPlanResponseDTO>();
										EnrollmentResponse enrollmentResponse = (EnrollmentResponse) xstreamForSADPList.fromXML(enrollmentListXML);
										LOGGER.debug("enrollmentResponse: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse)));
	
										if (enrollmentResponse != null) {
											temp_enrollemntResponse = enrollmentResponse.getEnrollmentPlanResponseDTOList();
	
											if (temp_enrollemntResponse != null
													&& temp_enrollemntResponse.size() > 0) {
												act_enrollListForSADPList.addAll(temp_enrollemntResponse);
												LOGGER.debug("act_enrollListForSADPList: " + SecurityUtil.sanitizeForLogging(String.valueOf(act_enrollListForSADPList.size())));
											}
										}
	
										if (act_enrollListForSADPList != null
												&& act_enrollListForSADPList.size() > 0) {
											listPageData = setDataForListPage(planLevelArrForSADPList[p], periodIDForSADP, act_enrollListForSADPList, enrollmentRequestForSADPList.getStartDate(), enrollmentRequestForSADPList.getEndDate(), PlanMgmtConstants.DENTAL, marketTypeArray[market], issuerObj);
	
											List<AdminReportFormBean> formBeans = new ArrayList<AdminReportFormBean>();
											formBeans.addAll(listPageData);
											LOGGER.debug("formBeans: " + SecurityUtil.sanitizeForLogging(String.valueOf(formBeans.size())));
	
											removeDuplicateInfo(listPageData);
											LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData.size())));
	
											if (listPageData.size() > 0) {
												for (AdminReportFormBean adminReportFormBean : listPageData) {
													HashMap<String, String> listPageMap = new HashMap<String, String>();
													Integer enrollcount = getEnrollmentCountForOneEnrollment(adminReportFormBean, formBeans);
													LOGGER.debug("enrollcount: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollcount)));
													listPageMap.put(PlanMgmtConstants.TIME_PERIOD, adminReportFormBean.getTimePeriod());
													listPageMap.put(PlanMgmtConstants.ISSUER_NAME, adminReportFormBean.getIssuerName());
													listPageMap.put(PlanMgmtConstants.PLAN_NAME, adminReportFormBean.getPlanName());
													listPageMap.put(PlanMgmtConstants.PLAN_NUMBER, adminReportFormBean.getPlanNumber());
													listPageMap.put(PlanMgmtConstants.PLAN_LEVEL, adminReportFormBean.getPlanLevel());
													listPageMap.put(PlanMgmtConstants.PLAN_MARKET, adminReportFormBean.getMarket());
													listPageMap.put(PlanMgmtConstants.ENROLLMENT, String.valueOf(enrollcount));
													listPageMapData.add(listPageMap);
												}
											}
										}
									}
								}
							}
	
						}
					}
				}
			}
			model.addAttribute(PlanMgmtConstants.PLAN_LIST, planList);
			model.addAttribute(PlanMgmtConstants.PLAN_ID, planNumberForSADP);
			model.addAttribute(PlanMgmtConstants.MARKET_ID, marketForSADP);
			model.addAttribute(PlanMgmtConstants.PERIOD_ID, periodIDForSADP);
			model.addAttribute(PlanMgmtConstants.REGION_NAME, rNameForSADPList);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_DATA, listPageMapData);
			model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, PlanMgmtConstants.PAGE_SIZE);

			if (regionList != null) {
				model.addAttribute(PlanMgmtConstants.REGION_LIST, regionList);
			}
		}
		}catch (Exception e) {			
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.QHPENROLLMENT_REPORT_LIST_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.HIGH);
		}
		return "issuer/reports/sadpenrollmentreport/list";
	}

	private String getConcatedString(String planLevelString, String levelString) {
		if (levelString != null) {
			if (planLevelString != null) {
				planLevelString = planLevelString + levelString + splitStringForReport;
			} else {
				planLevelString = levelString + splitStringForReport;
			}
		}
		return planLevelString;
	}

	@SuppressWarnings("rawtypes")
	public List<String> setDataForGraph(String planLevelString, Integer limitPeriod, String periodID, List<EnrollmentPlanResponseDTO> enrollmentList, String startDate, String endDate, String graphType, String market, Issuer issuerObj, List<String> returnList) {
		
		List<String> tempList = new ArrayList<String>();
		List<String> blankspaceList = new ArrayList<String>();
		List<String> temp_returnList = new ArrayList<String>();

		if (periodID != null && periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
			/*for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
				LOGGER.info("PlanID: " + enrollment.getPlanId() +" and Market:"+ market.toUpperCase());
				//Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
				Plan plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
				
				if(plan!=null && plan.getIssuer().getId()==issuerObj.getId()){
					LOGGER.info("PlanInfo: " + plan.getId() + plan.getName() +":::"+ plan.getInsuranceType() + plan.getMarket());
					tempList=produceGraphOutPutforMonthly(plan,enrollment,planLevelString,tempList,periodID,graphType);
					
				}
			}*/

			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), issuerObj.getId(), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), issuerObj.getId(), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, issuerObj.getId(), market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanHealth().getPlanLevel()))) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanDental().getPlanLevel()))) {
								tempList = produceGraphOutPutforMonthly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						}
						break;
					}
				}
			}
			// End changes

			blankspaceList.addAll(tempList);
			if (blankspaceList != null && blankspaceList.size() > 0) {
				LOGGER.debug("blankspaceList: " + SecurityUtil.sanitizeForLogging(String.valueOf(blankspaceList)));
				temp_returnList = dataForReport.getResultantGraph(blankspaceList, periodID);
				if (returnList.size() <= 0) {
					returnList.addAll(temp_returnList);
				} else {
					List<String> demo = new ArrayList<String>();
					demo.addAll(returnResultantList(returnList, temp_returnList));
					returnList.clear();
					returnList.addAll(demo);
				}
			}

			plans.clear();

		} else if (periodID != null
				&& periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
			float fquarter1 = 0.0f;
			float fquarter2 = 0.0f;
			float fquarter3 = 0.0f;
			float fquarter4 = 0.0f;

			// for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
			// //Plan
			// plan=planMgmtService.getPlan(enrollment.getPlan().getId());
			// Plan
			// plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()),
			// market.toUpperCase());
			// if(plan!=null && plan.getIssuer().getId()==issuerObj.getId()){
			// LOGGER.debug("QuarterlyPlanInfo: " + plan.getName() +":::"+
			// plan.getInsuranceType());
			// tempList=produceGraphOutPutforQuarterly(plan,enrollment,planLevelString,tempList,periodID,graphType);
			//
			// }
			// }

			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			// List<Plan> newPlanList = new ArrayList<Plan>();

			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), issuerObj.getId(), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), issuerObj.getId(), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, issuerObj.getId(), market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanHealth().getPlanLevel()))) {
								tempList = produceGraphOutPutforQuarterly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanDental().getPlanLevel()))) {
								tempList = produceGraphOutPutforQuarterly(plan, enrollment, planLevelString, tempList, periodID, graphType);
							}
						}
						break;
					}
				}
			}
			// End changes

			for (int k = 0; k < tempList.size(); k += 4) {
				if (!tempList.get(k).equals("")) {
					fquarter1 = fquarter1 + Float.parseFloat(tempList.get(k));
				}
				if (!tempList.get(k + 1).equals("")) {
					fquarter2 = fquarter2
							+ Float.parseFloat(tempList.get(k + 1));
				}
				if (!tempList.get(k + 2).equals("")) {
					fquarter3 = fquarter3
							+ Float.parseFloat(tempList.get(k + 2));
				}
				if (!tempList.get(k + 3).equals("")) {
					fquarter4 = fquarter4
							+ Float.parseFloat(tempList.get(k + 3));
				}
			}
			returnList.add(String.valueOf(fquarter1));
			returnList.add(String.valueOf(fquarter2));
			returnList.add(String.valueOf(fquarter3));
			returnList.add(String.valueOf(fquarter4));
			LOGGER.debug("returnList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnList)));

			plans.clear();

		} else if (periodID != null && periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
			Float pcount;
			Map<String, String> tempMap = new HashMap<String, String>();
			// for(EnrollmentPlanResponseDTO enrollment : enrollmentList){
			// //Plan
			// plan=planMgmtService.getPlan(enrollment.getPlan().getId());
			// Plan
			// plan=planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()),
			// market.toUpperCase());
			// if(plan!=null && plan.getIssuer().getId()==issuerObj.getId()){
			// LOGGER.debug("QuarterlyPlanInfo: " + plan.getName() +":::"+
			// plan.getInsuranceType());
			//
			// String
			// requiredDate=DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(),
			// REQUIRED_DATE_FORMAT), REQUIRED_DATE_FORMAT);
			// LocalDate enrollmentDate=new LocalDate(requiredDate);
			//
			// pcount=produceGraphOutPutforYearly(plan,enrollment,planLevelString,tempList,periodID,graphType);
			// if(tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))){
			// if(tempMap.get(String.valueOf(enrollmentDate.getYear()))!=null){
			// pcount=pcount+Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
			// tempMap.put(String.valueOf(enrollmentDate.getYear()),
			// String.valueOf(pcount));
			// }
			// }else{
			// tempMap.put(String.valueOf(enrollmentDate.getYear()),
			// String.valueOf(pcount));
			// }
			// }
			// }

			// Code Changes for Performance Improvement : AmitL
			List<Integer> planIdList = new ArrayList<Integer>();
			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				planIdList.add(Integer.parseInt(enrollment.getPlanId()));
			}

			List<Plan> plans = new ArrayList<Plan>();
			if (planIdList.size() > PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
				int fromIndex = 0, count = PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				while (fromIndex < planIdList.size()) {
					int remaining = planIdList.size() - fromIndex;
					if (remaining < PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT) {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, planIdList.size()), issuerObj.getId(), market.toUpperCase()));
					} else {
						plans.addAll(planMgmtService.getPlanByIDMarket(planIdList.subList(fromIndex, count), issuerObj.getId(), market.toUpperCase()));
					}

					fromIndex = count;
					count = count + PlanMgmtConstants.IN_CLAUSE_ITEM_COUNT;
				}
			} else {
				plans = planMgmtService.getPlanByIDMarket(planIdList, issuerObj.getId(), market.toUpperCase());
			}

			for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
				for (Plan plan : plans) {
					if (Integer.parseInt(enrollment.getPlanId()) == plan.getId()) {
						LOGGER.debug("PlanInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getId()))
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName())) + ":::"
								+ SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType() + plan.getMarket())));
						if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.HEALTH.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanHealth().getPlanLevel()))) {
								String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
								LocalDate enrollmentDate = new LocalDate(requiredDate);

								pcount = produceGraphOutPutforYearly(plan, enrollment, planLevelString, tempList, periodID, graphType);
								if (tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))) {
									if (tempMap.get(String.valueOf(enrollmentDate.getYear())) != null) {
										pcount = pcount
												+ Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
										tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
									}
								} else {
									tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
								}
							}
						} else if (plan.getInsuranceType().equalsIgnoreCase(Plan.PlanInsuranceType.DENTAL.toString())) {
							if ((plan.getIssuer().getId() == issuerObj.getId())
									&& (planLevelString.equalsIgnoreCase(plan.getPlanDental().getPlanLevel()))) {
								String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
								LocalDate enrollmentDate = new LocalDate(requiredDate);

								pcount = produceGraphOutPutforYearly(plan, enrollment, planLevelString, tempList, periodID, graphType);
								if (tempMap.containsKey(String.valueOf(enrollmentDate.getYear()))) {
									if (tempMap.get(String.valueOf(enrollmentDate.getYear())) != null) {
										pcount = pcount
												+ Float.parseFloat(tempMap.get(String.valueOf(enrollmentDate.getYear())));
										tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
									}
								} else {
									tempMap.put(String.valueOf(enrollmentDate.getYear()), String.valueOf(pcount));
								}
							}
						}
						break;
					}
				}
			}
			// End changes

			LOGGER.debug("tempMap: " + SecurityUtil.sanitizeForLogging(String.valueOf(tempMap)) + SecurityUtil.sanitizeForLogging(String.valueOf(tempMap.size())));
			Date currentDate = new TSDate();
			Calendar calendar = TSCalendar.getInstance();
			calendar.setTime(currentDate);
			calendar.add(Calendar.YEAR, PlanMgmtConstants.YEARLY_DEC);
			Date previousDate = calendar.getTime();
			SimpleDateFormat sdfDestination = new SimpleDateFormat(PlanMgmtConstants.REQUIRED_DATE_FORMAT);
			String newPreviousDate = sdfDestination.format(previousDate);
			LocalDate lastYearDate = new LocalDate(newPreviousDate);

			Iterator it = tempMap.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pairs = (Map.Entry) it.next();
				for (int k = 5; k >= 1; k--) {
					if (String.valueOf((lastYearDate.getYear() + k)).equals(pairs.getKey().toString())) {
						LOGGER.debug("LastYear = " + SecurityUtil.sanitizeForLogging(String.valueOf((lastYearDate.getYear() + k))));
						LOGGER.debug(SecurityUtil.sanitizeForLogging(String.valueOf(pairs.getKey())) + " = " + SecurityUtil.sanitizeForLogging(String.valueOf(pairs.getValue())));
						returnList.add(pairs.getValue().toString());
					}
				}
			}

		}
		LOGGER.debug("returnList: " + SecurityUtil.sanitizeForLogging(String.valueOf(returnList)) + "::" + SecurityUtil.sanitizeForLogging(String.valueOf(returnList.size())));
		return returnList;
	}

	public List<String> produceGraphOutPutforMonthly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		
		float plancount = 0;
		int i = 0;
		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		LocalDate lastYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE));
		LocalDate todayYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.CURRENT_DATE));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);
		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("graphType: " + SecurityUtil.sanitizeForLogging(String.valueOf(graphType)));
			LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.CURRENT_DATE))));
			LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
					+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE))));
			LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)));
			LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
			LOGGER.debug("todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));
			LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));
		}

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.getMonthOfYear() == lastYearDate.getMonthOfYear()
					&& enrollmentDate.isBefore(temp_todayYearDate)) {
				
				plancount++;
				tempList.add(i, String.valueOf(plancount));
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("lastYearDateforEnrollmentChecking: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
					LOGGER.debug("foundEnrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)));
					LOGGER.debug("plancount: " + SecurityUtil.sanitizeForLogging(String.valueOf(plancount)));
					LOGGER.debug("Iteration: " + SecurityUtil.sanitizeForLogging(String.valueOf(i)));
				}
			} else {
				tempList.add(i, "");
			}
			i++;
			lastYearDate = lastYearDate.plus(Period.months(1));
		}
		// }
		return tempList;
	}

	public List<String> produceGraphOutPutforQuarterly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		// List<PlanHealth> planList_health=null;
		// List<PlanDental> planList_dental=null;
		// if(graphType.equalsIgnoreCase("HEALTH")){
		// planList_health=planMgmtService.getPlanHealthdebug(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }else if(graphType.equalsIgnoreCase("DENTAL")){
		// planList_dental=planMgmtService.getPlanDentaldebug(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }
		//
		// if((planList_health!=null && planList_health.size()>0) ||
		// (planList_dental!=null && planList_dental.size()>0)){
		float plancount = 0;
		float q1 = 0;
		float q2 = 0;
		float q3 = 0;
		float q4 = 0;

		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.CURRENT_DATE))));
		LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE))));

		LocalDate lastYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE));
		LocalDate todayYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.CURRENT_DATE));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);
		LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)));
		LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
		LOGGER.debug("todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));

		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.getMonthOfYear() == lastYearDate.getMonthOfYear()
					&& enrollmentDate.isBefore(temp_todayYearDate)) {
				LOGGER.debug("monthInfo: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate.getMonthOfYear())));
				if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q1_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q1_END) {
					plancount++;
					q1 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q2_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q2_END) {
					plancount++;
					q2 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q3_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q3_END) {
					plancount++;
					q3 = plancount;
				} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q4_START
						&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q4_END) {
					plancount++;
					q4 = plancount;
				}
			}
			lastYearDate = lastYearDate.plus(Period.months(1));
		}
		LOGGER.debug("q1: " + SecurityUtil.sanitizeForLogging(String.valueOf(q1)));
		LOGGER.debug("q2: " + SecurityUtil.sanitizeForLogging(String.valueOf(q2)));
		LOGGER.debug("q3: " + SecurityUtil.sanitizeForLogging(String.valueOf(q3)));
		LOGGER.debug("q4: " + SecurityUtil.sanitizeForLogging(String.valueOf(q4)));

		tempList.add(String.valueOf(q1));
		tempList.add(String.valueOf(q2));
		tempList.add(String.valueOf(q3));
		tempList.add(String.valueOf(q4));
		LOGGER.debug("tempList: " + SecurityUtil.sanitizeForLogging(String.valueOf(tempList)));
		// }
		return tempList;
	}

	public Float produceGraphOutPutforYearly(Plan plan, EnrollmentPlanResponseDTO enrollment, String planLevelString, List<String> tempList, String periodID, String graphType) {
		float plancount = 0;
		// List<PlanHealth> planList_health=null;
		// List<PlanDental> planList_dental=null;
		// if(graphType.equalsIgnoreCase("HEALTH")){
		// planList_health=planMgmtService.getPlanHealthdebug(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }else if(graphType.equalsIgnoreCase("DENTAL")){
		// planList_dental=planMgmtService.getPlanDentaldebug(plan.getIssuer().getId(),
		// plan.getId(),planLevelString.toUpperCase());
		// }
		//
		// if((planList_health!=null && planList_health.size()>0) ||
		// (planList_dental!=null && planList_dental.size()>0)){
		Map<String, String> timeMap = new HashMap<String, String>();
		timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
		LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.CURRENT_DATE))));
		LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
				+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE))));

		LocalDate lastYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE));
		LocalDate todayYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.CURRENT_DATE));

		String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
		LocalDate enrollmentDate = new LocalDate(requiredDate);

		LocalDate tempNextDate = new LocalDate(lastYearDate);
		tempNextDate = lastYearDate.plus(Period.years(1));

		LOGGER.debug("enrollmentDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate)));
		LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
		LOGGER.debug("todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));

		LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
		LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));

		while (lastYearDate.isBefore(temp_todayYearDate)
				&& enrollmentDate.isBefore(temp_todayYearDate)) {
			if (enrollmentDate.isAfter(lastYearDate)
					&& enrollmentDate.isBefore(tempNextDate)) {
				plancount++;
				LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
				LOGGER.debug("tempNextDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(tempNextDate)));
			}
			lastYearDate = tempNextDate;
			tempNextDate = tempNextDate.plus(Period.years(1));
		}
		// }
		return plancount;
	}

	public List<AdminReportFormBean> setDataForListPage(String planLevelString, String periodID, List<EnrollmentPlanResponseDTO> enrollmentList, String startDate, String endDate, String graphType, String market, Issuer issuerObj) {
		List<AdminReportFormBean> listPageData = new ArrayList<AdminReportFormBean>();
		for (EnrollmentPlanResponseDTO enrollment : enrollmentList) {
			// Plan plan=planMgmtService.getPlan(enrollment.getPlan().getId());
			Map<String, String> timeMap = new HashMap<String, String>();
			timeMap = dataForReport.setTimePeriods(periodID.toUpperCase());
			LOGGER.debug("CURRENT_DATE: " + SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.CURRENT_DATE))));
			LOGGER.debug("REQUIRED_PREVIOUS_DATE: "
					+ SecurityUtil.sanitizeForLogging(String.valueOf(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE))));

			LocalDate lastYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.REQUIRED_PREVIOUS_DATE));
			LocalDate todayYearDate = new LocalDate(timeMap.get(PlanMgmtConstants.CURRENT_DATE));

			String requiredDate_Check = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
			LocalDate enrollmentDate_Check = new LocalDate(requiredDate_Check);

			LocalDate tempNextDate = new LocalDate(lastYearDate);
			tempNextDate = lastYearDate.plus(Period.years(1));

			LOGGER.debug("enrollmentDate_Check: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentDate_Check)));
			LOGGER.debug("lastYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(lastYearDate)));
			LOGGER.debug("todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(todayYearDate)));

			LocalDate temp_todayYearDate = todayYearDate.plus(Period.months(1));
			LOGGER.debug("temp_todayYearDate: " + SecurityUtil.sanitizeForLogging(String.valueOf(temp_todayYearDate)));

			if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)
					|| periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
				while (lastYearDate.isBefore(temp_todayYearDate)
						&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {
					if (enrollmentDate_Check.getMonthOfYear() == lastYearDate.getMonthOfYear()
							&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {

						getListPageData(planLevelString, enrollment, graphType, market, listPageData, periodID, issuerObj);
					}
					lastYearDate = lastYearDate.plus(Period.months(1));
				}
			} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
				while (lastYearDate.isBefore(temp_todayYearDate)
						&& enrollmentDate_Check.isBefore(temp_todayYearDate)) {
					if (enrollmentDate_Check.isAfter(lastYearDate)
							&& enrollmentDate_Check.isBefore(tempNextDate)) {
						getListPageData(planLevelString, enrollment, graphType, market, listPageData, periodID, issuerObj);
					}
					lastYearDate = tempNextDate;
					tempNextDate = tempNextDate.plus(Period.years(1));
				}
			}

		}
		return listPageData;
	}

	public void getListPageData(String planLevelString, EnrollmentPlanResponseDTO enrollment, String graphType, String market, List<AdminReportFormBean> listPageData, String periodID, Issuer issuerObj) {
		List<PlanHealth> planHealth_List = new ArrayList<PlanHealth>();
		List<PlanDental> planDental_List = new ArrayList<PlanDental>();

		Plan plan = planMgmtService.getPlanByIDMarket(Integer.parseInt(enrollment.getPlanId()), market.toUpperCase());
		if (plan != null && plan.getIssuer().getId() == issuerObj.getId()) {
			LOGGER.debug("PlanInfoInsetDataForListPage: " + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getName()))
					+ ":::" + SecurityUtil.sanitizeForLogging(String.valueOf(plan.getInsuranceType())) + ":::" + SecurityUtil.sanitizeForLogging(String.valueOf(graphType)));

			if (plan.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.HEALTH)) {
				planHealth_List = iPlanHealthRepository.getPlanHealthInfoList(plan.getId(), planLevelString.toUpperCase());
				LOGGER.debug("planHealth_List: " + SecurityUtil.sanitizeForLogging(String.valueOf(planHealth_List)));

				if (planHealth_List != null && planHealth_List.size() > 0) {
					for (PlanHealth planHealth : planHealth_List) {
						if (planHealth.getPlanLevel().equals(planLevelString.toUpperCase())) {
							String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
							LocalDate enrollmentDate = new LocalDate(requiredDate);
							AdminReportFormBean adminReportFormBean = new AdminReportFormBean();
							if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(enrollmentDate.toString("MMM-yyyy"));
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
								String quaterString = getQuarterValue(enrollmentDate);
								adminReportFormBean.setTimePeriod(quaterString);
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(String.valueOf(enrollmentDate.getYear()));
							}

							Issuer issuer = issuerService.getIssuerById(plan.getIssuer().getId());
							adminReportFormBean.setIssuerName(issuer.getName());
							adminReportFormBean.setPlanName(plan.getName());
							adminReportFormBean.setPlanNumber(plan.getIssuerPlanNumber());
							adminReportFormBean.setCostSharing(plan.getPlanHealth().getCostSharing());
							adminReportFormBean.setPlanLevel(planHealth.getPlanLevel());
							adminReportFormBean.setMarket(plan.getMarket());
							LOGGER.debug("getPlanID: " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollment.getPlanId())));
							adminReportFormBean.setEnrollcount("0");
							listPageData.add(adminReportFormBean);
							LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData)));
						}
					}
				}
			} else if (plan.getInsuranceType().equalsIgnoreCase(PlanMgmtConstants.DENTAL)) {
				planDental_List = iPlanDentalRepository.getPlanDentalInfoList(plan.getId(), planLevelString.toUpperCase());
				LOGGER.debug("planDental_List: " + SecurityUtil.sanitizeForLogging(String.valueOf(planDental_List)));
				if (planDental_List != null && planDental_List.size() > 0) {
					for (PlanDental planDental : planDental_List) {
						if (planDental.getPlanLevel().equals(planLevelString.toUpperCase())) {
							String requiredDate = DateUtil.dateToString(DateUtil.StringToDate(enrollment.getUpdatedDate(), PlanMgmtConstants.REQUIRED_DATE_FORMAT), PlanMgmtConstants.REQUIRED_DATE_FORMAT);
							LocalDate enrollmentDate = new LocalDate(requiredDate);
							AdminReportFormBean adminReportFormBean = new AdminReportFormBean();
							if (periodID.equalsIgnoreCase(PlanMgmtConstants.MONTHLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(enrollmentDate.toString("MMM-yyyy"));
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.QUARTERLY_PERIOD)) {
								String quaterString = getQuarterValue(enrollmentDate);
								adminReportFormBean.setTimePeriod(quaterString);
							} else if (periodID.equalsIgnoreCase(PlanMgmtConstants.YEARLY_PERIOD)) {
								adminReportFormBean.setTimePeriod(String.valueOf(enrollmentDate.getYear()));
							}

							Issuer issuer = issuerService.getIssuerById(plan.getIssuer().getId());
							adminReportFormBean.setIssuerName(issuer.getName());
							adminReportFormBean.setPlanName(plan.getName());
							adminReportFormBean.setPlanNumber(plan.getIssuerPlanNumber());
							adminReportFormBean.setPlanLevel(planDental.getPlanLevel());
							adminReportFormBean.setMarket(plan.getMarket());
							adminReportFormBean.setEnrollcount("0");
							listPageData.add(adminReportFormBean);
							LOGGER.debug("listPageData: " + SecurityUtil.sanitizeForLogging(String.valueOf(listPageData)));
						}
					}
				}
			}
		}
	}

	public void removeDuplicateInfo(List<AdminReportFormBean> listPageData) {
		for (int i = 0; i < listPageData.size(); i++) {
			String timePeriod = listPageData.get(i).getTimePeriod();
			String level = listPageData.get(i).getPlanLevel();
			String market = listPageData.get(i).getMarket();
			String name = listPageData.get(i).getPlanName();
			for (int j = i + 1; j < listPageData.size(); j++) {
				if (timePeriod.equals(listPageData.get(j).getTimePeriod())
						&& level.equals(listPageData.get(j).getPlanLevel())
						&& market.equals(listPageData.get(j).getMarket())
						&& name.equalsIgnoreCase(listPageData.get(j).getPlanName())) {
					listPageData.remove(j--);
				}
			}
		}
	}

	public Integer getEnrollmentCountForOneEnrollment(AdminReportFormBean adminReportFormBean, List<AdminReportFormBean> formBeans) {
		Integer count = 0;
		for (int k = 0; k < formBeans.size(); k++) {
			if (adminReportFormBean.getTimePeriod().contains(formBeans.get(k).getTimePeriod())
					&& adminReportFormBean.getPlanName().equalsIgnoreCase(formBeans.get(k).getPlanName())) {
				count++;
			}
		}
		return count;
	}

	public String getQuarterValue(LocalDate enrollmentDate) {
		String quaterString = null;
		if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q1_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q1_END) {
			quaterString = "Q1";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q2_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q2_END) {
			quaterString = "Q2";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q3_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q3_END) {
			quaterString = "Q3";
		} else if (enrollmentDate.getMonthOfYear() >= PlanMgmtConstants.Q4_START
				&& enrollmentDate.getMonthOfYear() <= PlanMgmtConstants.Q4_END) {
			quaterString = "Q4";
		}
		return quaterString;
	}

	private List<String> returnResultantList(List<String> resultant, List<String> temp_resultant) {
		Integer fval = 0;
		Integer sval = 0;
		Integer sum = 0;
		List<String> final_resultList = new ArrayList<String>();
		for (int i = 0; i < resultant.size(); i++) {
			if (resultant.get(i) != null && !(resultant.get(i).isEmpty())) {
				fval = Integer.parseInt(resultant.get(i));
			}
			if (temp_resultant.get(i) != null
					&& !(temp_resultant.get(i).isEmpty())) {
				sval = Integer.parseInt(temp_resultant.get(i));
			}
			if (fval != 0 && sval != 0) {
				sum = fval + sval;
				final_resultList.add(String.valueOf(sum).trim());
				sum = 0;
				fval = 0;
				sval = 0;
			} else if (fval != 0 && sval == 0) {
				final_resultList.add(String.valueOf(fval).trim());
				sum = 0;
				fval = 0;
			} else if (fval == 0 && sval != 0) {
				final_resultList.add(String.valueOf(sval).trim());
				sum = 0;
				sval = 0;
			} else {
				final_resultList.add(String.valueOf("").trim());
			}
		}
		LOGGER.debug("final_resultList: " + SecurityUtil.sanitizeForLogging(String.valueOf(final_resultList)));
		return final_resultList;
	}

	@SuppressWarnings("unused")
	private AdminReportFormBean createDummyAdminReportFormBean() {
		AdminReportFormBean adminReportFormBean = new AdminReportFormBean();
		adminReportFormBean.setCostSharing("CS1");
		adminReportFormBean.setEnrollcount("EnrlCount");
		adminReportFormBean.setEnrollmentCount("EnrlmtCount");
		adminReportFormBean.setIssuerName("IssuerOne");
		adminReportFormBean.setMarket("Market");
		adminReportFormBean.setPlanLevel("Platinum");
		adminReportFormBean.setPlanName(PlanMgmtConstants.PLAN_NAME);
		adminReportFormBean.setPlanNumber("PLN007");
		adminReportFormBean.setTimePeriod("MONTHLY_PERIOD");

		return adminReportFormBean;
	}
	
	
	/* END : HIX-1811
	 * @author raja
	 * @UserStory : HIX-1811
	 */

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/planmgmt/issuer/indvmembers")
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String manageIndvMemebrs(Model model, HttpServletRequest request) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
				
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep, if issuer status is registered then re-directing to the new
													  // issuer application steps to submit
				String postResp_ = null;
				int currentPage = 1;
				Long iResultCt = (long) 0;
				List<Map<String, Object>> applicationlist = null;
	
				List<String> plantypes = new ArrayList<String>();
				for (PlanInsuranceType plantype : PlanInsuranceType.values()) {
					plantypes.add(plantype.toString());
				}
	
				Map<String, Object> searchCriteria = new HashMap<String, Object>();
				searchCriteria = issuerService.createEnrolleeSearchCriteria(request);
				if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ENROLLED)){
					searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_CONFIRMED); 
				}
				if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ORDER_ED)){
					searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_ORDER_CONFIRMED); 
				}

				searchCriteria.put("issuer", issuerService.getIssuerId().toString());
				searchCriteria.put("lookup", GhixConstants.ENROLLMENT_TYPE_INDIVIDUAL);
	
				postResp_ = ghixRestTemplate.postForObject(EnrollmentEndPoints.SEARCH_ENROLLEE_URL, String.class, searchCriteria);
	
				if (postResp_ != null) { // if response is not empty
//					XStream xstream = GhixUtils.getXStreamStaxObject();
					XStream xstream = GhixUtils.getXStreamStaxObject();
					EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
					enrolleeResponse = (EnrolleeResponse) xstream.fromXML(postResp_);
	
					Map<String, Object> enrolleesAndRecordCount = enrolleeResponse.getSearchResultAndCount();
	
					applicationlist = (List<Map<String, Object>>) enrolleesAndRecordCount.get("enroleelist");
					iResultCt = (Long) enrolleesAndRecordCount.get("recordCount");
	
					String param = request.getParameter(PlanMgmtConstants.PAGE_NUMBER);
					if (param != null) {
						currentPage = Integer.parseInt(param);
					}
				}
				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Individual market members");
				model.addAttribute("plantypes", plantypes);
				model.addAttribute(PlanMgmtConstants.APPLICATIONLIST, applicationlist);
				model.addAttribute("searchCriteria", searchCriteria);
				model.addAttribute("resultSize", (iResultCt != null) ? iResultCt.intValue()
						: 0);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
				model.addAttribute(PlanMgmtConstants.CURRENT_PAGE, currentPage);
				model.addAttribute("issuerid", issuerService.getIssuerId());
	
				return "planmgmt/issuer/indvmembers";
	
			}
		} catch (RestClientException re) {
			LOGGER.error("Message : ", re.getMessage());
			LOGGER.error("Cause : ", re.getCause());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.REST_CLIENT_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(re), Severity.LOW);
		} catch (Exception e) {
			LOGGER.error("Exception in manageIndvMemebrs : ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_INDIV_MEMBERS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/planmgmt/issuer/shopmembers")
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String manageShopMemebrs(Model model, HttpServletRequest request) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is // issuer or issuer rep
				LOGGER.info("Issuer portal : manage SHOP market members");
				Issuer issuerObj = issuerService.getIssuerById(issuerService.getIssuerId());
				model.addAttribute("issuerObj", issuerObj);
	
				// if issuer status is registered then re-directing to the new issuer application steps to submit
				String postResp_ = null;
				Long iResultCt = (long) 0;
				List<Map<String, Object>> applicationlist = null;
	
				List<String> plantypes = new ArrayList<String>();
				for (PlanInsuranceType plantype : PlanInsuranceType.values()) {
					plantypes.add(plantype.toString());
				}
				Map<String, Object> searchCriteria = issuerService.createEnrolleeSearchCriteria(request);
				if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ENROLLED)){
					searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_CONFIRMED); 
				}
				if(StringUtils.isNotBlank(request.getParameter(PlanMgmtConstants.STATUS)) && request.getParameter(PlanMgmtConstants.STATUS).equals(ENROLLMENT_STATUS_ORDER_ED)){
					searchCriteria.put(PlanMgmtConstants.STATUS, ENROLLMENT_STATUS_ORDER_CONFIRMED); 
				}
				searchCriteria.put("issuer", issuerService.getIssuerId().toString());
				searchCriteria.put("lookup", GhixConstants.ENROLLMENT_TYPE_SHOP);
	
				/**
				 * Following Code is added by Pratap panda
				 * 
				 * For Modularization purpose.
				 */
				// set issuer id to fetch issuer specific records
				postResp_ = ghixRestTemplate.postForObject(EnrollmentEndPoints.SEARCH_ENROLLEE_URL, String.class, searchCriteria);
				if (postResp_ != null) { // if response is not empty
//					XStream xstream = GhixUtils.getXStreamStaxObject();
					XStream xstream = GhixUtils.getXStreamStaxObject();
					EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
					enrolleeResponse = (EnrolleeResponse) xstream.fromXML(postResp_);
	
					Map<String, Object> enrolleesAndRecordCount = enrolleeResponse.getSearchResultAndCount();
	
					/**
					 * End of Modularization changes by Pratap panda
					 */
					applicationlist = (List<Map<String, Object>>) enrolleesAndRecordCount.get("enroleelist");
					iResultCt = (Long) enrolleesAndRecordCount.get("recordCount");
	
				}
				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "SHOP market members");
				model.addAttribute("plantypes", plantypes);
				model.addAttribute(PlanMgmtConstants.APPLICATIONLIST, applicationlist);
				model.addAttribute("searchCriteria", searchCriteria);
				model.addAttribute("resultSize", (iResultCt != null) ? iResultCt.intValue() : 0);
				model.addAttribute(PlanMgmtConstants.LIST_PAGE_SIZE, GhixConstants.PAGE_SIZE);
	
				return "planmgmt/issuer/shopmembers";
			}
		} catch (RestClientException re) {
			LOGGER.error("Message : ", re.getMessage());
			LOGGER.error("Cause : ", re.getCause());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.REST_CLIENT_EXCEPTION.getCode(), null,
					ExceptionUtils.getFullStackTrace(re), Severity.LOW);
		} catch (Exception e) {
			LOGGER.error("Exception in manageIndvMemebrs : ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.MANAGE_SHOP_MEMBERS_EXCEPTION.getCode(),
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@RequestMapping(value = "/planmgmt/issuer/viewindvmember/{id}")
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String viewIndvMemebrs(Model model, @PathVariable("id") String enrolleeId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
				
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				LOGGER.info("Issuer portal : view Individual market members");
				String getResp_ = null;
				getResp_ = ghixRestTemplate.getForObject(EnrollmentEndPoints.FIND_ENROLLEE_BY_ID_URL + enrolleeId, String.class);

//				XStream xstream = GhixUtils.getXStreamStaxObject();
				XStream xstream = GhixUtils.getXStreamStaxObject();
				EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
				enrolleeResponse = (EnrolleeResponse) xstream.fromXML(getResp_);
				Enrollee enrollee = enrolleeResponse.getEnrollee();

				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "Individual market member details");
				model.addAttribute("enrollee", enrollee);
				model.addAttribute(PlanMgmtConstants.ISSUERID, issuerService.getIssuerId());

				return "planmgmt/issuer/viewindvmember";
			}
		} catch (RestClientException re) {
			LOGGER.error("Message : ", re.getMessage());
			LOGGER.error("Cause : ", re.getCause());
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.REST_CLIENT_EXCEPTION.getCode(), null, ExceptionUtils.getFullStackTrace(re), Severity.LOW);
		} catch (Exception ex) {
			LOGGER.error("Issuer portal : fail to access individual market members" + ex.getMessage());
			return "redirect:/planmgmt/issuer/indvmembers";
		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

	@RequestMapping(value = "/planmgmt/issuer/viewshopmember/{id}")
	@PreAuthorize(PlanMgmtConstants.VIEW_ISSUER_PROFILE_PERMISSION)
	public String viewShopMemebrs(Model model, @PathVariable("id") String enrolleeId) {
		
		// redirect suspended issuer representative to suspend screen
		if(issuerService.isIssuerRepSuspended()){
			return PlanMgmtConstants.SUSPEND_REPRESENTATIVE_REDIRECT_URL;
		}
		
		try {
			if (issuerService.isIssuerRepLoggedIn()) { // if logged in user is issuer or issuer rep
				LOGGER.info("Issuer portal : view SHOP market members");
				model.addAttribute(PlanMgmtConstants.PAGE_TITLE, "SHOP market member details");
				String getResp_ = null;
				getResp_ = ghixRestTemplate.getForObject(EnrollmentEndPoints.FIND_ENROLLEE_BY_ID_URL + enrolleeId, String.class);
//				XStream xstream = GhixUtils.getXStreamStaxObject();
				XStream xstream = GhixUtils.getXStreamStaxObject();
				EnrolleeResponse enrolleeResponse = new EnrolleeResponse();
				enrolleeResponse = (EnrolleeResponse) xstream.fromXML(getResp_);
				Enrollee enrollee = enrolleeResponse.getEnrollee();
				model.addAttribute("enrollee", enrollee);
				model.addAttribute(PlanMgmtConstants.ISSUERID, issuerService.getIssuerId());
				return "planmgmt/issuer/viewshopmember";
			}
		} catch (Exception e) {
			LOGGER.error("Exception in viewShopMemebrs : ", e);
			throw new GIRuntimeException(PlanMgmtConstants.MODULE_NAME + PlanMgmtErrorCodes.ErrorCode.VIEW_SHOP_MEMBERS_EXCEPTION.getCode(), 
					null, ExceptionUtils.getFullStackTrace(e), Severity.LOW);

		}
		return PlanMgmtConstants.ISSUER_LOGIN_PATH;
	}

}
