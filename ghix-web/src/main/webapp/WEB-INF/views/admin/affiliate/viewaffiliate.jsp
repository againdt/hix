<%@page import="java.util.ArrayList"%>
<%@page import="antlr.collections.List"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="com.getinsured.hix.platform.util.*"   %>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}
.dob label {display: inline;}

</style>





<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/general.css" />" media="screen" />
<style>
.table th, .table th a {
	background:#666;
	color:#fff;
}
table th {
	height:22px;
}
</style>

<script type="text/javascript" src="<:url value="/resources/js/affiliate/jquery.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/affiliate/parsley.js" />"></script>
 <script type="text/javascript" src="<c:url value="/resources/js/affiliate/highlight.min.js" />"></script>

<c:set var="PAGE_NAME" value="viewaffiliate"/>
<div class="row-fluid">
	<div class="gutter10">
        <div id="skip" class="hide"></div>
        <ul class="page-breadcrumb">
            <li><a href="/hix/admin/affiliate/manage">&lt;  Back</a></li>
            <li><a href="/hix/admin/affiliate/manage">Manage Affiliate</a></li>
            <li><a href="#">View Affiliate</a></li>
        </ul><!--page-breadcrumb ends-->
        <h1>View Affiliate</h1>
	</div>
</div>

<div class="row-fluid">	
		<div style="font-size: 14px; color: red">
			<c:if test="${errorMsg != ''}">
				<p><c:out value="${errorMsg}"></c:out><p/>
			</c:if>
			<br>
		</div>
</div>
<div class="gutter10">
	<div class="row-fluid"></div>
<input type="hidden" name="affiliateId" id="affiliateId" value="${affiliateId}">

	<table class="table table-condensed table-border-none">
		<tbody>
			<tr>
				<td class="txt-right">Company Name</td>
				<td><strong>${affiliate.companyName}</strong></td>
			</tr>
			<tr>
				<td class="txt-right">Address</td>
				<td><strong>${affiliate.location.address1}</strong></td>
				
				<td class="txt-right">Zip</td>
				<td><strong>${affiliate.location.zip}</strong></td>
				
				<td class="txt-right">Email</td>
				<td><strong>${affiliate.email}</strong></td>
				
			</tr>
			<tr>
				<td class="txt-right">City</td>
				<td><strong>${affiliate.location.city}</strong></td>
				
				<td class="txt-right">Website</td>
				<td><strong>${affiliate.website}</strong></td>
				
				<td class="txt-right">Phone</td>
				<td><strong>${affiliate.phone}</strong></td>
				
			</tr>
			<tr>
				<td class="txt-right">State</td>
				<td class="span2"><strong>${affiliate.location.state}</strong></td>
				<td class="txt-right">Account Executive</td>
				<td><strong>${affiliate.accountExecutive}</strong></td>
				<td class="txt-right">Affiliate Manager</td>
				<td><strong>${affiliate.managerName}</strong></td>
			</tr>
			<tr>
				<td class="txt-right">API Key</td>
				<td><strong>${affiliate.apiKey}</strong></td>
			</tr>
		
			
		</tbody>
	</table>
	<div> 
	<hr class="marginTop10">
	<a href="<c:url value="/admin/affiliate/addflow/${affiliate.affiliateId}"/>" class="btn btn-primary small primary addmember offset9 pull-right" id="addflow" name="addflow">Add Flow</a>
	<div  class="clrflt">&nbsp;</div>
	<hr>
	</div>
	<div  style="height:300px; overflow-x: auto; overflow-y: auto;">
	<table class="table table-striped">
		<thead>
		 <tr>
			<th>Id</th>
			<th>Flow</th>
			<th>Status</th>
			<th>Type of Traffic</th>
			<th>IVR #</th>
			<th>Web Pricing Model</th>
			<th>Web Payout($/%)</th>
			<th>Call Pricing Model</th>
			<th>Call Payout($/%)</th>
			<th>Portal</th>
			<th>Campaign</th>
			<th></th>
		 <tr>
		</thead>
		<tbody>
		 <c:forEach items="${affiliate.affiliateFlowList}" var="affFlow">
		 <tr>
		 <td>${affFlow.affiliateflowId} </td>
		 <td>${affFlow.flowName} </td>
		 <td>${affFlow.flowStatus} </td>
		 <td>${affFlow.trafficType} </td>
		 <td>${affFlow.ivrNumber} </td>
		 <td>${affFlow.webPricingModel} </td>
		 <td>
		 <c:if test="${affFlow.webPayoutType == 'FIXED' }">
              $ ${affFlow.webPayoutvalue}
         </c:if>
         <c:if test="${affFlow.webPayoutType == 'PERCENTAGE' }">
              ${affFlow.webPayoutvalue} %
         </c:if>
		 </td>
		 <td>${affFlow.callPricingModel} </td>
		 <td>
		 <c:if test="${affFlow.callPayoutType == 'FIXED_PAYOUT' }">
              $ ${affFlow.callPayoutvalue}
         </c:if>
         <c:if test="${affFlow.callPayoutType == 'PERCENTAGE' }">
              ${affFlow.callPayoutvalue} %
         </c:if>
		 </td>
		 <td>View </td>
		 <td>
		 <a href="<c:url value="/admin/affiliate/addcampaign/${PAGE_NAME}/${affiliate.affiliateId}/${affFlow.affiliateflowId}"/>"  id="createView" name="createView">Create</a>
		 </td>
		 
		  <td>
		 <div class="dropdown">
		 <a class="dropdown-toggle" data-toggle="dropdown" href="#"><i class="icon-cog"></i><i class="caret"></i></a>
		 <ul class="dropdown-menu dropdown-left" role="menu" aria-labelledby="dLabel">
		 <li><a href="<c:url value="/admin/affiliate/viewflow/${affiliate.affiliateId}/${affFlow.affiliateflowId}" />"><i class="icon-refresh"></i>View</a></li>
		 <li><a href="<c:url value="/admin/affiliate/editflow/${PAGE_NAME}/${affiliate.affiliateId}/${affFlow.affiliateflowId}" />"><i class="icon-pencil"></i>Edit</a></li>
		 </ul>
		 </div>
		 </td>
	
		 </tr>
		  
		  
		 </c:forEach>
		</tbody>
		
	</table>
	</div>
	<!--row-fluid-->
</div>
<!--gutter10-->

    