<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ page isErrorPage="true" %>

<div class="row-fluid">
	<div class="txt-center margin100">
		<p><spring:message code="pd.label.timeout.errorMsg"/></p>
        <div class="margin30-t">
			<a href="<c:url value="/" />" class="btn btn-primary margin20-l"><spring:message code="pd.button.showcart.goBack"/></a>
		</div>
	</div>
</div>