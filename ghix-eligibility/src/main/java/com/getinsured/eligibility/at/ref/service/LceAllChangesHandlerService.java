package com.getinsured.eligibility.at.ref.service;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination;
import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination.AllChangesHandlerType;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.dto.enrollment.EnrolleeShopDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;


/**
 * @author chopra_s
 * 
 */
@Component("lceAllChangesHandlerService")
@Scope("singleton")
public class LceAllChangesHandlerService extends LceAllChangesHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceAllChangesHandlerService.class);

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	
	@Autowired private SsapEnrolleeHandler ssapEnrolleeHandler;
	 	
	@Override
	//@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceAllChangesHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		final SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		
		
		final SsapApplicant primaryApplicant = ReferralUtil.retreiveApplicant(currentApplication, ReferralConstants.PRIMARY);
		final Map<String, List<ApplicantEvent>> applicantExtensionEvents = ApplicationExtensionEventUtil.populateApplicantEventsFromExtension(currentApplication.getSsapApplicants(), lceProcessRequestDTO.getApplicationExtension(), primaryApplicant);
		final Map<String, Boolean> applicantWithInvalidExtensionEvents = ApplicationExtensionEventUtil.populateInvalidApplicantEventsFromExtension(currentApplication.getSsapApplicants(), lceProcessRequestDTO.getApplicationExtension(), primaryApplicant);
		final AllChangesHandlerType handlerToInvoke = LceAllChangesHandlerDetermination.handlerToInvoke(currentApplication, applicantExtensionEvents, lceProcessRequestDTO.getEnrolledApplicationAttributes(), applicantWithInvalidExtensionEvents);
		LOGGER.info("Handler to invoke - " + handlerToInvoke);
		if (AllChangesHandlerType.SINGLE.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.SINGLE + " handler for " + currentApplication.getId());
			handleSingle(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.PURE_REMOVE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.PURE_REMOVE + " handler for " + currentApplication.getId());
			handlePureRemove(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.PURE_CS_CHANGE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.PURE_CS_CHANGE + " handler for " + currentApplication.getId());
			handleCSAutomation(currentApplication, lceProcessRequestDTO, applicantExtensionEvents, handlerToInvoke);
		} else if (AllChangesHandlerType.CS_CHANGE.equals(handlerToInvoke)){
			LOGGER.info("Invoking case " + AllChangesHandlerType.CS_CHANGE + " handler for " + currentApplication.getId());
			handleCSAutomation(currentApplication, lceProcessRequestDTO, applicantExtensionEvents, handlerToInvoke);
		} else if (AllChangesHandlerType.MIXED.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.MIXED + " handler for " + currentApplication.getId());
			handleMixed(currentApplication, lceProcessRequestDTO, applicantExtensionEvents);
		} else if (AllChangesHandlerType.DENIAL.equals(handlerToInvoke)) {
			LOGGER.info("Invoking case " + AllChangesHandlerType.DENIAL + " handler for " + currentApplication.getId());
			handleDenial(currentApplication, applicantExtensionEvents);
		} else {
			LOGGER.info("Invoking case " + AllChangesHandlerType.MANUAL + " handler for " + currentApplication.getId());
			handleManual(currentApplication,applicantExtensionEvents);
		}
		/**
		 * In MN, we need to support only dental enrollment. In such cases the application status is in ER and application dental status is EN.
		 * Whenever all changes handler is invoked and manual flow is determined, we We run into situation where 2 applications are in ER status. 
		 * Following method is called to specifically handle this scenario after 
		 * All changes handler and QLE handler execution completes.
		 * 
		 * */
		super.closePreviousERApplication(currentApplication,lceProcessRequestDTO);
		LOGGER.info("LceAllChangesHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}
	
	
	
	private void handleCSAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, AllChangesHandlerType handlerToInvoke) {
		
		boolean pureCsChange = handlerToInvoke == AllChangesHandlerType.PURE_CS_CHANGE ? true : false;
		final SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		SsapApplicationEvent appEvent = ssapApplicationEventService.createApplicationEventForCSChanges(currentApplication, applicantExtensionEvents, pureCsChange,enrolledApplication);
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		if (appEvent != null){
			Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
			
			currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
			String changePlan = "Y";
			if (handlerToInvoke == AllChangesHandlerType.PURE_CS_CHANGE){
				ignoreGatedNonPrimaryEvents(currentApplication.getCaseNumber());
			}
			if(isApplicationValidated(validationStatus)){
				// if this code is invoked for member add + cs change, check if auto add is allowed. If this is invoked from pure cs change, check if auto cs change is allowed.
				boolean isCSAutomationAllowed = isAddCase(currentApplication) ? super.isAutoAddAllowed(lceProcessRequestDTO) : super.isAutoCSChangeAllowed(lceProcessRequestDTO);
				if(isCSAutomationAllowed){
					String previousDentalApplicationStatus = enrolledApplication.getApplicationDentalStatus();
					String previousApplicationStatus = enrolledApplication.getApplicationStatus();
					boolean doAutomation = true;
					
					boolean isCustomGrouping = isCustomGroupingAllowed();
					
					if(isCustomGrouping && !sameCSLevel(currentApplication)){
						doAutomation = false;
					}
					
					if(doAutomation) {
					
					final AptcUpdate aptcUpdate = populateAptcUpdateRequest(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId(),coverageStartDate);
					if (aptcUpdate == null) {
						LOGGER.info("APTC redistribution failure - " + currentApplication.getId());
						processMixed(currentApplication, true, lceProcessRequestDTO);
					} else {
							final boolean status = ind71GCall(lceProcessRequestDTO.getEnrolledApplicationId(), lceProcessRequestDTO.getCurrentApplicationId(),currentApplication.getCoverageYear(), aptcUpdate,changePlan) ;
								if (status) {
									LOGGER.info("IND71G update successful for  " + currentApplication.getId());
									
									processCSSuccess(currentApplication.getId(), lceProcessRequestDTO.getEnrolledApplicationId(),previousApplicationStatus,previousDentalApplicationStatus);
									if(isAddCase(currentApplication) && "CA".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE))) {
										triggerAutomationAddRemoveNotice(currentApplication.getCaseNumber());
									}else {
										triggerEmailForCSAutomation(currentApplication,lceProcessRequestDTO);
									}
								} else {
									LOGGER.info("IND71G update failed for  " + currentApplication.getId());
									processCSFailure(currentApplication, lceProcessRequestDTO);
								}
						}
					}else {
						LOGGER.info("Different CS level so no auto add - " + currentApplication.getId());
						processMixed(currentApplication, true, lceProcessRequestDTO);
				}
				}
				else {
					handleManual(currentApplication,applicantExtensionEvents); //TODO: check is status == ER
				}
			}
			else{
				handlePendingValidation(currentApplication);
			}
		} else {
			handleManual(currentApplication,applicantExtensionEvents); //TODO: check is status == ER
		}
	}

	void triggerEmailForCSAutomation(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO) {
		// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
		String stateCode = DynamicPropertiesUtil
				.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		if (stateCode.equalsIgnoreCase("MN")) {
			// check for AI/AN change
			boolean nativeAmericanChange = checkAIANChange(currentApplication,lceProcessRequestDTO.getEnrolledApplicationId());
			// check for APTC / CSR change
			boolean lossAPTC_CSRChange = checkLossOfAptcCsrHH(currentApplication,lceProcessRequestDTO.getEnrolledApplicationId());
			// check for APTC/CSR newly eligible
			boolean newEigibleForAPTC_CSR = checknewEigibleAptcCsrHH(currentApplication,lceProcessRequestDTO.getEnrolledApplicationId());
			boolean lossofCSR_ind = false;
			if(nativeAmericanChange) {
				// trigger EE066 notice if AI/AN change
				triggerNativeAmericanStatusNotice(currentApplication);	
			} 
			if(lossAPTC_CSRChange) {
				// trigger EE058 notice if Loss of APTC/CSR eligibility for entire HH
				triggerNonFinConversionNoticeWithAptcCsrIneligibile(currentApplication.getCaseNumber());
			}
			else {
				lossofCSR_ind = checkLossofCSR_IND(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId());
				if (lossofCSR_ind) {
					triggerLossofCSRNotice(currentApplication);
				}
		
			}
			if(newEigibleForAPTC_CSR) {
				// trigger EE032(newly eligible for APTC)
				if(isCSChangeEffected(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId()))
					triggerNFtoFinancialConversionEmail(currentApplication);
				else
					triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);
				
			}
			
			if(!nativeAmericanChange && !lossAPTC_CSRChange && !newEigibleForAPTC_CSR && !lossofCSR_ind) {
				// trigger existing EE060 if no AI/AN change and APTC_CSR loss for entire HH
				if(isCSChangeEffected(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId()))
					triggerAutomationCSNotice(currentApplication.getCaseNumber());
				else
					triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);
			}	
		} else {
			// for all other states continue with the old notice
			if(isCSChangeEffected(currentApplication, lceProcessRequestDTO.getEnrolledApplicationId()))
				triggerAutomationCSNotice(currentApplication.getCaseNumber());
			else
				triggerChangeActionEmail(currentApplication, lceProcessRequestDTO, true);
		}
	}
	
	private void handlePureRemove(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.PURE_REMOVE + " case for " + currentApplication.getId());
		ssapApplicationEventService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents, true);
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
		String changePlan = "N";
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		if(isApplicationValidated(validationStatus)){
			handleAutomation(currentApplication, lceProcessRequestDTO,coverageStartDate,changePlan);
		}else{
			handlePendingValidation(currentApplication);
		}
	}

	

	private void handleSingle(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.SINGLE + " case for " + currentApplication.getId());
		ssapApplicationEventService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
		Date coverageStartDate = updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		Optional<ApplicationValidationStatus> validationStatus = getValidationStatus(currentApplication.getCaseNumber());
		String changePlan = "Y";
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		if(isApplicationValidated(validationStatus)){
			if(isAutoAddAllowed(lceProcessRequestDTO)){
				handleAutomation(currentApplication, lceProcessRequestDTO, coverageStartDate,changePlan);	
			}
			else{
				handleManual(currentApplication,applicantExtensionEvents); 
			}
		}else{
			handlePendingValidation(currentApplication);
		}
	}

	private void handleMixed(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.MIXED + " case for " + currentApplication.getId());
		ssapApplicationEventService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		processMixed(currentApplication, true, lceProcessRequestDTO);
	}

	private void handleDenial(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		LOGGER.info("Handle " + AllChangesHandlerType.DENIAL + " case for " + currentApplication.getId());
		ssapApplicationEventService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
		updateEffectiveDate(currentApplication,ssapApplicationEventRepository.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId()));
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplication.getId());////HIX-108047
		
		processDenial(currentApplication.getCaseNumber());
	}

	private void handleManual(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		//TODO will comment below allow enrollment line when we capture events in UI
		final String askQleOEPFlag =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ASK_QLE_IN_OEP);
		boolean isOEPeriod = false;
		if(askQleOEPFlag.equalsIgnoreCase(Boolean.FALSE.toString()) && LceAllChangesHandlerDetermination.isOpenEnrollment(currentApplication.getCoverageYear()) && LceAllChangesHandlerDetermination.isOepEsd(currentApplication.getStartDate())){
			SsapApplicationEvent applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getId());
			if(null == applicationEvent) {
				ssapApplicationEventService.createApplicationEventForAllChanges(currentApplication, applicantExtensionEvents);
				isOEPeriod = true;
			}			
		}
		updateAllowEnrollment(currentApplication, Y);
		if(!isOEPeriod) {
			triggerUserActionEmail(currentApplication);
		}
	}
	
	private void triggerUserActionEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger User Action Email - Notice LCE2");
		try {
			referralLceNotificationService.generateFinancialLCENotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE2").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());

		}
	}
	
	private boolean isCSChangeEffected(SsapApplication currentApplication, long enrolledApplicationId) {
		SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		List<SsapApplicant> currentApplicants = currentApplication.getSsapApplicants();
		List<SsapApplicant> enrolledApplicants = enrolledApplication.getSsapApplicants();
		Map<String, String> enrolledApplicantCS = new HashMap<String, String>();
		Map<Integer,List<String>> enrollmentWiseMembers = new HashMap<Integer, List<String>>();
		List<String> enrolledApplicantIds = null;
		List<EnrollmentShopDTO> enrollments = null;
		try {
			EnrolleeResponse enrolleeResponse = ssapEnrolleeHandler.invokeEnrollmentApi(currentApplication.getId());
			if (null != enrolleeResponse && enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
				enrollments = enrolleeResponse.getEnrollmentShopDTOList();
				enrollments = enrollments.stream()
						   .filter(enrollment -> enrollment.getEnrollmentStatusValue().equalsIgnoreCase("CONFIRM") || enrollment.getEnrollmentStatusValue().equalsIgnoreCase("PENDING"))
						   .filter(enrollment -> enrollment.getCmsPlanId().endsWith("01"))
						   .filter(enrollment -> !enrollment.getPlanLevel().equals("SILVER") && enrollment.getPlanType().equals("Health"))
						   .collect(Collectors.toList());
				if(enrollments==null || (enrollments!=null && enrollments.isEmpty())) {
					return true;
				}
				
				for(SsapApplicant ssapApplicant: enrolledApplicants) {
					enrolledApplicantCS.put(ssapApplicant.getApplicantGuid(), ssapApplicant.getCsrLevel() != null ? ssapApplicant.getCsrLevel() : "CS1");
				}
				
				for(EnrollmentShopDTO enrollment : enrollments) {
					List<EnrolleeShopDTO>  enrolleeShopDTOList = enrollment.getEnrolleeShopDTOList();	
					enrolledApplicantIds = new ArrayList<String>();
					for(EnrolleeShopDTO enrollee : enrolleeShopDTOList) {
						enrolledApplicantIds.add(enrollee.getExchgIndivIdentifier()); 
					}
					enrollmentWiseMembers.put(enrollment.getEnrollmentId(), enrolledApplicantIds);
				}
				
				outer:
					for (EnrollmentShopDTO enrollment : enrollments) {
							enrolledApplicantIds = enrollmentWiseMembers.get(enrollment.getEnrollmentId());
							for(SsapApplicant ssapApplicant : currentApplicants) {
								if(enrolledApplicantIds!=null && enrolledApplicantIds.contains(ssapApplicant.getApplicantGuid())) {
									if(ssapApplicant.getCsrLevel() == null) {
										continue outer;
									}
									else if(enrolledApplicantCS.get(ssapApplicant.getApplicantGuid()) != null 
											&& !ssapApplicant.getCsrLevel().equals(enrolledApplicantCS.get(ssapApplicant.getApplicantGuid()))){
										return false;
									}
								}
							}
					}
			}
			else {
				if (enrolleeResponse != null) {
					throw new GIRuntimeException("Unable to get Enrollment Plan Details. Error Details: " + enrolleeResponse.getErrCode() + ":" + enrolleeResponse.getErrMsg());
				}
			}
		} catch (Exception e) {
			throw new GIRuntimeException("Exception occured while fetching enrollment details :", e);
		}
		
		return true;
	}
	
}
