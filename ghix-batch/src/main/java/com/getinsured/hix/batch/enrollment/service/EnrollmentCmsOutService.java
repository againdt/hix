package com.getinsured.hix.batch.enrollment.service;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.springframework.batch.admin.service.JobService;

import com.getinsured.enrollment.cms.dsh.sbmi.PolicyType;
import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.model.enrollment.Enrollment;

public interface EnrollmentCmsOutService {
	
	public List<BatchJobExecution> getRunningBatchList(String jobName);

	public List<Integer> getEnrollmentIdByIssuerAndYear(int issuerId, int year);

	public List<Enrollment> getEnrollmentById(List<Integer> enrollmentIdList);

	public void makeAndWriteCMSXml(List<Integer> enrollmentIdList, Integer year, Integer month, Integer issuerId, Integer batchYear, Long batchExecutionId, Integer partition, long batchStartTime) throws Exception, InterruptedException, ExecutionException;

	public List<PolicyType> makePolicyTypeList(List<Integer> enrollmentIdList, int year, int issuerId, Map<String, String> enrollmentSkipMap, Long batchExecutionId) throws Exception;

	public boolean validateGeneratedEnrollmentCMSOutXML(Long jobId);

	public List<Integer> findIssuerByYear(String string);

	public Integer getIssuerIdByHiosIssuerId(String hiosId);

	public void processCmsInFile(Long jobId, JobService jobService) throws Exception;

	public List<Integer> findIssuersForCorrection(Integer year, Integer month);

	void logInAactiveIssuer(Integer issuerId, Integer year, Integer month, Integer batchYear, Long batchExecutionId);

	public void deleteFileFromWip();
	
	void deleteFromEnrollmentCmsOut( Long batchExecutionId); 
}
