package com.getinsured.iex.hub.apc.wrapper;

import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.APTCRequestPayloadType;
import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;


public class APTCRequestPayloadTypeWrapper {
	
	private com.getinsured.iex.hub.apc.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory;
	private APTCRequestPayloadType request;
	
	private String requestID;
    private IncomeTypeWrapper incomeWrapper;
    private InsurancePremiumTypeWrapper slcspPremium;
    

    public APTCRequestPayloadTypeWrapper(){
    	ObjectFactory factory = new ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.apc.niem.niem.proxy.xsd._2.ObjectFactory();
		this.request = factory.createAPTCRequestPayloadType();
		incomeWrapper = new IncomeTypeWrapper();
		slcspPremium = new InsurancePremiumTypeWrapper();
	}

	public APTCRequestPayloadType getRequest() throws HubServiceException {
		request.setIncome(incomeWrapper.getSystemRepresentation());
		request.setSLCSPPremium(slcspPremium.getSystemRepresentation());
		com.getinsured.iex.hub.apc.niem.niem.proxy.xsd._2.String id = proxyFactory.createString();
		id.setValue(requestID);
		request.setRequestID(id);
		return request;
	}
	
	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	
	public void setIncomeTypeWrapper(IncomeTypeWrapper incomeTypeWrapper) {
		this.incomeWrapper = incomeTypeWrapper;
	}
	
	public void setInsurancePremiumTypeWrapper(InsurancePremiumTypeWrapper insurancePremiumTypeWrapper) {
		this.slcspPremium = insurancePremiumTypeWrapper;
	}
}
