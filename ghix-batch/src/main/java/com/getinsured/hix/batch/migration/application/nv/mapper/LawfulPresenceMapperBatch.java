package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Predicate;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.LawfulPresenceDocument;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceDocumentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.LawfulPresenceStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ImmigrationDocumentCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;

import net.minidev.json.JSONObject;

@Component
public class LawfulPresenceMapperBatch {
	
	private static final Map<String,ImmigrationDocumentCategoryCodeSimpleType> lawfulPresenceDocumentMap;
	
	static {
		lawfulPresenceDocumentMap = new HashMap<String, ImmigrationDocumentCategoryCodeSimpleType>();
		lawfulPresenceDocumentMap.put("PERMANENT_RESIDENT_CARD_I_551", ImmigrationDocumentCategoryCodeSimpleType.I_551);
		lawfulPresenceDocumentMap.put("TEMPORARY_I_551_STAMP_ON_PASSPORT_OR_I_94_I_94A",
				ImmigrationDocumentCategoryCodeSimpleType.TEMPORARY_I_551_STAMP);
		lawfulPresenceDocumentMap.put("MACHINE_READABLE_IMMIGRANT_VISA_WITH_TEMPORARY_I_551_LANGUAGE", ImmigrationDocumentCategoryCodeSimpleType.MACHINE_READABLE_VISA);
		lawfulPresenceDocumentMap.put("EMPLOYMENT_AUTHORIZATION_CARD_I_766", ImmigrationDocumentCategoryCodeSimpleType.I_766);
		lawfulPresenceDocumentMap.put("ARRIVAL_DEPARTURE_RECORD_IN_FOREIGN_PASSPORT_I_94",
				ImmigrationDocumentCategoryCodeSimpleType.I_94);
		lawfulPresenceDocumentMap.put("ARRIVAL_DEPARTURE_RECORD_IN_UNEXPIRED_FOREIGN_PASSPORT_I_94", ImmigrationDocumentCategoryCodeSimpleType.I_94_IN_PASSPORT);
		lawfulPresenceDocumentMap.put("FOREIGN_PASSPORT", ImmigrationDocumentCategoryCodeSimpleType.UNEXPIRED_FOREIGN_PASSPORT);
		lawfulPresenceDocumentMap.put("REENTRY_PERMIT", ImmigrationDocumentCategoryCodeSimpleType.I_327);
		lawfulPresenceDocumentMap.put("REFUGEE_TRAVEL_DOCUMENT", ImmigrationDocumentCategoryCodeSimpleType.I_571);
		lawfulPresenceDocumentMap.put("CERTIFICATE_OF_ELIGIBILITY_FOR_NONIMMIGRANT_STUDENT_STATUS_I_20", ImmigrationDocumentCategoryCodeSimpleType.I_20);
		lawfulPresenceDocumentMap.put("CERTIFICATE_OF_ELIGIBILITY_FOR_EXCHANGE_VISITOR_STATUS_DS_2019", ImmigrationDocumentCategoryCodeSimpleType.DS_2019);
		lawfulPresenceDocumentMap.put("NOTICE_OF_ACTION_I_797", ImmigrationDocumentCategoryCodeSimpleType.I_797);
		lawfulPresenceDocumentMap
				.put("DOCUMENT_INDICATING_A_MEMBER_OF_RECOGNIZED_INDIAN_TRIBE_OR_INDIAN_BORN_IN_CANADA", ImmigrationDocumentCategoryCodeSimpleType.NATIVE);
		lawfulPresenceDocumentMap.put("CERTIFICATION_FROM_HHS_ORR", ImmigrationDocumentCategoryCodeSimpleType.ORR_CERTIFICATION);
		lawfulPresenceDocumentMap.put("ORR_ELIGIBILITY_LETTER", ImmigrationDocumentCategoryCodeSimpleType.ORR_ELIGIBILITY_LETTER);
		lawfulPresenceDocumentMap.put("CUBAN_HAITAN_ENTRANT", ImmigrationDocumentCategoryCodeSimpleType.CUBAN_HAITIAN_ENTRANT);
		lawfulPresenceDocumentMap.put("DOCUMENT_INDICATING_WITHHOLDING_OF_REMOVAL", ImmigrationDocumentCategoryCodeSimpleType.WITHHOLDING_OF_REMOVAL);
		lawfulPresenceDocumentMap.put("RESIDENT_OF_AMERICAN_SAMOA_CARD",ImmigrationDocumentCategoryCodeSimpleType.AMERICAN_SAMOAN);
		lawfulPresenceDocumentMap.put("ADMINISTRATIVE_STAY_BY_DHS", ImmigrationDocumentCategoryCodeSimpleType.STAY_OF_REMOVAL);
		lawfulPresenceDocumentMap.put("OTHER", ImmigrationDocumentCategoryCodeSimpleType.OTHER);
		lawfulPresenceDocumentMap.put("OTHER1", ImmigrationDocumentCategoryCodeSimpleType.OTHER);
		lawfulPresenceDocumentMap.put("CERTIFICATE_OF_CITIZENSHIP", ImmigrationDocumentCategoryCodeSimpleType.CERTIFICATE_OF_CITIZENSHIP);
		lawfulPresenceDocumentMap.put("CERTIFICATE_OF_NATURALIZATION", ImmigrationDocumentCategoryCodeSimpleType.NATURALIZATION_CERTIFICATE);
		lawfulPresenceDocumentMap.put("NS1_MEMBERS_OF_A_FEDERALLY_RECOGNIZED_INDIAN_TRIBE", ImmigrationDocumentCategoryCodeSimpleType.NATIVE);
		lawfulPresenceDocumentMap.put(
				"NS2_NON_CITIZEN_WHO_IS_LAWFULLY_PRESENT_IN_AMERICAN_SAMOA_UNDER_THE_IMMIGRATION_LAWS_OF_AMERICAN_SAMOA",
				ImmigrationDocumentCategoryCodeSimpleType.AMERICAN_SAMOAN);
		lawfulPresenceDocumentMap.put("NS4_NON_CITIZEN_WHO_IS_LAWFULLY_PRESENT_IN_AMERICAN_SAMOA", ImmigrationDocumentCategoryCodeSimpleType.AMERICAN_SAMOAN);
		lawfulPresenceDocumentMap.put("VAWA_SELF_PETITIONER", ImmigrationDocumentCategoryCodeSimpleType.OTHER);
	}
	
	
	public LawfulPresenceStatusType createLawfulPresenceStatusType(AttestationsMember member,Member computedMember) {
		
		LawfulPresenceStatusType lawfulPresenceStatusType = AccountTransferUtil.insuranceApplicationObjFactory.createLawfulPresenceStatusType();
		boolean hasLawfulDocuments = false;
		if(member.getLawfulPresence()!=null && member.getLawfulPresence().getLawfulPresenceDocumentation() != null) {
			for (Map.Entry<String, LawfulPresenceDocument> documentEntry : member.getLawfulPresence()
					.getLawfulPresenceDocumentation().entrySet()) {
				if (!documentEntry.getValue().getNoIdentifiersProvidedIndicator()) {
					hasLawfulDocuments = true;
				}
			}	
		}
		lawfulPresenceStatusType.setLawfulPresenceStatusArrivedBefore1996Indicator(AccountTransferUtil.addBoolean(hasLawfulDocuments));
		EligibilityType eligibilityIndicator = AccountTransferUtil.insuranceApplicationObjFactory.createEligibilityType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean eligibilityIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();
		eligibilityIndicatorBoolean.setValue(false);
		eligibilityIndicator.setEligibilityIndicator(eligibilityIndicatorBoolean);
		lawfulPresenceStatusType.getLawfulPresenceStatusEligibility().add(eligibilityIndicator);
		if(member.getLawfulPresence()!=null && member.getLawfulPresence().getLawfulPresenceStatusIndicator()!=null && member.getLawfulPresence().getLawfulPresenceStatusIndicator()) {
			
			
			if(lawfulPresenceStatusType.getLawfulPresenceStatusArrivedBefore1996Indicator() != null && lawfulPresenceStatusType.getLawfulPresenceStatusArrivedBefore1996Indicator().isValue() == true){
				
				//DateType documentExpiryDate = AccountTransferUtil.niemCoreFactory.createDateType();
				
				Map<String,LawfulPresenceDocument> documents = member.getLawfulPresence().getLawfulPresenceDocumentation();
				
				Predicate<LawfulPresenceDocument> cardNumberPresent = d -> StringUtils.isNotEmpty(d.getCardNumber());
				Predicate<LawfulPresenceDocument> sevisIdPresent = d -> StringUtils.isNotEmpty(d.getSevisId());
				Predicate<LawfulPresenceDocument> cardNumberOrSevisIdPresent = cardNumberPresent.or(sevisIdPresent);
				
				Predicate<LawfulPresenceDocument> alienNumberPresent = d -> StringUtils.isNotEmpty(d.getAlienNumber());
				Predicate<LawfulPresenceDocument> i94NumberPresent = d -> StringUtils.isNotEmpty(d.getI94Number());
				Predicate<LawfulPresenceDocument> passportNumberPresent = d -> StringUtils.isNotEmpty(d.getPassportNumber());
				Predicate<LawfulPresenceDocument> anyPersonIdentificationPresent = alienNumberPresent.or(i94NumberPresent).or(passportNumberPresent);
				
				
				documents.forEach((k,v) -> {
					
					ImmigrationDocumentCategoryCodeType immigrationDocumentCategoryCodeType = AccountTransferUtil.hixTypeFactory.createImmigrationDocumentCategoryCodeType();
					
					LawfulPresenceDocumentType lawfulPresenceDocumentType = AccountTransferUtil.insuranceApplicationObjFactory.createLawfulPresenceDocumentType();
					
					IdentificationType documentPersonIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
					com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String documentPersonIdentificationString = AccountTransferUtil.basicFactory.createString();
					TextType documentPersonIdentificationCategoryText =AccountTransferUtil.niemCoreFactory.createTextType();
					TextType documentNumberIdentificationCategoryText = AccountTransferUtil.niemCoreFactory.createTextType();
					
					IdentificationType documentNumberIdentificationType =AccountTransferUtil.niemCoreFactory.createIdentificationType();
					com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String documentNumberIdentificationString = AccountTransferUtil.basicFactory.createString();
					ImmigrationDocumentCategoryCodeSimpleType documentCategoryCode = lawfulPresenceDocumentMap.get(k);
					
					if(cardNumberOrSevisIdPresent.test(v) && !v.getNoIdentifiersProvidedIndicator()) {
						
						
						if(cardNumberPresent.test(v)) {
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentNumberIdentificationCategoryText.setValue("cardNumber");
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							documentNumberIdentificationType.setIdentificationCategoryText(documentNumberIdentificationCategoryText);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							
						}
						if(sevisIdPresent.test(v)) {
							documentNumberIdentificationString.setValue(v.getSevisId());
							documentNumberIdentificationCategoryText.setValue("sevisId");
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							documentNumberIdentificationType.setIdentificationCategoryText(documentNumberIdentificationCategoryText);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
						}
					}
					
					
					
					if(anyPersonIdentificationPresent.test(v) && !v.getNoIdentifiersProvidedIndicator()) {
						if(alienNumberPresent.test(v)) {
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentPersonIdentificationType.setIdentificationID(documentPersonIdentificationString);
							documentPersonIdentificationCategoryText.setValue("alienNumber");
							documentPersonIdentificationType.setIdentificationCategoryText(documentPersonIdentificationCategoryText);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().add(documentPersonIdentificationType);
						}
						if(passportNumberPresent.test(v)) {
							documentPersonIdentificationString.setValue(v.getPassportNumber());
							documentPersonIdentificationType.setIdentificationID(documentPersonIdentificationString);
							documentPersonIdentificationCategoryText.setValue("passportNumber");
							documentPersonIdentificationType.setIdentificationCategoryText(documentPersonIdentificationCategoryText);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().add(documentPersonIdentificationType);
						}
						if(i94NumberPresent.test(v)) {
							documentPersonIdentificationString.setValue(v.getI94Number());
							documentPersonIdentificationType.setIdentificationID(documentPersonIdentificationString);
							documentPersonIdentificationCategoryText.setValue("i94Number");
							documentPersonIdentificationType.setIdentificationCategoryText(documentPersonIdentificationCategoryText);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentPersonIdentification().add(documentPersonIdentificationType);
						}
						
					}
					
					
					immigrationDocumentCategoryCodeType.setValue(documentCategoryCode);
					lawfulPresenceDocumentType.setLawfulPresenceDocumentCategoryCode(immigrationDocumentCategoryCodeType);
					lawfulPresenceStatusType.getLawfulPresenceStatusImmigrationDocument().add(lawfulPresenceDocumentType);
					
					
					/*switch (documentCategoryCode) {
						case I_551:
							
							
							break;
						case TEMPORARY_I_551_STAMP:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							break;
						case MACHINE_READABLE_VISA:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							break;
						case I_766:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							break;
						case I_94:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							break;
						case I_94_IN_PASSPORT:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							break;
						case UNEXPIRED_FOREIGN_PASSPORT:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
							com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String countryAlpha3CodeType= AccountTransferUtil.basicFactory.createString();
							//countryAlpha3CodeType.setValue(((String)((JSONObject) citizenshipImmigrationStatus.get(kEY_CITIZENSHIP_DOCUMENT)).get("foreignPassportCountryOfIssuance")));
							documentPersonIdentificationType.setIdentificationJurisdictionISO3166Alpha3Code(countryAlpha3CodeType);			
							break;
						case I_327:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							break;
						case I_571:
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							break;
						case I_20:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							break;
						case DS_2019:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
						case I_797:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							break;
						case NATIVE:
							break;
						case ORR_CERTIFICATION:
							break;
						case ORR_ELIGIBILITY_LETTER:
							break;
						case CUBAN_HAITIAN_ENTRANT:
							break;
						case WITHHOLDING_OF_REMOVAL:
							break;
						case AMERICAN_SAMOAN:
							break;
						case STAY_OF_REMOVAL:
							break;
						case CERTIFICATE_OF_CITIZENSHIP:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
						case NATURALIZATION_CERTIFICATE:
							documentNumberIdentificationString.setValue(v.getCardNumber());
							documentPersonIdentificationString.setValue(v.getAlienNumber());
							documentNumberIdentificationType.setIdentificationID(documentNumberIdentificationString);
							lawfulPresenceDocumentType.getLawfulPresenceDocumentNumber().add(documentNumberIdentificationType);
						default:
							break;
					}*/
					
					
				});
				
			}
			eligibilityIndicatorBoolean.setValue(member.getLawfulPresence().getLawfulPresenceStatusIndicator());
		}
		
		
		
		return lawfulPresenceStatusType;
		
	}
}
