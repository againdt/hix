package com.getinsured.iex.hub.consumers.hub;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.ChannelAwareMessageListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.client.SoapFaultClientException;

import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.rabbitmq.client.Channel;

public class NonESIMECQueueConsumer extends MessageProcessor implements ChannelAwareMessageListener {

	@Autowired
	private WebServiceTemplate nonMECApplicantServiceTemplate;

	private static final Logger LOGGER = LoggerFactory
			.getLogger(NonESIMECQueueConsumer.class);


	private void invokeNonESIMEC() throws HubMappingException, BridgeException, HubServiceException, InputDataValidationException 
	{
		Exception ex = null;
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		String nMECRequestJSON = "{\"taxHousehold\":[{\"householdMember\":{\"person\":{\"dateOfBirth\":\"01/25/1970\",\"name\":{\"firstName\":\"John\",\"middleName\":\"D\",\"lastName\":\"Fox\",\"personNameSuffixText\":\"Jr\"},\"socialSecurityCard\":{\"socialSecurityNumber\":\"222223701\"},\"gender\":\"F\"},\"householdContact\":{\"homeAddress\":{\"state\":\"AZ\"}},\"insurance\":{\"insurancePolicyEffectiveDate\":\"05/01/2014\",\"insurancePolicyExpirationDate\":\"12/31/2014\"},\"organization\":[{\"organizationCode\":\"MEDC\"}]}}]}";
		HubServiceBridge nesiMecBridge = HubServiceBridge.getHubServiceBridge("NonMECApplicantRequest");
		handler = nesiMecBridge.getServiceHandler();
		handler.setJsonInput(nMECRequestJSON);
		
		Object payLoad = handler.getRequestpayLoad();
		if(payLoad != null)
		{
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("NMEC");
			requestRecord.setEndpointOperationName("NMEC Eligibility");
			response = this.executeRequest(requestRecord, handler, nonMECApplicantServiceTemplate);
		}	
	}
	
	@Override
	public void onMessage(Message message, Channel channel) throws BridgeException {
		String threadId = Thread.currentThread().getName();
		String routingKey = message.getMessageProperties().getReceivedRoutingKey();
		if(routingKey == null){
			throw new IllegalArgumentException("No routing key available for incoming MEC message, don't know how to process it");
		}
		LOGGER.info("Thread[" + threadId + "]Routing Key:"
				+ routingKey
				+ " Received Message:" + new String(message.getBody())
				+ " from channel:" + channel.getChannelNumber());

		try {
			invokeNonESIMEC();
		} catch (SoapFaultClientException e) {
			LOGGER.error("Request failed with Reason:"
					+ e.getFaultStringOrReason() + " message:"
					+e.getMessage()
					+ "And fault code:"+e.getFaultCode(),e);
		} catch (HubServiceException e) {
			LOGGER.info("Exception", e);
		} catch (HubMappingException e) {
			LOGGER.info("Exception", e);
		} catch (InputDataValidationException ie) {
			LOGGER.info("Exception", ie);
		}
	}
		
}
