import React, {Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {hideModal} from '../../actions/actionCreators';

// Components

// ActionCreators


const MandatorySeekingForCoverageModal = props => {

    const {
        dashboardUrl
    } = props;

    const handleDashboardClick = () => {
        window.location = dashboardUrl;
    };

    const {
        modal: {
            modalOwner
        }
    } = props;

    const handleHideClick = () => {
        props.actions.hideModal(modalOwner);
    };


    return (
        <Fragment>
            <div className="modal__header" id="modal__seeking__for__coverage">
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideClick}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div className="modal__body txt-left">
                <p>
                    At least one household member should be seeking for coverage to continue.
                </p>
                <p>
                    Click close to go back and update your information.
                </p>
                <p>
                    Click dashboard to go to dashboard (Your information will be saved).
                </p>
                <div className="modal__controls">
                    <Button
                        className="modal-cancel-btn usa-button-secondary"
                        onClick={handleHideClick}
                    >
                        CLOSE
                    </Button>
                    <Button
                        className="modal-confirm-btn"
                        onClick={handleDashboardClick}
                    >
                        DASHBOARD
                    </Button>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    dashboardUrl: state
        .headerLinks
        .links
        .DASHBOARD,
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(MandatorySeekingForCoverageModal);
