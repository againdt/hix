package com.getinsured.iex.dto;

public 	class SsapSEPIntegrationDTO {
	
	long applicationId;
	Boolean isAddressChangeDemoEvent;
	Boolean isDisenrollCase;
	Boolean doMoveEnrollment;
	Boolean isSepDenialCase;
	Boolean isNoChangeCase;
	
	public Boolean getIsNoChangeCase() {
		return isNoChangeCase;
	}

	public void setIsNoChangeCase(Boolean isNoChangeCase) {
		this.isNoChangeCase = isNoChangeCase;
	}

	public long getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(long applicationId) {
		this.applicationId = applicationId;
	}
	
	public Boolean getIsAddressChangeDemoEvent() {
		return isAddressChangeDemoEvent;
	}
	
	public void setIsAddressChangeDemoEvent(Boolean isAddressChangeDemoEvent) {
		this.isAddressChangeDemoEvent = isAddressChangeDemoEvent;
	}

	public Boolean getIsDisenrollCase() {
		return isDisenrollCase;
	}

	public void setIsDisenrollCase(Boolean isDisenrollCase) {
		this.isDisenrollCase = isDisenrollCase;
	}

	public Boolean isDoMoveEnrollment() {
		return doMoveEnrollment;
	}

	public void setDoMoveEnrollment(Boolean doMoveEnrollment) {
		this.doMoveEnrollment = doMoveEnrollment;
	}

	public Boolean getIsSepDenialCase() {
		return isSepDenialCase;
	}

	public void setIsSepDenialCase(Boolean isSepDenialCase) {
		this.isSepDenialCase = isSepDenialCase;
	}
}