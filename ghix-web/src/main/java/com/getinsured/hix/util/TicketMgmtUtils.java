package com.getinsured.hix.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;


import com.getinsured.hix.dto.tkm.HistoryDto;
import com.getinsured.hix.dto.tkm.ReportDto;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CancelTicketRequest;
import com.getinsured.hix.model.TaskFormProperties;
import com.getinsured.hix.model.TkmDocuments;
import com.getinsured.hix.model.TkmQueueUsersRequest;
import com.getinsured.hix.model.TkmQueues;
import com.getinsured.hix.model.TkmQueuesResponse;
import com.getinsured.hix.model.TkmQuickActionDto;
import com.getinsured.hix.model.TkmTicketTaskResponse;
import com.getinsured.hix.model.TkmTicketTasks;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.model.TkmTicketsRequest;
import com.getinsured.hix.model.TkmTicketsResponse;
import com.getinsured.hix.model.TkmTicketsTaskRequest;
import com.getinsured.hix.model.TkmUserDTO;
import com.getinsured.hix.model.TkmWorkflows;
import com.getinsured.hix.model.TkmWorkflowsResponse;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.TicketMgmtEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

/**
 * 
 * @author Sharma_k This class is written to communicate with ghix-tkm war
 *         through web using rest api
 */
@Component
public class TicketMgmtUtils {
	@Autowired
	private RestTemplate mvcRestTemplate;
	@Autowired private Gson platformGson;
	
	private static final Logger LOGGER = Logger
			.getLogger(TicketMgmtUtils.class);
	
	private static final String TKM = "TKM-";
	
	public Map<String, Object> searchTickets(Map<String, Object> searchCriteria)  {
		LOGGER.info("In Rest call searchTickets to communicate with ghix-tkm module");

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.SEARCHTICKETS, searchCriteria, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		
		return tkmTicketsResponse.getTkmTicketsMap();
	}

	public List<String> getTicketType() {
		List<String> ticketType = new ArrayList<String>();
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETTICKETTYPE, "", String.class);
			String get_resp = responseEntity.getBody();
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmWorkflowsResponse tkmWorkflowsResponse = (TkmWorkflowsResponse) xstream.fromXML(get_resp);
			
			if (tkmWorkflowsResponse != null) {
				//return tkmWorkflowsResponse.getTicketType();
				ticketType.addAll(tkmWorkflowsResponse.getTicketType());
			}
		
		return ticketType;
	}

	public TkmTickets getTkmTicketsById(int ticketId) {

		ResponseEntity<String> responseEntity = mvcRestTemplate.getForEntity(
				TicketMgmtEndPoints.SEARCH_TKMTICKETS_ID + ticketId,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTickets();
	}
	
	public TkmTicketsResponse getTkmTicketsDetailsById(int ticketId) {

		ResponseEntity<String> responseEntity = mvcRestTemplate.getForEntity(
				TicketMgmtEndPoints.SEARCH_TKMTICKETS_ID + ticketId,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		
		return tkmTicketsResponse;
		
	
	}

	public List<String> getTicketSubject(AccountUser user) {
		
		TkmTicketsRequest request = new TkmTicketsRequest();
		request.setId(user==null?null:Integer.valueOf(user.getId()));
		
		List<String> ticketSubject = new ArrayList<String>();
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.SEARCH_TKMTICKETS_SUBJECT, request, String.class);
		XStream xstream = GhixUtils.getXStreamStaxObject();
		String get_resp = responseEntity.getBody();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		
		if (tkmTicketsResponse != null) {
			// return tkmTicketsResponse.getTkmTicketsSubjectList();
			ticketSubject.addAll(tkmTicketsResponse.getTkmTicketsSubjectList());
		}
		return ticketSubject;
	}

	public List<TkmWorkflows> getTicketTypeList() {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.GETWORKFLOWLIST, "", String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmWorkflowsResponse response = (TkmWorkflowsResponse) xstream.fromXML(get_resp);
		return response.getTkmWorkflowsList();
	}

	public List<TkmQueues> getQueuesList() {
		return getQueuesList(null);
	}
		
	public List<TkmQueues> getQueuesList(Integer userId) {
		TkmTicketsRequest tkmReq = new TkmTicketsRequest();
		tkmReq.setRequester(userId==null ? null: userId);
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.QUEUEURL, tkmReq, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmQueuesResponse tkmQueuesResponse = (TkmQueuesResponse) xstream.fromXML(get_resp);
		return tkmQueuesResponse.getTkmQueuesList();
	}

	public List<ReportDto> getTicketsAgeingReport(Map<String, Object> reportCriteria) {
		 
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.TICKETAGEINGREPORT, reportCriteria, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getTicketReportDtoList();
		
	
	}
	public TkmTickets addNewTicket(TkmTicketsRequest tkmTicketsRequest) {

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.ADDNEWTICKET_URL, tkmTicketsRequest,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTickets();

	}
	
	public TkmTickets createNewTicket(TkmTicketsRequest tkmTicketsRequest) {

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.CREATENEWTICKET, tkmTicketsRequest,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTickets();

	}

    public void createOrUpdateTicket(TkmTicketsRequest tkmTicketsRequest) {
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("createOrUpdateTicket::calling create or update for case number: " + tkmTicketsRequest.getCaseNumber());
        }
        ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
            TicketMgmtEndPoints.CREATEORUPDATETICKET, tkmTicketsRequest, String.class);
        String get_resp = responseEntity.getBody();
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("createOrUpdateTicket::returned with body: " + get_resp);
        }
        TkmTicketsResponse tkmTicketsResponse = platformGson.fromJson(get_resp, TkmTicketsResponse.class);
        if(LOGGER.isDebugEnabled()) {
            LOGGER.debug("createOrUpdateTicket::tkmTicketsResponse status: " + tkmTicketsResponse.getStatus());
        }
  }

	/**
	 * @author Sharma_k
	 * @since 02nd May 2013
	 * @param ticketId
	 * @return
	 * @description rest call to fetch TkmTicketTasks data from
	 *              TicketMgmtController
	 */
	public List<TkmTicketTasks> getTicketTaskList(String ticketId) {
		// TODO add url to ghix
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.GETTICKETTASKLIST, ticketId, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream
				.fromXML(get_resp);
		return tkmTicketTaskResponse.getTkmTicketTaskList();

	}

	public TkmTickets saveComment(int ticketId, String comment, Integer createdBy) {
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
		tkmTicketsRequest.setTicketId(ticketId);
		tkmTicketsRequest.setComment(comment);
		tkmTicketsRequest.setLastUpdatedBy(createdBy);
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.SAVECOMMENTS, tkmTicketsRequest,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTickets();
		
	}

	public TkmTickets editTicket(TkmTicketsRequest tkmTicketsRequest) {

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.EDIT_TICKET_URL, tkmTicketsRequest,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTickets();
	}

	public List<TkmTickets> getUserTickets(int userId, List<Integer> userQueueId) {
		LOGGER.info("In Rest call getUserTickets to communicate with ghix-tkm module");
		List<TkmTickets> ticketList = new ArrayList<TkmTickets>();
		
		
			TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
			tkmTicketsRequest.setAssignee(userId);
			tkmTicketsRequest.setQueue(userQueueId);
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
					TicketMgmtEndPoints.USERTICKETLIST, tkmTicketsRequest,
					String.class);
			String get_resp = responseEntity.getBody();
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
					.fromXML(get_resp);
			if (tkmTicketsResponse != null) {
				//return tkmTicketsResponse.getTkmTicketsList();
				ticketList.addAll(tkmTicketsResponse.getTkmTicketsList());
			}
		
		return ticketList;
	}

	public String claimUserTask(
			TkmTicketsTaskRequest ticketsTaskRequest) {
		LOGGER.info("In Rest call claimUserTask to communicate with ghix-tkm module");

		try {
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
					TicketMgmtEndPoints.CLAIMUSERTASK, ticketsTaskRequest,
					String.class);
			String get_resp = responseEntity.getBody();
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmTicketTaskResponse tkmTicketsTaskResponse = (TkmTicketTaskResponse) xstream
					.fromXML(get_resp);
			if (tkmTicketsTaskResponse != null) {
				return tkmTicketsTaskResponse.getStatus();
			}
		} catch (RestClientException ex) {
			LOGGER.error("User Ticket: " + ex.getMessage());
		}
		return null;
	}
	public TkmTicketTaskResponse completeUserTask(TkmTicketsTaskRequest ticketsTaskRequest){
		LOGGER.info("In Rest call completeUserTask to communicate with ghix-tkm module");
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
					TicketMgmtEndPoints.COMPLETEUSERTASK, ticketsTaskRequest,
					String.class);
			String get_resp = responseEntity.getBody();
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmTicketTaskResponse tkmTicketsTaskResponse  = (TkmTicketTaskResponse) xstream
					.fromXML(get_resp);
			if (tkmTicketsTaskResponse != null) {
				tkmTicketsTaskResponse.getStatus();
			}
		
		return tkmTicketsTaskResponse;
	}
	
	/**
	 * @author Kuldeep
	 * @since 28th May 2013
	 * @param ticketId
	 * @return rest call to fetch ticket history details
	 */
	public List<HistoryDto> getTicketHistory(Integer ticketId, boolean isDetailedView) {
		LOGGER.info("In Rest call getTicketHistory to communicate with ghix-tkm module");
			TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
			tkmTicketsRequest.setTicketId(ticketId);
			tkmTicketsRequest.setDetailedHistoryView(isDetailedView);
			
			ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
					TicketMgmtEndPoints.GETSIMPLEORDETAILEDHISTORY, tkmTicketsRequest, String.class);
			String get_resp = responseEntity.getBody();
			XStream xstream = GhixUtils.getXStreamStaxObject();
			TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
					.fromXML(get_resp);
			if (tkmTicketsResponse != null
					&& tkmTicketsResponse.getStatus().equalsIgnoreCase(
							"SUCCESS")) {
				return tkmTicketsResponse.getTkmTicketHistory();
			}
		
		return null;
	}
	
	public List<TaskFormProperties> getTaskFormProperties(String taskId) {
		// TODO add url to ghix
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.TASKFORMPROPERTIES, taskId, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream
				.fromXML(get_resp);
		return tkmTicketTaskResponse.getTkmTaskProperties();
	}

	public List<TkmDocuments> getTicketDocuments(String ticketId) {
		// TODO add url to ghix
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.GETTICKETDOCUMENTPATHLIST, ticketId, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse tkmTicketResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketResponse.getDocumentList();
		
	}

	public TkmTicketsResponse getTicketDocumentsAndFormParams(String ticketId) {
		// TODO add url to ghix
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETTICKETDOCUMENTPATHLISTANDFORMPARAMS, ticketId, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		TkmTicketsResponse tkmTicketResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketResponse;
	}
	
	public List<TkmTicketTasks> getTicketActiveTask(String ticketId) {
		// TODO add url to ghix
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.GETTICKETACTIVETASKS, ticketId,
				String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.xStreamHibernateJSONMashaling();
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream
				.fromXML(get_resp);
		return tkmTicketTaskResponse.getTkmTicketTaskList();

	}

	public boolean reassignTicket(String taskId, String queueName,String assignee, String ticketId)  {
		TkmTicketsTaskRequest taskRequest = new TkmTicketsTaskRequest();
		taskRequest.setTaskId(Integer.parseInt(taskId));
		if (null != assignee && !assignee.isEmpty()) {
			taskRequest.setUserId(Integer.parseInt(assignee));
		}
		if (queueName != null) {
			taskRequest.setGroupId(Integer.parseInt(queueName));
		}
		taskRequest.setTaskTicketId(ticketId);

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.REASSIGNTICKETACTIVETASKS, taskRequest,String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream.fromXML(get_resp);
		return tkmTicketTaskResponse.isActivitiProcessCompleted();

	}
	
	public boolean assignTicket(String taskId, String queueName,String assignee, String ticketId)  {
		TkmTicketsTaskRequest taskRequest = new TkmTicketsTaskRequest();
		taskRequest.setTaskId(Integer.parseInt(taskId));
		if (null != assignee && !assignee.isEmpty()) {
			taskRequest.setUserId(Integer.parseInt(assignee));
		}
		if (queueName != null) {
			taskRequest.setGroupId(Integer.parseInt(queueName));
		}
		taskRequest.setTaskTicketId(ticketId);

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.ASSIGNTICKET, taskRequest,String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream.fromXML(get_resp);
		return tkmTicketTaskResponse.isActivitiProcessCompleted();

	}

	/**
	 * 
	 * @author hardas_d
	 * @return String
	 * @param ticketId
	 * @param comment
	 * @param userId
	 * @return Status of the cancel ticket.
	 * @throws RestClientException
	 */
	public String cancelTicket(int ticketId, String comment, Integer userId) {
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
		tkmTicketsRequest.setId(ticketId);
		tkmTicketsRequest.setComment(comment);
		tkmTicketsRequest.setLastUpdatedBy(userId);
		String response=StringUtils.EMPTY;
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.CANCELTICKET, tkmTicketsRequest, String.class);
		if(null!=responseEntity){
			response = responseEntity.getBody();
		}
		return response;
	}

	/**
	 * @author hardas_d
	 * @return boolean
	 * @param queueId
	 * @param userList
	 * @return Returns true if the update is successful, else throws exceptions 
	 * @throws RestClientException
	 */
	public void updateQueue(Integer queueId, List<Integer> userList) {
		TkmQueueUsersRequest request = new TkmQueueUsersRequest();
		request.setQueueId(queueId);
		request.setUserList(userList);
		
		mvcRestTemplate.postForEntity(TicketMgmtEndPoints.UPDATEQUEUEUSERS, request, String.class);
	}
	
	public String saveTicketDocuments(TkmTicketsRequest ticketsRequest) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.SAVETICKETATTACHMENTS, ticketsRequest, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		if (tkmTicketsResponse != null) {
			return tkmTicketsResponse.getErrMsg();
		}
		return null;
		
	}
	
	/**
	 * @author Sharma_k
	 * @since 08th August 2013
	 * @param assigneeName
	 * @return
	 * @throws RestClientException
	 * Jira Id: HIX-14168 Manage ticket
	 */
	public List<TkmUserDTO> getTicketTaskByAssignee(String assigneeName) {
		TkmTicketsTaskRequest tkmTicketTaskRequest = new TkmTicketsTaskRequest();
		tkmTicketTaskRequest.setAssigneeName(assigneeName);

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GET_TASK_USER_INFO_BY_ASSIGNEE, tkmTicketTaskRequest, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		
		TkmTicketTaskResponse tkmTicketTaskResponse = (TkmTicketTaskResponse) xstream.fromXML(get_resp);
		return tkmTicketTaskResponse.getTkmUserDTO();
	}
	
	public String deleteDocumnet(String  docId) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.DELETETICKETATTACHMENTS, docId, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getStatus();
	
		
	}

	/**
	 * @author hardas_d
	 * @return List<String>
	 * @return List of ticket numbers
	 */
	public List<String> getTicketNumbers(){
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(
				TicketMgmtEndPoints.GET_TICKET_NUMBERS, "", String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		 
		return tkmTicketsResponse.getTicketNumbers();
	}

	/**
	 * @author hardas_d
	 * @return List<String>
	 * @return
	 */ 	
	public List<AccountUser> getRequesters(String userText) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GET_REQUESTERS, userText, String.class);
		String get_resp = responseEntity.getBody();
//		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		
		TkmTicketsResponse tkmTicketsResponse = null;
		try {
			tkmTicketsResponse = TicketMgmtUtils.getTkmTicketsResponseFromJson(get_resp,"getRequesters");
			
		} catch (Exception e) {
			LOGGER.error("Exception occurred in converting response to json.",e);
		}
		return (tkmTicketsResponse !=null) ? tkmTicketsResponse.getRequesters() : null;
	}
	
	
	/**
	 * @author yadav_sa
	 * @return List<String>
	 * @return
	 */
	public Map<String,Integer> getTicketCountForPieReport(String criteria) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETTICKETCOUNTFORPIEREPORT, criteria, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTicketNumbersByPriorityOrType();
	}

	
	
	public List<TaskFormProperties> getReclassfiedTargetProperties(
			String ticketType, String ticketSubType, String ticketId) {
		TkmTicketsRequest ticketsRequest = new TkmTicketsRequest();
		ticketsRequest.setTicketId(Integer.valueOf(ticketId));
		ticketsRequest.setCategory(ticketType);
		ticketsRequest.setType(ticketSubType);
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.FETCHTARGETTICKETFORMPROERTIES, ticketsRequest, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsTaskResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsTaskResponse.getTkmTicketFomProperties();
	}


	public List<TaskFormProperties> performReclassifyTicketWorkflow(
			TkmTicketsRequest tkmTicketsRequest) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.PERFORMRECLASSIFYTICKETWORKFLOW, tkmTicketsRequest, String.class);
		String get_resp = responseEntity.getBody();
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsTaskResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
				
		return tkmTicketsTaskResponse.getTkmTicketFomProperties();
	}
	public List<AccountUser> getAllUsersForQueue(String qName) {
		 
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETALLUSERSFORQUEUE, qName, String.class);
		String get_resp = responseEntity.getBody();

		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getQueueUsers();
		
	
	}
	
	public List<Integer> getQueuesForUser(String queueId) {
		 
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETQUEUESFORUSER, queueId, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getUserQueues();
		
	
	}

	public TkmQuickActionDto getDetailsForQuickActionBar(Integer id) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETDETAILSFORQUICKACTIONBAR, id, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTkmQuickActionDto();
	}

	public Map<String, Object> getPendingTicketDetails(Map<String, Object> searchCriteria) {
		LOGGER.info("In Rest call getPendingTicketDetails to communicate with ghix-tkm module");

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.SEARCHPENDINGTICKETS, searchCriteria, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return tkmTicketsResponse.getTkmTicketsMap();
	}
	
	/**
	 * Convert the time-stamp/date with the specified time-zone value.
	 * @param inDate
	 * @param dateFormat
	 * @param targetTimeZone
	 * @return
	 */
	public static String convertDateByTimeZone(String inDate, String dateFormat, String targetTimeZone) {
		String formattedDateString = inDate;
		try {
			return convertDateByTimeZone(new SimpleDateFormat(dateFormat).parse(inDate), dateFormat,targetTimeZone);
		} catch (ParseException ex) {
			LOGGER.error("Unable to convert the timestamp into specified timezone", ex);
		}
		return formattedDateString;
	}

	public static String convertDateByTimeZone(Date inDate, String dateFormat, String targetTimeZone) {
		SimpleDateFormat formatterTargetTimeZone = (new SimpleDateFormat(dateFormat));
		formatterTargetTimeZone.setTimeZone(TimeZone.getTimeZone(targetTimeZone));
		return formatterTargetTimeZone.format(inDate);
	}
	
	public static Date convertDateByTimezone(Date inDate, String targetTimeZone) throws ParseException {
		String strDefDateFormat = "MMM dd, yyyy hh:mm a"; 
		SimpleDateFormat sdf = new SimpleDateFormat(strDefDateFormat);
		sdf.setTimeZone(TimeZone.getTimeZone(targetTimeZone));
		String strConvertedDate =  sdf.format(inDate);
		
		SimpleDateFormat sdf1 = new SimpleDateFormat(strDefDateFormat);
		Date date = sdf1.parse(strConvertedDate);
		return date;
	}
	
	/**
	 * Added to check if the input text is valid
	 * @param comment
	 * @param commentLength
	 * @return
	 */
	public static boolean isCommentValid(String comment, String commentLength) {		
		boolean bValid= true;
			
		Integer commentLngth = Integer.parseInt(commentLength);
		if((commentLngth > 0) && StringUtils.isEmpty(comment)) {
			bValid=false;
		}		
			
		return bValid;
	}
	
	
	
	
	public TkmTicketsResponse getHouseholds(String userName) {

		
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GET_HOUSEHOLDS_CREATEDFOR, userName, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse;
		
	
	}

	public String getCreatedFor(String roleId, String moduleName, String userText) {
		String createdForListJSON = null;
		
		Map<String, String> searchCriteria = new HashMap<>();
		searchCriteria.put("roleId", roleId);
		searchCriteria.put("moduleName", moduleName);
		searchCriteria.put("userText", userText);
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GET_HOUSEHOLDS_CREATEDFOR, searchCriteria, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		createdForListJSON = tkmTicketsResponse.getCreatedForName();
		return createdForListJSON;
		
	}

	public Household getHouseholdByuserId(int houseHoldUserId) {
		
		Household household =null;
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GET_HOUSEHOLD_BY_USER_ID, houseHoldUserId, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		List<Household> householdlist=tkmTicketsResponse.getHouseholdList();
		
		if(null !=householdlist && householdlist.size() > 0)
		{
			household=householdlist.get(0);
		}
		
		return household;
	}
	
	public String archiveTicket(int ticketId, Integer userId) {
		TkmTicketsRequest tkmTicketsRequest = new TkmTicketsRequest();
		tkmTicketsRequest.setId(ticketId);
		tkmTicketsRequest.setArchivedBy(userId);
	
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.ARCHIVETICKET, tkmTicketsRequest, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		return null;
	}
	
	public List<Object[]> getUsersForQueue(String qName) {
		 
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETALLUSERSFORQUEUE, qName, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getTkmQueueUserDtoList();
	}
	
	public List<Object[]> getUsersForQueueForReassign(String qName) {
		 
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.GETALLUSERSFORQUEUEFORREASSIGN, qName, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream
				.fromXML(get_resp);
		return tkmTicketsResponse.getTkmQueueUserDtoList();
	}


	// NO XTREAM
	public String reopen(TkmTicketsRequest tkmRequest) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.REOPEN, tkmRequest, String.class);
		String get_resp = responseEntity.getBody();
		
		TkmTicketsResponse tkmTicketsResponse = TicketMgmtUtils.getTkmTicketsResponseFromJson(get_resp,"reopen");
		return tkmTicketsResponse.getErrMsg();
	}
	
	// NO XTREAM
	public String restart(TkmTicketsRequest tkmRequest) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.RESTART, tkmRequest, String.class);
		String get_resp = responseEntity.getBody();
		
		TkmTicketsResponse tkmTicketsResponse = TicketMgmtUtils.getTkmTicketsResponseFromJson(get_resp,"restart");
		return tkmTicketsResponse.getErrMsg();
	}
	
	public Map<String, Object> list(Map<String, Object> searchCriteria)  {
		LOGGER.info("In Rest call list to communicate with ghix-tkm module");

		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.LIST, searchCriteria, String.class);
		String get_resp = responseEntity.getBody();
		
		TkmTicketsResponse tkmTicketsResponse = TicketMgmtUtils.getTkmTicketsResponseFromJson(get_resp,"list");
		
		return tkmTicketsResponse.getTkmTicketsMap();
	}
	
	public void updateDefaultQueues(String defaultQueueIds) {
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity( TicketMgmtEndPoints.UPDATE_DEFAULT_QUEUES, defaultQueueIds, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();
		TkmTicketsResponse tkmTicketTaskResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		
	}
	
	public String cancelQleTicket(CancelTicketRequest tkmCancelTicketRequest) {
		
		ResponseEntity<String> responseEntity = mvcRestTemplate.postForEntity(TicketMgmtEndPoints.CANCEL_QLE_TICKET, tkmCancelTicketRequest, String.class);
		String get_resp = responseEntity.getBody();
		
		XStream xstream = GhixUtils.getXStreamStaxObject();

		TkmTicketsResponse tkmTicketsResponse = (TkmTicketsResponse) xstream.fromXML(get_resp);
		
		return null;
	}
	
	public static TkmTicketsResponse getTkmTicketsResponseFromJson(String jsonResponse,String callingAPI){
		TkmTicketsResponse tkmTicketsResponse = null;
		try {
			if(StringUtils.isNotEmpty(jsonResponse) && !jsonResponse.startsWith("ERROR IN JASON PARSING")){
			tkmTicketsResponse = JacksonUtils.getJacksonObjectReaderForJavaType(TkmTicketsResponse.class).readValue(jsonResponse);
			}
			else{
				tkmTicketsResponse = new TkmTicketsResponse();
				tkmTicketsResponse.setStatus(GhixConstants.RESPONSE_FAILURE);
				tkmTicketsResponse.setErrMsg(jsonResponse);
			}
		} catch (IOException e) {
			LOGGER.error("Exception occurred in retreiving response from json in " + callingAPI + " call.",e);
		}
		return tkmTicketsResponse;
	}
	
	public void updateApplicationEligibility(String caseNumber) {
		if(StringUtils.isNotBlank(caseNumber)){
			try {
				HttpEntity<Object> requestEntity = new HttpEntity<>(caseNumber);
				mvcRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+"nonfinancial/eligibility/updateEligibilityStatus", 
							HttpMethod.POST, requestEntity, String.class);
			}catch(Exception e) {
				LOGGER.error("Exception while updating EligibilityStatus", e);
			} 
		}						
	}
}
