package com.getinsured.hix.batch.enrollment.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.util.GhixBatchConstants;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;

public class ClearCarrierSendFlagTask implements Tasklet{

	private static final Logger LOGGER = LoggerFactory.getLogger(ClearCarrierSendFlagTask.class);
	private EnrollmentBatchService enrollmentBatchService;
	private String startDateStr;
	private String endDateStr;
	private String carrierFlagStatus;
	private JobService jobService;
	public static final String ERROR_MSG_VALID_INPUT="Please provide valid value( ";
	public static final String ERROR_MSG_VALID_INPUT_END= ") for Carrier_send_flag ";
	
	@Override
	public RepeatStatus execute(StepContribution stepContribution, ChunkContext chunkContext)throws GIException {
		LOGGER.info("ClearCarrierSendFlagTask.execute : START");
		try{
			//validate carrierResendFlag
			if(carrierFlagStatus!=null ){
				if(carrierFlagStatus.equalsIgnoreCase(GhixBatchConstants.RESEND_FLAG) || carrierFlagStatus.equalsIgnoreCase(GhixBatchConstants.HOLD_FLAG)){
					carrierFlagStatus= carrierFlagStatus.toUpperCase();
				}else{
					LOGGER.error(ERROR_MSG_VALID_INPUT+GhixBatchConstants.RESEND_FLAG+" or "+GhixBatchConstants.HOLD_FLAG+ERROR_MSG_VALID_INPUT_END);
					throw new GIException(ERROR_MSG_VALID_INPUT+GhixBatchConstants.RESEND_FLAG+" or "+GhixBatchConstants.HOLD_FLAG+ERROR_MSG_VALID_INPUT_END);
				}
			}else{
				LOGGER.error("Please provide value( "+GhixBatchConstants.RESEND_FLAG+" or "+GhixBatchConstants.HOLD_FLAG+ERROR_MSG_VALID_INPUT_END);
				throw new GIException("Please provide value( "+GhixBatchConstants.RESEND_FLAG+" or "+GhixBatchConstants.HOLD_FLAG+ERROR_MSG_VALID_INPUT_END);
			}
			if((getStartDateStr() == null && getEndDateStr() != null) || (getStartDateStr() != null && getEndDateStr() == null)){
				LOGGER.error("Please provide both Start Date and End Date");
				throw new GIException("Please provide both Start Date and End Date");
			}else if(getStartDateStr() != null && getEndDateStr() != null){
				if(getStartDateStr().equalsIgnoreCase("null") && getEndDateStr().equalsIgnoreCase("null")){
					setStartDateStr(null);
					setEndDateStr(null);
				}else{
					if(DateUtil.isValidDate(getStartDateStr(), EnrollmentConstants.BATCH_DATE_FORMAT) && DateUtil.isValidDate(getEndDateStr(), EnrollmentConstants.BATCH_DATE_FORMAT)){
						if(!DateUtil.isValidDateInterval(DateUtil.StringToDate(getStartDateStr(), EnrollmentConstants.BATCH_DATE_FORMAT),DateUtil.StringToDate(getEndDateStr(),EnrollmentConstants.BATCH_DATE_FORMAT))){
							LOGGER.error(" ClearCarrierSendFlagTask.execute : Start date is greater than End date.");
							throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						}
					}else{
						LOGGER.error("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
						throw new GIException("Please provide valid start date and End Date in "+ EnrollmentConstants.BATCH_DATE_FORMAT+ " format");
					}
				}
			}
			
			Long jobExecutionId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
			
			String batchJobStatus=null;
			if(jobService != null && jobExecutionId != -1){
				batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
			}
			if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
				throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
			}
			
			getEnrollmentBatchService().clearCarrierSendFlag(getCarrierFlagStatus(), getStartDateStr(), getEndDateStr());
		}
		catch (Exception e){
			LOGGER.error("ClearCarrierSendFlagTask :: failed to execute -- ",e);
			throw new GIException("ClearCarrierSendFlagTask :: failed to execute -- " + e.getMessage(), e);
		}
		LOGGER.info("ClearCarrierSendFlagTask.execute : END");
		return RepeatStatus.FINISHED;
	}


	public String getStartDateStr() {
		return startDateStr;
	}


	public void setStartDateStr(String startDateStr) {
		this.startDateStr = startDateStr;
	}


	public String getEndDateStr() {
		return endDateStr;
	}


	public void setEndDateStr(String endDateStr) {
		this.endDateStr = endDateStr;
	}


	public String getCarrierFlagStatus() {
		return carrierFlagStatus;
	}


	public void setCarrierFlagStatus(String carrierFlagStatus) {
		this.carrierFlagStatus = carrierFlagStatus;
	}


	public EnrollmentBatchService getEnrollmentBatchService() {
		return enrollmentBatchService;
	}


	public void setEnrollmentBatchService(EnrollmentBatchService enrollmentBatchService) {
		this.enrollmentBatchService = enrollmentBatchService;
	}


	public JobService getJobService() {
		return jobService;
	}


	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
