package com.getinsured.hix.batch.hub.reader;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import com.getinsured.hix.batch.hub.model.HubResponse;

public class HubResponseReaderMapper implements RowMapper<HubResponse> {
	
	private static final Logger LOGGER =LoggerFactory.getLogger(HubResponseReaderMapper.class);
	
	/*
	 * DB column mappings for HUB_RESPONSES table
	 */
	private static final String ID = "ID";
	
	private static final String CASE_NUMBER = "HUB_CASE_NUMBER";
	
	private static final String STATUS = "STATUS";
	
	private static final String RETRY_COUNT = "RETRY_COUNT";
	
	@Override
	public HubResponse mapRow(ResultSet resultSet, int count) throws SQLException {
		HubResponse response = new HubResponse();
		response.setId(resultSet.getLong(ID));
		response.setCaseNumber(resultSet.getString(CASE_NUMBER));
		response.setStatus(resultSet.getString(STATUS));
		response.setRetryCount(resultSet.getInt(RETRY_COUNT));
		LOGGER.info("Mapped row " + response);
		return response;
	}
	
	
}
