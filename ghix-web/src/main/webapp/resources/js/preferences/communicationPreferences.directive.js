(function() {
	"use strict";
	var app = angular.module("communicationPreferencesDirectiveApp", []);

	app.directive('numberOnly', numberOnly);
	app.directive('dateValidation', dateValidation);
	app.directive('bootstrapTooltip', bootstrapTooltip);

	function numberOnly(){
		var directive = {
			restrict: 'AE',
			require: 'ngModel',
			link: function (scope, element, attrs, ngModel) {
				function fromUser(userInput) {
	                if (userInput) {
	                    var transformedInput = userInput.replace(/[^0-9]/g, '');

	                    if (transformedInput !== userInput) {
	                        ngModel.$setViewValue(transformedInput);
	                        ngModel.$render();
	                    }
	                    return transformedInput;
	                }
	                return undefined;
	            }            
	            ngModel.$parsers.push(fromUser);
			}
		};

		return directive;
	}



	function dateValidation(){
		var directive = {
			restrict : 'AE',
			require : 'ngModel',
			link : function(scope, element, attrs, ngModel) {
				
				var dateType = attrs.dateValidation;
				
				function isDateValid(value){
					//remove placeholder
					var date = value.replace(/[A-Z]/g, '');
					
					//remove extra-last-character
					if(date.length === 11){
						date = date.substring(0, date.length - 1);
					}
					
					
					if(date.length === 10){
						if(!moment(date,'MM/DD/YYYY').isValid()){
							return false;
						}
						
						if(dateType === 'dob'){
							if(ageCheck('between',date, 0, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'future'){
							if(ageCheck('greater',date, 0)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'under21'){
							if(ageCheck('between',date, 21, 104)){
								return true;
							}else{
								return false;
							};
						}else if(dateType === 'qe'){
							var inputDate = date.split('/');
							var start = new Date(inputDate[2], inputDate[0]-1, inputDate[1]);

							var today = new Date();
							var end = new Date(today.getFullYear(), today.getMonth(), today.getDate());
							
							var diff = (start.getTime() - end.getTime()) / (24*60*60*1000);
							if(diff <= 0 && diff >= -60){
								return true;
							}else{
								return false;
							};
						}
					}else{
						return true;
					}
					
				}

				function ageCheck(comparator, birth, age, maxAge){
					var birthDate = new Date(birth);
					var today = new Date();
					
					 var years = (today.getFullYear() - birthDate.getFullYear());

				    if (today.getMonth() < birthDate.getMonth() || today.getMonth() == birthDate.getMonth() && today.getDate() < birthDate.getDate()) {
				        years--;
				    }
				    
				    if(comparator === 'older'){
				    	if(years >= parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else if(comparator === 'between'){
						if(years >= parseInt(age) && years <= parseInt(maxAge)){
					    	return true;
					    }else{
					    	return false;
					    }
					}else{
						if(years < parseInt(age)){
					    	return true;
					    }else{
					    	return false;
					    }
					}
					
				};

				ngModel.$parsers.unshift(function(value){
					var dateValidationResult = isDateValid(value);
					
					ngModel.$setValidity(dateType, dateValidationResult);
					
					return value;	
				});
				
				 ngModel.$formatters.unshift(function(value) {
					 if(value === undefined || value === null){
						 return;
					 }
					 var dateValidationResult = isDateValid(value);
						
					 ngModel.$setValidity(dateType, dateValidationResult);
					 
					 return value;
	             });
				
				
			}
		}

		return directive;
	}

	function bootstrapTooltip(){
		var directive = {
			restrict : 'AE',
			link : function(scope, elem, attrs) {
				elem.tooltip();
			}
		};
		
		return directive;
	}
})();
