package com.getinsured.hix.entity.assister.controller;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.entity.AssisterDesignateResponseDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityAssisterSearchResult;
import com.getinsured.hix.dto.entity.EnrollmentEntityRequestDTO;
import com.getinsured.hix.dto.entity.EnrollmentEntityResponseDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.entity.assister.service.AssisterDesignateService;
import com.getinsured.hix.entity.assister.service.AssisterLanguagesService;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.enrollmententity.service.EnrollmentEntityService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.DesignateAssister;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.thoughtworks.xstream.XStream;

/**
 * The controller class that receives Rest requests from ghix-web. It then
 * performs the requested operation and returns response
 */
@Controller
@RequestMapping(value = "/entity/assister")
public class LocateAssisterController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LocateAssisterController.class);

	@Autowired
	private AssisterDesignateService assisterDesignateService;

	@Autowired
	private AssisterService assisterService;

	@Autowired
	private AssisterLanguagesService assisterLanguagesService;

	@Autowired
	private EnrollmentEntityService enrollmentEntityService;
	
	/**
	 * Retrieves List of Enrollment Entities based on Search Criteria
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with
	 *         List<EnrollmentEntityAssisterSearchResult> object
	 */
	@RequestMapping(value = "/searchentities", method = RequestMethod.POST)
	@ResponseBody
	public String searchEnrollmentEntityForAssisters(@RequestBody String request) {
		LOGGER.info("searchEnrollmentEntityForAssisters: START");
		ObjectReader reader = null;
		ObjectWriter writer = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		String response = null;
		
		try{
			reader = JacksonUtils.getJacksonObjectReader(EnrollmentEntityRequestDTO.class);
			enrollmentEntityRequestDTO = reader.readValue(request);
			LOGGER.debug("Locate Assister : Retrieving List Of Entities : Starts");
			enrollmentEntityResponseDTO = enrollmentEntityService.searchEnrollmentEntityForAssisters(enrollmentEntityRequestDTO);
			LOGGER.debug("Locate Assister : Retrieving List Of Entities : Ends");
			writer = JacksonUtils.getJacksonObjectWriter(EnrollmentEntityResponseDTO.class);
			response = writer.writeValueAsString(enrollmentEntityResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while searching entites." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("searchEnrollmentEntityForAssisters: END");
		return response;
	}

	/**
	 * Retrieves Enrollment Entity Detail based on given ID
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with EnrollmentEntity object
	 */
	@RequestMapping(value = "/enrollmententitydetailbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getEnrollmentEntityDetailById(@RequestBody String request) {
		
		LOGGER.info("getEnrollmentEntityDetailById: START");
		
		EnrollmentEntityResponseDTO enrollmentEntityResponseDTO = null;
		EnrollmentEntityRequestDTO enrollmentEntityRequestDTO = null;
		String response = null;
		try{
		enrollmentEntityRequestDTO = JacksonUtils.getJacksonObjectReader(EnrollmentEntityRequestDTO.class).readValue(request);
		LOGGER.debug("Locate Assister : Retrieving Entity Detail : Starts");
		EnrollmentEntityAssisterSearchResult enrollEntityAssisterSearchResult = enrollmentEntityService
				.getEnrollmentEntityDetails(enrollmentEntityRequestDTO.getEnrollmentEntity().getId(),
						enrollmentEntityRequestDTO.getSite().getId());
		enrollmentEntityResponseDTO = new EnrollmentEntityResponseDTO();
		enrollmentEntityResponseDTO.setEnrollEntityAssisterSearchResult(enrollEntityAssisterSearchResult);
		LOGGER.debug("Locate Assister : Retrieving Entity Detail : Ends");
		response = JacksonUtils.getJacksonObjectWriter(EnrollmentEntityResponseDTO.class).writeValueAsString(enrollmentEntityResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while fecthing entity details." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getEnrollmentEntityDetailById: END");
		
		return response;
	}

	/**
	 * Invokes Assister Designate service to designate Assister for given
	 * Individual
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating individual id, assister id and
	 *            Esignature Name
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/designateassister", method = RequestMethod.POST)
	@ResponseBody
	public String designateAssister(@RequestBody String requestXML) {
		
		LOGGER.info("designateAssister: START");
		Assister assister = null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		DesignateAssister designateAssister = null;

		LOGGER.debug("Locate Assister : Save Assister Designation Detail Starts.");

		XStream xstream = GhixUtils.xStreamHibernateXmlMashaling();
		AssisterRequestDTO assisterRequestDTO = (AssisterRequestDTO) xstream.fromXML(requestXML);

		try {
			designateAssister = assisterDesignateService.findAssisterByIndividualId(assisterRequestDTO
					.getIndividualId());
			
			if(designateAssister==null){
				designateAssister = new DesignateAssister();
			}
			if(assisterRequestDTO.getId() != null) {
				assister = assisterService.findAssisterById(assisterRequestDTO.getId());
			}
			else {
				assister = assisterService.findByUserId(assisterRequestDTO.getUserId());
			}

			if(assisterRequestDTO.getDesigStatus() == null) {
				assisterRequestDTO.setDesigStatus(DesignateAssister.Status.Pending);
			}
			
			designateAssister.setIndividualId(assisterRequestDTO.getIndividualId());
			designateAssister.setAssisterId(assister.getId());
			designateAssister.setEntityId(assister.getEntity().getId());
			designateAssister.setStatus(assisterRequestDTO.getDesigStatus());
			designateAssister.setEsignBy(assisterRequestDTO.getEsignName());
			designateAssister.setEsignDate(new TSDate());
			designateAssister.setShow_switch_role_popup('Y');
			designateAssister.setCreated(new TSDate());
			int loggedInUserId=EntityUtils.getLoggedInUser();
			if(loggedInUserId != -1)
			{
				designateAssister.setCreatedBy(Long.valueOf(loggedInUserId));
				if(designateAssister.getLastUpdatedBy()!=null)
				{
				designateAssister.setLastUpdatedBy(Long.valueOf(loggedInUserId));
				}
				
			}	

			EntityResponseDTO entityResponseDTO = assisterDesignateService.saveDesignateAssister(designateAssister, 0);
			
			if(entityResponseDTO!=null && !EntityUtils.isEmpty(entityResponseDTO.getStatus()) && EntityConstants.RESPONSE_SUCCESS.equals(entityResponseDTO.getStatus())){
				assisterResponseDTO.setAssister(assister);
				
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
				assisterResponseDTO.setStatus(EntityConstants.RESPONSE_SUCCESS);
			} else {
				assisterResponseDTO.setStatus(EntityConstants.RESPONSE_FAILURE);
			}
		} catch (Exception exception) {
			LOGGER.error("Locate Assister : Save Assister Designation Detail : Exception occurred while saving Assister Detail : "
					, exception);

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
			
			assisterResponseDTO.setStatus(EntityConstants.RESPONSE_FAILURE);
		}

		LOGGER.debug("Locate Assister : Save Assister Designation Detail Ends.");

		LOGGER.info("designateAssister: END");
		
		return GhixUtils.xStreamHibernateXmlMashaling().toXML(assisterResponseDTO);
	}

	@RequestMapping(value = "/findIndividualDesignation", method = RequestMethod.POST)
	@ResponseBody
	public DesignateAssister getActiveDesignation(@RequestBody Integer householdId) {
		DesignateAssister designateAssister = null;
		designateAssister = assisterDesignateService.findAssisterByIndividualId(householdId);
		return designateAssister;
	}
	
	/**
	 * Invokes Assister Designate service to get Designate Assister for given
	 * Individual
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating individual id
	 * @return marshaled assister response DTO
	 */
	//@GiAudit(transactionName = "Get Designate Assister By Individual Identifier", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.EE_AGENT)
	@RequestMapping(value = "/getdesignateassisterbyindividualid", method = RequestMethod.POST)
	@ResponseBody
	public AssisterDesignateResponseDTO getDesignateAssisterByIndividualId(@RequestBody AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("getDesignateAssisterByIndividualId: START");
		
		AssisterDesignateResponseDTO assisterDesignateResponseDTO = new AssisterDesignateResponseDTO();

		LOGGER.debug("Locate Assister : Retrieve Assister Designate Detail Starts.");

		DesignateAssister designateAssister = null;
		Assister assister = null;

		try {
			designateAssister = assisterDesignateService.findAssisterByIndividualId(assisterRequestDTO
					.getIndividualId());
			if(designateAssister!=null){
				assister = assisterService.findAssisterById(designateAssister.getAssisterId());
			}
			
			assisterDesignateResponseDTO.setDesignateAssister(designateAssister);
			assisterDesignateResponseDTO.setAssister(assister);
			
			assisterDesignateResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterDesignateResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);


		} catch (Exception exception) {
			LOGGER.error("Locate Assister : Retrieve Assister Designate Detail : Exception occurred while retrieving Assister Designate Detail : "
					, exception);

			assisterDesignateResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterDesignateResponseDTO.setResponseDescription(exception.getMessage());
		}

		LOGGER.debug("Locate Assister : Retrieve Assister Designate Detail Ends.");

		LOGGER.info("getDesignateAssisterByIndividualId: END");
		
		return assisterDesignateResponseDTO;
	}

	/**
	 * Invokes Assister and AssisterLanguage service to get to get list of
	 * Assisters and associated Languages for given Enrollment Entity and Site
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating enrollment entity id and site id
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/getassistersbyentityandsite", method = RequestMethod.POST)
	@ResponseBody
	public String getListOfAssistersByEnrollmentEntityAndSite(@RequestBody String request) {
		
		LOGGER.info("getListOfAssistersByEnrollmentEntityAndSite: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		List<Assister> listOfAssisters = null;
		Map<Assister, AssisterLanguages> map = new LinkedHashMap<Assister, AssisterLanguages>();
		AssisterLanguages assisterLanguages = null;
		String response =  null;
		try {
			
			/*listOfAssisters = assisterService.findByEnrollmentEntityAndSite(assisterRequestDTO.getEnrollmentEntityId(),
					assisterRequestDTO.getSiteId());*/
			
			AssisterRequestDTO assisterRequestDTO = JacksonUtils.getJacksonObjectReader(AssisterRequestDTO.class).readValue(request);
			LOGGER.debug("Locate Assister : Retrieve Assisters By Enrollment Entity and Site Starts.");
			listOfAssisters = assisterService.searchAssistersByEntityAndSite(assisterRequestDTO);

			for (Assister assister : listOfAssisters) {
				assisterLanguages = assisterLanguagesService.findByAssister(assister);
				map.put(assister, assisterLanguages);
			}
			
			assisterResponseDTO.setListOfAssisters(listOfAssisters);
			assisterResponseDTO.setAssisterWithLanguages(map);
			assisterResponseDTO.setNoOfAssistersForEEAndSite(assisterService.totalAssistersByEntityAndSite(assisterRequestDTO));
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			LOGGER.debug("Locate Assister : Retrieve Assisters By Enrollment Entity and Site Ends.");

		} catch (Exception exception) {
			LOGGER.error("Locate Assister : Retrieve Assisters By Enrollment Entity and Site : Exception occurred while retrieving Assisters By Enrollment Entity and Site : "
					, exception);

			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing json response." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getListOfAssistersByEnrollmentEntityAndSite: END");
		
		return response;
	}

	/**
	 * Retrieves No Of Assisters by Enrollment Entity for a given Site
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with EnrollmentEntity object
	 */
	@RequestMapping(value = "/noofassistersbyenrollmententityandsite", method = RequestMethod.POST)
	@ResponseBody
	public String getNoOfAssistersForEnrollmentEntityAndSite(@RequestBody String request) {
		
		LOGGER.info("getNoOfAssistersForEnrollmentEntityAndSite: START");
		AssisterResponseDTO assisterResponseDTO = null;
		AssisterRequestDTO assisterRequestDTO = null;
		int noOfAssisters = 0;
		String response = null;
		try{
			assisterRequestDTO = JacksonUtils.getJacksonObjectReader(AssisterRequestDTO.class).readValue(request);
			LOGGER.debug("Locate Assister : Retrieving No Of Assisters For Enrollment Entity : Starts");
			noOfAssisters = assisterService.totalAssistersByEntityAndSite(assisterRequestDTO);
			assisterResponseDTO = new AssisterResponseDTO();
			assisterResponseDTO.setNoOfAssistersForEEAndSite(noOfAssisters);
			LOGGER.debug("Locate Assister : Retrieving No Of Assisters For Enrollment Entity : Ends");
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while feching no of assisters for Enrollment Entity." , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		LOGGER.info("getNoOfAssistersForEnrollmentEntityAndSite: END");
		return response;
	}
}
