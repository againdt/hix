
package com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ReverifyAgency3InitVerifRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ReverifyAgency3InitVerifRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}CaseNumber"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}ReceiptNumber" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}AlienNumber" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}I94Number" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}SEVISID" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}PassportCountry" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}NaturalizationNumber" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}CitizenshipNumber" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}VisaNumber" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}LastName" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}FirstName" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}MiddleName" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}DateOfBirth" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}RequestedCoverageStartDate"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}FiveYearBarApplicabilityIndicator"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}RequesterCommentsForHub" minOccurs="0"/>
 *         &lt;element ref="{http://vilpsav.ee.sim.dsh.cms.hhs.gov}CategoryCode" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ReverifyAgency3InitVerifRequestType", propOrder = {
    "caseNumber",
    "receiptNumber",
    "alienNumber",
    "i94Number",
    "sevisid",
    "passportCountry",
    "naturalizationNumber",
    "citizenshipNumber",
    "visaNumber",
    "lastName",
    "firstName",
    "middleName",
    "dateOfBirth",
    "requestedCoverageStartDate",
    "fiveYearBarApplicabilityIndicator",
    "requesterCommentsForHub",
    "categoryCode"
})
@XmlRootElement(name="ReverifyAgency3InitVerifRequest")
public class ReverifyAgency3InitVerifRequestType {

    @XmlElement(name = "CaseNumber", required = true)
    protected String caseNumber;
    @XmlElement(name = "ReceiptNumber")
    protected String receiptNumber;
    @XmlElement(name = "AlienNumber")
    protected String alienNumber;
    @XmlElement(name = "I94Number")
    protected String i94Number;
    @XmlElement(name = "SEVISID")
    protected String sevisid;
    @XmlElement(name = "PassportCountry")
    protected PassportCountryType passportCountry;
    @XmlElement(name = "NaturalizationNumber")
    protected String naturalizationNumber;
    @XmlElement(name = "CitizenshipNumber")
    protected String citizenshipNumber;
    @XmlElement(name = "VisaNumber")
    protected String visaNumber;
    @XmlElement(name = "LastName")
    protected String lastName;
    @XmlElement(name = "FirstName")
    protected String firstName;
    @XmlElement(name = "MiddleName")
    protected String middleName;
    @XmlElement(name = "DateOfBirth")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlElement(name = "RequestedCoverageStartDate", required = true)
    protected XMLGregorianCalendar requestedCoverageStartDate;
    @XmlElement(name = "FiveYearBarApplicabilityIndicator")
    protected boolean fiveYearBarApplicabilityIndicator;
    @XmlElement(name = "RequesterCommentsForHub")
    protected String requesterCommentsForHub;
    @XmlElement(name = "CategoryCode")
    protected String categoryCode;

    /**
     * Gets the value of the caseNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCaseNumber() {
        return caseNumber;
    }

    /**
     * Sets the value of the caseNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCaseNumber(String value) {
        this.caseNumber = value;
    }

    /**
     * Gets the value of the receiptNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceiptNumber() {
        return receiptNumber;
    }

    /**
     * Sets the value of the receiptNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceiptNumber(String value) {
        this.receiptNumber = value;
    }

    /**
     * Gets the value of the alienNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlienNumber() {
        return alienNumber;
    }

    /**
     * Sets the value of the alienNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlienNumber(String value) {
        this.alienNumber = value;
    }

    /**
     * Gets the value of the i94Number property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getI94Number() {
        return i94Number;
    }

    /**
     * Sets the value of the i94Number property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setI94Number(String value) {
        this.i94Number = value;
    }

    /**
     * Gets the value of the sevisid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSEVISID() {
        return sevisid;
    }

    /**
     * Sets the value of the sevisid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSEVISID(String value) {
        this.sevisid = value;
    }

    /**
     * Gets the value of the passportCountry property.
     * 
     * @return
     *     possible object is
     *     {@link PassportCountryType }
     *     
     */
    public PassportCountryType getPassportCountry() {
        return passportCountry;
    }

    /**
     * Sets the value of the passportCountry property.
     * 
     * @param value
     *     allowed object is
     *     {@link PassportCountryType }
     *     
     */
    public void setPassportCountry(PassportCountryType value) {
        this.passportCountry = value;
    }

    /**
     * Gets the value of the naturalizationNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNaturalizationNumber() {
        return naturalizationNumber;
    }

    /**
     * Sets the value of the naturalizationNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNaturalizationNumber(String value) {
        this.naturalizationNumber = value;
    }

    /**
     * Gets the value of the citizenshipNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCitizenshipNumber() {
        return citizenshipNumber;
    }

    /**
     * Sets the value of the citizenshipNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCitizenshipNumber(String value) {
        this.citizenshipNumber = value;
    }

    /**
     * Gets the value of the visaNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVisaNumber() {
        return visaNumber;
    }

    /**
     * Sets the value of the visaNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVisaNumber(String value) {
        this.visaNumber = value;
    }

    /**
     * Gets the value of the lastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the value of the lastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastName(String value) {
        this.lastName = value;
    }

    /**
     * Gets the value of the firstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the value of the firstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstName(String value) {
        this.firstName = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the requestedCoverageStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRequestedCoverageStartDate() {
        return requestedCoverageStartDate;
    }

    /**
     * Sets the value of the requestedCoverageStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRequestedCoverageStartDate(XMLGregorianCalendar value) {
        this.requestedCoverageStartDate = value;
    }

    /**
     * Gets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public boolean isFiveYearBarApplicabilityIndicator() {
        return fiveYearBarApplicabilityIndicator;
    }

    /**
     * Sets the value of the fiveYearBarApplicabilityIndicator property.
     * 
     */
    public void setFiveYearBarApplicabilityIndicator(boolean value) {
        this.fiveYearBarApplicabilityIndicator = value;
    }

    /**
     * Gets the value of the requesterCommentsForHub property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequesterCommentsForHub() {
        return requesterCommentsForHub;
    }

    /**
     * Sets the value of the requesterCommentsForHub property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequesterCommentsForHub(String value) {
        this.requesterCommentsForHub = value;
    }

    /**
     * Gets the value of the categoryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryCode() {
        return categoryCode;
    }

    /**
     * Sets the value of the categoryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryCode(String value) {
        this.categoryCode = value;
    }

}
