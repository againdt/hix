package com.getinsured.eligibility.nonfinancial;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.ssap.integration.household.service.NonFinancialApplicationEligibilityService;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

@Controller(value="nonFinancialEligibilityController")
@RequestMapping("/nonfinancial/eligibility")
public class EligibilityController {
    
    private static final Logger LOGGER = Logger.getLogger(EligibilityController.class);
    
    private static final String APPEVENT_APPLICATION = "APPEVENT_APPLICATION";
    private static final String APPLICATION_ELIGIBILITY_FULL = "FULL_ELIGIBILITY_RECEIVED";
	private static final String APPLICATION_ID  = "Application ID";
	private static final String APPLICATION_TYPE  = "Application Type";
	private static final String NON_FINANCIAL_APPLICATION  = "Non Financial Application";
    
    
    
    @Autowired
    private SsapApplicationRepository ssapApplicationRepository;
    
    @Autowired
    private NonFinancialApplicationEligibilityService nonFinancialApplicationEligibilityService;
    
    @Autowired 
    private SsapJsonBuilder ssapJsonBuilder;
    
    @Autowired private AppEventService appEventService;
    @Autowired private LookupService lookupService;
    
    @RequestMapping(value = "/rerun", method = RequestMethod.POST)
    public ResponseEntity<String> rerun(@RequestBody String caseNumber) throws Exception {
        try {
            List<SsapApplication>  ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
            
            if(null != ssapApplications && !ssapApplications.isEmpty()) {
                SsapApplication ssapApplication = ssapApplications.get(0);
                nonFinancialApplicationEligibilityService.determineEligibility(ssapApplication);
                
                // Re-fetch the application as it might have been updated during eligibility determination
                ssapApplication = ssapApplicationRepository.getApplicationsById(ssapApplication.getId()).get(0);
                SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());
                
                if(nonFinancialApplicationEligibilityService.isVerificationDone(ssapApplication.getSsapApplicants(), ssapJSON)) {
                    ssapApplication.setEligibilityStatus(EligibilityStatus.AE);
                    ssapApplication.setEligibilityReceivedDate(new TSDate());
                    ssapApplicationRepository.save(ssapApplication);
                    logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_FULL, ssapApplication);
                }
                nonFinancialApplicationEligibilityService.sendUpdatedEligibilityNotification(ssapApplication.getCaseNumber());
            } else {
                return new ResponseEntity<String>("No application with casenumber - " + caseNumber, HttpStatus.NOT_FOUND);
            }
        } catch(Exception ex) {
            LOGGER.error("Error re-running eligibility for application " + caseNumber, ex);
            return new ResponseEntity<String>("Error re-running eligibility for application. " + ExceptionUtils.getFullStackTrace(ex), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<String>("Success", HttpStatus.OK);
    }
    
    private void logIndividualAppEvent(String eventName, String eventType,SsapApplication ssapApplication){
	       
		try {

			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			Map<String, String> mapEventParam = new HashMap<>();
			mapEventParam.put(APPLICATION_ID,String.valueOf(ssapApplication.getId()));
			mapEventParam.put(APPLICATION_TYPE,NON_FINANCIAL_APPLICATION);
			
			if(null !=ssapApplication.getCmrHouseoldId()){
				eventInfoDto.setModuleId(ssapApplication.getCmrHouseoldId().intValue());	
			}
			
			eventInfoDto.setModuleName("INDIVIDUAL");
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event"+e.getMessage(), e);
			//throw new GIRuntimeException(e);
		}
	}
    
    @RequestMapping(value = "/updateEligibilityStatus", method = RequestMethod.POST)
	@ResponseBody
	public String updateEligibilityStatus(@RequestBody String caseNumber){
		
    	try {
    		List<SsapApplication>  ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);
             
    		if(null != ssapApplications && !ssapApplications.isEmpty()) {
    			SsapApplication ssapApplication = ssapApplications.get(0);
    			SingleStreamlinedApplication ssapJSON = ssapJsonBuilder.transformFromJson(ssapApplication.getApplicationData());

    			String response = nonFinancialApplicationEligibilityService.updateEligibilityStatus(ssapApplication, ssapJSON);

    			if(EligibilityConstants.SUCCESS.equalsIgnoreCase(response)) {
                    logIndividualAppEvent(APPEVENT_APPLICATION, APPLICATION_ELIGIBILITY_FULL, ssapApplication);
                }

    		} 
         } catch(Exception ex) {
             LOGGER.error("Error occured while updating eligibility Statusre for application " + caseNumber, ex);
             return EligibilityConstants.FAILURE;
         }
		
		return EligibilityConstants.FAILURE;		
	}
}
