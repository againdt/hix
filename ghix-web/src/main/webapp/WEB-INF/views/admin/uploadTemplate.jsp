<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>


<div class="gutter10">
	<div class="row-fluid">
		<h1>
			Upload Template
		</h1>
		<hr>
		<div style="font-size: 14px; color: blue;" >
			Please use ops admin login and edit the template "Notices->manage Notifications->[template]->edit template (gear menu option)".
		</div>
		<div style="font-size: 14px; color: blue; display:none;" >
			<c:if test="${Upload_Msg != null && Upload_Msg != ''}">
				<p>
					<c:out value="${Upload_Msg}"></c:out>
				<p />
				<c:remove var="Upload_Msg" />
			</c:if>
		</div>
		
		<hr>
	</div>
	
	<!-- start of span3 -->
	<div class="row-fluid" style="display:none;">
		<!-- add id for skip side bar -->
		<div id="sidebar"></div>
		<!-- end of span3 -->

		<!-- start of span9 --->
		<div class="span9 offset1">
			<form class="form-horizontal" id="uploadTemplate" name="uploadTemplate" enctype="multipart/form-data"
				action="<c:url value='/template/uploadTemplate'/>" method="POST" >
				<!--/#basic-information -->
				<div id="select-profile">
					<fieldset>
						<div class="control-group">  
							<label class="gutter10-tb control-label" for="templateProfile" ><spring:message code="label.selectProfile"/><img
								src='<c:url value="/resources/images/requiredAsterix.png" />'
								width="10" height="10" alt="Required!" />
							</label>
							<div class="gutter10-b controls">
								<select id="templateProfile" name="templateProfile">
									<option value="">Select</option>
									<option value="ms">MS</option>
									<option value="nm">NM</option>
									<option value="phix">PHIX</option>
								</select>
								<div id="templateProfile_error"></div>
							</div>
						</div>
					</fieldset>
				</div>
				<!--/#set-password -->
				
				<div>
						<label class="gutter10-tb control-label" for="templateProfile"><spring:message code="label.uploadTemplate"/><img
									src='<c:url value="/resources/images/requiredAsterix.png" />'
									width="10" height="10" alt="Required!" />
						</label>
						<!-- File upload plugin starts -->
						<div class="gutter10-b controls">
						<input type="file" class="input-file" id="templateUpload"
							name="files[]">&nbsp;
						<!-- The table listing the files available for attaching/removing -->
						<div id="templateUpload_error"></div>
						</div>
						<!-- File upload plugin ends -->
				</div>
				<!--/#terms -->

				<div class="form-actions">
					<input type="button"  name="submitbtn" id="submitbtn" onClick="javascript:uploadTemplateSubmit();" 
					value="<spring:message  code='label.signupBtn'/>" title="<spring:message  code='label.signupBtn'/>"	class="btn btn-primary" />
				</div>
				<!-- end of form-actions -->
			</form>
			<!-- end of form -->
		</div>
		<!-- end of span9 -->
	</div>
	<!-- end of row-fluid -->
</div>
<!-- end of gutter10-->

<script type="text/javascript">
    $('.ttclass').tooltip();

	function resetErrorMsg(element) {
		var elementId = element.id;

		$("#" + elementId + "_error").html('');
	}
	
	function uploadTemplateSubmit( ){
		 var fup = document.getElementById('templateUpload');
		 var fileName = fup.value
		 if(fileName==""){
			alert("Please select file to upload")
		 }
		 else{
			 $('#uploadTemplate').submit();
		 }
	}

	var validator = $("#uploadTemplate").validate(
	{
		rules : {
			templateProfile : {
				required : true
			},
			
		},
		messages : {

			templateProfile : {
				required : "<span> <em class='excl'>!</em>Please Select Profile</span>"
			},

		},
		onkeyup : false,
		errorClass : "error",
		errorPlacement : function(error, element) {
			var elementId = element.attr('id');
			error.appendTo($("#" + elementId + "_error"));
			$("#" + elementId + "_error")
					.attr('class', 'error');
		},

	});
	

</script>
