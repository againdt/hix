package com.getinsured.hix.admin.util;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.indportal.IndividualPortalConstants;

/**
 * HIX-82138
 * Display all Announcements to ADMIN
 * Sort by DESC order of announcement effective end date
 * 
 * @author sahoo_s
 */
public class AnnouncementComparator implements Comparator<AnnouncementDTO>{

	private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementComparator.class);
	
	@Override
	public int compare(AnnouncementDTO announcementOne, AnnouncementDTO announcementTwo) {
		if (null != announcementOne.getEffectiveEndDate() 
				&& null != announcementTwo.getEffectiveEndDate()) {
			
			SimpleDateFormat dateFormat = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);
			try {
				Date endDateOne = dateFormat.parse(announcementOne.getEffectiveEndDate());
				Date endDateTwo = dateFormat.parse(announcementTwo.getEffectiveEndDate());
				return Long.valueOf(endDateTwo.getTime()).compareTo(Long.valueOf(endDateOne.getTime()));
			} catch (Exception e) {
				LOGGER.error("Exception while date comparison in Announcement Comparator");
			}
		}
		return 0;
	}
	
}
