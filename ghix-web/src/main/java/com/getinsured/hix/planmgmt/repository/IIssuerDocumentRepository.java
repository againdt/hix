package com.getinsured.hix.planmgmt.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.IssuerDocument;

@Repository("iIssuerDocumentRepository")
public interface IIssuerDocumentRepository extends JpaRepository<IssuerDocument, Integer> {

	@Modifying
    @Transactional
	@Query("DELETE FROM IssuerDocument i WHERE i.documentName = :documentId AND i.issuer.id = :isserId")
	int deleteIssuerDocument(@Param("isserId") Integer isserId, @Param("documentId") String documentId);	

}


