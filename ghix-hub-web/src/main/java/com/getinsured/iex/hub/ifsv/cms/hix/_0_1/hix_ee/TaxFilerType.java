//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.27 at 09:16:54 AM IST 
//


package com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.hub.ifsv.niem.niem.structures._2.ComplexObjectType;


/**
 * A data type for an entity that files tax returns.
 * 
 * <p>Java class for TaxFilerType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxFilerType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}TINIdentification"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFilerType", propOrder = {
    "tinIdentification"
})
public class TaxFilerType
    extends ComplexObjectType
{

    @XmlElement(name = "TINIdentification", namespace = "http://hix.cms.gov/0.1/hix-core", required = true)
    protected IdentificationType tinIdentification;

    /**
     * Gets the value of the tinIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getTINIdentification() {
        return tinIdentification;
    }

    /**
     * Sets the value of the tinIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setTINIdentification(IdentificationType value) {
        this.tinIdentification = value;
    }

}
