package com.getinsured.eligibility.ssap.model;

import java.io.Serializable;

public class EligibilityResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String status;
	private String message;
	private EligibilityResult eligibilityResult; 
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public EligibilityResult getEligibilityResult() {
		return eligibilityResult;
	}
	public void setEligibilityResult(EligibilityResult eligibilityResult) {
		this.eligibilityResult = eligibilityResult;
	}

}
