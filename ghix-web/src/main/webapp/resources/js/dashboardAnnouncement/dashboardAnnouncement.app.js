(function(){
	'use strict';
	
	angular.module("dashboardAnnouncementApp",['ui.router', 'ng.ckeditor', 'ui.mask', 'ngSanitize', 'ui.bootstrap'])
	.config(function($stateProvider, $urlRouterProvider, $httpProvider){
		$urlRouterProvider.otherwise('/announcements');
		
		$stateProvider
			.state('announcementList',{
				url: '/announcements',
				templateUrl: '/hix/resources/html/dashboardAnnouncement/dashboardAnnouncementList.temp.html',
	            controller: 'DashboardAnnouncementListController',
	            controllerAs: 'vm'
	        })
	        .state('announcementEdit',{
				url: '/announcement/{id}',
				templateUrl: '/hix/resources/html/dashboardAnnouncement/editDashboardAnnouncement.temp.html',
	            controller: 'DashboardAnnouncementController',
	            controllerAs: 'vm',
	            resolve:{
	            	anouncementData:["$stateParams","$http",function($stateParams,$http){
	            		if($stateParams.id !== ''){
	            			return $http.get('../announcement?announcementId='+$stateParams.id);
	            		}else{
	            			return true;
	            		}
	            		
	            	}],
	            	editMode:["$stateParams", function($stateParams){
	            		if($stateParams.id !== ''){
	            			return true;
	            		}else{
	            			return false;
	            		}
	            		
	            	}]
	            }
	        });
		
		
		
		//initialize get if not there
	    if (!$httpProvider.defaults.headers.get) {
	        $httpProvider.defaults.headers.get = {};    
	    }

	    //disable IE ajax request caching
	    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
	    // extra
	    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
	    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
	});

})();