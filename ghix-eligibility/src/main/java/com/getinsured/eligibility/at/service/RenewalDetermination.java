package com.getinsured.eligibility.at.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.ref.common.AccountTransferCategoryEnum;
import com.getinsured.eligibility.at.ref.dto.ApplicationsWithSameIdDTO;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("renewalDetermination")
@Scope("singleton")
public class RenewalDetermination extends BaseDeterminationClass {
	private static final Logger LOGGER = Logger.getLogger(RenewalDetermination.class);

	@Override
	void processLogicToDetermine(AccountTransferRequestDTO accountTransferRequestDTO, List<SsapApplication> ssapApplications) {
		accountTransferRequestDTO.setRenewal(true);
		accountTransferRequestDTO.setAccountTransferCategory(AccountTransferCategoryEnum.RENEWAL.value());
		List<SsapApplication> applicationsToBeClosed = new ArrayList<SsapApplication>();
		List<SsapApplication> applicationsThatAreEnrolled = new ArrayList<SsapApplication>();
		List<SsapApplication> otherYearApplications = new ArrayList<SsapApplication>();
		List<SsapApplication> sameYearApplications = new ArrayList<SsapApplication>();
		List<ApplicationsWithSameIdDTO> applicationsWithSameId = new ArrayList<ApplicationsWithSameIdDTO>();
		Set<BigDecimal> enrolledAppCmrIds = new HashSet<>();
		Set<BigDecimal> allAppCmrIds = new HashSet<>();
		boolean hasEnrolledApplication = false;
		long enrolledApplicationId = 0l;
		
		
		for (SsapApplication ssapApplication : ssapApplications) {
			ApplicationsWithSameIdDTO fieldData = populateDataInDto(ssapApplication);
			if (ReferralUtil.convertToInt(ssapApplication.getCmrHouseoldId()) != ReferralConstants.NONE) {
				allAppCmrIds.add(ssapApplication.getCmrHouseoldId());
			}
			if (ssapApplication.getCoverageYear() == accountTransferRequestDTO.getCoverageYear()) {
				sameYearApplications.add(ssapApplication);
				if (ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) { // There should not be any enrolled application
					hasEnrolledApplication = true;
					applicationsThatAreEnrolled.add(ssapApplication);
					enrolledAppCmrIds.add(ssapApplication.getCmrHouseoldId());
				} else if (super.mesApplicationsAllowedToClose(ssapApplication.getApplicationStatus())// handle MES applications
						&& !ApplicationStatus.CLOSED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())
				        && !ApplicationStatus.CANCELLED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
					applicationsToBeClosed.add(ssapApplication);
				}
			} else {
				otherYearApplications.add(ssapApplication);
			}
			applicationsWithSameId.add(fieldData);
		}

		accountTransferRequestDTO.setApplicationsWithSameId(applicationsWithSameId);

		if (ReferralUtil.listSize(applicationsToBeClosed) != ReferralConstants.NONE) {
			closeApplications(applicationsToBeClosed);
		}

		if (hasEnrolledApplication) {
			enrolledApplicationId = retrieveComparedApplicationId(applicationsThatAreEnrolled, enrolledAppCmrIds);
			LOGGER.info("Comparison to be done with Enrolled Application  - " + enrolledApplicationId);
			accountTransferRequestDTO.setEnrolledApplicationId(enrolledApplicationId);
			accountTransferRequestDTO.setCompareToApplicationId(enrolledApplicationId);
		} else {
			if (ReferralUtil.collectionSize(allAppCmrIds) == ReferralConstants.ONE) {
				long coverageYear = accountTransferRequestDTO.getCoverageYear();
				List<SsapApplication> tmpYearApplications = new ArrayList<SsapApplication>();

				if (ReferralUtil.listSize(sameYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear();
					tmpYearApplications.addAll(sameYearApplications);
					accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(tmpYearApplications, coverageYear));
					if (ReferralUtil.listSize(otherYearApplications) != ReferralConstants.NONE) {
						coverageYear = accountTransferRequestDTO.getCoverageYear() - ReferralConstants.ONE;
						accountTransferRequestDTO.setRenewalCompareToApplicationId(retrieveComparedToApplication(otherYearApplications, coverageYear));
						if(accountTransferRequestDTO.getRenewalCompareToApplicationId() != ReferralConstants.NONE) {
							accountTransferRequestDTO.setCompareToApplicationId(accountTransferRequestDTO.getRenewalCompareToApplicationId());
						}
					}
				} else if (ReferralUtil.listSize(otherYearApplications) != ReferralConstants.NONE) {
					coverageYear = accountTransferRequestDTO.getCoverageYear() - ReferralConstants.ONE;
					tmpYearApplications.addAll(otherYearApplications);
					accountTransferRequestDTO.setCompareToApplicationId(retrieveComparedToApplication(tmpYearApplications, coverageYear));
					accountTransferRequestDTO.setRenewalCompareToApplicationId(accountTransferRequestDTO.getCompareToApplicationId());
				}
				LOGGER.info("Comparison to be done with Application  Compare - - " + accountTransferRequestDTO.getCompareToApplicationId() + ", Renewal - " + accountTransferRequestDTO.getRenewalCompareToApplicationId());
			}
		}

	}

	

}
