package com.getinsured.hix.broker.utils;

import java.io.InputStream;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.Query;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.jsoup.helper.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.service.jpa.ConsumerComposite;
import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.broker.DesignateBrokerDTO;
import com.getinsured.hix.dto.broker.EmployerIndividualBOBDTO;
import com.getinsured.hix.dto.broker.IndividualDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentBrokerUpdateDTO;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.entity.DesignateEntityDTO;
import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.indportal.PreferencesDTO;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountActivation.STATUS;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.GhixLanguage;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.platform.accountactivation.CreatedObject;
import com.getinsured.hix.platform.accountactivation.CreatorObject;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.config.AEEConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.dto.address.LocationDTO;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.notices.TemplateTokens;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.StateHelper;
import com.getinsured.hix.platform.util.StateHelper.State;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.household.service.PreferencesService;
import com.google.gson.Gson;

import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;

/**
 * Used to call ghix-ahbx using REST template, which in turn will call AHBX
 * web-services.
 */
@Component
public class BrokerMgmtUtils {

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerMgmtUtils.class);

	private static final String NEW_LINE_RESPONSE_IS = "\nResponse is ";

	private static final String ECMRELATIVEPATH = "individualnotifications";
	private static final String INDIVIDUAL_NOTIFICATION = "individualnotification_";
	private static final String PDF = ".pdf";
	private static final String INDIVIDUAL_MODULE = "individual";
	private static final String SEARCH_CRITERIA = "searchCriteria";
	private static final String EXCHANGE_FULL_NAME = "exchangeFullName";
	private static final String EXCHANGE_ADDRESS_LINE_ONE = "exchangeAddressLineOne";
	private static final String STATE_NAME = "stateName";
	private static final String COUNTRY_NAME = "countryName";
	private static final String EXCHANGE_URL = "exchangeURL";
	private static final String EXCHANGE_PHONE = "exchangePhone";
	//HIX-20774
	private static final String HEADER_CONTENT = "headerContent";
	private static final String EXCHANGE_NAME = "exchangeName";
	private static final String EXCHANGE_ADDRESS_ONE = "exchangeAddress1";
	private static final String EXCHANGE_CITY_NAME = "cityName";
	private static final String EXCHANGE_PIN_CODE = "pinCode";
	private static final String FOOTER_CONTENT = "footerContent";

	@Autowired private Gson platformGson;

	@Autowired private ApplicationContext appContext;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private NoticeService noticeService;

	@Autowired
	private AccountActivationService accountActivationService;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	 private BrokerService brokerService;
	
	@Autowired
	PreferencesService preferencesService;

	/**
	 * Sends {@link Broker} details to AHBX (IND35)
	 *
	 * @param broker
	 *            the broker object
	 */
	public void sendBrokerDetails(Broker broker, PaymentMethods paymentMethods) {
		// BrokerDTO brokerDto = populateBrokerDto(broker, paymentMethods);

		List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();
		EntityRequestDTO entityRequestDTO = populateBrokerDto(broker, paymentMethods);
		entityRequestDTOList.add(entityRequestDTO);
		/*
		 * Currently logging response received from AHBX.  Need to log
		 * response in db
		 */
		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Broker : ").append(broker.getId()).append(" : ");
		strResponse.append(broker.getUser().getFirstName()).append(" ");
		strResponse.append(broker.getUser().getLastName());

		try {
			LOGGER.info("IND35: Sending Broker Details to Ghix-AHBX");

			String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);

			String wsResponse = restTemplate.postForObject(GhixPlatformEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL,
					strEntityRequestDTOList, String.class);

			strResponse.append("\nResponse is: ").append(wsResponse);

			LOGGER.info("IND35: Successfully sent Broker Details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {


			LOGGER.error("IND35: Send Broker Details Web Service failed: Exception is :", exception);
		}
	}

	/**
	 * Populates {@link BrokerDTO} from {@link Broker} to send it to AHBX
	 * through REST API
	 *
	 * @param broker
	 * @return {@link BrokerDTO}
	 */
	private EntityRequestDTO populateBrokerDto(Broker broker, PaymentMethods paymentMethods) {
		EntityRequestDTO entityRequestDTO = new EntityRequestDTO();

		entityRequestDTO.setRecordId(broker.getId());
		entityRequestDTO.setRecordType("Agent");
		entityRequestDTO.setRecordIndicator("Update");

		entityRequestDTO.setFirstName(broker.getUser() != null ? broker.getUser().getFirstName() : "");
		entityRequestDTO.setLastName(broker.getUser() != null ? broker.getUser().getLastName() : "");

		entityRequestDTO.setBusinessLegalName(broker.getCompanyName());

		// Applying as quick fix to HIX-10371
		entityRequestDTO.setFunctionalIdentifier(new Long(broker.getBrokerNumber()));
		entityRequestDTO.setFederalEIN(broker.getFederalEIN());

		if (broker.getMailingLocation() != null) {
			entityRequestDTO.setAddressLine1(broker.getMailingLocation().getAddress1());
			entityRequestDTO.setAddressLine2(broker.getMailingLocation().getAddress2());
			entityRequestDTO.setCity(broker.getMailingLocation().getCity());
			entityRequestDTO.setState(broker.getMailingLocation().getState());
			entityRequestDTO.setZipCode(String.valueOf(broker.getMailingLocation().getZip()));
		} else {
			entityRequestDTO.setAddressLine1("");
			entityRequestDTO.setAddressLine2("");
			entityRequestDTO.setCity("");
			entityRequestDTO.setState("");
			entityRequestDTO.setZipCode("");
		}

		String contactNumber = "";
		if (broker.getContactNumber() != null && broker.getContactNumber().contains("-")) {
			contactNumber = broker.getContactNumber().replaceAll("-", "");
		} else {
			contactNumber = broker.getContactNumber();
		}
		entityRequestDTO.setPhoneNumber(contactNumber);
		entityRequestDTO.setEmail(broker.getUser() != null ? broker.getUser().getEmail() : "");

		entityRequestDTO.setAgentLicenseNum(broker.getLicenseNumber()); // !=
																		// null
																		// ?
																		// broker.getLicenseNumber().substring(0,
																		// 6) :
																		// "");
		populateCertificationInformation(broker, entityRequestDTO);

		populatePaymentMethod(paymentMethods, entityRequestDTO);

		return entityRequestDTO;
	}

	/**
	 * @param broker
	 * @param entityRequestDTO
	 */
	private void populateCertificationInformation(Broker broker,
			EntityRequestDTO entityRequestDTO) {
		entityRequestDTO.setCertiStatusCode(broker.getCertificationStatus());
		if (broker.getCertificationDate() != null) {
			entityRequestDTO.setCertiStartDate(formatDate(broker.getCertificationDate()));
		}
		if (broker.getStatusChangeDate() != null) {
			entityRequestDTO.setStatusDate(formatDate(broker.getStatusChangeDate()));
		}

		// If broker is certified then, setting certification end date as
		// certification date + 1 year, else set current date as certification
		// end date
		if (broker.getCertificationStatus() != null
				&& broker.getCertificationStatus().toString()
						.equalsIgnoreCase(certification_status.Certified.toString())) {
			Calendar date = TSCalendar.getInstance();
			date.setTime(broker.getCertificationDate());
			date.add(Calendar.YEAR, 1);
			Date newdate = date.getTime();
			entityRequestDTO.setCertiEndDate(formatDate(newdate));
			entityRequestDTO.setCertificationNumber(broker.getCertificationNumber());
		} else {
			entityRequestDTO.setCertiEndDate(formatDate(new TSDate()));
		}
	}

	/**
	 * Method to populate current EntityRequestDTO with payment method information.
	 *
	 * @param paymentMethods The current PaymentMethods instance.
	 * @param entityRequestDTO The current EntityRequestDTO instance.
	 */
	private void populatePaymentMethod(PaymentMethods paymentMethods,
			EntityRequestDTO entityRequestDTO) {
		// If payment type is EFT/ACH, setting flag to true. Else, false
		if (paymentMethods != null && "ACH".equals(paymentMethods.getPaymentType().name())) {
			LOGGER.debug("Payment Method is ACH. Setting Direct Deposit Flag to True");
			entityRequestDTO.setDirectDepositFlag("True");
		} else {
			LOGGER.debug("Payment Method is not ACH. Setting Direct Deposit Flag to False");
			entityRequestDTO.setDirectDepositFlag("False");
		}

		if (paymentMethods != null && paymentMethods.getFinancialInfo() != null
				&& paymentMethods.getFinancialInfo().getBankInfo() != null) {

			BankInfo bankInfo = paymentMethods.getFinancialInfo().getBankInfo();

			entityRequestDTO.setBankRoutingNumber(bankInfo.getRoutingNumber());
			entityRequestDTO.setAcctHolderName(bankInfo.getNameOnAccount());
			entityRequestDTO.setBankAcctNumber(bankInfo.getAccountNumber());

			// Checking if account type is 'C' or 'S' to set hard-coded values
			// in DTO
			String accountType = null;
			if (bankInfo.getAccountType() != null) {
				if ("C".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Checking";
				} else if ("S".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Savings";
				}
			}
			entityRequestDTO.setBankAcctType(accountType);
		}
	}

	/**
	 * Sends {@link DesignateBrokerDTO} broker designation details to AHBX
	 * (IND47)
	 *
	 * @param designateBroker
	 *            the designateBroker object
	 */
	public void sendDesignateBrokerDetails(DesignateBroker designateBroker) {
		DesignateEntityDTO designateEntityDTO = populateEntityBrokerDto(designateBroker);

		/*
		 * Currently logging response received from AHBX.  Need to log
		 * response in db when requirement comes
		 */

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Broker : ").append(designateBroker.getId()).append(" : ");

		try {
			LOGGER.info("IND47: Sending Broker Designation details to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.SEND_ENTITY_DESIGNATION_DETAIL,
					designateEntityDTO, String.class);

			strResponse.append(wsResponse);

			LOGGER.debug("IND47: Successfully sent Broker Designation details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {
			LOGGER.error("IND47: Send Broker Designation Web Service failed. Exception is :", exception);
		}
	}

	/**
	 * Populates {@link DesignateEntityDTO} from {@link DesignateEntityDTO} to
	 * send it to AHBX through REST API
	 *
	 * @param designateBroker
	 * @return {@link DesignateEntityDTO}
	 */
	private DesignateEntityDTO populateEntityBrokerDto(DesignateBroker designateBroker) {
		DesignateEntityDTO designEntityDto = new DesignateEntityDTO();

		designEntityDto.setBrokerAssisterId(designateBroker.getBrokerId());

		// Set them to default values
		designEntityDto.setEmployerId(0);
		designEntityDto.setId(0);
		designEntityDto.setStatus(designateBroker.getStatus().toString());

		// Adding record type as per latest AHBX WSDL update to differentiate if
		// request
		// is associated with agent or assister
		designEntityDto.setRecordType(BrokerConstants.AGENT);

		if (designateBroker.getExternalEmployerId() != null && !"".equals(designateBroker.getExternalEmployerId())) {
			designEntityDto.setEmployerId(designateBroker.getExternalEmployerId());
			designEntityDto.setRole(RoleService.EMPLOYER_ROLE);
		} else if (designateBroker.getExternalIndividualId() != null
				&& !"".equals(designateBroker.getExternalIndividualId())) {
			designEntityDto.setIndividualId(designateBroker.getExternalIndividualId());
			designEntityDto.setRole(RoleService.INDIVIDUAL_ROLE);
		}

		return designEntityDto;
	}

	/**
	 * Sends list of all individuals designated to a broker to AHBX (IND51)
	 *
	 * @param individualIdList
	 *            list of individual ids
	 */
	public void sendIndividualList(List<Long> individualIdList) {

		String wsResponse = null;

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Individuals: " + individualIdList);
		strResponse.append(NEW_LINE_RESPONSE_IS);

		// Populate the Individual DTO
		IndividualDTO individualDTO = new IndividualDTO();
		individualDTO.setIdlist(individualIdList);

		try {

			LOGGER.info("IND51:Sending Individual Status information to Ghix-AHBX");

			wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_INDIVIDUAL_BOB_STATUS, individualDTO,
					String.class);

			strResponse.append(wsResponse);

			LOGGER.debug("IND51:Successfully sent Individual Status details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {

			LOGGER.error("IND51:Send Individual Status information Web Service failed: Exception is :", exception);
		}
	}

	private String formatDate(Date dateInput) {
		return new SimpleDateFormat("MMddyyyy").format(dateInput);
	}

	/**
	 * Checks for null or blank String objects
	 *
	 * @param value
	 *            the string to be compared
	 * @return boolean true/false depending on whether the value is empty or not
	 */
	public static boolean isEmpty(String value) {

		return (value != null && !"".equals(value)) ? false : true;
	}

	/**
	 * Calls AHBX service to get External Employer BOB Details and saves in GI
	 * Database in AHBX layer This method just initiates the retrieving detail
	 * process and expect success/failure status
	 *
	 * @param id
	 *            external employer id
	 * @return employerResponse response object returned from AHBX
	 */
	public void retrieveAndSaveExternalEmployerBOBDetail(long id) {

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Employer Id: " + id);
		strResponse.append(NEW_LINE_RESPONSE_IS);

		try {
			EmployerIndividualBOBDTO dto = new EmployerIndividualBOBDTO();
			dto.setId(id);

			LOGGER.info("Sending External Employer BOB details to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_EMPLOYER_BOB_DETAIL, dto,
					String.class);

			strResponse.append(wsResponse);

			LOGGER.debug("Successfully sent External Employer BOB details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {

			LOGGER.error("Send External Employer BOB details Web Service failed. Exception is :", exception);
		}
	}

	/**
	 * Calls AHBX service to get External Employer BOB Status and update in GI
	 * Database in AHBX layer This method just initiates the retrieving status
	 * process and expect success/failure status
	 *
	 * @param listOfIDs
	 *            list of external employer id
	 * @return employerResponse response object returned from AHBX
	 */
	public void retrieveAndSaveExternalEmployersBOBStatus(List<Long> listOfIDs) {

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Employers: " + listOfIDs);
		strResponse.append(NEW_LINE_RESPONSE_IS);

		try {
			EmployerIndividualBOBDTO dto = new EmployerIndividualBOBDTO();
			dto.setListOfIDs(listOfIDs);

			LOGGER.info("Sending External Employers BOB Status information to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_EMPLOYER_BOB_STATUS, dto,
					String.class);

			strResponse.append(wsResponse);

			LOGGER.debug("Successfully sent External Employer BOB status information over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {

			LOGGER.error("Send External Employer BOB Status Web Service failed. Exception is :", exception);
		}
	}

	/**
	 * Calls AHBX service to get External Individual BOB Details and saves in GI
	 * Database in AHBX layer This method just initiates the retrieving detail
	 * process and expect success/failure status
	 *
	 * @param id
	 *            external individual id
	 * @return individualResponse response object returned from AHBX
	 */
	public String retrieveAndSaveExternalIndividualDetail(long id) {

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For External Individual Id: " + id);
		strResponse.append(NEW_LINE_RESPONSE_IS);

		try {
			IndividualDTO dto = new IndividualDTO();
			dto.setIndividualid(id);

			LOGGER.info("IND52:Sending External Individual BOB Details to Ghix-AHBX");

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.GET_EXTERNAL_INDIVIDUAL_BOB_DETAIL, dto,
					String.class);

			strResponse.append(wsResponse);

			LOGGER.debug("IND52:Successfully sent External Individual BOB Details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {

			LOGGER.error("IND52:Send External Individual BOB Detail Web Service failed. Exception is :", exception);
		}

		return strResponse.toString();
	}

	/**
	 * Method to fetch Languages matching the given query/lookup term from repository.
	 *
	 * @param query The query term.
	 * @param response The HttpServletResponse instance.
	 * @return String The languages matching the query term.
	 */
	public String getLanguageSpokenList(String query, HttpServletResponse response, List<String> languages) {
		LOGGER.info("getLanguageSpokenList : START");
		String tmpQuery;

		response.setContentType("application/json");

		String jsonData = null;
		String country = null;

		tmpQuery = query.toLowerCase();
		List<String> matched = new ArrayList<String>();
		for (int i = 0; i < languages.size(); i++) {
			country = languages.get(i).toLowerCase();
			if (country.startsWith(tmpQuery)) {
				matched.add(languages.get(i));
			}
		}

		LOGGER.info("getLanguageSpokenList : END");
		jsonData = platformGson.toJson(matched);

		return jsonData;
	}

	/**
	 * @param query
	 * @param columnValue
	 * @param isDateQuery
	 */
	public static void appendToQuery(StringBuilder query, String columnName, String columnValue, String columnReferenceKey, String operator, boolean isLikeQuery, boolean isDateQuery) {
		if (columnValue != null && !"".equals(columnValue)) {
			if(isLikeQuery) {
				query.append(" AND UPPER(").append(columnName).append(") ").append(operator).append(" UPPER(:").append(columnReferenceKey).append(")");
			}
			else {
				if(isDateQuery) {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" to_date(:").append(columnReferenceKey).append(",'mm-dd-yy')");
				}
				else {
					query.append(" AND ").append(columnName).append(" ").append(operator).append(" :").append(columnReferenceKey);
				}

			}
		}
	}

	/**
	 * @param searchQuery
	 * @param assisterFirstName
	 */
	public static void setQueryParameter(Query searchQuery, String parameter, String value, boolean isWildSearch) {
		if (value != null && !"".equals(value)) {
			if(isWildSearch) {
				searchQuery.setParameter(parameter, "%"+value.trim()+"%");
			}
			else {
				searchQuery.setParameter(parameter, value.trim());
			}
		}
	}

	/**
	 * Function to retrieveConsumerList from ghix-consumer-svc
	 * return map of:
	 * 1. List of ConsumerComposit
	 * 2. Total no. of Records
	 * @param map
	 * @return
	 */

	public Map<String, Object> retrieveConsumerList(Map<String, Object> map){
		List<ConsumerComposite> houseHoldlist = new LinkedList<>();
		Map<String, Object> houseHoldMap = new HashMap<>();
		String totalCount = null;

		/*
		 * Calling Consumer REST API to retrieve
		 * Consumer details
		 */
		LOGGER.info("retreiveConsumerList: REST call to get consumer list STARTS.");
		String jsonResponse = restTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_LIST, map, String.class);
		LOGGER.info("retreiveConsumerList: REST call to get consumer list ENDS.");

		if(jsonResponse != null){
			JSONObject jsonObj;
			try {
				jsonObj = (JSONObject)new JSONParser().parse(jsonResponse);
				JSONArray consumerArr = (JSONArray)jsonObj.get(BrokerConstants.CONSUMERS);
				if(consumerArr !=null && consumerArr.size()>0){

					for(Object consumerGenricObj : consumerArr) {
						JSONObject consumerJSONObj = (JSONObject) consumerGenricObj;
						Long consumerId = (Long)consumerJSONObj.get(BrokerConstants.CONSUMER_ID);
						Long cecId = (Long)consumerJSONObj.get(BrokerConstants.CEC_ID);
						String firstName = (String)consumerJSONObj.get(BrokerConstants.FIRSTNAME);
						String lastName = (String)consumerJSONObj.get(BrokerConstants.LASTNAME);
						String cecFirstName = (String)consumerJSONObj.get(BrokerConstants.CEC_FIRST_NAME);
						String cecLastName = (String)consumerJSONObj.get(BrokerConstants.CEC_LAST_NAME);
						String applicationStatus = (String)consumerJSONObj.get(BrokerConstants.APPLICATION_STATUS);
						String eligibilityStatus = (String)consumerJSONObj.get(BrokerConstants.ELIGIBILITY_STATUS);
						String requestSentStr = (String)consumerJSONObj.get(BrokerConstants.REQUEST_SENT);
						String inActiveSinceStr = (String)consumerJSONObj.get(BrokerConstants.INACTIVE_SINCE);
						String emailAddress = (String)consumerJSONObj.get(BrokerConstants.EMAIL_ADDRESS);
						String phoneNumber = (String)consumerJSONObj.get(BrokerConstants.PHONE_NUMBER);
						String address1 = (String)consumerJSONObj.get(BrokerConstants.ADDRESS1);
						String address2 = (String)consumerJSONObj.get(BrokerConstants.ADDRESS2);
						String state = (String)consumerJSONObj.get(BrokerConstants.STATE);
						String city = (String)consumerJSONObj.get(BrokerConstants.CITY);
						String zip = (String)consumerJSONObj.get(BrokerConstants.ZIP);
						Long familySizeStr = (Long)consumerJSONObj.get(BrokerConstants.FAMILY_SIZE);
						Double houseHoldIncomeStr = (Double)consumerJSONObj.get(BrokerConstants.HOUSEHOLD_INCOME);
						Long consumerUserId= (Long)consumerJSONObj.get(BrokerConstants.CONSUMER_USER_ID);

						SimpleDateFormat sdf = new SimpleDateFormat(BrokerConstants.DATE_FORMAT);

						Date requestSentDate = sdf.parse(requestSentStr);
						Date inActiveSinceDate = sdf.parse(inActiveSinceStr);

						ConsumerComposite consumer = new ConsumerComposite();
						consumer.setId(consumerId!=null?consumerId.longValue():-1);
						consumer.setCecId(cecId!=null?cecId.longValue():-1);
						consumer.setFirstName(firstName!=null?firstName:"");
						consumer.setLastName(lastName!=null?lastName:"");
						consumer.setCecFirstName(cecFirstName!=null?cecFirstName:"");
						consumer.setCecLastName(cecLastName!=null?cecLastName:"");
						consumer.setApplicationStatus(applicationStatus!=null?applicationStatus:"");

						if(applicationStatus!=null && applicationStatus.trim().length()>0){
							consumer.setApplicationStatus(ApplicationStatus.valueOf(applicationStatus).getDescription());
						}

						consumer.setEligibilityStatus(eligibilityStatus!=null?eligibilityStatus:"");
						consumer.setRequestSent(requestSentDate);
						consumer.setInActiveSince(inActiveSinceDate);
						consumer.setFamilySize(familySizeStr!=null?familySizeStr.longValue():0);
						consumer.setHouseHoldIncome(houseHoldIncomeStr!=null?houseHoldIncomeStr.doubleValue():0);
						consumer.setEmailAddress(emailAddress!=null?emailAddress:"");
						consumer.setPhoneNumber(phoneNumber!=null?phoneNumber:"");
						consumer.setAddress1(address1!=null?address1:"");
						consumer.setAddress2(address2!=null?address2:"");
						consumer.setCity(city!=null?city:"");
						consumer.setState(state!=null?state:"");
						consumer.setZip(zip!=null?zip:"");
						consumer.setConsumerUserId(consumerUserId!=null?consumerUserId.longValue():-1);

						houseHoldlist.add(consumer);
					}

					totalCount = jsonObj.get(BrokerConstants.TOTAL_RECORDS).toString();

				}


				houseHoldMap.put(BrokerConstants.HOUSEHOLDLIST, houseHoldlist);
				houseHoldMap.put(BrokerConstants.TOTAL_RECORD_COUNT, (totalCount!=null && totalCount.trim().length()>0)?totalCount:"0");
			}
			catch (java.text.ParseException de) {
				LOGGER.error("ERROR - Exception occured while parsing request or inActive Since dates. Error message is: "+de.getMessage());
			}
			catch (org.json.simple.parser.ParseException pe) {
				LOGGER.error("ERROR - Exception occured while parsing JSON Response. Error message is: "+pe.getMessage());
			}
		}
		LOGGER.debug("jsonResponse: "+jsonResponse);

		return houseHoldMap;
	}

	/**
	 * Method to check whether Payment flow is enabled for Agent.
	 *
	 * @return isPaymentEnabled The status of Payment flag.
	 */
	public static final boolean isPaymentEnabled() {
		boolean isPaymentEnabled = false;

		try {
			String paymentFlag = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENT_PAYMENT_IS_ENABLED);

			if(null != paymentFlag) {
				paymentFlag = paymentFlag.trim();

				if(paymentFlag.equalsIgnoreCase("Y")) {
					isPaymentEnabled = true;
				}
			}
		}
		catch(Exception ex) {
			LOGGER.error("Exception occurred while checking for Payment flag status", ex);
			isPaymentEnabled = false;
		}

		return isPaymentEnabled;
	}
	
	public static final boolean isAgencyPaymentEnabled(){
		boolean isAgencyPaymentEnabled = false;
		try {
			String paymentFlag = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PAYMENT_IS_ENABLED);

			if(null != paymentFlag) {
				paymentFlag = paymentFlag.trim();

				if(paymentFlag.equalsIgnoreCase("Y")) {
					isAgencyPaymentEnabled = true;
				}
			}
		}catch(Exception ex) {
			LOGGER.error("Exception occurred while checking for Payment flag status", ex);
			isAgencyPaymentEnabled = false;
		}
		return isAgencyPaymentEnabled;
	}
	

	/**
	 * Method to fetch Exchange's State Code configuration value.
	 * @return stateName The state code.
	 */
	public final String getExchangeStateCode() {
		LOGGER.info("getExchangeStateCode : START");
		String stateName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		LOGGER.info("getExchangeStateCode : END");
		return stateName;
	}


	/**
	 * Method to populate State List with supported states for address.
	 *
	 * @return allowedStates Collection of allowed State.
	 */
	public final List<State> filterStates() {
		LOGGER.info("fileterStates : START");
		// commented as a part of issue HIX-46558 and HIX-45915
		//List<State> allowedStates = new LinkedList<>();
		List<State> states = new StateHelper().getAllStates();
		/*String stateCode = getExchangeStateCode();

		if(null == stateCode) {
			allowedStates = states;
		}
		else {
			stateCode = stateCode.trim();

			for(State currState: states) {
				if(stateCode.equalsIgnoreCase(currState.getCode())) {
					allowedStates.add(currState);
				}
			}
		}
*/
		LOGGER.info("fileterStates : END");

		return states;
	}

	//Return the list of Individuals for the given moduleId and modulename

	public void sendNotificationsToIndividuals(Map<String, Object> consumerAPIRequestMap,
				String noticeTemplateName,Map<String, Object> templateData)
		{
			Map<String, Object> inividualAPIRequestMap = new HashMap<String, Object>();
			Broker brokerObj = null;
			Assister assister = null;
			List<ConsumerComposite> houseHoldObjList = new ArrayList<ConsumerComposite>();
			String senderModuleName = (String)consumerAPIRequestMap.get(BrokerConstants.MODULE_NAME);

			if(senderModuleName.equalsIgnoreCase(BrokerConstants.BROKER))
			{
				brokerObj = (Broker)consumerAPIRequestMap.get(BrokerConstants.SENDER_OBJ);
			}
			else if(senderModuleName.equalsIgnoreCase(BrokerConstants.ASSISTER))
			{
				assister = (Assister)consumerAPIRequestMap.get(BrokerConstants.SENDER_OBJ);
			}
			Integer startRecord = 0;
			consumerAPIRequestMap.put(BrokerConstants.START_PAGE, startRecord);
			inividualAPIRequestMap.put(SEARCH_CRITERIA, consumerAPIRequestMap);
			Map<String, Object> houseHoldMap = retrieveConsumerList(inividualAPIRequestMap);
			String fileName = null;
			try
			{
				if( houseHoldMap != null && houseHoldMap.size() > 0 ) {

					houseHoldObjList = (List<ConsumerComposite>) houseHoldMap.get(BrokerConstants.HOUSEHOLDLIST);
					List<String> individualEmailAdList = new ArrayList<String>();
					for(ConsumerComposite houseHoldObj:houseHoldObjList)
					{
						PreferencesDTO  preferencesDTO  =  preferencesService.getPreferences((int)houseHoldObj.getId(), false);
						individualEmailAdList.add(preferencesDTO.getEmailAddress());
						fileName = INDIVIDUAL_NOTIFICATION + TimeShifterUtil.currentTimeMillis() + PDF;

							if(senderModuleName.equalsIgnoreCase(BrokerConstants.BROKER))
							{
								noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, templateData, ECMRELATIVEPATH, fileName, INDIVIDUAL_MODULE,
										houseHoldObj.getId(), individualEmailAdList, brokerObj.getUser().getFullName(),
										(houseHoldObj.getFirstName() + " " + houseHoldObj.getLastName()),getLocationFromDTO(preferencesDTO.getLocationDto()),preferencesDTO.getPrefCommunication());


							}
							else if(null != templateData.get("IsEntity") && (boolean)templateData.get("IsEntity"))
							{
								noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, templateData, ECMRELATIVEPATH, fileName, INDIVIDUAL_MODULE,
										houseHoldObj.getId(), individualEmailAdList,  (assister.getEntity().getUser().getFirstName() + " " + assister.getEntity().getUser().getLastName()),(houseHoldObj.getFirstName() + " " + houseHoldObj.getLastName()),getLocationFromDTO(preferencesDTO.getLocationDto()),preferencesDTO.getPrefCommunication());
							}
							else if(senderModuleName.equalsIgnoreCase(BrokerConstants.ASSISTER))
							{
								noticeService.createModuleNotice(noticeTemplateName, GhixLanguage.US_EN, templateData, ECMRELATIVEPATH, fileName, INDIVIDUAL_MODULE,
										houseHoldObj.getId(), individualEmailAdList,  assister.getUser().getFullName(),(houseHoldObj.getFirstName() + " " + houseHoldObj.getLastName()),getLocationFromDTO(preferencesDTO.getLocationDto()),preferencesDTO.getPrefCommunication());
							}
					}
				}
			}
			catch (Exception e) {
				LOGGER.error("Exception occurred while sending Notifications To Individuals", e);
			}
		}

	public Location getLocationFromDTO(LocationDTO locationDto){
		if(locationDto == null){
			return null;
		}
		Location l = new Location();
		l.setAddress1(locationDto.getAddressLine1());
		l.setAddress2(locationDto.getAddressLine2());
		l.setCity(locationDto.getCity());
		l.setState(locationDto.getState());
		l.setZip(locationDto.getZipcode());
		l.setCounty(locationDto.getCountyName());
		l.setCountycode(locationDto.getCountyCode());
		return l;
	}
	
	public Map<String, Object> createTemplateMap(Broker broker)
	{
		String brokerFirstName = broker.getUser().getFirstName();
		String brokerLastName = broker.getUser().getLastName();
		Map<String, Object> brokerTemplateData = new HashMap<String, Object>();
		brokerTemplateData.put(EXCHANGE_FULL_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_FULL_NAME));
		brokerTemplateData.put(EXCHANGE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));

		brokerTemplateData.put(EXCHANGE_ADDRESS_ONE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		brokerTemplateData.put(EXCHANGE_CITY_NAME , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.CITY_NAME));
		brokerTemplateData.put(EXCHANGE_PIN_CODE , DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PIN_CODE));
		brokerTemplateData.put(TemplateTokens.HOST,GhixEndPoints.GHIXWEB_SERVICE_URL);

		brokerTemplateData.put(EXCHANGE_ADDRESS_LINE_ONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_ADDRESS_1));
		brokerTemplateData.put(STATE_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		brokerTemplateData.put(COUNTRY_NAME, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.COUNTRY_NAME));
		brokerTemplateData.put(EXCHANGE_URL, GhixEndPoints.GHIXWEB_SERVICE_URL);
		brokerTemplateData.put("EXCHANGE_URL", GhixEndPoints.GHIXWEB_SERVICE_URL);// need this token to match with template. Fix the template also to make tokens consistent
		brokerTemplateData.put(EXCHANGE_PHONE, DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		brokerTemplateData.put(BrokerConstants.BROKER_BUSINESS_NAME, brokerFirstName + " " + brokerLastName);
		brokerTemplateData.put(BrokerConstants.BROKER_COMPANY_NAME, broker.getCompanyName());
		StringBuilder reasonForDesignation = new StringBuilder();
		reasonForDesignation.append("Your Agent ");
		reasonForDesignation.append(brokerFirstName);
		reasonForDesignation.append(" ");
		reasonForDesignation.append(brokerLastName);
		reasonForDesignation.append(" does not wish to continue this relationship ");

		brokerTemplateData.put(BrokerConstants.AGENT_FIRST_NAME, brokerFirstName);
		brokerTemplateData.put(BrokerConstants.AGENT_LAST_NAME, brokerLastName);
		brokerTemplateData.put(BrokerConstants.REASON_FOR_DE_DESIGNATION, reasonForDesignation.toString());
		SimpleDateFormat sdf = new SimpleDateFormat("MMMMMMMMM dd, yyyy");
		brokerTemplateData.put(BrokerConstants.NOTIFICATION_DATE, sdf.format(new TSDate()));
		return brokerTemplateData;
	}

	private String getTemplateContentWithTokensReplaced(Map<String, Object> tokens, String location ){
		InputStream inputStreamUrl = null;
		String templateContent = "";
		try {
			Resource resource = appContext.getResource("classpath:"+location);
			inputStreamUrl = resource.getInputStream();
			templateContent = IOUtils.toString(inputStreamUrl, "UTF-8");
		} catch (Exception e) {
			LOGGER.error("Error reading content from  - " + location , e);
		}
		finally{
			IOUtils.closeQuietly(inputStreamUrl);
		}

		StringTemplateLoader stringLoader = new StringTemplateLoader();
		Configuration templateConfig = new Configuration();
		StringWriter sw = new StringWriter();
		Template tmpl = null;
		String responseData = null;
		try {
			stringLoader.putTemplate("welcomeEmail", templateContent);
			templateConfig.setTemplateLoader(stringLoader);
			tmpl = templateConfig.getTemplate("welcomeEmail");
			tmpl.process(tokens, sw);
			responseData = sw.toString();
		} catch (Exception e) {
			LOGGER.error("Exception ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		finally {
			IOUtils.closeQuietly(sw);
		}

		return responseData;
	}

	public String sendAccountActivationMail(Map<String, Object> parameters)throws GIException  {
		Household household = null;
		Assister assister = null;
		Broker broker = null;
		boolean isAgent = false;
		final String EXPIRATION_DAYS = "expirationDays";
		String activationLinkResult = "SUCCESS";

		if(null != parameters) {
			household =(Household) parameters.get("household");
			isAgent =  (boolean)parameters.get("isAgent");

			if(isAgent) {
				broker =  (Broker)parameters.get("broker");
			}
			else {
				assister = (Assister) parameters.get("assister");
			}
		}

		Validate.notNull(household);

		Map<String,String> assisterActivationDetails = new HashMap<String, String>();
		assisterActivationDetails.put("individualName", household.getFirstName());
		assisterActivationDetails.put(EXCHANGE_NAME,DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME));
		assisterActivationDetails.put("exchangePhone",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE));
		assisterActivationDetails.put("exchangeURL",DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_URL));
		assisterActivationDetails.put("stateName", DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_NAME));
		assisterActivationDetails.put("emailType","assisterActivationEmail");

		if(isAgent) {
			assisterActivationDetails.put(EXPIRATION_DAYS, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER));
		}
		else {
			assisterActivationDetails.put(EXPIRATION_DAYS, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ASSISTER));
		}


		CreatedObject createdObject = new CreatedObject();
		int createdObjectID = household.getId();
		String createdObjectType = RoleService.INDIVIDUAL_ROLE;
		List<String> phoneList = new ArrayList<String>();
		phoneList.add(household.getPhoneNumber());

		createdObject.setObjectId(createdObjectID);
		createdObject.setEmailId(household.getEmail());
		createdObject.setRoleName(createdObjectType);
		createdObject.setPhoneNumbers(phoneList);
		createdObject.setFullName(household.getFirstName() + " " + household.getLastName());
		createdObject.setFirstName(household.getFirstName());
		createdObject.setLastName(household.getLastName());
		createdObject.setCustomeFields(assisterActivationDetails);

		CreatorObject creatorObject = new CreatorObject();
		int creatorObjectID = -1;
		String creatorObjectType = null;

		if(isAgent) {
			creatorObjectID = broker.getId();
			creatorObjectType = GhixRole.BROKER.getRoleName();
			creatorObject.setObjectId(creatorObjectID);
			creatorObject.setFullName(broker.getUser().getFirstName() + " " + broker.getUser().getLastName());
		}
		else {
			creatorObjectID = assister.getId();
			creatorObjectType = GhixRole.ASSISTER.getRoleName();
			creatorObject.setObjectId(creatorObjectID);
			creatorObject.setFullName(assister.getFirstName() + " " + assister.getLastName());
		}
		creatorObject.setRoleName(creatorObjectType);

		// 1. Get the Activation Object w.r.t createdObjectID , creatorObjectID,creatorObjectType,createdObjectType and status=notprocessed.
		AccountActivation accountActivation =  accountActivationService.getAccountActivationByCreatedObjectIdAndCreatorObjectId(createdObjectID,createdObjectType,
				creatorObjectID, creatorObjectType, STATUS.NOTPROCESSED );

		// 2. If the activation object is not null, then call resendMail method and update the existing activation object.
		int expiryDays = 0;
		if(isAgent) {
			expiryDays = Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.USER));
		}
		else {
			expiryDays = Integer.valueOf(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.ASSISTER));
		}

		if(accountActivation != null) {
					accountActivation =  accountActivationService.resendActivationMail(createdObject, creatorObject, expiryDays, accountActivation);
		}
		else //3. If the activation object is null, then call initiateActivationForCreatedRecord method to create a new object
		{

			accountActivation =  accountActivationService.initiateActivationForCreatedRecord(createdObject, creatorObject, expiryDays);
		}
		if (accountActivation != null) {
			LOGGER.info("Account activation link sent to the employer...");
			activationLinkResult = "SUCCESS";
		}
		else
		{
			activationLinkResult = "FAILURE";

		}
		return activationLinkResult;
	}


public EnrollmentResponse updateEnrollmentDetails(Broker brokerObj,String marketType,String userName,Long householdId,String roleType,String actionFlag) {

		LOGGER.info("Update Enrollment Start : Broker :  "+brokerObj+" : Market Type : "+marketType+" : User Name : "+userName+" : Individual ID : "+householdId+" : Role Type : "+roleType+" : Action : "+actionFlag);
	
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		EnrollmentBrokerUpdateDTO enrollmentBrokerUpdateDTO =  new EnrollmentBrokerUpdateDTO();

		if(checkStateCode(BrokerConstants.CA_STATE_CODE)){
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(null);
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(String.valueOf(householdId));
		}else{
			enrollmentBrokerUpdateDTO.setHouseHoldCaseId(String.valueOf(householdId));
			enrollmentBrokerUpdateDTO.setExchgIndividualIdentifier(null);
		}
		
		enrollmentBrokerUpdateDTO.setRoleType(roleType);
		enrollmentBrokerUpdateDTO.setAgentBrokerName(brokerObj.getUser().getFirstName() + " " + brokerObj.getUser().getLastName());
		enrollmentBrokerUpdateDTO.setStateEINNumber(brokerObj.getFederalEIN());
		enrollmentBrokerUpdateDTO.setStateLicenseNumber(brokerObj.getLicenseNumber());
		// ID of agent/broker or assister
		enrollmentBrokerUpdateDTO.setAssisterBrokerId(brokerObj.getId());
		enrollmentBrokerUpdateDTO.setAddremoveAction(actionFlag);
		enrollmentBrokerUpdateDTO.setMarketType(marketType);
		enrollmentBrokerUpdateDTO.setBrokerTPAFlag(null);
		if (brokerObj.getNpn() != null) {
		enrollmentBrokerUpdateDTO.setAgentNPN(brokerObj.getNpn());
		}
		enrollmentRequest.setEnrollmentBrokerUpdateData(enrollmentBrokerUpdateDTO);

		String response = null;
		EnrollmentResponse enrollmentResponse = null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(EnrollmentRequest.class);
		try {
			LOGGER.info("Enrollment call Start. enrollmentBrokerUpdateDTO->  "+enrollmentBrokerUpdateDTO);
			LOGGER.info("GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS:"+GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS);
			response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.UPDATE_ENROLLMENT_BROKER_DETAILS,
					userName, HttpMethod.POST, MediaType.APPLICATION_JSON,
					String.class, writer.writeValueAsString(enrollmentRequest)).getBody();

			LOGGER.info("Update Enrollment API Response : " + SecurityUtil.sanitizeForLogging(response));
			
			if (null != response) {

				 enrollmentResponse = platformGson.fromJson(response, EnrollmentResponse.class);
				 LOGGER.info("enrollmentResponse->"+enrollmentResponse);
				 if(enrollmentResponse != null	&& enrollmentResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_FAILURE)) {

						LOGGER.info("Failed to update enrollment:  Error Code : " + SecurityUtil.sanitizeForLogging(String.valueOf(enrollmentResponse.getErrCode())) + " Error Message : " + SecurityUtil.sanitizeForLogging(enrollmentResponse.getErrMsg()));
						//throw new GIRuntimeException(null, null, Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
					}
			}

		} catch(GIRuntimeException giexception) {
			LOGGER.error("Exception occured while updating enrollment details : ", giexception);
			throw giexception;
		}
		catch (Exception e) {
			LOGGER.error("Exception occured while updating enrollment details : ", e);
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return enrollmentResponse;
	}

	public static boolean isBrokerCertificationStatusNotCertified(Broker broker)
	{
		if(broker== null || broker.getCertificationStatus() == null ||
			(null != broker && null != broker.getCertificationStatus() && !(broker.getCertificationStatus().equalsIgnoreCase(Broker.certification_status.Certified.toString())))) {

			return true;
		}
		return false;
	}
	
	public static boolean isBrokerStatusInActive(Broker broker)
	{
		if(broker.getStatus() != null && BrokerConstants.STATUS_INACTIVE.equals(broker.getStatus())) {
			return true;
		}
		return false;
	}

	public boolean isBrokerCertified(int id, boolean isUserSwitched) throws GIRuntimeException
	{
		Broker broker;

		try {
			if(isUserSwitched){
				broker = brokerService.getBrokerDetail(id);
			} else {
				broker = brokerService.findBrokerByUserId(id);
			}

			if(broker!= null && broker.getCertificationStatus() != null  && broker.getCertificationStatus().equalsIgnoreCase(Broker.certification_status.Certified.toString())) {
				return true;
			}
		} catch(Exception exception){
			LOGGER.error("Exception occured while check Broker status : ", exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}

		return false;
	}
	
	public static boolean checkStateCode(String code){
	 	 	String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	 	 	return stateCode.equalsIgnoreCase(code);
	 	 }
	
	public List<String> getAgentStatusList() {
		List<String> statusList = new ArrayList<String>();
		statusList.add("Incomplete");
		statusList.add("Pending");
		statusList.add("Withdrawn");
		statusList.add("Certified");
		statusList.add("Eligible");
		statusList.add("Denied");
		statusList.add("Terminated-Vested");
		statusList.add("Terminated-For-Cause");
		statusList.add("Deceased");
		if(checkStateCode(BrokerConstants.CA_STATE_CODE)){
			statusList.add("Suspended");
		}
		
		return statusList;
	}

}
