package com.getinsured.consumer.service;

import java.util.Date;
import com.getinsured.timeshift.util.TSDate;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.repository.IConsumerDocumentRepository;
import com.getinsured.consumer.repository.ISsapApplicantRepository;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.ssap.enums.SsapDocumentCategory;
import com.getinsured.iex.ssap.model.SsapApplicant;


@Service("consumerDocumentService")
public class ConsumerDocumentServiceImpl implements ConsumerDocumentService {

	private static final String RIDP_VERIFIED = "Y";
	private static final String DOCUMENT_APPROVED = RIDP_VERIFIED;
	private static final String HOUSEHOLD = "HOUSEHOLD";
	private static final String COMMENTS = "comments";
	private static final String ACCEPTED = "accepted";
	private static final String CMR_DOCUMENT_ID = "cmrDocumentId";
	private static final Logger LOGGER = Logger.getLogger(ConsumerDocumentServiceImpl.class);
	private static final String SUCCESS_RESPONSE = "{\"responseCode\": \"HS000000\"}";
	private static final String DOCUMENT_CATAGORY_VERIFIED_STATUS = "VERIFIED";
	private static final String SUCCESS_RESPONSE_CODE = "HS000000";

	@Autowired
	private ConsumerService consumerService;
	
    @Autowired
    private RestTemplate restTemplate;
	
	@Autowired
	private IConsumerDocumentRepository consumerDocumentRepository;
	@Autowired ISsapApplicantRepository iSsapApplicantRepository;
	
	@Override
	public String updateDocument(String documentDataJson) throws ConsumerServiceException {
		// Parse the Json
		JSONParser parser = new JSONParser();
		JSONObject requestJson = null;
		String errorMessage = null;
		try {
			requestJson = (JSONObject) parser.parse(documentDataJson);
		} catch (ParseException parseException) {
			errorMessage = "Error parsing input json";
			LOGGER.error(errorMessage, parseException);
			throw new ConsumerServiceException(errorMessage);			
		}
		
		// Extract the required fields from Json
		Long consumerDocumentId = Long.valueOf(requestJson.get(CMR_DOCUMENT_ID).toString());
		ConsumerDocument consumerDocument = consumerDocumentRepository.findOne(consumerDocumentId);
		consumerDocument.setAccepted(requestJson.get(ACCEPTED).toString());
		consumerDocument.setComments(requestJson.get(COMMENTS).toString());

		// Save the document to the database
		consumerDocumentRepository.save(consumerDocument);
		
		// Set RIDP_VERIFIED to "Y" if the document was approved
		if(null != consumerDocument.getTargetName() && consumerDocument.getTargetName().equals(HOUSEHOLD) && 
				null != consumerDocument.getAccepted() && consumerDocument.getAccepted().equals(DOCUMENT_APPROVED) ) {
			Household household = consumerService.findHouseholdById(consumerDocument.getTargetId().intValue());
			household.setRidpVerified(RIDP_VERIFIED);
			household.setRidpDate(new TSDate());
			consumerService.saveHousehold(household);
			if(null != household.getUser()) { 
			    String response = restTemplate.postForObject(GhixEndPoints.WebServiceEndPoints.GHIX_WEB_RIDP_WSO2_URL, household.getUser().getId(), String.class);
                if(StringUtils.contains(SUCCESS_RESPONSE_CODE, response)) {
                    LOGGER.info("RIDP Flag successfully pushed to SSO(wso)");
                } else {
                    LOGGER.error("Unable to push RIDP Flag to SSO(wso)");
                }
			}
		} else {
			StringBuilder debugMessage = new StringBuilder();
			debugMessage.append("Not marking RIDP status as verified as received document with accepted flag: "); 
			debugMessage.append(consumerDocument.getAccepted()); 
			debugMessage.append(" for ");
			debugMessage.append(consumerDocument.getTargetName());
			debugMessage.append(" with id: "); 
			debugMessage.append(consumerDocument.getTargetId());
			LOGGER.debug(debugMessage);
		}
		boolean isSsapApplicantVerificationStatusRequired = false;
		SsapApplicant ssapApplicant = null;
		if("Y".equalsIgnoreCase(consumerDocument.getAccepted())){
			String documentCategory = consumerDocument.getDocumentCategory().replaceAll("\\s+", "");
			if(SsapDocumentCategory.AMI_ALA_NATIVE_STATUS.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setNativeAmericanVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.CITIZENSHIP.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setCitizenshipImmigrationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.DEATH.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setDeathStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.INCARCERATION_STATUS.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setIncarcerationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.INCOME.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setIncomeVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.LEGAL_PRESENCE.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setVlpVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.MEC.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setMecVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.RESIDENCY.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setResidencyStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    }else if(SsapDocumentCategory.SSN.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
		    	ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setSsnVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
		    } else if(SsapDocumentCategory.NON_ESI_MEC.getTkmCategory().replaceAll("\\s+", "").equalsIgnoreCase(documentCategory)){
				ssapApplicant = getSsapApplicantReference(consumerDocument.getTargetId());
				ssapApplicant.setNonEsiMecVerificationStatus(DOCUMENT_CATAGORY_VERIFIED_STATUS);
				isSsapApplicantVerificationStatusRequired = true;
			}
			
			if(isSsapApplicantVerificationStatusRequired && ssapApplicant!=null){
				iSsapApplicantRepository.saveAndFlush(ssapApplicant);
			}
		}
		
		return SUCCESS_RESPONSE;
	}
	
	
	private SsapApplicant getSsapApplicantReference(long targetID){
		return iSsapApplicantRepository.findOne(targetID);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public String handleException(Throwable exception) {
		if(null == exception) {
			return "Unknown";
		}
		JSONObject exceptionJSONObj = new JSONObject();
		exceptionJSONObj.put("exception", exception.getClass().getName());
		exceptionJSONObj.put("message", exception.getMessage());
		exceptionJSONObj.put("cause", handleException(exception.getCause()));
		return exceptionJSONObj.toJSONString();
	}
}
