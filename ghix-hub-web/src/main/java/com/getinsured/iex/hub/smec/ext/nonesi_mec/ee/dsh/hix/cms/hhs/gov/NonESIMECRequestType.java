//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.05.06 at 09:48:17 AM IST 
//


package com.getinsured.iex.hub.smec.ext.nonesi_mec.ee.dsh.hix.cms.hhs.gov;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for NonESIMECRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="NonESIMECRequestType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NonESIMECindividualInformation" type="{http://gov.hhs.cms.hix.dsh.ee.nonesi_mec.ext}NonESIMECIndividualType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "NonESIMECRequestType", propOrder = {
    "nonESIMECindividualInformation"
})
public class NonESIMECRequestType {

    @XmlElement(name = "NonESIMECindividualInformation", required = true)
    protected NonESIMECIndividualType nonESIMECindividualInformation;

    /**
     * Gets the value of the nonESIMECindividualInformation property.
     * 
     * @return
     *     possible object is
     *     {@link NonESIMECIndividualType }
     *     
     */
    public NonESIMECIndividualType getNonESIMECindividualInformation() {
        return nonESIMECindividualInformation;
    }

    /**
     * Sets the value of the nonESIMECindividualInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link NonESIMECIndividualType }
     *     
     */
    public void setNonESIMECindividualInformation(NonESIMECIndividualType value) {
        this.nonESIMECindividualInformation = value;
    }

}
