package com.getinsured.eligibility.at.ref.dto;

import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;

/**
 * @author chopra_s
 * 
 */
public class LCEProcessRequestDTO {

	private long currentApplicationId;

	private long enrolledApplicationId;

	private ApplicationExtension applicationExtension;

	private EnrollmentAttributesDTO enrolledApplicationAttributes;
	
	private boolean isHouseholdLevelCsrChangeDetected;
	
	// this is added in order to make an entry with the appropriate user in application_event
	private int lastUpdatedUserId;

	public LCEProcessRequestDTO() {
	}

	public LCEProcessRequestDTO(long currentApplicationId, long enrolledApplicationId, ApplicationExtension applicationExtension) {
		this.currentApplicationId = currentApplicationId;
		this.enrolledApplicationId = enrolledApplicationId;
		this.applicationExtension = applicationExtension;
	}

	public LCEProcessRequestDTO(long currentApplicationId, long enrolledApplicationId, ApplicationExtension applicationExtension, EnrollmentAttributesDTO enrolledApplicationAttributes) {
		this.currentApplicationId = currentApplicationId;
		this.enrolledApplicationId = enrolledApplicationId;
		this.applicationExtension = applicationExtension;
		this.enrolledApplicationAttributes = enrolledApplicationAttributes;
	}

	public EnrollmentAttributesDTO getEnrolledApplicationAttributes() {
		return enrolledApplicationAttributes;
	}

	public void setEnrolledApplicationAttributes(EnrollmentAttributesDTO enrolledApplicationAttributes) {
		this.enrolledApplicationAttributes = enrolledApplicationAttributes;
	}

	public long getCurrentApplicationId() {
		return currentApplicationId;
	}

	public void setCurrentApplicationId(long currentApplicationId) {
		this.currentApplicationId = currentApplicationId;
	}

	public long getEnrolledApplicationId() {
		return enrolledApplicationId;
	}

	public void setEnrolledApplicationId(long enrolledApplicationId) {
		this.enrolledApplicationId = enrolledApplicationId;
	}

	public ApplicationExtension getApplicationExtension() {
		return applicationExtension;
	}

	public void setApplicationExtension(ApplicationExtension applicationExtension) {
		this.applicationExtension = applicationExtension;
	}

	public boolean isHouseholdLevelCsrChangeDetected() {
		return isHouseholdLevelCsrChangeDetected;
	}

	public void setHouseholdLevelCsrChangeDetected(boolean isHouseholdLevelCsrChangeDetected) {
		this.isHouseholdLevelCsrChangeDetected = isHouseholdLevelCsrChangeDetected;
	}

	public int getLastUpdatedUserId() {
		return lastUpdatedUserId;
	}

	public void setLastUpdatedUserId(int lastUpdatedUserId) {
		this.lastUpdatedUserId = lastUpdatedUserId;
	}
	
	

}
