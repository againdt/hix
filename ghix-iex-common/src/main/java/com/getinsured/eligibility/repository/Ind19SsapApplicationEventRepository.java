package com.getinsured.eligibility.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplicationEvent;

/**
 * Story HIX-53174 Invoke IND 19 with correct mapping and MRC codes for the SEP event
 * 
 * Sub-task HIX-55913
 * Populate enrollment and disenrollment information in IND19
 * 
 * @author Nikhil Talreja
 * 
 */
@Repository
public interface Ind19SsapApplicationEventRepository  extends JpaRepository<SsapApplicationEvent, Long> {
	
	@Query("Select applicationEvent " +
			" FROM SsapApplicationEvent as applicationEvent "+
			" left outer join fetch applicationEvent.ssapApplicantEvents as applicantEvent "+
			" left outer join fetch applicationEvent.ssapApplication as application "+
			" where application.id = :applicationId ")
	SsapApplicationEvent findBySsapApplicationId(@Param("applicationId") long applicationId);
	
}
