//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.07.16 at 11:43:39 PM PDT 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SelfEmploymentExpenses complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SelfEmploymentExpenses">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="expenseType" type="{http://www.cohbe.org/SSA/Person}ExpenseType-Enum" minOccurs="0"/>
 *         &lt;element name="expenseFrequency" type="{http://www.cohbe.org/SSA/Person}HCPFIncomeFrequency-Enum" minOccurs="0"/>
 *         &lt;element name="expenseAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SelfEmploymentExpenses", propOrder = {
    "expenseType",
    "expenseFrequency",
    "expenseAmount"
})
public class SelfEmploymentExpenses {

    protected ExpenseTypeEnum expenseType;
    protected HCPFIncomeFrequencyEnum expenseFrequency;
    protected Double expenseAmount;

    /**
     * Gets the value of the expenseType property.
     * 
     * @return
     *     possible object is
     *     {@link ExpenseTypeEnum }
     *     
     */
    public ExpenseTypeEnum getExpenseType() {
        return expenseType;
    }

    /**
     * Sets the value of the expenseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExpenseTypeEnum }
     *     
     */
    public void setExpenseType(ExpenseTypeEnum value) {
        this.expenseType = value;
    }

    /**
     * Gets the value of the expenseFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link HCPFIncomeFrequencyEnum }
     *     
     */
    public HCPFIncomeFrequencyEnum getExpenseFrequency() {
        return expenseFrequency;
    }

    /**
     * Sets the value of the expenseFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link HCPFIncomeFrequencyEnum }
     *     
     */
    public void setExpenseFrequency(HCPFIncomeFrequencyEnum value) {
        this.expenseFrequency = value;
    }

    /**
     * Gets the value of the expenseAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getExpenseAmount() {
        return expenseAmount;
    }

    /**
     * Sets the value of the expenseAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setExpenseAmount(Double value) {
        this.expenseAmount = value;
    }

}
