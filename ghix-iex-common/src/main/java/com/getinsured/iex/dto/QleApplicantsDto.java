package com.getinsured.iex.dto;

import java.util.List;

public class QleApplicantsDto {

	String firstName;
	String lastName;
	String middleName;
	long personId;
	List<QleEventDto> events;
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getMiddleName() {
		return middleName;
	}
	public void setMiddleName(String middleName) {
		this.middleName = middleName;
	}
	public List<QleEventDto> getEvents() {
		return events;
	}
	public void setEvents(List<QleEventDto> events) {
		this.events = events;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	
	
	
}
