<%@page import="java.text.SimpleDateFormat"%>
<%@page import="org.joda.time.DateTime"%>
<%@page import="org.joda.time.LocalDate"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration.IEXConfigurationEnum"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<script src="<c:url value="/resources/angular/mask.js"/>"></script>
<script src="<c:url value="/resources/angular/ui-bootstrap-tpls-0.9.0.min.js"/>"></script>
<script src="<c:url value="/resources/js/spring-security-csrf-token-interceptor.js"/>"></script>
 

<div ng-app="benefitsApp" class="preEligibilityPage">
  <div class="gutter2">
    <div class="row-fluid margin10-t eligibility-wrapper">      
        <div ng-view class="span11" id="rightpanel" aria-live="rude" role="application">
        </div>    
    </div>
  </div>
  <%
      String coverageYearOptionCutOffDate = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_PRESCREENER_SHOW_CURRENT_YEAR);
      String showCoverageYear = "Y";
	  try{
		    if(coverageYearOptionCutOffDate != null && coverageYearOptionCutOffDate.trim().length() != 0){
				SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
				LocalDate todaysDate = new LocalDate();
				LocalDate cutOffDate = new DateTime(formatter.parseObject(coverageYearOptionCutOffDate)).toLocalDate();
				if (todaysDate.isAfter(cutOffDate)) {
					showCoverageYear = "N";
					pageContext.setAttribute("gCoverageYear", 2016);
				}
		    }
		}
		catch(Exception ex){
			showCoverageYear = "Y";
		}
	    pageContext.setAttribute("leadPresent", request.getSession().getAttribute("leadId"));
  		pageContext.setAttribute("showCoverageYear", showCoverageYear);
  %>
  <!-- HOUSEHOLD INFO PAGE -->
<script type="text/ng-template" id="pp1">  

<form novalidate name="prescreen" class="form-horizontal">
<div class="question-group" ng-init="initPrescreen(true,'${leadPresent}')" aria-live="assertive">
<!-- Coverage year selection -->
<c:if test="${showCoverageYear == 'Y'}">
<div class="header">
    <h4><spring:message code="pd.label.quoteform.qCoverageYear" javaScriptEscape="true"/></h4>
</div>
<div class="control-group gutter2">
    <label class="control-label" for="coverageYear"><spring:message code="pd.label.quoteform.selectCoverageYear" javaScriptEscape="true"/>: <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
    <div class="controls">      
		<select id="coverageYear" ng-model="formInfo.coverageYear" ng-change="coverageYearChange()">
			<option value="2015"><spring:message code="pd.label.quoteform.coverageDate.2015"/></option>
			<option value="2016" selected><spring:message code="pd.label.quoteform.coverageDate.2016" /></option>
     	</select>
	</div>
</div>
</c:if>
<c:if test="${showCoverageYear != 'Y'}">
<input id="gCoverageYear" name="gCoverageYear" type="hidden" value="${gCoverageYear}">
</c:if>

  <div class="header">
    <h4 id="zipAnchor"><spring:message code="pd.label.quoteform.qAddress" javaScriptEscape="true"/></h4>
  </div>
  <div id="zipEntry" class="control-group gutter2">
    <label class="control-label" for="zipCode"><spring:message code="pd.label.quoteform.promZip" javaScriptEscape="true"/>: <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"/></label>
    <div class="controls">
      <input id="zipCode" class="input-small" name="zipCode" type="text" ng-model='formInfo.zipCode' ng-blur='submitZip()' data-container="body" data-toggle="popover" data-placement="bottom" maxlength="5" required>
   
    <span ng-show="formInfo.showOneCounty" aria-live="assertive"> @@formInfo.countyName@@ <span class="aria-hidden"><spring:message code="pd.label.quoteform.county" javaScriptEscape="true"/></span></span>
    <span class="popover-content ng-binding zip-warning redBorder" ng-if="countyNotSelected" aria-live="rude"><spring:message code="pd.label.quoteform.errCountyNotSelected" javaScriptEscape="true"/></span>   
  	<span class="popover-content ng-binding zip-warning redBorder" ng-if="zipWarning" aria-live="rude"><spring:message code="pd.label.quoteform.errInvalidZip" javaScriptEscape="true"/></span></a>
</div>
</div>
<div class="control-group" ng-show='formInfo.showMultipleCounties'>
      <label class="control-label" for="countyname"><spring:message code="pd.label.quoteform.multiCounty" javaScriptEscape="true"/><img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></label>
	<div class="controls">      
		<select id="countyname" ng-show="formInfo.showMultipleCounties" ng-options="item.codeCombined as item.nameCombined for item in formInfo.countyInfo" ng-model="formInfo.selectedCounty" ng-change="multiCountyChange()">
        <option value="" required><spring:message code="pd.label.quoteform.multiCountyOption" javaScriptEscape="true"/></option>
      </select>
</div> 
</div> 

<div class="question-group">
  <div class="header">
    <h4><a href="#" id="who"><spring:message code="pd.label.quoteform.qFamilyInfo" javaScriptEscape="true"/> <span class="aria-hidden"><spring:message code="pd.label.quoteform.familyEnterSequence" javaScriptEscape="true"/> <spring:message code="pd.label.quoteform.tableBelow" javaScriptEscape="true"/></span></a></h4>
  </div>
  <div class="alert alert-info">
    <spring:message code="pd.label.quoteform.familyEnterSequence" javaScriptEscape="true"/>. 
  </div>
    
  <div id="family-members-container" class="gutter2">
    <table class="table head center">
      <caption></caption>
        <tr>
          <th scope="col"><spring:message code="pd.label.quoteform.members" javaScriptEscape="true"/></th>
          <th scope="col"><spring:message code="pd.label.quoteform.birthdate" javaScriptEscape="true"/> <img alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></th>
		  <th></th>
        </tr>

        <tr id="members-repeat-container" ng-repeat="member in formInfo.family">
          <ng-form name='indiv'>
          <th scope="row" ng-if="member.type == 'SELF'" class="member-type-container"><span class="member-label"><spring:message code="pd.label.quoteform.YOU" javaScriptEscape="true"/></span></th>
          <th scope="row" ng-if="member.type != 'SELF'" class="member-type-container"><span class="member-label">@@ member.type @@</span></th>
          <td><label for="birthdate@@$index+1@@" class="aria-hidden"><spring:message code="pd.label.quoteform.pleaseEnter" javaScriptEscape="true"/> 
		  			<span ng-if="member.type == 'SELF'"><spring:message code="pd.label.quoteform.your" javaScriptEscape="true"/></span> <span ng-if="member.type == 'SPOUSE'"><spring:message code="pd.label.quoteform.spouses" javaScriptEscape="true"/></span> <span ng-if="member.type == 'CHILD'"><spring:message code="pd.label.quoteform.childs" javaScriptEscape="true"/></span> <spring:message code="pd.label.quoteform.birthdate" javaScriptEscape="true"/></label>
		  			<input name='age' class="member-birthday input-small" ng-class="member.preloaded"  type="text" name="birthdate@@$index + 1@@" id="birthdate@@$index+1@@" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true"
            	ng-model="member.birthday" date-check mtype="@@member.type@@" order="@@$index@@" popover="@@member.errorMsg@@" trigger-click="@@member.errorMsg@@" popover-trigger="blur" popover-placement="right" required test-check="@@$index@@"></td>
          <td class="removebtn"><a href="javascript:void(0);" title="<spring:message code='pd.label.title.viewDetail'/>" ng-show="member.removable" ng-click="remove($index, member.type)" class="btn btn-mini"><spring:message code="pd.label.quoteform.remove" javaScriptEscape="true"/></a></td>
        </ng-form>
        </tr> 

    </table>

<!-- This div has to be invisible --> 
<div style="display:none">
<div id="members-container" ng-repeat="member in formInfo.family" class="form-group">
          <ng-form name='indiv2'>
          test <label for="birthdate@@$index+1@@" class="aria-hidden"><spring:message code="pd.label.quoteform.pleaseEnter" javaScriptEscape="true"/> 
            <span ng-if="member.type == 'SELF'"><spring:message code="pd.label.quoteform.your" javaScriptEscape="true"/></span> <span ng-if="member.type == 'SPOUSE'"><spring:message code="pd.label.quoteform.spouses" javaScriptEscape="true"/></span> <span ng-if="member.type == 'CHILD'"><spring:message code="pd.label.quoteform.childs" javaScriptEscape="true"/></span> <spring:message code="pd.label.quoteform.birthdate" javaScriptEscape="true"/></label>
            <input name='age' class="member-birthday input-small" ng-class="member.preloaded"  type="text" name="birthdate@@$index + 1@@" id="birthdate@@$index+1@@" placeholder="mm/dd/yyyy" ui-mask="99/99/9999" ui-mask-use-viewvalue="true"
              ng-model="member.birthday" date-check mtype="@@member.type@@" order="@@$index@@" popover="@@member.errorMsg@@" trigger-click="@@member.errorMsg@@" popover-trigger="blur" popover-placement="right" required>
        </ng-form>
        </div>
 </div>

    <div class="form-actions">
      <div class="pull-right">
        <button class="btn margin5-r" type="button" id="addSpouse" ng-show="formInfo.spouse == 0" ng-click="addSpouse()"><span class="aria-hidden"><spring:message code="pd.label.quoteform.addA" javaScriptEscape="true"/></span> <spring:message code="pd.label.quoteform.spouse" javaScriptEscape="true"/> </button>
        <button class="btn addChild addChild2" type="button" id="addChild" ng-click="addChild()"><span class="aria-hidden"><spring:message code="pd.label.quoteform.addA" javaScriptEscape="true"/></span> <spring:message code="pd.label.quoteform.dependent" javaScriptEscape="true"/></button>
      </div>
    </div>
  </div><!-- #family-members-container -->
</div>







  <div class="form-actions">
    <button type="button" id="check-for-plans" class="btn btn-primary margin10-l pull-right" ng-disabled="(prescreen.indiv2 && !prescreen.indiv2.$valid) || !formInfo.zipValid || !checkDateInput()" ng-click="submitPrescreen('browse')">Submit</button>
  	
  </div>
</form>

<!-- Modal for failling submission of pre-eligibility-->
<div ng-show="openSubmitFail">
      <div modal-show="openSubmitFail" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="openSubmitFail" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="openSubmitFail"><spring:message code="pd.label.quoteform.technicalissue" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="pd.label.quoteform.submissionFailureMsg" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="pd.label.quoteform.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for failling submission of pre-eligibility-->

<!-- Modal for dependent limit-->
<div ng-show="openDependLimit">
      <div modal-show="openDependLimit" class="modal fade" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="openDependLimit" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h3 id="openDependLimit"><spring:message code="pd.label.quoteform.limitDependentsTitle" javaScriptEscape="true"/></h3>
          </div>
          <div class="modal-body">
          <p><spring:message code="pd.label.quoteform.limitDependentsContent" javaScriptEscape="true"/></p>
          </div>
          <div class="modal-footer">
            <button class="btn pull-left" data-dismiss="modal"><spring:message code="pd.label.quoteform.close" javaScriptEscape="true"/></button>
          </div>
        </div><!-- /.modal-content-->
      </div> <!-- /.modal-dialog-->
     </div>
</div>
<!-- Modal for dependent limit-->



</script>


   <script>
    $(document).ready(function() {
      $('body').tooltip({
        selector : '[rel=tooltip]',
        placement : 'right',
      });
   
    });
    
  </script> 
   <input id="tokid" name="tokid" type="hidden" value="${sessionScope.csrftoken}"/>  
   <script src="<c:url value="/resources/js/phix/quoteForm.js"/>?v=R2" type="text/javascript"></script>
  
</div>
