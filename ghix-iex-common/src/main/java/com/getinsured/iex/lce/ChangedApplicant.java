package com.getinsured.iex.lce;

public class ChangedApplicant {

	Long personId;
	String eventDate;
	Boolean isChangeInZipCodeOrCounty;
	
	public Long getPersonId() {
		return personId;
	}
	public void setPersonId(Long personId) {
		this.personId = personId;
	}
	public String getEventDate() {
		return eventDate;
	}
	public void setEventDate(String eventDate) {
		this.eventDate = eventDate;
	}
	public Boolean getIsChangeInZipCodeOrCounty() {
		return isChangeInZipCodeOrCounty;
	}
	public void setIsChangeInZipCodeOrCounty(Boolean isChangeInZipCodeOrCounty) {
		this.isChangeInZipCodeOrCounty = isChangeInZipCodeOrCounty;
	}
	
	
}
