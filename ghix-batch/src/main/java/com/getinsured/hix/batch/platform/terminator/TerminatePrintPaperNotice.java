package com.getinsured.hix.batch.platform.terminator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.platform.skip.PrintPaperNoticeSkip;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * 
 * Terminate Step Tasklet class for PrintPaperNoticeJob
 * @author Sharma_K
 * @Since 05th January 2016
 * @version 1.0
 */
public class TerminatePrintPaperNotice implements Tasklet{
	
	private PrintPaperNoticeSkip printPaperNoticeSkip;
	private String skipFilePath;
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminatePrintPaperNotice.class);
	
	
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception
	{
		ExecutionContext je = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		Object jobExecutionStatus = je.get("jobExecutionStatus");
		String jobStatus = null;
		
		if(jobExecutionStatus != null){
			jobStatus = (String)jobExecutionStatus;
		}
		Writer writer = null;
		File destination = null;
		File skipFileParentDir = null;
		try{
			if(printPaperNoticeSkip != null && printPaperNoticeSkip.getSkippedNoticeMap() != null &&
					!printPaperNoticeSkip.getSkippedNoticeMap().isEmpty() && StringUtils.isNotEmpty(skipFilePath))
			{
				skipFileParentDir = new File(skipFilePath);
				if(!skipFileParentDir.exists()){
					skipFileParentDir.mkdirs();
				}
				
				StringBuilder stringbuilder = new StringBuilder(64);
				stringbuilder.append(skipFilePath);
				stringbuilder.append(File.separator);
				stringbuilder.append(chunkContext.getStepContext().getStepExecution().getJobExecution().getJobId());
				stringbuilder.append("-");
				stringbuilder.append(DateUtil.dateToString(chunkContext.getStepContext().getStepExecution().getJobExecution().getCreateTime(), GhixConstants.FILENAME_DATE_FORMAT));
				String timeStampSkippedFolderPath = stringbuilder.toString();
				stringbuilder.append(File.separator);
				stringbuilder.append("SkippedNotices.txt");
				destination = new File(timeStampSkippedFolderPath);
				if(!destination.exists()){
					destination.mkdirs();
				}
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(stringbuilder.toString()), "utf-8"));
				writer.write(printPaperNoticeSkip.toString());
			}
		}catch(Exception e){
			LOGGER.error("Exception occurred while logging skipped records to the destination directory :: " + e.getMessage(), e);
			//Dumping all the records into log file
			dumpToLogger();
		}finally{
			if(printPaperNoticeSkip != null){
				printPaperNoticeSkip.clearSkippedNoticeMap();
			}
			if(writer != null){
				IOUtils.closeQuietly(writer);
			}
		}
		/**
		 * If jobStaus is not Failed then throw Exception
		 */
		if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
			String errMsg = "";
			if(null != je.get("errorMessage")){
				errMsg = je.get("errorMessage").toString(); 
			}
			throw new GIException(errMsg);
		}
		return RepeatStatus.FINISHED;
	}

	private void dumpToLogger() {
		LOGGER.error("Error while dumping the skipped record, dumping it in the log");
		LOGGER.error(printPaperNoticeSkip.toString());
	}

	/**
	 * @return the printPaperNoticeSkip
	 */
	public PrintPaperNoticeSkip getPrintPaperNoticeSkip() {
		return printPaperNoticeSkip;
	}

	/**
	 * @param printPaperNoticeSkip the printPaperNoticeSkip to set
	 */
	public void setPrintPaperNoticeSkip(PrintPaperNoticeSkip printPaperNoticeSkip) {
		this.printPaperNoticeSkip = printPaperNoticeSkip;
	}

	/**
	 * @return the skipFilePath
	 */
	public String getSkipFilePath() {
		return skipFilePath;
	}

	/**
	 * @param skipFilePath the skipFilePath to set
	 */
	public void setSkipFilePath(String skipFilePath) {
		this.skipFilePath = skipFilePath;
	}
}
