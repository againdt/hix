package com.getinsured.hix.account.service.jpa;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Employee;
import com.getinsured.hix.model.EmployeeDetails;
import com.getinsured.hix.platform.security.service.UserService;


public class UserServiceImplTest extends BaseTest {

	@Autowired private UserService userService;
	
	@Test
	public void testGetUserPermissions() {
		
		System.out.println("\r\n****************************************\r\n");
		String userName="get6user001";
		List<String> currPermissions = new ArrayList<String>();
		
		AccountUser accountUser=userService.findByUserName(userName);
		currPermissions = userService.getUserPermissions(accountUser);
		
		System.out.println("AccountUser :: Details ::\r\n"+accountUser);
		System.out.println("No Of Permissions :: Details ::\r\n"+currPermissions.size());
		
		System.out.println("\r\n****************************************\r\n");
	}
	
}