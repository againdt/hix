package com.getinsured.hix.batch.ssap.renewal.dto;

public enum GenerateNoticeEnum {
	SUCCESS, FAILURE, BOTH, ALL;
}
