package com.getinsured.iex.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;



@JsonIgnoreProperties(ignoreUnknown = true)
public class SsapApplicantEvent implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long id;
	
	private SsapApplicationEvent ssapAplicationEvent;

	private SsapApplicant ssapApplicant;

	public SsapApplicantEvent() {
	}

	public SsapApplicationEvent getSsapAplicationEvent() {
		return ssapAplicationEvent;
	}

	public void setSsapAplicationEvent(SsapApplicationEvent ssapAplicationEvent) {
		this.ssapAplicationEvent = ssapAplicationEvent;
	}

	public SsapApplicant getSsapApplicant() {
		return ssapApplicant;
	}

	public void setSsapApplicant(SsapApplicant ssapApplicant) {
		this.ssapApplicant = ssapApplicant;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


}