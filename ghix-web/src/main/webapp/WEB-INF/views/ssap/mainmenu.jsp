
	<div class="navbar" id="menu">
		<div class="navbar-inner">
				<ul class="nav navPhixbar">
					
					<!-- build menus...  -->
					
						
						
							
								
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Issuers" class="dropdown-toggle" data-toggle="dropdown">Issuers <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manager Issuer Accounts">Manage Issuer Accounts</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="New Issuer Account">New Issuer Account</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
								
									
								
							
								
							
						
						<li class="dropdown">
							
								
									<a href="#" title="Plans" class="dropdown-toggle" data-toggle="dropdown">Plans <i class="caret"></i></a>
								
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage QHPs">Manage QHPs</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage SADPs">Manage SADPs</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Rating Area Mapping">Rating Area Mapping</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Agents" class="dropdown-toggle" data-toggle="dropdown">Agents <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Agents">Manage Agents</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Employers" class="dropdown-toggle" data-toggle="dropdown">Employers <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Employers">Manage Employers</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Employees" class="dropdown-toggle" data-toggle="dropdown">Employees <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Employees">Manage Employees</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Users" class="dropdown-toggle" data-toggle="dropdown">Users <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Add User">Add User</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Users">Manage Users</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
								
							
								
							
								
							
								
							
								
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Reports" class="dropdown-toggle" data-toggle="dropdown">Reports <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Accounts Payable">Accounts Payable</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Accounts Receivable">Accounts Receivable</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="QHP Accounts Receivable">QHP Accounts Receivable</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Remittance Report">Remittance Report</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="QHP Enrollment Report">QHP Enrollment Report</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="SADP Enrollment Report">SADP Enrollment Report</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Payment Monitor Report">Payment Monitor Report</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
							
								
							
								
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Tickets" class="dropdown-toggle" data-toggle="dropdown">Tickets <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Create Tickets">Create Ticket</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Tickets">Manage Tickets</a></li>
														
													
												
											
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Manage Workgroup">Manage Workgroup</a></li>
														
													
												
											
										
									</ul>
							
						</li>
					
						
						
						
							
								
							
						
						<li class="dropdown ">
							
								
								
									<a href="#" title="Billing" class="dropdown-toggle" data-toggle="dropdown">Billing <i class="caret"></i></a>
								
							
							<!-- build sub menus...  -->
							
								
									<ul class="dropdown-menu">
										
											
												
												
												
													
														
														
														
														<li><a href="#" title="Re-issue Invoices">Re-issue Invoices</a></li>
														
													
												
											
										
									</ul>
							
						</li>
						
						
					
				</ul>
		</div>
	</div>
