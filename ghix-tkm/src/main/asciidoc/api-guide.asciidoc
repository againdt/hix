= Get Insured Eligibility Services API Guide
Dev Team;
:doctype: book
:toc:
:sectanchors:
:sectlinks:
:toclevels: 4
:source-highlighter: highlightjs

[[overview]]
= Overview

[[overview-http-verbs]]
== HTTP verbs

RESTful notes tries to adhere as closely as possible to standard HTTP and REST conventions in its
use of HTTP verbs.

|===
| Verb | Usage


| `GET`
| Used to retrieve a resource

| `POST`
| Used to create a new resource

| `PATCH`
| Used to update an existing resource, including partial updates

| `DELETE`
| Used to delete an existing resource
|===

[[overview-http-status-codes]]
== HTTP status codes

RESTful notes tries to adhere as closely as possible to standard HTTP and REST conventions in its
use of HTTP status codes.

|===
| Status code | Usage

| `200 OK`
| The request completed successfully

| `201 Created`
| A new resource has been created successfully. The resource's URI is available from the response's
`Location` header

| `204 No Content`
| An update to an existing resource has been applied successfully

| `400 Bad Request`
| The request was malformed. The response body will include an error providing further information

| `404 Not Found`
| The requested resource did not exist
|===

[[overview-errors]]
== Errors

Whenever an error response (status code >= 400) is returned, the body will contain a JSON object
that describes the problem. The error object has the following fields:

|===
| Field | Description

| error
| The HTTP error that occurred, e.g. `Bad Request`

| message
| A description of the cause of the error

| path
| The path to which the request was made

| status
| The HTTP status code, e.g. `400`

| timestamp
| The time, in milliseconds, at which the error occurred
|===


[[overview-hypermedia]]
== Hypermedia

RESTful Notes uses hypermedia and resources include links to other resources in their
responses. Responses are in http://stateless.co/hal_specification.html[Hypertext Application
Language (HAL)] format. Links can be found benath the `_links` key. Users of the API should
not created URIs themselves, instead they should use the above-described links to navigate
from resource to resource.


=== {counter:index}. Get Ticket Documents And Form Params

`POST` Method - Get the ticket documents and form properties

author : Not Provided

Resource URL

link: [/ticketmgmt/getTicketDocumentsAndFormParams]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Perform TicketWorkflow Reclassify

`POST` Method - Reclasify the ticket.

author : Not Provided

Resource URL

link: [/ticketmgmt/performTicketWorkflowReclassify]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Household Names For Created-For

`POST` Method - Get the list of households for created-for field.

author : Not Provided

Resource URL

link: [/ticketmgmt/getHouseholdNamesForCreatedFor]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Find by Id

`POST` Method - Get the ticket entity for the given id.

author : Not Provided

Resource URL

link: [/ticketmgmt/findbyid/{id}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get ticket type

`POST` Method - Get the ticket types.

author : Not Provided

Resource URL

link: [/ticketmgmt/gettickettype]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get subject list

`POST` Method - Get the list of subjects. Used in search ticket to show the subject dropdown

author : Not Provided

Resource URL

link: [/ticketmgmt/getticketsubject]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Task List

`POST` Method - Get the tasks list for the ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/gettickettasklist]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Save Comment

`POST` Method - Save the comment for the ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/savecomment]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get User Ticket List

`POST` Method - Get the ticket list for the user and workflow

author : Not Provided

Resource URL

link: [/ticketmgmt/getUserTicketList]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Claim Ticket

`POST` Method - Returns SUCCESS if ticket claimed successfully. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/claimTicketTask]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Complete Ticket

`POST` Method - Returns SUCCESS if ticket completed successfully. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/completeTicketTask]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Create New Ticket

`POST` Method - Returns SUCCESS if a new ticket created successfully. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/createnewticket]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Details

`POST` Method - Returns ticket details

author : Not Provided

Resource URL

link: [/ticketmgmt/getticketdetails]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Find Ticket History

`GET` Method - Will fetch ticket history details

author : Not Provided

Resource URL

link: [/ticketmgmt/findhistorybyid/{id}]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Form Properties of Task

`POST` Method - Will return form properties of a task

author : Not Provided

Resource URL

link: [/ticketmgmt/getTaskFormProperties]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Documents

`POST` Method - It returns ticket documents for ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/getticketdocuments]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Activity Tasks

`POST` Method - It returns ticket activity tasks for ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/getTicketActiveTask]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Reassign Ticket Activity Task

`POST` Method - Returns SUCCESS if reassigning ticket activity task is successfully. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/reassignTicketActiveTask]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Update Queue (Workgroup) Users

`POST` Method - Returns SUCCESS if workgroup users are successfully saved. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/updatequeueusers]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Save Ticket Attachment

`POST` Method - Returns SUCCESS if ticket attachment is successfully saved. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/saveTicketAttachments]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Tasks By Assignee

`POST` Method - Will return ticket tasks for an assignee

author : Not Provided

Resource URL

link: [/ticketmgmt/gettickettaskbyassignee]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Tasks and User Info By Assignee

`POST` Method - Will return ticket tasks and user info for an assignee

author : Not Provided

Resource URL

link: [/ticketmgmt/geTaskUserInfobyAssignee]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Delete Ticket Attachments

`POST` Method - Returns SUCCESS if ticket attachment is successfully deleted. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/deleteTicketAttachments]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Numbers

`POST` Method - Returns ticket numbers

author : Not Provided

Resource URL

link: [/ticketmgmt/getticketnumbers]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Simple or Detailed History

`POST` Method - Returns simple or detailed ticket history based on request

author : Not Provided

Resource URL

link: [/ticketmgmt/getSimpleOrDetailedHistory]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket Count For Pie Report

`POST` Method - Will return ticket count for pie report

author : Not Provided

Resource URL

link: [/ticketmgmt/getTicketCountForPieReport]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Fetch Form Properties of Target Ticket

`POST` Method - Will return form properties of target ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/fetchTargetTicketFormProerties]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get All Users For Queue

`POST` Method - Will return users for passed queue

author : Not Provided

Resource URL

link: [/ticketmgmt/getAllUsersForQueue]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Queues For User

`POST` Method - Will return queues for passed user

author : Not Provided

Resource URL

link: [/ticketmgmt/getQueuesForUser]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Details For Quick Action Bar

`POST` Method - Returns details for quick action bar

author : Not Provided

Resource URL

link: [/ticketmgmt/getDetailsForQuickActionBar]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Pending Ticket Details

`POST` Method - Returns details of pending ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/getPendingTicketDetails]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Household By User Id

`POST` Method - Returns household for passed user id

author : Not Provided

Resource URL

link: [/ticketmgmt/getHouseholdByuserId]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Add Queues to CSR User

`POST` Method - Persists default ticket queues for csr user

author : Not Provided

Resource URL

link: [/ticketmgmt/addqueuestocsruser]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Ageing Report

`POST` Method - Returns ageing report details

author : Not Provided

Resource URL

link: [/ticketmgmt/ageing/report]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Ticket Management Welcome page

`GET` Method - Ticket Management Welcome page

author : Not Provided

Resource URL

link: [/ticketmgmt/welcome]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Search Tickets

`POST` Method - Returns tickets based on serach criteria

author : Not Provided

Resource URL

link: [/ticketmgmt/searchtickets]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Workflows

`POST` Method - Will return workflows

author : Not Provided

Resource URL

link: [/ticketmgmt/getworkflowlist]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Queues

`POST` Method - Will return queues

author : Not Provided

Resource URL

link: [/ticketmgmt/queueslist]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Add New Ticket

`POST` Method - Returns SUCCESS if ticket is successfully created. Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/addnewtickets]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Edit Ticket

`POST` Method - Returns saved ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/editticket]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Update Ticket

`POST` Method - Returns updated ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/updateticket]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Cancel Ticket

`POST` Method - Returns cancelled ticket

author : Not Provided

Resource URL

link: [/ticketmgmt/cancelticket]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Tickets

`POST` Method - Returns Ticket list

author : Not Provided

Resource URL

link: [/ticketmgmt/getticketlist]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Requesters

`POST` Method - Will return list of requesters

author : Not Provided

Resource URL

link: [/ticketmgmt/getrequesters]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Archive Ticket

`POST` Method - Returns SUCCESS if ticket is successfully archived.  Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/archive]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Reopen Ticket

`POST` Method - Returns SUCCESS if ticket is successfully reopened.  Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/reopen]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Restart Ticket

`POST` Method - Returns SUCCESS if ticket is successfully restarted.  Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/restart]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Get Ticket List and Count

`POST` Method - Will return ticket list and count

author : Not Provided

Resource URL

link: [/ticketmgmt/list]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. Assign Ticket

`POST` Method - Returns SUCCESS if ticket is successfully assigned.  Else FAILURE

author : Not Provided

Resource URL

link: [/ticketmgmt/assign]

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. 

`Not Provided` Method - Not Provided

author : Not Provided

Resource URL

link: Not Provided

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----


=== {counter:index}. 

`Not Provided` Method - Not Provided

author : Not Provided

Resource URL

link: Not Provided

==== Resource Information

|===
| `Response formats` | JSON

| `Requires authentication?`
| No
|===

==== Sample Request

Input Parameters : Not Provided

----
Sample Request Not Provided
----

==== Sample Response

Output Parameters : Not Provided

----
Sample Response Not Provided
----

