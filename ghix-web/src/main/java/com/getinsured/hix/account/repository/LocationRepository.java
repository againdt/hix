package com.getinsured.hix.account.repository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.Location;

@Repository
@Transactional(readOnly = true)
public class LocationRepository {
	@Autowired
	private ILocationRepository locationRepository;
	
	public Location findById(Integer id) {
		if( locationRepository.exists(id) ){
			return locationRepository.findOne(id);
		}
		return null;
    }

	public List<Location> findAll() {
       return locationRepository.findAll();
    }
	
	public Page<Location> findAll(Pageable pageable) {
	       return locationRepository.findAll(pageable);
	}
	
    @Transactional
    public Location save(Location location) {
            return locationRepository.save(location);
    }
}
