//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.03.08 at 02:55:30 PM IST 
//


package com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlprda;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.hhs.cms.dsh.sim.ee.vlprda package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DeferredActionExpirationNAIndicator_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "DeferredActionExpirationNAIndicator");
    private final static QName _CoaCode_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "CoaCode");
    private final static QName _FamilyUnityExpirationDate_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "FamilyUnityExpirationDate");
    private final static QName _PendingPrimaFacieSelfPetitionIndicator_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "PendingPrimaFacieSelfPetitionIndicator");
    private final static QName _I360ApplicationFiledIndicator_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "I360ApplicationFiledIndicator");
    private final static QName _CommentsToAgencyText_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "CommentsToAgencyText");
    private final static QName _ExpirationDate_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "ExpirationDate");
    private final static QName _DurationOfStatusIndicator_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "DurationOfStatusIndicator");
    private final static QName _ApprovedSelfPetitionIndicator_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "ApprovedSelfPetitionIndicator");
    private final static QName _ResolutionData_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "ResolutionData");
    private final static QName _ParoleeExpirationDate_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "ParoleeExpirationDate");
    private final static QName _DeferredActionStatusExpirationDate_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "DeferredActionStatusExpirationDate");
    private final static QName _UscisBenefitsText_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "UscisBenefitsText");
    private final static QName _AdmittedToDate_QNAME = new QName("http://vlprda.ee.sim.dsh.cms.hhs.gov", "AdmittedToDate");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.hhs.cms.dsh.sim.ee.vlprda
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdditionalFieldsType }
     * 
     */
    public AdditionalFieldsType createAdditionalFieldsType() {
        return new AdditionalFieldsType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "DeferredActionExpirationNAIndicator")
    public JAXBElement<Boolean> createDeferredActionExpirationNAIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_DeferredActionExpirationNAIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "CoaCode")
    public JAXBElement<String> createCoaCode(String value) {
        return new JAXBElement<String>(_CoaCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "FamilyUnityExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createFamilyUnityExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_FamilyUnityExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "PendingPrimaFacieSelfPetitionIndicator")
    public JAXBElement<Boolean> createPendingPrimaFacieSelfPetitionIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_PendingPrimaFacieSelfPetitionIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "I360ApplicationFiledIndicator")
    public JAXBElement<Boolean> createI360ApplicationFiledIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_I360ApplicationFiledIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "CommentsToAgencyText")
    public JAXBElement<String> createCommentsToAgencyText(String value) {
        return new JAXBElement<String>(_CommentsToAgencyText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "ExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "DurationOfStatusIndicator")
    public JAXBElement<Boolean> createDurationOfStatusIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_DurationOfStatusIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "ApprovedSelfPetitionIndicator")
    public JAXBElement<Boolean> createApprovedSelfPetitionIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_ApprovedSelfPetitionIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalFieldsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "ResolutionData")
    public JAXBElement<AdditionalFieldsType> createResolutionData(AdditionalFieldsType value) {
        return new JAXBElement<AdditionalFieldsType>(_ResolutionData_QNAME, AdditionalFieldsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "ParoleeExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createParoleeExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_ParoleeExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "DeferredActionStatusExpirationDate")
    public JAXBElement<XMLGregorianCalendar> createDeferredActionStatusExpirationDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_DeferredActionStatusExpirationDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "UscisBenefitsText")
    public JAXBElement<String> createUscisBenefitsText(String value) {
        return new JAXBElement<String>(_UscisBenefitsText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link XMLGregorianCalendar }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://vlprda.ee.sim.dsh.cms.hhs.gov", name = "AdmittedToDate")
    public JAXBElement<XMLGregorianCalendar> createAdmittedToDate(XMLGregorianCalendar value) {
        return new JAXBElement<XMLGregorianCalendar>(_AdmittedToDate_QNAME, XMLGregorianCalendar.class, null, value);
    }

}
