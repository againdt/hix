package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBElement;
import javax.xml.datatype.XMLGregorianCalendar;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.log4j.Logger;

import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionRequest;
import com.getinsured.eligibility.at.dto.EligEngineHouseholdCompositionResponse;
import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxDependentType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxFilerType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxHouseholdType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.TaxReturnType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.TaxReturnFilingStatusCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.GYear;
import com.getinsured.iex.erp.gov.niem.niem.structures._2.ReferenceType;
import com.jayway.jsonpath.JsonPath;

public class TaxHouseHoldMapper {
	
	private static Logger lOGGER = Logger.getLogger(TaxHouseHoldMapper.class);
	private GhixRestTemplate ghixRestTemplate;

	public GhixRestTemplate getGhixRestTemplate() {
		return ghixRestTemplate;
	}

	public void setGhixRestTemplate(GhixRestTemplate ghixRestTemplate) {
		this.ghixRestTemplate = ghixRestTemplate;
	}
	
	// check if "taxHouseholdComposition" -> "taxFilingStatus":"FILING_JOINTLY" then isJointTaxFiling = true
			// if isJointTaxFiling == true then "bloodRelationship" -> "individualPersonId" == primaryFilerId then find the relation with "relation": 01 
	//and get the "relatedPersonId" and set it as spousePersonId
			// remove householdSize
			// go through all members taxHouseholdComposition and if "taxRelationship" is DEPENDENT 
	//for any member set hasDependants = true and put in list of dependent ids

	public TaxReturnType createTaxReturns(String appDataJson,Long coverageYear,AccountTransferRequestPayloadType returnObj, Map<Long, Map<String, VerificationMetadataType>> verificationMetadatas, Long ssapId) {
		lOGGER.debug(coverageYear);
		TaxReturnType taxReturnType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxReturnType();
		JSONObject application =  (JSONObject)JsonPath.read(appDataJson,"singleStreamlinedApplication");
		JSONArray houseHolds = (JSONArray)((JSONObject)((JSONArray)application.get("taxHousehold")).get(0)).get("householdMember");
		
		int primaryFilerId = (Integer)((JSONObject)((JSONArray)application.get("taxHousehold")).get(0)).get("primaryTaxFilerPersonId");
		int spousePersonId = 0;
		boolean isJointTaxFiling = false;
		
		JSONObject PrimaryHouseholdMember = (JSONObject) houseHolds.get(primaryFilerId-1);
		if(PrimaryHouseholdMember.get("taxHouseholdComposition") != null){
			JSONObject primaryTaxHouseholdComposition = (JSONObject) PrimaryHouseholdMember.get("taxHouseholdComposition");
			if("FILING_JOINTLY".equals(primaryTaxHouseholdComposition.get("taxFilingStatus"))){
				  isJointTaxFiling = true;
			}
			
			if(isJointTaxFiling == true){
				List<JSONObject> relations = (List<JSONObject>) PrimaryHouseholdMember.get("bloodRelationship");
				for (int i = 0; i < relations.size(); i++) {
					JSONObject relation = (JSONObject) relations.get(i);
					int individualPersonId = Integer.parseInt((String) relation.get("individualPersonId"));
					int relatedPersonId = Integer.parseInt((String)relation.get("relatedPersonId"));
					String relationCode = (java.lang.String)relation.get("relation");
					
					if( (primaryFilerId == individualPersonId) && relationCode.equals("01")){
						spousePersonId = relatedPersonId;
						break;
					}
				}
			}
		}
		
		List<Integer> dependentIds = new ArrayList<Integer>();
		boolean hasDependants = false;
		for(Object householdMember : houseHolds) {
			  JSONObject member = (JSONObject) householdMember;
			  if(member.get("taxHouseholdComposition") != null){
				  JSONObject taxHouseholdComposition = (JSONObject) member.get("taxHouseholdComposition");
				  if("DEPENDENT".equals(taxHouseholdComposition.get("taxRelationship"))){
					  hasDependants = true;
					  dependentIds.add(Integer.parseInt(member.get("personId").toString()));
				  }
			  }
		}
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean hasDependantsBoolean = AccountTransferUtil.basicFactory.createBoolean();
		hasDependantsBoolean.setValue(hasDependants);
		taxReturnType.setTaxReturnIncludesDependentIndicator(hasDependantsBoolean);
		
		GYear taxReturnYear = AccountTransferUtil.basicFactory.createGYear();
		XMLGregorianCalendar taxyear = AccountTransferUtil.getSystemDateYear();
		taxyear.setYear(coverageYear.intValue());
		taxReturnYear.setValue(taxyear);
		taxReturnType.setTaxReturnYear(taxReturnYear);
		
		TaxReturnFilingStatusCodeType taxReturnFilingStatusCodeType = AccountTransferUtil.hixTypeFactory.createTaxReturnFilingStatusCodeType();
		if(isJointTaxFiling){
			taxReturnFilingStatusCodeType.setValue("2");
		}else if(hasDependants){
			taxReturnFilingStatusCodeType.setValue("1");
		}else{
			taxReturnFilingStatusCodeType.setValue("4");
		}
		taxReturnType.setTaxReturnFilingStatusCode(taxReturnFilingStatusCodeType);
		
		// TaxReturn/TaxHousehold
		TaxHouseholdType taxHousehold = AccountTransferUtil.insuranceApplicationObjFactory.createTaxHouseholdType();
		EligEngineHouseholdCompositionResponse eligEngineHouseholdCompositionResponse = this.getHouseholdComposition(ssapId);
		if(eligEngineHouseholdCompositionResponse != null && eligEngineHouseholdCompositionResponse.getHouseholdIncome() > 0){
			IncomeType houseHoldIncome = AccountTransferUtil.hixCoreFactory.createIncomeType();
		    AmountType amount = AccountTransferUtil.niemCoreFactory.createAmountType();
		    amount.setValue(BigDecimal.valueOf(eligEngineHouseholdCompositionResponse.getHouseholdIncome()));
		    houseHoldIncome.setIncomeAmount(amount);
		    
		    //TODO:need confirmation for incomedate
		    DateType incomeDateType =  AccountTransferUtil.niemCoreFactory.createDateType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date incomeDate = AccountTransferUtil.basicFactory.createDate();
			incomeDate.setValue(AccountTransferUtil.getSystemDate());
			incomeDateType.setDate(incomeDate);
			houseHoldIncome.setIncomeDate(incomeDateType);
			
		    Map<String, VerificationMetadataType> personVerificationMetadata = verificationMetadatas.get(1L);
            VerificationMetadataType incomeMetadata = personVerificationMetadata.get(VerificationCategoryCodeSimpleType.ANNUAL_INCOME.value());
            if(incomeMetadata != null) {
                houseHoldIncome.getMetadata().add(incomeMetadata);
            } 
		    JAXBElement<IncomeType> jaxbhouseholdincome = AccountTransferUtil.insuranceApplicationObjFactory.createHouseholdIncome(houseHoldIncome);
		    taxHousehold.getHouseholdIncomeOrHouseholdAGIOrHouseholdMAGI().add(jaxbhouseholdincome);
		    
		    QuantityType householdSizeQuantity= AccountTransferUtil.niemCoreFactory.createQuantityType();
			householdSizeQuantity.setValue(new BigDecimal(eligEngineHouseholdCompositionResponse.getHouseholdSize()));
			taxHousehold.setHouseholdSizeQuantity(householdSizeQuantity);
		}

		taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent().add(createTaxFiler("primary",primaryFilerId,returnObj));
		if(isJointTaxFiling){
			taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent().add(createTaxFiler("spouse",spousePersonId,returnObj));
		}
		
		if(dependentIds.size() > 0){
			for (Integer dependentid : dependentIds) {
				TaxDependentType depTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxDependentType();
				ReferenceType dependantRefType = AccountTransferUtil.niemStructFactory.createReferenceType();
				dependantRefType.setRef(returnObj.getPerson().get(dependentid-1));
				depTaxFilerType.setRoleOfPersonReference(dependantRefType);
	            IdentificationType ssnIdType = createSSNIdentificationType((dependentid-1),returnObj);
				if(ssnIdType != null){
					depTaxFilerType.setTINIdentification(ssnIdType);
				}
				JAXBElement<TaxDependentType> dependantTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxDependent(depTaxFilerType);
				taxHousehold.getPrimaryTaxFilerOrSpouseTaxFilerOrTaxDependent().add(dependantTaxFilerType);
			}
			
		}
	    taxReturnType.setTaxHousehold(taxHousehold);
	    lOGGER.debug("taxReturnType "+ taxReturnType.toString());
	    return taxReturnType;
	}
	
	private EligEngineHouseholdCompositionResponse getHouseholdComposition(Long ssapApplicationId){
		try {
			EligEngineHouseholdCompositionRequest eligEngineHouseholdCompositionRequest = new EligEngineHouseholdCompositionRequest();
			eligEngineHouseholdCompositionRequest.setApplicationId(ssapApplicationId);
			eligEngineHouseholdCompositionRequest.setRequestId(String.valueOf(2));
			
			  
			EligEngineHouseholdCompositionResponse eligEngineHouseholdCompositionResponse = null;
			eligEngineHouseholdCompositionResponse = this.ghixRestTemplate.postForObject(
					GhixEndPoints.EligibilityEndPoints.HOUSEHOLD_COMPOSITION, eligEngineHouseholdCompositionRequest,
					EligEngineHouseholdCompositionResponse.class);
			if("sucess".equals(eligEngineHouseholdCompositionResponse.getStatus())){
				return eligEngineHouseholdCompositionResponse;
			}else{
				lOGGER.error("Response not ok from eligibility engine " + eligEngineHouseholdCompositionResponse.getStatus());
			}
		} catch (Exception e) {
			lOGGER.error(e.getStackTrace());
		}
		return null;
	}
	
	private JAXBElement<TaxFilerType> createTaxFiler(String primaryOrSpouse,int primaryorSpouseTaxFilerId, AccountTransferRequestPayloadType atRequest){
		int primaryorSpouseFilerId = primaryorSpouseTaxFilerId-1;
		TaxFilerType headTaxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createTaxFilerType();
		ReferenceType refType = AccountTransferUtil.niemStructFactory.createReferenceType();
		
		refType.setRef(atRequest.getPerson().get(primaryorSpouseFilerId));
		headTaxFilerType.setRoleOfPersonReference(refType);
	    IdentificationType ssnIdType = createSSNIdentificationType(primaryorSpouseFilerId,atRequest);
		if(ssnIdType != null){
			headTaxFilerType.setTINIdentification(ssnIdType);
		}
		JAXBElement<TaxFilerType> taxFilerType = null;
		if("primary".equalsIgnoreCase(primaryOrSpouse)){
			taxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createPrimaryTaxFiler(headTaxFilerType);
		}
		if("spouse".equalsIgnoreCase(primaryOrSpouse)){
			taxFilerType = AccountTransferUtil.insuranceApplicationObjFactory.createSpouseTaxFiler(headTaxFilerType);
		}
		return taxFilerType;		
	}
	
	private IdentificationType createSSNIdentificationType(int filerId, AccountTransferRequestPayloadType atRequest){
		IdentificationType ssnIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String ssnIdentificationValue = AccountTransferUtil.basicFactory.createString();
		if(!atRequest.getPerson().isEmpty()){
			if(!atRequest.getPerson().get(filerId).getPersonSSNIdentification().isEmpty()){
				ssnIdentificationValue.setValue(atRequest.getPerson().get(filerId).getPersonSSNIdentification().get(0).getIdentificationID().getValue());
	        	ssnIdentificationType.setIdentificationID(ssnIdentificationValue);
		    }else{
		    	return null;
		    }
		}        
		return ssnIdentificationType;
	}
}
