package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.InitialVerificationResponseType;

/**
 * Wrapper Class for Agency3InitVerifIndividualResponseType
 * 
 * @author Nikhil Talreja
 * @since 14-Feb-2014
 * 
 */
public class InitialVerificationResponseTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InitialVerificationResponseTypeWrapper.class);
	private InitialVerificationResponseType intialVerificationResponseType;
	
	private ResponseMetadataWrapper responseMetadata;
	private String lawfulPresenceVerifiedCode;
	private InitialVerificationResponseSetTypeWrapper intialVerificationResponseTypeResponseSet;
	
	public InitialVerificationResponseTypeWrapper(InitialVerificationResponseType initialVerificationResponseType){
		
		if(intialVerificationResponseType == null){
			return;
		}
		
		ResponseMetadataWrapper responseMetadataWrapper = new ResponseMetadataWrapper(initialVerificationResponseType.getResponseMetadata());
		this.responseMetadata = responseMetadataWrapper;
		
	
		InitialVerificationResponseSetTypeWrapper initialVerificationResponseSetTypeWrapper = 
				new InitialVerificationResponseSetTypeWrapper(intialVerificationResponseType.getInitialVerificationResponseSet());
		
		
		this.intialVerificationResponseTypeResponseSet = initialVerificationResponseSetTypeWrapper;
	}

	public InitialVerificationResponseType getInitialVerificationResponseType() {
		return intialVerificationResponseType;
	}

	public void setInitialVerificationResponseType(
			InitialVerificationResponseType initialVerificationResponseType) {
		this.intialVerificationResponseType = initialVerificationResponseType;
	}

	public ResponseMetadataWrapper getResponseMetadata() {
		return responseMetadata;
	}

	public void setResponseMetadata(ResponseMetadataWrapper responseMetadata) {
		this.responseMetadata = responseMetadata;
	}

	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}

	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public InitialVerificationResponseSetTypeWrapper getInitialVerificationResponseSet() {
		return intialVerificationResponseTypeResponseSet;
	}

	public void setInitialVerificationResponseSet(
			InitialVerificationResponseSetTypeWrapper intialVerificationResponseTypeResponseSet) {
		this.intialVerificationResponseTypeResponseSet = intialVerificationResponseTypeResponseSet;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		if(responseMetadata != null)
			obj.put("ResponseMetadata",this.responseMetadata);
		
		if(lawfulPresenceVerifiedCode != null)
			obj.put("LawfulPresenceVerifiedCode",this.lawfulPresenceVerifiedCode);
		
		if(intialVerificationResponseTypeResponseSet != null)
			obj.put("IntialVerificationResponseTypeResponseSet",this.intialVerificationResponseTypeResponseSet);
		return obj.toJSONString();
	}

}
