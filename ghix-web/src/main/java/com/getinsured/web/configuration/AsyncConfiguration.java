package com.getinsured.web.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * Asynchronous configuration, enables Async.
 *
 * @author Yevgen Golubenko
 * @since 2019-09-20
 */
@EnableAsync
@Configuration
public class AsyncConfiguration { }
