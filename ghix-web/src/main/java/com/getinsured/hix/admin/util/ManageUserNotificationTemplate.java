package com.getinsured.hix.admin.util;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.notify.EmailService;
import com.getinsured.hix.platform.notify.NoticeTemplateFactory;
import com.getinsured.hix.platform.notify.NotificationAgent;
import com.getinsured.hix.platform.repository.NoticeRepository;
import com.getinsured.hix.platform.repository.NoticeTypeRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.util.GhixDBSequenceUtil;
import com.getinsured.hix.platform.util.GhixEndPoints;

/**
 * Helper class to send Notification email in ticket Management flow.
 *
 * @author Satyakam kaul
 *
 */

@Component
public class ManageUserNotificationTemplate extends NotificationAgent {

	@Override
	public Map<String, String> getTokens(Map<String, Object> notificationContext) {
	    String exchangeName = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_NAME); 
	    String phoneNumber = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE);
		Map<String,String> bean = new HashMap<String, String>();
       
		String updatedFields = (String) notificationContext.get("UPDATED_FIELDS");
		String userName = (String) notificationContext.get("USER_NAME");
		bean.put("exchangeName", exchangeName );
		bean.put("updatedFields", updatedFields );
		bean.put("phoneNumber", phoneNumber );
		bean.put("host", GhixEndPoints.GHIXWEB_SERVICE_URL );
		bean.put("userNamee",userName );
		return bean;		
	}

	@Override
	public Map<String, String> getEmailData(
		Map<String, Object> notificationContext) {
		String recipient = (String) notificationContext.get("RECIPIENT");
		String subject = (String) notificationContext.get("SUBJECT");
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("To", recipient);
		data.put("Subject", subject);

		return data;
	}
	
	@Autowired
	public void setEmailService(EmailService emailService) {
		this.emailService = emailService;
	}

	@Autowired
	public void setNoticeTypeRepo(NoticeTypeRepository noticeTypeRepo) {
		this.noticeTypeRepo = noticeTypeRepo;
	}

	@Autowired
	public void setNoticeRepo(NoticeRepository noticeRepo) {
		this.noticeRepo = noticeRepo;
	}

	@Autowired
	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Autowired
	public void setAppContext(ApplicationContext appContext) {
		this.appContext = appContext;
	}

	@Autowired
	public void setEcmService(ContentManagementService ecmService) {
		this.ecmService = ecmService;
	}

	@Autowired
	public void setGhixDBSequenceUtil(GhixDBSequenceUtil ghixDBSequenceUtil) {
		this.ghixDBSequenceUtil = ghixDBSequenceUtil;
	}
	
	@Autowired
	public void setNoticeTemplateFactory(NoticeTemplateFactory templateFactory) {
		this.templateFactory = templateFactory;
	}
}