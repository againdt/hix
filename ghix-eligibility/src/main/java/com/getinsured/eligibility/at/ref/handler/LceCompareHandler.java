package com.getinsured.eligibility.at.ref.handler;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.LceDemoCompareApplicantStatusLogic;
import com.getinsured.eligibility.at.ref.dto.CompareApplicantDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.iex.util.ReferralConstants;

/**
 * @author chopra_s
 * 
 */
@Component("lceCompareHandler")
@Scope("singleton")
public class LceCompareHandler {
	public void processCompare(CompareMainDTO compareMainDTO) {
		final List<CompareApplicantDTO> enrolledList = compareMainDTO.getEnrolledApplication().getApplicants();
		final List<CompareApplicantDTO> currentList = compareMainDTO.getCurrentApplication().getApplicants();

		CompareApplicantDTO enrolledApplicant = null;

		for (CompareApplicantDTO currentApplicant : currentList) {
			if (currentApplicant.isPrimaryTaxFiler()) {
				enrolledApplicant = findCompareApplicant(currentApplicant, enrolledList);
				if (null != enrolledApplicant) {
					processBloodRelationship(enrolledList, currentList, currentApplicant, enrolledApplicant);
				}
				break;
			}
		}

		for (CompareApplicantDTO currentApplicant : currentList) {
			// System.out.println(currentApplicant.getExternalApplicantId() + " : " + currentApplicant.getStatus() + " : " + currentApplicant.getEligibilityStatus());
			if (ApplicantStatusEnum.ADD_NEW.value().equals(currentApplicant.getStatus())) {
				if (StringUtils.equalsIgnoreCase(ReferralConstants.QHP, currentApplicant.getEligibilityStatus())) {
					currentApplicant.setStatus(ApplicantStatusEnum.ADD_NEW_ELIGIBILE.value());
				}
			} else if (ApplicantStatusEnum.DELETED.value().equals(currentApplicant.getStatus())) {
				enrolledApplicant = findCompareApplicant(currentApplicant, enrolledList);
				if (enrolledApplicant != null && StringUtils.equalsIgnoreCase(ReferralConstants.QHP, enrolledApplicant.getEligibilityStatus())) {
					currentApplicant.setStatus(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.value());
				}
			} else {
				enrolledApplicant = findCompareApplicant(currentApplicant, enrolledList);
				if (null != enrolledApplicant) {
					if (enrolledApplicant.isEnrolled()) {
						compareAttributes(enrolledApplicant, currentApplicant);
					}
					currentApplicant.setStatus(LceDemoCompareApplicantStatusLogic.lceCompareApplicantStatus(currentApplicant, enrolledApplicant).value());
				}
			}
		}

	}

	private void compareAttributes(CompareApplicantDTO enrolledApplicant, CompareApplicantDTO currentApplicant) {
		enrolledApplicant.compareZipCounty(currentApplicant);
		enrolledApplicant.compareDob(currentApplicant);
		enrolledApplicant.compareCitizenShipStatus(currentApplicant);
	}

	private CompareApplicantDTO findCompareApplicant(CompareApplicantDTO currentApplicant, List<CompareApplicantDTO> enrolledList) {
		for (CompareApplicantDTO enrolledApplicant : enrolledList) {
			// System.out.println(currentApplicant.getExternalApplicantId() + " : " + enrolledApplicant.getExternalApplicantId());
			if (((null!=currentApplicant.getExternalApplicantId() && null!=enrolledApplicant.getExternalApplicantId()) && StringUtils.equalsIgnoreCase(currentApplicant.getExternalApplicantId(), enrolledApplicant.getExternalApplicantId()))   ||  StringUtils.equalsIgnoreCase( currentApplicant.getApplicantGuid(),enrolledApplicant.getApplicantGuid())) {
				return enrolledApplicant;
			}
		}
		return null;
	}

	private void processBloodRelationship(List<CompareApplicantDTO> enrolledList, List<CompareApplicantDTO> currentList, CompareApplicantDTO currentApplicant, CompareApplicantDTO enrolledApplicant) {
		CompareApplicantDTO currentApplicantTemp = null;
		Map<String, String> currentidMap = new HashMap<String, String>();
		Map<String, String> enrolledidMap = new HashMap<String, String>();
		ApplicantStatusEnum tempEnum;
		for (CompareApplicantDTO enrolledApplicantTemp : enrolledList) {
			if (enrolledApplicantTemp.isEnrolled()) {
				currentApplicantTemp = findCompareApplicant(enrolledApplicantTemp, currentList);
				if (null != currentApplicantTemp) {
					tempEnum = ApplicantStatusEnum.fromValue(currentApplicantTemp.getStatus());
					if (ReferralConstants.NO_CHANGE.contains(tempEnum) || ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(tempEnum)) {
						enrolledidMap.put("" + enrolledApplicantTemp.getPersonId(), enrolledApplicantTemp.getApplicantGuid());
					}
				}
			}
		}
		for (CompareApplicantDTO compareApplicantDTO : currentList) {
			currentidMap.put("" + compareApplicantDTO.getPersonId(), compareApplicantDTO.getApplicantGuid());
		}
		enrolledApplicant.compareBloodRelationship(currentApplicant, currentidMap, enrolledidMap);

	}
}
