package com.getinsured.eligibility;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.apache.commons.lang.math.NumberUtils;
import org.hibernate.validator.constraints.Email;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.at.ref.service.LceEditApplicationService;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationService;
import com.getinsured.eligibility.at.resp.service.SsapVerificationService;
import com.getinsured.eligibility.at.service.AccountTransferService;
import com.getinsured.eligibility.common.dto.AutomateApplicationResponse;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.externalnotices.service.ExternalNoticesService;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneService;
import com.getinsured.eligibility.redetermination.service.ApplicationCloneServiceImpl;
import com.getinsured.eligibility.redetermination.service.RenewalApplicantService;
import com.getinsured.eligibility.redetermination.service.RenewalApplicationService;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.ssap.model.EligibilityResponse;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.dto.externalnotices.ExternalNoticesResponseDTO;
import com.getinsured.hix.externalnotice.util.ExternalNoticeConstants;
import com.getinsured.hix.indportal.dto.dm.application.ApplicationJSONDTO;
import com.getinsured.hix.indportal.dto.dm.application.Attestations;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.Demographic;
import com.getinsured.hix.indportal.dto.dm.application.HomeAddress;
import com.getinsured.hix.indportal.dto.dm.application.MailingAddress;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.hix.indportal.dto.dm.application.Result;
import com.getinsured.hix.model.GIWSPayload;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.ssap.MultiHousehold;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.dto.SsapPreferencesDTO;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.request.ssap.SsapApplicationRequest;
import com.getinsured.iex.request.ssap.SsapApplicationResponse;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.SpecialCircumstances;
import com.getinsured.iex.ssap.TaxHousehold;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.financial.TaxHouseholdComposition;
import com.getinsured.iex.ssap.financial.type.TaxFilingStatus;
import com.getinsured.iex.ssap.financial.type.TaxRelationship;
import com.getinsured.iex.ssap.model.AccountTransferSpanInfo;
import com.getinsured.iex.ssap.model.AptcHistory;
import com.getinsured.iex.ssap.model.RenewalApplication;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.AptcHistoryRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.ssap.service.SsapAutoEnrollService;
import com.getinsured.iex.ssap.service.SsapEventsService;
import com.getinsured.iex.ssap.service.SsapService;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.sql.TSTimestamp;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;



@RestController
@Validated
@RequestMapping("/application")
public class SsapAppController {
	private static final String OUTPUT_HEALTH_ENROLLMENT_STATUS_KEY = "healthEnrollmentStatus";
	private static final String OUTPUT_VALID_EVENTS_EXIST_KEY = "validEventsExist";
	private static final String OUTPUT_SSAP_APPLICATION_ID_KEY = "ssapApplicationId";
	private static final String INPUT_ENROLLMENT_YEAR_KEY = "enrollmentYear";
	private static final String INPUT_HOUSEHOLD_ID_KEY = "houseHoldId";
	private static final String ERR_CODE_DESCRIPTION_KEY = "errCodeDescription";
	private static final String ERROR_CODE_204_DESCRIPTION = "General Exception";
	private static final String ERROR_CODE_203_DESCRIPTION = "Household Id and Enrollment Year are required arguments.";
	private static final String ERROR_CODE_201_DESCRIPTION = "No record found from the given search criteria.";
	private static final String ERROR_CODE_204 = "204";
	private static final String ERROR_CODE_203 = "203";
	private static final String ERROR_CODE_201 = "201";
	private static final String SUCEES_CODE_200 = "200";
	private static final String ERR_CODE_KEY = "errCode";
    public static final List<String> nonCloseablestatuses = Arrays.asList(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(),
            ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode());
    public static final List<String> allowedStatusesForClone = Arrays.asList(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode(),ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode(),
            ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode());

	@Autowired private SsapApplicationRepository ssapApplicationRepository;
	@Autowired private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired private SsapApplicantRepository ssapApplicantRepository;
	@Autowired private CmrHouseholdRepository cmrHouseholdRepository;
	@Autowired private SsapService ssapService;
	@Autowired private SsapAutoEnrollService ssapAutoEnrollService;
	@Autowired private Gson platformGson;
	@Autowired private SsapApplicationService ssapApplicationService;
	@Autowired private SsapEventsService ssapEventService;
	@Autowired private ExternalNoticesService externalNoticesService;
	@Autowired private ATSpanProcessingRepository atSpanProcessingRepository;
	@Autowired private RenewalApplicantService renewalApplicantService;
	@Autowired private RenewalApplicationService renewalApplicationService;
	@Autowired private ReferralOEService referralOEService;
	@Autowired private AptcHistoryRepository aptcHistoryRepository;
	@Autowired private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired private ILocationRepository iLocationRepository;
	@Autowired private ApplicationCloneService applicationCloneService;
	@Autowired private LceEditApplicationService lceEditApplicationService;
	@Autowired private SsapVerificationService ssapVerificationService;
	@Autowired private AccountTransferService accountTransferService;
	

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapAppController.class);
	
	private Gson mapper = new GsonBuilder().disableHtmlEscaping().create();
	private static final String JSON_START = "{\"application\":{";
	private static final String JSON_END = "}}";
	private static final String APPLICATION_START = "\"applications\":{";
		
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public void updateSsapApplicationStatus(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
		String applicationStatus = ssapApplicationRequest.getApplicationStatus();
		String caseNumber = ssapApplicationRequest.getCaseNumber();
		BigDecimal userId = ssapApplicationRequest.getUserId();
		boolean isDentalOnlyUpdate = ssapApplicationRequest.isApplicationDentalStatusUpdate();
		try {
			if(!isDentalOnlyUpdate) {
			ssapService.updateSsapApplicationStatusByCaseNumber(applicationStatus, caseNumber, new TSTimestamp(), userId);
			} else {
				ssapService.updateDentalApplicationStatusByCaseNumber(caseNumber, new TSTimestamp(), userId);
			}
		}
		catch(Exception ex) {
			LOGGER.error("Error in updating the ssap application status for caseNumber:" + caseNumber, ex);
		}
		
	}
	
	@RequestMapping(value = "/applications/coverageyearandcmrhousehold", method = RequestMethod.GET)
	public List<SsapApplicationResource> getSsapApplictionsByCoverageYearAndCmrHouseHoldId(HttpServletRequest request) {
		
		Integer coverageYear = Integer.parseInt(request.getParameter("coverageYear")==null?"0":request.getParameter("coverageYear").toString());
		BigDecimal cmrHouseHoldId= new BigDecimal(request.getParameter("cmrHouseoldId")==null?"0":request.getParameter("cmrHouseoldId").toString());
		
		AccountTransferSpanInfo atSpanInfo = null;
		AptcHistory aptcHistory = null;
		SsapApplicationResource res = null;
		List<SsapApplication> ssaps  = null;
		List<SsapApplicationResource> resp = new ArrayList<SsapApplicationResource>();
		try {
			ssaps = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYear(cmrHouseHoldId, coverageYear);
			
			for(int i=0; i<ssaps.size(); i++) {
				res = new SsapApplicationResource();
				res.setCaseNumber(ssaps.get(i).getCaseNumber());
				res.setExchangeEligibilityStatus(ssaps.get(i).getExchangeEligibilityStatus()==null?"":ssaps.get(i).getExchangeEligibilityStatus().toString());
				res.setEligibilityStatus(ssaps.get(i).getEligibilityStatus()==null?"":ssaps.get(i).getEligibilityStatus().toString());
				res.setApplicationData(ssaps.get(i).getApplicationData());
				res.setApplicationStatus(ssaps.get(i).getApplicationStatus());
				res.setValidationStatus(ssaps.get(i).getValidationStatus());
				res.setCmrHouseoldId(ssaps.get(i).getCmrHouseoldId());
				res.setCoverageYear(ssaps.get(i).getCoverageYear());
				res.setCurrentPageId(ssaps.get(i).getCurrentPageId());
				res.setEsignDate(ssaps.get(i).getEsignDate());
				res.setEsignFirstName(ssaps.get(i).getEsignFirstName());
				res.setEsignLastName(ssaps.get(i).getEsignLastName());
				res.setFinancialAssistanceFlag(ssaps.get(i).getFinancialAssistanceFlag());
				res.setSource(ssaps.get(i).getSource());
				res.setCreatedBy(ssaps.get(i).getCreatedBy());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				
				res.setExemptHouseHold(ssaps.get(i).getExemptHousehold());
				res.setEligibilityReceivedDate(ssaps.get(i).getEligibilityReceivedDate());
				res.setEhbAmount(ssaps.get(i).getEhbAmount());
				res.setCsrLevel(ssaps.get(i).getCsrLevel());
				res.setNativeAmerican(ssaps.get(i).getNativeAmerican());
				res.setMaximumAPTC(ssaps.get(i).getMaximumAPTC());
				res.setElectedAPTC(ssaps.get(i).getElectedAPTC());
				res.setAvailableAPTC(ssaps.get(i).getAvailableAPTC());
				res.setMaximumStateSubsidy(ssaps.get(i).getMaximumStateSubsidy());
				res.setCreationTimestamp(ssaps.get(i).getCreationTimestamp());
				res.setLastUpdateTimestamp(ssaps.get(i).getLastUpdateTimestamp());
				res.setApplicationType(ssaps.get(i).getApplicationType());
				res.setStartDate(ssaps.get(i).getStartDate());
				res.setLastUpdatedBy(ssaps.get(i).getLastUpdatedBy());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				
				res.setEligibilityResponseType(ssaps.get(i).getEligibilityResponseType());
				res.setExternalApplicationId(ssaps.get(i).getExternalApplicationId());
				res.setRenewalStatus(ssaps.get(i).getRenewalStatus()==null?null:ssaps.get(i).getRenewalStatus().getStatus());
				res.setEsignMiddleName(ssaps.get(i).getEsignMiddleName());
				res.setLastNoticeId(ssaps.get(i).getLastNoticeId());
				res.setSsapApplicationSectionId(ssaps.get(i).getSsapApplicationSectionId());
				if(ssaps.get(i).getParentSsapApplicationId() != null){
					res.setParentSsapApplicationId(ssaps.get(i).getParentSsapApplicationId().longValue());
				}
				res.setEnrollmentStatus(ssaps.get(i).getEnrollmentStatus());
				res.setSepQepDenied(ssaps.get(i).getSepQepDenied());
				
				Map<String,String> href = new HashMap<String,String>();
				href.put("href", GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + ssaps.get(i).getId());
				Map<String,Object> self = new HashMap<String,Object>();
				self.put("self", href);
				res.set_links(self);

				res.setApplicationId(ssaps.get(i).getId());
				
				SsapApplicant ssapapplicant = ssapApplicantRepository.findPTF(res.getApplicationId());
				if(ssapapplicant != null) {
				res.setPtfFirstName(ssapapplicant.getFirstName() != null ? ssapapplicant.getFirstName() : null);
				res.setPtfMiddleName(ssapapplicant.getMiddleName() != null ? ssapapplicant.getMiddleName() : null);
				res.setPtfLastName(ssapapplicant.getLastName() != null ? ssapapplicant.getLastName() : null);
				}
				
				String iexAtMesConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
				LocalDate localDate = null;
				if (iexAtMesConfig.equalsIgnoreCase(Boolean.TRUE.toString())) {
					atSpanInfo = atSpanProcessingRepository.findBySsapApplicationId(res.getApplicationId());
					if (atSpanInfo != null) {
						Timestamp dueDate = null;
						if(atSpanInfo.getDueDate() != null) {
						localDate = LocalDate.parse(new SimpleDateFormat(SSAPConstants.DATE_FORMAT).format(atSpanInfo.getDueDate()));
						dueDate = Timestamp.valueOf(localDate.atStartOfDay());
						}
						res.setDueDate(atSpanInfo.getDueDate() != null ? dueDate : null);
						res.setEligibilityStartDate(atSpanInfo.getEligibilityStartDate() != null ? atSpanInfo.getEligibilityStartDate() : null);
						res.setEligibilityEndDate(atSpanInfo.getEligibilityEndDate() != null ? atSpanInfo.getEligibilityEndDate() : null);
						res.setQueuedStatus(atSpanInfo.getQueueStatus() != null ? atSpanInfo.getQueueStatus().toString() : null);
						
					}
					
				} else {
					aptcHistory = aptcHistoryRepository.findAptcHistoryBySsapApplicationIdForCap(res.getApplicationId());
					if (aptcHistory != null) {
						res.setEligibilityStartDate(aptcHistory.getEligStartDate() != null ? aptcHistory.getEligStartDate() : null);
						res.setEligibilityEndDate(aptcHistory.getEligEndDate() != null ? aptcHistory.getEligEndDate() : null);
					}
				}
				
				res.setApplicationDentalStatus(null == ssaps.get(i).getApplicationDentalStatus()? "" : ssaps.get(i).getApplicationDentalStatus());

				resp.add(res);
				
			}
			
		}
		catch (Exception e) {
			LOGGER.error("Eexception in getting getSsapApplictionsByCoverageYearAndCmrHouseHoldId::coverageYear:" + coverageYear + ":CmrHouseHoldId:" + cmrHouseHoldId, e);
		}
		return resp ;
	}
	
	
	@RequestMapping(value = "/applications/coverageyearandcmrhouseholdnoapp", method = RequestMethod.GET)
	public List<SsapApplicationResource> getSsapApplictionsByCoverageYearAndCmrHouseHoldIdNoAppData(HttpServletRequest request) {
		
		Integer coverageYear = Integer.parseInt(request.getParameter("coverageYear")==null?"0":request.getParameter("coverageYear").toString());
		BigDecimal cmrHouseHoldId= new BigDecimal(request.getParameter("cmrHouseoldId")==null?"0":request.getParameter("cmrHouseoldId").toString());
		
		SsapApplicationResource res = null;
		List<SsapApplication> ssaps  = null;
		List<SsapApplicationResource> resp = new ArrayList<SsapApplicationResource>();
		try {
			ssaps = ssapApplicationRepository.findByCmrHouseoldIdAndCoverageYearNoAppData(cmrHouseHoldId, coverageYear);
			
			for(int i=0; i<ssaps.size(); i++) {
				res = new SsapApplicationResource();
				res.setCaseNumber(ssaps.get(i).getCaseNumber());
				res.setExchangeEligibilityStatus(ssaps.get(i).getExchangeEligibilityStatus()==null?"":ssaps.get(i).getExchangeEligibilityStatus().toString());
				res.setEligibilityStatus(ssaps.get(i).getEligibilityStatus()==null?"":ssaps.get(i).getEligibilityStatus().toString());
				res.setApplicationStatus(ssaps.get(i).getApplicationStatus());
				res.setValidationStatus(ssaps.get(i).getValidationStatus());
				res.setCmrHouseoldId(ssaps.get(i).getCmrHouseoldId());
				res.setCoverageYear(ssaps.get(i).getCoverageYear());
				res.setCurrentPageId(ssaps.get(i).getCurrentPageId());
				res.setEsignDate(ssaps.get(i).getEsignDate());
				res.setEsignFirstName(ssaps.get(i).getEsignFirstName());
				res.setEsignLastName(ssaps.get(i).getEsignLastName());
				res.setFinancialAssistanceFlag(ssaps.get(i).getFinancialAssistanceFlag());
				res.setSource(ssaps.get(i).getSource());
				res.setCreatedBy(ssaps.get(i).getCreatedBy());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				
				res.setExemptHouseHold(ssaps.get(i).getExemptHousehold());
				res.setEligibilityReceivedDate(ssaps.get(i).getEligibilityReceivedDate());
				res.setEhbAmount(ssaps.get(i).getEhbAmount());
				res.setCsrLevel(ssaps.get(i).getCsrLevel());
				res.setNativeAmerican(ssaps.get(i).getNativeAmerican());
				res.setMaximumAPTC(ssaps.get(i).getMaximumAPTC());
				res.setElectedAPTC(ssaps.get(i).getElectedAPTC());
				res.setAvailableAPTC(ssaps.get(i).getAvailableAPTC());
				res.setMaximumStateSubsidy(ssaps.get(i).getMaximumStateSubsidy());
				res.setCreationTimestamp(ssaps.get(i).getCreationTimestamp());
				res.setLastUpdateTimestamp(ssaps.get(i).getLastUpdateTimestamp());
				res.setApplicationType(ssaps.get(i).getApplicationType());
				res.setStartDate(ssaps.get(i).getStartDate());
				res.setLastUpdatedBy(ssaps.get(i).getLastUpdatedBy());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				
				res.setEligibilityResponseType(ssaps.get(i).getEligibilityResponseType());
				res.setExternalApplicationId(ssaps.get(i).getExternalApplicationId());
				res.setRenewalStatus(ssaps.get(i).getRenewalStatus()==null?null:ssaps.get(i).getRenewalStatus().getStatus());
				res.setEsignMiddleName(ssaps.get(i).getEsignMiddleName());
				res.setLastNoticeId(ssaps.get(i).getLastNoticeId());
				res.setSsapApplicationSectionId(ssaps.get(i).getSsapApplicationSectionId());
				if(ssaps.get(i).getParentSsapApplicationId() != null){
					res.setParentSsapApplicationId(ssaps.get(i).getParentSsapApplicationId().longValue());
				}
				res.setEnrollmentStatus(ssaps.get(i).getEnrollmentStatus());
				res.setSepQepDenied(ssaps.get(i).getSepQepDenied());
				
				Map<String,String> href = new HashMap<String,String>();
				href.put("href", GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + ssaps.get(i).getId());
				Map<String,Object> self = new HashMap<String,Object>();
				self.put("self", href);
				res.set_links(self);

				res.setApplicationId(ssaps.get(i).getId());
				res.setApplicationDentalStatus(null == ssaps.get(i).getApplicationDentalStatus()? "" : ssaps.get(i).getApplicationDentalStatus());
				resp.add(res);
				
			}
			
		}
		catch (Exception e) {
			LOGGER.error("Eexception in getting getSsapApplictionsByCoverageYearAndCmrHouseHoldId::coverageYear:" + coverageYear + ":CmrHouseHoldId:" + cmrHouseHoldId, e);
		}
		return resp ;
	}
	
	@RequestMapping(value = "/applications/cmrHousehold", method = RequestMethod.GET)
	public List<SsapApplicationResource> getSsapApplicationsByCmrHouseHoldId(HttpServletRequest request) {
		BigDecimal cmrHouseHoldId= new BigDecimal(request.getParameter("cmrHouseoldId")==null?"0":request.getParameter("cmrHouseoldId").toString());
		AccountTransferSpanInfo atSpanInfo = null;
		AptcHistory aptcHistory = null;
		SsapApplicationResource res = null;
		List<SsapApplication> ssaps  = null;
		List<SsapApplicationResource> resp = new ArrayList<SsapApplicationResource>();
		
		try {
			ssaps = ssapApplicationRepository.findByCmrHouseoldId(cmrHouseHoldId);
			
			for(int i=0; i<ssaps.size(); i++) {
				res = new SsapApplicationResource();
				res.setCaseNumber(ssaps.get(i).getCaseNumber());
				res.setExchangeEligibilityStatus(ssaps.get(i).getExchangeEligibilityStatus()==null?"":ssaps.get(i).getExchangeEligibilityStatus().toString());
				res.setEligibilityStatus(ssaps.get(i).getEligibilityStatus()==null?"":ssaps.get(i).getEligibilityStatus().toString());
				res.setApplicationData(ssaps.get(i).getApplicationData());
				res.setApplicationStatus(ssaps.get(i).getApplicationStatus());
				res.setValidationStatus(ssaps.get(i).getValidationStatus());
				res.setCmrHouseoldId(ssaps.get(i).getCmrHouseoldId());
				res.setCoverageYear(ssaps.get(i).getCoverageYear());
				res.setCurrentPageId(ssaps.get(i).getCurrentPageId());
				res.setEsignDate(ssaps.get(i).getEsignDate());
				res.setEsignFirstName(ssaps.get(i).getEsignFirstName());
				res.setEsignLastName(ssaps.get(i).getEsignLastName());
				res.setFinancialAssistanceFlag(ssaps.get(i).getFinancialAssistanceFlag());
				res.setSource(ssaps.get(i).getSource());
				res.setCreatedBy(ssaps.get(i).getCreatedBy());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				
				res.setExemptHouseHold(ssaps.get(i).getExemptHousehold());
				res.setEligibilityReceivedDate(ssaps.get(i).getEligibilityReceivedDate());
				res.setEhbAmount(ssaps.get(i).getEhbAmount());
				res.setCsrLevel(ssaps.get(i).getCsrLevel());
				res.setNativeAmerican(ssaps.get(i).getNativeAmerican());
				res.setMaximumAPTC(ssaps.get(i).getMaximumAPTC());
				res.setElectedAPTC(ssaps.get(i).getElectedAPTC());
				res.setAvailableAPTC(ssaps.get(i).getAvailableAPTC());
				res.setMaximumStateSubsidy(ssaps.get(i).getMaximumStateSubsidy());
				res.setCreationTimestamp(ssaps.get(i).getCreationTimestamp());
				res.setApplicationType(ssaps.get(i).getApplicationType());
				res.setStartDate(ssaps.get(i).getStartDate());
				res.setLastUpdatedBy(ssaps.get(i).getLastUpdatedBy());
				//HIX-58446
				res.setLastUpdateTimestamp(ssaps.get(i).getLastUpdateTimestamp());
				res.setAllowEnrollment(ssaps.get(i).getAllowEnrollment());
				res.setSepQepDenied(ssaps.get(i).getSepQepDenied());
				
				res.setEligibilityResponseType(ssaps.get(i).getEligibilityResponseType());
				res.setExternalApplicationId(ssaps.get(i).getExternalApplicationId());
				
				Map<String,String> href = new HashMap<String,String>();
				href.put("href", GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + ssaps.get(i).getId());
				Map<String,Object> self = new HashMap<String,Object>();
				self.put("self", href);
				res.set_links(self);

				res.setApplicationId(ssaps.get(i).getId());
				
				SsapApplicant ssapapplicant = ssapApplicantRepository.findPTF(res.getApplicationId());
				if(ssapapplicant != null) {
				res.setPtfFirstName(ssapapplicant.getFirstName() != null ? ssapapplicant.getFirstName() : null);
				res.setPtfMiddleName(ssapapplicant.getMiddleName() != null ? ssapapplicant.getMiddleName() : null);
				res.setPtfLastName(ssapapplicant.getLastName() != null ? ssapapplicant.getLastName() : null);
				}
				
				String iexAtMesConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
				LocalDate localDate = null;
				if (iexAtMesConfig.equalsIgnoreCase(Boolean.TRUE.toString())) {
					atSpanInfo = atSpanProcessingRepository.findBySsapApplicationId(res.getApplicationId());
					if (atSpanInfo != null) {
						Timestamp dueDate = null;
						if(atSpanInfo.getDueDate() != null) {
						localDate = LocalDate.parse(new SimpleDateFormat(SSAPConstants.DATE_FORMAT).format(atSpanInfo.getDueDate()));
						dueDate = Timestamp.valueOf(localDate.atStartOfDay());
						}
						res.setDueDate(atSpanInfo.getDueDate() != null ? dueDate : null);
						res.setEligibilityStartDate(atSpanInfo.getEligibilityStartDate() != null ? atSpanInfo.getEligibilityStartDate() : null);
						res.setEligibilityEndDate(atSpanInfo.getEligibilityEndDate() != null ? atSpanInfo.getEligibilityEndDate() : null);
						res.setQueuedStatus(atSpanInfo.getQueueStatus() != null ? atSpanInfo.getQueueStatus().toString() : null);
					}
				} else {
					aptcHistory = aptcHistoryRepository.findAptcHistoryBySsapApplicationIdForCap(res.getApplicationId());
					if (aptcHistory != null) {
						res.setEligibilityStartDate(aptcHistory.getEligStartDate() != null ? aptcHistory.getEligStartDate() : null);
						res.setEligibilityEndDate(aptcHistory.getEligEndDate() != null ? aptcHistory.getEligEndDate() : null);
					}
				}
				
				res.setApplicationDentalStatus(null == ssaps.get(i).getApplicationDentalStatus()? "" : ssaps.get(i).getApplicationDentalStatus());

				resp.add(res);
				
			}
		}
		catch (Exception e) {
			LOGGER.error("Exception in getting application for cmrHouseHoldId:" + cmrHouseHoldId, e);
			return resp ;
		}
		return resp ;
	}
	
	@RequestMapping(value = "/applications/casenumber", method = RequestMethod.GET)
	public SsapApplicationResource getSsapApplicationsByCaseNumber(HttpServletRequest request) {
		BigDecimal cmrHouseHoldId= new BigDecimal(request.getParameter("cmrHouseoldId")==null?"0":request.getParameter("cmrHouseoldId").toString());
		String caseNumber= request.getParameter("caseNumber")==null?"0":request.getParameter("caseNumber").toString();
		SsapApplicationResource res = null;
		SsapApplication ssapApplication = null;
		
		try {
			
			ssapApplication = ssapApplicationRepository.findByCmrHouseoldIdAndCaseNumber(cmrHouseHoldId, caseNumber);
			
			if(ssapApplication == null) {
				return null;
			}
			
			res = new SsapApplicationResource();
			res.setCaseNumber(ssapApplication.getCaseNumber());
			res.setExchangeEligibilityStatus(ssapApplication.getExchangeEligibilityStatus()==null?"":ssapApplication.getExchangeEligibilityStatus().toString());
			res.setEligibilityStatus(ssapApplication.getEligibilityStatus()==null?"":ssapApplication.getEligibilityStatus().toString());
			res.setApplicationData(ssapApplication.getApplicationData());
			res.setApplicationStatus(ssapApplication.getApplicationStatus());
			res.setCmrHouseoldId(ssapApplication.getCmrHouseoldId());
			res.setCoverageYear(ssapApplication.getCoverageYear());
			res.setCurrentPageId(ssapApplication.getCurrentPageId());
			res.setEsignDate(ssapApplication.getEsignDate());
			res.setEsignFirstName(ssapApplication.getEsignFirstName());
			res.setEsignLastName(ssapApplication.getEsignLastName());
			res.setFinancialAssistanceFlag(ssapApplication.getFinancialAssistanceFlag());
			res.setSource(ssapApplication.getSource());
			res.setCreatedBy(ssapApplication.getCreatedBy());
			res.setAllowEnrollment(ssapApplication.getAllowEnrollment());
			res.setExemptHouseHold(ssapApplication.getExemptHousehold());
			
			res.setEligibilityReceivedDate(ssapApplication.getEligibilityReceivedDate());
			res.setEhbAmount(ssapApplication.getEhbAmount());
			res.setCsrLevel(ssapApplication.getCsrLevel());
			res.setNativeAmerican(ssapApplication.getNativeAmerican());
			res.setMaximumAPTC(ssapApplication.getMaximumAPTC());
			res.setElectedAPTC(ssapApplication.getElectedAPTC());
			res.setAvailableAPTC(ssapApplication.getAvailableAPTC());
			res.setMaximumStateSubsidy(ssapApplication.getMaximumStateSubsidy());
			res.setCreationTimestamp(ssapApplication.getCreationTimestamp());
			res.setApplicationType(ssapApplication.getApplicationType());
			res.setStartDate(ssapApplication.getStartDate());
			res.setLastUpdatedBy(ssapApplication.getLastUpdatedBy());
			res.setAllowEnrollment(ssapApplication.getAllowEnrollment());
			
			res.setEligibilityResponseType(ssapApplication.getEligibilityResponseType());
			res.setExternalApplicationId(ssapApplication.getExternalApplicationId());
			
			Map<String,String> href = new HashMap<String,String>();
			href.put("href", GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + ssapApplication.getId());
			Map<String,Object> self = new HashMap<String,Object>();
			self.put("self", href);
			res.set_links(self);

			res.setApplicationId(ssapApplication.getId());
		}
		catch (Exception e) {
			LOGGER.error("Exception in getting application for case number:" + caseNumber, e);
			return res;
		}
		return res;
	}
	
	@RequestMapping(value = "/applications/casenumberid", method = RequestMethod.GET)
	public SsapApplicationResource getSsapApplicationsByCaseNumberId(HttpServletRequest request) {
		String caseNumber= request.getParameter("caseNumber")==null?"0":request.getParameter("caseNumber").toString();
		SsapApplicationResource res = null;
		SsapApplication ssapApplication = null;
		
		try {
			
			ssapApplication = ssapApplicationRepository.findByCaseNumberId(caseNumber);
			
			if(ssapApplication == null) {
				return null;
			}
			
			res = new SsapApplicationResource();
			res.setCaseNumber(ssapApplication.getCaseNumber());
			res.setExchangeEligibilityStatus(ssapApplication.getExchangeEligibilityStatus()==null?"":ssapApplication.getExchangeEligibilityStatus().toString());
			res.setEligibilityStatus(ssapApplication.getEligibilityStatus()==null?"":ssapApplication.getEligibilityStatus().toString());
			res.setApplicationData(ssapApplication.getApplicationData());
			res.setApplicationStatus(ssapApplication.getApplicationStatus());
			res.setCmrHouseoldId(ssapApplication.getCmrHouseoldId());
			res.setCoverageYear(ssapApplication.getCoverageYear());
			res.setCurrentPageId(ssapApplication.getCurrentPageId());
			res.setEsignDate(ssapApplication.getEsignDate());
			res.setEsignFirstName(ssapApplication.getEsignFirstName());
			res.setEsignLastName(ssapApplication.getEsignLastName());
			res.setFinancialAssistanceFlag(ssapApplication.getFinancialAssistanceFlag());
			res.setSource(ssapApplication.getSource());
			res.setCreatedBy(ssapApplication.getCreatedBy());
			res.setAllowEnrollment(ssapApplication.getAllowEnrollment());
			res.setExemptHouseHold(ssapApplication.getExemptHousehold());
			
			res.setEligibilityReceivedDate(ssapApplication.getEligibilityReceivedDate());
			res.setEhbAmount(ssapApplication.getEhbAmount());
			res.setCsrLevel(ssapApplication.getCsrLevel());
			res.setNativeAmerican(ssapApplication.getNativeAmerican());
			res.setMaximumAPTC(ssapApplication.getMaximumAPTC());
			res.setElectedAPTC(ssapApplication.getElectedAPTC());
			res.setAvailableAPTC(ssapApplication.getAvailableAPTC());
			res.setMaximumStateSubsidy(ssapApplication.getMaximumStateSubsidy());
			res.setCreationTimestamp(ssapApplication.getCreationTimestamp());
			res.setApplicationType(ssapApplication.getApplicationType());
			res.setStartDate(ssapApplication.getStartDate());
			res.setLastUpdatedBy(ssapApplication.getLastUpdatedBy());
			res.setAllowEnrollment(ssapApplication.getAllowEnrollment());
			
			res.setEligibilityResponseType(ssapApplication.getEligibilityResponseType());
			res.setExternalApplicationId(ssapApplication.getExternalApplicationId());
			
			
			Map<String,String> href = new HashMap<String,String>();
			href.put("href", GhixEndPoints.SsapEndPoints.SSAP_APPLICATION_DATA_URL + ssapApplication.getId());
			Map<String,Object> self = new HashMap<String,Object>();
			self.put("self", href);
			res.set_links(self);

			res.setApplicationId(ssapApplication.getId());
		}
		catch (Exception e) {
			LOGGER.error("Exception in getting application for case number:" + caseNumber, e);
			return res;
		}
		return res;
	}
	
	/**
	 *  This is a referral callback API invoked by enrollment. This updates 
	 *  - application status to EN/PN depending on member health enrollment status.
	 *  - application dental status depending on dental enrollment status
	 *  - effective date of application based on coverage start date of health and or dental enrollment.
	 *   
	 * */
	@RequestMapping(value = "/updateSsapApplicationById", method = RequestMethod.POST)
	public SsapApplicationResponse updateSsapApplicationStatusById(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
		
		String applicationStatus = ssapApplicationRequest.getApplicationStatus() != null ? ssapApplicationRequest.getApplicationStatus() : null;
		String dentalApplicationStatus = ssapApplicationRequest.getApplicationDentalStatus() !=null ? ssapApplicationRequest.getApplicationDentalStatus() : null;
		long ssapApplicationId = ssapApplicationRequest.getSsapApplicationId();
		SsapApplicationResponse ssapApplicationResponse = new SsapApplicationResponse();
		boolean isAppUpdateByReInstatement = ssapApplicationRequest.isAppUpdateByReInstatement();
		
		LOGGER.info("updateSsapApplicationStatusById is called for ssapApplicationId: {} ",ssapApplicationId);
		LOGGER.info("Application dental status in IEX -"+ ssapApplicationRequest.getApplicationDentalStatus());
		
		int count = 0;
		int dentalCount = 0;
		try {
				SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
				
				boolean isOpenEnrollment = false;
				if(ssapApplication != null ){
					isOpenEnrollment = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear());
				}
				
				if(!ssapApplicationRequest.isEnrollmentCanceledOrTerminated()) {
					if(isAppUpdateByReInstatement) {
						List<String> appStatus = new ArrayList<String>();
						appStatus.add("EN");
						appStatus.add("PN");
						appStatus.add("ER");
						List<Long> ssapApplicationIdList = ssapApplicationRepository.getApplicatonIdForCMRIdandStatus(ssapApplication.getCmrHouseoldId(), ssapApplication.getCoverageYear(), appStatus, ssapApplicationId);
						if(ssapApplicationIdList != null && !ssapApplicationIdList.isEmpty()) {
							ssapApplicationResponse.setCount(0);
							ssapApplicationResponse.setErrorCode(ERROR_CODE_204);
							return ssapApplicationResponse;
						}
					}

				/**
				 * Update application status for health enrollments.
				 * */
				if (applicationStatus != null) {
					count = ssapService.updateSsapAppStatusOnMemberStatus(ssapApplicationRequest.getExchgIndivIdList(),applicationStatus, ssapApplicationId, new TSTimestamp());
					
					if(isOpenEnrollment && !isAppUpdateByReInstatement)
					{
						RenewalApplication renewalApplication = renewalApplicationService.findBySsapApplicationId(ssapApplicationId);
						RenewalStatus renewalStatus = null;
						if(renewalApplication != null) {
							renewalStatus = renewalApplicationService.getRenewalStatus(ssapApplicationRequest.getEnrlRenewalFlag(),renewalApplication.getHealthRenewalStatus());
						}
						
						if(renewalStatus != null)
						{
							renewalApplicantService.updateHealthRenewalStatus(ssapApplicationRequest.getExchgIndivIdList(),ssapApplicationId,renewalStatus);
							Integer toConsiderCount = renewalApplicantService.getToConsiderMemerCount(RenewalStatus.TO_CONSIDER, RenewalConstants.HEALTH, ssapApplicationId);
							if(toConsiderCount == 0){
								renewalApplicationService.updateRenewalStatus(RenewalConstants.HEALTH,ssapApplicationId,renewalStatus);
							}
						}
					}
	
					if (applicationStatus.equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode())) {
						ssapApplicationEventRepository.updateChangePlan(ssapApplicationId, "N");
					}
				}
				/**
				 * Update application status for dental enrollments.
				 * */
				if (dentalApplicationStatus != null) {
					LOGGER.info("Application dental status in Eligibility- "+ ssapApplicationRequest.getApplicationDentalStatus());
					dentalCount = ssapApplicationRepository.updateDentalApplicationStatusById(dentalApplicationStatus,ssapApplicationId, new Timestamp(new TSDate().getTime()));
					SsapApplication previousApplication = ssapApplicationRepository.findLatestEnPnSsapApplicationForCoverageYear(ssapApplication.getCmrHouseoldId(), ssapApplication.getCoverageYear());
					 
					if(null != previousApplication) {
						LOGGER.debug("Previous Application ID in EN/PN state" + previousApplication.getId());		 
						if (previousApplication.getApplicationDentalStatus() != null
								&& previousApplication.getId() != ssapApplication.getId()
								&& !previousApplication.getApplicationDentalStatus().equals(ApplicationStatus.CLOSED.getApplicationStatusCode())) {
							ssapApplicationRepository.updateDentalApplicationStatusById(
									ApplicationStatus.CLOSED.getApplicationStatusCode(), previousApplication.getId(),
									new Timestamp(new TSDate().getTime()));
						}
					} else {
						LOGGER.debug("Previous Application is null for " + ssapApplicationId);
					}
					if(isOpenEnrollment && !isAppUpdateByReInstatement){
						RenewalApplication renewalApplication = renewalApplicationService.findBySsapApplicationId(ssapApplicationId);
						RenewalStatus renewalStatus = null;
						if(renewalApplication != null) {
							renewalStatus = renewalApplicationService.getRenewalStatus(ssapApplicationRequest.getEnrlRenewalFlag(),renewalApplication.getDentalRenewalStatus());
						}
						if(renewalStatus != null)
						{
							renewalApplicantService.updateDentalRenewalStatus(ssapApplicationId,renewalStatus);
							renewalApplicationService.updateRenewalStatus(RenewalConstants.DENTAL,ssapApplicationId,renewalStatus);
						}
					}
				}
				
				/**
				 * Update effective date of health and or dental enrollment into application table.
				 * */
				if (ssapApplicationRequest != null && null != ssapApplicationRequest.getEffectiveDate()) {
					// date passed would be in MM/dd/yyyy
					Date effectiveDate = DateUtil.StringToDate(ssapApplicationRequest.getEffectiveDate(), "MM/dd/yyyy");
					LOGGER.info("Updating the effective date passed for ssap application id: {} to : {}",ssapApplicationId, effectiveDate);
					// update the date
					ssapApplicationRepository.updateApplicationEffectiveDateById(ssapApplicationId, effectiveDate);
				}
				
				/*
				 * Condition Added for Story HIX-62817 :: Reset the Overriden Coverage Start
				 * Date once the Consumer/CSR enrolls.
				 */
				// condition is updated for HIX-115408 
				// get the applicationStatus and dentalApplicationStatus from the Application id by making query to ssap_applications table
				// Only when application_status = EN and application_dental_status = EN of ssap_application table, 
				// set overwritten_coverage_date of ssap_application_events table to NULL
				SsapApplication ssapApplicationUpdated = ssapApplicationRepository.findOne(ssapApplicationId);
				if(null != ssapApplicationUpdated) {
					applicationStatus = ssapApplicationUpdated.getApplicationStatus() != null ? ssapApplicationUpdated.getApplicationStatus() : null;
					dentalApplicationStatus = ssapApplicationUpdated.getApplicationDentalStatus() !=null ? ssapApplicationUpdated.getApplicationDentalStatus() : null;	
				}
				if ((count == 1 || dentalCount == 1) && ((null!= applicationStatus && applicationStatus.equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode())) &&
						(null!= dentalApplicationStatus && dentalApplicationStatus.equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode())))) {
					ssapApplicationEventRepository.updateDatesForEvent(ssapApplicationId);
				}
				
				if(count == 0 && dentalCount != 0) {
					ssapApplicationResponse.setCount(dentalCount);
					ssapApplicationResponse.setErrorCode(SUCEES_CODE_200);
					return ssapApplicationResponse;
				}
			} else {

				String applicationType = ssapApplication.getApplicationType();
				String caseNumber = ssapApplication.getCaseNumber();
				boolean isAppCreatedInOEP = referralOEService.isDateInsideOEWindow(ssapApplication.getCreationTimestamp());
				String appStatus = ssapApplicationRequest.getApplicationStatus() != null ? ssapApplicationRequest.getApplicationStatus() : null;
				boolean isDentalOnlyUpdate = ssapApplicationRequest.isApplicationDentalStatusUpdate();
				BigDecimal userId = ssapApplicationRequest.getUserId();
				count = 0;

				if (applicationType.equalsIgnoreCase("OE") || applicationType.equalsIgnoreCase("SEP")) {
					LOGGER.debug("Disenrolling the application is complete");
					Boolean reEnroll = false;
					if (isOpenEnrollment && isAppCreatedInOEP) {
						reEnroll = true;
					}
					if (reEnroll) {
						LOGGER.debug("Resetting the app status to ER/EN by enrollmentDisenrollmentApiCall");
						if (!isDentalOnlyUpdate) {
							count = ssapService.updateSsapApplicationStatusByCaseNumber(appStatus, caseNumber, new TSTimestamp(), userId);
						} else {
							ssapService.updateDentalApplicationStatusByCaseNumber(caseNumber, new TSTimestamp(), userId);
						}
					}
				}
				ssapApplicationResponse.setCount(count);
				ssapApplicationResponse.setErrorCode(SUCEES_CODE_200);
				
			}
		}
		catch(Exception ex) {
			LOGGER.error("Error in updating the ssap application status for ssapApplicationId:" + ssapApplicationId, ex);
			count = 0;
			ssapApplicationResponse.setCount(count);
			ssapApplicationResponse.setErrorCode(ERROR_CODE_201);
			return ssapApplicationResponse;
		}
		
		ssapApplicationResponse.setCount(count);
		ssapApplicationResponse.setErrorCode(SUCEES_CODE_200);
		return ssapApplicationResponse;
		
	}
	
	
	@RequestMapping(value = "/createSsapApplicationEvent", method = RequestMethod.POST)
	public ResponseEntity<SsapApplicationEvent> insertSsapDocument(@RequestBody SsapApplicationEvent ssapApplicationEvent) {
		
		SsapApplicationEvent objSsapApplicationEvent;
		try{
			/** Perform Service - Save Operation */
			objSsapApplicationEvent = ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		}catch(Exception e){
			/** return error response */
			LOGGER.error("Error in /createSsapApplicationEvent>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"+e);
			return new ResponseEntity<SsapApplicationEvent>(new SsapApplicationEvent(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
		LOGGER.info("SSAPAPPLICATIONEVENT insert successful");
		/** Step 3 : Form Success Response */
		return new ResponseEntity<SsapApplicationEvent>(objSsapApplicationEvent, HttpStatus.OK);
	}
	
	@Transactional
    @RequestMapping(value = "/deleteSsapApplicationApplicant", method = RequestMethod.POST)
    public boolean deleteSsapApplicationApplicants(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
          try {
                SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationRequest.getSsapApplicationId());
                String ssapApplicationStatus = ssapApplication.getApplicationStatus();
                
                if(!("OP".equalsIgnoreCase(ssapApplicationStatus) || "ER".equalsIgnoreCase(ssapApplicationStatus) || "SU".equalsIgnoreCase(ssapApplicationStatus) )){
                      return false;
                }
                
                /*List<SsapApplicant> listSsapApplicant = ssapApplicantRepository.findBySsapApplicantGuid(ssapApplicationRequest.getApplicantGuidToRemove());
                SsapApplicant ssapApplicant = listSsapApplicant.get(0);
                ssapApplicant.setOnApplication("N");
                ssapApplicantRepository.saveAndFlush(ssapApplicant);*/
                
                ssapApplicantRepository.updateApplicantOnApplicationFlag("N",
                		ssapApplicationRequest.getSsapApplicationId(), 
                		ssapApplicationRequest.getApplicantGuidToRemove(), "N");
                //ssapApplication.setApplicationData(ssapApplicationRequest.getApplicationData());
                //ssapApplicationRepository.save(ssapApplication);
          } catch (Exception e) {
               LOGGER.error("Exception occurred in deleteSsapApplicationApplicants API: ",e);
                return false;
          }
          return true;

    }

	@RequestMapping(value = "/getmaxpersonid", method = RequestMethod.POST)
	public  Integer getMaxpersonId(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
		return ssapApplicantRepository.getMaxPersonId(ssapApplicationRequest.getCaseNumber());
	}
	
	@RequestMapping(value = "/syncApplicantDOB", method = RequestMethod.POST)
	public String syncAppicantDateOfBirth(@RequestBody String request) {
		JsonElement applicantId = null;
		JsonParser jsonParser = new JsonParser();
		Map<String,List<Long>>  result = null;
		try{
			JsonElement jsonObject =  jsonParser.parse(request);
			JsonArray applicantIds = jsonObject.getAsJsonObject().get("applicantIds").getAsJsonArray();
			Iterator<JsonElement> itr = applicantIds.iterator();
			List<Long> applicantList = new ArrayList<Long>();
			while(itr.hasNext()){
				applicantId = itr.next();
				if(applicantId !=null && !applicantId.isJsonNull()){
					applicantList.add(applicantId.getAsLong());
				}
			}
			result = ssapService.updateApplicantDateOfBirth(applicantList);
			if(result!= null){
				JsonElement resultJson =  platformGson.toJsonTree(result, new TypeToken<Map<String,List<Long>>>() {}.getType());
				return resultJson.getAsJsonObject().toString();
			}else{
				return "Failed to update date of Birth";
			}
		}catch(Exception ex){
			LOGGER.error("Exception occurred while updating date of Birth");
			return "Failed to update date of Birth";
		}
		
		
	}
	
	
	@RequestMapping(value = "/autoEnroll", method = RequestMethod.POST)
	public String automateEnrollment(@RequestBody String request) {
		JsonParser jsonParser = new JsonParser();
		try{
			JsonObject jsonObject =  jsonParser.parse(request).getAsJsonObject();
			Map<String,Set<String>> enrollee =  platformGson.fromJson(jsonObject.get("enrollee"),new TypeToken<Map<String, Set<String>>>() {}.getType());
			//long enrolledSsapApplicationId = (long) jsonObject.get("enrolledSsapApplicationId").getAsLong();
			long ssapApplicationId = (long) jsonObject.get("ssapApplicationId").getAsLong();
			Map<String, EnrollmentShopDTO> enrollmentIdMap = platformGson.fromJson(jsonObject.get("enrollmentIdMap"),new TypeToken<Map<String, EnrollmentShopDTO>>() {}.getType());
			Map<String,Set<String>> disenrollMap = platformGson.fromJson(jsonObject.get("disenrollMap"),new TypeToken<Map<String, Set<String>>>() {}.getType());
			return ssapAutoEnrollService.autoEnroll(ssapApplicationId, enrollmentIdMap, enrollee, disenrollMap);	
		}catch(Exception ex){
			LOGGER.error("Exception occurred while processing auto enrollment",ex);
		}
		return GhixConstants.RESPONSE_FAILURE;
	}
	
	
	
	@RequestMapping(value = "/updatePreferences", method = RequestMethod.POST)
	public String updateMailingAddress(@RequestBody SsapPreferencesDTO ssapPreferencesDTO) {
		String response = GhixConstants.RESPONSE_FAILURE;
		try{
			ssapService.updatePreferences(ssapPreferencesDTO);
			response = GhixConstants.RESPONSE_SUCCESS;
		}catch(Exception ex){
			LOGGER.error("Exception occurred while processing auto enrollment",ex);
		}
		return response;
	}
	
	@RequestMapping(value = "/updateRenewalStatus", method = RequestMethod.POST)
	public String updateApplicationRenewalStatus(@RequestBody SsapApplicationRequest ssapApplicationRequest) {
		String response = GhixConstants.RESPONSE_FAILURE;
		String caseNumber = ssapApplicationRequest.getCaseNumber();
		BigDecimal userId = ssapApplicationRequest.getUserId();
		try {
			ssapService.updateApplicationRenewalStatus(caseNumber, RenewalStatus.OTR, new Timestamp(new TSDate().getTime()), userId);
			response = GhixConstants.RESPONSE_SUCCESS;
		}
		catch(Exception ex) {
			LOGGER.error("Error in updating the ssap application renewal status for caseNumber:" + caseNumber, ex);
		}
		return response;
	}
	
	
	@RequestMapping(value = "/ssapIntegrationCallback", method = RequestMethod.POST)
	public String ssapIntegationCallback(@RequestBody String caseNumber) {
		String response = GhixConstants.RESPONSE_FAILURE;
		try{
			ssapService.processQleValidation(caseNumber);
			response = GhixConstants.RESPONSE_SUCCESS;
		}catch(Exception ex){
			LOGGER.error("Exception occurred while qle validation",ex);
		}
		return response;
	}
	
	/**
	 * 
	 * @param phone
	 * @param email
	 * @return
	 */
	@RequestMapping(value = "/validatehousehold", method = RequestMethod.GET)
	public HttpEntity<Integer> validateHouseholdByPhoneOrEmail(@RequestParam(value = "phone", required = false) @Pattern(regexp = "(^$|[0-9]{10})", message = "should be 10 digit number.") String phone, 
			@RequestParam(value = "email", required = false) @Email String email){
		List<Household> cmrHouseholdList = cmrHouseholdRepository.findHouseHoldbyPhoneOrEmail(phone, email);
		if(cmrHouseholdList != null && !cmrHouseholdList.isEmpty() && cmrHouseholdList.size() == 1){
			return new ResponseEntity<>(cmrHouseholdList.get(0).getId(), HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	/**
	 * 
	 * @param householdCaseID
	 * @param last4DigitSSN
	 * @return
	 */
	@RequestMapping(value = "/{householdCaseID}/verifyssn", method= RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Boolean> verifySSN(@PathVariable @NotNull @Min(1) final Integer householdCaseID, @RequestBody @NotNull @Pattern(regexp = "(^$|[0-9]{4})", message = "should be 4 digit number.") String last4DigitSSN)
	{
		long matchCount = ssapApplicantRepository.verifyIfSSNMatches(SSAPConstants.PRIMARY_APPLICANT_PERSON_ID_LONG, new BigDecimal(householdCaseID), last4DigitSSN);
		if(matchCount != 0){
			return new ResponseEntity<>(true, HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/fetchSepEventsDetails", method = RequestMethod.POST)
	public ResponseEntity<Map<String,String>> getSepWindowDetails(@RequestBody Map<String, String> inputData) {
		Map<String,String> response = new HashMap<String,String>();
		try{
			if(this.validInputRequest(inputData)){
				// add coverage year is remaining.
				Long coverageYear = Long.parseLong(inputData.get(INPUT_ENROLLMENT_YEAR_KEY));
				BigDecimal householdId = new BigDecimal((inputData.get(INPUT_HOUSEHOLD_ID_KEY)));
				List<SsapApplication> ssapApplications = ssapApplicationService.findByHHIdAndCoverageYear(householdId,coverageYear);
				if(ssapApplications != null && ssapApplications.size() > 0){
					this.fetchApplicationDetails(ssapApplications,response);	
					response.put(ERR_CODE_KEY, SUCEES_CODE_200);
				}
				else{
					response.put(ERR_CODE_KEY, ERROR_CODE_201);
					response.put(ERR_CODE_DESCRIPTION_KEY, ERROR_CODE_201_DESCRIPTION);
				}
			}
			else{
				response.put(ERR_CODE_KEY, ERROR_CODE_203);
				response.put(ERR_CODE_DESCRIPTION_KEY, ERROR_CODE_203_DESCRIPTION);
			}
			
		}catch(Exception ex){
			LOGGER.error("Exception occurred while retrieving sep windo details.",ex);
			response.put(ERR_CODE_KEY, ERROR_CODE_204);
			response.put(ERR_CODE_DESCRIPTION_KEY, ERROR_CODE_204_DESCRIPTION);
		}
		return new ResponseEntity<>(response, HttpStatus.OK);
	}

	private Map<String, String> fetchApplicationDetails(List<SsapApplication> ssapApplications,Map<String,String> response) {
		SsapApplication latestSsapApplication =  ssapApplications.get(0);
		
		response.put(OUTPUT_VALID_EVENTS_EXIST_KEY, this.doesValidLceEventExists(latestSsapApplication).toString());
		response.put(OUTPUT_HEALTH_ENROLLMENT_STATUS_KEY, this.determineAllowedEnrollment(latestSsapApplication));
		response.put(OUTPUT_SSAP_APPLICATION_ID_KEY, String.valueOf(latestSsapApplication.getId()));
		return response;
	}

	private Boolean doesValidLceEventExists(SsapApplication latestSsapApplication) {
		SsapApplicationEvent ssapApplicationEvent = ssapEventService.findSsapApplicationEventByApplicationId(latestSsapApplication.getId());
		if(ssapApplicationEvent != null && ssapApplicationEvent.getEnrollmentStartDate() != null && ssapApplicationEvent.getEnrollmentEndDate() != null){
			Timestamp ts = new Timestamp(new TSDate().getTime());
			if(ssapApplicationEvent.getEnrollmentEndDate().after(ts) && ssapApplicationEvent.getEnrollmentStartDate().before(ts)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	private String determineAllowedEnrollment(SsapApplication latestSsapApplication) {
		String allowedHealth = "";
		if(latestSsapApplication.getApplicationStatus().equals(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode())){
			allowedHealth = "NO_HEALTH";
		}
		else if(latestSsapApplication.getApplicationStatus().equals(ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode())
				|| latestSsapApplication.getApplicationStatus().equals(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode())){
			allowedHealth = "HEALTH";
		}
		else{
			allowedHealth = "NONE";
		}
		return allowedHealth;
	}
	
	private boolean validInputRequest(Map<String,String> inputData){
		if(inputData != null && 
				inputData.containsKey(INPUT_HOUSEHOLD_ID_KEY) && inputData.get(INPUT_HOUSEHOLD_ID_KEY) != null && 
				inputData.containsKey(INPUT_ENROLLMENT_YEAR_KEY) && inputData.get(INPUT_ENROLLMENT_YEAR_KEY)!= null ){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}

    @RequestMapping(value = "/multiHouseholdMembers", method= RequestMethod.POST)
    public @ResponseBody List<MultiHousehold> retrieveHouseholdsForIDs(@RequestBody List<Integer> householdIDs) {
		List<MultiHousehold> response = new ArrayList<>();
		LOGGER.info("retrieveHouseholdsForIDs::householdIDs = " + householdIDs.size());

        try {
            if(householdIDs != null && householdIDs.size() > 0) {
            	LOGGER.debug("retrieveHouseholdsForIDs::calling getCMRHouseholdApplicationMembers");
				response = ssapService.getCMRHouseholdApplicationMembers(householdIDs);
				LOGGER.debug("retrieveHouseholdsForIDs::back from getCMRHouseholdApplicationMembers");
            }
        } catch(Exception ex) {
			LOGGER.error("retrieveHouseholdsForIDs::exception getting CMR Household Members", ex);
        }

        LOGGER.info("retrieveHouseholdsForIDs::returning response = " + response.toString());
        return response;
    }
    
    /**
     * Jira Id: HIX-112607
     * Api to get Details For External Notices
     * @author Anoop
     * @since 21st March 2019
     * @return ExternalNoticesResponseDTO
     */
	@RequestMapping(value = "/getdetailsfornotices/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<ExternalNoticesResponseDTO> getDetailsForNotices(@PathVariable("ssapApplicationId") Long ssapApplicationId) {

		LOGGER.info(" started getDetailsForNotices Service ssapApplicationId " +ssapApplicationId );
		ExternalNoticesResponseDTO externalNoticesResponse = new ExternalNoticesResponseDTO();
		try {
			if (ssapApplicationId != null) {
				externalNoticesResponse = externalNoticesService.getSsapApplicationDetails(ssapApplicationId);
				externalNoticesResponse.setErrCode(ExternalNoticeConstants.ERROR_CODE_200);
				externalNoticesResponse.setStatus(ExternalNoticeConstants.SUCCESS);
				return new ResponseEntity<>(externalNoticesResponse, HttpStatus.OK);
			} else {
				LOGGER.error("SSAP Application id is null ::" + ssapApplicationId);
				return new ResponseEntity<>(externalNoticesResponse, HttpStatus.BAD_REQUEST);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred in getting the Details for the Notices API: ", ex);
			return new ResponseEntity<>(externalNoticesResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * Jira Id: HIX-115462 Api to get ssap appication status for give Application id
	 * 
	 * @author Anoop
	 * @since 20th June 2019
	 * @return String
	 */
	@RequestMapping(value = "/getApplicationStatusById/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getApplicationStatusById(@PathVariable("ssapApplicationId") Long ssapApplicationId) {

		LOGGER.info(" started getApplicationStatusById " + ssapApplicationId);
		try {
			String applicationStatus = null;
			if (ssapApplicationId != null) {
				applicationStatus = ssapService.getApplicationStatusById(ssapApplicationId);
				return new ResponseEntity<>(applicationStatus, HttpStatus.OK);
			} else {
				LOGGER.error("SSAP Application id is null ::" + ssapApplicationId);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred getApplicationStatusForId API: ", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	
	@RequestMapping(value = "/updateProjectedIncome/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> updateProjectedIncome(@PathVariable("ssapApplicationId") Long ssapApplicationId) {
		LOGGER.info(" started updateProjectedIncome " + ssapApplicationId);
		
		String applicationStatus = "Excecution Successful.";
		
		try {
			if (ssapApplicationId != null) {
				SsapApplication application = ssapApplicationRepository.findOne(ssapApplicationId);
				String applicationData = application.getApplicationData();
				try {
					List<SsapApplication> DMApp = ssapApplicationRepository.getAppsBySourceAndHousehold(application.getCmrHouseoldId(), "DM");
					if(DMApp == null || DMApp.size() <= 0) {
						LOGGER.error("DM Application id is null ::" + ssapApplicationId);
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
						
					List<GIWSPayload> payload = giWSPayloadRepository.findBySsapApplicationId(DMApp.get(0).getId(), "ERP-TRANSFERACCOUNT");
					if(payload == null || payload.size() <= 0) {
						LOGGER.error("payload is null  for app id ::" + DMApp.get(0).getId());
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					}
					final String requestBody = ReferralUtil.extractSoapBoady(payload.get(0).getRequestPayload());
					
					AccountTransferRequestPayloadType source = ReferralUtil.unmarshal(requestBody);
					SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(applicationData);
					boolean updatedIncome = ssapAssembler.updateIncomes(source, singleStreamlinedApplication);
					
					if(updatedIncome)
					{
						applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
						application.setApplicationData(applicationData);
						ssapApplicationRepository.save(application);
						applicationStatus = "Application data updated Successfully.";
					}
				} catch (Exception jsonException) {
					LOGGER.error("Error while updating the Application Data -> ProjectedIncome", jsonException);
					throw new GIRuntimeException(jsonException);
				}
				return new ResponseEntity<>(applicationStatus, HttpStatus.OK);
			} else {
				LOGGER.error("SSAP Application id is null ::" + ssapApplicationId);
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception ex) {
			LOGGER.error("Exception occurred updateProjectedIncome API: ", ex);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/updateMemberOrder/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> updateMemberOrder(@PathVariable("ssapApplicationId") Long ssapApplicationId) {
		String applicationStatus = "Excecution Successful.";
		SsapApplication application = ssapApplicationRepository.findById(ssapApplicationId);
		
		try {
			String applicationData = application.getApplicationData();
			
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(applicationData);
			if(singleStreamlinedApplication.getTaxHousehold() != null && singleStreamlinedApplication.getTaxHousehold().get(0) != null 
					&& singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember() != null && singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember().size() > 0) {
				Collections.sort(singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember(), new Comparator<HouseholdMember>() {
					@Override
					public int compare(HouseholdMember householdMember1, HouseholdMember householdMember2) {
						return householdMember1.getPersonId().compareTo(householdMember2.getPersonId());
					}
				});
			}

			applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
			application.setApplicationData(applicationData);
			ssapApplicationRepository.save(application);
			applicationStatus = "Application data updated Successfully.";
			return new ResponseEntity<>(applicationStatus, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Unable to read next JSON object" , e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value = "/updateTaxHousehold/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> updateTaxHousehold(@PathVariable("ssapApplicationId") Long ssapApplicationId, @RequestParam(value = "path", required = false) String absPathWithNameAndExt) {
		String applicationStatus = "Excecution Successful.";
		SsapApplication application = ssapApplicationRepository.findById(ssapApplicationId);
		String externalApplicationId = application.getExternalApplicationId();
		String record = getApplicationRecordFromFile(absPathWithNameAndExt,externalApplicationId);
		String wellFormedRecord = getWellFormedRecord(record);
		try {
			JsonReader jsonReader = mapper.newJsonReader(new InputStreamReader(new ByteArrayInputStream(wellFormedRecord.getBytes())));
			ApplicationJSONDTO dmJsonObj = mapper.fromJson(jsonReader, ApplicationJSONDTO.class);
			String applicationData = application.getApplicationData();
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(applicationData);
			SingleStreamlinedApplication singleStreamlinedApplicationObj = updateAppDataJson(singleStreamlinedApplication, dmJsonObj);

			applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplicationObj);
			application.setApplicationData(applicationData);
			ssapApplicationRepository.save(application);
			applicationStatus = "Application data updated Successfully.";
			return new ResponseEntity<>(applicationStatus, HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("Unable to read next JSON object" , e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/updateHouseholdLocation/{cmrHouseholdId}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> updateTaxHousehold(@PathVariable("cmrHouseholdId") Integer cmrHouseholdId, @RequestBody Location location) {

		try {
			ssapService.updateHouseholdLocation(location, cmrHouseholdId);
			return new ResponseEntity<>(cmrHouseholdId.toString(), HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@Deprecated
	@RequestMapping(value = "/updateApplicationAddressTwo/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> updateApplicationAddressTwo(@PathVariable("ssapApplicationId") Long ssapApplicationId, @RequestParam(value = "path", required = false) String absPathWithNameAndExt) {
		try {
			SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);

			if (ssapApplication != null) {
				LOGGER.debug("updateApplicationAddressTwo::cloning application with id: " + ssapApplicationId);
				Map<String, String> responseData = applicationCloneService.cloneSsap(ssapApplication, true, new AtomicBoolean(true), false);
				if (!responseData.containsKey(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID)) {
					LOGGER.error("updateApplicationAddressTwo::cloning did not return new application id");
					return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
				}

				closeNonENPNApplication(ssapApplication);

				String clonedAppId = responseData.get(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID);
                SsapApplication clonedApplication = ssapApplicationRepository.findOne(Long.valueOf(clonedAppId));
                clonedApplication.setEligibilityStatus(ssapApplication.getEligibilityStatus());
                clonedApplication.setExchangeEligibilityStatus(ssapApplication.getExchangeEligibilityStatus());
                clonedApplication.setValidationStatus(ssapApplication.getValidationStatus());
                clonedApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
                clonedApplication.setMaximumAPTC(ssapApplication.getMaximumAPTC());
                ssapApplicationRepository.save(clonedApplication);
                List<SsapApplicant> originalApplicants = ssapApplicantRepository.findBySsapApplicationId(ssapApplication.getId());
                for (SsapApplicant applicant : originalApplicants)
                {
                	SsapApplicant clonedSsapApplicant = ssapApplicantRepository.findBySsapApplicantGuidAndSsapApplicationId(applicant.getApplicantGuid(), Long.parseLong(clonedAppId));
                	clonedSsapApplicant.setEligibilityStatus(applicant.getEligibilityStatus());
                	ssapApplicantRepository.save(clonedSsapApplicant);
                }
				LOGGER.debug("updateApplicationAddressTwo::updating address two for cloned app id: " + clonedAppId);
				if(updateAddressTwo(Long.valueOf(clonedAppId), absPathWithNameAndExt).equals("FAILURE")) {
                    LOGGER.error("updateApplicationAddressTwo::failed to update address two");
                    return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
                }

				LOGGER.debug("updateApplicationAddressTwo::calling AT Automation API edit application for cloned app id: " + clonedAppId);
				lceEditApplicationService.editApplicationProcess(Long.valueOf(clonedAppId));

				return new ResponseEntity<>("Successfully cloned and updated with new id " + clonedAppId, HttpStatus.OK);
			} else {
				LOGGER.error("updateApplicationAddressTwo::error no application found for ssap app id: " + ssapApplicationId);
				return new ResponseEntity<>("No application found for ssap app id", HttpStatus.INTERNAL_SERVER_ERROR);
			}
		} catch (Exception e) {
			LOGGER.error("updateApplicationAddressTwo::error processing update address two for app id: " + ssapApplicationId, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	public void closeNonENPNApplication(SsapApplication ssapApplication) {
        if (!nonCloseablestatuses.contains(ssapApplication.getApplicationStatus())) {
            if (ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationStatus())) {
                if (ssapApplication.getApplicationDentalStatus() != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationDentalStatus())) {
                    //do nothing
                } else {
                    lceEditApplicationService.closeApplications(ssapApplication);
                }
            } else {
                lceEditApplicationService.closeApplications(ssapApplication);
            }
            LOGGER.info("closeNonENPNApplications::Updating existing application, status is now closed");
        }
    }

	private SingleStreamlinedApplication updateAppDataJson(SingleStreamlinedApplication singleStreamlinedApplication,ApplicationJSONDTO dmJsonObj) {
		Result result = dmJsonObj.getApplication().entrySet().iterator().next().getValue().getResult();
		Map<String, AttestationsMember> members = result.getAttestations().getMembers();
		TaxHousehold taxHousehold = singleStreamlinedApplication.getTaxHousehold().get(0);
		setPersonTrackingNumberWhenNull(taxHousehold, result);
		Map<String, Set<Integer>> claimerIds = getClaimerIds(singleStreamlinedApplication,result.getComputed().getMembers(), result.getAttestations());

		for(Map.Entry<String, AttestationsMember>  memberEntry : members.entrySet())
		{
			AttestationsMember member = memberEntry.getValue();
			Member computedMember = result.getComputed().getMembers().get(memberEntry.getKey());
			String personTrackingNumber = computedMember.getPersonTrackingNumber();
			if(StringUtils.isEmpty(personTrackingNumber))
			{
				// If persontrackingNumber is not found then we can not get the corresponding HouseholdMember to update.
				//Do we need to search and update?
				continue; 
			}
			HouseholdMember householdMember = getHouseholdMember(singleStreamlinedApplication, personTrackingNumber);
			boolean dmTaxFilerIndicator = (member.getFamily().getTaxFilerIndicator() != null ? member.getFamily().getTaxFilerIndicator() : false);
			boolean dmTaxDependentIndicator = (member.getFamily().getTaxDependentIndicator() != null ? member.getFamily().getTaxDependentIndicator() : false);
			
			if(householdMember.getTaxHouseholdComposition() == null)
			{
				TaxHouseholdComposition taxHouseholdComposition = new TaxHouseholdComposition();
				householdMember.setTaxHouseholdComposition(taxHouseholdComposition);
			}
			
			householdMember.getTaxHouseholdComposition().setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
			householdMember.getTaxHouseholdComposition().setTaxRelationship(TaxRelationship.UNSPECIFIED);
			householdMember.getTaxHouseholdComposition().getClaimerIds().clear();
			
			
			if (!dmTaxFilerIndicator)
			{
				householdMember.getTaxHouseholdComposition().setTaxFilingStatus(TaxFilingStatus.UNSPECIFIED);
				if(dmTaxDependentIndicator)
				{
					householdMember.getTaxHouseholdComposition().setTaxRelationship(TaxRelationship.DEPENDENT);
					if(claimerIds.get(memberEntry.getKey()) != null) {
					householdMember.getTaxHouseholdComposition().getClaimerIds().addAll(claimerIds.get(memberEntry.getKey()));
					}
				}
			}else {
				if (!dmTaxDependentIndicator)
				{
					TaxFilingStatus taxFilingStatus = TaxFilingStatus.UNSPECIFIED;
					if(member.getFamily().getTaxReturnFilingStatusType() != null)
					{
						taxFilingStatus=getTaxFilingStatusMap(member.getFamily().getTaxReturnFilingStatusType());
					}
					householdMember.getTaxHouseholdComposition().setTaxFilingStatus(taxFilingStatus);
					householdMember.getTaxHouseholdComposition().setTaxRelationship(TaxRelationship.FILER);
				}
			}

			boolean longTermCare = false;

			if(member.getNonMagi() != null && member.getNonMagi().getLongTermCareIndicator() != null){
				longTermCare = member.getNonMagi().getLongTermCareIndicator();
			}

			householdMember.setLongTermCare(longTermCare);
			householdMember.setLivingArrangementIndicator(false);
			if(householdMember.getSpecialCircumstances() == null)
			{
				SpecialCircumstances specialCircumstances = new SpecialCircumstances();
				householdMember.setSpecialCircumstances(specialCircumstances);
			}
			if("FEMALE".equalsIgnoreCase(member.getDemographic().getSex())) 
			{
				boolean pregnantIndicator = (member.getFamily().getPregnancyIndicator() != null ? member.getFamily().getPregnancyIndicator() : false);
				householdMember.getSpecialCircumstances().setPregnantIndicator(pregnantIndicator);
				householdMember.getSpecialCircumstances().setNumberBabiesExpectedInPregnancy(0);
				if(null != member.getFamily().getBabyDueQuantity() && NumberUtils.isNumber(member.getFamily().getBabyDueQuantity().toString())) {
					householdMember.getSpecialCircumstances().setNumberBabiesExpectedInPregnancy(Integer.valueOf(member.getFamily().getBabyDueQuantity().toString()));
				}
			}
			householdMember.setHasESI("YES".equalsIgnoreCase(computedMember.getEscMecStatus()) ? true : false);
			householdMember.getHealthCoverage().getCurrentOtherInsurance().setHasNonESI("YES".equalsIgnoreCase(computedMember.getNonEscMecStatus()) ? true : false);
			int addressId = getExistingAddress(taxHousehold.getOtherAddresses(), member.getDemographic().getHomeAddress());
			if(addressId != -1) {
				householdMember.setAddressId(addressId);
			} else if(member.getDemographic() != null && member.getDemographic().getHomeAddress() != null) {
				HomeAddress homeAddress = member.getDemographic().getHomeAddress();
				Address homeAddressSSAP = new Address();
				homeAddressSSAP.setStreetAddress1(homeAddress.getStreetName1());
				homeAddressSSAP.setCity(homeAddress.getCityName());
				homeAddressSSAP.setCountyCode(homeAddress.getCountyFipsCode());
				homeAddressSSAP.setPostalCode(homeAddress.getZipCode());
				homeAddressSSAP.setState(homeAddress.getStateCode());
				homeAddressSSAP.setAddressId(taxHousehold.getOtherAddresses().size()+1);
				householdMember.setAddressId(homeAddressSSAP.getAddressId());
				taxHousehold.getOtherAddresses().add(homeAddressSSAP);
			}
		}
		if(singleStreamlinedApplication.getTaxHousehold() != null && singleStreamlinedApplication.getTaxHousehold().size() >=0 && singleStreamlinedApplication.getPrimaryTaxFilerPersonId() != null){
			singleStreamlinedApplication.getTaxHousehold().get(0).setPrimaryTaxFilerPersonId(singleStreamlinedApplication.getPrimaryTaxFilerPersonId());
		}
		return singleStreamlinedApplication;
	}

	private void setPersonTrackingNumberWhenNull(TaxHousehold taxHousehold, Result result) {
		Map<String, AttestationsMember> members = result.getAttestations().getMembers();
		for(Map.Entry<String, AttestationsMember>  memberEntry : members.entrySet())
		{
			AttestationsMember member = memberEntry.getValue();
			Member computedMember = result.getComputed().getMembers().get(memberEntry.getKey());
			String personTrackingNumber = computedMember.getPersonTrackingNumber();
			if(StringUtils.isEmpty(personTrackingNumber))
			{
				List<HouseholdMember> ssapMembers = taxHousehold.getHouseholdMember();
				for( HouseholdMember ssapMember :ssapMembers)
				{
					if(ssapMember.getName().getFirstName().equalsIgnoreCase(member.getDemographic().getName().getFirstName())
							&& ssapMember.getName().getLastName().equalsIgnoreCase(member.getDemographic().getName().getLastName())
							&& ssapMember.getGender().equalsIgnoreCase(member.getDemographic().getSex()))
					{
						computedMember.setPersonTrackingNumber(ssapMember.getExternalId());
					}
				}
			}
		}


	}

	private int getExistingAddress(List<Address> otherAddresses, HomeAddress homeAddress) {
		boolean matchFound = true;
		if(otherAddresses != null && otherAddresses.size() > 0 && homeAddress != null) {
			for(Address address: otherAddresses) {
				matchFound = matchFound && org.apache.commons.lang3.StringUtils.equals(address.getStreetAddress1(),homeAddress.getStreetName1()) ? true : false;
				matchFound = matchFound && org.apache.commons.lang3.StringUtils.equals(address.getCity(), homeAddress.getCityName()) ? true : false;
				matchFound = matchFound && org.apache.commons.lang3.StringUtils.equals(address.getCountyCode(),homeAddress.getCountyFipsCode()) ? true : false;
				matchFound = matchFound && org.apache.commons.lang3.StringUtils.equals(address.getPostalCode(),homeAddress.getZipCode()) ? true : false;
				matchFound = matchFound && org.apache.commons.lang3.StringUtils.equals(address.getState(),homeAddress.getStateCode()) ? true : false;
				if(matchFound) {
					return address.getAddressId();
				}
			}
		}
		return -1;
	}

	private TaxFilingStatus getTaxFilingStatusMap(String taxReturnFilingStatusType) {
		TaxFilingStatus taxFilingStatus = null;
		switch(taxReturnFilingStatusType) {
			case "SINGLE_FILER":
				taxFilingStatus = TaxFilingStatus.SINGLE;
				break;
			case "MARRIED_FILING_JOINTLY":
				taxFilingStatus = TaxFilingStatus.FILING_JOINTLY;
				break;
			case "MARRIED_FILING_SEPARATELY":
				taxFilingStatus = TaxFilingStatus.FILING_SEPARATELY;
				break;
			case "HEAD_OF_HOUSEHOLD":
				taxFilingStatus = TaxFilingStatus.HEAD_OF_HOUSEHOLD;
				break;
			default:
				taxFilingStatus = TaxFilingStatus.UNSPECIFIED;
				break;
		}
		return taxFilingStatus;
	}

	private Map<String, Set<Integer>> getClaimerIds(SingleStreamlinedApplication singleStreamlinedApplication,
													Map<String, Member> computedMembers,Attestations attestations)
	{	
		Map<String, Set<Integer>> claimerIdsMap = new HashMap<>();
		Map<String, AttestationsMember> attestedMembers = attestations.getMembers();
		boolean hasJointlyFilingMembers = false;
		for(Map.Entry<String, AttestationsMember>  memberEntry : attestedMembers.entrySet()){
			if("MARRIED_FILING_JOINTLY".equals(memberEntry.getValue().getFamily().getTaxReturnFilingStatusType())){
				hasJointlyFilingMembers = true;
				break;
			}
		}
		if (hasJointlyFilingMembers) {
			Set<Integer> claimerIds = new HashSet<Integer>();
			Set<String> dependents = new HashSet<String>();
			for(Map.Entry<String, AttestationsMember>  memberEntry : attestedMembers.entrySet())
			{
				Member computedMember = computedMembers.get(memberEntry.getKey());
				String personTrackingNumber = computedMember.getPersonTrackingNumber();
				HouseholdMember householdMember = getHouseholdMember(singleStreamlinedApplication, personTrackingNumber);
				boolean dmTaxFilerIndicator = memberEntry.getValue().getFamily().getTaxFilerIndicator();
				if("MARRIED_FILING_JOINTLY".equals(memberEntry.getValue().getFamily().getTaxReturnFilingStatusType()) && dmTaxFilerIndicator)
				{
					claimerIds.add(householdMember.getPersonId());
				}
				boolean dmTaxDependentIndicator = (memberEntry.getValue().getFamily().getTaxDependentIndicator() != null ? memberEntry.getValue().getFamily().getTaxDependentIndicator() :false);;
				if(dmTaxDependentIndicator) {
					dependents.add(memberEntry.getKey());
				}
			}
			if(dependents.size() > 0) {
				for(String dependentId : dependents) {
					claimerIdsMap.put(dependentId, claimerIds);
				}
			}
		} else if(attestations.getHousehold() != null && attestations.getHousehold().getTaxRelationships() != null) {
			ArrayList<ArrayList<String>> taxRelationships = attestations.getHousehold().getTaxRelationships();

			for(ArrayList<String> taxRelationship : taxRelationships){
				String claimerId = taxRelationship.get(0);
				String dependentId = taxRelationship.get(2);

				if(claimerId.equals(dependentId)){
					continue;
				}

				Set<Integer> claimersSet = claimerIdsMap.computeIfAbsent(dependentId, k -> new HashSet<>());
	
				Member computedMember = computedMembers.get(claimerId);
				String personTrackingNumber = computedMember.getPersonTrackingNumber();
				HouseholdMember householdMember = getHouseholdMember(singleStreamlinedApplication, personTrackingNumber);
				claimersSet.add(householdMember.getPersonId());
			}
		}

		return claimerIdsMap;
	}

	private HouseholdMember getHouseholdMember(SingleStreamlinedApplication singleStreamlinedApplication,String personTrackingNumber) {
		List<HouseholdMember> members = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		for( HouseholdMember member :members)
		{
			if(personTrackingNumber.equals(member.getExternalId()))
			{
				return member;
			}
		}
		return null;
	}

	private String getWellFormedRecord(String result) {
		if (!StringUtils.isEmpty(result)) {
			
			if(StringUtils.startsWithIgnoreCase(result, APPLICATION_START)) {
				result = result.substring(APPLICATION_START.length(), result.length()-1);
			}
			else if (StringUtils.endsWithIgnoreCase(result, ",")) {
				result = result.substring(0, result.length() - 1);
			}
			result = JSON_START + result + JSON_END;
		}
		return result;
	}

	private String getApplicationRecordFromFile(String absPathWithNameAndExt, String externalApplicationId) {
		String lineStart = "\""+externalApplicationId+"\"";
		try (Stream<String> stream = Files.lines(Paths.get(absPathWithNameAndExt))) {
			String line = stream.filter(x -> x.startsWith(lineStart))
            .findFirst()
            .orElse(null);
			return line;
		} catch (IOException e) {
			LOGGER.error("Unable in getApplicationRecordFromFile" , e);
		}
		return null;
	}

	@RequestMapping(value = "/updateAddressTwo/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> updateAddressTwo(@PathVariable("ssapApplicationId") Long ssapApplicationId, @RequestParam(value = "path", required = false) String absPathWithNameAndExt) {
		SsapApplication application = ssapApplicationRepository.findById(ssapApplicationId);
		String externalApplicationId = application.getExternalApplicationId();
		String record = getApplicationRecordFromFile(absPathWithNameAndExt,externalApplicationId);
		String wellFormedRecord = getWellFormedRecord(record);
		try {
			JsonReader jsonReader = mapper.newJsonReader(new InputStreamReader(new ByteArrayInputStream(wellFormedRecord.getBytes())));
			ApplicationJSONDTO dmJsonObj = mapper.fromJson(jsonReader, ApplicationJSONDTO.class);

			updateAddress(dmJsonObj, application);
			return new ResponseEntity<>("SUCCESS", HttpStatus.OK);
		} catch (Exception e) {
			LOGGER.error("updateAddressTwo::error processing update address two for app id: " + ssapApplicationId, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	private void updateAddress(ApplicationJSONDTO dmJsonObj, SsapApplication application) {
		String applicationData = application.getApplicationData();
		SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(applicationData);
		Result result = dmJsonObj.getApplication().entrySet().iterator().next().getValue().getResult();
		Map<String, AttestationsMember> members = result.getAttestations().getMembers();
		TaxHousehold taxHousehold = singleStreamlinedApplication.getTaxHousehold().get(0);
		setPersonTrackingNumberWhenNull(taxHousehold, result);

		boolean applicationChanged = false;
		List<Location> locations = new ArrayList<Location>();

		for(Map.Entry<String, AttestationsMember>  memberEntry : members.entrySet())
		{
			AttestationsMember member = memberEntry.getValue();
			Member computedMember = result.getComputed().getMembers().get(memberEntry.getKey());
			String personTrackingNumber = computedMember.getPersonTrackingNumber();
			if(StringUtils.isEmpty(personTrackingNumber)){
				continue;
			}

			SsapApplicant applicant = ssapApplicantRepository.findByExternalApplicantIdAndSsapApplicationId(personTrackingNumber,application.getId());
			if(member.getDemographic().getHomeAddress() != null && !StringUtils.isEmpty(member.getDemographic().getHomeAddress().getStreetName2())) {
				Location homeLocation = iLocationRepository.findOne(applicant.getOtherLocationId().intValue());
				if(homeLocation != null && StringUtils.isEmpty(homeLocation.getAddress2())) {
					homeLocation.setAddress2(member.getDemographic().getHomeAddress().getStreetName2());
					locations.add(homeLocation);
				}
				applicationChanged = true;
			}

			if(member.getDemographic().getMailingAddress() != null && !StringUtils.isEmpty(member.getDemographic().getMailingAddress().getStreetName2())) {
				Location mailLocation = iLocationRepository.findOne(applicant.getMailiingLocationId().intValue());
				if(mailLocation != null && StringUtils.isEmpty(mailLocation.getAddress2())) {
					mailLocation.setAddress2(member.getDemographic().getMailingAddress().getStreetName2());
					locations.add(mailLocation);
				}
				applicationChanged = true;
			}
			HouseholdMember householdMember = getHouseholdMember(singleStreamlinedApplication, personTrackingNumber);
			updateExistingAddress(taxHousehold.getOtherAddresses(), member.getDemographic(), householdMember);
		}

		if(applicationChanged) {
			List<HouseholdMember> householdMembers = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
			HouseholdMember primaryHouseholdMember = null;
			for (HouseholdMember householdMember : householdMembers) {
				if (householdMember.getPersonId() == 1) {
					primaryHouseholdMember = householdMember;
				}
			}

			if(locations.size() > 0) {
				iLocationRepository.save(locations);
			}
			applicationData = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
			application.setApplicationData(applicationData);

			if (primaryHouseholdMember != null && primaryHouseholdMember.getHouseholdContact() != null
					&& primaryHouseholdMember.getHouseholdContact().getHomeAddress() != null
					&& primaryHouseholdMember.getHouseholdContact().getHomeAddress().getStreetAddress2() != null
					&& !primaryHouseholdMember.getHouseholdContact().getHomeAddress().getStreetAddress2().isEmpty()) {
				Household household = cmrHouseholdRepository.findOne(application.getCmrHouseoldId().intValue());
				String streetAddress2 = primaryHouseholdMember.getHouseholdContact().getHomeAddress().getStreetAddress2();

				Location householdLocation = household.getLocation();
				if(householdLocation != null && primaryHouseholdMember.getHouseholdContact().getHomeAddress().getStreetAddress1().equalsIgnoreCase(householdLocation.getAddress1())
						&& primaryHouseholdMember.getHouseholdContact().getHomeAddress().getCity().equalsIgnoreCase(householdLocation.getCity())
						&& primaryHouseholdMember.getHouseholdContact().getHomeAddress().getPostalCode().equalsIgnoreCase(householdLocation.getZip())
						&& !streetAddress2.equalsIgnoreCase(householdLocation.getAddress2())) {
					householdLocation.setAddress2(streetAddress2);
					household.setLocation(householdLocation);
				}

				Location prefContactLocation = household.getPrefContactLocation();
				if(prefContactLocation != null && primaryHouseholdMember.getHouseholdContact().getHomeAddress().getStreetAddress1().equalsIgnoreCase(prefContactLocation.getAddress1())
						&& primaryHouseholdMember.getHouseholdContact().getHomeAddress().getCity().equalsIgnoreCase(prefContactLocation.getCity())
						&& primaryHouseholdMember.getHouseholdContact().getHomeAddress().getPostalCode().equalsIgnoreCase(prefContactLocation.getZip())
						&& !streetAddress2.equalsIgnoreCase(prefContactLocation.getAddress2())) {
					prefContactLocation.setAddress2(streetAddress2);
					household.setPrefContactLocation(prefContactLocation);
				}

				cmrHouseholdRepository.save(household);
			}

			ssapApplicationRepository.save(application);
		}
	}

	private void updateExistingAddress(List<Address> otherAddresses, Demographic demographic, HouseholdMember householdMember) {
		HomeAddress homeAddress = demographic.getHomeAddress();
		MailingAddress mailingAddress = demographic.getMailingAddress();
		for(Address address: otherAddresses) {
			if(StringUtils.isEmpty(address.getStreetAddress2()) && address.getStreetAddress1().equalsIgnoreCase(homeAddress.getStreetName1())
				&& address.getCity().equalsIgnoreCase(homeAddress.getCityName())
				&& address.getPostalCode().equalsIgnoreCase(homeAddress.getZipCode())
				) {

				address.setStreetAddress2(homeAddress.getStreetName2());
			}
		}
		if(homeAddress != null && householdMember.getHouseholdContact() != null && householdMember.getHouseholdContact().getHomeAddress() != null) {
			if(householdMember.getHouseholdContact().getHomeAddress().getStreetAddress1().equalsIgnoreCase(homeAddress.getStreetName1())
				&& householdMember.getHouseholdContact().getHomeAddress().getCity().equalsIgnoreCase(homeAddress.getCityName())
				&& householdMember.getHouseholdContact().getHomeAddress().getPostalCode().equalsIgnoreCase(homeAddress.getZipCode())
				) {
				householdMember.getHouseholdContact().getHomeAddress().setStreetAddress2(homeAddress.getStreetName2());
			}
		}
		if(mailingAddress != null && householdMember.getHouseholdContact() != null && householdMember.getHouseholdContact().getMailingAddress() != null) {
			if(householdMember.getHouseholdContact().getMailingAddress().getStreetAddress1().equalsIgnoreCase(mailingAddress.getStreetName1())
				&& householdMember.getHouseholdContact().getMailingAddress().getCity().equalsIgnoreCase(mailingAddress.getCityName())
				&& householdMember.getHouseholdContact().getMailingAddress().getPostalCode().equalsIgnoreCase(mailingAddress.getZipCode())
				) {
				householdMember.getHouseholdContact().getMailingAddress().setStreetAddress2(mailingAddress.getStreetName2());
			}
		}

	}

	@RequestMapping(value = "/cleanupverifications/{ssapApplicationId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> cleanupApplicationVerifications(@PathVariable("ssapApplicationId") Long ssapApplicationId) {
		try {
			if (ssapApplicationId != null && ssapApplicationId > 0L) {
				boolean cleanupSuccessful = ssapService.cleanupApplicationApplicantVerifications(ssapApplicationId);
				return new ResponseEntity<>(cleanupSuccessful, cleanupSuccessful ? HttpStatus.OK : HttpStatus.INTERNAL_SERVER_ERROR);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error cleaning up application {}", ssapApplicationId, e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/applicableVerifications/{ssapApplicantId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<String>> applicableVerifications(@PathVariable("ssapApplicantId") Long ssapApplicantId) {
		List<String> applicableVerifications = new ArrayList<>();
		try {
			if (ssapApplicantId != null && ssapApplicantId > 0L) {
				applicableVerifications = ssapService.getApplicableVerifications(ssapApplicantId);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error retrieving verifications for applicant id: " + ssapApplicantId, e);
			return new ResponseEntity<>(applicableVerifications, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(applicableVerifications, HttpStatus.OK);
	}

	@RequestMapping(value = "/householdMember/{ssapApplicantId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<HouseholdMember> householdMemberFromSSAPApplicant(@PathVariable("ssapApplicantId") Long ssapApplicantId) {
		HouseholdMember householdMember = null;
		try {
			if (ssapApplicantId != null && ssapApplicantId > 0L) {
				SsapApplicant applicant = ssapApplicantRepository.findOne(ssapApplicantId);
				SsapApplication application = applicant.getSsapApplication();
				householdMember = ssapService.getHouseholdMemberForApplicant(application, applicant);
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error retrieving household member for applicant id", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(householdMember, HttpStatus.OK);
	}

	@RequestMapping(value = "/applicant/verified/{ssapApplicantId}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<Boolean> applicantVerified(@PathVariable("ssapApplicantId") Long ssapApplicantId) {
		boolean verified = false;
		try {
			if (ssapApplicantId != null && ssapApplicantId > 0L) {
				SsapApplicant applicant = ssapApplicantRepository.findOne(ssapApplicantId);
				if (applicant != null) {
					SsapApplication application = applicant.getSsapApplication();
					ResponseEntity<HouseholdMember> response = householdMemberFromSSAPApplicant(ssapApplicantId);
					if (application != null && response != null && response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
						verified = ssapVerificationService.isApplicantVerified(application.getFinancialAssistanceFlag(), applicant, response.getBody());
						return new ResponseEntity<>(verified, HttpStatus.OK);
					}
				}
			} else {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		} catch (Exception e) {
			LOGGER.error("error retrieving household member for applicant id", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	private enum AutomateApplicationStages
	{
		CLONEONLY(1),
		CLONE_N_ELIGIBILITY(2),
		CLONE_ELIGIBILITY_AUTOMATE(3);
		 private final int value;
		 
		 private AutomateApplicationStages(int value) {
		        this.value = value;
		    }

		    public int getValue() {
		        return value;
		    }
	}
	/*
	 * create a request object
	 * having ssapApplicationID
	 * and ENUM having 3 
	 * 
	 * create responseObejct
	 * having clonedd ApplicationID
	 * having boolean isCloned
	 * boolean ranEligibility
	 * boolean ranAutomation
	 * String status
	 */
	// -- clone / runelig / automation //CLONEONLY/CLONE_N_ELIGIBILITY / CLONE_ELIGIBILITY_AUTOMATE
	@RequestMapping(value = "/automateApplication/{ssapApplicationId}/{stage}", method = RequestMethod.GET)
	@ResponseBody
	public AutomateApplicationResponse automateApplication(@PathVariable("ssapApplicationId") Long ssapApplicationId, @PathVariable("stage") String stage) {
		LOGGER.info("automateApplication is called for ssapApplicationId: {} ",ssapApplicationId);
		AutomateApplicationResponse response = new AutomateApplicationResponse();
		AutomateApplicationStages applicationStage = null;
		try {
			applicationStage = AutomateApplicationStages.valueOf(stage);
			LOGGER.debug("automateApplication::applicationStage {} ", applicationStage.getValue());
		}
		catch (Exception e){
			LOGGER.error("Invalid Stage specified {}. Stage can either be CLONEONLY or CLONE_N_ELIGIBILITY or CLONE_ELIGIBILITY_AUTOMATE",stage);
			response.setMessage("Invalid Stage specified. Stage can either be CLONEONLY or CLONE_N_ELIGIBILITY or CLONE_ELIGIBILITY_AUTOMATE");
			response.setStatus("FAILED");
			return response;
		}
		if (applicationStage == null)
		{
			response.setMessage("Unable to instantiate stage from " + stage + ". Stage can either be CLONEONLY or CLONE_N_ELIGIBILITY or CLONE_ELIGIBILITY_AUTOMATE");
			response.setStatus("FAILED");			
			return response;
		}
		SsapApplication ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
		String clonedAppId = "";
		if (ssapApplication != null) {
			 if (allowedStatusesForClone.contains(ssapApplication.getApplicationStatus())) {
				LOGGER.debug("automateApplication::cloning application with id {} ", ssapApplicationId);
				if(applicationStage.getValue()>=1) {
					Map<String, String> responseData = applicationCloneService.cloneSsap(ssapApplication, true, new AtomicBoolean(true), false);
					if (!responseData.containsKey(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID)) {
						LOGGER.error("automateApplication::cloning did not return new application id");
						response.setMessage("Unable to clone the application.");
						response.setStatus("FAILED");
						return response;
					}
					response.setApplicationCloned(true);
					clonedAppId = responseData.get(ApplicationCloneServiceImpl.RENEWED_APPLICATION_ID);
					Long clonedId = Long.valueOf(clonedAppId);
					response.setClonedApplicationId(clonedId);
					LOGGER.debug("automateApplication::invokeEligibilityEngine with id {} " , clonedAppId);
					if (!nonCloseablestatuses.contains(ssapApplication.getApplicationStatus())  &&
							 !ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(ssapApplication.getApplicationDentalStatus()))
					{
						lceEditApplicationService.closeApplications(ssapApplication);
						LOGGER.info("Updating existing application status is closed now.");
					}
					if(applicationStage.getValue()>=2) {
						// --- update flag isEditApplication to true so that duplicate events wont be created ---
						SsapApplication clonedApplication = ssapApplicationRepository.findOne(clonedId);
						ssapApplicationService.updateIsEditApplicationFlag(clonedApplication.getCaseNumber(), true);
						
						EligibilityResponse eligibilityResponse = accountTransferService.invokeEligibilityEngine(clonedId);
						if("200".equalsIgnoreCase(eligibilityResponse.getStatus())){
							 response.setRanEligibility(true);
						 }
						 
						 LOGGER.debug("automateApplication::editApplicationProcess with id {} " , clonedAppId);
						 if(applicationStage.getValue()>=3) {
							try {
								lceEditApplicationService.editApplicationProcess(clonedId);
								response.setRanAutomation(true);
							} catch (Exception e) {
								LOGGER.error("automateApplication::unable to execute AUtomation API");
								response.setMessage("Unable to clone the application.");
								response.setRanAutomation(false);
							}
						 }
					}

					response.setStatus("SUCCESS");
				}
				 LOGGER.debug("automateApplication::editApplication execution complete with id {} " , clonedAppId);
			 }
		}
		return response;
	}
}
