package com.getinsured.eligibility.prescreen.util;

import org.springframework.stereotype.Component;

@Component
public final class PrescreenConstants {
	
	private PrescreenConstants(){
		
	}
	
	public static final String PRESCREEN_SVC_URL = "#";
	/*@Value("#{configProp.ghixPrescreenServiceURL}")
	public void setGhixPrescreenServiceURL(String ghixPrescreenServiceURL) {
		PRESCREEN_SVC_URL = ghixPrescreenServiceURL;
	}*/
	
	//Benchmark premium URL
	public static final String BENCHMARCK_PREMIUM_URL="#";
	
	
	//Social Media URLs
	public static final String FACEBOOK_SHARE_URL="#";
	public static final String TWITTER_SHARE_URL="#";
	public static final String GOOGLEPLUS_SHARE_URL="#";
	
	
	public static final String AUTO_REG_PARTNERS = "#";
	/*@Value("#{configProp['phix.affiliates.autoRegistrationEnabled']}")
	public void setAutoRegPartners(String autoRegPartners) {
		AUTO_REG_PARTNERS = autoRegPartners;
	}*/
	
	
	//Smarty street URL
	public static final String SMARTY_STREET_URL = "https://api.smartystreets.com/zipcode";
	
	public static final String CALCULATE_APTC_URL 				= "eligibility/calculateAptc";
	public static final String CALCULATE_APTC_PRODUCT_URL 		= "eligibility/getAptc";
	public static final String GET_PRESCREEN_RECORD 			= "eligibility/getPrescreenRecord";
	public static final String GET_PRESCREEN_RECORD_BY_SESSION 	= "eligibility/getPrescreenRecordBySessionId";
	public static final String SAVE_PRESCREEN_RECORD 			= "eligibility/savePrescreenRecord";
	public static final String CALCULATE_CSR_URL 				= "eligibility/computeCSR";
	public static final String CALCULATE_MAGI_URL 				= "eligibility/calculateMagi";
	public static final String CALCULATE_FPL_URL 				= "eligibility/calculateFpl";
}
