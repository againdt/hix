package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsResponseSetType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.GetCaseDetailsResponseType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ResponseMetadataType;

/**
 * Wrapper Class for GetCaseDetailsResponseWrapper
 * 
 * @author Vishaka Tharani
 * @since 17-Feb-2014
 */

public class GetCaseDetailsResponseWrapper implements JSONAware{

	private ResponseMetadataWrapper responseMetadata;
	private GetCaseDetailsResponseSetTypeWrapper getCaseDetailsResult;
	
	public GetCaseDetailsResponseWrapper(GetCaseDetailsResponseType response){
		
		if (response != null){
			GetCaseDetailsResponseSetType getCaseDetailsResponseSetType = response.getGetCaseDetailsResponseSet();
			
			if(getCaseDetailsResponseSetType != null){
				this.getCaseDetailsResult = new GetCaseDetailsResponseSetTypeWrapper(getCaseDetailsResponseSetType);
			}
			ResponseMetadataType responseMetadataType = response.getResponseMetadata();
			
			if(responseMetadataType != null){
				this.responseMetadata = new ResponseMetadataWrapper(responseMetadataType);
			}
		}
	}
	
	public GetCaseDetailsResponseSetTypeWrapper getGetCaseDetailsResult() {
		return getCaseDetailsResult;
	}
	
	public ResponseMetadataWrapper getResponseMetadata(){
		return this.responseMetadata;
	}
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseMetadata",this.responseMetadata.toJSONString());
		obj.put("GetCaseDetailsResultType", this.getCaseDetailsResult.toJSONString());
		return obj.toJSONString();
	}
}
