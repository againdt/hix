/**
 *
 */
package com.getinsured.hix.batch.referral.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.service.IntegrationLogService;
import com.getinsured.hix.batch.referral.ReferralBatchResponse;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

/**
 * @author Biswakesh
 *
 */
@Service("csChangeDisenrollService")
public class CSChangeDisenrollServiceImpl implements CSChangeDisenrollService {

	private static final Logger LOGGER = Logger.getLogger(CSChangeDisenrollServiceImpl.class);

	private static final int IDX_SSAP_ZERO = 0;
	private static final int IDX_SSAP_ONE = 1;
	private static final int IDX_SSAP_TWO = 2;

	private final String CS_CHANGE_BATCH_DISENROLL = "CS_CHANGE_BATCH_DISENROLL";
	private final String GEO_LOCALE_CHANGE_BATCH_DISENROLL = "GEO_LOCALE_CHANGE_BATCH_DISENROLL";
	private final String CURRENTAPPLICATION = "currentapplication";
	private final String ENROLLEDAPPLICATION = "enrolledapplication";
	private final String CMRHOUSEHOLD = "cmrhousehold";

	@Value("#{configProp['database.type']}")
	private String dbType;

	private Gson gson = new Gson();

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	@Autowired
	private UserService userService;

	@Autowired
	private IntegrationLogService integrationLogService;

	/*
	 * (non-Javadoc)
	 *
	 * @see com.getinsured.hix.batch.referral.service.CSChangeDisenrollService# processCSPlanChange()
	 */
	@Override
	public void processCSPlanChange() {
		List<Object[]> ssapApplicationDTOList = null;
		ReferralBatchResponse batchResponse = new ReferralBatchResponse();
		String response = GhixConstants.RESPONSE_SUCCESS;

		try {
			batchResponse.setStartTime(new Date());
			batchResponse.setBatchName(CS_CHANGE_BATCH_DISENROLL);
			if(dbType.equalsIgnoreCase("Oracle")){
				ssapApplicationDTOList = ssapApplicationRepository.findCSPlanChangedApplications_Oracle(new Timestamp(new Date().getTime()));
			}else if(dbType.equalsIgnoreCase("POSTGRESQL")){
				ssapApplicationDTOList = ssapApplicationRepository.findCSPlanChangedApplications_Postgres(new Timestamp(new Date().getTime()));
			} else {
				LOGGER.error("Unknown db type found in configuration.properties: " + dbType );
				throw new GIRuntimeException("Unknown db type found in configuration.properties: " + dbType);
			}

			processBatchDisenrollment(ssapApplicationDTOList, batchResponse);
		} catch (Exception ex) {
			response = GhixConstants.RESPONSE_FAILURE;
			LOGGER.error("ERR: WHILE PROCESSING CS PLAN CHANGE: ", ex);
		} finally {
			batchResponse.setEndTime(new Date());
			logData(batchResponse, response, CS_CHANGE_BATCH_DISENROLL);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see com.getinsured.hix.batch.referral.service.CSChangeDisenrollService# processCSGeoLocaleChange()
	 */
	@Override
	public void processCSGeoLocaleChange() {
		List<Object[]> ssapApplicationDTOList = null;
		ReferralBatchResponse batchResponse = new ReferralBatchResponse();
		String response = GhixConstants.RESPONSE_SUCCESS;
		try {
			batchResponse.setStartTime(new Date());
			batchResponse.setBatchName(GEO_LOCALE_CHANGE_BATCH_DISENROLL);
			if(dbType.equalsIgnoreCase("Oracle")){
				ssapApplicationDTOList = ssapApplicationRepository.findCSGeoLocaleChangedApplications_Oracle(new Timestamp(new Date().getTime()));
			}else if(dbType.equalsIgnoreCase("POSTGRESQL")){
				ssapApplicationDTOList = ssapApplicationRepository.findCSGeoLocaleChangedApplications_Postgres(new Timestamp(new Date().getTime()));
			} else {
				LOGGER.error("Unknown db type found in configuration.properties: " + dbType );
				throw new GIRuntimeException("Unknown db type found in configuration.properties: " + dbType);
			}

			processBatchDisenrollment(ssapApplicationDTOList, batchResponse);
		} catch (Exception ex) {
			response = GhixConstants.RESPONSE_FAILURE;
			LOGGER.error("ERR: WHILE PROCESSING CS PLAN CHANGE: ", ex);
		} finally {
			batchResponse.setEndTime(new Date());
			logData(batchResponse, response, GEO_LOCALE_CHANGE_BATCH_DISENROLL);
		}
	}

	/**
	 * @param ssapApplicationDTOList
	 */
	private void processBatchDisenrollment(List<Object[]> ssapApplicationDTOList, ReferralBatchResponse batchResponse) {

		if (null != ssapApplicationDTOList && !ssapApplicationDTOList.isEmpty()) {
			final String userName = getUserName();
			final String terminationDateStr = getTerminationDate();
			for (Object[] ssapApplicationDTO : ssapApplicationDTOList) {
				processApplication(userName, terminationDateStr, ssapApplicationDTO, batchResponse);
			}
		}
	}

	/**
	 * @param userName
	 * @param terminationDateStr
	 * @param ssapApplicationDTO
	 */
	private void processApplication(final String userName, final String terminationDateStr, Object[] ssapApplicationDTO, ReferralBatchResponse batchResponse) {
		Long currentApplicationId = 1L;
		Long enrolledApplicationId = 1L;
		int cmrHousehold = 0;
		String response = GhixConstants.RESPONSE_FAILURE;
		try {
			currentApplicationId = ((Long) ssapApplicationDTO[IDX_SSAP_ZERO]).longValue();
			enrolledApplicationId = ((Long) ssapApplicationDTO[IDX_SSAP_ONE]).longValue();
			cmrHousehold = ((BigDecimal) ssapApplicationDTO[IDX_SSAP_TWO]).intValue();
			response = disenrollApplication(currentApplicationId, enrolledApplicationId, terminationDateStr, userName, batchResponse.getBatchName());
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE PRCCSNG APP DISENROLL: ", ex);
		} finally {
			populateBatchResponse(currentApplicationId, enrolledApplicationId, cmrHousehold, response, batchResponse);
		}
	}

	private void populateBatchResponse(Long currentApplicationId, Long enrolledApplicationId, int cmrHousehold, String response, ReferralBatchResponse batchResponse) {
		Map<String, String> successFailureApps = new HashMap<String, String>();

		successFailureApps.put(CURRENTAPPLICATION, String.valueOf(currentApplicationId));
		successFailureApps.put(ENROLLEDAPPLICATION, String.valueOf(enrolledApplicationId));
		successFailureApps.put(CMRHOUSEHOLD, String.valueOf(cmrHousehold));

		if (GhixConstants.RESPONSE_SUCCESS.equals(response)) {
			batchResponse.getSuccessfullApplications().add(successFailureApps);
		} else {
			batchResponse.getFailedApplications().add(successFailureApps);
		}
	}

	/**
	 *
	 * @return
	 */
	private String getTerminationDate() {
		String terminationDateStr = null;
		Calendar calendar = null;
		DateFormat df = null;

		try {
			calendar = Calendar.getInstance();
			calendar.setTimeInMillis(System.currentTimeMillis());
			calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
			df = new SimpleDateFormat(ReferralConstants.DB_DATE_FORMAT);
			terminationDateStr = df.format(calendar.getTime());
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FETCHING DATE: ", ex);
		}

		return terminationDateStr;
	}

	/**
	 *
	 * @param currentApplicationId
	 * @param enrolledApplicationId
	 * @param terminationDateStr
	 * @param userName
	 * @param serviceName
	 */
	private String disenrollApplication(final Long currentApplicationId, final Long enrolledApplicationId, final String terminationDateStr, final String userName, String serviceName) {
		String response = GhixConstants.RESPONSE_FAILURE;
		String url = null;
		if (CS_CHANGE_BATCH_DISENROLL.equals(serviceName)) {
			url = GhixEndPoints.EligibilityEndPoints.BATCH_AUTOMATE_CSLEVEL;
		} else {
			url = GhixEndPoints.EligibilityEndPoints.BATCH_AUTOMATE_GEOLOCALE;
		}
		final ResponseEntity<String> responseEntity = ghixRestTemplate.exchange(url, userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, new String[] { String.valueOf(currentApplicationId), String.valueOf(enrolledApplicationId),
		        terminationDateStr });

		if (responseEntity == null || responseEntity.getBody() == null) {
			LOGGER.error("Unable to disenroll for " + currentApplicationId);
			throw new GIRuntimeException("UNABLE TO TRIGGER NOTIFICATION FOR SSAP ID:" + currentApplicationId);
		} else {
			response = responseEntity.getBody();
			if (!GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response)) {
				if (LOGGER.isInfoEnabled()) {
					LOGGER.info("DISENROLLED FAILED FOR SSAP ID: " + currentApplicationId);
				}
			}
		}
		return response;
	}

	/**
	 *
	 * @return
	 */
	private String getUserName() {
		String userName = ReferralConstants.EXADMIN_USERNAME;
		try {
			AccountUser currentUser = getLoggedInUser();
			if (currentUser != null) {
				userName = currentUser.getUserName();
			}
		} catch (Exception e) {
			LOGGER.error("Error fetching currently logged in user details", e);
		}
		return userName;
	}

	/**
	 *
	 * @return
	 */
	private AccountUser getLoggedInUser() {
		AccountUser currentUser = null;
		String errorMessage;
		try {
			currentUser = userService.getLoggedInUser();
		} catch (InvalidUserException invalidUserException) {
			errorMessage = "Error fetching currently logged in user details";
			LOGGER.error(errorMessage, invalidUserException);
		}
		return currentUser;
	}

	private void logData(ReferralBatchResponse message, String status, String serviceName) {
		integrationLogService.save(gson.toJson(message), serviceName, status, null, null);
	}

}
