(function (views) {
	views.SortView = Backbone.View.extend({
		
		events : {
			//'change .sort': 'sort',
			'change input[name="sortBy"]': 'sort'
		},
		
		el : '#sort',
		initialize : function () {
			_.bindAll (this, 'render');
			var self = this;

			self.tmpl = _.template($('#tmpSort').html());
			self.collection.bind('reset', this.render);

		},
		render : function () {
            var self;
            self = this;

			var html = this.tmpl(self.collection.info());
			$(this.el).html(html);
		},
		
		sort: function (e) {
			var currentSort = this.$(":checked").attr('id');
			
			if(currentSort == 'smartScore'){
				this.collection.setSort(currentSort, 'desc');
			}else this.collection.setSort(currentSort, 'asc');
			
//			var currentSort = this.$(":checked").attr('id');
			
			$("#"+currentSort).prop('checked', true);
			this.collection.preserveOtherOptions();
		},
		
	});
})(App.views);