import React from 'react';

import SummaryItem from './SummaryItem';

function AddressItem ({label, streetAddress1, streetAddress2, city, state, postalCode, textCode}) {
    return (
        <>
            {textCode &&
                <SummaryItem
                    textCode={textCode}
                    value={`${streetAddress1}`}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label margin-b-10',
                        divClass2: 'summary__value'
                    }}
                />
            }
            {!textCode &&
                <SummaryItem
                    category={label}
                    value={`${streetAddress1}`}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: 'summary__label margin-b-10',
                        divClass2: 'summary__value margin-b-0 margin-t-5-ve '
                    }}
                />
            }
            {streetAddress2 && streetAddress2.length > 0 &&
                <SummaryItem
                    category=""
                    value={`${streetAddress2}`}
                    classes2={{
                        spanClass: 'className1 className001',
                        divClass1: '',
                        divClass2: 'summary__value margin-b-0 margin-t-5-ve'
                    }}
                />
            }
            <SummaryItem
                category=""
                value={`${city} ${state}, ${postalCode}`}
                classes2={{
                    spanClass: 'className1 className001',
                    divClass1: '',
                    divClass2: 'summary__value margin-b-0 margin-t-5-ve'
                }}
            />
        </>
    );
}

export default AddressItem;
