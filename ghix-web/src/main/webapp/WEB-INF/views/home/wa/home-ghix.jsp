<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<div class="row-fluid" role="main">
  <div class="landing-signup">
	<div class="row-fluid">	
	  <div class="span6">
		<div class="register-signin">
		  <p><i class="icon-lock"></i> <spring:message code="label.homePage.haveAccountQuestion"/> 
		  <a class="btn btn-primary" href="<c:url value='/account/user/login'/>"><spring:message code="label.homePage.haveAccountAction"/></a></p>			
		</div>
	  </div> 	
	</div>
  </div>				
</div>