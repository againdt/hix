<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<c:choose>
			<c:when test="${isCACall == true}"></c:when>
			<c:otherwise>
				<form name="employeeViewForm" id="employeeViewForm" action="<c:url value="/broker/employer/dashboard"/>" novalidate="novalidate">	
				<df:csrfToken/>
				 <input type="hidden" name="switchToModuleName" id="switchToModuleName" value="employer" />
				 <input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${employer.id}" />
				 <input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${employer.name}" />
			</c:otherwise>			
</c:choose>
<div id="viewempModal" class="modal hide fade" tabindex="-1"
	role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal"
			aria-hidden="true">&times;</button>
		<h3 id="myModalLabel"><spring:message code="label.agent.employers.ViewEmployerAccount"/></h3>
	</div>
	<div class="modal-body">
		<p><spring:message code="label.agent.employers.EmpAccViewconfMsg1"/>&nbsp;${employer.name}<spring:message code="label.agent.employers.EmpAccViewconfMsg2"/> </p>  
		<p><spring:message code="label.agent.employers.ProceedtoEmployerView"/></p>
	</div>
	<div class="modal-footer">
		<label class="checkbox span5 pull-left"> 
		  
		  	<c:choose>
							<c:when test="${isCACall == 'TRUE'}">
								 <input	type="checkbox" name="empchkshowpopup"	onclick="window.parent.setPopupValue();" />
								 <input type="hidden" name="showPopupInFuture" value="" />
							</c:when>
							<c:otherwise>
								 <input	type="checkbox" name="showPopupInFuture"	 />
							</c:otherwise>
			</c:choose>
		 <spring:message code="label.agent.employers.DontShowMsgAgain"/> 
		</label>
		<button class="btn" data-dismiss="modal"><spring:message code="label.brkCancel"/></button>
		<c:choose>
			<c:when test="${isCACall == true}">
				<a class="btn btn-primary" type="button"
					href="<c:url value="${switchAccUrl}&_pageLabel=employerHomePage&ahbxId=${employer.id}&ahbxUserType=0&recordId=${brokerId}&recordType=2"/>"><spring:message code="label.agent.employers.EmployerView"/></a>
					<input type="hidden" name="showPopupInFuture" value="" />
			</c:when>
			<c:otherwise>
				<button class="btn btn-primary" type="submit">  <spring:message code="label.agent.employers.EmployerView"/>  </button>
			</c:otherwise>
		</c:choose>
	</div>
</div>
<c:choose>
			<c:when test="${isCACall == true}"></c:when>
			<c:otherwise>
				</form>			
			</c:otherwise>			
</c:choose>
