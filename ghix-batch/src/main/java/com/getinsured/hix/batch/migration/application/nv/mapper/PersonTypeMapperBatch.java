package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Predicate;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.indportal.dto.dm.application.AttestationApplication;
import com.getinsured.hix.indportal.dto.dm.application.AttestationMembersIncome;
import com.getinsured.hix.indportal.dto.dm.application.AttestationsMember;
import com.getinsured.hix.indportal.dto.dm.application.ContactInformation;
import com.getinsured.hix.indportal.dto.dm.application.CurrentIncome;
import com.getinsured.hix.indportal.dto.dm.application.EscOffer;
import com.getinsured.hix.indportal.dto.dm.application.Member;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.EmploymentStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.ExpenseType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.OrganizationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonContactInformationAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonEmploymentAssociationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.PregnancyStatusType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.TribalAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ESIAugmentationType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EmployerSponsoredInsuranceType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.SSFPrimaryContactType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.InsurancePlanRateType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_pm.InsurancePlanType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactPreferenceCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.EmploymentStatusCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExpenseCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExpenseCategoryCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AddressType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ContactInformationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.FullTelephoneNumberType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.IdentificationType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonLanguageType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonNameType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.ProperNameTextType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StreetType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.StructuredAddressType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TelephoneNumberType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.usps_states._2.USStateCodeType;

@Component(value="personTypeMapperBatch")
public class PersonTypeMapperBatch {
	private static final String kEY_AMERICAN_INDIAN_ALASKA_NATIVE = "americanIndianAlaskaNative";
	private static final String kEY_HOUSEHOLD_CONTACT = "householdContact";
	private static final String kEY_PHONE = "phone";
	private static final String OTHER_PHONE = "otherPhone";
	//private static String lABEL = "label";
	private static String cODE = "code";
	private static String ethnicityOtherLabel = "otherLabel";
	private static String raceLabel = "label";
	private static Logger lOGGER = Logger.getLogger(PersonTypeMapperBatch.class);
	private PersonType person;
	@Autowired IncomeMapperBatch incomeMapper;
	//private Predicate<AttestationsMember> homeAddrswessPresent = m->null!=m.getDemographic().getHomeAddress();
	//private Predicate<AttestationsMember> mailingAddressPrsesent = m->null!=m.getDemographic().getMailingAddress();
	
	
	private PersonNameType createPersonName(AttestationsMember member ){
		// person name
		PersonNameType name = AccountTransferUtil.niemCoreFactory.createPersonNameType();
		if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getFirstName())){
			name.setPersonGivenName(AccountTransferUtil.createPersonNameTextType(member.getDemographic().getName().getFirstName()));
		}
		if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getMiddleName())){
			name.setPersonMiddleName(AccountTransferUtil.createPersonNameTextType(member.getDemographic().getName().getMiddleName()));
		}
		if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getLastName())){
			name.setPersonSurName(AccountTransferUtil.createPersonNameTextType(member.getDemographic().getName().getLastName()));
		}
		if(AccountTransferUtil.isNotNullAndEmpty(member.getDemographic().getName().getSuffix())){
			name.setPersonNameSuffixText(AccountTransferUtil.createPersonNameTextType(member.getDemographic().getName().getSuffix()));
		}
		return name;
	}
	
	private DateType createPersonDateOfBirth(String dob){
		DateType dateTypeDOB = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date dateDOB = AccountTransferUtil.basicFactory.createDate();
		dateDOB.setValue(AccountTransferUtil.stringToXMLGregorianCalendar(dob));
		dateTypeDOB.setDate(dateDOB);
		return dateTypeDOB;
	}
	private IdentificationType createPersonSSNID(String ssn){
		IdentificationType ssnIdentificationType = null;
		if(AccountTransferUtil.isNotNullAndEmpty(ssn)){
			ssnIdentificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
			ssnIdentificationType.setIdentificationID(AccountTransferUtil.createString(ssn));
		}else{
			lOGGER.debug("passed ssn is blank");
			//handle later
		}
		return ssnIdentificationType;
	} 
	
	private PersonContactInformationAssociationType createHomeAddress(AttestationsMember member,boolean mailingAddressSameAsHomeAddressIndicator ){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		//TODO:set associationBeginDate
		DateType associationBeginDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date associationBeginDate = AccountTransferUtil.basicFactory.createDate();
		associationBeginDate.setValue(AccountTransferUtil.getSystemDate());
		associationBeginDateType.setDate(associationBeginDate);
		personContactInformationAssociation.setAssociationBeginDate(associationBeginDateType);
		
		ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
		AddressType addressType =AccountTransferUtil.niemCoreFactory.createAddressType();
		StructuredAddressType structuredAddressType = AccountTransferUtil.niemCoreFactory.createStructuredAddressType();
		StreetType streetType1 = AccountTransferUtil.niemCoreFactory.createStreetType();
		TextType streetTextType = AccountTransferUtil.niemCoreFactory.createTextType();
		
		
		//homeAddressIndicator
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
		
		if(mailingAddressSameAsHomeAddressIndicator == false){
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.HOME);
		}else if(mailingAddressSameAsHomeAddressIndicator == true){
			personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING);
		}else{
			personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING);
		}
		
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		
		personContactInformationAssociation.setContactInformationCategoryCode(contactInformationCategoryCodeType);
		
		String streetAddress1 = null;
		String streetAddress2 = null;
		String city = null;
		String state = null;
		String postalCodestr = null;
		String  countystr = null;
		String countyCode = null;
		Predicate<AttestationsMember> homeAddressPresent = m->null!=m.getDemographic().getHomeAddress();
		
		if(homeAddressPresent.test(member)) {
			streetAddress1 = member.getDemographic().getHomeAddress().getStreetName1();
			streetAddress2 = member.getDemographic().getHomeAddress().getStreetName2();
			city = (String) member.getDemographic().getHomeAddress().getCityName();
			state = (String) member.getDemographic().getHomeAddress().getStateCode();
			postalCodestr = member.getDemographic().getHomeAddress().getZipCode();
			countystr = member.getDemographic().getHomeAddress().getCountyName();
			countyCode = member.getDemographic().getHomeAddress().getCountyFipsCode();
		}else {
			streetAddress1 = member.getDemographic().getMailingAddress().getStreetName1();
			streetAddress2 = member.getDemographic().getMailingAddress().getStreetName2();
			city = (String) member.getDemographic().getMailingAddress().getCityName();
			state = (String) member.getDemographic().getMailingAddress().getStateCode();
			postalCodestr = member.getDemographic().getMailingAddress().getZipCode();
			countystr = member.getDemographic().getMailingAddress().getCountyName();
			countyCode = member.getDemographic().getMailingAddress().getCountyFipsCode();
		}
		
		
		
		streetTextType.setValue(streetAddress1);
		streetType1.setStreetFullText(streetTextType);
		structuredAddressType.setLocationStreet(streetType1);
		
		if(null!=streetAddress2 && streetAddress2.length()>0) {
			TextType streetTextType2 = AccountTransferUtil.niemCoreFactory.createTextType();
			streetTextType2.setValue(streetAddress2);
			structuredAddressType.setAddressSecondaryUnitText(streetTextType2);
		}
		
		ProperNameTextType properNameTextType = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
		properNameTextType.setValue(city);
		structuredAddressType.setLocationCityName(properNameTextType);
		
		ProperNameTextType countyName = AccountTransferUtil.niemCoreFactory.createProperNameTextType();
		if (countystr != null && countystr.length()>0) {
		    countyName.setValue(countystr);
		    structuredAddressType.setLocationCountyName(countyName);
		}
		
		//USCountyCodeType USCountCodeType = AccountTransferUtil.niemFipsFactory.createUSCountyCodeType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String tempCountyCode = AccountTransferUtil.basicFactory.createString();
		if (countyCode != null && countyCode.length()>0) {
			tempCountyCode.setValue(countyCode.substring(countyCode.length() - 3,countyCode.length()));
			structuredAddressType.setLocationCountyCode(tempCountyCode);
		}
		
		USStateCodeType uSStateCodeType = AccountTransferUtil.statesObjFactory.createUSStateCodeType();
		uSStateCodeType.setValue(USStateCodeSimpleType.fromValue(state));
		structuredAddressType.setLocationStateUSPostalServiceCode(uSStateCodeType);
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String postalCode = AccountTransferUtil.basicFactory.createString();
		postalCode.setValue(postalCodestr);
		structuredAddressType.setLocationPostalCode(postalCode);
		
		addressType.setStructuredAddress(structuredAddressType);
		contactInformationType.setContactMailingAddress(addressType);
		personContactInformationAssociation.setContactInformation(contactInformationType);
		return personContactInformationAssociation;
	}
	
	private PersonContactInformationAssociationType createPersonPhoneNumberAssociation(ContactInformation contactInformation){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		
		DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		sigDate.setValue(AccountTransferUtil.getSystemDate());
		sigDateType.setDate(sigDate);
		personContactInformationAssociation.setAssociationBeginDate(sigDateType);
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
				
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		
		String phoneNumStr = null;
		
		if(null!=contactInformation && null!=contactInformation.getPrimaryPhoneNumber() && null!=contactInformation.getPrimaryPhoneNumber().getNumber()) {
			phoneNumStr =contactInformation.getPrimaryPhoneNumber().getNumber();
			contactInformationCategoryCodeType.setValue(com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.SELF);
		}
		
		if(phoneNumStr!= null && phoneNumStr.length()>0){
			ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
			TelephoneNumberType telephoneNumberType = AccountTransferUtil.niemCoreFactory.createTelephoneNumberType();
			FullTelephoneNumberType fullPhone = AccountTransferUtil.niemCoreFactory.createFullTelephoneNumberType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String phonenumber = AccountTransferUtil.basicFactory.createString();
			phonenumber.setValue(phoneNumStr);
			fullPhone.setTelephoneNumberFullID(phonenumber);
			if(contactInformation.getPrimaryPhoneNumber().getExt() != null){
				fullPhone.setTelephoneSuffixID(AccountTransferUtil.createString(contactInformation.getPrimaryPhoneNumber().getExt()));
			}
			telephoneNumberType.setFullTelephoneNumber(fullPhone);
			contactInformationType.setContactTelephoneNumber(telephoneNumberType);
			personContactInformationAssociation.setContactInformationCategoryCode(contactInformationCategoryCodeType);
			personContactInformationAssociation.setContactInformation(contactInformationType);
		}
		
		return personContactInformationAssociation;
	}
	private PersonContactInformationAssociationType createPersonEmailAssociation(ContactInformation contactInformation){
		PersonContactInformationAssociationType personContactInformationAssociation = AccountTransferUtil.hixCoreFactory.createPersonContactInformationAssociationType();
		com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeType contactInformationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createContactInformationCategoryCodeType();
		
		DateType sigDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date sigDate = AccountTransferUtil.basicFactory.createDate();
		sigDate.setValue(AccountTransferUtil.getSystemDate());
		sigDateType.setDate(sigDate);
		personContactInformationAssociation.setAssociationBeginDate(sigDateType);
		
		personContactInformationAssociation.setContactInformationIsPrimaryIndicator(AccountTransferUtil.addBoolean(false));
		String emailaddress = contactInformation.getEmail();
		if(emailaddress!= null && emailaddress.length()>0){
			ContactInformationType contactInformationType = AccountTransferUtil.niemCoreFactory.createContactInformationType();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.String emailStr = AccountTransferUtil.basicFactory.createString();
			emailStr.setValue(emailaddress);
			contactInformationType.setContactEmailID(emailStr);
			personContactInformationAssociation.setContactInformation(contactInformationType);
		}
		return personContactInformationAssociation;
	}
		
	private PersonLanguageType createPreferredLanguage(String language,boolean writes,boolean reads){

			PersonLanguageType personLanguageType = AccountTransferUtil.niemCoreFactory.createPersonLanguageType();
			TextType languageName =  AccountTransferUtil.niemCoreFactory.createTextType();
			languageName.setValue(language);
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean writesLang = AccountTransferUtil.basicFactory.createBoolean();
			com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean readsLang = AccountTransferUtil.basicFactory.createBoolean();
			writesLang.setValue(writes);
			personLanguageType.setPersonWritesLanguageIndicator(writesLang);
			readsLang.setValue(reads);
			personLanguageType.setPersonSpeaksLanguageIndicator(readsLang);
			personLanguageType.getLanguageName().add(languageName);
			return personLanguageType;
	}
	private List<TextType> getEthnicityList(List<String> ethnicities, java.lang.Boolean hispanicOriginIndicator){
		List<TextType> ethnicityList = new ArrayList<TextType>();
		if(hispanicOriginIndicator != null && hispanicOriginIndicator== true){
			for (String ethnicity : ethnicities) {
				TextType ethnicityTextType = AccountTransferUtil.niemCoreFactory.createTextType();
				ethnicityTextType.setValue(ethnicity);
				ethnicityList.add(ethnicityTextType);
			}
		}
		return ethnicityList;
	}
	
	private List<TextType> getRaceList(List<String> races){
		List<TextType> raceList = new ArrayList<TextType>();
		TextType raceText = AccountTransferUtil.niemCoreFactory.createTextType();
		if(races.isEmpty()){
			raceText = AccountTransferUtil.niemCoreFactory.createTextType();
			raceText.setValue("Unknown");
			raceList.add(raceText);
		}else{
			for (String race: races) {
				raceText = AccountTransferUtil.niemCoreFactory.createTextType();
				raceText.setValue(race);
				raceList.add(raceText);
				raceText = null;
			}
		}
		return raceList;
	}
	public PersonType createPerson(Entry<String,AttestationsMember> memberEntry,AttestationApplication application, Map<Long, Map<String, 
			VerificationMetadataType>> verificationMetadatas, AccountTransferRequestPayloadType returnObj, Map<String, Member> computedMembers, SSFPrimaryContactType ssfPrimaryContactType) {
		
		person = AccountTransferUtil.hixCoreFactory.createPersonType();
		
		String personId = AccountTransferUtil.getPersonTrackingNumber(memberEntry.getKey(), computedMembers);
		// Id
		person.setId(personId);
		// date of birth	
		person.setPersonBirthDate(this.createPersonDateOfBirth(memberEntry.getValue().getDemographic().getBirthDate()));
		//name
		person.getPersonName().add(this.createPersonName(memberEntry.getValue()));
		// SSN
		String ssnNumber = memberEntry.getValue().getDemographic().getSsn();
		if(StringUtils.isNotBlank(ssnNumber))
		{
		IdentificationType personSSNID = this.createPersonSSNID(ssnNumber);
		person.getPersonSSNIdentification().add(personSSNID);
		}
		
		// Set verification metadata for SSN if available
        // Map<String, VerificationMetadataType> personVerificationsMetadata = verificationMetadatas.get(Long.valueOf(personId));
        /*if(null != personSSNID) {
            VerificationMetadataWrapper.setMetadata(personVerificationMetadata,personSSNID , VerificationCategoryCodeSimpleType.SSN);
            person.getPersonSSNIdentification().add(personSSNID);
        }*/
		
		PersonAugmentationType personAugmentationType = AccountTransferUtil.hixCoreFactory.createPersonAugmentationType();
		//address

		Predicate<AttestationsMember> homeAddressPresent = m->null!=m.getDemographic().getHomeAddress();
		Predicate<AttestationsMember> mailingAddressPresent = m->null!=m.getDemographic().getMailingAddress();
		if(homeAddressPresent.or(mailingAddressPresent).test(memberEntry.getValue())) {
			personAugmentationType.getPersonContactInformationAssociation().add(this.createHomeAddress(memberEntry.getValue(),false));
			personAugmentationType.getPersonContactInformationAssociation().add(this.createHomeAddress(memberEntry.getValue(), true));
		}
		
		List<PersonContactInformationAssociationType> personContactInformationAssociationTypeList = personAugmentationType.getPersonContactInformationAssociation();
		boolean mailingAddress = false;
		boolean homeAddress = false;
		List<PersonContactInformationAssociationType> personContactInformationAssociationTypeNewList = new ArrayList<PersonContactInformationAssociationType>();
		for (PersonContactInformationAssociationType personContactInformationAssociationType : personContactInformationAssociationTypeList) {
			if(mailingAddress == false && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() == com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.MAILING ){
				mailingAddress = true;
				personContactInformationAssociationTypeNewList.add(personContactInformationAssociationType);
			}
			if(homeAddress == false && personContactInformationAssociationType.getContactInformationCategoryCode().getValue() == com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ContactInformationCategoryCodeSimpleType.HOME ){
				homeAddress = true;
				personContactInformationAssociationTypeNewList.add(personContactInformationAssociationType);
			}
		}
		personAugmentationType.setPersonContactInformationAssociation(personContactInformationAssociationTypeNewList);
		personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonPhoneNumberAssociation(application.getContactInformation()));
		// contactPreferences:emailAddress
		
		//preferred language
		//TODO this will never be 1 in our case, should it be contactperson  from application??
		String contactPersonId = AccountTransferUtil.getPersonTrackingNumber(application.getContactMemberIdentifier(), computedMembers);
		if (personId.equals(contactPersonId)) {
			personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonEmailAssociation(application.getContactInformation()));
			if(StringUtils.isEmpty(application.getContactInformation().getEmail()) && ssfPrimaryContactType.getSSFPrimaryContactPreferenceCode().getValue().equals(ContactPreferenceCodeSimpleType.EMAIL)){
				ssfPrimaryContactType.getSSFPrimaryContactPreferenceCode().setValue(ContactPreferenceCodeSimpleType.MAIL);
			}
			String speaks = application.getSpokenLanguageType();
			String writes =	application.getWrittenLanguageType();
			if(AccountTransferUtil.isNotNullAndEmpty(speaks) && AccountTransferUtil.isNotNullAndEmpty(writes) ){
				if(speaks.equalsIgnoreCase(writes)){
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(speaks,true,true));
				}else{
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(speaks,true,false));
					personAugmentationType.getPersonPreferredLanguage().add(createPreferredLanguage(writes,false,true));
				}
			}
		}else{
			personAugmentationType.getPersonContactInformationAssociation().add(this.createPersonEmailAssociation(application.getContactInformation()));

		}
		if ("MARRIED".equalsIgnoreCase(memberEntry.getValue().getDemographic().getMaritalStatus()))
		{
			personAugmentationType.setPersonMarriedIndicator(AccountTransferUtil.addBoolean(true));
		}
		else
		{
			personAugmentationType.setPersonMarriedIndicator(AccountTransferUtil.addBoolean(false));
		}
		personAugmentationType.setPersonMarriedIndicatorCode(AccountTransferUtil.createTextType("R"));

		//personAugmentationType.setPersonMarriedIndicator(AccountTransferUtil.addBoolean(householdMemberJSON.get("marriedIndicator")));

		if(null != memberEntry.getValue().getDemographic() && null!=memberEntry.getValue().getDemographic().getEthnicity()) {
			person.getPersonEthnicityText().addAll(getEthnicityList(memberEntry.getValue().getDemographic().getEthnicity(),memberEntry.getValue().getDemographic().getHispanicOriginIndicator()));
		}
		
		if(null!=memberEntry.getValue().getDemographic().getRace()) {
			person.getPersonRaceText().addAll(getRaceList(memberEntry.getValue().getDemographic().getRace()));
		}
		
		/*JSONObject specialCircumstances = (JSONObject) householdMemberJSON.get("specialCircumstances");
		
		PregnancyStatusType pregnancyStatusType = AccountTransferUtil.hixCoreFactory.createPregnancyStatusType();
		pregnancyStatusType.setStatusIndicator(AccountTransferUtil.addBoolean(specialCircumstances.get("pregnantIndicator")));
		
		QuantityType babyQuantity = AccountTransferUtil.niemCoreFactory.createQuantityType();
		if(null != specialCircumstances.get("numberBabiesExpectedInPregnancy") && NumberUtils.isNumber(specialCircumstances.get("numberBabiesExpectedInPregnancy").toString())) {
		    babyQuantity.setValue(new BigDecimal(specialCircumstances.get("numberBabiesExpectedInPregnancy").toString()));
		    pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
		    personAugmentationType.setPersonPregnancyStatus(pregnancyStatusType);
		}else{
			 babyQuantity.setValue(new BigDecimal("0"));
			 pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
			 personAugmentationType.setPersonPregnancyStatus(pregnancyStatusType);
		}*/
		if("FEMALE".equalsIgnoreCase(memberEntry.getValue().getDemographic().getSex()))
		{
			PregnancyStatusType pregnancyStatusType = AccountTransferUtil.hixCoreFactory.createPregnancyStatusType();
			pregnancyStatusType.setStatusIndicator(AccountTransferUtil.addBoolean(memberEntry.getValue().getFamily().getPregnancyIndicator()));
			
			QuantityType babyQuantity = AccountTransferUtil.niemCoreFactory.createQuantityType();
			
			if(null != memberEntry.getValue().getFamily().getBabyDueQuantity() && NumberUtils.isNumber(memberEntry.getValue().getFamily().getBabyDueQuantity().toString())) {
			    babyQuantity.setValue(new BigDecimal(memberEntry.getValue().getFamily().getBabyDueQuantity().toString()));
			    pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
			}else{
				 babyQuantity.setValue(new BigDecimal("0"));
				 pregnancyStatusType.setPregnancyStatusExpectedBabyQuantity(babyQuantity);
			}
			
			personAugmentationType.setPersonPregnancyStatus(pregnancyStatusType);
		}
		//if(householdMemberJSON.get("externalId") != null){
			//String externalId = householdMemberJSON.get("externalId").toString();
			IdentificationType identificationType = AccountTransferUtil.niemCoreFactory.createIdentificationType();
			identificationType.setIdentificationID(AccountTransferUtil.createString(personId));
			personAugmentationType.setPersonMedicaidIdentification(identificationType);
			personAugmentationType.setPersonCHIPIdentification(identificationType);
		//}
		
		//String applicantGuid = (String) householdMemberJSON.get("applicantGuid");
		
		/*JSONObject detailedIncome = ((JSONObject) householdMemberJSON.get("detailedIncome"));
		
		// Alimony expenses mapping
		JSONObject deductions = (JSONObject)detailedIncome.get("deductions");
        ExpenseType alimonyExpense = createExpenseFor(ExpenseCategoryCodeSimpleType.ALIMONY, deductions.get("alimonyDeductionAmount"), deductions.get("alimonyDeductionFrequency"), null);
        if(null != alimonyExpense) {
            personAugmentationType.getPersonExpense().add(alimonyExpense); 
        }

		// Student loan expenses mapping
        ExpenseType studentLoanExpense = createExpenseFor(ExpenseCategoryCodeSimpleType.STUDENT_LOAN_INTEREST, deductions.get("studentLoanDeductionAmount"), deductions.get("studentLoanDeductionFrequency"), null);
        if(null != studentLoanExpense) {
            personAugmentationType.getPersonExpense().add(studentLoanExpense); 
        }
        
        // Other expenses mapping
        ExpenseType otherExpense = createExpenseFor(null, deductions.get("otherDeductionAmount"), deductions.get("otherDeductionFrequency"),""); 
        		//((JSONArray)deductions.get("deductionType")).get(2).toString());
        if(null != otherExpense) {
            personAugmentationType.getPersonExpense().add(otherExpense); 
        }*/

		//person.getPersonUSCitizenIndicator().add(addUSCitizenIndicator(computedMembers.get(memberEntry.getKey())));
			person.getPersonUSCitizenIndicator().add(addUSCitizenIndicator(memberEntry.getValue()));
		
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean uSNaturalizedCitizenIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();

		if(memberEntry.getValue().getLawfulPresence() != null && memberEntry.getValue().getLawfulPresence().getNaturalizedCitizenIndicator() != null){
			uSNaturalizedCitizenIndicatorBoolean.setValue(memberEntry.getValue().getLawfulPresence().getNaturalizedCitizenIndicator());
			personAugmentationType.getUSNaturalizedCitizenIndicator().add(uSNaturalizedCitizenIndicatorBoolean);
		}
		//if(memberEntry.getValue().getInsuranceCoverage()!= null && memberEntry.getValue().getInsuranceCoverage().getEmployerSponsoredCoverageOffers() != null && memberEntry.getValue().getInsuranceCoverage().getEmployerSponsoredCoverageOffers().getEscOffer1() != null)
		if(memberEntry.getValue().getInsuranceCoverage()!= null &&  memberEntry.getValue().getInsuranceCoverage().getEmployerSponsoredCoverageOffers() != null && !memberEntry.getValue().getInsuranceCoverage().getEmployerSponsoredCoverageOffers().isEmpty())
		{
			for(Entry<String, EscOffer> escOffer : memberEntry.getValue().getInsuranceCoverage().getEmployerSponsoredCoverageOffers().entrySet())
			{
				personAugmentationType.getPersonEmploymentAssociation().add(this.createPersonEmploymentAssociationType(memberEntry.getValue(),escOffer.getValue()));
			}
		}
		if(memberEntry.getValue().getOther() != null && memberEntry.getValue().getOther().getVeteranIndicator() != null) {
			personAugmentationType.getPersonUSVeteranIndicator().add(AccountTransferUtil.addBoolean(memberEntry.getValue().getOther().getVeteranIndicator()));
		}
		
		if(null!=memberEntry.getValue().getIncome() && null!=memberEntry.getValue().getIncome().getCurrentIncome()) {
			personAugmentationType.getPersonIncome().addAll(incomeMapper.mapIncomes(memberEntry.getValue().getIncome()));
			List<ExpenseType> expenses = createExpenseFor(memberEntry.getValue().getIncome());
	        if(null != expenses && expenses.size()>0) {
	            personAugmentationType.getPersonExpense().addAll(expenses); 
	        }
		}
	
		//personAugmentationType.getPersonIncome().addAll(new IncomeMapper().mapIncomes(detailedIncome, personVerificationMetadata));
		person.setPersonAugmentation(personAugmentationType);
		person.setPersonSexText(setMaleFemale(memberEntry.getValue()));

		person.setTribalAugmentation(createTribalAugmentation(memberEntry.getValue()));		
		boolean personSeekingCoverageIndicator = false;
		if(memberEntry.getValue().getRequestingCoverageIndicator() != null)
		{
			personSeekingCoverageIndicator = memberEntry.getValue().getRequestingCoverageIndicator();
		}
		person.setPersonSeekingCoverageIndicator(AccountTransferUtil.addBoolean(personSeekingCoverageIndicator));
		if (personSeekingCoverageIndicator)
		{
			Member computedMember = computedMembers.get(memberEntry.getKey());
			if(computedMember != null && "NO".equals(computedMember.getQhpStatus()))
			{
				person.setPersonSeekingCoverageIndicator(AccountTransferUtil.addBoolean(false));
			}
		}
		return person;
	}

	protected com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean addUSCitizenIndicator(AttestationsMember member) {
		// naturalizedCitizenshipIndicator
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean attestedCitizenIndicatorBoolean = AccountTransferUtil.basicFactory.createBoolean();
		if(member.getLawfulPresence() != null && member.getLawfulPresence().getCitizenshipIndicator() != null) {
		VerificationMetadataType verificationMetadataType = AccountTransferUtil.hixCoreFactory.createVerificationMetadataType();
        
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean verificationStatusValue = AccountTransferUtil.basicFactory.createBoolean();
        verificationStatusValue.setValue(member.getLawfulPresence().getCitizenshipIndicator());
        verificationMetadataType.setVerificationIndicator(verificationStatusValue);
	        
        VerificationCategoryCodeType verificationCategoryCodeType = AccountTransferUtil.hixTypeFactory.createVerificationCategoryCodeType();
        verificationCategoryCodeType.setValue(VerificationCategoryCodeSimpleType.CITIZENSHIP);
	    verificationMetadataType.getVerificationCategoryCode().add(verificationCategoryCodeType); 	
		// Set verification metadata for citizenship if available.
        //attestedCitizenIndicatorBoolean.getMetadata().add(verificationMetadataType);
	    attestedCitizenIndicatorBoolean.setValue(member.getLawfulPresence().getCitizenshipIndicator());
		}
		return attestedCitizenIndicatorBoolean;
	}
	
    private List<ExpenseType> createExpenseFor(AttestationMembersIncome income) {
    	List<ExpenseType> expenses = new ArrayList<ExpenseType>();
        if(null != income ) {
        	ExpenseType expenseType = null;
        	for(Map.Entry<String,CurrentIncome> currentIncome : income.getCurrentIncome().entrySet())
        	{
        		if(currentIncome.getValue() != null && currentIncome.getValue().getIncomeAmount() != null 
        				&& (
        						("ALIMONY_PAYMENT".equals(currentIncome.getValue().getIncomeSourceType()) && BigDecimal.ZERO.compareTo(currentIncome.getValue().getIncomeAmount() ) > 0 ) 
        						|| "STUDENT_LOAN_INTEREST".equals(currentIncome.getValue().getIncomeSourceType())
        					)
        			)
        		{
        			expenseType = AccountTransferUtil.hixCoreFactory.createExpenseType();
                    ExpenseCategoryCodeType expenseCategoryCodeType = AccountTransferUtil.hixTypeFactory.createExpenseCategoryCodeType();
                    
                    if("ALIMONY_PAYMENT".equals(currentIncome.getValue().getIncomeSourceType())){
                    expenseCategoryCodeType.setValue(ExpenseCategoryCodeSimpleType.ALIMONY);
                    }
                    else
                    {
                    	expenseCategoryCodeType.setValue(ExpenseCategoryCodeSimpleType.STUDENT_LOAN_INTEREST);	
                    }
                    expenseType.setExpenseCategoryCode(expenseCategoryCodeType);
                    
                    AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
                    amountType.setValue(currentIncome.getValue().getIncomeAmount().abs());
                    expenseType.setExpenseAmount(amountType);
                    FrequencyType frequencyType = AccountTransferUtil.hixCoreFactory.createFrequencyType();
                    FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
                    frequencyCodeType.setValue(FrequencyCodeSimpleType.valueOf(currentIncome.getValue().getIncomeFrequencyType()).value());
                    frequencyType.setFrequencyCode(frequencyCodeType);
                    expenseType.setExpenseFrequency(frequencyType);
        		}
        		if (expenseType != null)
        		{
        			expenses.add(expenseType);
            		expenseType = null;
        		}
        	}
        }
        return expenses;
    }
	
	private TextType setMaleFemale(AttestationsMember member){
		TextType sexTextType = AccountTransferUtil.niemCoreFactory.createTextType();
		if("FEMALE".equalsIgnoreCase(member.getDemographic().getSex())){
				sexTextType.setValue("Female");
			}else{
				sexTextType.setValue("Male");
			}
		return sexTextType;
	}
	
	private TribalAugmentationType createTribalAugmentation(AttestationsMember member){
	
		TribalAugmentationType tribalAugmentationType = AccountTransferUtil.hixCoreFactory.createTribalAugmentationType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean indianOrAlaskaNativeIndicator = AccountTransferUtil.basicFactory.createBoolean();
		indianOrAlaskaNativeIndicator.setValue(AccountTransferUtil.checkBoolean( member.getDemographic().getAmericanIndianAlaskanNativeIndicator()));
		tribalAugmentationType.setPersonAmericanIndianOrAlaskaNativeIndicator(indianOrAlaskaNativeIndicator);
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Boolean recognisedTribalIndicator = AccountTransferUtil.basicFactory.createBoolean();
		if (member.getOther()!= null && member.getOther().getAmericanIndianAlaskanNative() != null )
		{
			recognisedTribalIndicator.setValue(AccountTransferUtil.checkBoolean(member.getOther().getAmericanIndianAlaskanNative().getPersonRecognizedTribeIndicator()));
			tribalAugmentationType.setPersonRecognizedTribeIndicator(recognisedTribalIndicator);
		}		
		if(member.getOther() != null && member.getOther().getAmericanIndianAlaskanNative() != null && member.getOther().getAmericanIndianAlaskanNative().getFederallyRecognizedTribeName() != null){
			ProperNameTextType personTribeName=AccountTransferUtil.niemCoreFactory.createProperNameTextType();
			personTribeName.setValue(member.getOther().getAmericanIndianAlaskanNative().getFederallyRecognizedTribeName());
			tribalAugmentationType.setPersonTribeName(personTribeName);
		}
		return tribalAugmentationType;
	}
	
	private PersonEmploymentAssociationType createPersonEmploymentAssociationType(AttestationsMember member, EscOffer escOffer ) {
		
		PersonEmploymentAssociationType personEmploymentAssociationType = AccountTransferUtil.hixCoreFactory.createPersonEmploymentAssociationType();
		OrganizationType organization = AccountTransferUtil.hixCoreFactory.createOrganizationType();
		if(member.getInsuranceCoverage().getEmployerSponsoredCoverageOffers() != null && !member.getInsuranceCoverage().getEmployerSponsoredCoverageOffers().isEmpty())
		{
		String orgNamestr = escOffer.getEmployer().getName();
		if(StringUtils.isNotBlank(orgNamestr)){
			TextType orgNameTextType = AccountTransferUtil.niemCoreFactory.createTextType();
			orgNameTextType.setValue(orgNamestr);
			organization.setOrganizationName(orgNameTextType);
		}
		
		DateType associationBeginDateType = AccountTransferUtil.niemCoreFactory.createDateType();
		com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date associationBeginDate = AccountTransferUtil.basicFactory.createDate();
		associationBeginDate.setValue(AccountTransferUtil.getSystemDate());
		associationBeginDateType.setDate(associationBeginDate);
		personEmploymentAssociationType.setAssociationBeginDate(associationBeginDateType);
		personEmploymentAssociationType.setEmployer(organization);
		EmploymentStatusType employmentStatus = AccountTransferUtil.hixCoreFactory.createEmploymentStatusType();
		EmploymentStatusCodeType employmentStatusCodeType = AccountTransferUtil.hixTypeFactory.createEmploymentStatusCodeType();
		employmentStatusCodeType.setValue(AccountTransferMapperBatch.employeeStatus.get(escOffer.getEmployeeStatus()));
		employmentStatus.setEmploymentStatusCode(employmentStatusCodeType);
		personEmploymentAssociationType.setEmploymentStatus(employmentStatus);
		personEmploymentAssociationType.setESIAugmentation( this.createESIAssociation(member,escOffer) );
		}
		return personEmploymentAssociationType;
	}
	

	private ESIAugmentationType createESIAssociation(AttestationsMember member, EscOffer escOffer){
		ESIAugmentationType eSIAugmentationType = AccountTransferUtil.insuranceApplicationObjFactory.createESIAugmentationType();
		EmployerSponsoredInsuranceType employerSponsoredInsuranceType = AccountTransferUtil.insuranceApplicationObjFactory.createEmployerSponsoredInsuranceType();
		InsurancePlanType insurancePlanType = AccountTransferUtil.hixPMFactory.createInsurancePlanType();
		
		
		insurancePlanType.setMinimumActuarialValueStandardIndicator(AccountTransferUtil.addBoolean(escOffer.getDoNotKnowLcsopPremiumIndicator()));
		employerSponsoredInsuranceType.setESILowestCostPlan(insurancePlanType);
		if(escOffer.getNewEmployeeLcsopPremiumAmount() !=null) {
		InsurancePlanType insurancePlanTypeLCP = AccountTransferUtil.hixPMFactory.createInsurancePlanType();
		InsurancePlanRateType insurancePlanRateType = AccountTransferUtil.hixPMFactory.createInsurancePlanRateType();
		AmountType amount = AccountTransferUtil.niemCoreFactory.createAmountType();
		amount.setValue(escOffer.getNewEmployeeLcsopPremiumAmount());
		insurancePlanRateType.setInsurancePlanRateAmount(amount);
		insurancePlanTypeLCP.setInsurancePlanRate(insurancePlanRateType);
		employerSponsoredInsuranceType.setESILowestCostPlan(insurancePlanTypeLCP);
		}
		
		if(escOffer.getLcsopPremium()!= null) {
		InsurancePlanType insurancePlanTypeELCP = AccountTransferUtil.hixPMFactory.createInsurancePlanType();
		
		InsurancePlanRateType insurancePlanRateTypeELCP = AccountTransferUtil.hixPMFactory.createInsurancePlanRateType();
		AmountType amountELCP = AccountTransferUtil.niemCoreFactory.createAmountType();
		amountELCP.setValue(escOffer.getLcsopPremium());
		insurancePlanRateTypeELCP.setInsurancePlanRateAmount(amountELCP);
		insurancePlanTypeELCP.setInsurancePlanRate(insurancePlanRateTypeELCP);
		employerSponsoredInsuranceType.setESIExpectedLowestCostPlan(insurancePlanTypeELCP);
		}
		/*esiExpectedChangeDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar((java.lang.String) ((JSONObject) ((JSONObject) currentEmployer.get(0)).get("currentEmployerInsurance")).get("employerCoverageEndingDate")));
		esiExpectedChangeDateType.setDate(esiExpectedChangeDate);
		employerSponsoredInsuranceType.setESIExpectedChangeDate(esiExpectedChangeDateType);*/
		employerSponsoredInsuranceType.setESIViaCurrentEmployeeIndicator(AccountTransferUtil.addBoolean(escOffer.getCobraAvailableIndicator()));
		employerSponsoredInsuranceType.setESIRetireePlanIndicator(AccountTransferUtil.addBoolean(escOffer.getRetireePlanCoverageIndicator()));
		employerSponsoredInsuranceType.setESIMinimumAVStandardUnknownIndicator(AccountTransferUtil.addBoolean(escOffer.getDoNotKnowLcsopPremiumIndicator()));
		/* //householdMember.healthCoverage.currentEmployerInsurance:expectedChangesToEmployerCoverageIndicator
		  EmployerInsuranceSponsorshipStatusCodeType employerInsuranceSponsorshipStatusCodeType =hixTypeFactory.createEmployerInsuranceSponsorshipStatusCodeType(); //employerInsuranceSponsorshipStatusCodeType.setValue(
		  EmployerInsuranceSponsorshipStatusCodeSimpleType.valueOf((java.lang.String)((JSONObject)((JSONObject)houseHold.get("healthCoverage")).get("currentEmployerInsurance")).get("expectedChangesToEmployerCoverageIndicator")));
		  employerInsuranceSponsorshipStatusCodeType.setValue(EmployerInsuranceSponsorshipStatusCodeSimpleType.fromValue
		  ((java.lang.String)((JSONObject)((JSONObject)((JSONArray)((JSONObject)houseHold.get("healthCoverage")).get("currentEmployer")).get(0)).get("currentEmployerInsurance")).get("expectedChangesToEmployerCoverageIndicator")));
		  employerSponsoredInsuranceType.setESIExpectedChange(employerInsuranceSponsorshipStatusCodeType);
		*/
		  eSIAugmentationType.setESI(employerSponsoredInsuranceType);
		
		return eSIAugmentationType;
		 
	}
	
}
