package com.getinsured.hix.cap.util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;

import com.getinsured.eligibility.startdates.dto.EligibilityDatesOverrideDTO;
import com.getinsured.hix.crm.web.AdminConsumerController;
import com.getinsured.hix.dto.enrollment.EnrolleeRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.indportal.IndividualPortalUtility;
import com.getinsured.hix.indportal.dto.IndPortalEligibilityDateDetails;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.GIMonitor;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.EnrolleeResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.gimonitor.service.GIMonitorService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;


@Component
public class CapPortalConfigurationUtil {

	@Autowired
	private UserService userService;
	
	@Autowired
	private AppEventService appEventService;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired 
	public GIMonitorService giMonitorService;
	
	@Autowired
	public SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	
    @Autowired private Gson platformGson;
	
	private static final String CASE_NUMBER = "caseNumber";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminConsumerController.class);
	
	
	public void setMemberInfoPermissions(Model model, HttpServletRequest request) throws InvalidUserException {
		AccountUser user = userService.getLoggedInUser();
		Set<String> userPermissions = getUserPermissions(request, user);
		Map<String, Boolean> memberInfoPermissionMap = new HashMap<>();
		for (CapPortalConfigurationConstants.MemberInfo memberInfo : CapPortalConfigurationConstants.MemberInfo
				.values()) {
			if (userPermissions.contains(memberInfo.name())) {
				memberInfoPermissionMap.put(memberInfo.permissionLabel, true);
			} else {
				memberInfoPermissionMap.put(memberInfo.permissionLabel, false);
			}
		}

		/*
		 * Logic to get permission map from request and update it with permissions for member info
		 */
		Map<String, Boolean> permissionMap = (Map) request.getAttribute("permissionMap");
		if(request.getAttribute("permissionMap") != null) {
			LOGGER.info("got the permission map from request scope");
			for(String permission : memberInfoPermissionMap.keySet()) {
				boolean value = memberInfoPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		} else {
			LOGGER.info("creating the permission map from request scope");
			permissionMap = new HashMap<>();
			for(String permission : memberInfoPermissionMap.keySet()) {
				boolean value = memberInfoPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		}
		request.setAttribute("permissionMap", permissionMap);
	}

	public void setMemberNavMenuPermissions(Model model, HttpServletRequest request) throws InvalidUserException {
		AccountUser user = userService.getLoggedInUser();
		Set<String> userPermissions = getUserPermissions(request, user);
		Map<String, Boolean> navMenuPermissionMap = new HashMap<>();
		for (CapPortalConfigurationConstants.NAVMenu navMenu : CapPortalConfigurationConstants.NAVMenu.values()) {
			if (userPermissions.contains(navMenu.name())) {
				navMenuPermissionMap.put(navMenu.permissionLabel, true);
			} else {
				navMenuPermissionMap.put(navMenu.permissionLabel, false);
			}
		}
		/*
		 * Logic to get permission map from request and update it with permissions for Navigation menus
		 */
		Map<String, Boolean> permissionMap = (Map) request.getAttribute("permissionMap");
		if(request.getAttribute("permissionMap") != null) {
			LOGGER.info("got the permission map from request scope");
			for(String permission : navMenuPermissionMap.keySet()) {
				boolean value = navMenuPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		} else {
			permissionMap = new HashMap<>();
			LOGGER.info("creating the permission map from request scope");
			for(String permission : navMenuPermissionMap.keySet()) {
				boolean value = navMenuPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		}
		request.setAttribute("permissionMap", permissionMap);

	}

	public void setEnrollmentInfoPermissions(Model model, HttpServletRequest request) throws InvalidUserException {
		AccountUser user = userService.getLoggedInUser();
		Set<String> userPermissions = getUserPermissions(request, user);
		Map<String, Boolean> enrollmentInfoPermissionMap = new HashMap<>();
		for (CapPortalConfigurationConstants.EnrollmentInfo enrollmentInfo : CapPortalConfigurationConstants.EnrollmentInfo
				.values()) {
			if (userPermissions.contains(enrollmentInfo.name())) {
				enrollmentInfoPermissionMap.put(enrollmentInfo.permissionLabel, true);
			} else {
				enrollmentInfoPermissionMap.put(enrollmentInfo.permissionLabel, false);
			}
		}
		/*
		 * Logic to get permission map from request and update it with permissions for Navigation menus
		 */
		Map<String, Boolean> permissionMap = (Map) request.getAttribute("permissionMap");
		if(request.getAttribute("permissionMap") != null) {
			LOGGER.info("got the permission map from request scope");
			for(String permission : enrollmentInfoPermissionMap.keySet()) {
				boolean value = enrollmentInfoPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		} else {
			permissionMap = new HashMap<>();
			LOGGER.info("creating the permission map from request scope");
			for(String permission : enrollmentInfoPermissionMap.keySet()) {
				boolean value = enrollmentInfoPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		}
		request.setAttribute("permissionMap", permissionMap);

	}

	public void setMemberHistoryPermissions(Model model, HttpServletRequest request) throws InvalidUserException {
		AccountUser user = userService.getLoggedInUser();
		Set<String> userPermissions = getUserPermissions(request, user);
		Map<String, Boolean> memberHistoryPermissionMap = new HashMap<>();
		for (CapPortalConfigurationConstants.MemberHistory memberHistory : CapPortalConfigurationConstants.MemberHistory
				.values()) {
			if (userPermissions.contains(memberHistory.name())) {
				memberHistoryPermissionMap.put(memberHistory.permissionLabel, true);
			} else {
				memberHistoryPermissionMap.put(memberHistory.permissionLabel, false);
			}
		}
		/*
		 * Logic to get permission map from request and update it with permissions for Navigation menus
		 */
		Map<String, Boolean> permissionMap = (Map) request.getAttribute("permissionMap");
		if(request.getAttribute("permissionMap") != null) {
			LOGGER.info("got the permission map from request scope");
			for(String permission : memberHistoryPermissionMap.keySet()) {
				boolean value = memberHistoryPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		} else {
			permissionMap = new HashMap<>();
			LOGGER.info("creating the permission map from request scope");
			for(String permission : memberHistoryPermissionMap.keySet()) {
				boolean value = memberHistoryPermissionMap.get(permission);
				permissionMap.put(permission, value);
			}
		}
		request.setAttribute("permissionMap", permissionMap);
	}

	public Set<String> getUserPermissions(HttpServletRequest request, AccountUser user) {
		String userActiveRoleName=null;
		if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(user.getDefRole().getName()) != 0){
			userActiveRoleName =user.getSwitchedRoleName();
		}else {
			userActiveRoleName = user.getCurrentUserRole();
		}
		return user.getUserPermission(userActiveRoleName);
	}
	
	public void logAppEvent(String eventName, String eventType, Map<String,String> params, HttpServletRequest request){
	       
		try {
			
			Map<String, String> mapEventParam = new HashMap<String, String>();
			EventInfoDto eventInfoDto = new EventInfoDto();
			
			if (eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_APPLICATION)) {
				
				String caseNumber = request.getParameter(CASE_NUMBER);
				mapEventParam.put(IndividualPortalConstants.APPLICATION_ID, caseNumber);
				
			}else if(eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_CSR_OVERRIDES)) {
				
				if(params.containsKey(IndividualPortalConstants.OVERRIDE_COMMENT)) {
					eventInfoDto.setComments(params.get(IndividualPortalConstants.OVERRIDE_COMMENT));
				}
				
				if(params.containsKey(IndividualPortalConstants.CMR_HOUSEHOLD_ID)) {
					eventInfoDto.setModuleId(Integer.valueOf(params.get(IndividualPortalConstants.CMR_HOUSEHOLD_ID)));
					mapEventParam.put(IndividualPortalConstants.CMR_HOUSEHOLD_ID, params.get(IndividualPortalConstants.CMR_HOUSEHOLD_ID));
				}
				
			} else if(eventName.equalsIgnoreCase(IndividualPortalConstants.APPEVENT_PREFERENCES)) {
				String changeDate = dateToString(IndividualPortalConstants.SHORT_DATE_FORMAT, new TSDate());
				mapEventParam.put(IndividualPortalConstants.PREFERENCES_CHANGE_DATE, changeDate);
				
				//HIX-101055
				if(params.containsKey(IndividualPortalConstants.PREV_PREF_COMMUNICATION))
					mapEventParam.put(IndividualPortalConstants.PREFERENCES_PREV_NOTIFICATION, params.get(IndividualPortalConstants.PREV_PREF_COMMUNICATION));
				if(params.containsKey(IndividualPortalConstants.TEXT_MESSAGING_PREF))
					mapEventParam.put(IndividualPortalConstants.PREF_TEXT_MESSAGING, params.get(IndividualPortalConstants.TEXT_MESSAGING_PREF));
				if(params.containsKey(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV))
					mapEventParam.put(IndividualPortalConstants.PREF_PREV_TEXT_MESSAGING, params.get(IndividualPortalConstants.TEXT_MESSAGING_PREF_PREV));
				if(params.containsKey(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION))
				mapEventParam.put(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION, params.get(IndividualPortalConstants.PREFERENCES_NEW_NOTIFICATION));
				
				if(params.containsKey(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE))
					mapEventParam.put(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE, params.get(IndividualPortalConstants._1095_A_NOTIFICATION_PREFERENCE));
				
				if(params.containsKey(IndividualPortalConstants.UPDATED_BY))
					mapEventParam.put(IndividualPortalConstants.UPDATED_BY, params.get(IndividualPortalConstants.UPDATED_BY));
			}
			
			//event type and category
			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(eventName, eventType);
			
			eventInfoDto.setEventLookupValue(lookupValue);
			appEventService.record(eventInfoDto, mapEventParam);
			
		} catch(Exception e){
			LOGGER.error("Exception occured while log Application event",e);
			persistToGiMonitor(e);
		}
	}
	
	public String persistToGiMonitor(Throwable throwable){
		AccountUser user =null;
		String giMonitorId=null;
		try{
			user = userService.getLoggedInUser();
			GIMonitor giMonitor = new GIMonitor();
			//API is setting some other value if errorcode is null
			giMonitor.setComponent("INDPORTAL");
			giMonitor.setEventTime(new TSDate());
			giMonitor.setException(throwable.getClass().getName());
			giMonitor.setExceptionStackTrace(ExceptionUtils.getStackTrace(throwable));
			giMonitor.setUser(user);
			
			giMonitorId = giMonitorService.saveOrUpdateGIMonitor(giMonitor).getId().toString();
			return  giMonitorId;
		}catch(Exception ex){
			LOGGER.error("Exception occured while persisting to GI_MONITOR",ex);
			return giMonitorId;
		}
	}
	
	public String dateToString(String format, Date date){
		
		if(date == null || StringUtils.isBlank(format)){
			return null;
		}
		
		DateFormat formatter = new SimpleDateFormat(format);
		formatter.setLenient(false);
		return formatter.format(date);
	}
	
	private void populateSSAPDetailsForESD(AccountUser user, String caseNumber, int year, IndPortalEligibilityDateDetails indPortalEligibilityDateDetails) throws GIException {

		String currentYear = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_COVERAGE_YEAR);

		int activeModuleId = user.getActiveModuleId();
		List<Object[]> ssapIdToAppStatusList = ssapApplicationRepository
												.findSsapByCmrAppStatusAndCoverageYr(new BigDecimal(activeModuleId), year, Arrays.asList("ER", "EN", "PN"));
		if(ssapIdToAppStatusList == null || ssapIdToAppStatusList.isEmpty() || ssapIdToAppStatusList.size() != 2) {
			throw new GIException("Required data not found for SSAP application with CMR Household Id: " + activeModuleId + " and Case number: " + caseNumber);
		}

		for(Object[] ssapIdToAppStatus : ssapIdToAppStatusList) {

			String appStatus = ssapIdToAppStatus[1].toString();
			switch(appStatus) {
				case "EN":
					indPortalEligibilityDateDetails.setEnrolledAppId(ssapIdToAppStatus[0].toString());
					break;
				case "PN":
					indPortalEligibilityDateDetails.setEnrolledAppId(ssapIdToAppStatus[0].toString());
					break;
				case "ER":
					indPortalEligibilityDateDetails.setSepAppId(ssapIdToAppStatus[0].toString());
					break;
				default:
					throw new GIException("Invalid SSAP application found with CMR Household Id: : " + activeModuleId
							+ ", Case number: " + caseNumber + " and Application Status: " + appStatus);
			}
		}

	}
	
	/**
	 * Fetch existing Enrollment and APTC Effective date details for an application
	 * @param caseNumber
	 * @param year 
	 * @return
	 * @throws GIException
	 */
	public IndPortalEligibilityDateDetails fetchEligibilityDetailsForAppln(String caseNumber, int year, int cmrHouseholdid) throws GIException {

		LOGGER.debug("Fetching existing Enrollment and APTC Effective date details for application "+ caseNumber);

		IndPortalEligibilityDateDetails indPortalEligibilityDateDetails = null;
		IndividualPortalUtility indPortalUtil = new IndividualPortalUtility();
		SimpleDateFormat formatter = new SimpleDateFormat(IndividualPortalConstants.SHORT_DATE_FORMAT);

		try {
			AccountUser user = userService.getLoggedInUser();

			indPortalEligibilityDateDetails = new IndPortalEligibilityDateDetails();
			user.setActiveModuleId(cmrHouseholdid);

			populateSSAPDetailsForESD(user, caseNumber, year, indPortalEligibilityDateDetails);

			Long enrolledAppId =  Long.valueOf(indPortalEligibilityDateDetails.getEnrolledAppId());
			Map<String, EnrollmentShopDTO> enrollments = getEnrollmentDetails(enrolledAppId);

			boolean dentalOnly = false;
			EnrollmentShopDTO enrollmentShopDTO = null;
			if(enrollments.containsKey(IndividualPortalConstants.HEALTH_PLAN)){
				enrollmentShopDTO = enrollments.get(IndividualPortalConstants.HEALTH_PLAN);
			}else{
				enrollmentShopDTO = enrollments.get(IndividualPortalConstants.DENTAL_PLAN);
				dentalOnly = true;
			}
			
			EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO = fetchFinEffDateFroromELigibilty(user, enrollmentShopDTO, indPortalEligibilityDateDetails, dentalOnly);

			indPortalEligibilityDateDetails.setCmrHouseholdId(String.valueOf(user.getActiveModuleId()));
			indPortalEligibilityDateDetails.setEnrollmentStartDate(formatter.format(enrollmentShopDTO.getCoverageStartDate()));
			indPortalEligibilityDateDetails.setEnrollmentEndDate(formatter.format(enrollmentShopDTO.getCoverageEndDate()));
			indPortalEligibilityDateDetails.setFinancialEffectiveDate(formatter.format(eligibilityDatesOverrideDTO.getFinancialEffectiveDate()));
			indPortalEligibilityDateDetails.setStatus(eligibilityDatesOverrideDTO.getStatus());

		} catch(Exception e) {
			LOGGER.error("Error fetching existing Enrollment and APTC Effective date details for application "+ caseNumber, e);
			throw new GIException(e);
		}
		return indPortalEligibilityDateDetails;
	}
	
	
	public EligibilityDatesOverrideDTO fetchFinEffDateFroromELigibilty(AccountUser user, EnrollmentShopDTO enrollmentShopDTO,
			IndPortalEligibilityDateDetails indPortalEligibilityDateDetails, boolean dentalOnly) throws GIException {

		EligibilityDatesOverrideDTO eligibilityDatesOverrideDTO = null;

		try {

			eligibilityDatesOverrideDTO = new EligibilityDatesOverrideDTO();
			eligibilityDatesOverrideDTO.setEditMode(false);
			eligibilityDatesOverrideDTO.setCmrHouseholdId(Long.valueOf(user.getActiveModuleId()));
			eligibilityDatesOverrideDTO.setEnrollmentStartDate(enrollmentShopDTO.getCoverageStartDate());
			eligibilityDatesOverrideDTO.setEnrollmentEndDate(enrollmentShopDTO.getCoverageEndDate());
			eligibilityDatesOverrideDTO.setEnAppId(Long.valueOf(indPortalEligibilityDateDetails.getEnrolledAppId()));
			eligibilityDatesOverrideDTO.setErAppId(Long.valueOf(indPortalEligibilityDateDetails.getSepAppId()));
			eligibilityDatesOverrideDTO.setDentalOnly(dentalOnly);

			LOGGER.debug("Getting Financial Effective Date details from Eligibility for: " + eligibilityDatesOverrideDTO);

			ResponseEntity<EligibilityDatesOverrideDTO> response =  ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL + "eligibility/dates/override",
					user.getUsername(), HttpMethod.POST, MediaType.APPLICATION_JSON, EligibilityDatesOverrideDTO.class, eligibilityDatesOverrideDTO);

			if(response != null && response.getStatusCode().equals(HttpStatus.OK)){
				eligibilityDatesOverrideDTO = response.getBody();
			}

		} catch(Exception e) {
			LOGGER.error("Error fetching Financial Effective Date details from Eligibility", e);
			throw new GIException(e);
		}

		return eligibilityDatesOverrideDTO;
	}
	
	//Used for coverage start date computation
	public Map<String, EnrollmentShopDTO> getEnrollmentDetails(long applicationId){

		List<EnrollmentShopDTO> enrollmentShopDTOs = null;
		EnrolleeResponse enrolleeResponse = null;
		Map<String, EnrollmentShopDTO> enrollments = new HashMap<String, EnrollmentShopDTO>();

		try {

				AccountUser user = this.userService.getLoggedInUser();

				String userName = user == null ? IndividualPortalConstants.EXCHANGE_ADMIN  : user.getUsername();

				EnrolleeRequest enrolleeRequest = new EnrolleeRequest();
				enrolleeRequest.setSsapApplicationId(applicationId);
				
				String response = ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.FIND_ENROLLEE_BY_APPLICATION_ID_JSON,
						//ENROLLEE_BY_APPLICATION_ID_JSON,
						userName, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class,platformGson.toJson(enrolleeRequest)).getBody();

				if (null != response){
					enrolleeResponse = platformGson.fromJson(response, EnrolleeResponse.class);
					if(enrolleeResponse != null
						&& enrolleeResponse.getStatus().equalsIgnoreCase(GhixConstants.RESPONSE_SUCCESS)) {
						enrollmentShopDTOs = enrolleeResponse.getEnrollmentShopDTOList();
						List<EnrollmentShopDTO> enrollmentForHealth = new ArrayList<EnrollmentShopDTO>();
						List<EnrollmentShopDTO> enrollmentForDental = new ArrayList<EnrollmentShopDTO>();

						for (EnrollmentShopDTO enrollmentShopDto : enrollmentShopDTOs) {
							if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.HEALTH_PLAN)){
								enrollmentForHealth.add(enrollmentShopDto);
							}else if(enrollmentShopDto.getPlanType().equalsIgnoreCase(IndividualPortalConstants.DENTAL_PLAN)){
								enrollmentForDental.add(enrollmentShopDto);
							}
						}

						if(enrollmentForHealth.size()>1){
							Collections.sort(enrollmentForHealth, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
						}
						if(!enrollmentForHealth.isEmpty()){
							enrollments.put(IndividualPortalConstants.HEALTH_PLAN, enrollmentForHealth.get(0));
						}
						if(enrollmentForDental.size()>1){
							Collections.sort(enrollmentForDental, (enrollmentId1, enrollmentId2) -> Long.valueOf(enrollmentId2.getEnrollmentCreationTimestamp().getTime()).compareTo(Long.valueOf(enrollmentId1.getEnrollmentCreationTimestamp().getTime())));
						}
						if(!enrollmentForDental.isEmpty()){
							enrollments.put(IndividualPortalConstants.DENTAL_PLAN, enrollmentForDental.get(0));
						}

					}
					else{
						throw new GIException("Unable to get Enrollment Plan Details.");
					}
				}
		} catch (Exception e) {
			LOGGER.error("Exception occured while fetching enrollment details : ", e);
			persistToGiMonitor(e);
			throw new GIRuntimeException(IndividualPortalConstants.ENROLLMENT_EXCEPTION_MESSAGE);
		}
		return enrollments;
	}
	
	/**
	 * set cmrHouseholdId as activeModuleId in user
	 * @param cmrHouseholdId
	 * @throws InvalidUserException
	 */
	public void setActiveModuleId(String cmrHouseholdId)  throws InvalidUserException {
		AccountUser user = this.userService.getLoggedInUser();
		user.setActiveModuleId(Integer.parseInt(cmrHouseholdId));
	}

}
