#!/bin/bash
echo Terminating EC2 instances
echo Enter your Ansible Vault password when asked, it is usually usual ghix password.
ansible-playbook -i hosts --ask-vault-pass terminate.yml
