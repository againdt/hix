<%@ include file="../include.jsp" %>
<div id="appscr73" style="display:none;">
	<div class="row-fluid">
		<div class="page-title">
			<h4 class="titleForPages"><spring:message code="ssap.page23.incomeDiscrepancies" text=" Discrepancies - Additional Income Questions"/></h4>
		</div>
		<form id="appscr73Form" class="form-horizontal">
			<div class="header">
				<h4><spring:message code="ssap.menu.income" text="Income" /></h4>
			</div>
			<div class="gutter10">
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.did" text=" Did"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page23.stopWorkingAt" text=" stop working at"/>
					<span class="appscr73EmployerName"></span>
					<spring:message code="ssap.page23.withinTheLast" text="within the last"/>
					<span class="fromLastNumOfMonths"></span>
					<spring:message code="ssap.page23.months" text=" months?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73stopWorkingIndicatorYes">
						<input type="radio" id="appscr73stopWorkingIndicatorYes" name="stopWorking" value="yes">
						<spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73stopWorkingIndicatorNo">
						<input type="radio" id="appscr73stopWorkingIndicatorNo" name="stopWorking" value="no">
						<spring:message code="ssap.page23.no" text="No"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.did" text="Did"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page23.everWorkAt" text="ever work at"/>
					<span class="appscr73EmployerName"></span>
					<spring:message code="ssap.page23.questionMark" text="?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73WorkingIndicatorYes">
						<input type="radio" id="appscr73WorkingIndicatorYes" name="workAt" value="yes">
						<spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73WorkingIndicatorNo">
						<input type="radio" id="appscr73WorkingIndicatorNo" name="workAt" value="no">
						<spring:message code="ssap.page23.no" text="No"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.have" text=" Have"/>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page23.hoursDecreasedAt" text=" 's hours decreased at "/>
					<span class="appscr73EmployerName"></span>
					<spring:message code="ssap.page23.duringTheLast" text=" during the last "/>
					<span class="fromLastNumOfMonths"><spring:message code="ssap.page23.one" text="1"/></span>
					<spring:message code="ssap.page23.months" text="months?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73DecresedAtYes">
						<input  type="radio" id="appscr73DecresedAtYes" name="decresedAt" value="yes" />
						<spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73DecresedAtNo">
						<input type="radio" id="appscr73DecresedAtNo" name="decresedAt" value="no" />
						<spring:message code="ssap.page23.no" text="No"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.has" text="Has"/>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page23.wageOrSalaryBeenCut" text="'s wage or salary been cut at"/>
					<span class="appscr73EmployerName"></span>
					<spring:message code="ssap.page23.duringTheLast" text="during the last"/>
					<span class="fromLastNumOfMonths"><spring:message code="ssap.page23.one" text="1"/></span>
					<spring:message code="ssap.page23.months" text="months?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73SalaryAtYes">
						<input type="radio" id="appscr73SalaryAtYes" name="salaryAt" value="yes" />
						<spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73SalaryAtNo">
						<input type="radio" id="appscr73SalaryAtNo" name="salaryAt" value="no" />
						<spring:message code="ssap.page23.no" text="No"/>
					</label>
					<p>
						<spring:message code="ssap.page23.resonForPaycheckAmountIsLower" text="The paycheck amount might be lower than the gross amount we are asking for here because taxes or other deductions are taken out of most paychecks. Double check the amount you listed for"/>
						<span class="appscr73EmployerName"></span>
						<spring:message code="ssap.page23.grossAmount" text=" is the gross amount."/>
					</p>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.isThereAnotherExplanation " text="Is there another explanation for why the amount reported for "/>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page23.jobIncomeIsLowerThanWhatOurElectronicRecordsShow" text="'s job income is lower than what our electronic records show?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<div for="explanationTextArea" class="radio">
						<textarea id="explanationTextArea" name="explanation" rows="2" cols="80" 
						placeholder="Explanation" aria-label="Explanation"></textarea>
					</div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.is" text="Is "/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page23.AseasonalWorker" text="a seasonal worker?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73SesWorkerYes">
						<input type="radio" id="appscr73SesWorkerYes" name="sesWorker" value="yes">
						<spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73SesWorkerNo">
						<input type="radio" id="appscr73SesWorkerNo"  name="sesWorker" value="no" >
						<spring:message code="ssap.page23.no" text="No"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.explanation" text="Explain why you told us that"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page23.willClaimMoreDependents" text="will claim more dependents on"/>
					<strong class="nameOfHouseHold"></strong><spring:message code="ssap.page23.taxReturnThanThereWereOn" text="'s tax return than there were on"/>
					<strong class="nameOfHouseHold"></strong>
					<spring:message code="ssap.page23.federalIncomeTaxReturn" text="- federal income tax return in [IRS reported tax year]"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<div class="radio" for="appscr73ExplanationTaxtArea">
						<textarea id="appscr73ExplanationTaxtArea" name="itTax"  rows="2" cols="80" 
						placeholder="Explanation" aria-label="Explanation"></textarea>
					</div>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			
			<div class="control-group">
				<p>
					<spring:message code="ssap.page23.IsThereAnyOtherIncomeThePeopleAbovegetIncludingJointIncome" text="Is there any other income the people above get, including joint income?"/>
				</p><!-- end of label -->
				<div class="margin5-l">
					<label class="radio" for="appscr73OtherJointIncomeYes">
						<input type="radio" id="appscr73OtherJointIncomeYes" name="OtherJointIncome" value="yes"><spring:message code="ssap.page23.yes" text="Yes"/>
					</label>
					<label class="radio" for="appscr73OtherJointIncomeNo">
						<input type="radio" id="appscr73OtherJointIncomeNo" name="OtherJointIncome" value="no" ><spring:message code="ssap.page23.no" text="No"/>
					</label>
				</div><!-- end of controls-->
			</div><!-- control-group ends -->
			</div>
		</form>
	</div>
</div> 
