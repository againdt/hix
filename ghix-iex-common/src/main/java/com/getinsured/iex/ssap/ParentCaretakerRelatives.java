
package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.google.gson.annotations.Expose;

/**
 * @author chopra_s
 * 
 */
public class ParentCaretakerRelatives {

    @Expose
    private String relationship;
    @Expose
    private Date dateOfBirth;
    @Expose
    private Boolean mainCaretakerOfChildIndicator=false;
    @Expose
    private Name name=new Name();
    @Expose
    private Boolean liveWithAdoptiveParentsIndicator=false;
    @Expose
    private List<Integer> personId = new ArrayList<Integer>();
    @Expose
    private Boolean anotherChildIndicator=false;

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getMainCaretakerOfChildIndicator() {
        return mainCaretakerOfChildIndicator;
    }

    public void setMainCaretakerOfChildIndicator(Boolean mainCaretakerOfChildIndicator) {
        this.mainCaretakerOfChildIndicator = mainCaretakerOfChildIndicator;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

    public Boolean getLiveWithAdoptiveParentsIndicator() {
        return liveWithAdoptiveParentsIndicator;
    }

    public void setLiveWithAdoptiveParentsIndicator(Boolean liveWithAdoptiveParentsIndicator) {
        this.liveWithAdoptiveParentsIndicator = liveWithAdoptiveParentsIndicator;
    }

    public List<Integer> getPersonId() {
        return personId;
    }

    public void setPersonId(List<Integer> personId) {
        this.personId = personId;
    }

    public Boolean getAnotherChildIndicator() {
        return anotherChildIndicator;
    }

    public void setAnotherChildIndicator(Boolean anotherChildIndicator) {
        this.anotherChildIndicator = anotherChildIndicator;
    }

}
