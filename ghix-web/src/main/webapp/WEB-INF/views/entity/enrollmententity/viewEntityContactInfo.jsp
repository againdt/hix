<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%> 
<link href="<gi:cdnurl value="/resources/css/entity-custom.css" />" media="screen" rel="stylesheet" type="text/css" />

<div class="gutter10">
	<!--<div class="l-page-breadcrumb">
		
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li>
				<li><a
					href="<c:url value="/entity/enrollmententity/viewentitycontactinfo"/>"><spring:message code="label.account" /></a></li>
				
			</ul>
		</div>
	</div><!--page-breadcrumb ends-->

<div class="row-fluid">
	<%-- <div class="page-header">
		<h1><spring:message code="label.entity.primaryContact"/></h1>
	</div>  Removed as part of HIX-33915 --%>
	<c:if test="${loggedUser =='entityAdmin'}">
	<div id="titlebar">
		<h1 style="margin-top: 0px;margin-bottom: 0px;"><a name="skip"></a>${enrollmentEntity.entityName}</h1>
	</div>	
	</c:if>  
</div>

	<div class="row-fluid">
			
			<jsp:include page="../leftNavigationMenu.jsp" />
			<div class="span9" id="rightpanel">
			 <div class="header">  
			 	<h4 class="pull-left"><spring:message code="label.entity.primaryContact"/></h4>
			 	<c:set var="enrollmentEntityId" ><encryptor:enc value="${enrollmentEntity.id}" isurl="true" /> </c:set>
	           	<c:choose>          
	         	 	<c:when test="${loggedUser =='entityAdmin' && enrollmentEntity.registrationStatus!='Incomplete'}">         
	             		<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entitycontactinfo/${enrollmentEntityId}"/>"><spring:message  code="label.edit"/></a>
	             	</c:when> 
	             	<c:when test="${CA_STATE_CODE}">
	             		<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus =='Incomplete'}">
	             			<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entitycontactinfo"/>"><spring:message  code="label.edit"/></a>
	             		</c:if>
					</c:when>
					<c:otherwise>
						<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus !='Pending'}">
							<a class="btn btn-small pull-right" href="<c:url value="/entity/enrollmententity/entitycontactinfo"/>"><spring:message  code="label.edit"/></a>
						</c:if>
					</c:otherwise>	             		
	             </c:choose>
             </div>
             	<form class="form-horizontal" id="frmviewEntityContact" name="frmviewEntityContact" action="viewentitycontactinfo" method="POST">
             <df:csrfToken/>
				<div class="row-fluid">					
					<div class="span">
					<table class="table table-border-none table-condensed">
						<tbody>
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.entity.name"/></td>
								<td><strong>${enrollmentEntity.priContactName}</strong></td>
							</tr>

							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.entity.email"/></td>
								<td><strong>${enrollmentEntity.priContactEmailAddress}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code="label.entity.enterPrimaryPhoneNumber"/></td>
								<td><strong> <c:if test="${enrollmentEntity.priContactPrimaryPhoneNumber!=null && not empty enrollmentEntity.priContactPrimaryPhoneNumber}">
										(${priContactPrimaryPhoneNumber1})${priContactPrimaryPhoneNumber2}-${priContactPrimaryPhoneNumber3}
									</c:if>
								</strong></td>
							</tr>

							<tr>
								<td class="txt-right"><spring:message code="label.entity.enterSecondaryPhoneNumber"/></td>
								<td><strong> <c:if test="${enrollmentEntity.priContactSecondaryPhoneNumber!=null && not empty enrollmentEntity.priContactSecondaryPhoneNumber}">
										(${priContactSecondaryPhoneNumber1})${priContactSecondaryPhoneNumber2}-${priContactSecondaryPhoneNumber3}
									</c:if>
								</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code="label.entity.howWouldthisPersonLiketobeContacted"/></td>
								<td><strong>${enrollmentEntity.priCommunicationPreference}</strong></td>
							</tr>
							
						</tbody>
					</table>
				</div>
				</div>
				
			
					<div class="header">  
						<h4><spring:message code="label.entity.financialContact"/></h4>
					</div>
					<table class="table table-border-none table-condensed">
						<tbody>
							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.entity.name"/></td>
								<td><strong>${enrollmentEntity.finContactName}</strong></td>
							</tr>

							<tr>
								<td class="span4 txt-right" scope="row"><spring:message code="label.entity.email"/></td>
								<td><strong>${enrollmentEntity.finEmailAddress}</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code="label.entity.enterPrimaryPhoneNumber"/></td>
								<td><strong> <c:if test="${enrollmentEntity.finPrimaryPhoneNumber!='null' && not empty enrollmentEntity.finPrimaryPhoneNumber}">
										(${finPrimaryPhoneNumber1})${finPrimaryPhoneNumber2}-${finPrimaryPhoneNumber3}
									</c:if>
								</strong></td>
							</tr>

							<tr>
								<td class="txt-right"><spring:message code="label.entity.enterSecondaryPhoneNumber"/></td>
								<td><strong> <c:if test="${enrollmentEntity.finFaxNumber!=null && not empty enrollmentEntity.finFaxNumber}">
										(${finFaxNumber1})${finFaxNumber2}-${finFaxNumber3}
									</c:if>
								</strong></td>
							</tr>
							<tr>
								<td class="txt-right"><spring:message code="label.entity.howWouldthisPersonLiketobeContacted"/></td>
								<td><strong>${enrollmentEntity.finCommunicationPreference}</strong></td>
							</tr>
							
						</tbody>
					</table>
			<c:if test="${loggedUser !='entityAdmin' && enrollmentEntity.registrationStatus=='Incomplete'}">
							<div class="form-actions">
								<a class="btn btn-primary offset8" id="label.entity.next" onClick="window.location.href='/hix/entity/assister/registration'"><spring:message code="label.entity.next"/></a>
							</div>
				   </c:if>	
           </form>
           </div>
           </div>
   </div> 

<script type="text/javascript">

$("#entityContacts").removeClass("link");
$("#entityContacts").addClass("active");
</script>
