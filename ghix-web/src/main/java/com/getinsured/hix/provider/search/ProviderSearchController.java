package com.getinsured.hix.provider.search;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.dto.plandisplay.ProviderSearchRequest;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.provider.service.zipcode.SolrZipCodeService;
import com.getinsured.hix.provider.service.zipcode.ZipCode;

@Controller
public class ProviderSearchController {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderSearchController.class);
	
	private static final String SEARCH_KEY = "searchKey";
	private static final String USER_ZIP = "userZip";
	private static final String GROUP_KEY = "groupkey";
	private static final String OTHER_LOCATION = "otherLocation";
	private static final String OTHER_DISTANCE = "otherDistance";
	private static final String LANGUAGE = "language";
	private static final String GENDER = "gender";
	private static final String CURRENT_PAGE = "currentPage";
	private static final String SEARCH_TYPE = "searchType";
	private static final String PROVIDER_SPECIALTY = "providerSpecialty";
	private static final String PAGE_SIZE = "pageSize";
	private static final String NETWORK_ID = "networkId";
	private static final String ERROR = "error";
    private static final String STATUS = "status";

	@Autowired private SolrProxyImpl solrProxy;
	@Autowired private SolrZipCodeService solrZipCodeService;       
	@Autowired private ZipCodeService zipCodeService;
	@Autowired private GhixRestTemplate ghixRestTemplate;

	private ZipCode getUserLocation(String userZip){
		if(userZip != null){
			List<ZipCode> userLocationList = this.solrZipCodeService.getZipList(userZip);
			if(userLocationList != null && userLocationList.size() > 0){
				ZipCode zip = userLocationList.get(0);
				return zip;
			}else{
				LOGGER.error("failed to retrieve user's location using zip code:"+userZip);
			}		
		}
		return null;
	}

	@RequestMapping(value = "/provider/doctors/search",method = RequestMethod.GET, headers="Accept=*/*")
	public @ResponseBody String getProvidersSearchResults (Model model, @RequestParam(value = SEARCH_KEY, required = true) String searchKey,
			@RequestParam(value = USER_ZIP, required = true) String userZip,@RequestParam(value = CURRENT_PAGE, required = true) String currentPage
			,@RequestParam(value = LANGUAGE, required = false) String language
			,@RequestParam(value = GENDER, required = false) String gender
			,@RequestParam(value = OTHER_LOCATION, required = false) String otherLocation
			,@RequestParam(value = OTHER_DISTANCE, required = false) String otherDistance
			,@RequestParam(value = SEARCH_TYPE, required = false) String searchType
			,@RequestParam(value = PROVIDER_SPECIALTY, required = false) String providerSpecialty
			,@RequestParam(value = PAGE_SIZE, required = false) String pageSize
			,@RequestParam(value = NETWORK_ID, required = false) String networkId, HttpServletResponse httpResponse)
	{
		ZipCode userLocation = null;
		boolean findByCity = false;
		int currentPageNumber = 1;
		int currentPageSize = 3;
		
		if(!StringUtils.isBlank(currentPage)){
			currentPageNumber = Integer.parseInt(currentPage);
		}
		
		if(!StringUtils.isBlank(pageSize)){
			
			try {
			currentPageSize = Integer.parseInt(pageSize);
			}
			catch(NumberFormatException e)
			{
				httpResponse.setHeader("FailureReason", "Invalid value for pageSize. It should be less than 100.");
				httpResponse.setStatus(521);
				return "";
			}
		}
		
		if(currentPageSize > 100) {
			httpResponse.setHeader("FailureReason", "Invalid value for pageSize. It should be less than 100.");
			httpResponse.setStatus(521);
			return "";
		}
		  		
		int recordStartNumber = currentPageNumber!=0?((currentPageNumber-1)*currentPageSize):0;
		currentPageNumber = currentPageNumber-1;
		float distance = 25.0f; // Default 25 Miles

		if(StringUtils.isNotBlank(otherDistance)){
		try {
			distance = Integer.valueOf(otherDistance);
			}
			catch(NumberFormatException e)
			{
				httpResponse.setHeader("FailureReason", "Invalid value for otherDistance. It should be less than 100.");
				httpResponse.setStatus(521);
				return "";
			}
		} 
		
		if(distance > 100) {
			httpResponse.setHeader("FailureReason", "Invalid value for otherDistance. It should be less than 100.");
			httpResponse.setStatus(521);
			return "";
		}

		/* We will set it as per request from preferances.jsp, for now keeping it blank*/
		String providerType = "";

		if(searchKey!=null){
			try {
				String providerSearchAPI = DynamicPropertiesUtil.getPropertyValue("planSelection.providerSearchAPI");
				if(StringUtils.isNotBlank(providerSearchAPI) && ("vericred".equalsIgnoreCase(providerSearchAPI) || "betterDoctor".equalsIgnoreCase(providerSearchAPI))){
					if("vericred".equalsIgnoreCase(providerSearchAPI)) {						
						if("hospital".equalsIgnoreCase(searchType)) {
							searchType = "organization";
						}else{
							searchType = "individual";
						}						
					}
					ProviderSearchRequest providerSearchRequest = createProviderSearchRequest(currentPageSize, currentPage, otherDistance, searchType, userZip, searchKey);
				
					String response = ghixRestTemplate.postForObject(GhixEndPoints.PlandisplayEndpoints.GET_PROVIDERS_SEARCH_RESULTS, providerSearchRequest, String.class);
					/*ObjectReader objreader = JacksonUtils.getJacksonObjectReaderForHashMap(String.class, Object.class);
					Map<String,Object> responseData = objreader.readValue(response);*/
					JSONParser jsonParser = new JSONParser();
					JSONObject responseData = (JSONObject) jsonParser.parse(response);
				
					return generateResponse(currentPage, currentPageSize, responseData);
				}else{
					if(StringUtils.isNotBlank(otherLocation)){
						if(!Character.isDigit(otherLocation.charAt(0))){
							findByCity = true;
							//find by city doesn't care about the zip code
						}else{
							userLocation = this.getUserLocation(otherLocation);
						}
					}else{
						userLocation = this.getUserLocation(userZip);
					}
					if(findByCity){
						LOGGER.debug("Searching provider by city:"+otherLocation);
						return solrProxy.findProvidersByCity(searchKey, otherLocation, currentPageSize, recordStartNumber, language, gender,providerType,searchType,providerSpecialty);
					}else{
						LOGGER.debug("Searching provider by zip: User Location:"+userZip+" provided location:"+otherLocation);
						return solrProxy.findProviders(searchKey, userLocation, currentPageSize, recordStartNumber, language, gender, distance,providerType,searchType,providerSpecialty,networkId);
					}
				}
			} catch (Exception e) {
				LOGGER.error("Error in find provider",e);
				return "";					
			}
		}
		return "";
	}

	@RequestMapping(value = "/provider/doctors/details",method = RequestMethod.GET, headers="Accept=*/*")
	public @ResponseBody String getProvidersDetails (Model model, @RequestParam(value = USER_ZIP, required = true) String userZip,
			@RequestParam(value = GROUP_KEY, required = true) String groupKey)
	{
		ZipCode userLocation = this.getUserLocation(userZip);
		if(groupKey!=null && !(groupKey.isEmpty())){
			try {
				return solrProxy.findProvidersDetails(groupKey,userLocation);
			} catch (Exception e) {
				LOGGER.error("Error in provider details",e);
			}
		}
		return null;
	}

	@RequestMapping(value = "/provider/getLocationList",method = RequestMethod.GET, headers="Accept=*/*")
	public @ResponseBody List<String> getLocationList(@RequestParam("term") String query) {
		List<String> locationList = new ArrayList<String>();
		if(Character.isDigit(query.charAt(0)) && query.length() >= 3){
			for(Object zipCode : zipCodeService.getZipList(query)){
				locationList.add((String) zipCode);
			}
		}else{
			for(Object city : zipCodeService.getCityList(query.toUpperCase())){
				locationList.add((String) city);
			}
		}
		return locationList;
	}
	
	@SuppressWarnings("unchecked")
	private String generateResponse(String currentPage, int currentPageSize, JSONObject responseData) {
		JSONObject respObj = new JSONObject();		
		respObj.put("total_hits", 0);
		respObj.put("current_start", Integer.parseInt(currentPage));
		respObj.put("page_size", currentPageSize);
		respObj.put("next_start", currentPageSize);			
		
		JSONArray providers = (JSONArray)responseData.get("providers");
		respObj.put("providers", providers);	
		if(responseData.get("total") != null){
			respObj.put("total_hits", responseData.get("total"));
		}
		String status = (String)responseData.get(STATUS);
		respObj.put(STATUS, status);
		if(GhixConstants.RESPONSE_FAILURE.equals(status)){
			respObj.put(ERROR, (String)responseData.get(ERROR));
		}					
		return respObj.toJSONString();
	}

	private ProviderSearchRequest createProviderSearchRequest(int currentPageSize, String currentPage, String other_distance, String requestType, String userZip, String searchKey) {
		ProviderSearchRequest providerSearchRequest = new ProviderSearchRequest();
		int page = Integer.parseInt(currentPage) > 0 ? Integer.parseInt(currentPage) : 1;
		float radius = 0; // Default 25 Miles
		if(StringUtils.isNotBlank(other_distance)){
			radius = Integer.valueOf(other_distance);
		}
		if (radius == 0 && StringUtils.isNotBlank(requestType) && requestType.toLowerCase().contains("searchpage")){
			radius = 50f;
		}		
		
		providerSearchRequest.setPage(page);
		providerSearchRequest.setRadius(radius);
		providerSearchRequest.setType(requestType);
		providerSearchRequest.setZip_code(userZip);
		providerSearchRequest.setSearch_term(searchKey);
		providerSearchRequest.setPer_page(currentPageSize);		
		
		return providerSearchRequest;
	}

}