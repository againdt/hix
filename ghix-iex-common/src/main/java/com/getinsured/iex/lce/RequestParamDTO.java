package com.getinsured.iex.lce;

import java.util.List;

public class RequestParamDTO {
	
	List<ChangedApplicant> changedApplicants;
	boolean dataChanged;
	String eventCategory;
	String eventSubCategory;
	String eventSubCategoryDate;
	
	public List<ChangedApplicant> getChangedApplicants() {
		return changedApplicants;
	}
	public void setChangedApplicants(List<ChangedApplicant> changedApplicants) {
		this.changedApplicants = changedApplicants;
	}
	public boolean isDataChanged() {
		return dataChanged;
	}
	public void setDataChanged(boolean dataChanged) {
		this.dataChanged = dataChanged;
	}
	public String getEventCategory() {
		return eventCategory;
	}
	public void setEventCategory(String eventCategory) {
		this.eventCategory = eventCategory;
	}
	public String getEventSubCategory() {
		return eventSubCategory;
	}
	public void setEventSubCategory(String eventSubCategory) {
		this.eventSubCategory = eventSubCategory;
	}
	public String getEventSubCategoryDate() {
		return eventSubCategoryDate;
	}
	public void setEventSubCategoryDate(String eventSubCategoryDate) {
		this.eventSubCategoryDate = eventSubCategoryDate;
	}

}
