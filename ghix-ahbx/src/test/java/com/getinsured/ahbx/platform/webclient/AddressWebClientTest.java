package com.getinsured.ahbx.platform.webclient;

import static org.junit.Assert.assertNotNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.PlatformRequest;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public class AddressWebClientTest {

	@Autowired private AddressWebClient addressWebClient;
	@Test
	public void testSendRequest() {
		Location address = new Location();
		address.setAddress1("3200 E camelback rd");
		address.setState("CA");
		address.setZip("12345");

		PlatformRequest platformRequest = new PlatformRequest();
		platformRequest.setAddress(address);

		String response = addressWebClient.sendRequest(platformRequest);
		assertNotNull(response);

		//System.out.println("Response - " + response);

	}

}
