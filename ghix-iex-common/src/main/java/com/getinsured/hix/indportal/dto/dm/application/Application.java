package com.getinsured.hix.indportal.dto.dm.application;

import java.util.ArrayList;

public class Application {
	 private BestSEP bestSEP;
	 private String finalQhpEffectiveStartDate;
	 private String finalQhpEffectiveEndDate;
	 private ArrayList<EnrollmentGroup> enrollmentGroups;

	
	 // Getter Methods 

	 public ArrayList<EnrollmentGroup> getEnrollmentGroups() {
		return enrollmentGroups;
	}

	public void setEnrollmentGroups(ArrayList<EnrollmentGroup> enrollmentGroups) {
		this.enrollmentGroups = enrollmentGroups;
	}

	public BestSEP getBestSEP() {
	  return bestSEP;
	 }

	 public String getFinalQhpEffectiveStartDate() {
	  return finalQhpEffectiveStartDate;
	 }

	 public String getFinalQhpEffectiveEndDate() {
	  return finalQhpEffectiveEndDate;
	 }

	 // Setter Methods 

	 public void setBestSEP(BestSEP bestSEP) {
	  this.bestSEP = bestSEP;
	 }

	 public void setFinalQhpEffectiveStartDate(String finalQhpEffectiveStartDate) {
	  this.finalQhpEffectiveStartDate = finalQhpEffectiveStartDate;
	 }

	 public void setFinalQhpEffectiveEndDate(String finalQhpEffectiveEndDate) {
	  this.finalQhpEffectiveEndDate = finalQhpEffectiveEndDate;
	 }
	}
