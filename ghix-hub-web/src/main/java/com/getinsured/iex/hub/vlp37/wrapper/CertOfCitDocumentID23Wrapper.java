package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.CertOfCitDocumentID23Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;


public class CertOfCitDocumentID23Wrapper implements JSONAware{
	
	private ObjectFactory factory;
	private CertOfCitDocumentID23Type certOfCitDocumentID23Type;
	
	private String alienNumber;
	private String citizenshipNumber;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String CITIZENSHIP_NUMBER_KEY = "CitizenshipNumber";
	

	public CertOfCitDocumentID23Wrapper(){
		this.factory = new ObjectFactory();
		this.certOfCitDocumentID23Type = factory.createCertOfCitDocumentID23Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.certOfCitDocumentID23Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setCitizenshipNumber(String citizenshipNumber) throws HubServiceException{
		
		if(citizenshipNumber == null){
			throw new HubServiceException("No Citizenship number provided");
		}
		
		this.citizenshipNumber = citizenshipNumber;
		this.certOfCitDocumentID23Type.setCitizenshipNumber(citizenshipNumber);
	}
	
	public String getCitizenshipNumber() {
		return citizenshipNumber;
	}
	
	public CertOfCitDocumentID23Type getCertOfCitDocumentID23Type(){
		return this.certOfCitDocumentID23Type;
	}
	
	/**
	 * Converts the JSON String to CertOfCitDocumentID23Wrapper object
	 * 
	 * @param jsonString - String representation for CertOfCitDocumentID23Wrapper
	 * @return CertOfCitDocumentID23Wrapper Object
	 */
	public static CertOfCitDocumentID23Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create CertOfCitDocumentID23Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		CertOfCitDocumentID23Wrapper wrapper = new CertOfCitDocumentID23Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.certOfCitDocumentID23Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(CITIZENSHIP_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.citizenshipNumber = (String)tmpObj;
			wrapper.certOfCitDocumentID23Type.setCitizenshipNumber(wrapper.citizenshipNumber);
		}
	
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(CITIZENSHIP_NUMBER_KEY, this.citizenshipNumber);
		return obj.toJSONString();
	}
	
	public static CertOfCitDocumentID23Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		CertOfCitDocumentID23Wrapper wrapper = new CertOfCitDocumentID23Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setCitizenshipNumber((String) jsonObj.get(CITIZENSHIP_NUMBER_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(CITIZENSHIP_NUMBER_KEY, this.citizenshipNumber);
		return obj;
	}
	
	
}
