package com.getinsured.hix.batch.enrollment.service;

import java.util.Map;

import com.getinsured.hix.platform.util.exception.GIException;

public interface ResendInd21Service {
	/**
	 * 
	 * @return
	 * @throws GIException
	 */
	Map<Integer, String> sendCarrierUpdatedDataToAHBX(long jobExecutionId) throws Exception;
	
	/**
	 * 
	 * @param errorMap
	 */
	void updateEnrolleeUpdateSendStatusForFailure(Map<Integer, String> errorMap);
}
