<%@page session="true" %>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<style>
h4 small {
	font-size: 12px;
	margin-top: 4px;
}
.table th, 
.table td {
	text-align: center;
}
.reduced-font {
	font-size: 12px!important;
}
.reduced-letter-spacing {
	letter-spacing: -0.06em;
}
</style>

<div class="gutter10">
	<div class="row-fluid" id="titlebar">
		<h1><a name="skip"></a><spring:message code="label.faq.consumer.pageTitle" htmlEscape="false"/></h1>
		<small class="gutter10"><a href="<c:url value='/faqhome'/>"class="pull-right"><spring:message code="label.faq.consumer.FAQHome" htmlEscape="false"/></a></small>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
	    	<div class="header">
				<h4 class="margin0">&nbsp;</h4>
			</div>
			<df:csrfToken/>
			<ul class="nav nav-list faq-nav" id="nav">
	            <li><a href="#employergroup1"><spring:message code="label.faq.consumer-connectors.Q1" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup2"><spring:message code="label.faq.consumer-connectors.Q2" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup3"><spring:message code="label.faq.consumer-connectors.Q3" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup4"><spring:message code="label.faq.consumer-connectors.Q4" htmlEscape="false"/></a></li>
	           <%--  <li><a href="#employergroup5"><spring:message code="label.faq.consumer-connectors.Q5" htmlEscape="false"/></a></li> --%>
	            <li><a href="#employergroup6"><spring:message code="label.faq.consumer-connectors.Q6" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup7"><spring:message code="label.faq.consumer-connectors.Q7" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup8"><spring:message code="label.faq.consumer-connectors.Q8" htmlEscape="false"/></a></li>
	           <%--  <li><a href="#employergroup9"><spring:message code="label.faq.consumer-connectors.Q9" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup10"><spring:message code="label.faq.consumer-connectors.Q10" htmlEscape="false"/></a></li> --%>
	            <li><a href="#employergroup11"><spring:message code="label.faq.consumer-connectors.Q11" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup12"><spring:message code="label.faq.consumer-connectors.Q12" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup13"><spring:message code="label.faq.consumer-connectors.Q13" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup13a"><spring:message code="label.faq.consumer-connectors.Q13.1" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup13b"><spring:message code="label.faq.consumer-connectors.Q13.2" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup13c"><spring:message code="label.faq.consumer-connectors.Q13.3" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup13d"><spring:message code="label.faq.consumer-connectors.Q13.4" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup14"><spring:message code="label.faq.consumer-connectors.Q14" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup15"><spring:message code="label.faq.consumer-connectors.Q15" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup16"><spring:message code="label.faq.consumer-connectors.Q16" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17"><spring:message code="label.faq.consumer-connectors.Q17" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17a"><spring:message code="label.faq.consumer-connectors.Q17.1" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17b"><spring:message code="label.faq.consumer-connectors.Q17.2" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17c"><spring:message code="label.faq.consumer-connectors.Q17.3" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17d"><spring:message code="label.faq.consumer-connectors.Q17.4" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17e"><spring:message code="label.faq.consumer-connectors.Q17.5" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17f"><spring:message code="label.faq.consumer-connectors.Q17.6" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup17g"><spring:message code="label.faq.consumer-connectors.Q17.7" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup19"><spring:message code="label.faq.consumer-connectors.Q19" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup20"><spring:message code="label.faq.consumer-connectors.Q20" htmlEscape="false"/></a></li>
	            <li><a href="#employergroup21"><spring:message code="label.faq.consumer-connectors.Q21" htmlEscape="false"/></a></li>
	      </ul>
		</div>
		
		   <div class="span9" id="rightpanel">
    
    
      <!-------------------------------group 1----------------------------------------------->
      <div id="employergroup1" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q1" htmlEscape="false"/></h4>
        </div>
        <div class="gutter10">        
            <p><spring:message code="label.faq.consumer-connectors.A1.p1" htmlEscape="false"/></p>
          </div>        
      </div>
      
      
      
      
      <!-------------------------------group 2----------------------------------------------->
      <div id="employergroup2" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q2" htmlEscape="false"/><small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A2.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A2.p2" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A2.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A2.li2" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A2.li3" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A2.li4" htmlEscape="false"/></li>
          </ul>
          </div>
      </div>
      
      
      
      <!-------------------------------group 3----------------------------------------------->
      <div id="employergroup3" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q3" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A3.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A3.p2" htmlEscape="false"/></p>
          <p><strong><spring:message code="label.faq.consumer-connectors.A3.p3" htmlEscape="false"/></strong></p>
          <%-- <ul>
            <li><spring:message code="label.faq.consumer-connectors.A3.li1" htmlEscape="false"/></li> 
            <li><spring:message code="label.faq.consumer-connectors.A3.li2" htmlEscape="false"/></li> 
            <li><spring:message code="label.faq.consumer-connectors.A3.li3" htmlEscape="false"/></li> 
          </ul> --%>
          <p><spring:message code="label.faq.consumer-connectors.A3.p4" htmlEscape="false"/></p>
          <%-- <p><spring:message code="label.faq.consumer-connectors.A3.p5" htmlEscape="false"/></p> --%>
          </div>
      </div>
      
      
      
      <!-------------------------------group 4----------------------------------------------->
      <div id="employergroup4" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q4" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A4.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A4.p2" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 5----------------------------------------------->
    <%--   <div id="employergroup5" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q5" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A5.p1" htmlEscape="false"/></p>
          </div>
      </div> --%>
      
      
      
      
      
      <!-------------------------------group 6----------------------------------------------->
      <div id="employergroup6" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q6" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A6.p1" htmlEscape="false"/></p>
         <%--  <p><spring:message code="label.faq.consumer-connectors.A6.p2" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A6.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A6.li2" htmlEscape="false"/></li> 
          </ul> --%>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 7----------------------------------------------->
      <div id="employergroup7" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q7" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A7.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      
      <!-------------------------------group 8----------------------------------------------->
      <div id="employergroup8" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q8" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A8.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A8.p2" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer.click" htmlEscape="false"/> <spring:message code="label.faq.consumer-connectors.A8.p3" htmlEscape="false"/></p> 
          </div>
      </div>
      
      
      
      <!-------------------------------group 9----------------------------------------------->
    <%--   <div id="employergroup9" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q9" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A9.p1" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A9.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A9.li2" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A9.li3" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A9.li4" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A9.li5" htmlEscape="false"/></li>
          </ul>
          <p><spring:message code="label.faq.consumer-connectors.A9.p2" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A9.p3" htmlEscape="false"/></p> 
          </div>
      </div> --%>
      
      
      
      
      <!-------------------------------group 10----------------------------------------------->
      <%-- <div id="employergroup10" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q10" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A10.p1" htmlEscape="false"/></p> 
          </div>
      </div> --%>
      
      
      
      <!-------------------------------group 11----------------------------------------------->
      <div id="employergroup11" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q11" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A11.p1" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A11.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A11.li2" htmlEscape="false"/></li>
          </ul> 
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 12----------------------------------------------->
      <div id="employergroup12" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q12" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A12.p1" htmlEscape="false"/></p>
          <table class="table table-bordered">
                  <thead>
                      <tr>
                          <th style="width: 20%; text-align: center;">Metal Level</th>
                          <th style="width: 40%; text-align: center;">Percentage of Medical Costs Paid by Health Plan</th>
                          <th style="width: 40%; text-align: center;">Percentage of Medical Costs Paid by Individuals</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                          <td>Catastrophic</td>
                          <td colspan="2">A catastrophic plan generally requires you to pay all of your medical costs up to a certain amount, usually several thousand dollars.*</td>
                      </tr>
                      <tr>
                          <td>Bronze</td>
                          <td>60%</td>
                          <td>40%</td>
                      </tr>
                      <tr>
                          <td>Silver</td>
                          <td>70%</td>
                          <td>30%</td>
                      </tr>
                      <tr>
                          <td>Gold</td>
                          <td>80%</td>
                          <td>20%</td>
                      </tr>
                      <!-- <tr>
                          <td>Platinum</td>
                          <td>90%</td>
                          <td>10%</td>
                      </tr> -->
                    </tbody>
                </table> 
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 13----------------------------------------------->
      <div id="employergroup13" class="margin20-b">
        <div class="header" style="height: 60px;">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q13" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A13.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      <!-------------------------------group 13.1----------------------------------------------->
      <div id="employergroup13a" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q13.1" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A13.1.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      <!-------------------------------group 13.2----------------------------------------------->
      <div id="employergroup13b" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q13.2" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A13.2.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      <!-------------------------------group 13.3----------------------------------------------->
      <div id="employergroup13c" class="margin20-b">
        <div class="header" style="height: 60px;">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q13.3" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A13.3.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      <!-------------------------------group 13.4----------------------------------------------->
      <div id="employergroup13d" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q13.4" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A13.4.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A13.4.p2" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A13.4.p3" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 14----------------------------------------------->
      <div id="employergroup14" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q14" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A14.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A14.p2" htmlEscape="false"/></p>
         <%--  <p><spring:message code="label.faq.consumer-connectors.A14.p3" htmlEscape="false"/></p> --%>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 15----------------------------------------------->
      <div id="employergroup15" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q15" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A15.p1" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A15.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li2" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li3" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li4" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li5" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li6" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li7" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li8" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li9" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li10" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A15.li11" htmlEscape="false"/></li>
          </ul>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 16----------------------------------------------->
      <div id="employergroup16" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q16" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A16.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A16.p2" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 17----------------------------------------------->
      <div id="employergroup17" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A17.p2" htmlEscape="false"/></p>
          </div>
      </div>
      
      
       <!-------------------------------group 17.1----------------------------------------------->
      <div id="employergroup17a" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.1" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.1.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
       <!-------------------------------group 17.2----------------------------------------------->
      <div id="employergroup17b" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.2" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.2.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
       <!-------------------------------group 17.3----------------------------------------------->
      <div id="employergroup17c" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.3" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.3.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
       <!-------------------------------group 17.4----------------------------------------------->
      <div id="employergroup17d" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.4" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.4.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
       <!-------------------------------group 17.5----------------------------------------------->
      <div id="employergroup17e" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.5" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.5.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      <!-------------------------------group 17.6----------------------------------------------->
      <div id="employergroup17f" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.6" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.6.p1" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      <!-------------------------------group 17.7----------------------------------------------->
      <div id="employergroup17g" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q17.7" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A17.7.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A17.7.p2" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A17.7.p3" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A17.7.p4" htmlEscape="false"/></p>
          </div>
      </div>
      
      
      
      
      <%-- <!-------------------------------group 18----------------------------------------------->
      <div id="employergroup18" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q18" htmlEscape="false"/>  <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A18.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A18.p2" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A18.p3" htmlEscape="false"/></p>
          </div>
      </div> --%>
      
      
      
      
      <!-------------------------------group 19----------------------------------------------->
      <div id="employergroup19" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q19" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A19.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A19.p2" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A19.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li2" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li3" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li4" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li5" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li6" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li7" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li8" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li9" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li10" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li11" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li12" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li13" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A19.li14" htmlEscape="false"/></li>
          </ul>
          </div>
      </div>
      
      
      
      
      
      
      <!-------------------------------group 20----------------------------------------------->
      <div id="employergroup20" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q20" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A20.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A20.p2" htmlEscape="false"/></p>
          <ul>
            <li><spring:message code="label.faq.consumer-connectors.A20.li1" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A20.li2" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A20.li3" htmlEscape="false"/></li>
            <li><spring:message code="label.faq.consumer-connectors.A20.li4" htmlEscape="false"/></li>
          </ul>
          </div>
      </div>
      
      
      
      
      <!-------------------------------group 21----------------------------------------------->
      <div id="employergroup21" class="margin20-b">
        <div class="header">
          <h4 class="reduced-font"><spring:message code="label.faq.consumer-connectors.Q21" htmlEscape="false"/> <small class="pull-right"><a href="#">Back to Top</a></small></h4>
        </div>
        <div class="gutter10">
          <p><spring:message code="label.faq.consumer-connectors.A21.p1" htmlEscape="false"/></p>
          <p><spring:message code="label.faq.consumer-connectors.A21.p2" htmlEscape="false"/></p>
          </div>
      </div>
      
      

    </div>
	</div>
</div>




<script>
$(document).ready(function(){
	document.title="Help for Nevada Health Link";
});

$(document).ready(function() {
 $('a[href*=#]:not([href=#])').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top-80
            }, 1000);
            return false;
        }
    }
});
 
});

</script>		