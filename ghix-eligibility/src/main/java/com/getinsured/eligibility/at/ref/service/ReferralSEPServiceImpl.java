package com.getinsured.eligibility.at.ref.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.eligibility.model.SepEvents.Gated;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.eligibility.referral.ui.dto.LceActivityDTO;
import com.getinsured.eligibility.referral.ui.dto.SepEventDTO;
import com.getinsured.eligibility.util.ApplicationExtensionEventUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * 
 * @author raguram_p
 * 
 */

@Component("referralSEPService")
@DependsOn("dynamicPropertiesUtil")
public class ReferralSEPServiceImpl implements ReferralSEPService {

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}
	private static final String MN_STATE_CODE = "MN";
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";

	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicationEventService ssapApplicationEventService;

	@Autowired
	private IUserRepository userRepository;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;
	
	@Autowired private QLEValidationService qleValidationService;
	
	@Autowired 	private ReferralOEService referralOEService;

	private static final Logger LOGGER = LoggerFactory.getLogger(ReferralSEPServiceImpl.class);

	private static int numberOfenrollmentDays = 60;

	private static final Set<String> ADD_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.ADD_EXISTING_ELIGIBLE.name(), ApplicantStatusEnum.ADD_NEW_ELIGIBILE.name()));

	private static final Set<String> REMOVE_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.REMOVE_DELETED_INELIGIBLE.name(), ApplicantStatusEnum.REMOVE_NOTDELETED_INELIGIBLE.name()));

	private static final Set<String> OTHER_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.CHANGE_IN_ELIGIBILITY.name(), ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT.name(), ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL.name(),
	        ApplicantStatusEnum.CHANGE_IN_CITIZENSHIP_STATUS.name(), ApplicantStatusEnum.CHANGE_IN_BLOOD_RELATION.name()));

	private static final Set<String> COA_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.UPDATED_ZIP_COUNTY.name()));

	private static final Set<String> NO_CHANGE_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.NO_CHANGE.name(), ApplicantStatusEnum.UPDATED_DOB.name(), ApplicantStatusEnum.DEMO_FIRSTNAME.name(),
			ApplicantStatusEnum.DEMO_NAMESUFFIX.name(),ApplicantStatusEnum.DEMO_MIDDLENAME.name(), ApplicantStatusEnum.DEMO_LASTNAME.name(), ApplicantStatusEnum.DEMO_SSN.name(), ApplicantStatusEnum.DEMO_ADDRESS_LINE1.name(), ApplicantStatusEnum.DEMO_ADDRESS_LINE2.name(),ApplicantStatusEnum.DEMO_ADDRESS_CITY.name(),
	        ApplicantStatusEnum.DEMO_PRIMARY_ADDRESS.name(), ApplicantStatusEnum.DEMO_MAILING_ADDRESS.name(), ApplicantStatusEnum.DEMO_OTHER.name(),ApplicantStatusEnum.DEMO_PTF.name(), ApplicantStatusEnum.DEMO_MULTIPLE.name(), ApplicantStatusEnum.ADD_NEW.name(),
	        ApplicantStatusEnum.DEMO_MARITIAL_STATUS.name(),ApplicantStatusEnum.DEMO_SPOKEN_LANGUAGE.name(),ApplicantStatusEnum.DEMO_WRITTEN_LANGUAGE.name(),
	        ApplicantStatusEnum.DELETED.name()));

	private static final Set<String> QUALIFYING_EVENTS = new HashSet<String>(Arrays.asList(ApplicantStatusEnum.QUALIFYING_EVENT.name()));

	private static String ERROR_LCE_NOTIFICATION = "An error occurred while sending the LCE notification";

	private int calculateEnrollmentGracePeriod() {
		int enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (StringUtils.isNumeric(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
		return enrollmentGracePeriod;
	}

	/**
	 * To save all SEP and QEP Events captured
	 * 
	 */

	@Override
	public Map<String, String> persistSEPAndQEPEventforApplicantandApplication(LceActivityDTO lceActivityDTO) {
		/* handle multiple updates */
		SsapApplicationEvent appEventDO = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(lceActivityDTO.getSsapApplicationId());
		if (appEventDO != null) {
			Map<String, String> output = new HashMap<String, String>();
			output.put(ReferralConstants.STATUS_KEY, ReferralConstants.STATUS_NOT_VALID + "");
			output.put(ReferralConstants.RESULT_KEY, ReferralUtil.formatDate(appEventDO.getEnrollmentEndDate(), "MM/dd/yyyy") + "");
			return output;
		}

		SsapApplicationEvent ssapAplicationEvent = new SsapApplicationEvent();
		Timestamp todayTimestamp = new Timestamp(new TSDate().getTime());

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(lceActivityDTO.getSsapApplicationId());

		if (ReferralUtil.listSize(ssapApplications) == 0) {
			throw new GIRuntimeException("Ssap Application not found for that id");
		}

		List<SsapApplicant> ssapApplicants = ssapApplications.get(0).getSsapApplicants();
		ssapAplicationEvent.setSsapApplication(ssapApplications.get(0));
		ssapAplicationEvent.setEnrollmentStartDate(todayTimestamp);

		SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();

		Map<String, SsapApplicant> applicantMap = new HashMap<String, SsapApplicant>();
		for (SsapApplicant applicant : ssapApplicants) {
			applicantMap.put(applicant.getApplicantGuid(), applicant);
		}

		if (lceActivityDTO != null && lceActivityDTO.getQualifyingEventMap() != null && lceActivityDTO.getQualifyingEventMap().keySet().size() > 0) {
			return persistQEPEventsforApplicantandApplication(lceActivityDTO, applicantMap, ssapAplicationEvent, todayTimestamp, ssapApplicants, applicationSepEndDates);
		} else {
			return persistSEPEventsforApplicantandApplication(lceActivityDTO, applicantMap, ssapAplicationEvent, todayTimestamp, ssapApplicants, applicationSepEndDates);
		}
	}

	@Override
	public void persistQEPEventforApplicantandApplication(SsapApplication ssapApplication, Date eventDate, String qualifyEventSelected) {
		LceActivityDTO lceActivityDTO = new LceActivityDTO();
		SsapApplicationEvent appEventDO = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplication.getId());
		if (appEventDO != null) {
			throw new GIRuntimeException("Application Event already exists");
		}
		SsapApplicationEvent ssapAplicationEvent = new SsapApplicationEvent();
		Timestamp todayTimestamp = new Timestamp(new TSDate().getTime());

		ssapAplicationEvent.setSsapApplication(ssapApplication);
		ssapAplicationEvent.setEnrollmentStartDate(todayTimestamp);

		List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();

		lceActivityDTO.setSsapApplicationId(ssapApplication.getId());
		Integer userId = null;
		List<Integer> userIds = userRepository.userIdsFromEmail(ReferralConstants.EXADMIN_USERNAME);
		if (ReferralUtil.listSize(userIds) > 0) {
			userId = userIds.get(0);
		}
		// background process. User id will capture always as 1. If csr links the account user id will be null in cmr_household table. so as default will capture as EXADMIN_USERNAME.
		if(userId != null) { // sonar fix
			lceActivityDTO.setUserId(userId.longValue());
		}
		
		lceActivityDTO.setQualifyEventSelected(qualifyEventSelected);
		lceActivityDTO.setQualifyEventDate(DateUtil.dateToString(eventDate, ReferralConstants.DEFDATEPATTERN));

		persistQEPEventsforApplicantandApplication(lceActivityDTO, ssapAplicationEvent, todayTimestamp, ssapApplicants);
	}

	private Map<String, String> persistSEPEventsforApplicantandApplication(LceActivityDTO lceActivityDTO, Map<String, SsapApplicant> applicantMap, SsapApplicationEvent ssapAplicationEvent, Timestamp todayTimestamp,
	        List<SsapApplicant> ssapApplicants, SortedSet<Date> applicationSepEndDates) {

		Map<String, SepEvents> sepEventsAdd = fetchAddEvents();
		Map<String, SepEvents> sepEventsRemove = fetchRemoveEvents();
		Map<String, SepEvents> sepEventsOther = fetchOtherEvents();

		try {
			boolean blnCheck = true;
			int gracePeriod = calculateEnrollmentGracePeriod();
			Date todaysDate = ReferralUtil.convertStringToDate(ReferralUtil.formatDate(new TSDate(), ReferralConstants.DEFDATEPATTERN));
			DateTime todaysDateTime = new DateTime(ReferralUtil.convertStringToDate(ReferralUtil.formatDate(new TSDate(), ReferralConstants.DEFDATEPATTERN)));
			Map<String, String> output = new HashMap<String, String>();
			output.put(ReferralConstants.PROCESS_FLOW, ReferralConstants.SEP);

			Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();

			boolean keepOnly = false;
			boolean blnKeepOnlyAllow = false;
			boolean blnKeepOnlyDontAllow = false;
			SsapApplication application = ssapApplicationRepository.findById(lceActivityDTO.getSsapApplicationId());
			boolean isNextYearsOE = false;
			if (application != null)
			{
				isNextYearsOE = referralOEService.isOpenEnrollment(application.getCoverageYear()) ;
			}

			/**
			 * Step 1 - CoA
			 */
			for (String coaApplicantGuid : lceActivityDTO.getChangeOfAddressMembersMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(coaApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsOther.get("CHANGE_IN_ADDRESS");
					blnKeepOnlyAllow = true;
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSsapApplicant(applicant);
					ssapApplicantEvent.setEventDate(new Timestamp((ReferralUtil.convertStringToDate(lceActivityDTO.getZipCountyChangeEventDate()).getTime())));
					ssapApplicantEvent.setSepEvents(sepEvent);

					if (blnCheck) {
						blnCheck = checkEventDateisPast(todaysDateTime, ssapApplicantEvent.getEventDate());
					}
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(ReferralUtil.convertStringToDate(lceActivityDTO.getZipCountyChangeEventDate()),isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}

			/**
			 * Step 2 - Change in Eligibility Only - Change in Cost Savings (APTC/CSR)
			 */
			for (String cieApplicantGuid : lceActivityDTO.getHouseholdChangeMembersMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(cieApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsOther.get(lceActivityDTO.getHouseholdChangeReason() == null ? "OTHER" : lceActivityDTO.getHouseholdChangeReason());
					blnKeepOnlyDontAllow = isKeepOnlyCase(sepEvent, blnKeepOnlyDontAllow);
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSsapApplicant(applicant);
					Timestamp eventdate = lceActivityDTO.getHouseholdChangeEventDate() == null ? minusDaysDate(new TSDate()) : new Timestamp((ReferralUtil.convertStringToDate(lceActivityDTO.getHouseholdChangeEventDate()).getTime()));
					ssapApplicantEvent.setEventDate(eventdate);
					ssapApplicantEvent.setSepEvents(sepEvent);
					if (blnCheck) {
						blnCheck = checkEventDateisPast(todaysDateTime, ssapApplicantEvent.getEventDate());
					}
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate, isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}

			/**
			 * Step 3 - Added Members
			 */
			for (String addedApplicantGuid : lceActivityDTO.getAddedMembersMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(addedApplicantGuid);
				SepEventDTO addedApplicantUi = lceActivityDTO.getAddedMembersMap().get(addedApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsAdd.get(addedApplicantUi.getName());
					blnKeepOnlyDontAllow = isKeepOnlyCase(sepEvent, blnKeepOnlyDontAllow);
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSsapApplicant(applicant);
					Date eventdate = ReferralUtil.convertStringToDate(addedApplicantUi.getEventDate());
					ssapApplicantEvent.setEventDate(new Timestamp(eventdate.getTime()));
					ssapApplicantEvent.setSepEvents(retrieveEvent(sepEvent, eventdate, addedApplicantUi.getName()));
					if (blnCheck) {
						blnCheck = checkEventDateisPast(todaysDateTime, ssapApplicantEvent.getEventDate()) == false ? false : !(ApplicationExtensionEventUtil.futureEventDenied(eventdate, gracePeriod));
					}
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate, isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}

			/**
			 * Step 4 - Remove Members
			 */
			for (String removedApplicantGuid : lceActivityDTO.getRemovedMembersMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(removedApplicantGuid);
				SepEventDTO removedApplicantUi = lceActivityDTO.getRemovedMembersMap().get(removedApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsRemove.get(removedApplicantUi.getName());
					blnKeepOnlyAllow = true;
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSsapApplicant(applicant);
					Date eventdate = ReferralUtil.convertStringToDate(removedApplicantUi.getEventDate());
					ssapApplicantEvent.setEventDate(new Timestamp(eventdate.getTime()));
					ssapApplicantEvent.setSepEvents(retrieveEvent(sepEvent, eventdate, removedApplicantUi.getName()));
					if (blnCheck) {
						//blnCheck = checkEventDateisPast(todaysDate, ssapApplicantEvent.getEventDate()) == false ? false : !(ApplicationExtensionEventUtil.futureEventDenied(eventdate, gracePeriod));
						blnCheck = !(ApplicationExtensionEventUtil.futureEventDenied(eventdate, gracePeriod));
					}
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					//ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate));
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(todaysDate, isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}
			/**
			 * Step 5 - Updated DOB Members
			 */
			for (String dobApplicantGuid : lceActivityDTO.getChangeOfDobMembersMap().keySet()) {
				SsapApplicant applicant = applicantMap.get(dobApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsOther.get("DOB_CHANGE");
					blnKeepOnlyAllow = true;
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSepEvents(sepEvent);
					ssapApplicantEvent.setSsapApplicant(applicant);

					Timestamp eventdate = minusDaysDate(new TSDate());
					ssapApplicantEvent.setEventDate(eventdate);
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate,isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}

			/**
			 * Step 6 - No Change Members
			 */
			for (String noChangeApplicantGuid : lceActivityDTO.getNoChangeMembersMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(noChangeApplicantGuid);
				if (applicant != null) {
					SepEvents sepEvent = sepEventsOther.get("OTHER");
					blnKeepOnlyAllow = true;
					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
					ssapApplicantEvent.setSepEvents(sepEvent);
					ssapApplicantEvent.setSsapApplicant(applicant);

					Timestamp eventdate = minusDaysDate(new TSDate());
					ssapApplicantEvent.setEventDate(eventdate);
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate,isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}
			if (blnKeepOnlyAllow && !blnKeepOnlyDontAllow) {
				keepOnly = true;
			}
			
			if(MapUtils.isEmpty(finalApplicantEvents) && MapUtils.isNotEmpty(applicantMap) && CollectionUtils.isEmpty(applicationSepEndDates))
			{
				for(Entry<String, SsapApplicant> applicantMapEntry : applicantMap.entrySet())
				{
					SsapApplicant applicant = applicantMapEntry.getValue();
					if (applicant != null) {
						SepEvents sepEvent = sepEventsOther.get("OTHER");
						blnKeepOnlyAllow = true;
						SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
						ssapApplicantEvent.setSepEvents(sepEvent);
						ssapApplicantEvent.setSsapApplicant(applicant);

						Timestamp eventdate = minusDaysDate(new TSDate());
						ssapApplicantEvent.setEventDate(eventdate);
						ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
						ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate,isNextYearsOE));

						applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

						finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
					}
				}
			}
			
			ssapAplicationEvent = ssapApplicationEventService.createApplication_Applicant_SepEvents(lceActivityDTO.getSsapApplicationId(), finalApplicantEvents, applicationSepEndDates.last(), (int) lceActivityDTO.getUserId(), keepOnly);

			AccountUser updateUser = userRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
			qleValidationService.runValidationEngine(application.getCaseNumber(),  Source.EXCHANGE, updateUser.getId());
			
			
			if (!blnCheck) {
				qleValidationService.denySep(application.getCaseNumber(),updateUser.getId());
				output.put(ReferralConstants.STATUS_KEY, ReferralConstants.FAILURE + "");
			} else {
				output.put(ReferralConstants.STATUS_KEY, ReferralConstants.SUCCESS + "");
			}
			

			if(blnCheck){
				if (ssapAplicationEvent != null) {
					if ("Y".equalsIgnoreCase(ssapAplicationEvent.getKeepOnly())) {
						triggerSepEventKeepOnlyNotice(ssapAplicationEvent.getSsapApplication().getCaseNumber());
					} else {
						triggerSepEventNotice(ssapAplicationEvent.getSsapApplication().getCaseNumber());
					}
				}
			}

			if (ssapAplicationEvent != null) {
				output.put(ReferralConstants.RESULT_KEY, ReferralUtil.formatDate(ssapAplicationEvent.getEnrollmentEndDate(), "MM/dd/yyyy"));
			}

			return output;
		} catch (Exception e) {
			throw new GIRuntimeException(e);
		}

	}

	@Override
	public boolean checkForDenial(Date eventdate) {
		DateTime todaysDate = new DateTime(ReferralUtil.convertStringToDate(ReferralUtil.formatDate(new TSDate(), ReferralConstants.DEFDATEPATTERN)));

		Timestamp eventDateTimestamp = new Timestamp(eventdate.getTime());
		return !(checkEventDateisPast(todaysDate, eventDateTimestamp));
	}

	private void triggerSepEventKeepOnlyNotice(String caseNumber) {
		try {
			referralLceNotificationService.generateSEPEventKeepOnlyNotice(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- LCE4 Keep Only Notice").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}

	}

	private void triggerDenialNotice(String caseNumber) {
		try {
			referralLceNotificationService.generateSEPDenialNotice(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice SEP Denial").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private void triggerSepEventNotice(String caseNumber) {

		try {
			referralLceNotificationService.generateSEPEventNotice(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE4").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private Map<String, String> persistQEPEventsforApplicantandApplication(LceActivityDTO lceActivityDTO, Map<String, SsapApplicant> applicantMap, SsapApplicationEvent ssapAplicationEvent, Timestamp todayTimestamp,
	        List<SsapApplicant> ssapApplicants, SortedSet<Date> applicationSepEndDates) {

		Map<String, SepEvents> qualifyingEvent = fetchQEPEvents();

		try {
			SsapApplication application = ssapApplicationRepository.findOne(lceActivityDTO.getSsapApplicationId());
			boolean isNextYearsOE = referralOEService.isOpenEnrollment(application.getCoverageYear()) ;
			boolean blnCheck = true;
			int gracePeriod = calculateEnrollmentGracePeriod();
			DateTime todaysDate = new DateTime(ReferralUtil.convertStringToDate(ReferralUtil.formatDate(new TSDate(), ReferralConstants.DEFDATEPATTERN)));
			Map<String, String> output = new HashMap<String, String>();
			output.put(ReferralConstants.PROCESS_FLOW, ReferralConstants.QEP);

			for (SsapApplicant applicant : ssapApplicants) {
				applicantMap.put(applicant.getApplicantGuid(), applicant);
			}

			Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();

			for (String qepApplicantGuid : lceActivityDTO.getQualifyingEventMap().keySet()) {

				SsapApplicant applicant = applicantMap.get(qepApplicantGuid);

				if (applicant != null) {
					SepEvents sepEvent = qualifyingEvent.get(lceActivityDTO.getQualifyEventSelected() == null ? "OTHER" : lceActivityDTO.getQualifyEventSelected());

					SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();

					ssapApplicantEvent.setSsapApplicant(applicant);
					Timestamp eventdate = lceActivityDTO.getQualifyEventDate() == null ? minusDaysDate(new TSDate()) : new Timestamp((ReferralUtil.convertStringToDate(lceActivityDTO.getQualifyEventDate()).getTime()));
					ssapApplicantEvent.setEventDate(eventdate);
					ssapApplicantEvent.setSepEvents(retrieveEvent(sepEvent, eventdate, lceActivityDTO.getQualifyEventSelected()));
					if (blnCheck) {
						blnCheck = checkEventDateisPast(todaysDate, eventdate) == false ? false : !(ApplicationExtensionEventUtil.futureEventDenied(eventdate, gracePeriod));
					}
					ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
					ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(eventdate,isNextYearsOE));

					applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

					finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
				}
			}
			ssapAplicationEvent = ssapApplicationEventService.createApplication_Applicant_QepEvents(lceActivityDTO.getSsapApplicationId(), finalApplicantEvents, applicationSepEndDates.last(), (int) lceActivityDTO.getUserId());

			AccountUser updateUser = userRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
			qleValidationService.runValidationEngine(application.getCaseNumber(),  Source.EXCHANGE, updateUser.getId());
			
			
			if (!blnCheck) {
				qleValidationService.denySep(application.getCaseNumber(),updateUser.getId());
				output.put(ReferralConstants.STATUS_KEY, ReferralConstants.FAILURE + "");
			} else {
				output.put(ReferralConstants.STATUS_KEY, ReferralConstants.SUCCESS + "");
			}
			

			if(blnCheck){
				if (ssapAplicationEvent != null) {
					triggerQepEventNotice(ssapAplicationEvent.getSsapApplication().getCaseNumber());
					String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					// TODO : the below notice will be triggered in case of MN - we need to remove this once content is finalized
					if(stateCode.equalsIgnoreCase(MN_STATE_CODE)){
						boolean sendLCE04notice = false;
						// New change for MN notices LCE04 notice needs to be triggered when event is not gated - check for not gated event
						for(SsapApplicantEvent applicantEvent : ssapAplicationEvent.getSsapApplicantEvents()) {
							SepEvents sepEvent = applicantEvent.getSepEvents();
							if(sepEvent.getGated()== Gated.N) {
								sendLCE04notice = true;
								break;
							}							
						}
						if(sendLCE04notice) {
						triggerLCE04Email(ssapAplicationEvent.getSsapApplication().getCaseNumber());
					}
					}
					
				}
			}

			if (ssapAplicationEvent != null) {
				output.put(ReferralConstants.RESULT_KEY, ReferralUtil.formatDate(ssapAplicationEvent.getEnrollmentEndDate(), "MM/dd/yyyy"));
			}
			return output;
		} catch (Exception e) {
			throw new GIRuntimeException(e);
		}
	}

	@Override
	public SepEvents retrieveEvent(SepEvents sepEvent, Date eventDate, String selectedEventName) {

		SepEvents sepEvents = null;
		if (ReferralUtil.isFutureDate(eventDate)) {
			if (ReferralConstants.Y.equals(sepEvent.getAllowFutureEvent()) && sepEvent.getReferenceFutureEvent() != null) {
				sepEvents = sepEventsService.findSepEventById(sepEvent.getReferenceFutureEvent());
			} else if (ReferralConstants.QUALIFYING_EVENT.equals(sepEvent.getChangeType()) && ReferralConstants.REMOVE_DEPENDENT.equals(selectedEventName)) {
				sepEvents = sepEventsService.findFinancialSepEventsByEventNameAndCategory(ReferralConstants.PRIMARY_GAIN_OF_MEC_FUTURE, ReferralConstants.QUALIFYING_EVENT);
			} else if (ReferralConstants.REMOVE.equals(sepEvent.getChangeType()) && ReferralConstants.REMOVE_DEPENDENT.equals(selectedEventName)) {
				sepEvents = sepEventsService.findFinancialSepEventsByEventNameAndCategory(ReferralConstants.GAIN_OF_MEC_FUTURE, ReferralConstants.REMOVE);
			}
		}
		if (sepEvents == null) {
			sepEvents = sepEvent;
		}
		return sepEvents;
	}

	private void persistQEPEventsforApplicantandApplication(LceActivityDTO lceActivityDTO, SsapApplicationEvent ssapAplicationEvent, Timestamp todayTimestamp, List<SsapApplicant> ssapApplicants) {
		Timestamp enrollmentEndDate = null;
		Map<String, SepEvents> qualifyingEvent = fetchQEPEvents();
		Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();
		SepEvents sepEvent = qualifyingEvent.get(lceActivityDTO.getQualifyEventSelected());
		LOGGER.info("Persist QEP Event code "+lceActivityDTO.getQualifyEventSelected());
		if(sepEvent!=null) {
			LOGGER.info("Persist QEP Event code with DB event "+sepEvent.getId()+" "+sepEvent.getName());
		}
		SsapApplication application = ssapApplicationRepository.findById(lceActivityDTO.getSsapApplicationId());
		boolean isNextYearsOE = false;
		if (application != null)
		{
			isNextYearsOE = referralOEService.isOpenEnrollment(application.getCoverageYear()) ;
		}
		Timestamp eventdate = new Timestamp((ReferralUtil.convertStringToDate(lceActivityDTO.getQualifyEventDate()).getTime()));
		enrollmentEndDate = calculateEndDate(eventdate, isNextYearsOE);

		for (SsapApplicant applicant : ssapApplicants) {
			SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
			ssapApplicantEvent.setSsapApplicant(applicant);
			ssapApplicantEvent.setEventDate(eventdate);
			ssapApplicantEvent.setSepEvents(retrieveEvent(sepEvent, eventdate, lceActivityDTO.getQualifyEventSelected()));
			ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
			ssapApplicantEvent.setEnrollmentEndDate(enrollmentEndDate);

			finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
		}
		ssapAplicationEvent = ssapApplicationEventService.createApplication_Applicant_QepEvents(lceActivityDTO.getSsapApplicationId(), finalApplicantEvents, enrollmentEndDate, (int) lceActivityDTO.getUserId());

	}

	private void triggerQepEventNotice(String caseNumber) {
		try {
			referralLceNotificationService.generateQEPEventNotice(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice QEP ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
	
	private void triggerLCE04Email(String caseNumber) {
		LOGGER.info("Trigger Email - Notice LCE LCE04");
		try {
			// we pass the flag for skipping the Document notice as the EE061 is passed while passing EE031QEPEventNotification
			referralLceNotificationService.generateSEPEventNoticeQEPConfirmLifeEvent(caseNumber,false);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE LCE04 while QEP confirm life event").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private boolean checkEventDateisPast(DateTime todaysDate, Timestamp eventDate) {
		//Here event date should be as provided.
		Date eventdate = new Date(eventDate.getTime());
		DateTime selEventDate = new DateTime(eventdate);
		DateTime eventCheckDate = selEventDate.plusDays(calculateEnrollmentGracePeriod() + numberOfenrollmentDays);
		if (eventCheckDate.isBefore(todaysDate)) {
			return false;
		} else {
			return true;
		}
	}

	private Timestamp calculateEndDate(Date currDate, boolean isNextYearsOE) {
		if (isNextYearsOE)
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				LOGGER.error("Error while calculating the SsapApplicantEvent Enrollment End Date",e);
				return null;
			}
		}
		else {
			DateTime endDate = new DateTime(currDate.getTime());
			endDate = endDate.plusDays(calculateEnrollmentGracePeriod() + numberOfenrollmentDays);
			return new Timestamp(endDate.getMillis());
		}
	}

	private Timestamp minusDaysDate(Date date) {
		DateTime endDate = new DateTime(date.getTime());
		endDate = endDate.minusDays(calculateEnrollmentGracePeriod() + numberOfenrollmentDays);
		return new Timestamp(endDate.getMillis());
	}

	@Override
	public Map<String, List<SepEvents>> fetchSepandQEPEvents() {
		Map<String, List<SepEvents>> sepEventsMap = new HashMap<String, List<SepEvents>>();

		Map<String, SepEvents> sepEventsAdd = fetchAddEvents();
		Map<String, SepEvents> sepEventsRemove = fetchRemoveEvents();
		Map<String, SepEvents> sepEventsOther = fetchOtherEvents();
		Map<String, SepEvents> qualifyingEvent = fetchQEPEvents();

		sepEventsMap.put(Events.ADD.name(), getSepEventsfromMap(sepEventsAdd));
		sepEventsMap.put(Events.REMOVE.name(), getSepEventsfromMap(sepEventsRemove));
		sepEventsMap.put(Events.OTHER.name(), getSepEventsfromMap(sepEventsOther));
		sepEventsMap.put(Events.QUALIFYING_EVENT.name(), getSepEventsfromMap(qualifyingEvent));

		return sepEventsMap;
	}

	private Map<String, SepEvents> fetchQEPEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.QUALIFYING_EVENT.name());
	}

	private Map<String, SepEvents> fetchOtherEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
	}

	private Map<String, SepEvents> fetchRemoveEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.REMOVE.name());
	}

	private Map<String, SepEvents> fetchAddEvents() {
		return sepEventsService.findFinancialSepEventsByChangeType(Events.ADD.name());
	}

	private List<SepEvents> getSepEventsfromMap(Map<String, SepEvents> sepEventMap) {
		List<SepEvents> sepEvents = new ArrayList<SepEvents>();
		for (Map.Entry<String, SepEvents> entry : sepEventMap.entrySet()) {
			if (ReferralConstants.Y.equalsIgnoreCase(entry.getValue().getDisplayOnUI())) {
				sepEvents.add(entry.getValue());
			}
		}
		return sepEvents;
	}

	@Override
	public LceActivityDTO populateApplicantsforSEPEvents(String caseNumber) {
		return populateSsapApplicants(caseNumber);
	}

	private LceActivityDTO populateSsapApplicants(String caseNumber) {

		List<SsapApplication> ssapApplications = ssapApplicationRepository.findByCaseNumber(caseNumber);

		if (ReferralUtil.listSize(ssapApplications) == 0) {
			LOGGER.warn("Unable to find records for caseNumber - " + caseNumber);
			throw new GIRuntimeException("Unable to find records for caseNumber - " + caseNumber);
		}

		LceActivityDTO lceActivityDTO = new LceActivityDTO();
		SsapApplicationEvent appEventDO = ssapApplicationEventService.getSsapApplicationEventsByApplicationId(ssapApplications.get(0).getId());
		if (appEventDO != null) {
			lceActivityDTO.setApplicationEventExists(true);
			return lceActivityDTO;
		}

		Map<String, SepEventDTO> addedMembersMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> removedMembersMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> householdChangeMembersMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> changeOfAddressMembersMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> noChangeMembersMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> qualifyingEventsMap = new HashMap<String, SepEventDTO>();
		Map<String, SepEventDTO> changeOfDobMembersMap = new HashMap<String, SepEventDTO>();

		List<SsapApplicant> ssapApplicants = ssapApplications.get(0).getSsapApplicants();
		for (SsapApplicant applicant : ssapApplicants) {

			SepEventDTO sepEventDTO = new SepEventDTO();
			sepEventDTO.setApplicantGuid(applicant.getApplicantGuid());
			sepEventDTO.setApplicantId(applicant.getId());
			sepEventDTO.setApplicantName(applicant.getFirstName() + " " + applicant.getLastName());
			sepEventDTO.setSsapApplicationId(applicant.getSsapApplication().getId());

			if (ADD_EVENTS.contains(applicant.getStatus())) {
				addedMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
			}

			if (REMOVE_EVENTS.contains(applicant.getStatus())) {
				removedMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
			}

			if (OTHER_EVENTS.contains(applicant.getStatus())) {
				householdChangeMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
				lceActivityDTO.setHouseholdDisplayed("Yes");
			}

			if (COA_EVENTS.contains(applicant.getStatus())) {
				changeOfAddressMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
			}
			
			if (NO_CHANGE_EVENTS.contains(applicant.getStatus())) {
				if(applicant.getStatus().equals(ApplicantStatusEnum.UPDATED_DOB.name())) {
					changeOfDobMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
				}
				else {
					noChangeMembersMap.put(applicant.getApplicantGuid(), sepEventDTO);
				}
			}
			if (QUALIFYING_EVENTS.contains(applicant.getStatus())) {
				qualifyingEventsMap.put(applicant.getApplicantGuid(), sepEventDTO);
			}
		}

		lceActivityDTO.setSsapApplicationId(ssapApplications.get(0).getId());
		lceActivityDTO.setCaseNumber(caseNumber);
		lceActivityDTO.setAddedMembersMap(addedMembersMap);
		lceActivityDTO.setRemovedMembersMap(removedMembersMap);
		lceActivityDTO.setHouseholdChangeMembersMap(householdChangeMembersMap);
		lceActivityDTO.setChangeOfAddressMembersMap(changeOfAddressMembersMap);
		lceActivityDTO.setChangeOfDobMembersMap(changeOfDobMembersMap);
		lceActivityDTO.setNoChangeMembersMap(noChangeMembersMap);
		lceActivityDTO.setQualifyingEventMap(qualifyingEventsMap);
		lceActivityDTO.setApplicationEventExists(false);
		
		return lceActivityDTO;

	}

	private Set<String> KEEP_ONLY_EVENTS = new HashSet<String>(Arrays.asList("OTHER_ELIGIBILITY_CHANGE-HEALTH_STATUS_CHANGE", "OTHER","OTHER_ELIGIBILITY_CHANGE"));

	private boolean isKeepOnlyCase(SepEvents sepEvent, boolean existingDontKeepOnlyValue) {
		return existingDontKeepOnlyValue ? true : KEEP_ONLY_EVENTS.contains(sepEvent.getName()) ? false : true;
	}
}
