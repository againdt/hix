//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.02.27 at 09:16:54 AM IST 
//


package com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_ee;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ifsv.cms.hix._0_1.hix_types.TaxReturnFilingStatusCodeType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.hub.ifsv.niem.niem.niem_core._2.QuantityType;
import com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.GYear;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.cms.hix._0_1.hix_ee package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TaxReturnAGIAmount_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnAGIAmount");
    private final static QName _TaxReturnTaxableSocialSecurityBenefitsAmount_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnTaxableSocialSecurityBenefitsAmount");
    private final static QName _TaxFiler_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxFiler");
    private final static QName _TaxReturnYear_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnYear");
    private final static QName _TaxReturnAmount_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnAmount");
    private final static QName _TaxReturnFiscalPeriod_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnFiscalPeriod");
    private final static QName _TaxReturnFilingStatusCode_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnFilingStatusCode");
    private final static QName _TaxReturnTotalExemptionsQuantity_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnTotalExemptionsQuantity");
    private final static QName _SpouseTaxFiler_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "SpouseTaxFiler");
    private final static QName _PrimaryTaxFiler_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "PrimaryTaxFiler");
    private final static QName _TaxReturnMAGIAmount_QNAME = new QName("http://hix.cms.gov/0.1/hix-ee", "TaxReturnMAGIAmount");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.cms.hix._0_1.hix_ee
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link TaxFilerType }
     * 
     */
    public TaxFilerType createTaxFilerType() {
        return new TaxFilerType();
    }

    /**
     * Create an instance of {@link HouseholdType }
     * 
     */
    public HouseholdType createHouseholdType() {
        return new HouseholdType();
    }

    /**
     * Create an instance of {@link TaxReturnType }
     * 
     */
    public TaxReturnType createTaxReturnType() {
        return new TaxReturnType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnAGIAmount", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxReturnAmount")
    public JAXBElement<AmountType> createTaxReturnAGIAmount(AmountType value) {
        return new JAXBElement<AmountType>(_TaxReturnAGIAmount_QNAME, AmountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnTaxableSocialSecurityBenefitsAmount", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxReturnAmount")
    public JAXBElement<AmountType> createTaxReturnTaxableSocialSecurityBenefitsAmount(AmountType value) {
        return new JAXBElement<AmountType>(_TaxReturnTaxableSocialSecurityBenefitsAmount_QNAME, AmountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFilerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxFiler")
    public JAXBElement<TaxFilerType> createTaxFiler(TaxFilerType value) {
        return new JAXBElement<TaxFilerType>(_TaxFiler_QNAME, TaxFilerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GYear }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnYear", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxReturnFiscalPeriod")
    public JAXBElement<GYear> createTaxReturnYear(GYear value) {
        return new JAXBElement<GYear>(_TaxReturnYear_QNAME, GYear.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnAmount")
    public JAXBElement<AmountType> createTaxReturnAmount(AmountType value) {
        return new JAXBElement<AmountType>(_TaxReturnAmount_QNAME, AmountType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnFiscalPeriod")
    public JAXBElement<Object> createTaxReturnFiscalPeriod(Object value) {
        return new JAXBElement<Object>(_TaxReturnFiscalPeriod_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxReturnFilingStatusCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnFilingStatusCode")
    public JAXBElement<TaxReturnFilingStatusCodeType> createTaxReturnFilingStatusCode(TaxReturnFilingStatusCodeType value) {
        return new JAXBElement<TaxReturnFilingStatusCodeType>(_TaxReturnFilingStatusCode_QNAME, TaxReturnFilingStatusCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuantityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnTotalExemptionsQuantity")
    public JAXBElement<QuantityType> createTaxReturnTotalExemptionsQuantity(QuantityType value) {
        return new JAXBElement<QuantityType>(_TaxReturnTotalExemptionsQuantity_QNAME, QuantityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFilerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "SpouseTaxFiler", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxFiler")
    public JAXBElement<TaxFilerType> createSpouseTaxFiler(TaxFilerType value) {
        return new JAXBElement<TaxFilerType>(_SpouseTaxFiler_QNAME, TaxFilerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TaxFilerType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "PrimaryTaxFiler", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxFiler")
    public JAXBElement<TaxFilerType> createPrimaryTaxFiler(TaxFilerType value) {
        return new JAXBElement<TaxFilerType>(_PrimaryTaxFiler_QNAME, TaxFilerType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://hix.cms.gov/0.1/hix-ee", name = "TaxReturnMAGIAmount", substitutionHeadNamespace = "http://hix.cms.gov/0.1/hix-ee", substitutionHeadName = "TaxReturnAmount")
    public JAXBElement<AmountType> createTaxReturnMAGIAmount(AmountType value) {
        return new JAXBElement<AmountType>(_TaxReturnMAGIAmount_QNAME, AmountType.class, null, value);
    }

}
