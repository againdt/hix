package com.getinsured.hix.batch.provider;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProviderRecord implements Serializable {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	private String recordData = null;
	private int lineNo;
	private int errorCount;
	private ArrayList<ProviderDataField> validFields;
	private ArrayList<ProviderDataField> invalidFields;
	private HashMap<String,String> validationErrors;
	private String id = null;
	private String idName = null;
	private Logger logger = LoggerFactory.getLogger(ProviderRecord.class);
	private boolean fieldGiModified = false;
	private ArrayList<String> modifiedFields = new ArrayList<String>();
	private String columnDefLine;

	public ProviderRecord() {
		// TODO Auto-generated constructor stub
	}

	public void addDataField(ProviderDataField field) throws InvalidOperationException{
		String name = field.getName();
		if(field.getFieldMetaData().isGi_modified()){
			this.modifiedFields.add(name);
		}
		if(field.isAnIdentityField()){
			if(field.getValue() == null){
				field.setValidationException(new ProviderValidationException(name+" is declared as an Identity field, value can not be null"));
			}else{
				if(this.id != null){
					throw new InvalidOperationException("Record can not have ["+field.getName()+"] identity fields, Already encountered "+this.idName+" as Identity");
				}
				this.id = (String)field.getValue();
				this.idName = field.getName();

			}
		}
		ProviderValidationException pe = field.getValidationException();
		if(pe!= null){
			if(this.validationErrors == null){
				this.validationErrors = new HashMap<String,String>();
			}
			this.validationErrors.put(field.getRecordInfo(), pe.getMessage());
			if(invalidFields == null){
				invalidFields = new ArrayList<ProviderDataField>();
			}
			invalidFields.add(field);
			this.errorCount++;
		}else{
			if(validFields == null){
				validFields = new ArrayList<ProviderDataField>();
			}
			validFields.add(field);
		}
	}

	public int getLineNo() {
		return lineNo;
	}

	public void setLineNo(int lineNo) {
		this.lineNo = lineNo;
	}

	public ArrayList<ProviderDataField> getValidFields() {
		return validFields;
	}

	public ArrayList<ProviderDataField> getInvalidFields() {
		return invalidFields;
	}

	public ArrayList<String> getModifiedFields() {
		return this.modifiedFields;
	}

	public int getErrorCount() {
		return errorCount;
	}

	public void setErrorCount(int errorCount) {
		this.errorCount = errorCount;
	}

	public String getId() {
		return id;
	}

	public String getRecordData() {
		return recordData;
	}

	public void setRecordData(String recordData) {
		this.recordData = recordData;
	}

	public String toString(){
		StringBuilder str = new StringBuilder();
		str.append("LINE#: "+this.lineNo+"\n");
		str.append("COLUMNS:"+this.columnDefLine+"\n");
		str.append("RAW DATA:"+this.recordData+"\n");
				if(this.validFields != null){
					str.append("VALID FIELDS: "+this.validFields.size()+"\n");
				}
				str.append("ERROR FIELDS: "+this.errorCount+"\n");
				if(this.validationErrors != null){
					Set<Entry<String,String>> s = this.validationErrors.entrySet();
					Iterator<Entry<String, String>> i = s.iterator();
					while(i.hasNext()){
						Map.Entry<String, String> me = (Entry<String, String>) i.next();
						str.append("ERROR: "+me.getKey()+" :-MESSAGE:"+me.getValue()+"\n");
					}
				}
				str.append("********************************** END PROVIDER RECORD *****************\n");
		return str.toString();
	}

	public void merge(ProviderRecord pr) throws InvalidOperationException {
		if(!this.id.equals(pr.getId())){
			throw new InvalidOperationException("Can not merge record with id:"+this.id+" with record id:"+pr.getId());
		}

	}

	public Object getField(String string) {
		ProviderDataField field = null;
		Iterator<ProviderDataField> cursor = this.validFields.iterator();
		while(cursor.hasNext()){
			field = cursor.next();
			if(field.getName().equals(string)){
				if(field.isMultiValue()){
					return field.getProcessedValue();
				}
				return field.getValue();
			}
		}
		return null;
	}

	public void setColumnDefinitionLine(String columnDefLine) {
		this.columnDefLine = columnDefLine;
	}

	public String getColumnDefinitionLine() {
		return this.columnDefLine;
	}


}
