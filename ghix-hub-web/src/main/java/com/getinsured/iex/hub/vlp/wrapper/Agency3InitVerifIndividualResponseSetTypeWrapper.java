package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifIndividualResponseSetType;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper class for gov.hhs.cms.dsh.sim.ee.vlp.DHSResponseFields
 * 
 * @author Nikhil Talreja
 * @since 10-Feb-2014
 * 
 */
public class Agency3InitVerifIndividualResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Agency3InitVerifIndividualResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String nonCitLastName;
	private String nonCitFirstName;
	private String nonCitMiddleName;
	private String nonCitBirthDate;
	private String nonCitEntryDate;
	private String nonCitAdmittedToDate;
	private String nonCitAdmittedToText;
	private String nonCitCountryBirthCd;
	private String nonCitCountryCitCd;
	private String nonCitCoaCode;
	private String nonCitEadsExpireDate;
	private int eligStatementCd;
	private String eligStatementTxt;
	private String iavTypeCode;
	private String iavTypeTxt;
	private String webServSftwrVer;
	private String grantDate;
	private String grantDateReasonCd;
	private boolean sponsorDataFoundIndicator;
	private ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData;
	private String sponsorshipReasonCd;
	private boolean photoIncludedIndicator;
	private byte[] photoBinaryAttachment;
	private boolean caseSentToSecondaryIndicator;
	private boolean dshAutoTriggerStepTwo;
	private String fiveYearBarApplyCode;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
    
    public Agency3InitVerifIndividualResponseSetTypeWrapper(Agency3InitVerifIndividualResponseSetType agency3InitVerifIndividualResponseSetType){
    	
    	if (agency3InitVerifIndividualResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = agency3InitVerifIndividualResponseSetType.getCaseNumber();
    	this.nonCitLastName = agency3InitVerifIndividualResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = agency3InitVerifIndividualResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = agency3InitVerifIndividualResponseSetType.getNonCitLastName();
    	try{
	    	if(agency3InitVerifIndividualResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(agency3InitVerifIndividualResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(agency3InitVerifIndividualResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(agency3InitVerifIndividualResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(agency3InitVerifIndividualResponseSetType.getNonCitAdmittedToDate() != null){
	    		this.nonCitAdmittedToDate = VLPServiceUtil.xmlDateToString(agency3InitVerifIndividualResponseSetType.getNonCitAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(agency3InitVerifIndividualResponseSetType.getNonCitEadsExpireDate() != null){
	    		this.nonCitEadsExpireDate = VLPServiceUtil.xmlDateToString(agency3InitVerifIndividualResponseSetType.getNonCitEadsExpireDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(agency3InitVerifIndividualResponseSetType.getGrantDate() != null){
	    		this.grantDate = VLPServiceUtil.xmlDateToString(agency3InitVerifIndividualResponseSetType.getGrantDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	this.nonCitAdmittedToText = agency3InitVerifIndividualResponseSetType.getNonCitAdmittedToText();
    	this.nonCitCountryBirthCd = agency3InitVerifIndividualResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = agency3InitVerifIndividualResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = agency3InitVerifIndividualResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = agency3InitVerifIndividualResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = agency3InitVerifIndividualResponseSetType.getEligStatementTxt();
    	this.iavTypeCode = agency3InitVerifIndividualResponseSetType.getIAVTypeCode();
    	this.iavTypeTxt = agency3InitVerifIndividualResponseSetType.getIAVTypeTxt();
    	this.webServSftwrVer = agency3InitVerifIndividualResponseSetType.getWebServSftwrVer();
    	this.grantDateReasonCd = agency3InitVerifIndividualResponseSetType.getGrantDateReasonCd();
    	this.sponsorDataFoundIndicator = agency3InitVerifIndividualResponseSetType.isSponsorDataFoundIndicator() == null ? false : agency3InitVerifIndividualResponseSetType.isSponsorDataFoundIndicator();
    	
    	this.arrayOfSponsorshipData = new ArrayOfSponsorshipDataTypeWrapper(agency3InitVerifIndividualResponseSetType.getArrayOfSponsorshipData());
    	
    	this.sponsorshipReasonCd = agency3InitVerifIndividualResponseSetType.getSponsorshipReasonCd();
    	this.photoIncludedIndicator = agency3InitVerifIndividualResponseSetType.isPhotoIncludedIndicator() == null ? false : agency3InitVerifIndividualResponseSetType.isPhotoIncludedIndicator();
    	this.photoBinaryAttachment = agency3InitVerifIndividualResponseSetType.getPhotoBinaryAttachment();
    	this.caseSentToSecondaryIndicator = agency3InitVerifIndividualResponseSetType.isCaseSentToSecondaryIndicator();
    	this.dshAutoTriggerStepTwo = agency3InitVerifIndividualResponseSetType.isDSHAutoTriggerStepTwo();
    	this.fiveYearBarApplyCode = agency3InitVerifIndividualResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = agency3InitVerifIndividualResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = agency3InitVerifIndividualResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = agency3InitVerifIndividualResponseSetType.getUSCitizenCode();
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}



	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}



	public String getNonCitLastName() {
		return nonCitLastName;
	}



	public void setNonCitLastName(String nonCitLastName) {
		this.nonCitLastName = nonCitLastName;
	}



	public String getNonCitFirstName() {
		return nonCitFirstName;
	}



	public void setNonCitFirstName(String nonCitFirstName) {
		this.nonCitFirstName = nonCitFirstName;
	}



	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}



	public void setNonCitMiddleName(String nonCitMiddleName) {
		this.nonCitMiddleName = nonCitMiddleName;
	}



	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}



	public void setNonCitBirthDate(String nonCitBirthDate) {
		this.nonCitBirthDate = nonCitBirthDate;
	}



	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}



	public void setNonCitEntryDate(String nonCitEntryDate) {
		this.nonCitEntryDate = nonCitEntryDate;
	}



	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}



	public void setNonCitAdmittedToDate(String nonCitAdmittedToDate) {
		this.nonCitAdmittedToDate = nonCitAdmittedToDate;
	}



	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}



	public void setNonCitAdmittedToText(String nonCitAdmittedToText) {
		this.nonCitAdmittedToText = nonCitAdmittedToText;
	}



	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}



	public void setNonCitCountryBirthCd(String nonCitCountryBirthCd) {
		this.nonCitCountryBirthCd = nonCitCountryBirthCd;
	}



	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}



	public void setNonCitCountryCitCd(String nonCitCountryCitCd) {
		this.nonCitCountryCitCd = nonCitCountryCitCd;
	}



	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}



	public void setNonCitCoaCode(String nonCitCoaCode) {
		this.nonCitCoaCode = nonCitCoaCode;
	}



	public String getNonCitEadsExpireDate() {
		return nonCitEadsExpireDate;
	}



	public void setNonCitEadsExpireDate(String nonCitEadsExpireDate) {
		this.nonCitEadsExpireDate = nonCitEadsExpireDate;
	}



	public int getEligStatementCd() {
		return eligStatementCd;
	}



	public void setEligStatementCd(int eligStatementCd) {
		this.eligStatementCd = eligStatementCd;
	}



	public String getEligStatementTxt() {
		return eligStatementTxt;
	}



	public void setEligStatementTxt(String eligStatementTxt) {
		this.eligStatementTxt = eligStatementTxt;
	}



	public String getIavTypeCode() {
		return iavTypeCode;
	}



	public void setIavTypeCode(String iavTypeCode) {
		this.iavTypeCode = iavTypeCode;
	}



	public String getIavTypeTxt() {
		return iavTypeTxt;
	}



	public void setIavTypeTxt(String iavTypeTxt) {
		this.iavTypeTxt = iavTypeTxt;
	}



	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}



	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}



	public String getGrantDate() {
		return grantDate;
	}



	public void setGrantDate(String grantDate) {
		this.grantDate = grantDate;
	}



	public String getGrantDateReasonCd() {
		return grantDateReasonCd;
	}



	public void setGrantDateReasonCd(String grantDateReasonCd) {
		this.grantDateReasonCd = grantDateReasonCd;
	}



	public boolean isSponsorDataFoundIndicator() {
		return sponsorDataFoundIndicator;
	}



	public void setSponsorDataFoundIndicator(boolean sponsorDataFoundIndicator) {
		this.sponsorDataFoundIndicator = sponsorDataFoundIndicator;
	}



	public ArrayOfSponsorshipDataTypeWrapper getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}



	public void setArrayOfSponsorshipData(
			ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData) {
		this.arrayOfSponsorshipData = arrayOfSponsorshipData;
	}



	public String getSponsorshipReasonCd() {
		return sponsorshipReasonCd;
	}



	public void setSponsorshipReasonCd(String sponsorshipReasonCd) {
		this.sponsorshipReasonCd = sponsorshipReasonCd;
	}



	public boolean isPhotoIncludedIndicator() {
		return photoIncludedIndicator;
	}



	public void setPhotoIncludedIndicator(boolean photoIncludedIndicator) {
		this.photoIncludedIndicator = photoIncludedIndicator;
	}



	public byte[] getPhotoBinaryAttachment() {
		
		if (photoBinaryAttachment == null) {
            return new byte[0];
		}
		
		byte [] attachment = new byte[photoBinaryAttachment.length];
		
		System.arraycopy(photoBinaryAttachment, 0, attachment, 0, attachment.length);
		
		return attachment;
	}



	public void setPhotoBinaryAttachment(byte[] photoBinaryAttachment) {
		
		if (null == photoBinaryAttachment) {
            return;
		}

		System.arraycopy(photoBinaryAttachment, 0, this.photoBinaryAttachment, 0,
    		 photoBinaryAttachment.length);

	}



	public boolean isCaseSentToSecondaryIndicator() {
		return caseSentToSecondaryIndicator;
	}



	public void setCaseSentToSecondaryIndicator(boolean caseSentToSecondaryIndicator) {
		this.caseSentToSecondaryIndicator = caseSentToSecondaryIndicator;
	}



	public boolean isDshAutoTriggerStepTwo() {
		return dshAutoTriggerStepTwo;
	}



	public void setDshAutoTriggerStepTwo(boolean dshAutoTriggerStepTwo) {
		this.dshAutoTriggerStepTwo = dshAutoTriggerStepTwo;
	}



	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}



	public void setFiveYearBarApplyCode(String fiveYearBarApplyCode) {
		this.fiveYearBarApplyCode = fiveYearBarApplyCode;
	}



	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public void setQualifiedNonCitizenCode(String qualifiedNonCitizenCode) {
		this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public void setFiveYearBarMetCode(String fiveYearBarMetCode) {
		this.fiveYearBarMetCode = fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	public void setUsCitizenCode(String usCitizenCode) {
		this.usCitizenCode = usCitizenCode;
	}



	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("CaseNumber",this.caseNumber);
		obj.put("NonCitLastName",this.nonCitLastName);
		obj.put("NonCitFirstName",this.nonCitFirstName);
		obj.put("NonCitMiddleName",this.nonCitMiddleName);
		obj.put("NonCitBirthDate",this.nonCitBirthDate);
		obj.put("NonCitEntryDate",this.nonCitEntryDate);
		obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		obj.put("NonCitCoaCode",this.nonCitCoaCode);
		obj.put("NonCitEadsExpireDate",this.nonCitEadsExpireDate);
		obj.put("EligStatementCd",this.eligStatementCd);
		obj.put("EligStatementTxt",this.eligStatementTxt);
		obj.put("IAVTypeCode",this.iavTypeCode);
		obj.put("IAVTypeTxt",this.iavTypeTxt);
		obj.put("WebServSftwrVer",this.webServSftwrVer);
		obj.put("GrantDate",this.grantDate);
		obj.put("GrantDateReasonCd",this.grantDateReasonCd);
		obj.put("SponsorDataFoundIndicator",this.sponsorDataFoundIndicator);
		obj.put("ArrayOfSponsorshipData",this.arrayOfSponsorshipData);
		obj.put("SponsorshipReasonCd",this.sponsorshipReasonCd);
		obj.put("PhotoIncludedIndicator",this.photoIncludedIndicator);
		obj.put("PhotoBinaryAttachment",this.photoBinaryAttachment);
		obj.put("CaseSentToSecondaryIndicator",this.caseSentToSecondaryIndicator);
		obj.put("DSHAutoTriggerStepTwo",this.dshAutoTriggerStepTwo);
		obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		obj.put("USCitizenCode",this.usCitizenCode);

		return obj.toJSONString();
	}

}
