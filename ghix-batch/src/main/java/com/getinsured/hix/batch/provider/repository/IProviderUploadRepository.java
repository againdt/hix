package com.getinsured.hix.batch.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.hix.model.ProviderUpload;

/**
 * Interface is used provide JpaRepository of table PROVIDER_UPLOAD.
 * @since Mar 14, 2018
 */
@Repository("iProviderUploadRepository")
public interface IProviderUploadRepository extends JpaRepository<ProviderUpload, Integer> {

	ProviderUpload findFirstByStatusAndProviderTypeIndicatorOrderByIdDesc(String status, String providerTypeIndicator);
}
