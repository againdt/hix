package com.getinsured.iex.hub.rest.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

/** This class is the controller for VCI services
* 
* HIX-30913
* 
* @author Nikhil Talreja
* @since 29-Apr-2014
*
*/
@Controller
public class VCIController extends MessageProcessor {

	@Autowired
	private WebServiceTemplate vciServiceTemplate;
	private static final Logger LOGGER = LoggerFactory.getLogger(VCIController.class);
	
	@RequestMapping(value = "/invokeVCI", method = RequestMethod.POST, produces="application/json")
	@ResponseBody
	public ResponseEntity<String> invokeIFSV(@RequestBody String vciRequestJSON){
		
		ServiceHandler handler  = null;
		GIWSPayload requestRecord = null;
		ResponseEntity<String> response = null;
		try{
			JSONParser parser = new JSONParser();
			JSONObject objInputJson = null;
			objInputJson = (JSONObject)parser.parse(vciRequestJSON);
			
			HubServiceBridge vciBridge = HubServiceBridge.getHubServiceBridge("VCIRequest");
			handler = vciBridge.getServiceHandler();
			String ifsvInputJSON = ((JSONObject) objInputJson.get("payload")).toJSONString();
			handler.setJsonInput(ifsvInputJSON);
			
			requestRecord = new GIWSPayload();
			requestRecord.setEndpointFunction("VCI");
			requestRecord.setEndpointOperationName("VCI");
			requestRecord.setAccessIp((String) objInputJson.get("clientIp"));
			requestRecord.setStatus("PENDING");
			if(objInputJson.get("applicationId") != null){
				requestRecord.setSsapApplicationId((Long) objInputJson.get("applicationId"));
			}
			response = this.executeRequest(requestRecord, handler, vciServiceTemplate);
			
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		return response;
	}
}
