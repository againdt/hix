package com.getinsured.eligibility.at.ref.service;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.disenrollment.service.ReferralDisenrollmentService;
import com.getinsured.eligibility.at.ref.common.ReferralBatchDisenrollDTO;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterRequest;
import com.getinsured.eligibility.plan.service.PlanAvailabilityAdapterResponse;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;

/**
 * @author chopra_s
 * 
 */
@Component("referralBatchDisenrollService")
@Scope("singleton")
public class ReferralBatchDisenrollServiceImpl extends ReferralBatchDisenrollBaseService {
	private static final Logger LOGGER = Logger.getLogger(ReferralBatchDisenrollServiceImpl.class);

	@Autowired
	@Qualifier("referralDisenrollmentService")
	private ReferralDisenrollmentService referralDisenrollmentService;

	@Autowired
	@Qualifier("referralBatchAddressService")
	private ReferralBatchAddressService referralBatchAddressService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Override
	public String csrLevelChange(String[] arguments) {
		String response = GhixConstants.RESPONSE_FAILURE;
		try {
			final ReferralBatchDisenrollDTO referralBatchDisenrollDTO = validateAndPopulateDTO(arguments);
			populateEnrollment(referralBatchDisenrollDTO);
			final boolean status = executeCsLevelChange(referralBatchDisenrollDTO);
			if (status) {
				response = GhixConstants.RESPONSE_SUCCESS;
				if (referralBatchDisenrollDTO.isTriggerNotification()) {
					triggerNotification("EE018FCSPlanChange", referralBatchDisenrollDTO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while referral batch disenrolling for currentApplicationId " + arguments[0] + " - " + ExceptionUtils.getFullStackTrace(e));
		}
		return response;
	}

	private void triggerNotification(String noticeTemplateName, ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		LOGGER.info("Trigger Email - noticeTemplateName: " + noticeTemplateName);

		try {
			if ("EE018FCSPlanChange".equalsIgnoreCase(noticeTemplateName)) {
				referralLceNotificationService.generateCSPlanChangeNotice(referralBatchDisenrollDTO.getEnrolledApplication().getCaseNumber(), referralBatchDisenrollDTO.isHealthDisEnrolled(), referralBatchDisenrollDTO.isDentalDisEnrolled(),referralBatchDisenrollDTO.getTerminationDate());
			} else {
				referralLceNotificationService.generateRatingAreaChangeNotice(referralBatchDisenrollDTO.getEnrolledApplication().getCaseNumber(), referralBatchDisenrollDTO.isHealthDisEnrolled(), referralBatchDisenrollDTO.isDentalDisEnrolled(), referralBatchDisenrollDTO.getTerminationDate());
			}
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("An error occurred while sending the notification" + noticeTemplateName).append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private boolean executeCsLevelChange(ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		boolean blnStatus = false;
		LOGGER.info("Execute Batch Referral Process for CS Level change for - " + referralBatchDisenrollDTO);
		if (referralBatchDisenrollDTO.isHealthEnrolled() && referralBatchDisenrollDTO.isDentalEnrolled()) { // Enrolled in both Health and Dental
			LOGGER.info("Enrolled in Both Health & Dental - " + referralBatchDisenrollDTO);
			if (referralBatchDisenrollDTO.isHealthTerminationFuture() && referralBatchDisenrollDTO.isDentalTerminationFuture()) { // Both Termination in Future
				LOGGER.info("Termination future for Health & Dental - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, false, true);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isDentalPlanAvailable()) {
						planAvailabilityAdapterResponse.setProcessHealth(true); // override this information only for CS flow
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setHealthDisEnrolled(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Dental Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollment(referralBatchDisenrollDTO);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setHealthDisEnrolled(true);
						referralBatchDisenrollDTO.setDentalDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else if (referralBatchDisenrollDTO.isHealthTerminationFuture()) {
				LOGGER.info("Termination future for Health only - " + referralBatchDisenrollDTO);
				blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getHealthEnrollmentId(), true);
				referralBatchDisenrollDTO.setTriggerNotification(true);
				referralBatchDisenrollDTO.setHealthDisEnrolled(true);
			} else if (referralBatchDisenrollDTO.isDentalTerminationFuture()) {
				LOGGER.info("Termination future for Dental only - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, false, true);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isDentalPlanAvailable()) {
						planAvailabilityAdapterResponse.setProcessHealth(true); // override this information only for CS flow
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Dental Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getDentalEnrollmentId(), false);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setDentalDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else {
				LOGGER.info("Termination future for None - " + referralBatchDisenrollDTO);
				closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
				blnStatus = true;
			}
		} else if (referralBatchDisenrollDTO.isHealthEnrolled()) { // Only Enrolled in Health
			LOGGER.info("Enrolled in Health only - " + referralBatchDisenrollDTO);
			if (referralBatchDisenrollDTO.isHealthTerminationFuture()) {
				LOGGER.info("Termination future true for Health only - " + referralBatchDisenrollDTO);
				blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getHealthEnrollmentId(), true);
				referralBatchDisenrollDTO.setTriggerNotification(true);
				referralBatchDisenrollDTO.setHealthDisEnrolled(true);
			} else {
				LOGGER.info("Termination future false for Health only - " + referralBatchDisenrollDTO);
				closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
				blnStatus = true;
			}
		} else {
			LOGGER.error("Cannot process this application - " + referralBatchDisenrollDTO);
		}
		return blnStatus;

	}

	private boolean executeDisenrollment(ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		LOGGER.info("Batch Disenroll complete starts for -  " + referralBatchDisenrollDTO);
		boolean status = false;
		final String responseValue = executeDisenrollment(referralBatchDisenrollDTO.getEnrolledApplication(), referralBatchDisenrollDTO.getTerminationDate());
		if (responseValue.equals(GhixConstants.RESPONSE_SUCCESS)) {
			LOGGER.info("Batch Disenroll successfull for - " + referralBatchDisenrollDTO);
			closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
			status = true;
		} else {
			LOGGER.error("Batch Disenroll failed for - " + referralBatchDisenrollDTO);
		}
		return status;
	}

	private boolean executeDisenrollmentEitherHealthOrDental(ReferralBatchDisenrollDTO referralBatchDisenrollDTO, long enrollmentId, boolean healthOnly) {
		LOGGER.info("Batch Disenroll complete starts for -  " + referralBatchDisenrollDTO);
		boolean status = false;
		final String[] arguments = new String[] { referralBatchDisenrollDTO.getEnrolledApplicationId() + "", enrollmentId + "", ReferralUtil.formatDate(referralBatchDisenrollDTO.getTerminationDate(), ReferralConstants.DEFDATEPATTERN),
		        ReferralConstants.OTHER_DISENROLLMENT_CODE, healthOnly + "" };
		final String responseValue = referralDisenrollmentService.disenrollHealthOrDentalOnly(arguments);
		if (responseValue.equals(GhixConstants.RESPONSE_SUCCESS)) {
			LOGGER.info("Batch Disenroll successfull for - " + referralBatchDisenrollDTO);
			closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
			status = true;
		} else {
			LOGGER.error("Batch Disenroll failed for - " + referralBatchDisenrollDTO);
		}
		return status;
	}

	private boolean executeEnrollmentUpdate(ReferralBatchDisenrollDTO referralBatchDisenrollDTO, PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse) {
		LOGGER.info("BatchPlanAvailabilityAdapterResponse Plan Available for -  " + referralBatchDisenrollDTO + ", planAvailabilityAdapterResponse - isHealthPlanAvailable:" + planAvailabilityAdapterResponse.isHealthPlanAvailable()
		        + " isDentalPlanAvailable:" + planAvailabilityAdapterResponse.isDentalPlanAvailable());
		final boolean status = executeEnrollment(referralBatchDisenrollDTO.getEnrolledApplicationId(), referralBatchDisenrollDTO.getHealthEnrollees(), referralBatchDisenrollDTO.getDentalEnrollees(), planAvailabilityAdapterResponse);
		if (status) {
			LOGGER.info("BatchAutoenrollment update successfull for - " + referralBatchDisenrollDTO);
			closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
		} else {
			LOGGER.error("BatchAutoenrollment update failed for - " + referralBatchDisenrollDTO);
		}
		return status;
	}

	private PlanAvailabilityAdapterResponse executePlanAvailability(ReferralBatchDisenrollDTO referralBatchDisenrollDTO, boolean health, boolean dental) {
		PlanAvailabilityAdapterRequest planAvailabilityAdapterRequest = new PlanAvailabilityAdapterRequest();
		planAvailabilityAdapterRequest.setSsapApplicationId(referralBatchDisenrollDTO.getEnrolledApplicationId());
		planAvailabilityAdapterRequest.setHealthEnrollees(referralBatchDisenrollDTO.getHealthEnrollees());
		planAvailabilityAdapterRequest.setDentalEnrollees(referralBatchDisenrollDTO.getDentalEnrollees());
		planAvailabilityAdapterRequest.setHealthEnrollmentId(referralBatchDisenrollDTO.getHealthEnrollmentId());
		planAvailabilityAdapterRequest.setDentalEnrollmentId(referralBatchDisenrollDTO.getDentalEnrollmentId());
		planAvailabilityAdapterRequest.setCoverageStartDate(referralBatchDisenrollDTO.getTerminationDate());
		planAvailabilityAdapterRequest.setHealthProcess(health);
		planAvailabilityAdapterRequest.setDentalProcess(dental);
		planAvailabilityAdapterRequest.setHealthEnrolled(referralBatchDisenrollDTO.isHealthEnrolled());
		planAvailabilityAdapterRequest.setDentalEnrolled(referralBatchDisenrollDTO.isDentalEnrolled());
		PlanAvailabilityAdapterResponse response = executePlanAvailability(planAvailabilityAdapterRequest);
		response.setProcessHealth(health);
		response.setProcessDental(dental);
		response.setHealthTerminationFuture(referralBatchDisenrollDTO.isHealthTerminationFuture());
		response.setDentalTerminationFuture(referralBatchDisenrollDTO.isDentalTerminationFuture());
		return response;
	}

	@Override
	public String geoLocaleChange(String[] arguments) {
		String response = GhixConstants.RESPONSE_FAILURE;
		try {
			final ReferralBatchDisenrollDTO referralBatchDisenrollDTO = validateAndPopulateDTO(arguments);
			populateEnrollment(referralBatchDisenrollDTO);

			updateAddress(referralBatchDisenrollDTO);

			final boolean status = executeGeoLocaleChange(referralBatchDisenrollDTO);
			if (status) {
				response = GhixConstants.RESPONSE_SUCCESS;
				if (referralBatchDisenrollDTO.isTriggerNotification()) {
					triggerNotification("EE018FRTRatingAreaChange", referralBatchDisenrollDTO);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while referral batch disenrolling for currentApplicationId " + arguments[0] + " - " + ExceptionUtils.getFullStackTrace(e));
		}
		return response;
	}

	private void updateAddress(ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		referralBatchAddressService.updateAddress(referralBatchDisenrollDTO.getCurrentApplication(), referralBatchDisenrollDTO.getEnrolledApplication());
	}

	private boolean executeGeoLocaleChange(ReferralBatchDisenrollDTO referralBatchDisenrollDTO) {
		boolean blnStatus = false;
		LOGGER.info("Execute Batch Referral Process for Geo locale change for - " + referralBatchDisenrollDTO);
		if (referralBatchDisenrollDTO.isHealthEnrolled() && referralBatchDisenrollDTO.isDentalEnrolled()) { // Enrolled in both Health and Dental
			LOGGER.info("Enrolled in Both Health & Dental - " + referralBatchDisenrollDTO);
			if (referralBatchDisenrollDTO.isHealthTerminationFuture() && referralBatchDisenrollDTO.isDentalTerminationFuture()) { // Both Termination in Future
				LOGGER.info("Termination future for Health & Dental - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, true, true);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isHealthPlanAvailable() || planAvailabilityAdapterResponse.isDentalPlanAvailable()) {
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						
						referralBatchDisenrollDTO.setHealthDisEnrolled(!planAvailabilityAdapterResponse.isHealthPlanAvailable());
						referralBatchDisenrollDTO.setDentalDisEnrolled(!planAvailabilityAdapterResponse.isDentalPlanAvailable());
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse No Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollment(referralBatchDisenrollDTO);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setHealthDisEnrolled(true);
						referralBatchDisenrollDTO.setDentalDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else if (referralBatchDisenrollDTO.isHealthTerminationFuture()) {
				LOGGER.info("Termination future for Health only - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, true, false);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isHealthPlanAvailable()) {
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Health Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getHealthEnrollmentId(), true);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setHealthDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else if (referralBatchDisenrollDTO.isDentalTerminationFuture()) {
				LOGGER.info("Termination future for Dental only - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, false, true);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isDentalPlanAvailable()) {
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Dental Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getDentalEnrollmentId(), false);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setDentalDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else {
				LOGGER.info("Termination future for None - " + referralBatchDisenrollDTO);
				closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
				blnStatus = true;
			}
		} else if (referralBatchDisenrollDTO.isHealthEnrolled()) { // Only Enrolled in Health
			LOGGER.info("Enrolled in Health only - " + referralBatchDisenrollDTO);
			if (referralBatchDisenrollDTO.isHealthTerminationFuture()) {
				LOGGER.info("Termination future for Health only - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, true, false);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isHealthPlanAvailable()) {
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Health Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getHealthEnrollmentId(), true);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setHealthDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else {
				LOGGER.info("Termination future for None - " + referralBatchDisenrollDTO);
				closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
				blnStatus = true;
			}
		} else if (referralBatchDisenrollDTO.isDentalEnrolled()) { // Only Enrolled in Dental
			LOGGER.info("Enrolled in Dental only - " + referralBatchDisenrollDTO);
			if (referralBatchDisenrollDTO.isDentalTerminationFuture()) {
				LOGGER.info("Termination future for Dental only - " + referralBatchDisenrollDTO);
				final PlanAvailabilityAdapterResponse planAvailabilityAdapterResponse = executePlanAvailability(referralBatchDisenrollDTO, false, true);
				if (!StringUtils.equals(planAvailabilityAdapterResponse.getStatus(), GhixConstants.RESPONSE_FAILURE)) {
					LOGGER.info("BatchPlanAvailabilityAdapterResponse success for -  " + referralBatchDisenrollDTO);
					if (planAvailabilityAdapterResponse.isDentalPlanAvailable()) {
						blnStatus = executeEnrollmentUpdate(referralBatchDisenrollDTO, planAvailabilityAdapterResponse);
						referralBatchDisenrollDTO.setTriggerNotification(true);
					} else {
						LOGGER.info("BatchPlanAvailabilityAdapterResponse Dental Plan Not Available for -  " + referralBatchDisenrollDTO);
						blnStatus = executeDisenrollmentEitherHealthOrDental(referralBatchDisenrollDTO, referralBatchDisenrollDTO.getDentalEnrollmentId(), false);
						referralBatchDisenrollDTO.setTriggerNotification(true);
						referralBatchDisenrollDTO.setDentalDisEnrolled(true);
					}
				} else {
					LOGGER.error("BatchPlanAvailabilityAdapterResponse failed for  " + referralBatchDisenrollDTO);
				}
			} else {
				LOGGER.info("Termination future for None - " + referralBatchDisenrollDTO);
				closeSsapApplicationById(referralBatchDisenrollDTO.getCurrentApplication().getId());
				blnStatus = true;
			}
		} else {
			LOGGER.error("Cannot process this application - " + referralBatchDisenrollDTO);
		}
		return blnStatus;
	}

}
