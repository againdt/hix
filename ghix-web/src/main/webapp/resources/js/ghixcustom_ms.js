$(document).ready(function(){
	/*HIX-17077*/
	//$('h1').parent('div').addClass('titlebar');
	
	//add .header to thead tr
    $('.table thead tr').addClass('header');
    
    
    $('.clearForm').click(function(){
        $(this).parents("form").find("input").not(':button,:hidden').val("");
        $('label.error').hide();
	});
    
    $('#rightpanel .graydrkbg').find('.btn').addClass('btn-small');
    
    //HIX-14907 //Commented below as the changes are made to broker_menu.properties file HIX-20152
    /*$('a[title="Account"]').text('My Information');*/
	
	/*HIX-18823 Remove Modal on press of escape button */
	$(document).keyup(function(e) {
		if (e.keyCode == 27) {
			e.preventDefault();
			if ($('#editdependent, #einModal, #worksite').is(':visible')){
				e.preventDefault();
			}else{
				//commented for - HIX-48280 modal-backdrop should not get removed on ESC press
				//$('.modal-backdrop, .modal-backdrop.in').remove(); 
				//$('#addressIFrame, .addressErrModal, #modal').remove();
			}
		}  
	});
	
	//HIX-48422 this makes a block issue 
	
	//Added keyboard false for all the modal by default for - HIX-48280
	/*$('.modal').removeData('modal').modal({		
		keyboard: false
	});*/
	
	$('.modalClose').live('click',function(){
		$('#modal, .modal-backdrop').remove();
		window.location.reload();
	});
});
