package com.getinsured.eligibility.at.ref.service;

import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.CompareApplicationDTO;
import com.getinsured.eligibility.at.ref.dto.CompareMainDTO;
import com.getinsured.eligibility.at.ref.handler.DemographicCompareHandler;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.ref.transform.CompareApplicationTransformer;
import com.getinsured.eligibility.repository.CmrHouseholdRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * @author chopra_s
 * 
 */
@Component("referralDemographicService")
@DependsOn("dynamicPropertiesUtil")
@Scope("singleton")
public class ReferralDemographicServiceImpl implements ReferralDemographicService {
	private static final Logger LOGGER = Logger.getLogger(ReferralDemographicServiceImpl.class);

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("compareApplicationTransformer")
	private CompareApplicationTransformer compareApplicationTransformer;

	@Autowired
	@Qualifier("demographicCompareHandler")
	private DemographicCompareHandler demographicCompareHandler;

	@Autowired
	@Qualifier("referralAdminUpdateService")
	private ReferralAdminUpdateService referralAdminUpdateService;

	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	
	@Autowired
	private CmrHouseholdRepository cmrHouseholdRepository;
	
	@Override
	@ReferralTransactionAnno
	public CompareMainDTO executeCompare(long enrolledApplicationId, long currentApplicationId) throws Exception {
		CompareMainDTO compareMainDTO = null;
		try {
			LOGGER.info("Demographic Compare Current Referral with Enrolled Application starts between enrolled application - " + enrolledApplicationId + " and current application " + currentApplicationId);
			final SsapApplication enrolledApplication = loadSsapApplicants(enrolledApplicationId);
			final SsapApplication currentApplication = loadSsapApplicants(currentApplicationId);
			compareMainDTO = transformCompareDto(enrolledApplication, currentApplication);
			getHHEnrolledMailingAddress(compareMainDTO.getEnrolledApplication(),enrolledApplication);
			
			//As per Srinis comment (e-MAIL#Subject-ID state specific check)Comparison will be applicable for all states
			//if (isStateID()) {
				executeDemographic(compareMainDTO);
			//}

			LOGGER.info("Demographic Compare Current Referral with Enrolled Application Ends");
		} catch (Exception e) {
			throw e;
		}
		return compareMainDTO;
	}
	
	private void getHHEnrolledMailingAddress(CompareApplicationDTO enrolledCompareApplicationDto, SsapApplication enrolledApplication) {
		Household enrolledHH = cmrHouseholdRepository.getOne(enrolledApplication.getCmrHouseoldId().intValue());
		enrolledCompareApplicationDto.setMailingAddress(getAddress(enrolledHH.getPrefContactLocation()));
	}

	private void executeDemographic(CompareMainDTO compareMainDTO) {
		ssapEnrolleeHandler.parseListofEnrollees(compareMainDTO);
		runComparator(compareMainDTO);
		executeAdminUpdate(compareMainDTO);
	}

	private void executeAdminUpdate(CompareMainDTO compareMainDTO) {
		if (!compareMainDTO.getEnrolledApplication().isHasAdminUpdate()) {
			return;
		}

		referralAdminUpdateService.executeAdminUpdate(compareMainDTO);
	}

	private void runComparator(CompareMainDTO compareMainDTO) {
		demographicCompareHandler.processCompare(compareMainDTO);
	}

	private CompareMainDTO transformCompareDto(SsapApplication enrolledApplication, SsapApplication currentApplication) {
		CompareMainDTO compareMainDTO = new CompareMainDTO();
		compareMainDTO.setEnrolledApplication(compareApplicationTransformer.transformApplication(enrolledApplication, true));
		compareMainDTO.setCurrentApplication(compareApplicationTransformer.transformApplication(currentApplication, true));
		return compareMainDTO;
	}

	private SsapApplication loadSsapApplicants(Long enApp) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(enApp);
	}

	private boolean isStateID() {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		return "ID".equals(stateCode);
	}
	
	private Address getAddress(Location location){
		Address address = new Address();

		if (location.getCity()!=null) {
			address.setCity(location.getCity());
		}
		if (location.getZip() != null) {
			address.setPostalCode(location.getZip());
		}
		if (location.getState()!= null) {
			address.setState(location.getState());
		}
		if (location.getAddress1() != null) {
			address.setStreetAddress1(location.getAddress1());
		}
		if (location.getAddress2() != null) {
			address.setStreetAddress2(location.getAddress2());
		}
		if (location.getCounty() != null) {
			address.setCounty(location.getCounty());
		}
		return address;
	}
}
