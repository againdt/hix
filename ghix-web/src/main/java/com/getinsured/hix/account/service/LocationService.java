package com.getinsured.hix.account.service;

import com.getinsured.hix.model.Location;

public interface LocationService{
	
	Location findById(int id);
	
	Location save(Location location);
	
	String getLoationsJSON(Location location);
}