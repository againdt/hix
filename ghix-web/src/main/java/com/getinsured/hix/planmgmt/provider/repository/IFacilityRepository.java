package com.getinsured.hix.planmgmt.provider.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.Facility;

public interface IFacilityRepository extends JpaRepository<Facility, Integer> {
	
	@Query("SELECT f FROM Facility f, Provider p left join p.providerNetwork as providerNetwork where LOWER(p.datasourceRefId) = LOWER(:datasourceRefId) AND (p.id=f.provider) And providerNetwork.network.id=:networkId")
    Facility getFacilityByDataSourceRefId(@Param("datasourceRefId") String datasourceRefId,@Param("networkId") int networkId);
	
	@Query("SELECT DISTINCT f FROM Facility f, Provider p , FacilityAddress fa, Specialty s, FacilitySpecialityRelation fsr where LOWER(p.type)=LOWER(:type) AND p.id=f.provider.id AND f.id=fa.facility.id AND (f.id=fsr.facility.id AND s.id=fsr.specialty.id) AND (LOWER(p.name) like '%'|| LOWER(:name) ||'%' AND LOWER(s.name) like '%'|| LOWER(:hospitalType) ||'%'AND fa.zip like '%'||:hospitalNearZip||'%')")
	List<Facility> getHospitals(@Param("name") String name,@Param("hospitalType") String hospitalType,@Param("hospitalNearZip") String hospitalNearZip, @Param("type") String type);
	
	@Query("SELECT DISTINCT f FROM Plan pl, Network n, Provider p, ProviderNetwork pn, Facility f, FacilityAddress fa, Specialty s, FacilitySpecialityRelation fsr WHERE LOWER(p.type)=LOWER(:type) AND pl.id=:planId AND pl.network.id=:networkId AND pl.network.id=n.id AND n.id=pn.network.id AND p.id=pn.provider.id AND p.id=f.provider.id AND f.id=fa.facility.id AND (f.id=fsr.facility.id AND s.id=fsr.specialty.id) AND (LOWER(p.name) like '%'|| LOWER(:name) ||'%' AND LOWER(s.name) like '%'|| LOWER(:hospitalType) ||'%'AND fa.zip like '%'||:hospitalNearZip||'%')")
	List<Facility> getFacilitiesForNetworkAndPlan(@Param("planId") Integer planId, @Param("networkId") Integer networkId, @Param("name") String name,@Param("hospitalType") String hospitalType,@Param("hospitalNearZip") String hospitalNearZip, @Param("type") String type);
	
	@Query("SELECT DISTINCT f FROM Facility f, Provider p, FacilityAddress fa " +
			" WHERE " +
			"LOWER(p.type)=LOWER(:type) " +
			"AND (p.id=f.provider.id AND f.id=fa.facility.id) " +
			"AND (( :name IS NOT NULL AND LOWER(p.name) like LOWER(:name)|| '%') " +
			"AND (((:cityName IS NOT NULL) AND (LOWER(fa.city) like LOWER(:cityName)||'%'  )) OR ((:zipCode IS NOT NULL) AND (LOWER(fa.zip) = LOWER(:zipCode)))))")
	Page<Facility> findFacility(@Param("name") String name, @Param("cityName") String cityName , @Param("zipCode") String zipCode ,@Param("type") String type,Pageable pageable);
	
	@Query("SELECT DISTINCT f FROM Facility f, Provider p, FacilityAddress fa " +
			" WHERE " +
			"LOWER(p.type)=LOWER(:type) " +
			"AND (p.id=f.provider.id AND f.id=fa.facility.id) " +
			"AND ((:name IS NOT NULL) AND (LOWER(p.name) like LOWER(:name)|| '%')) ")
	Page<Facility> findFacilityByName(@Param("name") String name, @Param("type") String type,Pageable pageable);
	
	@Query("SELECT DISTINCT f FROM Facility f, Provider p, FacilityAddress fa " +
			" WHERE " +
			"LOWER(p.type)=LOWER(:type) " +
			"AND (p.id=f.provider.id AND f.id=fa.facility.id) " +
			"AND (((:cityName IS NOT NULL) AND (LOWER(fa.city) like LOWER(:cityName)||'%'  )) OR ((:zipCode IS NOT NULL) AND (LOWER(fa.zip) = LOWER(:zipCode))))")
	Page<Facility> findFacilityByLocation(@Param("cityName") String cityName , @Param("zipCode") String zipCode ,@Param("type") String type,Pageable pageable);
}
