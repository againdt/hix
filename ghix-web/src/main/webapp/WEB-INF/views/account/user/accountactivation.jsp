<%-- <%@page isELIgnored="false"  %> --%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
	
<div class="gutter10">
	<div class="row-fluid">
		<h1>New Account Set-Up</h1>
	</div>
		
	<div class="row-fluid">	
		<div class="alert alert-error margin30-t gutter40-b">
			<c:if test="${errorMsg != ''}">
				<c:choose>
					<c:when test="${errorMsg =='ReferralActivationError' }">
									<p style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px;">
									 You have clicked on a link that is no longer usable. Please  check your mail for another message from ${exchangeName} with an updated link that you can use to activate your application</li>
									  </p>
							<br>
					</c:when>
					<c:when test="${errorMsg =='ReferralActivationExpired' }">
									<p style="font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 18px;">
									You have clicked on a link that is no longer usable. The Enrollment Period during which the link was valid has ended.
									  </p>
							<br>
					</c:when>
					<c:when test="${errorMsg =='accountActivationException' }">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(exchangeType,'individual')}">
									 You have tried to use a one time account activation link that has expired. <br/>
											<ul>
											<li> Please contact your ${exchangeName} Administrator.</li>
											</ul>
							</c:when>
							<c:otherwise>
									 You have tried to use a one time account activation link that has expired. <br/>
											<ul>
											<li>Please contact your employer if you are an employee looking to participate in employer sponsored health insurance.</li>
											<li> Please contact your ${exchangeName} Administrator.</li>
											</ul>
							</c:otherwise>
						</c:choose>
					</c:when>
					<c:otherwise>
						<p><c:out value="${errorMsg}"></c:out></p>
					</c:otherwise>
				</c:choose>			
			</c:if>
			<br>
		</div>
	</div>

	 <c:if test="${errorMsg == ''}">
		<div class="row-fluid">		
			<div class="span3" id="sidebar">
				<div class="header">
					<h4 class="margin0">You should know</h4>
				</div>
			    <ul class="nav nav-list">
				    <li>An account has been created for you on ${exchangeName}. In order to access it please follow the simple process.</li>
			    </ul>	
			</div>
	
			<div class="span9" id="rightpanel">
				<div class="header">
					<h4>Your information</h4>
				</div>
					<c:if test="${errorMsg == ''}">
						<form class="form-horizontal" id="frmactivation" name="frmactivation" action="<c:url value="/account/user/activation" />" method="POST">
							<df:csrfToken/>
							<div class="account_activation">
								<c:if test="${username == '' }">
									<div class="control-group">
										<p>A record for you to become a representative of the ${creatorType}, ${creatorName} has been created on ${exchangeName}.&nbsp;In order to access this record, you must create a new account on the exchange, or use an existing account.</p>
										<div class="controls">
											<!-- <label> <input type="radio" name="account" id="existingaccount" checked='checked'  value="YES">
												I have an account
											</label> -->
											<label> <input type="radio" name="account" checked='checked' id="newaccount" value="NO">
												I don't have an account
											</label> 
											
										</div>					
									</div>	
								</c:if>
								<c:if test="${username != '' }">
									<p>You have completed initial step(s). Please click on continue to complete pending account activation process.</p>
								</c:if>
							
								<input type="hidden" name="createdType" id="createdType"  value="${createdType}" />
								<input type="hidden" name="emailId" id="emailId"  value="${emailId}" />
								<input type="hidden" name="activationId" id="activationId"  value="${activationId}" />
								<input type="hidden" name="username" id="username"  value="${username}" />
								<div class="control-group">
									<div class="controls">							
										<input type="submit" name="continue" id="continue" value="<spring:message  code='label.brkContinue'/>" class="btn btn-primary btn-large" title="<spring:message  code='label.brkContinue'/>">
									</div>
								</div>									
							</div>						
						</form>
					</c:if>
				</div>
		</div>
	</c:if>
</div>	