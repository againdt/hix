package com.getinsured.hix.broker.repository;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.history.Revision;
import org.springframework.data.history.Revisions;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.Broker;

public class IBrokerRepositoryTest extends BaseTest {
	
	@Autowired	private IBrokerRepository brokerRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(IBrokerRepositoryTest.class);
	
	@Test
	public void test() {
		
		Broker brokerObj =  brokerRepository.findByUserId(4);
		
		//revision....
		Page<Revision<Integer, Broker>> revisions = brokerRepository.findRevisions(brokerObj.getId(), new PageRequest(0, 10));
		Revisions<Integer, Broker> wrapper = new Revisions<Integer, Broker>(revisions.getContent());
		
		for (Revision<Integer, Broker> revision : wrapper) {
			logger.info(revision.getEntity().toString());
		}

		Revision<Integer, Broker> revision = brokerRepository.findLastChangeRevision(brokerObj.getId());
		Broker broker = revision.getEntity();
		logger.info(broker.getCertificationStatus());
		
		Revisions<Integer, Broker> revision_7 = brokerRepository.findRevisions(brokerObj.getId());
		List<Revision<Integer, Broker>> brokers = revision_7.getContent();
		for (Revision<Integer, Broker> revision2 : brokers) {
			logger.info(revision2.getEntity().toString());
		}				
	}

}
