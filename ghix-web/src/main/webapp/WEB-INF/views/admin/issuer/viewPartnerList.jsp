<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>

<style>
th .icon-cog,
th .caret {color: #fff;}
.table th {padding: 0 0 0 5px !important;}
</style>

<div class="gutter10">
<div class="row-fluid">
	<ul class="page-breadcrumb">
       <li><a href="<c:url value="/admin/issuer/details/${issuerObj.id}"/>">&lt; <spring:message  code="label.back"/></a></li>
       <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
       <li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
       <li><spring:message  code="pgheader.partners"/></li>
    </ul><!--page-breadcrumb ends-->
    
    <h1><img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img"/>${issuerObj.name}</h1>
</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4><spring:message  code="pgheader.issuerAbout"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                        <li><a href="<c:url value="/admin/issuer/details/${issuerObj.id}"/>"><spring:message  code="pgheader.issuerDetails"/></a></li>
                        <li><a href="<c:url value="/admin/issuer/representative/manage/${issuerObj.id}"/>"><spring:message  code="pgheader.issuerRepresentative"/></a></li>
                        <c:if test="${exchangeState != 'ID' && exchangeState != 'MN'}">
                        	<li><a href="<c:url value="/admin/issuer/financial/info/list/${issuerObj.id}"/>"><spring:message  code="pgheader.financialInformation"/></a></li>
                        </c:if>	      
                       	<li><a href="<c:url value="/admin/issuer/company/profile/${issuerObj.id}"/>"><spring:message  code="pgheader.update.companyProfile"/></a></li>
                        <li><a href="<c:url value="/admin/issuer/market/individual/profile/${issuerObj.id}"/>"><spring:message  code="pgheader.individualMarketProfile"/></a></li>
                       <%--  <c:if test="${exchangeState != 'ID'}">
                        	<li><a href="<c:url value="/admin/issuer/market/shop/profile/${issuerObj.id}"/>"><spring:message  code="pgheader.update.shopMarketProfile"/></a></li>
                        </c:if> --%>
                        <li><a href="<c:url value="/admin/issuer/accreditationdocument/view/${issuerObj.id}" />"><spring:message  code="pgheader.accreditationDocuments"/></a></li>
                        <li><a href="<c:url value="/admin/issuer/certification/status/${issuerObj.id}"/>"><spring:message  code="pgheader.certificationStatus"/></a></li>
                        <li class="active"><a href="<c:url value="/admin/issuer/partner/list/${issuerObj.id}"/>"><spring:message  code="pgheader.ediTransactions"/></a></li>
                        <li><a href="<c:url value="/admin/issuer/history/${issuerObj.id}"/>"><spring:message  code="pgheader.issuerHistory"/></a></li>
                        <li><a href="<c:url value="/admin/issuer/crossWalkStatus/${issuerObj.id}"/>"><spring:message  code="pgheader.crossWalkStatus"/></a></li>
                        <c:if test="${exchangeState == 'ID'}">
                        <li><a href="<c:url value="/admin/issuer/displayNetworkTransparencyData/${encIssuerId}"/>"><spring:message code="pgheader.networkTransparencyStatus" /></a></li>
                        </c:if>
            			 <c:if test="${activeRoleName == 'OPERATIONS'}">
							<li><a href="<c:url value="/admin/issuer/issuerPaymentInfo/${encIssuerId}"/>"><spring:message  code="pgheader.IssuerPaymentInfo"/></a></li>
						</c:if>
                       
                </ul>
                <!-- end of side bar -->
                <br />
                <a href='<c:url value="/admin/issuer/partner/add/${issuerObj.id}"/>' class="btn btn-primary"><i class="icon-plus icon-white"></i>  <spring:message  code="label.addEdiTransaction"/></a>
			</div>
			<!-- end of span3 -->
			<div class="span9" id="rightpanel">
             	<c:choose>
						<c:when test="${fn:length(listExchgPartnersObj) > 0}">
							<table class="table table-condensed table-border-none table-striped">
								<thead>				
								 		<tr class="header">
										
										<spring:message code="label.insurerName" var="insurerName" />
										<spring:message code="label.type" var="type" />
										<spring:message code="label.direction" var="direction" />
										<spring:message code="label.market" var="market" />
										
 										<!-- <th scope="col" class="sortable"><dl:sort title="${insurerName}" sortBy="insurerName" sortOrder="${sortOrder}"></dl:sort></th> -->
										<th scope="col" class="sortable"><dl:sort title="${type}" sortBy="st01" sortOrder="${sortOrder}"></dl:sort></th>
										<th scope="col" class="sortable"><dl:sort title="${direction}" sortBy="direction" sortOrder="${sortOrder}"></dl:sort></th>
										<th scope="col" class="sortable"><dl:sort title="${market}" sortBy="market" sortOrder="${sortOrder}"></dl:sort></th>
								 		
										<th><i class="icon-cog"></i><b class="caret"></b></th>
									</tr>
								</thead>
								
								<c:forEach items="${listExchgPartnersObj}" var="listExchgPartnersObj" varStatus="vs">
									<tr>
										<!-- <td id="name_${listExchgPartnersObj.id}"><a href="<c:url value="/admin/issuer/partner/${issuerObj.id}?partnerId=${listExchgPartnersObj.id}" />">${listExchgPartnersObj.insurerName}</a></td> -->
										<td align="center">${listExchgPartnersObj.st01} </td>
										<td align="center">${listExchgPartnersObj.direction} </td>
										<c:if test="${listExchgPartnersObj.market == 'FI'}">
											<td align="center"><spring:message code="label.individual"/></td>
										</c:if>
										<c:if test="${listExchgPartnersObj.market == '24'}">
											<td align="center"><spring:message code="label.shop"/></td>
										</c:if>
										<td align="center">
											<div class="" style="width:50px">
													<div class="dropdown">
														<a href="/page.html" data-target="#"
															data-toggle="dropdown" role="button" id="dLabel"
															class="dropdown-toggle"><i
															class="icon-cog"></i><b class="caret"></b>
														</a>
														<ul id='action_${vs.index}' aria-labelledby="dLabel" role="menu"
															class="dropdown-menu pull-right"><!-- class removed "action-align-l" -->	
															<li><a href="#" onClick="updatePartner(this,'edit');" id="edit_${vs.index}"><i class="icon-pencil"></i> Edit</a></li>													
															<li><a href="#" onClick="updatePartner(this,'delete');" id="delete_${vs.index}"><i class="icon-remove"></i> Delete</a></li>
													
														</ul>
													</div>
												</div>
											<input type="hidden" value="${listExchgPartnersObj.id}" name="partnerid" id="partnerid_${vs.index}" /> 			
											<input type="hidden" value="" name="existingpartnerid" id="existingpartnerid" />
										</td>
									</tr>
								
								</c:forEach>
							</table>
							<input type="hidden" value="${issuerObj.id}" name="issuerid" id="issuerid" />
							<input type="hidden" id="redirectTo" name="redirectTo" value="${redirectUrl}"/>
							<div class="center"><dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/></div>
						</c:when>
					<c:otherwise>
						<div class="alert alert-info"><spring:message code="label.recordsNotFound"/></div>
					</c:otherwise>
				</c:choose>
           </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
</div>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<script type="text/javascript">
	$('.ttclass').tooltip();
	
	function updatePartner(e,test) {		
		if (test == 'edit') {
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);				
			location.href= "<c:url value='/admin/issuer/partner/edit/"+ $("#issuerid").val() +"?partnerId="+ $("#partnerid" + index).val() +"'/>";		
											
		} else if (test == 'delete') {
			var startindex = (e.id).indexOf("_");
			var index = (e.id).substring(startindex, (e.id).length);				
			location.href= "<c:url value='/admin/issuer/partner/delete/"+ $("#issuerid").val() +"?partnerId="+ $("#partnerid" + index).val() +"'/>";		
											
		}
		
	}	
</script>
