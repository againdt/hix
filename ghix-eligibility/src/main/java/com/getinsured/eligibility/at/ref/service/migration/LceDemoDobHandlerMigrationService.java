package com.getinsured.eligibility.at.ref.service.migration;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.common.ReferralTransactionAnno;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerBaseService;
import com.getinsured.eligibility.at.ref.service.LceProcessHandlerService;
import com.getinsured.iex.ssap.model.SsapApplication;

@Component("lceDemoDobHandlerMigrationService")
public class LceDemoDobHandlerMigrationService extends LceProcessHandlerBaseService implements LceProcessHandlerService {
	private static final Logger LOGGER = Logger.getLogger(LceDemoDobHandlerMigrationService.class);

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;

	@Override
	@ReferralTransactionAnno
	public void execute(LCEProcessRequestDTO lceProcessRequestDTO) {
		LOGGER.info("LceDemoDobHandlerService starts for current application id - " + lceProcessRequestDTO.getCurrentApplicationId() + " and enrolled application id - " + lceProcessRequestDTO.getEnrolledApplicationId());
		final long currentApplicationId = lceProcessRequestDTO.getCurrentApplicationId();
		final long enrolledApplicationId = lceProcessRequestDTO.getEnrolledApplicationId();
		SsapApplication currentApplication = loadCurrentApplication(currentApplicationId);
		createSepEvent(currentApplication);
		getValidationStatus(currentApplication.getCaseNumber());
		
		currentApplication = loadCurrentApplication(currentApplicationId);//HIX-108047
		
		updateAllowEnrollment(currentApplication, Y);
		LOGGER.info("LceDemoDobHandlerService ends for current application id - " + currentApplicationId + " and enrolled application id - " + enrolledApplicationId);
	}

	private void createSepEvent(SsapApplication currentApplication) {
		LOGGER.info("Create Dob SEP event - " + currentApplication.getId());
		ssapApplicationEventMigrationService.createDoBApplicationEvent(currentApplication.getId());
	}
}

