package com.getinsured.hix.batch.enrollment.util;

import java.io.File;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.beanutils.BeanPredicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.getinsured.enrollment._1095.cy_2019.treasury.irs.common.BatchCategoryCodeType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.common.CompletePersonNameType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.AnnualPolicyInformationType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.CoverageHouseholdGrpType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.CoveredIndividualType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.EOYPersonType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.EPDPersonType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.Form1095AUpstreamDetailType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.MonthlyPolicyInformationType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.OtherCompletePersonNameType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.PersonInformationType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.RecipientPolicyCoverageDtlType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.StateType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.TaxHouseholdPolicyInformationDtlType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.ext.aca.air.ty19a.USAddressGrpType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.msg.form1095atransmissionupstreammessage.Form1095ATransmissionUpstreamType;
import com.getinsured.enrollment._1095.cy_2019.treasury.irs.msg.form1095atransmissionupstreammessage.ObjectFactory;
import com.getinsured.hix.batch.enrollment.service.Enrollment1095NamespacePrefixMapper;
import com.getinsured.hix.batch.enrollment.service.EnrollmentPremiumMonthPredicate;
import com.getinsured.hix.enrollment.service.EnrollmentServiceImpl;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentIrsEscapeHandler;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.enrollment.Enrollment1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095;
import com.getinsured.hix.model.enrollment.EnrollmentMember1095.MemberType;
import com.getinsured.hix.model.enrollment.EnrollmentPremium1095;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.exception.GIException;

/**
 * Coverage year 2019 and above schema service implementation of outbound 1095 XML
 */
@Service
public class Enrollment1095XmlCy2019 extends Enrollment1095Xml {

	private static final Logger LOGGER = LoggerFactory.getLogger(Enrollment1095XmlCy2019.class);

	@Override
	public String generateXml( List<Integer> enrollment1095Ids,Integer docSeqId, String wipFolder, Map<Integer, String> recordSequenceIdMap,
		Enrollment1095XmlWrapper enrollment1095XmlWrapper, String batchCategoryCode) throws GIException {
		
		Form1095ATransmissionUpstreamType annual1095TransmissionObj = new Form1095ATransmissionUpstreamType();
		StringBuilder enrollmentIds = new StringBuilder();
		Map<String, List<Enrollment1095>> successSkipMap = getUpstreamReportDetails(annual1095TransmissionObj.getForm1095AUpstreamDetail(), enrollment1095Ids, recordSequenceIdMap, enrollmentIds, enrollment1095XmlWrapper);
		String generatedFileName = null;
		if(annual1095TransmissionObj != null && !annual1095TransmissionObj.getForm1095AUpstreamDetail().isEmpty()){
			generatedFileName = generateIrsXml(annual1095TransmissionObj, docSeqId, wipFolder);
			enrollmentIds.deleteCharAt(enrollmentIds.length()-1);
			logEnrollmentOut1095(enrollmentIds.toString(), generatedFileName, enrollment1095XmlWrapper);
		}else {
			LOGGER.error("Empty Form1095AUpstreamDetailType list, please check the following IDs for processing errors : " + enrollment1095Ids);
		}
		updateStagingTable(successSkipMap, generatedFileName, docSeqId, recordSequenceIdMap, batchCategoryCode, enrollment1095XmlWrapper);
		return null;
	}

	private Map<String, List<Enrollment1095>> getUpstreamReportDetails(List<Form1095AUpstreamDetailType> form1095aUpstreamDetail,
			List<Integer> enrollment1095Ids, Map<Integer, String> recordSequenceIdMap, StringBuilder enrollmentIds, Enrollment1095XmlWrapper enrollment1095XmlWrapper) {
		Integer recordSequenceNumber = 1;
		Map<String, List<Enrollment1095>> successSkipMap = new HashMap<String, List<Enrollment1095>>();
		List<Enrollment1095> successList = new ArrayList<Enrollment1095>();
		List<Enrollment1095> skipList = new ArrayList<Enrollment1095>();
		for(Integer enrollment1095Id : enrollment1095Ids){
			Enrollment1095 enrollment1095 = enrollment1095Repository.findOne(enrollment1095Id);
			try{
				if(null != enrollment1095){
					Form1095AUpstreamDetailType form1095aUpstreamDetailType = new Form1095AUpstreamDetailType();
					form1095aUpstreamDetailType.setCorrectedInd("0");
					form1095aUpstreamDetailType.setVoidInd("0");
					form1095aUpstreamDetailType.setLineNum(BigInteger.ZERO);
					form1095aUpstreamDetailType.setRecordType(StringUtils.EMPTY);
					if(enrollment1095XmlWrapper.isRegenerateIndicator()){
						if(!BatchCategoryCodeType.IRS_EOY_REQ.name().equalsIgnoreCase(enrollment1095XmlWrapper.getBatchCategoryCode()) && !enrollment1095XmlWrapper.isVoidIndicator()){
							form1095aUpstreamDetailType.setCorrectedInd("1");
							form1095aUpstreamDetailType.setVoidInd("0");
							form1095aUpstreamDetailType.setCorrectedRecordSequenceNum(enrollment1095.getCorrectedRecordSeqNum());
						}else if(enrollment1095XmlWrapper.isVoidIndicator()){
							form1095aUpstreamDetailType.setCorrectedInd("0");
							form1095aUpstreamDetailType.setCorrectedRecordSequenceNum(null);
							form1095aUpstreamDetailType.setVoidInd("1");
							form1095aUpstreamDetailType.setVoidedRecordSequenceNum(enrollment1095.getCorrectedRecordSeqNum());
						}
					}else if(!enrollment1095XmlWrapper.isFreshRun() && !enrollment1095XmlWrapper.isVoidIndicator()){
						form1095aUpstreamDetailType.setCorrectedInd("1");
						form1095aUpstreamDetailType.setVoidInd("0");
						form1095aUpstreamDetailType.setCorrectedRecordSequenceNum(enrollment1095.getCorrectedRecordSeqNum());
					}else if(enrollment1095XmlWrapper.isVoidIndicator()){
						form1095aUpstreamDetailType.setCorrectedInd("0");
						form1095aUpstreamDetailType.setCorrectedRecordSequenceNum(null);
						form1095aUpstreamDetailType.setVoidInd("1");
						form1095aUpstreamDetailType.setVoidedRecordSequenceNum(enrollment1095.getCorrectedRecordSeqNum());
					}
					form1095aUpstreamDetailType.setMarketplaceId(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_HEALTH_EXCHANGE_ID));
					form1095aUpstreamDetailType.setRecordSequenceNum(recordSequenceNumber.toString());
					form1095aUpstreamDetailType.setTaxYr(String.valueOf(enrollment1095.getCoverageYear()));
					form1095aUpstreamDetailType.setPolicy(getPolicyDetails(enrollment1095));

					List<EnrollmentMember1095> enrollmentMembers = enrollment1095.getEnrollmentMembers();
					List<EnrollmentPremium1095> enrollmentPremium = enrollment1095.getEnrollmentPremiums();

					form1095aUpstreamDetailType.setRecipient(getPersonInformationType(getMemberFromList(enrollmentMembers, MemberType.RECEPIENT)));
					form1095aUpstreamDetailType.setRecipientSpouse(getEOYPersonType(getMemberFromList(enrollmentMembers, MemberType.SPOUSE)));

					form1095aUpstreamDetailType.setCoverageHouseholdGrp(getCoveredHouseholdGroup(getMemberFromList(enrollmentMembers, MemberType.MEMBER)));
					form1095aUpstreamDetailType.setRecipientPolicyInformation(getRecipientPolicyInformation(enrollmentPremium, enrollment1095));

					recordSequenceIdMap.put(enrollment1095Id, recordSequenceNumber.toString());
					enrollmentIds.append(enrollment1095.getExchgAsignedPolicyId());
					enrollmentIds.append(",");
					recordSequenceNumber++;
					form1095aUpstreamDetail.add(form1095aUpstreamDetailType);
					successList.add(enrollment1095);
				}
			}catch(Exception e){
				LOGGER.info("Skipping enrollment 1095 Id :: "+ enrollment1095Id, e);
				enrollment1095.setXmlSkippedFlag(Enrollment1095.YorNFlagIndicator.Y.toString());
				enrollment1095.setXmlSkippedMsg(EnrollmentUtils.shortenedStackTrace(e, 3));
				skipList.add(enrollment1095);
			}
		}
		successSkipMap.put(SUCCESS, successList);
		successSkipMap.put(SKIPPED, skipList);

		return successSkipMap;
	}
	
	/**
	 * Get Policy Info
	 * @param enrollmentHouseholdInfoDTO
	 * @return
	 */
	private RecipientPolicyCoverageDtlType getPolicyDetails(Enrollment1095 enrollment1095) {
		RecipientPolicyCoverageDtlType coverageDtlType = new RecipientPolicyCoverageDtlType();
		coverageDtlType.setMarketPlacePolicyNum(String.format("%06d", enrollment1095.getExchgAsignedPolicyId()));
		coverageDtlType.setPolicyIssuerNm(removeSplCharactersForPolicyNm(enrollment1095.getPolicyIssuerName()));
		coverageDtlType.setPolicyStartDt(getXmlGregorianCalendarDate(enrollment1095.getPolicyStartDate()));
		coverageDtlType.setPolicyTerminationDt(getXmlGregorianCalendarDate(enrollment1095.getPolicyEndDate()));
		return coverageDtlType;
	}
	
	/**
	 * Get Spouse PersonInformationType obj
	 * @param memberFromList
	 * @return {@link PersonInformationType}
	 */
	private PersonInformationType getPersonInformationType(List<EnrollmentMember1095> memberFromList) {
		PersonInformationType personInformationType = null;
		if(null != memberFromList && !memberFromList.isEmpty()){
			EnrollmentMember1095 member = memberFromList.get(0);
			if(null != member){
				personInformationType = new PersonInformationType();
				personInformationType.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
				personInformationType.setSSN(member.getSsn());
				personInformationType.setOtherCompletePersonName(getCompletePersonNameType(member));
				personInformationType.setUSAddressGrp(getUSAddressGrpFromIndividual(member));
			}
		}
		return personInformationType;
	}
	
	/**
	 * Returns the CompletePersonNameType
	 * @param member
	 * @return {@link CompletePersonNameType}
	 */
	private OtherCompletePersonNameType getCompletePersonNameType(
			EnrollmentMember1095 member) {
		OtherCompletePersonNameType completePersonName = new OtherCompletePersonNameType();
		completePersonName.setPersonFirstNm(cleanedNames(member.getFirstName()));
		completePersonName.setPersonLastNm(cleanedNames(member.getLastName()));
		completePersonName.setPersonMiddleNm(cleanedNames(member.getMiddleName()));
		completePersonName.setSuffixNm(removePeriodAndExtraSpaces(member.getNameSuffix()));
		return completePersonName;
	}
	
	/**
	 * Get Address from covered individual DTO
	 * @param member
	 * @param usAddressGrpType 
	 * @return USAddressGrpType updated address
	 */
	private USAddressGrpType getUSAddressGrpFromIndividual(EnrollmentMember1095 member) {

		USAddressGrpType updatedAddress =  new USAddressGrpType();
		updatedAddress.setAddressLine1Txt(cleanedAddress(member.getAddress1()));
		updatedAddress.setAddressLine2Txt(cleanedAddress(member.getAddress2()));
		updatedAddress.setCityNm(removePeriodAndExtraSpaces(member.getCity()));
		updatedAddress.setUSZIPCd(member.getZip());

		// HIX-66659 : Remove USZIPExtensionCd tag
		// updatedAddress.setUSZIPExtensionCd((individual.getUszipExtensionCd() !=null) ? individual.getUszipExtensionCd() : usAddressGrpType.getUSZIPExtensionCd());
		StateType usStateCd = null;
		if(null != member.getState()){
			usStateCd = StateType.fromValue(member.getState());
		}
		updatedAddress.setUSStateCd(usStateCd);
		return updatedAddress;
	}
	
	/**
	 * Get recipient spouse EOYPersonType from memberFromList
	 * @param memberFromList
	 * @return {@link EOYPersonType}
	 */
	private EOYPersonType getEOYPersonType(List<EnrollmentMember1095> memberFromList) {
		EOYPersonType eoyPersonType = null;
		if(null != memberFromList && !memberFromList.isEmpty()){
			EnrollmentMember1095 member = memberFromList.get(0);
			eoyPersonType = new EOYPersonType();
			eoyPersonType.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
			eoyPersonType.setSSN(member.getSsn());
			eoyPersonType.setOtherCompletePersonName(getCompletePersonNameType(member));
		}
		return eoyPersonType;
	}


	/**
	 * Get covered Individuals from DTO
	 * @param memberList
	 * @return {@link CoverageHouseholdGrpType}
	 */
	private CoverageHouseholdGrpType getCoveredHouseholdGroup(List<EnrollmentMember1095> memberList) {
		CoverageHouseholdGrpType coverageHouseholdGrpType = new CoverageHouseholdGrpType();
		for(EnrollmentMember1095 member : memberList){
			CoveredIndividualType coveredIndividualType = new CoveredIndividualType();
			coveredIndividualType.setInsuredPerson(getEPDPersonType(member));
			coveredIndividualType.setCoverageStartDt(getXmlGregorianCalendarDate(member.getCoverageStartDate()));
			coveredIndividualType.setCoverageEndDt(getXmlGregorianCalendarDate(member.getCoverageEndDate()));
			coverageHouseholdGrpType.getCoveredIndividual().add(coveredIndividualType);
		}
		return coverageHouseholdGrpType;
	}
	
	/**
	 * Get Insured Person Details for Insurance Policy
	 * @param individual
	 * @return  {@link EPDPersonType}
	 */
	private EPDPersonType getEPDPersonType(EnrollmentMember1095 member) {
		EPDPersonType insuredPerson = new EPDPersonType();
		insuredPerson.setBirthDt(getXmlGregorianCalendarDate(member.getBirthDate()));
		insuredPerson.setSSN(member.getSsn());
		insuredPerson.setOtherCompletePersonName(getCompletePersonNameType(member));
		return insuredPerson;
	}
	
	/**
	 * Get TaxHousholdPolicy Information
	 * @param coverageYear 
	 * @param enrollmentPremium
	 * @return {@link TaxHouseholdPolicyInformationDtlType }
	 */
	private TaxHouseholdPolicyInformationDtlType getRecipientPolicyInformation(List<EnrollmentPremium1095> enrollmentPremiums, Enrollment1095 enrollment1095) {
		TaxHouseholdPolicyInformationDtlType taxHouseholdPolicyInformationDtlType = new TaxHouseholdPolicyInformationDtlType();
		BigDecimal annualPremium = BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal annualSLCSP=BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		BigDecimal annualAPTC=BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_HALF_UP);
		Integer lastPremPaidMonth = null;
		if (enrollment1095.isFinancial() && enrollment1095.isTermByNonPayment() && enrollment1095.getPremiumPaidToDateEnd() != null) {
			lastPremPaidMonth = enrollment1095.getPremiumPaidToDateEnd().getMonth() + 1;
		}
		if(null != enrollmentPremiums && !enrollmentPremiums.isEmpty()){
			//Run the loop for entire year
			for(Integer monthNumber=1; monthNumber<=12; monthNumber++){
				//enrollmentPremiums.
				EnrollmentPremiumMonthPredicate enrollmentMonthPredicate = new EnrollmentPremiumMonthPredicate(monthNumber);
				BeanPredicate beanPredicate = new BeanPredicate("monthNumber", enrollmentMonthPredicate);
				 Collection<EnrollmentPremium1095> enrollmentPremium1095 = 
						 CollectionUtils.select(enrollmentPremiums, beanPredicate);
				 MonthlyPolicyInformationType monthlyPolicyInformationType = null;
				 if(enrollmentPremium1095 != null && !enrollmentPremium1095.isEmpty() && (lastPremPaidMonth == null || (lastPremPaidMonth != null  && monthNumber <= lastPremPaidMonth))){
					 for(EnrollmentPremium1095 premium1095 : enrollmentPremium1095){
					 monthlyPolicyInformationType = setMonthlyPremium(premium1095, enrollment1095.getCoverageYear());
					 }
				 }
				 else{
					 monthlyPolicyInformationType =  new MonthlyPolicyInformationType();
					 monthlyPolicyInformationType.setMonthlyAdvancedPTCAmt(ZERO_AMOUNT);
					 monthlyPolicyInformationType.setMonthlyPremiumAmt(ZERO_AMOUNT);
					 monthlyPolicyInformationType.setMonthlyPremiumSLCSPAmt(ZERO_AMOUNT);
				 }
				 if(monthlyPolicyInformationType.getMonthlyPremiumAmt() != null){
					 annualPremium = annualPremium.add(new BigDecimal(monthlyPolicyInformationType.getMonthlyPremiumAmt()));
				 }
				 if(monthlyPolicyInformationType.getMonthlyAdvancedPTCAmt() != null){
					 annualAPTC = annualAPTC.add(new BigDecimal(monthlyPolicyInformationType.getMonthlyAdvancedPTCAmt()));
				 }
				 if(monthlyPolicyInformationType.getMonthlyPremiumSLCSPAmt() != null){
					 annualSLCSP = annualSLCSP.add(new BigDecimal(monthlyPolicyInformationType.getMonthlyPremiumSLCSPAmt()));
				 }
				 setMonthlyPremium(taxHouseholdPolicyInformationDtlType, monthNumber, monthlyPolicyInformationType);
			}			
			if (lastPremPaidMonth != null && lastPremPaidMonth != 12) {
				annualAPTC = populateGracePeriodMonth(lastPremPaidMonth, taxHouseholdPolicyInformationDtlType, enrollmentPremiums, annualAPTC);
			}
			taxHouseholdPolicyInformationDtlType.setAnnualPolicyTotalAmounts(getAnnnualPolicyAmt(annualPremium, annualAPTC, annualSLCSP));
		}
		return taxHouseholdPolicyInformationDtlType;
	}
	
	private MonthlyPolicyInformationType setMonthlyPremium(EnrollmentPremium1095 enrollmentPremium1095, Integer coverageYear){
		MonthlyPolicyInformationType monthlyPolicyInformationType =  new MonthlyPolicyInformationType();
		if(!EnrollmentPremium1095.YorNFlagIndicator.N.toString().equalsIgnoreCase(enrollmentPremium1095.getIsActive())){
			//((grossPremium*ehbPercent)+(pediatricEhbAmt*accountableMemberDental))
			Float monthlyPremiumAmount = 0.0f;
			if(enrollmentPremium1095.getGrossPremium()!=null && enrollmentPremium1095.getEhbPercent()!=null){
				monthlyPremiumAmount=enrollmentPremium1095.getGrossPremium()*enrollmentPremium1095.getEhbPercent();
			}
			//HIX-85422 2017 EHB portion for SADPs is a percentage of total premium and not a dollar amount
			if(coverageYear >= 2017 && enrollmentPremium1095.getPediatricEhbAmt()!=null){
				monthlyPremiumAmount += enrollmentPremium1095.getPediatricEhbAmt();
			}else if(coverageYear < 2017 && enrollmentPremium1095.getPediatricEhbAmt()!=null && enrollmentPremium1095.getAccountableMemberDental()!=null){
				monthlyPremiumAmount+=(enrollmentPremium1095.getPediatricEhbAmt()*enrollmentPremium1095.getAccountableMemberDental());
			}
			//annualPremium+=monthlyPremiumAmount;
			monthlyPolicyInformationType.setMonthlyPremiumAmt(getBigDecimalStringFromFloat(monthlyPremiumAmount));
			monthlyPolicyInformationType.setMonthlyAdvancedPTCAmt(getBigDecimalStringFromFloat(0f));
			if(enrollmentPremium1095.getAptcAmount()!=null){
				monthlyPolicyInformationType.setMonthlyAdvancedPTCAmt(getBigDecimalStringFromFloat(enrollmentPremium1095.getAptcAmount()));
				//annualAPTC+=enrollmentPremium1095.getAptcAmount();
			}
			monthlyPolicyInformationType.setMonthlyPremiumSLCSPAmt(getBigDecimalStringFromFloat(0f));
			if(enrollmentPremium1095.getSlcspAmount()!=null){
				monthlyPolicyInformationType.setMonthlyPremiumSLCSPAmt(getBigDecimalStringFromFloat(enrollmentPremium1095.getSlcspAmount()));
				//annualSLCSP+=enrollmentPremium1095.getSlcspAmount();
			}
		}else{
			monthlyPolicyInformationType.setMonthlyAdvancedPTCAmt(ZERO_AMOUNT);
			monthlyPolicyInformationType.setMonthlyPremiumAmt(ZERO_AMOUNT);
			monthlyPolicyInformationType.setMonthlyPremiumSLCSPAmt(ZERO_AMOUNT);
		}
		return monthlyPolicyInformationType;
	}
	
	private TaxHouseholdPolicyInformationDtlType setMonthlyPremium(TaxHouseholdPolicyInformationDtlType taxHouseholdPolicyInformationDtlType, 
			Integer monthNumber, MonthlyPolicyInformationType monthlyPolicyInformationType){
		switch (monthNumber) {
		case 1 :
			taxHouseholdPolicyInformationDtlType.setJanPremiumInformation(monthlyPolicyInformationType);
			break;
		case 2 :
			taxHouseholdPolicyInformationDtlType.setFebPremiumInformation(monthlyPolicyInformationType);
			break;
		case 3 :
			taxHouseholdPolicyInformationDtlType.setMarPremiumInformation(monthlyPolicyInformationType);
			break;
		case 4 :
			taxHouseholdPolicyInformationDtlType.setAprPremiumInformation(monthlyPolicyInformationType);
			break;
		case 5 :
			taxHouseholdPolicyInformationDtlType.setMayPremiumInformation(monthlyPolicyInformationType);
			break;
		case 6 :
			taxHouseholdPolicyInformationDtlType.setJunPremiumInformation(monthlyPolicyInformationType);
			break;
		case 7 :
			taxHouseholdPolicyInformationDtlType.setJulPremiumInformation(monthlyPolicyInformationType);
			break;
		case 8 :
			taxHouseholdPolicyInformationDtlType.setAugPremiumInformation(monthlyPolicyInformationType);
			break;
		case 9 :
			taxHouseholdPolicyInformationDtlType.setSepPremiumInformation(monthlyPolicyInformationType);
			break;
		case 10 :
			taxHouseholdPolicyInformationDtlType.setOctPremiumInformation(monthlyPolicyInformationType);
			break;
		case 11 :
			taxHouseholdPolicyInformationDtlType.setNovPremiumInformation(monthlyPolicyInformationType);
			break;
		case 12 :
			taxHouseholdPolicyInformationDtlType.setDecPremiumInformation(monthlyPolicyInformationType);
			break;
		default :
			LOGGER.error("Invalid Month not in [1-12] range");
			break;
		}
		
		return taxHouseholdPolicyInformationDtlType;
	}

	/**
	 * Get Annual Amounts
	 * @param annualAmountMap
	 * @return {@link AnnualPolicyInformationType}
	 */
	private AnnualPolicyInformationType getAnnnualPolicyAmt(BigDecimal annualPremium, BigDecimal annualAPTC, BigDecimal annualSLCSP) {
		AnnualPolicyInformationType annualPolicyInformationType = new AnnualPolicyInformationType();
		annualPolicyInformationType.setAnnualAdvancedPTCAmt(annualAPTC.toPlainString());
		annualPolicyInformationType.setAnnualPremiumAmt(annualPremium.toPlainString());
		annualPolicyInformationType.setAnnualPremiumSLCSPAmt(annualSLCSP.toPlainString());
		return annualPolicyInformationType;
	}
	
	/**
	 * Generate XML
	 * @param annual1095TransmissionObj
	 * @param fileNumber
	 * @param wipFolder 
	 * @return String
	 * @throws GIException 
	 */
	private String generateIrsXml(Form1095ATransmissionUpstreamType annual1095TransmissionObj, int fileNumber, String wipFolder) throws GIException {
		ObjectFactory objectFactory = new ObjectFactory();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmssSSS'Z'");
		String fileName = null;
		
		InputStream strEmptyTagXSLPath = null;
		try {
			fileName = "EOY_Request_"+String.format("%05d", fileNumber+1)+"_"+dateFormat.format(new Date())+".xml";
			StringBuilder irsFileNameBuilder  = EnrollmentUtils
					.getReportingBasePathBuilderByTypeAndDirection(EnrollmentConstants.ReportType.ANNUAL.toString(),
							EnrollmentConstants.TRANSFER_DIRECTION_OUT)
					.append(File.separator).append(EnrollmentConstants.WIP_FOLDER_NAME)
					.append(File.separator).append(wipFolder);
			if (irsFileNameBuilder!=null && !("".equals(irsFileNameBuilder.toString().trim())) &&!(new File(irsFileNameBuilder.toString().trim()).exists())) {
				new File(irsFileNameBuilder.toString().trim()).mkdirs();
			}
			irsFileNameBuilder.append("/");
			irsFileNameBuilder.append(fileName);

			JAXBContext jaxbContext = JAXBContext.newInstance(Form1095ATransmissionUpstreamType.class);
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			
			String isNameSpacePrefixRequired = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.IRS_1095_XML_NAMESPACE_PREFIX_REQUIRED);
			/* Configured to set NameSpacePrefix mapper*/ 
			if(StringUtils.isNotEmpty(isNameSpacePrefixRequired) && isNameSpacePrefixRequired.equalsIgnoreCase(EnrollmentConstants.TRUE)){
				try{
					jaxbMarshaller.setProperty("com.sun.xml.bind.namespacePrefixMapper", new Enrollment1095NamespacePrefixMapper());
				}
				catch(Exception ex){
					LOGGER.error("Error occurred while setting namespace Prefix Mapper: ", ex);
				}
			}

			// output pretty printed
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller.setProperty(Marshaller.JAXB_ENCODING, "utf-8");
			jaxbMarshaller.setProperty("com.sun.xml.bind.marshaller.CharacterEscapeHandler", new EnrollmentIrsEscapeHandler());

			JAXBElement<Form1095ATransmissionUpstreamType> je =  objectFactory.createForm1095ATransmissionUpstream(annual1095TransmissionObj);
		
//			jaxbMarshaller.marshal(je, new File(irsFileNameBuilder.toString()));
			strEmptyTagXSLPath = EnrollmentServiceImpl.class.getClassLoader().getResourceAsStream("removeEmptyTags.xsl");
			StringWriter writer1095 = new StringWriter();
			StreamResult result1095 = new StreamResult(writer1095);
			TransformerFactory factory = new net.sf.saxon.TransformerFactoryImpl();
			jaxbMarshaller.marshal(je,result1095);
			
			String xmlString = new String(writer1095.toString());
			Source removeEmptyTagXslSource = new StreamSource(strEmptyTagXSLPath);
			StreamSource xmlJAXBSource = new StreamSource(new StringReader(xmlString));
			Templates template = factory.newTemplates(removeEmptyTagXslSource);
			Transformer transformer = template.newTransformer();
			transformer.transform(xmlJAXBSource, new StreamResult(new File(irsFileNameBuilder.toString())));

		} catch (JAXBException | TransformerException e) {
			LOGGER.error("Enrollment1095XmlBatchServiceImpl @ generateIrsXml  ", e);
			throw new GIException("Error generating XML @ generateIrsXml", e);
		} finally{
			IOUtils.closeQuietly(strEmptyTagXSLPath);
		}
		return fileName;
	}
	
	
	/**
	 * Replaces everything that is not a word character (a-z in any case, 0-9 or
	 * - , (, ), &, ') or whitespace.
	 * 
	 * @author negi_s
	 * @param str the input string
	 * @return String with special characters removed
	 */
	private String removeSplCharactersForPolicyNm(String str){
		String result = null;
		if(null != str){
			result = str.trim().replaceAll(" +", " ").replaceAll("[^A-Za-z0-9\\-\\(\\) ]", "").trim();
		}
		return result;
	}
	
	/**
	 * Replaces frivolous characters from the text
	 * 
	 * @author negi_s
	 * @param str the input string
	 * @return String with special characters removed
	 */
	private String removeSplCharactersFromAddress(String str){
		String result = null;
		if(null != str){
			result = str.trim().replaceAll("[^ A-Za-z0-9\\-/]", "").trim();
		}
		return result;
	}
	
	/**
	 * Gets us the cleaned address
	 * 
	 * @author negi_s
	 * @param str the input string
	 * @return String with special characters removed
	 */
	private String cleanedAddress(String str){
		String result = null;
		if(null != str){
			result = removePeriodAndExtraSpaces(removeSplCharactersFromAddress(str));
		}
		return result;
	}
	
	/**
	 * Gets us the cleaned names
	 * 
	 * @author negi_s
	 * @param str the input string
	 * @return String with special characters removed
	 */
	private String cleanedNames(String str){
		String result = null;
		if(null != str){
			result = removePeriodAndExtraSpaces(str);
		}
		return result;
	}
	
	private BigDecimal populateGracePeriodMonth(Integer lastPremPaidMonth, TaxHouseholdPolicyInformationDtlType taxHouseholdPolicyInformationDtlType, List<EnrollmentPremium1095> enrollmentPremium1095List, BigDecimal annualAPTC) {

		Float aptcAmt = null;
		for (EnrollmentPremium1095 enrollmentPremium1095 : enrollmentPremium1095List) {
			if (enrollmentPremium1095.getMonthNumber() != null && enrollmentPremium1095.getMonthNumber() == lastPremPaidMonth && enrollmentPremium1095.getAptcAmount() != null) {
				aptcAmt = enrollmentPremium1095.getAptcAmount();
			} else if (enrollmentPremium1095.getMonthNumber() != null && enrollmentPremium1095.getMonthNumber() == lastPremPaidMonth + 1 && enrollmentPremium1095.getAptcAmount() != null) {
				aptcAmt = enrollmentPremium1095.getAptcAmount();
				break;
			}
		}
		
		annualAPTC = annualAPTC.add(new BigDecimal(aptcAmt));

		MonthlyPolicyInformationType monthlyPolicyInformationType = new MonthlyPolicyInformationType();
		monthlyPolicyInformationType.setMonthlyPremiumAmt(ZERO_AMOUNT);
		monthlyPolicyInformationType.setMonthlyAdvancedPTCAmt(getBigDecimalStringFromFloat(aptcAmt));
		monthlyPolicyInformationType.setMonthlyPremiumSLCSPAmt(ZERO_AMOUNT);
		
		switch (lastPremPaidMonth + 1) {
		case 1:
			taxHouseholdPolicyInformationDtlType.setJanPremiumInformation(monthlyPolicyInformationType);
			break;
		case 2:
			taxHouseholdPolicyInformationDtlType.setFebPremiumInformation(monthlyPolicyInformationType);
			break;
		case 3:
			taxHouseholdPolicyInformationDtlType.setMarPremiumInformation(monthlyPolicyInformationType);
			break;
		case 4:
			taxHouseholdPolicyInformationDtlType.setAprPremiumInformation(monthlyPolicyInformationType);
			break;
		case 5:
			taxHouseholdPolicyInformationDtlType.setMayPremiumInformation(monthlyPolicyInformationType);
			break;
		case 6:
			taxHouseholdPolicyInformationDtlType.setJunPremiumInformation(monthlyPolicyInformationType);
			break;
		case 7:
			taxHouseholdPolicyInformationDtlType.setJulPremiumInformation(monthlyPolicyInformationType);
			break;
		case 8:
			taxHouseholdPolicyInformationDtlType.setAugPremiumInformation(monthlyPolicyInformationType);
			break;
		case 9:
			taxHouseholdPolicyInformationDtlType.setSepPremiumInformation(monthlyPolicyInformationType);
			break;
		case 10:
			taxHouseholdPolicyInformationDtlType.setOctPremiumInformation(monthlyPolicyInformationType);
			break;
		case 11:
			taxHouseholdPolicyInformationDtlType.setNovPremiumInformation(monthlyPolicyInformationType);
			break;
		case 12:
			taxHouseholdPolicyInformationDtlType.setDecPremiumInformation(monthlyPolicyInformationType);
			break;
		default:
			LOGGER.error("Invalid Month ");
			break;
		}
		return annualAPTC;
	}
	
}
