package com.getinsured.iex.hub.vci.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.StreetType;

/**
 * Wrapper class for
 * com.getinsured.iex.hub.vci.gov.niem.niem.niem_core._2.StreetType;
 * 
 * @author Nikhil Talreja
 * @since 29-Apr-2014
 * 
 */
public class StreetTypeWrapper implements JSONAware{

	private String streetFullText;
	private String streetExtensionText;
	
	private static final String STREET_FULL_TEXT_KEY = "StreetFullText";
	private static final String STREET_EXTENSION_KEY ="StreetExtensionText";
	
	
	public StreetTypeWrapper(StreetType streetType) {
		
		if(streetType == null){
			return;
		}
		
		this.streetFullText = streetType.getStreetFullText() != null ? streetType.getStreetFullText().getValue() : null;
		this.streetExtensionText = streetType.getStreetExtensionText() != null ? streetType.getStreetExtensionText().getValue() : null;
	}

	public String getStreetFullText() {
		return streetFullText;
	}

	public String getStreetExtensionText() {
		return streetExtensionText;
	}




	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(STREET_FULL_TEXT_KEY, this.streetFullText);
		obj.put(STREET_EXTENSION_KEY, this.streetExtensionText);
		return obj.toJSONString();
	}

}
