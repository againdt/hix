package com.getinsured.hix.entity.enrollmententity.service;

import java.util.List;

import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.PopulationIndustry;

/**
 * Encapsulates service layer method calls for industries served by EE
 * 
 */
public interface PopulationIndustryService {

	/**
	 * Retrieves population industry object based on id
	 * 
	 * @param industryId
	 *            industry id
	 * @return {@link PopulationIndustry}
	 */
	PopulationIndustry findById(int industryId);

	/**
	 * Retrieves list of {@link PopulationIndustry} by entity Id
	 * 
	 * @param entityId
	 *            EE Id
	 * @return {@link List} of {@link PopulationIndustry}
	 */
	List<PopulationIndustry> findByEntityId(int entityId);

	/**
	 * Removes industries associated with EE
	 * 
	 * @param entityId
	 * @return Number of records deleted
	 */
	int deleteAssociatedIndustries(int entityId);

	/**
	 * Saves industries associated with EE
	 * 
	 * @param enrollmentEntity
	 *            {@link EnrollmentEntity}
	 * @param populationIndustries
	 *            list of {@link PopulationIndustry}
	 */
	void saveAssociatedIndustries(EnrollmentEntity enrollmentEntity,
			List<PopulationIndustry> populationIndustries);
}
