package com.getinsured.hix.consumer.repository;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.iex.ssap.model.SsapApplication;

@Repository
@Transactional(readOnly = true)
public interface ISSAPApplicationRepository extends JpaRepository<SsapApplication, Integer>{

	
	@Query("select distinct(coverageYear) FROM SsapApplication where coverageYear is not null and coverageYear>0 order by coverageYear desc")
	public List<String>findAllEffectiveYears();
	
	@Query("FROM SsapApplication WHERE CMR_HOUSEOLD_ID = :householdId ORDER BY ID DESC")
	public List<SsapApplication> findLatestSsapApplicationByHouseholdId(@Param("householdId") Integer householdId);

}
