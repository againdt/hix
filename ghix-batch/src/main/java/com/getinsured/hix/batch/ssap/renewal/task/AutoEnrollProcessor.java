package com.getinsured.hix.batch.ssap.renewal.task;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.ssap.renewal.service.AutoEnrollService;
import com.getinsured.hix.batch.ssap.renewal.util.AutoEnrollPartitionerParams;
import com.getinsured.iex.ssap.model.RenewalApplication;

/**
 * Auto Enroll Processor class is used to return Renewal Application Model to Writer class.
 * 
 * @since July 19, 2019
 */
@Component("ssapAutoEnrollProcessor")
public class AutoEnrollProcessor implements ItemProcessor<RenewalApplication, RenewalApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(AutoEnrollProcessor.class);

	private AutoEnrollService autoEnrollService;
	private AutoEnrollPartitionerParams autoEnrollPartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		}

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			int partition = executionContext.getInt("partition");
			int startIndex = executionContext.getInt("startIndex");
			int endIndex = executionContext.getInt("endIndex");

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);
			}
		}
	}

	@Override
	public RenewalApplication process(RenewalApplication processRenewalApplication) throws Exception {

		if (LOGGER.isDebugEnabled()) {
			LOGGER.debug("Returning Renewal Application is null : {}", (null == processRenewalApplication));
		}
		return processRenewalApplication;
	}

	public AutoEnrollService getAutoEnrollService() {
		return autoEnrollService;
	}

	public void setAutoEnrollService(AutoEnrollService autoEnrollService) {
		this.autoEnrollService = autoEnrollService;
	}

	public AutoEnrollPartitionerParams getAutoEnrollPartitionerParams() {
		return autoEnrollPartitionerParams;
	}

	public void setAutoEnrollPartitionerParams(AutoEnrollPartitionerParams autoEnrollPartitionerParams) {
		this.autoEnrollPartitionerParams = autoEnrollPartitionerParams;
	}
}