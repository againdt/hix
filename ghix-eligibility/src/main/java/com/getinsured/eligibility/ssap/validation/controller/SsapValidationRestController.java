package com.getinsured.eligibility.ssap.validation.controller;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.ssap.validation.service.QleValidationService;
import com.getinsured.hix.model.ConsumerDocument;
import com.getinsured.hix.model.TkmTickets;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.iex.dto.QleRequestDto;
import com.getinsured.iex.dto.QleResponeDto;
import com.google.gson.Gson;

@Controller
@RequestMapping("/ssap")
public class SsapValidationRestController {

	private static final String UNEXPECTED_ERROR_OCCURED_WHILE_SAVING_DOCUMENT = "Unexpected error occured while saving document.";
	@Autowired
	QleValidationService qleValidationService;
	@Autowired private Gson platformGson;
	
	@RequestMapping(value="/retrieveEventsByApplication/{moduleId}/{caseNumber}/{gated}", produces=MediaType.APPLICATION_JSON_VALUE,method=RequestMethod.GET)
	@ResponseBody
	public QleResponeDto retrieveEventsByAppllication(@PathVariable("caseNumber") String caseNumber,@PathVariable("moduleId") long moduleId,@PathVariable("gated") String gated){
		QleResponeDto qleResponeDto = qleValidationService.retrieveEventDataByApplicationId(caseNumber,moduleId,gated);		
		return qleResponeDto;
	}
	
	@RequestMapping(value="/saveDocumentForApplicantEvent", produces=MediaType.APPLICATION_JSON_VALUE,method=RequestMethod.POST)
	@ResponseBody
	public Map<String, Object> saveDocumentForApplicantEvent(@RequestBody QleRequestDto docSaveRequest){
		List<Map<String,String>> returnResult = null;
		Map<String,Object> resultMap = new HashMap<String,Object>();
		try{
			qleValidationService.validateRequest(docSaveRequest);
			String ecmDocumentId = qleValidationService.saveValidationDocument(docSaveRequest.getRelativePath(), docSaveRequest.getFileName(), docSaveRequest.getFileContents(), docSaveRequest.getFileCategory(), docSaveRequest.getFileSubCategory(), docSaveRequest.getFileType());
			if(null == ecmDocumentId){
				resultMap.put("status", "500");
				resultMap.put("error", UNEXPECTED_ERROR_OCCURED_WHILE_SAVING_DOCUMENT);
				return resultMap;
			}
			Long consumerDocId = qleValidationService.saveDocumentRecord(ecmDocumentId, docSaveRequest.getFileCategory(), docSaveRequest.getFileName(), docSaveRequest.getFileType(), docSaveRequest.getApplicantEventId(), docSaveRequest.getTableName(), docSaveRequest.getUserId());
			if(consumerDocId == null){
				resultMap.put("status", "500");
				resultMap.put("error", UNEXPECTED_ERROR_OCCURED_WHILE_SAVING_DOCUMENT);
				return resultMap;
			}
			TkmTickets tkmTckt= qleValidationService.createTicket(docSaveRequest , ecmDocumentId, consumerDocId);
			if(null == tkmTckt){
				resultMap.put("status", "500");
				resultMap.put("error", UNEXPECTED_ERROR_OCCURED_WHILE_SAVING_DOCUMENT);
				return resultMap;
			}
			 returnResult = new ArrayList<Map<String,String>>();
			
			List<ConsumerDocument> cdL=qleValidationService.getUploadedDocByTargetNameAndTargetId("SSAP_APPLICANT_EVENTS", docSaveRequest.getApplicantEventId());
			Map<String,String> tmpMap;
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss",new Locale("EN"));
			for(ConsumerDocument cd:cdL){
				tmpMap= new HashMap<String, String>();
				
				tmpMap.put("documentType", cd.getDocumentType());
				tmpMap.put("documentStatus", "Y".equalsIgnoreCase(cd.getAccepted()) ?"ACCEPTED":"N".equalsIgnoreCase(cd.getAccepted())?"REJECTED":"SUBMITTED");
				tmpMap.put("documentName", cd.getDocumentName());
				tmpMap.put("documentDate",dateFormat.format(cd.getCreatedDate()));
				
				returnResult.add(tmpMap);
			}
			resultMap.put("documentList",platformGson.toJson(returnResult));
			resultMap.put("status", "200");
		}
		catch(AccessDeniedException ex) {
			resultMap.put("status", "500");
			resultMap.put("error", ex.getMessage());
		}
		catch(ContentManagementServiceException|IllegalArgumentException ex){
			resultMap.put("status", "500");
			resultMap.put("error", ex.getMessage());
		}catch(Exception ex){
			resultMap.put("status", "500");
			resultMap.put("error", UNEXPECTED_ERROR_OCCURED_WHILE_SAVING_DOCUMENT);
		}
		return  resultMap ;
	}
	
	
	@RequestMapping(value="/retrieveDocumentListForEvent/{sepEventId}", produces=MediaType.APPLICATION_JSON_VALUE,method=RequestMethod.GET)
	@ResponseBody
	public String retrieveDocumentListForEvent(@PathVariable("sepEventId") Integer sepEventId){
		String result = qleValidationService.retrieveDocumentListForEvent(sepEventId);		
		return result;
	}
	
	@RequestMapping(value="/adminQleOverride", produces=MediaType.TEXT_HTML_VALUE,method=RequestMethod.POST)
	@ResponseBody
	public String adminQleOverride(@RequestBody QleRequestDto qleRequestDto){
		String result = qleValidationService.adminQleOverride(qleRequestDto);		
		return result;
	}
	
	
}
