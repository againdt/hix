package com.getinsured.iex.hub.vlp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.CountryCodeArrayType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.CountryCodeListType;

/**
 * This class is the wrapper for com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.model.CountryCodeArrayType
 * 
 * HIX-34220
 * 
 * @author Nikhil Talreja
 * @since 09-Apr-2014
 *
 */
public class CountryCodeArrayTypeWrapper implements JSONAware{
	
	private List<CountryCodeListTypeWrapper> countryCodeList;
	
	private static final String COUNTRY_CODE_LIST_KEY = "CountryCodeList";
	
	public CountryCodeArrayTypeWrapper(CountryCodeArrayType countryCodeArrayType){
		
		if(countryCodeArrayType.getCountryCodeList().isEmpty()){
			return;
		}
		
		for(CountryCodeListType countryCodeListType : countryCodeArrayType.getCountryCodeList()){
			getCountryCodeList().add(new CountryCodeListTypeWrapper(countryCodeListType));
		}
		
	}
	
	public List<CountryCodeListTypeWrapper> getCountryCodeList() {
		
		if(this.countryCodeList == null){
			this.countryCodeList = new ArrayList<CountryCodeListTypeWrapper>();
		}
		
		return countryCodeList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		
		JSONArray countryCodeListArray = new JSONArray();
		for(CountryCodeListTypeWrapper countryCodeListTypeWrapper : this.countryCodeList){
			countryCodeListArray.add(countryCodeListTypeWrapper);
		}
		
		obj.put(COUNTRY_CODE_LIST_KEY, countryCodeListArray);
		
		return obj.toJSONString();
	}
}
