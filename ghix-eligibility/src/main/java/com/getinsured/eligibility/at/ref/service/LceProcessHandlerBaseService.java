package com.getinsured.eligibility.at.ref.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.functors.NullPredicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;

import com.getinsured.eligibility.at.disenrollment.service.ReferralDisenrollmentService;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.dto.APTCUpdateRequest;
import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.handler.SsapEnrolleeHandler;
import com.getinsured.eligibility.at.resp.service.AptcHistoryService;
import com.getinsured.eligibility.at.resp.service.SsapApplicationEventService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.ApplicationValidationStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.ind71.client.Ind71ClientAdapter;
import com.getinsured.eligibility.ind71G.client.Ind71GClientAdapter;
import com.getinsured.eligibility.model.SepEvents.Source;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentRequest;
import com.getinsured.eligibility.move.enrollment.service.MoveEnrollmentService;
import com.getinsured.eligibility.plan.service.EnrollmentUpdateAdapter;
import com.getinsured.eligibility.qlevalidation.QLEValidationService;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest;
import com.getinsured.hix.dto.enrollment.EnrollmentRequest.EnrollmentStatus;
import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.hix.indportal.dto.AppGroupRequest;
import com.getinsured.hix.indportal.dto.AppGroupResponse;
import com.getinsured.hix.indportal.dto.AptcUpdate;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.repository.IUserRepository;
import com.getinsured.hix.platform.security.service.GhixRole;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.EnrollmentEndPoints;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.household.service.RelationshipTypeService;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.google.gson.Gson;

/**
 * @author chopra_s
 *
 */
public abstract class LceProcessHandlerBaseService {
	private static final Logger LOGGER = Logger.getLogger(LceProcessHandlerBaseService.class);
	private static final String APP_EVENT_TYPE = "APPEVENT_APPLICATION";
	private static final String APP_EVENT_CS_CHANGE = "CS_LEVEL_CHANGED";
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;

	@Autowired
	@Qualifier("moveEnrollmentService")
	private MoveEnrollmentService moveEnrollmentService;

	@Autowired
	@Qualifier("referralDisenrollmentService")
	private ReferralDisenrollmentService referralDisenrollmentService;

	@Autowired
	@Qualifier("referralLceNotificationService")
	private ReferralLceNotificationService referralLceNotificationService;

	@Autowired
	@Qualifier("aptcUpdateService")
	private APTCUpdateService aptcUpdateService;
	
	@Autowired
	@Qualifier("ind71ClientAdapter")
	private Ind71ClientAdapter ind71ClientAdapter;
	
	@Autowired
	@Qualifier("ind71GClientAdapter")
	private Ind71GClientAdapter ind71GClientAdapter;
	
	@Autowired
	@Qualifier("ssapApplicationEventService")
	private SsapApplicationEventService ssapApplicationEventService;
	
	@Autowired private IUserRepository iUserRepository;
	@Autowired private QLEValidationService qleValidationService;
	
	@Autowired
	@Qualifier("enrollmentUpdateAdapter")
	private EnrollmentUpdateAdapter enrollmentUpdateAdapter;
	
	@Autowired
	private AptcHistoryService aptcHistoryService;
	
	@Autowired
	public GhixRestTemplate ghixRestTemplate;
	
	@Autowired private Gson platformGson;
	
	@Autowired
	private LookupService lookupService;
	
	@Autowired
	private AppEventService appEventService;
	
	@Autowired
	@Qualifier("ssapEnrolleeHandler")
	private SsapEnrolleeHandler ssapEnrolleeHandler;
	
	@Autowired
	private RelationshipTypeService relationshipTypeService;
	
	private static String ERROR_LCE_NOTIFICATION = "An error occurred while sending the LCE notification";

	protected static final String N = "N";

	protected static final String Y = "Y";

	
	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

	public SsapApplication loadApplication(Long applicationId) {
		return ssapApplicationRepository.findOne(applicationId);
	}

	public String moveEnrollmentToNewApplication(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Move Enrollment for Enrolled Application - " + enrolledApplication.getId() + " to Application id - " + currentApplication.getId());
		MoveEnrollmentRequest request = new MoveEnrollmentRequest();
		request.setEnrolledSsapApplicationId(enrolledApplication.getId());
		request.setNewSsapApplicationId(currentApplication.getId());
		request.setUserName(ReferralConstants.EXADMIN_USERNAME);
		return moveEnrollmentService.process(request);
	}

	public void closeSsapApplication(SsapApplication ssapApplication) {
		LOGGER.info("Close Application id " + ssapApplication.getId());
		ssapApplication.setApplicationDentalStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
		setApplicationStatus(ssapApplication, ApplicationStatus.CLOSED.getApplicationStatusCode());
	}

	public void updateCurrentAppToEN(SsapApplication currentApplication) {
		setApplicationStatus(currentApplication, ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
	}

	public void updateAllowEnrollment(SsapApplication currentApplication, String allowEnrollment) {
		currentApplication.setAllowEnrollment(allowEnrollment);
		ssapApplicationRepository.save(currentApplication);
	}

	public void updateCurrentAppToER(SsapApplication currentApplication) {
		setApplicationStatus(currentApplication, ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
	}

	public void copyEHBAmount(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		LOGGER.info("Copy EHB amount from application - " + enrolledApplication.getId() + " to - " + currentApplication.getId());
		currentApplication.setEhbAmount(enrolledApplication.getEhbAmount());
		ssapApplicationRepository.save(currentApplication);
	}

	private void setApplicationStatus(SsapApplication ssapApplication, String applicationStatus) {
		LOGGER.debug("setApplicationStatus - " + applicationStatus);
		ssapApplication.setApplicationStatus(applicationStatus);
		ssapApplicationRepository.save(ssapApplication);
	}

	public void updateSsapApplication(SsapApplication ssapApplication) {
		ssapApplicationRepository.save(ssapApplication);
	}

	public String executeDisenrollment(SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap) {
		return referralDisenrollmentService.invokeDisenrollmentAPI(applicantEventMap, enrolledApplication);
	}

	public String executeAptcChangesApi(SsapApplication currentApplication, long enrolledApplicationId, AptcUpdate aptcUpdate) {
		APTCUpdateRequest aptcUpdateRequest = new APTCUpdateRequest();
		aptcUpdateRequest.setEnrolledApplicationId(enrolledApplicationId);
		aptcUpdateRequest.setCurrentApplicationId(currentApplication.getId());
		aptcUpdateRequest.setMaximumAPTC(currentApplication.getMaximumAPTC());
		return aptcUpdateService.process(aptcUpdateRequest,aptcUpdate);
	}

	public AptcUpdate populateAptcUpdateRequest(SsapApplication currentApplication, long enrolledApplicationId, Date finEffectiveDate) {
		AptcUpdate aptcUpdate = new AptcUpdate();
		String appGroupResponseJson = null;
		AppGroupResponse appGroupResponse = null;
		try {
			AppGroupRequest appGroupRequest = new AppGroupRequest();
        	appGroupRequest.setApplicationId(currentApplication.getId());
        	
        	if(finEffectiveDate == null) {
        		throw new GIRuntimeException("APTC redistribution financial Effective date can not be null : "+ currentApplication.getId());
			}
        	
        	getUpdatedDateBasedonEnrollment(enrolledApplicationId, finEffectiveDate, aptcUpdate);
        	if(aptcUpdate.getAptcEffectiveDate()!=null) {
        		finEffectiveDate = aptcUpdate.getAptcEffectiveDate();
        	}
        	String coverageStartDate = new SimpleDateFormat("MM/dd/yyyy").format(finEffectiveDate);
        	
        	appGroupRequest.setCoverageStartDate(coverageStartDate);
        	
        	try{
        		/*
        		 * this had to be changed as when called from web the logged in user is lost
        		 * this is done same as when called from LCE auto flow we pass exadmin user   
        		 */
        		appGroupResponseJson = ghixRestTemplate.exchange(GhixEndPoints.ELIGIBILITY_URL+ "eligibility/api/getAppGroups/", ReferralConstants.EXADMIN_USERNAME, 
    					HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, appGroupRequest).getBody();
    			aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
    		}catch(Exception e){
    			LOGGER.error("Exception occured for calling getAppGroups api",e);
    			throw new GIRuntimeException("Error occoured while getting aptc redistribution: " + appGroupResponseJson);
    		}
        	try{
        		appGroupResponse = platformGson.fromJson(appGroupResponseJson, AppGroupResponse.class);
        		aptcUpdate.setAppGroupResponse(appGroupResponse);
    		}catch(Exception e){
    			throw new GIRuntimeException("Invalid response in APTC redistribution for application id : " + currentApplication.getId());
    		}
    	} catch (Exception e) {
			/** Eat the Exception */
		}
		return aptcUpdate;
	}
	
	private void getUpdatedDateBasedonEnrollment(long enrolledApplicationId, Date finEffectiveDate, AptcUpdate aptcUpdate) {
		final Map<String, List<EnrollmentShopDTO>> enrollmentDetails = ssapEnrolleeHandler.fetchMultipleEnrollmentID(enrolledApplicationId);
		
		List<EnrollmentShopDTO> healthList = enrollmentDetails.get(ReferralConstants.HEALTH_ENROLLMENT_DTO);
		if(enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO)!=null) {
			healthList.addAll(enrollmentDetails.get(ReferralConstants.DENTAL_ENROLLMENT_DTO));
		}
		
		SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
		try {
			if(healthList!=null) {
				for(EnrollmentShopDTO health : healthList) {
					String healthEnrollmentStartDate = df.format(health.getCoverageStartDate());
					if(StringUtils.isNotEmpty(healthEnrollmentStartDate) && isAptcStartDateBeforeEnrollmentStartDate(finEffectiveDate, healthEnrollmentStartDate)){
						finEffectiveDate = df.parse(healthEnrollmentStartDate);
					}
				}
			}
			aptcUpdate.setAptcEffectiveDate(finEffectiveDate);
			aptcUpdate.setEnrollmentList(healthList);
		}catch (Exception e) {
			LOGGER.error("Exception occured while parsing new financial effective date and enrollment start date",e);
			throw new GIRuntimeException("Exception occured while parsing new financial effective date and enrollment start date: " + enrolledApplicationId );
		}
	}
	
	public boolean isAptcStartDateBeforeEnrollmentStartDate(Date aptcEffectiveDate, String enrollmentEffectiveStartDate) throws ParseException{
		boolean isAptcStartDateBeforeEnrollmentStartDate = false;
		
		SimpleDateFormat mmddyyyy = new SimpleDateFormat("MM/dd/yyyy");
		LocalDate aptcStartDate = new DateTime(aptcEffectiveDate.getTime()).toLocalDate();
		LocalDate enrollmentStartDate = new DateTime(mmddyyyy.parseObject(enrollmentEffectiveStartDate)).toLocalDate();
		if(aptcStartDate.isBefore(enrollmentStartDate)){
			isAptcStartDateBeforeEnrollmentStartDate = true;
		}
		return isAptcStartDateBeforeEnrollmentStartDate;
	}
	
	public void handleDisenrollmentResponse(SsapApplication currentApplication, SsapApplication enrolledApplication, String response) {
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(response)) {
			closeSsapApplication(enrolledApplication);
			closeSsapApplication(currentApplication);
			triggerDisenrollEmail(currentApplication, enrolledApplication);
		} else {
			updateAllowEnrollment(currentApplication, N);
			triggerDisenrollEmail(currentApplication);
		}
	}

	private void triggerDisenrollEmail(SsapApplication currentApplication, SsapApplication enrolledApplication) {
		try {
			LOGGER.info("Trigger Email - Notice EE019");
			referralLceNotificationService.generateHHLostEligibilityNoticewithNonQhpIndicators(currentApplication.getCaseNumber(), enrolledApplication.getId());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE019").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	private void triggerDisenrollEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger Disenroll Email - Notice LCE3");
		try {
			referralLceNotificationService.generateEligibilityLossNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE3").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerSEPEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger SEP Email - Notice LCE EE020");
		try {
			referralLceNotificationService.generateSEPEventNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE EE020").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
	
	private static String ERROR_SEP_KEEP_ONLY_NOTIFICATION = "An error occurred while sending the SEP Keep Only notification - LCE4 Keep Only Notice";
	
	public void triggerSepEventKeepOnlyNotice(SsapApplication currentApplication) {
		try {
			referralLceNotificationService.generateSEPEventKeepOnlyNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_SEP_KEEP_ONLY_NOTIFICATION)
					.append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}

	}

	public void triggerDenialSEPEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger Denial SEP Email - Notice LCE EE021");
		try {
			referralLceNotificationService.generateSEPDenialNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE EE021").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerChangeActionEmail(SsapApplication currentApplication, LCEProcessRequestDTO lceProcessRequestDTO, boolean triggerEE20) {
		LOGGER.info("Trigger Change Action Email - Notice LCE EE057");
		try {
			referralLceNotificationService.generateChangeActionNotice(currentApplication.getCaseNumber(), lceProcessRequestDTO, triggerEE20);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice LCE EE057").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerAPTCSuccessEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger APTC Change Success Notice");
		try {
			referralLceNotificationService.generateAPTCChangeNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE053").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());

		}
	}

	public void triggerNonFinConversionNoticeWithAptcCsrIneligibile(String caseNumber) {
		LOGGER.info("Trigger Email - Notice EE058");
		try {
			referralLceNotificationService.generateNonFinConversionNoticeWithAptcCsrIneligibile(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE058").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerNonFinConversionNoticeWithAptcIneligibile(String caseNumber) {
		LOGGER.info("Trigger Email - Notice EE056");
		try {
			referralLceNotificationService.generateNonFinConversionNoticeWithAptcOnlyIneligibile(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE056").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
	
	/**
	 * this notice will be triggered when there is a loss/gain of native american status i.e AI / AN change along with CSR change for entire HH or IND
	 * @param currentApplication
	 * @param enrolledApplication
	 */
	public void triggerNativeAmericanStatusNotice(SsapApplication currentApplication) {
		// this notice will be triggred if CS change has happended for IND / HH and AI/AN has changed
			LOGGER.info("Trigger Loss of Native American Status Email - Notice EE066 for application: "+currentApplication.getId());
			try {
				referralLceNotificationService.generateAIANLCENotice(currentApplication.getCaseNumber());
			} catch (Exception e) {
				StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE066 ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
				LOGGER.error(errorReason.toString());

			}		
	}
	
	/**
	 * this notice will be triggered when there is a loss/gain of native american status i.e AI / AN change along with CSR change for entire HH or IND
	 * @param currentApplication
	 * @param enrolledApplication
	 */
	public void triggerLossofCSRNotice(SsapApplication currentApplication) {
			// 	this notice will be triggered if CS change has happended for IND
			LOGGER.info("Trigger Loss of CSR for individual Email - Notice EE065 for application: "+currentApplication.getId());
			try {
				referralLceNotificationService.generateLossOfCSRIndNotice(currentApplication.getCaseNumber());
			} catch (Exception e) {
				StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE065 ").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
				LOGGER.error(errorReason.toString());

			}		
	}

	
	public void triggerAutomationCSNotice(String caseNumber) {
		LOGGER.info("Trigger Automation CS Notice Email - Notice EE060");
		try {
			referralLceNotificationService.generateAutomationNoticeEE060(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE060").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerAutomationAddRemoveNotice(String caseNumber) {
		LOGGER.info("Trigger Add/Remove Email - Notice EE052");
		try {
			referralLceNotificationService.generateAddRemoveAutomationNotice(caseNumber);
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append(ERROR_LCE_NOTIFICATION + "- Notice EE052").append(ReferralProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerNFtoFinancialConversionManualEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger NF to Financial Manual Email for application - " + currentApplication.getId());
		try {
			referralLceNotificationService.generateNonFinToFinConversionNoticeManual(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("An error occurred while sending the NF to Financial Manual Email notification" + "- Notice EE033").append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public void triggerNFtoFinancialConversionEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger NF to Financial Conversion Email for application - Notice EE032 - " + currentApplication.getId());
		try {
			referralLceNotificationService.generateNonFinToFinConversionNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("An error occurred while sending the NF to Financial Conversion Email notification" + "- Notice EE032").append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}

	public List<String> fetchApplicantGuidsForApplicantsOnCurrentApplication(List<SsapApplicant> currentSsapApplicants) {

		List<String> applicantGuidList = new ArrayList<String>();
		for (SsapApplicant applicant : currentSsapApplicants) {
			if (Y.equals(applicant.getOnApplication())) {
				applicantGuidList.add(applicant.getApplicantGuid());
			}
		}
		return applicantGuidList;
	}

	public void updateSepQepDeniedFlag(SsapApplication currentApplication, String sepQepDenied) {
		currentApplication.setSepQepDenied(sepQepDenied);
		ssapApplicationRepository.save(currentApplication);
	}

	public void changeApplicationType(SsapApplication currentApplication, String applicationType) {
		currentApplication.setApplicationType(applicationType);
		ssapApplicationRepository.save(currentApplication);
	}

	public void updateSsapApplicationEvent(SsapApplicationEvent applicationEvent) {
		ssapApplicationEventRepository.save(applicationEvent);
	}

	public String executeDisenrollment(SsapApplication enrolledApplication, Date terminationDate) {
		return referralDisenrollmentService.invokeDisenrollmentAPI(enrolledApplication, terminationDate);
	}
	
	public boolean executeIND71(long ssapApplicationId, List<String> healthEnrollees, List<String> dentalEnrollees) {
		String ind71Response = GhixConstants.RESPONSE_FAILURE;
		try {
			/* Call IND71 client */
			ind71Response = ind71ClientAdapter.execute(ssapApplicationId, healthEnrollees, dentalEnrollees);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		
		boolean status = false;
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(ind71Response)) {
			status = true;
		}
		return status;
	}
	
	private boolean executeIND71G(long ssapApplicationId, AptcUpdate aptcUpdate, EnrollmentResponse enrollmentResponse,String isChangePlan) {
		String ind71GResponse = GhixConstants.RESPONSE_FAILURE;
		try {
			/* Call IND71G client */
			ind71GResponse = ind71GClientAdapter.execute(ssapApplicationId, aptcUpdate, enrollmentResponse,isChangePlan);
		} catch (Exception e) {
			/* eat and send failure response to avoid event rollback */
		}
		
		boolean status = false;
		if (GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(ind71GResponse)) {
			status = true;
		}
		return status;
	}
	
	public boolean processCSSuccess(Long currentApplicationId, Long enrolledApplicationId, String previousApplicationStatus, String previousApplicationDentalStatus) {
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		final SsapApplication currentApplication = loadApplication(currentApplicationId);
		closeSsapApplication(enrolledApplication);
		String enrollmentType = getEnrollmentType(currentApplicationId);
		if(StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.DENTAL)){ 
			if(currentApplication.getApplicationDentalStatus() == null) {
				currentApplication.setApplicationDentalStatus(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
			}
		}
		else{
			if(StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.HEALTH) || StringUtils.equalsIgnoreCase(enrollmentType, ReferralConstants.BOTH)){
				if(currentApplication.getApplicationStatus() == null) {
					currentApplication.setApplicationStatus(ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode());
				}	
			}
		}
		
		updateSsapApplication(currentApplication);
		logCSChangeEvent(currentApplication, enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		openSepForAutomation(currentApplicationId);
		return true;
	}
	
	public boolean processCSSuccess(Long currentApplicationId, Long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadApplication(enrolledApplicationId);
		final SsapApplication currentApplication = loadApplication(currentApplicationId);
		closeSsapApplication(enrolledApplication);
		updateCurrentAppToEN(currentApplication);
		logCSChangeEvent(currentApplication, enrolledApplication);
		updateAllowEnrollment(currentApplication, Y);
		openSepForAutomation(currentApplicationId);
		return true;
	}
	
	private void openSepForAutomation(Long currentApplicationId) {
		setChangePlan(currentApplicationId);
		
	}
	
	private void setChangePlan(Long applicationId) {
		ssapApplicationEventService.setChangePlan(applicationId);
	}
	
	public Optional<ApplicationValidationStatus> getValidationStatus(String caseNumber) {
		LOGGER.info("Get validation status of case number " + caseNumber);
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		return qleValidationService.runValidationEngine(caseNumber,  Source.EXTERNAL, updateUser.getId());
	}
	
	public void ignoreGatedNonPrimaryEvents(String caseNumber) {
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.ignoreNonPrimaryEvents(caseNumber, updateUser.getId());
	}
	
	public boolean isApplicationValidated(Optional<ApplicationValidationStatus> validationStatus) {
		boolean isApplicationValidated = false;
		if (validationStatus.isPresent() && (validationStatus.get()==ApplicationValidationStatus.VERIFIED || validationStatus.get()==ApplicationValidationStatus.NOTREQUIRED)){
			isApplicationValidated = true;
		}
		return isApplicationValidated;
	}
	
	void processDenial(String caseNumber) {
		AccountUser updateUser = iUserRepository.getUserBasicInfo(ReferralConstants.EXADMIN_USERNAME);
		qleValidationService.denySep(caseNumber,updateUser.getId());
	}
	
	public void triggerDocumentRequiredEmail(SsapApplication currentApplication) {
		LOGGER.info("Trigger Document Required Email for application - " + currentApplication.getId());
		try {
			referralLceNotificationService.generateLceDocumentRequiredNotice(currentApplication.getCaseNumber());
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder().append("An error occurred while sending the Document Required Email Email notification" + "- Notice EE061").append(ReferralProcessingConstants.REASON)
			        .append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
	}
	
	public Date updateEffectiveDate(SsapApplication currentApplication,SsapApplicationEvent ssapApplicationEvent) {
		Date coverageStartDate = enrollmentUpdateAdapter.getCoverageStartDate(currentApplication, ssapApplicationEvent);
		aptcHistoryService.updateEffectiveDate(coverageStartDate, new Long(currentApplication.getId()));
		return coverageStartDate;
	}
	
	private void logCSChangeEvent(SsapApplication currentApplication, SsapApplication enrolledApplication){
		EventInfoDto eventInfoDto = new EventInfoDto();
		Map<String, String> mapEventParam = new HashMap<String, String>();
		
		//event type and category
		LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode(APP_EVENT_TYPE,APP_EVENT_CS_CHANGE);
		eventInfoDto.setEventLookupValue(lookupValue);
		mapEventParam.put("Current CS Level", currentApplication.getCsrLevel());
		mapEventParam.put("Previous CS Level", enrolledApplication.getCsrLevel());
		eventInfoDto.setModuleId(currentApplication.getCmrHouseoldId().intValue());
		eventInfoDto.setModuleName(GhixRole.INDIVIDUAL.toString());
		appEventService.record(eventInfoDto, mapEventParam);
	}
	
	public String getEnrollmentsByMember(List<SsapApplicant> ssapApplicants, String coverageYear) {
		List<String> applicantGuidList = new ArrayList<String>();
		for(SsapApplicant ssapApplicant : ssapApplicants) {
			applicantGuidList.add(ssapApplicant.getApplicantGuid());
		}
		
		List<EnrollmentStatus> enrollmentStatusList = new ArrayList<EnrollmentStatus>();
		enrollmentStatusList.add(EnrollmentStatus.PENDING);
		enrollmentStatusList.add(EnrollmentStatus.CONFIRM);
		
		EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
		enrollmentRequest.setMemberIdList(applicantGuidList);
		enrollmentRequest.setEnrollmentStatusList(enrollmentStatusList);
		
		List<String> coverageYearList = new ArrayList<String>();
		coverageYearList.add(coverageYear);
		enrollmentRequest.setCoverageYearList(coverageYearList);
		String enrollmentResponseStr = ghixRestTemplate.exchange(EnrollmentEndPoints.GET_ENROLLMENT_DATA_BY_MEMBERID_LIST, GhixConstants.USER_NAME_EXCHANGE, HttpMethod.POST, MediaType.APPLICATION_JSON, String.class, platformGson.toJson(enrollmentRequest)).getBody();
	
		return enrollmentResponseStr;
	}
	
	public boolean ind71GCall(long enrolledAppId, long currentAppId,long coverageYear, AptcUpdate aptcUpdate,String changePlan) {
		
        boolean status = false;	
		SsapApplication enrolledApplication = loadCurrentApplication(enrolledAppId);////HIX-108047
		String enrollmentResponseStr = getEnrollmentsByMember(enrolledApplication.getSsapApplicants(),String.valueOf(coverageYear));
		if(StringUtils.isNotBlank(enrollmentResponseStr)) {
			EnrollmentResponse enrollmentResponse = platformGson.fromJson(enrollmentResponseStr, EnrollmentResponse.class);
			if(enrollmentResponse != null) {
				status = executeIND71G(currentAppId,aptcUpdate,enrollmentResponse,changePlan);
			}else {
				LOGGER.info("Member enrollment mapping call failure - " + currentAppId);
			}
		}else {
			LOGGER.info("Member enrollment mapping call failure - " + currentAppId);
		}
		return status;
	}
	
	public Map<String,Integer> fetchEnrollmentCount(Long enrolledApplicationId){
		Map<String,Integer> enrollmentCountMap = new HashMap<String,Integer>(5);
		EnrollmentResponse enrollmentResponse = null;
		String jsonResponse = "";
		try{
			EnrollmentRequest enrollmentRequest = new EnrollmentRequest();
			enrollmentRequest.setSsapApplicationId(enrolledApplicationId);	
			jsonResponse = (ghixRestTemplate.exchange(GhixEndPoints.EnrollmentEndPoints.GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID, 
					"",HttpMethod.POST, 
					MediaType.APPLICATION_JSON, 
					String.class, platformGson.toJson(enrollmentRequest))).getBody();
			if(LOGGER.isInfoEnabled()){
				LOGGER.info("*****************************************Received response from enrollemnt GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID" + jsonResponse);
			}
			enrollmentResponse = platformGson.fromJson(jsonResponse, EnrollmentResponse.class);
			if(enrollmentResponse != null){
				if(StringUtils.equalsIgnoreCase(enrollmentResponse.getStatus(),GhixConstants.RESPONSE_SUCCESS)){
					if(enrollmentResponse.getEnrollmentCountMap() != null){
						enrollmentCountMap = enrollmentResponse.getEnrollmentCountMap();
					}
				}
				else{
					LOGGER.error("Received Error Response from enrollemnt GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID, Error Code:" + enrollmentResponse.getErrCode()+", Error Message:"+enrollmentResponse.getErrMsg());
				}
			}
		}catch(Exception e){
			LOGGER.error("Exception occured for calling GET_COUNT_OF_HEALTH_AND_DENTAL_BY_APPID api"+ ExceptionUtils.getFullStackTrace(e));
			throw new GIRuntimeException("Error occoured while getting health and dental enrollment count " + jsonResponse);
		}
		return enrollmentCountMap;
	}
	
	public boolean isActiveEnrollment(long applicationId){
		boolean flag = false;
		
			Map<String,Integer> enrollmentCount = fetchEnrollmentCount(applicationId);
			if(enrollmentCount != null && enrollmentCount.size()>0){
				int healthCount = enrollmentCount.get(ReferralConstants.HEALTH) != null ? enrollmentCount.get(ReferralConstants.HEALTH) : 0;
				int dentalCount = enrollmentCount.get(ReferralConstants.DENTAL) != null ? enrollmentCount.get(ReferralConstants.DENTAL) : 0;
				if(healthCount > 0 ||  dentalCount > 0){
					flag = true;
				}
			}	
		return flag;
	}
	
	public boolean isAddCase(SsapApplication currentApplication){
		boolean isAdd = false;
		for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
			if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
				isAdd = true;
				break;
			}
		}
		return isAdd;
	}
	
	/**
	 * This method is to check whether there is an AI/AN change for the IND ssap applicant or entire HH 
	 * @param currentApplication
	 * @param enrolledApplication
	 * @return
	 */
	public boolean checkAIANChange(SsapApplication currentApplication, long enrolledApplicationId) {
		// this notice will be triggered if CS change has happened for IND / HH and
		// AI/AN has changed
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		List<SsapApplicant> ssapApplicantsCurrent = currentApplication.getSsapApplicants();
		List<SsapApplicant> ssapApplicantsEnrolled = enrolledApplication.getSsapApplicants();
		String nativeAmericanFlag = null;

		// make a map of native American flags for enrolled applicants
		Map<String, String> enrolledAIANFlagMap = new HashMap<String, String>();

		for (SsapApplicant enrolledApplicant : ssapApplicantsEnrolled) {
			enrolledAIANFlagMap.put(enrolledApplicant.getApplicantGuid(), enrolledApplicant.getNativeAmericanFlag());
		}
		for (SsapApplicant currentApplicant : ssapApplicantsCurrent) {
			nativeAmericanFlag = enrolledAIANFlagMap.get(currentApplicant.getApplicantGuid());
			if (nativeAmericanFlag != null && !nativeAmericanFlag.equals(currentApplicant.getNativeAmericanFlag())) {
				return true;
			}
		}

		return false;
	}
	

	/**
	 * This will check the loss of CSR eligibility for a Individual
	 * @param currentApplication
	 * @param enrolledApplicationId
	 * @return
	 */
	public boolean checkLossofCSR_IND(SsapApplication currentApplication, long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		List<SsapApplicant> ssapApplicantsCurrent = currentApplication.getSsapApplicants();
		List<SsapApplicant> ssapApplicantsEnrolled = enrolledApplication.getSsapApplicants();
		// make a map of CSR Level of current and enrolled applications with SSN as a Key
		Map<String,String> currentCSREligibilityMap = new HashMap<>();
		Map<String,String> enrolledCSREligibilityMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(ssapApplicantsCurrent)) {
			for (SsapApplicant ssapApplicant : ssapApplicantsCurrent) {
				currentCSREligibilityMap.put(ssapApplicant.getSsn(),ssapApplicant.getCsrLevel());
			}
		}
		if (CollectionUtils.isNotEmpty(ssapApplicantsEnrolled)) {
			for (SsapApplicant ssapApplicant : ssapApplicantsEnrolled) {
				enrolledCSREligibilityMap.put(ssapApplicant.getSsn(),ssapApplicant.getCsrLevel());
			}
		}
		// suppose for that applicant CS level was CS4 (enrolled) and now its null in current AT then its considered as LOSS
		// check added for entire HH loss with all values null in the currentCSREligibilityMap 
		int size = currentCSREligibilityMap.size();
		if(CollectionUtils.countMatches(currentCSREligibilityMap.values(), NullPredicate.INSTANCE) == size) {
			// as if all csr are null that means its entire HH elg lost
			return false;
		} else {
			boolean lossCSRIND = false;
			for (String currentSSNkey : currentCSREligibilityMap.keySet()) {
				if(enrolledCSREligibilityMap.get(currentSSNkey) != null && currentCSREligibilityMap.get(currentSSNkey) == null) {
					// this is loss of CSR for IND
					lossCSRIND =  true;
				} 
			}
			
			return lossCSRIND;
		}		
	}
	
	
	/**
	 * this will loss of APTC / CSR loss for the entire HH
	 * @param currentApplication
	 * @param enrolledApplicationId
	 * @return
	 */
	public boolean checkLossOfAptcCsrHH(SsapApplication currentApplication, long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		// check for the APTC/ CSR loss of eligibility
		if (currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR) {
			// entire HH aptc / csr eligibility is lost
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * this will new eligible for APTC / CSR for the entire HH
	 * @param currentApplication
	 * @param enrolledApplicationId
	 * @return
	 */
	public boolean checknewEigibleAptcCsrHH(SsapApplication currentApplication, long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		// check for the APTC/ CSR eligibility
		if ((currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP) ||
				(currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR )) {
			// entire HH aptc / csr eligibility is newly eligible
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * this will new eligible for APTC / CSR for the entire HH
	 * @param currentApplication
	 * @param enrolledApplicationId
	 * @return
	 */
	public boolean checknewEigibleAptcOnlyHH(SsapApplication currentApplication, long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		// check for the APTC eligibility
		if (((currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC || 
				currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR)
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP) ||
				((currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC || 
				  currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR)
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR )) {
			// entire HH aptc eligibility is newly eligible
			return true;
		} else {
			return false;
		}
	}
	
	
	/**
	 * This is for sending notice for Demo change - where zip and country is changed and no change in CSR level
	 * Then notice to be sent is EE060
	 * @param currentApplication
	 * @return
	 */
	public boolean checkDemoChange(SsapApplication currentApplication) {
		boolean flag = false;
		SsapApplicationEvent ssapApplicationEvent = ssapApplicationEventRepository
				.findApplicationEventAndApplicantEventsBySsapApplication(currentApplication.getId());
		final List<SsapApplicantEvent> applicantEvents = ssapApplicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent applicantEvent : applicantEvents) {
			if ("CHANGE_IN_ADDRESS".equalsIgnoreCase(applicantEvent.getSepEvents().getName())){
				if(!(ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL == ApplicantStatusEnum.fromValue(applicantEvent.getSsapApplicant().getStatus()))) {
					flag =  true;
					return flag;
				}
			}	
		}
		return flag;
		
	}
	
	/**
	 * this check for loss of aptc eligibility of current application
	 * @param currentApplication
	 * @param enrolledApplicationId
	 * @return
	 */
	public boolean checklossofEligiblityAptcOnlyHH(SsapApplication currentApplication, long enrolledApplicationId) {
		final SsapApplication enrolledApplication = loadCurrentApplication(enrolledApplicationId);
		// check for the loss of APTC eligibility
		if ((currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC) ||
				(currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR ) ||
				(currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR
				&& enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC )) {
			// entire HH  loss of aptc eligibility
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * In MN, we need to support only dental enrollment. In such cases the application status is in ER and application dental status is EN.
	 * Once any handler completes it's work, we might run into situation where 2 applications are in ER status. 
	 * Following method is called to specifically handle this scenario. 
	 * It will close the previous ER app when all of following conditions are met.
	 * enrolled application's application status = ER and 
	 * enrolled application's application dental status = EN and
	 * current application's status = ER and 
	 * current application's application dental status = EN
	 * 
	 * @param currentApplication
	 * @param lceProcessRequestDTO
	 */
	protected void closePreviousERApplication(SsapApplication currentApplication,
			LCEProcessRequestDTO lceProcessRequestDTO) {
		
		if(lceProcessRequestDTO != null && lceProcessRequestDTO.getEnrolledApplicationId() > 0){
			SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
			if(enrolledApplication != null && currentApplication != null &&
				ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(enrolledApplication.getApplicationDentalStatus()) &&
				ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(enrolledApplication.getApplicationStatus())	   &&
				ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(currentApplication.getApplicationDentalStatus())  &&
				ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(currentApplication.getApplicationStatus())){
				enrolledApplication.setApplicationStatus(ApplicationStatus.CLOSED.getApplicationStatusCode());
				LOGGER.info("CLOSING PREVIOUS ONLY DENTAL ENROLLED ER APPLICATION. ID = " + enrolledApplication.getId());
				ssapApplicationRepository.save(enrolledApplication);
			}
					
					
	}
		
	}
	
	protected boolean isDentalOnlyEnrollment(SsapApplication enrolledApplication){
		if(enrolledApplication != null && ApplicationStatus.ENROLLED_OR_ACTIVE.getApplicationStatusCode().equalsIgnoreCase(enrolledApplication.getApplicationDentalStatus()) &&
		ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode().equalsIgnoreCase(enrolledApplication.getApplicationStatus())){
			return Boolean.TRUE;
		}
		else{
			return Boolean.FALSE;
		}
	}
	
	protected String getEnrollmentType(Long applicationId){
		String enrollmentType = null;
		
		Map<String,Integer> enrollmentCount = fetchEnrollmentCount(applicationId);
		if(enrollmentCount != null && enrollmentCount.size()>0){
			int healthCount = enrollmentCount.get(ReferralConstants.HEALTH) != null ? enrollmentCount.get(ReferralConstants.HEALTH) : 0;
			int dentalCount = enrollmentCount.get(ReferralConstants.DENTAL) != null ? enrollmentCount.get(ReferralConstants.DENTAL) : 0;
			if(healthCount > 0 &&  dentalCount > 0){
				enrollmentType = ReferralConstants.BOTH;
			}
			else if(healthCount > 0 &&  dentalCount == 0){
				enrollmentType = ReferralConstants.HEALTH;
			}
			else if(healthCount == 0 &&  dentalCount > 0){
				enrollmentType = ReferralConstants.DENTAL;
			}
		}	
		return enrollmentType;
	}
	
	protected boolean isAutoAddAllowed(LCEProcessRequestDTO lceProcessRequestDTO){
		boolean flag = false;
		SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		
		if(enrolledApplication.getApplicationStatus().equalsIgnoreCase(ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode())){
			flag =  false;
		}
		else{
			
			Map<String,Integer> enrollmentCount = fetchEnrollmentCount(lceProcessRequestDTO.getEnrolledApplicationId());
			if(enrollmentCount != null && enrollmentCount.size()>0){
				int healthCount = enrollmentCount.get(ReferralConstants.HEALTH) != null ? enrollmentCount.get(ReferralConstants.HEALTH) : 0;
				int dentalCount = enrollmentCount.get(ReferralConstants.DENTAL) != null ? enrollmentCount.get(ReferralConstants.DENTAL) : 0;
				final String allowAutoAddDentalFlag =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ALLOW_AUTO_ADD_DENTAL);
				/**
				 * we need to allow auto add for the below conditions
				 * healthCount == 1 && dentalCount == 0 : autoadd = true
				 * healthCount == 1 && dentalCount == 1 : 
				 * 	check the flag (if true) then autoadd = true
				 * 	check the flag (if false) then autoadd = false 
				 */
				if(healthCount == 1){					
					if(dentalCount == 0 || (dentalCount == 1 && allowAutoAddDentalFlag.equalsIgnoreCase(Boolean.TRUE.toString()))) {
						flag = true;	
					}					
				}
			}
			
			// only for IDAHO the config  IEX_GROUPING_KEEP_NON_STANDARD_RELATIONSHIP_SEPARATE is true
			// check for relationship for auto add : If the relationship is standard (true or false returned by API) then proceed with automatic add. 
			// If the relationship is non standard then no automation and send EE057 notice to consumer
			// get the relationship of newly added applicant
			SsapApplicant ssapApplicantNewlyAdded = null;
			String nonStandardConfig = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_GROUPING_KEEP_NON_STANDARD_RELATIONSHIP_SEPARATE);
			if (nonStandardConfig != null && "true".equalsIgnoreCase(nonStandardConfig)) {
				// get the newly added member
				SsapApplication currentApplication = loadCurrentApplication(lceProcessRequestDTO.getCurrentApplicationId());
				for (SsapApplicant ssapApplicant : currentApplication.getSsapApplicants()) {
					if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(ssapApplicant.getStatus()))) {
						ssapApplicantNewlyAdded = ssapApplicant;
						break;
					}
				}
				
				boolean isStandard = false;
				
				if(null != ssapApplicantNewlyAdded) {
				// get the relationship code 
				String relationshipCode = ssapApplicantNewlyAdded.getRelationship();
				// call the api to get if its std or non std relationship
				isStandard = relationshipTypeService.isRelationshipStandardForCode(relationshipCode);
				}
				
				if(isStandard && flag) {
					flag = true;
				} else {
					// relationship is not std then autoadd is false
					flag = false;										
				}
			}
		}
		LOGGER.info("inside isAutoAddAllowed flag: "+flag);
		return flag;
	}

	protected boolean isAutoCSChangeAllowed(LCEProcessRequestDTO lceProcessRequestDTO){
		boolean flag = true;
		SsapApplication enrolledApplication = loadApplication(lceProcessRequestDTO.getEnrolledApplicationId());
		
		if(enrolledApplication.getApplicationStatus().equalsIgnoreCase(ApplicationStatus.PARTIALLY_ENROLLED.getApplicationStatusCode())){
			flag =  false;
		}
		/*
		 * else{ final String allowAutoAddDentalFlag =
		 * DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.
		 * IEX_ALLOW_AUTO_ADD_DENTAL); Map<String,Integer> enrollmentCount =
		 * fetchEnrollmentCount(lceProcessRequestDTO.getEnrolledApplicationId());
		 * if(enrollmentCount != null && enrollmentCount.size()>0){ int healthCount =
		 * enrollmentCount.get(ReferralConstants.HEALTH) != null ?
		 * enrollmentCount.get(ReferralConstants.HEALTH) : 0; int dentalCount =
		 * enrollmentCount.get(ReferralConstants.DENTAL) != null ?
		 * enrollmentCount.get(ReferralConstants.DENTAL) : 0;
		 * 
		 * if((healthCount == 1 && dentalCount == 0) || (healthCount == 0 && dentalCount
		 * == 1) || (healthCount == 1 && dentalCount == 1 &&
		 * allowAutoAddDentalFlag.equalsIgnoreCase(Boolean.TRUE.toString()))){ flag =
		 * true; } } }
		 */
		LOGGER.info("inside isAutoCSChangeAllowed flag: "+flag);
		return flag;
	}

			
}
