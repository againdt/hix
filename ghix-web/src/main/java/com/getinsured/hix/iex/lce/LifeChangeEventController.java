package com.getinsured.hix.iex.lce;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.HtmlUtils;

import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.eligibility.model.SepEvents.Financial;
import com.getinsured.hix.account.service.LocationService;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.iex.lce.task.LifeChangeEventHandler;
import com.getinsured.hix.iex.lce.task.LifeChangeEventTask;
import com.getinsured.hix.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.ZipCode;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.service.ZipCodeService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixEndPoints.SsapEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixRestServiceInvoker;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Component;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.LifeChangeEventDTO;
import com.getinsured.iex.lce.SepRequestParamDTO;
import com.getinsured.iex.lce.SepResponseParamDTO;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.LifeChangeEventUtil;
import com.getinsured.iex.util.SsapUtil;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Controller
@RequestMapping("/iex/lce")
public class LifeChangeEventController extends LifeChangeEventHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventController.class);

	private final String SSAP_JSON = "ssapJson";
	private final String STATE_CODE = "stateCode";
	
	@Autowired private Gson platformGson;
	@Autowired
	private ResourceCreator resourceCreator;
	@Autowired
	private UserService userService;
	@PersistenceUnit
	private EntityManagerFactory entityManagerFactory;
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private LookupService lookUpService;
	@Autowired
	private ZipCodeService zipCodeService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private SsapUtil ssapUtil;
	@Autowired 
	private LocationService locationService;
	@Autowired
	private NoticeService noticeService;
	@Autowired
	private GhixRestServiceInvoker ghixRestServiceInvoker;
	@Autowired
	private LifeChangeEventTask lifeChangeEventTask;
	@Autowired 
	private ConsumerPortalUtil consumerPortalUtil;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	@Autowired
	private LifeChangeEventUtil lifeChangeEventUtil;
	@Autowired
	SsapJsonBuilder ssapJsonBuilder;
	

	@RequestMapping(value = "/lceDenied", method = RequestMethod.GET)
	public String lceDenined(Model model,HttpServletRequest request){
		return "/iex/lce/lceDenied";
	}
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(Model model,HttpServletRequest request) throws InvalidUserException{
		LOGGER.info("viewReportOnChange : Starts");
		String ssapJson = "";
		
		try {
			AccountUser user = userService.getLoggedInUser();		
			LifeChangeEventDTO lifeChangeEventDTO = new LifeChangeEventDTO();
			lifeChangeEventDTO.setOldApplicationId(63050l);
			lifeChangeEventDTO.setUserName(user.getUsername());
			String responseDTO = ghixRestServiceInvoker.invokeRestService(lifeChangeEventDTO, 
					SsapEndPoints.LCE_GET_ENROLLED_APPLICANTS, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
			lifeChangeEventDTO =  JacksonUtils.getJacksonObjectReaderForJavaType(LifeChangeEventDTO.class).readValue(responseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in Get SSAP Application method", exception);
			giWSPayloadRepository.saveAndFlush(LifeChangeEventUtil.createGIWSPayload(exception, ssapJson, LifeChangeEventConstant.ENDPOINT_OPERATION_SHOW));
		}
		return "Welcome";
	}
	
	@RequestMapping(value = "/geteventnames/{caseNumber}", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getEventNames(@PathVariable("caseNumber") String caseNumber){
		LOGGER.info("get Event Name : Starts");
		String responseList="[]";
		try {
			AccountUser accountUser = userService.getLoggedInUser();
			
			if(RoleService.INDIVIDUAL_ROLE.equalsIgnoreCase(accountUser.getActiveModuleName())){
				SepRequestParamDTO sepRequestParamDTO = new SepRequestParamDTO();
				sepRequestParamDTO.setCaseNumber(caseNumber);
				sepRequestParamDTO.setHouseholdId((long) accountUser.getActiveModuleId());
				responseList = ghixRestServiceInvoker.invokeRestService(sepRequestParamDTO, SsapEndPoints.LCE_GET_EVENT_NAMES, HttpMethod.POST, GIRuntimeException.Component.LCE.toString());
			}
			//else{
				//responseList="{\"data\":[],\"status\":{\"response\":\"FAILURE\",\"message\":\"Unauthorized access\"}}";
			//}
		} catch (Exception exception) {
			//responseList="{\"data\":[],\"status\":{\"response\":\"FAILURE\",\"message\":\""+exception.getMessage()+"\"}}";
			LOGGER.error("Exception occured while fetching event names from case number.", exception);
		}
		return responseList;
	}
	

	
	
	/**
	 * handles view request for report your change
	 * @throws InvalidUserException 
	 */
	@GiAudit(transactionName = "Read individuals ssap-application data.", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.LIFE_CHANGING_EVENTS)
	@RequestMapping(value = "/reportyourchange", method = RequestMethod.GET)
	public String viewReportOnChange(Model model,HttpServletRequest request,@RequestParam("coverageYear") String coverageYearParam,
			@RequestParam(value ="mau",required = false) String mailingAddressUpdate ) throws InvalidUserException{
		LOGGER.info("viewReportOnChange : Starts");
		String ssapJson = "";
		long coverageYear = 2015;
		
		try {
			if(coverageYearParam !=null){
				coverageYear = Long.parseLong(coverageYearParam);
			}
			ssapJson = ssapApplicationRepository.findEnPnSsapApplicationForCoverageYear(new BigDecimal(userService.getLoggedInUser().getActiveModuleId()),coverageYear).getApplicationData();
			model.addAttribute(SSAP_JSON, ssapJson);
			loadEventProperties(model);
			long countOfApplicationsByStatus = ssapApplicationRepository.countOfApplicationsByStatusAndCoverageYear(new BigDecimal(userService.getLoggedInUser().getActiveModuleId()),"SEP",coverageYear);
			if(countOfApplicationsByStatus > 0){
				model.addAttribute("applicationInProgress", "true");
			}
			
			String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
			model.addAttribute(STATE_CODE, stateCode);
			model.addAttribute(LifeChangeEventConstant.COVERAGE_YEAR, coverageYear);
			if(mailingAddressUpdate != null && mailingAddressUpdate.equals("Y")){
				model.addAttribute("mailingAddressUpdate", "Y");
			}else{
				model.addAttribute("mailingAddressUpdate", "N");
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occured in Get SSAP Application method", exception);
			giWSPayloadRepository.saveAndFlush(LifeChangeEventUtil.createGIWSPayload(exception, ssapJson, LifeChangeEventConstant.ENDPOINT_OPERATION_SHOW));
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.LCE.toString(), null);
		}

		if(ssapJson == null || (ssapJson != null && ssapJson.equals(""))){
			return "redirect:"+userService.getLoggedInUser().getDefRole().getLandingPage();
		}
		LOGGER.info("viewReportOnChange : Ends");
		
		return "/iex/lce/reportyourchange";
	}
	
	@RequestMapping(value = "/savessapapplication", method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
	@GiAudit(transactionName = "Save modified ssap-application data of a individual.", eventType = EventTypeEnum.PII_WRITE, eventName = EventNameEnum.LIFE_CHANGING_EVENTS)
	@ResponseBody
	public String saveSSAPApplication(HttpServletRequest request, HttpServletResponse response) throws Exception {

		LOGGER.info("Save SSAP Application : Starts");
		String coverageYear = null;
		String ssapJson = null;
		String jsonResponse = null;
		response.setContentType(LifeChangeEventConstant.CONTENT_TYPE);
		String esignDate;
		String esignFirstName;
		String esignMiddleName;
		String esignLastName;
		SepResponseParamDTO sepResponseParamDTO;
		try{
			List<com.getinsured.iex.lce.RequestParamDTO>  requestParamDTOList = getRequestParameters(request);
			ssapJson = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.SSAP_JSON));
			SingleStreamlinedApplication singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapJson);
			//validation for json starts 
			Validator validator =  Validation.buildDefaultValidatorFactory().getValidator();
			Set<ConstraintViolation<SingleStreamlinedApplication>> constraintVoilation =  validator.validate(singleStreamlinedApplication);
			if(!constraintVoilation.isEmpty()){
				GIRuntimeException giRuntimeException = new GIRuntimeException();
				giRuntimeException.setComponent(Component.LCE.toString());
				giRuntimeException.setNestedStackTrace(constraintVoilation.toString());
				giRuntimeException.setErrorMsg(GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION);
				throw giRuntimeException; 
			}
			esignDate = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.ESIGN_DATE));
			esignFirstName = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.ESIGN_FIRST_NAME));
			esignMiddleName = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.ESIGN_MIDDLE_NAME));
			esignLastName = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.ESIGN_LAST_NAME));
			coverageYear = request.getParameter(LifeChangeEventConstant.COVERAGE_YEAR);
			
			//HIX-114766 Setting personType for LCE event
			List<HouseholdMember> members = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
			
			for(int icount=0;icount<members.size();icount++){
				HouseholdMember householdMember = members.get(icount);
				if(householdMember!=null){
					if(householdMember.getApplicantPersonType()==null) {
						if(householdMember.getPersonId() == 1) {
							householdMember.setApplicantPersonType(SsapApplicantPersonType.PC_PTF);
						}else {
							householdMember.setApplicantPersonType(SsapApplicantPersonType.DEP);
						}
					}
				}
			}
						
			String updatedJson = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);
			
			SepRequestParamDTO sepRequestParamDTO = new SepRequestParamDTO();
			sepRequestParamDTO.setEvents(requestParamDTOList);
			sepRequestParamDTO.setSsapJSON(updatedJson);
			sepRequestParamDTO.setHouseholdId(new Long(userService.getLoggedInUser().getActiveModuleId()));
			sepRequestParamDTO.setEsignDate(esignDate);
			sepRequestParamDTO.setEsignFirstName(esignFirstName);
			sepRequestParamDTO.setEsignLastName(esignLastName);
			sepRequestParamDTO.setEsignMiddleName(esignMiddleName);
			sepRequestParamDTO.setUserId(new Long(userService.getLoggedInUser().getId()));
			sepRequestParamDTO.setUserName(userService.getLoggedInUser().getUsername());
			sepRequestParamDTO.setAddTaxDependentReason(request.getParameter(LifeChangeEventConstant.ADD_TAX_DEPENDENT_REASON));
			sepRequestParamDTO.setFirstName(userService.getLoggedInUser().getFirstName());
			sepRequestParamDTO.setLastName(userService.getLoggedInUser().getLastName());
			sepRequestParamDTO.setCoverageYear(Long.parseLong(coverageYear));
	
			
			sepResponseParamDTO = ghixRestServiceInvoker.invokeRestService(sepRequestParamDTO, GhixEndPoints.SsapEndPoints.LCE_SAVEAPPLICATION_ENDPOINT, HttpMethod.POST, GIRuntimeException.Component.LCE.toString(), SepResponseParamDTO.class);
			//sepResponseParamDTO = (SepResponseParamDTO) GhixUtils.getXStreamStaxObject().fromXML(responseXML);
			jsonResponse = sepResponseParamDTO.getJsonObject();
			
			if(isUserSwitchToOtherView == null || isUserSwitchToOtherView.get()==null || isUserSwitchToOtherView.get().trim().equalsIgnoreCase("N")){
				if(sepResponseParamDTO.getIsNameChangedForPrimary()){
					AccountUser user = userService.getLoggedInUser();
					user.setFirstName(sepResponseParamDTO.getPrimaryFirstName());
					user.setLastName(sepResponseParamDTO.getPrimaryLastName());
				}
			}
		}catch(Exception ex){
			LOGGER.error("Exception occure while saving report a change",ex);
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.LCE.toString(), null);
		}
		return jsonResponse.toString();
	}
	
	private void loadEventProperties(Model model) throws Exception {
		
		JsonElement[] maritalStatus = new JsonElement[5];
		JsonElement[] updateDependents = new JsonElement[7];
		JsonElement[] incarceration = new JsonElement[2];
		JsonElement[] immigrationStatusChange = new JsonElement[4];
		JsonElement[] addressChange = new JsonElement[4];
		JsonElement[] income = new JsonElement[1];
		JsonElement[] changeInName = new JsonElement[1];
		JsonElement[] lossOfMec = new JsonElement[3];
		JsonElement[] gainMec = new JsonElement[2];
		JsonElement[] tribeStatus = new JsonElement[1];
		JsonElement[] demographics = new JsonElement[11];
		JsonElement[] other = new JsonElement[9];
		JsonParser parser = new JsonParser();
		
		try {
			String json = resourceCreator.findSepEventByFinancial(Financial.N);
			
			Object obj = parser.parse(json);
			JsonObject jsonObject = (JsonObject) obj;
			
			JsonArray sepEvents = jsonObject.get(LifeChangeEventConstant.SEP_EVENTS).getAsJsonArray();
			
			for(int i = 0; i < sepEvents.size(); i++) {
				JsonElement sepEvent = sepEvents.get(i);
				
				String eventCategory = sepEvents.get(i).getAsJsonObject().get(LifeChangeEventConstant.CATEGORY).getAsString();
				String eventName = sepEvent.getAsJsonObject().get(LifeChangeEventConstant.NAME).getAsString();
				
				if(LifeChangeEventConstant.MARITAL_STATUS.equals(eventCategory)){
					if(checkSupportedEventName(eventName, eventCategory)){
						if(LifeChangeEventConstant.MARITAL_STATUS_MARRIAGE.equals(eventName)){
							maritalStatus[0] = sepEvent;
						} else if(LifeChangeEventConstant.MARITAL_STATUS_DIVORCE_OR_ANULLMENT.equals(eventName)){
							maritalStatus[1] = sepEvent;
						} else if(LifeChangeEventConstant.MARITAL_STATUS_DEATH_OF_SPOUSE.equals(eventName)){
							maritalStatus[2] = sepEvent;
						} else if(LifeChangeEventConstant.MARITAL_STATUS_CIVIL_UNION.equals(eventName)){
							maritalStatus[3] = sepEvent;
						} else if(LifeChangeEventConstant.MARITAL_STATUS_LEGAL_SEPERATION.equals(eventName)){
							maritalStatus[4] = sepEvent;
						}
					}
				} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS.equals(eventCategory)){
					if(LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT.equals(eventName)){
						updateDependents[0] = sepEvent;
					} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_REMOVE_DEPENDENT.equals(eventName)){
						updateDependents[1] = sepEvent;
					} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH.equals(eventName)){
						updateDependents[2] = sepEvent;
					} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_ADOPTION.equals(eventName)){
						updateDependents[3] = sepEvent;
					} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEPENDENT_CHILD_AGES_OUT.equals(eventName)){
						updateDependents[4] = sepEvent;
					} else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_BIRTH.equals(eventName)){
						updateDependents[5] = sepEvent;
					}else if(LifeChangeEventConstant.UPDATE_DEPENDENTS_GAIN_COURT_APPOINTED_DEPENDENT.equals(eventName)){
						updateDependents[6] = sepEvent;
					}
				} else if(LifeChangeEventConstant.INCARCERATION_CHANGE.equals(eventCategory)){
					if(LifeChangeEventConstant.INCARCERATION.equals(eventName)){
						incarceration[0] = sepEvent;
					} else if(LifeChangeEventConstant.NOT_INCARCERATED.equals(eventName)){
						incarceration[1] = sepEvent;
					}
				} else if(LifeChangeEventConstant.ADDRESS_CHANGE.equals(eventCategory)){
					if(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE.equals(eventName)){
						addressChange[0] = sepEvent;
					} else if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_INTO_STATE.equals(eventName)){
						addressChange[1] = sepEvent;
					} else if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE.equals(eventName)){
						addressChange[2] = sepEvent;
					} else if(LifeChangeEventConstant.ADDRESS_CHANGE_MOVED_OUT_OF_STATE_DEPENDENTS.equals(eventName)){
						addressChange[3] = sepEvent;
					}
				} else if(LifeChangeEventConstant.TRIBE_STATUS.equals(eventCategory)){
					if(LifeChangeEventConstant.TRIBE_STATUS_GAIN_IN_TRIBE_STATUS.equals(eventName)){
						tribeStatus[0] = sepEvent;
					}
				} else if(LifeChangeEventConstant.LOSS_OF_MEC.equals(eventCategory)){
					if(LifeChangeEventConstant.LOSS_OF_MEC_LOSE_COVERAGE_NON_PAYMENT.equals(eventName)){
						lossOfMec[0] = sepEvent;
					} else if(LifeChangeEventConstant.LOSS_OF_MEC_LOSE_EMPLOYER_COVERAGE.equals(eventName)){
						lossOfMec[1] = sepEvent;
					} else if(LifeChangeEventConstant.LOSS_OF_MEC_LOSE_OTHER_PUBLIC_MEC.equals(eventName)){
						lossOfMec[2] = sepEvent;
					} 
				} else if(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE.equals(eventCategory)){
					if(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_GAIN_CITIZENSHIP.equals(eventName)){
						immigrationStatusChange[0] = sepEvent;
					} else if(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_GAIN_LPR.equals(eventName)){
						immigrationStatusChange[1] = sepEvent;
					} else if(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_LOSE_CITIZENSHIP.equals(eventName)){
						immigrationStatusChange[2] = sepEvent;
					} else if(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE_CHANGE_LPR.equals(eventName)){
						immigrationStatusChange[3] = sepEvent;
					}
				} else if(LifeChangeEventConstant.INCOME.equals(eventCategory)){
					if(LifeChangeEventConstant.INCOME.equals(eventName)){
						income[0] = sepEvent;
					}
				} else if(LifeChangeEventConstant.GAIN_MEC.equals(eventCategory)){
					if(LifeChangeEventConstant.GAIN_MEC_GAIN_EMPLOYER_COVERAGE_THRU_SPOUSE.equals(eventName)){
						gainMec[0] = sepEvent;
					} else if(LifeChangeEventConstant.GAIN_MEC_GAIN_OTHER_PUBLIC_MEC.equals(eventName)){
						gainMec[1] = sepEvent;
					} 
				} else if(LifeChangeEventConstant.CHANGE_IN_NAME.equals(eventCategory)){
					if(LifeChangeEventConstant.CHANGE_IN_NAME.equals(eventName)){
						changeInName[0] = sepEvent;
					} 
				} else if(LifeChangeEventConstant.DEMOGRAPHICS.equals(eventCategory)){
					if(LifeChangeEventConstant.DEMOGRAPHICS_NAME_CHANGE.equals(eventName)){
						demographics[0] = sepEvent;
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_SSN_CHANGE.equals(eventName)){
						demographics[1] = sepEvent;
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_MAILING_ADDRESS_CHANGE.equals(eventName)){
						demographics[2] = sepEvent;
					} else if(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE.equals(eventName)){
						demographics[3] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_ETHNICITY_RACE_CHANGE.equals(eventName)){
						demographics[4] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_GENDER_CHANGE.equals(eventName)){
						demographics[5] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_EMAIL_CHANGE.equals(eventName)){
						demographics[6] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_PHONE_NUMBER_CHANGE.equals(eventName)){
						demographics[7] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE.equals(eventName)){
						demographics[8] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_RELATIONSHIP_CHANGE_REMOVE.equals(eventName)){
						demographics[9] = sepEvent;
					}else if(LifeChangeEventConstant.DEMOGRAPHICS_DOB_CHANGE_REMOVE.equals(eventName)){
						demographics[10] = sepEvent;
					}  
				} else if(LifeChangeEventConstant.OTHER.equals(eventCategory)){
					if(LifeChangeEventConstant.OTHER_OTHER_EXCEPTIONAL_CIRCUMSTANCES.equals(eventName)){
						other[0] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_OTHER_MISREPRESENTATION.equals(eventName)){
						other[1] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_OTHER_ENROLLMENTERROR.equals(eventName)){
						other[2] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_OTHER_CANCEL_QHP.equals(eventName)){
						other[3] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_ENROLLMENT_BY_MARKETPLACE_OR_CONSUMER_CONNECTOR_AFTER_DAY_FREE_LOOK_PERIOD.equals(eventName)){
						other[4] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_ELIG_EXEMPTION.equals(eventName)){
						other[5] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_OTHER_CONTRACT.equals(eventName)){
						other[6] = sepEvent;
					} /*else if(LifeChangeEventConstant.OTHER_OTHER_CHANGE_COVERAGE.equals(eventName)){
						other[7] = sepEvent;
					} */ else if(LifeChangeEventConstant.OTHER_OTHER_DOMESTIC_ABUSE.equals(eventName)){
						other[7] = sepEvent;
					} else if(LifeChangeEventConstant.OTHER_OTHER_SYSTEM_ERRORS.equals(eventName)){
						other[8] = sepEvent;
					}
				}
			}
			
			model.addAttribute(LifeChangeEventConstant.MARITAL_STATUS, platformGson.toJson(maritalStatus));
			model.addAttribute(LifeChangeEventConstant.UPDATE_DEPENDENTS,  platformGson.toJson(updateDependents));
			model.addAttribute(LifeChangeEventConstant.INCARCERATION_CHANGE, platformGson.toJson(incarceration));
			model.addAttribute(LifeChangeEventConstant.IMMIGRATION_STATUS_CHANGE, platformGson.toJson(immigrationStatusChange));
			model.addAttribute(LifeChangeEventConstant.ADDRESS_CHANGE,  platformGson.toJson(addressChange));
			model.addAttribute(LifeChangeEventConstant.LOSS_OF_MEC,  platformGson.toJson(lossOfMec));
			model.addAttribute(LifeChangeEventConstant.INCOME,  platformGson.toJson(income));
			model.addAttribute(LifeChangeEventConstant.GAIN_MEC,  platformGson.toJson(gainMec));
			model.addAttribute(LifeChangeEventConstant.CHANGE_IN_NAME,  platformGson.toJson(changeInName));
			model.addAttribute(LifeChangeEventConstant.TRIBE_STATUS,  platformGson.toJson(tribeStatus));
			model.addAttribute(LifeChangeEventConstant.DEMOGRAPHICS,  platformGson.toJson(demographics));
			model.addAttribute(LifeChangeEventConstant.OTHER,  platformGson.toJson(other));
		} catch (Exception exception) {
			throw exception;
		}
	}
	
	private boolean checkSupportedEventName(String eventName, String category){
		if(getEventList(category).contains(eventName)){
			return true;
		}
		return false;
	}
	
	private List<String> getEventList(String category){
		List<String> maritalStatus = null;
		List<String> updateDependents = null;
		List<String> incarceration = null;
		List<String> lawfulPresence = null;
		List<String> changeInAddress = null;
		List<String> lossEssentialCoverage = null;
		List<String> gainEssentialCoverage = null;
		List<String> tribeStatus = null;
		List<String> other = null;
		List<String> demographics = null;
		
		List<String> list = new ArrayList<>();
		
		EVENTS event = EVENTS.valueOf(category);
		
		switch(event){
		    case MARITAL_STATUS :
		    	maritalStatus = new ArrayList<>();
		    	maritalStatus.add(LifeChangeEventConstant.MARITAL_STATUS_MARRIAGE);
		    	maritalStatus.add(LifeChangeEventConstant.MARITAL_STATUS_DIVORCE_OR_ANULLMENT);
		    	maritalStatus.add(LifeChangeEventConstant.MARITAL_STATUS_DEATH_OF_SPOUSE);
		    	list = maritalStatus;
		        break; 
		    case UPDATE_DEPENDENTS :
		    	updateDependents = new ArrayList<>();
		    	updateDependents.add(LifeChangeEventConstant.UPDATE_DEPENDENTS_ADD_DEPENDENT);
		    	updateDependents.add(LifeChangeEventConstant.UPDATE_DEPENDENTS_REMOVE_DEPENDENT);
		    	updateDependents.add(LifeChangeEventConstant.UPDATE_DEPENDENTS_DEATH);
		    	list = updateDependents;
		        break; 
		    case INCARCERATION :
		    	incarceration = new ArrayList<>();
		    	list = incarceration;
			   break; 
		    case LAWFUL_PRESENCE :
		    	lawfulPresence = new ArrayList<>();
		    	list = lawfulPresence;
			   break; 
		    case ADDRESS_CHANGE :
		    	changeInAddress = new ArrayList<>();
		    	changeInAddress.add(LifeChangeEventConstant.ADDRESS_CHANGE_WITHIN_STATE);
		    	list = changeInAddress;
		       break; 
		    case LOSS_OF_MEC :
		    	lossEssentialCoverage = new ArrayList<>();
		    	list = lossEssentialCoverage;
		    	break; 
		    case GAIN_MEC :
		    	gainEssentialCoverage = new ArrayList<>();
		    	list = gainEssentialCoverage;
			   break; 
		    case TRIBE_STATUS :
		    	tribeStatus = new ArrayList<>();
		    	list = tribeStatus;
				break;
		    case DEMOGRAPHICS :
		    	demographics = new ArrayList<>();
		    	list = demographics;
				break;
		    case OTHER :
		    	other = new ArrayList<>();
		    	list = other;
				break;
		}
		
		return list;
	}
	
	private enum EVENTS {
		MARITAL_STATUS, UPDATE_DEPENDENTS, INCARCERATION, ADDRESS_CHANGE, TRIBE_STATUS, GAIN_MEC, LOSS_OF_MEC, LAWFUL_PRESENCE, DEMOGRAPHICS, OTHER;
	}
	
	/**
	 * Method to load Counties for a given State.
	 *  
	 * @param stateCode The state code of whose counties are sought.
	 * @return jsonData Stringified JSON of County name collection.
	 */
	@RequestMapping(value = "/getCounties",  method = RequestMethod.GET)
	@GiAudit(transactionName = "Read list of counties using stateCode input.", eventType = EventTypeEnum.PII_READ, eventName = EventNameEnum.LIFE_CHANGING_EVENTS)
	@ResponseBody
	public String loadCountiesList(@RequestParam("state") String stateCode){
		LOGGER.info("loadCountiesList : START");
		
		String jsonData = null;
		
		List<Map<String, String>> counties = populateCountiesDataSet(stateCode);
		 
		
		jsonData = platformGson.toJson(counties);
		
		LOGGER.info("loadCountiesList : END");

		return jsonData;
	}
	
	/**
	 * Method to populate Counties data set for a given state code.
	 * 
	 * @param stateCode The state code.
	 * @return countyMap The collection of counties for the current State.
	 */
	private List<Map<String, String>> populateCountiesDataSet(String stateCode){
		LOGGER.info("populateCountiesDataSet : START");
		List<ZipCode> zipCodeVOList = null;  
		Set<String> countiesServedCollection = new TreeSet<String>();
		Map<String, String> countyMap = null;
		List<Map<String, String>> counties = new ArrayList<Map<String, String>>();
		
		if(null == stateCode || stateCode.trim().isEmpty()) {
			LOGGER.info("State code is missing in the request.", stateCode);
		}
		else {
			zipCodeVOList = zipCodeService.getCountyListForState(stateCode);
			
			for(ZipCode currZipCode : zipCodeVOList){
				if(null != currZipCode.getCounty() && !currZipCode.getCounty().trim().isEmpty()) {
					countiesServedCollection.add(currZipCode.getCounty());
				}
			}
			
			for(String currCounty: countiesServedCollection) {
				countyMap = new LinkedHashMap<String, String>();
				countyMap.put("code", currCounty);
				countyMap.put("name", currCounty);
				counties.add(countyMap);
			}
		}
		
		LOGGER.info("populateCountiesDataSet : END");
		
		return counties;
	}	
		
	private List<com.getinsured.iex.lce.RequestParamDTO> getRequestParameters(HttpServletRequest request) throws Exception{
		 
		com.getinsured.iex.lce.RequestParamDTO requestParams = null;
		JsonArray events;
		JsonParser parser = new JsonParser();
		List<com.getinsured.iex.lce.RequestParamDTO> list = new ArrayList<>();
		
		try {
			String eventJson = HtmlUtils.htmlUnescape(request.getParameter(LifeChangeEventConstant.EVENT_INFO));
			
			if(!LifeChangeEventUtil.isEmpty(eventJson)){
				Object obj = parser.parse(eventJson);
				events = (JsonArray) obj;
				
				Iterator<JsonElement> iterator = events.iterator();
				while (iterator.hasNext()) {
					requestParams = platformGson.fromJson(iterator.next().getAsJsonObject(),com.getinsured.iex.lce.RequestParamDTO.class);
					//validate future event date - currently validate for Tribe Change
					LifeChangeEventUtil.validateDate(requestParams);
					list.add(requestParams);
				}
			}
		} catch(Exception exception){
			throw exception;
		}
		
		return list;
	}
}
