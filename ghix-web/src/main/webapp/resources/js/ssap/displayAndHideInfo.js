
function displayAuthInfo() {
	/*$('#parsley-authorizedRepresentativeIndicator').hide();*/
	if ($("#displayAuthRepresentativeInfo").is(":checked")) {
		$("#authPerInfoPage54").show();
		
		
		//var householdName = $('#firstName').val() + " "+ $('#middleName').val() + " "+ $('#lastName').val();
		 $('.appscr54_houseHoldName').text(householdName);
		 updatePrimaryContactName();
	} else if ($("#hideAuthRepresentativeInfo").is(":checked")) {
		$("#authPerInfoPage54").hide();

	}
}
function nameOfCompanyAndOrg54()
{
	if ($("#cmpnyNameAndOrgRadio54_1").is(":checked")) {
		$("#cmpnyNameAndOrgDiv").show();
	} else if ($("#cmpnyNameAndOrgRadio54_2").is(":checked")) {
		$("#cmpnyNameAndOrgDiv").hide();

	}
}





function askForSSNNoPage61() {
	
	if ($("#socialSecurityCardHolderIndicatorYes").is(":checked")) {
		$("#askAboutSSNOnPage61").show();
		$("#DontaskAboutSSNOnPage61").hide();
	} else if ($("#socialSecurityCardHolderIndicatorNo").is(":checked")) {
		$("#askAboutSSNOnPage61").hide();
		$("#DontaskAboutSSNOnPage61").show();

	}
	
}

function hasSameSSNNumberP61() {
	if ($("#fnlnsSameIndicatorYes").is(":checked")) {
		$("#sameSSNPage61Info").hide();
	} else if ($("#fnlnsSameIndicatorNo").is(":checked")) {
		$("#sameSSNPage61Info").show();

	}
	
}
function hasAnIdividualMandateExemptionP61() {
	if ($("#fnlnsExemptionIndicatorYes").is(":checked")) {
		$("#mandateExemptionP61").show();
	} else if ($("#fnlnsExemptionIndicatorNo").is(":checked")) {
		$("#mandateExemptionP61").hide();

	}
	
}

function displayOrHideSameNameInfoPage62() {
	 if ($("#62Part2_UScitizen").is(":checked")) {
		$("#sameNameOnDocument").show();

	}else{
		$("#sameNameOnDocument").hide();
	}
}



function method4AnotherChildCheckBox() {
	if($("#takeCareOf_anotherChild").is(":checked"))
		{
			$(".parentOrCaretakerBasicDetailsContainer").show();
		}
	else{
		$(".parentOrCaretakerBasicDetailsContainer").hide();
	}
}


function money4OtherIncometypeCheckBox() {
	if($("#anotherIncomeTypeSource").is(":checked"))
		{
			$(".incomeTypeDetailcontainer").show();
		}
	else{
		$(".incomeTypeDetailcontainer").hide();
	}
}




function isUSCitizen() {
	
	if($('input[name=UScitizen]:radio:checked').val()=='true')
		{
			$("#naturalizedCitizenDiv").hide();
			$("#documentTypeDiv").hide();
			$("#eligibleImmigrationStatusDiv").hide();
		}
	else if($('input[name=UScitizen]:radio:checked').val()=='false'){
			$("#naturalizedCitizenDiv").show();
			
			isNaturalizedCitizen();
			/*$("#documentTypeDiv").hide();
			$("#eligibleImmigrationStatusDiv").hide();
			if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='yes')
			{
				$("#documentTypeDiv").show();
				$("#eligibleImmigrationStatusDiv").hide();
			}
			else{
				$("#documentTypeDiv").hide();
				$("#eligibleImmigrationStatusDiv").show();
			}*/
		}
}

$(document).ready(function(){
	$("#eligibleImmigrationStatus input:radio").click(function(){
		$("#sameNameInfoPage62P2, #haveTheseDocuments").show();
	});	
});


function parentCareTakerHideShow() {
	
	if($('input[name=appscr63AtLeastOneChildUnder19Indicator]:radio:checked').val()=='yes')
		{
			$(".parentCareTaker").show();
			if($("#takeCareOf_anotherChild").is(":checked")){
				$(".parentOrCaretakerBasicDetailsContainer").show();
			}
		}
	else{
		$(".parentCareTaker, .parentOrCaretakerBasicDetailsContainer").hide();
		
	}
}

function isNaturalizedCitizen()
{
	
	if($('#UScitizenIndicatorNo').is(':checked')){
		if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='true')
			{			
				$("#documentTypeDiv").show();
				$('#eligibleImmigrationStatusDiv').hide();
				
			}
		else if($('input[name=naturalizedCitizenshipIndicator]:radio:checked').val()=='false'){
	
			$("#documentTypeDiv").hide();
			$('#eligibleImmigrationStatusDiv').show();
		//	$('#naturalCitizen_eligibleImmigrationStatus_ID').hide();
			
		}
	}
	else{
		$('#eligibleImmigrationStatusDiv').hide();
		$("#documentTypeDiv").hide();
	}

}


function showOrHideAt62Part2()
{
	if($('input[name=docType1]:radio:checked').val() == '1')
	{
		$('#appscr62P1_otherAlignAndI-94_table').show();
		$('#appscr62P1_otherComment_label').show();
		$('#appscr62P1_otherComment').show();
	}
	else
	{
		$('#appscr62P1_otherAlignAndI-94_table').hide();
		$('#appscr62P1_otherComment_label').hide();
		$('#appscr62P1_otherComment').hide();
	}
}

function showOrHide81BInfo()
{
	if($('#peopleHealthInsurNone').is(":checked") == true)
	{
		$("#peopleHealthInsurDependChild").prop("checked",false);
		$('#showOrHide_divAt81B').show();
	}
	else 
	{
		$('#showOrHide_divAt81B').hide();
	}
}

function showOrHide81BInfoPart2() {
	if($("#peopleHealthInsurDependChild").is(":checked")){
		$('#peopleHealthInsurNone').prop("checked",false);
		$('#showOrHide_divAt81B').hide();
	}
}

function checkLanguage()
{
	if($('input[name=checkPersonNameLanguage]:radio:checked').val()=='yes')
		{
		$('#ethnicityDiv').show();
		}
	else
		{
		$('#ethnicityDiv').hide();
		}

}

function checkLivingInColorado(applicantActualName)
{
	
	if($('input[name=isTemporarilyLivingOutsideIndicator'+applicantActualName+']:radio:checked').val()=='yes')
	{
		$('#inColoradoDiv'+applicantActualName).show();
	}
	else
	{
		$('#inColoradoDiv'+applicantActualName).hide();
	}

}


function checkAmericonIndianQuestion(i)
{
	if($('input[name=AmericonIndianQuestionRadio'+ i +']:radio:checked').val()=='yes')
		{
		$('#americonIndianQuestionDiv' + i).show();
		}
	else if($('input[name=AmericonIndianQuestionRadio'+ i +']:radio:checked').val()=='no')
		{
		$('#americonIndianQuestionDiv' + i).hide();
		}
	
	
	
	
}

function showOrHideOptionDiv() {
	if ($("#wantToGetHelpPayingHealthInsurance_id1").is(":checked")) {
		$("#contant_message_ui").hide();
		/*if ($('#noOfPeople53').val().length <= 0 ) {*/		 	
		 	$('#noOfPeople53').val("1");
		 	$("#noOfPeople53").focus();
	    /*}*/ 
	} else if ($("#wantToGetHelpPayingHealthInsurance_id2").is(":checked") && $("#exchangeName ").val() == "NMHIX") {
		$("#contant_message_ui").show();
	}
}
function hideSSNOnPage80()
{
	var $isSSNDisplayable=$("input[name=ssnIndicator80]:radio:checked").val().toLowerCase();
	if($isSSNDisplayable=='yes')
	{
		$("#ssnOnPage80").show();
	}
	else if ($isSSNDisplayable=='no') {
		$('#ssn1_80').removeClass("validateColor");
		$('#ssn2_80').removeClass("validateColor");
		$('#ssn3_80').removeClass("validateColor");
		$("#ssnOnPage80").hide();
	}
}






function showOrHideNaturalCitizenImmigrationStatus()
{
	var $checkBox=$("#naturalCitizen_eligibleImmigrationStatus");
	if ($checkBox.is(":checked")) {
		$("#naturalCitizen_eligibleImmigrationStatus_ID").show();
	}
	else $("#naturalCitizen_eligibleImmigrationStatus_ID").hide();
}


function disabilityOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#disability'+i).prop("checked"))
		{
			flag = true;
		}
	}	
	if(flag)
		$('#disabilityNoneOfThese').prop("checked", false);
}
function disabilityNoneOfTheseChecked()
{
	if($('#disabilityNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#disability'+i).prop("checked", false);
		}
		
	}
}

function assistanceServicesOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#assistanceServices'+i).prop("checked"))
		{
			flag = true;
		}
	}	
	if(flag)
		$('#assistanceServicesNoneOfThese').prop("checked", false);
}
function assistanceServicesNoneOfTheseChecked()
{
	if($('#assistanceServicesNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#assistanceServices'+i).prop("checked", false);
		}
		
	}
}

function incarceratedOnChange(obj){
	
		if(obj.checked)
		{
			$('#incarceratedContent85').hide();				
			$("input[name=FNLNS]").prop("checked",false);	
			$("input[radio-name=deposition]").prop("checked",false);
		}else{
			$('#incarceratedContent85').show();			
		}
	
}

function incarcerationCBChange(i){	
	if($('#incarcerationCB'+i).prop("checked"))	{
		$('#incarcerationCBOption'+i).show();			
	}else{
		$('#incarcerationCBOption'+i).hide();			
	}
	
}

function nativeOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#native'+i).prop("checked"))
		{
			$('#nativeTribe'+i).show();
			checkAmericonIndianQuestion(i);
			flag = true;
		}else{
			$('#nativeTribe'+i).hide();
			checkAmericonIndianQuestion(i);
		}
	}	
	if(flag){
		$('#nativeNoneOfThese').prop("checked", false);
	}
}
function nativeNoneOfTheseChecked()
{
	if($('#nativeNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#native'+i).prop("checked", false);
			$('#native'+i).parent().next().hide();
		}
		
	}
}

function personfosterCareOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#personfosterCare'+i).prop("checked"))
		{
			flag = true;
		}
	}	
	if(flag)
		$('#personfosterCareNoneOfThese').prop("checked", false);
}
function personfosterCareNoneOfTheseChecked()
{
	if($('#personfosterCareNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#personfosterCare'+i).prop("checked", false);
		}
		
	}
}

function deceasedOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#deceased'+i).prop("checked"))
		{
			flag = true;
		}
	}	
	if(flag)
		$('#deceasedPersonNoneOfThese').prop("checked", false);
}
function deceasedNoneOfTheseChecked()
{
	if($('#deceasedPersonNoneOfThese').prop("checked"))
	{
		for(var i = 1; i<=noOfHouseHold; i++)
		{
			$('#deceased'+i).prop("checked", false);
		}
		
	}
}

function pregnancyOnChange()
{
	var flag = false;
	
	for(var i = 1; i<=noOfHouseHold; i++)
	{
		if($('#pregnancy'+i).html() != undefined)
			if($('#pregnancy'+i).prop("checked"))
			{
				flag = true;
			}
	}	
	if(flag)
		$('#pregnancyNoneOfThese').prop("checked", false);
}
function pregnancyNoneOfTheseChecked()
{
	if($('#pregnancyNoneOfThese').html() != undefined)
		if($('#pregnancyNoneOfThese').prop("checked"))
		{
			for(var i = 1; i<=noOfHouseHold; i++)
			{
				$('#pregnancy'+i).prop("checked", false);
			}
			document.getElementById('appscr66PregnancyDetail').innerHTML = "";
		}
}

/*code by @Shovan as per new requirement*/
function textboxFocusEventFire(){
	$('#dateOfBirth').keyup(function(){
		if($(this).val()!= ""){
			$('.dateSpan').removeClass('dateSpanWithError');
		}else{
			$('.dateSpan').addClass('dateSpanWithError');
		}
	});
}

function termsCheckboxIsCheck(){
	if(!$('#acceptanceCB').is(":checked")){
		$('#parsley-terms-checkbox').show();
	}
	$('#acceptanceCB').click(function(){
		if(!$(this).is(":checked")){
			$('#parsley-terms-checkbox').show();
		}else{
			$('#parsley-terms-checkbox').hide();
		}
	});
	
}

/*for hix-33700*/
function payPerWeekRadioCheck(){
	if($('input[name=payPerWeekRadio]:radio:checked').val()=='Yes'){
		$('#dateEmployementExpire').show();
	}else{
		$('#dateEmployementExpire').hide();
	}
}



function applyingHousehold(){
	$('#parsley-ApplyingForhouseHold').hide();
}

/*code by @Shovan ends*/

function displayDateUnemploymentExpire(sel){
	  var selectedValue = sel.value; 
	  if(selectedValue == "Weekly" || selectedValue == "Monthly"){
		  $("#isDateUnemploymentSet").show();
		  if($('input[name=payPerWeekRadio]:radio:checked').val()=='Yes'){
			  $('#dateEmployementExpire').show();
		  }
		  
	  }else{
		  $("#isDateUnemploymentSet, #dateEmployementExpire").hide();
	  }
}


function howOftenGetThisAmount(sel){
	  var selectedValue = sel.value; 
	  if(selectedValue == "Hourly"){
		  $("#howMuchPerWeek").show();
		  $("#Financial_Job_HoursOrDays_id").attr("data-parsley-pattern","^([1-9][0-9]?|1[0-5][0-9]|16[0-8])$");
		  
	  }else if(selectedValue == "Daily") {
		  $("#howMuchPerWeek").show();
		  $("#Financial_Job_HoursOrDays_id").attr("data-parsley-pattern","^[1-7]{1}$");
		  
	  }else{
		  $("#howMuchPerWeek").hide();
	  }
}


function displayWillBeOfferedFromJob(){
	 $("#willBeOfferedFromJob").show();
}

function displayAuthorizedRepresenatitive(){
	if($('input[name=authorizedRepresentativeHelp]:radio:checked').val() == 'yes'){
		$('#authorizedRepresentativeDiv').show();
	}else{
		$('#authorizedRepresentativeDiv').hide();
	}
}


function displaySigniture(){
	if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'signature'){
		$('#signitureLabel').show();
	}else if($('input[name=makeOtherizedRepresentative]:radio:checked').val() == 'later'){
		$('#signitureLabel').hide();
	}
}


function updateTaxReturnPeriod(){
	if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'yes'){
		$('#taxReturnPeriodDiv').hide();
	}else if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'no'){
		$('#taxReturnPeriodDiv').show();
	}	
}

function displayOtherEthnicity(){
	if($('#ethnicity_other').is(':checked')){
		$('#otherEthnicity').show();
	}else{
		$('#otherEthnicity').hide();
	}
}

function displayOtherRace(){
	if($('#race_other').is(':checked')){
		$('#otherRace').show();
	}else{
		$('#otherRace').hide();
	}
}