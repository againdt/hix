
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElementRef;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a telephone number for a telecommunication device.
 * 
 * <p>Java class for TelephoneNumberType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TelephoneNumberType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}TelephoneNumberRepresentation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TelephoneNumberType", propOrder = {
    "telephoneNumberRepresentation"
})
public class TelephoneNumberType
    extends ComplexObjectType
{

    @XmlElementRef(name = "TelephoneNumberRepresentation", namespace = "http://niem.gov/niem/niem-core/2.0", type = JAXBElement.class)
    protected JAXBElement<?> telephoneNumberRepresentation;

    /**
     * Gets the value of the telephoneNumberRepresentation property.
     * 
     * @return
     *     possible object is
     *     {@link JAXBElement }{@code <}{@link FullTelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public JAXBElement<?> getTelephoneNumberRepresentation() {
        return telephoneNumberRepresentation;
    }

    /**
     * Sets the value of the telephoneNumberRepresentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link JAXBElement }{@code <}{@link FullTelephoneNumberType }{@code >}
     *     {@link JAXBElement }{@code <}{@link Object }{@code >}
     *     
     */
    public void setTelephoneNumberRepresentation(JAXBElement<?> value) {
        this.telephoneNumberRepresentation = value;
    }

}
