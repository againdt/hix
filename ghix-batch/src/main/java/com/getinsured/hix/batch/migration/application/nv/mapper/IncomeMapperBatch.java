package com.getinsured.hix.batch.migration.application.nv.mapper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.indportal.dto.dm.application.AttestationMembersIncome;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;

@Component
public class IncomeMapperBatch {
	
	
public static final Map<String,IncomeCategoryCodeSimpleType> incomeCategoryMap;
	
	static {
		incomeCategoryMap = new HashMap<String, IncomeCategoryCodeSimpleType>();
		incomeCategoryMap.put("UNEMPLOYMENT", IncomeCategoryCodeSimpleType.UNEMPLOYMENT);
		incomeCategoryMap.put("JOB", IncomeCategoryCodeSimpleType.WAGES);
		incomeCategoryMap.put("SOCIAL_SECURITY_BENEFIT", IncomeCategoryCodeSimpleType.SOCIAL_SECURITY);
		incomeCategoryMap.put("ALIMONY_PAYMENT", IncomeCategoryCodeSimpleType.ALIMONY);
		incomeCategoryMap.put("OTHER_INCOME", IncomeCategoryCodeSimpleType.UNSPECIFIED);
		incomeCategoryMap.put("SELF_EMPLOYMENT", IncomeCategoryCodeSimpleType.SELF_EMPLOYMENT);
		incomeCategoryMap.put("STUDENT_LOAN_INTEREST", IncomeCategoryCodeSimpleType.ALIMONY); //yet to be mapped to correct value
		incomeCategoryMap.put("RETIREMENT", IncomeCategoryCodeSimpleType.RETIREMENT);
		incomeCategoryMap.put("CAPITAL_GAINS", IncomeCategoryCodeSimpleType.CAPITAL_GAINS);
		incomeCategoryMap.put("INVESTMENT_INCOME", IncomeCategoryCodeSimpleType.INTEREST);
		incomeCategoryMap.put("RENTAL_OR_ROYALTY_INCOME", IncomeCategoryCodeSimpleType.RENTAL_OR_ROYALTY);
		incomeCategoryMap.put("FARMING_OR_FISHING_INCOME", IncomeCategoryCodeSimpleType.FARMING_OR_FISHING);
		incomeCategoryMap.put("PRIZES_AWARDS_GAMBLING_WINNINGS", IncomeCategoryCodeSimpleType.WINNINGS);
		incomeCategoryMap.put("ALIMONY_RECEIVED", IncomeCategoryCodeSimpleType.ALIMONY);
		incomeCategoryMap.put("OTHER_DEDUCTION", IncomeCategoryCodeSimpleType.ALIMONY); //yet to be mapped to correct value
		incomeCategoryMap.put("COURT_AWARDS", IncomeCategoryCodeSimpleType.COURT_AWARD);
		incomeCategoryMap.put("JURY_DUTY_PAY", IncomeCategoryCodeSimpleType.JURY_DUTY);
		incomeCategoryMap.put("CANCELED_DEBT", IncomeCategoryCodeSimpleType.CANCELED_DEBT);
		incomeCategoryMap.put("PENSION", IncomeCategoryCodeSimpleType.PENSION);
		incomeCategoryMap.put("CASH_SUPPORT", IncomeCategoryCodeSimpleType.CASH_SUPPORT);
		incomeCategoryMap.put("SCHOLARSHIP", IncomeCategoryCodeSimpleType.SCHOLARSHIP);
	}

	private static final BigDecimal maxWeekDays = new BigDecimal(7);
	private static final BigDecimal maxWeekHours = new BigDecimal(0);
	public List<IncomeType> mapIncomes(AttestationMembersIncome income) {

		List<IncomeType> personIncomes = new ArrayList<IncomeType>();

		income.getCurrentIncome().forEach((k, v) -> {
			//if("ALIMONY_PAYMENT".equals(currentIncome.getValue().getIncomeSourceType()) || "STUDENT_LOAN_INTEREST".equals(currentIncome.getValue().getIncomeSourceType()))
			boolean processIncomeTag = true;
			if("STUDENT_LOAN_INTEREST".equals(v.getIncomeSourceType()) || 
					("ALIMONY_PAYMENT".equals(v.getIncomeSourceType()) && BigDecimal.ZERO.compareTo(v.getIncomeAmount() )>0))//process only positive alimony amounts
			{
				processIncomeTag = false;
			}
			if (processIncomeTag) {
				IncomeType incomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();

				IncomeCategoryCodeType incomeCategoryCodeType = AccountTransferUtil.hixTypeFactory
						.createIncomeCategoryCodeType();
				incomeCategoryCodeType.setValue(incomeCategoryMap.get(v.getIncomeSourceType()));
				incomeType.setIncomeCategoryCode(incomeCategoryCodeType);

				AmountType incomeAmountType = AccountTransferUtil.niemCoreFactory.createAmountType();
				incomeAmountType.setValue(v.getIncomeAmount().setScale(2, RoundingMode.HALF_UP));
				incomeType.setIncomeAmount(incomeAmountType);

				FrequencyType frequencyType = AccountTransferUtil.hixCoreFactory.createFrequencyType();
				FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
				String frequency = v.getIncomeFrequencyType();
				if ("ONE_TIME".equalsIgnoreCase(frequency)) {
					frequency = "ONCE";
				}
				frequencyCodeType.setValue(FrequencyCodeSimpleType.valueOf(frequency).value());
				frequencyType.setFrequencyCode(frequencyCodeType);
				incomeType.setIncomeFrequency(frequencyType);

				if (null != v.getJobIncome()) {

					if (null != v.getJobIncome().getAverageWeeklyWorkDays()) {
						BigDecimal weekWorkDays = new BigDecimal(v.getJobIncome().getAverageWeeklyWorkDays())
								.setScale(2, RoundingMode.HALF_UP);
						if (weekWorkDays.compareTo(BigDecimal.ZERO) > 0) {
							NumericType daysPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
							if (weekWorkDays.compareTo(maxWeekDays) > 0) {
								daysPerWeek.setValue(maxWeekDays);
							} else {
								daysPerWeek.setValue(weekWorkDays);
							}
							incomeType.setIncomeDaysPerWeekMeasure(daysPerWeek);
						}
					}

					if (null != v.getJobIncome().getAverageWeeklyWorkHours()) {
						BigDecimal hrsPerWeek = new BigDecimal(v.getJobIncome().getAverageWeeklyWorkHours()).setScale(2,
								RoundingMode.HALF_UP);
						if (hrsPerWeek.compareTo(BigDecimal.ZERO) > 0) {
							NumericType hoursPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
							hoursPerWeek.setValue(hrsPerWeek);
							incomeType.setIncomeHoursPerWeekMeasure(hoursPerWeek);
						} else if (hrsPerWeek.compareTo(BigDecimal.ZERO) == 0) {
							NumericType hoursPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
							if (hrsPerWeek.compareTo(maxWeekHours) > 0) {
								hoursPerWeek.setValue(maxWeekHours);
							}
							incomeType.setIncomeHoursPerWeekMeasure(hoursPerWeek);
						}
					} else if ("JOB".equals(v.getIncomeSourceType())
							&& "HOURLY".equalsIgnoreCase(v.getIncomeFrequencyType())
							&& null == v.getJobIncome().getAverageWeeklyWorkHours()) {
						NumericType hoursPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
						hoursPerWeek.setValue(maxWeekHours);
						incomeType.setIncomeHoursPerWeekMeasure(hoursPerWeek);
					}

				}

				if (IncomeCategoryCodeSimpleType.SELF_EMPLOYMENT == incomeCategoryMap.get(v.getIncomeSourceType())) {

					TextType selfEmploymentIncomeDescText = AccountTransferUtil.niemCoreFactory.createTextType();

					if (null != v.getSelfEmploymentIncomeDescription()) {
						selfEmploymentIncomeDescText.setValue(v.getSelfEmploymentIncomeDescription());
					} else {
						selfEmploymentIncomeDescText.setValue(v.getIncomeSourceType());
					}

					incomeType.setIncomeEmploymentDescriptionText(selfEmploymentIncomeDescText);
				}

				if (null != v.getUnemploymentIncome()) {
					TextType incomeUnemploymentSourceText = AccountTransferUtil.niemCoreFactory.createTextType();
					incomeUnemploymentSourceText.setValue(v.getUnemploymentIncome().getIncomeDescription());
					incomeType.setIncomeUnemploymentSourceText(incomeUnemploymentSourceText);

					DateRangeType incomeEarnedDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
					DateType incomeEarnedEndDate = AccountTransferUtil.niemCoreFactory.createDateType();
					com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDate = AccountTransferUtil.basicFactory
							.createDate();
					endDate.setValue(AccountTransferUtil
							.stringToXMLGregorianCalendar(v.getUnemploymentIncome().getExpirationDate()));
					incomeEarnedEndDate.setDate(endDate);
					incomeEarnedDateRange.setEndDate(incomeEarnedEndDate);
					incomeType.setIncomeEarnedDateRange(incomeEarnedDateRange);
				}

				personIncomes.add(incomeType);
			}
		});

		return personIncomes;
	}

}
