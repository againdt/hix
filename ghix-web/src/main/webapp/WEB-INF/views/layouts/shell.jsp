<%@page import="com.getinsured.hix.platform.util.GhixPlatformConstants" %>
<%@page import="com.getinsured.hix.platform.config.IEXConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration"%>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration.GlobalConfigurationEnum"%>
<%@page import="com.getinsured.hix.platform.config.UIConfiguration"%>
<%@page import="com.getinsured.hix.util.GhixConfiguration"%>
<%@page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@page import="com.getinsured.hix.platform.util.GhixConstants"%>
<%@ page session="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="java.util.List"%>
<%@page import="org.apache.commons.lang.StringUtils"%>
<%@page import="com.getinsured.hix.broker.utils.BrokerConstants" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@page import="com.getinsured.hix.platform.config.SecurityConfiguration"%>

<%!
	static final String Y = "Y", N = "N";
	String anonymousFlag = Y;
%>
<%


       String themeName = (String) request.getSession().getAttribute(
                     "switchToThemeName");
	   String iframe = (String) request.getSession().getAttribute("iframe");
	   if (StringUtils.isBlank(iframe)) {
		    themeName = "no";
     	}
       if (StringUtils.isBlank(themeName)) {
              themeName = "default";
       }
       String bodyId = "body-" + themeName;

       if(null != request && null != request.getSession() && null != request.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG)) {
			anonymousFlag = request.getSession().getAttribute(BrokerConstants.ANONYMOUS_FLAG).toString().trim();

			if(null != SecurityContextHolder.getContext() && null != SecurityContextHolder.getContext().getAuthentication() && null != SecurityContextHolder.getContext().getAuthentication().getPrincipal() && !BrokerConstants.ANONYMOUS_USER.equals(SecurityContextHolder.getContext().getAuthentication().getPrincipal())) {
				anonymousFlag = N;
			}
			else {
				anonymousFlag = Y;
			}
		}
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8" />
<!-- <meta http-equiv="Cache-Control" content="max-age=0" />
<meta http-equiv="Cache-Control" content="no-cache,no-store,must-revalidate" /> -->
<!-- <meta http-equiv="Expires" content="0" /> -->
<!-- <META HTTP-EQUIV="Expires" CONTENT="Mon, 22 Jul 2002 11:12:01 GMT"> -->
<!-- <meta http-equiv="Pragma" content="no-cache" />  -->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<%
       String defaultTitle = DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME);
       String pageTitle = "";

       String trackingCode= GhixConstants.GOOGLE_ANALYTICS_CODE;
	   String gtmContainerID = GhixPlatformConstants.GTM_CONTAINER_ID;
       request.setAttribute("trackingCode",trackingCode);
	   request.setAttribute("gtmContainerID",gtmContainerID);
       if (themeName == "employer"){
                   pageTitle = defaultTitle + "- Employer Portal";
       }
       else if (themeName == "employee"){
                   pageTitle = defaultTitle + "- Employee Portal";
       }
       else if (themeName == "issuer"){
                   pageTitle = defaultTitle + "- Issuer Portal";
       }
       if (themeName == "agent"){
                   pageTitle = defaultTitle + "- Agent Portal";
       }
       else{
                   pageTitle = defaultTitle;
       }
%>
<title><%=(pageTitle)%></title>
<meta name="description" content="" />
<meta name="author" content="" />

<c:set var="stateCode" value="<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE)%>" />
<c:set var="bootstrap_style" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BOOTSTRAP_LESS_FILE)%>"></c:set>
<c:set var="fav_icon" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDFAVICON)%>"></c:set>
<c:set var="quick_fixes" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.THEMEQUICKFIXES_FILE)%>"></c:set>
<c:set var="ghix_customjs" value="<%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.THEMEJS_FILE)%>"></c:set>
<link rel="shortcut icon" type="image/x-icon" href="<gi:cdnurl value='/resources/images/${fav_icon}' />"/>


<%--  <link href="<gi:cdnurl value="/resources/css/bootstrap.min.css" />" rel="stylesheet"  type="text/css" media="screen,print" /> --%>
 <link href="<gi:cdnurl value="/resources/css/bootstrap-responsive.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/bs3-responsive-classes.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/formvalidation.css" />" rel="stylesheet"  type="text/css" media="screen,print" />

  <link rel="stylesheet" type="text/css" media="screen" href='<gi:cdnurl value="/resources/css/browser-detection.css" />' />
 <link href="<gi:cdnurl value="/resources/css/custom-theme/jquery-ui-1.10.4.custom.min.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/jquery.ui.datepicker.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
<!--  <link href='//fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css' media="screen,print" />     -->
<link href="<gi:cdnurl value="/resources/fonts/droid-serif.css" />" rel='stylesheet' type='text/css' media="screen, print" /><!--  <link href='//fonts.googleapis.com/css?family=Merriweather' rel='stylesheet' type='text/css' media="screen,print"> -->

 <link href="<gi:cdnurl value="/resources/css/${bootstrap_style}" />" media="screen,print" rel="stylesheet" type="text/css" />
 <link href="<gi:cdnurl value="/resources/css/ghixcustom.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
 <link href="<gi:cdnurl value="/resources/css/${quick_fixes}" />" media="screen,print" rel="stylesheet" type="text/css" />
 <link href="<gi:cdnurl value="/resources/css/jquery.multiselect.css" />" rel="stylesheet"  type="text/css" media="screen,print" />

 <c:if test="${(iframe == 'yes')}">
 <link href="<gi:cdnurl value="/resources/css/iframe.css" />" media="screen,print" rel="stylesheet" type="text/css" />
 </c:if>

 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/common/ajaxinterceptor.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-2.0.0.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-migrate-1.4.1.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/angular.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/angular-cookies.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/angular-route.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/angular-resource.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/angular-translate.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-ui-1.9.2.custom.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.multiselect.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery.validate.min.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/bootstrap.min.js" />"></script>
 <%-- <script type="text/javascript" src="<gi:cdnurl value="/resources/js/ghixcustom.js" />"></script> --%>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/jquery-ui-touch-punch-0.2.2.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/accessibility.js" />"></script>
 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/accessibility-angular.js" />"></script>

 <script type="text/javascript" src="<gi:cdnurl value="/resources/js/${ghix_customjs}" />"></script>

	<sec:authorize access="isAuthenticated()" var="isUserAuthenticated" />
    <script type="text/javascript">
          var pingUrl = "<%=GhixPlatformConstants.PLATFORM_PING_URL%>"; //For external servers like ca
          var authenticatedPage = "${isUserAuthenticated eq true ? true : false}";
          var extendSessionUrl = "/hix/preeligibility/session/ping";
          var clientInactivityThreshold =  "<%=DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.CLIENT_INACTIVITY_THRESHOLD)%>";
          var popupThresholdValue =  "<%=DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.SESSION_POPUP_THRESHOLD)%>";
          var logoutUrl = "/hix/account/user/logout";
    </script>
    <script type="text/javascript" src="<gi:cdnurl value="/resources/js/sessioninactivity.js" />"></script>

  <c:if test="${(iframe == 'yes')}">
    <script type="text/javascript" src="<gi:cdnurl value="/resources/js/gi.iframe.js" />"></script>
  </c:if>
<script type="text/javascript" src="<gi:cdnurl value="/resources/js/bootstrap-datepicker.js" />"></script>
<link href="<gi:cdnurl value="/resources/css/datepicker.css" />" rel="stylesheet"  type="text/css" media="screen,print" />
<script>

  window.dataLayer = window.dataLayer || [];

/**
  * Datalayer variables that will be updated by another component while this page loads.
  * We declare them here with a default value and push them at the end of shell, 
  * after all components have loaded.
  */

  // Data layer variables to be pushed at the end of shell.jsp within pageViewGI event.
  var dl_pageCategory = "Other";
  var dl_caseID = "";

  // A stack of datalayer events that have to be pushed after the pageViewGI event.
  var dl_eventStack = [];

</script>

<!-- Google Tag Manager -->
<script>
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','${gtmContainerID}');
</script>

<!-- End Google Tag Manager -->
<c:if test="${not empty fn:trim(trackingCode)}">
  <script type="text/javascript">
    //need to put _gaq variable declaration to global scope
      var _gaq = _gaq || [];
      var gaPageName = null;
      var googleAnalyticsTrackingCodes = '${trackingCode}';
      var listOfGoogleAnalyticsTrackingCodes = googleAnalyticsTrackingCodes.split(',');
      function triggerGoogleTrackingEvent(eventData) {
        for (i = 0; i < listOfGoogleAnalyticsTrackingCodes.length; i++) {
          var updatedEventData = [];
            var namespacePrefix = i + "ga.";
            for (j = 0; j < eventData.length; j++) {
              var eventDataElement = eventData[j];
                if(j == 0) {
                  eventDataElement = namespacePrefix + eventData[j];
                }
                updatedEventData[j] = eventDataElement;
            }
            _gaq.push(updatedEventData);
      }
    }

         var errPage = false;
        $("img").each(function() {
            if ($(this).attr('src') && $(this).attr("src").slice(-13) == "404-error.png") {
               errPage = true;
            };
        });

        for (i = 0; i < listOfGoogleAnalyticsTrackingCodes.length; i++) {
            var domainName = window.location.hostname;
            domainName = domainName.replace('www.', '');
            var namespacePrefix = i + "ga.";
            _gaq.push([namespacePrefix + '_setAccount', listOfGoogleAnalyticsTrackingCodes[i]]);
            _gaq.push([namespacePrefix + '_setDomainName', domainName]);
            _gaq.push([namespacePrefix + '_setAllowLinker', true]);
            _gaq.push([namespacePrefix + '_addIgnoredRef', domainName]); // Stops Self Referrals

            if (errPage) {
                _gaq.push([namespacePrefix + '_trackPageview', '/404.html?page=' + document.location.pathname + document.location.search + '&from=' + document.referrer]);
            } else {
                _gaq.push([namespacePrefix + '_trackPageview', location.pathname + location.search + location.hash]);
            }
        }

        (function() {
            var ga = document.createElement('script');
            ga.type = 'text/javascript';
            ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'stats.g.doubleclick.net/dc.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(ga, s);
        })();

  </script>
</c:if>

</head>
<%--
<%
  String themeName = (String) request.getSession().getAttribute(
      "switchToThemeName");
  if (StringUtils.isBlank(themeName)) {
    themeName = "default";
  }
  String bodyId = "body-" + themeName;
%> --%>
<c:set var="themeName" value="<%=themeName%>" />
<div  id="<%=bodyId%>">

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=${gtmContainerID}"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<%
String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
String userActiveRoleLabel =  (String) request.getSession().getAttribute("userActiveRoleLabel");
String householdCaseID =  (String) request.getSession().getAttribute("householdCaseID");
String userName =  (String) request.getSession().getAttribute("USER_NAME");
String defaultRoleName = (String) request.getSession().getAttribute("defaultRoleName");
String showAgentPopup = "N";
if(request.getParameter("showAgentPopup") != null){
	showAgentPopup = "Y";
}
%>
<c:set var="userActiveRoleName" value="<%=userActiveRoleName%>" />
<c:set var="userActiveRoleLabel" value="<%=userActiveRoleLabel%>" />
<c:set var="householdCaseID" value="<%=householdCaseID%>" />
<c:set var="defaultRoleName" value="<%=defaultRoleName%>" />
<input type="hidden" id="gtm_userName" name="userName" value="<%=userName%>">
<input type="hidden" id="gtm_userActiveRoleName" name="userActiveRoleName" value="<%=userActiveRoleName%>">
<input type="hidden" id="gtm_userActiveRoleLabel" name="userActiveRoleLabel" value="<%=userActiveRoleLabel%>">
<input type="hidden" id="gtm_householdCaseID" name="householdCaseID" value="<%=householdCaseID%>">
<input type="hidden" id="defaultRoleName" name="defaultRoleName" value="<%=defaultRoleName%>">
<input type="hidden" id="showAgentPopup" name="showAgentPopup" value="<%=showAgentPopup%>">
    <c:url value="/broker/search" var="completeURL">
      <c:param name="anonymousFlag" value="<%=anonymousFlag%>"/>
    </c:url>
    <c:if test="${not (iframe == 'yes')}">
      <div class="topbar"><tiles:insertAttribute name="masthead" /></div>
      <div id="topnav" class="topbar"><tiles:insertAttribute name="topnav" /></div>
    </c:if>
    <div id="container-wrap" class="row-fluid">
      <div id="container" class="container">
      <%
      	List<String> roleNamesList = (List<String>) request.getAttribute("roleNames");
  		  String isExternalCall = (String) request.getSession().getAttribute("isExternalCall");
  		  boolean showMenu = true;
  		  if(Boolean.parseBoolean(isExternalCall) || "yes".equals(iframe)) {
  			 showMenu = false;
  		  }
  		  if (roleNamesList.size() > 0 && showMenu) {
        %>
          <tiles:insertAttribute name='menu' />
        <%
          }
        %>
            <tiles:insertAttribute name="main" />
        </div>
    </div>
    </div>

    <div id="footer"><tiles:insertAttribute name="footer" /></div>

    <div id="pingData"></div>


<script>

  $(document).ready(function() {
	if($("#showAgentPopup").val() == "Y"){
		var href = '<c:out value="${completeURL}"/>';
		openFindBrokerDialog(href);
	}
    function getSearchParameters() {
      var prmstr = window.location.search.substr(1);
      return prmstr ? transformToAssocArray(prmstr) : {};
    }

    function transformToAssocArray( prmstr ) {
      var params = {};
      var prmarr = prmstr.split('&');
      for ( var i = 0; i < prmarr.length; i++) {
        var tmparr = prmarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
      }
      return params;
    }

    var userId = $("#gtm_userName").val();
    var userActiveRoleName = $("#gtm_userActiveRoleName").val();
    var userActiveRoleLabel = $("#gtm_userActiveRoleLabel").val();
    var householdCaseID = $("#gtm_householdCaseID").val();

    var dl_getInsuredTool = '';
    var dl_userSignin = '';

    if(userId !="" && userId != undefined && userId != "null") {
        dl_getInsuredTool = 'Portal',
        dl_userSignin = 'Signed In'
    } 
    else {
        dl_getInsuredTool = 'Shop and Compare',
        dl_userSignin = 'Not Signed In'
    }

    /** 
     * Now that all the variables are ready, we push them to the dataLayer
     * under the pageViewGI event. GTM looks out for this event to grab
     * all relevant information for a page view.
     */
    window.dataLayer.push({
      'event': 'pageViewGI',
      'pageName' : document.title,
      'pageCategory': dl_pageCategory,
      'referringPageUrl': document.referrer,
      'destinationPageUrl': document.URL,
      'userSignin': dl_userSignin,
      'getInsuredTool': dl_getInsuredTool,
      'caseID': dl_caseID,
    });

    /** 
     * Now push the rest of the events inside the eventStack array.
     */
     while (dl_eventStack.length != 0) {
      window.dataLayer.push(dl_eventStack.pop());
     }

    var params = getSearchParameters();
    var searchParams = params['lang'] ? params['lang'] : 'en';

  });
</script>


 <script type="text/javascript">
  <%--  $(window).load(function(){
   var userActiveRoleName = '${userActiveRoleName}';
   var switchToModuleNameLocal = $('#encSwitchToModuleNameLocal').val();

   $.ajax({ url: "/hix/ping",
        type: "get",
       data: {
            localmodname:switchToModuleNameLocal
        },
        cache: false,
        success: function(response){
                if(null != response) {
                  if(1 == response) {
                  $("#pingData").html = response;
                window.location.href = "<%=GhixConstants.APP_URL%>account/user/login";
                  } else if(2 == response) {
                            $("#pingData").html = response;
                            window.location.href = "<%=GhixConstants.APP_URL%>account/user/switchNonDefRole/${userActiveRoleName}";
                        }

				  	}
	          },
	 		  error : function(e) {
      			//alert('Error: ' + e);
     	  }
	 });
	}); --%>
  
  function updateUrlParameter(uri, key, value) {
        // remove the hash part before operating on the uri
        var i = uri.indexOf('#');
        var hash = i === -1 ? ''  : uri.substr(i);
        uri = i === -1 ? uri : uri.substr(0, i);

        var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
        var separator = uri.indexOf('?') !== -1 ? "&" : "?";

        if (uri.match(re)) {
            uri = uri.replace(re, '$1' + key + "=" + value + '$2');
        } else {
            uri = uri + separator + key + "=" + value;
        }
        return uri + hash;  // finally append the hash as well
    }
    
	function changeLang(urlParamValue, language) {
        $.ajax({
            type : "GET",
            url : "/hix/private/changeLang?lang="+urlParamValue,
            success : function() {
                sessionStorage.setItem('language', language);
                window.location.href = updateUrlParameter(window.location.href, 'lang', urlParamValue);
            },
            error : function(e) {
              sessionStorage.setItem('language', language);
            }
        });
  }

  function setPageTitle(pageTitle) {
    document.title = pageTitle + ' / ' + $('#exchangeName').val();
  }

 function openInNewTab(url) {
    var win = window.open(url, '_blank');
    win.focus();
}
 function openInSameTab(url) {
    window.location.href= url;
}
function timeStamp() {
    // Get local time as ISO string with offset at the end
    var now = new Date();
    var tzo = -now.getTimezoneOffset();
    var dif = tzo >= 0 ? '+' : '-';
    var pad = function(num) {
        var norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
    };
    return now.getFullYear()
        + '-' + pad(now.getMonth()+1)
        + '-' + pad(now.getDate())
        + 'T' + pad(now.getHours())
        + ':' + pad(now.getMinutes())
        + ':' + pad(now.getSeconds())
        + '.' + pad(now.getMilliseconds())
        + dif + pad(tzo / 60)
        + ':' + pad(tzo % 60);
}
		$(function() {
			$("#getAssistance").click(function(e) {
				e.preventDefault();
				var href = $(this).attr('href');
				if (href != undefined && href.indexOf('#') != 0 && href.indexOf('/broker/search') != 0) {
					$(
							'<div id="searchBox" class="modal bigModal modalsize-l" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div class=""><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div>')
							.modal();
				}
			});
			$("#pop_AgentPromotion").click(function(e) {
				$('#agentPromotion').modal('hide');
				var href = '<c:out value="${completeURL}"/>';
				openFindBrokerDialog(href);
			});
			$("#pop_findAgent, #pop_findLocalAssistance, #pop_getAssistance").click(function(e) {
				e.preventDefault();
				var href = '<c:out value="${completeURL}"/>';
				//console.log("hre"+href);
				openFindBrokerDialog(href);
			});
		});

		<c:if test="${stateCode == 'ID'}">
			$(function() {
				$(".financialHelpCheck").click(function(e) {
				e.preventDefault();
					$(
					'<div id="financialHelp" class="modal hide fade bigger-modal" tabindex="-1" role="dialog" aria-labelledby="confirmApplyModal" aria-hidden="true">'
						+ '<div class="modal-header">'
							+ '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>'
							+ '<h3 id="confirmApplyModal">Apply for health insurance coverage</h3>'
						+ '</div>'
						+ '<div class="modal-body">'
						+ '<p>You may qualify for cost-savings on your health insurance coverage. If you are interested, click &#34;Apply for Cost-Savings&#34; to determine your eligibility for cost-savings programs through the application at Idaho&#39;s Department of Health and Welfare. If you are not interested in cost-savings you can apply for coverage without cost-savings on Your Health Idaho.</p>'
						+ '</div>'
						+ '<div class="modal-footer">'
							+ '<button class="btn pull-left" data-dismiss="modal" aria-hidden="true">Cancel</button>'
							+ '<button class="btn btn-primary" data-dismiss="modal" aria-hidden="true" onclick="window.open(\'<%=DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.FINANCIAL_SSAP_URL)%>\');$(\'.modal-backdrop\').hide();">Apply for Cost-Savings</button>'
							+ '<a class="btn btn-primary" href="<c:url value=".'
							+ $(this).attr('redirectOnNoUrl')
							+ '" />">Apply without Cost-Savings</a>'
						+ '</div>'
					+ '</div>').modal();
				});
			});
		</c:if>
		function openFindBrokerDialog(href)
		{
			$('<div id="brokersearchBox" class="modal" tabindex="-1" role="dialog"><div class="modal-dialog"><div class="modal-content"><div class="searchModal-header"> <button type="button" class="agentClose close"><span aria-hidden="true">&times;</span><span class="aria-hidden">close</span></button></div><div><iframe id="search" src="' + href + '" class="searchModal-body"></iframe></div></div></div></div>').modal({show: true});
		}

		function closeSearchLightBox() {
			$("#searchBox").remove();
			$("#brokersearchBox, .modal-backdrop").remove();
		}

		function closeSearchLightBoxOnCancel() {
			$("#searchBox").remove();
		}

		$('.agentClose').live('click',function(){
			$('#brokersearchBox, .modal-backdrop').remove();
			$('body').removeClass('modal-open');
			//location.reload();
		});

		$(document).keyup(function(e) {
			if (e.keyCode == 27) {
				//$('#brokersearchBox, .modal-backdrop').remove(); // commented modal-backdrop getting removed on ESC key - HIX-48280
			}
		});

		$('#closeAgentSerchBox').click(function(event){
			event.preventDefault();
			$( ".agentClose" ).trigger( "click" );
		});
	</script>

  <script type="text/javascript" src="<gi:cdnurl value="/resources/js/browser-detection.js" />"></script>
  <script>
  var notSupportedBrowsers = [
	  {'os': 'Any', 'browser': 'Chrome', 'version': 74},
	  {'os': 'Any', 'browser': 'MSIE', 'version': 9},
	  {'os': 'Any', 'browser': 'Safari', 'version': 9},
		{'os': 'Any', 'browser': 'Firefox', 'version': 65},
		{'os': 'Any', 'browser': 'Edge', 'version': 14}
	];
  </script>




<div id="activityModal" class="modal hide fade" tabindex="-1" >
  <div class="modal-body">
    <p><spring:message code="label.homePage.shell.sessionTimeout"/></p>
  </div>
  <div class="modal-footer">
    <button id="aid_timeoutOK" class="btn btn-primary" onclick="sessionRenew();"><spring:message code="label.homePage.shell.ok"/></button>
  </div>
</div>
</body>
</html>
