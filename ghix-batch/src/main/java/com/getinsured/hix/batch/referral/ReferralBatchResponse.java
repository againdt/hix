package com.getinsured.hix.batch.referral;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author chopra_s
 * 
 */
public class ReferralBatchResponse {

	private String batchName;
	
	private List<Map<String, String>> successfullApplications = new ArrayList<Map<String, String>>();
	
	private List<Map<String, String>> failedApplications = new ArrayList<Map<String, String>>();
	
	private Date startTime;
	
	private Date endTime;

	public String getBatchName() {
		return batchName;
	}

	public void setBatchName(String batchName) {
		this.batchName = batchName;
	}

	public List<Map<String, String>> getSuccessfullApplications() {
		return successfullApplications;
	}

	public void setSuccessfullApplications(List<Map<String, String>> successfullApplications) {
		this.successfullApplications = successfullApplications;
	}

	public List<Map<String, String>> getFailedApplications() {
		return failedApplications;
	}

	public void setFailedApplications(List<Map<String, String>> failedApplications) {
		this.failedApplications = failedApplications;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	
}
