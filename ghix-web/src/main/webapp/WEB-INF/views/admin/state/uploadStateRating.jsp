<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
        		<li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="#"><spring:message  code="label.plan"/></a></li>
        		<li><a href="<c:url value="/admin/staterating/view"/>"><spring:message  code="pgheader.staterating"/></a></li>
        		<li><spring:message  code="pgheader.uploadNewFile"/></li>
            </ul><!--page-breadcrumb ends-->
	            <h1 class="offset3"><spring:message  code="pgheader.stateratingmapping"/></h1>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="gray">
                    <h4 class="margin0"><spring:message  code="pgheader.aboutthisfile"/></h4>
                </div>
                <!--  beginning of side bar -->
                <ul class="nav nav-list">
                    <li><spring:message  code="label.uploadInfo"/></li>
                </ul>
                <!-- end of side bar -->
			</div>
			<!-- end of span3 -->
			
			<div class="span9" id="rightpanel">
                    <div class="header">
                        <h4 class="pull-left"><spring:message  code="pgheader.uploadNewStateRating"/></h4>
                        <a class="btn btn-primary btn-small pull-right" href="#" onclick="javascript:submitForm();"><spring:message  code="label.save"/></a>
                        <a class="btn btn-small pull-right" href="<c:url value="/admin/staterating/view"/>"><spring:message  code="label.cancel"/></a> 
                    </div>
                    <div class="gutter10">
                    <form class="form-horizontal" id="frmStateAreaRating" name="frmStateAreaRating" method="POST" enctype="multipart/form-data" action="<c:url value="/admin/stateAreaRating/document/save"/>">
                   		 <df:csrfToken/>
						<label class="control-label" for="stateAreaRatingFile"><spring:message  code="label.stateRatingFile"/><img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" />
							<a class="ttclass" rel="tooltip" href="#" data-original-title="Upload State Area Rating File "><i class="icon-question-sign"></i></a>
						</label>
						
						<div class="controls">
							<input type="file" id="stateAreaRatingFile" name="stateAreaRatingFile"  class="input-file" onchange="return validateFileExtension(this)"/>
							&nbsp; <button type="submit" class="btn" id="btn_state_area_rating" name="btn_state_area_rating"><spring:message  code="label.upload"/></button>
							<div id="stateAreaRating_display"></div>
							<div id="stateAreaRatingFile_error" class=""></div>
							<input type="hidden" id="hdnStateAreaRating" name="hdnStateAreaRating" value=""/>
						</div>
						
						<br />
						<label class="control-label" for="stateAreaRatingFile"><spring:message  code="label.addComment"/></label>
						<div class="controls paddingT5">
							<textarea class="input-xlarge" name="comment_text" id="comment_text" rows="4" cols="40" style="resize: none;" 
							maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();"></textarea>
							<input type="hidden" id="hdnCommentText" name="hdnCommentText" value=""/>
							<div id="comment_text_error"  class="help-inline"></div>
							<div id="chars_left"><spring:message  code="label.charLeft"/> <b>500</b></div>
						</div>
						
						<div id="invalidInfo" style="color:red;">
						<%String invalidZips=null;invalidZips=request.getParameter("invalidZips");%>
						<%if(invalidZips!=null && !(invalidZips.equals("null"))){ %>
							<%=invalidZips%> <spring:message  code="label.invalidZipCodes"/><br>
						<%}%>
						
						<%String invalidRatingArea=null;invalidRatingArea=request.getParameter("invalidRatingArea");%>
						<%if(invalidRatingArea!=null && !(invalidRatingArea.equals("null"))){ %>
							<%String stringDisplay=invalidRatingArea.toString().replace("%20", " "); %>
							<%=stringDisplay%> <spring:message  code="label.invalidRatingArea"/><br>
						<%}%>
						
						</div>
						
                    </form>
                </div>
            </div><!--  end of span9 -->
		</div><!-- end row-fluid -->
	</div>
	</div>
	<script>
	var csrValue = $("#csrftoken").val();
	function validateFileExtension(fld) {
	    if(!/(\.csv)$/i.test(fld.value)) {
	        $.alert("Invalid File Selected to upload", "Invalid Selection");
	        fld.form.reset();
	        fld.focus();        
	        return false;   
	    }   
	    return true; 
	 }
	
	function submitForm(){
		 $("#frmStateAreaRating").submit();
	 } 
		  
	  $(document).ready(function(){
		  $("#btn_state_area_rating").click(function(){
				$('#frmStateAreaRating').ajaxSubmit({
					url: "<c:url value='/admin/statearearatingfile/upload'/>",
					data: {"csrftoken":csrValue},
					success: function(responseText){
							if(responseText.value=='undefined'){
								return false;
							}
						    var val = responseText.split("|");
						    $("#stateAreaRating_display").text(val[1]);
							$("#hdnStateAreaRating").val(val[2]);
			       	}
				});
			  return false; 
			});	// button click close
		  });
  
	</script>
	<script type="text/javascript">
	$('.ttclass').tooltip();
	var validator = $("#frmStateAreaRating").validate({ 
		rules : {
			stateAreaRatingFile : {required : true},
		},
		messages : {
			stateAreaRatingFile: { required : "<span><em class='excl'>!</em><spring:message  code='err.qualityRating' javaScriptEscape='true'/></span>"},
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error');
		} 
	});
	
	function updateCharCount(){
		var commentMaxLen=500;
		var currentLen = $.trim(document.getElementById("comment_text").value).length;
		 var test=document.getElementById("comment_text").value;
		 $("#hdnCommentText").val(test);
		 //alert(document.getElementById("hdnCommentText").value);
		var charLeft = commentMaxLen - currentLen;
		$('#chars_left').html('Characters left <b>' + charLeft + '</b>' );
	}
	
	$.extend({ alert: function (message, title) {
		  $("<div></div>").dialog( {
		    buttons: { "Ok": function () { $(this).dialog("close"); } },
		    close: function (event, ui) { $(this).remove(); },
		    resizable: false,
		    title: title,
		    modal: true
		  }).text(message);
		}
		});
</script>