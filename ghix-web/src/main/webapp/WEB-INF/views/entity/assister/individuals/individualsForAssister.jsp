<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ page isELIgnored="false"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.tablesorter.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/tablesorter.css" />" />

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

 <%
	//Integer pageSizeInt = (Integer) request.getAttribute("pageSize");
	//String reqURI = (String) request.getAttribute("reqURI");
	//Integer totalResultCount = (Integer) request
	//		.getAttribute("resultSize");
	//if (totalResultCount == null) {
	//	totalResultCount = 0;
	//}
	//Integer currentPage = (Integer) request.getAttribute("currentPage");
	//int noOfPage = 0;
	//System.out.println("tes " + pageSizeInt);
	//if (totalResultCount > 0) {
	//	if ((totalResultCount % pageSizeInt) > 0) {
	//		noOfPage = (totalResultCount / pageSizeInt) + 1;
	//	} else {
	//		noOfPage = (totalResultCount / pageSizeInt);
	//	}
	//}
%> 
<!--  -->
<style>
#rightpanel tbody tr td:first-child{padding-left: 11px;}
.page-breadcrumb {margin-top: 0 !important;}
.page-breadcrumb li i.icon-circle-arrow-left {top: 0px;}
.dropdown-menu {min-width: 100px;}
.table tbody tr td .dropdown.open li a {
   text-align: left;
}

</style>
<!--start page-breadcrumb -->
<div class="gutter10">
  <!-- <div class="l-page-breadcrumb">
 	<div class="row-fluid"> 
		<ul class="page-breadcrumb">
			<li><a href="javascript:history.back()">&lt; <spring:message code="label.back" /></a></li> 
 			<li><a href="<c:url value="/assister/individuals"/>"><spring:message code="label.individuals" /></a></li>
			<li>${desigStatus}</li>
 		</ul>
 	</div> -->
	<!--page-breadcrumb ends-->
 </div>

	<div class="row-fluid">

		<c:if test="${message != null}">
			<div class="errorblock alert alert-info">
				<p>${message}</p>
			</div>
		</c:if>
		<div class="span9">
			<h1><a name="skip"></a>
				<c:choose>
						<c:when test="${CA_STATE_CODE}">
							<c:if 	test="${(desigStatus == 'Pending'  )}">
								<spring:message code="label.assister.indIndividuals"/>
							</c:if>
							<c:if 	test="${(desigStatus == 'InActive'  )}">
							 	<spring:message code="label.assister.delegationRequests"/>
							</c:if>
						</c:when>
                        <c:when test="${NV_STATE_CODE}">
                            <c:if 	test="${(desigStatus == 'Pending')}">
                                <spring:message code="label.assister.indIndividuals"/> <small>${resultSize} ${desigStatus} ${tableTitle}</small>
                            </c:if>
                            <c:if 	test="${(desigStatus == 'InActive')}">
                                <spring:message code="label.assister.in-Active"/> <spring:message code="label.individuals"/> <small>${resultSize} ${desigStatus} ${tableTitle}</small>
                            </c:if>
                            <c:if 	test="${(desigStatus == 'Active')}">
                                <spring:message code="label.assister.active"/> <spring:message code="label.individuals"/> <small>${resultSize} ${desigStatus} ${tableTitle}</small>
                            </c:if>
                        </c:when>
						<c:otherwise>
								<spring:message code="label.assister.indIndividuals"/> <small>${resultSize}
										${desigStatus} ${tableTitle}</small> 
						</c:otherwise>
				</c:choose>
				
			</h1>
		</div>	
		<div class="pull-right gutter10">
			<div class="controls">
			  <c:if test="${(desigStatus != 'InActive')}">
				<div class="dropdown" id="dropdown">
<!-- 					<span id="counter" class="gray"></span> <span id="bulkActions"> -->
<!-- 						<a href="#" data-target="#" data-toggle="dropdown" role="button" id="inactiveLabel" -->
<!-- 							class="dropdown-toggle btn btn-primary btn-small">Bulk Action <i class="icon-gear icon-white"></i><i class="caret"></i> -->
<!-- 						</a> -->
<!-- 						<ul aria-labelledby="inactiveLabel" role="menu"	class="dropdown-menu pull-right"> -->
<!-- 							<li> -->
<%-- 								<c:if test="${(desigStatus == 'Pending')}"> --%>
<!-- 									<a class="" onclick="submitRequest('Active');">Accept</a> -->
<%-- 							    </c:if> --%>
<!-- 							</li> -->
<!-- 							<li><a id="declinelink" class="validateDecline"	onclick="markInactive();" href="#mark-inactive">Decline</a>   -->
<%-- 								<c:if test="${(desigStatus == 'Active')}"> --%>
<!-- 									<li> -->
<!-- 										<a id="declinelink" class="validateDecline"	onclick="markInactive();" href="#mark-inactive">Mark as	Inactive</a> -->
<!-- 									</li> -->
<%-- 								</c:if> --%>
<!-- 							</li> -->
<!-- 						</ul> -->
<!-- 					</span> -->
				</div>
				</c:if>
			</div>
		</div>
	</div>

<div class="row-fluid">
		<div id="sidebar" class="span3">
			<div class="header">
			<spring:message code="label.entity.refResuBy"/> <a class="pull-right" href="#" onclick="resetAll()"><spring:message code="label.entity.resetAll"/></a>
			</div>
			<div class="graybg">
				<form class="form-vertical gutter10" id="individualsearch"
					name="individualsearch" action="individuals" method="POST">
					<df:csrfToken/>
					<input type="hidden" name="totalCountAtClient" value="${resultSize}" />
					<input type="hidden" name="desigStatus" id="desigStatus" value="${desigStatus}" />
					<input type="hidden" name="fromModule" id="fromModule" value="individuals" />
                    <input type="hidden" name="status" id="status" value="" />
                    <input type="hidden" id="sortBy" name="sortBy" >
					<input type="hidden" id="sortOrder" name="sortOrder" >
					<input type="hidden" id="pageNumber" name="pageNumber" value="1">
					<input type="hidden" id="changeOrder" name="changeOrder" >
					<input type="hidden" id="previousSortBy" name="previousSortBy" value="${previousSortBy}">
					<input type="hidden" id="previousSortOrder" name="previousSortOrder" value="${previousSortOrder}">
						
					<div class="control-group">
						<label class="control-label" for="firstName"><spring:message code="label.assister.firstName"/></label>
						<div class="controls">
							<input class="span" type="text" id="firstName" name="firstName"
								value="${searchCriteria.firstName}" placeholder="" />
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="lastName"><spring:message code="label.assister.lastName"/></label>
						<div class="controls">
							<input class="span" type="text" id="lastName" name="lastName"
								value="${searchCriteria.lastName}" placeholder="" />
						</div>
					</div>

					<c:if
						test="${(desigStatus == 'Pending' || desigStatus == '' || desigStatus == null)}">
					<fieldset>
					<legend><spring:message code="label.assister.indReqSent"/></legend>
						<div class="box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="requestSentFrom"><spring:message code="label.assister.from"/>:</label>
								<%-- <div class="controls">
									<input type="text" id="fromDate" name="fromDate" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.fromDate}" />
									<div class="help-inline" id="fromDate_error"></div>
								</div> --%>
								
								<div class="input-append date date-picker" data-date="">
									<input class="span10" type="text" name="requestSentFrom" id="requestSentFrom" value="${searchCriteria.requestSentFrom}">
									<span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
									<div class="help-inline" id="requestSentFrom_error"></div>
								</div>
								<!-- end of controls-->
							</div>

							<div class="control-group">
								<label class="required control-label" for="requestSentTo"><spring:message code="label.assister.to"/>:</label>
								<%-- <div class="controls">
									<input type="text" id="toDate" name="toDate" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.toDate}" />
									<div class="help-inline" id="toDate_error"></div>
								</div> --%>
								
								<div class="input-append date date-picker" data-date="">
									<input class="span10" type="text" name="requestSentTo" id="requestSentTo" value="${searchCriteria.requestSentTo}">
									<span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
									<div class="help-inline" id="requestSentTo_error"></div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
						</fieldset>
					</c:if>

					<c:if test="${(desigStatus == 'InActive')}">
					<fieldset>
					<legend><spring:message code="label.assister.indInactiveSince"/></legend>
						<div class="box-tight margin0">
							<div class="control-group">
								<label class="required control-label" for="inactiveDateFrom"><spring:message code="label.assister.from"/>:</label>
								<div class="controls">
									<%-- <input type="text" id="fromDate" name="fromDate" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.fromDate}" />
									<div class="help-inline" id="fromDate_error"></div> --%>
									
									<div class="input-append date date-picker" data-date="">
										<input class="span10" type="text" name="inactiveDateFrom" id="inactiveDateFrom" value="${searchCriteria.inactiveDateFrom}">
										<span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
										<div class="help-inline" id="inactiveDateFrom_error"></div>
									</div>
								</div>
								<!-- end of controls-->
							</div>

							<div class="control-group">
								<label class="required control-label" for="inactiveDateTo"><spring:message code="label.assister.to"/>:</label>
								<div class="controls">
									<%-- <input type="text" id="toDate" name="toDate" class="datepick input-small" title="MM/dd/yyyy" value="${searchCriteria.toDate}" />
									<div class="help-inline" id="toDate_error"></div> --%>
									
									<div class="input-append date date-picker" data-date="">
										<input class="span10" type="text" name="inactiveDateTo" id="inactiveDateTo" value="${searchCriteria.inactiveDateTo}">
										<span class="add-on"><i title="choose date" class="icon-calendar"></i></span>
										<div class="help-inline" id="inactiveDateTo_error"></div>
									</div>
								</div>
								<!-- end of controls-->
							</div>
						</div>
						</fieldset>
					</c:if>
					<c:if test="${(desigStatus == 'Active')}">
					<fieldset>
						<legend>Eligibility Status</legend>
						<div class="box-tight margin0">
							<select size="1" id="eligibilityStatus" name="eligibilityStatus" class="span12">
								<option value="">Select</option>
								<c:forEach var="eligibilityStatus" items="${eligibilityStatuslist}">
									<option <c:if test="${eligibilityStatus == searchCriteria.eligibilityStatus}"> SELECTED </c:if> value="${eligibilityStatus}">${eligibilityStatus.description2}</option>																													
								</c:forEach>
							</select>
						</div>
					</fieldset>
					<fieldset>
						<legend>Application Status</legend>
						<div class="box-tight margin0">
							<select size="1" id="applicationStatus" name="applicationStatus" class="span12">
								<option value="">Select</option>
								<c:forEach var="applicationStatus" items="${applicationStatusList}">
									<option <c:if test="${applicationStatus == searchCriteria.applicationStatus}"> SELECTED </c:if> value="${applicationStatus}">${applicationStatus.description}</option>																													
								</c:forEach>
							</select>
						</div>
					</fieldset>
					</c:if> 
					<div class="gutter10">
						<input type="submit" name="submit" id="submit"
							class="btn input-small offset3" value="<spring:message code="label.assister.indGo"/>" title="Go"/>
					</div>
				</form>
			</div>


		</div>
		<!-- end of span3 -->

		<div id="rightpanel" class="span9">

			<form class="form-vertical" id="individuals"
				name="individuals" action="<c:url value="/entity/assister/individuals" />" method="POST">
				<input type="hidden" name="pIndividualFirstName" id="pIndividualFirstName" value="">
				<input type="hidden" name="pIndividualNameLastName" id="pIndividualLastName" value="">
						<input type="hidden" name="pStatus" id="pStatus" value="">
						<input type="hidden" name="pFromDate" id="pFromDate" value="">
						<input type="hidden" name="pToDate" id="pToDate" value="">
						<input type="hidden" id="pageNumber" name="pageNumber" value="">
						<input type="hidden" id="pDesigStatus" name="pDesigStatus" value="">
						<input type="hidden" name="prevStatus" id="prevStatus"value="${desigStatus}"> 
						<input type="hidden" name="ids"	id="ids" value="">
					    <input type="hidden" name="fromModule"	id="fromModule" value="individuals">

				<div id="brokerlist">

					<input type="hidden" name="id" id="id" value="">
					
					<c:set var="firstNameSorter" value="${sortOrder}" />
					<c:set var="familySizeSorter" value="${sortOrder}" />
					<c:set var="requestSentSorter" value="${sortOrder}" />
					<c:set var="eligibilityStatusSort" value="${sortOrder}" />
					<c:set var="applicationStatusSort" value="${sortOrder}" />
					<c:set var="inactiveSinceSort" value="${sortOrder}" />
					
					<c:choose>

						<c:when test="${fn:length(individualList) > 0 and desigStatus == 'Active'}">
								<table class="table" id="individualList">
									<thead>
										<tr class="header">
												<spring:message code="label.assister.name1" var="nameVal"/>
												<spring:message code="label.assiter.indFamilySize" var="familySizeVal"/>												
												<spring:message code="label.assister.indHouseholdIncome" var="houseHoldIncomeVal"/>
												<spring:message code="label.assister.indELIGIBILITYSTATUS" var="eligibilityStatusVal"/>
												<spring:message code="label.ApplicationStatus" var="applicationStatusVal"/>
												<th class="header" scope="col"><!-- <input type="checkbox" value="" id="check_all${desigStatus}" name="check_all${desigStatus}"> --></th>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${sortOrder}"></dl:sort></th>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${familySizeVal}" sortBy="FAMILY_SIZE" sortOrder="${sortOrder}"></dl:sort></th>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort title="${houseHoldIncomeVal}" sortBy="HOUSEHOLD_INCOME" sortOrder="${sortOrder}"></dl:sort></th>
												<th class="sortable" scope="col" style="width: 150px;"><dl:sort title="${eligibilityStatusVal}" sortBy="ELIGIBILITY_STATUS" sortOrder="${sortOrder}"></dl:sort></th>
												<th class="sortable" scope="col" style="width: 150px;"><dl:sort title="${applicationStatusVal}" sortBy="APPLICATION_STATUS" sortOrder="${sortOrder}"></dl:sort></th>												
												<th class="sortable" scope="col" style="width: 60px;"><spring:message code="label.assister.indACT"/></th>
											</tr>
									</thead>
											<c:forEach items="${individualList}" var="entry">
											 <c:set var="encrEntryId" ><encryptor:enc value="${entry.id}" isurl="true"/> </c:set>
											<tr>
												<td></td>
												 <td><a href="/hix/entity/assister/individualinfo/${encrEntryId}">${entry.firstName}&nbsp;${entry.lastName}</a></td>
												 <td>
												 	<c:choose >
														<c:when  test="${entry.familySize==0}">N/A</c:when>
														<c:when  test="${entry.familySize!=0}">${entry.familySize}</c:when>
													</c:choose>
												 </td>
												 <td>${entry.houseHoldIncome}</td>
												 <td>${entry.eligibilityStatus}</td>
												 <td>${entry.applicationStatus}</td>
												 <td class="txt-right">
												  <c:set var="encDesigId" ><encryptor:enc value="${entry.id}" isurl="true"/> </c:set>
													<div class="dropdown">
															<a href="/page.html" data-target="#" data-toggle="dropdown"	role="menubar" id="dLabel" class="dropdown-toggle">
																<i class="icon-gear"></i><b class="caret"></b>
															</a>
															<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-right txt-left">
																<li><a href="/hix/entity/assister/individualinfo/${encrEntryId}"><spring:message code="label.assister.indDetails"/></a></li>
																<li><a id="declinelink" class="validateDecline"	href="<c:url value="/entity/assister/declinePopup?desigId=${encDesigId}&prevStatus=${desigStatus}&fromModule=individuals"/>"><spring:message code="label.assister.indMarkAsInactive"/></a></li>
															</ul>
													</div>
												 </td>
											</tr>
											</c:forEach>
										</table>			
							</c:when>
							<c:when test="${fn:length(individualList) > 0 and (desigStatus == 'Pending' || desigStatus == 'InActive')}">
								<table class="table" id="individualList">
									<thead>
										<tr class="header">
											<spring:message code="label.assister.name1" var="nameVal"/>
											<spring:message code="label.assiter.indFamilySize" var="familySizeVal"/>
											<spring:message code="label.assister.indRequestSent" var="requestSentVal"/>	
											<spring:message code="label.brkinactivesince" var="inactiveSinceVal"/>
											<!--  <th class="header" scope="col"><input type="checkbox" value="" id="check_all${desigStatus}" name="check_all${desigStatus}"> </th> -->	
											<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${firstNameSorter}"></dl:sort></th>
										
											<c:choose>
												<c:when test="${desigStatus == 'Pending' || desigStatus == '' || desigStatus == null}">
													<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse"  title="${familySizeVal}" sortBy="FAMILY_SIZE" sortOrder="${familySizeSorter}"></dl:sort></th>
													<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse"  title="${requestSentVal}" sortBy="REQUESTSENT" sortOrder="${requestSentSorter}"></dl:sort></th>
													<th class="sortable" scope="col"><spring:message code="label.assister.indACT"/></th>	
												</c:when>
											<c:otherwise>
												<th class="sortable" scope="col" style="width: 175px;"><dl:sort customFunctionName="traverse" title="${inactiveSinceVal}" sortBy="INACTIVESINCE" sortOrder="${inactiveSinceSort}"></dl:sort></th>
											</c:otherwise>
											</c:choose>								
										</tr>
									</thead>
											<c:forEach items="${individualList}" var="entry" varStatus="idx">
											<c:set var="encryptedEntryId" ><encryptor:enc value="${entry.id}" isurl="true"/> </c:set>
												<tr>
													<c:choose>
														<c:when test="${desigStatus == 'InActive'}">
													 		<td>
													 			<c:choose>
																	<c:when test="${CA_STATE_CODE}">
																		<a id="ind${idx}" name="indlink" class="inddetail" href="<c:url value="/entity/assister/viewindividualinfo/${encryptedEntryId}?desigStatus=${desigStatus}&phoneNumber=${entry.phoneNumber}&emailAddress=${entry.emailAddress }&firstName=${entry.firstName}&lastName=${entry.lastName}"/>">${entry.firstName} ${entry.lastName}</a>
																	</c:when>
																	<c:otherwise>
																		<a id="ind${idx}" name="indlink" class="inddetail" href="<c:url value="/entity/assister/viewindividualinfo/${encryptedEntryId}?desigStatus=${desigStatus}"/>">${entry.firstName}&nbsp;${entry.lastName}</a>
																	</c:otherwise>
																</c:choose>
															</td>
														</c:when>
														<c:when test="${desigStatus == 'Pending'}">
															<td>	
																
																<c:choose>
																	<c:when test="${CA_STATE_CODE}">
																		<a id="ind${idx}" name="indlink" class="inddetail" href="<c:url value="/entity/assister/viewindividualinfo/${encryptedEntryId}?desigStatus=${desigStatus}&phoneNumber=${entry.phoneNumber}&emailAddress=${entry.emailAddress }&firstName=${entry.firstName}&lastName=${entry.lastName}"/>">${entry.firstName} ${entry.lastName}</a>
																	</c:when>
																	<c:otherwise>
																		${entry.firstName}&nbsp;${entry.lastName}
																	</c:otherwise>
																</c:choose>
															</td>
														</c:when>
														<c:otherwise>
															<td><a href="/hix/entity/assister/individualinfo/${encryptedEntryId}?desigStatus=${desigStatus}">${entry.firstName}&nbsp;${entry.lastName}</a></td>														
														</c:otherwise>
													</c:choose>
													 <c:choose>
													 	<c:when test="${desigStatus == 'Pending' || desigStatus == '' || desigStatus == null}">
													 		<td>
													 			<c:choose >
																	<c:when  test="${entry.familySize==0}">N/A</c:when>
																	<c:when  test="${entry.familySize!=0}">${entry.familySize}</c:when>
																</c:choose>
													 		</td>
													 		<td class="txt-left"><fmt:formatDate value="${entry.requestSent}" type="date" pattern="MM/dd/yyyy"/></td>	
													 	 	<td class="txt-right">
																<div class="dropdown">
																
																	<a href="/page.html" data-target="#" data-toggle="dropdown"	role="menubar" id="dLabel" class="dropdown-toggle">
																		<i class="icon-gear"></i><b class="caret"></b>
																	</a>
																	
																	<ul aria-labelledby="dLabel" role="menu" class="dropdown-menu pull-left txt-left">
																		<li><a href="<c:url value="/entity/assister/approve/${encryptedEntryId}?prevStatus=${desigStatus}"/>"><spring:message code="label.assister.indAccept"/></a></li>
																		<li><a id="declinelink" class="validateDecline"	href="<c:url value="/entity/assister/declinePopup?desigId=${encryptedEntryId}&prevStatus=${desigStatus}"/>"><spring:message code="label.assister.indDecline"/></a></li>
																	</ul>
																</div>
															</td>	
													 	</c:when>
													 	<c:otherwise>
													 		<td class="txt-left"><fmt:formatDate value="${entry.inActiveSince}" type="date" pattern="MM/dd/yyyy"/></td>	
													 	</c:otherwise>
													 </c:choose>
												</tr>
											</c:forEach>
								</table>							
							</c:when>
						</c:choose>	
						<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
							<c:if test="${fn:length(individualList) > 0 and (desigStatus == 'Pending' || desigStatus == 'Active')}">
								<div  style="text-align: left;font-size:9px">N/A - Not Available</div>
							</c:if>
						</c:if>
					
					<c:choose>
						<c:when	test="${fn:length(individualList) > 0 || resultSize > 0}">
							 <div class="center"  id="paginationContainer">
				 					<%-- <div class='pagination'>
										<dl:paginate resultSize="${individualList + 0}"
										pageSize="${pageSize + 0}" hideTotal="true" />
									</div> --%>
									<dl:paginate resultSize="${resultSize + 0}" pageSize="${pageSize + 0}" hideTotal="true"/>
							 </div>	
						</c:when>
						<c:otherwise>
							<c:choose>
								<c:when test="$(\"#status option:selected\").val() == 'Pending'">
									<div class="alert alert-info"><spring:message code="label.assister.indNoApprovalsNeeded"/></div>
								</c:when>
								<c:otherwise>
									<div class="alert alert-info"><spring:message code="label.assister.indNoMatchingRecordsFound"/></div>
								</c:otherwise>
							</c:choose>
						</c:otherwise>
					</c:choose>
				
				</div>
			</form>
		</div>
</div>
</div>
<!-- end of .row-fluid -->

<%-- Secure Inbox Start--%>
<%-- <jsp:include page="../../../inbox/includeInboxCompose.jsp"></jsp:include> --%>
<%-- Secure Inbox End--%>

<script type="text/javascript">

$(document).ready(function() {
	if(${CA_STATE_CODE}){
		$('.date-picker').datepicker({
		    autoclose: true,
		    format: 'mm/dd/yyyy'
		});
	}else{
		$('.date-picker').datepicker({
		    autoclose: true,
		    format: 'mm-dd-yyyy'
		});
	}
	
	 //Code to add onlick event for pagination links
    if(${CA_STATE_CODE} == true){
    	
        
        $('#paginationContainer').find('a').click(function(e) {
        	//prevent Default functionality
            e.preventDefault();
        	
            var individualIdArray = new Array();
            <c:forEach items="${individualsId}" var="oneId" varStatus="status"> 
            	individualIdArray.push(${oneId});
        	</c:forEach>
        	
            var newForm = document.createElement("form");
			var existURL = $(this).attr('href');
			
			if (existURL.indexOf('individualsIdAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('individualsIdAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('individualsIdAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			
			if (existURL.indexOf('totalCountAtClient' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('totalCountAtClient'));
		        var suffix = existURL.substring(existURL.indexOf('totalCountAtClient'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
			
			if (existURL.indexOf('landingFromPrev' + "=") >= 0){
		        var prefix = existURL.substring(0, existURL.indexOf('landingFromPrev'));
		        var suffix = existURL.substring(existURL.indexOf('landingFromPrev'));
		        suffix = suffix.substring(suffix.indexOf("=") + 1);
		        suffix = (suffix.indexOf("&") >= 0) ? suffix.substring(suffix.indexOf("&")+1) : "";
		        existURL = prefix + suffix;
		    }
            
            $(newForm).attr("action", existURL)
                   .attr("method", "post");
            
            $(individualIdArray).each(function( index ) {
            	 $(newForm).append('<input type="hidden" name="individualsIdAtClient" value="' + individualIdArray[index] + '" />');
            });
            $(newForm).append('<input type="hidden" name="totalCountAtClient" value="' + ${resultSize}  + '" />');
            
            $(newForm).append('<input type="hidden" name="csrftoken" value="' + $("#csrftoken").attr('value') + '" />');
            
          //If Landing from Prev click
            var textOfPageLink =  $(this).text();
            textOfPageLink = textOfPageLink.trim();
            var textOfPageLinkArray = textOfPageLink.split( /\s+/);
            var isnum = /^\d+$/.test(textOfPageLinkArray[textOfPageLinkArray.length-1]);
            if(!isnum){
          	  var localTempPageNumStr = getParameterByName('pageNumber',existURL);
          	  isnum = /^\d+$/.test(localTempPageNumStr);
          	  if(isnum && ((parseInt(localTempPageNumStr)%10)==0) ){
          		  $(newForm).append('<input type="hidden" name="landingFromPrev" value="true" />');
          	  }
            }
            
            
            document.body.appendChild(newForm);
            $(newForm).submit();
            document.body.removeChild(newForm);
        	return false;
    	});
    }
	
	var desigStatus = '${desigStatus}';
	if(desigStatus =="Active"){
		$('table[name="indTable${desigStatus}"]').tablesorter({
			sortList : [ [ 1, 0 ]]
		});
	}
	else{
		$('table[name="indTable${desigStatus}"]').tablesorter({
			sortList : [ [ 3, 0 ]]
		});	
	}
	
 
	


var validator = $("#individualsearch")
.validate(
		{
			onkeyup : false,
			onclick : false,
			rules : {
				requestSentFrom : {
					chkDateFormat : true, chkDateFromAndTo:true
				},
				requestSentTo : {
					chkDateFormat : true, chkDateFromAndTo:true
				},
				inactiveDateFrom : {
					chkDateFormat : true, chkDateFromAndTo:true
				},
				inactiveDateTo : {
					chkDateFormat : true, chkDateFromAndTo:true
				}
/* 				Status :{
					commaSeperated :true
				} */
			},
			messages : {
				requestSentFrom : {
					chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
					chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
				},
				requestSentTo : {
					chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
					chkDateFromAndTo:"<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
				},
				inactiveDateFrom : {
					chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
					chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
				},
				inactiveDateTo : {
					chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='label.validateDate' javaScriptEscape='true'/></span>",
					chkDateFromAndTo: "<span> <em class='excl'>!</em><spring:message code='label.validatePleaseEnterTodategreaterthanfromDate'/></span>"
				}
/* 		       Status : {
					commaSeperated : "<span> <em class='excl'>!</em><spring:message code='label.validateEnterValidDate'/></span>",
					
				} */
			}
		});

jQuery.validator.addMethod("chkDateFromAndTo", function(value, element, param) {
 		var requestSentFrom = $('#requestSentFrom').val()
		var requestSentTo = $('#requestSentTo').val()
		
		var inactiveDateFrom = $('#inactiveDateFrom').val()
		var inactiveDateTo = $('#inactiveDateTo').val()
		var fromDate;
		var toDate;
		
		if(requestSentFrom!=null && requestSentFrom!=''){
			fromDate = requestSentFrom;
		}
		if(requestSentTo!=null && requestSentTo!=''){
			toDate = requestSentTo;
		}
		if(inactiveDateFrom!=null && inactiveDateFrom!=''){
			fromDate = inactiveDateFrom;
		}
		if(inactiveDateTo!=null && inactiveDateTo!=''){
			toDate = inactiveDateTo;
		}

		if((fromDate != null) &&(toDate !=null)){
			if((new Date(fromDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))  > (new Date(toDate.replace( /(\d{2})-(\d{2})-(\d{4})/, "$1/$2/$3")))){
	
				return false;
			}  
		}

		return true;
});

jQuery.validator.addMethod("chkDateFormat",
function(value, element, param) {
if(value!=""){

	var isValid = false;
	var reg = /^\d{1,2}\-\d{1,2}\-\d{4}$/;
	if(${CA_STATE_CODE}){
		reg = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
	} 
	if (reg.test(value)) {
		var splittedDate = value.split('-');
		if(${CA_STATE_CODE}){
			splittedDate = value.split('/');
		}
		var mm = parseInt(splittedDate[0], 10);
		var dd = parseInt(splittedDate[1], 10);
		var yyyy = parseInt(splittedDate[2], 10);
		
		var newDate = new Date(yyyy, mm - 1, dd);
		if ((newDate.getFullYear() == yyyy)
				&& (newDate.getMonth() == mm - 1)
				&& (newDate.getDate() == dd))
			isValid = true;
		else
			isValid = false;
	} else{
		isValid = false;
	}

	return isValid;
}
return true;

});

jQuery.validator.addMethod("commaSeperated",
	function(value, element, param) {
	var slvals=[];
	if ($('#Eligibility').is(":checked")){
	slvals.push($('#Eligibility').val());
	}
	if ($('#PlanSelection').is(":checked")){
	slvals.push($('#PlanSelection').val());
	}
	if ($('#OpenEnrollment').is(":checked")){
	slvals.push($('#OpenEnrollment').val());
	}
	
	
	$('#status').val(slvals);
return true;
});

	$(function() {
		$(".inddetail")
				.click(
						function(e) {
							e.preventDefault();
							var href = $(this).attr('href');
							var individualName = $(this).html();
							if(individualName){
								individualName = individualName.toUpperCase() ;
							}
							if (href.indexOf('#') != 0) {
								$(
										'<div id="inddetail" class="modal"><div class="modal-header" style="border-bottom:0;"><h4 style="text-transform:capitalize;font-weight:bold;margin:0;float:left;">' 
												+ individualName   
												+ '</h4><button type="button" title="x" class="close" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body" style="padding:10 10px"><iframe src="'
												+ href
												+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:224px;"></iframe><button class="btn offset2" data-dismiss="modal" aria-hidden="true">Cancel</button></div></div>') 
										.modal();
							}
						});
	});
	function validate(todo) {
		var confirmMessage = "Are you sure to " + todo + " ?";

		if (confirm(confirmMessage) == false) {
			return false;
		}
	};

	function closeMe(desigId, desigStatus, desigName) {
		$('#inddetail').remove();
		if (desigStatus == '') {
			desigStatus = 'Pending';
		}

		var href = "/entity/assister/declinePopup?desigId=" + desigId
				+ "&prevStatus=" + desigStatus + "&fromModule=individuals";
		$(
				'<div id="declinepopup" class="modal"><div class="modal-header"><h3><spring:message code="label.indAreYouSure"/></h3></div><div class="modal-body"><iframe id="declinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:240px;"></iframe></div></div>')
				.modal();
	}

	function closeIFrame() {
		$("#declinepopup").remove();
	}
	
	function getParameterByName(name, url) {
		if (!url) '10';
		name = name.replace(/[\[\]]/g, "\\$&");
		var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
		 results = regex.exec(url);
		if (!results) return null;
		if (!results[2]) return '';
		 return decodeURIComponent(results[2].replace(/\+/g, " "));
	}

	$(function() {
		$(".validateDecline")
				.click(
						function(e) {
							e.preventDefault();
							var href = $(this).attr('href');
							if (href.indexOf('#') != 0) {
								$(
										'<div id="declinepopup" class="modal"><div class="modal-header"><button type="button" title="" class="close" data-dismiss="modal" aria-hidden="true">&times;</button><h3><spring:message code="label.areusure"/></h3></div><div class="modal-body"><iframe id="declinepopup" src="'
												+ href
												+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:195px;"></iframe></div></div>')
										.modal();
							}
						});
	});

	//If the Master checkbox is checked or unchchecked, process the following code
	$('input[name="check_all${desigStatus}"]').click(function() {
		if ($('input[name="check_all${desigStatus}"]').is(":checked")) {
			$('input[name="indCheckbox${desigStatus}"]').each(function() {
				$(this).attr("checked", true);
			});
		} else {
			$('input[name="indCheckbox${desigStatus}"]').each(function() {
				$(this).attr("checked", false);
			});
		}
	});
	//For each of the ind checkboxes, process the following code when they are changed
	$('input[name="indCheckbox${desigStatus}"]').bind(
			'change',
			function() {
				if ($('input[name="indCheckbox${desigStatus}"]').filter(
						':not(:checked)').length == 0) {
					$('input[name="check_all${desigStatus}"]').attr("checked",
							true);
				} else {
					$('input[name="check_all${desigStatus}"]').attr("checked",
							false);
				}
			});

	function countChecked() {
		var n = $("input[name='indCheckbox${desigStatus}']:checked").length;
		var itemsSelected = "<small>&#40; " + n
				+ (n === 1 ? " Item" : " Items")
				+ " Selected &#41;&nbsp;</small>";
		//$(this).find(".counter").text(itemsSelected);
		$("#counter").html(itemsSelected);
		if (n > 0) {
			$("#counter").show();
			$("#bulkActions").show();
		} else {
			$("#counter").show();
			$("#bulkActions").hide();
		}
	}
	countChecked();
	$(":checkbox").click(countChecked);

	function markInactive() {
		var ids = '';
		$.each($("input[name='indCheckbox${desigStatus}']:checked"),
				function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
		ids += ',';

		var desigStatus = '${param.desigStatus}';
		var href = '/hix/entity/assister/declinePopup?desigId=' + ids + '&prevStatus='
				+ desigStatus + '&fromModule=individuals';
		$(
				'<div id="declinepopup" class="modal"><div class="modal-body"><iframe id="bulkdeclinepopup" src="'
						+ href
						+ '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:170px;"></iframe></div></div>')
				.modal();
	}

	function submitRequest(requestType) {
		var ids = '';
		$.each($("input[name='indCheckbox${desigStatus}']:checked"),
				function() {
					ids += (ids ? ',' : '') + $(this).attr('value');
				});
		ids += ',';

		var prevStatus = "${desigStatus}";
		var validUrl = "";
		if (requestType == 'InActive') {
			validUrl = "declineRequests";
		} else if (requestType == 'Active') {
			validUrl = "approveRequests";
		}
		$.ajax({
			url : validUrl,
			data : {
				ids : ids,
				desigStatus : prevStatus
			},
			success : function(data) {
				window.location.reload();
			}
		});
	}
});

function resetAll() {
	 var  contextPath =  "<%=request.getContextPath()%>";
	    var documentUrl = contextPath + "/entity/assister/resetall?desigStatus=${desigStatus}";
	    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}

function traverse(url){
	var queryString = {};
	url.replace(
		    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
		    function($0, $1, $2, $3) { queryString[$1] = $3; }
		);
	 if( queryString["sortBy"] ) $("#sortBy").val(queryString["sortBy"]);
	 if( queryString["sortOrder"] ) $("#sortOrder").val(queryString["sortOrder"]);
	 if( queryString["pageNumber"] ) $("#pageNumber").val(queryString["pageNumber"]);
	 if( queryString["changeOrder"] ) $("#changeOrder").val(queryString["changeOrder"]);
	$("#individualsearch input[type=submit]").click();
}
</script>
