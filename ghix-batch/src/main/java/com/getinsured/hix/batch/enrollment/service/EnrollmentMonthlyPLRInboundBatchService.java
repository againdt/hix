/**
 * 
 */
package com.getinsured.hix.batch.enrollment.service;

/**
 * PLR Inbound Processing Service
 * @author negi_s
 * @since 02/02/2017
 *
 */
public interface EnrollmentMonthlyPLRInboundBatchService {
	
	/**
	 * Process PLR inbound files received and log the data into the ENRL_PLR_IN table
	 * @param jobId Batch job ID
	 */
	void processPlrInFile(Long jobId);

}
