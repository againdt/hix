package com.getinsured.hix.batch.enrollment.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.dto.enrollment.SendUpdatedEnrolleeResponseDTO;
import com.getinsured.hix.enrollment.repository.IEnrolleeUpdateSendStatusRepository;
import com.getinsured.hix.enrollment.service.EnrolleeAuditService;
import com.getinsured.hix.enrollment.service.EnrolleeService;
import com.getinsured.hix.enrollment.service.EnrollmentBatchService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.enrollment.Enrollee;
import com.getinsured.hix.model.enrollment.EnrolleeAud;
import com.getinsured.hix.model.enrollment.EnrolleeUpdateSendStatus;
import com.getinsured.hix.model.enrollment.Enrollment;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIException;
import com.thoughtworks.xstream.XStream;

@Service("ind21Service")
@Transactional
public class Ind21ServiceImpl implements Ind21Service {

	@Autowired
	private JobService jobService;
	
	@Autowired 
	private UserService userService;
	
	@Autowired 
	private IEnrolleeUpdateSendStatusRepository enrolleeUpdateSendStatusRepository;
	
	@Autowired
	private EnrollmentBatchService enrollmentBatchService;
	
	@Autowired 
	private EnrolleeAuditService enrolleeAuditService;
	
	@Autowired 
	private EnrolleeService enrolleeService;
	
	@Autowired 
	private GhixRestTemplate restTemplate;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(Ind21ServiceImpl.class);
	
	/**
	 * @author panda_p
	 * @since 29-Mar-2013
	 * 
	 * This Will called thru batch and used to send the carrier updated data to AHBX  
	 * 
	 * 
	 */
	@Override	
	@Transactional(readOnly=true)
	public Map<Integer, String> sendCarrierUpdatedDataToAHBX(long jobExecutionId) throws Exception{
		LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : START");
		
		Map<Integer, String> errorMap= new HashMap<Integer, String>();
		Map<String,Object> reqParam = new HashMap<String,Object>();
		EnrolleeUpdateSendStatus enrolleeUpdateSendStatus =null;
		int count=0;
		
		Date lastRunDate=enrollmentBatchService.getJOBLastRunDate(EnrollmentConstants.SEND_CARRIER_UPDATED_ENROLLMENT_JOB);
		AccountUser user = userService.findByUserName(GhixConstants.USER_NAME_CARRIER);
		
		List<Integer> enrolleeIds = enrolleeAuditService.getEnrolleeIdsForCarrierUpdate(lastRunDate, user);

		if (enrolleeIds != null && !enrolleeIds.isEmpty()) {
			for (Integer enrolleeID : enrolleeIds) {
				
				String batchJobStatus=null;
				if(jobService != null && jobExecutionId != -1){
					batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
				}
				if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
					throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
				}
				try{
					//getting Enrollee list updated by carrier for specific issuer
					List<EnrolleeAud> enrolleeList= enrolleeAuditService.getEnrolleeAudForCarriersUpdatedEnrollments(lastRunDate, user, enrolleeID);
					
					if(enrolleeList!=null && enrolleeList.size()>0){
						List<Map<String,Object>> planList =null;
						//LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : enrolleeList.size = "+enrolleeList.size());
						
						for(EnrolleeAud enrolleeAud : enrolleeList){
								planList= new ArrayList<Map<String,Object>>();
								Map<String,Object> planMap = new HashMap<String,Object>();
								enrolleeUpdateSendStatus = new EnrolleeUpdateSendStatus();
								if(enrolleeAud.getEnrollment()!=null){
									Enrollment enrollment= enrolleeAud.getEnrollment();
									planMap.put(EnrollmentConstants.CASE_ID, enrollment.getHouseHoldCaseId());
									planMap.put(EnrollmentConstants.ENROLLMENT_ID, enrollment.getId());
									planMap.put(EnrollmentConstants.PLAN_ID, enrollment.getPlanId());
									planMap.put(EnrollmentConstants.CMSPLAN_ID, enrollment.getCMSPlanID());
								}else{
									LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : Enrollment not found for the enrolle updatedby carrier");
									throw new GIException("Enrollment not found for the enrolle updatedby carrier");
								}

								planMap.put(EnrollmentConstants.ENROLLEE_ID, enrolleeAud.getId());
								planMap.put(EnrollmentConstants.POLICY_ID, enrolleeAud.getHealthCoveragePolicyNo());
								
								if(enrolleeAud.getEffectiveStartDate()!=null){
									planMap.put(EnrollmentConstants.EFFECTIVE_START_DATE, DateUtil.dateToString(enrolleeAud.getEffectiveStartDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
								}else{
									planMap.put(EnrollmentConstants.EFFECTIVE_START_DATE,"");
								}
								if(enrolleeAud.getEnrolleeLkpValue()!=null 
										&& (enrolleeAud.getEnrolleeLkpValue().getLookupValueCode()!=null 
										&& (enrolleeAud.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_CANCEL) 
												|| enrolleeAud.getEnrolleeLkpValue().getLookupValueCode().equalsIgnoreCase(EnrollmentConstants.ENROLLMENT_STATUS_TERM)))){
									if(enrolleeAud.getEffectiveEndDate()!=null){
										planMap.put(EnrollmentConstants.EFFECTIVE_END_DATE, DateUtil.dateToString(enrolleeAud.getEffectiveEndDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
									}else{
										planMap.put(EnrollmentConstants.EFFECTIVE_END_DATE,"");
									}
								}
								planMap.put(EnrollmentConstants.STATUS, enrolleeAud.getEnrolleeLkpValue().getLookupValueCode());
								
								planMap.put(EnrollmentConstants.MEMBER_ID, enrolleeAud.getExchgIndivIdentifier());
								
								if(enrolleeAud.getLastPremiumPaidDate()!=null){
									planMap.put(EnrollmentConstants.LAST_PREMIUM_PAID_DATE, DateUtil.dateToString(enrolleeAud.getLastPremiumPaidDate(), GhixConstants.REQUIRED_DATE_FORMAT));	
								}else{
									planMap.put(EnrollmentConstants.LAST_PREMIUM_PAID_DATE,"");
								}
								
								if(enrolleeAud.getEnrollment() != null && enrolleeAud.getEnrollment().getEnrollmentConfirmationDate() != null){
									planMap.put(EnrollmentConstants.ENROLLMENT_CONFIRMATION_DATE, DateUtil.dateToString(enrolleeAud.getEnrollment().getEnrollmentConfirmationDate(), GhixConstants.REQUIRED_DATE_FORMAT));
								}
								else{
									planMap.put(EnrollmentConstants.ENROLLMENT_CONFIRMATION_DATE, "");
								}
								
								if(enrolleeAud.getLastEventId()!=null && 
										(enrolleeAud.getLastEventId().getEventTypeLkp()!=null 
										&& EnrollmentConstants.EVENT_TYPE_CANCELLATION.equalsIgnoreCase(enrolleeAud.getLastEventId().getEventTypeLkp().getLookupValueCode()) )){
									
									if(enrolleeAud.getLastEventId()!=null && enrolleeAud.getLastEventId().getEventReasonLkp()!=null){
										planMap.put("maintenanceReasonCode", enrolleeAud.getLastEventId().getEventReasonLkp().getLookupValueCode());
									}
								}
								
								planList.add(planMap);
								
								reqParam.put("planList", planList);
								EnrollmentResponse enrollmentResponse=null;
								String verifyResponse=null;
								LOGGER.info(" sendCarrierUpdatedDataToAHBX SERVICE : Calling IND21 ");
								enrolleeUpdateSendStatus.setEnrolleeStatus(enrolleeAud.getEnrolleeLkpValue());
								enrolleeUpdateSendStatus.setExchgIndivIdentifier(enrolleeAud.getExchgIndivIdentifier());
								Enrollee en = new Enrollee();
								en.setId(enrolleeAud.getId());
								enrolleeUpdateSendStatus.setEnrollee(en);
								enrolleeUpdateSendStatus.setAhbxStatusCode("GI01");
								enrolleeUpdateSendStatus.setAhbxStatusDesc("Sending Request to AHBX For Revision Number: "+enrolleeAud.getRev());
								try{
									LOGGER.info(count+". sendCarrierUpdatedDataToAHBX SERVICE : Sending for member = " +enrolleeAud.getExchgIndivIdentifier()+" Revision Number: "+enrolleeAud.getRev());
									enrolleeUpdateSendStatus=enrolleeUpdateSendStatusRepository.save(enrolleeUpdateSendStatus);
									verifyResponse = restTemplate.postForObject(GhixEndPoints.AHBXEndPoints.ENROLLMENT_IND21_CALL_URL, reqParam, String.class);
									XStream xStream = GhixUtils.getXStreamStaxObject();
									if(verifyResponse!=null){
										enrollmentResponse = (EnrollmentResponse) xStream.fromXML(verifyResponse);
									}
								}catch(Exception e){
									LOGGER.error(" sendCarrierUpdatedDataToAHBX SERVICE : Failed for Enrollee :: " +enrolleeID +" :: verifyResponse from IND21 = "+verifyResponse);
								}
								LOGGER.info(count+". sendCarrierUpdatedDataToAHBX SERVICE : Sending Completed for member = " +enrolleeAud.getExchgIndivIdentifier());
								count++;
								if(enrollmentResponse!=null && (enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList() !=null && enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList().size()>0)){
									List<SendUpdatedEnrolleeResponseDTO> sendUpdatedEnrolleeResponseDTOList =enrollmentResponse.getSendUpdatedEnrolleeResponseDTOList();
									enrolleeService.saveCarrierUpdatedDataResponse(sendUpdatedEnrolleeResponseDTOList, enrolleeUpdateSendStatus);
								}
								}
					}
				
				}
				catch(Exception ex){
					LOGGER.error("sendCarrierUpdatedDataToAHBX SERVICE : Failed for Enrollee :: " +enrolleeID +" Exception: ", ex);
					errorMap.put(enrolleeID, ex.getMessage() != null ? ex.getMessage() : EnrollmentUtils.shortenedStackTrace(ex, 3));
				}
			}//For Loop Ends here
			
		}
		LOGGER.info("sendCarrierUpdatedDataToAHBX SERVICE : END");	
		return errorMap;
	}
	

}
