
package com.getinsured.iex.hub.ssac.niem.niem_core._2_0;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;
import com.getinsured.iex.hub.ssac.niem.structures._2_0.ComplexObjectType;


/**
 * A data type for a human being.
 * 
 * <p>Java class for PersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/structures/2.0}ComplexObjectType">
 *       &lt;sequence>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonBirthDate"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonName"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonSSNIdentification"/>
 *         &lt;element ref="{http://niem.gov/niem/niem-core/2.0}PersonUSCitizenIndicator"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonType", propOrder = {
    "personBirthDate",
    "personName",
    "personSSNIdentification",
    "personUSCitizenIndicator"
})
public class PersonType
    extends ComplexObjectType
{

    @XmlElement(name = "PersonBirthDate", required = true, nillable = true)
    protected DateType personBirthDate;
    @XmlElement(name = "PersonName", required = true, nillable = true)
    protected PersonNameType personName;
    @XmlElement(name = "PersonSSNIdentification", required = true, nillable = true)
    protected IdentificationType personSSNIdentification;
    @XmlElement(name = "PersonUSCitizenIndicator", required = true, nillable = true)
    protected Boolean personUSCitizenIndicator;

    /**
     * Gets the value of the personBirthDate property.
     * 
     * @return
     *     possible object is
     *     {@link DateType }
     *     
     */
    public DateType getPersonBirthDate() {
        return personBirthDate;
    }

    /**
     * Sets the value of the personBirthDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateType }
     *     
     */
    public void setPersonBirthDate(DateType value) {
        this.personBirthDate = value;
    }

    /**
     * Gets the value of the personName property.
     * 
     * @return
     *     possible object is
     *     {@link PersonNameType }
     *     
     */
    public PersonNameType getPersonName() {
        return personName;
    }

    /**
     * Sets the value of the personName property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonNameType }
     *     
     */
    public void setPersonName(PersonNameType value) {
        this.personName = value;
    }

    /**
     * Gets the value of the personSSNIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link IdentificationType }
     *     
     */
    public IdentificationType getPersonSSNIdentification() {
        return personSSNIdentification;
    }

    /**
     * Sets the value of the personSSNIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link IdentificationType }
     *     
     */
    public void setPersonSSNIdentification(IdentificationType value) {
        this.personSSNIdentification = value;
    }

    /**
     * Gets the value of the personUSCitizenIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean getPersonUSCitizenIndicator() {
        return personUSCitizenIndicator;
    }

    /**
     * Sets the value of the personUSCitizenIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPersonUSCitizenIndicator(Boolean value) {
        this.personUSCitizenIndicator = value;
    }

}
