<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery-ui-1.8.20.datepicker.min.js" />"></script>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${plan.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
    	<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message code="label.plans"/></a></li>
            <li><a href="<c:url value="/admin/planmgmt/manageqdpplans"/>"><spring:message code="label.linkManageQDP"/></a></li>
            <li><spring:message  code='label.linkDentalPlanDetails'/></li>
        </ul><!--page-breadcrumb ends-->
	<h1>${plan.name}</h1>
	</div>
		<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message  code='label.aboutPlan'/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}" />"><spring:message  code='label.linkDentalPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpbenefits/${encPlanId}"/>"><spring:message  code='label.linkDentalPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdprates/${encPlanId}" />"><spring:message  code='label.linkDentalPlanRates'/></a></li>
                    <c:if test="${displayProviderNetwork == 'YES'}">
                    	<li><a href="<c:url value="/admin/planmgmt/viewqdpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkDentalProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdpcertification/${encPlanId}" />"><spring:message  code='label.linkCertificationStatus'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqdphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>

				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message  code='label.actions'/></h4>
					<ul class="nav nav-list graybg">
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqdpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
        </div><!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			<div class="header">
				<h4 class="pull-left"><spring:message  code='label.linkEditPlanDetails'/></h4>
				<a class="btn btn-small" title="<spring:message  code='label.btnCancel'/>" href="<c:url value="/admin/planmgmt/viewqdpdetail/${encPlanId}"></c:url>"><spring:message  code='label.btnCancel'/></a> 
				<a class="btn btn-primary btn-small" title="<spring:message  code='label.btnSave'/>" href="#" onclick="isExistingPlan();"><spring:message  code='label.btnSave'/></a>
			</div>
			<form class="form-horizontal" id="frmqdpEdit" name="frmqdpEdit" action="<c:url value="/admin/planmgmt/editqdpdetail" />" method="POST">
				<df:csrfToken/>
				<table class="table table-border-none">
					
					<tr>
						<td class="txt-right span3"><label for="issuer"><spring:message  code='label.issuerName'/></label></td>
						<td><div id="issuername" class="input">
								
							</div><div id="issuername_error" class="help-inline"></div></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.planName'/></td>
						<td><strong>${plan.name}</strong></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_number"><spring:message  code='label.planNumber'/></label></td>
						<td><strong>${plan.issuerPlanNumber}</strong>
						<input type="hidden" id="issuerPlanNumber" name="issuerPlanNumber" value="<encryptor:enc value="${plan.issuerPlanNumber}"></encryptor:enc>"/>
						
						<div id="issuerPlanNumber_error" class="help-inline"></div></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_market"><spring:message  code='label.planMarket'/></label></td>
						<td>
							<select class="input-medium" name="market" id="market">
								<c:forEach var="qdpmarket" items="${qdpmarketList}">
									<option<c:if test="${plan.market==qdpmarket.key}"><spring:message code='select.option'/></c:if> value="${qdpmarket.key}">${qdpmarket.value}</option>
								</c:forEach>
							</select>
						</td>
					</tr>
					<c:if test="${displayPlanLevel == 'YES'}"><tr>
						<td class="txt-right"><spring:message  code='label.planLevel'/></td>
						<td><select class="input-medium" name="planDental.planLevel" id="planDentalplanLevel" disabled="disabled">
								<option value=""><spring:message  code='label.choose'/></option>
								<c:forEach var="qdplevel" items="${qdplevelList}">
									<option <c:if test="${plan.planDental.planLevel==qdplevel.key}"><spring:message code='select.option'/></c:if> value="${qdplevel.key}">${qdplevel.value}</option>						    		
								</c:forEach>
								
							</select>
							<input type="hidden" id="planLevelReadOnly" name="planLevelReadOnly" value="${plan.planDental.planLevel}" /></td> 
					</tr></c:if>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
					<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>
					<tr>
						<td class="txt-right"><label for="plan_start"><spring:message code='label.planStartDate'/></label></td>
						<td><input type="text" title="MM/dd/yyyy" class="datepick input-small" name="startDate" id="startDate" readonly="true" value="<c:out value="${stDate}"/>" /></td>
					</tr>
					<tr>
						<td class="txt-right"><label for="plan_end"><spring:message  code='label.planEndDate'/></label></td>
						<td><input type="text" title="MM/dd/yyyy" class="datepick input-small" name="endDate" id="endDate" readonly="true" value="<c:out value="${enDate}"/>"/><div id="endDate_error" class="help-inline"></div></td>
						
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code='label.linkCertificationStatus'/></td>
						<td><strong>${plan.status}</strong> <a href="<c:url value="/admin/planmgmt/editqdpcertification/${encPlanId}" />"><spring:message code='button.label'/></a></td>
					</tr>
					<tr>
						<td class="txt-right"><spring:message  code="label.enrollAvail"/></td>
						<td><strong>${enrollAvailability}</strong> <a href="<c:url value="/admin/planmgmt/editqdpenrollmentavail/${encPlanId}" />"><spring:message code='button.label'/></a><input type="hidden" name="planDental.acturialValueCertificate" id="planDentalacturialValueCertificate" value="${planDentalacturialValueCertificate}" /></td>
					</tr><tr><td>
				<input type="hidden" id="rateFile" name="rateFile"/>
				<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
				<input type="hidden" id="name" name="name" value="<encryptor:enc value="${plan.name}"></encryptor:enc>"/>
				<input type="hidden" id="maxEnrolee" name="maxEnrolee" value="<encryptor:enc value="${plan.maxEnrolee}"></encryptor:enc>"/>
				<input type="hidden" id="isEdit" name="isEdit" value="${isEdit}"/></td><td></td>
				
				
					</tr>
				</table>
			</form>
			
		</div><!-- end of .span9 -->
	</div><!--  end of .row-fluid -->
</div><!--  end of .gutter -->

<script type="text/javascript">
$(document).ready(function(){
	$('.complete').each(function(){
		var completeStep = $(this).html();
		var replaceExpr = /html"\>/gi;
		$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
	});
	$('.datepick').each(function() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/calendar.gif';
		$(this).datepicker({
			showOn : "button",
			buttonImage : imgpath,
			buttonImageOnly : true
		});
	});
	
	getIssuerNames();
	
	var accreditationUpload = { 
	        //target:        '#certiName',   // target element(s) to be updated with server response 
	        beforeSubmit:  submitAccreditationRequest,  // pre-submit callback 
	        success:       submitAccreditationResponse  // post-submit callback 
	    };  
    $('#frmCerti').ajaxForm(accreditationUpload);
    if(('${plan.id}' >0) && '${plan.planDental.planLevel}' == 'Silver'  )
        {
    		$("#planDentalplanLevel").val('${plan.planDental.planLevel}'); 
   			$("#planDentalplanLevel").attr("disabled", true);
        }
});

function submitAccreditationResponse(responseText, statusText, xhr, $form)  {
	var val = responseText.split(",");
	if(val[0] != 'Error')
	{	
		$("#certificatesFileName").text(val[1]);
		$("#planDentalacturialValueCertificate").val(val[2]);
		$("#planDentalacturialValueCertificate_error").val("");
	}
	else
	{
		$("#planDentalacturialValueCertificate_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='label.fileSizeError'/></span></label>");
	}
}

function submitAccreditationRequest(formData, jqForm, options) { 
	var fileName = $('#certificates').val();
	if (fileName.length > 0)
	{
	    return true;	
	} 
	return false;
}


	function getIssuerNames() {
		var ctx = "${pageContext.request.contextPath}";
		var imgpath = ctx+'/resources/images/loader.gif';
		$("#issuername").html("<div class=\"issuerloader\"><img src=\"" + imgpath + "\" alt='Issuer Name'></div>");
		$.ajax({
			  type: 'GET',
			  url: "../getissuers",
			  success: function(data) {
			 	$("#issuername").html("");
				$("#issuername").html(data);
				$("#issuerId").val('${plan.issuer.id}');
				
				var el = document.getElementById("issuerId");
				for(var i=0; i<el.options.length; i++) {
				  if ( el.options[i].value == '${plan.issuer.id}' ) {
				    el.selectedIndex = i;
				    break;
				  }
				}
	           }
		});
}
</script>

<script type="text/javascript">

$.validator.addMethod("endDate", function(value, element) {
var startdatevalue = $('#startDate').val();
if (value == null || value.length <1)
	return true;
return Date.parse(startdatevalue) < Date.parse(value);
});
$.validator.addMethod("numericRegex", function(value, element) {
    return this.optional(element) || /^[0-9]+$/i.test(value);
});

$.validator.addMethod("planLevel", function(value, element) {
	return !(value == null || value.length <1);
});

$.validator.addMethod("maxlength", function (value, element, len) {
	   return value == "" || value.length <= len;
	});

var validator = $("#frmqdpEdit").validate({ 
	 ignore: "",
	rules : {issuername : { required : true},
		issuerPlanNumber : { required : true,
			maxlength : '30'},
		startDate : { required : true},
		endDate : { endDate : true},
		maxEnrolee : {	numericRegex : true},
		"planDental.planLevel" : {planLevel : true},
		"planDental.acturialValueCertificate" : { required : true}
	}, 
	messages : {
		issuername : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.issuerName'/></span>"},
		name : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planName'/></span>",
			maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		issuerPlanNumber : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planNumber'/></span>",
		maxlength : "<span> <em class='excl'>!</em> <spring:message  code='label.planName'/> exceeds Maximum length.</span>"},
		maxEnrolee : { numericRegex : "<span> <em class='excl'>!</em><spring:message  code='label.maximumEnrollment'/> <spring:message  code='label.planmaxEnrollment'/></span>" },
		startDate : { required : "<span> <em class='excl'>!</em><spring:message  code='label.showEnter'/> <spring:message  code='label.planStartDate'/></span>"},
		endDate : { endDate : "<span> <em class='excl'>!</em><spring:message  code='label.validateEndDate'/></span>"},
		"planDental.planLevel" : { planLevel : "<span> <em class='excl'>!</em><spring:message  code='label.showSelect'/><spring:message  code='label.planLevel'/></span>"},
		"planDental.acturialValueCertificate" : { required : "<span> <em class='excl'>!</em><spring:message  code='label.upload'/> <spring:message  code='label.valueCertificate'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		if(elementId == 'planDentalacturialValueCertificate')
			{
			$("#" + elementId + "_error").html("");
			}
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error help-block');
		
	} 
});
jQuery("#frmCerti").validate({});
</script>

<script type="text/javascript">

function  formSubmit()	{
	$("#frmqdpEdit").submit();
}

var oldPlanName='${plan.name}';
var oldIssuerId ='${plan.issuer.id}';
function isExistingPlan() {
	if($("#frmqdpEdit").valid()) 	{
		var ctx = "${pageContext.request.contextPath}";
		if($('#issuerId').val() == oldIssuerId && oldPlanName == $('#name').val()) {
			formSubmit();
		} else {
			$.ajax({
			  type: 'GET',
			  url: "../isExistingPlan",
			  data:{"issuerId":$('#issuerId').val(),"name":$('#name').val(),"oldPlanName":oldPlanName},
			  success: function(data) {
				 	if(data=='true')
				 	{
				 		$("#issuername_error").html("<label class='error'><span> <em class='excl'>!</em><spring:message  code='err.planNameExists'/></span></label>");
						$("#issuername_error").attr('class','error help-inline');
				 	}
				 	else
				 	{
				 		formSubmit();
				 	}
					
	            }	  
			});
		}
	}
}
</script>
