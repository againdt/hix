(function() {
'use strict';

angular.module('adminStaffStatusCtrlModule', ['translateApp'])
.controller('AdminStaffStatusCtrl', AdminStaffStatusCtrl);

AdminStaffStatusCtrl.$inject = ['$http', '$stateParams', '$scope', '$state', 'dataStorageService', '$translate', 'StatusService'];

    function AdminStaffStatusCtrl($http, $stateParams, $scope, $state, dataStorageService, $translate, StatusService) {
        var vm = this;

        vm.init = init;
        vm.cancel = cancel;
        vm.edit = edit;
        vm.save = save;
        vm.callViewCommentsModal = callViewCommentsModal;
        vm.currentUserRole = StatusService.CurrentUserRole;
        vm.editMode = false;

      function init(){
          vm.flowFlag = {};
          updateFlowFlag();
          vm.assistantId =  dataStorageService.getItem('assistantId');
          vm.registrationComplete = dataStorageService.getItem('registrationComplete');
          vm.viewAdminStaffFlow = dataStorageService.getItem('viewAdminStaffFlow');
          $scope.$parent.vm.sideNavListFour[0].showOKicon = true;
          $scope.$parent.vm.sideNavListFour[1].disableLink = false;
          $scope.$parent.vm.sideNavListFour[2].disableLink = false;
          updateFlowFlag();
          var promise = $http.get('/hix/agency/staff/statushistory/' + vm.assistantId);
          promise.success(function(response){
              vm.loader = false;
              vm.staffStatusData = response;
          })
          promise.error(function(response){
              vm.loader = false;
          })
      }
      function callViewCommentsModal(comments) {
          $scope.content = comments;
      }
      function updateFlowFlag() {
          //for admin flow
          var isAdminRole = $("#isAdminRole").val();

          //for agency flow
          var certificationStatus = $('#agencyCertificationStatus').val();

          if (isAdminRole === 'true') {
              vm.flowFlag.adminFlow = true;
              vm.editMode = false;
              $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
          if(vm.viewAdminStaffFlow) {
              $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
          if (vm.currentUserRole === 'APPROVEDADMINSTAFFL1' || vm.currentUserRole === 'APPROVEDADMINSTAFFL2') {
               $scope.$parent.vm.sideNavListFour[0].showOKicon = false;
          }
      }
      function edit(){
          vm.editMode = !vm.editMode;
          vm.staffStatusData = angular.copy(vm.staffStatusData);
      }
      function cancel() {
          vm.editMode = !vm.editMode;
      }
      function save() {
          vm.loader = false;
          vm.editMode = !vm.editMode;
          var data = {
              id : vm.assistantId || vm.staffStatusData.id,
              approvalStatus: vm.staffStatusData.approvalStatus,
              status: vm.status,
              comments: vm.staffStatusData.comment
          };
          var promise = $http.post('/hix/agency/staff/status/', data);
          promise.success(function(response){
              vm.loader = false;
              vm.editMode = false;
              vm.staffStatusData = response;
              if(response.statusCode === '200'){
                  $window.location.reload();
              }
          })
          promise.error(function(response){
              vm.loader = false;
          })
      }
  }
})();
