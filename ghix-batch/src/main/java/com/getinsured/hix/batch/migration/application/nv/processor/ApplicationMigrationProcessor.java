package com.getinsured.hix.batch.migration.application.nv.processor;

import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;

public class ApplicationMigrationProcessor implements Tasklet  {
	private String fileName;
	public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
		Long jobId = chunkContext.getStepContext().getStepExecution().getJobExecutionId();
		int idx = fileName.indexOf(':');
		fileName = fileName.substring(idx+1);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("jobId", jobId);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("fileName", fileName);
		chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext().put("jobStartTime", System.currentTimeMillis());
		return RepeatStatus.FINISHED;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
