<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>



<div id="mainSummary" class="ps-plans__tiles">
	<%-- cp-sample__container --%>
	<div class="cp-sample__container">
		<h2 class="cp-sample__header"><spring:message code="pd.label.title.findingDentalPlans"/></h2>
		<div class="cp-loading--outter"></div>
		<div class="cp-loading--inner"></div>

		<div class="cp-tile cp-sample__tile">

			<!-- tile body -->
            <div class="cp-tile__body">

            	<!-- sample logo img -->
                <a href="#detail" id="detail_<@=id@>" class="cp-tile__img-link cp-sample__img-link detail">
                    <img src="<c:url value="/resources/img/sampleinsurance.png" />" alt="sample Plan Logo" class="cp-tile__img">
                </a>
                <!-- sample logo img end-->

                <!-- sample plan name -->
                <div class="cp-tile__plan-name">
                    <spring:message code="pd.label.title.sampleInsurance"/>
                </div>
                <!-- sample plan name end-->

                <!-- sample premium -->
                <div class="cp-tile__premium">
                    <span class="cp-tile__premium-amount"><spring:message code="pd.label.title.dollarSigns"/></span>
                    <span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth"/></span>
                </div>
                <!-- sample premium end-->

                <table class="u-margin-top-20">
                	<!-- sample Routine Dental (Adult) -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.title.routineDentalAdult"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Routine Dental (Adult) end-->


                    <!-- sample Dental Checkup (Child)	 -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.title.dentalCheckupChild"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Dental Checkup (Child)	 end-->


                    <!-- sample Deductible (Child) -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
                            <spring:message code="pd.label.commontext.deductible"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Deductible (Child) end-->


                    <!-- sample Out-Of-Pocket Maximum (Child) -->
                    <tr>
                        <th class="cp-tile__label" scope="row">
							<spring:message code="pd.label.oopmax"/>
                        </th>
                        <td class="cp-tile__value">
                            <spring:message code="pd.label.title.dollarSigns4"/>
                        </td>
                    </tr>
                    <!-- sample Out-Of-Pocket Maximum (Child) end-->
                </table>

            </div>
            <!-- tile body end -->
            <!-- Sample tile footer -->
            <div class="cp-tile__footer">

                    <label for="sampleCompare" class="cp-tile__item">
                        <input type="checkbox" class="ps-form-check-input u-margin-top-0 u-margin-right-5 compare" id="sampleCompare">
                        <spring:message code="pd.label.title.compare" htmlEscape="true"/>
                    </label>

                    <a href="javascript:void(0)" class="detail cp-tile__item">
                        <spring:message code="pd.label.title.details"/>
                    </a>
            </div>
            <!-- Sample tile footer ends -->
		</div>
		<!-- Sample tile ends -->
	</div>
	<%-- cp-sample__container end --%>

</div> <!-- #mainSummary -->

  <div id="mainSummary_err" class="alert alert-block" style="display:none">
    <p><spring:message code="pd.label.info.plans.notAvailable"/></p>
  </div>


<script type="text/html" id="resultItemTemplateMainSummary">
    <!-- tile -->
    <div class="cp-tile" tabindex="0" aria-label="You are on plan <@=name@>">

        <!-- tile header -->
        <!-- <@ var expenseEstimateText = '' @>
        <@ if(level == "LOW"){ expenseEstimateText = 'Lower';@>
            <div class="cp-tile__header cp-tile__header--lower">
        <@ } else if(level == "AVERAGE"){ expenseEstimateText = 'Average'; @>
            <div class="cp-tile__header cp-tile__header--average">
        <@ } else if(level == "HIGH"){ expenseEstimateText = 'Higher';@>
            <div class="cp-tile__header cp-tile__header--higher">
        <@ } @>
            Total Expense Estimate <i class="icon-question-sign cp-tile__icon"></i>
            <span class="cp-tile__estimate"><@=expenseEstimateText @></span>
        </div> -->
        <!-- tile header end-->

        <!-- tile body -->
        <div class="cp-tile__body">
            <!-- logo img -->
            <a href="#detail" id="detail_<@=id@>" class="cp-tile__img-link detail">

                <@ if (issuer.length > 24) {@>
                    <img src="<@=issuerLogo@>" id="detail_<@=id@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
                <@ } else { @>
                    <img src="<@=issuerLogo@>" id="detail_<@=id@>" alt="<@=issuer.substr(0,24)@>..." class="cp-tile__img">
                <@ } @>
            </a>
            <!-- logo img end-->

            <!-- plan name -->
            <div class="cp-tile__plan-name">
                <a class="detail" href="#detail" id="detail_<@=id@>" data-placement="bottom" rel="tooltip" role="tooltip" data-original-title="<@=name@>" aria-label="<@=name@>">
                    <@ if (name.length > 24) {@>
                        <@=name.substr(0,24)@>...
                    <@ } else { @>
                        <@=name@>
                    <@ } @>
                </a>
            </div>
            <!-- plan name end-->


            <!-- metal tier -->
            <div class="cp-tile__metal-tier">
                <@=level@> &nbsp;<@=networkType@>
            </div>
            <!-- metal tier end-->

			<!-- current plan -->
			<@ if ($("#enrollmentType").val() == 'A' && $("#initialDentalPlanId").val() == planId){ @>
				<div>
					<span class="cp-tile__current-plan">
						<spring:message code="pd.label.title.yourCurrentPlan"/>
					</span>
				</div>
			<@ } @>
			<!-- current plan end -->

            <!-- premium -->
            <div class="cp-tile__premium">
				<span class="cp-tile__premium-amount">$<@=premiumAfterCredit.toFixed(2) @></span>
				<span class="cp-tile__premium-month"><spring:message code="pd.label.title.premiumPerMonth" htmlEscape="true"/></span>
			</div>
            <!-- premium end-->

            <!-- tax credit -->
            <div class="cp-tile__tax-credit">
                <@ if (aptc > 0) {@>
                    after $<@=aptc.toFixed(2)@> tax credit
                    <!-- <p class="was"><spring:message code="pd.label.title.was" htmlEscape="true"/> <del>$<@=premiumBeforeCredit.toFixed(2) @></del><br/> <spring:message code="pd.label.beforeCredit" htmlEscape="true"/></p> -->
                <@ } @>
			</div>
            <!-- tax credit end -->

            <table class="cp-tile__table">
                <!-- Routine Dental Adult -->
                <tr>
					<th class="cp-tile__label" scope="row">
						<spring:message code="pd.label.title.routineDentalAdult" htmlEscape="true"/>
					</th>
					<td class="cp-tile__value">
                        <@ if(planTier1.RTN_DENTAL_ADULT.tileDisplayVal != null && planTier1.RTN_DENTAL_ADULT.tileDisplayVal != ""){@>
                            <@ if($('#stateCode').val() !== 'MN') { @>
                                <spring:message code="pd.label.title.youpay"/>
                            <@ } @>
                            <@=planTier1.RTN_DENTAL_ADULT.tileDisplayVal@>

                            <@ if($('#stateCode').val() === 'MN' && planTier1.RTN_DENTAL_ADULT.tileDisplayVal.indexOf('$') !== -1) { @>
                                <spring:message code="pd.label.copay"/>
                            <@ } else if($('#stateCode').val() === 'MN' && planTier1.RTN_DENTAL_ADULT.tileDisplayVal.indexOf('%') !== -1) { @>
                                <spring:message code="pd.label.coinsurance"/>
                            <@ } @>
                        <@ } else { @>
                            <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
                        <@ } @>
					</td>
				</tr>
                <!-- primary care visits end-->


                <!-- Dental Checkup Child -->
                <tr>
					<th class="cp-tile__label" scope="row">
						<spring:message code="pd.label.title.dentalCheckupChild"/>
					</th>
					<td class="cp-tile__value">
                        <@ if(planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal != null && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal != ""){ @>
                            <@ if($('#stateCode').val() !== 'MN') { @>
                                <spring:message code="pd.label.title.youpay"/>
                            <@ } @>
                            <@=planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal@>
                            <@ if($('#stateCode').val() === 'MN' && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal.indexOf('%') === -1) { @>
                                <spring:message code="pd.label.copay"/>
                            <@ } else if($('#stateCode').val() === 'MN' && planTier1.DENTAL_CHECKUP_CHILDREN.tileDisplayVal.indexOf('%') !== -1) { @>
                                <spring:message code="pd.label.coinsurance"/>
                            <@ } @>
                        <@ } else { @>
                            <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
                        <@ } @>
					</td>
				</tr>
                <!-- Dental Checkup Child end-->

                <!-- deductible child -->
                <tr>
                    <th class="cp-tile__label" scope="row">
                        <a
                            href="#"
                            rel="tooltip"
                            data-placement="right"
                            data-html="true"
                            data-original-title="<spring:message code="pd.tooltip.dental.deductible"/>">
                            <spring:message code="pd.label.commontext.deductible" htmlEscape='true'/> (<spring:message code="pd.label.commontext.child"/>)
                        </a>
                    </th>
                    <td class="cp-tile__value">
                        <@ if(planCosts.DEDUCTIBLE_MEDICAL != null) { @>
                            <@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != 0 && $("#PERSON_COUNT").val() > 1 && $('#stateCode').val() == 'CA'){ @>
                                <@ if(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != null && planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly != ""){ @>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">
                                         $<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkFly).toFixed()@>
                                    </a>
                                <@ } else if(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != null && planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly != ""){ @>
                                    <a
                                        href="#"
                                        rel="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">
                                        $<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkFly).toFixed()@>
                                    </a>
                                <@ } else { @>
                                    <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
                                    <@ } @>
                            <@ } else { @>
                                <@ if(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != null && planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd != ""){@>

                                    <a
                                        href="#"
                                        rel="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">
                                       $<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.inNetworkInd).toFixed()@>
                                    </a>
                                <@ } else if(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != null && planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd != ""){ @>

                                    <a
                                        href="#"
                                        rel="tooltip"
                                        data-placement="top"
                                        data-html="true"
                                        data-original-title="<spring:message arguments="<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd).toFixed()@>" code="pd.tooltip.dental.combined.deductible" htmlEscape="false"/>">
                                        $<@=parseFloat(planCosts.DEDUCTIBLE_MEDICAL.combinedInOutNetworkInd).toFixed()@>
                                    </a>
                                <@ } else { @>
                                    <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
                                <@ } @>
                            <@ } @>
                        <@ } else { @>
                            <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
                        <@ } @>
                    </td>
                </tr>
                <!-- deductible child end -->


                <!-- oop maximum child -->
                <@ if($("#showOopOnOff").val() == 'ON') { @>
                    <tr>
                        <th class="cp-tile__label" scope="row">

                            <a

                                href="#" rel="tooltip"
                                data-placement="right"
                                data-html="true"
                                data-original-title="<spring:message code="pd.tooltip.dental.oopmax"/>">
                                <spring:message code="pd.label.oopmax"/>
								<@ if ($('#stateCode').val() != "MN"){ @>
									(<spring:message code="pd.label.commontext.child"/>)
								<@ } @>
                            </a>
                        </th>
                        <td class="cp-tile__value">
                            <@ if(planCosts.MAX_OOP_MEDICAL != null) { @>
                                <@ if($("#PERSON_COUNT").val() != "" && $("#PERSON_COUNT").val() != 0 && $("#PERSON_COUNT").val() > 1 ){ @>
                                    <@ if(planCosts.MAX_OOP_MEDICAL.inNetworkFly != null && planCosts.MAX_OOP_MEDICAL.inNetworkFly != ""){ @>

                                        <a
                                            href="#"
                                            rel="tooltip"
                                            data-placement="top"
                                            data-html="true"
                                            data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkFly).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">
                                             $<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkFly).toFixed()@>
                                        </a>
                                    <@ } else if(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != null && planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly != ""){ @>
                                        <a
                                            href="#"
                                            rel="tooltip"
                                            data-placement="top"
                                            data-html="true"
                                            data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">
                                            $<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkFly).toFixed()@>
                                        </a>
                                    <@ } else { @>
                                        <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
                                    <@ } @>
                                <@ } else { @>
                                    <@ if(planCosts.MAX_OOP_MEDICAL.inNetworkInd != null && planCosts.MAX_OOP_MEDICAL.inNetworkInd != ""){@>

                                        <a
                                            href="#"
                                            rel="tooltip"
                                            data-placement="top"
                                            data-html="true"
                                            data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkInd).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">
                                            $<@=parseFloat(planCosts.MAX_OOP_MEDICAL.inNetworkInd).toFixed()@>
                                        </a>
                                    <@ } else if(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != null && planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd != ""){ @>
                                        <a
                                            href="#"
                                            rel="tooltip"
                                            data-placement="top"
                                            data-html="true"
                                            data-original-title="<spring:message arguments="<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd).toFixed()@>" code="pd.tooltip.dental.oopmax1" htmlEscape="false"/>">
                                            $<@=parseFloat(planCosts.MAX_OOP_MEDICAL.combinedInOutNetworkInd).toFixed()@>
                                        </a>
                                    <@ } else { @>
                                        <spring:message code="pd.label.commontext.notApp" htmlEscape="true"/>
                                    <@ } @>
                                <@ } @>
                            <@ } else { @>
                                <spring:message code="pd.label.commontext.notAvailable" htmlEscape="true"/>
                            <@ } @>
                        </td>
                    </tr>
                <@ } @>
                <!-- oop maximum child end -->

            </table>
        </div>
        <!-- tile body end -->

        <!-- tile footer -->
        <div class="cp-tile__footer">

                <label for="compareChk_<@=id@>" class="cp-tile__item u-hide-xs" title="<spring:message code="pd.label.title.compare"/>">
                    <input
					type="checkbox"
					class="ps-form-check-input u-margin-top-0 u-margin-right-5 compare gtm_tile_compare"
					id="compareChk_<@=id@>">
                    <spring:message code="pd.label.title.compare" htmlEscape="true"/>
                </label>

                <a href="#" id="detail_<@=id @>" data-plan-id="<@=planId@>" class="detail cp-tile__item  gtm_detail" title="<spring:message code="pd.label.title.details"/>">
                    <spring:message code="pd.label.title.details"/>
                </a>

                <@ if ($('#stateCode').val() != "CT" && ($("#flowType").val() == 'PLANSELECTION' || $("#flowType").val() == 'PREELIGIBILITY')){ @>
    				<a href="#" id="cart_<@=planId@>" class="cp-tile__item cp-tile__add addToCart gtm_add_to_cart" title="<spring:message code="pd.label.title.add"/>">
                        <spring:message code="pd.label.title.add"/>
                        <i class="icon-shopping-cart"></i>
                    </a>
                <@ } @>
		</div>
        <!-- tile footer ends -->

    </div>
    <!-- tile end -->
</script>
