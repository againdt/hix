
package com.getinsured.iex.hub.ssac.extension;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.namespace.QName;

import com.getinsured.iex.hub.ssac.codes_schema.DeathConfirmationCodeSimpleType;
import com.getinsured.iex.hub.ssac.niem.proxy.xsd._2_0.Boolean;



/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the gov.hhs.cms.dsh.sim.ee.ssac.extn package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _YearlyIncomeAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "YearlyIncomeAmount");
    private final static QName _RequestTitleIIMonthlyIncomeDate_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestTitleIIMonthlyIncomeDate");
    private final static QName _PersonFullName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonFullName");
    private final static QName _PrisonerConfinementDate_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PrisonerConfinementDate");
    private final static QName _RequestedMonthMinusThreeInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestedMonthMinusThreeInformation");
    private final static QName _IncomeAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "IncomeAmount");
    private final static QName _PriorMonthAccrualAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PriorMonthAccrualAmount");
    private final static QName _PersonGivenName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonGivenName");
    private final static QName _NetMonthlyBenefitCreditedAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "NetMonthlyBenefitCreditedAmount");
    private final static QName _SSAQuartersOfCoverage_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAQuartersOfCoverage");
    private final static QName _SSAResponseCodeText_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAResponseCodeText");
    private final static QName _SSAIncarcerationInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAIncarcerationInformation");
    private final static QName _OngoingPaymentInSuspenseIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "OngoingPaymentInSuspenseIndicator");
    private final static QName _MonthlyIncomeAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "MonthlyIncomeAmount");
    private final static QName _SSAQuartersOfCoverageInformationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAQuartersOfCoverageInformationIndicator");
    private final static QName _IncomeDate_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "IncomeDate");
    private final static QName _LocationCityName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "LocationCityName");
    private final static QName _ResponseCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ResponseCode");
    private final static QName _SSAResponse_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAResponse");
    private final static QName _ReportingPersonText_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ReportingPersonText");
    private final static QName _DeathConfirmationCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "DeathConfirmationCode");
    private final static QName _InmateStatusIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "InmateStatusIndicator");
    private final static QName _SSATitleIIAnnualIncomeInformationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSATitleIIAnnualIncomeInformationIndicator");
    private final static QName _OverpaymentDeductionAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "OverpaymentDeductionAmount");
    private final static QName _ReturnedCheckAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ReturnedCheckAmount");
    private final static QName _RequestTitleIIMonthlyIncomeVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestTitleIIMonthlyIncomeVerificationIndicator");
    private final static QName _SSAResponseCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSAResponseCode");
    private final static QName _TDSResponseDescriptionText_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TDSResponseDescriptionText");
    private final static QName _PersonMiddleName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonMiddleName");
    private final static QName _TitleIIRequestedYearMinusThreeInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIRequestedYearMinusThreeInformation");
    private final static QName _RequestCitizenshipVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestCitizenshipVerificationIndicator");
    private final static QName _SSACompositeIndividualResponse_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositeIndividualResponse");
    private final static QName _SSATitleIIYearlyIncome_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSATitleIIYearlyIncome");
    private final static QName _SupervisionFacility_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SupervisionFacility");
    private final static QName _OngoingMonthlyOverpaymentDeductionAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "OngoingMonthlyOverpaymentDeductionAmount");
    private final static QName _PersonDisabledIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonDisabledIndicator");
    private final static QName _RequestedMonthMinusTwoInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestedMonthMinusTwoInformation");
    private final static QName _PersonSurName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonSurName");
    private final static QName _SSATitleIIMonthlyIncomeInformationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSATitleIIMonthlyIncomeInformationIndicator");
    private final static QName _RequestTitleIIAnnualIncomeVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestTitleIIAnnualIncomeVerificationIndicator");
    private final static QName _TitleIIRequestedYearInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIRequestedYearInformation");
    private final static QName _SSNVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSNVerificationIndicator");
    private final static QName _TitleIIRequestedYearMinusOneInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIRequestedYearMinusOneInformation");
    private final static QName _FacilityCategoryCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "FacilityCategoryCode");
    private final static QName _PrisonerIdentification_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PrisonerIdentification");
    private final static QName _RequestIncarcerationVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestIncarcerationVerificationIndicator");
    private final static QName _LocationPostalCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "LocationPostalCode");
    private final static QName _RequestedMonthInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestedMonthInformation");
    private final static QName _Person_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "Person");
    private final static QName _FacilityLocation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "FacilityLocation");
    private final static QName _TitleIIOtherYearInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIOtherYearInformation");
    private final static QName _ContactFaxNumber_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ContactFaxNumber");
    private final static QName _ContactTelephoneNumber_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ContactTelephoneNumber");
    private final static QName _PersonSSNIdentification_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonSSNIdentification");
    private final static QName _IncomeMonthYear_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "IncomeMonthYear");
    private final static QName _QualifyingQuarter_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "QualifyingQuarter");
    private final static QName _ResponseDescriptionText_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ResponseDescriptionText");
    private final static QName _RequestQuartersOfCoverageVerificationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestQuartersOfCoverageVerificationIndicator");
    private final static QName _BenefitCreditedAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "BenefitCreditedAmount");
    private final static QName _QualifyingYearAndQuarter_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "QualifyingYearAndQuarter");
    private final static QName _LifeTimeQuarterQuantity_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "LifeTimeQuarterQuantity");
    private final static QName _SSNVerificationCodeText_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSNVerificationCodeText");
    private final static QName _PaymentInSuspenseIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PaymentInSuspenseIndicator");
    private final static QName _PersonName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonName");
    private final static QName _RequestTitleIIAnnualIncomeDate_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestTitleIIAnnualIncomeDate");
    private final static QName _SSATitleIIMonthlyIncome_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSATitleIIMonthlyIncome");
    private final static QName _RequestedMonthMinusOneInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "RequestedMonthMinusOneInformation");
    private final static QName _SSNVerificationCode_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSNVerificationCode");
    private final static QName _PersonIncarcerationInformationIndicator_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "PersonIncarcerationInformationIndicator");
    private final static QName _FacilityContactInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "FacilityContactInformation");
    private final static QName _LocationStreet_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "LocationStreet");
    private final static QName _SSACompositeIndividualRequest_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "SSACompositeIndividualRequest");
    private final static QName _TitleIIRequestedYearMinusTwoInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIRequestedYearMinusTwoInformation");
    private final static QName _QualifyingYear_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "QualifyingYear");
    private final static QName _ResponseMetadata_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "ResponseMetadata");
    private final static QName _TitleIIRequestedYearMinusFourInformation_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "TitleIIRequestedYearMinusFourInformation");
    private final static QName _OngoingMonthlyBenefitCreditedAmount_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "OngoingMonthlyBenefitCreditedAmount");
    private final static QName _FacilityName_QNAME = new QName("http://extn.ssac.ee.sim.dsh.cms.hhs.gov", "FacilityName");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: gov.hhs.cms.dsh.sim.ee.ssac.extn
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PersonNameType }
     * 
     */
    public PersonNameType createPersonNameType() {
        return new PersonNameType();
    }

    /**
     * Create an instance of {@link SSATitleIIMonthlyIncomeType }
     * 
     */
    public SSATitleIIMonthlyIncomeType createSSATitleIIMonthlyIncomeType() {
        return new SSATitleIIMonthlyIncomeType();
    }

    /**
     * Create an instance of {@link QualifyingYearAndQuarterType }
     * 
     */
    public QualifyingYearAndQuarterType createQualifyingYearAndQuarterType() {
        return new QualifyingYearAndQuarterType();
    }

    /**
     * Create an instance of {@link QuarterYearType }
     * 
     */
    public QuarterYearType createQuarterYearType() {
        return new QuarterYearType();
    }

    /**
     * Create an instance of {@link ResponseMetadataType }
     * 
     */
    public ResponseMetadataType createResponseMetadataType() {
        return new ResponseMetadataType();
    }

    /**
     * Create an instance of {@link SupervisionFacilityType }
     * 
     */
    public SupervisionFacilityType createSupervisionFacilityType() {
        return new SupervisionFacilityType();
    }

    /**
     * Create an instance of {@link SSATitleIIYearlyInformationType }
     * 
     */
    public SSATitleIIYearlyInformationType createSSATitleIIYearlyInformationType() {
        return new SSATitleIIYearlyInformationType();
    }

    /**
     * Create an instance of {@link SSATitleIIMonthlyInformationType }
     * 
     */
    public SSATitleIIMonthlyInformationType createSSATitleIIMonthlyInformationType() {
        return new SSATitleIIMonthlyInformationType();
    }

    /**
     * Create an instance of {@link SSATitleIIYearlyIncomeType }
     * 
     */
    public SSATitleIIYearlyIncomeType createSSATitleIIYearlyIncomeType() {
        return new SSATitleIIYearlyIncomeType();
    }

    /**
     * Create an instance of {@link SSATitleIIRequestedYearInformationType }
     * 
     */
    public SSATitleIIRequestedYearInformationType createSSATitleIIRequestedYearInformationType() {
        return new SSATitleIIRequestedYearInformationType();
    }

    /**
     * Create an instance of {@link RestrictedSurNameType }
     * 
     */
    public RestrictedSurNameType createRestrictedSurNameType() {
        return new RestrictedSurNameType();
    }

    /**
     * Create an instance of {@link FacilityContactInformationType }
     * 
     */
    public FacilityContactInformationType createFacilityContactInformationType() {
        return new FacilityContactInformationType();
    }

    /**
     * Create an instance of {@link RestrictedMiddleNameType }
     * 
     */
    public RestrictedMiddleNameType createRestrictedMiddleNameType() {
        return new RestrictedMiddleNameType();
    }

    /**
     * Create an instance of {@link SSNVerificationCodeType }
     * 
     */
    public SSNVerificationCodeType createSSNVerificationCodeType() {
        return new SSNVerificationCodeType();
    }

    /**
     * Create an instance of {@link SSACompositeIndividualResponseType }
     * 
     */
    public SSACompositeIndividualResponseType createSSACompositeIndividualResponseType() {
        return new SSACompositeIndividualResponseType();
    }

    /**
     * Create an instance of {@link SSACompositeIndividualRequestType }
     * 
     */
    public SSACompositeIndividualRequestType createSSACompositeIndividualRequestType() {
        return new SSACompositeIndividualRequestType();
    }

    /**
     * Create an instance of {@link SSAQuartersOfCoverageType }
     * 
     */
    public SSAQuartersOfCoverageType createSSAQuartersOfCoverageType() {
        return new SSAQuartersOfCoverageType();
    }

    /**
     * Create an instance of {@link RestrictedGivenNameType }
     * 
     */
    public RestrictedGivenNameType createRestrictedGivenNameType() {
        return new RestrictedGivenNameType();
    }

    /**
     * Create an instance of {@link PersonSSNIdentificationType }
     * 
     */
    public PersonSSNIdentificationType createPersonSSNIdentificationType() {
        return new PersonSSNIdentificationType();
    }

    /**
     * Create an instance of {@link SSAResponseType }
     * 
     */
    public SSAResponseType createSSAResponseType() {
        return new SSAResponseType();
    }

    /**
     * Create an instance of {@link SSAIncarcerationInformationType }
     * 
     */
    public SSAIncarcerationInformationType createSSAIncarcerationInformationType() {
        return new SSAIncarcerationInformationType();
    }

    /**
     * Create an instance of {@link PersonType }
     * 
     */
    public PersonType createPersonType() {
        return new PersonType();
    }

    /**
     * Create an instance of {@link FacilityLocationType }
     * 
     */
    public FacilityLocationType createFacilityLocationType() {
        return new FacilityLocationType();
    }

    /**
     * Create an instance of {@link SSACompositeResponseType }
     * 
     */
    public SSACompositeResponseType createSSACompositeResponseType() {
        return new SSACompositeResponseType();
    }

    /**
     * Create an instance of {@link SSACompositeRequestType }
     * 
     */
    public SSACompositeRequestType createSSACompositeRequestType() {
        return new SSACompositeRequestType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "YearlyIncomeAmount")
    public JAXBElement<BigDecimal> createYearlyIncomeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_YearlyIncomeAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestTitleIIMonthlyIncomeDate")
    public JAXBElement<String> createRequestTitleIIMonthlyIncomeDate(String value) {
        return new JAXBElement<String>(_RequestTitleIIMonthlyIncomeDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonFullName")
    public JAXBElement<String> createPersonFullName(String value) {
        return new JAXBElement<String>(_PersonFullName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PrisonerConfinementDate")
    public JAXBElement<String> createPrisonerConfinementDate(String value) {
        return new JAXBElement<String>(_PrisonerConfinementDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIMonthlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestedMonthMinusThreeInformation")
    public JAXBElement<SSATitleIIMonthlyInformationType> createRequestedMonthMinusThreeInformation(SSATitleIIMonthlyInformationType value) {
        return new JAXBElement<SSATitleIIMonthlyInformationType>(_RequestedMonthMinusThreeInformation_QNAME, SSATitleIIMonthlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "IncomeAmount")
    public JAXBElement<BigDecimal> createIncomeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_IncomeAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PriorMonthAccrualAmount")
    public JAXBElement<BigDecimal> createPriorMonthAccrualAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_PriorMonthAccrualAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestrictedGivenNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonGivenName")
    public JAXBElement<RestrictedGivenNameType> createPersonGivenName(RestrictedGivenNameType value) {
        return new JAXBElement<RestrictedGivenNameType>(_PersonGivenName_QNAME, RestrictedGivenNameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "NetMonthlyBenefitCreditedAmount")
    public JAXBElement<BigDecimal> createNetMonthlyBenefitCreditedAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_NetMonthlyBenefitCreditedAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSAQuartersOfCoverageType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAQuartersOfCoverage")
    public JAXBElement<SSAQuartersOfCoverageType> createSSAQuartersOfCoverage(SSAQuartersOfCoverageType value) {
        return new JAXBElement<SSAQuartersOfCoverageType>(_SSAQuartersOfCoverage_QNAME, SSAQuartersOfCoverageType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAResponseCodeText")
    public JAXBElement<String> createSSAResponseCodeText(String value) {
        return new JAXBElement<String>(_SSAResponseCodeText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSAIncarcerationInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAIncarcerationInformation")
    public JAXBElement<SSAIncarcerationInformationType> createSSAIncarcerationInformation(SSAIncarcerationInformationType value) {
        return new JAXBElement<SSAIncarcerationInformationType>(_SSAIncarcerationInformation_QNAME, SSAIncarcerationInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "OngoingPaymentInSuspenseIndicator")
    public JAXBElement<Boolean> createOngoingPaymentInSuspenseIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_OngoingPaymentInSuspenseIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "MonthlyIncomeAmount")
    public JAXBElement<BigDecimal> createMonthlyIncomeAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_MonthlyIncomeAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAQuartersOfCoverageInformationIndicator")
    public JAXBElement<Boolean> createSSAQuartersOfCoverageInformationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSAQuartersOfCoverageInformationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "IncomeDate")
    public JAXBElement<String> createIncomeDate(String value) {
        return new JAXBElement<String>(_IncomeDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "LocationCityName")
    public JAXBElement<String> createLocationCityName(String value) {
        return new JAXBElement<String>(_LocationCityName_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ResponseCode")
    public JAXBElement<String> createResponseCode(String value) {
        return new JAXBElement<String>(_ResponseCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSAResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAResponse")
    public JAXBElement<SSAResponseType> createSSAResponse(SSAResponseType value) {
        return new JAXBElement<SSAResponseType>(_SSAResponse_QNAME, SSAResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ReportingPersonText")
    public JAXBElement<String> createReportingPersonText(String value) {
        return new JAXBElement<String>(_ReportingPersonText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DeathConfirmationCodeSimpleType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "DeathConfirmationCode")
    public JAXBElement<DeathConfirmationCodeSimpleType> createDeathConfirmationCode(DeathConfirmationCodeSimpleType value) {
        return new JAXBElement<DeathConfirmationCodeSimpleType>(_DeathConfirmationCode_QNAME, DeathConfirmationCodeSimpleType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "InmateStatusIndicator")
    public JAXBElement<Boolean> createInmateStatusIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_InmateStatusIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSATitleIIAnnualIncomeInformationIndicator")
    public JAXBElement<Boolean> createSSATitleIIAnnualIncomeInformationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSATitleIIAnnualIncomeInformationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "OverpaymentDeductionAmount")
    public JAXBElement<BigDecimal> createOverpaymentDeductionAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_OverpaymentDeductionAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ReturnedCheckAmount")
    public JAXBElement<BigDecimal> createReturnedCheckAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_ReturnedCheckAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestTitleIIMonthlyIncomeVerificationIndicator")
    public JAXBElement<Boolean> createRequestTitleIIMonthlyIncomeVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestTitleIIMonthlyIncomeVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSAResponseCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createSSAResponseCode(String value) {
        return new JAXBElement<String>(_SSAResponseCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TDSResponseDescriptionText")
    public JAXBElement<String> createTDSResponseDescriptionText(String value) {
        return new JAXBElement<String>(_TDSResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestrictedMiddleNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonMiddleName")
    public JAXBElement<RestrictedMiddleNameType> createPersonMiddleName(RestrictedMiddleNameType value) {
        return new JAXBElement<RestrictedMiddleNameType>(_PersonMiddleName_QNAME, RestrictedMiddleNameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIRequestedYearMinusThreeInformation")
    public JAXBElement<SSATitleIIYearlyInformationType> createTitleIIRequestedYearMinusThreeInformation(SSATitleIIYearlyInformationType value) {
        return new JAXBElement<SSATitleIIYearlyInformationType>(_TitleIIRequestedYearMinusThreeInformation_QNAME, SSATitleIIYearlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestCitizenshipVerificationIndicator")
    public JAXBElement<Boolean> createRequestCitizenshipVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestCitizenshipVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSACompositeIndividualResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSACompositeIndividualResponse")
    public JAXBElement<SSACompositeIndividualResponseType> createSSACompositeIndividualResponse(SSACompositeIndividualResponseType value) {
        return new JAXBElement<SSACompositeIndividualResponseType>(_SSACompositeIndividualResponse_QNAME, SSACompositeIndividualResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyIncomeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSATitleIIYearlyIncome")
    public JAXBElement<SSATitleIIYearlyIncomeType> createSSATitleIIYearlyIncome(SSATitleIIYearlyIncomeType value) {
        return new JAXBElement<SSATitleIIYearlyIncomeType>(_SSATitleIIYearlyIncome_QNAME, SSATitleIIYearlyIncomeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SupervisionFacilityType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SupervisionFacility")
    public JAXBElement<SupervisionFacilityType> createSupervisionFacility(SupervisionFacilityType value) {
        return new JAXBElement<SupervisionFacilityType>(_SupervisionFacility_QNAME, SupervisionFacilityType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "OngoingMonthlyOverpaymentDeductionAmount")
    public JAXBElement<BigDecimal> createOngoingMonthlyOverpaymentDeductionAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_OngoingMonthlyOverpaymentDeductionAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonDisabledIndicator")
    public JAXBElement<Boolean> createPersonDisabledIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_PersonDisabledIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIMonthlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestedMonthMinusTwoInformation")
    public JAXBElement<SSATitleIIMonthlyInformationType> createRequestedMonthMinusTwoInformation(SSATitleIIMonthlyInformationType value) {
        return new JAXBElement<SSATitleIIMonthlyInformationType>(_RequestedMonthMinusTwoInformation_QNAME, SSATitleIIMonthlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RestrictedSurNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonSurName")
    public JAXBElement<RestrictedSurNameType> createPersonSurName(RestrictedSurNameType value) {
        return new JAXBElement<RestrictedSurNameType>(_PersonSurName_QNAME, RestrictedSurNameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSATitleIIMonthlyIncomeInformationIndicator")
    public JAXBElement<Boolean> createSSATitleIIMonthlyIncomeInformationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSATitleIIMonthlyIncomeInformationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestTitleIIAnnualIncomeVerificationIndicator")
    public JAXBElement<Boolean> createRequestTitleIIAnnualIncomeVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestTitleIIAnnualIncomeVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIRequestedYearInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIRequestedYearInformation")
    public JAXBElement<SSATitleIIRequestedYearInformationType> createTitleIIRequestedYearInformation(SSATitleIIRequestedYearInformationType value) {
        return new JAXBElement<SSATitleIIRequestedYearInformationType>(_TitleIIRequestedYearInformation_QNAME, SSATitleIIRequestedYearInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSNVerificationIndicator")
    public JAXBElement<Boolean> createSSNVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_SSNVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIRequestedYearMinusOneInformation")
    public JAXBElement<SSATitleIIYearlyInformationType> createTitleIIRequestedYearMinusOneInformation(SSATitleIIYearlyInformationType value) {
        return new JAXBElement<SSATitleIIYearlyInformationType>(_TitleIIRequestedYearMinusOneInformation_QNAME, SSATitleIIYearlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "FacilityCategoryCode")
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    public JAXBElement<String> createFacilityCategoryCode(String value) {
        return new JAXBElement<String>(_FacilityCategoryCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PrisonerIdentification")
    public JAXBElement<String> createPrisonerIdentification(String value) {
        return new JAXBElement<String>(_PrisonerIdentification_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestIncarcerationVerificationIndicator")
    public JAXBElement<Boolean> createRequestIncarcerationVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestIncarcerationVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "LocationPostalCode")
    public JAXBElement<String> createLocationPostalCode(String value) {
        return new JAXBElement<String>(_LocationPostalCode_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIMonthlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestedMonthInformation")
    public JAXBElement<SSATitleIIMonthlyInformationType> createRequestedMonthInformation(SSATitleIIMonthlyInformationType value) {
        return new JAXBElement<SSATitleIIMonthlyInformationType>(_RequestedMonthInformation_QNAME, SSATitleIIMonthlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "Person")
    public JAXBElement<PersonType> createPerson(PersonType value) {
        return new JAXBElement<PersonType>(_Person_QNAME, PersonType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacilityLocationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "FacilityLocation")
    public JAXBElement<FacilityLocationType> createFacilityLocation(FacilityLocationType value) {
        return new JAXBElement<FacilityLocationType>(_FacilityLocation_QNAME, FacilityLocationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIOtherYearInformation")
    public JAXBElement<SSATitleIIYearlyInformationType> createTitleIIOtherYearInformation(SSATitleIIYearlyInformationType value) {
        return new JAXBElement<SSATitleIIYearlyInformationType>(_TitleIIOtherYearInformation_QNAME, SSATitleIIYearlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ContactFaxNumber")
    public JAXBElement<String> createContactFaxNumber(String value) {
        return new JAXBElement<String>(_ContactFaxNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ContactTelephoneNumber")
    public JAXBElement<String> createContactTelephoneNumber(String value) {
        return new JAXBElement<String>(_ContactTelephoneNumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonSSNIdentificationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonSSNIdentification")
    public JAXBElement<PersonSSNIdentificationType> createPersonSSNIdentification(PersonSSNIdentificationType value) {
        return new JAXBElement<PersonSSNIdentificationType>(_PersonSSNIdentification_QNAME, PersonSSNIdentificationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "IncomeMonthYear")
    public JAXBElement<String> createIncomeMonthYear(String value) {
        return new JAXBElement<String>(_IncomeMonthYear_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Integer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "QualifyingQuarter")
    public JAXBElement<Integer> createQualifyingQuarter(Integer value) {
        return new JAXBElement<Integer>(_QualifyingQuarter_QNAME, Integer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ResponseDescriptionText")
    public JAXBElement<String> createResponseDescriptionText(String value) {
        return new JAXBElement<String>(_ResponseDescriptionText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestQuartersOfCoverageVerificationIndicator")
    public JAXBElement<Boolean> createRequestQuartersOfCoverageVerificationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_RequestQuartersOfCoverageVerificationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "BenefitCreditedAmount")
    public JAXBElement<BigDecimal> createBenefitCreditedAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_BenefitCreditedAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QualifyingYearAndQuarterType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "QualifyingYearAndQuarter")
    public JAXBElement<QualifyingYearAndQuarterType> createQualifyingYearAndQuarter(QualifyingYearAndQuarterType value) {
        return new JAXBElement<QualifyingYearAndQuarterType>(_QualifyingYearAndQuarter_QNAME, QualifyingYearAndQuarterType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigInteger }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "LifeTimeQuarterQuantity")
    public JAXBElement<BigInteger> createLifeTimeQuarterQuantity(BigInteger value) {
        return new JAXBElement<BigInteger>(_LifeTimeQuarterQuantity_QNAME, BigInteger.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSNVerificationCodeText")
    public JAXBElement<String> createSSNVerificationCodeText(String value) {
        return new JAXBElement<String>(_SSNVerificationCodeText_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PaymentInSuspenseIndicator")
    public JAXBElement<Boolean> createPaymentInSuspenseIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_PaymentInSuspenseIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PersonNameType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonName")
    public JAXBElement<PersonNameType> createPersonName(PersonNameType value) {
        return new JAXBElement<PersonNameType>(_PersonName_QNAME, PersonNameType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestTitleIIAnnualIncomeDate")
    public JAXBElement<String> createRequestTitleIIAnnualIncomeDate(String value) {
        return new JAXBElement<String>(_RequestTitleIIAnnualIncomeDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIMonthlyIncomeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSATitleIIMonthlyIncome")
    public JAXBElement<SSATitleIIMonthlyIncomeType> createSSATitleIIMonthlyIncome(SSATitleIIMonthlyIncomeType value) {
        return new JAXBElement<SSATitleIIMonthlyIncomeType>(_SSATitleIIMonthlyIncome_QNAME, SSATitleIIMonthlyIncomeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIMonthlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "RequestedMonthMinusOneInformation")
    public JAXBElement<SSATitleIIMonthlyInformationType> createRequestedMonthMinusOneInformation(SSATitleIIMonthlyInformationType value) {
        return new JAXBElement<SSATitleIIMonthlyInformationType>(_RequestedMonthMinusOneInformation_QNAME, SSATitleIIMonthlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSNVerificationCodeType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSNVerificationCode")
    public JAXBElement<SSNVerificationCodeType> createSSNVerificationCode(SSNVerificationCodeType value) {
        return new JAXBElement<SSNVerificationCodeType>(_SSNVerificationCode_QNAME, SSNVerificationCodeType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "PersonIncarcerationInformationIndicator")
    public JAXBElement<Boolean> createPersonIncarcerationInformationIndicator(Boolean value) {
        return new JAXBElement<Boolean>(_PersonIncarcerationInformationIndicator_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FacilityContactInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "FacilityContactInformation")
    public JAXBElement<FacilityContactInformationType> createFacilityContactInformation(FacilityContactInformationType value) {
        return new JAXBElement<FacilityContactInformationType>(_FacilityContactInformation_QNAME, FacilityContactInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "LocationStreet")
    public JAXBElement<String> createLocationStreet(String value) {
        return new JAXBElement<String>(_LocationStreet_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSACompositeIndividualRequestType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "SSACompositeIndividualRequest")
    public JAXBElement<SSACompositeIndividualRequestType> createSSACompositeIndividualRequest(SSACompositeIndividualRequestType value) {
        return new JAXBElement<SSACompositeIndividualRequestType>(_SSACompositeIndividualRequest_QNAME, SSACompositeIndividualRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIRequestedYearMinusTwoInformation")
    public JAXBElement<SSATitleIIYearlyInformationType> createTitleIIRequestedYearMinusTwoInformation(SSATitleIIYearlyInformationType value) {
        return new JAXBElement<SSATitleIIYearlyInformationType>(_TitleIIRequestedYearMinusTwoInformation_QNAME, SSATitleIIYearlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuarterYearType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "QualifyingYear")
    public JAXBElement<QuarterYearType> createQualifyingYear(QuarterYearType value) {
        return new JAXBElement<QuarterYearType>(_QualifyingYear_QNAME, QuarterYearType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ResponseMetadataType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "ResponseMetadata")
    public JAXBElement<ResponseMetadataType> createResponseMetadata(ResponseMetadataType value) {
        return new JAXBElement<ResponseMetadataType>(_ResponseMetadata_QNAME, ResponseMetadataType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SSATitleIIYearlyInformationType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "TitleIIRequestedYearMinusFourInformation")
    public JAXBElement<SSATitleIIYearlyInformationType> createTitleIIRequestedYearMinusFourInformation(SSATitleIIYearlyInformationType value) {
        return new JAXBElement<SSATitleIIYearlyInformationType>(_TitleIIRequestedYearMinusFourInformation_QNAME, SSATitleIIYearlyInformationType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BigDecimal }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "OngoingMonthlyBenefitCreditedAmount")
    public JAXBElement<BigDecimal> createOngoingMonthlyBenefitCreditedAmount(BigDecimal value) {
        return new JAXBElement<BigDecimal>(_OngoingMonthlyBenefitCreditedAmount_QNAME, BigDecimal.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://extn.ssac.ee.sim.dsh.cms.hhs.gov", name = "FacilityName")
    public JAXBElement<String> createFacilityName(String value) {
        return new JAXBElement<String>(_FacilityName_QNAME, String.class, null, value);
    }

}
