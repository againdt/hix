package com.getinsured.eligibility.slcsp.service;

import com.getinsured.iex.ssap.model.SsapApplication;

public interface BenchMarkPremiumService {
	
	public Float getSLCSPAmount(SsapApplication ssapapplication);

	public Float getSLCSPAmountForApplicationId(Long ssapApplicationId);

}
