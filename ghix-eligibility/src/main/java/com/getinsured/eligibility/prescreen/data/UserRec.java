package com.getinsured.eligibility.prescreen.data;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;

public class UserRec {
	
	private int adultAge;
	private int childAge;
	
	private String adultDob;
	private String childDob;
	
	private String stateName;
	private String stateCode;
	private String zipCode;
	private String countyName;
	
	private long errorCode=0;
	private String errMsg;
	
	private double premium = 0.0;
	
	private long startTime=0;
	private long totalTime=0;
	
	
	public void startCall(){
		startTime = TSCalendar.getInstance().getTimeInMillis();
	}
	
	public void endCall(){
		totalTime = TSCalendar.getInstance().getTimeInMillis() - startTime;
	}
	
	public UserRec(int age1,int age2, String sn,String sc,String zip,String cn)
	{
		adultAge =age1;
		childAge = age2;
		stateName =sn;
		stateCode = sc;
		zipCode = zip;
		countyName = cn;
		
	}
	
	public String getData()
	{
		StringBuilder sb = new StringBuilder();
		sb.append("adult:").append(adultAge);
		sb.append(":child:").append(childAge);
		sb.append(":stateName:").append(stateName);
		sb.append(":stateCode:").append(stateCode);
		sb.append(":zip:").append(zipCode);
		sb.append(":county:").append(countyName);
		sb.append(":error:").append(errorCode);
		sb.append(":errMsg").append(errMsg);
		sb.append(":premium:").append(premium);
		//sb.append(":startTime:").append(startTime);
		sb.append(":totalTime:").append(totalTime);
			
		return sb.toString();
	}

	public int getAdultAge() {
		return adultAge;
	}

	public void setAdultAge(int adultAge) {
		this.adultAge = adultAge;
	}

	public int getChildAge() {
		return childAge;
	}

	public void setChildAge(int childAge) {
		this.childAge = childAge;
	}

	public String getAdultDob() {
		return adultDob;
	}

	public void setAdultDob(String adultDob) {
		this.adultDob = adultDob;
	}

	public String getChildDob() {
		return childDob;
	}

	public void setChildDob(String childDob) {
		this.childDob = childDob;
	}

	public String getStateName() {
		return stateName;
	}

	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	public String getStateCode() {
		return stateCode;
	}

	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	public String getZipCode() {
		return zipCode;
	}

	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}

	public String getCountyName() {
		return countyName;
	}

	public void setCountyName(String countyName) {
		this.countyName = countyName;
	}

	public long getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(long errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}

	public double getPremium() {
		return premium;
	}

	public void setPremium(double premium) {
		this.premium = premium;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getTotalTime() {
		return totalTime;
	}

	public void setTotalTime(long totalTime) {
		this.totalTime = totalTime;
	}

}
