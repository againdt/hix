package com.getinsured.hix.entity.admin.controller;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.entity.AssisterExportDTO;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.entity.admin.service.AssisterAdminService;
import com.getinsured.hix.entity.assister.repository.IAssisterCertificationNumberRepository;
import com.getinsured.hix.entity.assister.service.AssisterDesignateService;
import com.getinsured.hix.entity.assister.service.AssisterLanguagesService;
import com.getinsured.hix.entity.assister.service.AssisterService;
import com.getinsured.hix.entity.util.EntityConstants;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.AssisterCertificationNumber;
import com.getinsured.hix.model.entity.AssisterLanguages;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ContentManagementServiceException;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;

/**
 * The controller class that receives Entity Admin related Rest requests from
 * ghix-web. It then performs the requested operation and returns response
 */
@SuppressWarnings("unused")
@Controller
@RequestMapping(value = "/entity/assisteradmin")
public class AssisterAdminController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AssisterAdminController.class);
	
	@Autowired
	private AssisterService assisterService;

	@Autowired
	private AssisterAdminService assisterAdminService;

	@Autowired
	private AssisterLanguagesService assisterLanguagesService;

	@Autowired
	private ContentManagementService ecmService;

	@Autowired
	private IAssisterCertificationNumberRepository assisterCertificationNumberRepository;

	@Autowired
	private AssisterDesignateService assisterDesignateService;
	private static final int MAX_COMMENT_LENGTH = 4000;

	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	@ResponseBody
	public String welcome() {
		
		LOGGER.info("welcome: START");

		LOGGER.info("Welcome toAssister Admin module");

		LOGGER.info("welcome: END");
		return "Welcome to Assister Admin module";
	}

	/**
	 * Fetches Assisters associated with all enrollment entities. Parameter
	 * AssisterRequestDTO Returns a String containing list of all available
	 * assisters based on search parameters
	 * 
	 * @param requestXML
	 *            xml representation of the AssisterRequestDTO}
	 * @return xml representation of AssisterResponseDTO
	 */
	@RequestMapping(value = "/getassisterlist", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterList(@RequestBody String requestXML) {
		
		LOGGER.info("getAssisterList: START");
		
		LOGGER.info("Inside getAssisterList method in ghix-entity module");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
		String assisterResponseJSON=null;
		try {
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML);
			Assister assister = assisterRequestDTO.getAssister();

			LOGGER.debug("Name of Assister : " + assisterRequestDTO.getAssister().getFirstName() + " "
					+ assisterRequestDTO.getAssister().getLastName());
			List<Assister> assisters = assisterAdminService.findAssister(assister);
			assisterResponseDTO.setListOfAssisters(assisters);
			
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
			
		} catch (Exception exception) {
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Exception occured in retrieving list of assisters from db" , exception); 
		}
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
			assisterResponseJSON =  writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception e) {
			LOGGER.error("Exception occured while conveting Object to JSON in getAssisterList" , e); 
			throw new GIRuntimeException(e, ExceptionUtils.getFullStackTrace(e), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting getAssisterList in ghix-entity module");
		
		LOGGER.info("getAssisterList: END");
		
		return assisterResponseJSON;
	}

	/**
	 * Saves the modified Assister status into the DB
	 * 
	 * @param requestXML
	 *            xml representation of AssisterRequestDTO
	 * @return xml representation of assisterResponseDTO
	 */
	@RequestMapping(value = "/saveassisterstatus", method = RequestMethod.POST)
	@ResponseBody
	public String saveAssisterCertification(@RequestBody String requestXML) {
		
		LOGGER.info("saveAssisterCertification: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();

		LOGGER.debug("Save Assister Certification Status Starts : Received Rest call from ghix-web.");

		String assisterResponseDTOString =null;
		ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		try {
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML );

			Assister assister = assisterRequestDTO.getAssister();
			if (assister != null) {
				Assister existingAssister = assisterService.findById(assister.getId());
				//Fetching first 4000 char for comments text;
				String comments = assister.getComments();
				int beginIndex = 0;
				int endIndex = comments.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  comments.length();
				comments = comments.substring(beginIndex, endIndex);			
				existingAssister.setComments(comments);
				existingAssister.setDocumentId(assister.getDocumentId());
				if (compareCertificationStatus(assister, existingAssister)) {
					
					existingAssister.setCertificationStatus(assister.getCertificationStatus());
					existingAssister.setStatusChangeDate(new TSDate());
					

					if (assister.getCertificationStatus() != null
							&& assister.getCertificationStatus().contains(EntityConstants.STATUS_DECERTIFIED)) {
						// Set de-certification date as current date
						existingAssister.setDeCertificationDate(new TSDate());

					} else if (EntityConstants.STATUS_CERTIFIED.equals(assister.getCertificationStatus())) {

						// Set certification date to current date
						existingAssister.setCertificationDate((assister.getCertificationDate()!=null)?assister.getCertificationDate():new TSDate());
						// Generating Assister Certification Number and setting
						// to the Assister if status is changed to Certified
						existingAssister.setCertificationNumber(generateAssisterCertificationNumber(existingAssister));

						// Set re-certification date to one year from current
						// date
						Calendar date = TSCalendar.getInstance();
						date.setTime(new TSDate());
						date.add(Calendar.YEAR, 1);
						
						//existingAssister.setReCertificationDate(date.getTime());
						existingAssister.setReCertificationDate((assister.getReCertificationDate()!=null)?assister.getReCertificationDate():date.getTime());
					}
					// Save the object
					assister = assisterService.saveAssister(existingAssister);

					// If the Assister has been certified, invoke IND54
					if (EntityConstants.STATUS_CERTIFIED.equals(assister.getCertificationStatus())) {
						LOGGER.info("Assister status changed to Certified");
						assister = assisterService.update(existingAssister, true);
					}
				} else {
					
					existingAssister.setCertificationDate(assister.getCertificationDate());
					existingAssister.setReCertificationDate(assister.getReCertificationDate());
					assister = assisterService.saveAssister(existingAssister);
					
				}

				// De-designating individuals associated with Assister
				if (isAssisterNotCertified(assister)) {
					dedesignateAssociatedIndividuals(assister);
				}

				assisterResponseDTO.setAssister(assister);
				assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
				assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

			} else {
				LOGGER.error("Save Assister Status : Invalid input. Unable to save Assister Status");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
			
			
		} catch (Exception exception) {
			 assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			 assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Exception occured in saveAssisterCertification." , exception);
			
		}
		
		try {
			assisterResponseDTOString  = writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while converting Object to JSON in saveAssisterCertification." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);// TODO: handle exception
		}
		LOGGER.info("saveAssisterCertification: END");
		
		return assisterResponseDTOString;
	}

	/**
	 * @param assister
	 * @return
	 */
	private boolean isAssisterNotCertified(Assister assister) {
		return assister.getCertificationStatus() != null
				&& !EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(assister.getCertificationStatus());
	}

	/**
	 * @param assister
	 * @param existingAssister
	 * @return
	 */
	private boolean compareCertificationStatus(Assister assister,
			Assister existingAssister) {
		return assister.getCertificationStatus() != null
				&& !assister.getCertificationStatus().equalsIgnoreCase(
						existingAssister.getCertificationStatus());
	}

	/**
	 * De-designating individuals associated with Assister
	 * 
	 * @param assister
	 *            {@link Assister}
	 */
	private void dedesignateAssociatedIndividuals(Assister assister) {
		
		LOGGER.info("dedesignateAssociatedIndividuals: START");
		
		LOGGER.info("AssisterAdminController : dedesignateAssociatedIndividuals : START");

		LOGGER.debug("Dedesignating individuals associated with assister : ", assister.getFirstName(),
				assister.getLastName());
		int numberOfDedesignatedIndividuals = assisterDesignateService.deDesignateIndividualsForAssister(assister.getId());
		LOGGER.debug("Number of dedesignated individuals : ", numberOfDedesignatedIndividuals);
		LOGGER.info("AssisterAdminController : dedesignateAssociatedIndividuals : END");
		
		LOGGER.info("dedesignateAssociatedIndividuals: END");
	}

	/**
	 * Retrieves {@link AssisterCertificationNumber} if exists, else generate
	 * assister certification number and set into {@link Assister} object
	 * 
	 * @param assister
	 *            {@link Assister}
	 * @return {@link Assister}
	 */
	private long generateAssisterCertificationNumber(Assister assister) {
		
		LOGGER.info("generateAssisterCertificationNumber: START");
		
		Long certificationNumber = null;

		if (assister.getCertificationNumber() != null) {
			certificationNumber = assister.getCertificationNumber();
		} else {
			LOGGER.debug("Generating unique assister certification number for Assister : " + assister.getId());
			AssisterCertificationNumber assisterCertificationNumber = new AssisterCertificationNumber();
			assisterCertificationNumber.setAssisterId(assister.getId());
			assisterCertificationNumber = assisterCertificationNumberRepository
					.saveAndFlush(assisterCertificationNumber);
			certificationNumber = assisterCertificationNumber.getId();
			LOGGER.debug("Assister Certification Number : " + certificationNumber);
		}

		LOGGER.info("generateAssisterCertificationNumber: END");
		
		return certificationNumber;
	}

	/**
	 * Invokes Assister service to update assister registration information
	 * 
	 * @param assisterRequestDTO
	 *            the object encapsulating assister details
	 * @return marshaled assister response DTO
	 */

	@RequestMapping(value = "/updateassisterdetails", method = RequestMethod.POST)
	@ResponseBody
	public String updateAssisterDetails(@RequestBody String requestXML) {
		
		LOGGER.info("updateAssisterDetails: START");
		
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String assisterResponseJSONStr=null;
		LOGGER.debug("Update Assister Detail Starts : Received Rest call from ghix-web.");
		
		try {
			
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML );
			
			Assister assister = assisterRequestDTO.getAssister();
			AssisterLanguages assisterLanguages = null;

			if (assister != null) {
				try {
					assister = assisterService.saveAssister(assisterRequestDTO.getAssister());
					assisterLanguages = assisterLanguagesService.findByAssister(assister);

					assisterResponseDTO.setAssister(assister);
					assisterResponseDTO.setAssisterLanguages(assisterLanguages);

					assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
					assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

				} catch (Exception exception) {
		
					LOGGER.error("Update Assister Detail : Exception occurred while updating Assister Detail : " , exception); 

					assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
					assisterResponseDTO.setResponseDescription(exception.getMessage());
				}
			} else {
				LOGGER.error("Update Assister Detail : Invalid input. Unable to Update Assister Detail");

				assisterResponseDTO.setResponseCode(EntityConstants.INVALID_INPUT_CODE);
				assisterResponseDTO.setResponseDescription(EntityConstants.INVALID_INPUT_MSG);
			}
			
		} catch (Exception exception) {
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Exception occured while updating assister details" , exception);
		}
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
			assisterResponseJSONStr = writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while converting Object to JSON in assister details" , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.debug("Update Assister Detail Ends.");

		LOGGER.info("updateAssisterDetails: END");
		return assisterResponseJSONStr;
	}

	/**
	 * Retrieves Assister object by moduleId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with Assister object
	 */
	@RequestMapping(value = "/getassisteradmindetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterAdminDetailsbyId(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO) {
		
		LOGGER.info("getAssisterAdminDetailsbyId: START");
		LOGGER.info("inside get EnrollmentEntity Details by Id info");
		String responseJSON  = null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);

		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			List<Map<String, Object>> assisterEntityHistory = assisterAdminService.loadAssisterStatusHistoryForAdmin(id);
			
			assisterResponseDTO.setAssisterEntityHistory(assisterEntityHistory);
			
			LOGGER.info("getAssisterAdminDetailsbyId: END");
			
		   responseJSON = writer.writeValueAsString(assisterResponseDTO);

		} catch (Exception exception) {
			LOGGER.error("Exception occured while getAssisterAdminDetailsbyId" , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		return responseJSON ;
	}

	/**
	 * Retrieves Assister object by moduleId
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with Assister object
	 */
	@RequestMapping(value = "/getassisterhistorydetailsbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterHistoryDetailsbyId(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO) {
		
		LOGGER.info("getAssisterHistoryDetailsbyId: START");
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		LOGGER.info("inside get EnrollmentEntity Details by Id info");
		String responseJSON  =null;
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			List<Map<String, Object>> assisterEntityHistory = assisterService.loadAssisterActivityStatusHistoryForAdmin(id);
			
			assisterResponseDTO.setAssisterEntityHistory(assisterEntityHistory);
			responseJSON  = writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getAssisterHistoryDetailsbyId." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssisterHistoryDetailsbyId: END");
		return responseJSON;
	}
	

	/**
	 * Fetches Assisters associated with all enrollment entities. Parameter
	 * AssisterRequestDTO Returns a String containing list of all available
	 * assisters based on search parameters
	 * 
	 * @param requestXML
	 *            xml representation of the AssisterRequestDTO}
	 * @return xml representation of AssisterResponseDTO
	 */
	@RequestMapping(value = "/getsearchassisterlist", method = RequestMethod.POST)
	@ResponseBody
	public String getSearchAssisterList(@RequestBody String requestXML) {
		
		LOGGER.info("getSearchAssisterList: START");
		
		LOGGER.info("Inside getAssisterList method in ghix-entity module");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		List<Assister> assisters = null;
		String assisterResponseDTOStr=null;
		try {
			
			ObjectReader reader = JacksonUtils.getJacksonObjectReaderForJavaType(AssisterRequestDTO.class);
			
			AssisterRequestDTO assisterRequestDTO = reader.readValue(requestXML );
			
			if(!assisterRequestDTO.isAdmin()) {
				assisters = assisterAdminService.searchAssisterForEntity(assisterRequestDTO); 
			}
			else {
				assisters = assisterAdminService.searchAssister(assisterRequestDTO);
			}
			assisterResponseDTO.setListOfAssisters(assisters);
			assisterResponseDTO.setTotalAssistersBySearch(assisterAdminService
					.totalNoOfAssistersBySearch(assisterRequestDTO));
			if(!assisterRequestDTO.isAdmin()) {
				assisterResponseDTO.setTotalAssisters(assisterAdminService.findTotalNoOfAssistersForEntity(assisterRequestDTO.getEnrollmentEntityId()));	
			}
			else{
				assisterResponseDTO.setTotalAssisters(assisterAdminService.findTotalNoOfAssisters());
			}
			assisterResponseDTO.setAssistersUpForRenewal(0);
			assisterResponseDTO.setInactiveAssisters(assisterAdminService.findAssistersByStatus("InActive"));
			 
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);

		} catch (Exception exception) {
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_FAILURE);
			LOGGER.error("Exception occured in retrieving list of assisters from db" , exception);
		}
		
		try {
			assisterResponseDTOStr = writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured while converting Object to JSON in getSearchAssisterList " , exception);
			
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("Exiting getAssisterList in ghix-entity module");
		
		LOGGER.info("getSearchAssisterList: END");
		return assisterResponseDTOStr;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getassisteradmindocbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterDocumentsdetailsbyId(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO) {
		
		LOGGER.info("getAssisterDocumentsdetailsbyId: START");
		
		LOGGER.info("inside get assister documents details by id info");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String assisterResponseJSONStr=null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		
		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			EntityDocuments entityDocuments = assisterAdminService.getAssisterDocumentsByRecordID(id);
			
			assisterResponseDTO.setAssisterDocuments(entityDocuments);
			assisterResponseJSONStr = writer.writeValueAsString(assisterResponseDTO);
			
		} catch (Exception exception) {
			LOGGER.error("Exception occured in retrieving list of assisters from db" , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssisterDocumentsdetailsbyId: END");
		
		return assisterResponseJSONStr;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getassisterdocfromecmbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterDocumentsFromECMbyid(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO) {
		
		LOGGER.info("getAssisterDocumentsFromECMbyid: START");
		
		LOGGER.info("inside retrieve assister document from ecm info");
		
		String assisterResponseJSONStr=null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		try {
			
			String strId = enrollmentEntityRequestDTO.getAssisterType();
			byte[] attachment = null;
			try {
				attachment = ecmService.getContentDataById(strId);
			} catch (ContentManagementServiceException e) {
				LOGGER.error("Unable to show attachment", e);
			}
			if (attachment == null) {
				attachment = "UNABLE TO SHOW ATTACHMENT.PLEASE CONTACT ADMIN".getBytes();
			}
			
			assisterResponseDTO.setAttachment(attachment);
			assisterResponseJSONStr = writer.writeValueAsString(assisterResponseDTO);
			
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getAssisterDocumentsFromECMbyid." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("getAssisterDocumentsFromECMbyid: END");
		
		return assisterResponseJSONStr;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/getassisteradminbyid", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterAdminDocDetailsbyId(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO) {
		
		LOGGER.info("getAssisterAdminDocDetailsbyId: START");
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		String assisterResponseJSONStr= null;
		LOGGER.info("inside get assister Details by Id info");
		try {
			Integer id = enrollmentEntityRequestDTO.getModuleId();
			Assister assister = assisterAdminService.getAssisterByRecordID(id);
			
			assisterResponseDTO.setAssister(assister);
			assisterResponseJSONStr = writer.writeValueAsString(assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in getAssisterAdminDocDetailsbyId." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		 
		
		LOGGER.info("getAssisterAdminDocDetailsbyId: END");
		return assisterResponseJSONStr;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 */
	@RequestMapping(value = "/saveassisterdoconecm", method = RequestMethod.POST)
	@ResponseBody
	public String saveAssisterDocumentsOnECM(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO)
			throws ContentManagementServiceException {
		
		LOGGER.info("saveAssisterDocumentsOnECM: START");
		
		LOGGER.info("inside save assister document on ecm info");
		String strDocumentId = null;
		String assisterResponseJSONStr=null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		 
		Integer assisterId = enrollmentEntityRequestDTO.getModuleId();
		byte[] attachment = enrollmentEntityRequestDTO.getAttachment();
		String docName = enrollmentEntityRequestDTO.getAssisterType();
		try {
			strDocumentId = ecmService.createContent(GhixConstants.ENROLLMENT_ENTITY_DOC_PATH + "/" + assisterId + "/"
					+ "ADD_SUPPORT_FOLDER", docName, attachment
					, ECMConstants.Entity.DOC_CATEGORY, ECMConstants.Entity.ASSISTER_DOC, null);
			assisterResponseDTO.setDocument(strDocumentId);
			assisterResponseJSONStr = writer.writeValueAsString(assisterResponseDTO);
		
		} catch (ContentManagementServiceException cms) {
			LOGGER.error("Unable to upload file" , cms);
			throw new ContentManagementServiceException("Unable to upload file" ,cms);
		}catch (Exception exception){
			LOGGER.error("Exception occured in getAssisterAdminDocDetailsbyId." , exception);
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("saveAssisterDocumentsOnECM: END");
		return assisterResponseJSONStr;
	}

	/**
	 * Retrieves PaymentMethod object by moduleId and enitityType
	 * 
	 * @param enrollmentEntityRequestDTO
	 * @return String response DTO String with PaymentMethod object
	 * 
	 */
	@RequestMapping(value = "/saveassisterdocuments", method = RequestMethod.POST)
	@ResponseBody
	public String saveAssisterDocument(@RequestBody AssisterRequestDTO enrollmentEntityRequestDTO)   {
		
		LOGGER.info("saveAssisterDocument: START");
		String assisterResponseJSONStr= null;
		ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(AssisterResponseDTO.class);
		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		
		LOGGER.info("inside retrieve population served info");
		
		try {
			EntityDocuments entityDocuments = enrollmentEntityRequestDTO.getEntityDocuments();
			EntityDocuments entityDocumentsNew = assisterAdminService.saveAssisterDocuments(entityDocuments);
			
			assisterResponseDTO.setAssisterDocuments(entityDocumentsNew);
			assisterResponseJSONStr = writer.writeValueAsString( assisterResponseDTO);
		} catch (Exception exception) {
			LOGGER.error("Exception occured in saveAssisterDocument." , exception);	 
			throw new GIRuntimeException(exception, ExceptionUtils.getFullStackTrace(exception), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.info("saveAssisterDocument: END");
		
		return assisterResponseJSONStr;
	}

	@RequestMapping(value = "/getassisterexportlist", method = RequestMethod.POST)
	@ResponseBody
	public String getAssisterExportList() {
		
		LOGGER.info("getassisterexportlist : START");

		AssisterResponseDTO assisterResponseDTO = new AssisterResponseDTO();
		String response = null;
		
		try {
			List<AssisterExportDTO> assisterExportList = assisterService.getAssisterExportList();
			assisterResponseDTO.setAssisterExportDTOList(assisterExportList);
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_SUCCESS);
			assisterResponseDTO.setResponseDescription(EntityConstants.RESPONSE_DESC_SUCCESS);
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while getting Assister Export List : "	, exception);
			assisterResponseDTO.setResponseCode(EntityConstants.RESPONSE_CODE_FAILURE);
			assisterResponseDTO.setResponseDescription(exception.getMessage());
		}

		try{
			response = JacksonUtils.getJacksonObjectWriter(AssisterResponseDTO.class).writeValueAsString(assisterResponseDTO);
		}catch(Exception ex){
			LOGGER.error("Unexpected error occured in while processing response for get assister Export List" , ex); 
			throw new GIRuntimeException(ex, ExceptionUtils.getFullStackTrace(ex), Severity.HIGH, GIRuntimeException.Component.AEE.toString(), null);
		}
		
		LOGGER.debug("Get Assister Export List Response : " + assisterResponseDTO);

		LOGGER.info("getassisterexportlist : END");
		
		return response;
	}
}
