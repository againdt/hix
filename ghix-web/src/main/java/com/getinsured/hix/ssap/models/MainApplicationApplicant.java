package com.getinsured.hix.ssap.models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.getinsured.iex.dto.SsapApplicantResource;

public class MainApplicationApplicant {

	private Map<String,DocumentCategory> documentCategoryStatusMap = new HashMap<String,DocumentCategory>();
	private boolean isDocumentCategoryVerified = false;
    private long applicantID;
	private boolean citizenshipAsAttestedIndicator;
	private boolean americanIndianAlaskaNativeIndicator;
	private String applicantDisplayName;
	
	public String getApplicantDisplayName() {
		return applicantDisplayName;
	}
	public void setApplicantDisplayName(String applicantDisplayName) {
		this.applicantDisplayName = applicantDisplayName;
	}
	public boolean isCitizenshipAsAttestedIndicator() {
		return citizenshipAsAttestedIndicator;
	}
	public void setCitizenshipAsAttestedIndicator(
			boolean citizenshipAsAttestedIndicator) {
		this.citizenshipAsAttestedIndicator = citizenshipAsAttestedIndicator;
	}
	public boolean isAmericanIndianAlaskaNativeIndicator() {
		return americanIndianAlaskaNativeIndicator;
	}
	public void setAmericanIndianAlaskaNativeIndicator(
			boolean americanIndianAlaskaNativeIndicator) {
		this.americanIndianAlaskaNativeIndicator = americanIndianAlaskaNativeIndicator;
	}
	public Map<String, DocumentCategory> getDocumentCategoryStatusMap() {
		return documentCategoryStatusMap;
	}

	public void setDocumentCategoryStatusMap(
			Map<String, DocumentCategory> documentCategoryStatusMap) {
		this.documentCategoryStatusMap = documentCategoryStatusMap;
	}

	public long getApplicantID() {
		return applicantID;
	}

	public void setApplicantID(long applicantID) {
		this.applicantID = applicantID;
	}
	
	public boolean isDocumentCategoryVerified() {
		return isDocumentCategoryVerified;
	}

	public void setDocumentCategoryVerified(boolean isDocumentCategoryVerified) {
		this.isDocumentCategoryVerified = isDocumentCategoryVerified;
	}
}
