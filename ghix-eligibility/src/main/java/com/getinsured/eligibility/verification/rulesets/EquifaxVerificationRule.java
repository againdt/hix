package com.getinsured.eligibility.verification.rulesets;

import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

/**
 * Encapsulates Equifax Verification Rule methods
 */
@Component
public class EquifaxVerificationRule {

private static final Logger LOGGER = LoggerFactory.getLogger(EquifaxVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) throws Exception {
		EquifaxVerificationRule equifaxVerificationRule = new EquifaxVerificationRule();
		equifaxVerificationRule.verifyEquifax(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadHUBJSON());
	}
	
	/**
	 * This rule will verify Equifax Status of Household Members
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @return verification status string
	 * @throws Exception 
	 */
	public boolean verifyEquifax(String ssapJson, String hubResponseJson) throws Exception {
		LOGGER.info("Verify Equifax Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonArray individuals;
		JsonObject singleStreamlinedApplication;
		JsonObject individual;
		String individualSSN;
		JsonArray currentIncomeInformations;
		JsonObject currentIncomeInformation;
		String householdIncome;
		float aggregatedCompensation = 0;
		float totalHouseholdIncome = 0;
		boolean checkAggregateTotalCompensation = false;
		String updatedSsapJson = null;
		long ssapApplicationId;
		
		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			
			if(!SSAPUtils.isEmpty(hubResponseJson)){
				individuals = getIndividualsInHubResponse(hubResponseJson);
				
				Iterator<JsonElement> iterator = individuals.iterator();
				while (iterator.hasNext()) {
					individual = iterator.next().getAsJsonObject();
					if(checkIndividualResponseCode(individual)){
						currentIncomeInformations = individual.get(SSAPConstants.CURRENT_INCOME_INFORMATION).getAsJsonArray();
						
						for(JsonElement element:currentIncomeInformations){
							currentIncomeInformation = element.getAsJsonObject();
							individualSSN = getIndividualSSN(currentIncomeInformation);
							if(verifyIndividualSSN(singleStreamlinedApplication, individualSSN)){
								if(checkEmployeeStatusCode(currentIncomeInformation)){
									if(checkIncomeYear(currentIncomeInformation)){
										aggregatedCompensation = aggregatedCompensation + getTotalCompensation(currentIncomeInformation);
										checkAggregateTotalCompensation = true;
									}
								}
							}
						}
					}
				}
				
				if(checkAggregateTotalCompensation){
					householdIncome = SSAPUtils.getHouseholdIncome(singleStreamlinedApplication);
					if(!SSAPUtils.isEmpty(householdIncome)){
						totalHouseholdIncome = Float.valueOf(householdIncome);
						if(totalHouseholdIncome >= aggregatedCompensation){
							status = SSAPConstants.VERIFIED;
						} else {
							if(SSAPUtils.checkEquifaxCompatibilityRange(totalHouseholdIncome, aggregatedCompensation)){
								status = SSAPConstants.VERIFIED;
								updatedSsapJson = setAggregatedEquifaxIncomeInSSAPJson(ssapJson, String.valueOf(aggregatedCompensation));
							} else {
								status = SSAPConstants.NOT_VERIFIED;
							}
						}
					}
				} else {
					status = SSAPConstants.NOT_VERIFIED;
				}
			} else {
				status = SSAPConstants.EQUIFAX_PENDING;
			}
			
			ssapApplicationId = Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication));
/*			if(!SSAPUtils.isEmpty(updatedSsapJson)){
				verificationRulesService.updateSsapJson(ssapApplicationId, updatedSsapJson);
			}
*/			
			verificationStatus = verificationRulesService.updateVerificationStatus(ssapApplicationId,
					getPrimaryApplicantPersonId(singleStreamlinedApplication), status, SSAPConstants.INCOME_VERIFICATION_RULE);
			verificationStatus = true;
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Equifax : ",exception);
			String rule = SSAPConstants.RULES.EQUIFAX_VERIFICATION_RULE.toString();
			giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
					hubResponseJson, null, null, null, rule), rule));
			throw exception;
		}
		
		LOGGER.info("Verify Equifax Rule : Ends ");
		
		return verificationStatus;
	}
	
	/**
	 * Extract list of Individuals from Hub Response
	 * 
	 * @param hubResponse
	 * @return
	 */
	private JsonArray getIndividualsInHubResponse(String hubResponse){
		JsonParser parser = new JsonParser();
		JsonArray individuals;
	
		try {
			Object obj = parser.parse(hubResponse);
			JsonObject jsonObject = (JsonObject) obj;
			
			individuals = jsonObject.get(SSAPConstants.RESPONSE_INFORMATION).getAsJsonArray();
		} catch (Exception exception) {
			throw exception;
		}
		
		return individuals;
	}
	
	/**
	 * Checks response code.e:g-HS000000 is for success
	 * 
	 * @param individual
	 * @return
	 */
	private boolean checkIndividualResponseCode(JsonObject individual){
		JsonObject responseMetadata;
		String responseCode;
		boolean flag = false;
		
		try {
			responseMetadata = individual.get(SSAPConstants.RESPONSE_METADATA).getAsJsonObject();
			responseCode = responseMetadata.get(SSAPConstants.RESPONSE_CODE).getAsString();
			
			if(SSAPConstants.EQUIFAX_SUCCESS.equals(responseCode)){
				flag = true;
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	/**
	 * Checks if the Employee's Employment is Active
	 * 
	 * @param currentIncomeInformation
	 * @return
	 */
	private boolean checkEmployeeStatusCode(JsonObject currentIncomeInformation){
		JsonObject employmentInformation;
		boolean flag = false;
		String employeeStatusCode;
		
		try {
			employmentInformation = currentIncomeInformation.get(SSAPConstants.EMPLOYMENT_INFORMATION).getAsJsonObject();
			employeeStatusCode = employmentInformation.get(SSAPConstants.EMPLOYEE_STATUS_CODE).getAsString();
			if(!SSAPUtils.isEmpty(employeeStatusCode)){
				if(checkActiveEmployeeStatusCode(employeeStatusCode)){
					flag = true;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	/**
	 * Checks if the Employee's Employment is Active
	 * 
	 * @param code
	 * @return
	 */
	private boolean checkActiveEmployeeStatusCode(String code){
		boolean flag = false;
		
		Map<String, String> employeeCodes = getEmployeeStatusCodes();
		for(Map.Entry<String, String> entry : employeeCodes.entrySet()){
			if(entry.getKey().equals(code)){
				String value[] = entry.getValue().split(SSAPConstants.COLON);
				if(value[1].equals(SSAPConstants.ACTIVE)){
					flag = true;
				}
				break;
			}
		}
		
		return flag;
	}
	
	/**
	 * Gets Individual's SSN from Hub Response
	 * 
	 * @param currentIncomeInformation
	 * @return
	 */
	private String getIndividualSSN(JsonObject currentIncomeInformation){
		JsonObject employeeInformation;
		String personSSNIdentification;
		
		try {
			employeeInformation = currentIncomeInformation.get(SSAPConstants.EMPLOYEE_INFORMATION).getAsJsonObject();
			personSSNIdentification = employeeInformation.get(SSAPConstants.EQUIFAX_PERSON_SSN_IDENTIFICATION).getAsString();
		} catch (Exception exception) {
			throw exception;
		}
		
		return personSSNIdentification;
	}
	
	/**
	 * Checks Income Year which must be current year
	 * 
	 * @param currentIncomeInformation
	 * @return
	 */
	private boolean checkIncomeYear(JsonObject currentIncomeInformation){
		JsonObject annualCompensation;
		JsonArray annualCompensationInformation;
		boolean flag = false;
		String incomeYear;
		
		try {
			annualCompensation = currentIncomeInformation.get(SSAPConstants.ANNUAL_COMPENSATION).getAsJsonObject();
			annualCompensationInformation = annualCompensation.get(SSAPConstants.ANNUAL_COMPENSATION_INFORMATION).getAsJsonArray();
			incomeYear = annualCompensationInformation.get(0).getAsJsonObject().get(SSAPConstants.INCOME_YEAR).getAsString();
			
			if(!SSAPUtils.isEmpty(incomeYear)){
				Calendar calendar = TSCalendar.getInstance();
				int year = calendar.get(Calendar.YEAR);
				if(incomeYear.equals(String.valueOf(year))){
					flag = true;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	/**
	 * Gets total compensation from Hub Response
	 * 
	 * @param currentIncomeInformation
	 * @return
	 */
	private float getTotalCompensation(JsonObject currentIncomeInformation){
		JsonObject annualCompensation;
		JsonArray annualCompensationInformation;
		String totalCompensation;
		float compensation = 0;
		
		try {
			annualCompensation = currentIncomeInformation.get(SSAPConstants.ANNUAL_COMPENSATION).getAsJsonObject();
			annualCompensationInformation = annualCompensation.get(SSAPConstants.ANNUAL_COMPENSATION_INFORMATION).getAsJsonArray();
			totalCompensation = annualCompensationInformation.get(0).getAsJsonObject().get(SSAPConstants.TOTAL_COMPENSATION).getAsString();
			if(!SSAPUtils.isEmpty(totalCompensation)){
				compensation = Float.valueOf(totalCompensation);
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return compensation;
	}
	
	/**
	 * Checks Individuals present in Hub Response against ssap json
	 * 
	 * @param singleStreamlinedApplication
	 * @param individualSSN
	 * @return
	 */
	private boolean verifyIndividualSSN(JsonObject singleStreamlinedApplication, String individualSSN){
		JsonArray taxHousehold;
		JsonObject householdMember;
		JsonArray householdMembers;
		JsonObject socialSecurityCard;
		String ssn;
		boolean flag = false;
	
		try {
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(SSAPConstants.HOUSEHOLD_MEMBER).getAsJsonArray();
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				socialSecurityCard = householdMember.get(SSAPConstants.SOCIAL_SECURITY_CARD).getAsJsonObject();
				ssn = SSAPUtils.formatSSN(socialSecurityCard.get(SSAPConstants.SOCIAL_SECURITY_NUMBER).getAsString());
				
				if(individualSSN.equals(ssn)){
					flag = true;
					break;
				}
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return flag;
	}
	
	/**
	 * Get Primary Applicant Person ID from ssap json
	 * 
	 * @param singleStreamlinedApplication
	 * @return
	 */
	private long getPrimaryApplicantPersonId(JsonObject singleStreamlinedApplication) throws Exception{
		JsonArray taxHousehold;
		JsonObject householdMember;
		JsonArray householdMembers;
		boolean flag = false;
		String personId = null;
		long primaryApplicantPersonId = 0;
	
		try {
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);
			householdMembers = taxHousehold.get(0).getAsJsonObject().get(SSAPConstants.HOUSEHOLD_MEMBER).getAsJsonArray();
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				if(SSAPConstants.PRIMARY_APPLICANT_PERSON_ID.equals(personId)){
					primaryApplicantPersonId = Long.valueOf(personId);
					flag = true;
					break;
				}
			}
			
			if(!flag){
				throw new Exception("Primary Applicant does not exist in SSAP Json");
			}
		} catch (Exception exception) {
			throw exception;
		}
		
		return primaryApplicantPersonId;
	}
	
	/**
	 * Sets aggregated total compensation in ssap json 
	 * 
	 * @param ssapJson
	 * @param aggregatedTotalCompensation
	 * @return
	 */
	private String setAggregatedEquifaxIncomeInSSAPJson(String ssapJson, String aggregatedTotalCompensation) throws Exception {

		JsonParser parser = new JsonParser();
		JsonObject  singleStreamlinedApplication;
		JsonArray taxHousehold;
		JsonObject jsonObject = null;

		try {
			Object obj = parser.parse(ssapJson);
			jsonObject = (JsonObject) obj;
			
			singleStreamlinedApplication = jsonObject.get(SSAPConstants.SINGLE_STREAMLINED_APPLICATION).getAsJsonObject();
			taxHousehold = singleStreamlinedApplication.getAsJsonArray(SSAPConstants.TAX_HOUSEHOLD);
			taxHousehold.get(0).getAsJsonObject().addProperty(SSAPConstants.VERIFIED_SOURCE_INCOME, aggregatedTotalCompensation);
		} catch (Exception exception) {
			throw exception;
		}
		
		return jsonObject.toString();
	}
	
	/**
	 * Loads Employee Status Codes
	 * 
	 * @return
	 */
	private Map<String, String> getEmployeeStatusCodes(){
		Map<String, String> codeDescriptions = new HashMap<>();
		codeDescriptions.put("1","Active:Active");
		codeDescriptions.put("2","On International Assignment:Active");
		codeDescriptions.put("3","Casual:Active");
		codeDescriptions.put("4","On Long Term Disability:Inactive");
		codeDescriptions.put("5","Sick Leave:Active");
		codeDescriptions.put("6","Surviving Spouse:Active");
		codeDescriptions.put("7","No Longer Employed:Inactive");
		codeDescriptions.put("8","Inactive:Inactive");
		codeDescriptions.put("9","On Leave:Active");
		codeDescriptions.put("10","Multiple Positions:Active");
		codeDescriptions.put("11","New Employee:Active");
		codeDescriptions.put("12","Lay Off:Inactive");
		codeDescriptions.put("13","Part Time:Active");
		codeDescriptions.put("14","Retired:Active");
		codeDescriptions.put("15","Separated:Inactive");
		codeDescriptions.put("16","No Longer Employed:Inactive");
		codeDescriptions.put("17","Seasonal:Active");
		codeDescriptions.put("18","Temporary:Active");
		codeDescriptions.put("19","Intern:Active");
		codeDescriptions.put("20","Transferred:Inactive");
		codeDescriptions.put("37","Deceased:Inactive");
		codeDescriptions.put("38","Severed with Pay:Inactive");
		codeDescriptions.put("42","Currently Employed:Active");
		codeDescriptions.put("43","On Sabbatical:Active");
		codeDescriptions.put("46","Part of Divested Population:Inactive");
		codeDescriptions.put("52","Temporarily Inactive:Active");
		codeDescriptions.put("53","Full-Time:Active");
		codeDescriptions.put("54","Non-employee Beneficiary:Active");
		codeDescriptions.put("55","Not Currently on Assignment:Inactive");
		codeDescriptions.put("56","Not Currently on Payroll:Inactive");
		
		return codeDescriptions;
	}
}
