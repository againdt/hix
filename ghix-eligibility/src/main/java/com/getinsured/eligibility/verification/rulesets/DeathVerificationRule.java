package com.getinsured.eligibility.verification.rulesets;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Encapsulates Death Verification Rule methods
 */
@Component
public class DeathVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(DeathVerificationRule.class);
	
	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	
	public static void main(String[] asdf) {
		
		//DeathVerificationRule deathVerificationRule = new DeathVerificationRule();
		//deathVerificationRule.verifyDeath(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadHUBJSON());
	}

	/**
	 * This rule will verify Death Status of Household Member
	 * 
	 * @param ssapJson
	 * @param hubResponseJson
	 * @throws Exception 
	 */
	public boolean verifyDeath(String ssapJson, String hubResponseJson, String pPersonId) throws Exception {
		LOGGER.info("Verify Death Rule : Starts");
		
		String status = null;
		boolean verificationStatus = false;
		JsonArray householdMembers;
		JsonObject householdMember;
		JsonObject socialSecurityCard;
		String personId;
		boolean deathIndicator;
		boolean personChecked = false;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				try {
					if(personId.equals(pPersonId)){
						socialSecurityCard = householdMember.get(SSAPConstants.SOCIAL_SECURITY_CARD).getAsJsonObject();
						deathIndicator = socialSecurityCard.get(SSAPConstants.DEATH_INDICATOR).getAsBoolean();
						
						if(deathIndicator){
							status = SSAPConstants.VERIFIED;
						} else {
							if(!SSAPUtils.isEmpty(hubResponseJson)){
								if(!SSAPConstants.NO_SSN.equalsIgnoreCase(hubResponseJson)){
									String isIndividualDeadAsPerSSA = SSAPUtils.searchHubResponseJson( 
											SSAPConstants.DEATH_CONFIRMATION_CODE, hubResponseJson);
									if(!SSAPUtils.isEmpty(isIndividualDeadAsPerSSA) && SSAPConstants.CONFIRMED.equals(isIndividualDeadAsPerSSA)){
										status = SSAPConstants.NOT_VERIFIED;
									} else {
										status = SSAPConstants.VERIFIED;
									}
								} else {
									status = SSAPConstants.NOT_VERIFIED;
								}
							} else {
								status = SSAPConstants.PENDING;
							}	
						}
						personChecked = true;
					}
					
					if(personChecked)break;
				} catch(Exception exception){
					LOGGER.error("Exception occurred while Verifying Death : Person : "+pPersonId, exception);
					String rule = SSAPConstants.RULES.DEATH_VERIFICATION_RULE.toString();
					giWSPayloadRepository.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, 
							hubResponseJson, null, null, null, rule), rule));
				}
			}
			
			if(!SSAPUtils.isEmpty(status)){
				verificationStatus = verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
						Long.valueOf(pPersonId), status, SSAPConstants.DEATH_VERIFICATION_RULE);
				verificationStatus = true;
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying Death : ",exception);
			throw exception;
		}
		
		LOGGER.info("Verify Death Rule : Ends ");
		
		return verificationStatus;
	}
}
