package com.getinsured.ssap.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.ssap.model.hub.vlp.VlpActionResult;
import com.getinsured.ssap.model.hub.vlp.VlpIntegrationLog;

/**
 * Repository interface for {@link VlpIntegrationLog}.
 *
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
@Repository
public interface VlpIntegrationLogRepository extends JpaRepository<VlpIntegrationLog, Long>
{
  /**
   * Returns list of {@link VlpIntegrationLog} for given applicantId.
   * @param applicantId applicant id.
   * @return list of {@link VlpIntegrationLog}
   */
  List<VlpIntegrationLog> findBySsapApplicantId(final Long applicantId);

  /**
   * Returns list of integration logs for given SSAP id.
   * @param applicationId ssap id.
   * @return list of {@link VlpIntegrationLog}.
   */
  List<VlpIntegrationLog> findBySsapApplicationId(final Long applicationId);

  /**
   * Finds VLP Integration records by given action result.
   *
   * @param actionResult {@link VlpActionResult}
   * @return list of records that matched given {@link VlpActionResult}.
   */
  List<VlpIntegrationLog> findByActionResult(VlpActionResult actionResult);

  /**
   * Finds VLP Integration records by given action result where retry count is less than given {@code maxRetryCount}
   * @param actionResult {@link VlpActionResult}.
   * @param maxRetryCount maximum retry count.
   * @return list of  {@link VlpIntegrationLog} records that match given criteria.
   */
  List<VlpIntegrationLog> findByActionResultAndRetryCountLessThan(VlpActionResult actionResult, int maxRetryCount);

  /**
   * Finds VLP Integration record by given SSAP id and Applicant Id.
   * @param applicationId SSAP id.
   * @param applicantId applicant id.
   * @return {@link VlpIntegrationLog} or {@code null}
   */
  VlpIntegrationLog findBySsapApplicationIdAndSsapApplicantId(final Long applicationId, final Long applicantId);
}
