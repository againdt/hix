<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.maskedinput.js" />"></script>

<div class="row">
	<div class="span4">
		<h2>Application</h2>
		<p></p>
	</div> 
	<div class="span12">
	<form class="form-stacked" id= "frmEditapplication" name="frmEditapplication" action="<c:url value="/enrollment/application"/>"   method="post">
	 <input type="hidden" name="id" id="id" value="${application.id}">
			<fieldset>
				
				<legend><spring:message code="${application.id == null ? 'label.Application' : 'label.EditApplication'}"/></legend>
				<div class="">
					<div class="clearfix">
						<label for="policyNumber" class="required"><spring:message  code="label.policyNumber"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="input">
							<input type="text" name="policyNumber" id="policyNumber" value="${application.policyNumber}" class="xlarge" size="60">
						</div>	
						<div id="policyNumber_error"></div>						
					</div>
					<div class="clearfix">
						<label for="status" class="required"><spring:message  code="label.status"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="input">
							<select name="status" id="status">
								<option value="" ${application.status == '' ? 'selected="selected"' : ''} >SELECT</option>
								<option value="SOLD"   ${application.status == 'SOLD' ? 'selected="selected"' : ''} >SOLD</option>
								<option value="PENDING" ${application.status == 'PENDING' ? 'selected="selected"' : ''} >PENDING</option>
								<option value="ESIG_PENDING" ${application.status == 'ESIG_PENDING' ? 'selected="selected"' : ''} >ESIG PENDING</option>
								<option value="APPROVED" ${application.status == 'APPROVED' ? 'selected="selected"' : ''} >APPROVED</option>
								<option value="DECLINED" ${application.status == 'DECLINED' ? 'selected="selected"' : ''} >DECLINED</option>
								<option value="WITHDRAWN" ${application.status == 'WITHDRAWN' ? 'selected="selected"' : ''} >WITHDRAWN</option>
								<option value="KICKBACK" ${application.status == 'KICKBACK' ? 'selected="selected"' : ''} >KICKBACK</option>
								<option value="SUSPENDED" ${application.status == 'SUSPENDED' ? 'selected="selected"' : ''} >SUSPENDED</option>
								<option value="DELETED" ${application.status == 'DELETED' ? 'selected="selected"' : ''} >DELETED</option>
								<option value="TERMINATED" ${application.status == 'TERMINATED' ? 'selected="selected"' : ''} >TERMINATED</option>
								<option value="PAYMENT_PROCESSED" ${application.status == 'PAYMENT_PROCESSED' ? 'selected="selected"' : ''} >PAYMENT PROCESSED</option>
								<option value="PAYMENT_FAILED" ${application.status == 'PAYMENT_FAILED' ? 'selected="selected"' : ''} >PAYMENT FAILED</option>
							</select>
						</div>
						<div id="status_error"></div>							
					</div>
					<div class="clearfix">
						<label for="premium" class="required"><spring:message  code="label.premium"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="input">
							<input type="text" name="premium" id="premium" value="${application.premium}" class="xlarge" size="60">
						</div>
						<div id="premium_error"></div>						
					</div>

					
					<div class="clearfix">
						<label for="effectiveDateMM" class="effectiveDateMM"><spring:message  code="label.effectiveDateMM"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="input">
							<input type="text" name="effectiveDateMM" id="effectiveDateMM" size="2" maxlength="2" placeholder="MM" onKeyUp="shiftbox(this,&#39;effectiveDateDD&#39;)" value="${effectiveDateStr[1]}"> 
							<input type="text" name="effectiveDateDD" id="effectiveDateDD" size="2" maxlength="2" placeholder="DD" onKeyUp="shiftbox(this,&#39;effectiveDateYY&#39;)" value="${effectiveDateStr[2]}"> 
							<input type="text" name="effectiveDateYY" id="effectiveDateYY" size="4" maxlength="4" placeholder="YYYY" value="${effectiveDateStr[0]}">
						</div>
						<div id="effectiveDateYY_error"></div>			
					</div>
					<div class="clearfix">
						<label for="terminationDateMM" class="terminationDateMM"><spring:message  code="label.terminationDateMM"/> <img src="<c:url value="/resources/images/requiredAsterix.png" />" width="10" height="10" alt="Required!" /></label>
						<div class="input">
							<input type="text" name="terminationDateMM" id="terminationDateMM" size="2" maxlength="2" placeholder="MM" onKeyUp="shiftbox(this,&#39;terminationDateDD&#39;)" value="${terminationDateStr[1]}"> 
							<input type="text" name="terminationDateDD" id="terminationDateDD" size="2" maxlength="2" placeholder="DD" onKeyUp="shiftbox(this,&#39;terminationDateYY&#39;)" value="${terminationDateStr[2]}"> 
							<input type="text" name="terminationDateYY" id="terminationDateYY" size="4" maxlength="4" placeholder="YYYY" value="${terminationDateStr[0]}">
						</div>
						<div id="terminationDateYY_error"></div>			
					</div>
					<div class="actions">
						<input type="submit" name="submit" id="submit" value="Submit" class="btn primary" title="Submit">
					</div>
				</div>
			</fieldset>
		</form>
	</div>
</div>

<script type="text/javascript">
function shiftbox(element,nextelement){
	maxlength = parseInt(element.getAttribute('maxlength'));
	if(element.value.length == maxlength){
   		nextelement = document.getElementById(nextelement);
   		nextelement.focus();
	}
}

function isValidDate(dtMonth,dtDay,dtYear,validateFor)
{
  	if (dtMonth < 1 || dtMonth > 12)
      	return false;
  	else if (dtDay < 1 || dtDay> 31)
      	return false;
  	else if ((dtMonth==4 || dtMonth==6 || dtMonth==9 || dtMonth==11) && dtDay ==31)
      	return false;
  	else if (dtMonth == 2)
  	{
     	var isleap = (dtYear % 4 == 0 && (dtYear % 100 != 0 || dtYear % 400 == 0));
    	if (dtDay> 29 || (dtDay ==29 && !isleap))
        	return false;
  	}
  	var today=new Date();
  	var givenDate=new Date();
  	givenDate.setFullYear(dtYear, dtMonth, dtDay);
  	
  	switch(validateFor){
  		case "past":
  			if(today.getTime() < givenDate.getTime()){ return false; }
  			break;
  		case "today":
  			if(today.getTime() != givenDate.getTime()){ return false; }
  			break;
  		case "future":
  			if(today.getTime() > givenDate.getTime()){ return false; }
  			break;
  		default:
  			return true;
  	}
  	return true;
}

jQuery.validator.addMethod("effectiveDateCheck", function(value, element, param) {
	effectiveDateMM = $("#effectiveDateMM").val(); effectiveDateDD = $("#effectiveDateDD").val(); effectiveDateYY = $("#effectiveDateYY").val(); 
	
	if(!isValidDate(effectiveDateMM,effectiveDateDD,effectiveDateYY,"future")){ return false; }
  	
	return true;
});

jQuery.validator.addMethod("terminationDateCheck", function(value, element, param) {
	terminationDateMM = $("#terminationDateMM").val(); terminationDateDD = $("#terminationDateDD").val(); terminationDateYY = $("#terminationDateYY").val(); 

	if(!isValidDate(terminationDateMM,terminationDateDD,terminationDateYY,"future")){ return false; }
  	
	return true;
});


var validator = $("#frmEditapplication").validate({ 
	rules : {policyNumber : {required : true},
		    status : { required : true},
			premium : { required : true, number : true},
			effectiveDateYY : { effectiveDateCheck: true},
			terminationDateYY : { terminationDateCheck: true},
	},
	messages : {
		policyNumber : { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePolicyNumber' javaScriptEscape='true'/></span>"},
		status: { required : "<span> <em class='excl'>!</em><spring:message code='label.validateStatus' javaScriptEscape='true'/></span>"},
		premium: { required : "<span> <em class='excl'>!</em><spring:message code='label.validatePremium' javaScriptEscape='true'/></span>",
					number : "<span> <em class='excl'>!</em><spring:message code='label.validatePremiumFormat' javaScriptEscape='true'/></span>",
				},
		effectiveDateYY: { effectiveDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateEffectiveDateCheck' javaScriptEscape='true'/></span>"},
		terminationDateYY: { terminationDateCheck : "<span> <em class='excl'>!</em><spring:message code='label.validateTerminationDateCheck' javaScriptEscape='true'/></span>"}
	},
	errorClass: "error",
	errorPlacement: function(error, element) {
		var elementId = element.attr('id');
		error.appendTo( $("#" + elementId + "_error"));
		$("#" + elementId + "_error").attr('class','error span10');
	} 
});

$(function() {
	$('#effectiveDateMM').mask('99',{placeholder:""});
	$('#effectiveDateDD').mask('99',{placeholder:""});
	$('#effectiveDateYY').mask('9999',{placeholder:""});
	$('#terminationDateMM').mask('99',{placeholder:""});
	$('#terminationDateDD').mask('99',{placeholder:""});
	$('#terminationDateYY').mask('9999',{placeholder:""});
});
</script>