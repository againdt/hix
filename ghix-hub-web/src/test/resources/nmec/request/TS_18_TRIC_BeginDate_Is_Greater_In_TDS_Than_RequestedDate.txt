{
  "applicationId": 50,
  "clientIp": "192.168.56.10",
  "operation": "",
  "payload":{
  "taxHousehold": [
    {
      "householdMember": [ {
        
          "dateOfBirth": "04/04/1967",
          "name": {
            "firstName": "Krish",
            "middleName": "C",
            "lastName": "Peter"
          },
          "socialSecurityCard": {
            "socialSecurityNumber": "222224900"
          },
          "gender": "M",
        "householdContact": {
          "homeAddress": {
            "state": "AZ"
          }
        },
        "insurance": {
          "insurancePolicyEffectiveDate": "01/01/2014",
          "insurancePolicyExpirationDate": "12/31/2014"
        },
        "organization": [
          {
            "organizationCode": "TRIC"
          }
        ]
      }  ]
    }
  ]
}}