package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getinsured.eligibility.enums.SsapApplicantPersonType;
import com.getinsured.iex.ssap.financial.*;
import com.getinsured.ssap.model.eligibility.HouseholdMemberResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Describes household member.
 *
 * @author chopra_s
 */
public class HouseholdMember
{
  private static final String PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE = "PTF";
  private static final String PRIMARY_CONTACT_APPLICANT_PERSON_TYPE = "PC";

  /**
   * This stores to which tax household this member belongs to. All members
   * that are part of primary tax household should have default value as {@code 0}
   */
  @Expose
  private int householdId = 0;

  @NotNull
  @Expose
  private Date dateOfBirth;

  @Expose
  private ParentCaretakerRelatives parentCaretakerRelatives = new ParentCaretakerRelatives();

  @Expose
  private SpecialCircumstances specialCircumstances = new SpecialCircumstances();

  @Expose
  private Boolean livesWithHouseholdContactIndicator = Boolean.TRUE;

  @Valid
  @Expose
  private SocialSecurityCard socialSecurityCard = new SocialSecurityCard();

  @Expose
  private Boolean strikerIndicator = Boolean.FALSE;

  @Expose
  private String birthCertificateType;

  @Valid
  @Expose
  private CitizenshipImmigrationStatus citizenshipImmigrationStatus = new CitizenshipImmigrationStatus();

  @Expose
  private String medicalExpense;

  @Expose
  private SpecialEnrollmentPeriod specialEnrollmentPeriod = new SpecialEnrollmentPeriod();

  @Expose
  private Boolean marriedIndicator = Boolean.FALSE;
  
  @Expose
  private String marriedIndicatorCode;

  @Expose
  private List<BloodRelationship> bloodRelationship = new ArrayList<BloodRelationship>();

  @Expose
  private Boolean drugFellowIndicator = Boolean.FALSE;

  @Expose
  private Boolean livingArrangementIndicator = Boolean.FALSE;

  @Expose
  private TaxFiler taxFiler = new TaxFiler();

  @Expose
  private String shelterAndUtilityExpense;

  @Expose
  private Boolean tobaccoUserIndicator = Boolean.FALSE;

  @Expose
  private String dependentCareExpense;

  @Expose
  private Boolean residencyVerificationIndicator = Boolean.FALSE;

  @Expose
  private TaxFilerDependant taxFilerDependant = new TaxFilerDependant();

  @Valid
  @Expose
  private Name name = new Name();

  @Expose
  private EthnicityAndRace ethnicityAndRace = new EthnicityAndRace();

  @NotNull
  @Expose
  private Integer personId;

  @Expose
  private int addressId;

  @NotEmpty
  @Expose
  private String gender;

  @Expose
  @SerializedName("SSIIndicator")
  private Boolean sSIIndicator = Boolean.FALSE;

  @Expose
  @SerializedName("IPVIndicator")
  private Boolean iPVIndicator = Boolean.FALSE;

  @Expose
  private Boolean householdContactIndicator = Boolean.FALSE;

  @Expose
  private AmericanIndianAlaskaNative americanIndianAlaskaNative = new AmericanIndianAlaskaNative();

  @Expose
  @Valid
  private HouseholdContact householdContact = new HouseholdContact();

  @Expose
  @SerializedName("PIDVerificationIndicator")
  private Boolean pIDVerificationIndicator = Boolean.FALSE;

  @Expose
  private String applicantGuid;

  @Expose
  @SerializedName("PIDIndicator")
  private Boolean pIDIndicator = Boolean.FALSE;

  @NotNull
  @Expose
  private Boolean applyingForCoverageIndicator = Boolean.FALSE;

  @Expose
  private Boolean planToFileFTRJontlyIndicator = Boolean.FALSE;

  @Expose
  private Boolean livesAtOtherAddressIndicator = Boolean.FALSE;

  @Expose
  private HealthCoverage healthCoverage = new HealthCoverage();

  @Expose
  private Boolean planToFileFTRIndicator = Boolean.FALSE;

  @Expose
  @Valid
  private OtherAddress otherAddress = new OtherAddress();

  @Expose
  private Boolean disabilityIndicator = Boolean.FALSE;

  @Expose
  private Boolean heatingCoolingindicator = Boolean.FALSE;

  @Expose
  private IncarcerationStatus incarcerationStatus = new IncarcerationStatus();

  @Expose
  private Boolean infantIndicator = Boolean.FALSE;

  @Expose
  private String childSupportExpense;

  @Expose
  private Boolean migrantFarmWorker;

  @Expose
  private String status = "ADD_NEW";

  @Expose
  private boolean medicaidChipDenial;

  @Expose
  private Boolean under26Indicator = Boolean.FALSE;

  @Expose
  private String externalId;

  @Expose
  @JsonFormat(shape = JsonFormat.Shape.STRING)
  private SsapApplicantPersonType applicantPersonType;

  private String requestRef;

  private String hardshipExempt;

  private String ecnNumber;

  /**
   * Flag tells us if person reconciled premium
   * tax credit on their tax return for any past year.
   */
  private boolean reconciledAptc;

  /**
   * Flag indicates if person is blind or disabled.
   */
  private boolean blindOrDisabled;

  /**
   * Flag indicates if person requires long term care.
   */
  private boolean longTermCare;

  /**
   * Flags for household composition.
   */
  private long taxHouseholdCompositionFlags = 1<<1|1<<4;

  /**
   * Users annual tax income information.
   */
  private AnnualTaxIncome annualTaxIncome = new AnnualTaxIncome();

  /**
   * Contains list of incomes for SSAP Financial Flow.
   */
  @Expose
  private List<Income> incomes = new ArrayList<>();

  /**
   * Contains list of expenses for SSAP Financial Flow.
   */
  @Expose
  private List<Expense> expenses = new ArrayList<>();

  /**
   * Indicates if Medicaid was denied.
   */
  private boolean medicaidDenied;

  /**
   * Date on which Medicaid was denied.
   */
  private Date medicaidDeniedDate;

  /**
   * If Medicaid denied due to Immigration Status.
   */
  private Boolean medicaidDeniedDueToImmigration = null;

  /**
   * Indicates if there was a change in Immigration Status since 5 years ago.
   */
  private Boolean changeInImmigrationSince5Year = null;

  /**
   * Indicates if there was a change in Immigration since Medicaid was denied.
   */
  private Boolean changeInImmigrationSinceMedicaidDenial = null;

  /**
   * Contains flag bucket object.
   *
   * {@see FlagBucket}
   */
  private FlagBucket flagBucket = new FlagBucket();

  /**
   * Tax household composition descriptor object.
   */
  private TaxHouseholdComposition taxHouseholdComposition = new TaxHouseholdComposition();

  /**
   * Holds employer sponsored coverage records. For each household member there could
   * be multiple records.
   */
  private List<EmployerSponsoredCoverage> employerSponsoredCoverage = new ArrayList<>();

  /**
   * This flag is set if person is barred for five years from having medicaid/chip
   * after (?) immigrating to US.
   */
  @Expose
  private boolean fiveYearBar;

  /**
   * This stores eligibility response (if available) from eligibility engine
   * when we do a pre-eligibility call to pre-determine if this member is Medicaid/CHIP
   * eligible.
   */
  private HouseholdMemberResponse eligibilityResponse;

  /**
   * Has employer sponsored insurancecoverage
   */
  @Expose
  private boolean hasESI;

  /**
   * Flag indicating if this member is seeking QHP plan.
   *
   * Default value is true, it will be set to false if pre-eligibility
   * call determines if this member is eligible for state subsidies.
   */
  @Expose
  private boolean seeksQhp = true;

  /**
   * Flag that holds boolean value if this member is a parent caretaker
   *
   * Default is {@code null}, because it will be forced on UI to be selected.
   */
  private Boolean parentCaretaker = null;

  /**
   * Flag that holds boolean value if this member is a full time student.
   *
   * Default is {@code null}, because it will be forced on UI to be selected.
   */
  private Boolean fullTimeStudent = null;

  /**
   * Returns flags for household composition.
   * @return flags for household composition.
   */
  public long getTaxHouseholdCompositionFlags() {
    return taxHouseholdCompositionFlags;
  }

  /**
   * Sets flags for household composition.
   * @param taxHouseholdCompositionFlags flags for household composition.
   */
  public void setTaxHouseholdCompositionFlags(long taxHouseholdCompositionFlags) {
    this.taxHouseholdCompositionFlags = taxHouseholdCompositionFlags;
  }

  /**
   * Returns annual tax income {@link AnnualTaxIncome}
   * @return {@link AnnualTaxIncome}
   */
  public AnnualTaxIncome getAnnualTaxIncome() {
    return annualTaxIncome;
  }

  /**
   * Sets annual tax income.
   * @param annualTaxIncome {@link AnnualTaxIncome}.
   */
  public void setAnnualTaxIncome(AnnualTaxIncome annualTaxIncome) {
    this.annualTaxIncome = annualTaxIncome;
  }

  /**
   * Returns list of {@link com.getinsured.iex.ssap.financial.Income}
   * @return list of {@link Income}
   */
  public List<Income> getIncomes() {
    return incomes;
  }

  /**
   * Sets list of incomes for this household member.
   * @param incomes {@link com.getinsured.iex.ssap.financial.Income}
   */
  public void setIncomes(final List<Income> incomes) {
    this.incomes = incomes;
  }

  /**
   * Returns list of {@link Expense} for this household member.
   * @return list of {@link Expense} objects or empty.
   */
  public List<Expense> getExpenses() {
    return expenses;
  }

  /**
   * Sets list of {@link Expense} for this household member.
   * @param expenses list of {@link Expense} objects.
   */
  public void setExpenses(final List<Expense> expenses) {
    this.expenses = expenses;
  }

  /**
   * Returns boolean if Medicaid was denied.
   * @return boolean value
   */
  public boolean isMedicaidDenied() {
    return medicaidDenied;
  }

  /**
   * Sets Medicaid denied flag.
   * @param medicaidDenied boolean flag for denial
   */
  public void setMedicaidDenied(final boolean medicaidDenied) {
    this.medicaidDenied = medicaidDenied;
  }

  /**
   * Returns Medicaid denial date.
   * @return date of denial.
   */
  public Date getMedicaidDeniedDate() {
    return medicaidDeniedDate;
  }

  /**
   * Sets Medicaid denial date.
   * @param medicaidDeniedDate denial date to set.
   */
  public void setMedicaidDeniedDate(final Date medicaidDeniedDate) {
    this.medicaidDeniedDate = medicaidDeniedDate;
  }

  /**
   * Returns boolean flag if Medicaid was denied due to immigration status.
   * @return boolean flag.
   */
  public Boolean isMedicaidDeniedDueToImmigration() {
    return medicaidDeniedDueToImmigration;
  }

  /**
   * Sets flag if Medicaid was denied due to immigration status.
   * @param medicaidDeniedDueToImmigration boolean flag.
   */
  public void setMedicaidDeniedDueToImmigration(final Boolean medicaidDeniedDueToImmigration) {
    this.medicaidDeniedDueToImmigration = medicaidDeniedDueToImmigration;
  }

  /**
   * Returns if there was a change in immigration since 5 years ago.
   * @return boolean flag.
   */
  public Boolean isChangeInImmigrationSince5Year() {
    return changeInImmigrationSince5Year;
  }

  /**
   * Sets boolean flag if there was a change in immigration since 5 years ago.
   * @param changeInImmigrationSince5Year boolean flag.
   */
  public void setChangeInImmigrationSince5Year(final Boolean changeInImmigrationSince5Year) {
    this.changeInImmigrationSince5Year = changeInImmigrationSince5Year;
  }

  /**
   * Returns boolean if there was a change in immigration status since
   * Medicaid denial date.
   * @return boolean flag.
   */
  public Boolean isChangeInImmigrationSinceMedicaidDenial() {
    return changeInImmigrationSinceMedicaidDenial;
  }

  /**
   * Sets flag if there was a change in immigration since Medicaid denial date.
   * @param changeInImmigrationSinceMedicaidDenial boolean flag.
   */
  public void setChangeInImmigrationSinceMedicaidDenial(final Boolean changeInImmigrationSinceMedicaidDenial) {
    this.changeInImmigrationSinceMedicaidDenial = changeInImmigrationSinceMedicaidDenial;
  }

  /**
   * Returns to which household id this member belongs to.
   * For primary tax household id it will return {@code 0}.
   *
   * @return household id this member belongs to (for multi-household scenario)
   */
  public int getHouseholdId() {
    return householdId;
  }

  /**
   * Sets household id this member should belong to. Primary tax household member
   * should always have {@code householdId == 0} in order to support existing applications
   * and account transfers.
   *
   * @param householdId household id for multi-household scenario.
   */
  public void setHouseholdId(int householdId) {
    this.householdId = householdId;
  }

  /**
   * Helper method to determine if this household member is part of
   * primary tax household.
   * @return {@code true} if current member is part of primary tax household, {@code false} otherwise.
   */
  public boolean isPartOfPrimaryTaxHousehold() {
    return this.householdId == 0;
  }

  public String getEcnNumber()
  {
    return ecnNumber;
  }

  public void setEcnNumber(String ecnNumber)
  {
    this.ecnNumber = ecnNumber;
  }

  public String getHardshipExempt()
  {
    return hardshipExempt;
  }

  public void setHardshipExempt(String hardshipExempt)
  {
    this.hardshipExempt = hardshipExempt;
  }

  public String getStatus()
  {
    return status;
  }

  public void setStatus(String status)
  {
    this.status = status;
  }

  public String getRequestRef()
  {
    return requestRef;
  }

  public void setRequestRef(String requestRef)
  {
    this.requestRef = requestRef;
  }

  public String getExternalId()
  {
    return externalId;
  }

  public void setExternalId(String externalId)
  {
    this.externalId = externalId;
  }

  public Date getDateOfBirth()
  {
    return dateOfBirth;
  }

  public void setDateOfBirth(Date dateOfBirth)
  {
    this.dateOfBirth = dateOfBirth;
  }

  public ParentCaretakerRelatives getParentCaretakerRelatives()
  {
    return parentCaretakerRelatives;
  }

  public void setParentCaretakerRelatives(ParentCaretakerRelatives parentCaretakerRelatives)
  {
    this.parentCaretakerRelatives = parentCaretakerRelatives;
  }

  public SpecialCircumstances getSpecialCircumstances()
  {
    return specialCircumstances;
  }

  public void setSpecialCircumstances(SpecialCircumstances specialCircumstances)
  {
    this.specialCircumstances = specialCircumstances;
  }

  public Boolean getLivesWithHouseholdContactIndicator()
  {
    return livesWithHouseholdContactIndicator;
  }

  public void setLivesWithHouseholdContactIndicator(Boolean livesWithHouseholdContactIndicator)
  {
    this.livesWithHouseholdContactIndicator = livesWithHouseholdContactIndicator;
  }

  public SocialSecurityCard getSocialSecurityCard()
  {
    return socialSecurityCard;
  }

  public void setSocialSecurityCard(SocialSecurityCard socialSecurityCard)
  {
    this.socialSecurityCard = socialSecurityCard;
  }

  public Boolean getStrikerIndicator()
  {
    return strikerIndicator;
  }

  public void setStrikerIndicator(Boolean strikerIndicator)
  {
    this.strikerIndicator = strikerIndicator;
  }

  public String getBirthCertificateType()
  {
    return birthCertificateType;
  }

  public void setBirthCertificateType(String birthCertificateType)
  {
    this.birthCertificateType = birthCertificateType;
  }

  public CitizenshipImmigrationStatus getCitizenshipImmigrationStatus()
  {
    return citizenshipImmigrationStatus;
  }

  public void setCitizenshipImmigrationStatus(CitizenshipImmigrationStatus citizenshipImmigrationStatus)
  {
    this.citizenshipImmigrationStatus = citizenshipImmigrationStatus;
  }

  public String getMedicalExpense()
  {
    return medicalExpense;
  }

  public void setMedicalExpense(String medicalExpense)
  {
    this.medicalExpense = medicalExpense;
  }

  public SpecialEnrollmentPeriod getSpecialEnrollmentPeriod()
  {
    return specialEnrollmentPeriod;
  }

  public void setSpecialEnrollmentPeriod(SpecialEnrollmentPeriod specialEnrollmentPeriod)
  {
    this.specialEnrollmentPeriod = specialEnrollmentPeriod;
  }

  public Boolean getMarriedIndicator()
  {
    return marriedIndicator;
  }

  public void setMarriedIndicator(Boolean marriedIndicator)
  {
    this.marriedIndicator = marriedIndicator;
  }
  
  public String getMarriedIndicatorCode() 
  {
	return marriedIndicatorCode;
  }

  public void setMarriedIndicatorCode(String marriedIndicatorCode) 
  {
	this.marriedIndicatorCode = marriedIndicatorCode;
  }

public List<BloodRelationship> getBloodRelationship()
  {
    return bloodRelationship;
  }

  public void setBloodRelationship(List<BloodRelationship> bloodRelationship)
  {
    this.bloodRelationship = bloodRelationship;
  }

  public Boolean getDrugFellowIndicator()
  {
    return drugFellowIndicator;
  }

  public void setDrugFellowIndicator(Boolean drugFellowIndicator)
  {
    this.drugFellowIndicator = drugFellowIndicator;
  }

  public Boolean getLivingArrangementIndicator()
  {
    return livingArrangementIndicator;
  }

  public void setLivingArrangementIndicator(Boolean livingArrangementIndicator)
  {
    this.livingArrangementIndicator = livingArrangementIndicator;
  }

  public TaxFiler getTaxFiler()
  {
    return taxFiler;
  }

  public void setTaxFiler(TaxFiler taxFiler)
  {
    this.taxFiler = taxFiler;
  }

  public String getShelterAndUtilityExpense()
  {
    return shelterAndUtilityExpense;
  }

  public void setShelterAndUtilityExpense(String shelterAndUtilityExpense)
  {
    this.shelterAndUtilityExpense = shelterAndUtilityExpense;
  }

  public Boolean getTobaccoUserIndicator()
  {
    return tobaccoUserIndicator;
  }

  public void setTobaccoUserIndicator(Boolean tobaccoUserIndicator)
  {
    this.tobaccoUserIndicator = tobaccoUserIndicator;
  }

  public String getDependentCareExpense()
  {
    return dependentCareExpense;
  }

  public void setDependentCareExpense(String dependentCareExpense)
  {
    this.dependentCareExpense = dependentCareExpense;
  }

  public Boolean getResidencyVerificationIndicator()
  {
    return residencyVerificationIndicator;
  }

  public void setResidencyVerificationIndicator(Boolean residencyVerificationIndicator)
  {
    this.residencyVerificationIndicator = residencyVerificationIndicator;
  }

  public TaxFilerDependant getTaxFilerDependant()
  {
    return taxFilerDependant;
  }

  public void setTaxFilerDependant(TaxFilerDependant taxFilerDependant)
  {
    this.taxFilerDependant = taxFilerDependant;
  }

  public Name getName()
  {
    return name;
  }

  public void setName(Name name)
  {
    this.name = name;
  }

  public EthnicityAndRace getEthnicityAndRace()
  {
    return ethnicityAndRace;
  }

  public void setEthnicityAndRace(EthnicityAndRace ethnicityAndRace)
  {
    this.ethnicityAndRace = ethnicityAndRace;
  }

  public Integer getPersonId()
  {
    return personId;
  }

  public void setPersonId(Integer personId)
  {
    this.personId = personId;
  }

  public String getGender()
  {
    return gender;
  }

  public void setGender(String gender)
  {
    this.gender = gender;
  }

  public Boolean getSSIIndicator()
  {
    return sSIIndicator;
  }

  public void setSSIIndicator(Boolean sSIIndicator)
  {
    this.sSIIndicator = sSIIndicator;
  }

  public Boolean getIPVIndicator()
  {
    return iPVIndicator;
  }

  public void setIPVIndicator(Boolean iPVIndicator)
  {
    this.iPVIndicator = iPVIndicator;
  }

  public Boolean getHouseholdContactIndicator()
  {
    return householdContactIndicator;
  }

  public void setHouseholdContactIndicator(Boolean householdContactIndicator)
  {
    this.householdContactIndicator = householdContactIndicator;
  }

  public AmericanIndianAlaskaNative getAmericanIndianAlaskaNative()
  {
    return americanIndianAlaskaNative;
  }

  public void setAmericanIndianAlaskaNative(AmericanIndianAlaskaNative americanIndianAlaskaNative)
  {
    this.americanIndianAlaskaNative = americanIndianAlaskaNative;
  }

  public HouseholdContact getHouseholdContact()
  {
    return householdContact;
  }

  public void setHouseholdContact(HouseholdContact householdContact)
  {
    this.householdContact = householdContact;
  }

  public Boolean getPIDVerificationIndicator()
  {
    return pIDVerificationIndicator;
  }

  public void setPIDVerificationIndicator(Boolean pIDVerificationIndicator)
  {
    this.pIDVerificationIndicator = pIDVerificationIndicator;
  }

  public String getApplicantGuid()
  {
    return applicantGuid;
  }

  public void setApplicantGuid(String applicantGuid)
  {
    this.applicantGuid = applicantGuid;
  }

  public Boolean getPIDIndicator()
  {
    return pIDIndicator;
  }

  public void setPIDIndicator(Boolean pIDIndicator)
  {
    this.pIDIndicator = pIDIndicator;
  }

  public Boolean getApplyingForCoverageIndicator()
  {
    return applyingForCoverageIndicator;
  }

  public void setApplyingForCoverageIndicator(Boolean applyingForCoverageIndicator)
  {
    this.applyingForCoverageIndicator = applyingForCoverageIndicator;
  }

  public Boolean getPlanToFileFTRJontlyIndicator()
  {
    return planToFileFTRJontlyIndicator;
  }

  public void setPlanToFileFTRJontlyIndicator(Boolean planToFileFTRJontlyIndicator)
  {
    this.planToFileFTRJontlyIndicator = planToFileFTRJontlyIndicator;
  }

  public Boolean getLivesAtOtherAddressIndicator()
  {
    return livesAtOtherAddressIndicator;
  }

  public void setLivesAtOtherAddressIndicator(Boolean livesAtOtherAddressIndicator)
  {
    this.livesAtOtherAddressIndicator = livesAtOtherAddressIndicator;
  }

  public HealthCoverage getHealthCoverage()
  {
    return healthCoverage;
  }

  public void setHealthCoverage(HealthCoverage healthCoverage)
  {
    this.healthCoverage = healthCoverage;
  }

  public Boolean getPlanToFileFTRIndicator()
  {
    return planToFileFTRIndicator;
  }

  public void setPlanToFileFTRIndicator(Boolean planToFileFTRIndicator)
  {
    this.planToFileFTRIndicator = planToFileFTRIndicator;
  }

  public OtherAddress getOtherAddress()
  {
    return otherAddress;
  }

  public void setOtherAddress(OtherAddress otherAddress)
  {
    this.otherAddress = otherAddress;
  }

  public Boolean getDisabilityIndicator()
  {
    return disabilityIndicator;
  }

  public void setDisabilityIndicator(Boolean disabilityIndicator)
  {
    this.disabilityIndicator = disabilityIndicator;
  }

  public Boolean getHeatingCoolingindicator()
  {
    return heatingCoolingindicator;
  }

  public void setHeatingCoolingindicator(Boolean heatingCoolingindicator)
  {
    this.heatingCoolingindicator = heatingCoolingindicator;
  }

  public IncarcerationStatus getIncarcerationStatus()
  {
    return incarcerationStatus;
  }

  public void setIncarcerationStatus(IncarcerationStatus incarcerationStatus)
  {
    this.incarcerationStatus = incarcerationStatus;
  }

  public Boolean getInfantIndicator()
  {
    return infantIndicator;
  }

  public void setInfantIndicator(Boolean infantIndicator)
  {
    this.infantIndicator = infantIndicator;
  }

  public String getChildSupportExpense()
  {
    return childSupportExpense;
  }

  public void setChildSupportExpense(String childSupportExpense)
  {
    this.childSupportExpense = childSupportExpense;
  }

  public Boolean getMigrantFarmWorker()
  {
    return migrantFarmWorker;
  }

  public void setMigrantFarmWorker(Boolean migrantFarmWorker)
  {
    this.migrantFarmWorker = migrantFarmWorker;
  }

  public Boolean getUnder26Indicator()
  {
    return under26Indicator;
  }

  public void setUnder26Indicator(Boolean under26Indicator)
  {
    this.under26Indicator = under26Indicator;
  }

  public SsapApplicantPersonType getApplicantPersonType()
  {
    return applicantPersonType;
  }

  public void setApplicantPersonType(SsapApplicantPersonType applicantPersonType)
  {
    this.applicantPersonType = applicantPersonType;
  }

  public boolean isPrimaryContact()
  {
	  boolean isPrimaryContact = false;
	  if(this.applicantPersonType == null){
		  if(this.personId == 1){
			  isPrimaryContact = true;
		  }
	  }
	  else{
		  isPrimaryContact = this.applicantPersonType.getPersonType().contains(PRIMARY_CONTACT_APPLICANT_PERSON_TYPE);	  
	  }
	  return isPrimaryContact;
  }

  public boolean isPrimaryTaxFiler()
  {
	  boolean isPrimaryTaxFiler = false;
	  if(this.applicantPersonType == null){
		  if(this.personId == 1){
			  isPrimaryTaxFiler = true;
		  }
	  }
	  else{
		  isPrimaryTaxFiler = this.applicantPersonType.getPersonType().contains(PRIMARY_TAX_FILER_APPLICANT_PERSON_TYPE);
	  }
	  return isPrimaryTaxFiler;
  }

  public boolean isMedicaidChipDenial() {
    return medicaidChipDenial;
  }

  public void setMedicaidChipDenial(boolean medicaidChipDenial) {
    this.medicaidChipDenial = medicaidChipDenial;
  }

  /**
   * Returns boolean {@code true} if person is blind or disabled.
   * @return boolean flag
   */
  public boolean isBlindOrDisabled()
  {
    return blindOrDisabled;
  }

  /**
   * Sets boolean flag if person is blind or disabled.
   * @param blindOrDisabled boolean flag.
   */
  public void setBlindOrDisabled(final boolean blindOrDisabled)
  {
    this.blindOrDisabled = blindOrDisabled;
  }

  /**
   * Returns boolean {@code true} if person requires long term care.
   * @return boolean flag.
   */
  public boolean isLongTermCare()
  {
    return longTermCare;
  }

  /**
   * Sets flag that indicates if person is required to have long term care.
   * @param longTermCare boolean flag.
   */
  public void setLongTermCare(final boolean longTermCare)
  {
    this.longTermCare = longTermCare;
  }

  /**
   * Tells us if person reconciled premium
   * tax credit on their tax return for any past year.
   *
   * @return true if they reconciled, default false.
   */
  public boolean isReconciledAptc() {
    return reconciledAptc;
  }

  /**
   * Sets boolean flag if person reconciled premium
   * tax credit on their tax return for any past year.
   */
  public void setReconciledAptc(boolean reconciledAptc) {
    this.reconciledAptc = reconciledAptc;
  }

  /**
   * Returns {@link FlagBucket} object associated with this household member.
   * @return {@link FlagBucket}
   */
  public FlagBucket getFlagBucket() {
    return flagBucket;
  }

  /**
   * Sets {@link FlagBucket} for this household member.
   * @param flagBucket {@link FlagBucket} object to set.
   */
  public void setFlagBucket(FlagBucket flagBucket) {
    this.flagBucket = flagBucket;
  }

  /**
   * Returns {@link TaxHouseholdComposition} object associated with this household member.
   * @return {@link TaxHouseholdComposition}
   */
  public TaxHouseholdComposition getTaxHouseholdComposition() {
    return taxHouseholdComposition;
  }

  /**
   * Sets {@link TaxHouseholdComposition} object to be associated with this household member.
   * @param taxHouseholdComposition {@link TaxHouseholdComposition} object to set.
   */
  public void setTaxHouseholdComposition(TaxHouseholdComposition taxHouseholdComposition) {
    this.taxHouseholdComposition = taxHouseholdComposition;
  }

  /**
   * Returns employer sponsored coverage records {@link EmployerSponsoredCoverage}.
   * @return List of {@link EmployerSponsoredCoverage}.
   */
  public List<EmployerSponsoredCoverage> getEmployerSponsoredCoverage() {
    return employerSponsoredCoverage;
  }

  /**
   * Sets employer sponsored coverage records for this household.
   * @param employerSponsoredCoverage List of {@link EmployerSponsoredCoverage} records to set.
   */
  public void setEmployerSponsoredCoverage(List<EmployerSponsoredCoverage> employerSponsoredCoverage) {
    this.employerSponsoredCoverage = employerSponsoredCoverage;
  }

  /**
   * Returns address id associated with this person.
   * @return address id associated with this person.
   */
  public int getAddressId() {
    return addressId;
  }

  /**
   * Sets address id associated with this person.
   * @param addressId address id associated with this person.
   */
  public void setAddressId(int addressId) {
    this.addressId = addressId;
  }

  /**
   * Returns if Five Year bar is applied to this person.
   * @return boolean
   */
  public boolean isFiveYearBar() {
    return fiveYearBar;
  }

  /**
   * Sets Five year bar rule to this person.
   * @param fiveYearBar boolean flag to set.
   */
  public void setFiveYearBar(boolean fiveYearBar) {
    this.fiveYearBar = fiveYearBar;
  }

  /**
   * Returns last eligibility response for this member.
   * @return {@link HouseholdMemberResponse}
   */
  public HouseholdMemberResponse getEligibilityResponse() {
    return eligibilityResponse;
  }

  /**
   * Sets eligibility engine response for this member.
   * @param eligibilityResponse {@link HouseholdMemberResponse} to set.
   */
  public void setEligibilityResponse(HouseholdMemberResponse eligibilityResponse) {
    this.eligibilityResponse = eligibilityResponse;
  }

  public boolean isHasESI() {
    return hasESI;
  }

  public void setHasESI(boolean hasESI) {
    this.hasESI = hasESI;
  }

  /**
   * Returns flag indicating if this member is seeking QHP plan.
   *
   * @return {@code true} if member is seeking QHP, {@code false} decided
   * to leverage state subsidies.
   */
  public boolean isSeeksQhp() {
    return seeksQhp;
  }

  /**
   * Sets boolean flag if this member is seeking QHP (Qualified Health Plan).
   * @param seeksQhp boolean flag to set.
   */
  public void setSeeksQhp(boolean seeksQhp) {
    this.seeksQhp = seeksQhp;
  }

  /**
   * Returns flag indicating if this member is parent caretaker.
   * @return Boolean value or null if it was not selected on the UI.
   */
  public Boolean getParentCaretaker() {
    return parentCaretaker;
  }

  /**
   * Sets boolean flag indicating if this member is a parent caretaker.
   * @param parentCaretaker boolean flag to set.
   */
  public void setParentCaretaker(Boolean parentCaretaker) {
    this.parentCaretaker = parentCaretaker;
  }

  /**
   * Returns flag indicating if this member is a full time student.
   * @return Boolean value or null if it was not selected on the UI.
   */
  public Boolean getFullTimeStudent() {
    return fullTimeStudent;
  }

  /**
   * Sets boolean flag indicating if this member is a full time student.
   * @param fullTimeStudent boolean flag to set.
   */
  public void setFullTimeStudent(Boolean fullTimeStudent) {
    this.fullTimeStudent = fullTimeStudent;
  }
}
