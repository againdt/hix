package com.getinsured.hix.entity.admin.service.impl;

import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.ATTACHMENT;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.ATTACHMENT_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.COMMENTS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.COMMENT_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.MODEL_NAME;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.NEW_STATUS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.PREVIOUS_STATUS;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.REPOSITORY_NAME;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.STATUS_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.UPDATED_DATE_COL;
import static com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl.UPDATE_DATE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.entity.admin.repository.IEntityDocumentRepository;
import com.getinsured.hix.entity.admin.service.EntityAdminService;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.entity.enrollmententity.history.EnrollmentEntityHistoryRendererImpl;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.entity.EnrollmentEntity;
import com.getinsured.hix.model.entity.EntityDocuments;
import com.getinsured.hix.entity.util.EntityUtils;
import com.getinsured.hix.platform.audit.service.DisplayAuditService;

/**
 * Implements {@link EntityAdminService} to perform Entity Admin related
 * operations like find,save,update etc.
 */
@Service("entityAdminService")
@Transactional
public class EntityAdminServiceImpl implements EntityAdminService , ApplicationContextAware{

	private static final String END_RECORD = "endRecord";

	private static final String START_RECORD = "startRecord";

	private static final String TO_DATE = "toDate";

	private static final String FROM_DATE = "fromDate";

	private static final String IN_ACTIVE = "InActive";

	private static final String ACTIVE = "Active";

	private static final String CERTIFICATION_STATUS = "certificationStatus";

	private static final String ASSISTER_LAST_NAME = "assisterLastName";

	private static final String ASSISTER_FIRST_NAME = "assisterFirstName";

	private static final Logger LOGGER = LoggerFactory.getLogger(EntityAdminServiceImpl.class);

	@Autowired
	private ObjectFactory<DisplayAuditService> objectFactory;

	@Autowired
	private EnrollmentEntityHistoryRendererImpl enrollmentEntityHistoryRendererImpl;

	@Autowired
	private IEntityDocumentRepository documentRepository;
	@PersistenceUnit
	private EntityManagerFactory emf;

	private ApplicationContext context;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final int FIVE = 5;
	private static final int SIX = 6;

	private static final String DATE_STRING = "Date";
	private static final String NEW_CERTIFICATION_STATUS = "New Status";
	private static final String VIEW_COMMENT = "View Comment";
	private static final String VIEW_ATTACHMENT = "View Attachment";

	/**
	 * @see EntityAdminService#loadEntityrStatusHistory(entityId)
	 * 
	 * @param entityId
	 * 
	 * @return list of Entities Status History Map
	 */
	@Override
	public List<Map<String, Object>> loadEntityrStatusHistoryForAdmin(Integer entityId) {
		
		LOGGER.info("loadEntityrStatusHistoryForAdmin: START");
		
		Map<String, String> requiredFieldsMap = new LinkedHashMap<String, String>();
		requiredFieldsMap.put(UPDATED_DATE_COL, DATE_STRING);
		requiredFieldsMap.put(STATUS_COL, NEW_CERTIFICATION_STATUS);
		requiredFieldsMap.put(COMMENT_COL, VIEW_COMMENT);
		requiredFieldsMap.put(ATTACHMENT_COL, VIEW_ATTACHMENT);

		List<String> compareCols = new ArrayList<String>();
		compareCols.add(STATUS_COL);

		List<Integer> displayCols = new ArrayList<Integer>();
		displayCols.add(UPDATE_DATE);
		displayCols.add(PREVIOUS_STATUS);
		displayCols.add(NEW_STATUS);
		displayCols.add(COMMENTS);
		displayCols.add(ATTACHMENT);

		List<Map<String, Object>> data = null;
		try {
			DisplayAuditService service = objectFactory.getObject();
			service.setApplicationContext(context);
			List<Map<String, String>> entityHistoryData = service.findRevisions(REPOSITORY_NAME, MODEL_NAME,
					requiredFieldsMap, entityId);
			data = enrollmentEntityHistoryRendererImpl.processData(entityHistoryData, compareCols, displayCols);
		} catch (Exception e) {
			LOGGER.error("Exception occurred while loading Entity Status History" , e);
		}
	
		LOGGER.info("loadEntityrStatusHistoryForAdmin: END");
		
		return data;
	}

	/**
	 * @see EntityAdminService#getBrokerDocumentsByRecordID(Integer)
	 * 
	 * @param id
	 *            Entity Id
	 * 
	 * 
	 * @return {@link BrokerDocuments}
	 */
	@Override
	public EntityDocuments getEntityDocumentsByRecordID(Integer id) {
		
		LOGGER.info("getEntityDocumentsByRecordID: START");
		LOGGER.info("getEntityDocumentsByRecordID: END");
		
		return documentRepository.findOne(id);
	}

	/**
	 * @see EntityAdminService#saveBrokerDocuments(BrokerDocuments)
	 * 
	 * @param brokerDocuments
	 *            {@link BrokerDocuments}
	 * 
	 * 
	 * @return {@link BrokerDocuments}
	 */
	@Override
	public EntityDocuments saveEntityDocuments(EntityDocuments entityDocuments) {
		
		LOGGER.info("saveEntityDocuments: START");
		LOGGER.info("saveEntityDocuments: END");
		
		return documentRepository.save(entityDocuments);
	}

	private <E> void createAssisterList(List<Assister> assisterList, List<E> rsList) {
		
		LOGGER.info("createAssisterList: START");
		
		Iterator<E> rsIterator = rsList.iterator();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date convertedDate = null;
		while (rsIterator.hasNext()) {
			Object[] objArray = (Object[]) rsIterator.next();
			Assister a = new Assister();
			if (objArray[0] != null) {
				a.setFirstName(objArray[0].toString());
			}
			if (objArray[1] != null) {
				a.setLastName(objArray[1].toString());

			}
			if (objArray[2] != null) {
				EnrollmentEntity enrollmentEntity = new EnrollmentEntity();
				enrollmentEntity.setEntityName(objArray[2].toString());
				a.setEntity(enrollmentEntity);
			}
			if (objArray[THREE] != null) {
				a.setCertificationStatus(objArray[THREE].toString());
			}
			if (objArray[FOUR] != null) {
				a.setId(Integer.valueOf(objArray[FOUR].toString()));
			}
			if (objArray[FIVE] != null) {
				try {
					convertedDate = (Date) dateFormat.parse(objArray[FIVE].toString());
					a.setReCertificationDate(convertedDate);
				} catch (Exception exception) {
					LOGGER.error("Error occured while parsing date" , exception);
				}
			}
			if (objArray[SIX] != null) {
				a.setStatus(objArray[SIX].toString());
			}
			assisterList.add(a);
		}

		LOGGER.info("createAssisterList: END");
	}

	/**
	 * @see EntityAdminService#searchAssister(AssisterRequestDTO)
	 * 
	 * @param assisterRequestDTO
	 *            {@link AssisterRequestDTO}
	 * 
	 * 
	 * @return list of Assisters
	 */
	@Override
	public List<Assister> searchAssister(AssisterRequestDTO assisterRequestDTO) {
		
		LOGGER.info("searchAssister: START");
		
		StringBuilder query = new StringBuilder();
		EntityManager em = null;
		Assister assister = assisterRequestDTO.getAssister();
		List<Assister> assisterList =null;
		try {
			EnrollmentEntity enrollmentEntity = assisterRequestDTO.getEnrollmentEntity();
			 em = emf.createEntityManager();
			query.append("SELECT A.FIRST_NAME,A.LAST_NAME,E.ENTITY_NAME,A.CERTIFICATION_STATUS,A.ID,A.RECERTIFICATION_DATE,A.STATUS FROM EE_ASSISTERS A,EE_ENTITIES E WHERE A.EE_ENTITY_ID =E.ID AND  A.EE_ENTITY_ID=")
			.append(enrollmentEntity.getId());
		
			EntityUtils.appendToSearchAssisterQuery(query, "A.FIRST_NAME", assister.getFirstName(), ASSISTER_FIRST_NAME, "LIKE", true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.LAST_NAME", assister.getLastName(), ASSISTER_LAST_NAME, "LIKE", true, false);
			EntityUtils.appendToSearchAssisterQuery(query, "A.CERTIFICATION_STATUS", assister.getCertificationStatus(), CERTIFICATION_STATUS, "=", false, false);
			
			if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, "A.STATUS", assisterRequestDTO.getStatusactive(), ACTIVE, "=", false, false);
			}
			else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
				EntityUtils.appendToSearchAssisterQuery(query, "A.STATUS", assisterRequestDTO.getStatusInactive(), IN_ACTIVE, "=", false, false);
			}
			else {
				query.append(" AND A.STATUS IN (:"+ACTIVE+",:"+IN_ACTIVE+")");
			}
			
			String fromDate = assisterRequestDTO.getFromDate();
			String toDate = assisterRequestDTO.getToDate();
			
			if (fromDate != null && !"".equals(fromDate)) {
				query.append(" AND A.RECERTIFICATION_DATE >=to_date(:" + FROM_DATE + ",'mm-dd-yyyy')");
			}
			if (toDate != null && !"".equals(toDate)) {
				query.append("AND A.RECERTIFICATION_DATE <= to_date(:" + TO_DATE + ",'mm-dd-yyyy') + 1");
			}
			//query.append(" AND ROWNUM >= :" + START_RECORD + " AND ROWNUM < :" + END_RECORD);
			 assisterList = new ArrayList<Assister>();
			//get jpa query object from builded query
			Query searchQuery = em.createNativeQuery(query.toString());
			//set parameter in search query
			setParametersForSearchAssisters(assisterRequestDTO,searchQuery);
			List<?> list = searchQuery.getResultList();
			createAssisterList(assisterList, list);
		} finally {
			if(em != null && em.isOpen()){
				em.close();
			}
		}
		
		LOGGER.info("searchAssister: END");
		
		return assisterList;
	}

	private void setParametersForSearchAssisters(AssisterRequestDTO assisterRequestDTO, Query searchQuery) {
		Assister assister = assisterRequestDTO.getAssister();
		Integer startRecord = assisterRequestDTO.getStartRecord() + 1;
		Integer endRecord = startRecord + assisterRequestDTO.getPageSize();

		EntityUtils.setQueryParameter(searchQuery, ASSISTER_FIRST_NAME, assister.getFirstName(), true);
		EntityUtils.setQueryParameter(searchQuery, ASSISTER_LAST_NAME, assister.getLastName(), true);
		EntityUtils.setQueryParameter(searchQuery, CERTIFICATION_STATUS, assister.getCertificationStatus(), false);

		String statusActive = assisterRequestDTO.getStatusactive();
		String statusInActive = assisterRequestDTO.getStatusInactive();

		if (assisterRequestDTO.getStatusInactive() == null || "".equals(assisterRequestDTO.getStatusInactive())) {
			EntityUtils.setQueryParameter(searchQuery, ACTIVE, assisterRequestDTO.getStatusactive(), false);
		}
		else if (assisterRequestDTO.getStatusactive() == null || "".equals(assisterRequestDTO.getStatusactive())) {
			EntityUtils.setQueryParameter(searchQuery, IN_ACTIVE, assisterRequestDTO.getStatusInactive(), false);
		}
		else {
			searchQuery.setParameter(ACTIVE,statusActive);
			searchQuery.setParameter(IN_ACTIVE,statusInActive);
		}

		EntityUtils.setQueryParameter(searchQuery, FROM_DATE, assisterRequestDTO.getFromDate(), false);
		EntityUtils.setQueryParameter(searchQuery, TO_DATE, assisterRequestDTO.getToDate(), false);

		//searchQuery.setParameter(START_RECORD,startRecord);
		//searchQuery.setParameter(END_RECORD,endRecord);
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.context = applicationContext;
		
	}


}
