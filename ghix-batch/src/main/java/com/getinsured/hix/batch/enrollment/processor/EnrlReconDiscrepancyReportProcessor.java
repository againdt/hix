package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;


@Component("enrlReconDiscrepancyReportProcessor")
public class EnrlReconDiscrepancyReportProcessor  implements ItemProcessor< String, String>{
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrlReconDiscrepancyReportProcessor.class);

	@Override
	public String process(String applicableId) throws Exception {
		LOGGER.info("Id received : " + applicableId);
		return applicableId;
	}
}
