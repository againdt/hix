package com.getinsured.hix.batch.enrollment.skip;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

@Component("enrollments")
public class EnrollmentIds {
	List<Integer> enrollmentIdList=null;

	public EnrollmentIds() {
		enrollmentIdList= new ArrayList<Integer>();
	}
	
	public synchronized void addAllToEnrollmentIdList(List<Integer> enrollmentIdList){
		this.enrollmentIdList.addAll(enrollmentIdList);
	}
	
	public synchronized void addToEnrollmentIdList(Integer enrollmentId){
		this.enrollmentIdList.add(enrollmentId);
	}
	
	public List<Integer> getEnrollmentIdList(){
		return enrollmentIdList;
	}
	public synchronized void clearEnrollmentIdList(){
		this.enrollmentIdList.clear();
	}
}
