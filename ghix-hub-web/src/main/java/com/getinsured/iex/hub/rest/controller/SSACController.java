package com.getinsured.iex.hub.rest.controller;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.MessageProcessor;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;

@Controller
public class SSACController extends MessageProcessor{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SSACController.class);
	
	private static final String SERVICE_NAME = "SSAC";
	private static final String SERVICE_XML_NAME = "VerifySSAComposite";
	
	@Autowired
	private WebServiceTemplate ssacServiceTemplate;
	
	@RequestMapping(value="/invokeSSAC", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeSSAC(@RequestBody String ssacRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("SSN_DEATH_INC_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge ssacServiceBridge = HubServiceBridge.getHubServiceBridge(SERVICE_XML_NAME);
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(ssacRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
				ssacServiceBridge.addContextParameter(APP_ID_KEY, (Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get(CLIENT_IP_KEY);
			JSONObject payloadObj = (JSONObject)requestObj.get(PAYLOAD_KEY);
			request.setAccessIp(clientIp);
			ServiceHandler handler = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.ssacServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeSSACForSSNVerification", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeSSACForSSNVerification(@RequestBody String ssacRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("SSN_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge ssacServiceBridge = HubServiceBridge.getHubServiceBridge(SERVICE_XML_NAME);
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(ssacRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
				ssacServiceBridge.addContextParameter(APP_ID_KEY, (Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get(CLIENT_IP_KEY);
			JSONObject payloadObj = (JSONObject)requestObj.get(PAYLOAD_KEY);
			request.setAccessIp(clientIp);
			ServiceHandler handler = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.ssacServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeSSACForDeathVerification", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeSSACForDeathVerification(@RequestBody String ssacRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("DEATH_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge ssacServiceBridge = HubServiceBridge.getHubServiceBridge(SERVICE_XML_NAME);
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(ssacRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
				ssacServiceBridge.addContextParameter(APP_ID_KEY, (Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get(CLIENT_IP_KEY);
			JSONObject payloadObj = (JSONObject)requestObj.get(PAYLOAD_KEY);
			request.setAccessIp(clientIp);
			ServiceHandler handler = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.ssacServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
	@RequestMapping(value="/invokeSSACForIncarcerationVerification", method=RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<String> invokeSSACForIncarcerationVerification(@RequestBody String ssacRequestJSON) {
		
		GIWSPayload request = new GIWSPayload();
		request.setEndpointFunction(SERVICE_NAME);
		request.setEndpointOperationName("INCARCERATION_VERIFICATION");
		request.setRetryCount(MAX_RETRY_COUNT);
		request.setStatus(INITIAL_STATUS);
		ResponseEntity<String> response = null;
		try{
			HubServiceBridge ssacServiceBridge = HubServiceBridge.getHubServiceBridge(SERVICE_XML_NAME);
			JSONParser parser = new JSONParser();
			JSONObject requestObj = (JSONObject) parser.parse(ssacRequestJSON);
			if(requestObj.get(APPLICATION_ID_KEY) != null){
				request.setSsapApplicationId((Long) requestObj.get(APPLICATION_ID_KEY));
				ssacServiceBridge.addContextParameter(APP_ID_KEY, (Long) requestObj.get(APPLICATION_ID_KEY));
			}
			String clientIp = (String)requestObj.get(CLIENT_IP_KEY);
			JSONObject payloadObj = (JSONObject)requestObj.get(PAYLOAD_KEY);
			request.setAccessIp(clientIp);
			ServiceHandler handler = ssacServiceBridge.getServiceHandler();
			handler.setJsonInput(payloadObj.toJSONString());
			response = this.executeRequest(request, handler, this.ssacServiceTemplate);
		}
		catch (Exception e) {
			LOGGER.error(e.getMessage(),e);
			response = new ResponseEntity<String>(PlatformServiceUtil.exceptionToJson(e), HttpStatus.BAD_REQUEST);
		}
		
		return response;
	}
	
}
