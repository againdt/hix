package com.getinsured.iex.dto;


import org.springframework.hateoas.ResourceSupport;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class SepEventsResource extends ResourceSupport {

	private String name;
	private int type;
	private String mrcCode;
	private String label;
	private String category;
	private String categoryLabel;
	private String financial;
	public Object _links;
	private Integer eventPrecedenceOrder;
	private String changeType;

	public Integer getEventPrecedenceOrder() {
		return eventPrecedenceOrder;
	}

	public Object get_links() {
		return _links;
	}

	public void set_links(Object _links) {
		this._links = _links;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getMrcCode() {
		return mrcCode;
	}
	public void setMrcCode(String mrcCode) {
		this.mrcCode = mrcCode;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCategoryLabel() {
		return categoryLabel;
	}
	public void setCategoryLabel(String categoryLabel) {
		this.categoryLabel = categoryLabel;
	}
	public String getFinancial() {
		return financial;
	}
	public void setFinancial(String financial) {
		this.financial = financial;
	}

	public String getChangeType() {
		return changeType;
	}

	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}

	public void setEventPrecedenceOrder(Integer eventPrecedenceOrder) {
		this.eventPrecedenceOrder = eventPrecedenceOrder;
	}
	
	
}
