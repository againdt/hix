package com.getinsured.hix.planmgmt.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.model.Region;
import com.getinsured.hix.planmgmt.repository.IRegionRepository;
import com.getinsured.hix.platform.security.service.UserService;

@Service("regionService")
public class RegionService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RegionService.class);
	@Autowired private IRegionRepository iRegionRepository;
	@Autowired private UserService userService;

	private static Map<String, List<Region>> regionsMap;
	private static Map<String, String> users;

	public RegionService() {
		regionsMap = new HashMap<String, List<Region>>();
		users = new HashMap<String, String>();
	}

	public List<Region> getRegionsByName(String givenRegionname) {
		List<Region> regions = null;
		if (givenRegionname != null && givenRegionname.length() > 0) {
			if (!regionsMap.containsKey(givenRegionname)) {
				// fetch list of regions from DB
				regionsMap.put(givenRegionname, getRegionsByNameFromDB(givenRegionname));
			}
			regions = regionsMap.get(givenRegionname);
		} else {
			LOGGER.info("Error in RegionService: given Region name is empty");
		}
		return regions;
	}

	public List<Region> getRegionsByNameFromDB(String givenRegionname) {
		return iRegionRepository.findByRegionName(givenRegionname);
	}

	public String getUserName(String userId) {
		String userName = null;
		if (userId != null && userId.length() > 0) {
			if (!users.containsKey(userId)) {
				// fetch list of regions from DB
				users.put(userId, getUserNameFromDB(userId));
			}
			userName = users.get(userId);
		}
		return userName;
	}

	public String getUserNameFromDB(String givenRegionname) {
		return userService.getUserName(Integer.parseInt(givenRegionname));
	}
}