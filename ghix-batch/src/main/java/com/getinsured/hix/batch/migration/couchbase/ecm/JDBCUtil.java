package com.getinsured.hix.batch.migration.couchbase.ecm;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JDBCUtil {
	private JDBCUtil() {
	}

	public static Statement getStatement(Connection con) throws SQLException {
		return con.createStatement();
	}

	public static void closeStatement(Statement objStmt) {
		if (objStmt == null) {
			return;
		}

		try {
			objStmt.close();
		} catch (SQLException sqe) {
		}
	}

	public static int executeUpdate(Connection con, String strSQL) throws SQLException {
		Statement objStmt = null;

		int iReturn = 0;

		try {
			objStmt = getStatement(con);
			iReturn = objStmt.executeUpdate(strSQL);
		} catch (SQLException e) {
			throw e;
		} finally {
			closeStatement(objStmt);
			objStmt = null;
		}

		return iReturn;
	}

	public static ResultSet executeQuery(Statement objStmt, String strSQL) throws SQLException {
		ResultSet objRs = null;

		try {
			objRs = objStmt.executeQuery(strSQL);
		} catch (SQLException sqe) {
			throw sqe;
		}

		return objRs;
	}

	public static Map<String, String> recordByColumnId(Connection con, String strSQL, int iColCount)
			throws SQLException {
		Statement objStmt = null;
		ResultSet objRs = null;
		Map<String, String> objMap = null;

		try {
			objStmt = getStatement(con);
			objRs = executeQuery(objStmt, strSQL);
			objMap = recordByColumnId(objRs, iColCount);
		} catch (SQLException e) {
			throw e;
		} finally {
			closeStatement(objStmt);
			closeResultSet(objRs);
		}

		return objMap;
	}

	public static void closeResultSet(ResultSet objRs) {
		if (objRs == null) {
			return;
		}

		try {
			objRs.close();
		} catch (SQLException sqe) {
		}

	}

	public static Map<String, String> recordByColumnId(ResultSet objRs, int iColCount) throws SQLException {
		Map<String, String> objMap = null;

		if (objRs.next()) {
			objMap = new HashMap<String, String>(iColCount);

			for (int j = 1; j <= iColCount; j++) {
				objMap.put(String.valueOf(j), objRs.getString(j));
			}
		}

		return objMap;
	}

	public static List<Map<String, String>> records(Connection con, String strSQL, String[] columns)
			throws SQLException {

		Statement objStmt = null;
		ResultSet objRs = null;
		List<Map<String, String>> data = null;
		try {
			objStmt = getStatement(con);
			objRs = executeQuery(objStmt, strSQL);
			data = populateRecords(objRs, columns.length, columns);
		} catch (SQLException e) {
			throw e;
		} finally {
			closeStatement(objStmt);
			closeResultSet(objRs);
		}

		return data;
	}

	private static List<Map<String, String>> populateRecords(ResultSet objRs, int iColCount, String[] strColumnName)
			throws SQLException {
		List<Map<String, String>> data = new ArrayList<>();

		Map<String, String> mp;

		while (objRs.next()) {
			mp = new HashMap<String, String>(iColCount);

			for (int j = 0; j < iColCount; j++) {
				mp.put(strColumnName[j], objRs.getString(j + 1));
			}

			data.add(mp);

		}

		return data;
	}

	public static int[] executeBatchUpdate(Connection con, String[] strSql) throws SQLException {
		Statement objStmt = null;
		int[] iReturn = null;

		try {
			objStmt = getStatement(con);

			int iLen = strSql.length;

			for (int j = 0; j < iLen; j++) {
				objStmt.addBatch(strSql[j]);
			}

			iReturn = objStmt.executeBatch();
		} catch (SQLException e) {
			throw e;
		} finally {
			closeStatement(objStmt);
			objStmt = null;
		}

		return iReturn;
	}
}
