<script src="js/jquery.mask.js" type="text/javascript"></script>
<script src="js/jquery.mask.min.js" type="text/javascript"></script>

<!--main work page-------------------------------------------->
<div id="loginAndReg" class="right_contant_area backgroundWhite"	style='display: block;'>
	<div id="loginDiv" class="main">
		<div class="loginbox">
			<div class="radiobutton">
				<p class="newUser">Login</p>
			</div>
			<div class="boxlogin">
				<div id="errorMsgDivLogin" class="errorMsgDiv" style="display:none">This is error msg div</div>
					<div style="width: 25%; float: left;">
					<img src="img/login_icon.png" style="width: 130%; margin: 25px 0px 0px 20px; border-right: 1px solid rgb(228, 228, 228);">
					</div>
					<div style="float: right; width: 72%;">
									<div class="GetAlinment3">
										<label class="pass mininmumWidth90 boldFontClass">Username</label>
										<input style="background: url('./img/login-input-username.png') no-repeat scroll right center transparent !important;" type="text" id="loginUsername" value="" class="passbox">
									</div>
					
									<div class="GetAlinment">
					
										<label class="pass mininmumWidth90 boldFontClass">Password</label>
										<input style="background: url('./img/login-input-password.png') no-repeat scroll right center transparent !important;" type="password" id="loginPassword" value="" class="passbox">
									</div>
					
					
									<div class="GetAlinment">
										<p class="Getpassword">
											<a href="javascript:forgotPassword();">Forgot password.</a>
										</p>
										<p class="Getpassword1">
											<a href="javascript:registration();">New user.</a>
										</p>
									</div>
					
					
									<div class="GetAlinment">
										<input type="button" name="" onclick="startApplication();" class="login" value="Login">
									</div>
					</div>
			</div>
		</div>
	</div>


	<div id="registrationDiv" class="main" style="display: none">
		<div class="loginbox">
			<div class="radiobutton">
				<p class="newUser">New User Registration</p>
			</div>
			<div class="boxlogin">
				<div id="errorMsgDiv" class="errorMsgDiv" style="display:none">This is error msg div</div>
				<div class='GetAlinment1'>
					<label class="pass boldFontClass">First Name:</label> <input id="regiName"
						type="text" class="passbox" placeholder="First Name">
				</div>
				<div class='GetAlinment1'>
					<label class="pass boldFontClass">Middle Name:</label> <input id="regiMiddleName"
						type="text" class="passbox" placeholder="Middle Name">
				</div>
				<div class='GetAlinment1'>
					<label class="pass boldFontClass">Last Name:</label> <input id="regiLastName"
						type="text" class="passbox" placeholder="Last Name">
				</div>
				<div class='GetAlinment1'>
					<label class="pass boldFontClass">Date of Birth:</label> <input id="regiDateOfBirth"
						type="text" class="passbox" placeholder="MM/DD/YYYY" >
				</div>
				<div class='GetAlinment'>
					<label class="pass boldFontClass"> Choose your username:</label> <input
						type="text" id="regiUserName" class="passbox"
						placeholder="Choose username">
				</div>

				<div class='GetAlinment'>
					<label class="pass boldFontClass">Create Password:</label> <input
						type="password" id="regiPassword" class="passbox"
						placeholder="Password">
				</div>
				<div class='GetAlinment'>
					<label class="pass boldFontClass"> Confirm Password:</label> <input
						type="password" id="regiConfirmPassword" class="passbox"
						placeholder="Confirm password">
				</div>
				<div class='GetAlinment'>
					<label class="pass boldFontClass">Email Address:</label> <input
						id="regiEmailId" type="email" class="passbox" placeholder="email">
				</div>

				<div class='GetAlinment'>
					<label class="pass boldFontClass">Phone Number:</label> <input
						id="regiPhoneNo" placeHolder="xxxxxxxxxx" type="text"
						class="passbox">
				</div>
				<div class='GetAlinment'>
					
					
					<input id="newRegistrationSubmit" type="button" class="login"
						onclick="validateNewUser();" value="Submit">
					
					
					<input id="newLoginAtReg" type="button" class="login"
						onclick="backToLogin();" value="Cancel">
					
					
				</div>
			</div>
		</div>
	</div>


	<div id="forgotPasswordDiv" style="display: none">
		<div class="forgotPassworddivCSS">
			<div class='GetAlinment'>
				<div class='GetAlinment1 paddingTopAndBottom'>
					<p class="boldFontClass marginLeft60 ">Please Enter your email
						address.&nbsp; A link will be sent on the entered address.</p>
					<label class="pass boldFontClass minimumWidth150 marginTop10">Your
						Email Address</label> <input id='email4ForgotPassword'
						placeHolder="email address" type="email" value=""
						class="forgotPasswordContainer">
				</div>
				<div class="marginLeft180">
					<input name="forgotPasswordBackBtn" type="button"
						class="sendPasswordBtn" id="backOnForgotPage" value="Back">
					<input name="forgotPasswordBtn" type="button"
						class="sendPasswordBtn" id="forgotBtn" value="Submit">
				</div>
			</div>
		</div>
	</div>
</div>


<!-----------------------------------------------main work page------------->
<script type="text/javascript">
$(function() {
jQuery.fn.ForceCharactersOnly =
	function()
	{
	    return this.each(function()
	    {
	        $(this).keydown(function(e)
	        {
	            var key = e.charCode || e.keyCode || 0;
	            // allow backspace, tab, delete, arrows, Alfabetics 
	            // home, end, and Esc.
	            return (
	            		key == 27 ||
	            		key == 46 ||
	            		key == 8 ||
	            		key == 9 ||
	            		(key >= 35 && key <= 40) ||
	            		(key >= 65 && key <= 90));
	        });
	    });
	};

	$('#regiName').ForceCharactersOnly();
	$('#regiMiddleName').ForceCharactersOnly();
	$('#regiLastName').ForceCharactersOnly();



	
});
</script>

