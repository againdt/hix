package com.getinsured.ahbx.platform.webclient;

import static org.junit.Assert.assertNotNull;

import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.getinsured.hix.model.PlatformRequest;
import com.getinsured.hix.model.PlatformRequest.AdobeLiveCycleRequest;
import com.getinsured.hix.model.PlatformResponse;
import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:/applicationContext.xml")
public class NoticesServiceWebClientTest {

	@Autowired private NoticesServiceWebClient noticesServiceWebClient;

	@Test
	public void testSendRequest() throws NoticeServiceException {

		HashMap<String,String> dataMap =  new HashMap<String, String>();

		dataMap.put("caseId", "123456");
		dataMap.put("caseName", "Medi-Cal Case");
		dataMap.put("personName", "John");
		dataMap.put("dependant1Name", "Spouse");
		dataMap.put("dependant2Name", "Child1");
		dataMap.put("addressLine1", "5700 S Mopac Expwy");
		dataMap.put("addressLine2", "Bldg E");
		dataMap.put("addressLine3", "Austin, TX 78749");


		PlatformRequest platformRequest = new PlatformRequest();
		AdobeLiveCycleRequest adobeLiveCycleRequest = platformRequest.new AdobeLiveCycleRequest();
		adobeLiveCycleRequest.setFormat("PDF");
		adobeLiveCycleRequest.setTemplateName("template");
		adobeLiveCycleRequest.setTokens(dataMap);
		platformRequest.setAdobeLiveCycleRequest(adobeLiveCycleRequest);

		String ahbxResponse = noticesServiceWebClient.sendRequest(platformRequest);

		/**
		 * UnMarshal and from PlatformResponse object
		 */
		XStream xstream = GhixUtils.getXStreamStaxObject();
		PlatformResponse platformResponse = (PlatformResponse) xstream.fromXML(ahbxResponse);

		/**
		 * If AHBX Notices Service call failed, throw Exception to the caller
		 */
		if ("FAILURE".equals(platformResponse.getStatus())){
			throw new NoticeServiceException("AHBX Notices Service call failed - " + platformResponse.getErrMsg());
		}
		//System.out.println(platformResponse.getAhbxEcmId());
		assertNotNull(platformResponse.getAhbxEcmId());
	}

}
