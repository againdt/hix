package com.getinsured.hix.provider.service.jpa;

import static com.getinsured.hix.platform.util.GhixConstants.FTP_UPLOAD_MAX_SIZE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.model.Facility;
import com.getinsured.hix.model.Physician;
import com.getinsured.hix.model.Physician.PhysicianType;
import com.getinsured.hix.model.PhysicianAddress;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Provider;
import com.getinsured.hix.model.ProviderUpload;
import com.getinsured.hix.model.Specialty;
import com.getinsured.hix.model.Specialty.GroupName;
import com.getinsured.hix.planmgmt.provider.repository.IFacilityRepository;
import com.getinsured.hix.planmgmt.provider.repository.INetworkRepository;
import com.getinsured.hix.planmgmt.provider.repository.IPhysicianRepository;
import com.getinsured.hix.planmgmt.provider.repository.IProviderRepository;
import com.getinsured.hix.planmgmt.provider.repository.ISpecialtyRepository;
import com.getinsured.hix.platform.ecm.ContentManagementService;
import com.getinsured.hix.platform.ecm.ECMConstants;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.provider.repository.IProviderUploadRepository;
import com.getinsured.hix.provider.search.SolrProxyImpl;
import com.getinsured.hix.provider.service.ManageFacilitiesDTO;
import com.getinsured.hix.provider.service.ManageProvidersDTO;
import com.getinsured.hix.provider.service.ProviderLoadStatusDTO;
import com.getinsured.hix.provider.service.ProviderService;

@Service("providerService")
@Repository
@Transactional
public class ProviderServiceImpl implements ProviderService {

	@PersistenceUnit private EntityManagerFactory entityManagerFactory;
	@Autowired private IPhysicianRepository iPhysicianRepository;
	@Autowired private IFacilityRepository iFacilityRepository;
	@Autowired private ISpecialtyRepository iSpecialtyRepository;
	@Autowired private INetworkRepository iNetworkRepository;
	@Autowired private IProviderRepository iProviderRepository;
	@Autowired private IProviderUploadRepository iProviderUploadRepository;
	@Autowired private ContentManagementService ecmService;
	@Autowired private SolrProxyImpl solrProxy;

	private static final Logger LOGGER = LoggerFactory.getLogger(ProviderServiceImpl.class);
	private static final boolean HAS_PROVIDER_TYPE = true;

	/* Doctor/Dentist search methods */
	@Override
	public List<Physician> findDoctors(String name, String gender, int specialities, String languages, String doctorNearZip) {

		return iPhysicianRepository.getPhysicians(name,gender,specialities,languages,doctorNearZip,GroupName.DOCTOR.toString());
	}
	@Override
	public List<Physician> findDoctorsForNetworkAndPlan(Integer planId,
			Integer networkId, String name, String gender, Integer specialities,
			String languages, String doctorNearZip) {
	
		return iPhysicianRepository.getPhysiciansForNetworkAndPlan(planId, networkId, name, gender, specialities, languages, doctorNearZip,GroupName.DOCTOR.toString());
	}
	
	@Override
	public List<Physician> findDentists(String name, String gender, int specialities, String languages, String dentistNearZip){

		return iPhysicianRepository.getPhysicians(name,gender,specialities,languages,dentistNearZip,GroupName.DENTIST.toString());
	}
	@Override
	public List<Physician> findDentistsForNetworkAndPlan(int planId,
			int networkId, String name, String gender, int specialities,
			String languages, String dentistNearZip) {
	
		return iPhysicianRepository.getPhysiciansForNetworkAndPlan(planId, networkId, name, gender, specialities, languages, dentistNearZip,GroupName.DENTIST.toString());
	}
	
	@Override
	public Physician findPhysician(int id) {
		List<Object[]> physicianList = new ArrayList<Object[]>();
		Physician physician = new Physician();
		try{
			physicianList =  iPhysicianRepository.getPhysicianById(id);
			/**
			 * Received list of physician data, converting it into Physician Object to display on UI.
			 */
			Set<String> addressIds = new HashSet<String>();	
			Set<String> specialityIds = new HashSet<String>();
			Set<PhysicianAddress> addressSet = new HashSet< PhysicianAddress>();
			Set<Specialty> specialtySet = new HashSet<Specialty>();

			physician.setId(physicianList.get(0)[1]!=null?Integer.parseInt(physicianList.get(0)[0].toString()):0);
			physician.setFirst_name(physicianList.get(0)[1]!=null?physicianList.get(0)[1].toString():"");
			physician.setLast_name(physicianList.get(0)[2]!=null?physicianList.get(0)[2].toString():"");
			physician.setTitle(physicianList.get(0)[3]!=null?physicianList.get(0)[3].toString():"");
			physician.setSuffix(physicianList.get(0)[4]!=null?physicianList.get(0)[4].toString():"");
			physician.setBoardCertification(physicianList.get(0)[5]!=null?physicianList.get(0)[5].toString():"");
			physician.setLanguages(physicianList.get(0)[6]!=null?physicianList.get(0)[6].toString():"");

			for(Object[] physicianObj : physicianList){
				if(physicianObj[7]!=null && !addressIds.contains(physicianObj[7].toString())){
					
					addressIds.add(physicianObj[7].toString());
					
					PhysicianAddress pa = new PhysicianAddress();
					pa.setAddress1(physicianObj[8]!=null?physicianObj[8].toString():"");
					pa.setCity(physicianObj[9]!=null?physicianObj[9].toString():"");
					pa.setState(physicianObj[10]!=null?physicianObj[10].toString():"");
					pa.setZip(physicianObj[11]!=null?physicianObj[11].toString():"");
					pa.setPhoneNumber(physicianObj[12]!=null?physicianObj[12].toString():"");
					
					try{
						pa.setLattitude(physicianObj[13]!=null?Float.parseFloat(physicianObj[13].toString()):null);
						pa.setLongitude(physicianObj[14]!=null?Float.parseFloat(physicianObj[14].toString()):null);
					}catch(Exception e){
						LOGGER.error("Error",e);
					}
					
					addressSet.add(pa);
				}

				if(physicianObj[15]!=null && !specialityIds.contains(physicianObj[15].toString())){
					specialityIds.add(physicianObj[15].toString());
					Specialty spec = new Specialty();
					spec.setName(physicianObj[16].toString());
					specialtySet.add(spec);
				}
			}
			physician.setPhysicianAddress(addressSet);
			physician.setSpecialitySet(specialtySet);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return physician;
	}


/* Hospitals/Facilities search method */
	
	@Override
	public List<Facility> findHospitals(String name, String hospitalType, String hospitalNearZip) {
		
		return iFacilityRepository.getHospitals(name, hospitalType,hospitalNearZip,GroupName.FACILITY.toString());
	}
	@Override
	public Facility findFacility(int id) {
			return iFacilityRepository.findOne(id);
	}
	@Override
	public List<Facility> findHospitalsForNetworkAndPlan(Integer planId,
			Integer networkId, String name, String hospitalType,
			String hospitalNearZip) {
		
		return iFacilityRepository.getFacilitiesForNetworkAndPlan(planId, networkId, name, hospitalType, hospitalNearZip, GroupName.FACILITY.toString());
	}
	
/* Specialties search method */	

	@Override
	public List<Specialty> getParentSpecialtiesByGroupName(String groupName) {
		List<Specialty> specialties = new ArrayList<Specialty>();
		List<Object[]> parentSpecialties = iSpecialtyRepository.getParentSpecialtiesByGroupName(groupName);
		for(Object[] parentSpecialtiesObj:parentSpecialties){
			Specialty s = new Specialty();
			s.setId((Integer)parentSpecialtiesObj[0]);
			s.setName((String)parentSpecialtiesObj[1]);
			s.setParentSpecialtyId((Integer)parentSpecialtiesObj[2]);
			specialties.add(s);
		}
		return specialties;
	}
	@Override
	public List<Specialty> getChildSpecialtiesByParentId(String groupName, int parentSpecialtyId) {
	
		List<Specialty> specialties = new ArrayList<Specialty>();
		List<Object[]> childSpecialties=iSpecialtyRepository.getChildSpecialtiesByParentId(groupName,parentSpecialtyId); 
		for(Object[] childSpecialtiesObj:childSpecialties){
			Specialty s = new Specialty();
			s.setId((Integer)childSpecialtiesObj[0]);
			s.setName((String)childSpecialtiesObj[1]);
			specialties.add(s);
		}
		return specialties;
	}
	@Override
	public Specialty findSpecialtyById(int specialtyId) {
		return iSpecialtyRepository.findOne(specialtyId);
	}
	@Override
	public boolean isChildSpecialtiesExists(String groupName,int specialtyId) {
	  return iSpecialtyRepository.getCountOfSpecialty(groupName,specialtyId)>0?true:false;
	}
/* Plan, Network..etc related search method */
	
	@Override
	public Plan getPlanByNetworkId(int networkId) {
		
		return iNetworkRepository.getPlanByNetworkId(networkId);
	}
	
	@Override
	public Page<Facility> findFacility(String name, String cityName, String zipCode, String type, Pageable pageable) {
		if(StringUtils.isNotBlank(name)&&(StringUtils.isNotBlank(cityName)||StringUtils.isNotBlank(zipCode))){
			return iFacilityRepository.findFacility(name, cityName, zipCode, type, pageable);
		}else if(StringUtils.isNotBlank(name)){
			return iFacilityRepository.findFacilityByName(name, type, pageable);	
		}else{
			return iFacilityRepository.findFacilityByLocation(cityName, zipCode, type, pageable);
		}
	}
	
	
	@Override
	public Page<Physician> getPhysicianOrDentist(String name, boolean isFullName, String cityName, String zipCode, String physicianType, Pageable pageable) {
		if(physicianType.equals(PhysicianType.DOCTOR.toString())){
			physicianType = PhysicianType.DENTIST.toString();
			/*
			 * Here passing type as DENTIST, Because in query need to search all physician of type <DOCTOR & VISION> which is NOT EQUALS to DENTIST.
			 * 
			 * Please check in query, search criteria is <> to type in physician search.
			 */
			
			if(StringUtils.isNotBlank(name)&& !isFullName && (StringUtils.isBlank(cityName) && StringUtils.isBlank(zipCode))){
				
				return iPhysicianRepository.getPhysicianByName(name, physicianType, pageable);
				
			}else if(StringUtils.isNotBlank(name)&& !isFullName && (StringUtils.isNotBlank(cityName)|| StringUtils.isNotBlank(zipCode))){
				
				return iPhysicianRepository.getPhysicianByNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				
			}else if(StringUtils.isNotBlank(name)&& isFullName && (StringUtils.isBlank(cityName) && StringUtils.isBlank(zipCode))){
				
				Page<Physician> physicians = iPhysicianRepository.getPhysicianByFullName(name, physicianType, pageable);
				if(physicians.getContent().size()==0){
					physicians = iPhysicianRepository.getPhysicianByName(name, physicianType, pageable);
				}
				return physicians;
				
			}else if(StringUtils.isNotBlank(name)&& isFullName && (StringUtils.isNotBlank(cityName)|| StringUtils.isNotBlank(zipCode))){
				
				Page<Physician> physicians = iPhysicianRepository.getPhysicianByFullNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				if(physicians.getContent().size()==0){
					physicians = iPhysicianRepository.getPhysicianByNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				}
				return physicians;
				
			}else{
				return iPhysicianRepository.getPhysicianByLocation(cityName, zipCode, physicianType, pageable);
			}
		}else{
			physicianType = PhysicianType.DENTIST.toString();
			if(StringUtils.isNotBlank(name)&& !isFullName && (StringUtils.isBlank(cityName) && StringUtils.isBlank(zipCode))){
				
				return iPhysicianRepository.getDentistByName(name, physicianType, pageable);
				
			}else if(StringUtils.isNotBlank(name)&& !isFullName && (StringUtils.isNotBlank(cityName)|| StringUtils.isNotBlank(zipCode))){
				
				return iPhysicianRepository.getDentistByNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				
			}else if(StringUtils.isNotBlank(name)&& isFullName && (StringUtils.isBlank(cityName) && StringUtils.isBlank(zipCode))){
				
				Page<Physician> dentists = iPhysicianRepository.getDentistByFullName(name, physicianType, pageable);
				if(dentists.getContent().size()==0){
					dentists = iPhysicianRepository.getDentistByName(name, physicianType, pageable);
				}
				
				return dentists;
			}else if(StringUtils.isNotBlank(name)&& isFullName && (StringUtils.isNotBlank(cityName)|| StringUtils.isNotBlank(zipCode))){
				
				Page<Physician> dentists = iPhysicianRepository.getDentistByFullNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				if(dentists.getContent().size()==0){
					dentists = iPhysicianRepository.getDentistByNameAndLocation(name, cityName, zipCode, physicianType, pageable);
				}
				return dentists;
				
			}else{
				return iPhysicianRepository.getDentistByLocation(cityName, zipCode, physicianType, pageable);
			}			
		}
	}
	
	@Override
	public List<Specialty> getSpecialityList(){
		return iSpecialtyRepository.findAll();
	}
	
	@Override
	public String getProviderByGroupKey(String groupKey,Provider.ProviderType providerType){
		return iProviderRepository.getProviderByGroupKey(groupKey,providerType);
	}

	/**
	 * Method is used to get Provider Load Status Data List.
	 */
	@Override
	public Page<ProviderLoadStatusDTO> getProviderLoadStatusDataList(int page, int size) {

		LOGGER.debug("getProviderLoadStatusDataList() begin");
		Page<ProviderLoadStatusDTO> providerLoadStatusDataList = null;
		EntityManager em = null;
		Query query = null;
		int resultCount = 0;

		try {
			em = entityManagerFactory.createEntityManager();

			// Record count query
			String queryForCount = "SELECT COUNT(providerUpload) FROM ProviderUpload AS providerUpload "
					+ "WHERE providerUpload.status NOT IN ('" + ProviderUpload.STATUS.CANCEL + "') ";

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Load Status Data Count Query: " + queryForCount);
			}
			query = em.createQuery(queryForCount);

		   	Object objCount = query.getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}

			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("Result Count: " + resultCount);
			}

			// Actual record query
			String queryStr = "SELECT new com.getinsured.hix.provider.service.ProviderLoadStatusDTO(providerUpload.id, providerUpload.fileName, "
					+ "providerUpload.applicableYear, providerUpload.providerTypeIndicator, providerUpload.status, providerUpload.logs, "
					+ "providerUpload.sourceFileEcmId, providerUpload.creationTimestamp) "
					+ "FROM ProviderUpload AS providerUpload WHERE providerUpload.status NOT IN ('" + ProviderUpload.STATUS.CANCEL + "') "
					+ "ORDER BY providerUpload.id DESC";

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("Plan Load Status Data Query: " + queryStr);
			}

			query = em.createQuery(queryStr);
			query.setFirstResult(page * size);
			query.setMaxResults(size);

			providerLoadStatusDataList = new PageImpl<ProviderLoadStatusDTO>(query.getResultList(), new PageRequest(page, size), resultCount);
			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("providerLoadStatusDataList is null: " + (null == providerLoadStatusDataList));
			}
		}
		catch (Exception ex) {
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
			LOGGER.debug("getProviderLoadStatusDataList() end");
		}
		return providerLoadStatusDataList;
	}

	/**
	 * Method is used to upload Provider File at ECM & Disk for batch process and save tracking data in 'Provider Upload' table.
	 */
	@Override
	public boolean uploadProviderFileAndSaveTrackingDataInDB(MultipartFile fileToUpload, String selectedYear,
			String selectedType, int createdBy, StringBuffer errorMessage) {

		LOGGER.debug("uploadProviderFileAndSaveTrackingDataInDB() Start");
		boolean status = false;
		final String ecmBasePath = "provider/dataFiles/";

		try {

			if (hasValidProviderFileRequest(fileToUpload, selectedYear, selectedType, errorMessage)) {

				ProviderUpload.PROVIDER_TYPE_INDICATOR providerTypeIndicator = ProviderUpload.PROVIDER_TYPE_INDICATOR.valueOf(selectedType);
				ProviderUpload providerUpload = new ProviderUpload();
				providerUpload.setApplicableYear(Integer.valueOf(selectedYear));
				providerUpload.setProviderTypeIndicator(ProviderUpload.PROVIDER_TYPE_INDICATOR.valueOf(selectedType).name());
				providerUpload.setFileName(fileToUpload.getOriginalFilename());
				providerUpload.setFileType(ProviderUpload.FILE_TYPE.TEXT.name());
				providerUpload.setCreatedBy(createdBy);
				providerUpload = iProviderUploadRepository.save(providerUpload);

				String fileName = null;
				String providerID = String.valueOf(providerUpload.getId());

				if (ProviderUpload.PROVIDER_TYPE_INDICATOR.F.equals(providerTypeIndicator)) {
					fileName = new SimpleDateFormat("'"+ GhixConstants.ENCLARITY_FACILITY_FILE_PREFIX +"_'yyyyMMddHHmmss'_providerID.txt'").format(new TSDate()).replace("providerID", providerID);
				}
				else if (ProviderUpload.PROVIDER_TYPE_INDICATOR.P.equals(providerTypeIndicator)) {
					fileName = new SimpleDateFormat("'"+ GhixConstants.ENCLARITY_INDIVIDUAL_FILE_PREFIX +"_'yyyyMMddHHmmss'_providerID.txt'").format(new TSDate()).replace("providerID", providerID);
				}

				// Upload Provider file to ECM
				String sourceFileEcmId = ecmService.createContent(ecmBasePath, fileName, fileToUpload.getBytes(), ECMConstants.Provider.DOC_CATEGORY, ECMConstants.Provider.DOC_SUB_CATEGORY, null);

				providerUpload.setSourceFileEcmId(sourceFileEcmId);
				providerUpload.setStatus(ProviderUpload.STATUS.WAITING.name());
				providerUpload.setLastUpdatedBy(createdBy);
				providerUpload = iProviderUploadRepository.save(providerUpload);

				if (null != providerUpload) {
					status = true;
				}
				else {
					errorMessage.append("Failed to save Provider File Data in database.");
				}
			}
		}
		catch (Exception ex) {
			errorMessage.append("Failed to upload Provider File, please check logs.");
			LOGGER.error(ex.getMessage(), ex);
		}
		finally {

			if (StringUtils.isNotBlank(errorMessage)) {
				LOGGER.error("Erros: " + errorMessage.toString());
			}
			LOGGER.debug("uploadProviderFileAndSaveTrackingDataInDB() end");
		}
		return status;
	}

	/**
	 * Method is used to validate Provider File Request.
	 */
	private boolean hasValidProviderFileRequest(MultipartFile fileToUpload, String selectYear, String selectType, StringBuffer errorMessage) {

		boolean hasValidRequest = false;
		long maxSize = FTP_UPLOAD_MAX_SIZE / 1048576;

		if (null == fileToUpload || fileToUpload.isEmpty()) {
			errorMessage.append("Provider File is not attached, ");
		}
		else if (fileToUpload.getSize() > FTP_UPLOAD_MAX_SIZE) {
			errorMessage.append(fileToUpload.getOriginalFilename() + " File size is exceeding max size " + maxSize + "MB, ");
		}

		if (!StringUtils.isNumeric(selectYear)) {
			errorMessage.append("Year is required to select, ");
		}

		if (StringUtils.isBlank(selectType)) {
			errorMessage.append("Provider Type is required to select, ");
		}
		else {

			try {
				ProviderUpload.PROVIDER_TYPE_INDICATOR.valueOf(selectType);
			}
			catch (IllegalArgumentException ex) {
				errorMessage.append("Invalid Provider Type, ");
			}
		}

		if (StringUtils.isBlank(errorMessage)) {
			hasValidRequest = true;
		}
		return hasValidRequest;
	}

	/**
	 * Method is used get Provider Data List from SOLR server.
	 */
	@Override
	public Page<ManageProvidersDTO> getProviderDataList(String nameFilter, String specialityFilter, String networkIdFilter, int page, int size) {

		LOGGER.debug("getProviderDataList() Start");
		Page<ManageProvidersDTO> providerDTOList = null;

		try {
			long providerDataCount = solrProxy.getProviderDataCount(HAS_PROVIDER_TYPE, nameFilter, null, specialityFilter, networkIdFilter);
			List<ManageProvidersDTO> resultsList = solrProxy.getProviderDataList(nameFilter, specialityFilter, networkIdFilter, page, size);

			if (!CollectionUtils.isEmpty(resultsList)) {
				providerDTOList = new PageImpl<ManageProvidersDTO>(resultsList, new PageRequest(page, size), providerDataCount);
			}
			else {
				LOGGER.warn("No matching records found.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Failed to get Provider Data from SOLR: ", ex);
		}
		finally {
			LOGGER.debug("getProviderDataList() End");
		}
		return providerDTOList;
	}

	/**
	 * Method is used get Facility Data List from SOLR server.
	 */
	@Override
	public Page<ManageFacilitiesDTO> getFacilityDataList(String facilityFilter, String specialityFilter, String networkIdFilter, int page, int size) {

		LOGGER.debug("getFacilityDataList() Start");
		Page<ManageFacilitiesDTO> facilityDTOList = null;

		try {
			long providerDataCount = solrProxy.getProviderDataCount(!HAS_PROVIDER_TYPE, null, facilityFilter, specialityFilter, networkIdFilter);
			List<ManageFacilitiesDTO> resultsList = solrProxy.getFacilityDataList(facilityFilter, specialityFilter, networkIdFilter, page, size);

			if (!CollectionUtils.isEmpty(resultsList)) {
				facilityDTOList = new PageImpl<ManageFacilitiesDTO>(resultsList, new PageRequest(page, size), providerDataCount);
			}
			else {
				LOGGER.warn("No matching records found.");
			}
		}
		catch (Exception ex) {
			LOGGER.error("Failed to get Facility Data from SOLR: ", ex);
		}
		finally {
			LOGGER.debug("getFacilityDataList() End");
		}
		return facilityDTOList;
	}
}
