package com.getinsured.eligibility.at.ref.service;

import java.util.Map;

import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.resp.si.dto.ApplicationExtension;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * @author chopra_s
 * 
 */
public interface LceAppExtensionEvent {

	void createAptcOnlyEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension,Boolean isAPTCIncreased);

	Map<String, ApplicantEvent> createCitizenshipEvent(SsapApplication currentApplication, ApplicationExtension applicationExtension);

}
