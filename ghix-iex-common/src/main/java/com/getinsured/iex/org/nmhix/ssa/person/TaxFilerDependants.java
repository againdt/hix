//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TaxFilerDependants complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TaxFilerDependants">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dependantHouseholdMemeberId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="relationshipTypeToTaxFiler" type="{http://www.cohbe.org/SSA/Person}RelationshipType-Enum" minOccurs="0"/>
 *         &lt;element name="relationshipTypeToTaxFilerOther" type="{http://www.cohbe.org/SSA/Person}RelationshipTypeOther-Enum" minOccurs="0"/>
 *         &lt;element name="liveWithTaxFilerIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="liveWithOtherCustodianIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFilerDependants", propOrder = {
    "dependantHouseholdMemeberId",
    "relationshipTypeToTaxFiler",
    "relationshipTypeToTaxFilerOther",
    "liveWithTaxFilerIndicator",
    "liveWithOtherCustodianIndicator"
})

@Entity
@Table(name="TFDependants")
public class TaxFilerDependants {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@Column(name="DHMId")
    @XmlElement(required = true)
    protected String dependantHouseholdMemeberId;
	
	@Column(name="RTTTFiler")
    protected RelationshipTypeEnum relationshipTypeToTaxFiler;
	
	@Column(name="RTTTFOther")
    protected RelationshipTypeOtherEnum relationshipTypeToTaxFilerOther;
    
    @Column(name="LWTFIndicator")
    protected Boolean liveWithTaxFilerIndicator;
    
    @Column(name="LWOCIndicator")
    protected Boolean liveWithOtherCustodianIndicator;

    /**
     * Gets the value of the dependantHouseholdMemeberId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantHouseholdMemeberId() {
        return dependantHouseholdMemeberId;
    }

    /**
     * Sets the value of the dependantHouseholdMemeberId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantHouseholdMemeberId(String value) {
        this.dependantHouseholdMemeberId = value;
    }

    /**
     * Gets the value of the relationshipTypeToTaxFiler property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipTypeEnum }
     *     
     */
    public RelationshipTypeEnum getRelationshipTypeToTaxFiler() {
        return relationshipTypeToTaxFiler;
    }

    /**
     * Sets the value of the relationshipTypeToTaxFiler property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipTypeEnum }
     *     
     */
    public void setRelationshipTypeToTaxFiler(RelationshipTypeEnum value) {
        this.relationshipTypeToTaxFiler = value;
    }

    /**
     * Gets the value of the relationshipTypeToTaxFilerOther property.
     * 
     * @return
     *     possible object is
     *     {@link RelationshipTypeOtherEnum }
     *     
     */
    public RelationshipTypeOtherEnum getRelationshipTypeToTaxFilerOther() {
        return relationshipTypeToTaxFilerOther;
    }

    /**
     * Sets the value of the relationshipTypeToTaxFilerOther property.
     * 
     * @param value
     *     allowed object is
     *     {@link RelationshipTypeOtherEnum }
     *     
     */
    public void setRelationshipTypeToTaxFilerOther(RelationshipTypeOtherEnum value) {
        this.relationshipTypeToTaxFilerOther = value;
    }

    /**
     * Gets the value of the liveWithTaxFilerIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLiveWithTaxFilerIndicator() {
        return liveWithTaxFilerIndicator;
    }

    /**
     * Sets the value of the liveWithTaxFilerIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLiveWithTaxFilerIndicator(Boolean value) {
        this.liveWithTaxFilerIndicator = value;
    }

    /**
     * Gets the value of the liveWithOtherCustodianIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLiveWithOtherCustodianIndicator() {
        return liveWithOtherCustodianIndicator;
    }

    /**
     * Sets the value of the liveWithOtherCustodianIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLiveWithOtherCustodianIndicator(Boolean value) {
        this.liveWithOtherCustodianIndicator = value;
    }

}
