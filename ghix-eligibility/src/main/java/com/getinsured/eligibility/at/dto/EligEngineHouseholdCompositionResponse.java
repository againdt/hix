
package com.getinsured.eligibility.at.dto;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Response Object for the Eligibility Engine Household Composition API
 * 
 * @author Supreet Bhandari
 * @since 29 July, 2019
 *
 */
public class EligEngineHouseholdCompositionResponse {

	private String status;
	
	private int householdSize;
	
	private long householdIncome;

	private BigDecimal fplPercentage;

	private List<EligEngineHouseholdCompositionMember> members;

	private Map<Integer, String> errors;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getHouseholdSize() {
		return householdSize;
	}
	
	public void setHouseholdSize(int householdSize) {
		this.householdSize = householdSize;
	}
	
	public long getHouseholdIncome() {
		return householdIncome;
	}

	public void setHouseholdIncome(long householdIncome) {
		this.householdIncome = householdIncome;
	}
	
	public BigDecimal getFplPercentage() {
		return fplPercentage;
	}

	public void setFplPercentage(BigDecimal fplPercentage) {
		this.fplPercentage = fplPercentage;
	}
	
	public List<EligEngineHouseholdCompositionMember> getMembers() {
		return members;
	}

	public void setMembers(List<EligEngineHouseholdCompositionMember> members) {
		this.members = members;
	}

	public Map<Integer, String> getErrors() {

		if (errors == null) {
			errors = new HashMap<Integer, String>();
		}

		return errors;
	}

	public void setErrors(Map<Integer, String> errors) {
		this.errors = errors;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(", status=").append(status);
		builder.append(", householdSize=").append(householdSize);
		builder.append(", householdIncome=").append(householdIncome);
		builder.append(", fplPercentage=").append(fplPercentage);
		builder.append(", members=").append(members);
		builder.append(", errors=").append(errors);
		builder.append("]");
		return builder.toString();
	}

}
