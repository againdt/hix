package com.getinsured.ssap.model.hub.vlp;

import java.util.Date;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;

import com.getinsured.ssap.model.hub.vlp.response.ResponseCode;
import com.getinsured.ssap.model.hub.vlp.response.VlpResponse;


/**
 * VLP Integration entity that maps to {@code vlp_integration} table.
 * @author Yevgen Golubenko
 * @since 3/5/19
 */
@Entity
@Table(name = "vlp_integration")
public class VlpIntegrationLog
{
  /*
  create sequence vlp_integration_seq;

  create table vlp_integration (
    id NUMERIC PRIMARY KEY NOT NULL,
    ssap_application_id NUMERIC NOT NULL,
    ssap_applicant_id NUMERIC NOT NULL,
    case_number VARCHAR(20) DEFAULT NULL,
    eligibility_statement_code SMALLINT DEFAULT 0,
    action VARCHAR(32) NOT NULL,
    action_result VARCHAR(32) DEFAULT NULL,
    remote_url VARCHAR(1024) DEFAULT NULL,
    retry_count SMALLINT DEFAULT 3,
    response_code VARCHAR(32) DEFAULT NULL,
    response_message VARCHAR(1024) DEFAULT NULL,
    last_retry_timestamp TIMESTAMP(6),
    vlp_request JSONB NOT NULL,
    vlp_response JSONB DEFAULT NULL,
    last_updated_timestamp TIMESTAMP(6),
    creation_timestamp TIMESTAMP(6)
  );

  -- CREATE INDEX idx_vlp_integ_resp_code ON vlp_integration (response_code);

  CREATE INDEX idx_vlp_integ_action ON vlp_integration(action);
  CREATE INDEX idx_vlp_integ_action_res ON vlp_integration(action_result);
  CREATE INDEX idx_vlp_integ_ssapid ON vlp_integration(ssap_application_id);
  CREATE INDEX idx_vlp_integ_aplicantid ON vlp_integration(ssap_applicant_id);
  CREATE INDEX idx_vlp_integ_eligstatecode ON vlp_integration(eligibility_statement_code);

  -- Oracle:
  CREATE TABLE vlp_integration (
			id 	number(38,0) PRIMARY KEY,
			ssap_application_id number NOT NULL,
			ssap_applicant_id number NOT NULL,
			case_number VARCHAR2(20) DEFAULT NULL,
			eligibility_statement_code NUMBER(3) DEFAULT 0,
			action VARCHAR2(32) NOT NULL,
			action_result VARCHAR2(32) DEFAULT NULL,
			remote_url VARCHAR2(1024) DEFAULT NULL,
			retry_count NUMBER(3) DEFAULT 3,
			response_code VARCHAR2(32) DEFAULT NULL,
			response_message VARCHAR2(1024) DEFAULT NULL,
			last_retry_timestamp TIMESTAMP(6),
			vlp_request CLOB NOT NULL,
			vlp_response CLOB DEFAULT NULL,
			last_updated_timestamp TIMESTAMP(6),
			creation_timestamp TIMESTAMP(6),

			CONSTRAINT cnst_vlp_int_req_json CHECK (vlp_request IS JSON),
			CONSTRAINT cnst_vlp_int_res_json CHECK (vlp_response IS JSON)
		)
   */
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "vlp_integration_seq")
  @SequenceGenerator(name = "vlp_integration_seq", sequenceName = "vlp_integration_seq", allocationSize = 1)
  @Column(name="id")
  private long id;

  @Min(value = 1, message = "ssap_application_id should be >= 1")
  @Column(name = "ssap_application_id")
  private long ssapApplicationId;

  @Min(value = 1, message = "ssap_applicant_id should be >= 1")
  @Column(name = "ssap_applicant_id")
  private long ssapApplicantId;

  @Column(name = "case_number")
  private String caseNumber;

  @Column(name = "eligibility_statement_code")
  private int eligibilityStatementCode;

  @Enumerated(EnumType.STRING)
  @Column(name = "action")
  private VlpAction action;

  @Enumerated(EnumType.STRING)
  @Column(name = "action_result")
  private VlpActionResult actionResult;

  @NotEmpty
  @NotNull
  @Column(name = "remote_url")
  private String remoteUrl;

  @Column(name = "retry_count")
  private int retryCount;

  @Enumerated(EnumType.STRING)
  @Column(name = "response_code")
  private ResponseCode responseCode;

  @Column(name = "response_message")
  private String responseMessage;

  @Column(name = "last_retry_timestamp")
  private Date lastRetryDate;

  @Column(name = "vlp_request")
  @Type(type = "com.getinsured.ssap.repository.types.VlpRequestJsonbType")
  private VlpRequest vlpRequest;

  @Column(name = "vlp_response")
  @Type(type = "com.getinsured.ssap.repository.types.VlpResponseJsonbType")
  private VlpResponse vlpResponse;

  @Temporal(value = TemporalType.TIMESTAMP)
  @Column(name = "last_updated_timestamp")
  private Date updatedDate;

  @Temporal(value = TemporalType.TIMESTAMP)
  @Column(name="creation_timestamp")
  private Date createdDate;

  @PrePersist
  public void PrePersist()
  {
    this.createdDate = new Date();
    this.updatedDate = new Date();
  }

  @PreUpdate
  public void PreUpdate()
  {
    this.updatedDate = new Date();
  }

  public long getId()
  {
    return id;
  }

  public void setId(final long id)
  {
    this.id = id;
  }

  public long getSsapApplicationId()
  {
    return ssapApplicationId;
  }

  public void setSsapApplicationId(final long ssapApplicationId)
  {
    this.ssapApplicationId = ssapApplicationId;
  }

  public long getSsapApplicantId()
  {
    return ssapApplicantId;
  }

  public void setSsapApplicantId(final long ssapApplicantId)
  {
    this.ssapApplicantId = ssapApplicantId;
  }

  public String getCaseNumber()
  {
    return caseNumber;
  }

  public void setCaseNumber(final String caseNumber)
  {
    this.caseNumber = caseNumber;
  }

  public int getEligibilityStatementCode()
  {
    return eligibilityStatementCode;
  }

  public void setEligibilityStatementCode(final int eligibilityStatementCode)
  {
    this.eligibilityStatementCode = eligibilityStatementCode;
  }

  public VlpAction getAction()
  {
    return action;
  }

  public void setAction(final VlpAction action)
  {
    this.action = action;
  }

  public VlpActionResult getActionResult()
  {
    return actionResult;
  }

  public void setActionResult(final VlpActionResult actionResult)
  {
    this.actionResult = actionResult;
  }

  public String getRemoteUrl()
  {
    return remoteUrl;
  }

  public void setRemoteUrl(final String remoteUrl)
  {
    this.remoteUrl = remoteUrl;
  }

  public int getRetryCount()
  {
    return retryCount;
  }

  public void setRetryCount(final int retryCount)
  {
    this.retryCount = retryCount;
  }

  public Date getLastRetryDate()
  {
    return lastRetryDate;
  }

  public void setLastRetryDate(final Date lastRetryDate)
  {
    this.lastRetryDate = lastRetryDate;
  }

  public ResponseCode getResponseCode()
  {
    return responseCode;
  }

  public void setResponseCode(final ResponseCode responseCode)
  {
    this.responseCode = responseCode;
  }

  public String getResponseMessage()
  {
    return responseMessage;
  }

  public void setResponseMessage(final String responseMessage)
  {
    this.responseMessage = responseMessage;
  }

  public VlpRequest getVlpRequest()
  {
    return vlpRequest;
  }

  public void setVlpRequest(final VlpRequest vlpRequest)
  {
    this.vlpRequest = vlpRequest;
  }

  public VlpResponse getVlpResponse()
  {
    return vlpResponse;
  }

  public void setVlpResponse(final VlpResponse vlpResponse)
  {
    this.vlpResponse = vlpResponse;
  }

  public Date getUpdatedDate()
  {
    return updatedDate;
  }

  public void setUpdatedDate(final Date updatedDate)
  {
    this.updatedDate = updatedDate;
  }

  public Date getCreatedDate()
  {
    return createdDate;
  }

  public void setCreatedDate(final Date createdDate)
  {
    this.createdDate = createdDate;
  }
}
