<%@page import="com.getinsured.hix.platform.config.UIConfiguration" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ taglib uri="/WEB-INF/tld/ghix-url.tld" prefix="gi" %>
<%@ page import="com.getinsured.hix.platform.util.*" %>
<%@page import="com.getinsured.hix.platform.config.GlobalConfiguration" %>
<%@page import="com.getinsured.hix.platform.config.DynamicPropertiesUtil" %>
<%@page import="com.getinsured.hix.platform.config.AEEConfiguration" %>
<jsp:useBean id="date" class="java.util.Date"/>
<fmt:formatDate var="now" value="${date}" pattern="yyyy"/>

<%
 String agencyPortalEnable = DynamicPropertiesUtil.getPropertyValue(AEEConfiguration.AEEConfigurationEnum.AGENCY_PORTAL_ENABLE);
%>

<footer class="container" role="footer">
  <div class="row-fluid margin30-t">

    <div class="pull-left">
      <ul class="nav nav-pills">
        <li id="copyrights">&copy;${now} <%=DynamicPropertiesUtil.getPropertyValue(UIConfiguration.UIConfigurationEnum.BRANDNAME)%>
        </li>
        <li><a href="#" title="Link open in new window or tab"
               onclick="openInNewTab('<%=DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.PRIVACY_URL)%>')" class="margin20-l">Privacy Policy</a>
        </li>
      </ul>
    </div>

    <div class="pull-right">
      <%
        if (GhixConstants.GHIX_ENVIRONMENT.equals("PROD"))
        {
      %>
      <ul class="nav nav-list pull-right" id="build">

        <li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %>
        </a></li>
        <li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %>
        </a></li>

      </ul>
      <% }%>

      <%
        if (GhixConstants.GHIX_ENVIRONMENT.equals("DEV"))
        {
      %>
      <c:set var="assisterRoleName">
        <encryptor:enc value="assisterenrollmententity" isurl="true"/>
      </c:set>
      <c:set var="brokerRoleName">
        <encryptor:enc value="broker" isurl="true"/>
      </c:set>
      <%if(agencyPortalEnable.equalsIgnoreCase("Y")){ %>
          <c:set var="agencyRoleName">
              <encryptor:enc value="agency_manager" isurl="true" />
          </c:set>
          <a href="<c:url value='/account/signup/${agencyRoleName}' />">Agency</a>
          <br/>
          <c:set var="approvedAdminStaffL1">
              <encryptor:enc value="APPROVEDADMINSTAFFL1" isurl="true" />
          </c:set>
          <a href="<c:url value='/account/signup/${approvedAdminStaffL1}' />">Admin Staff L1</a>
          <br/>
          <c:set var="approvedAdminStaffL2">
              <encryptor:enc value="APPROVEDADMINSTAFFL2" isurl="true" />
          </c:set>
          <a href="<c:url value='/account/signup/${approvedAdminStaffL2}' />">Admin Staff L2</a>
          <br/>
      <% } %>
      <a href="<c:url value='/account/signup/${brokerRoleName}' />">Health Insurance Agent/Broker</a>
      <br/>
      <c:set var="assisterRoleName">
        <encryptor:enc value="assisterenrollmententity" isurl="true"/>
      </c:set>
      <a href="<c:url value="/account/signup/${assisterRoleName}"/>">Enrollment Entities</a>

      <ul class="nav nav-list pull-right hide" id="buildshow">
        <li style="white-space: nowrap;"><a href="#">Branch Name: <%= GhixPlatformConstants.BUILD_BRANCH_NAME %>
        </a></li>
        <li style="white-space: nowrap;"><a href="#">Build Number: <%= GhixPlatformConstants.BUILD_NUMBER %>
        </a></li>
        <li style="white-space: nowrap;"><a href="#">Build Date: <%= GhixPlatformConstants.BUILD_DATE %>
        </a></li>

      </ul>
      <% }%>

      <ul class="nav nav-list">
        <li><a id="callSupport" href="#"><span class="aria-hidden">Call Support</span></a></li>
      </ul>
    </div>

  </div>
</footer>
<!-- footer -->
