/**
 * Controller to validate user authentication and redirect as per the authorization
 * @author venkata_tadepalli
 * @since 11/28/2012
 */
package com.getinsured.hix.account.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.Validate;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.getinsured.hix.account.util.UserAccountAuditLog;
import com.getinsured.hix.account.util.UserChangeEmailManager;
import com.getinsured.hix.admin.service.AccountLockNotificationEmail;
import com.getinsured.hix.broker.service.BrokerService;
import com.getinsured.hix.broker.utils.BrokerConstants;
import com.getinsured.hix.cap.consumer.dto.ConsumerDto;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.crm.util.ConsumerEventsUtil;
import com.getinsured.hix.dto.account.AccountUserDto;
import com.getinsured.hix.dto.account.QuestionAnswer;
import com.getinsured.hix.dto.entity.AssisterRequestDTO;
import com.getinsured.hix.dto.entity.AssisterResponseDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterDTO;
import com.getinsured.hix.dto.externalassister.DesignateAssisterRequest;
import com.getinsured.hix.dto.externalassister.DesignateAssisterResponse;
import com.getinsured.hix.duosecurity.DuoWeb;
import com.getinsured.hix.entity.utils.EntityConstants;
import com.getinsured.hix.entity.utils.EntityUtils;
import com.getinsured.hix.entity.web.EERestCallInvoker;
import com.getinsured.hix.indportal.IndividualPortalConstants;
import com.getinsured.hix.model.AccountActivation;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.model.ModuleUser;
import com.getinsured.hix.model.Notice;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.TkmQueueUsersRequest;
import com.getinsured.hix.model.TkmQueueUsersResponse;
import com.getinsured.hix.model.UserRole;
import com.getinsured.hix.model.UserSession;
import com.getinsured.hix.model.UserSessionStatus;
import com.getinsured.hix.model.UserTokens;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.model.entity.Assister;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.platform.accountactivation.ActivationJson;
import com.getinsured.hix.platform.accountactivation.service.AccountActivationService;
import com.getinsured.hix.platform.auditor.GiAudit;
import com.getinsured.hix.platform.auditor.GiAuditParameter;
import com.getinsured.hix.platform.auditor.advice.GiAuditParameterUtil;
import com.getinsured.hix.platform.auditor.advice.GiAuditor;
import com.getinsured.hix.platform.auditor.enums.EventNameEnum;
import com.getinsured.hix.platform.auditor.enums.EventTypeEnum;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.SecurityConfiguration;
import com.getinsured.hix.platform.eventlog.EventInfoDto;
import com.getinsured.hix.platform.eventlog.service.AppEventService;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.duo.DuoAdminApi;
import com.getinsured.hix.platform.security.duo.DuoAdminApi.DuoAdminApiReturnStatus;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.repository.IUserTokensRepository;
import com.getinsured.hix.platform.security.repository.UserRepository;
import com.getinsured.hix.platform.security.scim.service.SCIMUserManager;
import com.getinsured.hix.platform.security.service.GhixGateWayService;
import com.getinsured.hix.platform.security.service.ModuleUserService;
import com.getinsured.hix.platform.security.service.PasswordService;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.security.session.SessionTrackerService;
import com.getinsured.hix.platform.security.tokens.UserTokenService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.DisplayUtil;
import com.getinsured.hix.platform.util.GhixAESCipherPool;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.hix.platform.util.GhixHttpSecurityConfig;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.GhixPlatformConstants;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.IPRestrictor;
import com.getinsured.hix.platform.util.Utils;
import com.getinsured.hix.platform.util.ValidationUtility;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.platform.util.exception.GIRuntimeException.Severity;
import com.getinsured.hix.platform.util.exception.WSO2Exception;
import com.getinsured.hix.screener.util.ScreenerUtil;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.timeshift.TimeShifterUtil;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.thoughtworks.xstream.XStream;

import ch.compile.recaptcha.ReCaptchaVerify;
import ch.compile.recaptcha.model.SiteVerifyResponse;
@Controller
public class UserController {

	//private static final int THIRTY_TWO = 32;

	private static final String ZERO = "0";

	private static final String INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST = "Invalid Verification State for Unlock Request.";

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	@Autowired private SessionTrackerService sessionTracker;
	@Autowired private UserRepository repository;
	@Autowired private RoleService roleService;
	@Autowired private UserService userService;
	@Autowired private AccountActivationService accountActivationService;
	@Autowired private BrokerService brokerService;
	@Autowired private ValidationUtility validationUtility;
	@Autowired private MessageSource messageSource;
	@Autowired private GhixGateWayService ghixGateWayService;
	@Autowired private ReCaptchaVerify reCaptchaV2;
	@Autowired private AccountLockNotificationEmail accountLockNotificationEmail;
	@Autowired private UserDetailsService userDetailsService;
	@Autowired private SCIMUserManager scimUserManager;
	@Autowired private ConsumerEventsUtil consumerEventsUtil;
	@Autowired private ConsumerPortalUtil consumerUtil;
	@Autowired private RestTemplate restTemplate;
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	@Autowired private GhixHttpSecurityConfig ghixHttpSecurityConfig;
	@Autowired private UserTokenService userTokenService;
	@Autowired private IUserTokensRepository iUserTokensRepository;
	@Autowired private UserChangeEmailManager userChangeEmailManager;
	@Autowired private IHouseholdRepository householdRepository;
	@Autowired private AppEventService appEventService;
	@Autowired private LookupService lookupService;
	@Autowired private GhixRestTemplate ghixRestTemplate;
	@Autowired private IPRestrictor ipRestrictor;
	@Autowired private PasswordService passwordService;
	@Autowired private EERestCallInvoker restClassCommunicator;
	@Autowired private EntityUtils entityUtils;
	@Autowired	@Qualifier("screenerUtil") private ScreenerUtil screenerUtil;
	@Autowired
	private GhixPlatformConstants platformConfig;

	@Autowired private UserAccountAuditLog userAccountAuditLog;
	
	private XStream xstream = null;
	private static final String ENCODING_UTF_8 = "UTF-8";

	private static boolean isScimEnabled = GhixPlatformConstants.SCIM_ENABLED;
	private static boolean isSSOEnabled = System.getProperty("GHIX_SECNTX_TYPE").equalsIgnoreCase("saml");
	private static int imCheckFrequencyInMillis = GhixPlatformConstants.WSO2_AVAILABILITY_CHECK_MILLIS;
	private static String samlRedirectionAlias = GhixPlatformConstants.SAML_ALIAS;

	private static final String MFA_ENABLED = "Y";

	private String exceptionMessage="";
	private static final String ERROR_MESSAGE = "errorMsg";
	private static final String EMAILADDRESS_VALIDATE = "We don't have a record of this email. Please try again";
	private static final String SECURITY_QUESTION_VALIDATION = "One or more of these answers do not match the answers in our system. Please try again!";
	private static final String AGENT = "agent";
	private static final String EMPLOYER = "employer";
	private static final String INDIVIDUAL = "individual";
	private static final String EMPLOYEE = "employee";
	private static final String CURRENT_SQ_RETRY_COUNT="currentSqRetryCount";
	private static final String MAX_SQ_RETRY_COUNT="maxSqRetryCount";


	private static final String VERIFY_STATE="verifyState";
	private static final String IS_ADMIN_ROLE = "isAdminRole";
	private static final String IS_VALID = "isValid";
	private static final String INCORRECT_ANSWERS = "One or more of these answers do not match the answers in our system. Please try again!";
	private static final String PHONE_NUMBER = "phoneNumber";
	private static final String PHONE_CODE = "phoneCode";
	private static final String CODE_TYPE = "codeType";
	private static final String PHONECODE_ERRORMSG = "Supplied activation code doesn't match with our record. Please enter a valid activation code or request a new activation code and re-try.";
	private static final String VIEW_PATH_ACCOUNT_UNLOCK_INFORMATION = "account/unlock/information";
	private static final String URL_ACCOUNT_UNLOCK_PASSWORD = "/account/unlock/password";
	private static final String URL_ACCOUNT_UNLOCK_INFORMATION = "/" + VIEW_PATH_ACCOUNT_UNLOCK_INFORMATION;
	private static final Integer EMAIL_VERIFIED = 1;
	private static final Integer ACTIVATIONCODE_VERIFIED = 2;
	private static final Integer SECURITYQUESTIONS_VERIFIED = 3;
	private static final Integer PERSONALINFO_VERIFIED = 4;

	private static final String BROKER = "broker";
	private static final Object START_VERIFICATION = 0;
	private static Timer timer = null;
	private static WSO2AvailabilityCall wso2Availability = null;
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	
	@PostConstruct
	public void initiateTime(){
		this.xstream = GhixUtils.getXStreamGenericObject();
		if(isSSOEnabled){
			LOGGER.info("Initiating WSO2 availability check");
			if(timer == null){
				wso2Availability = new WSO2AvailabilityCall();
				if(imCheckFrequencyInMillis > 0){
					timer = new Timer();
					timer.schedule(wso2Availability,10000,imCheckFrequencyInMillis);
				}
			}
		}
	}

	@PreDestroy
	public void stopTimer(){
		if(isSSOEnabled && timer != null){
			LOGGER.info("Stopping WSO2 availability check");
			timer.cancel();
		}
	}
	
	//To be removed
	/*@RequestMapping(value="/account/user/deactivate")
	@ResponseBody
	public String deactivateUsers(Model model, HttpServletRequest request) {
		List<String> strs = userService.disableInactiveAccounts();
		System.out.println(strs);
		return strs.toString();
	}
	*/
	
	@RequestMapping(value="/account/user/login")
	public String loginPage(Model model, HttpServletRequest request) {
		boolean blnReferral = false;
		String ssapReferralAppId = null;
		String referralId = null;
		String firstTimeLinking=null;
		/* HIX-40546 - Changes for State Referral Account Starts*/
		if(request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL) != null && request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL).equals("Y"))
		{
			LOGGER.debug("This Login is via Referral Activation");
			blnReferral = true;
			ssapReferralAppId = request.getSession().getAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID) +"";
			referralId = request.getSession().getAttribute(ReferralConstants.REFERRAL_ID) !=null ? String.valueOf(request.getSession().getAttribute(ReferralConstants.REFERRAL_ID)): null;
			firstTimeLinking= request.getSession().getAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING)!=null ? String.valueOf(request.getSession().getAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING)) : null;
		}
		/* HIX-40546 - Changes for State Referral Account Ends*/

		/* HIX-56933 - Account Activation Flow - Starts */
		/**
		 * Check if User came through account activation flow link and preferred
		 * NOT to Register instead login to an exiting account
		 *
		 * Store this information later in this method back to session.
		 */
		boolean blnAccountActivation = false;
		String activationId = null;
		if(request.getSession().getAttribute("accountActivationFlow") != null && request.getSession().getAttribute("accountActivationFlow").equals("true"))
		{
			blnAccountActivation = true;
			activationId = request.getSession().getAttribute("activationId") +"";
			LOGGER.debug("This Login is via Account Activation - activationId  - " + activationId);
		}
		/* HIX-56933 - Account Activation Flow - Ends */

		// HIX-29686 - Darshan Hardas : If it is Duo-Security post-init action sent it to login success
		request.getSession().setAttribute("IS_PASSWORD_EXPIRED", false);
	 	request.getSession().setAttribute("AUTH_FAILED_PASSWORD_MAX_AGE",false);
		AccountUser user = checkDuoSecurityFlowAtLogin(request);
		if(user !=null) {
			return "forward:/account/user/loginSuccess";
		}

		/*
		 * HIX-28796 :When adding a new role Confirm Your Identity text is missing on login page
		 *            added confirmId parameter
		 */
		String confirmId = request.getParameter("confirmId");
		if(StringUtils.isNotBlank(confirmId)){
			model.addAttribute("confirmId",confirmId);
		}else{
			//HIX-29983 : To handle redirection.
			 HttpSession session = request.getSession(false);
		     if (session != null) {
		    	 LOGGER.debug("usercontroller logout session : " + session.getId());
		    	 this.sessionTracker.markUserSessionLoggedOutByApplication(session.getId());
		    	 session.invalidate();
		    	 SecurityContextHolder.clearContext();
		     }
		}

		HttpSession session = request.getSession();
		session.setAttribute("module", request.getParameter("module"));

		/* HIX-40546 - Changes for State Referral Account Starts*/
		if(blnReferral)
		{
			session.setAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL, "Y");
			session.setAttribute(IndividualPortalConstants.REFERRAL_SSAP_APPLICATION_ID, ssapReferralAppId);
			session.setAttribute(ReferralConstants.REFERRAL_LINKING_FLOW, "true");
			session.setAttribute(ReferralConstants.REFERRAL_ID, referralId);
			request.getSession().setAttribute(ReferralConstants.FIRST_TIME_REFERRAL_LINKING, firstTimeLinking);
		}
		/* HIX-40546 - Changes for State Referral Account Ends*/

		/* HIX-56933 - Account Activation Flow - Starts */
		/**
		 * Re-Storing activation information back to session.
		 */
		if(blnAccountActivation){
			session.setAttribute("loginWithAccountActivationFlow", "true");
			session.setAttribute("accountActivationFlow", "true");
			session.setAttribute("activationId", activationId);
		}
		/* HIX-56933 - Account Activation Flow - Ends */


		//Commented as Remember-Me key has been removed from applicationSecurity-db.xml
		// To redirect the remembered user to their home-page instead of login page.
		// 	HIX-15569 - Darshan Hardas
//		try {
//			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//			Object loggedInUser = (auth != null) ? auth.getPrincipal() :  null;
//
//			if (loggedInUser instanceof AccountUser) {
//				AccountUser user = (AccountUser) loggedInUser;
//				LOGGER.info("Auto login to be done for user: " + user.getUsername());
//
//				return "redirect:/account/user/loginSuccess";
//			}
//		} catch (Exception e) {
//			LOGGER.error("Auto login failed redirecting to login page.", e);
//		}

		LOGGER.debug("User Login page ");

		//Check if SSO is enabled
		if(isSSOEnabled){
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("SSO Enabled, checking if Identity Server is available");
			}
			if(wso2Availability.isWsO2Available()){
				if("IDP".equalsIgnoreCase(GhixPlatformConstants.PLATFORM_SSO_STRATEGY)){
					String redirUrl = platformConfig.getIDPInitiatedSSO(null);
					if(redirUrl == null) {
						throw new RuntimeException("IDP Initiated SSO expected but no target URL for authentication");
					}
					return "redirect:"+redirUrl;
				}else {
					//Default SP
					return "redirect:/saml/login/alias/"+samlRedirectionAlias+"?disco=true";
				}
			}else{
				//If WSO2 is unavailable, re-direct to SSO maintenance page
				LOGGER.error("No Identity server available");
				return "account/ssoerrorpage";
			}
		}

		model.addAttribute("page_title", "Getinsured Health Exchange: Login" );
		return "account/user/login";
	}

	/**
	 * HIX-29686 - Darshan Hardas : If it is Duo-Security post-init action sent it to login success
	 * This function will give the spring security principal if the flow is coming from duosecurity auth.
	 * @param request
	 * @return The authenticated user or null
	 */
	private AccountUser checkDuoSecurityFlowAtLogin(HttpServletRequest request) {
		AccountUser user = null;

		String duoSigResponse = request.getParameter("sig_response");
		if(!StringUtils.isEmpty(duoSigResponse)) {
			Authentication authToken = (AbstractAuthenticationToken)request.getSession().getAttribute("authToken");
			SecurityContextHolder.getContext().setAuthentication( authToken);

			if(authToken != null) {
				request.getSession().removeAttribute("authToken");
			}

			Object loggedInUser = (authToken != null) ? authToken.getPrincipal() :  null;
			if (loggedInUser instanceof AccountUser) {
				user = (AccountUser) loggedInUser;
				LOGGER.info("Auto login to be done for user: " + user.getId());
			}
		}
		return user;
	}
	//account self-unlock
		@RequestMapping(value="/account/unlock/email", method = {RequestMethod.GET, RequestMethod.POST })
		@GiAudit(transactionName = "User Unlock account",
		eventType = EventTypeEnum.ACCESS_PII_DATA,
		eventName = EventNameEnum.PII_WRITE
		)
		public String unlockEmail(Model model, HttpServletRequest request,HttpServletResponse response,
				@RequestParam(value = "g-recaptcha-response", required = false) String gRecaptchaResponse,
				ServletRequest servletRequest) {
			String enteredEmailAddress = request.getParameter("emailAddress");
			if(isSSOEnabled){
				if(request.getMethod().equals("POST")){
					LOGGER.error("User attempted unlock account using email:"+ enteredEmailAddress +" This feature is not vailable in a SSO Enabled environment");
					model.addAttribute("errorMessage", "This feature is no longer available");
					return "/account/customerHelp";
				}
			}

			boolean hideCaptcha = ("Y").equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.HIDECAPTCHA));
			model.addAttribute("hideCaptcha", hideCaptcha);

			if(enteredEmailAddress != null) {
				// Get the email address from the database
				AccountUser user = userService.findByEmail(enteredEmailAddress.toLowerCase());
				if(user != null) {

					// Check for CAPTCHA Validation.
					String remoteAddress = servletRequest.getRemoteAddr();
					if(!hideCaptcha) {
						boolean isValidCAPTCHA = false;
						try {
							SiteVerifyResponse siteVerifyResponse = reCaptchaV2.verify(gRecaptchaResponse);
							if ( siteVerifyResponse.isSuccess() ) {
								isValidCAPTCHA = true;
							} else {
								isValidCAPTCHA = false;
							}
						} catch (IOException e) {
							isValidCAPTCHA = true;//setting to true incase of captcha issue.
							LOGGER.error("Error happened while validating recaptcha",e);
						}
						if(!isValidCAPTCHA){
							model.addAttribute("captchaError", "Incorrect Security Code");
							model.addAttribute("userEmailAddress", enteredEmailAddress);
							return "account/unlock/email";
						}
					}
					request.getSession().setAttribute(VERIFY_STATE, EMAIL_VERIFIED);
					request.getSession().setAttribute("user", user);
					return "redirect:/account/unlock/phone";
				}
				else
				{
					 model.addAttribute(ERROR_MESSAGE, EMAILADDRESS_VALIDATE);
					 return "account/unlock/email";
				}
			}
			return "account/unlock/email";

		}

		@RequestMapping(value="/account/unlock/phone", method = {RequestMethod.GET})
		public String unlockPhone(Model model, HttpServletRequest request) {
			String enteredEmailAddress = request.getParameter("emailAddress");
			if(isSSOEnabled){
				if(request.getMethod().equals("POST")){
					LOGGER.error("User attempted unlock account using email:"+ enteredEmailAddress +" This feature is not vailable in a SSO Enabled environment");
					model.addAttribute("errorMessage", "This feature is no longer available");
					return "/account/customerHelp";
				}
			}
			//Get user object from session..  if user object is null then redirect to email verification page
			AccountUser user = (AccountUser)request.getSession().getAttribute("user");
			//Get the phone number of user
			String phoneNumber = user.getPhone();

			Role userRole = userService.getDefaultRole(user);
			if(userRole != null &&
					!userRole.getName().equalsIgnoreCase("BROKER") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYEE") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYER") &&
					!userRole.getName().equalsIgnoreCase("INDIVIDUAL")){
				request.getSession().setAttribute(IS_ADMIN_ROLE, "ADMIN");
				model.addAttribute(IS_ADMIN_ROLE, "ADMIN");
			}

			model.addAttribute("phoneNumber", phoneNumber);
			return "account/unlock/phone";
		}

		/**
		 * Verify account activation code for Unlocking users
		 *
		 * @param request
		 *            {@link HttpServletRequest}
		 * @param model
		 *            {@link Model}
		 * @return URL for navigation
		 * @throws GIException
		 */
		@RequestMapping(value = "/account/unlock/sendcodeunlocking",  method = RequestMethod.POST)
		public String phoneVerificationToUnlock(HttpServletRequest request, Model model) throws GIException {
			LOGGER.info("phoneVerificationToUnlock : START");
			if(isSSOEnabled){
				model.addAttribute("errorMessage", "This feature is no longer available");
				return "/account/customerHelp";
			}
			final String ACTIVATION_CODE = "activationCode";

			Integer currVerifiedState = (Integer) request.getSession().getAttribute(VERIFY_STATE);
			String isAdminR = (String) request.getSession().getAttribute(IS_ADMIN_ROLE);

			// For ProgressBar to UI based on Role.
			if(isAdminR != null){
				model.addAttribute(IS_ADMIN_ROLE, isAdminR);
			}

			Validate.notNull(currVerifiedState, "Verified state for current unloack request is missing.");

			if(!currVerifiedState.equals(EMAIL_VERIFIED)) {
				throw new GIException(INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST);
			}

			//String toPhoneNumber = request.getParameter(PHONE_NUMBER);
			String phoneCode = request.getParameter(PHONE_CODE);
			String codeType = request.getParameter(CODE_TYPE);

			AccountUser user = (AccountUser)request.getSession().getAttribute("user");
			//Get the phone number of user
			String phoneNumber = user.getPhone();

			if (StringUtils.isEmpty(phoneCode)) {
				// Generate new activation code and Send new sms/call.
				String msg = accountActivationService.sendCode(phoneNumber, 123, codeType, request);
				if (!StringUtils.equals(msg, "SUCCESS")) {
					model.addAttribute(ERROR_MESSAGE, msg);
				}
			} else {
				// Get the activation code set in session
				String activationCodeGenerated = (String)request.getSession().getAttribute(ACTIVATION_CODE);
				// Verify entered activation/phone code
				if (!StringUtils.equals(activationCodeGenerated, phoneCode)) {


					model.addAttribute(PHONE_NUMBER, phoneNumber);
					model.addAttribute(ERROR_MESSAGE, PHONECODE_ERRORMSG);
					return "account/unlock/phone";
				}
				request.getSession().setAttribute(VERIFY_STATE, ACTIVATIONCODE_VERIFIED);
				return "redirect:/account/unlock/questions";
			}
			LOGGER.info("phoneVerificationToUnlock : END");
			return "redirect:/account/unlock/phone";
		}


	/**
	* This method check security answers provided by user with securityAnswers stored in system & redirect to jsp based on user role.
	*
	* @param model
	* @param request
	* @return
	 * @throws GIException
	*/
	@RequestMapping(value="/account/unlock/questions", method = {RequestMethod.GET, RequestMethod.POST })
	public String unlockSecurityQuestion(@ModelAttribute("accountUserDto") AccountUserDto accountUserDto,Model model, HttpServletRequest request) throws GIException {

		
		// get AccountUser object from session.
		AccountUser userObj= (AccountUser)request.getSession().getAttribute("user");
		Integer currVerifiedState = (Integer) request.getSession().getAttribute(VERIFY_STATE);

		String isAdminR = (String) request.getSession().getAttribute(IS_ADMIN_ROLE);

		// For ProgressBar to UI based on Role.
		if(isAdminR != null){
			model.addAttribute(IS_ADMIN_ROLE, isAdminR);
		}

		Validate.notNull(currVerifiedState, "Verified state for current unloack request is missing.");

		if(!currVerifiedState.equals(ACTIVATIONCODE_VERIFIED)) {
			throw new GIException(INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST);
		}

		if(userObj==null){
			return "account/unlock/email";
		}

		int NoOfSecQstnAns=accountUserDto.getUserSecQstnAnsList().size();

		if(NoOfSecQstnAns==0){
			// Accessing the uri with GET , as firstime
			//get securityAnswers.
			List<QuestionAnswer> dbUserQstnAnsList= AccountUserDto.getUserSecurityQuestionAnswers(userObj);
			for(QuestionAnswer dbQuestionAnswer:dbUserQstnAnsList){
				int idx = dbQuestionAnswer.getIdx();
				QuestionAnswer currQuestionAnswer = new QuestionAnswer(idx,dbQuestionAnswer.getQuestion(),"");
				accountUserDto.getUserSecQstnAnsList().add(currQuestionAnswer);
			}
			int maxSqRetryCount = userService.getUsersMaxRetryCount(userObj);
			request.getSession().setAttribute(MAX_SQ_RETRY_COUNT, maxSqRetryCount);
			request.getSession().setAttribute(CURRENT_SQ_RETRY_COUNT, userObj.getSecQueRetryCount());
			return "account/unlock/questions";
		}

		// get updated AccountUser object.
	/*	AccountUser user = userService.findById(userObj.getId());
		if(user==null){
			return "account/unlock/email";
		}*/
		List<QuestionAnswer> dbUserQstnAnsList= AccountUserDto.getUserSecurityQuestionAnswers(userObj);
		boolean hasQstnAnsMatch=accountUserDto.questionAnswersEqualWith(dbUserQstnAnsList);


		int maxAttempts = (int)request.getSession().getAttribute(MAX_SQ_RETRY_COUNT);
		int currSqRetryCount = (int)request.getSession().getAttribute(CURRENT_SQ_RETRY_COUNT) + 1;
		//check securityAnswers provided by user with securityAnswers stored in system.
		if((userObj.getConfirmed() == 0) &&
				(currSqRetryCount < maxAttempts) &&
				hasQstnAnsMatch){
			//updateSecQueRetryCount to 0 as user provided correct security answers.
			userService.updateSecQueRetryCount(userObj, 0);
			//get use role to redirect password page or information page.
			request.getSession().setAttribute(VERIFY_STATE, SECURITYQUESTIONS_VERIFIED);
			//Remove the session attributes that has used only to the scope of /unlock/questions
			request.getSession().removeAttribute(MAX_SQ_RETRY_COUNT);
			request.getSession().removeAttribute(CURRENT_SQ_RETRY_COUNT);
			Role userRole = userService.getDefaultRole(userObj);
			if(userRole != null &&
					!userRole.getName().equalsIgnoreCase("BROKER") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYEE") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYER") &&
					!userRole.getName().equalsIgnoreCase("INDIVIDUAL")){
				model.addAttribute("STEP_NO", "Step 4: Change your password");
				return "account/unlock/password";
			}

			return "redirect:"+ URL_ACCOUNT_UNLOCK_INFORMATION;
		}else{
			String errorMessage = StringUtils.EMPTY;


			request.getSession().setAttribute(CURRENT_SQ_RETRY_COUNT,currSqRetryCount) ;
			//if(isNoOfAttemptsReachedMaxCount(user,noOfRetry,maxAttempts)){
			if(currSqRetryCount >= maxAttempts){
				//several unsuccessful attempts. make sure re-lock the account and send the email
				errorMessage = "Your account has been locked due to several unsuccessful attempts. Please call the customer service center at " + DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_PHONE) + " to unlock your account.";

				userService.updateConfirmedAndSecQueRetryCount(userObj, 0, maxAttempts);
				//HIX-29631 Sends Email on Account Lock
				try {
					boolean skipMail=true;
					if(!skipMail)
						sendAccountLockMail(userObj);
				} catch (NotificationTypeNotFound e) {
					LOGGER.error(e.getMessage());
					LOGGER.error("Account Lock e-mail cannot be sent - ", e);
				}
			//	LOGGER.info(" User '" + userObj.getUsername() + "' is locked. Retry Count " + currSqRetryCount + " reached max retry count " + maxAttempts);

			}else{
				// Supplied answers didn't match with our records...
				errorMessage = INCORRECT_ANSWERS;

				// update no. of retry for user
				userService.updateSecQueRetryCount(userObj, currSqRetryCount);

				//LOGGER.info(" User '" + userObj.getUsername() + "' security retry count increased by " + currSqRetryCount);
			}

			model.addAttribute(ERROR_MESSAGE, errorMessage);
			return "account/unlock/questions";
		}
	}

	/**
	 * HIX-29631
	 * Sends email for account UnLock
	 *
	 * @param userObj
	 *            {@link AccountUser}
	 * @throws NotificationTypeNotFound
	 *             Exception
	 */
	private void sendAccountLockMail(AccountUser userObj) throws NotificationTypeNotFound {
		LOGGER.trace("sendAccountLockMail : START");

		Map<String, Object> notificationContext = new HashMap<String, Object>();
		notificationContext.put("USER_OBJ", userObj);
		Map<String, String> tokens = accountLockNotificationEmail.getTokens(notificationContext);
		Map<String, String> emailData = accountLockNotificationEmail.getEmailData(notificationContext);
		Location location = null; //TODO: location has to be provided here 
		
		Notice noticeObj = accountLockNotificationEmail.generateEmail(accountLockNotificationEmail.getClass().getSimpleName(),location, emailData, tokens);
		if(noticeObj == null){
			throw new NotificationTypeNotFound("Failed to find the notification for location:"+accountLockNotificationEmail.getClass().getSimpleName());
		}
		if(LOGGER.isTraceEnabled()){
			LOGGER.trace((noticeObj.getEmailBody()));
		}
		accountLockNotificationEmail.sendEmail(noticeObj);
		LOGGER.trace("sendAccountLockMail : END");
	}

	/**
	 * Method to return view for verifying personal information of User to unlock.
	 * @param model The Model instance.
	 * @param request The HttpServletRequest instance
	 * @return String The view name.
	 */
	@RequestMapping(value=URL_ACCOUNT_UNLOCK_INFORMATION, method = {RequestMethod.GET})
	public String unlockPersonalInfoView(Model model, HttpServletRequest request) {
		LOGGER.info("unlockPersonalInfoView : START");
		AccountUser userObj= (AccountUser)request.getSession().getAttribute("user");

		String isAdminR = (String) request.getSession().getAttribute(IS_ADMIN_ROLE);

		// For ProgressBar to UI based on Role.
		if(isAdminR != null){
			model.addAttribute(IS_ADMIN_ROLE, isAdminR);
		}

		if(userObj==null){
			return "account/unlock/email";
		}
		Role userRole = userService.getDefaultRole(userObj);
		model.addAttribute("userRole", userRole.getName());
		LOGGER.info("unlockPersonalInfoView : END");
		return VIEW_PATH_ACCOUNT_UNLOCK_INFORMATION;
	}

	/**
	 * Method to process verification of personal information of User for unlocking.
	 *
	 * @param model The user's Model instance.
	 * @param request The current HttpServletRequest instance.
	 * @return String The view name.
	 */
	@RequestMapping(value=URL_ACCOUNT_UNLOCK_INFORMATION, method = {RequestMethod.POST })
	public String unlockPersonalInfo(Model model, HttpServletRequest request) {
		LOGGER.info("unlockPersonalInfo : START");

		boolean isValid = false;
		HashMap<String, Object> resultMap = null;

		try {
			Validate.notNull(userService, "The UserService instance is missing.");
			Validate.notNull(brokerService, "The BrokerService instance is missing.");

			Validate.notNull(request, "The Request instance is missing.");

			Validate.notNull(request.getSession(), "Session is not associated with current request.");

			Integer currVerifiedState = (Integer) request.getSession().getAttribute(VERIFY_STATE);

			Validate.notNull(currVerifiedState, "Verified state for current unloack request is missing.");

			if(!currVerifiedState.equals(SECURITYQUESTIONS_VERIFIED)) {
				throw new GIException(INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST);
			}

			//Integer verifyUserId = (Integer)request.getSession().getAttribute(VERIFY_USER_ID);

			//Validate.notNull(verifyUserId, "User to unlock is missing information in request.");
			AccountUser userInSession = (AccountUser)request.getSession().getAttribute("user");
			AccountUser user = userService.findById(userInSession.getId());

			Validate.notNull(user, "User not found in records.");

			Validate.isTrue(BooleanUtils.isFalse(user.isEnabled()), "Account is already unlocked.");

			//LOGGER.info("Verifying user: " + user.getUsername() + " to unlock.");

			//Validate.notNull(user.getDefRole(), "No Role associated with current user.");
			//Validate.notNull(user.getDefRole().getName(), "No role name associated with given user.");
			//Validate.notEmpty(user.getDefRole().getName().trim(), "Invalid user name associated with given user");
			AccountUser userObj= (AccountUser)request.getSession().getAttribute("user");
			Role userRole = userService.getDefaultRole(userObj);
			model.addAttribute("userRole", userRole.getName());

			//String currRoleName = user.getDefRole().getName().trim().toLowerCase();
			String currRoleName = userRole.getName().trim().toLowerCase();

			if(currRoleName.equalsIgnoreCase(BROKER) || currRoleName.equalsIgnoreCase(AGENT) || currRoleName.equalsIgnoreCase(EMPLOYER) || currRoleName.equalsIgnoreCase(INDIVIDUAL) || currRoleName.equalsIgnoreCase(EMPLOYEE)) {
				resultMap = validatePersonalInformation(request, user, currRoleName);

				if(resultMap.get(IS_VALID) != null){
          isValid = Boolean.valueOf(resultMap.get(IS_VALID).toString());
        }
			}
			else {
				LOGGER.warn("'" + currRoleName + "' User unlock not supported by this unlock mechanism.");
				throw new InvalidUserException("The current User type account unlocking is not supported by this unlock mechanism.");
			}


			if(isValid) {
				request.getSession().setAttribute(VERIFY_STATE, PERSONALINFO_VERIFIED);
				return "redirect:" + URL_ACCOUNT_UNLOCK_PASSWORD;
			}
			else {
				model.addAttribute(ERROR_MESSAGE, resultMap.get(ERROR_MESSAGE));

				return VIEW_PATH_ACCOUNT_UNLOCK_INFORMATION;
			}
		}
		catch (Exception e) {
			LOGGER.error("Unlock failed redirecting to login page.", e);

			model.addAttribute("page_title", "Getinsured Health Exchange: Login" );
			model.addAttribute(ERROR_MESSAGE, e.getMessage());

			return "account/user/login";
		}
		finally {
			LOGGER.info("unlockPersonalInfo : END");
		}
	}

	/**
	 * Method to validate personal information of supported user to unlock.
	 *
	 * @param request The current HttpServletRequest instance.
	 * @param currUser The current user's AccountUser instance.
	 * @param currRoleName The current User Role name.
	 * @return resultantMap The resultant collection.
	 */
	private HashMap<String, Object> validatePersonalInformation(HttpServletRequest request, AccountUser currUser, String currRoleName) {
		LOGGER.info("validatePersonalInformation : START");

		HashMap<String, Object> resultantMap = null;
		Location primaryLocation = null;
		try {
			if(currRoleName.equals(BROKER) || currRoleName.equals(AGENT)) {
				Broker broker = brokerService.findBrokerByUserId(currUser.getId());

				Validate.notNull(broker, "Agent record for the user not found.");

				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Agent identifier is " + broker.getId());
				}

				primaryLocation =  broker.getLocation();

				resultantMap = validatePersonalInformationZipCode(request, primaryLocation);

				if((Boolean)resultantMap.get(IS_VALID)) {
					resultantMap = validatePersonalInformationFederalTaxId(request, broker.getFederalEIN());
				}
			}
			
			else if(currRoleName.equals(INDIVIDUAL)) {
				Household household = householdRepository.findByUserId(currUser.getId());
				primaryLocation = household.getPrefContactLocation();

        resultantMap = validatePersonalInformationZipCode(request, primaryLocation);

        if((Boolean)resultantMap.get(IS_VALID)) {
          resultantMap = validatePersonalInformationSSN(request, household.getSsn().substring(5));
        }
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while validating personal information for user.", e);

			if(null == resultantMap) {
				resultantMap = new HashMap<String, Object>();
			}

			resultantMap.put(IS_VALID, false);
			resultantMap.put(ERROR_MESSAGE, e.getMessage());
		}
		finally {
			LOGGER.info("validatePersonalInformation : END");
		}

		return resultantMap;
	}

	/**
	 * Method to verify Zip code personal information of User.
	 *
	 * @param request The current HttpServletRequest instance
	 * @param primaryLocation The current user's Location instance.
	 * @return verificationResultant The verification result Map.
	 */
	private HashMap<String, Object> validatePersonalInformationZipCode(HttpServletRequest request, Location primaryLocation) {
		LOGGER.info("validatePersonalInformationZipCode : START");

		HashMap<String, Object> verificationResultant = new HashMap<String, Object>();
		try {
			Validate.notNull(primaryLocation, "Primary Location information for the user not found.");
			Validate.notNull(primaryLocation.getZip(), "Zipcode information not found for User.");
			Validate.notEmpty(primaryLocation.getZip(), "Zipcode information is missing for User.");

			String currZipCode = request.getParameter("zipCode");

			Validate.notNull(currZipCode, "Zip code field is required.");
			Validate.notEmpty(currZipCode, "Zip code field is mandatory.");

			if(currZipCode.equalsIgnoreCase(primaryLocation.getZip())) {
				verificationResultant.put(IS_VALID, true);
			}
			else {
				verificationResultant.put(IS_VALID, false);
				verificationResultant.put(ERROR_MESSAGE, SECURITY_QUESTION_VALIDATION);
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while validating user's Zip Code personal information.", e);

			verificationResultant.put(IS_VALID, false);
			verificationResultant.put(ERROR_MESSAGE, e.getMessage());
		}
		finally {
			LOGGER.info("validatePersonalInformationZipCode : END");
		}

		return verificationResultant;
	}

	/**
	 * Method to verify FederalTaxId personal information of User.
	 *
	 * @param request The current HttpServletRequest instance
	 * @param federalTaxId The user's federalTaxId.
	 * @return verificationResultant The verification result Map.
	 */
	private HashMap<String, Object> validatePersonalInformationFederalTaxId(HttpServletRequest request, String federalTaxId) {
		LOGGER.info("validatePersonalInformationFederalTaxId : START");

		HashMap<String, Object> verificationResultant = new HashMap<String, Object>();
		try {
			Validate.notNull(federalTaxId, "Federal Tax identifier information for the user not found.");
			Validate.notEmpty(federalTaxId, "Federal Tax identifier information is missing for User.");

			String currTaxid = request.getParameter("taxId");

			Validate.notNull(currTaxid, "Federal tax identifier field is required.");
			Validate.notEmpty(currTaxid, "Federal tax identifier field is mandatory.");

			if(currTaxid.trim().equalsIgnoreCase(federalTaxId.trim())) {
				verificationResultant.put(IS_VALID, true);
			}
			else {
				verificationResultant.put(IS_VALID, false);
				verificationResultant.put(ERROR_MESSAGE, SECURITY_QUESTION_VALIDATION);
			}
		}
		catch(Exception e) {
			LOGGER.error("Exception occurred while validating user's Federal tax identifier personal information.", e);

			verificationResultant.put(IS_VALID, false);
			verificationResultant.put(ERROR_MESSAGE, e.getMessage());
		}
		finally {
			LOGGER.info("validatePersonalInformationFederalTaxId : END");
		}

		return verificationResultant;
	}

  private HashMap<String, Object> validatePersonalInformationSSN(HttpServletRequest request, String ssn) {
    LOGGER.info("validatePersonalInformationSSN : START");

    HashMap<String, Object> verificationResultant = new HashMap<String, Object>();
    try {
      Validate.notNull(ssn, "SSN information for the user not found.");
      Validate.notEmpty(ssn, "SSN information is missing for User.");

      String currSSN = request.getParameter("ssn");

      Validate.notNull(currSSN, "SSN field is required.");
      Validate.notEmpty(currSSN, "SSN field is mandatory.");

      if(currSSN.trim().equalsIgnoreCase(ssn.trim())) {
        verificationResultant.put(IS_VALID, true);
      }
      else {
        verificationResultant.put(IS_VALID, false);
        verificationResultant.put(ERROR_MESSAGE, SECURITY_QUESTION_VALIDATION);
      }
    }
    catch(Exception e) {
      LOGGER.error("Exception occurred while validating user's SSN personal information.", e);

      verificationResultant.put(IS_VALID, false);
      verificationResultant.put(ERROR_MESSAGE, e.getMessage());
    }
    finally {
      LOGGER.info("validatePersonalInformationSSN : END");
    }

    return verificationResultant;
  }

	@RequestMapping(value="/account/unlock/password", method = RequestMethod.GET)
	public String unlockChangePassword(Model model, HttpServletRequest request) {
		return "account/unlock/password";
	}

	@RequestMapping(value="/account/unlock/password", method = RequestMethod.POST)
	public String unlockChangePassword(Model model, @ModelAttribute("newPassword") String newPassword,
			@ModelAttribute("confirmPassword") String confirmPassword, HttpServletRequest request) {

		String message = "";
		boolean error = false;

		try {
			//AccountUser user = repository.findByUserName(userService.getLoggedInUser().getUsername());
			//AccountUser user = repository.findByUserName("mona1@yopmail.com");
			AccountUser user = (AccountUser)request.getSession().getAttribute("user");

			Integer currVerifiedState = (Integer) request.getSession().getAttribute(VERIFY_STATE);

			Validate.notNull(currVerifiedState, "Verified state for current unloack request is missing.");

			Role userRole = userService.getDefaultRole(user);
			if(userRole != null &&
					!userRole.getName().equalsIgnoreCase("BROKER") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYEE") &&
					!userRole.getName().equalsIgnoreCase("EMPLOYER") &&
					!userRole.getName().equalsIgnoreCase("INDIVIDUAL")){
				if(!currVerifiedState.equals(SECURITYQUESTIONS_VERIFIED)) {
					throw new GIException(INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST);
				}
			} else if(!currVerifiedState.equals(PERSONALINFO_VERIFIED)) {
				throw new GIException(INVALID_VERIFIED_STATE_FOR_UNLOCK_REQUEST);
			}
			//String password = user.getPassword();
			if(!newPassword.equals(confirmPassword)) {
				throw new GIException("New Password and Confirm Password does not match");
			} else if(!validationUtility.checkForPasswordComplexity(newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX)) ||
					   validationUtility.isPasswordAvailableInPasswordHistory(user.getUsername(), newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))
					   )
					{
						message = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistoryandminage", null, LocaleContextHolder.getLocale()));
						error = Boolean.TRUE;

					} else {
						if(isScimEnabled){
							LOGGER.info("Account Unlock: Using SCIM Manager to update the password for remote user");
							if(!scimUserManager.changePassword(user, newPassword)){
								throw new WSO2Exception("Unable to change password for remote user");
							}
							user.setPasswordLastUpdatedTimeStamp(new TSDate());
							user.setRetryCount(0);
							user.setSecQueRetryCount(0);
							user.setConfirmed(1);
							user.setPasswordRecoveryToken(null);
							user.setPasswordRecoveryTokenExpiration(null);
							user.setPassword("PASSWORD_NOT_IN_USE");
							user.setStatus("Active");
							if(userService.getLoggedInUser() != null){
								user.setLastUpdatedBy(userService.getLoggedInUser().getId());
							}
							repository.save(user);
						}
						else{
							userService.changePassword(user.getId(), newPassword);
							userService.updateStatusCode("Active", user.getId());
						}
		
				message = BrokerConstants.SUCCESS_MESSAGE;
			}
		}
		catch (Exception ex) {
			message = BrokerConstants.FAILURE_MESSAGE;
			error = true;
			LOGGER.error("Exception in changing the password", ex);
		}
		request.getSession().setAttribute(VERIFY_STATE, START_VERIFICATION);
		model.addAttribute(BrokerConstants.MESSAGE, message);
		model.addAttribute(BrokerConstants.ERROR, error);
		model.addAttribute("isShow", true);

		return "account/unlock/password";
	}
	
	private AccountUser provisionUserIfRequired(AccountUser user) throws InvalidUserException, GIException{
		AccountUser tmpUser = user;
		if(!tmpUser.isHasGhixProvisioned())
		{
			if(LOGGER.isDebugEnabled()){
				LOGGER.debug("Authenticated user provisioning flow (IDALINK) initiated for user id:"+tmpUser.getEmail());
			}
			Role roleForNewUser = roleService.findRoleByName(tmpUser.getGhixProvisionRoleName().toUpperCase());
			if(roleForNewUser==null){
				String newUserRole = tmpUser.getGhixProvisionRoleName().toUpperCase();
				LOGGER.error("Invalid User :: Role ("+newUserRole+" does not defined in GHIX");
				//request.getSession().setAttribute("isUserFullyAuthorized", "false");
				throw new InvalidUserException("Invalid User :: Role ("+newUserRole+" does not defined in GHIX");
			}
			tmpUser = userService.createUser(tmpUser, tmpUser.getGhixProvisionRoleName());
		}
		Set<UserRole> userRoles = userService.getActiveUserRole(tmpUser);
		tmpUser.setUserRole(userRoles);
		return tmpUser;
	}

	/**
	 * Modified to get the redirect url (a.k.a landingPage) from getLandingPage(..)
	 * @author venkata_tadepalli
	 * @throws Exception
	 * @since 11/28/2012
	 */
	@RequestMapping(value="/account/user/loginSuccess")
	@GiAudit(transactionName = "User login",
	 eventType = EventTypeEnum.AUTHENTICATION,
	 eventName = EventNameEnum.USER_LOGIN_SUCCESSFUL
	 )
	//public String loginSuccess(Model model, UsernamePasswordAuthenticationToken authToken,  HttpServletRequest request) {
	public String loginSuccess(Model model, AbstractAuthenticationToken authToken,  HttpServletRequest request) throws Exception {

		int userId = -1;
		boolean isPrivileged = false;
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object loggedInUser = (auth != null) ? auth.getPrincipal() :  null;
		UserSession userSession = null;
		HttpSession httpSession = null;
		Household household = null;
		Role currentRole = null;
        if (loggedInUser instanceof AccountUser) {
               AccountUser user = (AccountUser) loggedInUser;
               currentRole = this.userService.getDefaultRole(user);// User always logs in with default user role
               String defaultRoleName = currentRole.getName();
               request.getSession().setAttribute("defaultRoleName", defaultRoleName);
               
               if(currentRole.getLoginRestricted() ==1){
   					if(!this.ipRestrictor.checkIpAccess()){
   						return "redirect:/account/user/logout";
   					}
   				}
             //  if(!validationUtility.checkForPasswordMaxAge(user,DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MAX_AGE)))
               if(!validationUtility.checkForPasswordMaxAge(user,String.valueOf(this.passwordService.getPasswordExpiryPeriod(user))))
               {
            	   request.getSession().invalidate();
            	   SecurityContextHolder.clearContext();
            	   request.getSession().setAttribute("AUTH_FAILED_MSG_PASSWORD_MAX_AGE",  messageSource.getMessage("label.authfailedmsgforpasswordmaxage", null, LocaleContextHolder.getLocale()));
            	   request.getSession().setAttribute("AUTH_FAILED_PASSWORD_MAX_AGE",true);
            	   request.getSession().setAttribute("PASSWORD_EXPIRED_MESSAGE", "Your Password has expired! Please change your password to continue");
            	   request.getSession().setAttribute(GhixPlatformConstants.USER_NAME,user.getEmail());
            	   return "redirect:/account/user/loginfailed";
               }
        }


		String landingPage = "login";
		String moduleName = (String) request.getSession().getAttribute("module");
		boolean mfa_enabled = false;

		//Added by Darshan Hardas for HIX-20857 :: Session param is handled to check if the logged in user is authorized
		//and has completed the security questions

		try
		{
			if(authToken == null) {
				authToken = (AbstractAuthenticationToken)request.getSession().getAttribute("authToken");
				SecurityContextHolder.getContext().setAuthentication( authToken);
			}
			if (authToken instanceof AbstractAuthenticationToken)  {
				UserDetails userDetails = (UserDetails)authToken.getPrincipal();
				if (userDetails instanceof AccountUser) {

					AccountUser user = (AccountUser) userDetails;
					currentRole = this.userService.getDefaultRole(user);
					request.getSession().setAttribute("userActiveRoleName", currentRole.getName());
					user.setCurrentUserRole(currentRole.getName());
		            if(currentRole.getLoginRestricted() ==1){
	   					if(!this.ipRestrictor.checkIpAccess()){
	   						return "redirect:/account/user/logout";
	   					}
	   				}
					LOGGER.info("Current LoggedIn User ::"+user.getId());
					Role currUserDefRole = userService.getDefaultRole(user);
					userId = user.getId();
					if(null != currUserDefRole) {
						isPrivileged = currUserDefRole.getPrivileged() ==1;
					}
					// set retry count to 0 on successful login if it's greater than zero
					if (user.getRetryCount() > 0){
						userService.updateRetryCount(user, 0);
					}
					/*Set Security Questions retry count to 0
					on successful login if it's greater than zero*/
					if (user.getSecQueRetryCount() > 0){
						userService.updateSecQueRetryCount(user, 0);
					}
					if(isSSOEnabled) {
						userSession = sessionTracker.fetchUserSession(userId);
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("USR SESSION: UID: "+userId);
						}
						httpSession = sessionTracker.replaceHttpSessionWithDBSession(request, userSession,
								httpSession, user);
						request.getSession().setAttribute("isUserFullyAuthorized", "true");
					}
					
				
					String exchangeCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
					//TODO : If other exchanges want to leverage the multiple household functionality , we need to remove the dependency on household email addresses.
				
				if(!exchangeCode.equalsIgnoreCase("MN".intern())) {
					try {
						household = householdRepository.findByEmailAddress(user.getEmail());
					}catch(Exception e) {
						LOGGER.error("Error fetching the household for user with id {}, Error message {}", user.getId(), e.getMessage());
					}
					if(!user.isHasGhixProvisioned() ) {
						
						if(household == null || household.getSsn() == null) {
							Role roleForNewUser = roleService.findRoleByName(user.getGhixProvisionRoleName().toUpperCase());
							if(roleForNewUser != null && roleForNewUser.getName().equalsIgnoreCase("Individual")) {
								request.getSession().setAttribute("user", user);
								request.getSession().setAttribute("ssnRequired", true);
								//Redirect to SSn info page
								return "account/user/ssnDetails";
								//return "redirect:/account/user/ssnDetails";
							}	
						}
					}
					
					user = provisionUserIfRequired(user);
					
					userId = user.getId();
					
					if(!user.isHasGhixProvisioned() && household != null && household.getUser() == null) {
						household.setUser(user);
						householdRepository.save(household);
					}
				}
					String activeModuleName = null;
					
					Role role = userService.getDefaultRole(user);
					activeModuleName = role.getName().toLowerCase();
					String activeUserLabel = role.getLabel();
					
					Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);//HIX-30004	//HIX-27183 : Showing roles from UserRole
					request.getSession().setAttribute("userDefRoleName", activeModuleName);

					String userDefLandingPage = null;
					
					if(user.getDefRole() != null){
						userDefLandingPage = user.getDefRole().getLandingPage(); //HIX-19109 :-- used to link masthed Logo
					}
					
					request.getSession().setAttribute("userLandingPage", userDefLandingPage);
					int activeModuleId=0;
					//HIX-28851 Application error is displayed on switching from Agent to Employer

					//ModuleUser userDefModule = userService.getUserSelfSignedModule(activeModuleName ,user.getId() );
					ModuleUser userDefModule =null;
					// As we may have multiple moduleUser entries for the user
					List<ModuleUser> userDefModules = userService.getUserSelfSignedModules(activeModuleName ,user.getId());
					if(userDefModules !=null && userDefModules.size()>0 ){
						userDefModule=userDefModules.get(0); //To deprecated
						
						activeModuleId=userDefModules.get(0).getModuleId();
						activeModuleName=userDefModules.get(0).getModuleName();
					}

					user.setActiveModuleId(activeModuleId);
					user.setActiveModuleName(activeModuleName);
					
					
					// Set the Linked active moduleId's to the user.
					List<Integer> linkedModuleIds = 
							userDefModules.stream()
						              .map(ModuleUser::getModuleId)
						              .collect(Collectors.toList());
					user.setLinkedModuleIds(linkedModuleIds);
					

					request.getSession().setAttribute("userActiveRoleName", activeModuleName);
					request.getSession().setAttribute("userConfiguredRoles", userConfiguredRole);
					request.getSession().setAttribute("userActiveRoleLabel", activeUserLabel);
					
					if(LOGGER.isDebugEnabled()){
						LOGGER.debug("User ::"+user.getId()+" with default Role ::"+activeModuleName.toUpperCase()+" exists in GHIX");
						LOGGER.debug("myAccountUrl ::"+ GhixConstants.MY_ACCOUNT_URL);
						LOGGER.debug("myInboxUrl ::"+ GhixConstants.MY_INBOX_URL);
						LOGGER.debug("loginUrl ::"+ GhixConstants.LOGIN_PAGE_URL);
					}
					Set<UserRole> userRoleList = user.getUserRole();
					request.getSession().setAttribute("userRoleList", userRoleList);
					request.getSession().setAttribute("USER_NAME".intern(), user.getUserName());
					request.getSession().setAttribute("USER_FIRST_NAME".intern(), user.getFirstName());
					request.getSession().setAttribute("USER_LAST_NAME".intern(), user.getLastName());
					//The following block is to check for two factor authentication
					String duoSecurityurl = "/account/user/duosecurity";
					String duoSigResponse = request.getParameter("sig_response");
					
					//Removed the bypass feature as per HIX-104035
					//request.getSession().setAttribute("MFA_CHECK", "BY-PASSED");
					ArrayList<GiAuditParameter> mfaAuditParams = new ArrayList<GiAuditParameter>();
					mfa_enabled = isTwoFactorAuthenticationRequired(user,mfaAuditParams);
					if( mfa_enabled ) {
						//request.getSession().setAttribute("MFA_CHECK", "CHECKED");
						GiAuditParameterUtil.add(mfaAuditParams);
						if(duoSigResponse == null ) {
							request.getSession().setAttribute("authToken", authToken);
							GiAuditParameterUtil.add( new GiAuditParameter("MFA Auth FIRED", user.getId()) );
							return "redirect:" + duoSecurityurl;
						}
						Map<String, String> keyMap = getkeysForTwofactorAuthentication();
						String ikey = keyMap.get("ikey");
						String skey = keyMap.get("skey");
						String akey = keyMap.get("akey");
						String authenticated_username = DuoWeb.verifyResponse(ikey, skey, akey, duoSigResponse);
						if (authenticated_username == null) {
							LOGGER.info("Two Factor Authentication failed for user:: " + user.getId());
							request.getSession().setAttribute("isUserFullyAuthorized", "false");
							GiAuditParameterUtil.add( new GiAuditParameter("MFA Auth FAILED", user.getId()) );
							return "redirect:login";
						}else {
							request.getSession().setAttribute("mfa_completed",true);
						}
						GiAuditParameterUtil.add( new GiAuditParameter("MFA Auth SUCCESS", user.getId()) );
					}
					else{
						GiAuditParameterUtil.add(mfaAuditParams);
					}
					//The following block is to check for two factor authentication


					 /* HIX-56933 - Account Activation Flow - Starts */
					 /**
					  * Setting loginWithAccountActivationFlow in session,
					  * gives @IndividualPortalController.individualPostRegistration method a chance
					  * to link Account User to Household linked with Activation Link
					  *
					  *
					  * If loginWithAccountActivationFlow found in session
					  * And Logged-In User already has module user entry for role type individual (activeModuleId != 0)
					  * Then, reset accountActivationFlow from session
					  *
					  * This is done to avoid linking more than one household with the logged-in user.
					  *
					  */
					 String loginWithAccountActivationFlow = (String) request.getSession().getAttribute("loginWithAccountActivationFlow");
					 if ("individual".equals(activeModuleName) && activeModuleId != 0 && "true".equals(loginWithAccountActivationFlow)){
						 request.getSession().removeAttribute("accountActivationFlow");
						 request.getSession().removeAttribute("activationId");
					 }
					 /* HIX-56933 - Account Activation Flow - Ends */

					/* Account Activation Code - Starts */
					/**
					 * Account activation flow.
					 * get nextActivationLandingPage and redirect to that.
					 * If not found then go with normal flow and give a chance to existing login and registration flow to validate and
					 * 		finally land to the corresponding ROLE's landingPage
					 */

					// check if the user came from email activation flow and ensure all the three steps are completed
					int activationId = checkEmailActivationFlow(user.getUserName());
					if(activationId > 0){
						if(LOGGER.isDebugEnabled()){
							LOGGER.debug("Processing activation record ["+activationId+"] for login");
						}
						request.getSession().setAttribute("accountActivationFlow", "true");
						request.getSession().setAttribute("activationId", Integer.toString(activationId));
					}
					String flag = (String) request.getSession().getAttribute("accountActivationFlow");
					if (!StringUtils.isEmpty(flag) && StringUtils.equals(flag, "true")){
						String postRegistrationUrl = userService.getDefaultRole(user).getPostRegistrationUrl();
						userService.enableRoleMenu(request,userService.getDefaultRole(user).getName());
						postRegistrationUrl = StringUtils.isEmpty(postRegistrationUrl) ? "/account/user/login" : postRegistrationUrl;
						Map<String, String> mapEventParam = new HashMap<>();
		    			
		    			//event type and category
		    			LookupValue lookupValue = lookupService.getlookupValueByTypeANDLookupValueCode("APPEVENT_ACCOUNT","ACCOUNT_CREATED");
//		    
		    			EventInfoDto eventInfoDto = new EventInfoDto();
		    			eventInfoDto.setEventLookupValue(lookupValue);
		    			if(userDefModule !=null){
			    			eventInfoDto.setModuleId(userDefModule.getModuleId());
			    			eventInfoDto.setModuleName(userDefModule.getModuleName());
			    			eventInfoDto.setModuleUserName(user.getUsername());
			    			appEventService.record(eventInfoDto, mapEventParam);
		    			}
						if(null != userSession && userSession.getStatus().equalsIgnoreCase(UserSessionStatus.INIT.toString())) {
							if(null != httpSession) {	
								sessionTracker.markUserSessionInitToInProgress(httpSession.getId(),userId);
							}
						} else if(null == userSession){
							sessionTracker.createUserSession(request.getSession(), request.getRemoteAddr(),userId,isPrivileged );
						}
						userService.updateLastLogin(user);
						return "redirect:"+postRegistrationUrl;
					}
					//HIX-45304 begin
					checkLoggingEvent();
					//HIX-45304 ends
					// update last login timestamp
					userService.updateLastLogin(user);
					
					/*if(LOGGER.isInfoEnabled()) {	
						LOGGER.info("IS EMAIL CHANGED: "+user.isEmailChanged());
					}*/
					activeModuleName= user.getActiveModuleName();
					// For MN we can't override the household's email which is set from AT's 
					if(activeModuleName != null && activeModuleName.equalsIgnoreCase("individual")&& !exchangeCode.equalsIgnoreCase("MN".intern())){
						ModuleUser moduleUser =  userService.getUserDefModule(activeModuleName,user.getId());
						if(moduleUser != null){
							household = householdRepository.findById(moduleUser.getModuleId());
							if(null != household ){
								household.setUser(user);
								String email = household.getEmail(); 
								if(email == null || !email.equalsIgnoreCase(user.getEmail())){
									household.setEmail(user.getEmail().toLowerCase());
									householdRepository.save(household);
								}
								if((household.getSsn() == null || household.getSsn().isEmpty()) &&
													(household.getSsnNotProvidedReason() == null || household.getSsnNotProvidedReason().isEmpty())) {
									landingPage = getLandingPage(user,moduleName,request);
									request.getSession().setAttribute("landingPageUrl", landingPage);
									request.getSession().setAttribute("userId", user.getId());
									request.getSession().setAttribute("ssnRequired", true);
									//Redirect to SSn info page
									//return "redirect:/account/user/ssnDetails";
									return "account/user/ssnDetails";
								}
								
							}
						}
					}
					request.getSession().setAttribute("sUser", user);
					landingPage = getLandingPage(user,moduleName,request);
					if(landingPage != null) {
						landingPage = URLDecoder.decode(landingPage,ENCODING_UTF_8);
					}
					
					/**
					 * HIX-47997
					 *
					 * When a user signs up on Idalink and comes to YHI for the
					 * first time we complete post registration for the user
					 * before completing login success flow
					 *
					 */
					if (isSSOEnabled
							&& StringUtils.equalsIgnoreCase(
									user.getActiveModuleName(), "INDIVIDUAL")
							&& user.getActiveModuleId() == 0) {
						LOGGER.debug("SSO is enabled: Post registration of user is not complete");
						landingPage = "/indportal/postreg";
					}

					LOGGER.info("User is redirecting after rebuilding to ::"+landingPage);

					
					if(exchangeCode.equalsIgnoreCase("MN".intern()))
					{
						if(StringUtils.equalsIgnoreCase(
								user.getActiveModuleName(), "EXTERNAL_ASSISTER"))
						{
							request.getSession().setAttribute("userActiveRoleName", "INDIVIDUAL".intern());
							request.getSession().setAttribute("switchToModuleName", "INDIVIDUAL".intern());
							consumerUtil.handleExternalAssister(user,user.getLoginConext());
							
						}
						// If there are multiple active moduleId's linked for user then let the user choose on the portal page,
						// User's Active Module ID will be set on the portal.
						if(user.getLinkedModuleIds()!=null && user.getLinkedModuleIds().size()>1)
						{
							user.setActiveModuleId(0);
						}
					}
				}
			}
		forkNewSession(request);
		} catch(Exception ex){
			
			if(mfa_enabled && request.getSession().getAttribute("mfa_completed") == null) {
				request.getSession().invalidate();
			}
			request.getSession().setAttribute("isUserFullyAuthorized", "false");
			LOGGER.error("Exception (/account/user/loginSuccess) :: " +ex.getMessage(),ex);
			if(StringUtils.isBlank(exceptionMessage)){
				exceptionMessage=ex.getMessage();
			}
			throw new Exception(exceptionMessage);

		}finally {
			request.getSession().removeAttribute("mfa_completed");
		}

		LOGGER.info("Creating sesion for user with userId:"+userId);

		if(null != userSession && userSession.getStatus().equalsIgnoreCase(UserSessionStatus.INIT.toString())) {
			if(null != httpSession) {	
				sessionTracker.markUserSessionInitToInProgress(httpSession.getId(),userId);
			}
		} else if(null == userSession){
			sessionTracker.createUserSession(request.getSession(), request.getRemoteAddr(),userId,isPrivileged );
		}
		return "redirect:"+landingPage;
	}

	

	/**
	 * This method is called to fork the existing HttpSession on successful
	 * login
	 *
	 * @param request
	 */
	private void forkNewSession(HttpServletRequest request) {
		HttpSession oldSession = null;
		HttpSession newSession = null;
		Map<String, Object> sessionValues = null;
		Enumeration<String> sessionNameEnum = null;

		try {
			oldSession = request.getSession(false);
			if (null != oldSession) {
				sessionNameEnum = oldSession.getAttributeNames();
				if (null != sessionNameEnum) {
					sessionValues = new ConcurrentHashMap<String, Object>();
					while (sessionNameEnum.hasMoreElements()) {
						String attrName = sessionNameEnum.nextElement();
						Object attrVal = oldSession.getAttribute(attrName);
						sessionValues.put(attrName, attrVal);
					}
				}
				oldSession.invalidate();
				newSession = request.getSession(true);
				if(sessionValues != null && sessionValues.size() > 0){
					for (Map.Entry<String, Object> entry : sessionValues.entrySet()) {
						newSession.setAttribute(entry.getKey(), entry.getValue());
					}
				}
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE FORKING SESSION: ", ex);
			throw ex;
		}
	}
	
	/**
	 * Method to add csr to work group
	 * 
	 * @param accountUser
	 */
	@RequestMapping(value="/account/user/addtowrkgrp",method=RequestMethod.GET)
	public String addCsrToWorkGroup(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws InvalidUserException{
		String landingUrl = null;
		
		AccountUser accountUser = null;
		Role defaultRole = null;
		TkmQueueUsersRequest tkmQueueUsersRequest = null;
		
		
		try {
			accountUser = userService.getLoggedInUser();
			defaultRole = accountUser.getDefRole();
			
			landingUrl = defaultRole.getLandingPage();
			
			if(LOGGER.isDebugEnabled()) {
				LOGGER.debug("ADD USR TO WRK GRP: LANDING URL: "+landingUrl);
			}
			
			tkmQueueUsersRequest = new TkmQueueUsersRequest();
			tkmQueueUsersRequest.setUserId(accountUser.getId());
			
			final String response = ghixRestTemplate.postForObject(GhixEndPoints.TicketMgmtEndPoints.ADD_QUEUES_TO_CSR_USER, tkmQueueUsersRequest, String.class);
					
			if (null == response || response.length() == 0) {
				LOGGER.warn("UNABLE TO ADD USR TO WRKGRP");
			} else {
				Gson gson = new Gson();
				final TkmQueueUsersResponse tkmQueueUsersResponse = gson.fromJson(response, TkmQueueUsersResponse.class);
				
				if(null != tkmQueueUsersResponse && GhixConstants.RESPONSE_SUCCESS.equalsIgnoreCase(tkmQueueUsersResponse.getStatus())) {
					if(LOGGER.isInfoEnabled()) {	
						LOGGER.info("SUCCESSFULLY ADDED USR TO WRKGRP");
					}
				}
			}
		} catch(Exception ex) {
			LOGGER.error("ERR: WHILE ADDING USR TO WRKGRP: ",ex);
			throw new GIRuntimeException(
					"PLATFORM-00003", null,
					GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION,
					Severity.HIGH);
		}
		
		return "redirect:"+landingUrl;
	}

	private void checkLoggingEvent() {
		Role defaultRole = null;
		AccountUser user = null;
		try {
			user = userService.getLoggedInUser();
			if(user != null){
				defaultRole = userService.getDefaultRole(user);
			}

		int userId = 0;
		if (null != defaultRole
				&& (defaultRole.getName().equalsIgnoreCase("consumer")
						|| defaultRole.getName().equalsIgnoreCase("EMPLOYEE")
						|| defaultRole.getName().equalsIgnoreCase("INDIVIDUAL") || defaultRole
						.getName().equalsIgnoreCase("BROKER"))) {
			userId = getModuleUserForRole(user, defaultRole.getName()) ;
			if (userId > 0) {
				ConsumerEventHistory consumerloginEventRequest = new ConsumerEventHistory(
						new TSDate());
				consumerloginEventRequest.setEventSource("User-Management");
				consumerloginEventRequest.setEventLevel("Logout");
				consumerloginEventRequest.setHouseholdId(userId);
				consumerloginEventRequest.setDatetime(new TSDate());
				consumerEventsUtil
						.postConsumerLoginEvent(consumerloginEventRequest);
			}

			else{
				LOGGER.warn("Error while setting the loggin event accountUser for the logged in "+defaultRole.getName() +" can not find the user id");
			}
		}
		} catch (Exception e) {
			LOGGER.error("Exception while setting the loggin event accountUser for the logged in user "+ e.getMessage());
		}

	}

	private int getModuleUserForRole(AccountUser user, String roleName) {
		int  userId = 0;
		List<ModuleUser> moduleUsers= null;
		String role = roleName;

		switch (role) {
		case "consumer":
			Household household = consumerUtil
			.getHouseholdRecordByUserId(user.getId());
			if(household!=null){
				userId =  household.getId();
			}
			break;
		case "EMPLOYEE":
			moduleUsers = userService.getModuleUsers(
					user.getId(),ModuleUserService.EMPLOYEE_MODULE);
			if(moduleUsers!=null && moduleUsers.size()>0){
				userId = moduleUsers.get(0).getId();
			}
			break;

		case "INDIVIDUAL":
			moduleUsers = userService.getModuleUsers(
					user.getId(),ModuleUserService.INDIVIDUAL_MODULE);
			if(moduleUsers!=null && moduleUsers.size()>0){
				userId =  moduleUsers.get(0).getId();
			}
			break;
		case "BROKER":
			moduleUsers = userService.getModuleUsers(
					user.getId(),ModuleUserService.BROKER_MODULE);
			if(moduleUsers!=null && moduleUsers.size()>0){
				userId =  moduleUsers.get(0).getId();
			}
			break;

		default:
			break;
		}
		return userId;
	}

	/**
	 * Switches the user role and redirects to targeted role controller
	 *  Update by biswakalyan: Don't update the session with active modulename and active label, This method is for internal switch.
	 * @author venkata_tadepalli
	 * @since 11/28/2012
	 */
	@RequestMapping(value="/account/user/switchUserRole",  method = {RequestMethod.GET, RequestMethod.POST })
	//public String switchUserRole(Model model, HttpServletRequest request,@ModelAttribute("switchToModuleName") String switchToModuleName, @ModelAttribute("switchToModuleId") String switchToModuleId) throws InvalidUserException,GIException {
	public String switchUserRole(Model model, HttpServletRequest request, RedirectAttributes redirectAttrs)
			throws InvalidUserException, GIException {
		LOGGER.debug("Switching Role For the User");// RedirectAttributes
													// redirectAttrs

		String landingPage = null;
		HttpSession session = request.getSession();
		// This flag is to display popup while account switching
		String showPopupInFuture = request.getParameter("showPopupInFuture");
		String switchToModuleId = null;
		String switchToResourceName = null;
		String switchToNonDefaultURL = null;

		AccountUser user = userService.getLoggedInUser();
		Role defRole = userService.getDefaultRole(user);
		if(defRole.getName().equalsIgnoreCase("individual")){
			LOGGER.error("Permission denied, Individual ["+user.getUserName()+"] users can not switch the roles");
			throw new GIRuntimeException("Permission denied.. check logs");
		}
		landingPage = defRole.getLandingPage();
		String assigned = request.getParameter("assigned");
		//String switchToModuleName = request.getParameter("switchToModuleName");
		String switchToModuleName = null;
		
		String reference = request.getParameter("ref");
		if (reference != null) {
			Map<String, String> moduleParams = GhixAESCipherPool.decryptParameterMap(reference);
			switchToModuleId = moduleParams.get("switchToModuleId");
			switchToResourceName = moduleParams.get("switchToResourceName");
			switchToNonDefaultURL = moduleParams.get("switchToNonDefaultURL");
			switchToModuleName = moduleParams.get("switchToModuleName");
		} else {
			LOGGER.error("Un secure URL:" + request.getRequestURI());
			switchToModuleId = request.getParameter("switchToModuleId");
			switchToResourceName = request.getParameter("switchToResourceName");
			switchToNonDefaultURL = request.getParameter("switchToNonDefaultURL");
			switchToModuleName = request.getParameter("switchToModuleName");
		}
		
		if (assigned != null) {
			LOGGER.debug("Switching role to " + switchToModuleName);
			request.getSession().setAttribute("userActiveRoleName", switchToModuleName);
		}
		
		if (null != switchToResourceName && !switchToResourceName.trim().isEmpty()) {
			switchToResourceName = switchToResourceName.trim();
			if ("broker".equalsIgnoreCase(switchToModuleName)) {
				try {
					switchToResourceName = URLDecoder.decode(switchToResourceName, ENCODING_UTF_8);
				} catch (UnsupportedEncodingException e) {
					LOGGER.error("Exception occurred while decoding switched to resource's name.", e);
				}
			}
		}
		user.setActiveModuleName(switchToModuleName);
		if (!StringUtils.isEmpty(switchToNonDefaultURL)) {
			if (!switchToNonDefaultURL.startsWith("/")) {
				LOGGER.info("PHISHING ATTACK :: INVALID URL in switchToNonDefaultURL: ");
				throw new GIException("PHISHING ATTACK :: INVALID URL in switchToNonDefaultURL: ");
			}
			landingPage = switchToNonDefaultURL;
		} else {
			landingPage = getLandingPage(user, switchToModuleName, request);
		}

		session.setAttribute("userLandingPage", landingPage);

		// When the broker has switched to employer view
		// the following session "isBrokerSwitchToEmplView" attribute will
		// be used in the menu
		session.setAttribute("isUserSwitchToOtherView", "Y");
		session.setAttribute(BrokerConstants.IS_BROKER_SWITCH_TO_EMPL_VIEW, BrokerConstants.NO);
		session.setAttribute(BrokerConstants.IS_BROKER_SWITCH_TO_INDV_VIEW, BrokerConstants.NO);
		session.setAttribute("adminReferralUrl", request.getHeader("Referer"));

		Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);// HIX-30004
		switchToModuleId = StringUtils.isNumeric(switchToModuleId) ? switchToModuleId
				: ghixJasyptEncrytorUtil.decryptStringByJasypt(switchToModuleId);
		int activeModuleId = 0;
		try {
			activeModuleId = Integer.parseInt(switchToModuleId);
		}catch(NumberFormatException ex){
			LOGGER.info("PHISHING ATTACK :: INVALID moduleid passed to switchUserRole: "+switchToModuleId);
			throw new GIException("PHISHING ATTACK :: INVALID moduleid in switchUserRole: "+switchToModuleId);
		}
		ModuleUser switchToModule = userService.findModuleUser(activeModuleId, switchToModuleName, user);

		user.setActiveModuleId(activeModuleId);
		user.setActiveModule(switchToModule);

		session.setAttribute("switchToModuleId", switchToModuleId);
		session.setAttribute("switchToResourceName", switchToResourceName);
		session.setAttribute("showPopupInFuture", showPopupInFuture);
		session.setAttribute("switchToModuleName", switchToModuleName);
		session.setAttribute("userActiveRoleName", switchToModuleName);

		session.setAttribute("userConfiguredRoles", userConfiguredRole);// HIX-30004
		user.setPersonatedRoleName(switchToModuleName);
		LOGGER.info("User (ID :: " + user.getId() + ") switching to role ::" + switchToModuleName);

		return "redirect:" + landingPage;
	}

	/**
	 * Switches the user role and redirects to targeted role controller
	 * @author venkata_tadepalli
	 * @throws InvalidUserException
	 * @since 11/28/2012
	 */
	@RequestMapping(value="/account/user/switchDefRole",  method = {RequestMethod.GET, RequestMethod.POST })
	public String switchDefRole(Model model, HttpServletRequest request) throws InvalidUserException,GIException {
		LOGGER.debug("User Role Switch to Default Role:: ");
		String landingPage = "";
		HttpSession session = request.getSession();

		AccountUser user = userService.getLoggedInUser();
		Role userDefaultRole = userService.getDefaultRole(user);
		String switchToModuleName = userDefaultRole.getName().toLowerCase();


		landingPage = getLandingPage(user,null,request);


		//HIX-30004
		String switchToModuleLabel = userDefaultRole.getLabel();
		Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);
		//HIX-30004
		int activeModuleId = 0;
		//ModuleUser switchToModule = userService.findModuleUser(activeModuleId, switchToModuleName, user);

		//HIX-28851 Application error is displayed on switching from Agent to Employer
		ModuleUser userDefModule = userService.getUserSelfSignedModule(switchToModuleName ,user.getId() );

		if(userDefModule !=null){
			activeModuleId=userDefModule.getModuleId();
			switchToModuleName=userDefModule.getModuleName();
		}



		user.setActiveModuleId(activeModuleId);
		user.setActiveModuleName(switchToModuleName);
		user.setActiveModule(userDefModule);

		session.setAttribute("userLandingPage", landingPage); //HIX-19109 :-- used to link masthed Logo
		session.setAttribute("switchToModuleId", activeModuleId);
		session.setAttribute("isUserSwitchToOtherView", "N");
		session.setAttribute("switchToResourceName", "");
		session.setAttribute("designatedBrokerProfile", null);//This will be in if condition once switch from employer to employee feature will be added.
		session.setAttribute("userActiveRoleName", switchToModuleName);
		//HIX-30004
		session.setAttribute("userConfiguredRoles", userConfiguredRole);
		session.setAttribute("userActiveRoleLabel", switchToModuleLabel);
		//HIX-30004
		LOGGER.info("User (ID :: "+ user.getId() +") switching to default role ::"+switchToModuleName);

		return "redirect:"+landingPage;
	}

	/**
	 * Switches the user role and redirects to targeted role controller
	 * @author venkata_tadepalli
	 * @throws InvalidUserException
	 * @since 11/28/2012
	 */
	@RequestMapping(value="/account/user/switchNonDefRole/{nonDefRoleName}",  method = {RequestMethod.GET, RequestMethod.POST })
	public String switchNonDefRole(Model model, HttpServletRequest request,@PathVariable("nonDefRoleName") String nonDefRoleName) throws InvalidUserException,GIException {
		LOGGER.debug("User Role Switch Role:: ");
		String landingPage = "";
		HttpSession session = request.getSession();

		AccountUser user = userService.getLoggedInUser();
		//Role defRole = userService.getDefaultRole(user);
		
		//commenting out this check as the same controller action is used to redirect to landing page from header.
		//also if individual comes via this action he can only land to individual not to any other role.
		//fix to HIX-93592
		
		/*if(defRole.getName().equalsIgnoreCase("individual")){
			LOGGER.error("Permission denied, Individual ["+user.getUserName()+"] users can not switch the roles");
			throw new GIRuntimeException("Permission denied.. check logs");
		}*/
		Role tmpRole = null;
		String switchToModuleName = null,switchToModuleLabel = null;
		if (null!=nonDefRoleName) {
			UserRole userRole = userService.getUserRoleByRoleName(user,nonDefRoleName);
			if(userRole != null){
				tmpRole = userRole.getRole();
				switchToModuleName = tmpRole.getName().toLowerCase();
				switchToModuleLabel = tmpRole.getLabel();
			}else{
				switchToModuleName = user.getActiveModuleName().toLowerCase();
				userRole = userService.getUserRoleByRoleName(user,switchToModuleName);
				if(LOGGER.isDebugEnabled()) {
					LOGGER.debug("Looking up user role by active module name {}, userrole {}".intern(),switchToModuleName, userRole);
				}
				if(userRole != null) {
					tmpRole = userRole.getRole();
					if(tmpRole != null) {
						switchToModuleLabel = userRole.getRole().getLabel();
					}
				}else {
					Role defaultUserRole = userService.getDefaultRole(user);
				
					if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(defaultUserRole.getName()) != 0){
						userRole = userService.getUserRoleByRoleName(user,user.getSwitchedRoleName());
						switchToModuleName = userRole.getRole().getName().toLowerCase();
						switchToModuleLabel = userRole.getRole().getLabel();
						tmpRole = userRole.getRole();
					}else{
						switchToModuleName = defaultUserRole.getName().toLowerCase();
						switchToModuleLabel = defaultUserRole.getLabel();
						tmpRole = defaultUserRole;
					}
				}
			}
		} else {
			Role defaultUserRole = userService.getDefaultRole(user);
			if( user.getSwitchedRoleName() != null && user.getSwitchedRoleName().compareToIgnoreCase(defaultUserRole.getName()) != 0){
				UserRole userRole = userService.getUserRoleByRoleName(user,user.getSwitchedRoleName());
				switchToModuleName = userRole.getRole().getName().toLowerCase();
				switchToModuleLabel = userRole.getRole().getLabel();
				tmpRole = userRole.getRole();
			}else{
				switchToModuleName = defaultUserRole.getName().toLowerCase();
				switchToModuleLabel = defaultUserRole.getLabel();
				tmpRole = defaultUserRole;
			}
		}

		landingPage = getLandingPage(user,switchToModuleName,request);
		user.setPersonatedRoleName(null);
		
		Object adminReferralUrl = session.getAttribute("adminReferralUrl");
		if(adminReferralUrl instanceof String){
			landingPage = (String)adminReferralUrl;
			session.removeAttribute("adminReferralUrl");
		}
		
		Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);//HIX-30004
		int activeModuleId = 0;
		//ModuleUser switchToModule = userService.findModuleUser(activeModuleId, switchToModuleName, user);
		//HIX-28851 Application error is displayed on switching from Agent to Employer
		ModuleUser userDefModule = userService.getUserSelfSignedModule(switchToModuleName ,user.getId() );

		if(userDefModule !=null){
			activeModuleId=userDefModule.getModuleId();
			switchToModuleName=userDefModule.getModuleName();
		}

		user.setActiveModuleId(activeModuleId);
		user.setActiveModuleName(switchToModuleName);
		user.setActiveModule(userDefModule);

		session.setAttribute("userLandingPage", landingPage); //HIX-19109 :-- used to link masthed Logo
		session.setAttribute("switchToModuleId", activeModuleId);
		session.setAttribute("isUserSwitchToOtherView", "N");
		session.setAttribute("switchToResourceName", "");
		//session.setAttribute("designatedBrokerProfile", null);//This will be in if condition once switch from employer to employee feature will be added.
		if(!"assister".equalsIgnoreCase(switchToModuleName)){
			session.setAttribute("designatedAssisterProfile", null);
		}
		session.setAttribute("userActiveRoleName", switchToModuleName);
		//HIX-30004
		session.setAttribute("userConfiguredRoles", userConfiguredRole);
		session.setAttribute("userActiveRoleLabel", switchToModuleLabel);
		//HIX-30004
		//request.getSession().removeAttribute(LAST_VISITED_URL);//HIX-29905
		LOGGER.info("User (ID :: "+ user.getId() +") switching role ::"+switchToModuleName);

		return "redirect:"+landingPage;
	}

	/**
	 * Switches the user role and redirects to targeted role controller
	 * @author Biswakalyan
	 * @throws InvalidUserException,GIException
	 * @since 01/22/2014
	 */
	@RequestMapping(value="/account/user/switchUserRole/{userRoleName}",  method = {RequestMethod.GET, RequestMethod.POST })
	public String switchUserRole(Model model, HttpServletRequest request,@PathVariable("userRoleName") String userRoleName) throws InvalidUserException,GIException {
		LOGGER.debug("User Role Switch Role:: ");
		String landingPage = "";
		HttpSession session = request.getSession();

		AccountUser user = userService.getLoggedInUser();
		Role defRole = userService.getDefaultRole(user);
		if(defRole.getName().equalsIgnoreCase("individual")){
			LOGGER.error("Permission denied, Individual ["+user.getUserName()+"] users can not switch the roles");
			throw new GIRuntimeException("Permission denied.. check logs");
		}
		String switchToModuleName = null,activeUserLabel = null;
		if(StringUtils.isNotBlank(userRoleName)){
			userRoleName =ghixJasyptEncrytorUtil.decryptStringByJasypt(userRoleName);
		}
		// will check for path varriable. if not found directly navigate to default role.
		if (null!=userRoleName) {
			Role switchRole = userService.getUserRoleByRoleName(user,userRoleName).getRole();
			if(null != switchRole){
				if (switchRole.getLoginRestricted() == 1) {
					if(LOGGER.isDebugEnabled()) {
						LOGGER.debug("THE USER WITH NAME " + user.getFullName()
								+ " HIS SWITCHED ROLE IS: " + switchRole);
					}
					
					boolean hasAccess = ipRestrictor.hasRoleAccess(
							request, request);
					if (!hasAccess) {
						return "redirect:/account/user/logout";
					}
				}
				user.setSwitchedRoleName(switchRole.getName());
				
				switchToModuleName = switchRole.getName().toLowerCase();
				activeUserLabel = switchRole.getLabel();
			}
		}
		if(null == switchToModuleName) {
			switchToModuleName = userService.getDefaultRole(user).getName().toLowerCase();
			activeUserLabel = userService.getDefaultRole(user).getLabel();
		}

		landingPage = getLandingPage(user,switchToModuleName,request);

		Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);//HIX-30004
		int activeModuleId = 0;
		//HIX-28851 Application error is displayed on switching from Agent to Employer
		ModuleUser userDefModule = userService.getUserSelfSignedModule(switchToModuleName ,user.getId());

		if(userDefModule !=null){
			activeModuleId=userDefModule.getModuleId();
			switchToModuleName=userDefModule.getModuleName();
		}

		userService.getLoggedInUser().setActiveModuleId(activeModuleId);
		userService.getLoggedInUser().setActiveModuleName(switchToModuleName);
		userService.getLoggedInUser().setActiveModule(userDefModule);

		session.setAttribute("userLandingPage", landingPage); //HIX-19109 :-- used to link masthed Logo
		session.setAttribute("switchToModuleId", activeModuleId);
		session.setAttribute("isUserSwitchToOtherView", "N");
		session.setAttribute("switchToResourceName", "");
		session.setAttribute("designatedBrokerProfile", null);//This will be in if condition once switch from employer to employee feature will be added.
		session.setAttribute("userActiveRoleName", switchToModuleName);
		session.setAttribute("userActiveRoleLabel", activeUserLabel);
		session.setAttribute("userConfiguredRoles", userConfiguredRole);//HIX-30004
		session.removeAttribute("unreadRecords");
		LOGGER.info("User (ID :: "+ user.getId() +") switching role ::"+switchToModuleName);

		return "redirect:"+landingPage;
	}


	/*@RequestMapping(value="/account/ssoerrorpage")
	public String ssoErrorPage(Model model, AbstractAuthenticationToken authToken,  HttpServletRequest request){
		LOGGER.debug("SSO Error page");
		return "account/ssoerrorpage";
	}*/

	@RequestMapping(value="/account/user/loginfailed",method = RequestMethod.GET)
	@GiAudit(transactionName = "User login failed",
	 eventType = EventTypeEnum.AUTHENTICATION,
	 eventName = EventNameEnum.USER_FAILED_TO_LOGIN)
	public String loginfailedPage(Model model, AbstractAuthenticationToken authToken,  HttpServletRequest request) {
		// get user name from session
		String userName = request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) + "";

		// check if account is inactive or inactive dormant status
		AccountUser userDetails = null;
		boolean isAccInactiveDormant = false;
		boolean isAccInactive = false;
		boolean isAccLocked = false;

		if (!userName.equals(""))
		{
			Boolean passwordInHistory = (Boolean) request.getSession().getAttribute("IS_PASSWORD_PRESENT_IN_HISTORY");
			
			Boolean passwordAuthExpired = (Boolean) request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE");
			// check if account is locked
			
			if((passwordInHistory == null || !passwordInHistory) && ( passwordAuthExpired == null || !passwordAuthExpired)){
				isAccLocked = checkNoOfAttempts(userName);				
			}

			// get user details from db
			userDetails = repository.findByUserName(userName.toLowerCase());

			if (userDetails != null && userDetails.getConfirmed() == 0){

				//String t = userDetails.getStatus();

				if ("Inactive-dormant".equals(userDetails.getStatus())) {
					isAccInactiveDormant = true;
					LOGGER.info(" User Id '" + userDetails.getId() + "' status is Inactive-dormant.");
				} else if ("Inactive".equals(userDetails.getStatus())){
					isAccInactive = true;
					LOGGER.info(" User Id '" + userDetails.getId() + "' status is Inactive.");
				}
			}
		}
		userAccountAuditLog.accntLockWrongPswrd(request, isAccLocked, isAccInactiveDormant, isAccInactive);
		
		//remove user name from session
		//request.getSession().removeAttribute(GhixPlatformConstants.USER_NAME);
		model.addAttribute("isAccLocked", isAccLocked);
		model.addAttribute("isAccInactiveDormant", isAccInactiveDormant);
		model.addAttribute("isAccInactive", isAccInactive);
		model.addAttribute("authfailed", "true" );
		model.addAttribute("AUTH_FAILED_PASSWORD_MAX_AGE", request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE"));
		model.addAttribute("AUTH_FAILED_PASSWORD_USERNAME", request.getSession().getAttribute(GhixPlatformConstants.USER_NAME)!=null?request.getSession().getAttribute(GhixPlatformConstants.USER_NAME):"" + "");

		GiAuditParameterUtil.add(new GiAuditParameter("isAccLocked", isAccLocked),new GiAuditParameter("isAccInactiveDormant", isAccInactiveDormant),new GiAuditParameter("isAccInactive", isAccInactive),
		new GiAuditParameter("authfailed", "true" ),new GiAuditParameter("AUTH_FAILED_PASSWORD_MAX_AGE", request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE")),
		new GiAuditParameter("AUTH_FAILED_PASSWORD_USERNAME", request.getSession().getAttribute(GhixPlatformConstants.USER_NAME)!=null?request.getSession().getAttribute(GhixPlatformConstants.USER_NAME):"" + ""));
		
		return "account/user/login";
	}
	
	@RequestMapping(value="/account/user/accountLockedOnChangePassword",method = RequestMethod.POST)
	public String accountLockedOnChangePassword(Model model, HttpServletRequest request) {
	   LOGGER.info("User account locked during change of password.");
	   userAccountAuditLog.accntLockOnChangePswrd(request);
	   
	   request.getSession().invalidate();
  	   SecurityContextHolder.clearContext();
 	   return "account/user/useraccountlocked";
	}
	
	@RequestMapping(value="/account/user/ssoaccountlocked",method = RequestMethod.GET)
	public String accountLockedPage(Model model, AbstractAuthenticationToken authToken,  HttpServletRequest request) {
		// get user name from session
		String userName = request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) + "";

		// check if account is locked
		boolean isAccLocked = checkNoOfAttempts(userName);

		// check if account is inactive or inactive dormant status
		AccountUser userDetails = null;
		boolean isAccInactiveDormant = false;
		boolean isAccInactive = false;
		if (!userName.equals("")){
			// get user details from db
			userDetails = repository.findByUserName(userName.toLowerCase());

			if (userDetails != null && userDetails.getConfirmed() == 0){
				if ("Inactive-dormant".equals(userDetails.getStatus())) {
					isAccInactiveDormant = true;
					LOGGER.info(" User '" + userDetails.getId() + "' status is Inactive-dormant.");
				} else if ("Inactive".equals(userDetails.getStatus())){
					isAccInactive = true;
					LOGGER.info(" User '" + userDetails.getId() + "' status is Inactive.");
				}
			}
		}
		userAccountAuditLog.accntLockWrongPswrd(request, isAccLocked, isAccInactiveDormant, isAccInactive);
		//remove user name from session
		//request.getSession().removeAttribute(GhixPlatformConstants.USER_NAME);
		model.addAttribute("isAccLocked", isAccLocked);
		model.addAttribute("isAccInactiveDormant", isAccInactiveDormant);
		model.addAttribute("isAccInactive", isAccInactive);
		model.addAttribute("authfailed", "true" );
		model.addAttribute("AUTH_FAILED_PASSWORD_MAX_AGE", request.getSession().getAttribute("AUTH_FAILED_PASSWORD_MAX_AGE"));
		model.addAttribute("AUTH_FAILED_PASSWORD_USERNAME", request.getSession().getAttribute(GhixPlatformConstants.USER_NAME)!=null?request.getSession().getAttribute(GhixPlatformConstants.USER_NAME):"" + "");

		return "account/user/ssoaccountlocked";
	}


	/**
	 *
	 * method to check no. of unsuccessfully logon attempts based on user name and lock account if it reaches max retry count
	 *
	 * @author Samir Gundawar
	 * @since 01/23/2014
	 *
	 * @param userName
	 */
	private boolean checkNoOfAttempts(String userName)
	{
		AccountUser userDetails = null;
		boolean isAccLocked = false;
		if (userName != null && !userName.equals(""))
		{
			// get user details from db
			userDetails = repository.findByUserName(userName.toLowerCase());

			//if user is active/not locked then check for no. of retry
			if (userDetails != null && userDetails.getConfirmed() == 1) {
				Set<Role> userRoles = userService.getAllRolesOfUser(userDetails);

				if (userRoles != null)
				{
					// increase no. of retry by 1
					int noOfRetry = userDetails.getRetryCount() + 1;
					int maxRetry = 0;

					//if User has multiple roles then set lower limit as the cut-off.
					for (Role userRole : userRoles)
					{
						if (maxRetry == 0)
						{
							maxRetry = userRole.getMaxRetryCount();
						}
						else if (maxRetry > userRole.getMaxRetryCount())
						{
							maxRetry = userRole.getMaxRetryCount();
						}
					}

					// set user as locked/inactive if it reaches max no. of retry
					if (noOfRetry >= maxRetry)
					{
						userService.updateConfirmedAndRetryCount(userDetails, 0, noOfRetry);
						isAccLocked = true;
						//HIX-29631 Sends Email on Account Lock
						try {
							sendAccountLockMail(userDetails);
						} catch (NotificationTypeNotFound e) {
							LOGGER.error(e.getMessage());
							LOGGER.error("Account Lock e-mail cannot be sent - ", e);
						}
					}
					else
					{
						// update no. of retry for user
						userService.updateRetryCount(userDetails, noOfRetry);
					}
				}
				else
				{
					LOGGER.info("Default role is not set for the user");
				}
	        }
			else
			{
				LOGGER.info(" User details not found or it's already locked.");

				if (userDetails != null && userDetails.getConfirmed() == 0 &&
						!"Inactive".equals(userDetails.getStatus())  && !"Inactive-dormant".equals(userDetails.getStatus())) {
					isAccLocked = true;
				}
			}
		}
		return isAccLocked;
	}
	
	@RequestMapping(value="/validateSSNDetails")
	@ResponseBody
	public String validateSSNDetails(@RequestParam String ssn, @RequestParam String birthDate) {
		String sRetMsg = "";
		if(ssn == null || ssn.isEmpty() ) {
			if(birthDate == null || birthDate.isEmpty()) {
				sRetMsg =  "SSN and Birth date details are not provided.";
			}
			
		}else {
			boolean validSSN = Utils.validateSSN(ssn);
			
			if(!validSSN) {
				sRetMsg =  "SSN provided is not valid.";
			}else {
				ssn = ssn.replaceAll("-", "");
				Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
				Household dbHouseHold = householdRepository.findBySSN(ssn);
				if(dbHouseHold != null) {
					dbHouseHold = householdRepository.findBySSNAndBirthDate(ssn, dob);
					
					if(dbHouseHold != null) {
						sRetMsg =  "Account already exists for the provided SSN and birth date";
					}else {
						sRetMsg =  "Account already exists for the provided SSN";
					}
				}	
			}
		}
		return sRetMsg;
	}
	

	@RequestMapping(value="/account/user/ssnDetails",method = RequestMethod.GET)
	public String getSSNDetailsPage(Model model, HttpServletRequest request) {
		LOGGER.info("getSSNDetailsPage : END");
		return "account/user/ssnDetails";
	}
	
	private void setSessionAttributesForProvisionedUser(HttpServletRequest request, AccountUser user) throws Exception
	{
		String activeModuleName = user.getDefRole().getName().toLowerCase();
		String activeUserLabel = user.getDefRole().getLabel();
		Set<UserRole> userConfiguredRole = userService.getActiveUserRole(user);//HIX-30004	//HIX-27183 : Showing roles from UserRole
		String userDefLandingPage = user.getDefRole().getLandingPage(); //HIX-19109 :-- used to link masthed Logo
	
		request.getSession().setAttribute("userDefRoleName", activeModuleName);
		request.getSession().setAttribute("userLandingPage", userDefLandingPage);
		int activeModuleId=0;
		//HIX-28851 Application error is displayed on switching from Agent to Employer
		ModuleUser userDefModule = userService.getUserSelfSignedModule(activeModuleName ,user.getId() );
	
		if(userDefModule !=null){
			activeModuleId=userDefModule.getModuleId();
			activeModuleName=userDefModule.getModuleName();
		}
		
		user.setActiveModuleId(activeModuleId);
		user.setActiveModuleName(activeModuleName);
		user.setActiveModule(userDefModule);//To deprecated
	
		request.getSession().setAttribute("userActiveRoleName", activeModuleName);
		request.getSession().setAttribute("userConfiguredRoles", userConfiguredRole);
		request.getSession().setAttribute("userActiveRoleLabel", activeUserLabel);
	
	
		Set<UserRole> userRoleList = user.getUserRole();
		request.getSession().setAttribute("userRoleList", userRoleList);
		request.getSession().setAttribute("USER_NAME", user.getUserName().toLowerCase());
	
		checkLoggingEvent();
		userService.updateLastLogin(user);
	}
	
	private EligLead persistEligLead(ConsumerDto consumer, int userId ){
		EligLead emptyEligLeadRecord = new EligLead();
		emptyEligLeadRecord.setEmailAddress(consumer.getEmailAddress());
		emptyEligLeadRecord.setCreatedBy(userId);
		emptyEligLeadRecord.setName(consumer.getFullName());
		emptyEligLeadRecord.setStage(EligLead.STAGE.WELCOME);
		emptyEligLeadRecord = screenerUtil.createEligLeadRecord(emptyEligLeadRecord);
		if (emptyEligLeadRecord == null) {
		 	throw new RuntimeException("[100] Could not persist to database. Please contact admin.");
		}
		if(LOGGER.isDebugEnabled()) {
			LOGGER.debug("Created & persisted empty EligLead record. EligLeadId={}",emptyEligLeadRecord.getId());
		}
		return emptyEligLeadRecord;
	}
	
	@RequestMapping(value="/account/user/ssnDetails",method = RequestMethod.POST)
	public String getSSNDetails(Model model, HttpServletRequest request, RedirectAttributes redirectAttributes) throws Exception {
		String ssn = request.getParameter("ssn");
		String birthDate = request.getParameter("dob1");
		String ssnNotProvidedReason = null;
		int userId ;
		AccountUser user =  (AccountUser) request.getSession().getAttribute("user");
		String landingPage = null;
		UserSession userSession = null;
		HttpSession httpSession = null;
		boolean isPrivileged = false;
		Household household = null;
		
		try {
			if (user != null) {
				user = provisionUserIfRequired(user);
				setSessionAttributesForProvisionedUser(request, user);
				landingPage = user.getDefRole().getLandingPage();
				userId = user.getId();

				household = householdRepository.findByEmailAddress(user.getEmail());

				if (household == null) { // create new household
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("No household available, creating new");
					}
					household = new Household();
					household.setEmail(user.getEmail());

					ConsumerDto consumer = new ConsumerDto();
					consumer.setFirstName(user.getFirstName());
					consumer.setLastName(user.getLastName());
					consumer.setEmailAddress(user.getEmail());

					Object eligIdInSession = request.getSession().getAttribute(ScreenerUtil.LEAD_ID);
					Long eligLeadId = null;

					if (eligIdInSession != null && StringUtils.isNumeric(eligIdInSession.toString())) {
						eligLeadId = Long.valueOf(eligIdInSession.toString());
					}

					if (eligLeadId == null) {
						EligLead dummyEligLeadRecord = persistEligLead(consumer, user.getId());
						household.setEligLead(dummyEligLeadRecord);
					} else {
						EligLead eligLead = screenerUtil.fetchEligLeadRecord(eligLeadId);
						household.setEligLead(eligLead);
					}

					if (birthDate != null) {
						Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
						household.setBirthDate(dob);
					}
				}
				household.setUser(user);
				household.setFirstName(user.getFirstName());
				household.setLastName(user.getLastName());
				household.setPhoneNumber(user.getPhone());
				household.setSsn(ssn);

				if (birthDate != null) {
					Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
					household.setBirthDate(dob);
				}

				household = consumerUtil.saveHouseholdRecord(household);
			} else {
				landingPage = (String) request.getSession().getAttribute("landingPageUrl");
				userId = (Integer) request.getSession().getAttribute("userId");
				user = repository.findById(userId);
			}

			if (isSSOEnabled
					&& StringUtils.equalsIgnoreCase(
							user.getActiveModuleName(), "INDIVIDUAL")
					&& user.getActiveModuleId() == 0) {
				LOGGER.debug("SSO is enabled: Post registration of user is not complete");
				landingPage = "/indportal/postreg";

				userSession = sessionTracker.fetchUserSession(userId);
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("USR SESSION: UID: "+userId);
				}
				httpSession = sessionTracker.replaceHttpSessionWithDBSession(request, userSession,
						httpSession, user);
				request.getSession().setAttribute("isUserFullyAuthorized", "true");
			}
			request.getSession().setAttribute("sUser", user);

			if(household == null) {
				ModuleUser moduleUser =  userService.getUserDefModule("individual", userId);

				if(moduleUser != null){
					household = householdRepository.findById(moduleUser.getModuleId());
				}
				if(null != household ){
					if(ssn == null || ssn.isEmpty() ) {
						ssnNotProvidedReason = request.getParameter("ssnNotProvidedReason");
						household.setSsnNotProvidedReason(ssnNotProvidedReason);
					}else {
						ssn = ssn.replaceAll("-", "");
						household.setSsn(ssn);	
						Date dob = DateUtil.StringToDate(birthDate, DATE_FORMAT);
						household.setBirthDate(dob);
					}
					householdRepository.save(household);
				}
			}
		}catch(Exception ex){
			request.getSession().setAttribute("isUserFullyAuthorized", "false");
			LOGGER.error("Exception (/account/user/loginSuccess) :: " +ex.getMessage(),ex);
			if(StringUtils.isBlank(exceptionMessage)){
				exceptionMessage=ex.getMessage();
			}
			throw new Exception(exceptionMessage);

		}

		Role currUserDefRole = userService.getDefaultRole(user);
		if(LOGGER.isDebugEnabled()){
			LOGGER.info("currUserDefRole: "+ currUserDefRole.getName() != null ? currUserDefRole.getName() : "currUserDefRole is NULL");
		}
		userId = user.getId();
		if(null != currUserDefRole) {
			isPrivileged = currUserDefRole.getPrivileged() ==1;
		}

		forkNewSession(request);
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("Creating sesion for user with userId {}",userId);
		}

		if(null != userSession && userSession.getStatus().equalsIgnoreCase(UserSessionStatus.INIT.toString())) {
			if(null != httpSession) {	
				sessionTracker.markUserSessionInitToInProgress(httpSession.getId(),userId);
			}
		} else if(null == userSession){
			sessionTracker.createUserSession(request.getSession(), request.getRemoteAddr(),userId,isPrivileged );
		}

		request.getSession().removeAttribute("ssnRequired");
		request.getSession().removeAttribute("landingPageUrl");
		request.getSession().removeAttribute("userId");
		return "redirect:"+ landingPage;

	}
	
	@RequestMapping(value="/account/user/logout",method = RequestMethod.GET)
	@GiAudit(transactionName = "User logout",
	 eventType = EventTypeEnum.AUTHENTICATION,
	 eventName = EventNameEnum.USER_LOGOFF_SUCCESSFUL)
	public String logoutPage(Model model, HttpServletRequest request) {
		if(LOGGER.isInfoEnabled()) {
			LOGGER.info("User Logout page : SSO Enabled : {}",isSSOEnabled);
		}

		//HIX-45304 begin
		checkLoggingEvent();
		//HIX-45304 ends
		String logOutUrl = null;
		if (isSSOEnabled) {
			logOutUrl = "/saml/logout";
		}
		else {
			logOutUrl = "../../j_spring_security_logout";
		}
		LOGGER.info("logOutUrl: "+logOutUrl);
		this.sessionTracker.markUserSessionLoggedOut(request.getSession().getId());
		return "redirect:"+logOutUrl;
	}
	
	//This request is for California state to invalidate the existing session.
	@RequestMapping(value="/account/user/ssologout",method = RequestMethod.GET)
	public @ResponseBody String ssoLogoutPage(Model model, HttpServletRequest request) {
		LOGGER.info("Calling User SSO Logout Request.. ");
		String ssoLogoutResponse="GI SSO Logout Request...::";
		 Authentication currentUser = SecurityContextHolder.getContext().getAuthentication();
		 
		  HttpSession session = request.getSession(false);

	      if (session != null) {
	    	  LOGGER.info("Invalidating existing session (ssologout)");
	          session.invalidate();
	          SecurityContextHolder.clearContext();
	          this.sessionTracker.markUserSessionLoggedOut(session.getId());
	          if(currentUser!=null){
	        	  String userName = currentUser.getName();
	        	  ssoLogoutResponse = ssoLogoutResponse+"FAILED - Current Loggedin User Name :: "+userName;
	        	  LOGGER.error(ssoLogoutResponse);
		          return ssoLogoutResponse;
	          }else{
	        	  ssoLogoutResponse = ssoLogoutResponse+"SUCCESS ";
	        	  LOGGER.info(ssoLogoutResponse);
	        	  return ssoLogoutResponse;
	          }
	         
	      }
	      
	      return ssoLogoutResponse = ssoLogoutResponse+"SUCCESS ";
	      
	}

	/**
	 * Checks if the user profile had saved with security answers. If not, the contorller
	 * redirects to security questions page.
	 * @author venkata_tadepalli
	 * @since 12/19/2012
	 * @param  user AccountUser
	 * @return      boolean
	 * @see         loginSuccess, switchUserRole
	 */

	private boolean hasAnsweredSecurityQuestions(AccountUser user) {
		LOGGER.debug("hasAnsweredSecurityQuestions::");

		boolean result=true;

		if(StringUtils.isBlank(user.getSecurityAnswer1())
				|| StringUtils.isBlank(user.getSecurityAnswer2())
				/*|| StringUtils.isBlank(user.getSecurityAnswer3())*/){
			result=false;
		}
		LOGGER.debug("hasAnsweredSecurityQuestions::"+result);
		return result;
	}

	/**
	 * Returns an url for the landing page. If the User's active role is null then the landing page
	 * will be from default role.  This method will supports the role switch for the logged in user.
	 *
	 * @author venkata_tadepalli
	 * @since 11/28/2012
	 * @param  user AccountUser
	 * @return      landing page URL
	 * @throws GIException
	 * @see         loginSuccess, switchUserRole
	 */

	private String getLandingPage(AccountUser user,String roleName,HttpServletRequest request) throws GIException {
		LOGGER.debug("Get LandingPage ");
		Role activeRole = userService.getDefaultRole(user);
		//String redirectUri = request.getParameter("redirectUri");
		//LOGGER.info("Request param redirectUri ::"+redirectUri);

		String landingPage = "";

		try
		{
			//START :: Below block is added for individual signup flow

			String exchangeType = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.EXCHANGE_TYPE);
			LOGGER.info("Exchange Type ::" +exchangeType );
			if(StringUtils.isNotBlank(exchangeType)){
				if(exchangeType.equalsIgnoreCase("PHIX")){
					String referrerUrl = (String) request.getSession().getAttribute("referrerUrl");
					//String houseHoldId= (String) request.getSession().getAttribute("HOUSEHOLD_ID");// is it required here?
					//LOGGER.info("userController session.getAttribute(referrerUrl) ::" + referrerUrl);
					if(!StringUtils.isBlank(referrerUrl) ){
						//LOGGER.debug("Redirecting to Url::" + referrerUrl);
						landingPage = referrerUrl;
						return landingPage;//"redirect:"+referrerUrl;
					}

				}/*else if(currUserDefRole!= null && currUserDefRole.getDescription().equals("INDIVIDUAL")
							&& !GhixConfiguration.IS_SECURITY_QUESTIONS_REQUIRED )
					{
						String postRegistrationUrl = userService.getDefaultRole(user).getPostRegistrationUrl();
						return "redirect:"+postRegistrationUrl;
					}*/
			}
			//END :: Below block is added for individual signup flow
			boolean isSecQuestionMandatory=false; //HIX-24504 : Merged security questions page with account signup page. Note:
												  //      if the security questions page (while activation) is different page then
			                                      //      the default isSecQuestionMandatory value should be 'true'



			String isUserReg = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EXTERNAL);
			if(null!=isUserReg&&isUserReg.equalsIgnoreCase("true")){
				isSecQuestionMandatory = false;
			}

			LOGGER.debug("Is User Registration External ? :: "+ isUserReg);
			LOGGER.debug("Is Sec Question Mandatory ? :: "+ isSecQuestionMandatory);

			if(StringUtils.isBlank((roleName))){
				// if the moduleName is blank (empty , whitespace or null) then moduleName=user's default role name (in lowercase)
				roleName=StringUtils.lowerCase(userService.getDefaultRole(user).getName());
			}

			if(isSecQuestionMandatory&&!hasAnsweredSecurityQuestions(user)){
				//NOTE: the following block will executed due to the HIX-24504
				request.getSession().setAttribute("signUpRole",userService.getDefaultRole(user).getName());
				request.getSession().setAttribute("isUserFullyAuthorized", "false");
				landingPage="/account/securityquestions";
			} else {
				// HIX-29686 - Darshan Hardas : To check if it is a Duo-Security authentication
				String duoSecurityFlowFlag = request.getParameter("isDuoSecurityCleared");
				boolean isDuoSecurityFlow = ("true").equalsIgnoreCase(duoSecurityFlowFlag);
				if(isDuoSecurityFlow) { // Remove the param as User has already authenticated
					request.removeAttribute("isDuoSecurityCleared");
				}

				String redirectUri = (isDuoSecurityFlow) ? StringUtils.EMPTY : ghixGateWayService.getConversationUrl(request);

				//LOGGER.debug("redirectUri ::"+redirectUri);
				//if(userService.hasUserRole(user, roleName))
				{
					if(!StringUtils.isBlank(redirectUri)){
						landingPage =redirectUri + getRequestParameters(request);// required for rebuilding

					}else{
						activeRole=roleService.findRoleByName(StringUtils.upperCase(roleName));
						/* HIX-40546 - Changes for State Referral Account Starts*/
						if(request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL) != null && request.getSession().getAttribute(IndividualPortalConstants.FROM_STATE_REFERRAL).equals("Y") && roleName.equalsIgnoreCase("INDIVIDUAL"))
						{
							LOGGER.debug("Came Inside the State Referral Account Flow");
							landingPage = "/referral/activation/binduser";
						}
						else
						{
							//activerole is null, which shouldn't be the case HIX-102766
							activeRole  = (null != activeRole) ? activeRole : userService.getDefaultRole(user);
							landingPage = (null != activeRole) ? activeRole.getLandingPage() : "/";
							String pageContext = request.getParameter("pageContext");
							if(StringUtils.isNotBlank(pageContext) && StringUtils.isNotBlank(landingPage)) {
								landingPage += landingPage.indexOf("?") >= 0 ? "&pageContext="+pageContext : "?pageContext="+pageContext;
							}
							
							String previewId = request.getParameter("previewId");
							if(StringUtils.isNotBlank(previewId) && StringUtils.isNotBlank(landingPage)) {
								landingPage += landingPage.indexOf("?") >= 0 ? "&previewId="+previewId : "?previewId="+previewId;
							}
							
						}
						/* HIX-40546 - Changes for State Referral Account Ends*/
					}

				}
				userService.enableRoleMenu(request,roleName);

			}

			request.getSession().setAttribute("switchToThemeName", roleName);







			/*
			 * START:: HIX-10725 - Program Menu Links for Inbox, account manintenance

			if(StringUtils.isNotBlank(myAccountUrl)){
				myAccountUrl = myAccountUrl+"&userId="+user.getUserName();
			}else{
				myAccountUrl="#";
			}
			if(StringUtils.isBlank(myInboxUrl)){
				myInboxUrl="#";
			}

			request.getSession().setAttribute("myAccountUrl", myAccountUrl);
			request.getSession().setAttribute("myInboxUrl", myInboxUrl);

			 * END:: HIX-10725 - Program Menu Links for Inbox, account manintenance
			 */

		}
	    catch(Exception ex){
	    	LOGGER.error("Landing Page Exception ::"+ex.getMessage(),ex);
	    	throw new GIException("Landing Page Exception ::"+ex.getMessage(),ex);
	    }
		//LOGGER.debug("Redirecting to LandingPage after rebuilding :: "+landingPage);
		return landingPage;
	}

	/*
	 * Generates the sign request from the Duo Security
	 * using Secret Key, Integration key and Application Secret Key
	 *
	 */
	@RequestMapping(value="/account/user/duosecurity",method = RequestMethod.GET)
	public String verifyduo(Model model, HttpServletRequest request) throws Exception{
		AccountUser user = userService.getLoggedInUser();
		if(null == user || ! isTwoFactorAuthenticationRequired(user, null)) {
			LOGGER.warn("Directly accessing the account/user/duosecurity URL redirecting to login page ");
			return "redirect:/account/user/login";
		}
		
		LOGGER.debug("Generating sig_request from Duo Security");
		model.addAttribute("page_title", "Getinsured Health Exchange: Second Level Authentication" );
		Map<String, String> keyMap = getkeysForTwofactorAuthentication();
		String ikey = keyMap.get("ikey");
		String skey = keyMap.get("skey");
		String akey = keyMap.get("akey");
		String sig_request = DuoWeb.signRequest(ikey, skey, akey, user.getUsername());
		LOGGER.info("sig_request for " + user.getUsername() + "::"+ sig_request);
		model.addAttribute("sig_request", sig_request );
		model.addAttribute("duoHostName", GhixPlatformConstants.DUO_HOSTNAME);	//HIX-29686 - Darshan Hardas : Added to set the server
		SecurityContextHolder.getContext().setAuthentication( null);
		return "account/user/duosecurity";
	
	}
	/*
	 * Checks if the two factored authentication is required for the given role
	 */
	private boolean isTwoFactorAuthenticationRequired(AccountUser user, List<GiAuditParameter> mfaAuditParams) {
		boolean isTwoFactorRequired = false;
		
		try {
			Set<UserRole> userRoles = user.getUserRole();
			RoleLoop : for (UserRole userRole : userRoles) {
				if(!userRole.isActiveUserRole()){
					continue RoleLoop;
				}
				if( MFA_ENABLED.equalsIgnoreCase( userRole.getRole().getMfaEnabled() )  && userRole.getRole().getPrivileged() ==0){
					switch (userRole.getRole().getName().toUpperCase()) {
					case "BROKER":
						Broker broker = brokerService.findBrokerByUserId(user.getId());
						if (null == broker || !("certified".equalsIgnoreCase(broker.getCertificationStatus() ) ) ){
							continue RoleLoop;
						}
					break;
					case "ASSISTER":
						AssisterRequestDTO assisterRequestDTO  = new AssisterRequestDTO();
						assisterRequestDTO.setUserId(user.getId());
						String assisterDetailsResponse = restClassCommunicator.getAssisterDetailByUserId(assisterRequestDTO);
						AssisterResponseDTO assisterResponseDTO = (AssisterResponseDTO) xstream.fromXML(assisterDetailsResponse);
						Assister existingAssister = assisterResponseDTO.getAssister();
						if (existingAssister == null || !( EntityConstants.STATUS_CERTIFIED.equalsIgnoreCase(existingAssister.getCertificationStatus() ) ) ) {
							continue RoleLoop;
						}
					break;	
					case "ASSISTERENROLLMENTENTITY":
						if(entityUtils.isEnrollmentEntityCertifiedAndActive(user.getId()) ) {
							continue RoleLoop;
						}
					break;	
					default:
						break;
					}
				}
				if( MFA_ENABLED.equalsIgnoreCase( userRole.getRole().getMfaEnabled() ) ){
					isTwoFactorRequired = true;
					LOGGER.info("isTwoFactorRequired for the role::  " + userRole.getRole().getLabel() + "::" + isTwoFactorRequired);
				}
			}
			
			
			/*
			 * removed the code as per HIX-104035
			 * 		if the role and a/c has passed for mfa check add additional bypass logic 
			 * 		from HIX-100929:  from giappconfig.global.mfaBypass config extract list of username and time for which the mfa 
			 * 		needs to be bypassed; if time not mentioned default to 30 mints.
			 * 
			 */
			/*
			 * if(isTwoFactorRequired && null != mfaAuditParams){
				isTwoFactorRequired = ( canBypassMFA(user, mfaAuditParams) ) ? false : isTwoFactorRequired;
			}
			*/
			
		}
		catch(Exception ex) {
			LOGGER.error("Errors in isTwoFactorAuthenticationRequired: ",ex);
		}
		return isTwoFactorRequired;
	}

	/*
	 * removed the code as per HIX-104035
	 */
	/*get whitelisted users from giappconfig
	 *match the logged in user email with the whitelisted user email
	 *get the time allocated for the user, till which he can bypass the mfa.
	 *if user lastlogedin time + duration allowed is less the current time then 
	 *bypass mfa check, returns true. 
	 */
	/*private boolean canBypassMFA(AccountUser user, List<GiAuditParameter> mfaAuditParams) {
		boolean byPassMFA = false;
		String bypassChecks = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.BYPASS_MFA_CHECK);
		List<String> bypassCheckList  = Splitter.on("#").omitEmptyStrings().trimResults().splitToList(bypassChecks);
		String bypassUserName = "";
		int duration  = 30;
		String crr = "";
		for (String bypassstr : bypassCheckList) {
			Map<String, String> bypassCheckMap = Splitter.on(",").trimResults().withKeyValueSeparator(":").split(bypassstr);
			bypassUserName = (bypassCheckMap.get("userName") == null) ? "": bypassCheckMap.get("userName");
			duration  = (bypassCheckMap.get("duration") == null) ? 30 : Integer.parseInt(bypassCheckMap.get("duration")); 
			crr = (bypassCheckMap.get("crr") == null) ? "" : bypassCheckMap.get("crr"); 
			if(null != mfaAuditParams){
				String cofiguredAudit = "Configured userName : "+bypassUserName+", CRR:"+crr+", Duration:"+duration;
				mfaAuditParams.add(new GiAuditParameter("Configured user,CRR,duration for mfa check", cofiguredAudit));
				mfaAuditParams.add(new GiAuditParameter("Logged in user", user.getId()));
			}
			if(bypassUserName.equals(user.getUsername()) ){
				return (! sessionTracker.isLasMFACheckBypassed(user,duration,mfaAuditParams) );
			}
		}
		
		return byPassMFA;
	}*/
	/*
	 * Get the keys from the configuration.properties
	 * The keys are generated through duo security
	 */
	private Map<String, String> getkeysForTwofactorAuthentication() {
		Map <String, String>keyMap = new HashMap<String, String>();
		try {
			keyMap.put("ikey", (GhixPlatformConstants.DUO_INTEGRATION_KEY==null)?"":GhixPlatformConstants.DUO_INTEGRATION_KEY);
			keyMap.put("skey", (GhixPlatformConstants.DUO_SECRET_KEY==null)?"":GhixPlatformConstants.DUO_SECRET_KEY);
			keyMap.put("akey", (GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY==null)?"":GhixPlatformConstants.DUO_APPLICATION_SECRET_KEY);
		}
		catch (Exception ex) {
			LOGGER.error("Errors in getkeysForTwofactorAuthentication: ",ex);
		}
		return keyMap;
	}


	/*
	 * This function is to be used for displaying account activation users for the user management story
	 */
	@SuppressWarnings("unused")
	private List<AccountUser> getActivationUsers(
			Map<String, Object> searchCriteria) {

		List<AccountUser> accountUsers = new ArrayList<AccountUser>();
		List<AccountActivation> activationUserList = accountActivationService.getAccountActivationAdminUsers();
		for (AccountActivation accountActivation : activationUserList) {
			AccountUser accountUser  = new AccountUser();
			ActivationJson activationJson = accountActivationService.getActivationJson(accountActivation.getJsonString());
			Map<String, String> customFields = activationJson.getCreatedObject().getCustomeFields();

				accountUser.setFirstName(customFields.get("firstName"));
				accountUser.setLastName(customFields.get("lastName"));
				UserRole userRole = new UserRole();
				Role role = roleService.findById(Integer.parseInt(customFields.get("userRoleId")));
				userRole.setRole(role);
				Arrays.asList(userRole);
				Set<UserRole> userRoles = new HashSet<UserRole>(Arrays.asList(userRole));
				accountUser.setUserRole(userRoles);
				accountUser.setEmail(customFields.get("email"));
				accountUser.setPhone(customFields.get("phone"));
				accountUser.setFirstName(customFields.get("firstName"));
				accountUser.setStatus("Pending");
				accountUsers.add(accountUser);

		}
			return accountUsers;
	}
	
	private String getRequestParameters(HttpServletRequest request) {
		StringBuffer sb = new StringBuffer();
		Enumeration<String> e = request.getParameterNames();
		try {
			while( e.hasMoreElements() ){
				 String key = e.nextElement();
				 String[] val = request.getParameterValues( key );
				 
				 if(!key.equalsIgnoreCase("redirectUri")) {
					sb.append((sb.length()==0)?"?":"&");
					 sb.append(key);
					 
					 
					 sb.append("=");
					 sb.append(val[0]);
				 }
			}

		}
		catch ( Exception ex) {
			LOGGER.error("UserController:getRequestParameters:Exception", ex);
		}
		if(sb.length() > 0) {
			LOGGER.debug("Request headers from user controller:" + sb.toString());
			return sb.toString();
		}
		else {
			LOGGER.debug("Request headers from user controller:" + sb.toString());
			return "";
		}
	}

	/** Loads the account settings information for the user
	 *  HIX-46480  : Configuration to set the number of SecurityQuestion Sets ;  Enhanced to support configurable no of security questions
	 *  HIX-47504  : Server Side validation
	 * -Venkata Tadepalli
	 * @param model
	 * @param request
	 * @return
	 */
		@RequestMapping(value="/account/user/accountsettings", method=RequestMethod.GET)
		@GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_READ)
		public String accountSetup(Model model, HttpServletRequest request) {
			try {
				HttpSession session = request.getSession();

				AccountUser user = userService.getLoggedInUser();
				if(null == user){
					return "redirect:/account/user/logout";
				}
				user = repository.findByUserName(userService.getLoggedInUser().getUsername());
				
				if(user != null) {
					model.addAttribute("email", user.getEmail());
					GiAuditParameterUtil.add(new GiAuditParameter("email", user.getEmail()), new GiAuditParameter("user id", user.getId()));
					//START : HIX-464680 , HIX-47504
					List<QuestionAnswer> userSecQstnAnsList = AccountUserDto.getUserSecurityQuestionAnswers(user);
					model.addAttribute("userSecQstnAnsList",userSecQstnAnsList);
				
					String errorMessage = (session.getAttribute("errorMsg")==null) ?"":session.getAttribute("errorMsg").toString();
					model.addAttribute("errorMsg", errorMessage);
					session.removeAttribute("errorMsg");
					
					String successMsg = (session.getAttribute("successMsg")==null) ?"":session.getAttribute("successMsg").toString();
					model.addAttribute("successMsg", successMsg);
					session.removeAttribute("successMsg");
					LOGGER.info("account settings  page");
					model.addAttribute("page_title", "Getinsured Health Exchange: Account Settings" );
				}else {
					return "redirect:/account/user/logout";					
				}
			}

			catch (Exception ex) {
				LOGGER.error("Errors in account settings::", ex);
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error loading account settings page"));
			}
			return "account/user/accountsettings";
		}

		/** Chnages the password for the user. The current password must be correctly entered for
		 * successful update of the password
		 * @param model
		 * @param request
		 * @return
		 */
		@RequestMapping(value="/account/user/passwordchange",method = RequestMethod.POST)
		@GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_WRITE)
		public String passwordChangeSubmit(Model model, HttpServletRequest request) {
			String currentPassword = request.getParameter("currentpswd");
			String newPassword = request.getParameter("newpassword");

			String submitRedirectURL = request.getParameter("submitRedirectURL");
			String errorMsg = "";

			try {
				HttpSession session = request.getSession();
				AccountUser domainUser = repository.findByUserName(userService.getLoggedInUser().getUsername());
				String domainUserPassword = domainUser.getPassword();


				//if(!domainUserPassword.equalsIgnoreCase(currentPassword)) {
				if(!isScimEnabled && !userService.isPasswordMatch(domainUser, currentPassword,domainUserPassword)) {//HIX-32110
					//String errorMSg = "The current password doesn't match with our records.";
					//model.addAttribute("errorMsg", errorMSg);
					errorMsg = "The current password doesn't match with our records.";
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));

					session.setAttribute("errorMsg", errorMsg);
					if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
						return "redirect:" + submitRedirectURL;
					}
					return "redirect:/account/user/accountsettings";
				}
				LOGGER.info("checkUserPassword in /account/user/passwordchange link: START");
		        boolean passwordreseterror = false;

		       /*
		        * As per HIX-38037 id .Passing passing password minimum Age as 0.
   		         * Commented method can be used in future to fetch property from Database.
		        *
		        * if(!validationUtility.checkForPasswordMinAge(userService.getLoggedInUser(), DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_MIN_AGE))
		        		){
        			errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
        			passwordreseterror = true;
		        }*/

		        if(!validationUtility.checkForPasswordMinAge(userService.getLoggedInUser(),ZERO)
		        		){
        			errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordminage", null, LocaleContextHolder.getLocale()));
        			passwordreseterror = true;
		        }

				if(!passwordreseterror && (!validationUtility.checkForPasswordComplexity(newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX)) ||
				   (!isScimEnabled && validationUtility.isPasswordAvailableInPasswordHistory(userService.getLoggedInUser().getUsername(), newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))))
				   ){
		        		errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistoryandminage", null, LocaleContextHolder.getLocale()));
		        		passwordreseterror = true;

				}
				if(passwordreseterror){
					session.setAttribute("errorMsg", errorMsg);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, errorMsg));
					if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
						return "redirect:" + submitRedirectURL;
					}
					return "redirect:/account/user/accountsettings";
				}


		        LOGGER.info("checkUserPassword in /account/user/passwordchange link: END");

				session.setAttribute("errorMsg", errorMsg);

				//String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
				if(isScimEnabled){
					LOGGER.info("Using SCIM Manager to update the password for remote user");
					/*domainUser.setPassword(currentPassword);
					if(!scimUserManager.changePassword(domainUser, newPassword)){
						GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),
								new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Unable to change password for remote user"));
						throw new WSO2Exception("Unable to change password for remote user");
					}
					domainUser.setPassword("PASSWORD_NOT_IN_USE");
					domainUser.setPasswordLastUpdatedTimeStamp(new TSDate());
					domainUser.setRetryCount(0);
					domainUser.setSecQueRetryCount(0);
					domainUser.setConfirmed(1);
					domainUser.setPasswordRecoveryToken(null);
					domainUser.setPasswordRecoveryTokenExpiration(null);
					domainUser.setLastUpdatedBy(userService.getLoggedInUser().getId());
					repository.save(domainUser);*/
					session.setAttribute("successMsg", "Password changed successfully");
				}
				else{
					if(userService.changePasswordSelf(domainUser.getId(), currentPassword,  newPassword) && userService.getLoggedInUser() != null){
						userService.getLoggedInUser().setPassword(domainUser.getPassword());
						session.setAttribute("successMsg", "Password changed successfully");
					}
					
				}


			}
			/*catch (WSO2Exception ex) {
				LOGGER.error("Exception in changing the password for the remote user", ex);
				HttpSession session = request.getSession();
				session.setAttribute("errorMsg", ex.getMessage());
			}*/
			catch (Exception ex) {
				LOGGER.error("Exception in changing the password", ex);
				HttpSession session = request.getSession();
				if(ex.getMessage().equalsIgnoreCase("30000")) {
					session.setAttribute("errorMsg", messageSource.getMessage("label.wso2msgforpasswordhistoryplugin", null, LocaleContextHolder.getLocale()));
				} else {
					session.setAttribute("errorMsg", "Errors in changing the password");
				}
			}
			if(null!= submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
				return "redirect:" + submitRedirectURL;
			}
			return "redirect:/account/user/accountsettings";
		}


		/** Chnages the password for the user. The current password must be correctly entered for
			 * successful update of the password
			 * @param model
			 * @param request
			 * @return
			 */
			@RequestMapping(value="/account/user/passwordchangeforlogin",method = RequestMethod.POST)
			@GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.PASSWORD_CHANGE, eventName = EventNameEnum.PII_WRITE)
			public String passwordChangeSubmitForLogin(Model model, HttpServletRequest request) {
				String currentPassword = request.getParameter("currentpswd");
				String newPassword = request.getParameter("newpassword");
				String submitRedirectURL = request.getParameter("submitRedirectURL");
				String errorMsg = "";

				try {
					HttpSession session = request.getSession();
					String username = request.getParameter("email");
					if(StringUtils.isEmpty(username)){
						username =	request.getSession().getAttribute(GhixPlatformConstants.USER_NAME)+"";
					}
					AccountUser domainUser = repository.findByUserName(username);
					String domainUserPassword = domainUser.getPassword();
					
					if(isScimEnabled){
		    			LOGGER.info("Using SCIM Manager to authenticate user using old pwd");
		    			try {
		    				if(!scimUserManager.authenticate(domainUser.getUsername(), currentPassword)){
		    					errorMsg = "The current password doesn't match with our records.".intern();
		    					model.addAttribute("isPasswordChanged", false);

								session.setAttribute("errorMsgForPasswordExpiry".intern(), errorMsg);
								session.setAttribute("IS_PASSWORD_EXPIRED".intern(), true);
								session.setAttribute("PASSWORD_EXPIRED_MESSAGE".intern(), errorMsg);

								if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue".intern())){
									return "redirect:" + submitRedirectURL;
								}
								return "redirect:/account/user/loginfailed";
		    				}
		    			} catch (GIException e) {
		    				if(e.getMessage().contains("17003".intern())){
		    					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account Locked for "+domainUser.getUserName()));
		    				   request.getSession().invalidate();
			            	   SecurityContextHolder.clearContext();
			            	   request.getSession().setAttribute(GhixPlatformConstants.USER_NAME,domainUser.getEmail());
			            //	   return "redirect:/account/user/loginfailed";
			            	   return "account/user/useraccountlocked";
		    				}
		    				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, e.getMessage()));
		    			}
		    		}else if(!userService.isPasswordMatch(domainUser, currentPassword,domainUserPassword)) {//HIX-32110
		    			//String errorMSg = "The current password doesn't match with our records.";
		    			//model.addAttribute("errorMsg", errorMSg);
		    			errorMsg = "The current password doesn't match with our records.";
		    			model.addAttribute("isPasswordChanged", false);

						session.setAttribute("errorMsgForPasswordExpiry", errorMsg);
						session.setAttribute("IS_PASSWORD_EXPIRED", true);
						session.setAttribute("PASSWORD_EXPIRED_MESSAGE", errorMsg);

						if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
							return "redirect:" + submitRedirectURL;
						}
						return "redirect:/account/user/loginfailed";
		    		}
					
					LOGGER.info("checkUserPassword in /account/user/passwordchangeforlogin link: START");
			        boolean passwordreseterror = false;


					if(!validationUtility.checkForPasswordComplexity(newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX)) ||
					   validationUtility.isPasswordAvailableInPasswordHistory(domainUser.getUsername(), newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))
					   ){
			        		errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistoryandminage", null, LocaleContextHolder.getLocale()));
			        		passwordreseterror = true;

					}
					if(passwordreseterror){
						session.setAttribute("errorMsgForPasswordExpiry", errorMsg);
						session.setAttribute("IS_PASSWORD_EXPIRED", true);
						session.setAttribute("PASSWORD_EXPIRED_MESSAGE", errorMsg);
						if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
							return "redirect:" + submitRedirectURL;
						}
						return "redirect:/account/user/login";
					}


			        LOGGER.info("checkUserPassword in /account/user/passwordchangeforlogin link: END");

			        if(isScimEnabled){
						LOGGER.info("Using SCIM Manager to update the password for remote user");
						domainUser.setPassword(currentPassword);
						if(!scimUserManager.changePassword(domainUser, newPassword)){
							throw new WSO2Exception("Unable to change password for remote user");
						}
						domainUser.setPasswordLastUpdatedTimeStamp(new TSDate());
						domainUser.setRetryCount(0);
						domainUser.setSecQueRetryCount(0);
						domainUser.setConfirmed(1);
						domainUser.setPasswordRecoveryToken(null);
						domainUser.setPasswordRecoveryTokenExpiration(null);
						domainUser.setPassword("PASSWORD_NOT_IN_USE");
						if(userService.getLoggedInUser() != null){
							domainUser.setLastUpdatedBy(userService.getLoggedInUser().getId());
						}
						repository.save(domainUser);
			        	session.setAttribute("errorMsg", "Password changed successfully");
					}
					else{
						userService.changePassword(domainUser.getId(), newPassword);
					}

					session.setAttribute("IS_PASSWORD_EXPIRED", false);
					request.getSession().setAttribute("AUTH_FAILED_PASSWORD_MAX_AGE",false);
	         	   	request.getSession().removeAttribute(GhixPlatformConstants.USER_NAME);


				}
				catch (WSO2Exception ex) {
					LOGGER.error("Exception in changing the password for the remote user", ex);
					HttpSession session = request.getSession();
					errorMsg = ex.getMessage();
					session.setAttribute("errorMsg", errorMsg);
					model.addAttribute("isPasswordChanged", false);
					session.setAttribute("errorMsgForPasswordExpiry", errorMsg);
					session.setAttribute("IS_PASSWORD_EXPIRED", true);
					session.setAttribute("PASSWORD_EXPIRED_MESSAGE", errorMsg);
					
					if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
						return "redirect:" + submitRedirectURL;
					}
					return "redirect:/account/user/loginfailed";
				}
				catch (Exception ex) {
					LOGGER.error("Exception in changing the password", ex);
					HttpSession session = request.getSession();
					if(ex.getMessage().equalsIgnoreCase("30000")) {
						errorMsg = messageSource.getMessage("label.wso2msgforpasswordhistoryplugin", null, LocaleContextHolder.getLocale());
						session.setAttribute("IS_PASSWORD_PRESENT_IN_HISTORY", true);
					} else {
						errorMsg = "Errors in changing the password";
					}
					session.setAttribute("errorMsg", errorMsg);
					model.addAttribute("isPasswordChanged", false);
					session.setAttribute("errorMsgForPasswordExpiry", errorMsg);
					session.setAttribute("IS_PASSWORD_EXPIRED", true);
					session.setAttribute("PASSWORD_EXPIRED_MESSAGE", errorMsg);
					
					if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
						return "redirect:" + submitRedirectURL;
					}
					return "redirect:/account/user/loginfailed";
				}
				/*if(null!= submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){

					return "redirect:/account/user/login";
				}
				return "redirect:/account/user/login"; */
         		return "account/user/passwordchangedonexpiry";

			}

			/** Chnages the password for the user. The current password must be correctly entered for
			 * successful update of the password
			 * @param model
			 * @param request
			 * @return
			 */
			@RequestMapping(value="/account/user/passwordchangeprelogin",method = RequestMethod.POST)
			public String passwordChangeSubmitPreLogin(Model model, HttpServletRequest request) {
				String currentPassword = request.getParameter("currentpswd");
				String newPassword = request.getParameter("newpassword");
				String userName = request.getSession().getAttribute(GhixPlatformConstants.USER_NAME) + "";

				String submitRedirectURL = request.getParameter("submitRedirectURL");
				String errorMsg = "";

				try {
					HttpSession session = request.getSession();
					AccountUser domainUser = repository.findByUserName(userName);
					String domainUserPassword = domainUser.getPassword();
					if(!domainUserPassword.equalsIgnoreCase(currentPassword)) {
						//String errorMSg = "The current password doesn't match with our records.";
						//model.addAttribute("errorMsg", errorMSg);
						errorMsg = "The current password doesn't match with our records.";

						session.setAttribute("errorMsg", errorMsg);
						if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
							return "redirect:" + submitRedirectURL;
						}
						return "redirect:/account/user/accountsettings";
					}
					LOGGER.info("checkUserPassword in /account/user/passwordchange link: START");
			        boolean passwordreseterror = false;


					if(!validationUtility.checkForPasswordComplexity(newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.REGEX)) ||
					   validationUtility.isPasswordAvailableInPasswordHistory(userService.getLoggedInUser().getUsername(), newPassword, DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.PASSWORD_HISTORY_LIMIT))
					   ){
			        		errorMsg = DisplayUtil.replaceDynamicValuesInMessageSource(messageSource.getMessage("label.validatemsgforpasswordcomplexitywithhistoryandminage", null, LocaleContextHolder.getLocale()));
			        		passwordreseterror = true;

					}
					if(passwordreseterror){
						session.setAttribute("errorMsg", errorMsg);
						if (null!=submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
							return "redirect:" + submitRedirectURL;
						}
						return "redirect:/account/user/accountsettings";
					}


			        LOGGER.info("checkUserPassword in /account/user/passwordchange link: END");

					session.setAttribute("errorMsg", errorMsg);
					userService.changePassword(domainUser.getId(), newPassword);


				}
				catch (Exception ex) {
					LOGGER.error("Exception in changing the password", ex);
					HttpSession session = request.getSession();
					session.setAttribute("errorMsg", "Errors in changing the password");
				}
				if(null!= submitRedirectURL && !submitRedirectURL.equalsIgnoreCase("submitRedirectURLValue")){
					return "redirect:" + submitRedirectURL;
				}
				return "redirect:/account/user/accountsettings";
			}
		/** The security questions are updated
		 * @param user
		 * @param result
		 * @param model
		 * @param request
		 * @return
		 * @throws InvalidUserException
		 */
		@RequestMapping(value = "/account/user/securityquestions", method = RequestMethod.POST)
		@GiAudit(transactionName = "Account Settings", eventType = EventTypeEnum.ACCOUNT_SETTINGS, eventName = EventNameEnum.PII_WRITE)
		public String submitQuestions(
				@ModelAttribute("AccountUser") AccountUser user,
				BindingResult result, Model model, HttpServletRequest request) {
			LOGGER.info("Security Questions Update");
			boolean isValid = true;
			List<QuestionAnswer> questions = null;

			try {
				if (null != user) {
					questions = AccountUserDto.getUserSecurityQuestionAnswers(user);
					if(null != questions && !questions.isEmpty()) {
						isValid = AccountUserDto
								.validateSecurityQuestionAnswers(questions);
						} else {
						isValid = false;
					}
				}
			} catch (Exception ex) {
				isValid = false;
			}

			if(!isValid) {
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Error occured while fetching security questions"));
				throw new GIRuntimeException(
						"PLATFORM-00004", null,
						GhixPlatformConstants.SERVER_SIDE_VALIDATION_EXCEPTION,
						Severity.HIGH);
			}

			try {
				AccountUser accountUser = repository.findByUserName(userService.getLoggedInUser().getUsername());
				if (user != null) {

					accountUser.setSecurityQuestion1(user
							.getSecurityQuestion1());
					accountUser.setSecurityQuestion2(user
							.getSecurityQuestion2());
//					AS PER HIX-25785 and HIX-21569 story - By Darshan Hardas
					accountUser.setSecurityQuestion3(user.getSecurityQuestion3());
					accountUser.setSecurityAnswer1(user.getSecurityAnswer1());
					accountUser.setSecurityAnswer2(user.getSecurityAnswer2());
					//AS PER HIX-25785 and HIX-21569 story - By Darshan Hardas
					accountUser.setSecurityAnswer3(user.getSecurityAnswer3());
					accountUser.setLastUpdatedBy(userService.getLoggedInUser().getId());
					userService.updateUser(accountUser);
					GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Security Question/Answer updated successfully"));
				}
			} catch(Exception ex) {
				LOGGER.error("Exception in setting the security questions::", ex);
				HttpSession session = request.getSession();
				session.setAttribute("errorMsg", "Errors in updating the security questions");
				GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.FAILURE),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Errors in updating the security questions"));
			}

			if(null!=request.getParameter("submitRedirectURL")&&!request.getParameter("submitRedirectURL").equalsIgnoreCase("submitRedirectURLValue")){
				return "redirect:"+request.getParameter("submitRedirectURL");
			}
			return "redirect:/account/user/accountsettings";
		}

		/**
		 *  HIX-27181 , HIX-26903 : Add controller method for secondary role provisioning
		 *  HIX-49523 :  The /account/user/addrole/{nonDefUserRoleName} is not available until further analysis
		 *               (Venkata Tadepalli : Sept 15-2014)
		 * @author Venkata Tadepalli
		 * @since 01/13/2014
		 * @param model
		 * @param redirectAttributes
		 * @param defUserRoleName
		 * @return String
		 * @throws InvalidUserException
		 * @throws GIException
		 */
		//@RequestMapping(value = "/account/user/addrole/{nonDefUserRoleName}", method = RequestMethod.GET)
		public String accountAddRoleSignup(Model model, HttpServletRequest request,
				@PathVariable("nonDefUserRoleName") String nonDefUserRoleName) throws InvalidUserException, GIException {

			LOGGER.info("Adding non-default Role ("+nonDefUserRoleName+") to User : START");
			String redirectRoleSignUpUrl="/";

			try {
				// MSHIX-734 : Add Role for existing users, security weakness
				Role nonDefUserRole = roleService
						.findRoleByName(nonDefUserRoleName);
				if (nonDefUserRole == null || (nonDefUserRole.getPrivileged() == 1)) {
					LOGGER.error("Invalid User role");
					return "redirect:/";
				}
				AccountUser user=userService.getLoggedInUser();
				userService.addUserRole(user, nonDefUserRoleName);
				redirectRoleSignUpUrl= getLandingPage(user,nonDefUserRoleName,request);
				//HIX-27183 : Redirect user to selected URL and then show appropriate role label on masthead.jsp
				String switchToModuleName = null,activeUserLabel=null;
				if (null!=nonDefUserRoleName) {
					UserRole userRole = userService.getUserRoleByRoleName(user,nonDefUserRoleName);
					switchToModuleName = userRole.getRole().getName().toLowerCase();
					activeUserLabel = userRole.getRole().getLabel();
				} else {
					switchToModuleName = userService.getDefaultRole(user).getName().toLowerCase();
					activeUserLabel= userService.getDefaultRole(user).getLabel();
				}
				//----------Added for HIX-29813. This will check if any entry there for this user----------//
				int activeModuleId = 0;
				ModuleUser userDefModule = userService.getUserSelfSignedModule(switchToModuleName ,user.getId());
				if(userDefModule !=null){
					activeModuleId=userDefModule.getModuleId();
					switchToModuleName=userDefModule.getModuleName();
				}
				userService.getLoggedInUser().setActiveModuleId(activeModuleId);
				userService.getLoggedInUser().setActiveModuleName(switchToModuleName);
				userService.getLoggedInUser().setActiveModule(userDefModule);
				//----------------------------------------------------------------------------------------//
				LOGGER.info("Adding non-default Role ("+nonDefUserRoleName+") to User : END");
				request.getSession().setAttribute("userConfiguredRoles", user.getUserRole());
				//HIX-27183 : Redirect user to selected URL and then show appropriate role label on masthead.jsp
				request.getSession().setAttribute("userActiveRoleName", switchToModuleName);
				request.getSession().setAttribute("userActiveRoleLabel", activeUserLabel);
			}
			catch (Exception e) {
				throw  new GIRuntimeException("PLATFORM-00002",e,"",Severity.HIGH);
			}


			return "redirect:"+redirectRoleSignUpUrl;
		}



		/**Check if the user came from email activation link and didn't finish the steps required
		 * and then set the session so that the user is forced to finish all the required steps
		 *
		 * To avoid querying DB we can enhance the AccountUser object to include AccountActivationId and ActivationStatus
		 * so that only for the users with NOTPROCESSED status can be searched in the AccountActivation object
		 * @param string
		 */
		public int checkEmailActivationFlow(String userName) {
			String isEmailActivation = DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_ACTIVATION);
			if(null!=isEmailActivation&&isEmailActivation.equalsIgnoreCase("true")) {
				AccountActivation accountActivation = accountActivationService.getUnProcessedAccountActivationByUserName(userName);
				if(accountActivation != null) {
					
					return accountActivation.getId();
				}
			}
			return -1;
		}


		protected UsernamePasswordAuthenticationToken getPrincipal(String username) {

			UserDetails user = this.userDetailsService.loadUserByUsername(username);

	        UsernamePasswordAuthenticationToken authentication =
	                new UsernamePasswordAuthenticationToken(
	                        user,
	                        user.getPassword(),
	                        user.getAuthorities());

	        return authentication;
	    }
		
	@RequestMapping(value = "/account/user/resetemail/{tokenid}", method = RequestMethod.GET)
	public String resetEmail(HttpServletRequest request,
			HttpServletResponse response,
			@PathVariable("tokenid") String tokenId) {
		String redirectPath = "account/user/changemail";
		String userIdentifier = null;
		String newEmailAddress = null;
		String errorMsg = "Invalid token. Either token has expired or it does not exist.";

		AccountUser accountUser = null;
		Household household = null;
		UserTokens userTokens = null;

		try {
			if ("TRUE".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_CHANGE_ENABLED))) {
				if (!StringUtils.isEmpty(tokenId)) {
					newEmailAddress = ghixJasyptEncrytorUtil
							.decryptGencStringByJasypt(tokenId);
					userTokens = userTokenService.findUserToken(tokenId);

					if (null != userTokens) {
						Timestamp expiryTime = userTokens.getTokenExpiry();
						Timestamp currentTime = new Timestamp(
								TimeShifterUtil.currentTimeMillis());

						if (LOGGER.isInfoEnabled()) {
							LOGGER.info("RESET EMAIL: CURR TIME: "
									+ currentTime.toString() + " EXPIRY TIME: "
									+ expiryTime.toString());
						}

						if (currentTime.after(expiryTime)) {
							errorMsg = "Token has expired. Please initiate the change email process again.";
						} else {
							userIdentifier = userTokens.getUserIdentifier();
							accountUser = userService
									.findByEmail(userIdentifier);

							
							if(accountUser != null){
								if (LOGGER.isDebugEnabled()) {
								LOGGER.debug("RESET EMAIL: OLD EMAIL: "
										+ accountUser.getUserName()
										+ " NEW EMAIL: " + newEmailAddress);
								}
								accountUser.setEmail(newEmailAddress);
							
								userService.changeEmail(accountUser);
								DuoAdminApiReturnStatus duoAdminApiReturnStatus = DuoAdminApi.removeDuoCheck(userIdentifier); 
								if (LOGGER.isDebugEnabled()) {
									LOGGER.debug("Duo a/c checked : "+ duoAdminApiReturnStatus
											+ " OLD EMAIL: " + userIdentifier
											+ " NEW EMAIL: " + newEmailAddress);
								}
								
							    household = householdRepository.findByUserId(accountUser.getId());
							
								if(null != household) {
									household.setEmail(newEmailAddress);
									householdRepository.save(household);
								}
							
								userTokens.setStatus('0');
								iUserTokensRepository.save(userTokens);
								//setting Security Auth to null in case of changed username and when there is loggin user in session
								AccountUser logginuser = userService.getLoggedInUser();
								if(null != logginuser && logginuser.getUserName().equalsIgnoreCase(userIdentifier)){
									SecurityContextHolder.getContext().setAuthentication(null);
								}
								
								errorMsg = null;
							}else{
								errorMsg = "Token has expired or already used, Please initiate the change email process again";
							}
						}

					}
				}
			} else {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				errorMsg = "This operation is not allowed";
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE RESETTING EMAIL: ", ex);
		} finally {
			if (LOGGER.isInfoEnabled()) {
				LOGGER.info("CHNG EMAIL: ERR MSG(IF ANY): " + errorMsg);
			}
			request.getSession().setAttribute("errorMsg", errorMsg);
		}

		return redirectPath;
	}

	public String checkChangeEmailCurrentPwd(AccountUser userObj , String oldpassword, String oldEmailAddress, String newEmailAddress) throws InvalidUserException {
	        LOGGER.info("checkResetQuestionPasswordLoggedInUser started");
	       	String errorMsg = StringUtils.EMPTY;

			AccountUser accountUser;
			accountUser = userService.findByEmail(newEmailAddress);

			if (null != accountUser) {
				errorMsg = "Target email address entered already exists in the system. Please try again.";
				return errorMsg;
			}
	       	
	       	//Try authenticating on WSO2 using old password
	       	if(isScimEnabled){
				try {
					if(!scimUserManager.authenticate(userObj.getUsername(), oldpassword)){
						errorMsg = "The current password doesn't match with our records.".intern();
						return errorMsg;
					}
				} catch (GIException e) {
					if(e.getMessage().contains("17003".intern())){
					   GiAuditParameterUtil.add(new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_KEY, GiAuditor.SUCCESS),new GiAuditParameter(GiAuditor.STATUS_ATTRIBUTE_MESSAGE, "Account Locked for "+userObj.getUserName()));
					   return "accountlocked";
					}else{
					   LOGGER.info("Error in changing password on WSO2: ".intern() + e.getMessage());
					   errorMsg = e.getMessage();
	   				   return errorMsg;
					}
				}
			}else if(!userService.isPasswordSameInDatabase(userObj, oldpassword,userObj.getPassword())) {//HIX-32110
				//String errorMSg = "The current password doesn't match with our records.";
				//model.addAttribute("errorMsg", errorMSg);
				errorMsg = "The current password doesn't match with our records.";
				return errorMsg;
			}
			
			 
	       return errorMsg;
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/account/user/changeemail", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	@GiAudit(transactionName = "CHANGE EMAIL REQUEST",
	 eventType = EventTypeEnum.PII_WRITE,
	 eventName = EventNameEnum.EMAIL_CHANGE
	 )
	public String changeemail(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		String status = null;
		String oldEmailAddress = null;
		String newEmailAddress = null;
		String emailFromLoggedinUser = null;
		String currentPassword = null;
		Exception e = null;

		JSONObject jsonObject = null;
		JSONObject jsonResponse = null;
		JSONParser jsonParser = null;
		String jsonRequest = request.getParameter("jsonRequest");
		jsonRequest = jsonRequest.replace("&quot;", "\"");
		
		try {
			if ("TRUE".equalsIgnoreCase(DynamicPropertiesUtil.getPropertyValue(SecurityConfiguration.SecurityConfigurationEnum.IS_EMAIL_CHANGE_ENABLED))) {
				jsonParser = new JSONParser();
				jsonObject = (JSONObject) jsonParser.parse(jsonRequest);
				
				AccountUser userObj = userService.getLoggedInUser();
				
				emailFromLoggedinUser = userObj.getUserName();
				oldEmailAddress = ((String) jsonObject.get("oldEmailAddress")).toLowerCase();
				newEmailAddress = ((String) jsonObject.get("newEmailAddress")).toLowerCase();
				currentPassword = ((String) jsonObject.get("currpassword"));

				if(!emailFromLoggedinUser.equalsIgnoreCase(oldEmailAddress)){
					GiAuditParameterUtil.add(new GiAuditParameter("ATTEMPETED_HACK_BY", emailFromLoggedinUser));
					throw new GIRuntimeException("Invalid email change request, Received email address change request is different than logged in email address");
				}
				
				status = checkChangeEmailCurrentPwd(userObj, currentPassword, oldEmailAddress, newEmailAddress);
				
				if(StringUtils.isEmpty(status)) {
					if (LOGGER.isInfoEnabled()) {
						LOGGER.info("CHNG EMAIL: OLD EMAIL: " + oldEmailAddress
								+ " NEW EMAIL: " + newEmailAddress);
					}
	
					status = userChangeEmailManager.processEmailChangeRequest(
							oldEmailAddress, newEmailAddress);
				}
				else{
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				}
			} else {
				response.setStatus(HttpServletResponse.SC_FORBIDDEN);
				status = "Change email operation is not allowed";
			}
		} catch (Exception ex) {
			LOGGER.error("ERR: WHILE CHANGING EMAIL: ", ex);

			e = ex;
			request.getSession().setAttribute("error",
					"Oops!!! Something went wrong. Please try again later!!!");
			status = "Error in sending email to New Email addressd";
		} finally {
			jsonResponse = new JSONObject();
			jsonResponse.put("STATUS", status);

			if (null != e) {
				StringWriter errorStackTrace = new StringWriter();
				e.printStackTrace(new PrintWriter(errorStackTrace));
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				jsonResponse.put("EXCEPTION", errorStackTrace.toString());
			}
		}
		GiAuditParameterUtil.add(new GiAuditParameter("OLD EMAIL", oldEmailAddress),new GiAuditParameter("NEW EMAIL", newEmailAddress),
				new GiAuditParameter("USERNAME", emailFromLoggedinUser));

		return jsonResponse.toJSONString();
	}

		@RequestMapping(value = "/ping", method = RequestMethod.GET)
		@ResponseBody
		public int pingCheck(HttpServletRequest request, HttpServletResponse response) throws Exception{
			int pingStatus = 0;
			boolean isUserAuthenticated = false;
			String refererUri = null;
			String localModName = null;
			String switchToModuleName = null;

			Authentication auth = null;
			Object loggedInUser = null;

			try {
				refererUri = request.getHeader("Referer");
				auth = SecurityContextHolder.getContext()
						.getAuthentication();
				loggedInUser = (auth != null) ? auth.getPrincipal() : null;
				if (loggedInUser instanceof AccountUser) {
					isUserAuthenticated = true;
				}

				/*if (LOGGER.isDebugEnabled()) {
					LOGGER.debug("URI: " + requestURI + " REFERER URI: "
							+ refererUri + " URL PROTECTED: "+ghixHttpSecurityConfig
							.isUrlProtected(refererUri)+ " AUTH: " + isUserAuthenticated);
				}*/

				if ((refererUri != null) && !refererUri.equalsIgnoreCase(GhixConstants.APP_URL)
						&& ghixHttpSecurityConfig
								.isUrlProtected(refererUri)
						&& !isUserAuthenticated) {
					pingStatus = 1;
				}

				if(1 != pingStatus) {
					switchToModuleName = (String)request.getSession().getAttribute("switchToModuleName");
					localModName = request.getParameter("localmodname");
					if(!StringUtils.isEmpty(localModName) && !StringUtils.isEmpty(switchToModuleName) && !switchToModuleName.equalsIgnoreCase(localModName)) {
						if(LOGGER.isInfoEnabled()) {
							LOGGER.info("CURRENT MOD NAME: " + switchToModuleName
									+ "PREV MOD NAME: " + localModName);
						}
						pingStatus = 2;
					}
				}
			} catch (Exception ex) {
				LOGGER.error("ERR: WHILE CHCKNG PING STATUS: ",ex);
				throw ex;
			}

			return pingStatus;
		}
		
		@RequestMapping(value = "/accessCode", method = RequestMethod.GET)
		public String accessCodeLoginPage(Model model, HttpServletRequest request) throws InvalidUserException {

			return "accessCode";
		}
		
		/**
		 * HIX-47990<br><br>
		 *
		 * This method checks for WSO2 availability for SSO on the fly.
		 *
		 * Makes a REST call to ghix-identity service to check availability status
		 *
		 * @author - Nikhil Talreja
		 * @return - true if check was a success (HTTP call is successful)
		 * 			 false otherwise
		 */


	private class WSO2AvailabilityCall extends TimerTask {
		private boolean lastAvailable = true;
		private Thread wso2ProbeThread = null;
		public boolean isWsO2Available() {
			if(LOGGER.isInfoEnabled()) {
				LOGGER.info("IAM Availability check is set to {}",GhixPlatformConstants.WSO2_AVAILABILITY_CHECK_MILLIS);
			}
			if(GhixPlatformConstants.WSO2_AVAILABILITY_CHECK_MILLIS == -1){
				if(LOGGER.isInfoEnabled()) {
					LOGGER.info("IAM Availability check is disabled");
				}
				return true;
			}
			
			return this.lastAvailable;
		}

		@Override
		public void run() {
				wso2ProbeThread = Thread.currentThread();
				wso2ProbeThread.setName("WSO2 Availability Check["+Thread.currentThread().getId()+"]");
			try {
				// REST call to ghix-identity service
					Integer response = restTemplate.getForObject(
								GhixEndPoints.IdentityServiceEndPoints.WSO2_AVAILABILIITY_CHECK,
								Integer.class);
				// If REST call returns a response, it means success
				if (response != null) {
					LOGGER.debug("WSO2 is available now");
					lastAvailable = true;
				} else {
					LOGGER.debug("WSO2 is currently unavailable");
					lastAvailable = false;
				}
				}
			catch (Exception e) {
				LOGGER.error("Exception occured during WSO2 availability check", e);
				lastAvailable = false;
			}
		}
	}

}
