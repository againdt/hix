package com.getinsured.iex.ssap.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.iex.ssap.model.RenewalApplication;

@RestResource(path = "RenewalApplication")
public interface RenewalApplicationRepository extends JpaRepository<RenewalApplication, Long> {
	
	@Query("FROM RenewalApplication WHERE (healthRenewalStatus IN (:renewalStatus) and "
			+ " ssapApplicationId in (SELECT id from SsapApplication where applicationStatus in ('ER','PN') and applicationType in ('OE') and coverageYear = :coverageYear))"
			+ " or (dentalRenewalStatus IN (:renewalStatus) and "
			+ " ssapApplicationId in (SELECT id from SsapApplication where applicationDentalStatus is null and applicationType in ('OE') and coverageYear = :coverageYear))")
	List<RenewalApplication> getAutoRenewalApplicationsByCoverageYear( @Param("renewalStatus") List<RenewalStatus> renewalStatusList, @Param("coverageYear") Long coverageYear, Pageable pageable);
	
	@Query("SELECT id FROM RenewalApplication WHERE (healthRenewalStatus IN (:renewalStatus) or dentalRenewalStatus IN (:renewalStatus)) and "
			+ " ssapApplicationId in (SELECT id from SsapApplication where applicationStatus in ('ER','EN','PN') and applicationType in ('OE') and coverageYear = :coverageYear) ")
	List<Object> getAutoRenewalApplIdListByCoverageYear(@Param("renewalStatus") List<RenewalStatus> renewalStatusList, @Param("coverageYear") Long coverageYear, Pageable pageable);

	@Query("SELECT id FROM RenewalApplication WHERE (healthRenewalStatus IN (:renewalStatus) or dentalRenewalStatus IN (:renewalStatus)) and "
			+ " ssapApplicationId in (SELECT id from SsapApplication where applicationStatus in ('ER','EN','PN') and applicationType in ('OE') and coverageYear = :coverageYear) and MOD(ID, :serverCount)=:serverName ")
	List<Object> getAutoRenewalApplIdListByServerName(@Param("renewalStatus") List<RenewalStatus> renewalStatusList, @Param("coverageYear") Long coverageYear,  @Param("serverCount") int serverCount,  @Param("serverName") int serverName, Pageable pageable);

	@Query("FROM RenewalApplication WHERE id IN (:renewalApplIdList)")
	List<RenewalApplication> getAutoRenewalApplListByIds(@Param("renewalApplIdList") List<Long> renewalApplIdList);

	@Query("FROM RenewalApplication WHERE ssapApplicationId = :applicationId")
	RenewalApplication findBySsapApplicationId(@Param("applicationId") long applicationId);

	@Modifying
	@Transactional
	@Query(nativeQuery = true, value = "DELETE FROM RENEWAL_APPLICATIONS WHERE SSAP_APPLICATION_ID = :applicationId")
	void deleteBySsapApplicationId(@Param("applicationId") long applicationId);
	
	@Query("select ra from RenewalApplication ra where (ra.healthRenewalStatus in (:healthRenewalStatus) or ra.dentalRenewalStatus in (:dentalRenewalStatus)) and ra.noticeId is NULL")
	List<RenewalApplication> getRenewalApplicationList(@Param("healthRenewalStatus") List<RenewalStatus> healthRenewalStatus, @Param("dentalRenewalStatus") List<RenewalStatus> dentalRenewalStatus, Pageable pageable);

	@Modifying
	@Transactional
	@Query("UPDATE RenewalApplication SET healthRenewalStatus = :setRenewalStatus, "
			+ "healthReasonCode = :setReasonCode "
			+ "WHERE ssapApplicationId = :ssapApplicationId")
	int updateHealthApplicationRenewalStatusByApplId(@Param("setRenewalStatus") RenewalStatus setRenewalStatus,
			@Param("setReasonCode") Integer setReasonCode, @Param("ssapApplicationId") Long ssapApplicationId);

	@Modifying
	@Transactional
	@Query("UPDATE RenewalApplication SET dentalRenewalStatus = :setRenewalStatus, "
			+ "dentalReasonCode = :setReasonCode "
			+ "WHERE ssapApplicationId = :ssapApplicationId")
	int updateDentalApplicationRenewalStatusByApplId(@Param("setRenewalStatus") RenewalStatus setRenewalStatus,
			@Param("setReasonCode") Integer setReasonCode, @Param("ssapApplicationId") Long ssapApplicationId);

	@Modifying
	@Transactional
	@Query("UPDATE RenewalApplication SET healthRenewalStatus = :setRenewalStatus, dentalRenewalStatus = :setRenewalStatus, lastUpdateTimestamp = :lastUpdateTimestamp, "
			+ "healthReasonCode = :setReasonCode, dentalReasonCode = :setReasonCode "
			+ "WHERE ssapApplicationId = :ssapApplicationId")
	int updateRenewalStatusByApplicationId(@Param("setRenewalStatus") RenewalStatus setRenewalStatus,
			@Param("setReasonCode") Integer setReasonCode, @Param("ssapApplicationId") long ssapApplicationId,
			@Param("lastUpdateTimestamp") Timestamp lastUpdateTimestamp);
	
	@Query("select ra from RenewalApplication ra where (ra.healthRenewalStatus in (:healthRenewalStatus) or ra.dentalRenewalStatus in (:dentalRenewalStatus)) and ra.failureNoticeId is NULL")
	List<RenewalApplication> getRenewalApplicationListForFailureNotice(@Param("healthRenewalStatus") List<RenewalStatus> healthRenewalStatus, @Param("dentalRenewalStatus") List<RenewalStatus> dentalRenewalStatus, Pageable pageable);
	
	@Query("select ra from RenewalApplication ra where ra.healthRenewalStatus in (:healthRenewalStatus) and ra.dentalRenewalStatus in (:dentalRenewalStatus) and ra.noticeId is NULL")
	List<RenewalApplication> getRenewalApplicationListExceptMN(@Param("healthRenewalStatus") List<RenewalStatus> healthRenewalStatus, @Param("dentalRenewalStatus") List<RenewalStatus> dentalRenewalStatus, Pageable pageable);
}
