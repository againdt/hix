/**
 * 
 */
package com.getinsured.eligibility.prescreen.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.eligibility.prescreen.service.PrescreenPremiumService;
import com.getinsured.hix.model.estimator.mini.Member;
import com.getinsured.hix.model.prescreen.PrescreenPremiumData;
import com.knappsack.swagger4springweb.annotation.ApiExclude;

/**
 * This is the entry point for clients to get the Premium rates for Members
 * for various QHP Plans like Lowest Catastrophe, Lowest Bronze, Lowest Silver,
 * Second Lowest Silver, Lowest Gold Plans
 * 
 * @author bhatia_s
 * @since 27 Sep 2013
 */
@Controller
@RequestMapping("/eligibility")
public class PrescreenPremiumController {
	
	private static final Logger LOGGER = Logger.getLogger(PrescreenPremiumController.class);

	@Autowired private PrescreenPremiumService prescreenPremiumService;
	
	/**
	 * This REST invocation for calculating the premiums for various QHPs for members 
	 * @param memberList
	 * 			List of PhixMember objects where only the DOB will be populated for each member
	 * @param countyCode
	 * 			CountyCode will be used to find the Rating Area from CMS data
	 * @param zipCode
	 * 			ZipCode will be used to find the Rating Area from CMS data
	 * @param stateName
	 * 			State Name required to get the Multiplier factor from HashMap
	 * @return
	 */
	@ApiExclude
	@RequestMapping(value = "/calculatePremium", method = RequestMethod.POST)
	@ResponseBody
	public PrescreenPremiumData calculatePremium(@RequestBody List<Member> memberList, 
			String countyCode, String zipCode, String stateName){
		
		LOGGER.info("Invoking the Premium Calculation Service");
		PrescreenPremiumData prescreenPremiumData = prescreenPremiumService.calculatePremium(memberList, countyCode, zipCode, stateName);
		
		return prescreenPremiumData;
	}

}
