(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.Plans();
		var planModel = new App.models.Plan();
		App.views.plans = new App.views.Plans({collection:collection, planModel:planModel});
		new App.views.Pagination({collection:collection});
		new App.views.SortView({collection:collection});
	});

})();