import React, { Fragment } from 'react';
import _ from 'lodash';

import UniversalInput from '../../../../../lib/UniversalInput';
import DROPDOWN_OPTIONS from '../../../../../constants/dropdownOptions';

import INPUT_TYPES from '../../../../../constants/inputTypes';
import VALIDATION_TYPES from '../../../../../constants/validationTypes';
import { VALIDATION_ERROR_MESSAGES } from '../../../../../constants/validationErrorMessages';

const NamesOnSsnCardInputs = props => {

    const {
        firstNameOnSSNCard,
        middleNameOnSSNCard,
        lastNameOnSSNCard,
        suffixOnSSNCard,
        currentData,
        ...restProps
    } = props;

    const cleanedRestProps = _.omit(restProps, 'children');

    return (
        <Fragment>
            <UniversalInput
                label={{
                    labelClasses: [],
                    text: 'First Name',
                    isLabelRequired: true
                }}
                inputData={{
                    inputClasses: [],
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'firstNameOnSSNCard',
                    helperClasses: [],
                    value: firstNameOnSSNCard,
                    placeholder: 'Enter First Name',
                    validationType: VALIDATION_TYPES.NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.FIRST_NAME,
                    currentData: currentData,
                    fields: ['socialSecurityCard']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Middle Name',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'middleNameOnSSNCard',
                    helperClasses: [],
                    value: middleNameOnSSNCard,
                    placeholder: 'Enter Middle Name',
                    validationType: VALIDATION_TYPES.MIDDLE_NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.MIDDLE_NAME,
                    currentData: currentData,
                    fields: ['socialSecurityCard']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Last Name',
                    isLabelRequired: true
                }}
                inputData={{
                    isInputRequired: true,
                    type: INPUT_TYPES.REGULAR_INPUT,
                    name: 'lastNameOnSSNCard',
                    helperClasses: [],
                    value: lastNameOnSSNCard,
                    placeholder: 'Enter Last Name',
                    validationType: VALIDATION_TYPES.NAME,
                    validationErrorMessage: VALIDATION_ERROR_MESSAGES.LAST_NAME,
                    currentData: currentData,
                    fields: ['socialSecurityCard']
                }}
                {...cleanedRestProps}
            />
            <UniversalInput
                label={{
                    text: 'Suffix',
                    isLabelRequired: false
                }}
                inputData={{
                    isInputRequired: false,
                    type: INPUT_TYPES.DROPDOWN,
                    name: 'suffixOnSSNCard',
                    value: suffixOnSSNCard,
                    options: DROPDOWN_OPTIONS.SUFFIX,
                    currentData: currentData,
                    fields: ['socialSecurityCard']
                }}
                {...cleanedRestProps}
            />
        </Fragment>
    );
};

export default NamesOnSsnCardInputs;
