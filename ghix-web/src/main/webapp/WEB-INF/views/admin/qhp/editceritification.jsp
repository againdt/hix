<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js" /></script>
<style>
.accordion-border-none .accordion-group {
	border: 0 none;
}
</style>

<div class="gutter10">
<c:set var="encPlanId" ><encryptor:enc value="${planId}" isurl="true"/> </c:set>
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
                <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
                <li><a href="#"><spring:message code="label.plans"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/manageqhpplans"/>"><spring:message code="label.manageQHPS"/></a></li>
                <li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message code="label.linkPlanDetails"/></a></li>
                <li><spring:message code="label.certificationStatus"/></li>
            </ul><!--page-breadcrumb ends-->
			<h1><a class="skip"></a><img class="resize-img" src="<c:url value="/admin/issuer/company/profile/logo/hid/${plan.issuer.hiosIssuerId}"/>"/>${planName}</h1>
	</div>
	<div class="row-fluid">
		<div class="span3" id="sidebar">
       		<div class="header">
       			<h4 class="margin0"><spring:message code="label.aboutPlan"/></h4>
       		</div>
       		<!--  beginning of side bar -->
	            <ul class="nav nav-list graybg">
     				<li><a href="<c:url value="/admin/planmgmt/viewqhpdetail/${encPlanId}" />"><spring:message  code='label.linkPlanDetails'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpbenefits/${encPlanId}"/>"><spring:message  code='label.linkPlanBenefits'/></a></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhprates/${encPlanId}" />"><spring:message  code='label.linkPlanRates'/></a></li>
					<c:if test="${displayProviderNetwork == 'YES'}">
						    <li><a href="<c:url value="/admin/planmgmt/viewqhpprovidernetwork/${encPlanId}" />"><spring:message  code='label.linkProviderNetwork'/></a></li>
					</c:if>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhpenrollmentavail/${encPlanId}"/>"><spring:message  code='label.linkEnrollmentAvail'/></a></li>
                    <li class="active"><spring:message  code='label.linkCertificationStatus'/></li>
                    <li><a href="<c:url value="/admin/planmgmt/viewqhphistory/${encPlanId}"/>"><spring:message  code='label.linkPlanHistory'/></a></li>
			   </ul>
				<br /><br />
				<c:if test="${displayAction == 'YES'}">
					<h4 class="margin0"><spring:message code='label.action'/></h4>
					<ul class="nav nav-list graybg">
						<li><a href="<c:url value="/admin/planmgmt/editqhpdetail/${encPlanId}" />"><i class="icon icon-pencil"></i> <spring:message  code='label.linkEditPlanDetails'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpbenefits/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanBenefits'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhprates/${encPlanId}" />"><i class="icon icon-arrow-up"></i> <spring:message  code='label.linkEditPlanRates'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpprovidernetwork/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditProviderNetwork'/></a></li>
						<li><a href="<c:url value="/admin/planmgmt/editqhpenrollmentavail/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditEnrollmentAvail'/></a></li>
						<li class="active"><a href="<c:url value="/admin/planmgmt/editqhpcertification/${encPlanId}" />"><i class="icon icon-folder-open"></i> <spring:message  code='label.linkEditCertificationStatus'/></a></li>
					</ul>
				</c:if>
			
        </div><!-- end of span3 -->
        
		<div class="span9" id="rightpanel">
			
		<div class="header">
			<h4 class="span8"><spring:message code="label.certificationStatus"/></h4>			
			<c:if test="${currentStatus != 'DECERTIFIED'}">
				<a class="btn btn-small pull-right" title="<spring:message  code="label.btnCancel"/>" href='<c:url value="/admin/planmgmt/viewqhpcertification/${encPlanId}"></c:url>'><spring:message  code="label.btnCancel"/></a> 
				<c:if test="${showSaveButton == true}">
					<a class="btn btn-primary btn-small pull-right" id="updateButton" title="<spring:message  code="label.btnSave"/>" href="#"  onclick="formSubmit();"><spring:message  code="label.btnSave"/></a>
				</c:if>
			</c:if>
		</div>
		 <form class="form-horizontal" id="frmUpdtCert" name="frmUpdtCert" action='<c:url value="/admin/planmgmt/updateqhpcertification"/>' method="post">
	        <fmt:formatDate pattern="MM/dd/yyyy" value="${plan.deCertificationEffDate}" var="deCertiEffDate"/>
	       	<fmt:formatDate pattern="MM/dd/yyyy" value="${sysDate}" var="systemDate" /> 			
			<input type="hidden" id="planId" name="planId" value="<encryptor:enc value="${planId}"></encryptor:enc>"/>
			<input type="hidden" id="issuerStatus" name="issuerStatus" value="${issuerStatus}"/>
			<input type="hidden" id="id" name="id" value="<encryptor:enc value="${plan.id}"></encryptor:enc>"/>
			
			<input type="hidden" id="currentEnrollmentAvailability" name="currentEnrollmentAvailability" value="${plan.enrollmentAvail}"/>
			<c:set var="encVarPlanId"><encryptor:enc value="${plan.id}" isurl="false"></encryptor:enc></c:set>
			
			<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.startDate}" var="stDate"/>
			<fmt:formatDate pattern="MM/dd/yyyy" value="${plan.endDate}" var="enDate"/>
				<df:csrfToken/>
				<table class="table table-border-none">
	               <tbody>
	                   <tr>
	                       <td class="span4 txt-right"><spring:message  code='label.issuerName'/></td>
	                       <td><strong>${issuerName}</strong></td>
	                   </tr>
	                   <tr>
	                       <td class="txt-right"><spring:message  code="label.planName"/></td>
	                       <td><strong>${planName}</strong></td>
	                   </tr>
	                   <tr>
	                       <td class="txt-right"><spring:message  code="label.planNumber"/></td>
	                       <td><strong>${planNumber}</strong></td>
	                   </tr>
			   			<tr>
	                   		 <td class="txt-right"><spring:message  code="label.planStartDate"/></td>
	                   		 <td><strong>${stDate}</strong></td>
	                   </tr>
	                   <tr>
	                   		<td class="txt-right"><spring:message  code="label.planEndDate"/></td>
	                   		<td><strong>${enDate}</strong></td>
	                   </tr>
	                   <tr>
	                       <td class="txt-right"><spring:message  code="label.currentStatus"/></td>
	                       <td><strong><c:choose><c:when test="${currentStatus == 'DECERTIFIED'}">De-Certified</c:when><c:otherwise>${currentStatus}</c:otherwise></c:choose>
	                       <c:if test="${currentStatus != 'DECERTIFIED' && plan.deCertificationEffDate != null && (terminateEnrollmentStatus != 'SUCCESS')}"><font color="red">*</font></c:if></strong>
	                   </tr>
	                   
	                   <c:if test="${plan.deCertificationEffDate != null}">
							<tr>
								<td class="txt-right"></td>
								<td>
									<div id="undoDecertification_display">
										<font color="red">*</font>
										<spring:message code="label.planDecertificationInfo" />
										<c:out value="${deCertiEffDate}" />
										<c:if test="${currDate <= enrollmentEndDate && (terminateEnrollmentStatus != 'SUCCESS')}">
											<a class="btn btn-primary btn-small pull-right" 
												title="<spring:message  code="label.undoDecertification"/>" href="#"
												onclick="undoDecertification();" style="background-color: #4DB6CB;"><spring:message
													code="label.undoDecertification" /></a>
											<br/>&nbsp;<br/>
											
											<c:if test="${enrollmentCountForPlanId > 0  && showTerminateEnrollment == 'ON'}">
												<a class="btn btn-primary btn-small pull-right" 
												title="<spring:message  code="label.terminateEnrollments"/>" href="#"
												onclick="terminateEnrollments();" style="background-color: red;"><spring:message
													code="label.terminateEnrollments" /></a>
											</c:if>
										</c:if>
									</div>
								</td>
								<td class="txt-right"></td>
							</tr>
						</c:if>
						
						<c:if test="${currentStatus != 'DECERTIFIED'}">
							<tr>
		                       <td class="txt-right"><label for="status"><spring:message  code="label.certifictionStatus"/></label></td>
		                       <td>
		                            <select id="status" name="status" onchange="displayDates(this.value);">
		                                 <option value="">-- <spring:message  code="label.selectUpperCase"/> --</option>
		                                <c:forEach var="qhpcertiStatus" items="${qhpcertiStatusList}">
								    		<option value="${qhpcertiStatus.key}">${qhpcertiStatus.value}</option>
										</c:forEach>
		                            </select>
		                            <div id="status_error" class="error help-block"></div>
		                            
		                       </td>
		                   </tr>
		                   <tr id="deCertificationEffDateId">
		                       <td class="txt-right"><label for="deCertificationEffDateUI"><spring:message code="label.deCertiEffectiveDate"/></label></td>
		                       <td>
									<div class="input-append date date-picker" id="deCertificationEffDateUIDate" data-date="${systemDate}" data-date-format="mm/dd/yyyy">
										<input class="span10" type="text" name="deCertificationEffDateUI" id="deCertificationEffDateUI" value="" pattern="MM/DD/YYYY" onChange="setdate();"/> 
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>
									<div id="deCertificationEffDateUI_error" class="help-inline"></div>
		                       </td>
		                   </tr>
		                   <tr id="enrollmentEndDateId">
		                       <td class="txt-right"><label for="enrollmentEndDateUI"><spring:message code="label.enrollmentEndDate"/></label></td>
		                       <td>
		                       		<%-- <fmt:formatDate pattern="MM/dd/yyyy" value="${plan.enrollmentEndDate}" var="enrollmtEndDate"/> --%>
									<div class="input-append date date-picker" id="enrollmentEndDateUIDate" data-date="${plan.enrollmentEndDate}" data-date-format="mm/dd/yyyy">
										<input class="span10" type="text" name="enrollmentEndDateUI" id="enrollmentEndDateUI" value="" pattern="MM/DD/YYYY" onChange="setdate();" readonly="readonly"/> 
										<span class="add-on"><i class="icon-calendar"></i></span>
									</div>
								
									<div id="enrollmentEndDateUI_error" class="error help-block"></div>
									<c:if test="${(plan.enrollmentAvail == 'AVAILABLE' || plan.enrollmentAvail == 'DEPENDENTSONLY')}">
										<label for="decertificationUI_error" class="error" aria-live="assertive" role="alert" id="decertificationUI_error_label">
								    	<span><em class='excl'>!</em><spring:message  code='label.decertificationError1'/></span>
									    	<c:if test="${plan.enrollmentAvail == 'AVAILABLE'}">
									    		"<spring:message  code='label.available'/>"
									    	</c:if>
									    	<c:if test="${plan.enrollmentAvail == 'DEPENDENTSONLY'}">
									    		"<spring:message  code='label.dependentsOnly'/>"
									    	</c:if>
									    <span><spring:message  code='label.decertificationError2'/></span></label>
									</c:if>
		                       </td>
		                   </tr>
		                   <c:if test="${exchangeType == 'PHIX'}">
		                   <tr>
		                     <td class="txt-right"><label for="verification"><spring:message  code="label.issuerVerification"/></label></td>
		                       <td>
		                            <select id="issuerVerificationStatus" name="issuerVerificationStatus">	                                
		                                <c:forEach var="qhpIssuerVerification" items="${qhpIssuerVerificationList}">
								    		<option value="${qhpIssuerVerification.key}">${qhpIssuerVerification.value}</option>
										</c:forEach>
		                            </select>
		                            <div id="status_error" class="error help-block"></div>
		                       
		                       </td>
		                   </tr>
		                   </c:if>	                   
		                   <tr>
		                       <td class="txt-right"><label for="comments"><spring:message  code="label.comment"/></label></td>
		                       
		                       <td>
		                        <textarea class="input-xlarge" name="comments" id="comments" rows="4" cols="40" style="resize: none;" 
								maxlength="500" spellcheck="true" onkeyup="updateCharCount();" onchange="updateCharCount();" onKeyPress="return(this.value.length < 500 );"></textarea>	                       
		                       		<div id="chars_left"><spring:message  code="label.charLeft"/> <b><spring:message code='label.charLeftValue'/></b></div>
		                       </td>
		                   </tr>
		                   <tr>
			                   	<td class="txt-right"><label for="certSuppDoc"><spring:message code='label.uploadSuppDoc'/></label></td>
			                   	<td>
			                   		<input type="file" class="input-file" id="certSuppDoc" name="certSuppDoc">
			                   		<button type="submit" id="btnCertSuppDoc" name="btnCertSuppDoc" class="btn" ><spring:message code='label.upload'/></button>
			                   	    <div id="certSuppDoc_display">${certiSuppDoc}</div>
			                   	    <input type="hidden" id="hdnCertSuppDoc" name="hdnCertSuppDoc" value=""/>
			                   	</td>
		                   </tr>
						</c:if>	                  
	                    <tr><td><input type="hidden" name = "check_CurrentStatus" id = "check_CurrentStatus" value="${currentStatus}"/></td></tr>
	               </tbody>
	           </table>
           </form>
			
		</div><!-- end of .span9 -->
	</div>
</div>

<c:if test="${(decertificationError == 'true')}">
<div id="dialog-modal" style="display: none;">
  	<p>
	  	<c:if test="${plan.enrollmentAvail == 'AVAILABLE' || plan.enrollmentAvail == 'DEPENDENTSONLY'}">
	    	<spring:message  code='label.decertificationError1'/>
		    	<c:if test="${plan.enrollmentAvail == 'AVAILABLE'}">
		    		<b><spring:message  code='label.available'/></b>
		    	</c:if>
		    	<c:if test="${plan.enrollmentAvail == 'DEPENDENTSONLY'}">
		    		<b><spring:message  code='label.dependentsOnly'/></b>
		    	</c:if>
		    <spring:message  code='label.decertificationError2'/>
		</c:if>
	</p>
</div>

<script type="text/javascript">
$( "#dialog-modal" ).dialog({
    modal: true,
	title: "De-Certification Error",
	buttons: {
	   Ok: function() {
	    $( this ).dialog( "close" );
	   }
	}
});
</script>
</c:if>

<div id="dialog-confirm" title="Terminate Enrollment Confirmation" style="display: none;">
  <p id="confirmationText"></p>
</div>

<div id="terminationInProcessDialog" title="Enrollment Termination in process" style="display: none;">
  <p id="processRunningText"></p>
</div>

<script type="text/javascript">
var csrValue = $("#csrftoken").val();
	var commentMaxLen = 500;
	var planStatus = '${currentStatus}';
	
	$(document).ready(function(){
		displayDates(planStatus);
		$('.complete').each(function(){
			var completeStep = $(this).html();
			var replaceExpr = /html"\>/gi;
			$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
		});
		/* $('.datepick').each(function() {
			var ctx = "${pageContext.request.contextPath}";
			var imgpath = ctx+'/resources/images/calendar.gif';
			$(this).datepicker({
				showOn : "button",
				buttonImage : imgpath,
				buttonImageOnly : true,
				minDate: 0
			});
		}); */
		
		
		$('#deCertificationEffDateUIDate').datepicker({
		    startDate: '+' + '${systemDate}' + 'd',
		    autoclose: true,
		 	format: 'mm/dd/yyyy',
		 	forceParse: false
		});
		
		$('#enrollmentEndDateUIDate').datepicker({
		    startDate: '+' + '${enrollmentEndSysDate}' + 'd',
		    autoclose: true,
		 	format: 'mm/dd/yyyy',
		 	forceParse: false,
		 	beforeShowDay: unavailable
		});
	})
	
	function unavailable(date) {
	  dmy = date.getDate() + "-" + (date.getMonth()+1) + "-" + date.getFullYear();
	  var currentMonth = (date.getMonth()+1);
	  if(currentMonth == 2 && checkLeapYear(date.getFullYear())){
		  if(date.getDate() == 29){
			  return true;
		  }else{
			  return false;
		  }
			  
	  }
	  if(currentMonth == 2 && !(checkLeapYear(date.getFullYear()))){
		  if(date.getDate() == 28){
			  return true;
		  }else{
			  return false;
		  }
	  }
	  
	  if(currentMonth != 2 && (currentMonth == 4 || currentMonth == 6 || currentMonth == 9 || currentMonth == 11)){
		  if(date.getDate() == 30){
			  return true;
		  }else{
			  return false;
		  }
	  }else if(currentMonth != 2 && (currentMonth == 1 || currentMonth == 3 || currentMonth == 5 || currentMonth == 7 || currentMonth == 8 || currentMonth == 10 || currentMonth == 12)){
		  if(date.getDate() == 31){
			  return true;
		  }else{
			  return false;
		  }
	  }
	  
	}

	function checkLeapYear(year){
	  return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
	}

	$(document).ready(function(){
	  $("#btnCertSuppDoc").click(function(){
			$('#frmUpdtCert').ajaxSubmit({
				url: "<c:url value='/admin/planmgmt/uploadcertificationdoc'/>",
				data: {"planId": "${planId}","csrftoken":csrValue},
				success: function(responseText){
					if(responseText.value == 'undefined'){
						return false;
					}
				    var val = responseText.split("|");
					$("#certSuppDoc_display").text(val[1]); // show original file name
					$("#hdnCertSuppDoc").val(val[2]); // store modified file name
		       	},error: function (responseText){
		       		var parsedJSON = responseText;
		       		var val = parsedJSON.responseText.split("|");
				     $("#certSuppDoc_display").text(val[1]);
					 $("#hdnCertSuppDoc").val(val[2]);
		       	}
			});
		  return false; 
		});	// button click close
		
		
		$('.complete').each(function(){
			var completeStep = $(this).html();
			var replaceExpr = /html"\>/gi;
			$(this).html(completeStep.replace(replaceExpr,'html"><i class="icon icon-ok"></i> '));
		});
	});
	
	function displayDates(selectedIdValue) {
		var currentStatus = $('#check_CurrentStatus').val();
		var currentCertifiationStatus = $("#status").val();
		var currentEnrollmentAvailability = '${plan.enrollmentAvail}';
	   if(currentStatus != 'DECERTIFIED' && selectedIdValue == 'DECERTIFIED'){
		   $("#deCertificationEffDateId").show();
		   $("#enrollmentEndDateId").show();
	   }else{
		   $("#deCertificationEffDateId").hide();
		   $("#enrollmentEndDateId").hide();
	   }
	   
	   if((currentEnrollmentAvailability == "AVAILABLE" || currentEnrollmentAvailability == "DEPENDENTSONLY") && (currentCertifiationStatus == "DECERTIFIED")){
		   $('#updateButton').prop('disabled', true);
	   }else{
		   $('#updateButton').prop('disabled', false);
	   }
	}
	
	$.validator.addMethod("greaterThanToday", function(value, element) {
		var today = '${currDate}';
		if(Date.parse(value) > Date.parse(today)){
			return true;
		}
		return false;
	});
	
	$.validator.addMethod("lessThanDeCertificationDate", function(value, element) {
		var decrtificationDate = $("#deCertificationEffDateUI").val();
		if(Date.parse(value) < Date.parse(decrtificationDate)){
			return true;
		}
		return false;
	});
	
	var validator = $("#frmUpdtCert").validate({
		rules : {
			status : { required : true},
			deCertificationEffDateUI : { required : true, greaterThanToday : true },
			enrollmentEndDateUI : { required : true, greaterThanToday : true, lessThanDeCertificationDate : true}
		}, 
		messages : {
			status : { required : "<span><em class='excl'>!</em><spring:message  code='err.selectCertStatus' javaScriptEscape='true'/></span>"},
			deCertificationEffDateUI : { required : "<span><em class='excl'>!</em><spring:message  code='err.deCertEffdate' javaScriptEscape='true'/></span>",
				greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='error.furtueDeCertDate'/></span>"},
				enrollmentEndDateUI : { required : "<span><em class='excl'>!</em><spring:message code='err.enrollmentEndDate' javaScriptEscape='true'/></span>",
				greaterThanToday : "<span><em class='excl'>!</em><spring:message  code='error.furtueDeCertDate' javaScriptEscape='true'/></span>",
				lessThanDeCertificationDate : "<span><em class='excl'>!</em><spring:message  code='error.lessEnrollmentEndate' javaScriptEscape='true'/></span>"}	
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');			
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-block');
		} ,
		highlight: function(element) {
			$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        }
	});
	
	function formSubmit(){
		//var currentEnrollmentAvailability = '${plan.enrollmentAvail}';
		var currentCertifiationStatus = $("#status").val();
		//var terminateEnrollmentFlag = '${terminateEnrollmentStatus}';
		/* alert(terminateEnrollmentFlag);
		alert(currentCertifiationStatus); */
		//var deCertificationEffDate = '${plan.deCertificationEffDate}';
		if(currentCertifiationStatus == ''){
			$('#status_error').html('<label id="status_error_label" class="error" for="status" generated="true" aria-live="assertive" role="alert"><span><em class="excl">!</em>Please select Certification Status.</span></label>');
			return false;
		}else{
			/* if((currentEnrollmentAvailability == "NOTAVAILABLE") && (terminateEnrollmentFlag == '' || terminateEnrollmentFlag != 'SUCCESS')
					&& (currentCertifiationStatus == "INCOMPLETE" || currentCertifiationStatus == "DECERTIFIED") && deCertificationEffDate == ''){
				submitUpdateCertificationForm();
			}else if((currentEnrollmentAvailability != "NOTAVAILABLE") && (currentCertifiationStatus == "INCOMPLETE" || currentCertifiationStatus == "CERTIFIED")
										&& (terminateEnrollmentFlag == '' || terminateEnrollmentFlag != 'SUCCESS')){
				submitUpdateCertificationForm();
			} */
			
			submitUpdateCertificationForm();
		}
	}
	
	function submitUpdateCertificationForm(){
		if($("#status").val() != 'DECERTIFIED'){
			$("#deCertificationEffDateUI").val("");
			$("#enrollmentEndDateUI").val("");
		} 
		if(($("#issuerStatus").val() != 'CERTIFIED' && $("#issuerStatus").val() != 'RECERTIFIED')  && ( $("#status").val() == 'CERTIFIED' ||  $("#status").val() == 'RECERTIFIED') ){
			alert("<spring:message code='label.alert'/>");
			return false;
		}else{
			$("#frmUpdtCert").submit();
		}
	}
	
	function undoDecertification(){
		if(confirm("<spring:message code='confirm.undoDecertification'/>")){
		$('#frmUpdtCert').ajaxSubmit({
			url: "<c:url value='/admin/planmgmt/undoDecertification'/>",
			dataType: "json",
			type : "POST",
			data: {"csrftoken":csrValue},
			complete: function(xhr, textStatus){
				if(xhr.status == 200) {					
					window.location.href="<c:url value='/admin/planmgmt/viewqhpcertification/${encPlanId}'/>";
				} else {					
					var parsedJSON = responseText;
					var val = parsedJSON.responseText;
					$("#undoDecertification_display").text(val);
				}
			}
		
		});
	  }
	}
	
	function terminateEnrollments(){
		$('#dialog-confirm').show();
		$('#confirmationText').html("<spring:message code='label.termindateConfirmationAlert1'/>");
		$(document).ready(function(){
			$(function() {
				  $("#dialog-confirm").dialog({
				      resizable: false,
				      height:175,
				      modal: true,
				      buttons: {
				        "OK": function() {
				        	terminateEnrollmentsFinalAction();
				        },
				        Cancel: function() {
				          $( this ).dialog( "close" );
				        }
				      }
				});
			});
		});
	}
		
	function terminateEnrollmentsFinalAction(){
		$('#confirmationText').html("<spring:message code='label.termindateConfirmationAlert2'/>");
		$("#dialog-confirm").dialog({
		      resizable: false,
		      height:200,
		      modal: true,
		      closeOnEscape: false,
		      buttons: {
		        "Yes": function() {
		        	$(this).dialog( "close" );
		        	waitForEnrollmentTermination();
		        },
		        Cancel: function() {
		          $(this).dialog( "close" );
		        }
		      }
		});
	}
	
	function waitForEnrollmentTermination(){
		$("#terminationInProcessDialog").show();
		$('#processRunningText').html("<spring:message code='label.termindateInProcessAlert'/>");
		$("#terminationInProcessDialog").dialog({
		      resizable: false,
		      height:140,
		      open: function(event, ui) { $(".ui-dialog-titlebar-close", ui.dialog | ui).hide(); },
		      closeOnEscape: false,
		      modal: true
		});
		SubmittingTerminateEnrollmentsCall();
	}
	
	function SubmittingTerminateEnrollmentsCall(){
		var planId = '${encPlanId}';
		var url = $(location).attr('href');
		$.ajax({
            type: "POST",
            url: "<c:url value='/admin/planmgmt/qhp/terminateEnrollment'/>",
            data: {"planId": planId, "csrftoken": $("#csrftoken").val()},
            success: function(data) {
            	$("#terminationInProcessDialog").dialog("close");
            	$("#dialog-confirm").dialog({
      		      resizable: false,
      		      height:165,
      		      modal: true,
      		      closeOnEscape: false,
      		      buttons: {
      		        "OK": function() {
      		        	$(this).dialog( "close" );
	      		  		var actualUrl = url.replace('#' ,'');
	      				$(location).attr('href', actualUrl);

      		        }
      		      }
      			});
            	if('SUCCESS' == data){
            		$("#dialog-confirm").show();
            		$('#confirmationText').html("<spring:message code='label.termindateSuccessAlert'/>");
            	}else{
            		$("#dialog-confirm").show();
            		$('#confirmationText').html("<spring:message code='label.termindateFailureAlert'/>");
            	}
            }
        });
	}
	
	function updateCharCount(){	
		var currentLen = $.trim(document.getElementById("comments").value).length;
		var charLeft = commentMaxLen - currentLen;
		if(currentLen > commentMaxLen) {
			$("#comments").val($("#comments").val().substr(0, commentMaxLen));
			$('#chars_left').html('Characters left <b>' + 0 + '</b>' );
	    }
		$('#chars_left').html("<spring:message code='label.charactersLeft'/> <b>" + charLeft + " </b>");
	}

var validator = $("#frmUpdtCert").validate({
		rules : {
			status : { required : true}
		}, 
		messages : {
			status : { required : "<span><em class='excl'>!</em><spring:message  code='err.selectCertStatus' javaScriptEscape='true'/></span>"}
		},
		errorClass: "error",
		errorPlacement: function(error, element) {
			var elementId = element.attr('id');			
			error.appendTo( $("#" + elementId + "_error"));
			$("#" + elementId + "_error").attr('class','error help-block');
		} 
	});
</script>

