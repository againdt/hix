package com.getinsured.iex.hub.ridp.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.SecondaryRequestInformationType;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationAnswerSetType;

public class RIDPSecondaryRequestWrapper implements JSONAware {

	private static ObjectFactory factory = null;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPSecondaryRequestWrapper.class);

	private VerificationAnswerSetType verificationAnswerSetType = null;
	private SecondaryRequestInformationType secondaryRequest;
	private List<VerificationAnswerAndQuestionNumberWrapper> answers;

	private String sessionIdentification;

	public RIDPSecondaryRequestWrapper() {
		factory = new ObjectFactory();
		this.secondaryRequest = factory.createSecondaryRequestInformationType();
	}

	public static RIDPSecondaryRequestWrapper fromJSON(String secondaryJSONRequest) throws HubServiceException {
		VerificationAnswerAndQuestionNumberWrapper QnA;
		RIDPSecondaryRequestWrapper wrapper = new RIDPSecondaryRequestWrapper();
		JSONObject secondaryRequestObject = null;
		try {
			secondaryRequestObject = (JSONObject) new JSONParser().parse(secondaryJSONRequest);
		} catch (ParseException parserException) {
			LOGGER.error("Unable to parse secondary JSON request : " + secondaryJSONRequest, parserException);
			throw new HubServiceException("Malformed input:Input received does not conforms to agreed JSON format");
		}

		JSONArray verificationResponses = (JSONArray) secondaryRequestObject.get("verificationResponses");
		String sessionIdentifier = (String) secondaryRequestObject.get("sessionIdentifier");

		if (verificationResponses == null || verificationResponses.isEmpty()) {
			throw new HubServiceException("Invalid JSON for RIDP secondary : "+ secondaryJSONRequest);
		}

		int len = verificationResponses.size();
		JSONObject verificationResponse;
		for (int i = 0; i < len; i++) {
			verificationResponse = (JSONObject) verificationResponses.get(i);
			String questionNumber = verificationResponse.get("questionNumber").toString();
			if (questionNumber == null) {
				throw new HubServiceException("Invalid request:No question number found");
			}

			String choiceIndex = verificationResponse.get("choiceIndex").toString();
			if (choiceIndex == null) {
				throw new HubServiceException("Invalid request, at least one selection is required");
			}
			QnA = new VerificationAnswerAndQuestionNumberWrapper();
			QnA.setAnswerChoice(choiceIndex);
			QnA.setQuestionNumber(questionNumber);
			wrapper.addAnswer(QnA);
		}
		wrapper.setSessionIdentifaction(sessionIdentifier);
		return wrapper;
	}

	public void addAnswer(VerificationAnswerAndQuestionNumberWrapper answer) {
		if (this.answers == null) {
			this.answers = new ArrayList<VerificationAnswerAndQuestionNumberWrapper>();
		}
		this.answers.add(answer);
	}

	public void setSessionIdentifaction(String sessionIdentification) {
		this.sessionIdentification = sessionIdentification;
		this.secondaryRequest
				.setSessionIdentification(this.sessionIdentification);
	}

	public SecondaryRequestInformationType getSystemRepresentation() {
		if (this.verificationAnswerSetType == null) {
			verificationAnswerSetType = factory.createVerificationAnswerSetType();
		}
		this.secondaryRequest.setVerificationAnswerSet(verificationAnswerSetType);
		if (this.answers != null && !this.answers.isEmpty()) {
			for (VerificationAnswerAndQuestionNumberWrapper ansWrapper : this.answers) {
				this.verificationAnswerSetType.getVerificationAnswers().add(ansWrapper.getSystemRepresentation());
			}
			this.secondaryRequest.setVerificationAnswerSet(verificationAnswerSetType);
		}
		return this.secondaryRequest;
	}

	@SuppressWarnings("unchecked")
	@Override
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("sessionIdentification", this.sessionIdentification);
		JSONArray array;
		if (this.answers != null && !this.answers.isEmpty()) {
			array = new JSONArray();
			for (VerificationAnswerAndQuestionNumberWrapper ansWrapper : this.answers) {
				array.add(ansWrapper);	
			}
			obj.put("answers", array);
		}
		return obj.toJSONString();
	}
}
