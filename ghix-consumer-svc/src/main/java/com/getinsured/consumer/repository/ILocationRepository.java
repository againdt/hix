package com.getinsured.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Location;

public interface ILocationRepository extends JpaRepository<Location, Integer> {
	
}
