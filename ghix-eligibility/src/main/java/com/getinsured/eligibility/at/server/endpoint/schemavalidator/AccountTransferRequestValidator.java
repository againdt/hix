package com.getinsured.eligibility.at.server.endpoint.schemavalidator;

import java.io.IOException;
import java.net.URL;
import java.util.Collections;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.util.JAXBSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;

public final class AccountTransferRequestValidator {

	private static final String ERRORS_IN_RECEIVED_AT_REQUEST_XML = "Errors in received AT Request XML - ";
	private static final String ERROR_PERFORMING_VALIDATION_USING_JAXB_FOR_ACCOUNT_TRANSFER = "Error performing validation using JAXB for account transfer";
	private static final String PERFORMING_VALIDATION_USING_JAXB = "performing validation using JAXB ...";
	private static final String INSIDE_ACCOUNT_TRANSFER_DATA_VALIDATION_HANDLER = "inside Account Transfer Data Validation Handler ...";
	private static final String ERROR_LOADING_AND_CREATING_JAXB_XSD_VALIDATOR_FOR_ACCOUNT_TRANSFER = "Error loading and creating JAXB XSD validator for account transfer";

	private static final Logger LOGGER = Logger.getLogger(AccountTransferRequestValidator.class);

	private static JAXBContext jc;
	private static Schema schema;

	static{
		try {
			SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			URL schemaURL = new URL(GhixEndPoints.ELIGIBILITY_URL + "endpoints/AccountTransfer/wsdl/AT_2.4/XMLSchemas/constraint/exchange/ExchangeModel.xsd");
			jc = JAXBContext.newInstance(AccountTransferRequestPayloadType.class);
			schema = sf.newSchema(schemaURL);
		} catch (JAXBException | SAXException | IOException e) {
			LOGGER.error(ERROR_LOADING_AND_CREATING_JAXB_XSD_VALIDATOR_FOR_ACCOUNT_TRANSFER, e);
		}

	}

	private AccountTransferRequestValidator(){}

	public static List<String> validate(AccountTransferRequestPayloadType request) {

		LOGGER.info(INSIDE_ACCOUNT_TRANSFER_DATA_VALIDATION_HANDLER);
		LOGGER.info(PERFORMING_VALIDATION_USING_JAXB);

		List<String> errorList = Collections.emptyList();

		try{
			JAXBSource source = new JAXBSource(jc, request);
	        Validator validator = schema.newValidator();

			AccountTransferRequestErrorHandler myErrorHandler = new AccountTransferRequestErrorHandler();
			validator.setErrorHandler(myErrorHandler);
			validator.validate(source);

			errorList = myErrorHandler.getErrors();

		} catch (JAXBException | SAXException | IOException e){
			LOGGER.error(ERROR_PERFORMING_VALIDATION_USING_JAXB_FOR_ACCOUNT_TRANSFER, e);
		}

		if (LOGGER.isDebugEnabled()){
			LOGGER.debug(ERRORS_IN_RECEIVED_AT_REQUEST_XML + errorList);
		}

		return errorList;
	}

}
