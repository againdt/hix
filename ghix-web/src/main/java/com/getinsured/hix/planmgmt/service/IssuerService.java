package com.getinsured.hix.planmgmt.service;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.ui.Model;
import org.springframework.web.multipart.MultipartFile;

import com.getinsured.hix.dto.plandisplay.DrugDataResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.ExchgPartnerLookup;
import com.getinsured.hix.model.Issuer;
import com.getinsured.hix.model.Issuer.certification_status;
import com.getinsured.hix.model.IssuerRepresentative;
import com.getinsured.hix.model.IssuerRepresentative.PrimaryContact;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.util.exception.GIException;

public interface IssuerService {

	static enum DocumentType {
		AUTHORITY("Authority"),
		MARKETING("Marketing"),
		DISCLOSURES("Disclosures"),
		ACCREDITATION("Accreditation"),
		ADDITIONAL_INFO("Additional_info"),
		ADDITIONAL_SUPPORT("Additional_support"),
		ORGANIZATION("organization"),
		PAYMENT_POLICIES_PRACTICES("Payment_policies_practices"),
		ENROLLMENT_DISENROLLMENT("Enrollment_Disenrollment"),
		RATING_PRACTICES("Rating_practices"),
		FINANCIAL_DISCLOSURE("Financial_Disclosure"),
		CLAIMS_PAY_POL("Claims Payment Policies And Practices"),
		COST_SHARING_PAYMENTS("Cost_sharing_payments");

		private final String name;

		private DocumentType(String name) {
			this.name = name;
		}

		@Override
		public String toString() {
			return name;
		}
	}

	public static enum RepStatus {
		ACTIVE("Active"), INACTIVE("Inactive"), SUSPENDED("Suspended");

		private final String reprName;

		private RepStatus(String issuerReprName) {
			this.reprName = issuerReprName;
		}

		@Override
		public String toString() {
			return reprName;
		}

	}

	String UNDERSCORE = "_";
	String DOT = ".";
	// String BACKWARD_SLASH = "\\";

	String CERTI_FOLDER = "certificates";
	String BENEFITS_FOLDER = "benefits";
	String RATES_FOLDER = "rates";
	String AUTHORITY_FOLDER = "authority";
	String DISCLOSURES_FOLDER = "disclosures";
	String MARKETING_FOLDER = "marketing";
	String ACCREDITATION_FOLDER = "accreditation";
	String ADD_INFO_FOLDER = "additional_info";
	String BROCHURE_FOLDER = "brochures";
	String PLAN_SUPPORT_DOC_FOLDER = "plan_support";
	String ADD_SUPPORT_FOLDER = "support";
	String ORGANIZATION_FOLDER = "organization";
	String PAYMENT_POLICIES_PRACTICES_FOLDER = "pay_pol_pract";
	String ENROLLMENT_DISENROLLMENT_FOLDER = "rnrollment_disenrollment";
	String RATING_PRACTICES_FOLDER = "rating_practices";
	String COST_SHARING_PAYMENTS_FOLDER = "Cost_sharing_payments";
	String CERT_STATUS_WHEN_REGISTERED = certification_status.REGISTERED.toString();
	String CERT_STATUS_AFTER_ATTESTED = certification_status.PENDING.toString();
	String CERT_STATUS_AFTER_PWD_SETUP = certification_status.REGISTERED.toString();
	String SESSION_ISSUER_DETAILS_VAR = "SESSION_ISSUER_DETAILS";
	String SESSION_PRIMARY_REP_DETAILS_VAR = "SESSION_PRIMARY_REP_DETAILS";
	String SESSION_PRIMARY_ADDITONAL_DETAILS_VAR = "SESSION_PRIMARY_ADDITONAL_DETAILS";
	String CERT_STATUS_CERTIFIED = certification_status.CERTIFIED.toString();
	String CERT_STATUS_PENDING = certification_status.PENDING.toString();

	boolean isIssuerRepLoggedIn() throws InvalidUserException;

	Integer getIssuerId();// To be Deprecated; instead use
							// getIssuerId(AccountUser user)

	Issuer getIssuer(AccountUser user) throws InvalidUserException;

	Issuer getIssuerById(Integer issuerId);

	Issuer getIssuerId(String issuerName);
	
	List<Issuer> findAllIssuers();

	List<String> getAllIssuerNames();
	
	List<Issuer> getAllIssuerNamesByInsuranceType(String insuranceType);
	
	String saveFile(String fileLocation, MultipartFile mfile, String suggestedFileName) throws IOException;

	Issuer saveIssuer(Issuer issuer);

	IssuerRepresentative saveRepresentative(IssuerRepresentative issuerRep);

	String getAllCertifiedIssuerNames();

	boolean isIssuerExists(Issuer givenIssuer);

	List<Issuer> getAddedOrUpdatedIssuers() throws ParseException;

	List<Issuer> getIssuerList(HttpServletRequest request);

	Page<Issuer> getIssuerList(String issuerName, String status, Pageable pageable);

	List<Plan> getPlanList(HttpServletRequest request, Integer issuerId);

	List<AccountUser> getRepListForIssuer(HttpServletRequest manageRepRequest, Integer issuerIdForRep);

	Page<AccountUser> getRepListForIssuer(String moduleName, String roleName, Integer issuerId, String name, String title, List<Integer> status, Pageable pageable);

	/* @author Raja
	 * Fix of HIX-1709 Deleting a Authorized representative throws Application Error.
	 * NOTE:  THE method deleteRepForIssuer(Integer repId) has invalid implimentation
	 *        and not referenced any where in the project..
	 * 
	    *
	boolean deleteRepForIssuer(Integer repId);*/

	boolean updateRepStatus(Integer repId, String newRepStatus) throws InvalidUserException;

	void syncIssuerWithAHBX(int issuerId);

	boolean isDuplicate(String userEmailId);

	/* @author Raja
	 * @UserStory HIX:1811
	 * @HIX-3121
	 * the below methods which implements common pagination and sorting.
	 */
	Pageable getPaging(Model model, HttpServletRequest request);

	Sort getSorting(Model model, HttpServletRequest request, List<String> sortableColumnsList);

	Pageable getPagingAndSorting(Model model, HttpServletRequest request, List<String> sortableColumnsList);

	String getIssuerNameByRepUserId(Integer userId);

	/* @author Raja
	 * @UserStory HIX:3206
	 * @HIX-3850
	 * retrieving the issuer representative for an user
	 */
	IssuerRepresentative findIssuerRepresentative(AccountUser user, Issuer issuer);

	Page<IssuerRepresentative> findIssuerRepresentative(Integer issuerId, Pageable pageable);

	/* @author Kuldeep
	 * 
	 * retrieving the primary contact of a issuer
	 */
	IssuerRepresentative findPrimaryContact(Issuer issuer, PrimaryContact primarycontact);

	/* @author Raja
	 * @UserStory HIX:1807
	 * @HIX-4201
	 */
	List<Map<String, Object>> loadIssuerStatusHistory(Integer issuerId);

	List<Map<String, Object>> loadIssuerHistory(Integer issuerId);

	ArrayList<Map<String, String>> loadRateBenefitReportList(HttpServletRequest manageRepRequest, Integer issuerId, String type);
	
	List<IssuerRepresentative> findIssuerRepresentative(Integer issuerId);

	IssuerRepresentative findIssuerRepresentativeById(Integer repId);

	/*@author Raja
	 * HIX-4204*/
	Integer saveComment(Integer issuerId, String comments, TargetName targetName) throws InvalidUserException;

	/*Added by Kuldeep 
	 */
	IssuerRepresentative updateDelegateInfo(int representativeId, String delegationCode, int responseCode, String responseDescription);

	/*Added by Kuldeep 
	 */
	DelegationResponse getAhbxDelegationResp(DelegationRequest delegationRequest) throws GIException;

	/*Added by Sandip*/
	List<HashMap<String, String>> removeDuplicateFileName(List<HashMap<String, String>> duplicateFileNames);

	Issuer getIssuerByHiosID(String hiosIssuerID);

	/* Added by Vinayak HIX - 7035 */
	void delete(Issuer entity);

	IssuerRepresentative findIssuerRepresentativeById(int id);

	String getIssuerLogoByName(String issuerName);
	
	String getIssuerLogoByIssuerId(Integer issuerId);

	Page<ExchgPartnerLookup> findListPartnersByIssuer(String hiosIssuerId, Pageable pageable);

	ExchgPartnerLookup saveExchgPartnerLookup(ExchgPartnerLookup partner);

	ExchgPartnerLookup saveNewExchgPartnerLookup(ExchgPartnerLookup partner);

	void generateActivationLink(Integer repId, AccountUser accountUser, String issuerRepRole) throws GIException;

	Integer getDefaultServieAreaId(String servicerAreaName, Integer applicableYear);

	IssuerRepresentative getIssuerRepresentativeForIssuer(Issuer issuerObj);

	AccountUser getAccountUserFromID(Integer id);

	Page<IssuerRepresentative> findIssuerRepresentative(String repFirstName, String repLastName, String repTitle, String status, Integer issuerId, Pageable pageable);

	List<Map<String, String>> getIssuerRepresentatives(HttpServletRequest manageRepRequest, Integer issuerId, boolean escapePagination, boolean showIssuerEnrollmentRep);

	String getIssuerRepStatus(Object userStatus, Object issuerRepStatus);

	int getIssuerRepresentataivesCount(Integer issuerId);

	List<Issuer> findAllIssuerForReport();
	
	String getSerfServiceAreaIdById(Integer id);
	
	void sendFinancialNotificationToIssuerRep(Issuer issuerObj);

	String getIssuerStatus(Integer issuerId);
	
	String deleteAccreditationDocument(String documentID, int issuerId) throws Exception;
	
	String deleteContent(String documentID) throws Exception;
	
	String updatePrimaryContactToNo(Integer issuerId);
	
	String updatePrimaryContactToNo(Integer issuerId, Integer issuerRepId);
	
	Map<String, Object> createEnrolleeSearchCriteria(HttpServletRequest request);

	byte[] getIssuerLogoById(String issuerIdStr);

	byte[] getIssuerLogoByHiosId(String hiosIssuerId);

	//String getIssuerLogoPath(String encIssuerId, String basePath);
	
	Map<String, Object> getDownloadDocumentInfo(String documentId);
	
	public boolean isFileSizeExceedsLimit(List<MultipartFile> authority, List<MultipartFile> accrediting, List<MultipartFile> orgData, List<MultipartFile> marketing, List<MultipartFile> finDisclosures, 
			List<MultipartFile> addSupportDoc, List<MultipartFile> payPolPractices, List<MultipartFile> enrollmentAndDis, List<MultipartFile> ratingPractices, List<MultipartFile> costSharAndPayment);
	
	public boolean isValidExtension(List<MultipartFile> authority, List<MultipartFile> accrediting, List<MultipartFile> orgData, List<MultipartFile> marketing, List<MultipartFile> finDisclosures, 
			List<MultipartFile> addSupportDoc, List<MultipartFile> payPolPractices, List<MultipartFile> enrollmentAndDis, List<MultipartFile> ratingPractices, List<MultipartFile> costSharAndPayment);
	
	public void removeDataFromSession(HttpServletRequest request);

	public String processAndPersistNetworkTransparencyDataFile(MultipartFile[] fileList, String applicableYear, Issuer currentIssuer, String stateCode, int userId);
	
	public boolean isIssuerRepSuspended();

	DrugDataResponseDTO getDrugDataByRxcuiSearchList(String rxcuiData, StringBuffer errorMessage);
}

