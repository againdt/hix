import { DataKeepService } from '../../../services/data-keep.service';
import { Component, OnInit, Input } from '@angular/core';
import { InputSet } from "../../input-group/input-group.component";

@Component({
    selector: 'gi-prescription-search',
    template: `
        <fieldset name="prescriptionFrenquency">
            <legend><h2 class="question-header">{{"PRESCRIPTION_SEARCH.question" | translate}}</h2></legend>

            <div class="col-12">
                <input-group [inputSet]="prescriptions" (select)="getPrescriptionFrequency($event)" [picked]="picked_prescription_frequency"></input-group>

            </div>
        </fieldset>
    `,
    styleUrls: ['./prescription-search.component.scss']
})
export class PrescriptionSearchComponent implements OnInit {
    @Input() picked_prescription_frequency: string;
    public prescriptions: InputSet[] = [
        {
            name: "monthltPrescription",
            value: "LEVEL1",
            text: "INPUT_GROUP.dosage.level1"
        },
        {
            name: "monthltPrescription",
            value: "LEVEL2",
            text: "INPUT_GROUP.dosage.level2",
        },
        {
            name: "monthltPrescription",
            value: "LEVEL3",
            text: "INPUT_GROUP.dosage.level3",
        },
        {
            name: "monthltPrescription",
            value: "LEVEL4",
            text: "INPUT_GROUP.dosage.level4",
        },
    ]
    constructor(
        private datakeep: DataKeepService,
    ) { }

    ngOnInit() {
    }

    public getPrescriptionFrequency(value: string) {
        // console.log(value);
        this.datakeep.updatePersonalizedState("MONTHLY_PRECRIPTION", "ADD", value)
    }

}
