
package com.getinsured.iex.ssap;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author chopra_s
 */
public class OtherImmigrationDocumentType {

  @SerializedName("CubanHaitianEntrantIndicator")
  @Expose
  private Boolean cubanHaitianEntrantIndicator = false;
  @SerializedName("ORREligibilityLetterIndicator")
  @Expose
  private Boolean oRREligibilityLetterIndicator = false;
  @SerializedName("StayOfRemovalIndicator")
  @Expose
  private Boolean stayOfRemovalIndicator = false;
  @SerializedName("ORRCertificationIndicator")
  @Expose
  private Boolean oRRCertificationIndicator = false;
  @SerializedName("AmericanSamoanIndicator")
  @Expose
  private Boolean americanSamoanIndicator = false;
  @SerializedName("WithholdingOfRemovalIndicator")
  @Expose
  private Boolean withholdingOfRemovalIndicator = false;

  /**
   * Document indicating that person is battered spouse, child or parent under the
   * Violence Against Women Act
   */
  @Expose
  private boolean domesticViolence = false;

  /**
   * Document indicating that person is American Indian of Federally Recognized Tribe
   * or American Indian born in Canada.
   */
  @Expose
  private boolean indianOfUsOrCanada = false;

  @Expose
  private boolean noneOfThese = false;

  public Boolean getCubanHaitianEntrantIndicator() {
    return cubanHaitianEntrantIndicator;
  }

  public void setCubanHaitianEntrantIndicator(Boolean cubanHaitianEntrantIndicator) {
    this.cubanHaitianEntrantIndicator = cubanHaitianEntrantIndicator;
  }

  public Boolean getORREligibilityLetterIndicator() {
    return oRREligibilityLetterIndicator;
  }

  public void setORREligibilityLetterIndicator(Boolean oRREligibilityLetterIndicator) {
    this.oRREligibilityLetterIndicator = oRREligibilityLetterIndicator;
  }

  public Boolean getStayOfRemovalIndicator() {
    return stayOfRemovalIndicator;
  }

  public void setStayOfRemovalIndicator(Boolean stayOfRemovalIndicator) {
    this.stayOfRemovalIndicator = stayOfRemovalIndicator;
  }

  public Boolean getORRCertificationIndicator() {
    return oRRCertificationIndicator;
  }

  public void setORRCertificationIndicator(Boolean oRRCertificationIndicator) {
    this.oRRCertificationIndicator = oRRCertificationIndicator;
  }

  public Boolean getAmericanSamoanIndicator() {
    return americanSamoanIndicator;
  }

  public void setAmericanSamoanIndicator(Boolean americanSamoanIndicator) {
    this.americanSamoanIndicator = americanSamoanIndicator;
  }

  public Boolean getWithholdingOfRemovalIndicator() {
    return withholdingOfRemovalIndicator;
  }

  public void setWithholdingOfRemovalIndicator(Boolean withholdingOfRemovalIndicator) {
    this.withholdingOfRemovalIndicator = withholdingOfRemovalIndicator;
  }

  public Boolean getDomesticViolence() {
    return domesticViolence;
  }

  public void setDomesticViolence(Boolean domesticViolence) {
    this.domesticViolence = domesticViolence;
  }

  public boolean isIndianOfUsOrCanada() {
    return indianOfUsOrCanada;
  }

  public void setIndianOfUsOrCanada(boolean indianOfUsOrCanada) {
    this.indianOfUsOrCanada = indianOfUsOrCanada;
  }

  public boolean isNoneOfThese() {
    return noneOfThese;
  }

  public void setNoneOfThese(boolean noneOfThese) {
    this.noneOfThese = noneOfThese;
  }
}
