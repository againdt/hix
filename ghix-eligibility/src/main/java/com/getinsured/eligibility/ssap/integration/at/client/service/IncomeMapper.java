package com.getinsured.eligibility.ssap.integration.at.client.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.getinsured.eligibility.ssap.integration.util.AccountTransferUtil;
import com.getinsured.hix.webservice.applicanteligibility.gov.cms.hix._0_1.hix_types.FrequencyCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.FrequencyType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.IncomeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core.VerificationMetadataType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.FrequencyCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.IncomeCategoryCodeType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.VerificationCategoryCodeSimpleType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.AmountType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.NumericType;
import com.getinsured.iex.erp.gov.niem.niem.niem_core._2.TextType;

public class IncomeMapper {
	private static final String kEY_UNEMPLOYMENT_BENEFIT = "unemploymentBenefit";
	private static final String kEY_SOCIAL_SECURITY_BENEFIT = "socialSecurityBenefit";
	private static final String kEY_INCOME_FREQUENCY = "incomeFrequency";
	private static Logger lOGGER = Logger.getLogger(IncomeMapper.class);
	
	 private static final Map<String, IncomeCategoryCodeSimpleType> IncomeType = new HashMap<>();
	 private static final Map<String, IncomeCategoryCodeSimpleType> IncomeSubType = new HashMap<>();
	 public static final Map<IncomeCategoryCodeSimpleType, com.getinsured.iex.ssap.financial.type.IncomeType> incomeToAppDataJson;
	 public static final Map<IncomeCategoryCodeSimpleType, com.getinsured.iex.ssap.financial.type.IncomeSubType> incomeSubTypeToAppDataJson;
	 public static final Map<String, com.getinsured.iex.ssap.financial.type.Frequency> incomeFrequencyTypeToAppDataJson;
	    static{
	    	IncomeType.put("JOB",IncomeCategoryCodeSimpleType.WAGES);
	    	IncomeType.put("INVESTMENT",IncomeCategoryCodeSimpleType.INTEREST);
	    	IncomeType.put("RETIREMENT",IncomeCategoryCodeSimpleType.RETIREMENT);
	    	IncomeType.put("PENSION",IncomeCategoryCodeSimpleType.PENSION);
	    	IncomeType.put("SOCIAL_SECURITY_BENEFITS",IncomeCategoryCodeSimpleType.SOCIAL_SECURITY);
	    	IncomeType.put("SOCIAL_SECURITY_DISABILITY",IncomeCategoryCodeSimpleType.SOCIAL_SECURITY);
	    	IncomeType.put("ALIMONY",IncomeCategoryCodeSimpleType.ALIMONY);
	    	IncomeType.put("RENTAL_ROYALTY",IncomeCategoryCodeSimpleType.RENTAL_OR_ROYALTY);
	    	IncomeType.put("SELF_EMPLOYMENT",IncomeCategoryCodeSimpleType.SELF_EMPLOYMENT);
	    	IncomeType.put("FARMING_FISHING",IncomeCategoryCodeSimpleType.FARMING_OR_FISHING);
	    	IncomeType.put("CAPITAL_GAIN",IncomeCategoryCodeSimpleType.CAPITAL_GAINS);
	    	IncomeType.put("UNEMPLOYMENT",IncomeCategoryCodeSimpleType.UNEMPLOYMENT);
	    	IncomeType.put("SCHOLARSHIP",IncomeCategoryCodeSimpleType.SCHOLARSHIP);
	    	IncomeType.put("OTHER",IncomeCategoryCodeSimpleType.UNSPECIFIED);
	    	IncomeType.put("UNSPECIFIED",IncomeCategoryCodeSimpleType.UNSPECIFIED);
	    	//IncomeType.put("UNEMPLOYMENT","Unemployment");
	    	IncomeSubType.put("UNSPECIFIED",IncomeCategoryCodeSimpleType.UNSPECIFIED);
	    	IncomeSubType.put("CASH_SUPPORT",IncomeCategoryCodeSimpleType.CASH_SUPPORT);
	    	IncomeSubType.put("COURT_AWARD",IncomeCategoryCodeSimpleType.COURT_AWARD);
	    	IncomeSubType.put("JURY_DUTY_PAY",IncomeCategoryCodeSimpleType.JURY_DUTY);
	    	IncomeSubType.put("CANCELED_DEBT",IncomeCategoryCodeSimpleType.CANCELED_DEBT);
	    	IncomeSubType.put("COURT_AWARD",IncomeCategoryCodeSimpleType.UNSPECIFIED);
	    	IncomeSubType.put("GAMBLING_PRIZE_AWARD",IncomeCategoryCodeSimpleType.UNSPECIFIED);
	    	IncomeSubType.put("OTHER_INCOME",IncomeCategoryCodeSimpleType.UNSPECIFIED);

	    	incomeToAppDataJson = new HashMap<IncomeCategoryCodeSimpleType, com.getinsured.iex.ssap.financial.type.IncomeType>();
	    	
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.WAGES,com.getinsured.iex.ssap.financial.type.IncomeType.JOB);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.INTEREST,com.getinsured.iex.ssap.financial.type.IncomeType.INVESTMENT);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.RETIREMENT,com.getinsured.iex.ssap.financial.type.IncomeType.RETIREMENT);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.PENSION,com.getinsured.iex.ssap.financial.type.IncomeType.PENSION);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.SOCIAL_SECURITY,com.getinsured.iex.ssap.financial.type.IncomeType.SOCIAL_SECURITY_DISABILITY);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.ALIMONY,com.getinsured.iex.ssap.financial.type.IncomeType.ALIMONY);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.RENTAL_OR_ROYALTY,com.getinsured.iex.ssap.financial.type.IncomeType.RENTAL_ROYALTY);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.SELF_EMPLOYMENT,com.getinsured.iex.ssap.financial.type.IncomeType.SELF_EMPLOYMENT);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.FARMING_OR_FISHING,com.getinsured.iex.ssap.financial.type.IncomeType.FARMING_FISHING);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CAPITAL_GAINS,com.getinsured.iex.ssap.financial.type.IncomeType.CAPITAL_GAIN);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.UNEMPLOYMENT,com.getinsured.iex.ssap.financial.type.IncomeType.UNEMPLOYMENT);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.SCHOLARSHIP,com.getinsured.iex.ssap.financial.type.IncomeType.SCHOLARSHIP);
	    	//incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CASH_SUPPORT,"OTHER");
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.UNSPECIFIED,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	/*incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CASH_SUPPORT,"CASH_SUPPORT");
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.COURT_AWARD,"COURT_AWARD");
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.JURY_DUTY,"JURY_DUTY_PAY");
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CANCELED_DEBT,"CANCELED_DEBT");
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.WINNINGS,"PRIZES_AWARDS_GAMBLING_WINNINGS");*/
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CASH_SUPPORT,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.COURT_AWARD,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.JURY_DUTY,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.CANCELED_DEBT,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	incomeToAppDataJson.put(IncomeCategoryCodeSimpleType.WINNINGS,com.getinsured.iex.ssap.financial.type.IncomeType.OTHER);
	    	
	    	incomeSubTypeToAppDataJson = new HashMap<IncomeCategoryCodeSimpleType, com.getinsured.iex.ssap.financial.type.IncomeSubType>();
	    	incomeSubTypeToAppDataJson.put(IncomeCategoryCodeSimpleType.CASH_SUPPORT,com.getinsured.iex.ssap.financial.type.IncomeSubType.CASH_SUPPORT);
	    	incomeSubTypeToAppDataJson.put(IncomeCategoryCodeSimpleType.COURT_AWARD,com.getinsured.iex.ssap.financial.type.IncomeSubType.COURT_AWARD);
	    	incomeSubTypeToAppDataJson.put(IncomeCategoryCodeSimpleType.JURY_DUTY,com.getinsured.iex.ssap.financial.type.IncomeSubType.JURY_DUTY_PAY);
	    	incomeSubTypeToAppDataJson.put(IncomeCategoryCodeSimpleType.CANCELED_DEBT,com.getinsured.iex.ssap.financial.type.IncomeSubType.CANCELED_DEBT);
	    	incomeSubTypeToAppDataJson.put(IncomeCategoryCodeSimpleType.WINNINGS,com.getinsured.iex.ssap.financial.type.IncomeSubType.GAMBLING_PRIZE_AWARD);
	    	
	    	incomeFrequencyTypeToAppDataJson = new HashMap<String, com.getinsured.iex.ssap.financial.type.Frequency>();
	    	incomeFrequencyTypeToAppDataJson.put("Weekly", com.getinsured.iex.ssap.financial.type.Frequency.WEEKLY);
	    	incomeFrequencyTypeToAppDataJson.put("BiWeekly", com.getinsured.iex.ssap.financial.type.Frequency.BIWEEKLY);
	    	incomeFrequencyTypeToAppDataJson.put("Monthly", com.getinsured.iex.ssap.financial.type.Frequency.MONTHLY);
	    	incomeFrequencyTypeToAppDataJson.put("Quarterly", com.getinsured.iex.ssap.financial.type.Frequency.QUARTERLY);
	    	incomeFrequencyTypeToAppDataJson.put("Annually", com.getinsured.iex.ssap.financial.type.Frequency.YEARLY);
	    	incomeFrequencyTypeToAppDataJson.put("SemiMonthly", com.getinsured.iex.ssap.financial.type.Frequency.BIMONTHLY);
	    	incomeFrequencyTypeToAppDataJson.put("Hourly", com.getinsured.iex.ssap.financial.type.Frequency.HOURLY);
	    	incomeFrequencyTypeToAppDataJson.put("Once", com.getinsured.iex.ssap.financial.type.Frequency.ONCE);
	    	incomeFrequencyTypeToAppDataJson.put("Daily", com.getinsured.iex.ssap.financial.type.Frequency.DAILY);
		}

	    
	public List<IncomeType> mapIncomeData(List<JSONObject> jobIncomes,  Map<String, VerificationMetadataType> personVerificationMetadata, JSONObject householdMemberJSON){
		List<IncomeType> returnJobIncomes = new ArrayList<IncomeType>();
		for (Object incomes : jobIncomes) {
			HashMap jobIncome = (HashMap) incomes;
			if(jobIncome.get("amount") !=  null && StringUtils.isNotBlank(jobIncome.get("amount").toString())){
				returnJobIncomes.add(this.createIncomeType(jobIncome, "INCOME", householdMemberJSON));
				if(jobIncome.get("tribalAmount") !=  null && StringUtils.isNotBlank(jobIncome.get("tribalAmount").toString()) && (Integer) jobIncome.get("tribalAmount") > 0){
					returnJobIncomes.add(this.createIncomeType(jobIncome, "TRIBAL", householdMemberJSON));
				}
			}
		}
		return returnJobIncomes;
	}
	
	private IncomeType createIncomeType(HashMap jobIncome, String incomeTypeFlag, JSONObject householdMemberJSON){
		IncomeType jobIncomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();
		
		FrequencyType frequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
		FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
		frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType(jobIncome.get("frequency").toString()));
		frequency.setFrequencyCode(frequencyCodeType);
		jobIncomeType.setIncomeFrequency(frequency);
		
		if(com.getinsured.iex.ssap.financial.type.Frequency.HOURLY.toString().equalsIgnoreCase(frequency.getFrequencyCode().getValue())){
			NumericType hoursPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
			if(jobIncome.get("cyclesPerFrequency") != null){
				hoursPerWeek.setValue(new BigDecimal((Integer) jobIncome.get("cyclesPerFrequency")));
				jobIncomeType.setIncomeHoursPerWeekMeasure(hoursPerWeek);
			}
		}
		
		if( com.getinsured.iex.ssap.financial.type.Frequency.DAILY.toString().equalsIgnoreCase(frequency.getFrequencyCode().getValue())){
			NumericType daysPerWeek = AccountTransferUtil.niemCoreFactory.createNumericType();
			if(jobIncome.get("cyclesPerFrequency") != null){
				daysPerWeek.setValue(new BigDecimal((Integer) jobIncome.get("cyclesPerFrequency")));
				jobIncomeType.setIncomeDaysPerWeekMeasure(daysPerWeek);
			}
		}
		
		IncomeCategoryCodeType personIncomeCategoryCodeType = AccountTransferUtil.hixTypeFactory.createIncomeCategoryCodeType();
		String type = (String) jobIncome.get("type");
		if(org.apache.commons.lang3.StringUtils.isNotBlank(type)){
			personIncomeCategoryCodeType.setValue(IncomeType.get(type));
			
			if(personIncomeCategoryCodeType != null && personIncomeCategoryCodeType.getValue() != null && personIncomeCategoryCodeType.getValue().value() != null) {
				jobIncomeType.setIncomeCategoryCode(personIncomeCategoryCodeType);
			}
		}
		BigDecimal finalAmount = this.finalAmount(jobIncome);
		if(incomeTypeFlag.equals("INCOME")){
			jobIncomeType.setIncomeSubjectToFederalRestrictionsIndicator(AccountTransferUtil.addBoolean(Boolean.FALSE));
		}else if(incomeTypeFlag.equals("TRIBAL")){
			finalAmount = AccountTransferUtil.getAmount(jobIncome.get("tribalAmount"));
			jobIncomeType.setIncomeSubjectToFederalRestrictionsIndicator(AccountTransferUtil.addBoolean(Boolean.TRUE));
		}
		AmountType personIncomeAmountType = AccountTransferUtil.niemCoreFactory.createAmountType();
		personIncomeAmountType.setValue(finalAmount);
		jobIncomeType.setIncomeAmount(personIncomeAmountType);
		
		if(personIncomeCategoryCodeType != null && personIncomeCategoryCodeType.getValue() != null) {
			this.mapIncomesByType(jobIncomeType, personIncomeCategoryCodeType, householdMemberJSON, jobIncome);
		}
		return jobIncomeType;
	}
	
	private BigDecimal finalAmount(HashMap jobIncome){
		BigDecimal tribalAmount =  BigDecimal.ZERO;
		if(jobIncome.get("tribalAmount") != null){
			 tribalAmount =  AccountTransferUtil.getAmount(jobIncome.get("tribalAmount"));
		}
		BigDecimal amount = AccountTransferUtil.getAmount(jobIncome.get("amount"));
	    BigDecimal finalAmount = amount.subtract(tribalAmount);
		if(finalAmount.compareTo(BigDecimal.ZERO) != 0){
			finalAmount = finalAmount.divide(BigDecimal.valueOf(100));
		}
		return finalAmount;
	}

	
	
	private List<IncomeType>   mapJobIncomes(List<?> jobIncomes,  Map<String, VerificationMetadataType> personVerificationMetadata){
		List<IncomeType> returnJobIncomes = new ArrayList<IncomeType>();
		for (Object incomes : jobIncomes) {
			HashMap jobIncome = (HashMap) incomes;
			if(jobIncome.get("incomeAmountBeforeTaxes") !=  null && StringUtils.isNotBlank(jobIncome.get("incomeAmountBeforeTaxes").toString())){
				IncomeType jobIncomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();
				AmountType personIncomeAmountType = AccountTransferUtil.niemCoreFactory.createAmountType();
				//String incomeAmountBeforeTaxes = (java.lang.String)jobIncome.get("incomeAmountBeforeTaxes") ;
				personIncomeAmountType.setValue(AccountTransferUtil.getAmount(jobIncome.get("incomeAmountBeforeTaxes")));
				IncomeCategoryCodeType personIncomeCategoryCodeType = AccountTransferUtil.hixTypeFactory.createIncomeCategoryCodeType();
				personIncomeCategoryCodeType.setValue(IncomeCategoryCodeSimpleType.WAGES);
				jobIncomeType.setIncomeAmount(personIncomeAmountType);
				jobIncomeType.setIncomeCategoryCode(personIncomeCategoryCodeType);
				FrequencyType wagefrequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
				FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
				frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType(jobIncome.get(kEY_INCOME_FREQUENCY).toString()));
				wagefrequency.setFrequencyCode(frequencyCodeType);
				jobIncomeType.setIncomeFrequency(wagefrequency);
				if( (FrequencyCodeSimpleType.DAILY.value().equalsIgnoreCase((String) jobIncome.get(kEY_INCOME_FREQUENCY))) 
						|| (FrequencyCodeSimpleType.HOURLY.value().equalsIgnoreCase((String) jobIncome.get(kEY_INCOME_FREQUENCY)))) {
					NumericType daysPerWeekNT = AccountTransferUtil.niemCoreFactory.createNumericType();
					daysPerWeekNT.setValue(new BigDecimal((Integer) jobIncome.get("workHours")));
					jobIncomeType.setIncomeDaysPerWeekMeasure(daysPerWeekNT);
				}
				VerificationMetadataWrapper.setMetadata(personVerificationMetadata, jobIncomeType, VerificationCategoryCodeSimpleType.CURRENT_INCOME);

				returnJobIncomes.add(jobIncomeType);
			}
		}
		
		return returnJobIncomes;
	}
	
	private void mapIncomesByType(IncomeType incomeType, IncomeCategoryCodeType incomeCategoryCodeType, JSONObject householdMemberJSON, HashMap jobIncome){
		if("Unemployment".equals(incomeCategoryCodeType.getValue().value())){
			String incomeUnEmploymentSrcStr= (java.lang.String) jobIncome.get("sourceName") ;
			if(StringUtils.isNotBlank(incomeUnEmploymentSrcStr)){
				incomeType.setIncomeUnemploymentSourceText(AccountTransferUtil.createTextType(incomeUnEmploymentSrcStr));
			}				
		}
			
		if("SelfEmployment".equals(incomeCategoryCodeType.getValue().value())){
			String subType = (String) jobIncome.get("subType");
			String typeOfEmplomentStr = "UNSPECIFIED";  
			if(StringUtils.isNotBlank(subType)){
				typeOfEmplomentStr = subType;
			}
			TextType incomeEmploymentDescText = AccountTransferUtil.niemCoreFactory.createTextType();
			incomeEmploymentDescText.setValue(typeOfEmplomentStr);
			incomeType.setIncomeEmploymentDescriptionText(incomeEmploymentDescText);
		}
	}
	
	public List<IncomeType> mapIncomes(JSONObject detailedIncome, Map<String, VerificationMetadataType> personVerificationMetadata) {

		List<IncomeType> returnIncomes = new ArrayList<IncomeType>();
		List<?> jobIncomes = (JSONArray) detailedIncome.get("jobIncome");
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("jobIncomeIndicator") )){
			returnIncomes.addAll(this.mapJobIncomes(jobIncomes, personVerificationMetadata));
		}

		if(AccountTransferUtil.checkBoolean(detailedIncome.get("unemploymentBenefitIndicator"))){
			if ((JSONObject) detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT) != null) {
				IncomeType unemploymentIncomeType = this.createIncomeType(
						(JSONObject)detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT),
						AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT)).get("benefitAmount")),
						(java.lang.String)((JSONObject)detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT)).get(kEY_INCOME_FREQUENCY),
						IncomeCategoryCodeSimpleType.UNEMPLOYMENT,
						personVerificationMetadata);
				String incomeUnEmploymentSrcStr= (java.lang.String) ((JSONObject) detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT)).get("stateGovernmentName");
				if(StringUtils.isNotBlank(incomeUnEmploymentSrcStr)){
					unemploymentIncomeType.setIncomeUnemploymentSourceText(AccountTransferUtil.createTextType(incomeUnEmploymentSrcStr));
				}
				DateRangeType incomeEarnedDateRange = AccountTransferUtil.niemCoreFactory.createDateRangeType();
				DateType incomeEarnedEndDate = AccountTransferUtil.niemCoreFactory.createDateType();
				com.getinsured.iex.erp.gov.niem.niem.proxy.xsd._2.Date endDate = AccountTransferUtil.basicFactory.createDate();
				if (((JSONObject) detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT)).get("unemploymentDate") != null) {
					endDate.setValue(AccountTransferUtil.stringToXMLGregorianCalendar((java.lang.String) ((JSONObject) detailedIncome.get(kEY_UNEMPLOYMENT_BENEFIT)).get("unemploymentDate")));
					incomeEarnedEndDate.setDate(endDate);
					incomeEarnedDateRange.setEndDate(incomeEarnedEndDate);
					unemploymentIncomeType.setIncomeEarnedDateRange(incomeEarnedDateRange);
				}
				returnIncomes.add(unemploymentIncomeType);
			}
		}	
		//DetailedIncome<->personIncome
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("selfEmploymentIncomeIndicator"))){
			IncomeType selfIncomeType =this.createIncomeType(
					(JSONObject)detailedIncome.get("selfEmploymentIncome"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("selfEmploymentIncome")).get("monthlyNetIncome")),
					"Monthly",
					IncomeCategoryCodeSimpleType.SELF_EMPLOYMENT,
					personVerificationMetadata);
			String typeOfEmplomentStr = (java.lang.String) ((JSONObject) detailedIncome.get("selfEmploymentIncome")).get("typeOfWork"); 
			if(StringUtils.isNotBlank(typeOfEmplomentStr)){
				TextType incomeEmploymentDescText = AccountTransferUtil.niemCoreFactory.createTextType();
				incomeEmploymentDescText.setValue(typeOfEmplomentStr);
				selfIncomeType.setIncomeEmploymentDescriptionText(incomeEmploymentDescText);
			}
			returnIncomes.add(selfIncomeType);
		}
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("retirementOrPensionIndicator"))){

			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("retirementOrPension"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("retirementOrPension")).get("taxableAmount")),
					(java.lang.String)((JSONObject)detailedIncome.get("retirementOrPension")).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.RETIREMENT,
					personVerificationMetadata));
		}

		if(AccountTransferUtil.checkBoolean(detailedIncome.get("investmentIncomeIndicator"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("investmentIncome"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("investmentIncome")).get("incomeAmount")),
					(java.lang.String)((JSONObject)detailedIncome.get("investmentIncome")).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.INTEREST,
					personVerificationMetadata));
		}

		if(AccountTransferUtil.checkBoolean(detailedIncome.get("rentalRoyaltyIncomeIndicator"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("rentalRoyaltyIncome"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("rentalRoyaltyIncome")).get("netIncomeAmount")),
					(java.lang.String)((JSONObject)detailedIncome.get("rentalRoyaltyIncome")).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.RENTAL_OR_ROYALTY,
					personVerificationMetadata));
		}

		if(AccountTransferUtil.checkBoolean(detailedIncome.get("capitalGainsIndicator"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("capitalGains"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("capitalGains")).get("annualNetCapitalGains")),
					"Yearly",
					IncomeCategoryCodeSimpleType.CAPITAL_GAINS,
					personVerificationMetadata));
		}
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("alimonyReceivedIndicator"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("alimonyReceived"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("alimonyReceived")).get("amountReceived")),
					(java.lang.String)((JSONObject)detailedIncome.get("alimonyReceived")).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.ALIMONY,
					personVerificationMetadata));
		}
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("farmFishingIncomeIndictor"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get("farmFishingIncome"),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get("farmFishingIncome")).get("netIncomeAmount")),
					(java.lang.String)((JSONObject)detailedIncome.get("farmFishingIncome")).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.FARMING_OR_FISHING,
					personVerificationMetadata));
		}
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("socialSecurityBenefitIndicator"))){
			returnIncomes.add(this.createIncomeType(
					(JSONObject)detailedIncome.get(kEY_SOCIAL_SECURITY_BENEFIT),
					AccountTransferUtil.getAmount(((JSONObject)detailedIncome.get(kEY_SOCIAL_SECURITY_BENEFIT)).get("benefitAmount")),
					(java.lang.String)((JSONObject)detailedIncome.get(kEY_SOCIAL_SECURITY_BENEFIT)).get(kEY_INCOME_FREQUENCY),
					IncomeCategoryCodeSimpleType.SOCIAL_SECURITY,
					personVerificationMetadata));
		}

		createOtherIncomes(detailedIncome, personVerificationMetadata,
				returnIncomes);
		return returnIncomes;
	}

	protected void createOtherIncomes(JSONObject detailedIncome,
			Map<String, VerificationMetadataType> personVerificationMetadata,
			List<IncomeType> returnIncomes) {
		if(AccountTransferUtil.checkBoolean(detailedIncome.get("otherIncomeIndicator"))){
			Object[] otherincomes = ((JSONArray)detailedIncome.get("otherIncome")).toArray();
			for(Object  otherincome: otherincomes){
				JSONObject income = (JSONObject)otherincome;
				if(income.get("incomeAmount") != null ){
					returnIncomes.add(this.createIncomeType(
							income,
							AccountTransferUtil.getAmount(income.get("incomeAmount")),
							(java.lang.String)income.get(kEY_INCOME_FREQUENCY),
							AccountTransferUtil.getIncomeType((java.lang.String)income.get("otherIncomeTypeDescription")),
							personVerificationMetadata));
				}	
			}	
		}
	}

	private IncomeType createIncomeType(JSONObject incomeJSON, BigDecimal amount, String frequency, IncomeCategoryCodeSimpleType categoryCode,  Map<String, VerificationMetadataType> personVerificationMetadata){
		JSONObject incomeInJSON = incomeJSON;
		lOGGER.debug("incomeJSON "+ incomeJSON);
		IncomeType incomeType = AccountTransferUtil.hixCoreFactory.createIncomeType();
		AmountType amountType = AccountTransferUtil.niemCoreFactory.createAmountType();
		amountType.setValue(amount);
		incomeType.setIncomeAmount(amountType);

		if(categoryCode != null){
			IncomeCategoryCodeType incomeCategoryCodeType = AccountTransferUtil.hixTypeFactory.createIncomeCategoryCodeType();
			incomeCategoryCodeType.setValue(categoryCode);
			incomeType.setIncomeCategoryCode(incomeCategoryCodeType);
		}

		if(StringUtils.isNotBlank(frequency)){
			FrequencyType  incomeFrequency = AccountTransferUtil.hixCoreFactory.createFrequencyType();
			FrequencyCodeType frequencyCodeType = AccountTransferUtil.hixTypeFactory.createFrequencyCodeType();
			frequencyCodeType.setValue(AccountTransferUtil.getFrequencyType(frequency));
			incomeFrequency.setFrequencyCode(frequencyCodeType);
			incomeType.setIncomeFrequency(incomeFrequency);
		}	

		VerificationMetadataWrapper.setMetadata(personVerificationMetadata, incomeType, VerificationCategoryCodeSimpleType.CURRENT_INCOME);
		return incomeType;
	}

}
