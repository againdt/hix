package com.getinsured.consumer.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.SsapApplication;

@Repository
public interface ISsapApplicationRepository extends JpaRepository<SsapApplication, Long>{

}
