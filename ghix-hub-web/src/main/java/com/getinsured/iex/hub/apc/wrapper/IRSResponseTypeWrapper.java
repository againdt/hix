package com.getinsured.iex.hub.apc.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.IRSResponseType;

public class IRSResponseTypeWrapper implements JSONAware{
	
    private String requestID;
    private APTCTypeWrapper aptcWrapper;
    private String systemCode;
    
	public IRSResponseTypeWrapper(IRSResponseType iRSResponseType){
		
		if(iRSResponseType != null)
		{
			requestID = iRSResponseType.getRequestID().getValue();
			systemCode = iRSResponseType.getSystemCode().getValue();
			aptcWrapper = new APTCTypeWrapper(iRSResponseType.getAPTC());
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public java.lang.String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("RequestID", this.requestID);
		obj.put("APTC", this.aptcWrapper);
		obj.put("SystemCode", this.systemCode);
		return obj.toJSONString();
	}
}
