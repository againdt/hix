package com.getinsured.eligibility.ssap.integration.notices;


public interface QEPNonFinancialNotificationService {

	String generateQEPGrantNoticeForNonFinancial(String caseNumber) throws Exception;

}
