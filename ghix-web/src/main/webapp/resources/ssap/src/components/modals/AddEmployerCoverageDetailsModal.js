import React, {Fragment, useEffect, useState} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import NamesInputs from '../../components/pages/pageIncludes/common/names/NamesInputs';
import FormForModal from '../../components/pages/FormForModal';
import UniversalInput from '../../lib/UniversalInput';
import {changeData, hideModal} from '../../actions/actionCreators';
import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import INPUT_TYPES from '../../constants/inputTypes';
import VALIDATION_TYPES from '../../constants/validationTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';


// Components

// ActionCreators

// Utils

// Constants


const AddEmployerCoverageModal = props => {

    let {
        contexts,
        data,
        householdMembers,
        modal: {
            modalOwner,
            data: [
                modalData,
                index,
                parameter,
                pageToReturn
            ]
        }
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        name
    } = currentHhm;



    const [ localEmployerState, setLocalEmployerState ] = useState({...modalData});
    const {
        name: {
            firstName,
            middleName,
            lastName,
            suffix,
        },
        phone,
        email,
        address: {
            postalCode,
            streetAddress1,
            streetAddress2,
            state,
            city
        },
        ein
    } = localEmployerState;
    console.log('localEmployerState', localEmployerState);
    const [ areChangesApplied, changeStatus ] = useState(false);

    const handleSaveClick = () => {
        if(!areChangesApplied) {
            props.actions.hideModal(modalOwner);
        }
        const updateData = () => {
            const newState = { ...data };
            console.log(newState);
            newState
                .singleStreamlinedApplication
                .taxHousehold[0]
                .householdMember[member].employerSponsoredCoverage[index] = localEmployerState;
            console.log(newState);
            props.actions.changeData(newState);
        };
        contexts.FormForModal.onFormSubmitForValidationOnly([updateData, () => props.actions.hideModal(modalOwner)]);
    };
    console.log('householdMembers[member][parameter]', householdMembers[member][parameter]);
    const valuesForWatch = householdMembers[member][parameter].length === 0 ?
        'no values' : householdMembers[member][parameter].map(value => Object.values(value).toString()).toString();

    useEffect(() => {
        if(valuesForWatch !== 'no values' && areChangesApplied) {
            changeStatus(false);
            contexts.FormForModal.onFormSubmit(pageToReturn,
                [() => props.actions.hideModal(modalOwner)]
            );
        }
    }, [valuesForWatch]);
    const onFieldChange = newData => {
        console.log(newData);
        setLocalEmployerState({...newData});
        areChangesApplied === false && changeStatus(true);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal__validate__address">
                <h2> Employer Details
                </h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={() => props.actions.hideModal(modalOwner)}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <div id="aiAddIncomeOrDeductionModal" className="usa-grid">
                <div className="modal__body">

                    <FormForModal>
                        <h3>Who can we contact about {normalizeNameOfPerson(name)}&apos;s health coverage?</h3>
                        <NamesInputs
                            {...{
                                firstName,
                                middleName,
                                lastName,
                                suffix,
                                currentData: localEmployerState,
                                fields: ['name'],
                                isRequired: false,
                                inputActions: {
                                    onFieldChange
                                }
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Phone',
                                labelClasses: ['spl__width--01']
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.PHONE_NUMBER_INPUT,
                                name: 'phone',
                                helperClasses: [],
                                value: phone,
                                placeholder: '(xxx) xxx-xxxx',
                                validationType: VALIDATION_TYPES.PHONE_NUMBER,
                                currentData: localEmployerState,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Email',
                                labelClasses: ['spl__width--01']
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'email',
                                helperClasses: [''],
                                value: email,
                                placeholder: 'Email Address',
                                currentData: localEmployerState,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Address 1',
                                labelClasses: ['spl__width--01']
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'streetAddress1',
                                helperClasses: [''],
                                value: streetAddress1,
                                placeholder: 'Address 1',
                                currentData: localEmployerState,
                                fields: ['address']
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />

                        <UniversalInput
                            label={{
                                text: 'Address 2',
                                labelClasses: ['spl__width--01']
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'streetAddress2',
                                helperClasses: [],
                                value: streetAddress2,
                                placeholder: 'Address 2',
                                currentData: localEmployerState,
                                fields: ['address']
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'City',
                                labelClasses: ['spl__width--01']
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'city',
                                helperClasses: [],
                                value: city,
                                placeholder: 'City',
                                currentData: localEmployerState,
                                fields: ['address']
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'Zip',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'postalCode',
                                helperClasses: [],
                                value: postalCode,
                                placeholder: 'Zip',
                                currentData: localEmployerState,
                                fields: ['address']
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'State',
                                isLabelRequired: false
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.DROPDOWN,
                                name: 'state',
                                value: state,
                                options: DROPDOWN_OPTIONS.STATE,
                                validationType: VALIDATION_TYPES.CHOSEN,
                                currentData: localEmployerState,
                                fields: ['address']
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                        <UniversalInput
                            label={{
                                text: 'EIN',
                                labelClasses: []
                            }}
                            inputData={{
                                isInputRequired: false,
                                type: INPUT_TYPES.REGULAR_INPUT,
                                name: 'ein',
                                helperClasses: [],
                                value: ein,
                                placeholder: 'Employer Identification Number',
                                currentData: localEmployerState,
                                fields: []
                            }}
                            inputActions={{
                                onFieldChange: onFieldChange
                            }}
                        />
                    </FormForModal>
                </div>
            </div>
            <div className='modal__footer'>
                <div className="usa-grid controls">
                    <div className="usa-offset-one-third txt-left">
                        <Button
                            className='modal-cancel-btn usa-button-secondary'
                            onClick={() => props.actions.hideModal(modalOwner)}
                        >Cancel
                        </Button>
                        <Button
                            className='modal-confirm-btn'
                            onClick={handleSaveClick}
                        >Save
                        </Button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    contexts: state.contexts,
    data: state.data,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData,
        hideModal
    }, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AddEmployerCoverageModal);
