package com.getinsured.eligibility.rrv.model;

import java.util.List;

public class RrvRenewalRequest {
    private RrvMetadata metadata;
    private List<RrvHousehold> households;

    public RrvMetadata getMetadata() {
        return metadata;
    }

    public void setMetadata(RrvMetadata metadata) {
        this.metadata = metadata;
    }

    public List<RrvHousehold> getHouseholds() {
        return households;
    }

    public void setHouseholds(List<RrvHousehold> households) {
        this.households = households;
    }
}
