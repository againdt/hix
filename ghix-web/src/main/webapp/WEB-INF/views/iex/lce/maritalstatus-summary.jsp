<script type="text/ng-template" id="maritalstatus-summary.jsp">
	<div class="gutter10">
	  <h3><spring:message code="label.lce.individualsummary"/></h3>
	  <div class="gutter10" ng-init="householdMember = jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember[jsonObject.singleStreamlinedApplication.taxHousehold[jsonObject.singleStreamlinedApplication.taxHousehold.length-1].householdMember.length-1]">
	    <table class="table  table-border-none" role="presentation">
	      <tbody>
		      <tr class="header">
			      <td class="primaryContact">
			        <h5>{{householdMember.name.firstName}} {{householdMember.name.middleName}} {{householdMember.name.lastName}}</h5>
			      </td>
			      <td class="primaryContact">
			        <h5>(<spring:message code="label.lce.primarycontact"/>) <a ng-click="firstPartial()" class="btn btn-primary pull-right"><spring:message code="label.lce.gobacktochangeinformation"/></a></h5>
			      </td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.dob"/></td>
			      <td date-Formatter-Non-Input date="householdMember.dateOfBirth"></td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.applyingforhealthcare"/></td>
			      <td>
			  			<div ng-switch on="householdMember.applyingForCoverageIndicator">
						    <div ng-switch-when="true">
						      <spring:message code="label.lce.yes"/>
						    </div>
						    <div ng-switch-when="false">
						      <spring:message code="label.lce.no"/>
						    </div>
						    <div ng-switch-default></div>
						  </div>
			      </td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.sex"/></td>
			      <td>
		      		<div ng-switch on="householdMember.gender">
				    	<div ng-switch-when="male">
				       	 	<spring:message code="label.lce.male"/>
				    	</div>
				    	<div ng-switch-when="female">
				        	<spring:message code="label.lce.female"/>
				    	</div>
				    	<div ng-switch-default>
				    		{{householdMember.gender}}
					   </div>
			      </td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.ssn"/></td>
			      <td>
						<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber==''">
							N.A.
						</div>

						<div ng-if="householdMember.socialSecurityCard.socialSecurityNumber!=''">
							<div ssn-Formatter-Non-Input ssn="householdMember.socialSecurityCard.socialSecurityNumber"></div>
						</div>


				  </td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.citizenship"/></td>
			      <td>
						<div ng-if="householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator">
		      				Yes
		      			</div>
		      			<div ng-if="!householdMember.citizenshipImmigrationStatus.citizenshipStatusIndicator">
							No
			      		</div>

                  </td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.ethnicityandrace"/></td>
			      <td>
						<div ng-repeat="ethnicity in householdMember.ethnicityAndRace.ethnicity">

								<div ng-if="(householdMember.ethnicityAndRace.ethnicity.length -1) == $index">
									{{ethnicity.label}}
								</div>
								<div ng-if="((householdMember.ethnicityAndRace.ethnicity.length -1) !=$index)">
									{{ethnicity.label}},&nbsp;
								</div>

						</div>
						<br/>
						<div ng-repeat="race in householdMember.ethnicityAndRace.race">

								<div ng-if="(householdMember.ethnicityAndRace.race.length -1) == $index">
									{{race.label}}
								</div>
								<div ng-if="(householdMember.ethnicityAndRace.race.length -1) != $index">
									{{race.label}},&nbsp;
								</div>

						</div>
					</td>
		      </tr>
		      <tr>
			      <td><spring:message code="label.lce.address"/></td>
			      <td>
		      		<div ng-if="householdMember.livesAtOtherAddressIndicator==false">
		      			{{householdMember.householdContact.homeAddress.streetAddress1}} {{householdMember.householdContact.homeAddress.streetAddress2}} {{householdMember.householdContact.homeAddress.city}} {{householdMember.householdContact.homeAddress.state}} {{householdMember.householdContact.homeAddress.postalCode}}  {{householdMember.householdContact.homeAddress.county}}
		      		</div>
		      		<div ng-if="householdMember.livesAtOtherAddressIndicator==true">
		      			{{householdMember.otherAddress.address.streetAddress1}} {{householdMember.otherAddress.address.streetAddress2}} {{householdMember.otherAddress.address.city}} {{householdMember.otherAddress.address.state}} {{householdMember.otherAddress.address.postalCode}}  {{householdMember.otherAddress.address.county}}</td>
		      		</div>
			       </td>
		      </tr>
		    </tbody>
	    </table>


	  <!-- BACK & NEXT BUTTON -->
	  <dl class="dl-horizontal pull-right">
	    <a role="button" tabindex="0" class="btn btn-secondary" ng-click="back()"><spring:message code="label.lce.back"/></a>
	    <a role="button" tabindex="0" class="btn btn-primary" ng-click="next()"><spring:message code="label.lce.next"/></a>
	  </dl>
	</div>
</script>