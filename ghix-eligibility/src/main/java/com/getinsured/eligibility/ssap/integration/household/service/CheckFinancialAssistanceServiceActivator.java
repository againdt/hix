package com.getinsured.eligibility.ssap.integration.household.service;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.ssap.integration.household.util.SSAPIntegrationConstants;

@Component(value = "checkFinancialAssistanceServiceActivator")
public class CheckFinancialAssistanceServiceActivator {

	private static final Logger LOGGER = Logger.getLogger(CheckFinancialAssistanceServiceActivator.class);

	@ServiceActivator
	public String receive(Message<String> message) throws ParseException {
		JSONParser parser = new JSONParser();
		String ssapJSON = message.getHeaders().get(SSAPIntegrationConstants.SSAP_JSON_HEADER).toString();
		JSONObject originalJSONObject = (JSONObject)parser.parse(ssapJSON);
		JSONObject ssapJSONObject = (JSONObject) originalJSONObject.get(SSAPIntegrationConstants.SINGLE_STREAMLINED_APPLICATION);
		Boolean applyingForFinancialAssistance = Boolean.valueOf(ssapJSONObject.get(SSAPIntegrationConstants.APPLYING_FOR_FINANCIAL_ASSISTANCE_INDICATOR).toString());
		if(applyingForFinancialAssistance) {
			return ssapJSON;
		} else {
			LOGGER.debug("Stop further processing as the user did not apply for financial assistance" );
			return null;
		}
	}
}
