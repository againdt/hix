package com.getinsured.hix;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.SHOPConfiguration;




@Controller
public class TopNavController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TopNavController.class);

	/**
	 * Faq controller
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/faq", method = {RequestMethod.GET})
	public String getAssistance(Model model, HttpServletRequest request) {
		LOGGER.info("TopNavController has been invoked");
		String userActiveRoleName=  (String) request.getSession().getAttribute("userActiveRoleName");
		//LOGGER.info("TopNavController has been invoked for role: "+userActiveRoleName);

		if("employee".equals(userActiveRoleName)){
			model.addAttribute("showMenu", false);
			return "faq/employee";
		}else if("employers".equalsIgnoreCase(userActiveRoleName) || "employer".equalsIgnoreCase(userActiveRoleName)){
			return "faq/employer";
		}else if("broker".equalsIgnoreCase(userActiveRoleName)){
			return "faq/broker";
		}else if("issuer_representative".equalsIgnoreCase(userActiveRoleName)){
			return "faq/issuer";
		}else if("individual".equalsIgnoreCase(userActiveRoleName)){
			return "faq/individual";
		}else if("admin".equalsIgnoreCase(userActiveRoleName) || "CSR".equalsIgnoreCase(userActiveRoleName)){
			return "faq/admin";
		}else if("broker_admin".equalsIgnoreCase(userActiveRoleName)){
			return "faq/broker_admin";
		}else if("issuer_admin".equalsIgnoreCase(userActiveRoleName)){
			return "faq/issuer_admin";
		}else if("assisterenrollmententityadmin".equalsIgnoreCase(userActiveRoleName)){
			return "faq/assisterenrollmententityadmin";
		}else if ("assister".equalsIgnoreCase(userActiveRoleName)) {
	      return "faq/assister";
	    }else if ("assisterenrollmententity".equalsIgnoreCase(userActiveRoleName)) {
	      return "faq/assisterenrollmententity";
	    }else{
			return "faq/home";
		}

	}

	@RequestMapping(value = "/faq/broker", method = {RequestMethod.GET})
	public String getFaqBroker(Model model, HttpServletRequest request) {
		return "faq/broker";
	}

	@RequestMapping(value = "/faq/individual", method = {RequestMethod.GET})
	public String getFaqIndividual(Model model, HttpServletRequest request) {
		return "faq/individual";
	}

	@RequestMapping(value = "/faq/assister", method = {RequestMethod.GET})
	public String getFaqAssister(Model model, HttpServletRequest request) {
		return "faq/assister";
	}

	@RequestMapping(value = "/faq/issuer", method = {RequestMethod.GET})
	public String getFaqIssuer(Model model, HttpServletRequest request) {
		return "faq/issuer";
	}

	@RequestMapping(value = "/faq/admin", method = {RequestMethod.GET})
	public String getFaqAdmin(Model model, HttpServletRequest request) {
		return "faq/admin";
	}

	@RequestMapping(value = "/faq/issuer_admin", method = {RequestMethod.GET})
	public String getFaqIssuerAdmin(Model model, HttpServletRequest request) {
		return "faq/issuer_admin";
	}

	@RequestMapping(value = "/faq/assisterenrollmententityadmin", method = {RequestMethod.GET})
	public String getFaqEntityAdmin(Model model, HttpServletRequest request) {
		return "faq/assisterenrollmententityadmin";
	}

	@RequestMapping(value = "/faq/assisterenrollmententity", method = {RequestMethod.GET})
	public String getFaqEntity(Model model, HttpServletRequest request) {
		return "faq/assisterenrollmententity";
	}

	@RequestMapping(value = "/faq/broker_admin", method = {RequestMethod.GET})
	public String getFaqBrokerAdmin(Model model, HttpServletRequest request) {
		return "faq/broker_admin";
	}

	@RequestMapping(value = "/faq/employer", method = {RequestMethod.GET})
	public String getFaqEmployer(Model model, HttpServletRequest request) {
		 model.addAttribute("maxShopEmployee",DynamicPropertiesUtil.getPropertyValue(SHOPConfiguration.SHOPConfigurationEnum.EMPLOYER_MAX_EMP_SHOP));
		return "faq/employer";
	}

	@RequestMapping(value = "/faq/employee", method = {RequestMethod.GET})
	public String getFaqEmployee(Model model, HttpServletRequest request) {
		model.addAttribute("showMenu", false);
		return "faq/employee";
	}

	@RequestMapping(value = "/faqhome", method = {RequestMethod.GET})
	public String getFaqHome(Model model, HttpServletRequest request) {
		return "faq/home";
	}

	@RequestMapping(value = "/disclosure/disclosureStatement", method = {RequestMethod.GET})
	public String getDisclosureStatement(Model model, HttpServletRequest request) {
		return "disclosure/disclosureStatement";
	}

	@RequestMapping(value = "/externalserver/ping",  method = RequestMethod.GET)
	@ResponseBody
	public String pingExternalServer(Model model, HttpServletRequest request, HttpServletResponse response) {
		String responseStr = "callBack({Status: 'OK'});";
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "GET");
		response.addHeader("Access-Control-Allow-Headers", "Origin");

		return responseStr;
	}
}
