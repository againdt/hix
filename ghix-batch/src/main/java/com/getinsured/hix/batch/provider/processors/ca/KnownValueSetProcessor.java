package com.getinsured.hix.batch.provider.processors.ca;

import com.getinsured.hix.batch.provider.InvalidOperationException;
import com.getinsured.hix.batch.provider.ProviderDataFieldProcessor;
import com.getinsured.hix.batch.provider.ProviderValidationException;
import com.getinsured.hix.batch.provider.ValidationContext;

public class KnownValueSetProcessor implements ProviderDataFieldProcessor {

	private ValidationContext context;
	private int index;

	@Override
	public Object process(Object objToBeValidated)
			throws ProviderValidationException, InvalidOperationException {
		String valueSet = context.getNamedConstraintField("value_set");
		if(valueSet == null){
			throw new InvalidOperationException(" Value set required for this this processor, please check you metadata file");
		}
		String[] values = valueSet.split(",");
		boolean valid = false;
		String validatedData = null;

		if (null != objToBeValidated) {

			validatedData = objToBeValidated.toString().trim();

			for (String s : values) {
	
				if (s.equals(validatedData)) {
					valid = true;
					break;
				}
			}
		}
		if(!valid){
			throw new ProviderValidationException(" Validation failed for value:"+objToBeValidated+" One of the following was expected:"+valueSet);
		}
		return validatedData;
	}

	@Override
	public void setIndex(int idx) {
		this.index = idx;
	}

	@Override
	public int getIndex() {
		// TODO Auto-generated method stub
		return this.index;
	}

	@Override
	public void setValidationContext(ValidationContext validationContext) {
		this.context = validationContext;
	}

	@Override
	public ValidationContext getValidationContext() {
		return this.context;
	}

}
