package com.getinsured.hix.indportal.dto.dm.application;

public class Assister {
	 private String applicationAssistorType;
	 private String assistorOrganizationId;
	 private String assistorOrganizationName;
	 private String assistorFirstName;
	 private String assistorLastName;
	 private String assistorMiddleName;
	 private String assistorSuffix;
	 private String assistorNationalProducerNumber;
	 private String assistorSystemUserName;


	 // Getter Methods 

	 public String getApplicationAssistorType() {
	  return applicationAssistorType;
	 }

	 public String getAssistorOrganizationId() {
	  return assistorOrganizationId;
	 }

	 public String getAssistorOrganizationName() {
	  return assistorOrganizationName;
	 }

	 public String getAssistorFirstName() {
	  return assistorFirstName;
	 }

	 public String getAssistorLastName() {
	  return assistorLastName;
	 }

	 public String getAssistorMiddleName() {
	  return assistorMiddleName;
	 }

	 public String getAssistorSuffix() {
	  return assistorSuffix;
	 }

	 public String getAssistorNationalProducerNumber() {
	  return assistorNationalProducerNumber;
	 }

	 public String getAssistorSystemUserName() {
	  return assistorSystemUserName;
	 }

	 // Setter Methods 

	 public void setApplicationAssistorType(String applicationAssistorType) {
	  this.applicationAssistorType = applicationAssistorType;
	 }

	 public void setAssistorOrganizationId(String assistorOrganizationId) {
	  this.assistorOrganizationId = assistorOrganizationId;
	 }

	 public void setAssistorOrganizationName(String assistorOrganizationName) {
	  this.assistorOrganizationName = assistorOrganizationName;
	 }

	 public void setAssistorFirstName(String assistorFirstName) {
	  this.assistorFirstName = assistorFirstName;
	 }

	 public void setAssistorLastName(String assistorLastName) {
	  this.assistorLastName = assistorLastName;
	 }

	 public void setAssistorMiddleName(String assistorMiddleName) {
	  this.assistorMiddleName = assistorMiddleName;
	 }

	 public void setAssistorSuffix(String assistorSuffix) {
	  this.assistorSuffix = assistorSuffix;
	 }

	 public void setAssistorNationalProducerNumber(String assistorNationalProducerNumber) {
	  this.assistorNationalProducerNumber = assistorNationalProducerNumber;
	 }

	 public void setAssistorSystemUserName(String assistorSystemUserName) {
	  this.assistorSystemUserName = assistorSystemUserName;
	 }
	}
