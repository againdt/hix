package com.getinsured.eligibility.indportal.dto;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.getinsured.hix.dto.enrollment.EnrollmentShopDTO;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;

public class HouseholdData {

	private boolean issep;
	private Date coverageDate;
	private String keepOnlyFlag;
	private String terminationFlag;
	private String dentalTerminationFlag;	
	private Map<String, EnrollmentShopDTO> enrollmentsByTypeMap;
	private Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap;
	private SingleStreamlinedApplication applicationData;
	private SsapApplicationEvent ssapApplicationEvent;
	private long priorSsapApplicationId;
	private String productType;
	private String enrollmentType;
	
	public boolean isIssep() {
		return issep;
	}
	public void setIssep(boolean issep) {
		this.issep = issep;
	}
	public Date getCoverageDate() {
		return coverageDate;
	}
	public void setCoverageDate(Date coverageDate) {
		this.coverageDate = coverageDate;
	}
	public String getKeepOnlyFlag() {
		return keepOnlyFlag;
	}
	public void setKeepOnlyFlag(String keepOnlyFlag) {
		this.keepOnlyFlag = keepOnlyFlag;
	}
	public String getTerminationFlag() {
		return terminationFlag;
	}
	public void setTerminationFlag(String terminationFlag) {
		this.terminationFlag = terminationFlag;
	}
	public String getDentalTerminationFlag() {
		return dentalTerminationFlag;
	}
	public void setDentalTerminationFlag(String dentalTerminationFlag) {
		this.dentalTerminationFlag = dentalTerminationFlag;
	}
	public Map<String, EnrollmentShopDTO> getEnrollmentsByTypeMap() {
		return enrollmentsByTypeMap;
	}
	public void setEnrollmentsByTypeMap(Map<String, EnrollmentShopDTO> enrollmentsByTypeMap) {
		this.enrollmentsByTypeMap = enrollmentsByTypeMap;
	}
	public Map<Long, SsapApplicantEvent> getSsapApplicantEventPrimaryKeyMap() {
		return ssapApplicantEventPrimaryKeyMap;
	}
	public void setSsapApplicantEventPrimaryKeyMap(Map<Long, SsapApplicantEvent> ssapApplicantEventPrimaryKeyMap) {
		this.ssapApplicantEventPrimaryKeyMap = ssapApplicantEventPrimaryKeyMap;
	}
	public SingleStreamlinedApplication getApplicationData() {
		return applicationData;
	}
	public void setApplicationData(SingleStreamlinedApplication applicationData) {
		this.applicationData = applicationData;
	}
	public SsapApplicationEvent getSsapApplicationEvent() {
		return ssapApplicationEvent;
	}
	public void setSsapApplicationEvent(SsapApplicationEvent ssapApplicationEvent) {
		this.ssapApplicationEvent = ssapApplicationEvent;
	}
	public long getPriorSsapApplicationId() {
		return priorSsapApplicationId;
	}
	public void setPriorSsapApplicationId(long priorSsapApplicationId) {
		this.priorSsapApplicationId = priorSsapApplicationId;
	}
	public String getProductType() {
		return productType;
	}
	public void setProductType(String productType) {
		this.productType = productType;
	}
	public String getEnrollmentType() {
		return enrollmentType;
	}
	public void setEnrollmentType(String enrollmentType) {
		this.enrollmentType = enrollmentType;
	}
	
}
