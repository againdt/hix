package com.getinsured.iex.hub.ridp.wrapper;

import java.io.IOException;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.getinsured.iex.hub.GHIXApplicationContext;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.ObjectFactory;
import com.getinsured.iex.hub.ridp.cms.dsh.ridp.extension._1.VerificationAnswerAndQuestionNumberType;
import com.google.gson.Gson;


public class VerificationAnswerWrapper {

	private static ObjectFactory factory;
	private VerificationAnswerAndQuestionNumberType verificationAnswerAndQuestionNumberType;

	private String questionNumber = null;
	private String verificationAnswer = null;

	private static Gson gson ;
	
	static{
		gson = (Gson) GHIXApplicationContext.getBean("iex_platformGson");
	}
	
	public String getQuestionNumber() {
		return questionNumber;
	}

	public void setQuestionNumber(String questionNumber) {
		if (factory == null) {
			factory = new ObjectFactory();
		}
		if (verificationAnswerAndQuestionNumberType == null) {
			verificationAnswerAndQuestionNumberType = factory
					.createVerificationAnswerAndQuestionNumberType();
		}
		verificationAnswerAndQuestionNumberType
				.setVerificationQuestionNumber(questionNumber);
		this.questionNumber = questionNumber;
	}

	public String getVerificationAnswer() {
		return verificationAnswer;
	}

	public void setVerificationAnswer(String verificationAnswer) {
		if (factory == null) {
			factory = new ObjectFactory();
		}
		if (verificationAnswerAndQuestionNumberType == null) {
			verificationAnswerAndQuestionNumberType = factory
					.createVerificationAnswerAndQuestionNumberType();
		}
		verificationAnswerAndQuestionNumberType
				.setVerificatonAnswer(verificationAnswer);
		this.verificationAnswer = verificationAnswer;
	}

	@JsonIgnore
	public VerificationAnswerAndQuestionNumberType getSystemRepresentation() {
		return this.verificationAnswerAndQuestionNumberType;
	}

	public String toJsonString() throws JsonProcessingException {
		VerificationAnswerWrapper verAnsAndQuestion = new VerificationAnswerWrapper();
		verAnsAndQuestion.setQuestionNumber(this.questionNumber);
		verAnsAndQuestion.setVerificationAnswer(this.verificationAnswer);
		return gson.toJson(verAnsAndQuestion, VerificationAnswerWrapper.class);
	}

	public static VerificationAnswerWrapper fromJsonString(String jsonString)
			throws IOException {
		return gson.fromJson(jsonString, VerificationAnswerWrapper.class);
	}
}
