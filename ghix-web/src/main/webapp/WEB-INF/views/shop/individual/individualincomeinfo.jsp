<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<c:set var="individualUserId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/chosen.css" />" />
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/inbox.css" />" />

<!-- File upload scripts -->
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript"
	src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>

<script type="text/javascript">
$(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href = $(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
      }
    });
  });
  
  function closeIFrame() {
    $("#newcommentiframe").remove();
    window.location.href = '/hix/individual/individualcomments/'+'${individualUserId}';
  }
  
  function closeCommentBox() {
	  $("#newcommentiframe").remove();	  
	  var url = '/hix/individual/individualcomments/'+'${individualUserId}';
	  window.location.assign(url);
  }
</script>

<div class="row-fluid">
	<div style="font-size: 14px; color: red">
		<c:if test="${errorMsg != 'false'}">
			<c:out value="${errorMsg}"></c:out>
		</c:if>
	</div>
</div>


<!--  Latest UI -->
<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a href="<c:url value="/broker/individuals"/>"><spring:message
							code="label.individuals" /></a></li>
				<li><spring:message code="label.brkactive"/></li>
				<li>${individualUser.firstName} ${individualUser.lastName}</li>
				<li><spring:message code="label.indIncomeInformation"/></li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<c:if test="${message != null}">
		<div class="errorblock alert alert-info">
			<p>${message}</p>
		</div>
	</c:if>
	<h1>${individualUser.firstName} ${individualUser.lastName}</h1>
	<div class="row-fluid">
		<div id="sidebar" class="span3">
				<h4 class="header"><spring:message code="label.indAboutthisHousehold"/></h4>
			<ul class="nav nav-list">
				<li class="link"><a
					href="/hix/individual/individualcase/${individualUserId}"><spring:message code="label.employeeDetails.Summary"/></a></li>
				<li class="active"><a href="#"><spring:message code="label.indIncomeInformation"/></a></li>
<%-- 				<li class="link"><c:url value="/inbox/secureInboxSearch"
						var="searchURL">
						<c:param name="searchText"
							value="${individualUser.firstName} ${individualUser.lastName}" />
					</c:url> <a href="${searchURL}">Messages</a></li> --%>
				<li class="link"><a
					href="/hix/individual/individualcomments/${individualUserId}"><spring:message code="label.employeeDetails.Comments"/></a></li>
			</ul>
			<br>
			<div class="header"><h4><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
			</div>
			<ul class="nav nav-list">
				<li><a href="#viewindModal" role="button" class=""
					data-toggle="modal"><i class="icon-eye-open"></i> <spring:message code="label.indViewIndividualAccount"/></a></li>
				<!-- 							<li><a href="#"><i class="icon-eye-open"></i>View Individual Account</a></li> 
				<li><a href="#new-msg" data-toggle="modal"
					onclick="resetForm();saveDraftOfMessage();"><i
						class="icon-envelope-unread"></i> Compose Message</a></li> -->
				<li><a name="addComment"
					href="<c:url value="/individual/newcomment?target_id=${individualUserId}&target_name=DESIGNATEBROKER&individualName=${individualUser.firstName} ${individualUser.lastName}"/>"
					id="addComment" class="newcommentiframe"> <i
						class="icon-comment"></i><spring:message code="label.employeeDetails.NewComments"/>
				</a></li>
			</ul>

		</div>
		<!-- Modal -->

		<div id="rightpanel" class="span9">
			<form class="form-horizontal" id="" name="" action="" method="POST">
				<div class="header">
					<h4><spring:message code="label.employeeDetails.Summary"/></h4>
				</div>
				<table class="table table-border-none">
					<tbody>
						<tr>
							<td class="span3 txt-right"><spring:message code="label.indTotalProjectedHouseholdIncome"/></td>
							<td><strong>&#36;35,250</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code="label.enrollment.CurrentPlan"/></td>
							<td><strong>N&#92;A</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code="label.agent.employers.EligibilityStatus"/></td>
							<td><strong><spring:message code="label.brkDenied"/> on 7&#92;11</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code="label.indMedicaidEligibility"/></td>
							<td><strong></strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code="label.indCurrentMonthlyIncome"/></td>
							<td><strong>&#36;35,250</strong></td>
						</tr>
						<tr>
							<td class="txt-right"><spring:message code="label.indMedicaidFamilySize"/></td>
							<td><strong>5</strong></td>
						</tr>
					</tbody>
				</table>
			</form>

		</div>
	</div>
</div>


<!-- Modal -->
 <!-- Modal -->
      <div id="viewindModal" class="modal hide fade" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">&times;</button>
          <h3 id="myModalLabel"><spring:message code="label.indViewIndividualAccount"/></h3>
        </div>
        <div class="modal-body">
          <p><spring:message code="label.indClickingIndividualView"/> ${individualUser.firstName} ${individualUser.lastName}. <spring:message code="label.IndThroughThisPortal"/>
          </p>
          <p><spring:message code="label.indProceedtoIndividualview"/></p>
        </div>
        
        <form action="<c:url value='/account/user/switchUserRole'/>" id="frmPopup" name="frmPopup">
        	<div class="modal-footer clearfix">       	
	 				<input class="pull-left"  type="checkbox" id="showPopupInFuture" name="showPopupInFuture"/>
	    			<div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
				    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
					<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="individual" />
					<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${individualUser.id}" />
					<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${individualUser.firstName} ${individualUser.lastName}" />
				   <button class="btn btn-primary" type="submit" ><spring:message code="label.indIndividualView"/></button >
			</div>			
		</form>
      </div>
      <!-- Modal end -->
<!--  Latest UI -->
