import React, { Fragment } from 'react';
import { FormattedMessage } from 'react-intl';

const Page3_31_E1 = () => {
    return (
        <Fragment>
            <div className="usa-alert usa-alert-info alert--info">
                <div className="usa-alert-body" >
                    <p className="usa-alert-text">
                        <strong>
                            <FormattedMessage
                                id="ssap.incomeGetReady.msg.one"
                                defaultMessage="We ask for current information for everyone in your family                                and household to make sure you get the most benefits possible"
                            />
                        </strong><br/>
                        <FormattedMessage
                            id="ssap.incomeGetReady.msg.two"
                            defaultMessage="Before you start, please take a moment now to gather the information listed below"
                        />
                    </p>
                </div>
            </div>
            <div className="subsection">
                <p>
                    <i>
                        <FormattedMessage
                            id="ssap.incomeGetReady.msg.three"
                            defaultMessage="All fields on this application marked with an asterisk (*)
                              are required unless otherwise indicated."
                        />
                    </i>
                </p>
                <p>
                    <FormattedMessage
                        id="ssap.incomeGetReady.msg.four"
                        defaultMessage="You may need:"
                    />
                </p>
                <ul>
                    <li>
                        <FormattedMessage
                            id="ssap.incomeGetReady.msg.five"
                            defaultMessage="Pay stubs"
                        />
                    </li>
                    <li>
                        <FormattedMessage
                            id="ssap.incomeGetReady.msg.six"
                            defaultMessage="W-2 forms"
                        />
                    </li>
                    <li>
                        <FormattedMessage
                            id="ssap.incomeGetReady.msg.seven"
                            defaultMessage="Information about income"
                        />
                    </li>
                </ul>
            </div>
        </Fragment>
    );
};

Page3_31_E1.defaultProps = {
    name: 'Page3_31_E1'
};

export default Page3_31_E1;