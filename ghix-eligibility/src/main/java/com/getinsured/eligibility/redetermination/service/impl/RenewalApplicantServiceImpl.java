package com.getinsured.eligibility.redetermination.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.enums.RenewalStatus;
import com.getinsured.eligibility.redetermination.service.RenewalApplicantService;
import com.getinsured.eligibility.renewal.util.RenewalConstants;
import com.getinsured.hix.model.LookupValue;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.lookup.service.LookupService;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.model.RenewalApplicant;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.repository.RenewalApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;

@Service
public class RenewalApplicantServiceImpl implements RenewalApplicantService,RenewalConstants {

	private static final Logger LOGGER = LoggerFactory.getLogger(RenewalApplicantServiceImpl.class);
	
	@Autowired
	private RenewalApplicantRepository renewalApplicantRepository;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;
	
	@Autowired
	private LookupService lookupService;

	@Override
	public RenewalApplicant saveRenewalApplicant(RenewalApplicant renewalApplicant) {
		return renewalApplicantRepository.save(renewalApplicant);
	}
	
	@Override
	public Integer getToConsiderMemerCount(RenewalStatus renewalStatus,String planType, Long ssapApplicationId)
	{
		List<RenewalApplicant> list =  renewalApplicantRepository.findMembersByPlanType(renewalStatus, planType, ssapApplicationId);
		return list.size();
	}
	
	@Override
	public void updateHealthRenewalStatus(List<String> applicantGUIdList,Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException 
	{
		if(CollectionUtils.isNotEmpty(applicantGUIdList) && renewalStatus != null) 
		{
			String useMigratedEnrollments = DynamicPropertiesUtil.getPropertyValue(USE_MIGRATION_ENROLLMENTS);
			boolean useExternalMemberId = TRUE.equalsIgnoreCase(useMigratedEnrollments) ? true : false;
			SsapApplicant ssapApplicant = null;
			
			for(String applicantGuid : applicantGUIdList)
			{
				if(useExternalMemberId)
				{
					ssapApplicant = ssapApplicantRepository.findBySsapApplicantGuidAndSsapApplicationId(applicantGuid, ssapApplicationId);
					LOGGER.info("SsapApplicant with applicantGuid: {}, ssapApplicationId: {}, ExternalApplicantId: {}", applicantGuid, ssapApplicationId, ssapApplicant.getExternalApplicantId());
					applicantGuid = ssapApplicant.getExternalApplicantId();
				}
				
				RenewalApplicant ssapRenewalApplicant = renewalApplicantRepository.findByMemberIdAndPlanType(applicantGuid,HEALTH,Long.valueOf(ssapApplicationId));
				if(ssapRenewalApplicant != null) 
				{
					ssapRenewalApplicant.setRenewalStatus(renewalStatus);
					ssapRenewalApplicant.setGroupRenewalStatus(renewalStatus);
					if(RenewalStatus.MANUAL_ENROLMENT.equals(renewalStatus))
					{
						ssapRenewalApplicant.setGroupReasonCode(getReasonCode(USER_SHOPPED_PLANS));
						ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(USER_SHOPPED_PLANS));
					}
					ssapRenewalApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
					renewalApplicantRepository.save(ssapRenewalApplicant);
				}
			}
		}
		else
		{
			LOGGER.error("Error: Member Id List null/empty while updating RenewalStatus." + ssapApplicationId );
			throw new GIException("Error: Member Id List null/empty while updating RenewalStatus. ");
		}
	}
	
	private Integer getReasonCode(String lookupValueCode)
	{
		Integer reasonCode =  lookupService.getlookupValueIdByTypeANDLookupValueCode(RENEWALS,lookupValueCode );
		return reasonCode;
	}
	
	@Override
	public void updateDentalRenewalStatus(Long ssapApplicationId,RenewalStatus renewalStatus) throws GIException {
		List<RenewalApplicant> renewalApplicantList = renewalApplicantRepository.findMembersByPlanType(RenewalStatus.TO_CONSIDER,DENTAL, ssapApplicationId);
		if(CollectionUtils.isNotEmpty(renewalApplicantList) && renewalStatus != null) 
		{
			for(RenewalApplicant ssapRenewalApplicant : renewalApplicantList)
			{
				ssapRenewalApplicant.setRenewalStatus(renewalStatus);
				if(RenewalStatus.MANUAL_ENROLMENT.equals(renewalStatus))
				{
					ssapRenewalApplicant.setMemberFalloutReasonCode(getReasonCode(USER_SHOPPED_PLANS));
				}
				ssapRenewalApplicant.setLastUpdateTimestamp(new Timestamp(new TSDate().getTime()));
				renewalApplicantRepository.save(ssapRenewalApplicant);
			}
		}
	}

	@Override
	public Map<Long, String> getEnrollmentsWithGroupReasonCodes(Long ssapApplicationId) {
		List<Object[]> renewalApplicantData = renewalApplicantRepository.getEnrollmentWithGroupReasonCodes(ssapApplicationId);
		Map<Long, String> codes = new HashMap<Long, String>();
		for (Object[] data : renewalApplicantData) {
			LookupValue lookupValue = lookupService.findLookupValuebyId(((Integer) data[1]).intValue());
			if (lookupValue != null) {
				codes.put((Long) data[0], lookupValue.getLookupValueCode());
			}
		}
		return codes;
	}

	@Override
	public Map<Long, List<String>> getEnrollmentsWithMemberAndFalloutReasonCodes(Long ssapApplicationId, String planType) {
		List<Object[]> renewalApplicantData = renewalApplicantRepository.getEnrollmentWithMembersAndFallOutReasonCodesDetail(ssapApplicationId, planType);
		Map<Long, List<String>> codes = new HashMap<Long, List<String>>();
		
		for (Object[] data : renewalApplicantData) {
			Long enrollmentId = (Long) data[0];
			String memberId = data[1].toString();
			Integer memberFalloutReasonCode = (Integer) data[2];
			String reasonCode = null;

			LookupValue lookupValue = lookupService.findLookupValuebyId(memberFalloutReasonCode.intValue());
			if (lookupValue != null) {
				reasonCode = lookupValue.getLookupValueLabel();
			}

			List<String> falloutCodes = null;
			if (!codes.containsKey(enrollmentId)) {
				falloutCodes = new ArrayList<>();
			} else {
				falloutCodes = codes.get(enrollmentId);
			}
			falloutCodes.add(memberId + "," + reasonCode);
			codes.put(enrollmentId, falloutCodes);
		}
		
		return codes;
	}
}
