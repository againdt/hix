package com.getinsured.eligibility.externalnotices.service;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.externalnotices.util.ExternalNoticeUtil;
import com.getinsured.eligibility.externalnotices.util.ExternalNoticesRestUtil;
import com.getinsured.hix.dto.externalnotices.ExternalNoticesResponseDTO;
import com.getinsured.hix.externalnotice.util.ExternalNoticeConstants;
import com.getinsured.hix.model.ExternalNotice;
import com.getinsured.hix.model.ExternalNotice.ExternalNoticesStatus;
import com.getinsured.hix.model.ExternalNoticeDto;
import com.getinsured.hix.model.ExternalNoticeDto.DocumentCategory;
import com.getinsured.hix.model.ExternalNoticeRequest;
import com.getinsured.hix.platform.notices.NoticeService;
import com.getinsured.hix.platform.repository.IExternalNoticeRepository;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.exception.GIException;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.timeshift.util.TSDate;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

@Service("externalNoticesService")
public class ExternalNoticesServiceImpl implements ExternalNoticesService {
	
	private static final Logger LOGGER = Logger.getLogger(ExternalNoticesService.class);
	
	private SsapApplicationRepository ssapApplicationRepository;
	private IExternalNoticeRepository iExternalNoticesRepository;
	private NoticeService noticeService;
	private Gson platformGson;
	private ExternalNoticesRestUtil externalNoticesUtil;
	
	@Autowired
	public ExternalNoticesServiceImpl(SsapApplicationRepository ssapApplicationRepository,
			IExternalNoticeRepository iExternalNoticesRepository, NoticeService noticeService, Gson platformGson, ExternalNoticesRestUtil externalNoticesUtil) {
		this.ssapApplicationRepository = ssapApplicationRepository;
		this.iExternalNoticesRepository = iExternalNoticesRepository;
		this.noticeService = noticeService;
		this.platformGson = platformGson;
		this.externalNoticesUtil = externalNoticesUtil;
	}

	@Override
	@Transactional
	public ExternalNoticesResponseDTO getSsapApplicationDetails(Long ssapApplicationId) { 
			ExternalNoticesResponseDTO externalNoticesResponse = new ExternalNoticesResponseDTO(); 
			if (ssapApplicationId != null) {
				Object[] ssapApplicationNoticeDetails = ssapApplicationRepository.getSsapApplicationDetails(ssapApplicationId);
				if (ssapApplicationNoticeDetails != null && ssapApplicationNoticeDetails.length > 0) {
						Object[] arrayObject = ssapApplicationNoticeDetails != null && ssapApplicationNoticeDetails.length > 0 ? (Object[]) ssapApplicationNoticeDetails[ExternalNoticeConstants.ZERO] : null;
						if (arrayObject != null && arrayObject.length == 6) {
							externalNoticesResponse.setMnsureId(arrayObject[ExternalNoticeConstants.ZERO].toString());
							externalNoticesResponse.setFirstName(arrayObject[ExternalNoticeConstants.ONE].toString());
							externalNoticesResponse.setLastName(arrayObject[ExternalNoticeConstants.TWO].toString());
							externalNoticesResponse.setDateOfBirth(DateUtil.dateToString((Date) arrayObject[ExternalNoticeConstants.THREE],ExternalNoticeConstants.DOB_FORMAT));
							if(arrayObject[ExternalNoticeConstants.FOUR] != null) {
								externalNoticesResponse.setIntegratedCaseId(arrayObject[ExternalNoticeConstants.FOUR].toString());
							}
							externalNoticesResponse.setCoverageYear((arrayObject[ExternalNoticeConstants.FIVE]).toString());
						}
				}
			}
			return externalNoticesResponse;
	}
	
	@Override
	@Transactional
	public void processBatchInitRecords(ExternalNotice externalNotices) {
		if (externalNotices != null) {
			ExternalNoticesResponseDTO externalNoticesResponseDTO = getSsapApplicationDetails(new Long(externalNotices.getSsapId()));
			if (externalNoticesResponseDTO != null && externalNoticesResponseDTO.getMnsureId() != null && externalNoticesResponseDTO.getIntegratedCaseId() == null) {
				ExternalNoticeDto externalNoticeDto = platformGson.fromJson(externalNotices.getRequestPayload(),ExternalNoticeDto.class);
				ExternalNoticeRequest externalNoticeRequest = createExternalNoticeRequest(externalNoticesResponseDTO, externalNoticeDto);
				externalNotices.setRequestPayload(platformGson.toJson(externalNoticeRequest));
				externalNotices.setStatusType(ExternalNoticesStatus.valueOf("QUEUED"));
				iExternalNoticesRepository.saveAndFlush(externalNotices);
			} else if (externalNoticesResponseDTO != null && externalNoticesResponseDTO.getIntegratedCaseId() != null) {
				processBatchQueuedRecords(externalNotices);
			}
		}
	}

	@Override
	@Transactional
	public void processBatchQueuedRecords(ExternalNotice externalNotices) {
		ExternalNoticesResponseDTO externalNoticesResponseDTO = getSsapApplicationDetails(new Long(externalNotices.getSsapId()));
		if (externalNoticesResponseDTO != null && externalNoticesResponseDTO.getIntegratedCaseId() != null) {
			try {
				ExternalNoticeDto externalNoticeDto = platformGson.fromJson(externalNotices.getRequestPayload(),ExternalNoticeDto.class);
				ExternalNoticeRequest externalNoticeRequest = createExternalNoticeRequest(externalNoticesResponseDTO,externalNoticeDto);
				
				externalNoticeRequest.setIntegratedCaseId(externalNoticesResponseDTO.getIntegratedCaseId());
				externalNoticeRequest.setDocument(noticeService.getPDFContentAsBase64Encoded(externalNoticeDto.getDocument()));
				String externalNoticeResponse = externalNoticesUtil.postToExternalSystem(externalNoticeRequest);
				String status = ExternalNoticeConstants.STATUS_FAILED;	
				if (externalNoticeResponse != null) {
					JsonParser parser = new JsonParser();
					JsonElement jsonTree = parser.parse(externalNoticeResponse);
					if (jsonTree.isJsonObject()) {
						JsonObject jsonObject = jsonTree.getAsJsonObject();
						if (jsonObject.has(ExternalNoticeConstants.ERROR_RESPONSE)) {
							status = ExternalNoticeConstants.STATUS_FAILED;
						} else if (jsonObject.has(ExternalNoticeConstants.RESPONSE)) {
							JsonElement f1 = jsonObject.get(ExternalNoticeConstants.RESPONSE);
							if (f1.isJsonObject()) {
								String code = f1.getAsJsonObject().get(ExternalNoticeConstants.CODE).getAsString();
								if (code.equalsIgnoreCase(ExternalNoticeConstants.ERROR_RESPONSE_200)) {
									status = ExternalNoticeConstants.STATUS_SENT;
								} else {
									status = ExternalNoticeConstants.STATUS_FAILED;
								}
							}
						}
					}
				}
				externalNotices.setExternalHouseholdCaseId(externalNoticesResponseDTO.getIntegratedCaseId());
				externalNoticeRequest.setDocument(externalNoticeDto.getDocument());
				externalNotices.setRequestPayload(platformGson.toJson(externalNoticeRequest));
				externalNotices.setResponsePayload(platformGson.toJson(externalNoticeResponse));
				externalNotices.setStatusType(ExternalNoticesStatus.valueOf(status));
				iExternalNoticesRepository.saveAndFlush(externalNotices);
			} catch (Exception e) {
				LOGGER.info("Exception occured while processing External Notice", e);
				externalNotices.setExternalHouseholdCaseId(externalNoticesResponseDTO.getIntegratedCaseId());
				externalNotices.setStatusType(ExternalNoticesStatus.valueOf(ExternalNoticeConstants.STATUS_FAILED));
				String errorMsg=ExternalNoticeUtil.shortStackTrace(e, ExternalNoticeConstants.THREE_NINE_NINE_NINE);
				externalNotices.setResponsePayload(errorMsg);
				if(externalNotices.getRetryCount()<=3)
				{
				//increment retry count	
				externalNotices.setRetryCount(externalNotices.getRetryCount()+1);
				}
				iExternalNoticesRepository.saveAndFlush(externalNotices);
			}
		}
	}
	
	private ExternalNoticeRequest createExternalNoticeRequest(ExternalNoticesResponseDTO externalNoticesResponseDTO,ExternalNoticeDto externalNoticeDto) {
		ExternalNoticeRequest externalNoticeRequest = new ExternalNoticeRequest();
		externalNoticeRequest.setMnsureId(externalNoticesResponseDTO.getMnsureId());
		externalNoticeRequest.setCoverageYear(externalNoticesResponseDTO.getCoverageYear());
		externalNoticeRequest.setDateOfBirth(externalNoticesResponseDTO.getDateOfBirth());
		externalNoticeRequest.setFirstName(externalNoticesResponseDTO.getFirstName());
		externalNoticeRequest.setLastName(externalNoticesResponseDTO.getLastName());
		externalNoticeRequest.setDocumentName(externalNoticeDto.getDocumentName());
		externalNoticeRequest.setDocument(externalNoticeDto.getDocument());
		externalNoticeRequest.setDocumentCategory((externalNoticeDto.getDocumentName().contains(ExternalNoticeConstants.PDF_1095) ? DocumentCategory.TAX_FORMS.getDescription() : DocumentCategory.QHP_ENROLLMENT_NOTICE.getDescription()));
		externalNoticeRequest.setNoticeId(externalNoticeDto.getNoticeId());
		externalNoticeRequest.setTransactionId(externalNoticeDto.getNoticeId());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		externalNoticeRequest.setTransactionTimestamp(sdf.format(new TSDate()));
		return externalNoticeRequest;
	}
}
