package com.getinsured.hix.broker.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.agency.repository.IAgencyAssistantRepository;
import com.getinsured.hix.dto.broker.BrokerDTO;
import com.getinsured.hix.dto.entity.DesignateEntityDTO;
import com.getinsured.hix.dto.entity.EntityRequestDTO;
import com.getinsured.hix.dto.entity.EntityResponseDTO;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.BankInfo;
import com.getinsured.hix.model.Broker;
import com.getinsured.hix.model.Broker.certification_status;
import com.getinsured.hix.model.DelegationRequest;
import com.getinsured.hix.model.DelegationResponse;
import com.getinsured.hix.model.DesignateBroker;
import com.getinsured.hix.model.PaymentMethods;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant;
import com.getinsured.hix.model.agency.assistant.AgencyAssistant.ApprovalStatus;
import com.getinsured.hix.platform.feature.GiFeature;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.security.service.jpa.UserServiceImpl;
import com.getinsured.hix.platform.util.GhixEndPoints.AHBXEndPoints;
import com.getinsured.hix.platform.util.GhixPlatformEndPoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;

@Service("brokerIndTriggerService")
public class BrokerIndTriggerServiceImpl implements BrokerIndTriggerService {
	
	private static final String DATE_FORMAT_PLAIN = "MMddyyyy";
	
	@Autowired
	private RestTemplate restTemplate;
	@Autowired
	private RoleService roleService;
	@Autowired
	private IAgencyAssistantRepository agencyAssistantRepository;
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

	@GiFeature("ahbx.aee.ind35.enabled")
	@Override
	public void triggerInd35(Broker broker, PaymentMethods paymentMethods) {
		// TODO Auto-generated method stub
				LOGGER.info("Start of Ind35 call");
				if(broker == null){
					throw new GIRuntimeException("Exception occured while processing IND 35");
				}
				List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();
				EntityRequestDTO entityRequestDTO = populateBrokerDto(broker, paymentMethods);
				entityRequestDTOList.add(entityRequestDTO);

				try {
					LOGGER.info("IND35: Sending Broker Details to Ghix-AHBX");

					String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);

					String wsResponse = restTemplate.postForObject(GhixPlatformEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL,
							strEntityRequestDTOList, String.class);

					LOGGER.info("IND35: Successfully sent Broker Details over Rest call to Ghix-Ahbx.");

				} catch (Exception exception) {
					LOGGER.error("IND35: Send Broker Details Web Service failed: Exception is :", exception);
				}
				LOGGER.info("end of Ind35 call");
	}
	
	@GiFeature("ahbx.aee.ind35.enabled")
	@Override
	public void triggerInd35(AgencyAssistant agencyAssistant) {
		LOGGER.info("Start of Ind35 call");
		if(agencyAssistant == null){
			throw new GIRuntimeException("Exception occured while processing IND 35");
		}
		List<EntityRequestDTO> entityRequestDTOList = new ArrayList<EntityRequestDTO>();
		EntityRequestDTO entityRequestDTO = populateAgencyAssistantDto(agencyAssistant);
		entityRequestDTOList.add(entityRequestDTO);

		try {
			LOGGER.info("IND35: Sending Agency Assistant Details to Ghix-AHBX");

			String strEntityRequestDTOList = GhixUtils.xStreamHibernateXmlMashaling().toXML(entityRequestDTOList);

			restTemplate.postForObject(GhixPlatformEndPoints.AHBXEndPoints.SEND_ENTITY_DETAIL,
					strEntityRequestDTOList, String.class);

			LOGGER.info("IND35: Successfully sent Agency Assistant Details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {
			LOGGER.error("IND35: Send Agency Assistant Details Web Service failed: Exception is :", exception);
		}
		LOGGER.info("end of Ind35 call");
	}
	
	/**
	 * Populates {@link BrokerDTO} from {@link Broker} to send it to AHBX
	 * through REST API
	 *
	 * @param broker
	 * @return {@link BrokerDTO}
	 */
	private EntityRequestDTO populateBrokerDto(Broker broker, PaymentMethods paymentMethods) {
		EntityRequestDTO entityRequestDTO = new EntityRequestDTO();

		entityRequestDTO.setRecordId(broker.getId());
		entityRequestDTO.setRecordType("Agent");
		entityRequestDTO.setRecordIndicator("Update");

		entityRequestDTO.setFirstName(broker.getUser() !=null?broker.getUser().getFirstName():broker.getFirstName());
		entityRequestDTO.setLastName(broker.getUser() !=null?broker.getUser().getLastName():broker.getLastName());

		entityRequestDTO.setBusinessLegalName(broker.getCompanyName());

		// Applying as quick fix to HIX-10371
		entityRequestDTO.setFunctionalIdentifier(new Long(broker.getBrokerNumber()));
		entityRequestDTO.setFederalEIN(broker.getFederalEIN());

		if (broker.getMailingLocation() != null) {
			entityRequestDTO.setAddressLine1(broker.getLocation().getAddress1());
			entityRequestDTO.setAddressLine2(broker.getLocation().getAddress2());
			entityRequestDTO.setCity(broker.getLocation().getCity());
			entityRequestDTO.setState(broker.getLocation().getState());
			entityRequestDTO.setZipCode(String.valueOf(broker.getLocation().getZip()));
		} else {
			entityRequestDTO.setAddressLine1("");
			entityRequestDTO.setAddressLine2("");
			entityRequestDTO.setCity("");
			entityRequestDTO.setState("");
			entityRequestDTO.setZipCode("");
		}

		String contactNumber = "";
		if (broker.getContactNumber() != null && broker.getContactNumber().contains("-")) {
			contactNumber = broker.getContactNumber().replaceAll("-", "");
		} else {
			contactNumber = broker.getContactNumber();
		}
		entityRequestDTO.setPhoneNumber(contactNumber);
		entityRequestDTO.setEmail(broker.getPersonalEmailAddress());

		entityRequestDTO.setAgentLicenseNum(broker.getLicenseNumber()); // !=
																		// null
																		// ?
																		// broker.getLicenseNumber().substring(0,
																		// 6) :
																		// "");
		populateCertificationInformation(broker, entityRequestDTO);

		populatePaymentMethod(paymentMethods, entityRequestDTO);

		return entityRequestDTO;
	}
	
	private EntityRequestDTO populateAgencyAssistantDto(AgencyAssistant agencyAssistant) {
		EntityRequestDTO entityRequestDTO = new EntityRequestDTO();

		entityRequestDTO.setRecordId(agencyAssistant.getId());
		entityRequestDTO.setRecordType(getRole(agencyAssistant.getUser(), agencyAssistant.getAssistantRole()));
		entityRequestDTO.setRecordIndicator("Update");

		entityRequestDTO.setFirstName(agencyAssistant.getUser() !=null?agencyAssistant.getUser().getFirstName():agencyAssistant.getFirstName());
		entityRequestDTO.setLastName(agencyAssistant.getUser() !=null?agencyAssistant.getUser().getLastName():agencyAssistant.getLastName());

		entityRequestDTO.setBusinessLegalName(agencyAssistant.getBusinessLegalName());
		
		if(StringUtils.isNotEmpty(agencyAssistant.getAssistantNumber())){
			entityRequestDTO.setFunctionalIdentifier(Long.valueOf(agencyAssistant.getAssistantNumber().substring(2)));
		}
		
		if (agencyAssistant.getBusinessAddress() != null) {
			entityRequestDTO.setAddressLine1(agencyAssistant.getBusinessAddress().getAddress1());
			entityRequestDTO.setAddressLine2(agencyAssistant.getBusinessAddress().getAddress2());
			entityRequestDTO.setCity(agencyAssistant.getBusinessAddress().getCity());
			entityRequestDTO.setState(agencyAssistant.getBusinessAddress().getState());
			entityRequestDTO.setZipCode(String.valueOf(agencyAssistant.getBusinessAddress().getZip()));
		} else {
			entityRequestDTO.setAddressLine1("");
			entityRequestDTO.setAddressLine2("");
			entityRequestDTO.setCity("");
			entityRequestDTO.setState("");
			entityRequestDTO.setZipCode("");
		}

		String contactNumber = "";
		if (agencyAssistant.getPrimaryContactNumber() != null && agencyAssistant.getPrimaryContactNumber().contains("-")) {
			contactNumber = agencyAssistant.getPrimaryContactNumber().replaceAll("-", "");
		} else {
			contactNumber = agencyAssistant.getPrimaryContactNumber();
		}
		entityRequestDTO.setPhoneNumber(contactNumber);
		entityRequestDTO.setEmail(agencyAssistant.getPersonalEmailAddress());

		entityRequestDTO.setCertiStatusCode(agencyAssistant.getApprovalStatus().getValue());
		if (agencyAssistant.getApprovalDate() != null) {
			entityRequestDTO.setCertiStartDate(formatDate(agencyAssistant.getApprovalDate()));
			entityRequestDTO.setStatusDate(formatDate(agencyAssistant.getApprovalDate()));
		} else {
			entityRequestDTO.setStatusDate(formatDate(new TSDate()));
		}
		
		if (agencyAssistant.getApprovalStatus() != null
				&& agencyAssistant.getApprovalStatus().toString()
						.equalsIgnoreCase(ApprovalStatus.APPROVED.toString())) {
			entityRequestDTO.setCertificationNumber(agencyAssistant.getApprovalNumber());
		} 
		
		entityRequestDTO.setDirectDepositFlag("False");
		
		if(agencyAssistant.getChangeType()!=null){
			entityRequestDTO.setChangeType(agencyAssistant.getChangeType());
		}
		
		return entityRequestDTO;
	}
	
	/**
	 * @param broker
	 * @param entityRequestDTO
	 */
	private void populateCertificationInformation(Broker broker,
			EntityRequestDTO entityRequestDTO) {
		entityRequestDTO.setCertiStatusCode(broker.getCertificationStatus());
		if (broker.getCertificationDate() != null) {
			entityRequestDTO.setCertiStartDate(formatDate(broker.getCertificationDate()));
		}
		if (broker.getStatusChangeDate() != null) {
			entityRequestDTO.setStatusDate(formatDate(broker.getStatusChangeDate()));
		}

		// If broker is certified then, setting certification end date as
		// certification date + 1 year, else set current date as certification
		// end date
		if (broker.getCertificationStatus() != null
				&& broker.getCertificationStatus().toString()
						.equalsIgnoreCase(certification_status.Certified.toString())) {
			Calendar date = TSCalendar.getInstance();
			date.setTime(broker.getCertificationDate());
			date.add(Calendar.YEAR, 1);
			Date newdate = date.getTime();
			entityRequestDTO.setCertiEndDate(formatDate(newdate));
			entityRequestDTO.setCertificationNumber(broker.getCertificationNumber());
			entityRequestDTO.setCertiRenewalDate(formatDate(broker.getReCertificationDate()));
		} else {
			entityRequestDTO.setCertiEndDate(formatDate(new TSDate()));
			entityRequestDTO.setCertiRenewalDate(formatDate(new TSDate()));
		}
	}
	
	/**
	 * Method to populate current EntityRequestDTO with payment method information.
	 *
	 * @param paymentMethods The current PaymentMethods instance.
	 * @param entityRequestDTO The current EntityRequestDTO instance.
	 */
	private void populatePaymentMethod(PaymentMethods paymentMethods,
			EntityRequestDTO entityRequestDTO) {
		// If payment type is EFT/ACH, setting flag to true. Else, false
		if (paymentMethods != null && "ACH".equals(paymentMethods.getPaymentType().name())) {
			LOGGER.debug("Payment Method is ACH. Setting Direct Deposit Flag to True");
			entityRequestDTO.setDirectDepositFlag("True");
		} else {
			LOGGER.debug("Payment Method is not ACH. Setting Direct Deposit Flag to False");
			entityRequestDTO.setDirectDepositFlag("False");
		}

		if (paymentMethods != null && paymentMethods.getFinancialInfo() != null
				&& paymentMethods.getFinancialInfo().getBankInfo() != null) {

			BankInfo bankInfo = paymentMethods.getFinancialInfo().getBankInfo();

			entityRequestDTO.setBankRoutingNumber(bankInfo.getRoutingNumber());
			entityRequestDTO.setAcctHolderName(bankInfo.getNameOnAccount());
			entityRequestDTO.setBankAcctNumber(bankInfo.getAccountNumber());

			// Checking if account type is 'C' or 'S' to set hard-coded values
			// in DTO
			String accountType = null;
			if (bankInfo.getAccountType() != null) {
				if ("C".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Checking";
				} else if ("S".equalsIgnoreCase(bankInfo.getAccountType())) {
					accountType = "Savings";
				}
			}
			entityRequestDTO.setBankAcctType(accountType);
		}
	}
	
	private String formatDate(Date dateInput) {
		return new SimpleDateFormat("MMddyyyy").format(dateInput);
	}

	
	/**
	 * Checks for null or blank String objects
	 * 
	 * @param value
	 *            the string to be compared
	 * @return boolean true/false depending on whether the value is empty or not
	 */
	public static boolean isEmpty(String value) {

		return (value != null && !"".equals(value)) ? false : true;
	}
	
	@GiFeature("ahbx.aee.ind47.enabled")
	@Override
	public EntityResponseDTO triggerInd47(DesignateBroker designateBroker) {
		DesignateEntityDTO designateEntityDTO = populateEntityBrokerDto(designateBroker);
		EntityResponseDTO entityResponseDTO = new EntityResponseDTO();

		/*
		 * Currently logging response received from AHBX. TODO Need to log
		 * response in db when requirement comes
		 */

		StringBuilder strResponse = new StringBuilder();
		strResponse.append("For Broker : ").append(designateBroker.getId()).append(" : ");

		try {
			LOGGER.error("IND47: Sending Broker Designation details to Ghix-AHBX . Designate Broker ID : "+designateBroker.getId());

			String wsResponse = restTemplate.postForObject(AHBXEndPoints.SEND_ENTITY_DESIGNATION_DETAIL,
					designateEntityDTO, String.class);
			LOGGER.error("IND47: wsResponse : "+wsResponse);
			if(!isEmpty(wsResponse)){
				LOGGER.error("IND47: wsResponse is not empty");
				entityResponseDTO = (EntityResponseDTO) GhixUtils.getXStreamStaxObject().fromXML(wsResponse);
				LOGGER.error("IND47: entityResponseDTO :"+entityResponseDTO);
			}

			LOGGER.info("IND47: Successfully sent Broker Designation details over Rest call to Ghix-Ahbx.");

		} catch (Exception exception) {
			LOGGER.error("IND47: Send Broker Designation Web Service failed. Exception is :" , exception);
		}
		
		return entityResponseDTO;
	}
	
	/**
	 * Populates {@link DesignateEntityDTO} from {@link DesignateEntityDTO} to
	 * send it to AHBX through REST API
	 * 
	 * @param designateBroker
	 * @return {@link DesignateEntityDTO}
	 */
	private DesignateEntityDTO populateEntityBrokerDto(DesignateBroker designateBroker) {
		DesignateEntityDTO designEntityDto = new DesignateEntityDTO();

		designEntityDto.setBrokerAssisterId(designateBroker.getBrokerId());

		// Set them to default values
		designEntityDto.setEmployerId(0);
		designEntityDto.setId(0);
		designEntityDto.setStatus(designateBroker.getStatus().toString());

		// Adding record type as per latest AHBX WSDL update to differentiate if
		// request
		// is associated with agent or assister
		designEntityDto.setRecordType("AGENT");

		if (designateBroker.getExternalEmployerId() != null && !"".equals(designateBroker.getExternalEmployerId())) {
			designEntityDto.setEmployerId(designateBroker.getExternalEmployerId());
			designEntityDto.setRole(RoleService.EMPLOYER_ROLE);
		} else if (designateBroker.getExternalIndividualId() != null
				&& !"".equals(designateBroker.getExternalIndividualId())) {
			designEntityDto.setIndividualId(designateBroker.getExternalIndividualId());
			designEntityDto.setRole(RoleService.INDIVIDUAL_ROLE);
		}

		return designEntityDto;
	}
	
	@GiFeature("ahbx.aee.ind54.enabled")
	@Override
	public void triggerInd54(AgencyAssistant agencyAssistant) {
		String wsResponse = null;
		String delegationCode = null;
		
		try {
			DelegationRequest delegationRequest = populateDelegationRequestDTO(agencyAssistant);

			LOGGER.info("IND54: Sending Staff Information to AHBX : Broker ID : "+agencyAssistant.getId());

			wsResponse = restTemplate.postForObject(AHBXEndPoints.DELEGATIONCODE_RESPONSE, delegationRequest,
					String.class);
			
			DelegationResponse delegationResponse = (DelegationResponse) GhixUtils
					.xStreamHibernateXmlMashaling().fromXML(wsResponse);

			if (delegationResponse != null && delegationResponse.getResponseCode() == 200) {
				delegationCode = delegationResponse.getDelegationCode();
				AgencyAssistant existingAgencyAssistant = agencyAssistantRepository.findById(agencyAssistant.getId());
				existingAgencyAssistant.setDelegationCode(delegationCode);
				agencyAssistantRepository.save(existingAgencyAssistant);

				LOGGER.debug("Successfully persisted the delegation code into DB");
			}

			LOGGER.info("IND54: Successfully sent Staff Information over Rest call to Ghix-Ahbx : "+wsResponse);
		} catch (Exception exception) {
			LOGGER.error("IND54: Send Staff Information Web Service failed. Exception is :" , exception);
		}
	}
	
	private DelegationRequest populateDelegationRequestDTO(AgencyAssistant agencyAssistant) {

		DelegationRequest delegationRequest = new DelegationRequest();
		delegationRequest.setRecordId(agencyAssistant.getId());
		delegationRequest.setContactFirstName(agencyAssistant.getUser() !=null?agencyAssistant.getUser().getFirstName():agencyAssistant.getFirstName());
		delegationRequest.setContactLastName(agencyAssistant.getUser() !=null?agencyAssistant.getUser().getLastName():agencyAssistant.getLastName());
		delegationRequest.setBusinessLegalName(agencyAssistant.getBusinessLegalName());
		
		if(agencyAssistant.getBusinessAddress()!=null){
			delegationRequest.setAddressLineOne(agencyAssistant.getBusinessAddress().getAddress1());
			delegationRequest.setAddressLineTwo(agencyAssistant.getBusinessAddress().getAddress2());
			delegationRequest.setCity(agencyAssistant.getBusinessAddress().getCity());
			delegationRequest.setState(agencyAssistant.getBusinessAddress().getState());
			delegationRequest.setZipCode(String.valueOf(agencyAssistant.getBusinessAddress().getZip()));
		}
		if(StringUtils.isNotEmpty(agencyAssistant.getAssistantNumber())){
			delegationRequest.setFunctionalId(Long.valueOf(agencyAssistant.getAssistantNumber().substring(2)));
		}
		delegationRequest.setCertificationStausCode(agencyAssistant.getApprovalStatus().getValue());
		delegationRequest.setCertificationDate(agencyAssistant.getApprovalDate() != null ? formatDate(
				DATE_FORMAT_PLAIN, agencyAssistant.getApprovalDate()) : null);
		
		if(StringUtils.isNotEmpty(agencyAssistant.getPrimaryContactNumber())) {
			delegationRequest.setPhoneNumber(Long.parseLong(getOnlyDigits(agencyAssistant.getPrimaryContactNumber())));
		}
		delegationRequest.setEmailId(agencyAssistant.getUser() !=null?agencyAssistant.getUser().getEmail():agencyAssistant.getPersonalEmailAddress());
		delegationRequest.setCertificationId(agencyAssistant.getApprovalNumber());
		delegationRequest.setRecordType(getRole(agencyAssistant.getUser(), agencyAssistant.getAssistantRole()));
		
		return delegationRequest;
	}
	
	public static String formatDate(String dateFormat, Date dateInput) {
		return dateInput != null ? new SimpleDateFormat(dateFormat).format(dateInput) : null;
	}
	
	public static String getOnlyDigits(String s) {
	    Pattern pattern = Pattern.compile("[^0-9]");
	    Matcher matcher = pattern.matcher(s);
	    String number = matcher.replaceAll("");
	    return number;
	 }
	
	private String getRole(AccountUser user, String assistantRole){
		String roleName=null;
		if(user!=null){
			Role role = roleService.findRoleByUserId(user.getId());
			roleName = role.getName();
		} else {
			if(assistantRole!=null && !assistantRole.isEmpty()){
				if(assistantRole.equalsIgnoreCase("Level1")){
					roleName = "APPROVEDADMINSTAFFL1";
				}else{
					roleName = "APPROVEDADMINSTAFFL2";
				}
			}
			
		}
		return roleName;
	}
}
