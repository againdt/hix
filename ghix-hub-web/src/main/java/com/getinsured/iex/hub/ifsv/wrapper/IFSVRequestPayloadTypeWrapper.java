package com.getinsured.iex.hub.ifsv.wrapper;

import java.util.ArrayList;
import java.util.List;

import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IFSVRequestPayloadType;
import com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;

/**
 * Wrapper com.getinsured.iex.hub.ifsv.cms.dsh.ifsv.extension._1.IFSVRequestPayloadType
 * 
 * @author Nikhil Talreja
 * @since 27-Feb-2014
 *
 */
public class IFSVRequestPayloadTypeWrapper {
	
	private com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.ObjectFactory proxyFactory; 
	
	private IFSVRequestPayloadType request;
	
	private String requestID;
	private List<IFSVApplicantTypeWrapper> ifsvApplicant;
	
	public IFSVRequestPayloadTypeWrapper(){
		ObjectFactory factory = new ObjectFactory();
		proxyFactory = new com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.ObjectFactory();
		this.request = factory.createIFSVRequestPayloadType();
	}

	public IFSVRequestPayloadType getRequest() throws HubServiceException {
		
		/*
		 * Every SSN in request should be unique
		 */
		List<String> requestSSNs = new ArrayList<String>();
		for(IFSVApplicantTypeWrapper applicant : this.ifsvApplicant){
			String ssn = applicant.getPerson().getPersonSSNIdentification();
			if(requestSSNs.contains(ssn)){
				throw new HubServiceException("Duplicate SSN found in request : ");
			}
			else{
				requestSSNs.add(ssn);
			}
			this.request.getIFSVApplicant().add(applicant.getIfsvApplicantType());
		}
		
		return request;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		com.getinsured.iex.hub.ifsv.niem.niem.proxy.xsd._2.String id = proxyFactory.createString();
		id.setValue(requestID);
		this.request.setRequestID(id);
		this.requestID = requestID;
	}

	/**
     * Gets the value of the ifsvApplicant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ifsvApplicant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIFSVApplicant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IFSVApplicantTypeWrapper }
     * 
     * 
     */
    public List<IFSVApplicantTypeWrapper> getIFSVApplicant() {
    	
        if (ifsvApplicant == null) {
            ifsvApplicant = new ArrayList<IFSVApplicantTypeWrapper>();
        }
        
        return this.ifsvApplicant;
    }
	
}
