var incomeFalg = false;
var incomeLabel = new Object();

	incomeLabel.Financial_Job = 'Job Income';
	incomeLabel.Financial_Self_Employment = 'Self Employment Income';
	incomeLabel.Financial_SocialSecuritybenefits = 'Social Security Benefits Income';
	incomeLabel.Financial_Unemployment = 'Unemployment Income';
	incomeLabel.Financial_Retirement_pension = 'Retirement and Pension Income';
	incomeLabel.Financial_Capitalgains = 'Capital Gain Income';
	incomeLabel.Financial_InvestmentIncome = 'Investment Income';
	incomeLabel.Financial_RentalOrRoyaltyIncome = 'Rental & Royality Income';
	incomeLabel.Financial_FarmingOrFishingIncome = 'Farming or Fishing income';
	incomeLabel.Financial_AlimonyReceived = 'Alimony Received Income';
	incomeLabel.Financial_OtherIncome = 'Other Income';


var incomeMultiply = new Object();
incomeMultiply.Hourly="8640";
incomeMultiply.Daily="360";
incomeMultiply.Weekly="480";
incomeMultiply.EveryTwoWeeks="24";
incomeMultiply.Monthly="12";
incomeMultiply.Yearly="1";
incomeMultiply.OneTimeOnly="1";
incomeMultiply.SelectFrequency="1";

var noOfHouseHold = 1;
var count = 1;

function createHousehold()
{

	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	} 


	noOfHouseHold = $('#noOfPeople53').val();
	$('#numberOfHouseHold').text(noOfHouseHold);
	var i;
	var strOuter = "";
	/*$('.parsley-error-list').hide();*/
	for (i = 1; i <= noOfHouseHold; i++) {

		if($('#appscr57FirstName'+i).html() != undefined)
		{
			continue;
		}

		var temp = jQuery.i18n.prop('ssap.primaryContact');
		if(i!=1){
			temp="";
		}

		var str='<div class="header margin10-b"><h4><strong id="applicantTitle57'+i+'" class = "camelCaseName"></strong> '+temp +'</h4></div>'
		/*+'<h4>heading'+jQuery.i18n.prop("ssap.applicant")+'&nbsp;'+i+'&nbsp;'+temp +'</h4>'*/
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop('ssap.firstName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<input id="appscr57FirstName'+i+'"type="text" onkeyup="removeValidationFirstName('+i+'); setName('+i+')"'
		+ 'onclick="removeValidationFirstName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.firstName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.firstNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.firstNameRequired")+'" />'
		+'</div></div>'
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.middleName")+'</label><div class="controls">'
		+'<input id="appscr57MiddleName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.middleNameValid")+'"'
		+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.middleName")+'" class="input-medium">'
		+'</div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.lastName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<input id="appscr57LastName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')"'
		+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.lastName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z]))+((\\s|-)?([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z])))*)$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" />'
		+'</div></div>'
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.suffix")+'</label><div class="controls">'
		+'<select id="appscr57Suffix'+i+'" class="input-medium">'
		+'<option value="">'+jQuery.i18n.prop("ssap.suffix")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.sr")+'">'+jQuery.i18n.prop("ssap.sr")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.jr")+'">'+jQuery.i18n.prop("ssap.jr")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.III")+'">'+jQuery.i18n.prop("ssap.III")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.IV")+'">'+jQuery.i18n.prop("ssap.IV")+'</option>'
		+'</select></div></div>'
		+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.dateOfBirth")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
		+'<span aria-label="Required!"></span></label><div class="controls">'
		+'<div class="input-append date date-picker" id="calDiv'+i+'" data-date="">'
		+'<input type="text" onkeyup="removeValidationDob('+i+')" onclick="removeValidationDob('+i+')" id="appscr57DOB'+i+'" placeholder="'+jQuery.i18n.prop("ssap.mmDDYYYY")+'"'
		+'class="input-medium dob" '
		+'onfocusout = "validateBirthDate($(this))" '
		+'onblur = "validateBirthDate($(this))" '
		+'data-parsley-trigger="change" data-parsley-errors-container="#dateOfBirthErrorDiv'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.dobRequired")+'" '
		+'/><span class="add-on"><i class="icon-calendar"></i></span></div>'
		+'<div id="dateOfBirthErrorDiv'+i+'"></div>'
		+'</div></div>'
		/*hix-33010*/
		/*+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.dateOfBirthVerified")+'</label><div class="controls">'
		+'<select id="appscr57DOBVerificationStatus'+i+'" class="input-medium">'
		+'<option value ="yes">'+jQuery.i18n.prop("ssap.yes")+'</option>'
		+'<option value ="no">'+jQuery.i18n.prop("ssap.no")+'</option>'
		+'</select></div></div>'*/
		+'<div class="control-group hide">'
		+'<p>'+jQuery.i18n.prop("ssap.has")+'&nbsp;<strong id="applicantName57'+i+'"></strong>&nbsp;'+jQuery.i18n.prop("ssap.usedTobaccoProducts")+'<img width="10" height="10"  src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>'
		+'<div class="controls">'
		+'<label><input type="radio" name="appscr57Tobacco'+ i+ '" value="yes" id="appscr57SexYes'+ i+ '">'+jQuery.i18n.prop("ssap.yes")+'</label>'
		+'<label><input type="radio" name="appscr57Tobacco'+ i+ '" value="no" id="appscr57SexNo'+ i+ '">'+jQuery.i18n.prop("ssap.no")+'</label>'
		+'</div>'
		+'</div>'

		/*+'<div class="control-group hide"><label class="control-label">dobver'+jQuery.i18n.prop('ssap.sex')+'</label><div class="controls">'
		+'<div class="isSex"><label><input type="radio" name="appscr57Sex'+i+'" value="male" id="appscr57SexMale'+i+'" required class="radioValidate">'+jQuery.i18n.prop("ssap.male")+'</label>'
		+'<label><input type="radio" name="appscr57Sex'+i+'"value="female" id="appscr57SexFemale'+i+'">'+jQuery.i18n.prop("ssap.female")+'</label>'
		+'</div></div><span class="errorMessage"></span></div>'*/

/*		+'<div class="control-group">'
		+'<label class="control-label">Relationship to <strong id="appscr57relationshiplbl'+i+'"></strong> (Primary Contact)</label>'
		+'<div class="controls">'
		+'<select id="appscr57relationship'+i+'" name="appscr57relationship'+i+'" class="input-large">'
		+'<option value="0">Select</option>'
		+'<option value="Wife">Wife</option>'
		+'<option value="Child">Child</option>'
		+'<option value="Parent">Parent</option>'
		+'<option value="Caretaker">Caretaker</option>'
		+'</select>'
		+'</div></div>'*/

		+'<div class="control-group"><label class="controls capitalize-none"><input type="checkbox" name="appscr57checkseekingcoverage'+i+'" id="appscr57checkseekingcoverage'+i+'">Are you seeking coverage?</label></div>';

		if (i != noOfHouseHold)
			str += "<hr>";
		strOuter += str;

	}

	$('#appscr57HouseholdForm').html(strOuter);

	/*retrieve data once user go back after adding a new applicant Refer HIX-32515 --- (code done by shovan)*/
	var firstName,middleName, lastName, identifire, suffix, dob, dobVerified, emailAddress, tobacco, gender, isChild, isInfant;
	$('#appscr57HouseHoldPersonalInformation tr').each(function(index) {	      
	        $this = $(this);
	        firstName = $this.find(".firstName").text(),
	        middleName= $this.find(".middleName").text(),
	        lastName= $this.find(".lastName").text(),
	        identifire= $this.find(".identifire").text(),
	        suffix= $this.find(".suffix").text(),
	        dob= $this.find(".dob").text(),
	        dobVerified= $this.find(".dobVerified").text(),
	        emailAddress= $this.find(".emailAddress").text(),
	        tobacco= $this.find(".tobacco").text(),
	        gender= $this.find(".gender").text(),
	        isChild= $this.find(".isChild").text(),
	        isInfant= $this.find(".isInfant").text();
	        $('#appscr57FirstName'+(index+1)).val(firstName);
	        $('#appscr57FirstName'+(index+1)).attr("value",firstName);
	        $('#appscr57MiddleName'+(index+1)).val(middleName);
	        $('#appscr57MiddleName'+(index+1)).attr("value",middleName);
	        $('#appscr57LastName'+(index+1)).val(lastName);
	        $('#appscr57LastName'+(index+1)).attr("value",lastName);
	        var applicantName = trimSpaces(firstName)+" "+trimSpaces(middleName)+" "+trimSpaces(lastName);
	        $('#applicantName57'+(index+1)).text(applicantName);
	        $('#appscr57DOB'+(index+1)).val(dob);
	        $('#appscr57DOB'+(index+1)).attr("value",dob);
	        $('#appscr57DOBVerificationStatus'+(index+1)).val(dobVerified);
	        $('#appscr57DOBVerificationStatus'+(index+1)).attr("value",dobVerified);
	        $('#appscr57Suffix'+(index+1)).val(suffix);
	        $('#appscr57Suffix'+(index+1)).attr("value",suffix);
	        $('#applicantTitle57'+(index+1)).text(applicantName);

	        if(tobacco === "yes"){
	        	$('input[id=appscr57SexYes'+(index+1)+']:radio').prop('checked', true);
	        }else{
	        	$('input[id=appscr57SexNo'+(index+1)+']:radio').prop('checked', true);
	        }

	});
	/*retrieve data once user go back after adding a new applicant--- (code done by shovan)*/

	$('#appscr57FirstName1').val($('#firstName').val());
	$('#appscr57FirstName1').attr("value",$('#firstName').val());
	$('#appscr57MiddleName1').val($('#middleName').val());
	$('#appscr57MiddleName1').attr("value",$('#middleName').val());
	$('#appscr57LastName1').val($('#lastName').val());
	$('#appscr57LastName1').attr("value",$('#lastName').val());
	var applicantName = trimSpaces($('#firstName').val())+" "+trimSpaces($('#middleName').val())+" "+trimSpaces($('#lastName').val());
	$('#applicantName571').text(applicantName);
	$('#appscr57DOB1').val($('#dateOfBirth').val());
	$('#appscr57DOB1').attr("value",$('#dateOfBirth').val());
	$('#appscr57DOBVerificationStatus1').val($('#dobVerificationStatus').val());
	$('#appscr57DOBVerificationStatus1').attr("value",$('#dobVerificationStatus').val());
	$('#appscr57Suffix1').val($('#appscr53Suffix').val());
	$('#appscr57Suffix1').attr("value",$('#appscr53Suffix').val());

	//var applicantName = $('#appscr57FirstName' + i).val()+" "+ $('#appscr57MiddleName' + i).val()+" "+$('#appscr57LastName' + i).val();
	var applicantName = trimSpaces($('#firstName').val())+" "+trimSpaces($('#middleName').val())+" "+trimSpaces($('#lastName').val());;
	$('#applicantTitle571').text(applicantName);

	addMaskingOnAppscr57();

	for(var j=1;j<=i;j++){
		createCalendar(j);
		/*$('.characterStroke').ForceCharacterOnly();*/
	}

	var noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		if(householdMemberObj.applyingForCoverageIndicator == true)
			$('#appscr57checkseekingcoverage'+cnt).prop("checked", true);
		else
			$('#appscr57checkseekingcoverage'+cnt).prop("checked", false);
	}

	next();
}

function addHouseHoldDetail(){
	if(editFlag == true && currentPage == "appscr57")
	{
		appscr58EditUdate(editMemberNumber,false);
		return false;
	}

	if(currentPage != 'appscr58'){ //  HIX-50570
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	}
	$('#cancelButtonCancel').hide();
	cancleFlag = true;

	var houseHoldNames=new Array();
	
	noOfHouseHold = $('#numberOfHouseHold').text();
	//noOfHouseHold = getNoOfHouseHolds()+1;
	//empty array
	applyingForCoverageIndicatorArrCopy = [];

	//noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	if(useJSON == true){
	for ( var cnt = 1; cnt <= noOfHouseHold; cnt++) {
		noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;

		if(cnt > noOfHouseHoldmember){
			defaultJSON = getDefatulJson();
			householdMemberObj=defaultJSON.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
			//householdMemberObj.personId = cnt;
			var lastPersonid = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[noOfHouseHoldmember-1].personId;			
			householdMemberObj.personId = lastPersonid + 1;
		}else{
			householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		}
		if(trimSpaces($('#appscr57FirstName'+cnt).val())!=""){
			householdMemberObj.name.firstName = $('#appscr57FirstName'+cnt).val();
			householdMemberObj.name.middleName = $('#appscr57MiddleName'+cnt).val();
			householdMemberObj.name.lastName = $('#appscr57LastName'+cnt).val();
			householdMemberObj.name.suffix = $('#appscr57Suffix'+cnt).val();
			householdMemberObj.dateOfBirth = DBDateformat($('#appscr57DOB'+cnt).val());
			householdMemberObj.tobaccoUserIndicator = false;
	
			if($('input[name=liveWithSpouseIndicator'+ cnt +']:radio:checked').val()=='yes'){
				householdMemberObj.tobaccoUserIndicator = true;
			}
	
			householdMemberObj.applyingForCoverageIndicator = $('input[name=appscr57checkseekingcoverage'+cnt+']').is(':checked') == true  ? true : false;
	
			//for updating seeking coverage indicator, saveJsonData line 250
			applyingForCoverageIndicatorArrCopy.push(householdMemberObj.applyingForCoverageIndicator);		
			webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1] = householdMemberObj;	
		}
	}
	}

	for ( var i = 1; i <= noOfHouseHold; i++) {
		if((haveData && i<=houseHoldNumberValue) || $('#HouseHold'+i).html() != undefined){
			appscr58EditUdate(i,false);
			continue;
		}

		if($('#appscr57MiddleName' + i).val()=='Middle Name'){
			$('#appscr57MiddleName' + i).val('');
		}

		if($('#appscr57LastName' + i).val()=='Last Name'){
			$('#appscr57LastName' + i).val('');
		}
		//var HouseHoldName = "" + $('#appscr57FirstName' + i).val() + " " + $('#appscr57MiddleName' + i).val() + " " + $('#appscr57LastName' + i).val();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName + "" + householdMemberObj.name.suffix;
		houseHoldNames[i-1]=HouseHoldName;
		if(i>1){

			$('#appscr57FirstName' + i).attr("value",$('#appscr57FirstName'+i).val());
			$('#appscr57MiddleName'+i).attr("value",$('#appscr57MiddleName'+i).val());
			$('#appscr57LastName'+i).attr("value",$('#appscr57LastName'+i).val());
			$('#appscr57Suffix'+i).attr("value",$('#appscr57Suffix'+i).val());
			$('#appscr57DOBVerificationStatus'+i).attr("value",$('#appscr57DOBVerificationStatus'+i).val());
			$('#appscr57DOB'+i).attr("value",$('#appscr57DOB'+i).val());			
			/*HIX-45293 - remove don't allow same name*/
			//don't allow same name(first + middle + last + suffix)
			/*for(var j=0; j<(houseHoldNames.length-1); j++){
				if(houseHoldNames[j] == HouseHoldName ){
					$('#duplicatedNameErrorDiv' + i).html('<div class="errorsWrapper filled"><span class="validation-error-text-container parsley-required">Two  applicant cannot have the same name.</span></div>');
					//addLabels('Two  applicant cannot have the same name',"appscr57FirstName"+i);
					$('#appscr57FirstName' + i).focus();
					return false;
				}else{
					$('#duplicatedNameErrorDiv' + i).html('');
				}
			}*/
		}

	}
	if(SavingData == false){
	next();
	addAppscr58Detail();
	$('#addAnotherIndividualButton').show();
	$('#addAnotherIndividual').hide();
	}
}

function resetHouseHoldDetail(i)
{


}


function repeatPage() {
	noOfHouseHold = getNoOfHouseHolds();
	if (count <= noOfHouseHold) {
		setNonFinancialPageProperty(count);
		count++;
	} else {
		count = 1;
		next();
	}
}

function goToAppscr61()
{

	setNonFinancialPageProperty(1);
	count++;
	next();
}


function setNonFinancialPageProperty(i)
{	
	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
	var houseHoldname =householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
	if(useJSON == true){
		$('.nameOfHouseHold').html(""+houseHoldname);
	}
	//$('#houseHoldTRNo').val('HouseHold'+i);
	saveCurrentApplicantID(i);	
	//appscr67EditControllerJSON(i);	
	setHisOrHerAfterContinue();
	updateLiveWithChildUnderAgeView();
	return;	
}

//keys

function Appscr58DetailJson(){
	document.getElementById('appscr58HouseholdForm').innerHTML = '';
	var strOuter = "";
	noOfHouseHoldmember=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	for ( var cnt = 1; cnt <= noOfHouseHoldmember; cnt++) {
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[cnt-1];
		var HouseHoldName="" + householdMemberObj.name.firstName + " " + householdMemberObj.name.middleName + " " + householdMemberObj.name.lastName;
		var dateOfBirth = UIDateformat(householdMemberObj.dateOfBirth);
		var str="";
		if(cnt==1)
		{
			str+="<div class='hixApplicantInfo camelCaseName'><h5>"+HouseHoldName+" "+jQuery.i18n.prop('ssap.primaryContact')+"<span class='pull-right'><input type='button' value='Edit' class='btn btn-primary' onclick=appscr58EditController("+cnt+") /></span></h5>";
		    str+="<p style='text-transform: none;'>"+jQuery.i18n.prop('ssap.dateOfBirth1')+" "+dateOfBirth+"</p></div>";
		}
		else if(cnt<=noOfHouseHoldmember && cnt!=1)
		{
			str+="<div class='hixApplicantInfo camelCaseName'><h5>"+HouseHoldName+" <span class='pull-right'><input type='button' value='Delete' class='btn' style='background-image: none; padding: 2px 8px; margin: 2px 0 0 9px;' onclick=removeApplicant("+householdMemberObj.personId+") /></span><span class='pull-right'><input type='button' value='Edit' class='btn btn-primary' onclick=appscr58EditController("+cnt+") /></span></h5>";
		    str+="<p style='text-transform: none;'>"+jQuery.i18n.prop('ssap.dateOfBirth1')+" "+dateOfBirth+" </p></div>";
		}			
		strOuter += str;
	}
	$("#appscr58HouseholdForm").html(strOuter);
}
function addAppscr58Detail(){
	if(useJSON == true){
		Appscr58DetailJson();
		return;
	}
	
}


//Keys
function addSpecialCircumstances()
{
	noOfHouseHold = getNoOfHouseHolds();
	
	var i;
	var strOuter = "";
	var femaleFlag=0;

	//strOuter+= '<h5>'+jQuery.i18n.prop("ssap.answerTheQuestionsBelow")+'</h5>';
	strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.doAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

	for (i = 1; i <= noOfHouseHold; i++){
		//var HouseHoldName=''+$("#appscr57FirstNameTD"+i).text()+' '+$("#appscr57MiddleNameTD"+i).text()+' '+$("#appscr57LastNameTD"+i).text();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;		
		strOuter+='<label><input type="checkbox" id="disability'+i+'" onchange="disabilityOnChange()" name="personDisability" value="disability'+i+'" data-parsley-errors-container="#disabilityErrorDiv" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">'+HouseHoldName+'</label>';
	}

	strOuter+='<label class="capitalize-none"><input type="checkbox"  name="personDisability" onchange="disabilityNoneOfTheseChecked()" id="disabilityNoneOfThese" value="disabilityNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
	strOuter+='<div id="disabilityErrorDiv"></div>';
	strOuter+='</div></div><div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.doAnyOfThePeopleBelowNeedHelp")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l"  data-rel="multipleInputs">';

	for (i = 1; i <= noOfHouseHold; i++){
		//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
		strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="assistanceServicesOnChange()" name="personPersonalAssistance" id="assistanceServices'+i+'" value="assistanceServices'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#assistanceServicesErrorDiv">'+HouseHoldName+'</label>';
	}

		strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="assistanceServicesNoneOfTheseChecked()" name="personPersonalAssistance" id="assistanceServicesNoneOfThese" value="assistanceServicesNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
		strOuter+='<div id="assistanceServicesErrorDiv"></div>';
		strOuter+='</div></div>';
		strOuter+='<div class="control-group"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

	for (i = 1; i <= noOfHouseHold; i++){
		
		//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];		
		HouseHoldName=getNameByPersonId(i);
		var HouseHoldgender = householdMemberObj.gender;
		if(HouseHoldgender != "" && HouseHoldgender != undefined){
			if(HouseHoldgender.toLowerCase() == 'female') {
				femaleFlag=1;			
			}
		}		
		strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="nativeOnChange()" name="peopleBelowTo" id="native'+i+'" value="native'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#nativeErrorDiv"><span class="camelCaseName">'+HouseHoldName+'</span></label>';
		strOuter+='<div class="control-group hide" id="nativeTribe'+i+'"><p>';
		strOuter+=jQuery.i18n.prop("ssap.is")+'&nbsp;<strong><span class="camelCaseName">'+HouseHoldName+'</span></strong>&nbsp';
		strOuter+=jQuery.i18n.prop("ssap.page29.aMemberOfAFederally")+'?<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p>';
		strOuter+='<div class="margin5-l controls">';
		strOuter+='<label><input type="radio" name="AmericonIndianQuestionRadio' + i + '" id="AmericonIndianQuestionRadioYes' + i + '" value="yes" onchange="checkAmericonIndianQuestion(' + i + ');"  data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#americonIndianQuestionRadioYesErrorDiv' + i + '"/>';
		strOuter+=jQuery.i18n.prop("ssap.yes")+'</label>';
		strOuter+='<label><input type="radio" name="AmericonIndianQuestionRadio' + i + '" id="AmericonIndianQuestionRadioNo' + i + '" value="no"  onchange="checkAmericonIndianQuestion(' + i + ');">';
		strOuter+=jQuery.i18n.prop("ssap.no")+'</label>';
		strOuter+='<div id="americonIndianQuestionRadioYesErrorDiv' + i + '"></div>';
		strOuter+='</div>';

		strOuter+='<div class="hide clearfix" id="americonIndianQuestionDiv' + i + '">';
		strOuter+='<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.State")+''
		+'<span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<select id="americonIndianQuestionSelect'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" onChange="getAmericanAlaskaTribeList(this.value,tribeName'+i+')" >'
		+'<option value="">'+jQuery.i18n.prop("ssap.State")+'</option>'
		+'<option value="AL">'+jQuery.i18n.prop("ssap.state.Albama")+'</option>'
		+'<option value="AK">'+jQuery.i18n.prop("ssap.state.Alaska")+'</option>'
		+'<option value="AZ">'+jQuery.i18n.prop("ssap.state.Arizona")+'</option>'
		+'<option value="CA">'+jQuery.i18n.prop("ssap.state.California")+'</option>'
		+'<option value="CO">'+jQuery.i18n.prop("ssap.state.Colorado")+'</option>'
		+'<option value="CT">'+jQuery.i18n.prop("ssap.state.Connecticut")+'</option>'
		+'<option value="FL">'+jQuery.i18n.prop("ssap.state.Florida")+'</option>'
		+'<option value="ID">'+jQuery.i18n.prop("ssap.state.Idaho")+'</option>'
		+'<option value="IA">'+jQuery.i18n.prop("ssap.state.Iowa")+'</option>'
		+'<option value="KS">'+jQuery.i18n.prop("ssap.state.Kansas")+'</option>'
		+'<option value="LA">'+jQuery.i18n.prop("ssap.state.Louisiana")+'</option>'
		+'<option value="ME">'+jQuery.i18n.prop("ssap.state.Maine")+'</option>'
		+'<option value="MA">'+jQuery.i18n.prop("ssap.state.Massachusetts")+'</option>'
		+'<option value="MI">'+jQuery.i18n.prop("ssap.state.Michigan")+'</option>'
		+'<option value="MN">'+jQuery.i18n.prop("ssap.state.Minnesota")+'</option>'
		+'<option value="MS">'+jQuery.i18n.prop("ssap.state.Mississippi")+'</option>'		
		+'<option value="MT">'+jQuery.i18n.prop("ssap.state.Montana")+'</option>'
		+'<option value="NE">'+jQuery.i18n.prop("ssap.state.Nebraska")+'</option>'
		+'<option value="NV">'+jQuery.i18n.prop("ssap.state.Nevada")+'</option>'
		+'<option value="NM">'+jQuery.i18n.prop("ssap.state.NewMexico")+'</option>'
		+'<option value="NY">'+jQuery.i18n.prop("ssap.state.NewYork")+'</option>'
		+'<option value="NC">'+jQuery.i18n.prop("ssap.state.NorthCarolina")+'</option>'
		+'<option value="ND">'+jQuery.i18n.prop("ssap.state.NorthDakota")+'</option>'
		+'<option value="OK">'+jQuery.i18n.prop("ssap.state.Oklahoma")+'</option>'
		+'<option value="OR">'+jQuery.i18n.prop("ssap.state.Oregon")+'</option>'
		+'<option value="RI">'+jQuery.i18n.prop("ssap.state.RhodeIsland")+'</option>'
		+'<option value="SC">'+jQuery.i18n.prop("ssap.state.SouthCarolina")+'</option>'
		+'<option value="SD">'+jQuery.i18n.prop("ssap.state.SouthDakota")+'</option>'
		+'<option value="TX">'+jQuery.i18n.prop("ssap.state.Texas")+'</option>'
		+'<option value="UT">'+jQuery.i18n.prop("ssap.state.Utah")+'</option>'
		+'<option value="WA">'+jQuery.i18n.prop("ssap.state.Washington")+'</option>'
		+'<option value="WI">'+jQuery.i18n.prop("ssap.state.Wisconsin")+'</option>'
		+'<option value="WY">'+jQuery.i18n.prop("ssap.state.Wyoming")+'</option>'
		+'</select>';
		strOuter+='</div></div>';


		strOuter+='<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.page29.tribeName")+''
		+'<span aria-label="Required!"></span></label>'
		+'<div class="controls">'
		+'<select id="tribeName'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">'
		+'<option value="">'+jQuery.i18n.prop("ssap.page29.tribeName")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.JApacheNation")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.JApacheNation")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.MApacheTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.MApacheTribe")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.NNation")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.NNation")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.OOwingeh")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.OOwingeh")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PAcoma")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PAcoma")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PCochiti")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PCochiti")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PJemez")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PJemez")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PIsleta")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PIsleta")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PLaguna")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PLaguna")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PNambe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PNambe")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPicuris")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPicuris")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPojoaque")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PPojoaque")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanFelipe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanFelipe")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanIldefonso")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSanIldefonso")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSandia")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSandia")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaAna")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaAna")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaClara")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantaClara")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantoDomingo")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PSantoDomingo")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTaos")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTaos")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTesuque")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PTesuque")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PZia")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.PZia")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.UMountainTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.UMountainTribe")+'</option>'
		+'<option value="'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.ZTribe")+'">'+jQuery.i18n.prop("ssap.page16.americanIndianTribe.ZTribe")+'</option>'
		+'</select>';
		strOuter+='</div></div>';
		strOuter+='</div>';
		strOuter+='</div>';
	}

	strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="nativeNoneOfTheseChecked()" name="peopleBelowTo" id="nativeNoneOfThese" value="nativeNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
	strOuter+='<div id="nativeErrorDiv"></div>';
	strOuter+="</div></div>";

	if(femaleFlag == 1){
		//check num of female that are 13 or above
		var pregnantAgeCount = checkPregnantCount();		
	}
	var flag=0;
	var cnt = 0;
	for (i = 1; i <= noOfHouseHold; i++){
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var fDob = householdMemberObj.dateOfBirth;
		var HouseHoldgender = householdMemberObj.gender;		
		if(HouseHoldgender.toLowerCase() == 'female' && checkBirthDate(fDob)>(365*13)){
			//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
			if(cnt == 0){
				strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelowPregnant")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';
				cnt++;
			}
			var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
			strOuter+='<label><input type="checkbox" id="pregnancy'+i+'" onchange="pregnancyOnChange()" onclick="getExpectedInPregnancy()" name="personPregnant" value="pregnancy'+HouseHoldName+''+i+'"'
					+' data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#pregnancyErrorDiv"'
					+'>'+HouseHoldName+'</label>';
			flag=1;
		}
	}

	if(flag==1){
		strOuter+='<label class="capitalize-none"><input type="checkbox" id="pregnancyNoneOfThese" onchange="pregnancyNoneOfTheseChecked()" name="personPregnant" value="pregnancyNoneOfThese" value="None of these people">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>'
				+'<div id="pregnancyErrorDiv"></div>'
				+'</div></div>';
	}

	strOuter+='<div id="appscr66PregnancyDetail" class="margin30-l margin10-b"> </div>';
	strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.wereAnyOfThePeopleBelow")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

    for (i = 1; i <= noOfHouseHold; i++)
	{
    	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
    	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
    	var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
    	strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="personfosterCareOnChange()" name="personfosterCare" id="personfosterCare'+i+'" value="personfosterCare'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#personfosterCareErrorDiv">'+HouseHoldName+'</label>';
	}
    strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="personfosterCareNoneOfTheseChecked()" name="personfosterCare" id="personfosterCareNoneOfThese" value="personfosterCareNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
    strOuter+='<div id="personfosterCareErrorDiv"></div>';
    strOuter+='</div></div>';
    strOuter+='<div id="appscr66DeceasedDetail"></div>';
    strOuter+='<div class="control-group non_financial_hide"><p>'+jQuery.i18n.prop("ssap.areAnyOfThePeopleBelowDeceased")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true" /></p><div class="margin5-l">';

    for (i = 1; i <= noOfHouseHold; i++)
	{
    	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
    	householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

    	strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="deceasedOnChange()" name="deceasedPerson" id="deceased'+i+'" value="deceased'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#deceasedErrorDiv">'+HouseHoldName+'</label>';
	}
    strOuter+='<label class="capitalize-none"><input type="checkbox" onchange="deceasedNoneOfTheseChecked()" name="deceasedPerson" id="deceasedPersonNoneOfThese" value="deceasedPersonNoneOfThese">'+jQuery.i18n.prop("ssap.noneOfThesePeople")+'</label>';
    strOuter+='<div id="deceasedErrorDiv"></div>';
    strOuter+='</div></div>';

	for (i = 1; i <= noOfHouseHold; i++){
		//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
		strOuter+='<div class="control-group eligibleCoverage non_financial_hide"><p>';
		strOuter+=jQuery.i18n.prop("ssap.is")+'&nbsp;<strong>'+HouseHoldName+'</strong>&nbsp';
		strOuter+=jQuery.i18n.prop("ssap.page28.eligibleForHealthCovergae")+'&nbsp;'+HouseHoldName+'&nbsp;'+jQuery.i18n.prop("ssap.page28.isntCurrentlyEnrolled")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></p>';
		strOuter+='<div class="margin5-l">';
		strOuter+='<label class="capitalize-none"><input type="radio" id="appscr78Medicare'+i+'" name="fedHealth'+i+'" value="Medicare" onchange="otherInsuranceCheckbox('+ i +')" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'" data-parsley-errors-container="#appscr78MedicareErrorDiv'+i+'">';
		strOuter+=jQuery.i18n.prop("ssap.page28.medicare")+'</label>';

		strOuter+='<label ><input type="radio" id="appscr78Tricare'+i+'" name="fedHealth'+i+'" value="TRICARE" onchange="otherInsuranceCheckbox('+ i +')">';
		strOuter+=jQuery.i18n.prop("ssap.page28.tricare")+'</label>';

		strOuter+='<label ><input type="radio" id="appscr78PeaceCorps'+i+'" name="fedHealth'+i+'" value="PeaceCorps" onchange="otherInsuranceCheckbox('+ i +')">';
		strOuter+=jQuery.i18n.prop("ssap.page28.peaceCorps")+'</label>';

		strOuter+='<label class="capitalize-none"><input type="radio" name="fedHealth'+i+'" id="appscr78OtherStateFHBP'+i+'" value="Other" onchange="otherInsuranceCheckbox('+ i +')">';
		strOuter+=jQuery.i18n.prop("ssap.page28.otherStateOrFederal")+'</label>';

		strOuter+='<div id="appscr78OtherStateOrFederalHealthPlan'+i+'" class="appscr78OtherStateOrFederalHealthPlan hide"><div class="control-group">';
		strOuter+='<label class="control-label">'+jQuery.i18n.prop("ssap.page28.programType")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></label>';
		/*strOuter+='<div class="controls"><input type="text"  id="appscr78TypeOfProgram'+i+'" name="typeTxt" placeholder="'+jQuery.i18n.prop("ssap.page28.type")+'" class="input-medium"/>';*/
		strOuter+='<div class="controls"><select id="appscr78TypeOfProgram'+i+'" class="input-medium" data-parsley-trigger="change" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.programType")+'"><option value>'+jQuery.i18n.prop("ssap.page28.selectType")+'</option><option value="State">'+jQuery.i18n.prop("ssap.page28.state")+'</option><option value="Federal">'+jQuery.i18n.prop("ssap.page28.federal")+'</option></select>'
		strOuter+='</div></div>';
		strOuter+='<div class="control-group"><label class="control-label">';
		strOuter+=jQuery.i18n.prop("ssap.page28.programName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" /></label>';
		strOuter+='<div class="controls"><input type="text" id="appscr78NameOfProgram'+i+'" name="programTxt" placeholder="'+jQuery.i18n.prop("ssap.page28.programName")+'" class="input-medium" data-parsley-trigger="change" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.programName")+'"/>';
		strOuter+='</div></div></div><!-- appscr78OtherStateOrFederalHealthPlan ends -->';

		strOuter+='<label class="capitalize-none"><input type="radio" id="appscr78None'+i+'" name="fedHealth'+i+'" value="None" onchange="otherInsuranceCheckbox('+ i +')">';
		strOuter+=jQuery.i18n.prop("ssap.noneOfTheAbove")+'</label>';
		strOuter+='<div id="appscr78MedicareErrorDiv'+i+'"></div>';
		strOuter+='</div></div>';
	}

	$("#appscr66SpecialCircumstances").html(strOuter);

	if(ssapApplicationType == "NON_FINANCIAL"){
		$(".non_financial_hide").css("display","none");
		$(".non_financial_hide input").removeAttr("parsley-mincheck").removeClass("parsley-validated parsley-error");

	}
	appscr66FromMongodbJson();
	//next();
}


//Keys
function getExpectedInPregnancy()
{
	$('#appscr66PregnancyDetail').html("");
	var strOut="";
	noOfHouseHold = getNoOfHouseHolds();
	 for (var i = 1; i <= noOfHouseHold; i++)
		{
		 householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		 var HouseHoldgender = householdMemberObj.gender;
		 
		 if(HouseHoldgender.toLowerCase() == 'female')
			 {
			 //if(document.getElementById("pregnancy"+i).checked == true)
			 if($('#pregnancy' + i).is(":checked"))
			 	{
				 	//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();
				 	
					var HouseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
				 	strOut+='<div>'+jQuery.i18n.prop("ssap.howManyBabiesIs")+' <strong>'+HouseHoldName+'</strong> '+jQuery.i18n.prop("ssap.expectingDuringThisPregnancy")+'</div>';
				 	strOut+='<div  class="form_row controls margin5-l"><select name="numberOfBabies" id="number-of-babies'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.required.field")+'">';
				 	strOut+='<option value=""> Please Select </option>	<option value="1">1</option>	<option value="2">2</option>	  <option value="3">3</option>';
				 	strOut+='<option value="4">4</option>	 <option value="5">5</option>	 <option value="6">6</option>	 <option value="7">7</option>';
				 	strOut+='<option value="8">8</option>	 <option value="9">9</option>	</select><span class="errorMessage"></span></div>  ';
			 	}
			 }
		}

	 $("#appscr66PregnancyDetail").html(strOut);	 
	 for (var i = 1; i <= noOfHouseHold; i++){
		 householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		 var HouseHoldgender = householdMemberObj.gender;
		 
		 if(HouseHoldgender.toLowerCase() == 'female' && $('#pregnancy' + i).is(":checked") && householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy>0) {
			 $('#number-of-babies'+i).val(householdMemberObj.specialCircumstances.numberBabiesExpectedInPregnancy);
		 }		 
	 }
}

function saveSpecialCircumstances()
{
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	} 
	//showHouseHoldContactSummary();
	next();

}

function appscr72CheckBoxAction(idOfCb)
{
	document.getElementById("appscr72incomeNone").checked = false;
	var currentId = $(idOfCb).attr('id');
	if(document.getElementById(currentId).checked == true)
		{

		$('#'+currentId+"TR").show();
		}
	else
		{
		$('#'+currentId+"TR").hide();

		appscr72CheckBoxesValuesSet(idOfCb);
		}

}

function appscr72CheckBoxesValuesSet(idOfCb)
{
	idOfCb = $(idOfCb).attr('id');

	if(idOfCb == "appscr72alimonyPaid")
	{
		$('#alimonyAmountInput').val('');
		$('#alimonyFrequencySelect').val('SelectFrequency');
	}
	else if(idOfCb == "appscr72studentLoanInterest")
	{
		$('#studentLoanInterestAmountInput').val('');
		$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
	}
	else if(idOfCb == "appscr72otherDeduction")
	{
		$('#deductionTypeInput').val('');
		$('#deductionAmountInput').val('');
		$('#deductionsFrequencySelect').val('SelectFrequency');
	}
}

function appscr72DisableDeductions(idOfCb)
{
	if(document.getElementById("appscr72incomeNone").checked == true)
		{
		document.getElementById("appscr72alimonyPaid").checked = false;
		document.getElementById("appscr72studentLoanInterest").checked = false;
		document.getElementById("appscr72otherDeduction").checked = false;

		$('#alimonyAmountInput').val('');
		$('#studentLoanInterestAmountInput').val('');
		$('#deductionTypeInput').val('');
		$('#deductionAmountInput').val('');
		$('#alimonyFrequencySelect').val('SelectFrequency');
		$('#studentLoanInterestFrequencySelect').val('SelectFrequency');
		$('#deductionsFrequencySelect').val('SelectFrequency');

		document.getElementById("appscr72alimonyPaidTR").style.display = 'none';
		document.getElementById("appscr72studentLoanInterestTR").style.display = 'none';
		document.getElementById("appscr72otherDeductionTR").style.display = 'none';
		}

}

function appscr72basedOnAmountInput()
{
	if((document.getElementById("basedOnAmountInput").value.length) >= 1)
		{
		document.getElementById("basedOnAmountCB").checked = false;
		}
}


function appscr72BasedOnAmountCB()
{
	if(document.getElementById("basedOnAmountCB").checked == true)
		document.getElementById("basedOnAmountInput").value = "0";

}

function appscr72incomeHighOrLow(){
	if($('input[name=aboutIncomeSpped]:radio:checked').val() =="no"){
		$('#incomeHighOrLowDiv').show();
	}else{
		$('#incomeHighOrLowDiv').hide();		
		$('#incomeVariationHigh').prop("checked",false);		
		$('#incomeVariationLow').prop("checked",false);	
	}
}

function addHouseHold(){

	/*if(validation(currentPage)==false)
		return false;*/
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	} 
	
	var noOfHouseHold = parseInt($('#numberOfHouseHold').text());

	createTaxRelation();
	setTaxRelatedData();	
	if(flagToUpdateFromResult== true){
		HouseHoldRepeat=editMemberNumber;
		flagToUpdateFromResult=false;
		if(checkStillUpdateRequired())
			{

			return true;
			}
		else
			{
			$('#finalEditDiv').show();
			goToPageById('appscrBloodRel');
			return false;
			}


	}	
	var primaryTaxFilerPersonId = webJson.singleStreamlinedApplication.primaryTaxFilerPersonId;
	var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;

	//go to sex page if
	//1.Primary Contact is a tax filer(primaryTaxFilerPersonId == 1)
	//2.Primary Contact is not a tax filer and not being claimed as a dependent of anyone else(primaryTaxFilerPersonId == 0)
	//3.Tax file page is not Primary Contact
	if(primaryTaxFilerPersonId == 1 || primaryTaxFilerPersonId == 0 || HouseHoldRepeat != 1){


		var notSeekingCoverageCount = [];

		$("#notSeekingCoverageList").html("");
		
		//PC never show in the list
		for(var j=1; j<householdMemberObj.length; j++){
			 if(householdMemberObj[j].applyingForCoverageIndicator == false){
				$("#notSeekingCoverageList").append("<li><strong>" + householdMemberObj[j].name.firstName + " " + householdMemberObj[j].name.middleName + " " + householdMemberObj[j].name.lastName + "</strong></li>");
				notSeekingCoverageCount++;
			}
		}

		if(primaryTaxFilerPersonId == 0){
			$(".PCName").html(householdMemberObj[0].name.firstName + " " + householdMemberObj[0].name.middleName + " " + householdMemberObj[0].name.lastName);
			$("#notSeekingCoverageAlert .PCNotEligible").show();
		}else{
			$("#notSeekingCoverageAlert .PCNotEligible").hide();
		}


		if(notSeekingCoverageCount == 1){
			$("#notSeekingCoverageAlert .dependentNotEligible").show();
			$("#notSeekingCoverageAlert .plural").hide();
			$("#notSeekingCoverageAlert .singular").show();
		}else if(notSeekingCoverageCount > 1){
			$("#notSeekingCoverageAlert .dependentNotEligible").show();
			$("#notSeekingCoverageAlert .singular").hide();
			$("#notSeekingCoverageAlert .plural").show();
		}
		else{
			$("#notSeekingCoverageAlert .dependentNotEligible").hide();
		}

		if(($("#notSeekingCoverageList").html() != "" || primaryTaxFilerPersonId == 0)  && ssapApplicationType == "FINANCIAL"){
			$('#notSeekingCoverageAlert').modal();

			$('#notSeekingCoverageAlert').on('hide', function () {
				setNonFinancialPageProperty(1);
			});

		}else{
			setNonFinancialPageProperty(1);
			next();
		}

	}else{
		HouseHoldRepeat=primaryTaxFilerPersonId;

		goToPageById('appscr60');

		getSpouse(HouseHoldRepeat);
		setNonFinancialPageProperty(HouseHoldRepeat);		
		updateAppscr60FromJSON(HouseHoldRepeat);
		
		//tax page: TF is not PC
		$("#planToFileFTRIndicatorYes").prop("checked",true);
		
		planToFileYes();
		//hide will Be Claimed
		$("#willBeClaimed, #appscr60ClaimedFilerDiv").hide();
		
		
		$("input[name=planToFileFTRIndicator]").prop("disabled",true);

		$("#householdHasDependantYes").prop("checked",true);
		$("input[name=householdHasDependant]").prop("disabled",true);

		$("#householdDependentMainDiv").show();
		//getSpouse();
		$("#householdContactDependant1").prop("checked",true).prop("disabled",true);		
	}
	$('html, body').animate({scrollTop:$('#menu').position().top}, 'fast');

}

function resetPage69Values()
{
	$('#appscr69Form input:text').val('');
	$('#appscr69Form select').val('0');
	irsIncomeResetValues();	
}

function resetPage71Values()
{
	$('#appscr71Form input:text').val('');
	$('#appscr71Form select').val('0');	
}


function resetPage72DeductionValues()
{
	$('#appscr72Form input:text').val('');
	$('#appscr72Form select').val('SelectFrequency');

	$('#appscr72alimonyPaidTR').hide();
	$('#appscr72studentLoanInterestTR').hide();
	$('#appscr72otherDeductionTR').hide();

	document.getElementById('appscr72alimonyPaid').checked = false;
	document.getElementById('appscr72studentLoanInterest').checked = false;
	document.getElementById('appscr72otherDeduction').checked = false;
}


function addCurrentIncomeDetail()
{
	if(editFlag == true)
		return true;
}

function incomeDidcrepancies(){
	//
}


//Keys
function addIncarceratedContent()
{

	//noOfHouseHold = $('#numberOfHouseHold').text();
	noOfHouseHold = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember.length;
	var i;
	var strOuter = "";
	
	var str = '<div class="control-group margin0"><p>'+jQuery.i18n.prop('ssap.whoIsIncarcerated')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>';

	for (i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var HouseHoldName = householdMemberObj.name.firstName+" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;
		//var HouseHoldName=""+$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

		str += '<div class="margin5-l"><label>'
		+'<input type="checkbox" id="incarcerationCB'+i+'" onchange="incarcerationCBChange('+i+')"  name="FNLNS" value="yes" data-parsley-required data-parsley-required-message="This Value Is Required"><span class="camelCaseName">'+HouseHoldName+' <span style="text-transform: none">is incarcerated</span></span></label></div>'
		+'<div id="incarcerationCBOption'+i+'" class="control-group margin0 hide"><p>'+jQuery.i18n.prop('ssap.isThisPersonPendingDisposition')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p><div class="margin5-l"><label>'
		+'<input type="radio" name="disposition'+i+'" value="yes" radio-name = "deposition" data-parsley-required data-parsley-required-message="This Value Is Required" />'+jQuery.i18n.prop('ssap.yes')+'</label>'
		+'<label><input type="radio" name="disposition'+i+'" value="no" radio-name = "deposition" data-parsley-required data-parsley-required-message="This Value Is Required" />'+jQuery.i18n.prop('ssap.no')+'</label></div></div>';
		

		if(i != noOfHouseHold)
		str +="<hr />";

		

	}
	str +='</div>';
	strOuter += str;
	document.getElementById('incarceratedContent85').innerHTML = strOuter;
	next();
}
function enableSubmitCSRApplicationType(){
	$('#btnSubmitCSRApplicationType').removeAttr('disabled');
}
function cancelCSRApplicationType(){	
	$('#modalCSRApplicationType').modal('hide');
}

function saveIncarceratedDetail()
{	
	/*pageValidation();*/	
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	//var userActiveRoleName = $('#userActiveRoleName').val();
	if("Y" == $("#trackApplicationSource").val()){
		$('#modalCSRApplicationType').modal();
	}else {
		saveIncarceratedDetailDB();
	}	
	return;	
}

function saveIncarceratedDetailDB()
{	
	/*pageValidation();*/	
	if(parsleyPageValidation($("#"+currentPage).find("form").attr("id")) == false){ 
		return;
	}
	var appendRowData='';
	noOfHouseHold = getNoOfHouseHolds();
	webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = false;
	for (i = 1; i <= noOfHouseHold; i++) {
		var householdMemberObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		householdMemberObj.incarcerationStatus.incarcerationStatusIndicator = false;
		householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator = false;
		
		if(document.getElementById("ononeIncarceratedStatus").checked == false){
			if(document.getElementById("incarcerationCB"+i).checked == true){
				householdMemberObj.incarcerationStatus.incarcerationStatusIndicator = true;
				//householdMemberObj.applyingForCoverageIndicator = false;
				webJson.singleStreamlinedApplication.incarcerationAsAttestedIndicator = true;
			
				if($('input[name=disposition'+i+']:radio:checked').val() == 'yes'){
					householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator = true;
				}else{
					householdMemberObj.incarcerationStatus.incarcerationPendingDispositionIndicator =false;
				}
			}
		}
		
		householdMemberObj.householdContact.homeAddress.primaryAddressCountyFipsCode = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0].householdContact.homeAddress.primaryAddressCountyFipsCode;
	}	
	//agreement
	var parimaryContactObj = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[0];
	if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'yes'){	
		webJson.singleStreamlinedApplication.consentAgreement = true;
		webJson.singleStreamlinedApplication.numberOfYearsAgreed = "";
	}else if($('input[name=useIncomeDataIndicator]:radio:checked').val() == 'no'){
		webJson.singleStreamlinedApplication.consentAgreement = false;
		webJson.singleStreamlinedApplication.numberOfYearsAgreed = $('input[name=taxReturnPeriod]:radio:checked').val();
	}
	for(var i=1; i<=noOfHouseHold; i++)	{
		if($("#appscr85incarcerationTD"+i).html()!= undefined)
		{
			updateIncarceratedDetail(i);
			continue;
		}

		if(document.getElementById("incarcerationCB"+i) != null && document.getElementById("incarcerationCB"+i).checked == true)
			appendRowData="<td  class='incarcerationIndicator' id='appscr85incarcerationTD"+i+"'>yes</td>";
		else
			appendRowData="<td class='incarcerationIndicator' id='appscr85incarcerationTD"+i+"'>no</td>";

		appendRowData+="<td class='incarcerationPendingDispositionIndicator' id='appscr85disposition"+i+"'>"+$('input[name=disposition'+i+']:radio:checked').val()+"</td>";
		 $('#appscr57HouseHoldPersonalInformation').find('#HouseHold'+i).append(appendRowData);
		 appendRowData='';
	}

	 webJson.singleStreamlinedApplication.applicationSignatureDate = getCurrentDateTime();
	 
	//Atleast 1 person should be seeking coverage to continue the application
	 if(coverageMinimumCheckWithUnder26Indicator() == false){
		$('#coverageMinimumAlert').modal();
		return;
	 }else{
 		var householdMemberArr = webJson.singleStreamlinedApplication.taxHousehold[0].householdMember;
 		for(var i = 0; i < householdMemberArr.length; i++){
 			
 			if(householdMemberArr[i].applyingForCoverageIndicator == true && householdMemberArr[i].under26Indicator == true){
 				
 				householdMemberArr[i].applyingForCoverageIndicator = true;
 			}
 			else{
 				
 				householdMemberArr[i].applyingForCoverageIndicator = false;
 			}
 			
 		}
	}
	 
     saveDataSubmit(); //suneel 05092014
     next();
}


function addQuestionSummery()
{
	noOfHouseHold = parseInt($('#numberOfHouseHold').text());
	if (currentPage == 'appscr81B' && addQuetionsHouseHoldDone < noOfHouseHold) {
		addQuetionsHouseHoldDone++;
		appscr75AEditController(addQuetionsHouseHoldDone);
	}else{
		next();
	}
	document.getElementById('appscr82addQuestionSummery').innerHTML = '';
	var i;
	var strOuter = "";

	for (i = 1; i <= noOfHouseHold; i++) {
		//var houseHoldName =$('#appscr57FirstNameTD'+i).text()+" "+$('#appscr57MiddleNameTD'+i).text()+" "+$('#appscr57LastNameTD'+i).text();

		householdMemberObj=webJson.singleStreamlinedApplication.taxHousehold[0].householdMember[i-1];
		var houseHoldName=""+householdMemberObj.name.firstName +" "+householdMemberObj.name.middleName+" "+householdMemberObj.name.lastName;

		var str ='<div class="control-group"><label class="pull-left"><strong>'+houseHoldName+'</strong>&nbsp;</label>';
		if(i==1){
			str+= jQuery.i18n.prop("ssap.primaryContact")+'&nbsp;';
		}

		str+='<input type="button" value="Edit" class="btn btn-primary" onclick="appscr75AEditController('+i+')"/></div>';
		/*
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.currentlyEnrolled")+'</label><div class="controls margin5-t">'
		+jQuery.i18n.prop("ssap.naYesNot")+'</div></div>'
		+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.hasOptionToEnroll")+'</label><div class="controls margin5-t">'
		+jQuery.i18n.prop("ssap.naEnrollmentOptions")+'</div></div>'; */

		if(i != noOfHouseHold)
			str+="<hr />";
			strOuter+= str;
	}
	document.getElementById('appscr82addQuestionSummery').innerHTML = strOuter;
	//next();
}

function getTotalDeductions(houseHoldID)
{
	totalDeduction= 0;	
	return totalDeduction;
}

function showIndividualDiv()
{
	editNonFinacial = false;
	editFlag = false;
	//$('#contBtn').text('Update');
	$('#countinue_button_div').removeClass("Continue_button");
	$('#countinue_button_div').addClass("ContinueForUpdate_button");
	$('#cancelButtonCancel').show();
	$('#back_button_div').hide();

	prev();
	var i= getNoOfHouseHolds();	
	noOfHouseHold++;	
	$('#numberOfHouseHold').text(noOfHouseHold);
	$('#noOfPeople53').val(noOfHouseHold);
	addAnotherIndividual((i+1));
	addMaskingOnAppscr57();
}

//Keys
function addAnotherIndividual(i)
{
	$('#cancelButtonCancel').show();
	$('#back_button_div').hide();
	cancleFlag = false;
	if($('#appscr57FirstName'+i).html() != undefined)
	{
		$('#addAnotherHouseHold'+noOfHouseHold).show();
		return false;
	}	
	
	
	var str='<div id="addAnotherHouseHold'+i+'">'
	+'<h4><strong id="applicantTitle57'+i+'" class = "camelCaseName">'+jQuery.i18n.prop("ssap.applicant")+'&nbsp;'+i+'</strong></h4>'
	+'<div class="margin5-b" id="duplicatedNameErrorDiv'+i+'"></div>'
	+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop('ssap.firstName')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
	+'<span aria-label="Required!"></span></label><div class="controls">'
	+'<input id="appscr57FirstName'+i+'"type="text" onkeyup="removeValidationFirstName('+i+'); setName('+i+')"'
	+ 'onclick="removeValidationFirstName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.firstName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.firstNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.firstNameRequired")+'" />'
	+'</div></div>'
	+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.middleName")+'</label><div class="controls">'
	+'<input id="appscr57MiddleName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')" data-parsley-trigger="change" data-parsley-pattern="^[a-zA-Z]{1,45}$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.middleNameValid")+'"'
	+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.middleName")+'" class="input-medium characterStroke">'
	+'</div></div>'
	+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.lastName")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
	+'<span aria-label="Required!"></span></label><div class="controls">'
	+'<input id="appscr57LastName'+i+'"type="text" onkeyup="removeValidationLastName('+i+'); setName('+i+')"'
	+ 'onclick="removeValidationLastName('+i+')" placeHolder="'+jQuery.i18n.prop("ssap.lastName")+'" class="input-medium characterStroke" data-parsley-trigger="change" data-parsley-pattern="^(([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z]))+((\\s|-)?([A-Za-z]|([O|L|D|o|l|d]\\\'[A-Za-z])))*)$"  data-parsley-pattern-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.lastNameValid")+'" />'
	+'</div></div>'
	+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.suffix")+'</label><div class="controls">'
	+'<select id="appscr57Suffix'+i+'" class="input-medium">'
	+'<option value="">'+jQuery.i18n.prop("ssap.suffix")+'</option>'
	+'<option value="'+jQuery.i18n.prop("ssap.sr")+'">'+jQuery.i18n.prop("ssap.sr")+'</option>'
	+'<option value="'+jQuery.i18n.prop("ssap.jr")+'">'+jQuery.i18n.prop("ssap.jr")+'</option>'
	+'<option value="'+jQuery.i18n.prop("ssap.III")+'">'+jQuery.i18n.prop("ssap.III")+'</option>'
	+'<option value="'+jQuery.i18n.prop("ssap.IV")+'">'+jQuery.i18n.prop("ssap.IV")+'</option>'
	+'</select></div></div>'
	+'<div class="control-group"><label class="required control-label">'+jQuery.i18n.prop("ssap.dateOfBirth")+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true">'
	+'<span aria-label="Required!"></span></label><div class="controls">'
	+'<div class="input-append date date-picker" id="calDiv'+i+'" data-date="">'
	+'<input type="text" onkeyup="removeValidationDob('+i+')" onclick="removeValidationDob('+i+')" id="appscr57DOB'+i+'" placeholder="'+jQuery.i18n.prop("ssap.mmDDYYYY")+'"'
	+'class="input-medium dob" '
	+'onfocusout = "validateBirthDate($(this))" '
	+'onblur = "validateBirthDate($(this))" '
	+'data-parsley-trigger="change" data-parsley-errors-container="#dateOfBirthErrorDiv'+i+'" data-parsley-required data-parsley-required-message="'+jQuery.i18n.prop("ssap.error.dobRequired")+'" '
	+'/><span class="add-on"><i class="icon-calendar"></i></span></div>'
	+'<div id="dateOfBirthErrorDiv'+i+'"></div>'
	+'</div></div>'
	/*hix-33010*/
	/*+'<div class="control-group"><label class="control-label">'+jQuery.i18n.prop("ssap.dateOfBirthVerified")+'</label><div class="controls">'
	+'<select id="appscr57DOBVerificationStatus'+i+'" class="input-medium">'
	+'<option value ="yes">'+jQuery.i18n.prop("ssap.yes")+'</option>'
	+'<option value ="no">'+jQuery.i18n.prop("ssap.no")+'</option>'
	+'</select></div></div>'*/
	+'<div class="control-group hide">'
	+'<p>'+jQuery.i18n.prop("ssap.has")+'&nbsp;<strong id="applicantName57'+i+'"></strong>&nbsp;'+jQuery.i18n.prop("ssap.usedTobaccoProducts")+'<img width="10" height="10"  src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></p>'
	+'<div class="controls">'
	+'<label>'
	+'<input type="radio" name="appscr57Tobacco'+ i+ '" value="yes" id="appscr57SexYes'+ i+ '" >'+jQuery.i18n.prop("ssap.yes")+'</label>'
	+'<label>'
	+'<input type="radio" name="appscr57Tobacco'+ i+ '" value="no" id="appscr57SexNo'+ i+ '">'+jQuery.i18n.prop("ssap.no")+'</label>'
	+'<span class="errorMessage"></span>'
	+'</div>'
	+'</div>'

	/*+'<div class="control-group hide"><label class="control-label">'+jQuery.i18n.prop('ssap.sex')+'<img width="10" height="10" alt="Required!" src="/hix/resources/images/requiredAsterix.png" aria-hidden="true"></label><div class="controls">'
	+'<div  class="isSex"><label><input type="radio" name="appscr57Sex'+i+'" value="male" id="appscr57SexMale'+i+'">'+jQuery.i18n.prop("ssap.male")+'</label>'
	+'<label><input type="radio" name="appscr57Sex'+i+'"value="female" id="appscr57SexFemale'+i+'">'+jQuery.i18n.prop("ssap.female")+'</label>'
	+'</div></div></div>'*/
	/*
	+'<div class="control-group margin20-t"><label class="control-label">Relationship to <strong id="appscr57relationshiplbl'+i+'"></strong> (Primary Contact)</label>'
	+'<div class="controls">'
	+'<select id="appscr57relationship'+i+'" name="appscr57relationship'+i+'" class="input-large">'
	+'<option value="0">Select</option>'
	+'<option value="Wife">Wife</option>'
	+'<option value="Child">Child</option>'
	+'<option value="Parent">Parent</option>'
	+'<option value="Caretaker">Caretaker</option>'
	+'</select>'
	+'<div class="help-inline" id="appscr57relationship_error"></div></div></div>'
	*/
	+'<div class="control-group margin20-t"><label class="controls capitalize-none"><input type="checkbox" name="appscr57checkseekingcoverage'+i+'" id="appscr57checkseekingcoverage'+i+'">Are you seeking coverage?</label></div>';


var existing = $('#appscr57HouseholdForm').html();
/*var strComplete = existing + str;*/
$('#appscr57HouseholdForm').append(str);
document.getElementById('numberOfHouseHold').innerHTML = i;

for(var j=1;j<=i;j++){
	 createCalendar(j);
	 /*$('.characterStroke').ForceCharacterOnly();*/
}

//removeRequiredValidation();

}

function setName(i)
{	
	var applicantName = getNameByPersonId(i);
	if( applicantName == ""){
		if(($('#appscr57FirstName' + i).val() !="") || ($('#appscr57MiddleName' + i).val() !="") || ($('#appscr57LastName' + i).val() )!=""){
			applicantName = $('#appscr57FirstName' + i).val()+" "+ $('#appscr57MiddleName' + i).val()+" "+ $('#appscr57LastName' + i).val();
		}
	}
	$('#applicantName57'+i).text(applicantName);
	$('#applicantTitle57'+i).text(applicantName);
	$('#appscr57relationshiplbl'+i).text(applicantName);
}

function setHisOrHerAfterContinue(){


	$('#appscr61SSNHisHer').text('');
	$('#appscr62Part2SSNHisHer').text('');
	$('#appscr63ChildHeShe').text('');
	if($('input[name=appscr61_gender]:radio:checked').val()== 'male' || $('input[name=gender]:radio:checked').val()== 'male'){
		$('#appscr61SSNHisHer').text('his');
		$('#appscr62Part2SSNHisHer').text('his');
		$('#appscr63ChildHeShe').text('he');
		$('#appscr61HimOrHer').text('him');
	}
	else{
		$('#appscr61SSNHisHer').text('her');
		$('#appscr62Part2SSNHisHer').text('her');
		$('#appscr63ChildHeShe').text('she');
		$('#appscr61HimOrHer').text('her');

	}
}

function addChildRelatedDetails(i,dateOfBirth)
{
	var childData="";

	childData+="<td id='appscr57IsChildTD" + i + "' class='isChild'>";
	if(checkBirthDate(dateOfBirth)<(365*18))
		childData+="yes";
	else
		childData+="no";

	childData+= "</td>";


	childData+="<td id='appscr57IsInfantTD" + i + "' class='isInfant'>";
	if(checkBirthDate(dateOfBirth)<365)
		childData+="yes";
	else
		childData+="no";

	childData+="</td>";


	return childData;
}

function updateInfantRelatedDetails(i,dateOfBirth)
{
	return "no"; // we do no consider infant, HIX-42563	
	if(checkBirthDate(dateOfBirth)<365)
		return "yes";
	else
		return "no";
}

function updateChildRelatedDetails(i,dateOfBirth)
{
	if(checkBirthDate(dateOfBirth)<(365*18))
		return "yes";
	else
		return "no";
}



function getInfantData(tdIndex)
{
	return "no"; // we do no consider infant, HIX-42563	
	var isInfant = $('#appscr57IsInfantTD'+(tdIndex)).html();
	if(isInfant!='undefined')
		return isInfant;
	else
		return no;
}

var HoldContactDetailsArray = new Array();
HoldContactDetailsArray[0] = 'firstName';
HoldContactDetailsArray[1]= 'middleName';
HoldContactDetailsArray[2] = 'lastName';
HoldContactDetailsArray[3] = 'suffix';
HoldContactDetailsArray[4] = 'dateOfBirth';
HoldContactDetailsArray[5] = 'emailAddress';
HoldContactDetailsArray[6] = 'home_addressLine1';
HoldContactDetailsArray[7] = 'home_addressLine2';
HoldContactDetailsArray[8] = 'home_primary_city';
HoldContactDetailsArray[9] = 'home_primary_zip';
HoldContactDetailsArray[10] = 'home_primary_state';
HoldContactDetailsArray[11] = 'home_primary_county';
HoldContactDetailsArray[12] = 'mailing_addressLine1';
HoldContactDetailsArray[13] = 'mailing_addressLine2';
HoldContactDetailsArray[14] = 'mailing_primary_city';
HoldContactDetailsArray[15] = 'mailing_primary_zip';
HoldContactDetailsArray[16] = 'mailing_primary_county';
HoldContactDetailsArray[17] = 'first_phoneNo';
HoldContactDetailsArray[18] = 'first_ext';
HoldContactDetailsArray[19] = 'first_phoneType';
HoldContactDetailsArray[20] = 'second_phoneNo';
HoldContactDetailsArray[21] = 'second_ext';
HoldContactDetailsArray[22] = 'second_phoneType';
HoldContactDetailsArray[23] = 'preffegred_spoken_language';
HoldContactDetailsArray[24] = 'preffered_written_language';

function createCalendar(i){
	$('#calDiv'+i).on('click',function() {
		$('#calDiv'+i).each(function() {
			$(this).datepicker({
			    autoclose: true,
			    dateFormat: 'MM/DD/YYYY'
			});

		});
	});
	$('#calDiv'+i).each(function() {
		$(this).datepicker({
		    autoclose: true,
		    dateFormat: 'MM/DD/YYYY'
		});
	});	
}

function validateBirthDate(memberDob){	
	var strDate = memberDob.val();
	var mon = parseInt(strDate.substring(0, 2), 10);
	var dt = parseInt(strDate.substring(3, 5), 10);
	var yr = parseInt(strDate.substring(6, 10), 10);	
	var validDate =/(0[1-9]|1[012])[\/](0[1-9]|[12][0-9]|3[01])[\/](19|20)\d\d/;
	var today = new Date();	
	var today_mon = today.getMonth()+1;
	var today_dt = today.getDate();
	var today_yr = today.getFullYear();	
	if(memberDob.closest('div.controls').find('.dobCustomError').length == 0){
		memberDob.closest('div.controls').append('<div class ="dobCustomError"></div>');
	};
	
	var dobCustomError = memberDob.closest('div.controls').find('.dobCustomError');
	if(validDate.test(strDate)){
	if (yr > today_yr) {	
		dobCustomError.html('');
		dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
		dateValidation = false;
	} else if ((yr == today_yr) && mon > today_mon) {
		dobCustomError.html('');
		dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
		dateValidation = false;
	} else if ((yr == today_yr) && (mon == today_mon) && (dt > today_dt)) {
		dobCustomError.html('');
		dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
		dateValidation = false;		
	} else if ((today_yr - yr) >= 104) {
		dobCustomError.html('');
		dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date ,Age Is More Than 104.</span>');
		dateValidation = false;		
	}else{
		dobCustomError.html('');
		dateValidation = true;
	}
	} else{
		dobCustomError.html('');
		dobCustomError.html('<span class="validation-error-text-container">Please Enter valid Date.</span>');
		dateValidation = false;		
		
	}
	//alert("I am here");
}