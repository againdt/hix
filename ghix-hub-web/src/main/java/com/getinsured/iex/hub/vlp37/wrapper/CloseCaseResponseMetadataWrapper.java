package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpcc.ResponseMetadataType;


/**
 * Wrapper class for gov.hhs.cms.dsh.sim.ee.vlp.ResponseMetadataType
 * 
 */
public class CloseCaseResponseMetadataWrapper implements JSONAware{
	
	private String responseCode;
	private String responseDescriptionText;
	private String tdsResponseDescriptionText;
    
    public CloseCaseResponseMetadataWrapper(ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
    public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpsav.ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
    public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vilpsav.ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
    public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpcc.ResponseMetadataType responseMetadataType){
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.gcd.ResponseMetadataType responseMetadataType){
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
    public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlpcoi.ResponseMetadataType responseMetadataType){
    	
    	if(responseMetadataType == null){
    		return;
    	}
    	
    	this.responseCode = responseMetadataType.getResponseCode();
    	this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
    	this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
    }
    
	public CloseCaseResponseMetadataWrapper() {
		// TODO Auto-generated constructor stub
	}

	public CloseCaseResponseMetadataWrapper(com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vclpcc_v33.ResponseMetadataType responseMetadataType) {
		this.responseCode = responseMetadataType.getResponseCode();
		this.responseDescriptionText = responseMetadataType.getResponseDescriptionText();
		this.tdsResponseDescriptionText = responseMetadataType.getTDSResponseDescriptionText();
	}

	public String getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(String responseCode) {
		this.responseCode = responseCode;
	}

	public String getResponseDescriptionText() {
		return responseDescriptionText;
	}

	public void setResponseDescriptionText(String responseDescriptionText) {
		this.responseDescriptionText = responseDescriptionText;
	}

	public String getTdsResponseDescriptionText() {
		return tdsResponseDescriptionText;
	}

	public void setTdsResponseDescriptionText(String tdsResponseDescriptionText) {
		this.tdsResponseDescriptionText = tdsResponseDescriptionText;
	}
    
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("ResponseCode", this.responseCode);
		obj.put("ResponseDescriptionText", this.responseDescriptionText);
		obj.put("TDSResponseDescriptionText", this.tdsResponseDescriptionText);
		return obj.toJSONString();
	}
	
}
