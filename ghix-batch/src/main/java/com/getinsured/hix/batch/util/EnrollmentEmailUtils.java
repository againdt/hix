package com.getinsured.hix.batch.util;

import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.email.service.EnrollmentBatchFailureEmailNotificationService;
import com.getinsured.hix.dto.enrollment.EnrollmentBatchEmailDTO;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.notification.exception.NotificationTypeNotFound;

@Component
public class EnrollmentEmailUtils {

	@Autowired private EnrollmentBatchFailureEmailNotificationService enrollmentBatchFailureEmailNotificationService;
	
	public  void enrollmentBatchFailureEmailNotification(EnrollmentBatchEmailDTO enrollmentBatchEmailDTO, ChunkContext chunkContext){
    	
		try {
			enrollmentBatchEmailDTO.setTo(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENTBATCHFAILUREEMAILNOTIFICATIONTO));
			enrollmentBatchEmailDTO.setFrom(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENTBATCHFAILUREEMAILNOTIFICATIONFROM));
			enrollmentBatchFailureEmailNotificationService.sendEnrollmentBatchFailureEMailNotification(enrollmentBatchEmailDTO,chunkContext);
			
		} catch (NotificationTypeNotFound e1) {
			// TODO Auto-generated catch block
			
		}
	}
}
