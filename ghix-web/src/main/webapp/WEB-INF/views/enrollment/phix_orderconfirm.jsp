<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page import="java.util.List, com.getinsured.hix.model.*"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld"%>
<style type="text/css">
body {
	padding-top: 40px;
	padding-bottom: 0px;
}

.sidebar-nav {
	padding: 9px 0;
}
#menu {
	display: none;
}
</style>

<style type="text/css">
.accordion-inner {
	padding: 10px 20px 0 20px;
}

.accordion-inner  h3 {
	margin-bottom: 15px;
	color: #ccc;
	border-bottom: solid 1px #e1e3e3;
}

.accordion-inner #sliderEmp,.accordion-inner #sliderDep {
	width: 95%;
	margin: 50px 20px 0px 20px;
}

.no-health-plan {
	background: #f9f7c1;
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.no-dental-plan {
	border: 1px dashed #ccc;
	padding: 36px 20px 30px;
}

.cart {
	border: 1px solid #c9c9c9;
}

h3 {
	line-height: 25px;
	margin: 0
}

.darkblue {
	background: #0c9bd2;
	color: #fff;
}

.darkgreen {
	background: #008080;
	color: #fff;
}

.SILVER {
	background: url(../resources/img/silver-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.GOLD {
	background: url(../resources/img/gold-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.BRONZE {
	background: url(../resources/img/bronze-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.CATASTROPHIC {
	background: url(../resources/img/catastrophic-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.PLATINUM {
	background: url(../resources/img/platinum-corner.png) no-repeat -4% -13%;
	height: 50px;
}

.planimg {
	margin-top: 20px;
	margin-left: 30px;
	padding-bottom: 10px;
}

.closeout {
	position: relative;
	top: 24px;
	right: 10px;
	margin: -19px 0 0 0;
}

.mini-cart {
	width: 380px;
	border: 4px solid #4f4f4f;
	background: #fff;
	position: absolute;
	z-index: 10;
}

.mini-cart h3 {
	color: #fff;
}

.mini-cart .header {
	background: #4f4f4f;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
		$('.info').tooltip();

		$('.remove').hide();
		$('.close').hover(function() {
			$(this).prev('div').toggle();
		});
	});

	function gotoparent() {
		window.top.location.href = document.getElementById("redirectLink").value;
	}
</script>



<div class="row-fluid">
	<div class="gutter10">
		<div class="row-fluid">
			<div class="span3" id="sidebar">

				<div class="header">
					<h4><spring:message code='enroll.chkOut' /></h4>
				</div>
				<ol class="nav nav-list">
					<li class="done"> <i class="icon-ok"></i>
						<spring:message code='enroll.cart'/>
					</li>
					<li class="done"> <i class="icon-ok"></i>
						<spring:message code='enroll.eSignature'/>
					</li>
					<li class="active">
						<spring:message code='enroll.conf'/>
					</li>
				</ol>
			</div>
			<!--sidebar-->

			<div class="span9" id="rightpanel">
				<c:if test="${errorMsg == ''}">
					<div class="gutter10">
						<h3>
							<spring:message code='enroll.orderConfirm.confirm' />
						</h3>
						<c:choose>
							<c:when test="${isBBUser==true}">
								<p>
									<spring:message
										code='enroll.phix.bb.orderConfirm.confirm.value' />
								</p>
							</c:when>
							<c:otherwise>
								<p>
									<spring:message
										code='enroll.phix.orderConfirm.confirm.value'/>
								</p>
							</c:otherwise>
						</c:choose>

						<form name="frmorderconfirm" id="frmorderconfirm"
							action="orderconfirmsubmit" method="post">
							<input type="hidden" name="cart_id" id="cart_id"
								value="${cartId}"> <input type="hidden"
								name="redirectLink" id="redirectLink" value="${redirectLink}">


							<c:if test="${fn:length(enrollmentMap) gt 0}">
								<c:forEach var="enrollmentMapEntry" items="${enrollmentMap}">
									<c:set var="enrollmentlist" value="${enrollmentMapEntry.value}"></c:set>
									<c:if test="${fn:length(enrollmentlist) gt 0}">
										<div class="cart">
											<h4 class="graydrkbg gutter10">
												<spring:message
													code='enroll.orderConfirm.${enrollmentMapEntry.key}' />
											</h4>

											<c:forEach items="${enrollmentlist}" var="enrollmentObj">
												<fmt:formatDate pattern="MM/dd/yyyy"
													value="${enrollmentObj.benefitEffectiveDate}"
													var="effDate" />
												<div class="gutter10">
													<div class="health-plan">
														<c:choose>
															<c:when
																test="${enrollment_type == enrollment_type_individual}">
																<p>
																	<strong>${enrolleeNameMap[enrollmentObj.id]}</strong><span
																		class="pull-right"><spring:message
																			code='enroll.orderConfirm.effDate' /> &nbsp;<strong>${effDate}</strong>
																	</span>
																</p>
															</c:when>
															<c:when test="${enrollment_type == enrollment_type_shop}">
																<p>
																	${enrollmentObj.sponsorName}
																	<spring:message
																		code='enroll.orderConfirm.sponsoredPlanFor' />
																	&nbsp; <strong>
																		${enrolleeNameMap[enrollmentObj.id]}</strong><span
																		class="pull-right"><spring:message
																			code='enroll.orderConfirm.effDate' /> &nbsp; <strong>${effDate}</strong>
																	</span>
																</p>
															</c:when>
														</c:choose>
														<c:if test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
														<div class="pull-right closeout">
															<div class="remove pull-left">
																<spring:message code='enroll.orderConfirm.remove' />
															</div>
															<button type="button" title="x" class="close" data-dismiss="modal"
																aria-hidden="true">�</button>

														</div>
														</c:if>
														<table class="table table-border" >
															<tbody>
																<tr>
																	<c:choose>
																			<c:when
																				test="${enrollmentObj.planLevel == 'GOLD'}">
																				<td class="padding0 GOLD">
																					<div class="planimg">
																						<img src='<c:url value="/resources/img/logo_${fn:trim(fn:toLowerCase(enrollmentObj.insurerName))}.png"/>' alt="Issuer name logo">
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'SILVER'}">
																				<td class="padding0 SILVER">
																					<div class="planimg">
																						<img src='<c:url value="/resources/img/logo_${fn:trim(fn:toLowerCase(enrollmentObj.insurerName))}.png"/>' alt="Issuer name logo">
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'BRONZE'}">
																				<td class="padding0 BRONZE">
																					<div class="planimg">
																						<img src='<c:url value="/resources/img/logo_${fn:trim(fn:toLowerCase(enrollmentObj.insurerName))}.png"/>' alt="Issuer name logo">
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'PLATINUM'}">
																				<td class="padding0 PLATINUM">
																					<div class="planimg">
																						<img src='<c:url value="/resources/img/logo_${fn:trim(fn:toLowerCase(enrollmentObj.insurerName))}.png"/>' alt="Issuer name logo">
																						<br>
																					</div>
																				</td>
																			</c:when>
																			<c:when
																				test="${enrollmentObj.planLevel == 'CATASTROPHIC'}">
																				<td class="padding0 CATASTROPHIC">
																					<div class="planimg">
																						<img src='<c:url value="/resources/img/logo_${fn:trim(fn:toLowerCase(enrollmentObj.insurerName))}.png"/>' alt="Issuer name logo">
																						<br>
																					</div>
																				</td>
																			</c:when>
																	</c:choose>
																	<td><strong>${enrollmentObj.insurerName}</strong><br>
																		<a href="#">${enrollmentObj.planName}</a></td>
																	<td><spring:message
																			code='enroll.orderConfirm.mnthlyPremium' /><br>
																		<c:choose>
																			<%-- <c:when
																				test="${enrollment_type == enrollment_type_individual}">
																				<spring:message code='enroll.orderConfirm.taxCredit' />
																			</c:when> --%>
																			<c:when
																				test="${enrollment_type == enrollment_type_shop}">
																				<spring:message
																					code='enroll.orderConfirm.employerContribution' />
																			</c:when>
																		</c:choose></td>



																	<td class="txt-right"><strong>$ <fmt:formatNumber
																				value="${enrollmentObj.grossPremiumAmt}"
																				minFractionDigits="2" />   
																				<c:choose>
																				<%-- <c:when
																					test="${enrollment_type == enrollment_type_individual}">
																					<fmt:formatNumber value="${enrollmentObj.aptcAmt}"
																						minFractionDigits="2" />
																				</c:when> --%>
																				<c:when
																					test="${enrollment_type == enrollment_type_shop}">
																					<br>-$
																					<fmt:formatNumber
																						value="${enrollmentObj.employerContribution}"
																						minFractionDigits="2" />
																				</c:when>
																			</c:choose>
																	</strong></td>
																	<td></td>
																</tr>
																<c:if test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
																<tr class="blue">
																	<td>&nbsp;</td>
																	<td>&nbsp;</td>
																	
																	<td>
																	<c:choose>
																			<c:when
																				test="${enrollment_type == enrollment_type_individual}">
																				<spring:message code='enroll.orderConfirm.payment' />
																			</c:when>
																			<c:when
																				test="${enrollment_type == enrollment_type_shop}">
																				<spring:message
																					code='enroll.orderConfirm.payrollDeduction' />
																			</c:when>

																		</c:choose></td>
																		
																	<td colspan="2" class="txt-right"><h3>
																			$
																			<fmt:formatNumber
																				value="${enrollmentObj.netPremiumAmt}"
																				minFractionDigits="2" />
																			/<small>mo</small>
																		</h3></td>
																</tr>
																</c:if>
															</tbody>
														</table>

													</div>

												</div>
											</c:forEach>
										</div>
									</c:if>
								</c:forEach>

								<c:choose>
									<%--Totals Table for Individual  --%>
									<c:when test="${enrollment_type == enrollment_type_individual}">
										<table class="table table-border-none table-condensed totalTable">
											<tr class="darkblue">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.allPlans" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalMonthlyPremium' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalMonthlyPremium}"
															minFractionDigits="2" />

													</h3></td>
											</tr>
											<tr class="darkblue">
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.mnthlyTaxCredit'/></td>
												<td class="txt-right"><h3>
														-$
														<fmt:formatNumber value="${totalAptcContribution}"
															minFractionDigits="2" />

													</h3></td>
											</tr>

											<tr class="darkgreen">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.cartTotal" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalPayment' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalCost}"
															minFractionDigits="2" />
														/<small>mo</small>
													</h3></td>
											</tr>
										</table>
									</c:when>

									<%--Totals Table for Employee  --%>
									<c:when test="${enrollment_type == enrollment_type_shop}">
										<table class="table table-border-none table-condensed totalTable">
											<tr class="darkblue">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.allPlans" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalMonthlyPremium' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalMonthlyPremium}"
															minFractionDigits="2" />

													</h3></td>
											</tr>
											<tr class="darkblue">
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalEmployerContribution' /></td>
												<td class="txt-right"><h3>
														-$
														<fmt:formatNumber value="${totalEmployerContribution}"
															minFractionDigits="2" />

													</h3></td>
											</tr>

											<tr class="darkgreen">
											<c:choose>
											<c:when test="${fn:length(enrollmentMap) gt 1 || fn:length(enrollmentlist) gt 1}">
												<td><h3 class="gutter10-lr">
														<spring:message code="enroll.orderConfirm.cartTotal" />
												</h3></td>
											</c:when>
											<c:otherwise>
												<td><h3 class="gutter10-lr">&nbsp;</h3></td>
											</c:otherwise>
											</c:choose>
												<td class="txt-right"><spring:message
														code='enroll.orderConfirm.totalPayrollDeduct' /></td>
												<td class="txt-right"><h3>
														$
														<fmt:formatNumber value="${totalCost}"
															minFractionDigits="2" />
														/<small>mo</small>
													</h3></td>
											</tr>
										</table>
									</c:when>
								</c:choose>
							</c:if>
							<br>
							<c:if test="${showChngPlans==true}">

								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.mngPlanChng' />
								</h4>
								<div class="gutter10">
									<c:choose>
										<c:when
											test="${enrollment_type == enrollment_type_individual}">
											<p>
												<spring:message
													code='enroll.phix.makingChangesToPlan.value' />
											</p>
										</c:when>
										<c:when test="${enrollment_type == enrollment_type_shop}">
											<p>
												<spring:message
													code='enroll.phix.makingChangesToPlan.value' />
											</p>
										</c:when>
									</c:choose>
								</div>
							</c:if>
							<c:if test="${fn:length(disEnrollmentMap) gt 0}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disenrollmentsFromPlanTitle' />
								</h4>
								<div class="gutter10">
									<p>
										<spring:message code='enroll.orderConfirm.disenrollmentsFromPlan' />
									</p>
									
									<p>
									<c:forEach var="disEnrollment" items="${disEnrollmentMap}">
  										&nbsp<b><c:out value="${disEnrollment.key}"/>: &nbsp <c:out value="${disEnrollment.value}"/></b><br>
									</c:forEach>
									</p>
								</div>
							</c:if>
							<c:if test="${showDisc==true}">
								<h4 class="graydrkbg gutter10 marginTop20">
									<spring:message code='enroll.orderConfirm.disclaimer' />
								</h4>
								<div class="gutter10">
									<c:choose>
										<c:when
											test="${enrollment_type == enrollment_type_individual}">
											<p>
												<spring:message
													code='enroll.phix.orderConfirm.individual.disclaimer.value' />
											</p>
										</c:when>
										<c:when test="${enrollment_type == enrollment_type_shop}">
											<p>
												<spring:message
													code='enroll.phix.orderConfirm.employee.disclaimer.value' />
											</p>
										</c:when>
									</c:choose>
								</div>
							</c:if>
							<div><b><spring:message code='enroll.phix.makePayment.title'/></b></div>
							<div class="form-actions margin-top50 form-horizontal">
								<div class="controls">
									<a href="#" class="btn" onclick="window.print()"> <i class="icon-print"></i>
										 <spring:message
											code='enroll.orderConfirm.print' /> </a> <a href="../shop/employee/home"
										onclick="gotoparent()" class="btn"><spring:message
											code='enroll.phix.makePayment.value' /></a>
								</div>
							</div>
						</form>
					</div>
				</c:if>
			</div>
		</div>
	</div>
</div>


