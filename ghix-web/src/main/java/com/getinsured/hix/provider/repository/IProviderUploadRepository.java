package com.getinsured.hix.provider.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.ProviderUpload;

public interface IProviderUploadRepository extends JpaRepository<ProviderUpload, Integer> {

}
