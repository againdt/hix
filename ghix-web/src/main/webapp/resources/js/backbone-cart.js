(function (backbone) {
	/**
	 * @class
	 * Cart
	 */
	backbone.Cart = {
		/**  how many items to show per page */
		perPage : 5,
		
		/** page to start off on */
		page : 1,
	
		/**
		 *
		 */
		initialize: function(){

			this.resetProps();
		},
		
		resetProps: function(){
			this.cartSummary = [];
			this.cartSummary.planCount = 0;
			this.cartSummary.platinum = 0;
			this.cartSummary.gold = 0;
			this.cartSummary.silver = 0;
			this.cartSummary.bronze = 0;
			this.cartSummary.platinumLevel = 0;
			this.cartSummary.goldLevel = 0;
			this.cartSummary.silverLevel = 0;
			this.cartSummary.bronzeLevel = 0;
			this.cartSummary.platinumLevelCount = 0;
			this.cartSummary.goldLevelCount = 0;
			this.cartSummary.silverLevelCount = 0;
			this.cartSummary.bronzeLevelCount = 0;
			
			this.cartItems = [];
		},
		
		setCartSummary: function (models) {
			var self = this;
			if(!models){
				models = self.models;
			}
			self.cartSummary['planCount'] = models.length;
			if(models.length){
				_.each(models, function(model){
					if(model.get("planId") == 0){
						self.cartItems.push(model.get("level").toLowerCase());
						self.cartSummary[model.get("level").toLowerCase()+"Level"]++;
						self.cartSummary[model.get("level").toLowerCase()+"LevelCount"] = model.get("planCount");
					}else{
						self.cartItems.push(model.get("planId"));
						self.cartSummary[model.get("level").toLowerCase()]++;
					}
				});
			}else{
				self.resetProps();
			}
			self.reset(models);
		},
		
		removeSelectedItems: function () {
			var self = this
			var itemId = "";
			var updatedModels = [];
			var srNo = 0;
			_.each(self.models, function(model){
				itemId = model.get("id");
				if($("#item_"+itemId).is(':checked')){
					model.destroy();
				}else{
					srNo++;
					model.set({srNo: srNo});
					model.change();
					updatedModels.push(model);
				}
			});
			this.resetProps();
			this.setCartSummary(updatedModels);
		},
		
		selectAllItems: function () {
			var self = this
			var itemId = "";
			_.each(self.models, function(model){
				itemId = model.get("id");
				$("#item_"+itemId).attr('checked',true);
			});
		},
	};
	
})(App.backbone);