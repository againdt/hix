package com.getinsured.hix.batch.enrollment.processor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

import com.getinsured.hix.batch.enrollment.service.EnrollmentAnnualIrsReportService;
import com.getinsured.hix.batch.enrollment.skip.Enrollment1095StagingParams;



@Component("enrollmentAnnualIRSOutProcessor")
public class EnrollmentAnnualIRSOutProcessor implements ItemProcessor< Integer, Integer> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EnrollmentAnnualIRSOutProcessor.class);
	
	private EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService;
	private Enrollment1095StagingParams enrollment1095StagingParams;
	
	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution){
		
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Processor ");
		ExecutionContext ec = stepExecution.getExecutionContext();
		
	}
	@Override
	public Integer process(Integer enrollmentId){
		LOGGER.info("Process :: " + enrollmentId);
		return enrollmentId;
	}
	
	public EnrollmentAnnualIrsReportService getEnrollmentAnnualIrsReportService() {
		return enrollmentAnnualIrsReportService;
	}
	public void setEnrollmentAnnualIrsReportService(
			EnrollmentAnnualIrsReportService enrollmentAnnualIrsReportService) {
		this.enrollmentAnnualIrsReportService = enrollmentAnnualIrsReportService;
	}
	public Enrollment1095StagingParams getEnrollment1095StagingParams() {
		return enrollment1095StagingParams;
	}
	public void setEnrollment1095StagingParams(Enrollment1095StagingParams enrollment1095StagingParams) {
		this.enrollment1095StagingParams = enrollment1095StagingParams;
	}
}