package com.getinsured.eligibility.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.eligibility.model.EligibilityProgram;

public interface IEligibilityProgram extends JpaRepository<EligibilityProgram, Long> {

	List<EligibilityProgram> findBySsapApplicantId(Long ssapApplicantId);

	/**
	 * HQL-JPA query to return List<EligibilityProgramDTO> for ssapApplicantId.
	 *
	 * This helps in leveraging LAZY behavior as default
	 * for EligibilityProgramDTO and  EligibilityProgramGiwspayload entities.
	 *
	 * @param ssapApplicantId
	 * @return List<EligibilityProgramDTO> including all linked EligibilityProgramGiwspayload objects
	 */
	/*@Query("Select ep " +
			" FROM EligibilityProgramDTO as ep "+
			" inner join fetch ep.eligibilityProgramGiwspayload as epGiPayloads"+
			" where ep.ssapApplicantId = :ssapApplicantId ")*/

	@Query("Select ep " +
			" FROM EligibilityProgram as ep "+
			" inner join fetch ep.eligibilityProgramGiwspayload as epGiPayloads"+
			" inner join fetch ep.ssapApplicant as ssapApplicant"+
			" where ssapApplicant.id = :ssapApplicantId ")
	List<EligibilityProgram> getEligibilities(@Param("ssapApplicantId") Long ssapApplicantId);


	@Query("Select ep " +
			" FROM EligibilityProgram as ep "+
			" inner join fetch ep.ssapApplicant as ssapApplicant"+
			" inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
			" where ssapApplication.id = :ssapApplicantionId ")
	List<EligibilityProgram> getEligibilitiesForApplicantion(@Param("ssapApplicantionId") Long ssapApplicantionId);

    @Query("Select ep " +
            " FROM EligibilityProgram as ep "+
            " inner join fetch ep.ssapApplicant as ssapApplicant"+
            " where ssapApplicant.id = :ssapApplicantId ")
    List<EligibilityProgram> getApplicantEligibilities(@Param("ssapApplicantId") Long ssapApplicantId);

    @Query("Select ep " +
            " FROM EligibilityProgram as ep "+
            " inner join fetch ep.ssapApplicant as ssapApplicant"+
            " inner join fetch ssapApplicant.ssapApplication as ssapApplication"+
            " where ssapApplicant.personId = 1 "
            + " and ssapApplication.id = :ssapApplicationId ")
    List<EligibilityProgram> getPrimaryApplicantEligibilities(@Param("ssapApplicationId") Long ssapApplicationId);
    
    @Query( " Select ep " +
            " FROM EligibilityProgram as ep "+
    		" inner join fetch ep.ssapApplicant as ssapApplicant"+
            " where ssapApplicant.applicantGuid IN (:ssapApplicantId) AND TO_CHAR(ep.eligibilityStartDate, 'yyyy') = :coverageYear ")
    List<EligibilityProgram> getApplicantEligibilitiesByYear(@Param("ssapApplicantId") List<String> ssapApplicantId, @Param("coverageYear") String coverageYear);
    
    @Query("Select eligibilityProgram " +
			" FROM EligibilityProgram as eligibilityProgram "+
			" where eligibilityProgram.ssapApplicant.ssapApplication.id = :applicationId "+
			" and eligibilityType in('AssessedCHIPEligibilityType','AssessedMedicaidMAGIEligibilityType', 'AssessedMedicaidNonMAGIEligibilityType')"+
			" and eligibilityIndicator = 'TRUE'"+
			" and eligibilityProgram.ssapApplicant.id = :applicantId"
			)
    List<EligibilityProgram> getAssessedChipAndMedicaidEligibilityByApplicant(@Param("applicationId") Long applicationId, @Param("applicantId") Long applicantId);
}
