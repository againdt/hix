<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<!-- Tag library for showing comments -->
<%@ taglib prefix="comment" uri="/WEB-INF/tld/comments-view.tld"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>

<%-- Secure Inbox Start--%>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/jquery.fileupload-ui.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/js/upload/css/style.css" />" />
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/chosen.css" />" />
<%-- <link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/inbox.css" />" /> --%>

<!-- File upload scripts -->
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.ui.widget.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.iframe-transport.js" />"></script> 
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-fp.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/upload/jquery.fileupload-ui.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/chosen.jquery.js" />"></script>
<%-- Secure Inbox End--%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.form.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js" />"></script>

<c:set var="individualUserId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
<c:set var="isIndividualUserExists" value="${isIndividualUserExists}" ></c:set>
<c:set var="isConsumerActivated" value="${isConsumerActivated}" ></c:set>
<script type="text/javascript">
  $(function() {
    $(".newcommentiframe").click(function(e){
          e.preventDefault();
          var href = $(this).attr('href');
          if (href.indexOf('#') != 0) {
             $('<div id="newcommentiframe" class="modal"><div class="modal-body"><iframe id="newcommentiframe" src="' + href + '" style="overflow-x:hidden;width:100%;border:0;margin:0;padding:0;height:340px;"></iframe></div></div>').modal({backdrop:false});
      }
    });
  });
  
  function closeIFrame() {
    $("#newcommentiframe").remove();
    window.location.href = '<c:url value="/individual/individualcomments/"/>'+'${individualUserId}';
  }
  
  function closeCommentBox() {
	  $("#newcommentiframe").remove();	  
	  var url = '<c:url value="/individual/individualcomments/"/>'+'${individualUserId}';
	  window.location.assign(url);
  }
  
  function setPopupValue() {
		if(document.getElementById("frmPopup").elements['indchkshowpopup'].checked) {
			document.frmPopup.showPopupInFuture.value = "N";
		}
  }
</script>

<div class="gutter10-lr">
	<div class="l-page-breadcrumb">
		<!--start page-breadcrumb -->
		<div class="row-fluid">
			<ul class="page-breadcrumb">
				<li><a href="javascript:history.back()">&lt; <spring:message
							code="label.back" /></a></li>
				<li><a
					href="<c:url value="/broker/individuals"/>"><spring:message
						code="label.individuals" /></a></li>
				<li><spring:message code="label.brkactive"/></li>
				<li>${individualUser.firstName} ${individualUser.lastName}</li>
				<li><spring:message code="label.employeeDetails.Summary"/></li>
			</ul>
		</div>
		<!--  end of row-fluid -->
	</div>
	<!--  end l-page-breadcrumb -->
	<div class="row-fluid">
  <div style="font-size: 14px; color: red">
    <c:if test="${errorMsg != 'false'}">
      <c:out value="${errorMsg}"></c:out>
    </c:if>
  </div>
</div>


<!--  Latest UI -->
	<c:if test="${message != null}">
	<div class="errorblock alert alert-info">
		<p>${message}</p>
	</div>
	</c:if>


	<h1>
    ${individualUser.firstName} ${individualUser.lastName}
	</h1>
  <div class="row-fluid">
    <div id="sidebar" class="span3">
        <h4 class="header"></h4>
        <ul class="nav nav-list">         
          <li class="active"><spring:message code="label.employeeDetails.Summary"/></li>
		
          <li class="link"><a href="<c:url value="/individual/individualcomments/${individualUserId}"/>"><spring:message code="label.employeeDetails.Comments"/></a></li>
        </ul>
      <br>
        <h4 class="header"><i class="icon-cog icon-white"></i> <spring:message code="label.brkactions"/></h4>
        <ul class="nav nav-list">  
        <li><a name="addComment" href="<c:url value="/individual/newcomment?target_id=${individualUserId}&target_name=DESIGNATEBROKER&individualName=${individualUser.firstName} ${individualUser.lastName}"/>" id ="addComment" class="newcommentiframe"> <i class="icon-comment"></i><spring:message code="label.employeeDetails.NewComments"/></a></li>      
       <c:if test="${userActiveRoleName != 'admin' && userActiveRoleName != 'broker_admin'}">
        	<li>
		        <c:choose>
					<c:when test="${showSwitchRolePopup == \"N\"}">					
						<a href="#" onclick="document.frmPopup.submit();"  role="button" ><i class="icon-eye-open"></i> <spring:message code="label.indViewIndividualAccount"/></a>
					</c:when>
					<c:otherwise>					
						<c:if test="${showPopupInFuture == null}">												
							<a href="#viewindModal" role="button" class="" data-toggle="modal"><i class="icon-eye-open"></i><spring:message code="label.indViewIndividualAccount"/></a>  
						</c:if>
						<c:if test="${showPopupInFuture != null}">												
							<a href="#" onclick="document.frmPopup.submit();" role="button" class="" ><i class="icon-eye-open"></i><spring:message code="label.indViewIndividualAccount"/></a>
						</c:if>						
					</c:otherwise>
				</c:choose>
			</li>
		</c:if>
<!--           <li><a href="#"><i class="icon-eye-open"></i>View Individual Account</a></li> -->
          <!-- <li><a href="#new-msg" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();"><i class="icon-envelope-unread"></i> Compose Message</a></li> -->
          <!--  <li><a href="#new-msg" class="btn span11 btn-block btn-primary" data-toggle="modal" onclick="resetForm();saveDraftOfMessage();">-->
         <c:if test="${isConsumerActivated == false}"> 
          	<li><a name="resendActivationLink" href="#" id ="resendActivationLink" class=""><i class="icon-envelope"></i><spring:message code="label.agent.individual.activationLink"/></a></li>
         </c:if> 
        </ul>
    </div>
    <!-- Modal -->
    <%-- Secure Inbox Start--%>
	<jsp:include page="../../inbox/includeInboxCompose.jsp"></jsp:include>
	<%-- Secure Inbox End--%>
    
    <div id="rightpanel" class="span9">
	<c:set var="encryptedId" ><encryptor:enc value="${individualUser.id}" isurl="true"/> </c:set>
      <form class="form-horizontal" id="editIndividualCaseDetail" name="editIndividualCaseDetail" action="<c:url value="/broker/saveindividualcasedetails/${encryptedId}"/>" method="POST">
        <df:csrfToken/>
      	<div class="gray">
      		<h4 class="header"><spring:message code="label.employeeDetails.Summary"/>
					<a class="btn btn-small pull-right save-form"  href="javascript:void(0)">
					<spring:message code="label.agent.individual.individualCaseDetail.save"/>
				</a>
			</h4>
      	</div>
        <table class="table table-border-none" role="presentation">
	          <tbody>
	            <tr>
	              <td class="span3 txt-right"><spring:message code="label.employeeDetails.PrimaryApplicant"/></td>
	              <td><strong>${individualUser.firstName} ${individualUser.lastName}</strong></td>
	            </tr>
	          
				<tr>
<!-- 					<div class="control-group"> -->
                      <td class="txt-right"><label class="required control-label" for="email"><spring:message  code="label.employeeDetails.EmailAddress"/></label></td>
                      <td>
	                      <div >
	                          <input type="text" name="email" id="email" value='${individualUser.emailAddress}' class="input-xlarge" size="30" maxlength="100"  />
	                          <div id="email_error" class="help-inline"></div>	
	                      </div>
	                  </td>
<!--                     </div> -->
				</tr>
				
	            <tr>
<%-- 					<strong>${individualUser.phone}</strong> --%>
					
					  <td class="txt-right"><label class="control-label" for="phone1"><spring:message  code="label.employeeDetails.PhoneNumber"/></label>
					  <label class="control-label hidden" for="phone2"><spring:message  code="label.employeeDetails.PhoneNumber"/></label>
					  <label class="control-label hidden" for="phone3"><spring:message  code="label.employeeDetails.PhoneNumber"/></label>
					  </td>
					<td>
						<div class="phone-group">
								<div>
									<input type="text" name="phone1" id="phone1" value="${phone1}" maxlength="3" class="area-code input-mini" />
									<input type="text" name="phone2" id="phone2" value="${phone2}" maxlength="3" class="input-mini" />
									<input type="text" name="phone3" id="phone3" value="${phone3}" maxlength="4" class="input-small" />
									<div id="phone3_error"></div>
								</div>
						</div>
					</td>
				</tr>
				
			</tbody>
		</table>
		<div>
	           <hr/>
	           <div class="txt-right">
	           <%-- <a class="btn btn-small pull-right save-form" href="javascript:void(0)" >
				<spring:message code="label.agent.individual.individualCaseDetail.save"/> --%>
				</a></div>
		</div>

      </form>

      <!-- Modal -->
      <div id="viewindModal" class="modal hide fade" tabindex="-1"
        role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-hidden="true">&times;</button>
          <h3 id="myModalLabel"><spring:message code="label.indViewIndividualAccount"/></h3>
        </div>
        <div class="modal-body">
          <p><spring:message code="label.indClickingIndividualView"/> ${individualUser.firstName} ${individualUser.lastName}.<spring:message code="label.IndThroughThisPortal"/>
          </p>
          <p><spring:message code="label.indProceedtoIndividualview"/></p>
        </div>
        
        <form action="<c:url value='/account/user/switchUserRole'/>" id="frmPopup" name="frmPopup">
        	<div class="modal-footer clearfix"> 
        			<label for="showPopupInFuture" class="aria-hidden">Show pop up in future checkbox</label> <!-- Accessibility Use -->      	
	 				<input class="pull-left"  type="checkbox" id="showPopupInFuture" name="showPopupInFuture" <c:if test="${showPopupInFuture != null}">checked</c:if>  />
	    			<div class="pull-left">&nbsp; <spring:message code="label.employee.dontshow"/></div>
				    <button class="btn btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.brkCancel"/></button>
					<input type="hidden" name="switchToModuleName" id="switchToModuleName" value="individual" />
					<input type="hidden" name="switchToModuleId" id="switchToModuleId" value="${individualUser.id}" />
					<input type="hidden" name="switchToResourceName" id="switchToResourceName" value="${individualUser.firstName} ${individualUser.lastName}" />
				   <button class="btn btn-primary" type="submit" ><spring:message code="label.indIndividualView"/></button >
			</div>			
		</form>
      </div>
      <!-- Modal end -->
    </div>
  </div>
  </div>

  <!-- gutter10 -->
<!--  Latest UI -->
  <script type="text/javascript">
  function ie8Trim(){
		if(typeof String.prototype.trim !== 'function') {
          String.prototype.trim = function() {
          	return this.replace(/^\s\s*/, '').replace(/\s\s*$/, ''); 
          }
		}
	}
  function isPositiveInteger(s) {
      return /^\d+$/.test(s);
  }
  function isInvalidCSRFToken(xhr) {
		var rv = false;
		if(xhr.status == 403 && xhr.getResponseHeader('InvalidCSRFToken') == 'true') {			
			alert($('Session is invalid').text());
			rv = true;
		}
		return rv;
	}
	
	

	$("document").ready(function (){
		  var validator = $("#editIndividualCaseDetail").validate({ 
				onkeyup: false,
				rules : {
					email : { email: true, validateEmail:true},
					phone3 : { required : true, phonecheck:true}
				},
				messages : {
					email : {validateEmail:"<span> <em class='excl'>!</em><spring:message code='label.validatePublicEmail' javaScriptEscape='true'/></span>"},
					phone3: { phonecheck : "<span> <em class='excl'>!</em><spring:message code='label.broker.validatePhoneNo' javaScriptEscape='true'/></span>"}
				},

				errorClass: "error",
				errorPlacement : function(error, element) {
					var elementId = element.attr('id');
					
					if(-1 < elementId.search("phone") ) {
						elementId = "phone3";
					}
					
					error.appendTo($("#" + elementId + "_error"));
					$("#" + elementId + "_error").addClass('error help-inline');
				}
			});
		  
		  jQuery.validator.addMethod("validateEmail", function(value, element) {
				var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
				if($.trim(value) === ""){
					return false;
				}
				if (filter.test(value)) {
				    return true;
				}
				else {
				    return false;
				}
			});
		  
		  /**
		   * Validate the entered phone number.
		   *
		   * @param 1 The first three digits of phone number.
		   * @param 2 The next three digits of phone number.
		   * @param 3 The last four digits of phone number.
		   * @return The status of phone number validation post validation check.
		   */
		  function isValidPhoneNumber(phone1, phone2, phone3) {
		  	if(!isPositiveInteger(phone1) || !isPositiveInteger(phone2) || !isPositiveInteger(phone3) || 
		  		(isNaN(phone1)) || (phone1.length < 3 ) || (isNaN(phone2)) || (phone2.length < 3 ) || (isNaN(phone3)) || 
		  		(phone3.length < 4 ) || (phone1 == '000') ) {
		  			return false;
		  	}
		  	else {
		  		var phOneInt = parseInt(phone1);
		  		if(phOneInt < 100){
		  			return false;
		  		}
		  		return true;
		  	}
		  }
		  
		  jQuery.validator.addMethod("phonecheck", function(value, element, param) {
				try {
					//As phone number is mandatory no need to check for communication prefrence select box
					ie8Trim();
					var phone1 = $("#phone1").val().trim(); 
					var phone2 = $("#phone2").val().trim(); 
					var phone3 = $("#phone3").val().trim(); 
				
						if( (null != phone1 && null != phone2 && null != phone3) && 
								("undefined" !== phone1 && "undefined" !== phone2 && "undefined" !== phone3) &&
								(phone1 !== "" && phone2 !== "" && phone3 !== "") ) {
							return isValidPhoneNumber(phone1, phone2, phone3);
						}else {		
							return false;
						}
					
				}catch(err) {
					return false;
				}
					
			});
		
		$("#resendActivationLink").click(function () {
		   if(${isConsumerActivated}) {
			   $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
				       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		   }
		   
		   else if(${isIndividualUserExists}) {
			   $('<div class="modal popup-address" id="fileUoload"><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p> ${individualUser.firstName}&nbsp;${individualUser.lastName}&nbsp;<spring:message code="label.agent.individual.activationLink.message.denied"/></p>'
				       +'</div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
		   }
		   else {			   	
			   $.post("<c:url value='/broker/sendconsumeractivationlink/${encryptedId}'><c:param name='${df:csrfTokenParameter()}'> <df:csrfToken plainToken='true'/> </c:param></c:url>", null,
				   function(data,status){
					   if(data.indexOf('Missing CSRF token') != -1){
							$('<div class="modal popup-address" id="fileUoload"><div class="modal-header"><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true">&times;</button></div><div class="modal-body txt-center" style="max-height:470px;"><h4><spring:message code="label.broker.brokerUnexpectedErrorOccured"/></h4></div><div class="modal-footer"><button class="btn" id="iFrameClose" data-dismiss="modal" aria-hidden="true"  ><spring:message code="label.agent.details.okbtn"/></button></div></div>').modal({backdrop:"static",keyboard:"false"});
							return false;
						}
						if(status === 'success') {
					    	 if(-1 == data.search("SUCCESS")) { 
					    		 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px; "><P>   <spring:message code="label.agent.individual.activationLink.message.success.negative.pre"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}<spring:message code="label.agent.individual.activationLink.message.success.negative.post"/>' + data + '.</P></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					    	 else {
					    		 $('<div class="modal popup-address" ><div class="modal-header " style="border-bottom:0; "><h3>Success!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p><spring:message code="label.agent.individual.activationLink.message.success.positive"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					    	 }
					     }
					     else {
					    	 $('<div class="modal popup-address" ><div class="modal-header" style="border-bottom:0; "><h3>Failure!</h3><button type="button" class="close" id="crossClose" data-dismiss="modal" aria-hidden="true"> </button></div><div class="modal-body" style="max-height:470px;"><p>   <spring:message code="label.agent.individual.activationLink.message.failure"/> ${individualUser.firstName}&nbsp;${individualUser.lastName}.</p></div><div class="modal-footer txt-center"><button class="btn btn-primary" id="iFrameClose" data-dismiss="modal" aria-hidden="true"><spring:message code="label.agent.details.okbtn"/> </button></div></div>').modal({backdrop:"static",keyboard:"false"});
					     }
					   });
		  	 } 
 
		});
			
		$(".save-form").click(function (){
			if($("#editIndividualCaseDetail").valid()) {
				$("#resendActivationLink").parent().remove();
				$('<div id="confirm-popup" class="modal fade"><div class="modal-header"><h3 class="pull-left"><spring:message code="label.agent.individual.individualCaseDetail.save.confirmation.header"/></h3><button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="window.location.reload();">&times;</button></div><div class="modal-body"><p><spring:message code="label.agent.individual.individualCaseDetail.save.confirmation.pre"/>&nbsp;<strong>${individualUser.firstName}&nbsp;${individualUser.lastName}</strong><spring:message code="label.agent.individual.individualCaseDetail.save.confirmation.post"/></p></div><div class="modal-footer txt-right"><button class="btn" data-dismiss="modal" aria-hidden="true"type="button" onClick="window.location.reload();">No</button><button class="btn btn-primary submit-pop"  aria-hidden="true" type="button" onClick="$(\'#editIndividualCaseDetail\').submit()">Yes</button></div></div>').modal();
			}
		});
	});
</script>