(function() {
    'use strict';

    angular.module('modalDirective', [])
    .directive('giModal', giModal);

    
    function giModal(){
        var ddo = {
            restrict: 'EA',
            scope: {
                templateName: '=',
                modalContent: '=',
                handler: '=',
                callbackbtn: '&',
                callbackbtnTwo: '&'
            },
            link: function(scope, element, attr) {
                scope.$watch('templateName', function(templateName){
                    if (templateName && templateName.length){
                        scope.dynamicTemplateUrl = scope.templateName;
                    }
                 });
            },
            template: '<ng-include src="dynamicTemplateUrl"></ng-include>',
            transclude: true,
            controller: function ($scope) {
                
            }
        };
        return ddo;
    }



})();