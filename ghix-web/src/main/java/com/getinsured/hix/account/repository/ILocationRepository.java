package com.getinsured.hix.account.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.Location;

public interface ILocationRepository extends JpaRepository<Location, Integer> {
	
}
