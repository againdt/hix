package com.getinsured.iex.hub.apc.servicehandler;

import javax.xml.bind.JAXBElement;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.getinsured.iex.hub.apc.cms.dsh.aptc.exchange._1.ObjectFactory;
import com.getinsured.iex.hub.apc.cms.dsh.aptc.extension._1.APTCResponsePayloadType;
import com.getinsured.iex.hub.apc.prefixmappers.APCNamespaceMapper;
import com.getinsured.iex.hub.apc.wrapper.APTCRequestPayloadTypeWrapper;
import com.getinsured.iex.hub.apc.wrapper.APTCResponsePayloadTypeWrapper;
import com.getinsured.iex.hub.apc.wrapper.IncomeTypeWrapper;
import com.getinsured.iex.hub.apc.wrapper.InsurancePremiumTypeWrapper;
import com.getinsured.iex.hub.platform.AbstractServiceHandler;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;

public class APCServiceHandler extends AbstractServiceHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(APCServiceHandler.class);
	
	private ObjectFactory factory = new ObjectFactory();

	private Jaxb2Marshaller marshaller;
	
	@Override
	public Object getRequestpayLoad() throws HubServiceException {
		HubServiceException he = null;
		Object response = null;
		try
		{
			response = getPayLoadFromJSONInput(getJsonInput());
		}
		catch (HubServiceException e) {
			he = e;
			throw he;
		} 
		
		return response;
	}
	
	public Object getPayLoadFromJSONInput(String jsonStr) throws HubServiceException{
		
		logger.info("received JSON String: "+jsonStr);
		
		HubServiceException hse;
		JSONObject obj = null;
		JSONParser parser = new JSONParser();
		try {
			obj = (JSONObject)parser.parse(jsonStr);
		} catch (ParseException e) {
			logger.error("Invalid JSON:"+jsonStr);
			hse = new HubServiceException("Failed to parse JSON input", e);
			throw hse;
		}
		this.setServiceName("APTC");
		
		APTCRequestPayloadTypeWrapper payLoad = new APTCRequestPayloadTypeWrapper();
		try {
			//Request Id validation
			String namespace = APTCRequestPayloadTypeWrapper.class.getName();
	
			if(!setRequestParameterFromJson(payLoad, obj, "requestID", "requestID", namespace)){
				hse = new HubServiceException("requestID not provided in input json, it is a required parameter");
				throw hse;
			}
		
			IncomeTypeWrapper incomeWrapper = new IncomeTypeWrapper();
			namespace = IncomeTypeWrapper.class.getName();
			JSONObject objAmount = (JSONObject) obj.get("Income");
			if(!setRequestParameterFromJson(incomeWrapper, objAmount, "incomeAmount", "incomeAmount", namespace)){
				hse = new HubServiceException("incomeAmount not provided in input json, it is a required parameter");
				throw hse;
			}
			
			if(!setRequestParameterFromJson(incomeWrapper, objAmount, "incomeDate", "incomeDate", namespace))
			{
				hse = new HubServiceException("incomeDate not provided in input json, it is a required parameter");
				throw hse;
			}
			
			if(!setRequestParameterFromJson(incomeWrapper, objAmount, "incomeFederalPovertyLevelPercent", "incomeFederalPovertyLevelPercent", namespace)){
				hse = new HubServiceException("incomeFederalPovertyLevelPercent not provided in input json, it is a required parameter");
				throw hse;
			}
			
			payLoad.setIncomeTypeWrapper(incomeWrapper);
			
			JSONObject objSLCSPPremium = (JSONObject) obj.get("SLCSPPremium");
			InsurancePremiumTypeWrapper insurancePremiumTypeWrapper = new InsurancePremiumTypeWrapper();
			namespace = InsurancePremiumTypeWrapper.class.getName();
			
			if(!setRequestParameterFromJson(insurancePremiumTypeWrapper, objSLCSPPremium, "insurancePremiumAmount", "insurancePremiumAmount", namespace)){
				hse = new HubServiceException("insurancePremiumAmount not provided in input json, it is a required parameter");
				throw hse;
			}
			payLoad.setInsurancePremiumTypeWrapper(insurancePremiumTypeWrapper);
		} catch (HubMappingException e) {
			hse = new HubServiceException(e.getMessage(), e);
			throw hse;
		}
		
		return  factory.createAPTCRequest(payLoad.getRequest());
	}


	@Override
	@SuppressWarnings("unchecked")
	public String handleResponse(Object responseObject)
			throws HubServiceException {	
		JAXBElement<APTCResponsePayloadType> response = (JAXBElement<APTCResponsePayloadType>)(responseObject);
		APTCResponsePayloadType resoponsePayload = response.getValue();
		APTCResponsePayloadTypeWrapper aptcResponeWrapper =  new APTCResponsePayloadTypeWrapper(resoponsePayload);
		this.setResponseCode(aptcResponeWrapper.getResponseCodes());
		this.requestStatus = RequestStatus.SUCCESS;
		return aptcResponeWrapper.toJSONString();
		}

	@Override
	public RequestStatus getRequestStatus() {
		return this.requestStatus;
	}

	@Override
	public String getServiceIdentifier() {
		return "H19-T";
	}
	
	@Override
	public Marshaller getServiceMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.apc.cms.dsh.aptc.exchange._1",new APCNamespaceMapper());
		}
		return this.marshaller;
	}

	@Override
	public Unmarshaller getServiceUnMarshaller() {
		if(this.marshaller == null) {
			this.marshaller= this.getMarshaller("com.getinsured.iex.hub.apc.cms.dsh.aptc.exchange._1",new APCNamespaceMapper());
		}
		return this.marshaller;
	}

}
