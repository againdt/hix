package com.getinsured.hix.batch.util;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;

/**
 * @author meher_a 
 * 
 * In this class we can define constants for Ghix-batch. These Constants can
 * be either direct (define a constant of type psf & directly assign any value to it) or it
 * can from config file. (define a constant of type psf & populate value from config file by
 * using 'Value' attribute of annotation)
 */

@Component
public class GhixBatchConstants {
	
	private GhixBatchConstants() {
	}
	public static final String RESEND_FLAG = "RESEND";
	public static final String HOLD_FLAG = "HOLD";
	/***************************************************************************************/
	/********************** Enrollment Batch configuration properties *********************************/
	/***************************************************************************************/

	public static String appserver;
	public static String xmlShopReconExtractPath;
	public static String DB_TYPE = "";
	public static final String POSTGRESQL = "POSTGRESQL";
	
	
	@Value("#{configProp['appServer']}")
	public void setAppserver(String appserver) {
		GhixBatchConstants.appserver = appserver;
	}

	@Value("#{configProp['enrollment.XMLShopReconExtractPath']}")
	public void setXmlShopReconExtractPath(String xMLShopReconExtractPath) {
		xmlShopReconExtractPath = xMLShopReconExtractPath;
	}

	@Value("#{configProp['database.type']}")
	public void setDataBaseType(String databaseType) {
		DB_TYPE = databaseType;
	}
	
	private static final String STATE_CODE_CA = "CA";
	private static final String STATE_CODE_NM = "NM";
	private static final String STATE_CODE_MS = "MS";
	private static final String STATE_CODE_PHIX = "PHIX";
	private static final String STATE_CODE_ID = "ID";
	private static final String STATE_CODE_MN = "MN";
	private static final String STATE_CODE_NV = "NV";
	
	private static String STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	
	public static boolean isNmCall() {
		boolean isNmCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_NM)) {
			isNmCall = true;
		}
		return isNmCall;
	}

	public static boolean isCaCall() {
		boolean isCaCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}

		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_CA)) {
			isCaCall = true;
		}
		return isCaCall;
	}

	public static boolean isMsCall() {
		boolean isMsCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_MS)) {
			isMsCall = true;
		}
		return isMsCall;
	}

	public static boolean isPhixCall() {
		boolean isPhixCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_PHIX)) {
			isPhixCall = true;
		}
		return isPhixCall;
	}

	public static boolean isIdCall() {
		boolean isIdCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_ID)) {
			isIdCall = true;
		}
		return isIdCall;
	}

	public static boolean isMnCall() {
		boolean isMnCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_MN)) {
			isMnCall = true;
		}
		return isMnCall;
	}

	public static boolean isNvCall() {
		boolean isNvCall = false;
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		if (STATE_CODE.equalsIgnoreCase(STATE_CODE_NV)) {
			isNvCall = true;
		}
		return isNvCall;
	}

	public static void getStateCode() {
		STATE_CODE = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
	}

	public static String returnStateCode() {
		if (STATE_CODE == null || STATE_CODE.equalsIgnoreCase("")) {
			getStateCode();
		}
		return STATE_CODE;
	}
}
