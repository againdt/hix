package com.getinsured.hix.admin.service.jpa;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.getinsured.hix.admin.repository.IAnnouncementRoleRepository;
import com.getinsured.hix.admin.service.AnnouncementRoleService;
import com.getinsured.hix.model.Announcement;
import com.getinsured.hix.model.AnnouncementRole;
import com.getinsured.hix.model.Role;

@Service("announcementRoleService")
public class AnnouncementRoleServiceImpl implements AnnouncementRoleService{
	private static final Logger LOGGER = LoggerFactory.getLogger(AnnouncementRoleServiceImpl.class);
	
	@Autowired IAnnouncementRoleRepository announcementRoleRepository;
	
	public List<Role> findRolesByAnnouncement(Announcement announcement){
		return announcementRoleRepository.findRolesByAnnouncement(announcement);
	}
	
	public AnnouncementRole setAnnouncementRole(Announcement announcement, Role role){
		AnnouncementRole announcementRole = new AnnouncementRole();
		announcementRole.setAnnouncement(announcement);
		announcementRole.setRole(role);
		
		return announcementRole;
	}
	
	public void deleteAnnouncementRole(Announcement announcement){
		announcementRoleRepository.deleteInBatch(announcement.getAnnouncementRole());
	}
}