package com.getinsured.consumer.service;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import com.getinsured.timeshift.TSCalendar;
import java.util.Collections;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getinsured.consumer.exceptions.ConsumerServiceException;
import com.getinsured.consumer.repository.CmrEventsLogRepository;
import com.getinsured.consumer.repository.IConsumerCallLogsRepository;
import com.getinsured.consumer.repository.IHouseholdRepository;
import com.getinsured.hix.consumer.history.ConsumerEnrollEventHistory;
import com.getinsured.hix.consumer.history.ConsumerEventHistory;
import com.getinsured.hix.consumer.history.ConsumerHistoryBase;
import com.getinsured.hix.consumer.history.ConsumerHistoryView;
import com.getinsured.hix.dto.crm.ConsumerHistoryRequest;
import com.getinsured.hix.model.AppEvent;
import com.getinsured.hix.model.Comment;
import com.getinsured.hix.model.CommentTarget;
import com.getinsured.hix.model.CommentTarget.TargetName;
import com.getinsured.hix.model.consumer.CmrEventsLog;
import com.getinsured.hix.model.consumer.ConsumerCallLog;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.comment.repository.ICommentRepository;
import com.getinsured.hix.platform.comment.service.CommentTargetService;
import com.getinsured.hix.platform.eventlog.AppEventDto;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixJasyptEncrytorUtil;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.exception.GIException;

@Service("consumerHistoryService")
public class ConsumerHistoryServiceImpl implements ConsumerHistoryService{
	
	private static ObjectMapper mapper = new ObjectMapper();
	private static final int MAX_COMMENT_LENGTH = 4000;
	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerHistoryServiceImpl.class);
	@Autowired private CommentTargetService commentTargetService;
	
	
	@Autowired
	private IConsumerCallLogsRepository callLogsRepository;
	@Autowired
	private ICommentRepository commentRepository;
	@Autowired
	private CmrEventsLogRepository cmrEventsLogRepository;
	@Autowired
	private UserService userService;
	
	@Autowired
	private ConsumerService consumerService;
	@PersistenceUnit
	private EntityManagerFactory emf;
	
	@Autowired private GhixJasyptEncrytorUtil ghixJasyptEncrytorUtil;
	
	@Autowired private IHouseholdRepository householdRepository;
	
	@Override
	public List<Object[]> getConsumerCallLogHistory(String houdeholdId) {
		if(!StringUtils.isEmpty(houdeholdId)){
			return callLogsRepository.getConsumerCallLogs(Integer.parseInt(houdeholdId));
		}
		return null;
	}
	@Override
	public List<Comment> getConsumerCommentsHistory(String houdeholdId) {
		if(!StringUtils.isEmpty(houdeholdId)){
			return commentRepository.findByCommentTargetUserId(houdeholdId);
		}
		return null;
	}
	
	@Override
	public List<CmrEventsLog> getConsumerEventsHistory(String houdeholdId) {
		if(!StringUtils.isEmpty(houdeholdId)){
			return cmrEventsLogRepository.getConsumerEventLogs(Integer.parseInt(houdeholdId));
		}
		return null;
	}
	
	@Override
	public ConsumerHistoryView getConsumerHistory(String houdeholdId) throws ConsumerServiceException {
		LinkedList<ConsumerHistoryBase> consumerHistoryEntries = new LinkedList<ConsumerHistoryBase>();
		
		
		List<Object[]> callLogList = getConsumerCallLogHistory(houdeholdId);
		if (null != callLogList && callLogList.size() > 0) {
			mapCallLogData(consumerHistoryEntries, callLogList);
		}
		
		List<Comment> commentList = getConsumerCommentsHistory(houdeholdId);
		if (null != commentList && commentList.size() > 0) {
			mapNotesData(consumerHistoryEntries, commentList);
		}
		
		List<CmrEventsLog> cmrEventsLog = getConsumerEventsHistory(houdeholdId);
		if (null != cmrEventsLog && cmrEventsLog.size() > 0) {
			mapEventHistoryData(consumerHistoryEntries, cmrEventsLog);
		}
		

		Collections.sort(consumerHistoryEntries, new HistoryListComparator());
		
		

		ConsumerHistoryView historyView = new ConsumerHistoryView();
		historyView.setHouseholdid(houdeholdId);
		historyView.setHistoryList(consumerHistoryEntries);
		return historyView;
	}
	
	private void mapNotesData(
			LinkedList<ConsumerHistoryBase> historyNotesEntries,
			List<Comment> commentList) {
		for (Comment comment : commentList) {
			
			Date dt = comment.getCreated();
			String time  = getTimeFromDate(dt);
			String enteredBy = comment.getCommenterName();
			
			NoteMetaData noteMetadata = new NoteMetaData(enteredBy, dt, time);
			MainData mainData = new MainData(comment.getComment());
			CommentHistory comments = new CommentHistory(mainData, noteMetadata, "Notes");
			historyNotesEntries.add(comments);

		}
	}
	private void mapCallLogData(
			LinkedList<ConsumerHistoryBase> callHistoryEntries,
			List<Object[]> callLogList) throws ConsumerServiceException {
		for (Object[] callLog : callLogList) {
			Date dt = (Date) callLog[0];
//			String time  = getTimeFromDate(dt);
			String enteredBy = (String)callLog[1];
			String callDataJson = (String)callLog[2];
			Date dateTime = (Date)callLog[3];
			String duration = String.valueOf(callLog[4]);
			String description = (String)callLog[5];
			CallData callData = new CallData();
			
			try {
				callData = JacksonUtils.getJacksonObjectReaderForJavaType(CallData.class).readValue(callDataJson);
			} catch (IOException e) {
				throw new ConsumerServiceException("Exception in parsing callDataJson value :" + callDataJson + "\n Exception:" + e.getMessage());
			}
			String callType = null;
			String wrapCode =  null;
			callType = callData.getCallType();
			wrapCode  = callData.getWrapCode();
			
			CallMetaData callMetadata = new CallMetaData(enteredBy, dt,dateTime, duration, wrapCode, callType);
			MainData mainData = new MainData(description);
			CallHistory call = new CallHistory(mainData, callMetadata, "Call");
			callHistoryEntries.add(call);				
		}
	}
	
	private void mapEventHistoryData(
			LinkedList<ConsumerHistoryBase> consumerHistoryEntries,
			List<CmrEventsLog> cmrEventsLog) {
		
			for (CmrEventsLog cmrEvents : cmrEventsLog) {
				
			ConsumerEventHistory consumerEventHistory = new ConsumerEventHistory(cmrEvents.getCreated());
			
			
			consumerEventHistory.setCreatedBy(cmrEvents.getCreatedBy());
			if(cmrEvents.getCreatedBy()!=null){
 	 	 	 consumerEventHistory.setCreatedByName(userService.getUserName(cmrEvents.getCreatedBy()));
 	 	 	 }
			consumerEventHistory.setDatetime(cmrEvents.getCreated());
			if(cmrEvents.getEvenetSource().equalsIgnoreCase("Enrollment")){
				try {
					ConsumerEnrollEventHistory enrollEventHistory =  mapper.readValue(cmrEvents.getEvenetDetails(),ConsumerEnrollEventHistory.class);
					consumerEventHistory.setEventMetaData(enrollEventHistory);
				}catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		    consumerEventHistory.setEventLevel(cmrEvents.getEvenetLevel());
		    consumerEventHistory.setEventSource(cmrEvents.getEvenetSource());
		    consumerEventHistory.setHouseholdId(cmrEvents.getHouseholdId());
		    consumerEventHistory.setDescription(cmrEvents.getDescription());
			
			
			consumerHistoryEntries.add(consumerEventHistory);
		}
		
		
	}
	private String getTimeFromDate(Date dt)  {
	        DateFormat time = new SimpleDateFormat("hh:mm:ss a");
		    return time.format(dt);
	}
	
	
	private List<Comment> createAndSaveComment(ConsumerHistoryRequest historyRequest,
			String commentText, CommentTarget commentTarget) {
		List<Comment> comments = null;
		
		if(commentTarget.getComments() == null || commentTarget.getComments().size() == 0 ){
			comments = new ArrayList<Comment>();
		}
		else{
			comments = commentTarget.getComments();
		}
		
		
		LOGGER.info("Retrieving logged in user's name");
		
		
		String commenterName = historyRequest.getAgentName();

		//Checking if user's name is blank
		if(commenterName == null || commenterName.trim().length() == 0){
			commenterName = "Anonymous User";
		}
		
		LOGGER.info("Commented by : " + commenterName);
		
		Comment comment = new Comment();
		//comment.setComentedBy(request.getParameter("commented_by").trim());
		
		//Setting logged in user's id
		LOGGER.info("Commenter's user Id : " + historyRequest.getAgentId());
		comment.setComentedBy(historyRequest.getAgentId());
		
		comment.setCommenterName(commenterName);
		
		//Fetching first 4000 char for comment text;
		
		int beginIndex = 0;
		int endIndex = commentText.length() > MAX_COMMENT_LENGTH ? MAX_COMMENT_LENGTH :  commentText.length();
		commentText = commentText.substring(beginIndex, endIndex);
		//commentText = commentText.replaceAll("\\n|\\r", " ");
		comment.setComment(commentText);
		
		comment.setCommentTarget(commentTarget);
		
		comments.add(comment);
		commentTarget.setComments(comments);
		
		commentTargetService.saveCommentTarget(commentTarget);
		return comments;
		
	}
	
	@Override
	public ConsumerCallLog populateCallLogObject(
			ConsumerHistoryRequest historyRequest) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		int agentId = historyRequest.getAgentId();
		int householdId = historyRequest.getHouseholdId();
		CallHistory callHistory;
		ConsumerCallLog callLogs = new ConsumerCallLog();
		callHistory = mapper.readValue(historyRequest.getLogData(),
				CallHistory.class);

		callLogs.setAgentId(agentId);
		callLogs.setHouseholdId(householdId);
		String duration = callHistory.getMetadata().getDuration();
		callLogs.setDuration(Integer.parseInt(duration));
		callLogs.setStartTime(callHistory.getMetadata().getDateTime());
		CallData callData = new CallData();
		callData.setCallType(callHistory.getMetadata().getCallType());
		callData.setWrapCode(callHistory.getMetadata().getWrapCode());
		callLogs.setCallData(mapper.writeValueAsString(callData));
		consumerService.saveCallLogs(callLogs);
		if (null != callLogs) {
			Long targetId = callLogs.getId().longValue();
			String targetName = "CONSUMERCALLLOG";
			CommentTarget commentTarget = commentTargetService
					.findByTargetIdAndTargetType(targetId,
							CommentTarget.TargetName.CONSUMERCALLLOG);
			if (commentTarget == null) {
				commentTarget = new CommentTarget();
				commentTarget.setTargetId(targetId);
				commentTarget.setTargetName(TargetName.valueOf(targetName));
			}
			String commentText = callHistory.getMainData().getDesc();
			createAndSaveComment(historyRequest, commentText, commentTarget);


		}
		return callLogs;
	}
	
	@Override
	public List<Comment> saveCommentLogObject(
			ConsumerHistoryRequest historyRequest) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
	//	int agentId = historyRequest.getAgentId();
		String  householdId = String.valueOf(historyRequest.getHouseholdId());
		CommentHistory commentHistory;
		commentHistory = mapper.readValue(historyRequest.getLogData(),
				CommentHistory.class);
		CommentTarget commentTarget;
		String commentText = commentHistory.getMainData().getDesc();
		if (!StringUtils.isEmpty(commentText)) {
			Long targetId = Long.valueOf(householdId);
			String targetName = "CONSUMERCALLLOG";
			commentTarget = commentTargetService
					.findByTargetIdAndTargetType(targetId,
							CommentTarget.TargetName.CONSUMERCALLLOG);
			if (commentTarget == null) {
				commentTarget = new CommentTarget();
				commentTarget.setTargetId(targetId);
				commentTarget.setTargetName(TargetName.valueOf(targetName));
			}
			
			return createAndSaveComment(historyRequest, commentText, commentTarget);


		}
		return null;
	}
	
	@Override
	public String  saveConsumerEverntLog(com.getinsured.hix.consumer.history.ConsumerEventHistory consumerEventHistory) {
		CmrEventsLog cmrEventsLog = new CmrEventsLog();
		cmrEventsLog.setCreated(consumerEventHistory.getDatetime());
		cmrEventsLog.setCreatedBy(consumerEventHistory.getCreatedBy());
		cmrEventsLog.setDescription(consumerEventHistory.getDescription());
		if(consumerEventHistory.getEventSource().equalsIgnoreCase("Enrollment")){
			try {
				cmrEventsLog.setEvenetDetails(mapper.writeValueAsString(consumerEventHistory.getEventMetaData()));
			} catch (JsonProcessingException e) {
				LOGGER.error("Error in getting event meta data for enrollment while saving events"+e.getMessage());
			}
		}
		cmrEventsLog.setEvenetLevel(consumerEventHistory.getEventLevel());
		cmrEventsLog.setEvenetSource(consumerEventHistory.getEventSource());
		cmrEventsLog.setHouseholdId(consumerEventHistory.getHouseholdId());
		Household cmr  = householdRepository.findOne(consumerEventHistory.getHouseholdId());
		if(null!=cmr && cmr.getId()>0){
		cmrEventsLog = cmrEventsLogRepository.save(cmrEventsLog);
		}
		else{
			LOGGER.error("Could not create the household event for the invalid Id: "+consumerEventHistory.getHouseholdId());
		}
		if(null!=cmrEventsLog){
			return "success";
		}
		return "failure";
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AppEventDto> searchAppEvents(Map<String, Object> searchCriteria)  {
		
		String moduleId = StringUtils.defaultIfBlank((String) searchCriteria.get("moduleId"), "0") ;
		List<AppEventDto> eventDtoList = new LinkedList<AppEventDto>();
		Map<String, Object> mapQueryParam = new HashMap<>();
		
		String hql = " from AppEvent ae where ae.moduleId= '" + moduleId + "'";
				
		StringBuilder query = new StringBuilder(hql);
		
		// apply the filters and create parameter map
		applyAppEventFilters(query, searchCriteria, mapQueryParam);
		
		// apply sort filters
		addAppEventSortFilters(query, searchCriteria);
		
		LOGGER.info("Search application event query: " + query.toString());

		EntityManager em = null;
		try {					
			em = emf.createEntityManager();
			Query hqlQuery = em.createQuery(query.toString());
					
			// Set query parameters
			setNamedQueryParams(hqlQuery, mapQueryParam);
			
			List<AppEvent> appEventList = hqlQuery.getResultList();
			if(appEventList != null && !appEventList.isEmpty()) {
				AppEventDto dto = null;
				for (AppEvent appEvent : appEventList) {
					dto = new AppEventDto();
					dto.setCreatedBy(appEvent.getCreatedBy());
					dto.setCreatedByUserName(appEvent.getCreatedByUserName());
					dto.setCreatedByUserRole(appEvent.getCreatedByUserRole());
					dto.setCreationTimeStamp(appEvent.getCreationTimeStamp());
					dto.setEventJson(appEvent.getEventJson());
					dto.setModuleId(appEvent.getModuleId());
					dto.setModuleName(appEvent.getModuleName());
					dto.setName(appEvent.getName());
					dto.setType(appEvent.getType());
					dto.setId(ghixJasyptEncrytorUtil.encryptStringByJasypt(String.valueOf(appEvent.getId())));
					
					eventDtoList.add(dto);
				}
			}
		} catch (Exception e) {
			LOGGER.error("Unable to search the events", e);
		} finally {
			if (em != null && em.isOpen()) {
				em.clear();
				em.close();
			}
		}
		
		return eventDtoList;
	}
	
	private void applyAppEventFilters(StringBuilder query, Map<String, Object> searchCriteria, Map<String, Object> mapQueryParam) {
		
		String type = (String) searchCriteria.get("type");
		if (StringUtils.isNotEmpty(type)) {
			query.append("and ae.type=:type ");
			mapQueryParam.put("type", type);
		}
		
		String name = (String) searchCriteria.get("name");
		if (StringUtils.isNotEmpty(name)) {
			query.append("and ae.name=:name ");
			mapQueryParam.put("name", name);
		}
		
		String role = (String) searchCriteria.get("role");
		if (StringUtils.isNotEmpty(role)) {
			query.append("and ae.createdByUserRole=:createdByUserRole ");
			mapQueryParam.put("createdByUserRole", role);
		}
		
		String strFromDate = (String) searchCriteria.get("fromDate");
		if (!StringUtils.isEmpty(strFromDate)) {
			Date fromDate = prepareDateCriteria(strFromDate, false);
			query.append("and ae.creationTimeStamp >= :fromDate ");
			mapQueryParam.put("fromDate", fromDate);
		}
		
		String strToDate = (String) searchCriteria.get("toDate");
		if (!StringUtils.isEmpty(strToDate)) {
			Date toDate = prepareDateCriteria(strToDate, true);
			query.append("and ae.creationTimeStamp <= :toDate ");
			mapQueryParam.put("toDate", toDate);
		}
	}
	
	private void addAppEventSortFilters(StringBuilder query, Map<String, Object> searchCriteria) {		
		String columnName = (searchCriteria.get("sortBy") == null) ? "ae.creationTimeStamp" : searchCriteria.get("sortBy").toString();
		
		String sortOrder = (searchCriteria.get("sortOrder") == null) ? "DESC" : searchCriteria.get("sortOrder").toString();
		sortOrder = (("DESC").compareToIgnoreCase(sortOrder) == 0) ? "DESC" : "ASC";
		
		query.append("ORDER BY ").append(columnName).append(" ").append(sortOrder);
	}
	
	private void setNamedQueryParams(Query hqlQuery, Map<String, Object> mapQueryParam) {
		for(String param : mapQueryParam.keySet()){
			hqlQuery.setParameter(param, mapQueryParam.get(param));
		}
	}
	
	private Date prepareDateCriteria(String submittedDateString, boolean maxtime) {
		Calendar calendarDate = null;
		Date effectiveEndDate = null;
		
		DateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		
		if(StringUtils.isEmpty(submittedDateString)) {
			return effectiveEndDate;
		}
		
		try {
			effectiveEndDate = formatter.parse(submittedDateString);
		} catch (ParseException e) {
			LOGGER.error(e.getMessage());
			return effectiveEndDate;
		}

		calendarDate = TSCalendar.getInstance();
		calendarDate.setTime(effectiveEndDate);
		if(maxtime){
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMaximum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMaximum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMaximum(Calendar.SECOND));
		}else{
			calendarDate.set(Calendar.HOUR_OF_DAY, calendarDate.getActualMinimum(Calendar.HOUR_OF_DAY));
			calendarDate.set(Calendar.MINUTE, calendarDate.getActualMinimum(Calendar.MINUTE));
			calendarDate.set(Calendar.SECOND, calendarDate.getActualMinimum(Calendar.SECOND));
		}
		
		return  calendarDate.getTime();
	}
}

