
package com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.extension._1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FinalDecisionCodeSimpleType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="FinalDecisionCodeSimpleType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="ACC"/>
 *     &lt;enumeration value="REF"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "FinalDecisionCodeSimpleType")
@XmlEnum
public enum FinalDecisionCodeSimpleType {


    /**
     * Accept
     * 
     * 
     */
    ACC,

    /**
     * Refer (default until all questions are scored for KIQ processing)
     * 
     */
    REF;

    public String value() {
        return name();
    }

    public static FinalDecisionCodeSimpleType fromValue(String v) {
        return valueOf(v);
    }

}
