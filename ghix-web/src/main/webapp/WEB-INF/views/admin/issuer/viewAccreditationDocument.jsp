<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="issuerLogo" uri="/WEB-INF/tld/issuer-logo.tld" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<%@ page isELIgnored="false"%>
<script type="text/javascript" src="/hix/resources/js/preserveImgAspectRatio.js"></script>
<div class="gutter10">
<c:set var="encIssuerId" ><encryptor:enc value="${issuerObj.id}" isurl="true"/> </c:set>
	<div class="row-fluid">
        	<ul class="page-breadcrumb">
        		<li><a href="<c:url value="/admin/issuer/details/${encIssuerId}"/>">&lt; <spring:message  code="label.back"/></a></li>
     			<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.issuers"/></a></li>
          		<li><a href="<c:url value="/admin/manageissuer"/>"><spring:message  code="pgheader.manageIssuers"/></a></li>
            	<li><spring:message  code="pgheader.accreditationDocuments"/></li>
            </ul><!--page-breadcrumb ends-->
		</div>
		<div class="row-fluid issuer_info">
			<div class="centered_all">
				<div class="span3">
					<img src="<c:url value="/admin/issuer/company/profile/logo/hid/${issuerObj.hiosIssuerId}"/>"  class="resize-img img_center_h" />
				</div>
				<div class="span9">
					<h1>${issuerObj.name}</h1>
				</div>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span3" id="sidebar">
                <div class="header">
                    <h4 class="margin0"><spring:message code="pgheader.issuerAbout"/></h4>
			</div>
			<!--  beginning of side bar -->
			<jsp:include page="issuerDetailsLeftNav.jsp">
        			<jsp:param name="pageName" value="accreditation"/>
			</jsp:include>
			<!-- end of side bar -->
		</div>
		<!-- end of span3 -->
		
		<div class="span9" id="rightpanel">
			<div class="header">
						<h4 class="pull-left"><spring:message  code="pgheader.accreditationDocuments"/></h4>
						<a class="btn btn-small pull-right" href="<c:url value="/admin/issuer/viaccreditationdocument/edit/${encIssuerId}"/>"><spring:message  code="label.edit"/></a>
					</div>
					<div class="gutter10-t">
					<b><spring:message  code="pgheader.uploadDocumentsInfoview"/></b>
					<form class="form-horizontal" id="frmIssuerApp" name="frmIssuerApp" action="" method="GET">
						
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAuthority"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(authFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
						   <c:otherwise>
								<c:forEach var="authFilesListObj" items="${authFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${authFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAccreditation"/></h4>
						</div>
						<div class="gutter10-t">
						<div class="control-group">
							<label class="control-label required"><spring:message  code="label.issuerUploadAccreditation"/></label>
							<div class="controls gutter5-t txt-bold">
								<c:out value="${issuerAccreditation}"/>
							</div>
						</div><!-- end of control-group -->
						
						<div class="control-group">
							<label class="control-label required"><spring:message  code="label.accreditingEntity"/></label>
							<div class="controls gutter5-t txt-bold">
								<c:out value="${accreditionEntity}"/>
							</div>
						</div><!-- end of control-group -->
						
						<fmt:formatDate pattern="MM/dd/yyyy" value="${issuerObj.accreditationExpDate}" var="expDate" timeZone="${timezone}"/>
						<div class="control-group">
							<label class="control-label required"><spring:message  code="label.expirationDate"/></label>
							<div class="controls gutter5-t txt-bold">
								<c:out value="${expDate}"/>
							</div>
						</div><!-- end of control-group-->
						
						<c:forEach var="accreditationFilesListObj" items="${accreditationFilesList}" varStatus="indexvar">
							<div class="control-group">
								<label for="issuerName" class="control-label required"> <spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
									<div class="controls gutter5-t txt-bold">${accreditationFilesListObj.uploadedFileName}</div>	
							</div><!-- end of control-group -->						
						</c:forEach>
						</div>
						<div class="header">
							<h4><spring:message  code="label.organizationData"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(organizationFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="organizationFilesListObj" items="${organizationFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
											<div class="controls gutter5-t txt-bold">${organizationFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							<h4><spring:message  code="label.issuerUploadMarketingReq"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(marketingFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="marketingFilesListObj" items="${marketingFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${marketingFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.financialDisclosures"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(financeDisclosureFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="disclosureFilesListObj" items="${financeDisclosureFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${disclosureFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							 <h4><spring:message  code="label.claimsPaymentPolPract"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(payPolPractFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="payPolPractFilesListObj" items="${payPolPractFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${payPolPractFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							<h4><spring:message  code="label.enrollmentAndDisenroll"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(enrollDisDataFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="enrollDisDataFilesListObj" items="${enrollDisDataFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${enrollDisDataFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.ratingPractices"/></h4>
						</div>
						<div class="gutter10-t">
						<c:choose>
							<c:when test="${fn:length(ratingPractFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="ratingPractFilesListObj" items="${ratingPractFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${ratingPractFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						<div class="header">
							 <h4><spring:message  code="label.costSharAndPayments"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(costSharPaymentFilesList) == 0}">
								<div class="control-group">
									<div class="controls gutter5-t txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="costSharPaymentFilesListObj" items="${costSharPaymentFilesList}" varStatus="indexvar">
									<div class="control-group">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls gutter5-t txt-bold">${costSharPaymentFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
						
						<div class="header">
							<h4><spring:message  code="label.issuerUploadAdditionalInfo"/></h4>
						</div>
						<div class="gutter10-t">
						 <c:choose>
							<c:when test="${fn:length(addsuppFilesList) == 0}">
								<div class="control-group clearfix">
									<div class="controls paddingT5 txt-bold"></div>
								</div>
							</c:when>
							<c:otherwise>
								<c:forEach var="addsuppFilesListObj" items="${addsuppFilesList}" varStatus="indexvar">
									<div class="control-group clearfix">
										<label for="issuerName" class="control-label required"><spring:message code="label.fileName"/> <c:out value="${indexvar.index+1}"/></label>
										<div class="controls paddingT5 txt-bold">${addsuppFilesListObj.uploadedFileName}</div>	
									</div><!-- end of control-group --> 
								</c:forEach>
							</c:otherwise>
						</c:choose>
						</div>
					</form>
				</div>
			</div><!--  end of span9 -->
		</div><!-- end row-fluid --->
	</div>

