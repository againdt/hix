package com.getinsured.hix.batch.enrollment.terminator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Arrays;
import java.util.Map;
import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.repeat.RepeatStatus;

import com.getinsured.hix.batch.enrollment.skip.EnrollmentMonthlyPremiumParams;
import com.getinsured.hix.enrollment.util.EnrollmentConfiguration;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.enrollment.util.EnrollmentGIMonitorUtil;
import com.getinsured.hix.enrollment.util.EnrollmentUtils;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.util.JiraUtil;
import com.getinsured.hix.platform.util.exception.GIException;

public class TerminatePopulateMonthlyEnrollmentPremiumsJob  implements Tasklet{
	private static final Logger LOGGER = LoggerFactory.getLogger(TerminatePopulateMonthlyEnrollmentPremiumsJob.class);
	private EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams;
	private String ghixEnvironment;
	private EnrollmentGIMonitorUtil enrollmentGIMonitorUtil;
	
	@Override
	public RepeatStatus execute(StepContribution contribution,
			ChunkContext chunkContext) throws Exception {
		ExecutionContext je = chunkContext.getStepContext().getStepExecution().getJobExecution().getExecutionContext();
		String jobStatus = null;
		Object jobExecutionStatus = je.get("jobExecutionStatus");
		if(jobExecutionStatus != null){
			jobStatus = (String)jobExecutionStatus;
		}
		if(jobStatus != null && jobStatus.compareToIgnoreCase("failed") == 0){
			String errMsg = "";
			if(null != je.get("errorMessage")){
				errMsg = je.get("errorMessage").toString(); 
			}
			LOGGER.error("Error message for job 'populateMonthlyEnrollmentPremiumsJob': " +errMsg);
			throw new GIException("Error occured while updating enrollment premium table :: " + errMsg);
		}
		if(null != enrollmentMonthlyPremiumParams){
			if(EnrollmentConstants.STATE_CODE_CA.equalsIgnoreCase(EnrollmentConfiguration.returnStateCode())){
				logSkippedEnrollments(enrollmentMonthlyPremiumParams.getSkippedEnrollmentMap(), chunkContext.getStepContext().getStepExecution().getJobExecutionId().toString());
			}else{
			logBug(enrollmentMonthlyPremiumParams, chunkContext.getStepContext().getStepExecution().getJobExecutionId());
			}
			enrollmentMonthlyPremiumParams.resetFields();
		}
		LOGGER.info("Terminate");
		return RepeatStatus.FINISHED;

	}
	
	private void logBug(EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams, Long executionId) {
		if(!enrollmentMonthlyPremiumParams.getSkippedEnrollmentMap().isEmpty()){
			Boolean isJiraEnabled = Boolean.parseBoolean(DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENABLE_JIRA_CREATION));
		StringBuilder descriptionstringBuilder = new StringBuilder();
			for (Entry<Integer, String> entry : enrollmentMonthlyPremiumParams.getSkippedEnrollmentMap().entrySet()) {
				descriptionstringBuilder.append(" Record skipped while running enrollment premium population batch enrollment ID : "+ entry.getKey()+"\n"
                                  +" Exception Stack : "+ entry.getValue());
                 descriptionstringBuilder.append("\n");
         }
			String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
			String fixVersion = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_FIX_VERSION);
			if(!"PROD".equalsIgnoreCase(ghixEnvironment)){
				ghixEnvironment = "QA";
			}
			String summary = ghixEnvironment+" : "+ "Records skipped while running enrollment premium population batch : "+ executionId;
			try{
				if(isJiraEnabled){
					JiraUtil.logBug(Arrays.asList(enrollmentComponent), 
							Arrays.asList(fixVersion), 
						descriptionstringBuilder.toString().trim(), 
						summary,
        		         null);
				}
			}catch(Exception e){
				LOGGER.error("Exception in creating JIRA :: " + e.getMessage());
			}
			enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.PREM_POPULATION,
					summary, descriptionstringBuilder.toString());
		}
	}
	
	/**
	 * Log skipped records in a file
	 * @param skippedEnrollmentMap
	 * @param executionId
	 * @return String Skipped file location
	 */
	private String logSkippedEnrollments(Map<Integer, String> skippedEnrollmentMap, String executionId) {
		String skipListFilePath = "";
		if(null !=skippedEnrollmentMap && !skippedEnrollmentMap.isEmpty()){
			String enrollmentComponent = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.JIRA_UTIL_ENROLLMENT_COMPONENT);
			StringBuilder descriptionstringBuilder = new StringBuilder();
			Writer writer = null;
			String premiumPopulationJobPath = DynamicPropertiesUtil.getPropertyValue(EnrollmentConfiguration.EnrollmentConfigurationEnum.ENROLLMENT_OUT_JOB_PATH) + File.separator + "PremiumPopulationJob";
			try{
				EnrollmentUtils.createDirectory(premiumPopulationJobPath);
				if(null != premiumPopulationJobPath && new File(premiumPopulationJobPath).exists()){
					String executionIdFolderPath = premiumPopulationJobPath + File.separator + executionId;
					skipListFilePath = executionIdFolderPath+File.separator + "PremiumPopulationJobSkippedRecords.txt";
					if(! new File(executionIdFolderPath).exists()){
						new File(executionIdFolderPath).mkdirs();
					}
					writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(skipListFilePath), "utf-8"));
					writer.write(skippedEnrollmentMap.toString());
				}
			}catch(Exception e){
				LOGGER.error("Exception in creating SkippedHousehold log :: " + e.getMessage(), e);
			}finally{
				if(writer != null){
					try {
						writer.close();
					} catch (IOException e) {
						LOGGER.error("Exception closing writer :: " + e.getMessage(), e);
					}
				}
			}
			for (Entry<Integer, String> entry : enrollmentMonthlyPremiumParams.getSkippedEnrollmentMap().entrySet()) {
				descriptionstringBuilder.append(" Record skipped while running enrollment premium population batch enrollment ID : "+ entry.getKey()+"\n"
						+" Exception Stack : "+ entry.getValue());
				descriptionstringBuilder.append("\n");
			}
			enrollmentGIMonitorUtil.logToGiMonitor(enrollmentComponent, EnrollmentConstants.GiErrorCode.PREM_POPULATION,
					descriptionstringBuilder.toString(), "Records skipped while running enrollment premium population batch : "+ executionId);
		}
		return skipListFilePath;
	}
	
	public EnrollmentMonthlyPremiumParams getEnrollmentMonthlyPremiumParams() {
		return enrollmentMonthlyPremiumParams;
	}
	public void setEnrollmentMonthlyPremiumParams(EnrollmentMonthlyPremiumParams enrollmentMonthlyPremiumParams) {
		this.enrollmentMonthlyPremiumParams = enrollmentMonthlyPremiumParams;
	}

	/**
	 * @return the ghixEnvironment
	 */
	public String getGhixEnvironment() {
		return ghixEnvironment;
	}

	/**
	 * @param ghixEnvironment the ghixEnvironment to set
	 */
	public void setGhixEnvironment(String ghixEnvironment) {
		this.ghixEnvironment = ghixEnvironment;
	}

	/**
	 * @return the enrollmentGIMonitorUtil
	 */
	public EnrollmentGIMonitorUtil getEnrollmentGIMonitorUtil() {
		return enrollmentGIMonitorUtil;
	}

	/**
	 * @param enrollmentGIMonitorUtil the enrollmentGIMonitorUtil to set
	 */
	public void setEnrollmentGIMonitorUtil(EnrollmentGIMonitorUtil enrollmentGIMonitorUtil) {
		this.enrollmentGIMonitorUtil = enrollmentGIMonitorUtil;
	}
}
