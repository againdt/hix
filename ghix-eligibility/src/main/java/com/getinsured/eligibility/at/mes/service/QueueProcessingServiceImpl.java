package com.getinsured.eligibility.at.mes.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.common.MESProcessingConstants;
import com.getinsured.eligibility.at.mes.common.MESUtils;
import com.getinsured.eligibility.at.mes.queue.dto.ATSpanInfoRequestDTO;
import com.getinsured.eligibility.at.mes.queue.dto.ATQueueInfoResponseDTO;
import com.getinsured.eligibility.util.EligibilityConstants;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.EligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.ExchangeEligibilityType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_ee.InsuranceApplicantType;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * This class is used to run the queue algorithm on received AT.
 * Also determine whether the AT should be closed, processed or queued
 * @author sahasrabuddhe_n
 *
 */
@Component("queueProcessingServiceImpl")
public class QueueProcessingServiceImpl implements QueueProcessingService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(QueueProcessingService.class);
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	/**
	 * This method is used to determine whether the AT should be closed, processed Realtime or queued
	 */
	@Override
	public ATQueueInfoResponseDTO determineATProcessingType(ATSpanInfoRequestDTO atSpanInfoRequestDTO) {
		LOGGER.info("Calling determineATProcessingType to determine processing type for AT");
		
		ATQueueInfoResponseDTO atQueueInfoResponseDTO = new ATQueueInfoResponseDTO();
		try {

			// get the currently enrolled AT ESD
			DateTime enrolledEligibilityStartDate = retrieveCurrentlyEnrolledESD(atSpanInfoRequestDTO.getCmrHouseoldId(),atSpanInfoRequestDTO.getCoverageYear());
			DateTime currentDate =new DateTime(new TSDate());
			
			String atProcessingType;
			DateTime dueDate = null;
			DateTime renewalDateJan = new DateTime((int)atSpanInfoRequestDTO.getCoverageYear(), 1, 1, 0, 0, 0, 0);
			DateTime renewalDateFeb = new DateTime((int)atSpanInfoRequestDTO.getCoverageYear(), 2, 1, 0, 0, 0, 0);
			
			if(atSpanInfoRequestDTO.getEligibilityEndDate().isBefore(currentDate)) {
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_CLOSED;
				// TODO : we may need to update the AT_INFO_SPAN table for this			
			} else if (null != atSpanInfoRequestDTO.getAtEffectiveDate() && (atSpanInfoRequestDTO.getEligibilityEndDate().isBefore(atSpanInfoRequestDTO.getAtEffectiveDate()))) {
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_CLOSED;
			} else if((atSpanInfoRequestDTO.getEligibilityStartDate().isBefore(currentDate.minusDays(MESProcessingConstants.SIXTY_DAYS))) 
					&& (atSpanInfoRequestDTO.getEligibilityEndDate().isBefore(currentDate))) {
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_CLOSED;
			} else if(null!= enrolledEligibilityStartDate && atSpanInfoRequestDTO.getEligibilityEndDate().isBefore(enrolledEligibilityStartDate)) {
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_CLOSED;
			} else if((atSpanInfoRequestDTO.getEligibilityStartDate().isAfter(currentDate.plusDays(MESProcessingConstants.SIXTY_DAYS))) && 
					!(atSpanInfoRequestDTO.getCurrentSpan() == 1 && 
						(atSpanInfoRequestDTO.getEligibilityStartDate().isEqual(renewalDateJan) || atSpanInfoRequestDTO.getEligibilityStartDate().isEqual(renewalDateFeb))
					)){
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_QUEUE;
				dueDate = atSpanInfoRequestDTO.getEligibilityStartDate().minusDays(MESProcessingConstants.FIFTY_NINE_DAYS);
			} else {
				atProcessingType = MESProcessingConstants.AT_PROCESSING_TYPE_REALTIME;
			}
			
			// set the ATQueueInfoResponseDTO
			atQueueInfoResponseDTO.setAtProcessingType(atProcessingType);
			atQueueInfoResponseDTO.setDueDate(dueDate);
			
		} catch (Exception e) {
			StringBuilder errorReason = new StringBuilder()
					.append(MESProcessingConstants.ERROR_CALLING_DETERMINE_PROCESSING_TYPE)
					.append(MESProcessingConstants.REASON).append(ExceptionUtils.getFullStackTrace(e));
			LOGGER.error(errorReason.toString());
		}
		
		return atQueueInfoResponseDTO;
	}
	
	
	private DateTime retrieveCurrentlyEnrolledESD(int cmrHouseholdId, long coverageYear) {
		Long enrolledAppId = ssapApplicationRepository.getHealthOrDentalEnrolledApplicationByCoverageYearAndCmrId(coverageYear, new BigDecimal(cmrHouseholdId));
		if(null != enrolledAppId) {
		Timestamp timestamp = ssapApplicationRepository.findById(enrolledAppId).getStartDate();
		return new DateTime(timestamp.getTime());
		} else {
			return null;
		}		
	}


	/**
	 * This method is used to populate the AT queue req DTO with the information needed to determine the AT status
	 * @param request
	 * @return
	 */
	@Override
	public ATSpanInfoRequestDTO populateATQueueInfoRequestDTO(AccountTransferRequestDTO request) {
		ATSpanInfoRequestDTO atSpanInfoRequestDTO = null;
		String mutipleEligibiltySpanEnabled = DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_AT_MULTIPLE_ELIGIBILITY_SPAN_CONFIG);
		if(mutipleEligibiltySpanEnabled.equalsIgnoreCase(Boolean.TRUE.toString())){
			
		
			atSpanInfoRequestDTO = new ATSpanInfoRequestDTO();
			// populate the dates for this DTO
			String eligibilityIndicatorType = null;
			
			ExchangeEligibilityType exchangeEligibility = null;
			final List<InsuranceApplicantType> insuranceApplicantTypeList = request.getAccountTransferRequestPayloadType().getInsuranceApplication().getInsuranceApplicant();
			// assumption : all the insurance applicant will have same Exchange eligibility start and end dates
			for (InsuranceApplicantType insuranceApplicantType : insuranceApplicantTypeList) {
				List<EligibilityType> eligibilityTypeList = insuranceApplicantType.getEmergencyMedicaidEligibilityOrMedicaidMAGIEligibilityOrMedicaidNonMAGIEligibility();
				for (EligibilityType eligibilityType : eligibilityTypeList) {
					eligibilityIndicatorType = StringUtils.substringAfter(eligibilityType.getClass().toString(), MESProcessingConstants.EE_PACKAGE_NAME);
					if (eligibilityIndicatorType.equalsIgnoreCase(EligibilityConstants.EXCHANGE_ELIGIBILITY_TYPE)) {
						// get exchangeEligibility object
						exchangeEligibility = (ExchangeEligibilityType) eligibilityType;
						break;
					}
				}
				// as all applications will have same dates if we get one exchange 
				// eligibility of any applicant we can break the loop
				if(exchangeEligibility != null) {
					break;	
				}
				
			}
			
			// if exchangeEligibility is not null then set the ESD & EED
			if(null != exchangeEligibility) {
				//set the ESD & EED
				atSpanInfoRequestDTO.setEligibilityStartDate(MESUtils.getDate(exchangeEligibility.getEligibilityDateRange().getStartDate().getDate()));
				atSpanInfoRequestDTO.setEligibilityEndDate(MESUtils.getDate(exchangeEligibility.getEligibilityDateRange().getEndDate().getDate()));			
				atSpanInfoRequestDTO.setAtEffectiveDate(this.retrieveATEffectiveDateFromPreviousApplication(request.getCompareToApplicationId()));
			}
			atSpanInfoRequestDTO.setTotalSpans(ReferralUtil.convertToInt(request.getAccountTransferRequestPayloadType().getInsuranceApplication().getApplicationExtension().getEligibilitySpans().getTotalEligibilitySpan().getValue()));
			atSpanInfoRequestDTO.setCurrentSpan(ReferralUtil.convertToInt(request.getAccountTransferRequestPayloadType().getInsuranceApplication().getApplicationExtension().getEligibilitySpans().getCurrentEligibilitySpan().getValue()));
			atSpanInfoRequestDTO.setAtSpanInfoId(request.getAtSpanInfoId());
		}
		
		return atSpanInfoRequestDTO;
	}

	
	/**
	 * This API will return the AT Effective date by calling Enrollment API
	 * @return
	 */
	private DateTime retrieveATEffectiveDateFromPreviousApplication(long prevSsapApplicationId) {
		// TODO : call API to get the AT effective date 
		
		DateTime effectiveDate = null;
		if(prevSsapApplicationId > 0){
			SsapApplication previousSsapApplication = ssapApplicationRepository.findById(prevSsapApplicationId);
			if(previousSsapApplication != null && previousSsapApplication.getEffectiveDate() != null){
				effectiveDate = new DateTime(previousSsapApplication.getEffectiveDate());
			}
			
		}
			
		return effectiveDate;
	}
	
}
