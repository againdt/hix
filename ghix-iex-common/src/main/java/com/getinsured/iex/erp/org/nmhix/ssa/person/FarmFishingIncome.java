//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.erp.org.nmhix.ssa.person;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FarmFishingIncome complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FarmFishingIncome">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="netIncomeAmount" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="netIncomeFrequency" type="{http://www.cohbe.org/SSA/Person}IncomeFrequency-Enum" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FarmFishingIncome", propOrder = {
    "netIncomeAmount",
    "netIncomeFrequency"
})

@Entity
public class FarmFishingIncome {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
    protected Double netIncomeAmount;
    protected IncomeFrequencyEnum netIncomeFrequency;

    /**
     * Gets the value of the netIncomeAmount property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getNetIncomeAmount() {
        return netIncomeAmount;
    }

    /**
     * Sets the value of the netIncomeAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setNetIncomeAmount(Double value) {
        this.netIncomeAmount = value;
    }

    /**
     * Gets the value of the netIncomeFrequency property.
     * 
     * @return
     *     possible object is
     *     {@link IncomeFrequencyEnum }
     *     
     */
    public IncomeFrequencyEnum getNetIncomeFrequency() {
        return netIncomeFrequency;
    }

    /**
     * Sets the value of the netIncomeFrequency property.
     * 
     * @param value
     *     allowed object is
     *     {@link IncomeFrequencyEnum }
     *     
     */
    public void setNetIncomeFrequency(IncomeFrequencyEnum value) {
        this.netIncomeFrequency = value;
    }

}
