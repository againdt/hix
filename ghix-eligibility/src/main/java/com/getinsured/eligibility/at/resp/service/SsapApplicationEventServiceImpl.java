package com.getinsured.eligibility.at.resp.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Service;

import com.getinsured.eligibility.at.ref.common.LceAllChangesHandlerDetermination;
import com.getinsured.eligibility.at.ref.service.ReferralOEService;
import com.getinsured.eligibility.at.ref.service.ReferralSEPService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.at.sep.service.SepEventsService;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.SsapApplicationEventTypeEnum;
import com.getinsured.eligibility.model.SepEvents;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.config.IEXConfiguration;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantEventCodeSimpleType;
import com.getinsured.iex.erp.gov.cms.hix._0_1.hix_types.ExtendedApplicantNonQHPCodeSimpleType;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplicantEvent;
import com.getinsured.iex.ssap.model.SsapApplicantEvent.EventPrecedenceIndicator;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.model.SsapApplicationEvent;
import com.getinsured.iex.ssap.repository.SsapApplicantEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationEventRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.timeshift.TSDateTime;
import com.getinsured.timeshift.util.TSDate;

@Service("ssapApplicationEventService")
@DependsOn("dynamicPropertiesUtil")
public class SsapApplicationEventServiceImpl implements SsapApplicationEventService {
	
	private static final Logger LOGGER = Logger.getLogger(SsapApplicationEventServiceImpl.class);

	private static final String CHANGE_PLAN = "Y";
	private static final String SEP_REMOVE_OTHER = "OTHER_ELIGIBILITY_CHANGE-HEALTH_STATUS_CHANGE";
	private static final String SEP_REMOVE_OTHER_CA = "OTHER_ELIGIBILITY_CHANGE";
	private static final String SEP_OTHER_EVENT = "OTHER";
	private static final String SEP_DOB_CHANGE_EVENT = "DOB_CHANGE";
	private static final String SEP_AUTO_DEMO_CHANGE_EVENT = "AUTO_DEMO_CHANGE";
	private static final String KEEP_ONLY_FALSE = "N";
	private static final String KEEP_ONLY_TRUE = CHANGE_PLAN;
	private static final String RELATIONSHIP_CHANGE = "RELATIONSHIP_CHANGE";
	private static final String AUTO_REMOVAL_WITHOUT_EVENT = "AUTO_REMOVAL_WITHOUT_EVENT";
	private static final String AUTO_CHANGE_IN_CSR_WITHOUT_EVENT = "AUTO_CHANGE_IN_CSR_WITHOUT_EVENT";
	private static final String INCOME_CHANGE_CSR_EVENT = "INCOME_CHANGE_CSR";
	private static final String LOSS_OF_ELIGIBILITY_AUTO_EVENT = "LOSS_OF_ELIGIBILITY_AUTO";
	private static final String AUTO_APTC_CSR_GAIN_EVENT = "AUTO_APTC_CSR_GAIN";
	
	private static final String INCOME_CHANGE_EVENT = "INCOME_CHANGE";
	private static final String SEP_NO_CHANGE_EVENT = "NO_CHANGE";
	public static final String LONG_DATE_FORMAT = "MM/dd/yyyy HH:mm:ss";
	
	@Autowired
	private SsapApplicationEventRepository ssapApplicationEventRepository;
	@Autowired
	private SsapApplicantEventRepository ssapApplicantEventRepository;
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	@Qualifier("referralSEPService")
	private ReferralSEPService referralSEPService;
	
	@Autowired
	private ReferralOEService referralOEService;

	private enum Events {
		ADD, REMOVE, QUALIFYING_EVENT, OTHER
	}

	private static Map<String, SepEvents> sepEventsOther;
	private static Map<String, SepEvents> sepRemoveEvents;
	private static Map<String, SepEvents> sepAddEvents;

	private static int enrollmentGracePeriod;

	private static int numberOfenrollmentDays = 60;

	@Autowired
	@Qualifier("sepEventsService")
	private SepEventsService sepEventsService;
	
	public static Map<String, SepEvents> getSepRemoveEvents() {
		return sepRemoveEvents;
	}
	
	public static Map<String, SepEvents> getSepAddEvents() {
		return sepAddEvents;
	}
	
	public static Map<String, SepEvents> getSepOtherEvents() {
		return sepEventsOther;
	}

	@PostConstruct
	public void doPost() {
		sepEventsOther = sepEventsService.findFinancialSepEventsByChangeType(Events.OTHER.name());
		sepRemoveEvents = sepEventsService.findFinancialSepEventsByChangeType(Events.REMOVE.name());
		sepAddEvents = sepEventsService.findFinancialSepEventsByChangeType(Events.ADD.name());
		enrollmentGracePeriod = 9;
		String enrollmentGracePeriodstr = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.LCE_ENROLLMENT_DAYS_GRACE_PERIOD);
		if (!StringUtils.isEmpty(enrollmentGracePeriodstr)) {
			enrollmentGracePeriod = Integer.parseInt(enrollmentGracePeriodstr);
		}
	}

	@Override
	public SsapApplicationEvent setChangePlan(Long ssapApplicationId) {
		SsapApplicationEvent ssapApplicationEvent = getSsapApplicationEventsByApplicationId(ssapApplicationId);
		ssapApplicationEvent.setChangePlan(CHANGE_PLAN);
		return ssapApplicationEventRepository.save(ssapApplicationEvent);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = today with time as 00:00:00 and 3. Enrollment End Date = today + (60 + buffer) days with time as 23:59:59 3. Keep Only to Y
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today Enrollment Start Date as today Enrollment End Date = today + (60 + buffer) days
	 */
	@Override
	public SsapApplicationEvent createDoBApplicationEvent(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear());
		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp todayTimestamp = new Timestamp(todayDate.getMillis());
		DateTime endDate = new DateTime(calculateEndDate(new TSDate(),isNextYearsOE)).withTime(23, 59, 59, 999);

		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			// if (ReferralConstants.DOB_CHANGED.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) || ReferralConstants.NO_EFFECT.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
			if (!ReferralConstants.NON_DEMO_DOB_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				// new event will be captured for DOB change for FA 
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_DOB_CHANGE_EVENT));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEnrollmentEndDate(calculateEndDate(new TSDate(), isNextYearsOE));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = null and 3. Enrollment End Date = null 3. Keep Only to N
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today Enrollment Start Date as today Enrollment End Date = today + (60 + buffer) days
	 */
	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEvent(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		// ssapApplicationEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
		// ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(sepEndDate.getTime()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		Date todayDate = new TSDate();
		Timestamp todayTimestamp = new Timestamp(todayDate.getTime());

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if ("NONE".equals(applicant.getEligibilityStatus())) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvent.setEnrollmentStartDate(todayTimestamp);
				applicantEvent.setEnrollmentEndDate(calculateEndDate(todayDate,isNextYearsOE));

				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createDuplicateOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId) {
		return createDemoOnlyApplicationEvent(ssapApplicationId, enrolledApplicationId);
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Copy Enrollment Start Date and Enrollment End Date from previous application 3. Keep Only to N
	 * 
	 * 
	 * Create Events for all members with Event Type as OTHER (MRC code as AI) Report Start Date as today Event Date as today
	 */
	@Override
	public SsapApplicationEvent createDemoOnlyApplicationEvent(Long ssapApplicationId, Long enrolledApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		if (enrolledApplicationId == null || enrolledApplicationId == 0) {
			throw new GIRuntimeException("enrolledApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}
		final SsapApplicationEvent enrolledEvent = getSsapApplicationEventsByApplicationId(enrolledApplicationId);
		SsapApplication ssapApplication = ssapApplications.get(0);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear());

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		ssapApplicationEvent.setCoverageStartDate(null);
		Timestamp enrollmentStartDate = new Timestamp((TSDateTime.getInstance().withTime(0, 0, 0, 0).toDate()).getTime());
		Timestamp enrollmentEndDate = calculateEndDate(enrollmentStartDate,isNextYearsOE);
		// HIX-117156 Change 
		// if the enrolled application is OE and comes from demo or no change handler
		if(StringUtils.equals(enrolledEvent.getSsapApplication().getApplicationType(), "OE")) {
			// then we can not use the same enrolled event dates as its enrollment start and end dates 
			// so we calculate the dates and set it
			ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
			ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);	
		} else {
			// for all other applications apart enrolled application OE and no change handler then dates would be set from enrolled events
			ssapApplicationEvent.setEnrollmentStartDate(enrolledEvent != null ? enrolledEvent.getEnrollmentStartDate() : enrollmentStartDate);
			ssapApplicationEvent.setEnrollmentEndDate(enrolledEvent != null ? enrolledEvent.getEnrollmentEndDate() : enrollmentEndDate);	
		}
		
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		Timestamp todayTimestamp = new Timestamp(new TSDate().getTime());
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (ReferralConstants.DEMOGRAPHIC_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) || ReferralConstants.NO_EFFECT.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				// if the event is of type NO change then have NO_CHANGE event inserted else have AUTO_DEMO_CHANGE inserted into sep applicant event
				applicantEvent.setSepEvents(ReferralConstants.NO_EFFECT
						.contains(ApplicantStatusEnum.fromValue(applicant.getStatus())) ? sepEventsOther.get(SEP_NO_CHANGE_EVENT)
								: sepEventsOther.get(SEP_AUTO_DEMO_CHANGE_EVENT));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
				applicantEvent.setReportStartDate(todayTimestamp);
				applicantEvent.setEventDate(todayTimestamp);
				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent getSsapApplicationEventsByApplicationId(Long ssapApplicationId) {

		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplicationEvent> applicationEvents = ssapApplicationEventRepository.findEventBySsapApplicationId(ssapApplicationId);
		return (applicationEvents != null && applicationEvents.size() > 0) ? applicationEvents.get(0) : null;
	}

	/**
	 * Create SEP event with below details:- 1. Coverage Start Date as null 2. Enrollment Start Date = today with time as 00:00:00 and 3. Enrollment End Date = passed sepEndDate with time as 23:59:59 4. Keep Only to N
	 * 
	 * 
	 * Create Events for all members based on applicantEventMap
	 */
	@Override
	public SsapApplicationEvent createApplication_Applicant_SepEvents(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date sepEndDate, Integer createdBy, boolean keepOnly) {
		LOGGER.info("in createApplication_Applicant_SepEvents applicantEventMap: "+applicantEventMap);
		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(ssapApplicationId, applicantEventMap, sepEndDate, createdBy, SsapApplicationEventTypeEnum.SEP, keepOnly, null);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createApplication_Applicant_QepEvents(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date sepEndDate, Integer createdBy) {

		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(ssapApplicationId, applicantEventMap, sepEndDate, createdBy, SsapApplicationEventTypeEnum.QEP, false, null);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createAptcOnlyApplicationEvent(SsapApplication ssapApplication, SepEvents event, Date applicantEventDate, Date applicantReportDate) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

		DateTime endDate = new DateTime(applicantReportDate.getTime()).withTime(23, 59, 59, 999);

		ssapApplicationEvent.setEnrollmentStartDate(new Timestamp(applicantReportDate.getTime()));
		ssapApplicationEvent.setEnrollmentEndDate(calculateEndDate(endDate.toDate(),isNextYearsOE));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_APTC_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(event);
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(new Timestamp(applicantReportDate.getTime()));
				applicantEvent.setEventDate(new Timestamp(applicantEventDate.getTime()));
				applicantEvent.setEnrollmentStartDate(new Timestamp(applicantReportDate.getTime()));
				applicantEvent.setEnrollmentEndDate(calculateEndDate(endDate.toDate(),isNextYearsOE));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	private SsapApplicationEvent createApplicationAndApplicantEvent(Long ssapApplicationId, Map<String, SsapApplicantEvent> applicantEventMap, Date sepEndDate, Integer createdBy, SsapApplicationEventTypeEnum eventType, boolean keepOnly,  Date sepStartDate) {
		if (ssapApplicationId == null || ssapApplicationId == 0) {
			throw new GIRuntimeException("ssapApplicationId cannot be null");
		}

		List<SsapApplication> ssapApplications = ssapApplicationRepository.getApplicationsById(ssapApplicationId);

		if (ssapApplications == null || ssapApplications.size() == 0) {
			throw new GIRuntimeException("Unable to find application for ssapApplicationId " + ssapApplicationId);
		}

		SsapApplication ssapApplication = ssapApplications.get(0);

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(createdBy);
		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp applicationSEPStartDate = new Timestamp(todayDate.getMillis());
		DateTime startDate = null;
		if(sepStartDate!=null) {
			startDate= new DateTime(sepStartDate.getTime()).withTime(0, 0, 0, 0);
			applicationSEPStartDate = new Timestamp(startDate.getMillis());
		}
		DateTime endDate = new DateTime(sepEndDate.getTime()).withTime(23, 59, 59, 999);
				
		ssapApplicationEvent.setEnrollmentStartDate(applicationSEPStartDate);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(eventType);
		ssapApplicationEvent.setKeepOnly(keepOnly ? KEEP_ONLY_TRUE : KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = applicantEventMap.get(applicant.getApplicantGuid());
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			applicantEvent.setCreatedBy(createdBy);
			applicantEvents.add(applicantEvent);
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEvent;

	}

	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEvent(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantEventMap) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		ApplicantEvent payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantEventMap.get(applicant.getApplicantGuid());
			SepEvents sepEvent = null;
			if (ReferralConstants.Y.equalsIgnoreCase(applicant.getOnApplication())) {
				if (null == payloadEvent) {
					sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
					applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
				} else {
					try {
						sepEvent = sepRemoveEvents.get(ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.getCode()).name());
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}					
					// this is added if the event is passed and not NonQHP Code is not passed then take out the SEP Event
					// this is mainly added for GainOfMedicaid passed as an Event and not as a QHP code
					try {
						if(null == sepEvent) {
							sepEvent = sepRemoveEvents.get(ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.getCode()).name());
						}						
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}
					
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
			} else {
				if (null == payloadEvent) {
					sepEvent = sepRemoveEvents.get(SEP_REMOVE_OTHER);
					if(sepEvent==null) {
						sepEvent = sepRemoveEvents.get(SEP_REMOVE_OTHER_CA);
					}
					applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
				} else {
					try {
						sepEvent = sepRemoveEvents.get(ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.getCode()).name());	
					}catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}
					
					try {
						// this is added if the event is passed and not NonQHP Code is not passed then take out the SEP Event
						// this is mainly added for GainOfMedicaid passed as an Event and not as a QHP code
						if(null == sepEvent) {
							sepEvent = sepRemoveEvents.get(ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.getCode()).name());
						}						
					}catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}
					
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
			}
			applicantEvent.setSepEvents(sepEvent);
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate(),isNextYearsOE));
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		DateTime eventDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp eventTimestamp = new Timestamp(eventDate.getMillis());
		ssapApplicationEvent.setEnrollmentStartDate(eventTimestamp);
		Timestamp enrollmentEndDate = calculateEndDate(eventDate.withTime(23, 59, 59, 999).toDate(),isNextYearsOE);
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent createDeniedHouseholdApplicationEventForMixedAndNone(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantEventMap) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		DateTime eventDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp eventTimestamp = new Timestamp(eventDate.getMillis());

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear()) ;

		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);

			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
			applicantEvent.setSepEvents(sepEvent);
			applicantEvent.setEnrollmentStartDate(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate(),isNextYearsOE));
			applicantEvents.add(applicantEvent);
		}

		ssapApplicationEvent.setEnrollmentStartDate(eventTimestamp);
		Timestamp enrollmentEndDate = calculateEndDate(eventDate.withTime(23, 59, 59, 999).toDate(),isNextYearsOE);
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	private Timestamp calculateEndDate(Date currDate, boolean isNextYearsOE) {
		if(!isNextYearsOE){
			DateTime endDate = new DateTime(currDate.getTime());
			endDate = endDate.plusDays(enrollmentGracePeriod + numberOfenrollmentDays);
			return new Timestamp(endDate.getMillis());
		}
		else
		{
			try {
				return new Timestamp(new SimpleDateFormat(LONG_DATE_FORMAT).
						parse(DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_CURRENT_OE_END_DATE) + " 23:59:59").getTime());
			} catch (ParseException e) {
				LOGGER.error("Error while calculating the SsapApplicantEvent Enrollment End Date",e);
				return null;
			}
		}
	}

	@Override
	public SsapApplicationEvent createCitizenshipApplicationEvent(SsapApplication ssapApplication, Map<String, ApplicantEvent> applicantExtensionEvent) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear());

		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp todayTimestamp = new Timestamp(todayDate.getMillis());
		DateTime endDate = new DateTime(calculateEndDate(new TSDate(),isNextYearsOE)).withTime(23, 59, 59, 999);

		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		ssapApplicationEvent.setChangePlan(ReferralConstants.N);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		ApplicantEvent payloadEvent;
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_CITIZEN_SHIP_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				if (ReferralConstants.CITIZEN_SHIP_CHANGED.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
					payloadEvent = applicantExtensionEvent.get(applicant.getApplicantGuid());
				} else {
					payloadEvent = null;
				}
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(SEP_OTHER_EVENT));
				if (null == payloadEvent) {
					applicantEvent.setReportStartDate(new Timestamp(new TSDate().getTime()));
					applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
				} else {
					applicantEvent.setReportStartDate(new Timestamp(payloadEvent.getReportDate().getTime()));
					applicantEvent.setEventDate(new Timestamp(payloadEvent.getEventDate().getTime()));
				}
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEnrollmentEndDate(calculateEndDate(new TSDate(),isNextYearsOE));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createRelationshipApplicationEvent(SsapApplication ssapApplication) {

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(ssapApplication.getCoverageYear());

		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp todayTimestamp = new Timestamp(todayDate.getMillis());
		DateTime endDate = new DateTime(calculateEndDate(new TSDate(),isNextYearsOE)).withTime(23, 59, 59, 999);

		ssapApplicationEvent.setEnrollmentStartDate(todayTimestamp);
		ssapApplicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		ssapApplicationEvent.setSsapApplication(ssapApplication);

		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();
		for (SsapApplicant applicant : ssapApplication.getSsapApplicants()) {
			if (!ReferralConstants.NON_RELATIONSHIP_APPLICANT_STATUS.contains(ApplicantStatusEnum.fromValue(applicant.getStatus()))) {
				SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
				applicantEvent.setSepEvents(sepEventsOther.get(RELATIONSHIP_CHANGE));
				applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
				applicantEvent.setSsapApplicant(applicant);
				applicantEvent.setReportStartDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEventDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEnrollmentStartDate(new Timestamp(new TSDate().getTime()));
				applicantEvent.setEnrollmentEndDate(calculateEndDate(new TSDate(),isNextYearsOE));
				applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);

				applicantEvents.add(applicantEvent);
			}
		}

		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);

	}

	@Override
	public SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean autoRemoveEvents) {
		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear());

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		boolean blnKeepOnlyDontAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			Date eventDate = new TSDate();
			Date reportDate = new TSDate();
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}

			if (!blnKeepOnlyDontAllow && ReferralConstants.DONT_ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyDontAllow = true;
			}
			
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (nonQHPEventName != null) {
							eventCode = nonQHPEventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}

					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							if(payloadEvent.get(0).getReportDate()!=null) {
								reportDate = payloadEvent.get(0).getReportDate();
							}
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (autoRemoveEvents){
						if (SEP_OTHER_EVENT.equals(eventCode)){
							eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
						}
					}
					
					sepEvent = sepRemoveEvents.get(eventCode);
				} else {
					sepEvent = sepRemoveEvents.get(AUTO_REMOVAL_WITHOUT_EVENT);
				}
			}

			if(statusEnum == ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT) {
				if(null != payloadEvent && payloadEvent.size() > 0) {
				try {
					ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (eventName != null && (eventName == ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE)) {
						eventCode = eventName.name();
						sepEvent = sepEventsOther.get(eventCode);
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
				}
				eventDate = payloadEvent.get(0).getEventDate();
				reportDate = payloadEvent.get(0).getReportDate();
				}
			}

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			Timestamp enrollmentStartDate = ReferralUtil.getEnrollmentStartDateForFutureEvent(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentStartDate(enrollmentStartDate);
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate(),isNextYearsOE));
			if(!SEP_OTHER_EVENT.equalsIgnoreCase(sepEvent.getName())) {
				applicationSepEnrollmentStartDates.add(enrollmentStartDate);
				applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			}
			applicantEvents.add(applicantEvent);
		}
		Timestamp enrollmentStartDate = new Timestamp((TSDateTime.getInstance().withTime(0, 0, 0, 0).toDate()).getTime());
		Timestamp enrollmentEndDate = calculateEndDate(enrollmentStartDate,isNextYearsOE);
		if(!applicationSepEnrollmentStartDates.isEmpty()) {
			if(applicationSepEnrollmentStartDates.first()!=null) {
	        	enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
	        }	
		}
		
        if(!applicationSepEnrollmentEndDates.isEmpty()) {
        	if(applicationSepEnrollmentEndDates.last()!=null) {
        		enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());	
        	}        	
        }
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		if (blnKeepOnlyAllow && !blnKeepOnlyDontAllow) {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		} else {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		}
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		
	}
	
	@Override
	public SsapApplicationEvent createApplicationEventForAllChanges(SsapApplication currentApplication, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		return createAutoApplicationEvents(currentApplication, applicantExtensionEvents, false);
	}

	private SsapApplicationEvent createAutoApplicationEvents(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean autoRemoveEvents) {
		final String askQleOEPFlag =  DynamicPropertiesUtil.getPropertyValue(IEXConfiguration.IEXConfigurationEnum.IEX_ASK_QLE_IN_OEP);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear());

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		boolean blnKeepOnlyDontAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			Date eventDate = new TSDate();
			Date reportDate = new TSDate();
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}

			if (!blnKeepOnlyDontAllow && ReferralConstants.DONT_ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyDontAllow = true;
			}

			if (ReferralUtil.listSize(payloadEvent) != ReferralConstants.NONE && ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(statusEnum)) {
				ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
				eventCode = eventName.name();
				eventDate = payloadEvent.get(0).getEventDate();
				reportDate = payloadEvent.get(0).getReportDate();
				sepEvent = sepAddEvents.get(eventCode);
			} else if (ReferralUtil.listSize(payloadEvent) != 0 && ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)) {
				try {
					ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (nonQHPEventName != null) {
						eventCode = nonQHPEventName.name();
						eventDate = payloadEvent.get(0).getEventDate();
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
				}

				try {
					ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (eventName != null) {
						eventCode = eventName.name();
						eventDate = payloadEvent.get(0).getEventDate();
						if(payloadEvent.get(0).getReportDate()!=null) {
							reportDate = payloadEvent.get(0).getReportDate();
						}
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
				}

				// TODO: handle AUTO_REMOVE_EVENT scenario
				if (autoRemoveEvents){
					if (SEP_OTHER_EVENT.equals(eventCode)){
						eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
					}
				}
				
				sepEvent = sepRemoveEvents.get(eventCode);
			}
			// For citizenship and other events
			else if (ReferralUtil.listSize(payloadEvent) != 0) {
				/* Handle custom logic for ChangeInAddress, 
				 * 		try to read from AT 
				 * or 	if fails continue with existing flow and create OTHER event */
				if (statusEnum == ApplicantStatusEnum.UPDATED_ZIP_COUNTY){
					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null && (eventName == ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS || 
								eventName == ExtendedApplicantEventCodeSimpleType.MOVED_INTO_STATE)) {
							eventCode = eventName.name();
							sepEvent = sepEventsOther.get(eventCode);
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}
				}  else if(statusEnum == ApplicantStatusEnum.CHANGE_IN_APTC_AMOUNT) {
					try {
						
							ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
							if (eventName != null && (eventName == ExtendedApplicantEventCodeSimpleType.INCOME_CHANGE)) {
								eventCode = eventName.name();
								sepEvent = sepEventsOther.get(eventCode);
							}
						
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}
				}
				eventDate = payloadEvent.get(0).getEventDate();
				reportDate = payloadEvent.get(0).getReportDate();
			} else if (askQleOEPFlag.equalsIgnoreCase(Boolean.FALSE.toString())
					&& LceAllChangesHandlerDetermination.isOpenEnrollment(currentApplication.getCoverageYear())
					&& LceAllChangesHandlerDetermination.isOepEsd(currentApplication.getStartDate())) {
				LOGGER.info("Inside OE period, creating specific events to fix SEP window related issues");
				if (statusEnum == ApplicantStatusEnum.UPDATED_ZIP_COUNTY) {
					sepEvent = sepEventsOther.get(ExtendedApplicantEventCodeSimpleType.CHANGE_IN_ADDRESS.name());
				}
			}

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			Timestamp enrollmentStartDate = ReferralUtil.getEnrollmentStartDateForFutureEvent(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentStartDate(enrollmentStartDate);
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate(),isNextYearsOE));
			if(!SEP_OTHER_EVENT.equalsIgnoreCase(sepEvent.getName())) {
				applicationSepEnrollmentStartDates.add(enrollmentStartDate);
				applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			}
			applicantEvents.add(applicantEvent);
		}
		Timestamp enrollmentStartDate = new Timestamp((TSDateTime.getInstance().withTime(0, 0, 0, 0).toDate()).getTime());
		Timestamp enrollmentEndDate = calculateEndDate(enrollmentStartDate, isNextYearsOE);
		if(!applicationSepEnrollmentStartDates.isEmpty()) {
			if(applicationSepEnrollmentStartDates.first()!=null) {
	        	enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
	        }	
		}
		
        if(!applicationSepEnrollmentEndDates.isEmpty()) {
        	if(applicationSepEnrollmentEndDates.last()!=null) {
        		enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());	
        	}        	
        }
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		if (blnKeepOnlyAllow && !blnKeepOnlyDontAllow) {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_TRUE);
		} else {
			ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		}
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	@Override
	public SsapApplicationEvent createAndUpdateApplicationEventForNonFinancialQE(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		SsapApplicationEvent applicationEvent = null;

		applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getId());
		applicationEvent = applicationEvent != null ? updateSsapApplicationEvent(currentApplication, applicationEvent, eventType, changePlan) : createSsapApplicationEventForNonFinancial(currentApplication, eventType, changePlan,applicantExtensionEvents);

		return applicationEvent;
	}

	@Override
	public SsapApplicationEvent updateSsapApplicationEventForNonFinancial(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan) {
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;

		SsapApplicationEvent applicationEvent = null;
		SepEvents sepEvent = sepEventsOther.get("OTHER");
		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp todayTimestamp = new Timestamp(todayDate.getMillis());
		applicationEvent = ssapApplicationEventRepository.findEventBySsapApplication(currentApplication.getId());
		applicationEvent.setCreatedBy(null);
		applicationEvent.setEnrollmentStartDate(todayTimestamp);
		applicationEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp,isNextYearsOE));
		applicationEvent.setEventType(eventType);
		applicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		applicationEvent.setChangePlan(changePlan);
		applicationEvent.setSsapApplication(currentApplication);
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
			ssapApplicantEvent.setEventDate(todayTimestamp);
			ssapApplicantEvent.setReportStartDate(todayTimestamp);
			ssapApplicantEvent.setSepEvents(sepEvent);
			ssapApplicantEvent.setEnrollmentStartDate(todayTimestamp);
			ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp,isNextYearsOE));
			ssapApplicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			ssapApplicantEvent.setSsapApplicant(applicant);
			ssapApplicantEvent.setSsapAplicationEvent(applicationEvent);
			applicantEvents.add(ssapApplicantEvent);
			ssapApplicantEventRepository.save(ssapApplicantEvent);
		}
		applicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(applicationEvent);
	}

	private SsapApplicationEvent createSsapApplicationEventForNonFinancial(SsapApplication currentApplication, SsapApplicationEventTypeEnum eventType, String changePlan, Map<String, List<ApplicantEvent>> applicantExtensionEvents) {
		List<ApplicantEvent> payloadEvent;
		Map<String, SsapApplicantEvent> finalApplicantEvents = new HashMap<String, SsapApplicantEvent>();
		SepEvents sepEvent = sepEventsOther.get(INCOME_CHANGE_EVENT);
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;

		SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepStartDates = new TreeSet<Date>();
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			Date eventDate = new TSDate();
			Date reportDate = new TSDate();
			String eventCode = SEP_OTHER_EVENT;
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			SsapApplicantEvent ssapApplicantEvent = new SsapApplicantEvent();
			if (ReferralUtil.listSize(payloadEvent) == 1){
				try {
					ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
					if (eventName != null) {
						eventCode = eventName.name();
						if(INCOME_CHANGE_EVENT.equals(eventCode)) {
							eventDate = payloadEvent.get(0).getEventDate();
							reportDate = payloadEvent.get(0).getReportDate();
						}
					}
				} catch (Exception e) {
					// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
				}
            }

			
			ssapApplicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			ssapApplicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			Timestamp enrollmentStartDate = ReferralUtil.getEnrollmentStartDateForFutureEvent(ssapApplicantEvent.getEventDate());
			ssapApplicantEvent.setEnrollmentStartDate(enrollmentStartDate);
			ssapApplicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			ssapApplicantEvent.setEnrollmentEndDate(calculateEndDate(ssapApplicantEvent.getEventDate(),isNextYearsOE));
			applicationSepStartDates.add(enrollmentStartDate);
			applicationSepEndDates.add(ssapApplicantEvent.getEnrollmentEndDate());

			finalApplicantEvents.put(applicant.getApplicantGuid(), ssapApplicantEvent);
		}
		SsapApplicationEvent ssapApplicationEvent = createApplicationAndApplicantEvent(currentApplication.getId(), finalApplicantEvents, applicationSepEndDates.last(), null, eventType, false, applicationSepStartDates.first());
		ssapApplicationEvent.setChangePlan(changePlan);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
	}

	private SsapApplicationEvent updateSsapApplicationEvent(SsapApplication currentApplication, SsapApplicationEvent applicationEvent, SsapApplicationEventTypeEnum eventType, String changePlan) {
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;

		DateTime todayDate = TSDateTime.getInstance().withTime(0, 0, 0, 0);
		Timestamp todayTimestamp = new Timestamp(todayDate.getMillis());
		SortedSet<Date> applicationSepEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepStartDates = new TreeSet<Date>();
		Timestamp enrollmentStartDate = ReferralUtil.getEnrollmentStartDateForFutureEvent(todayTimestamp);
		List<SsapApplicantEvent> applicantEvents = applicationEvent.getSsapApplicantEvents();
		for (SsapApplicantEvent applicantEvent : applicantEvents) {
			SepEvents sepEvent = sepEventsOther.get(INCOME_CHANGE_EVENT);
			applicantEvent.setSepEvents(sepEvent);

			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			applicantEvent.setEventDate(todayTimestamp);
			applicantEvent.setEnrollmentStartDate(enrollmentStartDate);
			applicantEvent.setEnrollmentEndDate(calculateEndDate(todayTimestamp,isNextYearsOE));
			applicationSepStartDates.add(enrollmentStartDate);
			applicationSepEndDates.add(applicantEvent.getEnrollmentEndDate());
		}

		DateTime startDate = new DateTime(applicationSepStartDates.first().getTime()).withTime(23, 59, 59, 999);
		DateTime endDate = new DateTime(applicationSepEndDates.last().getTime()).withTime(23, 59, 59, 999);
		applicationEvent.setEnrollmentStartDate(new Timestamp(startDate.getMillis()));
		applicationEvent.setEnrollmentEndDate(new Timestamp(endDate.getMillis()));
		applicationEvent.setEventType(eventType);
		applicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		applicationEvent.setChangePlan(changePlan);
		applicationEvent.setSsapApplication(currentApplication);
		return ssapApplicationEventRepository.saveAndFlush(applicationEvent);

	}

	@Override
	public SsapApplicationEvent createApplicationEventForCSChanges(SsapApplication currentApplication,
			Map<String, List<ApplicantEvent>> applicantExtensionEvents, boolean pureCsChange,SsapApplication enrolledApplication) {
		boolean isNextYearsOE = referralOEService.isOpenEnrollment(currentApplication.getCoverageYear()) ;

		SsapApplicationEvent ssapApplicationEvent = new SsapApplicationEvent();
		ssapApplicationEvent.setCreatedBy(null);

		ssapApplicationEvent.setEventType(SsapApplicationEventTypeEnum.SEP);

		ssapApplicationEvent.setSsapApplication(currentApplication);
		List<ApplicantEvent> payloadEvent;
		List<SsapApplicantEvent> applicantEvents = new ArrayList<SsapApplicantEvent>();

		SortedSet<Date> applicationSepEnrollmentEndDates = new TreeSet<Date>();
		SortedSet<Date> applicationSepEnrollmentStartDates = new TreeSet<Date>();
		boolean blnKeepOnlyAllow = false;
		ApplicantStatusEnum statusEnum;
		for (SsapApplicant applicant : currentApplication.getSsapApplicants()) {
			SsapApplicantEvent applicantEvent = new SsapApplicantEvent();
			applicantEvent.setSsapApplicant(applicant);
			applicantEvent.setSsapAplicationEvent(ssapApplicationEvent);
			applicantEvent.setEventPrecedenceIndicator(EventPrecedenceIndicator.Y);
			payloadEvent = applicantExtensionEvents.get(applicant.getApplicantGuid());
			statusEnum = ApplicantStatusEnum.fromValue(applicant.getStatus());
			// default event
			SepEvents sepEvent = sepEventsOther.get(SEP_OTHER_EVENT);
			Date eventDate = new TSDate();
			Date reportDate = new TSDate();
			String eventCode = SEP_OTHER_EVENT;

			if (!blnKeepOnlyAllow && ReferralConstants.ALLOW_KEEP_ONLY_APPLICANT_STATUS.contains(statusEnum)) {
				blnKeepOnlyAllow = true;
			}
			
			if (ReferralConstants.NEWLY_INELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantNonQHPCodeSimpleType nonQHPEventName = ExtendedApplicantNonQHPCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (nonQHPEventName != null) {
							eventCode = nonQHPEventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantEventCodeSimpleType event was sent in request.
					}

					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							if(payloadEvent.get(0).getReportDate()!=null) {
								reportDate = payloadEvent.get(0).getReportDate();
							}
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (SEP_OTHER_EVENT.equals(eventCode)){
						eventCode = AUTO_REMOVAL_WITHOUT_EVENT;
					}
					
					sepEvent = sepRemoveEvents.get(eventCode);
				} else {
					sepEvent = sepRemoveEvents.get(AUTO_REMOVAL_WITHOUT_EVENT);
				}
			} else if (ReferralConstants.NEWLY_ELIGIBLE_APPLICANT_STATUS.contains(statusEnum)){
				if (ReferralUtil.listSize(payloadEvent) == 1){
					try {
						ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
						if (eventName != null) {
							eventCode = eventName.name();
							eventDate = payloadEvent.get(0).getEventDate();
							reportDate = payloadEvent.get(0).getReportDate();
						}
					} catch (Exception e) {
						// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
					}

					if (SEP_OTHER_EVENT.equals(eventCode)){
						LOGGER.error("should not reach here. no valid add event found. go back!!");
						return null;
					}
					
					sepEvent = sepAddEvents.get(eventCode);
					
					if (sepEvent == null){
						LOGGER.error("should not reach here. incoming add events doesnot match with system info. go back!!");
						return null;
					}
					
				} else {
					LOGGER.error("should not reach here. no add event found. go back!!");
					return null;
				}
			} else if (statusEnum == ApplicantStatusEnum.CHANGE_IN_CSR_LEVEL){
				if (pureCsChange){
					// check for FA to NFA or NFA to FA conversion and accordingly we need to add new events
					// for FA - NFA : LOSS_OF_ELIGIBILITY_AUTO and NFA - FA : AUTO_APTC_CSR_GAIN
					if((enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC || 
					enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR||
							enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR)
					    && currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP) {
						//this is FA to NFA conversion
						sepEvent = sepEventsOther.get(LOSS_OF_ELIGIBILITY_AUTO_EVENT);	
						
					} else if((enrolledApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.QHP) 
					    && (currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC || 
							currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.CSR ||
									currentApplication.getExchangeEligibilityStatus() == ExchangeEligibilityStatus.APTC_CSR)) {
						// this is NFA to FA conversion
						sepEvent = sepEventsOther.get(AUTO_APTC_CSR_GAIN_EVENT);
						
					} else {
						sepEvent = sepEventsOther.get(INCOME_CHANGE_CSR_EVENT);	
					}
					// to handle the case if event not found and sepEvent is null we should set it to INCOME_CHANGE_CSR_EVENT
					if(null == sepEvent) {
						sepEvent = sepEventsOther.get(INCOME_CHANGE_CSR_EVENT);
					}
					if (ReferralUtil.listSize(payloadEvent) == 1){
						try {
							ExtendedApplicantEventCodeSimpleType eventName = ExtendedApplicantEventCodeSimpleType.fromValue(payloadEvent.get(0).getCode());
							if (eventName != null) {
								eventCode = eventName.name();
								if(INCOME_CHANGE_EVENT.equals(eventCode)) {
									eventDate = payloadEvent.get(0).getEventDate();
									reportDate = payloadEvent.get(0).getReportDate();
								}
							}
						} catch (Exception e) {
							// Exception occured since ExtendedApplicantNonQHPCodeSimpleType event was sent in request.
						}
                    }
				} else {
					sepEvent = sepEventsOther.get(AUTO_CHANGE_IN_CSR_WITHOUT_EVENT); /* create auto event */
				}
			} 

			applicantEvent.setEventDate(new Timestamp(eventDate.getTime()));
			applicantEvent.setSepEvents(referralSEPService.retrieveEvent(sepEvent, eventDate, eventCode));
			Timestamp enrollmentStartDate = ReferralUtil.getEnrollmentStartDateForFutureEvent(applicantEvent.getEventDate());
			applicantEvent.setEnrollmentStartDate(enrollmentStartDate);
			applicantEvent.setReportStartDate(new Timestamp(reportDate.getTime()));
			applicantEvent.setEnrollmentEndDate(calculateEndDate(applicantEvent.getEventDate(),isNextYearsOE));
			applicationSepEnrollmentStartDates.add(enrollmentStartDate);
			applicationSepEnrollmentEndDates.add(applicantEvent.getEnrollmentEndDate());
			applicantEvents.add(applicantEvent);
		}

		Timestamp enrollmentStartDate = new Timestamp(new DateTime(applicationSepEnrollmentStartDates.first().getTime()).withTime(0, 0, 0, 0).getMillis());
		ssapApplicationEvent.setEnrollmentStartDate(enrollmentStartDate);
		Timestamp enrollmentEndDate = new Timestamp(new DateTime(applicationSepEnrollmentEndDates.last().getTime()).withTime(23, 59, 59, 999).getMillis());
		ssapApplicationEvent.setEnrollmentEndDate(enrollmentEndDate);
		ssapApplicationEvent.setKeepOnly(KEEP_ONLY_FALSE);
		
		
		ssapApplicationEvent.setSsapApplicantEvents(applicantEvents);
		return ssapApplicationEventRepository.saveAndFlush(ssapApplicationEvent);
		
	}
}
