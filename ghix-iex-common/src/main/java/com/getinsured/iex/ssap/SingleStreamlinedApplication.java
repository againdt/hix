package com.getinsured.iex.ssap;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * @author chopra_s
 * 
 */
public class SingleStreamlinedApplication {

	@Expose
	@SerializedName("applicationType")
	private String appType;

	@SerializedName("ApplicationType")
	@Expose
	private String applicationType;

	@Expose
	@JsonFormat(shape = JsonFormat.Shape.STRING)
	private ApplicationStatus applicationStatus;

	@Expose
	private long coverageYear;

	@Expose
	private EligibilityStatus eligibilityStatus;

	/**
	 * This is used on UI to mark which page of SSAP currently
	 * user is editing.
	 * @see #maxAchievedPage
	 */
	@Expose
	private int currentPage;

	/**
	 * This is used on UI to mark which page in SSAP flow has been reached by the user.
	 * this is not used on the backend.
   * @see #currentPage
	 */
	@Expose
	private int maxAchievedPage;

	/**
	 * Indicates that SSAP was finalized on the UI.
	 */
	@Expose
	private boolean finalized;

	@Expose
	private Boolean acceptPrivacyIndicator = false;
	@Expose
	private String ssapApplicationId;
	@SerializedName("IP")
	@Expose
	private String iP;
	@NotNull
	@Expose
	private String applicationGuid;
	@Expose
	private Date applicationSignatureDate;

	@Expose
	private Boolean getHelpIndicator = null; // UI doesn't want anything preselected until user picks.

	@Expose
	private String helpType;
	@Expose
	private AuthorizedRepresentative authorizedRepresentative = new AuthorizedRepresentative();
	@Expose
	private String applyingForhouseHold;
	@Expose
	private Boolean applyingForFinancialAssistanceIndicator = null;
	@Expose
	private Boolean consentAgreement = null; // UI doesn't want anything preselected until user picks.

	/**
	 * If true, user agrees to allow Marketplace to use their income data for next 5 years
	 * to determinate eligibility.
	 */
	@Expose
	private Boolean agreeToUseIncomeData = null; // UI doesn't want anything preselected until user picks.

	/**
	 * Flag that allows automatically to end coverage if anyone on this application if conditions
	 * change.
	 * <p>
	 *   For example
	 * If (true and) anyone in this application after enrollment found to have other
	 * coverage, Marketplace will end their coverage. Otherwise (false), will stay enrolled,
	 * but will pay a full price, because they are no longer to be eligible for advance
	 * payments, premium tax credits or extra savings.
	 * </p>
	 */
	@Expose
	private Boolean agreeToEndCoverage = null; // UI doesn't want anything preselected until user picks.

	@Expose
	private String numberOfYearsAgreed = "";
	@Expose
	private Boolean authorizedRepresentativeIndicator = null; // UI doesn't want anything preselected until user picks.
	@Expose
	private Date applicationStartDate;
	@Expose
	private Date applicationDate;
	@Expose
	private List<Integer> taxFilerDependants = new ArrayList<Integer>();
	@Expose
	private Boolean isRidpVerified;
	@Expose
	private String clientIp;
	@Expose
	private Integer primaryTaxFilerPersonId;
	@Expose
	private Broker broker = new Broker();
	@Expose
	private Assister assister = new Assister();
	@Valid
	@Expose
	private List<TaxHousehold> taxHousehold = new ArrayList<TaxHousehold>();
	@Expose
	private Boolean incarcerationAsAttestedIndicator = null;
	
	@Expose
	private String mecCheckStatus;
	
	@Expose
	private Integer jointTaxFilerSpousePersonId;
	
	@Expose
	private Integer currentApplicantId;
	
	@Expose
	private Double ssapVersion;
	
	private String exemptHousehold;
		
	@Expose
	private Boolean brokerChanged = null;
	
	@Expose
	private Boolean renewalConsentChanged = null;
	
	@Expose
	private Boolean incomeConsentChanged = null;
	
	@Expose
	private Boolean isEditApplication = null;

	@Expose
	private boolean manuallyCreated = false;

	public String getAppType()
	{
		return appType;
	}

	public void setAppType(final String appType)
	{
		this.appType = appType;
		if(this.applicationType == null && this.appType != null) {
			setApplicationType(this.appType);
		}
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}

	public int getMaxAchievedPage() {
		return maxAchievedPage;
	}

	public void setMaxAchievedPage(int maxAchievedPage) {
		this.maxAchievedPage = maxAchievedPage;
	}

	public long getCoverageYear() {
		return coverageYear;
	}

	public void setCoverageYear(long coverageYear) {
		this.coverageYear = coverageYear;
	}

	public EligibilityStatus getEligibilityStatus() {
		return eligibilityStatus;
	}

	public void setEligibilityStatus(EligibilityStatus eligibilityStatus) {
		this.eligibilityStatus = eligibilityStatus;
	}

	public String getiP()
	{
		return iP;
	}

	public void setiP(final String iP)
	{
		this.iP = iP;
	}

	public ApplicationStatus getApplicationStatus() {
		return applicationStatus;
	}

	public void setApplicationStatus(ApplicationStatus applicationStatus) {
		this.applicationStatus = applicationStatus;
	}

	public Boolean getRidpVerified()
	{
		return isRidpVerified;
	}

	public void setRidpVerified(final Boolean ridpVerified)
	{
		isRidpVerified = ridpVerified;
	}

	public String getExemptHousehold() {
		return exemptHousehold;
	}

	public void setExemptHousehold(String exemptHousehold) {
		this.exemptHousehold = exemptHousehold;
	}

	public Integer getCurrentApplicantId() {
		return currentApplicantId;
	}

	public void setCurrentApplicantId(Integer currentApplicantId) {
		this.currentApplicantId = currentApplicantId;
	}

	private String externalApplicationId;
	
	private String outboundCaseNumber;

	/**
	 * If true, user agrees to allow Marketplace to use their income data for next 5 years
	 * to determinate eligibility.
	 *
	 * @return boolean value.
	 */
	public Boolean getAgreeToUseIncomeData() {
		return agreeToUseIncomeData;
	}

	/**
	 * Sets boolean flag that allows (or not) to use income data for next 5 years.
	 * @param agreeToUseIncomeData boolean flag
	 */
	public void setAgreeToUseIncomeData(Boolean agreeToUseIncomeData) {
		this.agreeToUseIncomeData = agreeToUseIncomeData;
	}

	/**
	 * Flag that allows automatically to end coverage if anyone on this application if conditions
	 * change.
	 * <p>
	 *   For example
	 * If (true and) anyone in this application after enrollment found to have other
	 * coverage, Marketplace will end their coverage. Otherwise (false), will stay enrolled,
	 * but will pay a full price, because they are no longer to be eligible for advance
	 * payments, premium tax credits or extra savings.
	 * </p>
	 *
	 * @return boolean value.
	 */
	public Boolean getAgreeToEndCoverage() {
		return agreeToEndCoverage;
	}

	/**
	 * Sets boolean flag to allow automatically end coverage for non-qualified
	 * members of this application.
	 * @param agreeToEndCoverage boolean flag.
	 */
	public void setAgreeToEndCoverage(Boolean agreeToEndCoverage) {
		this.agreeToEndCoverage = agreeToEndCoverage;
	}

	public Boolean getConsentAgreement() {
		return consentAgreement;
	}

	public void setConsentAgreement(Boolean consentAgreement) {
		this.consentAgreement = consentAgreement;
	}

	public String getNumberOfYearsAgreed() {
		return numberOfYearsAgreed;
	}

	public void setNumberOfYearsAgreed(String numberOfYearsAgreed) {
		this.numberOfYearsAgreed = numberOfYearsAgreed;
	}

	public String getExternalApplicationId() {
		return externalApplicationId;
	}

	public void setExternalApplicationId(String externalApplicationId) {
		this.externalApplicationId = externalApplicationId;
	}

	public String getOutboundCaseNumber() {
		return outboundCaseNumber;
	}

	public void setOutboundCaseNumber(String outboundCaseNumber) {
		this.outboundCaseNumber = outboundCaseNumber;
	}

	public String getApplicationType() {
		return applicationType;
	}

	public void setApplicationType(String applicationType) {
		this.applicationType = applicationType;
		if(this.appType == null && this.applicationType != null) {
			setAppType(this.applicationType);
		}
	}

	public Boolean getAcceptPrivacyIndicator() {
		return acceptPrivacyIndicator;
	}

	public void setAcceptPrivacyIndicator(Boolean acceptPrivacyIndicator) {
		this.acceptPrivacyIndicator = acceptPrivacyIndicator;
	}

	public String getSsapApplicationId() {
		return ssapApplicationId;
	}

	public void setSsapApplicationId(String ssapApplicationId) {
		this.ssapApplicationId = ssapApplicationId;
	}

	public String getIP() {
		return iP;
	}

	public void setIP(String iP) {
		this.iP = iP;
	}

	public String getApplicationGuid() {
		return applicationGuid;
	}

	public void setApplicationGuid(String applicationGuid) {
		this.applicationGuid = applicationGuid;
	}

	public Date getApplicationSignatureDate() {
		return applicationSignatureDate;
	}

	public void setApplicationSignatureDate(Date applicationSignatureDate) {
		this.applicationSignatureDate = applicationSignatureDate;
	}

	public AuthorizedRepresentative getAuthorizedRepresentative() {
		return authorizedRepresentative;
	}

	public void setAuthorizedRepresentative(AuthorizedRepresentative authorizedRepresentative) {
		this.authorizedRepresentative = authorizedRepresentative;
	}

	public String getApplyingForhouseHold() {
		return applyingForhouseHold;
	}

	public void setApplyingForhouseHold(String applyingForhouseHold) {
		this.applyingForhouseHold = applyingForhouseHold;
	}

	public Boolean getApplyingForFinancialAssistanceIndicator() {
		return applyingForFinancialAssistanceIndicator;
	}

	public void setApplyingForFinancialAssistanceIndicator(Boolean applyingForFinancialAssistanceIndicator) {
		this.applyingForFinancialAssistanceIndicator = applyingForFinancialAssistanceIndicator;
	}

	public Boolean getAuthorizedRepresentativeIndicator() {
		return authorizedRepresentativeIndicator;
	}

	public void setAuthorizedRepresentativeIndicator(Boolean authorizedRepresentativeIndicator) {
		this.authorizedRepresentativeIndicator = authorizedRepresentativeIndicator;
	}

	public Date getApplicationStartDate() {
		return applicationStartDate;
	}

	public void setApplicationStartDate(Date applicationStartDate) {
		this.applicationStartDate = applicationStartDate;
	}

	public Date getApplicationDate() {
		return applicationDate;
	}

	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	public List<Integer> getTaxFilerDependants() {
		return taxFilerDependants;
	}

	public void setTaxFilerDependants(List<Integer> taxFilerDependants) {
		this.taxFilerDependants = taxFilerDependants;
	}

	public Boolean getIsRidpVerified() {
		return isRidpVerified;
	}

	public void setIsRidpVerified(Boolean isRidpVerified) {
		this.isRidpVerified = isRidpVerified;
	}

	public String getClientIp() {
		return clientIp;
	}

	public void setClientIp(String clientIp) {
		this.clientIp = clientIp;
	}

	public Integer getPrimaryTaxFilerPersonId() {
		return primaryTaxFilerPersonId;
	}

	public void setPrimaryTaxFilerPersonId(Integer primaryTaxFilerPersonId) {
		this.primaryTaxFilerPersonId = primaryTaxFilerPersonId;
	}

	public Broker getBroker() {
		return broker;
	}

	public void setBroker(Broker broker) {
		this.broker = broker;
	}

	public Assister getAssister() {
		return assister;
	}

	public void setAssister(Assister assister) {
		this.assister = assister;
	}

	public List<TaxHousehold> getTaxHousehold() {
		return taxHousehold;
	}

	public void setTaxHousehold(List<TaxHousehold> taxHousehold) {
		this.taxHousehold = taxHousehold;
	}

	public Boolean getGetHelpIndicator() {
		return getHelpIndicator;
	}

	public void setGetHelpIndicator(Boolean getHelpIndicator) {
		this.getHelpIndicator = getHelpIndicator;
	}

	public String getHelpType() {
		return helpType;
	}

	public void setHelpType(String helpType) {
		this.helpType = helpType;
	}
	
	public Boolean getIncarcerationAsAttestedIndicator() {
		return incarcerationAsAttestedIndicator;
	}

	public void setIncarcerationAsAttestedIndicator(
			Boolean incarcerationAsAttestedIndicator) {
		this.incarcerationAsAttestedIndicator = incarcerationAsAttestedIndicator;
	}

	public String getMecCheckStatus() {
		return mecCheckStatus;
	}

	public void setMecCheckStatus(String mecCheckStatus) {
		this.mecCheckStatus = mecCheckStatus;
	}

	public Integer getJointTaxFilerSpousePersonId() {
		return jointTaxFilerSpousePersonId;
	}

	public void setJointTaxFilerSpousePersonId(Integer jointTaxFilerSpousePersonId) {
		this.jointTaxFilerSpousePersonId = jointTaxFilerSpousePersonId;
	}

	public Double getSsapVersion() {
		return ssapVersion;
	}

	public void setSsapVersion(Double ssapVersion) {
		this.ssapVersion = ssapVersion;
	}

	public boolean isFinalized() {
		return finalized;
	}

	public void setFinalized(boolean finalized) {
		this.finalized = finalized;
	}

	public Boolean getBrokerChanged() {
		return brokerChanged;
	}

	public void setBrokerChanged(Boolean brokerChanged) {
		this.brokerChanged = brokerChanged;
	}

	public Boolean getRenewalConsentChanged() {
		return renewalConsentChanged;
	}

	public void setRenewalConsentChanged(Boolean renewalConsentChanged) {
		this.renewalConsentChanged = renewalConsentChanged;
	}

	public Boolean getIncomeConsentChanged() {
		return incomeConsentChanged;
	}

	public void setIncomeConsentChanged(Boolean incomeConsentChanged) {
		this.incomeConsentChanged = incomeConsentChanged;
	}

	public Boolean getIsEditApplication() {
		return isEditApplication;
	}

	public void setIsEditApplication(Boolean isEditApplication) {
		this.isEditApplication = isEditApplication;
	}

	/**
	 * Flag that is set to true if original SSAP created via SSAP flow (online).
	 * @return true if this originally created by the user by clicking 'Start 20xx Application' on dashboard.
	 */
	public boolean isManuallyCreated() {
		return manuallyCreated;
	}

	public void setManuallyCreated(boolean manuallyCreated) {
		this.manuallyCreated = manuallyCreated;
	}
}
