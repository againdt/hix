package com.getinsured.hix.batch.enrollment.external.nv.processor;

import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ParseException;
import org.springframework.util.StringUtils;

import com.getinsured.hix.batch.enrollment.external.nv.dto.CoveredInsuredMembers;
import com.getinsured.hix.batch.enrollment.external.nv.dto.EnrollmentJsonDTO;
import com.getinsured.hix.batch.enrollment.external.nv.dto.HouseholdEnrollments;
import com.getinsured.hix.dto.enrollment.ExternalEnrollmentCsvDTO;
import com.getinsured.hix.model.enrollment.ExternalEnrollment;
import com.getinsured.hix.platform.util.DateUtil;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;

public class ExternalEnrollmentItemProcessor implements ItemProcessor<String, HouseholdEnrollments>{
 
	private static final Logger LOGGER = LoggerFactory.getLogger(ExternalEnrollmentItemProcessor.class);
	private Gson mapper = new Gson();
	private static final String ENROLLMENT_START = "{\"enrollments\":[";
	private static final Map<String,String> CS_VALUES = new HashMap<String,String>();
	
	static {
		CS_VALUES.put(null,"01");
		CS_VALUES.put("ZERO_COST_SHARING_PLAN_VARIATION", "02");
		CS_VALUES.put("LIMITED_COST_SHARING_PLAN_VARIATION", "03");
		CS_VALUES.put("73PCT_AV_LEVEL_SILVER_PLAN_CSR","04");
		CS_VALUES.put("87PCT_AV_LEVEL_SILVER_PLAN_CSR","05");
		CS_VALUES.put("94PCT_AV_LEVEL_SILVER_PLAN_CSR","06");
	}
	
    public HouseholdEnrollments process(String result) throws Exception {
    	//System.out.println("json"+result);
    	LOGGER.debug("Processing Line : "+ result);
        HouseholdEnrollments householdEnrollment = new HouseholdEnrollments();
    	List<ExternalEnrollmentCsvDTO> enrollments = new ArrayList<ExternalEnrollmentCsvDTO>(2);
    	List<String> parseErrors = new ArrayList<String>(1);
    	householdEnrollment.setParsingErrors(parseErrors);
    	householdEnrollment.setEnrollments(enrollments);
    	Map<String,ExternalEnrollmentCsvDTO> existingEnrollments = new HashMap<String, ExternalEnrollmentCsvDTO>();
		try {
			householdEnrollment.setJsonLine(result);
			
			if (StringUtils.startsWithIgnoreCase(result, ENROLLMENT_START)) {
				result = result.substring(ENROLLMENT_START.length(), result.length());
			}
			
			JsonReader jsonReader = mapper
					.newJsonReader(new InputStreamReader(new ByteArrayInputStream(result.getBytes())));
			jsonReader.beginArray();
			while (jsonReader.hasNext()) {
				EnrollmentJsonDTO obj = mapper.fromJson(jsonReader, EnrollmentJsonDTO.class);
				
				enrollments.addAll(createExternalEnrl(obj,existingEnrollments));
			}
		} catch (Exception e) {
	    	LOGGER.error("Error in Processing JSON Line : "+ result, e);
			//throw new ParseException("Unable to read next JSON object", e);
	    	enrollments.clear();
	    	householdEnrollment.setHasErrors(true);
	    	parseErrors.add("Error parsing line :"+result +" EXCEPTION:"+e.getMessage());
		}
    	return householdEnrollment;
    }
 
    private List<ExternalEnrollmentCsvDTO> createExternalEnrl(EnrollmentJsonDTO dto, Map<String, ExternalEnrollmentCsvDTO> existingEnrollments)
    {
    	List<ExternalEnrollmentCsvDTO> enrollments = new ArrayList<ExternalEnrollmentCsvDTO>();
    	if(isActive(dto))
    	{
    		ExternalEnrollmentCsvDTO enrollment = null;
    		for(CoveredInsuredMembers member : dto.getCoveredInsuredMembers()) 
    		{
    			enrollment = existingEnrollments.get(member.getPersonTrackingNumber());
    			if(enrollment == null)
    			{
    				enrollment = new ExternalEnrollmentCsvDTO();
			    	enrollment.setAgentNpn(null);
			    	enrollment.setApplicantDob(member.getMemberInfo().getBirthDate().replaceAll("-",""));
			    	enrollment.setApplicantExternalId(member.getPersonTrackingNumber());
			    	enrollment.setApplicantFirstName(member.getMemberInfo().getFirstName());
			    	enrollment.setApplicantLastName(member.getMemberInfo().getLastName());
			    	//enrollment.setApplicationId(Integer.toString(dto.getInsuranceApplicationIdentifier()));
			    	String tobaccoIndicator = null;
			    	if("TOBACCO_USED".equalsIgnoreCase(member.getIdentifyingTobaccoUseTypeCodeName()))
			    	{
			    		tobaccoIndicator = "Y";
			    	}
			    	else if("TOBACCO_NOT_USED".equalsIgnoreCase(member.getIdentifyingTobaccoUseTypeCodeName()))
			    	{
			    		tobaccoIndicator = "N";
			    	}
			    	enrollment.setTobaccoIndicator(tobaccoIndicator);
			    	enrollment.setApplicationExternalId(dto.getInsuranceApplicationIdentifier().toString());
			    	enrollments.add(enrollment);
			    	existingEnrollments.put(member.getPersonTrackingNumber(),enrollment);
    			}
		    	if("DENTAL".equalsIgnoreCase(dto.getAssociatedProductDivisionReferenceTypeCodeName()))
		    	{
		    		enrollment.setDentalExchangeAssignedPolicyId(Integer.toString(dto.getMarketplaceGroupPolicyIdentifier()));
		    		enrollment.setDentalPlanId(dto.getSelectedInsurancePlan());
		    		if(dto.getDefiningPlanVariantComponentTypeCodeName() != null)
		    		{
		    			String csLevel = CS_VALUES.get(dto.getDefiningPlanVariantComponentTypeCodeName());
		    			if(StringUtils.isEmpty(csLevel))
		    			{
		    				csLevel="01";
		    			}
		    			if(!StringUtils.isEmpty(csLevel) && enrollment.getDentalPlanId().length() == 14)
		    			{
		    				enrollment.setDentalPlanId(enrollment.getDentalPlanId()+csLevel); 
		    			}
		    		}
		    	}else
		    	{
		    		enrollment.setHealthExchangeAssignedPolicyId(Integer.toString(dto.getMarketplaceGroupPolicyIdentifier()));
		    		enrollment.setHealthPlanId(dto.getSelectedInsurancePlan());
		    		if(dto.getDefiningPlanVariantComponentTypeCodeName() != null)
		    		{
		    			String csLevel = CS_VALUES.get(dto.getDefiningPlanVariantComponentTypeCodeName());
		    			if(StringUtils.isEmpty(csLevel))
		    			{
		    				csLevel="01";
		    			}
		    			if(enrollment.getHealthPlanId().length() == 14)
		    			{
		    				enrollment.setHealthPlanId(enrollment.getHealthPlanId()+csLevel); 
		    			}
		    		}
		    	}
    		}
	    }
    	return enrollments;
    }
    
    private boolean isActive(EnrollmentJsonDTO dto) {
		if("ACTIVE_OR_TERMINATED".equalsIgnoreCase(dto.getInsurancePolicyStatus().getDefiningInsurancePolicyStatusTypeCodeName())
				&& "2019-12-31".equalsIgnoreCase(dto.getInsurancePlanPolicyEndDate()) 
				&& (!"true".equalsIgnoreCase(dto.getSpecifiedEOYEndDateIndicator()))
				&& dto.getSupersededIndicator() == false) {
			return true;
		}
		return false;
	}
}
