package com.getinsured.iex.hub.qac.wrapper;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.XMLGregorianCalendar;

import org.json.simple.JSONArray;
import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.utils.PlatformServiceUtil;
import com.getinsured.iex.hub.qac.cms.hix._0_1.hix_core.PersonType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.EnrollmentDataType;
import com.getinsured.iex.hub.qac.hhs.cms.dsh.sim.ee.vqac.extension._1.InsuranceApplicantType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateRangeType;
import com.getinsured.iex.hub.qac.niem.niem.niem_core._2.DateType;
import com.getinsured.iex.hub.qac.niem.niem.proxy.xsd._2.Date;

public class InsuranceApplicantResponseWrapper implements JSONAware {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(InsuranceApplicantResponseWrapper.class);
	
	private PersonWrapper person;
	private String coverageStartDate;
	private String coverageEndDate;
	private List<EnrollmentDataWrapper> enrollmentDataList;
	
	
	private String getDateString(DateType dateType){
		String dateStr = null;
		
		if(dateType != null){
			Date xsdDate = (Date) dateType.getDateRepresentation().getValue();
			XMLGregorianCalendar xmlDate = xsdDate.getValue();
			try {
				dateStr = PlatformServiceUtil.xmlDateToString(xmlDate, "yyyy-MM-dd");
			} catch (ParseException e) {
				LOGGER.info("Invalid date",e);
			} catch (DatatypeConfigurationException e) {
				LOGGER.info("Invalid date",e);
			}
		}
		return dateStr;
	}
	public InsuranceApplicantResponseWrapper(InsuranceApplicantType applicant) {
		if (applicant != null) {
			PersonType hubPersonRoleRef = applicant.getRolePlayedByPerson();
			this.person = new PersonWrapper(hubPersonRoleRef);

			DateRangeType hubCoverageRange = applicant.getInsuranceApplicantRequestedCoverage();
			this.coverageStartDate = getDateString(hubCoverageRange.getStartDate());
			this.coverageEndDate =  getDateString(hubCoverageRange.getEndDate());
			List<EnrollmentDataType> hubEnrollmentData = applicant.getEnrollmentData();
			this.setEnrollmentData(hubEnrollmentData);
		}
	}	
	private void setEnrollmentData(List<EnrollmentDataType> hubEnrollmentDataList){
		if(hubEnrollmentDataList != null){
			EnrollmentDataWrapper tmp = null;
			this.enrollmentDataList = new ArrayList<EnrollmentDataWrapper>();
			for(EnrollmentDataType hubData: hubEnrollmentDataList){
				tmp = new EnrollmentDataWrapper(hubData);
				this.enrollmentDataList.add(tmp);
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put("coverageStartDate", this.coverageStartDate);
		obj.put("coverageEndDate", this.coverageEndDate);
		obj.put("Person", person);
		JSONArray enrollmentData = null;
		
		if(enrollmentDataList != null && enrollmentDataList.size() > 0){
			enrollmentData = new JSONArray();
		}
		
		for(EnrollmentDataWrapper enrollmentDataWrapper : enrollmentDataList){
			enrollmentData.add(enrollmentDataWrapper);
		}
		
		if(enrollmentData != null){
			obj.put("EnrollmentData", enrollmentData);
		}
		return obj.toJSONString();
		
	}

}
