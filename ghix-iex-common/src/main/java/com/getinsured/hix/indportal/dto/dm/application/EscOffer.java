package com.getinsured.hix.indportal.dto.dm.application;

import java.math.BigDecimal;

public class EscOffer {
	 private Boolean cobraAvailableIndicator;
	 private Boolean doNotKnowLcsopPremiumIndicator;
	 private BigDecimal lcsopPremium;
	 private String lcsopPremiumFrequencyType;
	 private String employerOffersMinValuePlan;
	 private Boolean planToEnrollEscIndicator;
	 private Employer employer;
	 private Boolean employerWillChangeLcsopPremiumIndicator;
	 private Boolean employerWillNoLongerOfferLcsopIndicator;
	 private String employeeStatus;
	 private Boolean escEnrolledIndicator;
	 private BigDecimal newEmployeeLcsopPremiumAmount;
	 private Boolean retireePlanCoverageIndicator;
	 private Boolean waitingPeriodIndicator;
	 private String primaryInsuredMemberIdentifier;


	 // Getter Methods 

	 public Boolean getCobraAvailableIndicator() {
	  return cobraAvailableIndicator;
	 }

	 public Boolean getDoNotKnowLcsopPremiumIndicator() {
	  return doNotKnowLcsopPremiumIndicator;
	 }

	 public BigDecimal getLcsopPremium() {
	  return lcsopPremium;
	 }

	 public String getLcsopPremiumFrequencyType() {
	  return lcsopPremiumFrequencyType;
	 }

	 public String getEmployerOffersMinValuePlan() {
	  return employerOffersMinValuePlan;
	 }

	 public Boolean getPlanToEnrollEscIndicator() {
	  return planToEnrollEscIndicator;
	 }

	 public Employer getEmployer() {
	  return employer;
	 }

	 public Boolean getEmployerWillChangeLcsopPremiumIndicator() {
	  return employerWillChangeLcsopPremiumIndicator;
	 }

	 public Boolean getEmployerWillNoLongerOfferLcsopIndicator() {
	  return employerWillNoLongerOfferLcsopIndicator;
	 }

	 public String getEmployeeStatus() {
	  return employeeStatus;
	 }

	 public Boolean getEscEnrolledIndicator() {
	  return escEnrolledIndicator;
	 }

	 public Boolean getRetireePlanCoverageIndicator() {
	  return retireePlanCoverageIndicator;
	 }

	 public Boolean getWaitingPeriodIndicator() {
	  return waitingPeriodIndicator;
	 }

	 public String getPrimaryInsuredMemberIdentifier() {
	  return primaryInsuredMemberIdentifier;
	 }

	 // Setter Methods 

	 public void setCobraAvailableIndicator(Boolean cobraAvailableIndicator) {
	  this.cobraAvailableIndicator = cobraAvailableIndicator;
	 }

	 public void setDoNotKnowLcsopPremiumIndicator(Boolean doNotKnowLcsopPremiumIndicator) {
	  this.doNotKnowLcsopPremiumIndicator = doNotKnowLcsopPremiumIndicator;
	 }

	 public void setLcsopPremium(BigDecimal lcsopPremium) {
	  this.lcsopPremium = lcsopPremium;
	 }

	 public void setLcsopPremiumFrequencyType(String lcsopPremiumFrequencyType) {
	  this.lcsopPremiumFrequencyType = lcsopPremiumFrequencyType;
	 }

	 public void setEmployerOffersMinValuePlan(String employerOffersMinValuePlan) {
	  this.employerOffersMinValuePlan = employerOffersMinValuePlan;
	 }

	 public void setPlanToEnrollEscIndicator(Boolean planToEnrollEscIndicator) {
	  this.planToEnrollEscIndicator = planToEnrollEscIndicator;
	 }

	 public void setEmployer(Employer employerObject) {
	  this.employer = employerObject;
	 }

	 public void setEmployerWillChangeLcsopPremiumIndicator(Boolean employerWillChangeLcsopPremiumIndicator) {
	  this.employerWillChangeLcsopPremiumIndicator = employerWillChangeLcsopPremiumIndicator;
	 }

	 public void setEmployerWillNoLongerOfferLcsopIndicator(Boolean employerWillNoLongerOfferLcsopIndicator) {
	  this.employerWillNoLongerOfferLcsopIndicator = employerWillNoLongerOfferLcsopIndicator;
	 }

	 public void setEmployeeStatus(String employeeStatus) {
	  this.employeeStatus = employeeStatus;
	 }

	 public void setEscEnrolledIndicator(Boolean escEnrolledIndicator) {
	  this.escEnrolledIndicator = escEnrolledIndicator;
	 }

	 public void setRetireePlanCoverageIndicator(Boolean retireePlanCoverageIndicator) {
	  this.retireePlanCoverageIndicator = retireePlanCoverageIndicator;
	 }

	 public void setWaitingPeriodIndicator(Boolean waitingPeriodIndicator) {
	  this.waitingPeriodIndicator = waitingPeriodIndicator;
	 }

	 public void setPrimaryInsuredMemberIdentifier(String primaryInsuredMemberIdentifier) {
	  this.primaryInsuredMemberIdentifier = primaryInsuredMemberIdentifier;
	 }

	public BigDecimal getNewEmployeeLcsopPremiumAmount() {
		return newEmployeeLcsopPremiumAmount;
	}

	public void setNewEmployeeLcsopPremiumAmount(BigDecimal newEmployeeLcsopPremiumAmount) {
		this.newEmployeeLcsopPremiumAmount = newEmployeeLcsopPremiumAmount;
	}
	}
