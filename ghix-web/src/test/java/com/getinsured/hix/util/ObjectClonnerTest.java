package com.getinsured.hix.util;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.hix.BaseTest;
import com.getinsured.hix.model.InboxMsg;
import com.getinsured.hix.platform.util.ObjectClonner;
import com.getinsured.hix.platform.util.exception.GIException;

public class ObjectClonnerTest extends BaseTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ObjectClonnerTest.class);
	
	@Test
	public void testClonning() throws GIException{
		
		//Testing Map
		Map<String, String> testMap = new HashMap<String, String>();
		ObjectClonner cloner = new ObjectClonner();
		testMap.put("1", "One");
		testMap.put("2", "Two");
		testMap.put("3", "Three");
		Map<String, String> clonedMap = (HashMap<String, String>)cloner.clone(testMap);
		//Cloner cloner = new Cloner();
		//Map<String, String> clonedMap = cloner.deepClone(testMap);
		logger.info("Testing map");
		assertTrue(clonedMap != null);
		assertTrue(clonedMap.get("1").equalsIgnoreCase("One") );
		
		
		
		//Testing List
		List<String> testList = new ArrayList<String>();
		testList.add("One");
		testList.add("Two");
		testList.add("Three");
		List<String> clonedList = (List<String>) cloner.clone(testList);
		//List<String> clonedList = cloner.deepClone(testList);
		logger.info("Testing List");
		assertTrue(clonedList != null);
		assertTrue(clonedList.get(0).equalsIgnoreCase("One") );
		
		//Testing Set
		Set<String> testSet = new TreeSet<String>();
		testSet.add("One");
		testSet.add("Two");
		testSet.add("Three");
		Set<String> clonedSet = (Set<String>) cloner.clone(testSet);
		//Set<String> clonedSet = cloner.deepClone(testSet);
		logger.info("Testing Set");
		assertTrue(clonedSet != null);
		assertTrue(clonedSet.contains("One") );
		
		//Testing custom class
		InboxMsg msg = new InboxMsg();
		msg.setOwnerUserId(3);
		msg.setOwnerUserName("Exchange Admin");
		msg.setFromUserId(3);
		msg.setFromUserName("Exchange Admin");
		msg.setToUserIdList("6,7");
		msg.setPriority(InboxMsg.PRIORITY.L);
		InboxMsg clonnedMsg = (InboxMsg) cloner.clone(msg);
		logger.info("Testing custom class");
		assertTrue(clonnedMsg != null);
		assertTrue(clonnedMsg.getOwnerUserId() == 3);
		assertTrue(clonnedMsg.getFromUserName().equalsIgnoreCase("Exchange Admin"));
		assertTrue(clonnedMsg.getPriority() == InboxMsg.PRIORITY.L);
		
	}

}
