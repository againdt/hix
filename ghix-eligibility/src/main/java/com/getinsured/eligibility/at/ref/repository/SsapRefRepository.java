package com.getinsured.eligibility.at.ref.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.platform.accountactivation.repository.IGHIXCustomRepository;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

/**
 * @author chopra_s
 * Need to move this class under shared models
 */
@Repository
@Qualifier("ssapRefRepository")
@Transactional(readOnly = true)
public class SsapRefRepository  implements IGHIXCustomRepository<SsapApplication, Integer>{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SsapRefRepository.class);
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	@Override
	public boolean recordExists(Integer primaryKey) {
		LOGGER.info("Inside SsapRefRepository recordExists method primaryKey - " + primaryKey);
		return ssapApplicationRepository.exists(primaryKey.longValue());
	}

	@Override
	public void updateUser(AccountUser accountUser, Integer primaryKey) {
		LOGGER.info("Inside SsapRefRepository updateUser method primaryKey - " + primaryKey);
	}
}
