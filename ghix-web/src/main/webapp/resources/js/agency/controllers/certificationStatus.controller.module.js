(function() {
'use strict';

angular.module('certificationStatusCtrlModule', [])
.controller('CertificationStatusCtrl', CertificationStatusCtrl);

	CertificationStatusCtrl.$inject = ['$http', '$stateParams', '$state','$scope', '$window', 'fileUpload', 'dataStorageService', '$translate'];

	function CertificationStatusCtrl($http, $stateParams, $state, $scope, $window, fileUpload, dataStorageService, $translate) {
        var vm = this;
        vm.init = init;
        vm.uploadFile = uploadFile;
        vm.updateCertificationStatus = updateCertificationStatus;
		vm.callViewCommentsModal = callViewCommentsModal;
        vm.resetData = resetData;

        function init(){
        	vm.refreshFlag = $stateParams.refreshFlag;

            vm.editMode = false;
            vm.isAdminRole = $('#isAdminRole').val();
            vm.loader = true;
            vm.submitData = {};
            vm.updateCertificationData = {};
            vm.flowFlag = {};
            vm.submittedFile = {
            	contract: '',
            	supportingDocument: '',
            	eoDeclarationPage: ''
            }

            if (vm.refreshFlag){
                $window.location.reload();
                return;
            }
			if(vm.isAdminRole){
				vm.agencyId = dataStorageService.getItem('encryptedAgencyId');
			} else {
				vm.agencyId = $('#agencyId').val() || dataStorageService.getItem('agencyId');
			}

        	if(!vm.agencyId && vm.isAdminRole){
        		$state.go('manageAgencies');
        	}

        	dataStorageService.setItem('agencyId', vm.agencyId)

        	var promise = $http.get('/hix/agency/certificationhistory/' + vm.agencyId);

			promise.success(function(response){
				vm.loader = false;

				vm.agencyCertificationStatusData = response;

		        updateFlowFlag();
			})


			promise.error(function(response){
				vm.loader = false;
			})
        }

		function callViewCommentsModal(comments) {
            $scope.content = comments;
        }

        function uploadFile(file, documentType) {
        	vm.loader = true;

            var fields = [
                {'name': 'agencyId', 'data': vm.agencyId},
                {'name': 'documentType', 'data': documentType}
            ];

            var promise = fileUpload.uploadFileToUrl('document', file, fields, '/hix/agency/document/upload');

            promise.success(function(response){
            	vm.loader = false;

            	var documentTypeForSubmit = getDocumentTypeForSubmit(documentType);

            	if(response.status === 'SUCCESS'){
            		$('#fileUploadSuccessModal').modal();
            		vm.submitData[documentTypeForSubmit] = response.documentId;

            		if(documentType === 'Contract'){
            			vm.updateCertificationData.contract = '';
            			vm.submittedFile.contract = response.documentName;
            		}else if(documentType === 'Supporting'){
            			vm.updateCertificationData.supportingDocument = '';
            			vm.submittedFile.supportingDocument = response.documentName;
            		}else{
            			vm.updateCertificationData.eoDeclarationPage = '';
            			vm.submittedFile.eoDeclarationPage = response.documentName;
            		}
				}
            });

            promise.error(function(response){
            	vm.loader = false;
                console.log(response);
            });
        };


        function getDocumentTypeForSubmit(documentType){
        	switch (documentType){
	        	case 'Contract':
	    			return 'documentId_contract';
	        	case 'Supporting':
	    			return 'documentId_supporting';
	        	case 'E&O Declaration':
	    			return 'documentId_declaration';
        	}


        }


        function updateCertificationStatus(){
        	vm.loader = true;

            if (vm.updateCertificationData.certificationStatus.toUpperCase() !== 'TERMINATED' || (vm.updateCertificationData.certificationStatus.toUpperCase() === 'TERMINATED' && vm.agencyCertificationStatusData.agentCount === 0)) {

            	var url = '/hix/agency/certification?agencyId=' + vm.agencyId +'&certificationStatus=' + vm.updateCertificationData.certificationStatus.toUpperCase();

            	if(vm.updateCertificationData.comment !== undefined){
            		url += '&comment=' + vm.updateCertificationData.comment;
            	}

            	var promise = $http.post(url, $.param(vm.submitData), {headers: {'Content-Type': 'application/x-www-form-urlencoded'}}
            	);
            	//var promise = fileUpload.uploadFileToUrl('', undefined, vm.submitData, '/hix/agency/certification?agencyId=' + vm.agencyId +'&certificationStatus=' + vm.certificationStatus);


            	promise.success(function(response){
            		vm.agencyCertificationStatusData = response;

            		//reset data
            		resetData();

            		vm.loader = false;
            	})

            	promise.error(function(response){
            		vm.loader = false;
            		console.log(response);
            	})
            }
            else {
                $('#updateCertificationStatusModal').modal();
                vm.loader = false;
            }

        }


        function resetData(){
        	vm.updateCertificationData = {}
    		vm.submitData = {};
        	vm.submittedFile = {
            	contract: '',
            	supportingDocument: '',
            	eoDeclarationPage: ''
            }
        	vm.editMode = false;
        }

        function updateFlowFlag(){

        	//for admin flow
        	var isAdminRole = $("#isAdminRole").val();

        	//for agency flow
        	var certificationStatus = $('#agencyCertificationStatus').val();


        	if(isAdminRole === 'true'){
        		if(vm.agencyCertificationStatusData.certificationStatus === 'Incomplete'){
        			vm.flowFlag.adminViewOnlyFlow = true;
        		}else{
        			vm.flowFlag.adminViewEditFlow = true;
        		}
        	}else{

        		if(certificationStatus === '' || certificationStatus === 'INCOMPLETE'){
        			vm.flowFlag.agencyResgisterFlow = true;
        			vm.editMode = true;
        		}else if(certificationStatus === 'CERTIFIED'){
        			vm.flowFlag.agencyViewEditFlow = true;
        		}else{
        			vm.flowFlag.agencyViewOnlyFlow = true;
        		}
        	}
        }


    }

})();
