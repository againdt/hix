package com.getinsured.eligibility.at.resp.service;

import java.util.List;

import com.getinsured.iex.ssap.model.SsapMonitorPayload;

public interface SsapMonitorPayloadService {

	SsapMonitorPayload save(SsapMonitorPayload giwsPayload);
	
	SsapMonitorPayload findById(Integer id);
	
	List<SsapMonitorPayload> findBySSAPID(Long ssapId);
}
