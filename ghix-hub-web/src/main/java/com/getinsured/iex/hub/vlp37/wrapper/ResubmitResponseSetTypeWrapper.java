package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vclpsav.ResubmitResponseSetType;
import com.getinsured.iex.hub.vlp37.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp37.service.VLPServiceUtil;


public class ResubmitResponseSetTypeWrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ResubmitResponseSetTypeWrapper.class);
	
	private String caseNumber;
	private String nonCitLastName;
	private String nonCitFirstName;
	private String nonCitMiddleName;
	private String nonCitBirthDate;
	private String nonCitEntryDate;
	private String nonCitAdmittedToDate;
	private String nonCitAdmittedToText;
	private String nonCitCountryBirthCd;
	private String nonCitCountryCitCd;
	private String nonCitCoaCode;
	private String nonCitProvOfLaw;
	private int eligStatementCd;
	private String eligStatementTxt;
	private String webServSftwrVer;
	private String fiveYearBarApplyCode;
	private String lawfulPresenceVerifiedCode;
	private String qualifiedNonCitizenCode;
	private String fiveYearBarMetCode;
	private String usCitizenCode;
	private ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData;
	    
    public ResubmitResponseSetTypeWrapper(ResubmitResponseSetType resubmitResponseSetType){
    	
    	if (resubmitResponseSetType == null){
    		return;
    	}
    	
    	this.caseNumber = resubmitResponseSetType.getCaseNumber();
    	this.nonCitLastName = resubmitResponseSetType.getNonCitLastName();
    	this.nonCitFirstName = resubmitResponseSetType.getNonCitFirstName();
    	this.nonCitMiddleName = resubmitResponseSetType.getNonCitLastName();
    	try{
	    	if(resubmitResponseSetType.getNonCitBirthDate() != null){
	    		this.nonCitBirthDate = VLPServiceUtil.xmlDateToString(resubmitResponseSetType.getNonCitBirthDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	if(resubmitResponseSetType.getNonCitEntryDate() != null){
	    		this.nonCitEntryDate = VLPServiceUtil.xmlDateToString(resubmitResponseSetType.getNonCitEntryDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}
	    	/*if(resubmitResponseSetType.getNonCitAdmittedToDate() != null){
	    		this.nonCitAdmittedToDate = VLPServiceUtil.xmlDateToString(resubmitResponseSetType.getNonCitAdmittedToDate(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
	    	}*/
	    
    	}
    	catch(Exception e){
    		LOGGER.error("Exception occured while setting date",e);
    	}


    	//this.nonCitAdmittedToText = resubmitResponseSetType.getNonCitAdmittedToText();
    	this.nonCitCountryBirthCd = resubmitResponseSetType.getNonCitCountryBirthCd();
    	this.nonCitCountryCitCd = resubmitResponseSetType.getNonCitCountryCitCd();
    	this.nonCitCoaCode = resubmitResponseSetType.getNonCitCoaCode();
    	this.eligStatementCd = resubmitResponseSetType.getEligStatementCd().intValue();
    	this.eligStatementTxt = resubmitResponseSetType.getEligStatementTxt();
    	this.webServSftwrVer = resubmitResponseSetType.getWebServSftwrVer();
    	this.fiveYearBarApplyCode = resubmitResponseSetType.getFiveYearBarApplyCode();
    	this.qualifiedNonCitizenCode = resubmitResponseSetType.getQualifiedNonCitizenCode();
    	this.fiveYearBarMetCode = resubmitResponseSetType.getFiveYearBarMetCode();
    	this.usCitizenCode = resubmitResponseSetType.getUSCitizenCode();
    	this.nonCitProvOfLaw = resubmitResponseSetType.getNonCitProvOfLaw();
    	this.lawfulPresenceVerifiedCode = resubmitResponseSetType.getLawfulPresenceVerifiedCode();
    }
    
	
    
	public String getCaseNumber() {
		return caseNumber;
	}



	public void setCaseNumber(String caseNumber) {
		this.caseNumber = caseNumber;
	}



	public String getNonCitLastName() {
		return nonCitLastName;
	}



	public void setNonCitLastName(String nonCitLastName) {
		this.nonCitLastName = nonCitLastName;
	}



	public String getNonCitFirstName() {
		return nonCitFirstName;
	}



	public void setNonCitFirstName(String nonCitFirstName) {
		this.nonCitFirstName = nonCitFirstName;
	}



	public String getNonCitMiddleName() {
		return nonCitMiddleName;
	}



	public void setNonCitMiddleName(String nonCitMiddleName) {
		this.nonCitMiddleName = nonCitMiddleName;
	}



	public String getNonCitBirthDate() {
		return nonCitBirthDate;
	}



	public void setNonCitBirthDate(String nonCitBirthDate) {
		this.nonCitBirthDate = nonCitBirthDate;
	}



	public String getNonCitEntryDate() {
		return nonCitEntryDate;
	}



	public void setNonCitEntryDate(String nonCitEntryDate) {
		this.nonCitEntryDate = nonCitEntryDate;
	}



	public String getNonCitAdmittedToDate() {
		return nonCitAdmittedToDate;
	}



	public void setNonCitAdmittedToDate(String nonCitAdmittedToDate) {
		this.nonCitAdmittedToDate = nonCitAdmittedToDate;
	}



	public String getNonCitAdmittedToText() {
		return nonCitAdmittedToText;
	}



	public void setNonCitAdmittedToText(String nonCitAdmittedToText) {
		this.nonCitAdmittedToText = nonCitAdmittedToText;
	}



	public String getNonCitCountryBirthCd() {
		return nonCitCountryBirthCd;
	}



	public void setNonCitCountryBirthCd(String nonCitCountryBirthCd) {
		this.nonCitCountryBirthCd = nonCitCountryBirthCd;
	}



	public String getNonCitCountryCitCd() {
		return nonCitCountryCitCd;
	}



	public void setNonCitCountryCitCd(String nonCitCountryCitCd) {
		this.nonCitCountryCitCd = nonCitCountryCitCd;
	}



	public String getNonCitCoaCode() {
		return nonCitCoaCode;
	}



	public void setNonCitCoaCode(String nonCitCoaCode) {
		this.nonCitCoaCode = nonCitCoaCode;
	}

	public int getEligStatementCd() {
		return eligStatementCd;
	}



	public void setEligStatementCd(int eligStatementCd) {
		this.eligStatementCd = eligStatementCd;
	}



	public String getEligStatementTxt() {
		return eligStatementTxt;
	}



	public void setEligStatementTxt(String eligStatementTxt) {
		this.eligStatementTxt = eligStatementTxt;
	}

	public String getWebServSftwrVer() {
		return webServSftwrVer;
	}

	public void setWebServSftwrVer(String webServSftwrVer) {
		this.webServSftwrVer = webServSftwrVer;
	}


	public String getFiveYearBarApplyCode() {
		return fiveYearBarApplyCode;
	}

	public void setFiveYearBarApplyCode(String fiveYearBarApplyCode) {
		this.fiveYearBarApplyCode = fiveYearBarApplyCode;
	}

	public String getQualifiedNonCitizenCode() {
		return qualifiedNonCitizenCode;
	}



	public void setQualifiedNonCitizenCode(String qualifiedNonCitizenCode) {
		this.qualifiedNonCitizenCode = qualifiedNonCitizenCode;
	}



	public String getFiveYearBarMetCode() {
		return fiveYearBarMetCode;
	}



	public void setFiveYearBarMetCode(String fiveYearBarMetCode) {
		this.fiveYearBarMetCode = fiveYearBarMetCode;
	}



	public String getUsCitizenCode() {
		return usCitizenCode;
	}



	public void setUsCitizenCode(String usCitizenCode) {
		this.usCitizenCode = usCitizenCode;
	}



	public String getNonCitProvOfLaw() {
		return nonCitProvOfLaw;
	}



	public void setNonCitProvOfLaw(String nonCitProvOfLaw) {
		this.nonCitProvOfLaw = nonCitProvOfLaw;
	}



	public String getLawfulPresenceVerifiedCode() {
		return lawfulPresenceVerifiedCode;
	}



	public void setLawfulPresenceVerifiedCode(String lawfulPresenceVerifiedCode) {
		this.lawfulPresenceVerifiedCode = lawfulPresenceVerifiedCode;
	}

	public ArrayOfSponsorshipDataTypeWrapper getArrayOfSponsorshipData() {
		return arrayOfSponsorshipData;
	}



	public void setArrayOfSponsorshipData(
			ArrayOfSponsorshipDataTypeWrapper arrayOfSponsorshipData) {
		this.arrayOfSponsorshipData = arrayOfSponsorshipData;
	}


	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();

		if(this.caseNumber != null){
			obj.put("CaseNumber",this.caseNumber);
		}

		if(this.nonCitLastName != null){
			obj.put("NonCitLastName",this.nonCitLastName);
		}

		if(this.nonCitFirstName != null){
			obj.put("NonCitFirstName",this.nonCitFirstName);
		}

		if(this.nonCitMiddleName != null){
			obj.put("NonCitMiddleName",this.nonCitMiddleName);
		}

		if(this.nonCitBirthDate != null){
			obj.put("NonCitBirthDate",this.nonCitBirthDate);
		}

		if(this.nonCitEntryDate != null){
			obj.put("NonCitEntryDate",this.nonCitEntryDate);
		}

		if(this.nonCitAdmittedToDate != null){
			obj.put("NonCitAdmittedToDate",this.nonCitAdmittedToDate);
		}

		if(this.nonCitAdmittedToText != null){
			obj.put("NonCitAdmittedToText",this.nonCitAdmittedToText);
		}

		if(this.nonCitCountryBirthCd != null){
			obj.put("NonCitCountryBirthCd",this.nonCitCountryBirthCd);
		}

		if(this.nonCitCountryCitCd != null){
			obj.put("NonCitCountryCitCd",this.nonCitCountryCitCd);
		}

		if(this.nonCitCoaCode != null){
			obj.put("NonCitCoaCode",this.nonCitCoaCode);
		}

		obj.put("EligStatementCd",this.eligStatementCd);
	
		if(this.eligStatementTxt != null){
			obj.put("EligStatementTxt",this.eligStatementTxt);
		}

		if(this.webServSftwrVer != null){
			obj.put("WebServSftwrVer",this.webServSftwrVer);
		}

		if(this.fiveYearBarApplyCode != null){
			obj.put("FiveYearBarApplyCode",this.fiveYearBarApplyCode);
		}

		if(this.qualifiedNonCitizenCode != null){
			obj.put("QualifiedNonCitizenCode",this.qualifiedNonCitizenCode);
		}

		if(this.fiveYearBarMetCode != null){
			obj.put("FiveYearBarMetCode",this.fiveYearBarMetCode);
		}

		if(this.usCitizenCode != null){
			obj.put("USCitizenCode",this.usCitizenCode);
		}

		if(this.nonCitProvOfLaw != null){
			obj.put("NonCitProvOfLaw", this.nonCitProvOfLaw);
		}

		if(this.lawfulPresenceVerifiedCode != null){
			obj.put("LawfulPresenceVerifiedCode", this.lawfulPresenceVerifiedCode);
		}

		return obj.toJSONString();
	}

}
