package com.getinsured.eligibility.exception;

public class InvalidIncomingValueException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public InvalidIncomingValueException(String msg) {
		super(msg);
	}

}
