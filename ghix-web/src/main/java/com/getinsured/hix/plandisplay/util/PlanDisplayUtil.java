package com.getinsured.hix.plandisplay.util;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.getinsured.hix.dto.plandisplay.DrugDTO;
import com.getinsured.hix.dto.plandisplay.PdHouseholdDTO;
import com.getinsured.hix.dto.plandisplay.PdPersonDTO;
import com.getinsured.hix.dto.plandisplay.PdPreferencesDTO;
import com.getinsured.hix.dto.plandisplay.PlanDTO;
import com.getinsured.hix.dto.shop.mlec.Member;
import com.getinsured.hix.dto.shop.mlec.Member.Relationship;
import com.getinsured.hix.model.GroupList;
import com.getinsured.hix.model.IndividualPlan;
import com.getinsured.hix.model.Plan;
import com.getinsured.hix.model.Plan.PlanInsuranceType;
import com.getinsured.hix.model.PldHousehold;
import com.getinsured.hix.model.PldHousehold.HouseholdType;
import com.getinsured.hix.model.PldHousehold.ProgramType;
import com.getinsured.hix.model.Role;
import com.getinsured.hix.model.consumer.FFMHouseholdResponse;
import com.getinsured.hix.model.enrollment.EnrollmentResponse;
import com.getinsured.hix.model.estimator.mini.EligLead;
import com.getinsured.hix.model.estimator.mini.EligLead.ExchangeFlowType;
import com.getinsured.hix.model.estimator.mini.MiniEstimatorResponse;
import com.getinsured.hix.model.plandisplay.ApplicantEligibilityData;
import com.getinsured.hix.model.plandisplay.GroupData;
import com.getinsured.hix.model.plandisplay.IndividualHousehold;
import com.getinsured.hix.model.plandisplay.IndividualRequest;
import com.getinsured.hix.model.plandisplay.IndividualResponse;
import com.getinsured.hix.model.plandisplay.PdSavePrefRequest;
import com.getinsured.hix.model.plandisplay.PersonData;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentFlowType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.EnrollmentType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.InsuranceType;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.LogLevel;
import com.getinsured.hix.model.plandisplay.PlanDisplayEnum.YorN;
import com.getinsured.hix.model.plandisplay.PremiumTaxCreditRequest;
import com.getinsured.hix.model.plandisplay.PremiumTaxCreditResponse;
import com.getinsured.hix.model.plandisplay.SessionData;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.service.RoleService;
import com.getinsured.hix.platform.util.DateUtil;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.GhixEndPoints.PlandisplayEndpoints;
import com.getinsured.hix.platform.util.GhixUtils;
import com.getinsured.hix.platform.util.JacksonUtils;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.timeshift.TSCalendar;
import com.getinsured.timeshift.TimeShifterUtil;
import com.google.gson.Gson;


/**
 * @author atulz
 *
 */

@Component
public class PlanDisplayUtil {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PlanDisplayUtil.class);
		
	@Autowired private GhixRestTemplate ghixRestTemplate;
	
	@Autowired private PlanDisplayRestClient planDisplayRestClient;
	
	@Autowired private Gson platformGson;
	
	public static final String PARAM_CURRENT_GRP_ID  = "CURRENT_GROUP_ID";
	public static final String PARAM_DOB = "DOB";
	public static final String PARAM_FIRST_NAME = "FIRSTNAME";
	public static final String PARAM_HOUSEHOLD_ID = "HOUSEHOLD_ID";
	public static final String PARAM_LAST_NAME = "LASTNAME";
	public static final String PARAM_TYPE = "TYPE";
	public static final String PARAM_ZIPCODE = "ZIPCODE";
	public static final String PARAM_RELATIONSHIP = "RELATIONSHIP";
	public static final String PARAM_SMOKER = "SMOKER";
	public static final String PARAMPERSONFIRSTNAME = "personFirstName";
	public static final String PARAMPERSONLASTNAME = "personLastName";
	public static final String PARAM_COUNTY_CODE = "countyCode";
	private static final String  QUOTING_GROUP_FAMILY="FAMILY";
	private static final String  QUOTING_GROUP_CHILD="CHILD";
	private static final String  QUOTING_GROUP_ADULT="ADULT";
	private static final String PROGRAM_TYPE_SHOP="SHOP";
	private static final String PROGRAM_TYPE_INDIVIDUAL="INDIVIDUAL";
//	private final String MISSING_DENTAL_ADULT ="Adult";
//	private final String MISSING_DENTAL_CHILD ="Child";
//	private final String MISSING_DENTAL_ADULT_CHILD="AdultChild" ;
	private static final String TWENTY_TWO = "22";
	private static final String DATE_FORMAT = "MM/dd/yyyy";
	public PlanDisplayUtil() {
		// TODO Auto-generated constructor stub
	}	
	
	public static Date getCoverageStartDate(String quotingType, String coverageDate){
		Date coverageStart = null;
		DateFormat formatter1 = new SimpleDateFormat("MM/dd/yy");
		if("INDIVIDUAL".equals(quotingType)){
			try {
				coverageStart = formatter1.parse(coverageDate);
			} catch (ParseException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, "<-------->", e, false);
			}
		}else{
			//calculating the coverage date
			Calendar cal = TSCalendar.getInstance();
			
			coverageStart = cal.getTime();
			
			cal.clear();
			cal.set(2014, Calendar.DECEMBER, 15);
			Date date = cal.getTime();
			
			//current date is on or after 1/1/2014 then coverage start date is the first of the next month
			if(coverageStart.compareTo(date)>=0){
				cal.clear();
				cal.setTime(coverageStart);
				cal.add(cal.MONTH, 1);
				cal.set(cal.DAY_OF_MONTH, 1);
				Date newCoverageDate = cal.getTime();
				
				long divisor = 1000*60*60*24;
				long difference = (newCoverageDate.getTime() - coverageStart.getTime())/divisor;
				
				//calculated date is less than 15 days away then coverage start date is the first of the following month of calculated date.
				if(difference<=15){
					cal.add(cal.MONTH, 1);
					coverageStart = cal.getTime();
				}else{//coverage date is equal to the calculated date
					coverageStart = newCoverageDate;
				}
						
			}
			else{ //current date is prior to 1/1/2014, then assume Coverage start date is 1/1/2014
				cal.clear();
				cal.set(2015, Calendar.JANUARY, 1);
				coverageStart = cal.getTime();
			}
		}
		return coverageStart;
	}
	
    public static Date getCoverageStartDateDayBased(String quotingType, String coverageDate){
		Date coverageStart = null;
		DateFormat formatter1 = new SimpleDateFormat("MM/dd/yy");
		if("INDIVIDUAL".equals(quotingType)){
			try {
				coverageStart = formatter1.parse(coverageDate);
			} catch (ParseException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, "<-------->", e, false);
			}
		}else{
			//calculating the coverage date
			Calendar cal = TSCalendar.getInstance();
			coverageStart = cal.getTime();
			
			cal.clear();
			cal.set(2014, Calendar.DECEMBER, 15);
			Date date = cal.getTime();
			
			//current date is on or after 1/1/2014 then coverage start date is the first of the next month
			if(coverageStart.compareTo(date)>=0){
				cal.clear();
				cal.setTime(coverageStart);
				int initialDayOfMonth = cal.get(cal.DAY_OF_MONTH);
				cal.add(cal.MONTH, 1);
				cal.set(cal.DAY_OF_MONTH, 1);
				Date newCoverageDate = cal.getTime();
				//calculated date is less than 15 days away then coverage start date is the first of the following month of calculated date.
				if(initialDayOfMonth>15){
					cal.add(cal.MONTH, 1);
					coverageStart = cal.getTime();
				}else{//coverage date is equal to the calculated date
					coverageStart = newCoverageDate;
				}
						
			}
			else{ //current date is prior to 1/1/2014, then assume Coverage start date is 1/1/2014
				cal.clear();
				cal.set(2015, Calendar.JANUARY, 1);
				coverageStart = cal.getTime();
			}
		}
		return coverageStart;
	}
	
	public double calculateAdvancedPremiumTaxCredit(String coverageStartDate, String zip, List<Integer> ages, List<String> coverage,
			double householdIncome, int householdSize){
		
		double maxAPTC = 0.0;
		
		PremiumTaxCreditResponse response = getAPTC(coverageStartDate, zip, ages, coverage, householdIncome, householdSize);
		
		if (response == null) {
			return 0.0;
		}

		maxAPTC = response.getMaxAPTC();
		
		//LOGGER.info("Maximum Monthly Advanced Premium Tax Credit : " + maxAPTC);
		
		return maxAPTC;
	}

	private  PremiumTaxCreditResponse getAPTC(String coverageStartDate, String zip, List<Integer> ages, List<String> coverage,
			double householdIncome, int householdSize){
		
		PremiumTaxCreditRequest request = new PremiumTaxCreditRequest();
		request.setCoverageStartDate(coverageStartDate);
		request.setZip(zip);
		request.setAges(ages);
		request.setCoverage(coverage);
		request.setHouseholdIncome(householdIncome);
		request.setHouseholdSize(householdSize);		
		
		String postParams = null;
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(PremiumTaxCreditRequest.class);
			postParams = writer.writeValueAsString(request);
		} catch (JsonParseException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
				
		//LOGGER.info("Calculating Advanced Premium Tax Credit : " + postParams);
		
		String response = null;
		
		try {
			//response = hixHTTPClient.getPOSTData(PlandisplayEndpoints.CALCULATE_TAX_CREDIT_URL, postParams, PlanDisplayConstants.CONTENT_TYPE);
			response = ghixRestTemplate.postForObject(PlandisplayEndpoints.CALCULATE_TAX_CREDIT_URL, postParams,String.class);
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
				
		if(response != null){
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(PremiumTaxCreditResponse.class);
				return objectReader.readValue(response);
			} catch (JsonParseException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		}
		else{
			return null;
		}
	}
	
	
	public String getHouseholdId(IndividualRequest individualRequest){
		String postParams = null;
		
		try {
			ObjectWriter writer = JacksonUtils.getJacksonObjectWriterForJavaType(IndividualRequest.class);
			postParams = writer.writeValueAsString(individualRequest);
		} catch (JsonParseException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (JsonMappingException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		} catch (IOException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
			return null;
		}
				
		//LOGGER.info("Getting Household ID" + postParams);
		String response = null;
		
		try {
			//response = hixHTTPClient.getPOSTData(PlandisplayEndpoints.INDIVIDUAL_INFORMATION, postParams, PlanDisplayConstants.CONTENT_TYPE);
			response = ghixRestTemplate.postForObject(PlandisplayEndpoints.INDIVIDUAL_INFORMATION, postParams,String.class);
			
		} catch (Exception e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
		}
				
		if(response != null){
			try {
				ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(IndividualResponse.class);
				IndividualResponse individualResponse = objectReader.readValue(response);
				return individualResponse.getGiHouseholdId();
			} catch (JsonParseException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (JsonMappingException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			} catch (IOException e) {
				putLoggerStmt(LOGGER, LogLevel.ERROR, e.getMessage(), e, false);
				return null;
			}
		}
		else{
			return null;
		}
	}
	
	public PlanDTO createPlanDTO(Plan plan){
		PlanDTO planDTO = new PlanDTO();
		
//		IssuerDTO issuerDTO = new IssuerDTO();
//		PlanHealthDTO planHealthDTO = new PlanHealthDTO();
//		
//		PlanHealthBenefitDTO planHealthBenefitDTO = null;
//		PlanHealthCostDTO planHealthCostDTO = null;
//		
//		PlanHealthServiceRegionDTO planHealthServiceRegionDTO = null;
//		PlanHealthRateDTO planHealthRateDTO = null;
//		PlanDentalDTO planDentalDTO = new PlanDentalDTO();
//		PlanDentalBenefitDTO planDentalBenefitDTO = null;
//		PlanDentalServiceRegionDTO planDentalServiceRegionDTO = null;
//		PlanDentalRateDTO planDentalRateDTO = null;
//		
		
		try{
			//createDTOBean.copyProperties(planDTO, plan);
//			createDTOBean.copyProperties(issuerDTO, plan.getIssuer());
//			planDTO.setIssuer(issuerDTO);
//						
//			if(plan.getInsuranceType().toString().equals(PlanInsuranceType.HEALTH.toString())){
//				createDTOBean.copyProperties(planHealthDTO, plan.getPlanHealth());
//				planDTO.setPlanHealth(planHealthDTO);
//						
//				Map<String, PlanHealthBenefit> planHealthBenefitMap = plan.getPlanHealth().getPlanHealthBenefits();			
//				planHealthDTO.setPlanHealthBenefits(new HashMap<String, PlanHealthBenefit>());						
//				for(Map.Entry<String, PlanHealthBenefit> entry:planHealthBenefitMap.entrySet()){
//					PlanHealthBenefit planHealthBenefit = entry.getValue();
//					planHealthBenefitDTO = new PlanHealthBenefitDTO();
//					createDTOBean.copyProperties(planHealthBenefitDTO, planHealthBenefit);
//					planHealthDTO.getPlanHealthBenefits().put(entry.getKey(), planHealthBenefitDTO);				
//				}
//				
//				Map<String, PlanHealthCost> planHealthCostMap = plan.getPlanHealth().getPlanHealthCosts();			
//				planHealthDTO.setPlanHealthCosts(new HashMap<String, PlanHealthCost>());						
//				for(Map.Entry<String, PlanHealthCost> entry:planHealthCostMap.entrySet()){
//					PlanHealthCost planHealthCost = entry.getValue();
//					planHealthCostDTO = new PlanHealthCostDTO();
//					createDTOBean.copyProperties(planHealthCostDTO, planHealthCost);
//					planHealthDTO.getPlanHealthCosts().put(entry.getKey(), planHealthCostDTO);				
//				}
				
				/*List<PlanHealthServiceRegion> planHealthServiceRegionList = plan.getPlanHealth().getPlanHealthServiceRegion();			
				planHealthDTO.setPlanHealthServiceRegion(new ArrayList<PlanHealthServiceRegion>());			
				for(PlanHealthServiceRegion planHealthServiceRegion:planHealthServiceRegionList){
					planHealthServiceRegionDTO = new PlanHealthServiceRegionDTO();
					createDTOBean.copyProperties(planHealthServiceRegionDTO, planHealthServiceRegion);
					
					List<PlanHealthRate> planHealthRateList = planHealthServiceRegion.getPlanHealthRates();
					planHealthServiceRegionDTO.setPlanHealthRates(new ArrayList<PlanHealthRate>());
					for(PlanHealthRate planHealthRate:planHealthRateList){
						planHealthRateDTO = new PlanHealthRateDTO();
						createDTOBean.copyProperties(planHealthRateDTO, planHealthRate);
						planHealthServiceRegionDTO.getPlanHealthRates().add(planHealthRateDTO);
					}				
					planHealthDTO.getPlanHealthServiceRegion().add(planHealthServiceRegionDTO);				
				}*/
//			}
//			
//			if(plan.getInsuranceType().toString().equals(PlanInsuranceType.DENTAL.toString())){
//				createDTOBean.copyProperties(planDentalDTO, plan.getPlanDental());
//				planDTO.setPlanDental(planDentalDTO);			
//			
//				Map<String, PlanDentalBenefit> planDentalBenefitMap = plan.getPlanDental().getPlanDentalBenefits();			
//				planDentalDTO.setPlanDentalBenefits(new HashMap<String, PlanDentalBenefit>());						
//				for(Map.Entry<String, PlanDentalBenefit> entry:planDentalBenefitMap.entrySet()){
//					PlanDentalBenefit planDentalBenefit = entry.getValue();
//					planDentalBenefitDTO = new PlanDentalBenefitDTO();
//					createDTOBean.copyProperties(planDentalBenefitDTO, planDentalBenefit);
//					planDentalDTO.getPlanDentalBenefits().put(entry.getKey(), planDentalBenefitDTO);				
//				}
				
				/*List<PlanDentalServiceRegion> planDentalServiceRegionList = plan.getPlanDental().getPlanDentalServiceRegion();			
				planDentalDTO.setPlanDentalServiceRegion(new ArrayList<PlanDentalServiceRegion>());			
				for(PlanDentalServiceRegion planDentalServiceRegion:planDentalServiceRegionList){
					planDentalServiceRegionDTO = new PlanDentalServiceRegionDTO();
					createDTOBean.copyProperties(planDentalServiceRegionDTO, planDentalServiceRegion);
					
					List<PlanDentalRate> planDentalRateList = planDentalServiceRegion.getPlanDentalRates();
					planDentalServiceRegionDTO.setPlanDentalRates(new ArrayList<PlanDentalRate>());
					for(PlanDentalRate planDentalRate:planDentalRateList){
						planDentalRateDTO = new PlanDentalRateDTO();
						createDTOBean.copyProperties(planDentalRateDTO, planDentalRate);
						planDentalServiceRegionDTO.getPlanDentalRates().add(planDentalRateDTO);
					}				
					planDentalDTO.getPlanDentalServiceRegion().add(planDentalServiceRegionDTO);				
				}*/
//			}			
			
		}catch(Exception e){
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error inside method createPlanDTO---->", e, false);
		}
		
		return planDTO;		
	}
	
	public float getAgeFromDOB(Date dob, Date coverageStartDate){
		float age = (float) 0.0;
		Calendar coverageStartDateObj = TSCalendar.getInstance();
		coverageStartDateObj.setTime(coverageStartDate);
		Calendar dobObj = TSCalendar.getInstance();
		dobObj.setTime(dob);
		
		int  yearDiff = coverageStartDateObj.get(Calendar.YEAR) - dobObj.get(Calendar.YEAR);
		int monthDiff = coverageStartDateObj.get(Calendar.MONTH) - dobObj.get(Calendar.MONTH);
		
		if(monthDiff <0){
			yearDiff --;
			monthDiff = 12- dobObj.get(Calendar.MONTH) + coverageStartDateObj.get(Calendar.MONTH);
		}else if(monthDiff == 0){
			int dayDiff = coverageStartDateObj.get(Calendar.DAY_OF_MONTH) - dobObj.get(Calendar.DAY_OF_MONTH);
			if(dayDiff < 0){
				yearDiff --;
				monthDiff = 9;
			}
		}
		
		String ageValue = yearDiff + "." + monthDiff;
		age = Float.parseFloat(ageValue);
		
		return age;
	}
	
    /*private List<Member> getHouseHoldCensusList(GroupData groupData, int groupId, int employeeId, Date coverageStartDate) 
    {
    	putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getHouseHoldCensusList() :  Forming the Members List.---->", null);
		List<Member> membersData = new ArrayList<Member>();
		
			//if(groupData.getGroupId() == groupId)
			{
				List<PersonData> members = groupData.getPersonDataList();
				for(PersonData member: members)
				{
					Member singleMember = new Member();
					putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getHouseHoldCensusList() : instantiating Member object---->", null);
					singleMember.setMemberID(member.getPersonId());
					putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getHouseHoldCensusList() : instantiating Member object :  id :---->", null);
					String relation = member.getRelationship();
					putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getHouseHoldCensusList() : instantiating Member object : relation :---->", null);
					if("self".equalsIgnoreCase(relation)){
						singleMember.setRelationship(Relationship.SE);
					}
					else if ("spouse".equalsIgnoreCase(relation))
					{
						singleMember.setRelationship(Relationship.SP);
					}
					else if ("child".equalsIgnoreCase(relation))
					{
						singleMember.setRelationship(Relationship.CH);
					}
					else 
					{
						singleMember.setRelationship(Relationship.DT);
					}
					singleMember.setSmoker(false);
					Date memberDob = DateUtil.StringToDate(member.getDob(), DATE_FORMAT);
					//LOGGER.info("	 getHouseHoldCensusList() : instantiating Member object : memberDob : " + memberDob );
					singleMember.setMemberDOB(memberDob);
					Calendar today = TSCalendar.getInstance();
					today.setTime(coverageStartDate);		//DateUtil.StringToDate((String) member.get("coverageStartDate"),DATE_FORMAT));
					Calendar dob = TSCalendar.getInstance();
					dob.setTime(DateUtil.StringToDate((String) member.getDob(), DATE_FORMAT));
					Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);----Inside showplans
					if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) 
						|| (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE)))
					{
						age--;
					}
					//LOGGER.info("Inside getHouseHoldCensusList() : instantiating Member object : age : " + age );
					singleMember.setAge(age);
					//censusData.put("tobacco", (String) member.get("smoker"));
					//censusData = new HashMap<String, String>();
					//censusData.put("id", String.valueOf(employeeId));
					//Plan management API considers relation 'self' as 'member' for individual premium calculation.
					//String relation = (String) member.get("type");
					if(relation.equalsIgnoreCase("self")){
						//censusData.put("relation", "member");
					}else{
						censusData.put("relation", relation);
					}
					
					//censusData.put("age", age.toString());            
					//censusData.put("tobacco", (String) member.get("smoker"));
					//censusData.put("zip", (String) member.get("zipCode"));
					//censusData.put("countycode", (String) member.get("countyCode"));
					//Leaving salary blank as per discussion with enrollment team.
					//censusData.put("salary", "");
					membersData.add(singleMember);
					//censusList.add(censusData);
				}
			}		

		return membersData;
	}*/
	
    public IndividualRequest formIndividualRequest(HttpServletRequest request) {		

		IndividualRequest individualRequest = new IndividualRequest();
		IndividualHousehold individualHousehold = new IndividualHousehold();

		individualHousehold.setSadpFlag(request.getParameter("SADP_Flag"));
		
		Map<String,String> responsiblePerson = new HashMap<String, String>();
		responsiblePerson.put("responsiblePersonId", request.getParameter("responsiblePersonId"));
		responsiblePerson.put("responsiblePersonFirstName", request.getParameter("responsiblePersonFirstName"));
		responsiblePerson.put("responsiblePersonMiddleName", request.getParameter("responsiblePersonMiddleName"));
		responsiblePerson.put("responsiblePersonLastName", request.getParameter("responsiblePersonLastName"));
		responsiblePerson.put("responsiblePersonSuffix", request.getParameter("responsiblePersonSuffix"));
		responsiblePerson.put("responsiblePersonSsn", request.getParameter("responsiblePersonSsn"));
		responsiblePerson.put("responsiblePersonPrimaryPhone", request.getParameter("responsiblePersonPrimaryPhone"));
		responsiblePerson.put("responsiblePersonSecondaryPhone", request.getParameter("responsiblePersonSecondaryPhone"));
		responsiblePerson.put("responsiblePersonPreferredPhone", request.getParameter("responsiblePersonPreferredPhone"));
		responsiblePerson.put("responsiblePersonPreferredEmail", request.getParameter("responsiblePersonPreferredEmail"));
		responsiblePerson.put("responsiblePersonHomeAddress1", request.getParameter("responsiblePersonHomeAddress1"));
		responsiblePerson.put("responsiblePersonHomeAddress2", request.getParameter("responsiblePersonHomeAddress2"));
		responsiblePerson.put("responsiblePersonHomeCity", request.getParameter("responsiblePersonHomeCity"));
		responsiblePerson.put("responsiblePersonHomeState", request.getParameter("responsiblePersonHomeState"));
		responsiblePerson.put("responsiblePersonHomeZip", request.getParameter("responsiblePersonHomeZip"));
		responsiblePerson.put("responsiblePersonType", request.getParameter("responsiblePersonType"));
		
		individualHousehold.setResponsiblePerson(responsiblePerson);
		
		Map<String,String> householdContact = new HashMap<String, String>();
		householdContact.put("houseHoldContactId",request.getParameter("houseHoldContactId"));
		householdContact.put("houseHoldContactFirstName",request.getParameter("houseHoldContactFirstName"));
		householdContact.put("houseHoldContactLastName",request.getParameter("houseHoldContactLastName"));
		householdContact.put("houseHoldContactMiddleName",request.getParameter("houseHoldContactMiddleName"));
		householdContact.put("houseHoldContactSuffix",request.getParameter("houseHoldContactSuffix"));
		householdContact.put("houseHoldContactFederalTaxIdNumber",request.getParameter("houseHoldContactFederalTaxIdNumber"));
		householdContact.put("houseHoldContactPrimaryPhone",request.getParameter("houseHoldContactPrimaryPhone"));
		householdContact.put("houseHoldContactSecondaryPhone",request.getParameter("houseHoldContactSecondaryPhone"));
		householdContact.put("houseHoldContactPreferredPhone",request.getParameter("houseHoldContactPreferredPhone"));
		householdContact.put("houseHoldContactPreferredEmail",request.getParameter("houseHoldContactPreferredEmail"));
		householdContact.put("houseHoldContactHomeAddress1",request.getParameter("houseHoldContactHomeAddress1"));
		householdContact.put("houseHoldContactHomeAddress2",request.getParameter("houseHoldContactHomeAddress2"));
		householdContact.put("houseHoldContactHomeCity",request.getParameter("houseHoldContactHomeCity"));
		householdContact.put("houseHoldContactHomeState",request.getParameter("houseHoldContactHomeState"));
		householdContact.put("houseHoldContactHomeZip",request.getParameter("houseHoldContactHomeZip"));
		
		individualHousehold.setHouseholdContact(householdContact);
		if ("S".equals(request.getParameter("enrollmentType"))){
			if(request.getParameter("userRoleId")!= null){
				individualHousehold.setUserRoleId(request.getParameter("userRoleId"));
			}
			if(request.getParameter("userRoleType")!= null){
				individualHousehold.setUserRoleType(request.getParameter("userRoleType"));
			}
		}
		
		String employeeQuotingZipcode = "";
		String employeeQuotingCountyCode = "";
		
		int personCount = Integer.parseInt(request.getParameter("personCount").toString());
		String eligibleMembersIds = "";
		ArrayList<Map<String,String>> members = new ArrayList<Map<String,String>>();
		
		String employeeId = "";
		String spouseId = "";
		String dependentsId = "";
		
		//Temporary Considering MemberID as 'i' and members with coverage eligible yes as eligible member.
		for(int i=0 ; i<personCount ; i++){
			Map<String,String> member = new HashMap<String, String>();
			
			member.put("memberId",String.valueOf(i) );
			member.put("firstName", request.getParameter("pldPersonList["+i+"].firstName"));
			member.put("lastName", request.getParameter("pldPersonList["+i+"].lastName"));
			member.put("dob", request.getParameter("pldPersonList["+i+"].dob"));
			member.put("relationship", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			// To be confirmed 
			member.put("tobacco", "N");//request.getParameter("tobacco"));
			member.put("ssn", request.getParameter("ssn"));
			member.put("primaryPhone", request.getParameter("primaryPhone"));
			member.put("secondaryPhone", request.getParameter("secondaryPhone"));
			member.put("preferredPhone", request.getParameter("preferredPhone"));
			member.put("preferredEmail", request.getParameter("preferredEmail"));
			member.put("homeAddressLine1", request.getParameter("homeAddressLine1"));
			member.put("homeAddress2", request.getParameter("homeAddress2"));
			member.put("city", request.getParameter("city"));
			member.put("state", request.getParameter("state"));
			member.put("zip", request.getParameter("pldPersonList["+i+"].zip"));
			member.put("county", request.getParameter("county"));
			member.put("genderCode", request.getParameter("genderCode"));
			member.put("maritalStatusCode", request.getParameter("maritalStatusCode"));
			member.put("citizenshipStatusCode", request.getParameter("citizenshipStatusCode"));
			member.put("spokenLanguageCode", request.getParameter("spokenLanguageCode"));
			member.put("writtenLanguageCode", request.getParameter("writtenLanguageCode"));
			member.put("mailingAddress1", request.getParameter("mailingAddress1"));
			member.put("mailingAddress2", request.getParameter("mailingAddress2"));
			member.put("mailingCity", request.getParameter("mailingCity"));
			member.put("mailingState", request.getParameter("mailingState"));
			member.put("mailingZip", request.getParameter("mailingZip"));
			member.put("custodialParentId", request.getParameter("custodialParentId"));
			member.put("custodialParentFirstName", request.getParameter("custodialParentFirstName"));
			member.put("custodialParentMiddleName", request.getParameter("custodialParentMiddleName"));
			member.put("custodialParentLastName", request.getParameter("custodialParentLastName"));
			member.put("custodialParentSuffix", request.getParameter("custodialParentSuffix"));
			member.put("custodialParentSsn", request.getParameter("custodialParentSsn"));
			member.put("custodialParentPrimaryPhone", request.getParameter("custodialParentPrimaryPhone"));
			member.put("custodialParentSecondaryPhone", request.getParameter("custodialParentSecondaryPhone"));
			member.put("custodialParentPreferredPhone", request.getParameter("custodialParentPreferredPhone"));
			member.put("custodialParentPreferredEmail", request.getParameter("custodialParentPreferredEmail"));
			member.put("custodialParentHomeAddress1", request.getParameter("custodialParentHomeAddress1"));
			member.put("custodialParentHomeAddress2", request.getParameter("custodialParentHomeAddress2"));
			member.put("custodialParentHomeCity", request.getParameter("custodialParentHomeCity"));
			member.put("custodialParentHomeState", request.getParameter("custodialParentHomeState"));
			member.put("custodialParentHomeZip", request.getParameter("custodialParentHomeZip"));
			member.put("houseHoldContactRelationship",  request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			member.put("financialHardshipExemption", request.getParameter("financialHardshipExemption"));
			member.put("catastrophicEligible", request.getParameter("pldPersonList["+i+"].catastrophic_Eligible"));
			member.put("childOnlyPlanEligibile", request.getParameter("childOnlyPlanEligibile"));
			member.put("race", request.getParameter("race"));
			member.put("raceEthnicityCode1", request.getParameter("raceEthnicityCode1"));
			member.put("racedescription1", request.getParameter("racedescription1"));
			member.put("countyCode", request.getParameter("pldPersonList["+i+"].countyCode"));
					
			
			//Confirm behavior once, Special Enrollment true in case of both Shop and Individual.
			
			if ("S".equals(request.getParameter("enrollmentType"))){
				if(request.getParameter("pldPersonList["+i+"].maintenanceReasonCode")!= null){
					member.put("maintenanceReasonCode", request.getParameter("pldPersonList["+i+"].maintenanceReasonCode"));
				}
				if(request.getParameter("pldPersonList["+i+"].newPersonFLAG")!= null ){
					member.put("newPersonFLAG", request.getParameter("pldPersonList["+i+"].newPersonFLAG"));
				}
				if(request.getParameter("pldPersonList["+i+"].existingMedicalEnrollmentID")!= null){
					member.put("existingMedicalEnrollmentID", request.getParameter("pldPersonList["+i+"].existingMedicalEnrollmentID"));
				}
				if(request.getParameter("pldPersonList["+i+"].existingSADPEnrollmentID")!= null){
					member.put("existingSADPEnrollmentID", request.getParameter("pldPersonList["+i+"].existingSADPEnrollmentID"));
				}										
				
			}else{
				member.put("maintenanceReasonCode", "");
				member.put("newPersonFLAG", "");
				member.put("existingMedicalEnrollmentID", "");
				member.put("existingSADPEnrollmentID","");
			}
		
			
			if("Shop".equals(request.getParameter("pgrmType"))){
				
				String relationShipToEmployee = request.getParameter("pldPersonList["+i+"].emp_relationShip");
			
				//If employee
				if("18".equals(relationShipToEmployee)){
					//rough logic to be clarified yet.
					employeeQuotingZipcode = request.getParameter("pldPersonList["+i+"].Emp_zip");
					
					employeeQuotingCountyCode = request.getParameter("pldPersonList["+i+"].Emp_county");
					
					//LOGGER.info("employeeQuotingCountyCode   value: ## :"+ employeeQuotingCountyCode);
					
					employeeId = String.valueOf(i);
					//Only set the members with relationShip as Self, Spouse or dependents as eligible members
					if("".equals(eligibleMembersIds)){
						eligibleMembersIds = String.valueOf(i);
					}else{
						eligibleMembersIds = eligibleMembersIds + "," + i;
					}
				
				}else if("01".equals(relationShipToEmployee)){//Spouse
					spouseId = String.valueOf(i);
					if("".equals(eligibleMembersIds)){
						eligibleMembersIds = String.valueOf(i);
					}else{
						eligibleMembersIds = eligibleMembersIds + "," + i;
					}
				}else{ // Hence is dependent.
					if("".equals(dependentsId)){
						dependentsId = String.valueOf(i);
					}else{
						dependentsId = dependentsId + "," + i;
					}
					
					if("".equals(eligibleMembersIds)){
						eligibleMembersIds = String.valueOf(i);
					}else{
						eligibleMembersIds = eligibleMembersIds + "," + i;
					}
					
				}
				member.put("RelationToEmployee", relationShipToEmployee);
				
				
			}else{
				if("Y".equals(request.getParameter("pldPersonList["+i+"].coverage_Eligible"))){
						if("".equals(eligibleMembersIds)){
							eligibleMembersIds = String.valueOf(i);
						}else{
							eligibleMembersIds = eligibleMembersIds + "," + i;
						}
					}
			}
		
			members.add(member);
		}
		
		individualHousehold.setMembers(members);
		if("Individual".equals(request.getParameter("pgrmType"))){
			Map<String,String> individual = new HashMap<String, String>();
			individual.put("householdCaseId", request.getParameter("externalHouseholdId"));
			individual.put("coverageStartDate", request.getParameter("coverageStartDate"));
			individual.put("enrollmentType", request.getParameter("enrollmentType"));
			individual.put("aptc", request.getParameter("aptc"));
			individual.put("csr", request.getParameter("csr"));
			individual.put("eligibleMembersIds", eligibleMembersIds);
			individualHousehold.setIndividual(individual);
		}else{//for Shop
			Map<String,String> shop = new HashMap<String, String>();
			shop.put("employerCaseId", request.getParameter("emp_caseId"));
			shop.put("householdCaseId", request.getParameter("externalHouseholdId"));
			shop.put("enrollmentType", request.getParameter("enrollmentType"));
			shop.put("coverageStartDate", request.getParameter("coverageStartDate"));
			shop.put("employeeQuotingZipcode", employeeQuotingZipcode);
			shop.put("employeeQuotingCountyCode", employeeQuotingCountyCode);
			shop.put("employeeId",employeeId);
			shop.put("spouseId", spouseId);
			shop.put("dependentsId", dependentsId);
			individualHousehold.setShop(shop);
		}
		individualRequest.setIndividualHousehold(individualHousehold);

		
		return individualRequest;
	}
	
	


	public Float getOldPremium(List<IndividualPlan> oldPlans, int id) {
		Float oldPremium = null;
		if (oldPlans == null)
		{
			return oldPremium;
		}
		for (IndividualPlan oldPlan : oldPlans)
		{
			if (oldPlan.getPlanId().equals(id))
			{
				oldPremium = oldPlan.getPremium();
			}
		}
		return oldPremium;
	}

	public String getCommaSeparatedString(Set<Integer> planIds) 
	{
		StringBuffer planIdStr = new StringBuffer();
		for (Integer planId : planIds)
		{
			planIdStr.append(planId.toString());
			planIdStr.append(",");
		}
		if (!planIds.isEmpty())
		{
			planIdStr.deleteCharAt(planIdStr.length()-1);
		}
		return planIdStr.toString();
	}
	
	public String getCommaSeparatedNamesString(List<String> memberNames) 
	{
		StringBuffer memberNamesStr = new StringBuffer();
		for (String memberName : memberNames)
		{
			memberNamesStr.append(memberName);
			memberNamesStr.append(",");
		}
		if (!memberNames.isEmpty())
		{
			memberNamesStr.deleteCharAt(memberNamesStr.length()-1);
		}
		return memberNamesStr.toString();
	}
	
	public boolean getChildFlag(Date dob, Date coverageStartDate) {
		return getAgeFromDOB(dob, coverageStartDate)< 19 ? true : false;
	}
	
	public float getTotalTaxCredit(Float monthlyAptc){
		return (monthlyAptc != null ? BigDecimal.valueOf(monthlyAptc).multiply(BigDecimal.valueOf(12)).setScale(2,RoundingMode.HALF_UP).floatValue():0);
	}
	
		

	public IndividualRequest formIndividualRequestForPostFFM(HttpServletRequest request) {
		IndividualRequest individualRequest = new IndividualRequest();
		IndividualHousehold individualHousehold = new IndividualHousehold();
		String isSubsidized = request.getParameter("Subsidy_Flag");
		individualHousehold.setSubsidyFlag(isSubsidized);
		
		if(request.getParameter("userRoleId")!= null){
			individualHousehold.setUserRoleId(request.getParameter("userRoleId"));
		}
		if(request.getParameter("userRoleType")!= null){
			individualHousehold.setUserRoleType(request.getParameter("userRoleType"));
		}		
		
		
		int personCount = Integer.parseInt(request.getParameter("personCount").toString());
		String eligibleMembersIds = "";
		ArrayList<Map<String,String>> members = new ArrayList<Map<String,String>>();		
				
		
		for(int i=0 ; i<personCount ; i++){
			Map<String,String> member = new HashMap<String, String>();
			
			member.put("memberId",String.valueOf(i) );
			member.put("firstName", request.getParameter("pldPersonList["+i+"].firstName"));
			member.put("lastName", request.getParameter("pldPersonList["+i+"].lastName"));
			member.put("dob", request.getParameter("pldPersonList["+i+"].dob"));
			member.put("relationship", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			member.put("type", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			
			member.put("tobacco", "N");
			member.put("ssn", request.getParameter("ssn"));
			member.put("primaryPhone", request.getParameter("primaryPhone"));
			member.put("secondaryPhone", request.getParameter("secondaryPhone"));
			member.put("preferredPhone", request.getParameter("preferredPhone"));
			member.put("preferredEmail", request.getParameter("preferredEmail"));
			member.put("homeAddressLine1", request.getParameter("homeAddressLine1"));
			member.put("homeAddress2", request.getParameter("homeAddress2"));
			member.put("city", request.getParameter("city"));
			member.put("state", request.getParameter("state"));
			member.put("zip", request.getParameter("pldPersonList["+i+"].zip"));
			member.put("county", request.getParameter("county"));
			member.put("genderCode", request.getParameter("genderCode"));
			member.put("maritalStatusCode", request.getParameter("maritalStatusCode"));
			member.put("citizenshipStatusCode", request.getParameter("citizenshipStatusCode"));
			member.put("spokenLanguageCode", request.getParameter("spokenLanguageCode"));
			member.put("writtenLanguageCode", request.getParameter("writtenLanguageCode"));
			member.put("mailingAddress1", request.getParameter("mailingAddress1"));
			member.put("mailingAddress2", request.getParameter("mailingAddress2"));
			member.put("mailingCity", request.getParameter("mailingCity"));
			member.put("mailingState", request.getParameter("mailingState"));
			member.put("mailingZip", request.getParameter("mailingZip"));
			member.put("custodialParentId", request.getParameter("custodialParentId"));
			member.put("custodialParentFirstName", request.getParameter("custodialParentFirstName"));
			member.put("custodialParentMiddleName", request.getParameter("custodialParentMiddleName"));
			member.put("custodialParentLastName", request.getParameter("custodialParentLastName"));
			member.put("custodialParentSuffix", request.getParameter("custodialParentSuffix"));
			member.put("custodialParentSsn", request.getParameter("custodialParentSsn"));
			member.put("custodialParentPrimaryPhone", request.getParameter("custodialParentPrimaryPhone"));
			member.put("custodialParentSecondaryPhone", request.getParameter("custodialParentSecondaryPhone"));
			member.put("custodialParentPreferredPhone", request.getParameter("custodialParentPreferredPhone"));
			member.put("custodialParentPreferredEmail", request.getParameter("custodialParentPreferredEmail"));
			member.put("custodialParentHomeAddress1", request.getParameter("custodialParentHomeAddress1"));
			member.put("custodialParentHomeAddress2", request.getParameter("custodialParentHomeAddress2"));
			member.put("custodialParentHomeCity", request.getParameter("custodialParentHomeCity"));
			member.put("custodialParentHomeState", request.getParameter("custodialParentHomeState"));
			member.put("custodialParentHomeZip", request.getParameter("custodialParentHomeZip"));
			member.put("houseHoldContactRelationship",  request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			member.put("financialHardshipExemption", request.getParameter("financialHardshipExemption"));			
			member.put("childOnlyPlanEligibile", request.getParameter("childOnlyPlanEligibile"));
			member.put("race", request.getParameter("race"));
			member.put("raceEthnicityCode1", request.getParameter("raceEthnicityCode1"));
			member.put("racedescription1", request.getParameter("racedescription1"));					
			
			if("Y".equals(request.getParameter("pldPersonList["+i+"].coverage_Eligible"))){
				if("".equals(eligibleMembersIds)){
					eligibleMembersIds = String.valueOf(i);
				}else{
					eligibleMembersIds = eligibleMembersIds + "," + i;
				}
			}			
			members.add(member);
		}
		
		individualHousehold.setMembers(members);		
		
		Map<String,String> individual = new HashMap<String, String>();
		individual.put("householdCaseId", request.getParameter("externalHouseholdId"));
		individual.put("coverageStartDate", request.getParameter("coverageStartDate"));
		individual.put("enrollmentType", "S");
		if("YES".equals(isSubsidized)){
			individual.put("aptc", request.getParameter("aptc"));
			individual.put("csr", request.getParameter("csr"));
		}
		
		individual.put("eligibleMembersIds", eligibleMembersIds);
		
		individualHousehold.setIndividual(individual);
		
		individualRequest.setIndividualHousehold(individualHousehold);
		return individualRequest;
	}

	public int getCurrentGroupId(Integer householdId)
	{
		Map<String, Object> inputData = new HashMap<String, Object>();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----household id int value : ---->"+householdId, null, false);
		inputData.put("householdId", householdId);
		inputData.put("householdTypeString", HouseholdType.HOUSEHOLD);
		
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----invoking postSplitHousehold REST call---->", null, false);
		Map<String, Object> outputData = planDisplayRestClient.postSplitHousehold(inputData);
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----invoked postSplitHousehold REST call---->", null, false);
		return outputData.get("pldGroupId") != null ? (Integer)outputData.get("pldGroupId") : 0;
	}

	public Map<String, Object> formIndividualRequestForPHIX(EligLead eligLead) 
	{
		//Map eligibilityData = null;
		
		//Gson gsonObj = new Gson();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Extracting json eligibilityData into map---->", null, false);
		//eligibilityData = (Map)gsonObj.fromJson(eligibilityDataJSON, HashMap.class);
		
		//String csr = (String) eligibilityData.get("csr");
		String csr = "";
		if(eligLead.getCsr() != null && !eligLead.getCsr().isEmpty() && !"N/A".equalsIgnoreCase(eligLead.getCsr())){
			csr = eligLead.getCsr();
		}
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----CSR : ---->", null, false);
		String countyCode = eligLead.getCountyCode();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----countyCode : ---->", null, false);
		//String coverageStartDate = (String)  eligibilityData.get("coverageStart");
		Date coverageStartDate = eligLead.getCoverageStartDate();//getCoverageStartDateDayBased("", "");
		String covDate = DateUtil.dateToString(coverageStartDate, DATE_FORMAT);
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----coverageStartDate : ---->", null, false);
		String zip = eligLead.getZipCode();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----zip : ---->", null, false);
		String isSubsidized = "YES";
		if (eligLead.getExchangeFlowType() != null && eligLead.getExchangeFlowType().equals(ExchangeFlowType.OFF))
		{
			isSubsidized = "NO";
		}
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----isSubsidized : ---->"+isSubsidized, null, false);
		String aptc = "0";
		if(eligLead.getAptc() !=null && !eligLead.getAptc().isEmpty() && !"N/A".equalsIgnoreCase(eligLead.getAptc())){
			aptc = eligLead.getAptc();
		}
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----aptc : ---->"+aptc, null, false);
		long eligLeadId = eligLead.getId();
		MiniEstimatorResponse miniEstimatorResponse = null;
		List<com.getinsured.hix.model.estimator.mini.Member> members = null;
		try {
			ObjectReader objectReader = JacksonUtils.getJacksonObjectReaderForJavaType(MiniEstimatorResponse.class);
			miniEstimatorResponse = objectReader.readValue(eligLead.getApiOutput());
			members = miniEstimatorResponse.getMembers();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception : ---->", e, false);
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Exception : ---->", e, false);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occured while executing formIndividualRequestForPHIX() : ---->", e, false);
		}
		//LOGGER.debug("Members from lead are "+members.toString());
		
		/*ArrayList<Map> membersData = (ArrayList<Map>) eligibilityData.get("member");
		LOGGER.info("membersData : "+ membersData);*/
		List<Map<String,String>> personList = new ArrayList<Map<String,String>>();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming personList based on mebersData---->", null, false);
		
		boolean childFlag = false;
		int eligibleAdultCnt = 0,eligibleChildCnt = 0,inEligibleAdultCnt = 0,inEligibleChildCnt = 0;
		Map<String, Integer> quotedMembers = new HashMap<String, Integer>();
		quotedMembers.put("eligibleAdultCnt", 0);
		quotedMembers.put("eligibleChildCnt", 0);
		quotedMembers.put("inEligibleAdultCnt", 0);
		quotedMembers.put("inEligibleChildCnt", 0);
		
		//for(Map individualMember : membersData)
	if (members != null) {	
		for(com.getinsured.hix.model.estimator.mini.Member member : members)
		{
			Date dob = DateUtil.StringToDate((String)member.getDateOfBirth(), DATE_FORMAT);
			childFlag = getChildFlag(dob, coverageStartDate);
			if("Y".equals(member.getIsSeekingCoverage())){
				Map<String,String> personMap = new HashMap<String,String>();
				String relationshipToPrimary = member.getRelationshipToPrimary().toString();
				String relationship = relationshipToPrimary.substring(0, 1).toUpperCase() + relationshipToPrimary.substring(1).toLowerCase();
				personMap.put(PARAM_RELATIONSHIP, relationship);
				personMap.put(PARAM_FIRST_NAME, "Primary");
				personMap.put(PARAM_LAST_NAME, "Tax Filer"); 
				personMap.put(PARAM_ZIPCODE, zip);
				personMap.put(PARAM_COUNTY_CODE, countyCode);
				personMap.put(PARAM_TYPE, relationshipToPrimary);
				personMap.put(PARAM_DOB, DateUtil.dateToString(dob, GhixConstants.REQUIRED_DATE_FORMAT));
				personMap.put(PARAM_SMOKER, member.getIsTobaccoUser());
				personMap.put("pldPersonId", ""+member.getMemberNumber());
				
				personList.add(personMap);
								
				if(!childFlag){
					quotedMembers.put("eligibleAdultCnt", ++eligibleAdultCnt);
				}else{
					quotedMembers.put("eligibleChildCnt", ++eligibleChildCnt);
				}
			}else{
				if(!childFlag){
					quotedMembers.put("inEligibleAdultCnt", ++inEligibleAdultCnt);
				}else{
					quotedMembers.put("inEligibleChildCnt", ++inEligibleChildCnt);
				}
			}
		}
	}
		
		String quotedMembersString = this.prepareQuotedandNonQuotedMembersString(quotedMembers);
		
		Map<String, Object> householdData = new HashMap<String, Object>();
		householdData.put("ZIPCODE", zip);
		householdData.put(PARAM_COUNTY_CODE, countyCode);
		householdData.put("coverageStart", covDate);
		householdData.put("isSubsidized", isSubsidized);
		householdData.put("CSR", csr);
		if(aptc != null)
		{
			aptc = aptc.replace("$", "");
		}
		householdData.put("maxAPTC", aptc);
		householdData.put("membersData", personList);
		householdData.put("programType", "INDIVIDUAL");
		householdData.put("enrollmentType", "I");
		householdData.put("childFlag", childFlag);
		householdData.put("eligLeadId", eligLeadId);
		householdData.put("requestType", "PRESHOPPING");
		householdData.put("quotedMembers", quotedMembersString);
		
		return householdData;
	}
	
	public Map<String, Object> formIndividualRequestForPHIX(String eligibilityDataJSON) {
		Map eligibilityData = null;
		
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Extracting json eligibilityData into map---->", null, false);
		eligibilityData = (Map)platformGson.fromJson(eligibilityDataJSON, HashMap.class);
		
		String csr = (String) eligibilityData.get("csr");
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----CSR---->"+csr, null, false);
		String coverageStartDate = (String)  eligibilityData.get("coverageStart");
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----coverageStartDate---->"+coverageStartDate, null, false);
		String zip = (String) eligibilityData.get("zip");
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----zip : ---->"+zip, null, false);
		String isSubsidized = (String) eligibilityData.get("isSubsidized");
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----isSubsidized---->"+isSubsidized, null, false);
		String aptc = (String) eligibilityData.get("aptc");
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----aptc---->"+aptc, null, true);
		List<Map> membersData = (ArrayList<Map>) eligibilityData.get("member");
		//LOGGER.info("membersData : "+ membersData);
		List<Map<String,String>> personList = new ArrayList<Map<String,String>>();
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Forming personList based on mebersData---->", null, false);
		boolean childFlag = false;
		for(Map individualMember : membersData)
		{
				Map<String,String> personMap = new HashMap<String,String>();
				personMap.put(PARAM_RELATIONSHIP, (String)  individualMember.get("relationship"));
				personMap.put(PARAM_FIRST_NAME,(String)   individualMember.get("firstName"));
				if("".equals(individualMember.get("lastName"))){
					personMap.put(PARAM_LAST_NAME, (String)  individualMember.get("firstName")+ " Last");
				}else{
					personMap.put(PARAM_LAST_NAME, (String)  individualMember.get("lastName"));
				}
				personMap.put(PARAM_ZIPCODE, zip);
				String relationship = (String)individualMember.get("relationship");
				if(relationship != null)
				{
					relationship = relationship.toUpperCase();
				}
				
				personMap.put(PARAM_TYPE, relationship);
				Date dob = DateUtil.StringToDate((String)individualMember.get("dob"), "MMddyyyy");
				personMap.put(PARAM_DOB, DateUtil.dateToString(dob, GhixConstants.REQUIRED_DATE_FORMAT));
				personList.add(personMap);
				
				childFlag = getChildFlag(dob,DateUtil.StringToDate(coverageStartDate, DATE_FORMAT));
		}
		
		Map<String, Object> householdData = new HashMap<String, Object>();
		householdData.put("ZIPCODE", zip);
		householdData.put("coverageStart", coverageStartDate);
		householdData.put("isSubsidized", isSubsidized);
		householdData.put("CSR", csr);
		householdData.put("maxAPTC", aptc);
		householdData.put("membersData", personList);
		householdData.put("programType", "INDIVIDUAL");
		householdData.put("enrollmentType", "I");
		householdData.put("childFlag", childFlag);
		return householdData;
	}
	

	public List<Map<String, Object>> getProvidersList(String jsonString) 
	{
		List<Map<String, Object>> providers = new ArrayList<Map<String,Object>>();
		if(jsonString != null && jsonString.length()>0)
		{
			List<Map<String,Object>> gsonString = (ArrayList<Map<String,Object>>) platformGson.fromJson(jsonString, ArrayList.class);
			for (Map<String,Object> doctorsData : gsonString)
			{
				providers.add(convertToHashMap(doctorsData));
			}
		}
		return providers;
	}

	private Map<String, Object> convertToHashMap(Map<String, Object> doctorsData) 
	{
		Map<String, Object> hashMapObject = new HashMap<String, Object>();
		Object val = null;
		for (Map.Entry<String, Object> doctor : doctorsData.entrySet())
		{	
			if (doctor.getValue() instanceof Double) {
				val =  doctor.getValue().toString();
			}
			else
			{
				val = doctor.getValue();
			}
			hashMapObject.put(doctor.getKey(), val);
		}
		return hashMapObject;
	}
	
	/*public List<IndividualPlan> getPlanDetails(String planIdStr,
			HttpSession session, String insuranceType, boolean sendDummyData, boolean fetchOldData, int oldHousholdId, int oldGroupId,String isSpecialEnrollment,String enrollmentTypeString,boolean needSubscriber) {
		List<IndividualPlan> plansWithCost = new ArrayList<IndividualPlan>();
		int currentGroupId = 0;
		
		try {			
			Map<String,Integer> medicalconditions = null;
			Map<String,Integer> preferences =  null;
			Map<String,String> household = null;
			String coverageStartDate = null;
			List<GroupList> groupListArr = null;
			List<List<HashMap<String, Object>>>  providers = null;
			String showCatastrophicPlan = null;
			String planLevel = "";
			String pgrmType = null;
			Map<String, Object> subscriberData = null;
			if (fetchOldData)
			{
				currentGroupId = oldGroupId;
				medicalconditions = new HashMap<String, Integer>();
				preferences = new HashMap<String, Integer>();
				providers = new ArrayList<List<HashMap<String,Object>>>();

				PldHousehold houseHold = planDisplayRestClient.findPldHouseholdById(oldHousholdId);
				//List<PldHouseholdPerson> pldHouseHoldPersonList = planDisplayRestClient.findPldHouseholdPerson(houseHold.getId());
				

				//List<Map<String,String>> personList = new ArrayList<Map<String,String>>();
				//boolean isCatastrophicPlan = true;
				
				ProgramType programType = houseHold.getProgramType();
				
				String enrollmentType = houseHold.getEnrollmentType()!=null?houseHold.getEnrollmentType().toString():null;
				
				for(PldHouseholdPerson houseHoldPerson : pldHouseHoldPersonList){
					
					Map<String,String> personMap = createPersonMap(houseHoldPerson.getPldPerson(), houseHoldPerson.getPldPerson().getRelationship(),enrollmentType);
					personList.add(personMap);
					
					if(houseHoldPerson.getPldPerson().getCatastrophicEligible() == Eligible.NO){
						isCatastrophicPlan = false;
					}
				}
				
				Map<String, Object> result = getIndividualPersonList(houseHold.getId(), programType, false, true, false, false, enrollmentType);
				List<Map<String,String>> personList = (List<Map<String, String>>) result.get("personList");
				boolean isCatastrophicPlan = (Boolean) result.get("isCatastrophicPlan");
				List<PldHouseholdPerson> pldHouseHoldPersonList = (List<PldHouseholdPerson>) result.get("pldHouseHoldPersonList");
				
				showCatastrophicPlan = String.valueOf(isCatastrophicPlan);
								
				household = new HashMap<String, String>();
				if(houseHold.getProgramType()== ProgramType.INDIVIDUAL)
				{
					if(houseHold.getMonthlyAptc() != null){
						household.put(GhixConstants.HLD_APTC, houseHold.getMonthlyAptc().toString());
					}else{
						household.put(GhixConstants.HLD_APTC, "0");
					}
					if(houseHold.getCostSharing() != null){
						household.put(GhixConstants.HLD_CSR, houseHold.getCostSharing().toString());
					}else{
						household.put(GhixConstants.HLD_CSR, "");
					}
				}
				household = getHouseholdMap(houseHold.getProgramType(), houseHold.getMonthlyAptc(), houseHold.getCostSharing());
				Map<String, Object> inputData = new HashMap<String, Object>();
				inputData.put(paramCurrentGrpID, currentGroupId);
				inputData.put(paramHouseholdId, oldHousholdId);
				inputData.put(GhixConstants.INDIVIDUAL_PERSON_LIST, personList);
				PremiumTaxCreditResponse individualGroup = planDisplayRestClient.formGroupList(inputData);
				groupListArr = individualGroup.getGroupListArr();				
				groupListArr = getGroupListArr(String.valueOf(currentGroupId), String.valueOf(oldHousholdId), personList);

				coverageStartDate = DateUtil.dateToString(houseHold.getCoverageStart(), "MM/dd/yyyy");
				if(programType.equals(ProgramType.SHOP)){
					pgrmType = "Shop";
				}else{
					pgrmType = "Individual";
				}
				pgrmType = getProgramType(programType);
				
				PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
				planDisplayRequest.setPldHouseholdPersonList(pldHouseHoldPersonList);
				if (needSubscriber)
				{
					subscriberData = planDisplayRestClient.getSubscriberData(planDisplayRequest);
				}
				subscriberData = getSubscriberData(pldHouseHoldPersonList, needSubscriber);
			}
			else
			{
				currentGroupId = Integer.parseInt(session.getAttribute(paramCurrentGrpID).toString());
				if (sendDummyData)
				{
					medicalconditions = new HashMap<String, Integer>();
					preferences = new HashMap<String, Integer>();
					Integer housholdId = Integer.parseInt((String)session.getAttribute("HOUSEHOLD_ID"));
					PldHousehold houseHold = planDisplayRestClient.findPldHouseholdById(housholdId);
					coverageStartDate = DateUtil.dateToString(houseHold.getCoverageStart(), "MM/dd/yyyy");
					household = new HashMap<String, String>();
					if(houseHold.getProgramType()== ProgramType.INDIVIDUAL)
					{
						if(houseHold.getMonthlyAptc() != null){
							household.put(GhixConstants.HLD_APTC, houseHold.getMonthlyAptc().toString());
						}else{
							household.put(GhixConstants.HLD_APTC, "0");
						}
						if(houseHold.getCostSharing() != null){
							household.put(GhixConstants.HLD_CSR, houseHold.getCostSharing().toString());
						}else{
							household.put(GhixConstants.HLD_CSR, "");
						}
					}
					household = getHouseholdMap(houseHold.getProgramType(), houseHold.getMonthlyAptc(), houseHold.getCostSharing());
					Integer groupId = (Integer) session.getAttribute("CURRENT_GROUP_ID");
					Map<String, Object> inputData = new HashMap<String, Object>();
					inputData.put(paramCurrentGrpID, session.getAttribute(paramCurrentGrpID).toString());
					inputData.put(paramHouseholdId, session.getAttribute(paramHouseholdId).toString());
					inputData.put(GhixConstants.INDIVIDUAL_PERSON_LIST, session.getAttribute(GhixConstants.INDIVIDUAL_PERSON_LIST));
					PremiumTaxCreditResponse individualGroup = planDisplayRestClient.formGroupList(inputData);
					groupListArr = individualGroup.getGroupListArr();
					groupListArr = getGroupListArr(session.getAttribute(paramCurrentGrpID).toString(), session.getAttribute(paramHouseholdId).toString(), (List<Map<String,String>>) session.getAttribute(GhixConstants.INDIVIDUAL_PERSON_LIST));
					providers = new ArrayList<List<HashMap<String,Object>>>();
					
					session.setAttribute(GhixConstants.HOUSEHOLD,household);//db fetch && form map
					session.setAttribute(GhixConstants.COVERAGE_START,coverageStartDate);//houshold
					session.setAttribute(GhixConstants.GROUP_LIST,groupListArr);//fetchbased on householdid
					session.setAttribute("PROVIDERS",providers);
					
					List<PldHouseholdPerson> pldHouseHoldPersonList = planDisplayRestClient.findPldHouseholdPerson(houseHold.getId());
					PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
					planDisplayRequest.setPldHouseholdPersonList(pldHouseHoldPersonList);
					if (needSubscriber)
					{
						subscriberData = planDisplayRestClient.getSubscriberData(planDisplayRequest);
					}
					subscriberData = getSubscriberData(pldHouseHoldPersonList, needSubscriber);
				}
				else
				{
					medicalconditions = (Map<String, Integer>) session.getAttribute(GhixConstants.MEDICAL_CONDITIONS);//empty
					preferences =  (Map<String, Integer>) session.getAttribute(GhixConstants.HOUSEHOLD_PREFERENCES);//empty
					household =  (Map<String, String>) session.getAttribute(GhixConstants.HOUSEHOLD);//db fetch && form map
					coverageStartDate = (String) session.getAttribute(GhixConstants.COVERAGE_START);//houshold
					groupListArr = (ArrayList<GroupList>) session.getAttribute(GhixConstants.GROUP_LIST);//fetchbased on householdid
					providers = (ArrayList<List<HashMap<String, Object>>>)session.getAttribute("PROVIDERS");
					if (session.getAttribute(GhixConstants.SUBSCRIBER_DATA) != null)
					{
						subscriberData = (Map<String,Object>) session.getAttribute(GhixConstants.SUBSCRIBER_DATA);
					}
					
				}
				
				ProgramType programType = (ProgramType)session.getAttribute("Program_Type");		//keep same
				planLevel = (String)session.getAttribute("PLAN_TIER");			//same 
				showCatastrophicPlan = (String)session.getAttribute("showCatastrophicPlan"); ////same	
				if(programType.equals(ProgramType.SHOP)){
					showCatastrophicPlan = "false";
				}else{			
					planLevel = "";
				}
				
				pgrmType = session.getAttribute("PGRM_TYPE").toString();
			}
			plansWithCost = planDisplayRestClient.getIndividualPlans(insuranceType,currentGroupId,medicalconditions,preferences,household,coverageStartDate,groupListArr,planIdStr, planLevel, showCatastrophicPlan,"",pgrmType,providers,isSpecialEnrollment,enrollmentTypeString, subscriberData);
		} catch (Exception e) {
			LOGGER.error("Error while fetching plans data - quoting API ",e);
			//e.printStackTrace();
		}
		return plansWithCost;
	}
	*/
	
	public String getProgramType(ProgramType programType) {
		return programType.equals(ProgramType.SHOP)?"Shop":"Individual";
	}


	/*private Map<String, String> getHouseholdMap(ProgramType programType, Float monthlyAptc, String costSharing) 
	{
		Map<String, String> household = new HashMap<String, String>();
		if(programType == ProgramType.INDIVIDUAL)
		{
			if(monthlyAptc != null){
				household.put(GhixConstants.HLD_APTC, monthlyAptc.toString());
			}else{
				household.put(GhixConstants.HLD_APTC, "0");
			}
			if(costSharing != null){
				household.put(GhixConstants.HLD_CSR, costSharing);
			}else{
				household.put(GhixConstants.HLD_CSR, "");
			}
		}else{
			household.put(GhixConstants.HLD_APTC, "0");
			household.put(GhixConstants.HLD_CSR, "");
		}

		return household;
	}*/


	/*public List<IndividualPlan> getDentalPlanDetails(String planIdStr,
			HttpSession session, boolean sendDummy, boolean fetchOldData, int oldHousholdId, int oldGroupId,String isSpecialEnrollment,String enrollmentTypeString, boolean needSubscriber)
 {
		List<IndividualPlan> plansWithCost = new ArrayList<IndividualPlan>();
		int currentGroupId = 0;
		
		try {
			Map<String,Integer> medicalconditions =null;
			Map<String,Integer> preferences =  null;
			Map<String,String> household = null;
			String coverageStartDate = null;
			List<GroupList> groupListArr = null;
			List<List<HashMap<String, Object>>>  providers = null;
			String showCatastrophicPlan = null;
			String planLevel = "";
			String pgrmType = null;
			Map<String, Object> subscriberData = null;
			if (fetchOldData)
			{
				currentGroupId = 0;
				medicalconditions = new HashMap<String, Integer>();
				preferences = new HashMap<String, Integer>();
				providers = new ArrayList<List<HashMap<String,Object>>>();
				
				PldHousehold houseHold = planDisplayRestClient.findPldHouseholdById(oldHousholdId);
				//List<PldHouseholdPerson> pldHouseHoldPersonList = planDisplayRestClient.findPldHouseholdPerson(houseHold.getId());
				
				//List<Map<String,String>> personList = new ArrayList<Map<String,String>>();
				//boolean isCatastrophicPlan = true;
				
				ProgramType programType = houseHold.getProgramType();
				
				String enrollmentType = houseHold.getEnrollmentType()!=null?houseHold.getEnrollmentType().toString():null;
				
				for(PldHouseholdPerson houseHoldPerson : pldHouseHoldPersonList){
					
					Map<String,String> personMap = createPersonMap(houseHoldPerson.getPldPerson(), houseHoldPerson.getPldPerson().getRelationship(),enrollmentType);
					personList.add(personMap);
					
					if(houseHoldPerson.getPldPerson().getCatastrophicEligible() == Eligible.NO){
						isCatastrophicPlan = false;
					}
				}
				
				Map<String, Object> result = getIndividualPersonList(houseHold.getId(), programType, false, true, false, false, enrollmentType);
				List<Map<String,String>> personList = (List<Map<String, String>>) result.get("personList");
				boolean isCatastrophicPlan = (Boolean) result.get("isCatastrophicPlan");
				List<PldHouseholdPerson> pldHouseHoldPersonList = (List<PldHouseholdPerson>) result.get("pldHouseHoldPersonList");
				
				showCatastrophicPlan = String.valueOf(isCatastrophicPlan);
				currentGroupId = oldGroupId;
				
				household = new HashMap<String, String>();
				if(houseHold.getProgramType()== ProgramType.INDIVIDUAL)
				{
					if(houseHold.getMonthlyAptc() != null){
						household.put(GhixConstants.HLD_APTC, houseHold.getMonthlyAptc().toString());
					}else{
						household.put(GhixConstants.HLD_APTC, "0");
					}
					if(houseHold.getCostSharing() != null){
						household.put(GhixConstants.HLD_CSR, houseHold.getCostSharing().toString());
					}else{
						household.put(GhixConstants.HLD_CSR, "");
					}
				}
				household = getHouseholdMap(houseHold.getProgramType(), houseHold.getMonthlyAptc(), houseHold.getCostSharing());
				Map<String, Object> inputData = new HashMap<String, Object>();
				inputData.put(paramCurrentGrpID, currentGroupId);
				inputData.put(paramHouseholdId, oldHousholdId);
				inputData.put(GhixConstants.INDIVIDUAL_PERSON_LIST, personList);
				PremiumTaxCreditResponse individualGroup = planDisplayRestClient.formGroupList(inputData);
				groupListArr = individualGroup.getGroupListArr();
				groupListArr = getGroupListArr(String.valueOf(currentGroupId), String.valueOf(oldHousholdId), personList);

				coverageStartDate = DateUtil.dateToString(houseHold.getCoverageStart(), "MM/dd/yyyy");
				if(programType.equals(ProgramType.SHOP)){
					pgrmType = "Shop";
				}else{
					pgrmType = "Individual";
				}
				pgrmType = getProgramType(programType);

				PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
				planDisplayRequest.setPldHouseholdPersonList(pldHouseHoldPersonList);
				subscriberData = planDisplayRestClient.getSubscriberData(planDisplayRequest);
				subscriberData = getSubscriberData(pldHouseHoldPersonList, true);
			}
			else
			{
				currentGroupId = Integer.parseInt(session.getAttribute(paramCurrentGrpID).toString());
				if (sendDummy)
				{
					medicalconditions = new HashMap<String, Integer>();
					preferences = new HashMap<String, Integer>();
					Integer housholdId = Integer.parseInt((String)session.getAttribute("HOUSEHOLD_ID"));
					PldHousehold houseHold = planDisplayRestClient.findPldHouseholdById(housholdId);
					coverageStartDate = DateUtil.dateToString(houseHold.getCoverageStart(), "MM/dd/yyyy");
					household = new HashMap<String, String>();
					if(houseHold.getProgramType()== ProgramType.INDIVIDUAL)
					{
						if(houseHold.getMonthlyAptc() != null){
							household.put(GhixConstants.HLD_APTC, houseHold.getMonthlyAptc().toString());
						}else{
							household.put(GhixConstants.HLD_APTC, "0");
						}
						if(houseHold.getCostSharing() != null){
							household.put(GhixConstants.HLD_CSR, houseHold.getCostSharing().toString());
						}else{
							household.put(GhixConstants.HLD_CSR, "");
						}
					}
					household = getHouseholdMap(houseHold.getProgramType(), houseHold.getMonthlyAptc(), houseHold.getCostSharing());
					Integer groupId = (Integer) session.getAttribute("CURRENT_GROUP_ID");
					Map<String, Object> inputData = new HashMap<String, Object>();
					inputData.put(paramCurrentGrpID, session.getAttribute(paramCurrentGrpID).toString());
					inputData.put(paramHouseholdId, session.getAttribute(paramHouseholdId).toString());
					inputData.put(GhixConstants.INDIVIDUAL_PERSON_LIST, session.getAttribute(GhixConstants.INDIVIDUAL_PERSON_LIST));
					PremiumTaxCreditResponse individualGroup = planDisplayRestClient.formGroupList(inputData);
					groupListArr = individualGroup.getGroupListArr();
					groupListArr = getGroupListArr(session.getAttribute(paramCurrentGrpID).toString(), session.getAttribute(paramHouseholdId).toString(), (List<Map<String,String>>) session.getAttribute(GhixConstants.INDIVIDUAL_PERSON_LIST));
					
					providers = new ArrayList<List<HashMap<String,Object>>>();
					
					List<PldHouseholdPerson> pldHouseHoldPersonList = planDisplayRestClient.findPldHouseholdPerson(houseHold.getId());
					PlanDisplayRequest planDisplayRequest = new PlanDisplayRequest();
					planDisplayRequest.setPldHouseholdPersonList(pldHouseHoldPersonList);
					subscriberData = planDisplayRestClient.getSubscriberData(planDisplayRequest);
					subscriberData = getSubscriberData(pldHouseHoldPersonList, true);
				}
				else
				{
					medicalconditions = (Map<String, Integer>) session.getAttribute(GhixConstants.MEDICAL_CONDITIONS);
					preferences =  (Map<String, Integer>) session.getAttribute(GhixConstants.HOUSEHOLD_PREFERENCES);
					household =  (Map<String, String>) session.getAttribute(GhixConstants.HOUSEHOLD);
					coverageStartDate = (String) session.getAttribute(GhixConstants.COVERAGE_START);
					groupListArr = (ArrayList<GroupList>) session.getAttribute(GhixConstants.GROUP_LIST);
					providers = (ArrayList<List<HashMap<String, Object>>>)session.getAttribute("PROVIDERS");

					if (session.getAttribute(GhixConstants.SUBSCRIBER_DATA) != null)
					{
						subscriberData = (Map<String,Object>) session.getAttribute(GhixConstants.SUBSCRIBER_DATA);
				}
				}
				showCatastrophicPlan = (String)session.getAttribute("showCatastrophicPlan");
				planLevel = (String)session.getAttribute("PLAN_TIER");			
				ProgramType programType = (ProgramType)session.getAttribute("Program_Type");					
				if(programType.equals(ProgramType.SHOP)){
					showCatastrophicPlan = "false";
				}else{			
					planLevel = "";
				}
				pgrmType = session.getAttribute("PGRM_TYPE").toString();
			}
			String insuranceType = PlanInsuranceType.DENTAL.toString();
			plansWithCost = planDisplayRestClient.getIndividualPlans(insuranceType,currentGroupId,medicalconditions,preferences,household,coverageStartDate,groupListArr,planIdStr, planLevel, showCatastrophicPlan,"",pgrmType,providers,isSpecialEnrollment,enrollmentTypeString, subscriberData);
		} catch (Exception e) {
			LOGGER.error("Error in getDentalPlanDetails()", e.getCause());
		}
		return plansWithCost;
	}
*/

	/*private List<GroupList> getGroupListArr(String groupId, String householdId, List<Map<String,String>> individualPersonList) {
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put(paramCurrentGrpID, groupId);
		inputData.put(paramHouseholdId, householdId);
		inputData.put(GhixConstants.INDIVIDUAL_PERSON_LIST, individualPersonList);
		PremiumTaxCreditResponse individualGroup = planDisplayRestClient.formGroupList(inputData);
		return individualGroup.getGroupListArr();
	}*/


	public Map<String,Object> getSubscriberData(int householdId, String enrollmentType, String programType, String quotingGroup)  {
		 return planDisplayRestClient.getSubscriberData(householdId, enrollmentType, programType, quotingGroup);
	}
	
	public List<IndividualPlan> getPlanInfo(String planId, String coverageStartDate){
		return planDisplayRestClient.getPlanInfo(planId,coverageStartDate);		
	}

	public Map<String,Float> formContributionAmountsMap(List<Member> members)
	{
		Map<String,Float> contributions  = new HashMap<String, Float>();
		float dependantPremium = 0;
		for (Member member : members)
		{
			BigDecimal bd = new BigDecimal(Float.toString(member.getBenchmarkPremium()));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
			if(member.getRelationship().equals(Relationship.SE))
			{
				contributions.put("employeeContribution", bd.floatValue());
			}
			else
			{
				
				dependantPremium = BigDecimal.valueOf(dependantPremium).add(bd).setScale(2,RoundingMode.HALF_UP).floatValue();
			}
		}
		contributions.put("dependentContribution", dependantPremium);
		return contributions;
	}
	
	public String getPlanType(String ehb) {
		String planType = null;
		if("10.0".equals(ehb)){
			planType =  "Combined";
		}else if("9.5".equals(ehb)){
			planType =  "Health";
		}else if("0.5".equals(ehb)){
			planType = "Separate";
		}
		return planType;
	}
	
	public List<GroupList> createGroupList(int pdlGroupId, String aptc,List<Map<String, String>> individualPersonList) {
		List<GroupList> groupListArr = new ArrayList<GroupList>();
        GroupList groupList = new GroupList();
        groupList.setId(pdlGroupId);
        Float aptcVal = null;
		try{
			aptcVal = Float.valueOf(aptc);
		}catch(Exception e)
		{
			aptcVal = 0.0f;
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error while getting Float aptc : ---->", e, false);
		}
		groupList.setMonthlyAptc(aptcVal);
		List<Map<String, String>> nameList = new ArrayList<Map<String, String>>();
		int i = 1;
		for (Map<String, String> individualMap : individualPersonList) {
			Map<String, String> nameData = new HashMap<String, String>();
			//nameData.put("id",""+i);
			nameData.put("id",""+individualMap.get("pldPersonId"));
			nameData.put(PARAMPERSONFIRSTNAME,	individualMap.get(PARAM_FIRST_NAME));
			nameData.put(PARAMPERSONLASTNAME, individualMap.get(PARAM_LAST_NAME));
			nameData.put("type", individualMap.get(PARAM_TYPE));
			nameData.put("dob", individualMap.get(PARAM_DOB));
			nameData.put("smoker", individualMap.get(PARAM_SMOKER));
			nameData.put("zipCode", individualMap.get(PARAM_ZIPCODE));
			nameData.put("countyCode", individualMap.get(PARAM_COUNTY_CODE));
			//nameData.put("coverageStartDate", DateUtil.dateToString(houseHoldPerson.getPldPerson().getCoverageStart(), "MM/dd/yyyy"));
			nameList.add(nameData);
			i++;
		}

		groupList.setPersonList(nameList);
		groupListArr.add(groupList);
		return groupListArr;
	}
	
	public Map<String, Object> savePreferences(Integer householdId, Integer pldGroupId, Map<String, Object> householdData, Map pref) 
	{
		List<List<HashMap<String, Object>>> providers = new ArrayList<List<HashMap<String,Object>>>();
	    Map<String, Object> prefInputData = new HashMap<String, Object>();
        prefInputData.put("pref", pref);
        prefInputData.put("ANONYMOUS_INDIVIDUAL_TYPE", householdData.get("ANONYMOUS_INDIVIDUAL_TYPE"));
	    prefInputData.put(PARAM_HOUSEHOLD_ID, householdId);
	    prefInputData.put(PARAM_CURRENT_GRP_ID, pldGroupId);
        prefInputData.put("maxAPTC", householdData.get("maxAPTC"));
	    prefInputData.put("csr", householdData.get("CSR"));
	    prefInputData.put("coverageStart", householdData.get("coverageStart"));
	    prefInputData.put("PROVIDERS", providers);
	    prefInputData.put("sadpFlag", "NO");
		return prefInputData;
	}
	
	public Map<String, Object> getPldGroupData(Integer householdId, String householdTypeString)
	{
		Map<String, Object> inputData = new HashMap<String, Object>();
		inputData.put("householdId", householdId);
		inputData.put("householdTypeString", householdTypeString);
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----invoking postSplitHousehold REST call---->", null, false);
		return planDisplayRestClient.postSplitHousehold(inputData);
	}
	
	public Map<String, Object> formApplicantEligibilityRequestForFFM(HttpServletRequest request){
		Map<String, Object> householdMap = new HashMap<String, Object>();		
		
		int personCount = Integer.parseInt(request.getParameter("personCount").toString());
		String eligibleMembersIds = "";
		List<Map<String,Object>> memberList = new ArrayList<Map<String,Object>>();
		boolean childFlag = false;
		String zip = null;
		
		for(int i=0 ; i<personCount ; i++){
			Map<String,Object> member = new HashMap<String, Object>();
						
			member.put("memberId",String.valueOf(i) );
			member.put("firstName", request.getParameter("pldPersonList["+i+"].firstName"));
			member.put("lastName", request.getParameter("pldPersonList["+i+"].lastName"));
			member.put("middleName", request.getParameter("middleName"));
			member.put("suffixName", request.getParameter("suffixName"));
			String dob = request.getParameter("pldPersonList["+i+"].dob");
			member.put("dob", dob);
			member.put("relationshipCode", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			member.put("type", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));	
			zip = request.getParameter("pldPersonList["+i+"].zip");
			member.put("zipCode", zip);	
			member.put("ssn", request.getParameter("ssn"));
			member.put("sexCode", request.getParameter("genderCode"));
			member.put("maritalStatusCode", request.getParameter("maritalStatusCode"));			
			member.put("ssn", request.getParameter("ssn"));
			member.put("genderCode", request.getParameter("genderCode"));
			member.put("maritalStatusCode", request.getParameter("maritalStatusCode"));			
			member.put("language", request.getParameter("language"));			
			member.put("primaryIndicator", request.getParameter("primaryIndicator"));
			member.put("homeIndicator", request.getParameter("homeIndicator"));
			member.put("workIndicator", request.getParameter("workIndicator"));
			member.put("categoryCode", request.getParameter("categoryCode"));
			member.put("speaksLangIndicator", request.getParameter("speaksLangIndicator"));
			member.put("writesLangIndicator", request.getParameter("writesLangIndicator"));
			
			
			if("Y".equals(request.getParameter("pldPersonList["+i+"].coverage_Eligible"))){
				if("".equals(eligibleMembersIds)){
					eligibleMembersIds = String.valueOf(i);
				}else{
					eligibleMembersIds = eligibleMembersIds + "," + i;
				}
			}	
			
			Date dateOfBirth = DateUtil.StringToDate(dob, "MMddyyyy");
			boolean isChild = getChildFlag(dateOfBirth,DateUtil.StringToDate(request.getParameter("effectiveDate"), DATE_FORMAT));
			if(isChild){
				childFlag = true;
			}
			List<Map<String, String>> relationshipList = new ArrayList<Map<String, String>>();
			Map<String, String> relationshipMap = new HashMap<String, String>();
			relationshipMap.put("memberId", String.valueOf(i));
			relationshipMap.put("relationshipCode", request.getParameter("pldPersonList["+i+"].houseHoldContactRelationship"));
			relationshipList.add(relationshipMap);
			member.put("relationshipData", relationshipList);
			
			memberList.add(member);			
		}
		householdMap.put("membersData", memberList);		
		householdMap.put("eligibleMembersIds", eligibleMembersIds);	
		
		householdMap.put("isSubsidized", request.getParameter("Subsidy_Flag"));		
		/*if(request.getParameter("userRoleId")!= null){
			householdMap.put("userRoleId", request.getParameter("userRoleId"));
		}
		if(request.getParameter("userRoleType")!= null){
			householdMap.put("userRoleType", request.getParameter("userRoleType"));
		}	*/
		householdMap.put("coverageStartDate", request.getParameter("effectiveDate"));
		householdMap.put("aptc", request.getParameter("aptc"));
		householdMap.put("csr", request.getParameter("csr"));		
		householdMap.put("zipCode", zip);
		householdMap.put("programType", "INDIVIDUAL");
		householdMap.put("enrollmentType", "I");
		householdMap.put("childFlag", childFlag);
		householdMap.put("requestType", request.getParameter("requestType"));
		householdMap.put("eligLeadId", request.getParameter("eligLeadId"));		
		
		Map<String,String> taxFilerPersonMap = new HashMap<String, String>();
		taxFilerPersonMap.put("taxFilerFirstName", request.getParameter("taxFilerFirstName"));
		taxFilerPersonMap.put("taxFilerLastName", request.getParameter("taxFilerLastName"));
		taxFilerPersonMap.put("taxFilerMiddleName", request.getParameter("taxFilerMiddleName"));
		taxFilerPersonMap.put("taxFilerSuffix", request.getParameter("taxFilerSuffix"));		
		householdMap.put("taxFilerPersonMap", taxFilerPersonMap);
				
		return householdMap;
	}
	
	public Map<String, Integer> createPreferenceMap(String docVisitFrequency, int familySize) {
		Map<String, Integer> pref = new HashMap<String, Integer>();
		pref.put(GhixConstants.LOW_DRUG_USE_VAL,0);
		pref.put(GhixConstants.MODERATE_DRUG_USE_VAL,0);
		pref.put(GhixConstants.HIGH_DRUG_USE_VAL,0);
		pref.put(GhixConstants.V_HIGH_DRUG_USE_VAL,0);
		
		pref.put(GhixConstants.LOW_MEDICAL_VAL,0);
		pref.put(GhixConstants.MODERATE_MEDICAL_VAL,0);
		pref.put(GhixConstants.HIGH_MEDICAL_VAL,0);
		pref.put(GhixConstants.V_HIGH_MEDICAL_VAL,0);

		if ("1".equals(docVisitFrequency))
		{
			pref.put(GhixConstants.LOW_MEDICAL_VAL,familySize);
			pref.put(GhixConstants.LOW_DRUG_USE_VAL,familySize);
		}else if ("2".equals(docVisitFrequency))
		{
			pref.put(GhixConstants.MODERATE_MEDICAL_VAL,familySize);
			pref.put(GhixConstants.MODERATE_DRUG_USE_VAL,familySize);
		}else if ("5".equals(docVisitFrequency))
		{
			pref.put(GhixConstants.HIGH_MEDICAL_VAL,familySize);
			pref.put(GhixConstants.HIGH_DRUG_USE_VAL,familySize);
		}else if ("10".equals(docVisitFrequency))
		{
			pref.put(GhixConstants.V_HIGH_MEDICAL_VAL,familySize);
			pref.put(GhixConstants.V_HIGH_DRUG_USE_VAL,familySize);
		}
		return pref;
	}
	
	public String getShowTab(String showTabParam, List<IndividualPlan> individualPlans, List<IndividualPlan> oldPlans) {
		String showTab = showTabParam;
		if (showTab == null || showTab.length() < 1){
			showTab = "keep";
		}
		if (individualPlans == null || individualPlans.isEmpty() ){
			if(null!=oldPlans && !oldPlans.isEmpty()){
				showTab = "review";
			}else{
				showTab = "shop";
			}			
		}
		return showTab;
	}
	
	public Map<String, String> getPlanIdAndOrderIdInCart(int oldHouseholdId, int oldPldGroupId) 
	{
		Map<String, String> inputData = new HashMap<String, String>();		
		inputData.put("householdId", String.valueOf(oldHouseholdId));		
		inputData.put("CURRENT_GROUP_ID", String.valueOf(oldPldGroupId));		
		String commaSeparatedId = planDisplayRestClient.checkItemInCart(inputData); 
		Map<String, String> planIdAndOrderIdMap = new HashMap<String, String>();
		String planId = "0";
		String orderItemId = "0";
		if(null!=commaSeparatedId && !"0".equals(commaSeparatedId))
		{
			String[] arrayOfIds = commaSeparatedId.split(",");
			planId = arrayOfIds[0];
			orderItemId =  arrayOfIds[1];
		}
		planIdAndOrderIdMap.put("planId", planId);
		planIdAndOrderIdMap.put("orderItemId", orderItemId);
		
		return planIdAndOrderIdMap;
	}
	
	public void getOldPlanData(List<IndividualPlan> oldHealthPlans, Set<Integer> planIds, Float oldPlanPremium, Float oldPlanAptc) 
	{
		if(null != oldHealthPlans && !oldHealthPlans.isEmpty())
		{
			for(IndividualPlan oldPlan : oldHealthPlans)
			{
				oldPlan.setAptc(oldPlanAptc);
				oldPlan.setPremium(oldPlanPremium);
				
				if(BigDecimal.valueOf(oldPlanPremium).subtract(BigDecimal.valueOf(oldPlanAptc)).setScale(2,RoundingMode.HALF_UP).floatValue() <= 0) {
					oldPlan.setPremiumAfterCredit(0.0f);
				}
				else {
					oldPlan.setPremiumAfterCredit(BigDecimal.valueOf(oldPlanPremium).subtract(BigDecimal.valueOf(oldPlanAptc)).setScale(2,RoundingMode.HALF_UP).floatValue());
				}
				planIds.add(oldPlan.getPlanId());
			}
		}		
	}
	
	public void getAvailablePlansForRenewal(List<IndividualPlan> individualPlans, Set<Integer> planIds, Map<String, Float> oldPlanPremium, 
			Map<String, Float> oldPlanAptc, List<IndividualPlan> availablePlans) {
		
		if(individualPlans!=null){
			for (IndividualPlan individualPlan : individualPlans){
				if (planIds.contains(individualPlan.getPlanId())){
					//individualPlan.setEnrollmentId(planEnrollment.get(individualPlan.getPlanId()));
					Float oldPremiumStr = oldPlanPremium.get(Integer.toString(individualPlan.getPlanId()));
					float oldPremiumVal = oldPremiumStr != null ? oldPremiumStr : 0;
					Float oldAptcStr = oldPlanAptc.get(Integer.toString(individualPlan.getPlanId()));
					float oldAptcVal = oldAptcStr != null ? oldAptcStr : 0;				
					individualPlan.setOldPremium(oldPremiumVal);
					individualPlan.setOldAptc(oldAptcVal);
					availablePlans.add(individualPlan);
				}
			}
		}		
	}

	public void getHouseholdPreferences(Map pref) {
		if(pref.containsKey("benefits")){
			pref.remove("benefits");
		}
	}
	
	public String getSAMLResponse(Map<String,String> samlParams){
		return planDisplayRestClient.getIdFromSAMLResponse(samlParams);
	}
	
	public FFMHouseholdResponse createConsumerFamily(int pldHouseholdId, ApplicantEligibilityData applicantEligibilityData,String partnerAssginedConsumerId){
		return null;
		//TODO: HS : Umcomment the consumer code once it gets integrated
		/*FFMHouseholdResponse ffmHouseholdResponse = new FFMHouseholdResponse();	
		//Household household = null;
		List<com.getinsured.hix.model.consumer.FFMMemberResponse> members = new ArrayList<com.getinsured.hix.model.consumer.FFMMemberResponse>();
		PldHousehold pldHousehold = planDisplayRestClient.findPldHouseholdById(pldHouseholdId);
		ffmHouseholdResponse = createConsumerHousehold(pldHousehold, partnerAssginedConsumerId);
		
		InsuranceApplicationType insuranceApplication = applicantEligibilityData.getApplicantEligibilityResponse().getInsuranceApplication();
		List<JAXBElement<? extends InsuranceApplicantType>> insuranceApplicantList = insuranceApplication.getInsuranceApplicant();
		for(JAXBElement<? extends InsuranceApplicantType> insuranceApplicantElement:insuranceApplicantList){
			if(insuranceApplicantElement.getValue()!=null){					
				com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType insuranceApplicant = (com.getinsured.hix.webservice.applicanteligibility.gov.cms.ffe.ee.applicant_eligibility.extension._1.InsuranceApplicantType) insuranceApplicantElement.getValue();
				XStream xstream = GhixUtils.getXStreamStaxObject();
				String insuranceApplicantXml = xstream.toXML(insuranceApplicant);
				com.getinsured.hix.model.consumer.FFMMemberResponse member = createConsumerMember(insuranceApplicantXml);
				if(ffmHouseholdResponse.getHouseHoldpersonId().equals(member.getPersonId())){
				  member.setIsHouseHoldContact(MemberBooleanFlag.YES);
				}
				members.add(member);
			}
		}
		
		ffmHouseholdResponse.setMembers(members);
		
		return ffmHouseholdResponse;*/
	} 
	
	/*private FFMHouseholdResponse createConsumerHousehold(PldHousehold pldHousehold, String partnerAssginedConsumerId){
		return null;
		//TODO: HS : Umcomment the consumer code once it gets integrated
		String  householdData = pldHousehold.getHouseholdData();
		Map householdDataMap = planDisplayRestClient.extractFFMPldHouseholdBlobData(householdData);
		FFMHouseholdResponse houseHold = new FFMHouseholdResponse();
		String houseHoldContactEmail = (String)householdDataMap.get("houseHoldContactEmail");
		String houseHoldContactMobileNo = (String)householdDataMap.get("houseHoldContactMobileNo");
		//String houseHoldContactFirstName = (String)householdDataMap.get("houseHoldContactFirstName");
		//String houseHoldContactLastName = (String)householdDataMap.get("houseHoldContactLastName");
		String countyCode = (String)householdDataMap.get("countyCode");
		String zipCode = (String)householdDataMap.get("zipCode");
		String stateCode = (String)householdDataMap.get("stateCode");
		
		houseHold.setAptc(Float.toString(pldHousehold.getMonthlyAptc()));
		houseHold.setCountyCode(countyCode);
		houseHold.setCsr(pldHousehold.getCostSharing());
		houseHold.setEmail(houseHoldContactEmail);
		houseHold.setPhoneNumber(houseHoldContactMobileNo);		
		houseHold.setState(stateCode);
		houseHold.setZipCode(zipCode);
		houseHold.setFfmHouseholdId(pldHousehold.getExternalId());
		houseHold.setGiHouseholdId(partnerAssginedConsumerId);
		houseHold.setHouseHoldpersonId((String)householdDataMap.get("houseHoldContactPersonID"));
		//houseHold.setFirstName(houseHoldContactFirstName);
		//houseHold.setLastName(houseHoldContactLastName);
		
		
		return houseHold;
	}*/
	
	/*private void populateMemberAddress(Location addressLocation, HashMap<String,Object> addressDataMap )
	{
		if(null!=addressDataMap.get("zipCode"))
		{	
			addressLocation.setZip((String)addressDataMap.get("zipCode"));
		}
		if(null!=addressDataMap.get("streetName"))
		{
			addressLocation.setAddress1((String)addressDataMap.get("streetName"));
		}
		if(null!=addressDataMap.get("cityName"))
		{
			addressLocation.setCity((String)addressDataMap.get("cityName"));
		}
		if(null!=addressDataMap.get("stateCode"))
		{
			addressLocation.setState((String)addressDataMap.get("stateCode"));
		}
		if(null!=addressDataMap.get("countyCode"))
		{
			addressLocation.setCountycode((String)addressDataMap.get("countyCode"));
		}
		
	}*/
	
	
	/*private com.getinsured.hix.model.consumer.FFMMemberResponse createConsumerMember(String insuranceApplicantXml){
		return null;
		//TODO: HS : Umcomment the consumer code once it gets integrated
		Map personDataMap = planDisplayRestClient.extractFFMPersonData(insuranceApplicantXml);
		com.getinsured.hix.model.consumer.FFMMemberResponse member = new com.getinsured.hix.model.consumer.FFMMemberResponse();
		
		String dob = (String)personDataMap.get("dob");
		Date dateOfBirth = dob!=null?DateUtil.StringToDate(dob,"MM/dd/yyyy"):null;
		String firstName = (String)personDataMap.get("firstName");
		String lastName = (String)personDataMap.get("lastName");
		String sexCode = (String)personDataMap.get("sexCode");
		String personId = (String)personDataMap.get("personId");
		
		Gender gender = null;
		if(sexCode!=null){
			gender = sexCode.equalsIgnoreCase("F")?Gender.F:Gender.M;
		}
		String maritalStatusCode = (String)personDataMap.get("maritalStatusCode");
		MemberBooleanFlag maritalFlag = null;
		if(maritalStatusCode!=null){
			maritalFlag = maritalStatusCode.equalsIgnoreCase("false")?MemberBooleanFlag.NO:MemberBooleanFlag.YES;
		}
		
		//String ssn = (String)personDataMap.get("ssn");
		
		if(personDataMap.get("cSREligibilityMap")!=null){
			Map cSREligibilityMap = (Map)personDataMap.get("cSREligibilityMap");
			String csr = cSREligibilityMap.get("costSharing")!=null?cSREligibilityMap.get("costSharing").toString():null;
			member.setCsrLevel(csr);
			if(cSREligibilityMap.get("eligibilityIndicator")!=null){
				member.setCsrEligibilityIndicator(cSREligibilityMap.get("eligibilityIndicator").toString().equalsIgnoreCase("false")?MemberBooleanFlag.NO:MemberBooleanFlag.YES);
			}
			if(cSREligibilityMap.get("startDate")!=null){
				String startDate = cSREligibilityMap.get("startDate").toString();
				member.setCsrEligibilityStartDate(DateUtil.StringToDate(startDate,"yyyy-MM-dd"));
			}
			if(cSREligibilityMap.get("endDate")!=null){
				String endDate = cSREligibilityMap.get("endDate").toString();
				member.setCsrEligibilityEndDate(DateUtil.StringToDate(endDate,"yyyy-MM-dd"));
			}
			
		}
		
		if(personDataMap.get("aPTCEligibilityMap")!=null){
			Map aPTCEligibilityMap = (Map)personDataMap.get("aPTCEligibilityMap");
			if(aPTCEligibilityMap.get("eligibilityIndicator")!=null){
				member.setAptcEligibilityIndicator(aPTCEligibilityMap.get("eligibilityIndicator").toString().equalsIgnoreCase("false")?MemberBooleanFlag.NO:MemberBooleanFlag.YES);
			}
			if(aPTCEligibilityMap.get("startDate")!=null){
				String startDate = aPTCEligibilityMap.get("startDate").toString();
				member.setAptcEligibilityStartDate(DateUtil.StringToDate(startDate,"yyyy-MM-dd"));
			}
			if(aPTCEligibilityMap.get("endDate")!=null){
				String endDate = aPTCEligibilityMap.get("endDate").toString();
				member.setAptcEligibilityEndDate(DateUtil.StringToDate(endDate,"yyyy-MM-dd"));
			}
			
		}
		
		if(personDataMap.get("chipEligibility")!=null){
			com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean chipEligibilityIndicator = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean) personDataMap.get("chipEligibility");
			if(chipEligibilityIndicator.isValue())
			{
			member.setChipEligibilityIndicator(MemberBooleanFlag.YES);
			}
		}
		if(personDataMap.get("medicaidEligibility")!=null){
			com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean medicaidEligibilityIndicator = (com.getinsured.hix.webservice.applicanteligibility.gov.niem.niem.proxy.xsd._2.Boolean) personDataMap.get("medicaidEligibility");
			if(medicaidEligibilityIndicator.isValue())
			{
			member.setMedicaidEligibilityIndicator(MemberBooleanFlag.YES);
			}
		}
				
		if(personDataMap.get("exchangeEligibilityMap")!=null){
			Map exchangeEligibilityMap = (Map)personDataMap.get("exchangeEligibilityMap");
			if(exchangeEligibilityMap.get("programEligibilityStatusCode")!=null){
				member.setEligibilityStatus(exchangeEligibilityMap.get("programEligibilityStatusCode").toString().equalsIgnoreCase("NEW")?EligibilityStateValues.INITIAL_ELIGIBILITY_NEEDED:EligibilityStateValues.INITIAL_ELIGIBILITY_COMPLETE);
			}
			if(exchangeEligibilityMap.get("startDate")!=null){
				String startDate = exchangeEligibilityMap.get("startDate").toString();
				member.setEligibilityStartDate(DateUtil.StringToDate(startDate,"yyyy-MM-dd"));
			}
			if(exchangeEligibilityMap.get("endDate")!=null){
				String endDate = exchangeEligibilityMap.get("endDate").toString();
				member.setEligibilityEndDate(DateUtil.StringToDate(endDate,"yyyy-MM-dd"));
			}
			
		}
		
		String ffeApplicantId= personDataMap.get("exchangeAssignedInsuranceApplicantIdentificationId")!=null?personDataMap.get("exchangeAssignedInsuranceApplicantIdentificationId").toString():"0";
		member.setFfeApplicantId(Long.parseLong(ffeApplicantId));
		member.setBirthDate(dateOfBirth);
		member.setFirstName(firstName);
		member.setGender(gender);
		member.setLastName(lastName);
		member.setMaritalStatus(maritalFlag);
		member.setPersonId(personId);
		if(null!=personDataMap.get("personContactInfoMap"))
		{
			Location homeLocation=new Location();
		populateMemberAddress(homeLocation,(HashMap<String,Object>)personDataMap.get("personContactInfoMap"));
		member.setHomeAddress(homeLocation);
		}
		if(null!=personDataMap.get("personMailingAddress")){
			Location mailingLocation=new Location();
		populateMemberAddress(mailingLocation,(HashMap<String,Object>)personDataMap.get("personMailingAddress"));
		member.setMailingAddress(mailingLocation);
		}
		
		return member;
	}*/
	

	public String insertDashInPartnerAssignedConsumerID(String partnerAssignedConsumerID)
	{
		String partnerId = partnerAssignedConsumerID;
		if(partnerId!=null){
			partnerId = insertDash(partnerId, 20);
            partnerId = insertDash(partnerId, 16);
            partnerId = insertDash(partnerId, 12);
            partnerId = insertDash(partnerId, 8);
		}
        return partnerId;
        
	}
	
	private String insertDash(String partnerId, int position) {
		partnerId = partnerId.substring(0, position) + "-" + partnerId.substring(position, partnerId.length());
		return partnerId;
	}

	public  static Map<String,String> convertIntegerToStringMap(Map integreMap)
    {
           Iterator iterator = integreMap.entrySet().iterator();
           Map<String, String> stringMap = new HashMap<String, String>();
           while (iterator.hasNext())
           {
        	   Entry entry = (Entry) iterator.next();
        	   String key = entry.getKey().toString();
        	   Integer val = (entry.getValue()!=null)?Integer.valueOf(entry.getValue().toString()):0;
        	   stringMap.put(key, val.toString());
           }
           return stringMap;
    }

	public List<String> getNonEligiblePersonNameList(List<Map<String,String>> personMapList, List<Map<String,String>> personNameMapList){
		List<String> nonEligiblePersonNameList = new ArrayList<String>();
		List<String> eligiblePersonNameList = new ArrayList<String>();
		
		for(Map<String,String> personNameMap : personMapList){
			eligiblePersonNameList.add(personNameMap.get("FIRSTNAME"));
		}
		
		if(personNameMapList!=null){
			for(Map<String,String> personNameMap : personNameMapList){
				String firstName = personNameMap.get("firstName");
				String lastName = personNameMap.get("lastName")!=null?personNameMap.get("lastName"):"";
				if(firstName!=null && !eligiblePersonNameList.contains(firstName)){
					nonEligiblePersonNameList.add(firstName + " " +lastName);
				}				
			}
		}		
		return nonEligiblePersonNameList;
	}
	
	public String getNonEligiblePersonNames(List<String> memberNames) 
	{		
		StringBuffer memberNamesStr = new StringBuffer();
		for (String memberName : memberNames){
			memberNamesStr.append(memberName);
			memberNamesStr.append(", ");
		}
		if (memberNames.size() > 0){
			memberNamesStr.deleteCharAt(memberNamesStr.lastIndexOf(","));
			memberNamesStr.deleteCharAt(memberNamesStr.length()-1);
		}
		if(memberNames.size() > 1){
			memberNamesStr.replace(memberNamesStr.lastIndexOf(","), memberNamesStr.lastIndexOf(",") + 1, " and" );
		}		
		return memberNamesStr.toString();
	}
	
	public String prepareQuotedandNonQuotedMembersString(Map<String, Integer> quotedMembers)
	{
		String quotedMembersString = "";
		int eligibleAdultCnt = quotedMembers.get("eligibleAdultCnt");
		int eligibleChildCnt = quotedMembers.get("eligibleChildCnt");
		int inEligibleAdultCnt = quotedMembers.get("inEligibleAdultCnt");
		int inEligibleChildCnt = quotedMembers.get("inEligibleChildCnt");
		
		if(eligibleAdultCnt == 1)
		{
			quotedMembersString = eligibleAdultCnt + " adult";
		}
		if(eligibleAdultCnt > 1)
		{
			quotedMembersString = eligibleAdultCnt + " adults";
		}
		if(eligibleAdultCnt > 0 && eligibleChildCnt > 0)
		{
			quotedMembersString = quotedMembersString + " and ";
		}
		if(eligibleChildCnt == 1)
		{
			quotedMembersString = quotedMembersString + eligibleChildCnt + " child";
		}
		if(eligibleChildCnt > 1)
		{
			quotedMembersString = quotedMembersString + eligibleChildCnt + " children";
		}
		if((eligibleAdultCnt > 0 || eligibleChildCnt > 0) && (inEligibleAdultCnt > 0 || inEligibleChildCnt > 0)) 
		{
			quotedMembersString = quotedMembersString + ", excluding ";
		}
		if((eligibleAdultCnt == 0 && eligibleChildCnt == 0) && (inEligibleAdultCnt > 0 || inEligibleChildCnt > 0)) 
		{
			quotedMembersString = " Excluding ";
		}
		if(inEligibleAdultCnt == 1)
		{
			quotedMembersString = quotedMembersString + inEligibleAdultCnt + " adult";
		}
		if(inEligibleAdultCnt > 1)
		{
			quotedMembersString = quotedMembersString + inEligibleAdultCnt + " adults";
		}
		if(inEligibleAdultCnt > 0 && inEligibleChildCnt > 0)
		{
			quotedMembersString = quotedMembersString + " and ";
		}
		if(inEligibleChildCnt == 1)
		{
			quotedMembersString = quotedMembersString + inEligibleChildCnt + " child";
		}
		if(inEligibleChildCnt > 1)
		{
			quotedMembersString = quotedMembersString + inEligibleChildCnt + " children";
		}
		
		return quotedMembersString.concat(".");
	}
	
	public int getAgeFromCoverageDate(String coverageStartDate, String dateOfBirth){
		Calendar today = TSCalendar.getInstance();
		today.setTime(DateUtil.StringToDate(coverageStartDate,DATE_FORMAT));
		Calendar dob = TSCalendar.getInstance();
		dob.setTime(DateUtil.StringToDate(dateOfBirth,DATE_FORMAT));
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) 
			|| (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			age--;
		}
		
		return age;
	}
	
	public List<IndividualPlan> getCheapestPlansFromCarriers(List<IndividualPlan> plansWithCost) {
		int size = plansWithCost.size();
		List<IndividualPlan> updatedPlansList = new ArrayList<IndividualPlan>();
		//Sort the list according to premium.
		if(size >1){
			Collections.sort(plansWithCost,new PlanPremiumComparator());
		}
		
		if(size >PlanDisplayConstants.MIN_PLAN_SIZE){
			updatedPlansList.add(plansWithCost.get(0));
			for (int j = 1 ; j < size;j++){
				if(!(plansWithCost.get(0).getIssuerText().equalsIgnoreCase(plansWithCost.get(j).getIssuerText()))){
					boolean flag = false;
					for(int i = 1 ; i < updatedPlansList.size(); i++){
						if(plansWithCost.get(j).getIssuerText().equalsIgnoreCase(updatedPlansList.get(i).getIssuerText())){
							flag = true;
						}
					}
					if(!flag){
						updatedPlansList.add(plansWithCost.get(j));
					}
				}
				
				if(updatedPlansList.size() == PlanDisplayConstants.MIN_PLAN_SIZE){
					break;
				}
			}
			
			//Add the same issuer again.
			if(updatedPlansList.size() < PlanDisplayConstants.MIN_PLAN_SIZE){
				for(int j = 1; j< size ; j++){
					boolean isPlanAdded = false;
					for (int i = 1; i <updatedPlansList.size(); i++){
						if(plansWithCost.get(j).getPlanId().equals(updatedPlansList.get(i).getPlanId())){
							isPlanAdded = true;
						}
					}
					
					if(!isPlanAdded){
						updatedPlansList.add(plansWithCost.get(j));
					}
					
					if(updatedPlansList.size() == PlanDisplayConstants.MIN_PLAN_SIZE){
						break;
					}
				}
			}
		}else{
			updatedPlansList = plansWithCost;
		}
		
		if(size>PlanDisplayConstants.MIN_PLAN_SIZE){
			plansWithCost.removeAll(updatedPlansList);
			updatedPlansList.addAll(plansWithCost);
		}
		
		return updatedPlansList;
	}
	
	class PlanPremiumComparator implements Comparator<IndividualPlan>{
		@Override
		public int compare(IndividualPlan o1, IndividualPlan o2) {
			return o1.getPremium()<o2.getPremium() ? -1 : 1;
		}
	}
	
	
	public List<PersonData> createDentalCensusList(List<PersonData> personList, String quotingGroup, String coverageStartDate) 
	{
		List<PersonData> dentalCensusList = new ArrayList<PersonData>();
		
		if ("Family".equalsIgnoreCase(quotingGroup) || "None".equalsIgnoreCase(quotingGroup)){
			return personList;
		}			
		
		if ("Adult".equalsIgnoreCase(quotingGroup)) {
			for (PersonData member : personList) {
				int age = getAgeFromCoverageDate(coverageStartDate, member.getDob());
				if (age >= PlanDisplayConstants.CHILD_AGE) {
					dentalCensusList.add(member);
				}
			}
		} else if ("Child".equalsIgnoreCase(quotingGroup)) {
			for (PersonData member : personList) {
				int age = getAgeFromCoverageDate(coverageStartDate, member.getDob());
				if (age < PlanDisplayConstants.CHILD_AGE) {
					dentalCensusList.add(member);
				}
			}
		}
		return dentalCensusList;
	}
			
	/*private String getQuotedMemberNames(List<PersonData> dentalCensusList) 
	{
		StringBuilder memberNames = new StringBuilder();
		for(PersonData member : dentalCensusList){
 	 		String firstName = member.getFirstname();
 	 		memberNames.append(firstName).append(", ");
 	 	}
 	 	if (memberNames.lastIndexOf(",") > 0){
			memberNames.deleteCharAt(memberNames.lastIndexOf(","));
			memberNames.deleteCharAt(memberNames.length()-1);
		}
		if(memberNames.lastIndexOf(",") > 0){
			memberNames.replace(memberNames.lastIndexOf(","), memberNames.lastIndexOf(",") + 1, " and" );
		}	
		
		return memberNames.toString();
	}
	
	private String getQuotedMemberSize(String coverageStartDate, List<PersonData> dentalCensusList)
	{
		String adult = "";
		String child = "";
		int childCount = 0;
		int adultCount = 0;
		StringBuilder memberSize = new StringBuilder();
				
		for(PersonData member : dentalCensusList)
		{
			int age = getAgeFromCoverageDate(coverageStartDate, member.getDob());
			if (age < PlanDisplayConstants.CHILD_AGE) {
				childCount++;
			}else{
				adultCount++;
			}
 	 	}
		
		switch (adultCount) {
		case 0:
			adult = "";
			break;
		case 1:
			adult = adultCount + " Adult";
			break;
		default:
			adult = adultCount + " Adults";
			break;
		}
		
		switch (childCount) {
		case 0:
			child = "";
			break;
		case 1:
			child = childCount + " Child";
			break;
		default:
			child = childCount + " Childs";
			break;
		}	
		
		if(!adult.isEmpty() && !child.isEmpty()){
			memberSize.append(adult).append(" and ").append(child);
		}else{
			memberSize.append(adult).append(child);
		}
				
		return memberSize.toString();
	}*/
	
	public Map<String,String> getDentalQuotes(float aptc, String healthPlanName, int totalDentalPlans, String programType, String quotingGroup, String isAdultCovered, String isChildCovered, Boolean isAdultOnly) 
	{		
		Map<String,String> quoteMap = new HashMap<String,String>();
		String title = "Shop for Dental";		
		String iDSpecificMessage = "You may purchase adult dental plans directly from dental carriers after you complete the purchase of your qualified health plan. When you are ready to enroll, click 'CHECKOUT'";
		
		StringBuffer bodyMessageTmp = new StringBuffer();
		StringBuffer subHeadingTmp = new StringBuffer();
		StringBuffer subHeadingTmp2 = new StringBuffer();
				
		if(totalDentalPlans > 0)
		{
			if(QUOTING_GROUP_FAMILY.equalsIgnoreCase(quotingGroup))
			{
				bodyMessageTmp.append("Family dental plans cover all members in your household.");
			}
			else if(QUOTING_GROUP_CHILD.equalsIgnoreCase(quotingGroup))
			{
				bodyMessageTmp.append("Pediatric dental plans cover individuals under 19 years old. ");
				if (PROGRAM_TYPE_INDIVIDUAL.equalsIgnoreCase(programType)) {
					bodyMessageTmp.append("<br/>");
					bodyMessageTmp.append(iDSpecificMessage);
				}
			}
			else if(QUOTING_GROUP_ADULT.equalsIgnoreCase(quotingGroup))
			{
				bodyMessageTmp.append("Adult dental plans cover individuals over 19 years old. ");
			}
			if(aptc > 0)
			{	
				int indexOfCover=bodyMessageTmp.indexOf("cover");
				if(PROGRAM_TYPE_SHOP.equalsIgnoreCase(programType))
				{
					bodyMessageTmp.insert(indexOfCover," marked down by $"+aptc+" of employer contribution,");
				}
				else if(PROGRAM_TYPE_INDIVIDUAL.equalsIgnoreCase(programType))
				{
					bodyMessageTmp.insert(indexOfCover," marked down by $"+aptc+" of leftover credits,");
				}
			}
		}
		else
		{
			if(PROGRAM_TYPE_INDIVIDUAL.equalsIgnoreCase(programType) && isAdultOnly) {
				bodyMessageTmp.append(iDSpecificMessage);
			}
			else {
				bodyMessageTmp.append("Sorry, no plans are available.");
			}
		}
		
		subHeadingTmp.append("Your health plan, ");
		subHeadingTmp.append(healthPlanName);
		String missingDentalBenifit = "false";
		
		if (isAdultCovered != null && isChildCovered != null)
		{
			if (isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED) && isChildCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED)) 
			{
				missingDentalBenifit = "true";
				subHeadingTmp.append(", already includes family dental benefit.");
				subHeadingTmp2.append("Please carefully review your dental options to avoid duplication of coverage");
			}
			else if (isChildCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED))
			{
				subHeadingTmp.append(", already includes pediatric dental benefit.");
				subHeadingTmp2.append("Please carefully review your dental options to avoid duplication of coverage");			
			}
			else if (isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED))
			{
				subHeadingTmp.append(", already includes adult dental benefit. ");
				subHeadingTmp2.append("Please carefully review your dental options to avoid duplication of coverage");
			}
			else
			{
				missingDentalBenifit="true";
				subHeadingTmp.append(", does not include dental benefit. Get it here!");
			}
		}
		
		if(PROGRAM_TYPE_INDIVIDUAL.equalsIgnoreCase(programType) && isAdultOnly) {
			subHeadingTmp.delete(0, subHeadingTmp.length());
		}
		
		quoteMap.put("title", title);
		quoteMap.put("subHeading", subHeadingTmp.toString());
		quoteMap.put("subHeading2", subHeadingTmp2.toString());
		quoteMap.put("bodyMessage", bodyMessageTmp.toString());
		quoteMap.put("missingDentalBenifit", missingDentalBenifit);
		
		return quoteMap;
	}
	
	public List<IndividualPlan> getIndividualPlansFromCart(HttpSession session) 
	{
		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside getIndividualPlansFromCart()---->", null, false);
		SessionData sessionData = (SessionData)session.getAttribute("sessionData");
		Integer householdId = sessionData.getHouseholdId();		
		GroupData groupData = sessionData.getGroupData();
		
		Map<String,String> household = new HashMap<String,String>();		
		Map<String,Integer> preferences = new HashMap<String,Integer>();	
		Map<String,Object> subscriberData = new HashMap<String,Object>();
		String coverageStartDate = sessionData.getEnrollmentCoverageStartDate();
		String tenant = "";
		String planType = "";
		String planLevel = "";
		String programType = "";
		String renewalType = "";
		String exchangeType = "";
		String eligLeadBenefits = "";
 	 	String totalContribution = "";
 	 	String isSpecialEnrollment = "";
		Boolean showCatastrophicPlan = Boolean.TRUE;
 	 	
		List<IndividualPlan> cartItemsList = planDisplayRestClient.getCartItems(preferences, household, coverageStartDate, groupData, householdId, "CART", planLevel, showCatastrophicPlan, planType, programType, isSpecialEnrollment, subscriberData, eligLeadBenefits, tenant, totalContribution, renewalType, exchangeType);
		
		if(cartItemsList != null && !cartItemsList.isEmpty())
		{
			for(IndividualPlan plan : cartItemsList)
			{
				if("DENTAL".equalsIgnoreCase(plan.getPlanType()))
				{
					Float oldAptc = sessionData.getOldDentalAptc();
					Float oldPremium = sessionData.getOldDentalPremium();
					plan.setOldAptc(oldAptc);
					plan.setOldPremium(oldPremium);
				}
				else
				{
					Float oldAptc = sessionData.getOldHealthAptc();
					Float oldPremium = sessionData.getOldHealthPremium();
					plan.setOldAptc(oldAptc);
					plan.setOldPremium(oldPremium);
				}
			}
		}
		return cartItemsList;
	}
	
	public String prepareQuotedNameForAnonymous(List<Map<String,String>> personList, String coverageStartDate){
			
			int eligibleAdultCnt = 0;
			int eligibleChildCnt = 0;
			String quotedMembersString = "";
			
			Map<String, Integer> quotedMembers = new HashMap<String, Integer>();
			quotedMembers.put("eligibleAdultCnt", 0);
			quotedMembers.put("eligibleChildCnt", 0);
			Date coverageStart = DateUtil.StringToDate(coverageStartDate, GhixConstants.REQUIRED_DATE_FORMAT);
			
			for(Map<String,String> person : personList)
			{
				Date dob = DateUtil.StringToDate(person.get(PlanDisplayUtil.PARAM_DOB), GhixConstants.REQUIRED_DATE_FORMAT);
				boolean childFlag = getChildFlag(dob, coverageStart);
				if(!childFlag){
					quotedMembers.put("eligibleAdultCnt", ++eligibleAdultCnt);
				}else{
					quotedMembers.put("eligibleChildCnt", ++eligibleChildCnt);
				}
			}
							
			if(eligibleAdultCnt == 1){
				quotedMembersString = eligibleAdultCnt + " adult";
			}
			if(eligibleAdultCnt > 1){
				quotedMembersString = eligibleAdultCnt + " adults";
			}
			if(eligibleAdultCnt > 0 && eligibleChildCnt > 0){
				quotedMembersString = quotedMembersString + " and ";
			}
			if(eligibleChildCnt == 1){
				quotedMembersString = quotedMembersString + eligibleChildCnt + " child";
			}
			if(eligibleChildCnt > 1){
				quotedMembersString = quotedMembersString + eligibleChildCnt + " children";
			}
			
			return quotedMembersString.concat(".");		
		}
	
	public Map<String, Float> formInputData(final List<Member> members) {
		// TODO Auto-generated method stub
		/*Map<String, Float> inputData = new HashMap<String, Float>();
		inputData.put("2739", new Float(4.53));
		inputData.put("2738", new Float(6.53));
		inputData.put("2737", new Float(7.53));*/
		final Map<String,Float> premiumData  = new HashMap<String, Float>();
		for (final Member member : members)
		{
			BigDecimal bd = new BigDecimal(Float.toString(member.getBenchmarkPremium()));
	        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
			premiumData.put(member.getMemberID(), bd.floatValue());
		}
		return premiumData;
	}
	
	public List<PersonData> extractPersonList(List<Map<String,String>> personList)
	{
		List<PersonData> personDataList = new ArrayList<PersonData>();
		for(Map<String,String> person : personList)
		{
			PersonData personData = new PersonData();
			personData.setPersonId(""+person.get("pldPersonId"));
			personData.setExternalPersonId(""+person.get("externalPersonId"));
			personData.setExistingMedicalEnrollmentID(""+person.get("existingMedicalEnrollmentID"));
			personData.setExistingSADPEnrollmentID(""+person.get("existingSADPEnrollmentID"));
			personData.setFirstname(person.get("FIRSTNAME"));
			personData.setLastname(person.get("LASTNAME"));
			personData.setDob(person.get("DOB"));
			personData.setCoverageStartDate(person.get("coverageStartDate"));
			personData.setNewPersonFLAG(person.get("newPersonFLAG"));
			personData.setMaintenanceReasonCode(person.get("maintenanceReasonCode"));
			personData.setDentalEligible(SessionData.YESorNO.NO.toString());
			if("Y".equalsIgnoreCase(person.get("SMOKER"))){
				personData.setSmoker(SessionData.YorN.Y.toString());
			}
			else{
				personData.setSmoker(SessionData.YorN.N.toString());
			}			
			if("Self".equalsIgnoreCase(person.get("RELATIONSHIP"))){
				personData.setRelationship(SessionData.Relationship.Self.toString());
			}
			else if("Spouse".equalsIgnoreCase(person.get("RELATIONSHIP"))){
				personData.setRelationship(SessionData.Relationship.Spouse.toString());
			}
			else{
				personData.setRelationship(SessionData.Relationship.Child.toString());
			}
			personDataList.add(personData);
		}	
		return personDataList;		
	}
	
	public void deleteItemsFromCart(String currentHouseholdId, String currentGroupId) 
	{
		final Map<String, String> inputData = new HashMap<String, String>();		
		inputData.put("householdId", currentHouseholdId);		
		inputData.put(PARAM_CURRENT_GRP_ID, currentGroupId);
		String commaSeparatedId = planDisplayRestClient.checkItemInCart(inputData); 
		String orderItemId = "0";
		if(null!=commaSeparatedId && !"0".equals(commaSeparatedId))
		{
			String[] arrayOfIds = commaSeparatedId.split(",");
			for(int i=1;i<arrayOfIds.length;i=i+2)
			{
				orderItemId =  arrayOfIds[i];
				final int itemId = Integer.parseInt(orderItemId);
				planDisplayRestClient.deleteOrderItem(itemId);
			}
		}
	}
	
	private void createEnrollmntIdsAndMemberNames(PersonData individualPerson, String keyEnrollmentId, String enrollmentType, 
			boolean isNMDental, String coverateStart, Set<HashMap<String, String>> enrollmentIds, List<String> memberNames, String insuranceType) {
		if(enrollmentType!=null){
			HashMap<String, String> idTypeMap = new HashMap<String, String>();
			if(!isNMDental){
				idTypeMap.put("insuranceType", PlanInsuranceType.HEALTH.toString());
				idTypeMap.put("enrollmentId", individualPerson.getExistingMedicalEnrollmentID().toString());
				enrollmentIds.add(idTypeMap);
			}else{ // In case of NM existing Dental Id would be present only for child.
				final Date converageDate = DateUtil.StringToDate(coverateStart,GhixConstants.REQUIRED_DATE_FORMAT );
				final Date personDob = DateUtil.StringToDate(individualPerson.getDob(),GhixConstants.REQUIRED_DATE_FORMAT );
				final float age = getAgeFromDOB(personDob,converageDate);
				if(individualPerson.getExistingSADPEnrollmentID()!= null || age < 19){
					idTypeMap.put("insuranceType", PlanInsuranceType.DENTAL.toString());
					idTypeMap.put("enrollmentId", individualPerson.getExistingSADPEnrollmentID().toString());
					enrollmentIds.add(idTypeMap);
				}
			}
		}		
	}
	
	public String keepShopPlans(List<PersonData> individualPersonList, String currentHousholdId, String sadpFlag, String currentGroupId, String quotingGroup, List<IndividualPlan> availablePlans) 
	{
		String status = "";
		final Map<String, Object> inputDataMap = new HashMap<String, Object>();
		inputDataMap.put("individualPersonList", individualPersonList);
		inputDataMap.put("householdId", currentHousholdId);
		inputDataMap.put("SADP_FLAG", sadpFlag);	
		inputDataMap.put(PARAM_CURRENT_GRP_ID, currentGroupId);
		inputDataMap.put("multipleItemsEntry", DynamicPropertiesUtil.getPropertyValue("planSelection.MultipleItemsEntry"));	
		
		for(IndividualPlan availablePlan : availablePlans)
		{
			inputDataMap.put("PLAN_ID", availablePlan.getPlanId());
			BigDecimal bigDecimal=new BigDecimal(Float.toString(availablePlan.getPremiumAfterCredit()));
			bigDecimal=bigDecimal.setScale(2, BigDecimal.ROUND_HALF_UP);
			inputDataMap.put("PREMIUM", bigDecimal.floatValue());
			inputDataMap.put("PREMIUM_BEFORE_CREDIT", availablePlan.getPremiumBeforeCredit());
			inputDataMap.put("QUOTING_DATA", availablePlan.getPlanDetailsByMember());
			inputDataMap.put("QUOTING_GROUP", quotingGroup);
			inputDataMap.put("CONTRIBUTION", availablePlan.getContributionData());
			inputDataMap.put("MONTHLY_APTC", availablePlan.getAptc());
			status = planDisplayRestClient.keepPlan(inputDataMap);				
		}	
		return status;
	}
	
	public void getAvailablePlans(List<IndividualPlan> individualPlans, List<IndividualPlan> availablePlans, Set<Integer> planIds, Map<Integer,Integer> planEnrollment,
			Map<String,Float> oldPlanPremium, Map<String,Float> oldPlanAptc, SessionData sessionData) 
	{		
		if(!individualPlans.isEmpty())
		{
			for(IndividualPlan individualPlan : individualPlans)
			{
				if(planIds.contains(individualPlan.getPlanId())) 
				{
					if(planEnrollment!=null) {
						individualPlan.setEnrollmentId(planEnrollment.get(individualPlan.getPlanId()));
					}
					final Float oldPremiumStr = oldPlanPremium.get(Integer.toString(individualPlan.getPlanId()));
					final Float oldAptcStr = oldPlanAptc.get(Integer.toString(individualPlan.getPlanId()));
					individualPlan.setOldPremium(oldPremiumStr);
					individualPlan.setOldAptc(oldAptcStr);
					if("DENTAL".equals(individualPlan.getPlanType())) {
						sessionData.setOldDentalAptc(oldAptcStr);
						sessionData.setOldDentalPremium(oldPremiumStr);
					} else {
						sessionData.setOldHealthAptc(oldAptcStr);
						sessionData.setOldHealthPremium(oldPremiumStr);
					}					
					availablePlans.add(individualPlan);
				}
			}			
		}		
	}	
	
	public Map<String, Integer> formPreferencesMap(final int familySize, final String docVisitFrequency, final String noOfPrescriptions) 
	{
		final Map<String, Integer> pref = new HashMap<String, Integer>();
		
		pref.put(GhixConstants.LOW_DRUG_USE_VAL,0);
		pref.put(GhixConstants.MODERATE_DRUG_USE_VAL,0);
		pref.put(GhixConstants.HIGH_DRUG_USE_VAL,0);
		pref.put(GhixConstants.V_HIGH_DRUG_USE_VAL,0);

		pref.put(GhixConstants.LOW_MEDICAL_VAL,0);
		pref.put(GhixConstants.MODERATE_MEDICAL_VAL,0);
		pref.put(GhixConstants.HIGH_MEDICAL_VAL,0);
		pref.put(GhixConstants.V_HIGH_MEDICAL_VAL,0);

		if(docVisitFrequency!=null){
			if ("1".equals(docVisitFrequency))
			{
				pref.put(GhixConstants.LOW_MEDICAL_VAL,familySize);
			}else if ("2".equals(docVisitFrequency))
			{
				pref.put(GhixConstants.MODERATE_MEDICAL_VAL,familySize);
			}else if ("5".equals(docVisitFrequency))
			{
				pref.put(GhixConstants.HIGH_MEDICAL_VAL,familySize);
			}else if ("10".equals(docVisitFrequency))
			{
				pref.put(GhixConstants.V_HIGH_MEDICAL_VAL,familySize);
			}
		}

		if(noOfPrescriptions!=null){
			if ("1".equals(noOfPrescriptions))
			{
				pref.put(GhixConstants.LOW_DRUG_USE_VAL,familySize);
			}else if ("2".equals(noOfPrescriptions))
			{
				pref.put(GhixConstants.MODERATE_DRUG_USE_VAL,familySize);
			}else if ("5".equals(noOfPrescriptions))
			{
				pref.put(GhixConstants.HIGH_DRUG_USE_VAL,familySize);
			}else if ("10".equals(noOfPrescriptions))
			{
				pref.put(GhixConstants.V_HIGH_DRUG_USE_VAL,familySize);
			}
		}

		return pref;
	}
	
	public void getPreSSAPIndividualPlans(int oldPldHouseholdId, int oldPldGroupId, String coverageStart, Set<Integer> planIds, Map<String,Float> oldPlanAptc, List<IndividualPlan> oldPlans, Map<String,Float> oldPlanPremium, Map<String,String> oldPlanQuotingGroup, Set<Map<String,String>> planIdAndInsTypeSet) 
	{
		List<Map<String,String>> preshoppingPlansMapList = getPreshoppingPlans(String.valueOf(oldPldHouseholdId), String.valueOf(oldPldGroupId));
		for(Map<String,String> preshoppingPlanIdAndOrderId : preshoppingPlansMapList)
		{
			Map<String,Object> oldplanData = planDisplayRestClient.findPldOrderItemById(Integer.parseInt(preshoppingPlanIdAndOrderId.get("orderItemId")));
			oldPlanPremium.put(preshoppingPlanIdAndOrderId.get("planId"), Float.valueOf(oldplanData.get("planPremium").toString()));
		    oldPlanAptc.put(preshoppingPlanIdAndOrderId.get("planId"), Float.valueOf(oldplanData.get("aptc").toString()));
		    oldPlanQuotingGroup.put(preshoppingPlanIdAndOrderId.get("planId"), oldplanData.get("quotingGroup").toString());
		    
		    String formattedCoverageDate = DateUtil.changeFormat(coverageStart, DATE_FORMAT, "yyyy-MM-dd");
		    List<IndividualPlan> oldHealthPlans = planDisplayRestClient.getPlanInfo(preshoppingPlanIdAndOrderId.get("planId"), formattedCoverageDate, true);	
		    if(null != oldHealthPlans && !oldHealthPlans.isEmpty())
		    {
				getOldPlanData(oldHealthPlans, planIds, Float.valueOf(oldplanData.get("planPremium").toString()), Float.valueOf(oldplanData.get("aptc").toString()));
				oldPlans.addAll(oldHealthPlans);
				Map<String,String> planIdAndInsTypeMap = new HashMap<String,String>();
				planIdAndInsTypeMap.put("oldPlanId", preshoppingPlanIdAndOrderId.get("planId"));
				planIdAndInsTypeMap.put("oldPlanInsType", oldHealthPlans.get(0).getPlanType());					
				planIdAndInsTypeSet.add(planIdAndInsTypeMap);
		    }		    
		}		
	}

	public PldHousehold getOldHousehold(String leadId) 
	{
		Map<String,String> preShoppingHouseholdMap = new HashMap<String,String>();	
		preShoppingHouseholdMap.put("leadId", leadId);
		preShoppingHouseholdMap.put("requestType", "PRESHOPPING");
		PldHousehold preShoppingHousehold = planDisplayRestClient.findLatestPldHouseholdByLeadId(preShoppingHouseholdMap);
		
		return preShoppingHousehold;
	}

	public List<Map<String,String>> getPreshoppingPlans(String preShoppingHouseholdId, String oldPldGroupId) 
	{
		final Map<String,String> inputData = new HashMap<String,String>();		
		inputData.put("householdId", preShoppingHouseholdId);		
		inputData.put("CURRENT_GROUP_ID", oldPldGroupId);
		String commaSeparatedId = planDisplayRestClient.checkItemInCart(inputData); 
		List<Map<String,String>> list = new ArrayList<Map<String,String>>();
		if(null!=commaSeparatedId && !"0".equals(commaSeparatedId))	
		{			
			String[] arrayOfIds = commaSeparatedId.split(",");
			for(int i=0;i<arrayOfIds.length;i=i+2)
			{
				Map<String,String> planIdAndOrderIdMap = new HashMap<String,String>();
				planIdAndOrderIdMap.put("planId", arrayOfIds[i]);
				planIdAndOrderIdMap.put("orderItemId", arrayOfIds[i+1]);
				list.add(planIdAndOrderIdMap);
			}			
		}
		return list;
	}
	
	public String isSubscriberZipChanged(int oldHouseholdId, Integer newHouseholdId, String enrollmentType, String programType) 
	{
		String changeInZipCode = "N";
		final Map<String, Object> oldSubscriberData = getSubscriberData(oldHouseholdId, enrollmentType, programType, "FAMILY");
		final Map<String, Object> newSubscriberData = getSubscriberData(newHouseholdId, enrollmentType, programType, "FAMILY");
		String oldZipCode = null;
		String newZipCode = null;
		if (oldSubscriberData != null){
			 oldZipCode = (String)oldSubscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP);
		}
		if (newSubscriberData != null){
			 newZipCode = (String)newSubscriberData.get(GhixConstants.SUBSCRIBER_MEMBER_ZIP);
		}
		
		if(oldZipCode!=null && !oldZipCode.equalsIgnoreCase(newZipCode)){
			changeInZipCode = "Y";
		}
		return changeInZipCode;
	}
	public long startMethodLog(String methodname)
    {
           long startTime = TimeShifterUtil.currentTimeMillis();
           putLoggerStmt(LOGGER, LogLevel.INFO, "<----Method Start----> "+methodname +" - startTime "+startTime, null, false);
           return startTime;
    }
    public void endMethodLog(String methodname, long startTime)
    {
           long endTime = TimeShifterUtil.currentTimeMillis();
           putLoggerStmt(LOGGER, LogLevel.INFO, "<----Method End----> "+methodname +" - endTime "+endTime, null, false);
           putLoggerStmt(LOGGER, LogLevel.INFO, "<----Total execution time----> "+methodname +" : "+ (endTime - startTime), null, false);
    }
	
    /**
  	 * 
  	 * @param memberList
  	 * @param personList
  	 * @return List<PersonData
  	 */
  	public List<PersonData> extractDentalContribution(List<Member>memberList, List<PersonData>personList){
  		
  		if(personList!=null && memberList!=null && memberList.size() == personList.size()){
  			Collections.sort(personList, new PersonIntIdComparable());
  		    Collections.sort(memberList, new MemberIntIdComparable());
  		   
  		    for(int i=0;i<memberList.size();i++){
  		    	Member member =  memberList.get(i);
  		    	PersonData person = personList.get(i);
  		    	if(Integer.parseInt(member.getMemberID())==Integer.parseInt(person.getPersonId())){
  		    		person.setEmployerDentalContribution(new Double((member.getDentalContribution()!=null?member.getDentalContribution():0)));
  		    	}
  		    }
  		}else{
  			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----MemberList and PersonList dont match in extractDentalContribution---->", null, false);
  		}
  		return personList;
  	}
  	/**
  	 * Comparator class for persondata based on personid
  	 * @author majumdar_r
  	 *
  	 */
  	public class PersonIntIdComparable implements Comparator<PersonData>{
  		@Override
  	    public int compare(PersonData o1, PersonData o2) {
  	    	Integer id1 = Integer.parseInt(o1.getPersonId());
  	    	Integer id2 = Integer.parseInt(o2.getPersonId());	    	
  	        return (id1>id2 ? -1 : (id1.equals(id2) ? 0 : 1));
  	    }
  	} 
  	/**
  	 * Comparator class for Memberdaya based on memberid
  	 * @author majumdar_r
  	 *
  	 */
  	public class MemberIntIdComparable implements Comparator<Member>{
  		@Override
  	    public int compare(Member o1, Member o2) {
  	    	Integer id1 = Integer.parseInt(o1.getMemberID());
  	    	Integer id2 = Integer.parseInt(o2.getMemberID());	    	
  	        return (id1>id2 ? -1 : (id1.equals(id2) ? 0 : 1));
  	    }
  	}
  	
  	public boolean individualRenewal(HttpSession session)
  	{		
  		putLoggerStmt(LOGGER, LogLevel.INFO, "<----Inside individualRenewal method---->", null, false);
		SessionData sessionData = (SessionData)session.getAttribute("sessionData");
		GroupData groupData = sessionData.getGroupData();
		String currentGroupId = groupData.getGroupId().toString();
		String currentHouseholdId = sessionData.getHouseholdId().toString();
		String pgrmType = sessionData.getPgrmType();
		String sadpFlag = sessionData.getSadpFlag();
		String tenant = sessionData.getTenant();
		String enrollmentType = sessionData.getEnrollmentType();
		String enrollmentCoverageStartDate = sessionData.getEnrollmentCoverageStartDate();
		List<PersonData> personDataList = groupData.getPersonDataList();
		
		String cmsHealthPlanId = null;
		String cmsDentalPlanId = null;		
		String keepPlanStatus = null;		
		boolean isHealthPlanAvailable = false;
		Map<String,String> household = new HashMap<String,String>();
		household.put("coverageStartDate", sessionData.getCoverageStartDate());
		
		String existingMedicalEnrollmentID = null;
		String existingSADPEnrollmentID = null;	
		String maintenanceReasonCode = null;
		
		for(final PersonData individualPerson : personDataList)
		{	
			if(existingMedicalEnrollmentID == null || "".equals(existingMedicalEnrollmentID)) {
				existingMedicalEnrollmentID = individualPerson.getExistingMedicalEnrollmentID();
			}
			if(existingSADPEnrollmentID == null || "".equals(existingSADPEnrollmentID)) {
				existingSADPEnrollmentID = individualPerson.getExistingSADPEnrollmentID();
			}	
			if(maintenanceReasonCode == null || "".equals(maintenanceReasonCode)) {
				maintenanceReasonCode = individualPerson.getMaintenanceReasonCode();
			}	
		}
		
		if(existingMedicalEnrollmentID != null && !"".equals(existingMedicalEnrollmentID)) {
			EnrollmentResponse enrollmentResponse = setOldPlanDetails(PlanInsuranceType.HEALTH.toString(), existingMedicalEnrollmentID, sessionData);
			cmsHealthPlanId = enrollmentResponse.getCmsPlanId();
			household.put("healthSubscriberId", enrollmentResponse.getSubscriberMemberId());
		}
		if(existingSADPEnrollmentID != null && !"".equals(existingSADPEnrollmentID)) {
			EnrollmentResponse enrollmentResponse = setOldPlanDetails(PlanInsuranceType.DENTAL.toString(), existingSADPEnrollmentID, sessionData);
			cmsDentalPlanId = enrollmentResponse.getCmsPlanId();
			household.put("dentalSubscriberId", enrollmentResponse.getSubscriberMemberId());
			household.put("quotingGroup", sessionData.getDentalQuotingGroup());
		}			
		
		if(maintenanceReasonCode != null && TWENTY_TWO.equals(maintenanceReasonCode)) {
			sessionData.setMaintenanceReasonCode(maintenanceReasonCode);
		}
		
		String eligLeadBenefits = "";
		Map<String,Integer> preferences = new HashMap<String,Integer>();
		List<List<Map<String,Object>>> providers = new ArrayList<List<Map<String,Object>>>();
		
		sessionData.setHealthAvailableFlag("N");
		if(cmsHealthPlanId != null && !"".equals(cmsHealthPlanId)) 
		{
			household.put("hiosPlanNumber", cmsHealthPlanId);
			List<IndividualPlan> individualPlans = planDisplayRestClient.getIndividualPlans(PlanInsuranceType.HEALTH.toString(),Integer.parseInt(currentGroupId),preferences,household,enrollmentCoverageStartDate,groupData,"","",sessionData.getShowCatastrophicPlan(),"",pgrmType,providers,"NO",enrollmentType,null,eligLeadBenefits,tenant,null,"KEEP", "NO",true);
			if(individualPlans != null && !individualPlans.isEmpty()) 
			{
				BigDecimal groupAptc = new BigDecimal(Float.toString(groupData.getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
		 	 	BigDecimal healthAptc = new BigDecimal(Float.toString(individualPlans.get(0).getAptc())).setScale(2,BigDecimal.ROUND_HALF_UP);
		 	 	BigDecimal remainingAptc = groupAptc.subtract(healthAptc);
		 	 	if(remainingAptc.floatValue() > 0) {
		 	 		groupData.setRemainingAptc(remainingAptc.floatValue());
		 	 	} else {
		 	 		groupData.setRemainingAptc(0f);
		 	 	}
				keepPlanStatus = keepShopPlans(personDataList, currentHouseholdId, sadpFlag, currentGroupId, sessionData.getHealthQuotingGroup(), individualPlans);
				if(keepPlanStatus.equalsIgnoreCase("SUCCESS")) {
					isHealthPlanAvailable = Boolean.TRUE;
					sessionData.setHealthAvailableFlag("Y");
					session.setAttribute("QHPEnrollmentId", existingMedicalEnrollmentID);
				}
			}
		}
		
		sessionData.setDentalAvailableFlag("N");
		if(cmsDentalPlanId != null && !"".equals(cmsDentalPlanId)) {
			household.put("hiosPlanNumber", cmsDentalPlanId);
			List<IndividualPlan> individualPlans = planDisplayRestClient.getIndividualPlans(PlanInsuranceType.DENTAL.toString(),Integer.parseInt(currentGroupId),preferences,household,enrollmentCoverageStartDate,groupData,"","",sessionData.getShowCatastrophicPlan(),"",pgrmType,providers,"NO",enrollmentType,null,eligLeadBenefits,tenant,null,"KEEP", "NO",true);
			if(individualPlans != null && !individualPlans.isEmpty()) {
				keepPlanStatus = keepShopPlans(personDataList, currentHouseholdId, sadpFlag, currentGroupId, sessionData.getDentalQuotingGroup(), individualPlans);
				if(keepPlanStatus.equalsIgnoreCase("SUCCESS")) {
					sessionData.setDentalAvailableFlag("Y");
					session.setAttribute("SADPEnrollmentId", existingSADPEnrollmentID);
				}
			}
		}
		
		
		session.setAttribute("sessionData", sessionData);
		return isHealthPlanAvailable;	
	}
	
	public List<IndividualPlan> getIndividualUnavailablePlans(String enrollmentCoverageStartDate, String healthPlanId, String dentalPlanId) {
		List<IndividualPlan> unavailablePlans = new ArrayList<IndividualPlan>();
		String formattedCoverageDate = DateUtil.changeFormat(enrollmentCoverageStartDate, DATE_FORMAT, "yyyy-MM-dd");
		List<IndividualPlan> plansWithCost = getPlanInfo(healthPlanId, formattedCoverageDate);
		unavailablePlans.add(plansWithCost.get(0));
		if(dentalPlanId != null){
			plansWithCost = getPlanInfo(dentalPlanId, formattedCoverageDate);
			unavailablePlans.add(plansWithCost.get(0));				
		}
		return unavailablePlans;
	}


	private EnrollmentResponse setOldPlanDetails(String insuranceType, String enrollmentID, SessionData sessionData) 
	{
		EnrollmentResponse enrollmentResponse = planDisplayRestClient.findPlanIdAndOrderItemIdByEnrollmentId(enrollmentID);
		if(enrollmentResponse != null && enrollmentResponse.getPlanId() != null && enrollmentResponse.getOrderItemId() != null)
		{
			int planId = enrollmentResponse.getPlanId();
			Integer orderItemId = enrollmentResponse.getOrderItemId();
			
			final Map<String,Object> outputData = planDisplayRestClient.findPldOrderItemById(orderItemId);
			if (outputData != null)	{
				Float oldPremium = Float.valueOf(outputData.get("planPremium").toString());
				Float oldAptc = BigDecimal.valueOf(Float.valueOf(outputData.get("planPremium").toString())).subtract(BigDecimal.valueOf(Float.valueOf(outputData.get("premium").toString()))).setScale(2,RoundingMode.HALF_UP).floatValue();
				String quotingGroup = outputData.get("quotingGroup") != null ? outputData.get("quotingGroup").toString() : "";
				
				if(PlanInsuranceType.HEALTH.toString().equalsIgnoreCase(insuranceType)){
					sessionData.setInitialPlanId(String.valueOf(planId));
					sessionData.setHealthQuotingGroup(quotingGroup);
					sessionData.setOldHealthPremium(oldPremium);
					sessionData.setOldHealthAptc(oldAptc);
				}else{
					sessionData.setDentalPlanId(String.valueOf(planId));
					sessionData.setDentalQuotingGroup(quotingGroup);
					sessionData.setOldDentalPremium(oldPremium);
					sessionData.setOldDentalAptc(oldAptc);
				}
				
			}
		}
		return enrollmentResponse;
	}
	
	public Boolean isAdultOnlyHousehold(List<PersonData> personList, String coverageStartDate, String pgrmType) 
	{
		if("INDIVIDUAL".equalsIgnoreCase(pgrmType)) 
		{
			for (PersonData member : personList) 
			{
				int age = getAgeFromCoverageDate(coverageStartDate, member.getDob());
				if (age < PlanDisplayConstants.CHILD_AGE) {
					return false;
				}
			}	
			return true;
		}
		else 	
			return false;
	}
	
    public Boolean checkIfHouseholdIsCatastrophicEligible(List<PdPersonDTO> pdPersonDTOList, String coverageStartDate)  
    {
    	Boolean catastrophicEligible = Boolean.TRUE;
    	for(PdPersonDTO pdPersonDTO : pdPersonDTOList)
    	{
    		int age = this.getAgeFromCoverageDate(coverageStartDate, DateUtil.dateToString(pdPersonDTO.getBirthDay(), DATE_FORMAT));
    		if(age >= 30) {
    			catastrophicEligible = Boolean.FALSE;
    			break;
    		}
    	}
    	return catastrophicEligible;
    }
    
	 public boolean validateEncodedPremium(int planId,Float premiumBeforeCredit,Float premiumAfterCredit,List<Map<String, String>> planDetailsByMember, String encodedPremium) {
			boolean result = false;
			ArrayList<String> encodedPremiumList = new ArrayList<String>();
			encodedPremiumList.add(Integer.toString(planId));
			encodedPremiumList.add(Float.toString(premiumBeforeCredit));
			encodedPremiumList.add(Float.toString(premiumAfterCredit));

			if(planDetailsByMember!=null && !planDetailsByMember.isEmpty()){			
				for(Map<String, String> planDetailsByMemberMap : planDetailsByMember){
					encodedPremiumList.add(planDetailsByMemberMap.get("premium"));
				}
			}
			try {
				result = GhixUtils.validateGhixSecureCheckSum(encodedPremiumList, encodedPremium);
			} catch (NoSuchAlgorithmException e) {
		  		putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while converting encoded premium---->", e, false);
				return result;
			}
			return result;
		}
	 
	public void savePreferences(Long householdId, PdPreferencesDTO pdPreferencesDTO) throws GIRuntimeException {
		PdSavePrefRequest pdSavePrefRequest= new PdSavePrefRequest();
		pdSavePrefRequest.setHouseholdId(householdId);
		pdSavePrefRequest.setPdPreferencesDTO(pdPreferencesDTO);
		planDisplayRestClient.savePreferences(pdSavePrefRequest);
	}
		
	public String getStringPreferences(PdPreferencesDTO pdPreferencesDTO){
		return platformGson.toJson(pdPreferencesDTO);
	}

	public List<PdPersonDTO> getSeekingCovgDentalPersonList(List<PdPersonDTO> pdPersonList) {
		List<PdPersonDTO> dentalPersonList = new ArrayList<PdPersonDTO>();
		for(PdPersonDTO pdPersonDTO : pdPersonList) {
			if(YorN.Y.equals(pdPersonDTO.getSeekCoverage())){
				dentalPersonList.add(pdPersonDTO);
			}
		}		
		return dentalPersonList;
	}
	
	public String getMissingDental(IndividualPlan individualPlan, PdHouseholdDTO pdHouseholdDTO, List<PdPersonDTO> pdPersonDTOList, EnrollmentFlowType specialEnrollmentFlowType) {
		String missingDental = "None";
		boolean isAdult = false, isChild = false;
		String adultNames = null;
		String childNames = null;
		
		for (PdPersonDTO pdPersonDTO : pdPersonDTOList) {
			Date coverageDateforAge = pdHouseholdDTO.getCoverageStartDate();
			if (EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) && EnrollmentFlowType.KEEP.equals(specialEnrollmentFlowType)){
				//Flow will not reach here as we don't show Dental plan selection page for KEEP flow. we are calculating missing dental for Health plan so using health coverage start date.
				coverageDateforAge = pdPersonDTO.getHealthCoverageStartDate() == null ? coverageDateforAge:pdPersonDTO.getHealthCoverageStartDate() ;
			}
			int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
			if (age >= PlanDisplayConstants.CHILD_AGE) {
				isAdult = true;
				adultNames = adultNames == null ?  pdPersonDTO.getFirstName() : adultNames+", "+pdPersonDTO.getFirstName();
			} else if (age < PlanDisplayConstants.CHILD_AGE) {
				isChild = true;
				childNames = childNames == null ?  pdPersonDTO.getFirstName() : childNames+", "+pdPersonDTO.getFirstName();
			}
		}
			
		if(individualPlan != null) {
			String isAdultCovered = PlanDisplayConstants.NOT_COVERED;
			String isChildCovered = PlanDisplayConstants.NOT_COVERED;
			
			Map<String,String> basicDentalCareAdultMap = individualPlan.getMissingDentalCovg().get("BASIC_DENTAL_CARE_ADULT");
			Map<String,String> dentalCheckupChildrenMap = individualPlan.getMissingDentalCovg().get("DENTAL_CHECKUP_CHILDREN");
			
			if(basicDentalCareAdultMap != null && basicDentalCareAdultMap.get("isCovered") != null && !basicDentalCareAdultMap.get("isCovered").isEmpty()){
				isAdultCovered = basicDentalCareAdultMap.get("isCovered");
			}
			if(dentalCheckupChildrenMap != null && dentalCheckupChildrenMap.get("isCovered") != null && !dentalCheckupChildrenMap.get("isCovered").isEmpty()){
				isChildCovered = dentalCheckupChildrenMap.get("isCovered");
			}
			
			if(isAdult && !isChild && isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.NOT_COVERED)){
				missingDental = "Adult";
			} else if(isChild && !isAdult && isChildCovered.equalsIgnoreCase(PlanDisplayConstants.NOT_COVERED)){
				missingDental = "Child";
			} else if(isAdult && isChild) {
				 if(isChildCovered.equalsIgnoreCase(PlanDisplayConstants.NOT_COVERED)) {
					 if(isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED)) { 
						 missingDental = "Child";
					 } else if(isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.NOT_COVERED)) { 
						 missingDental = "AdultChild";
					 }
				 } else if(isChildCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED)) {
					 if(isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.COVERED)) {
						 missingDental = "None";
					 } else if(isAdultCovered.equalsIgnoreCase(PlanDisplayConstants.NOT_COVERED)) {
						 missingDental = "Adult";
					 }
				 }
			 }
		}
		if("Adult".equalsIgnoreCase(missingDental)) {
			missingDental += "#"+adultNames;
		}else if("Child".equalsIgnoreCase(missingDental)) {
			missingDental += "#"+childNames;
		}else if("AdultChild".equalsIgnoreCase(missingDental)) {
			missingDental += "#"+adultNames+", "+childNames;
		}else{
			missingDental += "#";
		}
		return missingDental;		
	}
	
	public static int getAgeFromCoverageDate(Date coverageStartDate, Date dateOfBirth) {
		Calendar today = TSCalendar.getInstance();
		today.setTime(coverageStartDate);
		Calendar dob = TSCalendar.getInstance();
		dob.setTime(dateOfBirth);
		Integer age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);
		if(dob.get(Calendar.MONTH) > today.get(Calendar.MONTH) || (dob.get(Calendar.MONTH) == today.get(Calendar.MONTH) && dob.get(Calendar.DATE) > today.get(Calendar.DATE))){
			age--;
		}
	
		return age;
	}
	
	public String prepareQuotedMember(PdHouseholdDTO pdHouseholdDTO, List<PdPersonDTO> pdPersonDTOList, EnrollmentFlowType specialEnrollmentFlowType, InsuranceType insuranceType, ResourceBundle bundle)	{		
		int eligibleAdultCnt = 0;
		int eligibleChildCnt = 0;
		String quotedMembersString = "";
		
		Map<String, Integer> quotedMembers = new HashMap<String, Integer>();
		quotedMembers.put("eligibleAdultCnt", 0);
		quotedMembers.put("eligibleChildCnt", 0);

		for (PdPersonDTO pdPersonDTO : pdPersonDTOList)	{
			Date coverageDateforAge = pdHouseholdDTO.getCoverageStartDate();
			if (EnrollmentType.S.equals(pdHouseholdDTO.getEnrollmentType()) && EnrollmentFlowType.KEEP.equals(specialEnrollmentFlowType)){
				if(InsuranceType.HEALTH.equals(insuranceType)){
					coverageDateforAge = pdPersonDTO.getHealthCoverageStartDate();
				} else {
					coverageDateforAge = pdPersonDTO.getDentalCoverageStartDate();
				}
			}
			int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageDateforAge, pdPersonDTO.getBirthDay());
			if(age >= PlanDisplayConstants.CHILD_AGE){
				quotedMembers.put("eligibleAdultCnt", ++eligibleAdultCnt);
			}else{
				quotedMembers.put("eligibleChildCnt", ++eligibleChildCnt);
			}
		}
		if(eligibleAdultCnt == 1){
			String adult = "adult";
			if(bundle != null && bundle.getString("pd.label.quotedMembersString.adult") != null){
				adult = bundle.getString("pd.label.quotedMembersString.adult");
			}
			quotedMembersString = eligibleAdultCnt + " " +adult;
		}
		if(eligibleAdultCnt > 1){
			String adults = "adults";
			if(bundle != null && bundle.getString("pd.label.quotedMembersString.adults") != null){
				adults = bundle.getString("pd.label.quotedMembersString.adults");
			}
			quotedMembersString = eligibleAdultCnt + " " +adults;
		}
		if(eligibleAdultCnt > 0 && eligibleChildCnt > 0){
			String and = "and";
			if(bundle != null && bundle.getString("pd.label.quotedMembersString.and") != null){
				and = bundle.getString("pd.label.quotedMembersString.and");
			}
			quotedMembersString = quotedMembersString + " "+and+" ";
		}
		if(eligibleChildCnt == 1){
			String child = "child";
			if(bundle != null && bundle.getString("pd.label.quotedMembersString.child") != null){
				child = bundle.getString("pd.label.quotedMembersString.child");
			}
			quotedMembersString = quotedMembersString + eligibleChildCnt + " "+child;
		}
		if(eligibleChildCnt > 1){
			String children = "children";
			if(bundle != null && bundle.getString("pd.label.quotedMembersString.children") != null){
				children = bundle.getString("pd.label.quotedMembersString.children");
			}
			quotedMembersString = quotedMembersString + eligibleChildCnt + " "+children;
		}		
		return quotedMembersString;		
	}
	
	public boolean validateEncodedPremium(IndividualPlan individualPlan) {
		boolean result = false;
		ArrayList<String> encodedPremiumList = new ArrayList<String>();
		encodedPremiumList.add(Long.toString(individualPlan.getPlanId()));
		encodedPremiumList.add(Float.toString(individualPlan.getPremiumBeforeCredit()));
		encodedPremiumList.add(Float.toString(individualPlan.getPremiumAfterCredit()));

		if(individualPlan.getPlanDetailsByMember()!=null && !individualPlan.getPlanDetailsByMember().isEmpty()){			
			for(Map<String, String> planDetailsByMemberMap : individualPlan.getPlanDetailsByMember()){
				encodedPremiumList.add(planDetailsByMemberMap.get("premium"));
			}
		}
		try {
			result = GhixUtils.validateGhixSecureCheckSum(encodedPremiumList, individualPlan.getEncodedPremium());
		} catch (NoSuchAlgorithmException e) {
			putLoggerStmt(LOGGER, LogLevel.ERROR, "<----Error occur while converting encoded premium---->", e, false);
			return result;
		}
		return result;
	}
	
	public boolean isAdultPresentInExistingMemberList(List<PdPersonDTO> pdPersonDTOList) {
		boolean isAdult = false;
		for(PdPersonDTO pdPersonDTO:pdPersonDTOList) {			
			if (YorN.N.equals(pdPersonDTO.getNewPersonFlag()) && pdPersonDTO.getDentalEnrollmentId() != null) {
				Date coverageStartDate = pdPersonDTO.getDentalCoverageStartDate();
				int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageStartDate, pdPersonDTO.getBirthDay());
				if(age >= PlanDisplayConstants.CHILD_AGE) {
					isAdult=true;
				}
			}			
		}
		return isAdult;
	}
	
	 public void setPersonSeekCoverageDental(PdHouseholdDTO pdHouseholdDTO, List<PdPersonDTO> pdPersonDTOList) 
	 {
		 boolean isAdultInExistingMembers = isAdultPresentInExistingMemberList(pdPersonDTOList);
	    	String dentalPlanSelection = this.getDentalConfig(pdHouseholdDTO.getCoverageStartDate());
	        
			for(PdPersonDTO pdPersonDTO : pdPersonDTOList)
			{
				Date coverageStartDate = pdPersonDTO.getDentalCoverageStartDate() == null ? pdHouseholdDTO.getCoverageStartDate() : pdPersonDTO.getDentalCoverageStartDate() ;
				int age = PlanDisplayUtil.getAgeFromCoverageDate(coverageStartDate, pdPersonDTO.getBirthDay());
				
				pdPersonDTO.setSeekCoverage(YorN.N);
				if(pdPersonDTO.getDentalEnrollmentId() != null && YorN.N.equals(pdPersonDTO.getNewPersonFlag())) {
					pdPersonDTO.setSeekCoverage(YorN.Y);
				} 
				else if(YorN.Y.equals(pdPersonDTO.getNewPersonFlag())) {				
					if (age < PlanDisplayConstants.CHILD_AGE || isAdultInExistingMembers) {
						pdPersonDTO.setSeekCoverage(YorN.Y);
					}
				}
				else{
		            if (YorN.N.equals(pdPersonDTO.getSeekCoverage()) && pdPersonDTO.getDentalEnrollmentId() == null) {
		            	if (("PO".equals(dentalPlanSelection) && age < PlanDisplayConstants.CHILD_AGE) || "ON".equals(dentalPlanSelection)) {
		            		if (age < PlanDisplayConstants.CHILD_AGE || isAdultInExistingMembers) {
		    					pdPersonDTO.setSeekCoverage(YorN.Y);
		    				}
		            	}
		            } 
				}
			}
	 }
	 
	 public boolean isSeekingCoverage(List<PdPersonDTO> pdPersonList){
		 boolean  isSeekingCoverage = false;
		 if(pdPersonList != null){
			 for(PdPersonDTO pdPersonDTO : pdPersonList) {
					if(YorN.Y.equals(pdPersonDTO.getSeekCoverage())){
						isSeekingCoverage = true;
					}
				}
		 }
		 return isSeekingCoverage;
	 }
	 
	 public String getDentalConfig(Date covgStartDate) 
	 {
		 String dentalConfig  = "ON";
		 if(!DynamicPropertiesUtil.getPropertyValue("planSelection.DentalPlanSelection").isEmpty()) {
			 dentalConfig = DynamicPropertiesUtil.getPropertyValue("planSelection.DentalPlanSelection");
		 }
		/* String configChangeDate = "01-01-2016";
		 DateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy", Locale.US);
		 Date formattedConfigChangeDate;
		 try 
		 {
			 String programType = DynamicPropertiesUtil.getPropertyValue("global.StateExchangeType");
			 if (programType != null && programType.equalsIgnoreCase(ProgramType.INDIVIDUAL.toString())) {
				 formattedConfigChangeDate = dateFormat.parse(configChangeDate);
				 if(covgStartDate != null && covgStartDate.before(formattedConfigChangeDate)) {
					 return "PO";
				 } else {
					 return "ON";
				 }
			 } 
			 else {
				 return dentalConfig;
			 }
		 }
		 catch (ParseException e) {
		 }*/
		 return dentalConfig;
	 }
	 
	 public int getCoverageyear(Date coverageStartDate)
	 {
		 Calendar cal = TSCalendar.getInstance();
		 cal.setTime(coverageStartDate);
		 return cal.get(Calendar.YEAR);
	 }
	 
	 
	 public static void putLoggerStmt(Logger loggerSource, LogLevel logLevel, String logMessage, Exception excep, boolean isSanitizeForLoggingOn) 
	 {
		 if(LogLevel.INFO.equals(logLevel) && loggerSource.isInfoEnabled()) {
			 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
			 loggerSource.info(msg);
		 } 
		 else if(LogLevel.DEBUG.equals(logLevel) && loggerSource.isDebugEnabled()) {
			 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
			 loggerSource.debug(msg);
		 }
		 else if(LogLevel.ERROR.equals(logLevel) && loggerSource.isErrorEnabled()) {
			 if(excep == null) {
				 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
				 loggerSource.error(msg);
			 } else {
				 String msg = isSanitizeForLoggingOn ? SecurityUtil.sanitizeForLogging(logMessage) : logMessage;
				 loggerSource.error(msg, excep);
			 }
		 }	
	 }
	 
	 public List<DrugDTO> getPrescriptionSearchRequestList(String drugData){
		List<DrugDTO> prescriptionRequestList = null;
		if(StringUtils.isNotBlank(drugData)){
			DrugDTO[] prescriptionSearchDTOsArr = platformGson.fromJson(drugData,DrugDTO[].class);
			List<DrugDTO> prescriptionSearchDTOsList = Arrays.asList(prescriptionSearchDTOsArr); 
			if(!prescriptionSearchDTOsList.isEmpty()){
				prescriptionRequestList = new ArrayList<DrugDTO>();
				for(DrugDTO drugDTO: prescriptionSearchDTOsList){										
					prescriptionRequestList.add(drugDTO);
				}
			}
		}
		return prescriptionRequestList;
	}
	 
	 public static String create14DigitNum(String hiosPlanNumber){
		 String hiosPlanId = StringUtils.EMPTY;
		 if(hiosPlanNumber != null){
			 hiosPlanId = hiosPlanNumber.substring(0, 14);
		 }
		 return hiosPlanId;
	 }
	 
	 public boolean is2018CovgYear(Date coverageStartDate) {
			int coverageYear = getCoverageyear(coverageStartDate);
			if(coverageYear == 2018){
				return true;
			}
			return false;
		}
	 
	 public boolean isIndividualDefaultRole(Role individualRole){
			boolean isIndividual = false;
			if(individualRole!=null && individualRole.getName().equalsIgnoreCase(RoleService.INDIVIDUAL_ROLE)){
				isIndividual = true;
			}
			return isIndividual;
	 }
}
