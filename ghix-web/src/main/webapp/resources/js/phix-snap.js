/*******************************************************************\
phix-snap: panel snapping based off of guidobouman's jquery-panelsnap
by Kenneth
easer: the snapping transition
snap_callback(): callback after snap has finished
cooldown: prevents any of the keys from interrupting the snapping
the setTimeout in the $("html,body").animate(...) provides a timeout
for cooldown after the animation is complete
\******************************************************************/

//extending easing
//examples at: http://easings.net/
//code at: http://pastebin.com/SKuHgurw

//prevent it for the scrollable divs
/*var shouldScroll = true; */

$.easing.easer = function (x, t, b, c, d) {
        return -c * ((t=t/d-1)*t*t*t - 1) + b;
    };

var cooldown = false;

$("html").keydown(function(event){
// left: 37, up: 38, right: 39, down: 40,
// spacebar: 32, pageup: 33, pagedown: 34, end: 35, home: 36

var keyC = event.which;
var where = "next";
	//page down, down
	if(keyC == 34|| keyC == 40)
	{	
		where = "next";
	}
	//page up, up
	else if (keyC == 33 || keyC == 38)
	{
		where = "previous";
	}
	//home, end
	else if(keyC == 36 || keyC == 35)
	{
		event.preventDefault();
		return false;
	}
	//anything else
	else
	{
		return true;
	}
	snapScroll(where,event);
});


$("html").on("DOMMouseScroll",function(event){
	
	
	if(event.originalEvent.detail > 0 && currentPanel() < $("section:visible").length - 1)
		{
		 snapScroll("next",event);
		}
     else if(event.originalEvent.detail < 0)
     {
         snapScroll("previous",event);
     }
});


$("html").on("mousewheel",function(event){

	if(event.originalEvent && event.originalEvent.wheelDelta < 0 && currentPanel() < $("section:visible").length - 1)
		{
		 snapScroll("next",event);
		}
	else if(event.originalEvent && event.originalEvent.wheelDelta > 0)
	{
         snapScroll("previous",event);
     }
});


function currentPanel(){  
	var lst = 999999;
    var currd = 0;
    var currv = 0;
    $("section").each(function (index) {
        currd = $(window).scrollTop() - $(this).offset().top;
        if (Math.abs(currd) < lst) {
            lst = currd;
            currv = index;
        }
    });
    return currv;
}

function snapScroll(where,event) {
	var destination = $("section");
	var currv = currentPanel();
	if(where == "next"){
		/*if($("section").length - 1 <= currv )*/
		//hide last 2 sections
		if($("section").length - 3 <= currv )
		{	
			//console.log('cannot go down');
			//resume standard functionality
			return true;//supposed to let event continue with default
		}
		else{
			event.preventDefault();
			destination = $("section").eq(currv + 1);
		}
	
	}
	else if(where =="previous"){
		if(currv <= 0)
		{	
			//console.log('cannot go up');
			//resume standard functionality
			return true;
		}
		else{
			event.preventDefault();
			destination = $("section").eq(currv  - 1);
		}
	}
	//snapping
	else{
		destination = $("section[data-panel="+where+"]");
	}
	
	
	destLocation = destination.offset().top;
	
	if(!cooldown){
		//console.log('run');
		cooldown = true;
		$("[data-panel].active").removeClass("active");
		$("[data-panel="+destination.attr("data-panel")+"]").addClass("active");
		
		//improve tabindex
		$("section").each(function(){
			if($(this).hasClass("active")){
				if(($(this).attr("data-panel"))=="intro"){
					$(this).find("a:not([data-toggle]),input,button,.custom-rc").attr("tabindex", "0");
				}else{
					$(this).find("a:not([data-toggle]),input,button,.custom-rc").attr("tabindex", "1");
				};
			}else{
				$(this).find("a:not([data-toggle]),input,button,.custom-rc").attr("tabindex", "-1");
			}
		});
		
		/*if($("#intro").hasClass("active")){
			$("#masthead a").attr("tabindex","0");
		}else{
			$("#masthead a").attr("tabindex","-1");
		}*/
		
		if($("section[data-panel=page2]").hasClass("active")){
			$("#disclaimer a,footer a").attr("tabindex","0");
		}else{
			$("#disclaimer a,footer a").attr("tabindex","-1");
		}
	
	$("html,body").stop(true).animate({scrollTop:destLocation},1300,"easer",function(){
					setTimeout(function(){
						cooldown = false;
						
						snap_callback();
					},150);
	});
				}

}

////////////////////////////
///	Clicking on menubar	///
//////////////////////////

$(document).ready(function(){
	
	$("a[data-panel]").click(function(event){
		event.preventDefault();
    snapScroll($(this).attr("data-panel"),event);
});

	/*$(".nano").hover(function(){
		//alert('false');
		shouldScroll = false;
	},
			function(){
		//alert('true');
		shouldScroll = true;
	});*/
});

function snap_callback() {	
	//any callbacks? put them here
	/*var currv = currentPanel();
	if(currv!=0){
		console.log("sdddd");
		$('.sticky-wrapper').css("display","block");
	}*/
	
	//paco:improve tabindex
	//$("section").not("[data-panel=intro]").find("a,input,button,.custom-rc").first().focus();
	if(!$("#intro").hasClass("active")){
		$("#intro").find("a,input,button,.custom-rc").attr("tabindex", "-1");
	}else{
		$("#intro").find("a,input,button,.custom-rc").attr("tabindex", "0");
	}
	
	$("#intro .sticky-wrapper").css("background-color","transparent");
	$("#fam-cov_error").hide();
}