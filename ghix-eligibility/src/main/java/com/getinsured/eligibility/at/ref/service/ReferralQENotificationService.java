package com.getinsured.eligibility.at.ref.service;

import com.getinsured.hix.platform.notices.jpa.NoticeServiceException;

public interface ReferralQENotificationService {

	String generateQEWithAutoLinkingNotice(long appId) throws Exception;
	String generateQEWithAutoLinkingNotice(String caseNumber) throws Exception;
	String generateOEWithAutoLinkingNotice(long ssapAppId) throws Exception;
	String generateOEWithAutoLinkingNotice(String caseNumber) throws Exception;
	String generateAutoRenewalNotice(String caseNumber, String noticeName, String fileName) throws NoticeServiceException;
}
