import React, {Fragment, useState} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Button from '@cmsgov/design-system-core/dist/components/Button/Button';
import FormForModal from '../pages/FormForModal';
import UniversalInput from '../../lib/UniversalInput';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {hideModal} from '../../actions/actionCreators';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import INPUT_TYPES from '../../constants/inputTypes';

// Components

// ActionCreators

// Constants

const QualifyingEventDetailsModal = props => {

    const {
        modal: {
            modalOwner,
            callbacks,
            data: [ url, body, isQHPshown, showQHP ]
        }
    } = props;

    const [ localState, setLocalState ] = useState({
        'sepEvent' : null,
        'sepEventDate' : null,
    });

    const {
        sepEvent,
        sepEventDate
    } = localState;

    const onFieldChange = newData => {
        setLocalState(state => ({
            ...state,
            ...newData
        }));
    };

    const handleConfirmationClick = () => {
        if(sepEvent !== null && sepEventDate !== null) {
            if(isQHPshown) {
                showQHP(localState);
            } else {
                callbacks(url, {...body, ...localState});
            }
        }
    };

    const handleHideModal = () => {
        props.actions.hideModal(modalOwner);
    };

    return (
        <Fragment>
            <div className="modal__header" id="modal-qep">
                <h2>Qualifying Event Details</h2>
                <div className="modal__btn--close">
                    <a href="#" onClick={handleHideModal}><FontAwesomeIcon icon="times"/><span className="hidden">close</span></a>
                </div>
            </div>
            <FormForModal className="modal__body">
                <UniversalInput
                    label={{
                        text: 'Qualifying Event',
                        isLabelRequired: true
                    }}
                    inputData={{
                        isInputRequired: true,
                        type: INPUT_TYPES.DROPDOWN,
                        name: 'sepEvent',
                        value: sepEvent || '',
                        options: DROPDOWN_OPTIONS.QUALIFYING_EVENT,
                        currentData: localState,
                        fields: []
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                />
                <UniversalInput
                    label={{
                        labelClass: ['date-picker-width-01'],
                        text: 'Date of Qualifying Event'
                    }}
                    inputData={{
                        inputClass:['gi-dropdown'],
                        isInputRequired: true,
                        type: INPUT_TYPES.DATE_PICKER,
                        name: 'sepEventDate',
                        value: sepEventDate || '',
                        placeholder: 'MM/DD/YYYY',
                        currentData: localState,
                        fields: []
                    }}
                    inputActions={{
                        onFieldChange
                    }}
                    inputInArray={0}
                />
            </FormForModal>
            <div className="modal__footer">
                <div className="usa-grid controls">
                    <div className="usa-offset-one-third txt-left">
                        <Button className='modal__btn--cancel usa-button-secondary' onClick={handleHideModal}>
                            Cancel
                        </Button>
                        <Button className='modal__btn--confirm' onClick={handleConfirmationClick}>
                            Submit Application
                        </Button>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data
});

const matDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        hideModal,
    }, dispatch)
});

export default connect(mapStateToProps, matDispatchToProps)(QualifyingEventDetailsModal);
