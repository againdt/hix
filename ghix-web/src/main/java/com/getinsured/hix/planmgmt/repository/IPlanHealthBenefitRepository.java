package com.getinsured.hix.planmgmt.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.PlanHealthBenefit;

public interface IPlanHealthBenefitRepository extends JpaRepository<PlanHealthBenefit, Integer> {

	@Query("SELECT pb from PlanHealthBenefit pb WHERE (pb.plan.id = :planId) and (TO_DATE (:effectiveDate, 'YYYY-MM-DD') BETWEEN pb.effStartDate AND pb.effEndDate)")
	List<PlanHealthBenefit> getPlanHealthBenefit(@Param("planId") Integer planId, @Param("effectiveDate") String effectiveDate);

	@Query("SELECT phb from PlanHealthBenefit phb WHERE phb.plan.id = :planId")
	List<PlanHealthBenefit> getPlanHelthBenefitByPlanId(@Param("planId") Integer planId);
}


