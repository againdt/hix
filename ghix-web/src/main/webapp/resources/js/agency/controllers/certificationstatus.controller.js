(function() {
'use strict';  

angular.module('agencyApp')
.controller('StatusController',StatusController);

	StatusController.$inject = ['$http', '$scope', '$state', '$stateParams', '$timeout', '$window', 'httpService', 'dataStorageService'];

	function StatusController ($http, $scope, $state, $stateParams, $timeout, $window, httpService, dataStorageService) {	
        var vm = this;
        vm.certificationStatus = $scope.$parent.vm.agencyCertificationStatus;
        vm.agencyId = $("#agencyId").val();       
        vm.refreshFlag = $stateParams.refreshFlag;
        $scope.$parent.vm.sideNavListOne[0].disableLink = false;
        $scope.$parent.vm.sideNavListOne[1].disableLink = false;
        $scope.$parent.vm.sideNavListOne[2].disableLink = false;
        $scope.$parent.vm.sideNavListOne[3].disableLink = false;
        $scope.$parent.vm.sideNavListOne[4].disableLink = false;
        $scope.$parent.vm.sideNavListOne[0].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[1].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[2].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[3].showOKicon = true;
        $scope.$parent.vm.sideNavListOne[4].showOKicon = true; 

        if (vm.refreshFlag){
            $window.location.reload();
        }                   
        var promise = httpService.apiGetRequest('/hix/agency/certification/view/'+vm.agencyId);
        promise.then(function(response){
            vm.data = response.data;
            $scope.$parent.vm.sideNavListOne[5].disableLink = false;
        })  
        .catch(function(error){
            console.log (error);
        });

    }
    
})();