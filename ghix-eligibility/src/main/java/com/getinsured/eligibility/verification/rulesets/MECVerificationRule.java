package com.getinsured.eligibility.verification.rulesets;

import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.verification.service.VerificationRulesService;
import com.getinsured.eligibility.verification.util.SSAPConstants;
import com.getinsured.eligibility.verification.util.SSAPUtils;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

/**
 * Encapsulates MEC Verification Rule methods
 */
@Component
public class MECVerificationRule {

	private static final Logger LOGGER = LoggerFactory.getLogger(MECVerificationRule.class);

	@Autowired
	private VerificationRulesService verificationRulesService;
	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;

	public static void main(String[] asdf) throws Exception {
		MECVerificationRule mecVerificationRule = new MECVerificationRule();
		//mecVerificationRule.verifyMEC(SSAPUtils.loadSSAPJSON(), SSAPUtils.loadESIJSON(), SSAPUtils.loadNonESIJSON());
	}

	/**
	 * This rule will verify MEC Status of Household Members
	 * 
	 * @param ssapJson
	 * @param esiJson
	 * @param nonEsiJson
	 * @throws Exception
	 */
	public boolean verifyMEC(String ssapJson, String esiJson, String nonEsiJson, String pPersonId) throws Exception {
		LOGGER.info("Verify MEC Rule : Starts");

		String status = null;
		boolean verificationStatus = false;
		String eligibleForEIS = null;
		String isInsured = null;
		JsonArray householdMembers = null;
		JsonObject householdMember;
		JsonObject currentOtherInsurance;
		boolean medicareEligibleIndicator;
		boolean tricareEligibleIndicator;
		boolean peaceCorpsIndicator;
		boolean otherStateOrFederalProgramIndicator;
		String personId = null;
		boolean personChecked = false;
		JsonObject singleStreamlinedApplication;

		try {
			singleStreamlinedApplication = SSAPUtils.getSingleStreamlinedApplication(ssapJson);
			householdMembers = SSAPUtils.getFilteredHouseholdMembers(singleStreamlinedApplication);
			
			Iterator<JsonElement> iterator = householdMembers.iterator();
			while (iterator.hasNext()) {
				householdMember = iterator.next().getAsJsonObject();
				personId = householdMember.get(SSAPConstants.PERSON_ID).getAsString();
				
				try {
					if(personId.equals(pPersonId)){
						currentOtherInsurance = householdMember.get(SSAPConstants.HEALTH_COVERAGE).getAsJsonObject().get(SSAPConstants.CURRENT_OTHER_INSURANCE).getAsJsonObject();
						medicareEligibleIndicator = currentOtherInsurance.get(SSAPConstants.MEDICARE_ELIGIBLE_INDICATOR).getAsBoolean();
						tricareEligibleIndicator = currentOtherInsurance.get(SSAPConstants.TRICARE_ELIGIBLE_INDICATOR).getAsBoolean();
						peaceCorpsIndicator = currentOtherInsurance.get(SSAPConstants.PEACE_CORPS_INDICATOR).getAsBoolean();
						otherStateOrFederalProgramIndicator = currentOtherInsurance.get(SSAPConstants.OTHER_STATE_OR_FEDERAL_PROGRAM_INDICATOR).getAsBoolean();

						if (medicareEligibleIndicator || tricareEligibleIndicator || peaceCorpsIndicator || otherStateOrFederalProgramIndicator) {
							status = SSAPConstants.VERIFIED;
						} else {
							if(!SSAPUtils.isEmpty(nonEsiJson)){
								if(!SSAPConstants.NO_SSN.equalsIgnoreCase(nonEsiJson)){
									if (SSAPUtils.checkMecVerificationCode(nonEsiJson)) {
										status = SSAPConstants.NOT_VERIFIED;
									} else {
										if(!SSAPUtils.isEmpty(esiJson)){
											if(!SSAPConstants.NO_SSN.equalsIgnoreCase(esiJson)){
												eligibleForEIS = SSAPUtils.getESIIndicatorValue(SSAPConstants.ELIGIBLE_FOR_EIS_INDICATOR, esiJson);
												isInsured = SSAPUtils.getESIIndicatorValue(SSAPConstants.IS_INSURED_INDICATOR, esiJson);

												if (!SSAPUtils.isEmpty(eligibleForEIS) && !SSAPUtils.isEmpty(isInsured) && SSAPConstants.FALSE.equals(eligibleForEIS)
														&& SSAPConstants.FALSE.equals(isInsured)) {
													status = SSAPConstants.VERIFIED;
												} else {
													status = SSAPConstants.NOT_VERIFIED;
												}
											} else {
												status = SSAPConstants.NOT_VERIFIED;
											}
										}	else {
											status = SSAPConstants.PENDING;
										}	
									}
								} else {
									status = SSAPConstants.NOT_VERIFIED;
								}
							}	else {
								status = SSAPConstants.PENDING;
							}
						}
						personChecked = true;
					}
						
					if(personChecked)break;
				} catch (Exception exception) {
					LOGGER.error("Exception occurred while Verifying MEC : Person : " + pPersonId, exception);
					String rule = SSAPConstants.RULES.MEC_VERIFICATION_RULE.toString();
					giWSPayloadRepository
							.saveAndFlush(SSAPUtils.createGIWSPayload(exception, SSAPUtils.createRequestPayload(ssapJson, null, null, esiJson, nonEsiJson, rule), rule));
				}
			}
			//HIX-117626 Override MEC status to VERIFIED
			status=SSAPConstants.VERIFIED;
			if(!SSAPUtils.isEmpty(status)){
				verificationStatus = verificationRulesService.updateVerificationStatus(Long.valueOf(SSAPUtils.getApplicationIdFromSsapJson(singleStreamlinedApplication)),
						Long.valueOf(pPersonId), status, SSAPConstants.MEC_VERIFICATION_RULE);
				verificationStatus = true;
			} 
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while Verifying MEC : ", exception);
			throw exception;
		}

		LOGGER.info("Verify MEC Rule : Ends ");
		
		return verificationStatus;
	}
}
