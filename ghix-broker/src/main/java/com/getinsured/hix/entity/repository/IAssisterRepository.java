package com.getinsured.hix.entity.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.getinsured.hix.model.entity.Assister;

/**
 * Spring-JPA repository that encapsulates methods that interact with the
 * Assister table
 */
public interface IAssisterRepository extends JpaRepository<Assister, Integer> {

}