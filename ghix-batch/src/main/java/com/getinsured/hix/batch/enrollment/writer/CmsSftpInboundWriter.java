package com.getinsured.hix.batch.enrollment.writer;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.admin.service.JobService;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemWriter;

import com.getinsured.hix.batch.enrollment.service.CmsSftpService;
import com.getinsured.hix.enrollment.util.EnrollmentConstants;
import com.getinsured.hix.platform.util.exception.GIException;

public class CmsSftpInboundWriter implements ItemWriter<String> {
	private static final Logger LOGGER = LoggerFactory.getLogger(CmsSftpInboundWriter.class);
	private CmsSftpService cmsSftpService;
	Integer partition;
	Long batchExecutionId;
	private JobService jobService;
	long jobExecutionId = -1;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.info(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext ec = stepExecution.getExecutionContext();
		if (ec != null) {
			partition = ec.getInt("partition");
			batchExecutionId = stepExecution.getJobExecutionId();
			jobExecutionId=stepExecution.getJobExecution().getId();
			LOGGER.info("CmsSftpInboundWriter : Thread Name: " + Thread.currentThread().getName() + "Partition :: "
					+ partition);
		}
	}

	@Override
	public void write(List<? extends String> reportTypeList) throws Exception {
		
		String batchJobStatus=null;
		if(jobService != null && jobExecutionId != -1){
			batchJobStatus = jobService.getJobExecution(jobExecutionId).getStatus().name();
		}
		if(batchJobStatus!=null && (batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPING) ||batchJobStatus.equalsIgnoreCase(EnrollmentConstants.BATCH_JOB_STATUS_STOPPED))){
			throw new Exception(EnrollmentConstants.BATCH_STOP_MSG);
		}
		
		if (null != reportTypeList && !reportTypeList.isEmpty()) {
			for (String reportType : reportTypeList) {
				LOGGER.info("CmsSftpInboundWriter : Thread Name: " + Thread.currentThread().getName()
						+ " Report Type :: " + reportType);
				try{
					cmsSftpService.downloadFilesForReportType(reportType, batchExecutionId);
				}catch(GIException e){
					LOGGER.error("CmsSftpInboundWriter :: Exception in downloading files", e);
				}
			}
		}
	}

	/**
	 * @return the cmsSftpService
	 */
	public CmsSftpService getCmsSftpService() {
		return cmsSftpService;
	}

	/**
	 * @param cmsSftpService
	 *            the cmsSftpService to set
	 */
	public void setCmsSftpService(CmsSftpService cmsSftpService) {
		this.cmsSftpService = cmsSftpService;
	}

	public JobService getJobService() {
		return jobService;
	}

	public void setJobService(JobService jobService) {
		this.jobService = jobService;
	}
}
