import { DataKeepService } from '../../services/data-keep.service';
import { templateJitUrl } from '@angular/compiler/compiler';

import { Component, Input, EventEmitter, Output } from "@angular/core";

@Component({
    selector: 'input-group',
	templateUrl: './input-group.component.html',
	styleUrls: ["./input-group.component.scss"]
})
export class InputGroupComponents {

    constructor(
        public dataKeep: DataKeepService
    ) { }
    @Input() public boxType:"radio"| "checkbox" = "radio";
    @Input() public inputSet: InputSet[];
    @Input() public picked: string;

    @Output() public select = new EventEmitter<string>();

    public isChecked(value: string): boolean {
        if (value === this.picked) {
            return true;
        } else {
            return false
        }
    }

    private generateId(set: InputSet) {
        return `${set.name}(${set.value})`;
    }

    public output(value: string) {
        this.select.emit(value);
    }


}

export interface InputSet {
    name: string,
    value: string,
    text: string
}
