package com.getinsured.eligibility.at.ref.service;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.getinsured.eligibility.at.dto.AccountTransferRequestDTO;
import com.getinsured.eligibility.at.mes.repository.ATSpanProcessingRepository;
import com.getinsured.eligibility.at.ref.common.ReferralProcessingConstants;
import com.getinsured.eligibility.at.ref.dozzer.assembler.SsapAssembler;
import com.getinsured.eligibility.enums.AccountTransferProcessStatus;
import com.getinsured.eligibility.enums.ApplicantStatusEnum;
import com.getinsured.eligibility.enums.ApplicationSource;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.eligibility.enums.AtResponseTypeEnum;
import com.getinsured.eligibility.enums.EligibilityStatus;
import com.getinsured.eligibility.enums.ExchangeEligibilityStatus;
import com.getinsured.eligibility.enums.ReferralActivationEnum;
import com.getinsured.eligibility.model.ReferralActivation;
import com.getinsured.eligibility.repository.ILocationRepository;
import com.getinsured.eligibility.repository.ReferralActivationRepository;
import com.getinsured.hix.model.Location;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.GlobalConfiguration;
import com.getinsured.hix.platform.giwspayload.repository.GIWSPayloadRepository;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.iex.erp.gov.cms.dsh.at.extension._1.AccountTransferRequestPayloadType;
import com.getinsured.iex.ssap.Address;
import com.getinsured.iex.ssap.BloodRelationship;
import com.getinsured.iex.ssap.HouseholdContact;
import com.getinsured.iex.ssap.HouseholdMember;
import com.getinsured.iex.ssap.SingleStreamlinedApplication;
import com.getinsured.iex.ssap.builder.SsapJsonBuilder;
import com.getinsured.iex.ssap.model.SsapApplicant;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicantRepository;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;
import com.getinsured.iex.util.ReferralConstants;
import com.getinsured.iex.util.ReferralUtil;
import com.getinsured.iex.util.SsapUtil;
import com.getinsured.timeshift.util.TSDate;

/**
 * @author chopra_s
 * 
 */
@Component("referralSsapApplicationService")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class ReferralSsapApplicationServiceImpl implements ReferralSsapApplicationService {
	private static final Logger LOGGER = Logger.getLogger(ReferralSsapApplicationServiceImpl.class);

	private static final String ERROR_IN_JSON_STRING = "Json String is not in correct format";

	@Autowired
	private GIWSPayloadRepository giwsPayloadRepository;

	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;

	@Autowired
	private SsapApplicantRepository ssapApplicantRepository;

	@Autowired
	private SsapUtil ssapUtil;

	@Autowired
	@Qualifier("referralActivationRepository")
	private ReferralActivationRepository referralActivationRepository;

	@Autowired
	@Qualifier("iLocationRepository")
	private ILocationRepository iLocationRepository;

	@Autowired
	@Qualifier("ssapJsonBuilder")
	private SsapJsonBuilder ssapJsonBuilder;
	
	@Autowired
	@Qualifier("ssapAssembler")
	private SsapAssembler ssapAssembler;
	
	private final static long COVERAGE_YEAR = 2015;

	private static final String OE = "OE";

	private static final String SEP = "SEP";

	private static final String QEP = "QEP";
	
	@Autowired
	ATSpanProcessingRepository atSpanProcessingRepository;


	@Override
	@Transactional
	public SsapApplication executeCreate(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication, AccountTransferRequestDTO accountTransferRequest) {
		LOGGER.info("In execute create of SsapApplication");
		final AccountTransferRequestPayloadType source = accountTransferRequest.getAccountTransferRequestPayloadType();
		boolean isDMData = ssapAssembler.isATRequestFromFFM(source);
		
		SsapApplication ssapApplication = createSsapApplication(ssapJson, singleStreamlinedApplication, accountTransferRequest.isLCE(), 
				accountTransferRequest.isQE(), accountTransferRequest.isRenewal(), accountTransferRequest.getCoverageYear(),accountTransferRequest.getRequester(),accountTransferRequest.getCurrentSpanApplicationId(),accountTransferRequest.getCompareToApplicationId(), isDMData);

		if (ssapApplication == null) {
			LOGGER.info(ReferralProcessingService.UNEXPECTED_ERROR);
			throw new GIRuntimeException(ReferralProcessingService.UNEXPECTED_ERROR);
		}
		this.markSpanInProgress(accountTransferRequest,ssapApplication.getId());
		ssapApplication = updateJsonValues(ssapApplication.getId());

		persistReferralActivation(ssapApplication.getId(), accountTransferRequest.isLCE(), AtResponseTypeEnum.fromValue(accountTransferRequest.getAtResponseType()),accountTransferRequest.getRequester());

		updateGiwspayload(ssapApplication.getId(), accountTransferRequest.getGiwsPayloadId());

		LOGGER.info("Created SSap application Id is - " + ssapApplication.getId());
		return ssapApplication;
	}

	private void markSpanInProgress(AccountTransferRequestDTO accountTransferRequest, long ssapId) {
		
		if(accountTransferRequest.getAtSpanInfoId() > 0){
			atSpanProcessingRepository.updateAtSpanStatusById(AccountTransferProcessStatus.INPROGRESS, accountTransferRequest.getAtSpanInfoId(), ssapId);
		}
	}

	/* Conversion - Starts */
	/**
	 * To create complete ssap db object for conversion project. including ssap json and marking verifications for each applicants.
	 * 
	 */
	@Override
	public SsapApplication executeCreate(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication) {
		LOGGER.info("In execute create of SsapApplication conversion...");
		SsapApplication ssapApplication = createSsapApplication(ssapJson, singleStreamlinedApplication, false, false, false, COVERAGE_YEAR,null,0,0,false);

		if (ssapApplication == null) {
			LOGGER.info(ReferralProcessingService.UNEXPECTED_ERROR);
			throw new GIRuntimeException(ReferralProcessingService.UNEXPECTED_ERROR);
		}

		/* Update Conversion related data - Starts */
		updateVerifications(ssapApplication);
		ssapApplication = updateCNJsonValues(ssapJson, ssapApplication.getId());
		/* Update Conversion related data - Ends */

		LOGGER.info("Created SSap application Id (conversion) is - " + ssapApplication.getId());
		return ssapApplication;
	}

	private void updateVerifications(SsapApplication ssapApplication) {

		List<SsapApplicant> applicants = ssapApplication.getSsapApplicants();
		for (SsapApplicant ssapApplicant : applicants) {
			updateVerificationStatus(ssapApplicant.getId());
		}

	}

	private static final String VERIFIED = "VERIFIED";

	private SsapApplicant updateVerificationStatus(Long ssapApplicantId) {
		SsapApplicant ssapApplicant = ssapApplicantRepository.findOne(ssapApplicantId);
		ssapApplicant.setSsnVerificationStatus(VERIFIED);
		ssapApplicant.setCitizenshipImmigrationStatus(VERIFIED);
		ssapApplicant.setVlpVerificationStatus(VERIFIED);
		ssapApplicant.setIncomeVerificationStatus(VERIFIED);
		ssapApplicant.setIncarcerationStatus(VERIFIED);
		ssapApplicant.setMecVerificationStatus(VERIFIED);
		ssapApplicant.setDeathStatus(VERIFIED);
		ssapApplicant.setResidencyStatus(VERIFIED);
		ssapApplicant.setNativeAmericanVerificationStatus(VERIFIED);

		return ssapApplicantRepository.save(ssapApplicant);

	}

	private SsapApplication updateCNJsonValues(String ssapJson, long ssapApplicationId) {
		String ssapJsonString = "";
		JSONObject ssapJsonObj;
		SsapApplication ssapApplication = null;
		try {
			ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
			ssapJsonObj = new JSONObject(ssapJson);
			JSONObject singleStreamlinedApplicationJson = ssapJsonObj.getJSONObject("singleStreamlinedApplication");
			singleStreamlinedApplicationJson.put("ssapApplicationId", "" + ssapApplication.getId());
			singleStreamlinedApplicationJson.put("applyingForFinancialAssistanceIndicator", "false");
			ssapJsonString = ssapJsonObj.toString();
			ssapApplication.setApplicationData(ssapJsonString);

			ssapApplication.setSource(ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode());
			ssapApplication.setFinancialAssistanceFlag("N");

			ssapApplication = ssapApplicationRepository.save(ssapApplication);
		} catch (JSONException jsonException) {
			LOGGER.error(ERROR_IN_JSON_STRING, jsonException);
			throw new GIRuntimeException(jsonException);
		}
		return ssapApplication;
	}

	/* Conversion - Ends */

	private void updateGiwspayload(long ssapApplicationId, int giwsPayloadId) {
		giwsPayloadRepository.updateSsapApplicationId(ssapApplicationId, giwsPayloadId);
	}

	private void persistReferralActivation(long ssapApplication, boolean isLce, AtResponseTypeEnum responseType,String requester) {
		LOGGER.info("Create Referral Activation Starts");
		ReferralActivation referralActivation = null;
		if(ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(StringUtils.stripToEmpty(requester))){
			referralActivation = referralActivationRepository.findBySsapApplication((int) ssapApplication);
		}
		else{
			referralActivation = new ReferralActivation();	
		}
		
		referralActivation.setSsapApplicationId((int) ssapApplication);
		referralActivation.setWorkflowStatus(ReferralActivationEnum.INITIAL_PHASE);
		referralActivation.setIsLce(ReferralUtil.converToYN(isLce));
		referralActivation.setResponseType(responseType);
		referralActivation = referralActivationRepository.save(referralActivation);
		LOGGER.info("Referral Activation Created with Id " + referralActivation.getId());
	}

	private SsapApplication updateJsonValues(long ssapApplicationId) {
		String ssapJsonString = "";
		SingleStreamlinedApplication singleStreamlinedApplication;
		SsapApplication ssapApplication = null;
		try {

			ssapApplication = ssapApplicationRepository.findOne(ssapApplicationId);
			ssapJsonString = ssapApplication.getApplicationData();
			singleStreamlinedApplication = ssapJsonBuilder.transformFromJson(ssapJsonString);
			singleStreamlinedApplication.setSsapApplicationId(String.valueOf(ssapApplication.getId()));
			ssapJsonString = ssapJsonBuilder.transformToJson(singleStreamlinedApplication);

			ssapApplication.setApplicationData(ssapJsonString);

			ssapApplication = ssapApplicationRepository.save(ssapApplication);
		} catch (Exception jsonException) {
			LOGGER.error(ERROR_IN_JSON_STRING, jsonException);
			throw new GIRuntimeException(jsonException);
		}
		return ssapApplication;
	}

	private SsapApplication createSsapApplication(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication, boolean isLce, boolean isQe, 
			boolean isRenewal, long coverageYear,String requester, long currentSpanApplicationId,long comparedToApplicationId,boolean isDMData) {
		SsapApplication ssapApplication = populateSsapApplication(ssapJson, singleStreamlinedApplication, isLce, isQe, isRenewal, coverageYear,requester,currentSpanApplicationId,comparedToApplicationId,isDMData);
		populateSsapApplicants(ssapApplication, singleStreamlinedApplication, isQe,requester);
		
		ssapApplication = ssapApplicationRepository.save(ssapApplication);

		return ssapApplication;
	}

	private void populateSsapApplicants(SsapApplication ssapApplication, SingleStreamlinedApplication singleStreamlinedApplication, boolean isQe,String requester) {
		
		// do not create new applicants when flow is from mes batch processing. 
		if(!ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(StringUtils.stripToEmpty(requester))){
			List<SsapApplicant> ssapApplicants = new ArrayList<SsapApplicant>();
			ssapApplication.setSsapApplicants(ssapApplicants);
		}
		
		// Create Current Applicants
		final List<HouseholdMember> householdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getHouseholdMember();
		String responsibleMemberPersonId = ssapUtil.getPrimaryTaxFilerPersonIdFromHouseholdMembers(householdMemberList);
		List<BloodRelationship> bloodRelationship = ssapUtil.getBloodRelationship(householdMemberList);
		final int size = ReferralUtil.listSize(householdMemberList);
		for (int i = 0; i < size; i++) {
			populateSsapApplicants(ssapApplication, householdMemberList.get(i), ReferralConstants.Y, isQe, bloodRelationship,responsibleMemberPersonId);
		}

		// Create Deleted Applicants
		final List<HouseholdMember> deletedHouseholdMemberList = singleStreamlinedApplication.getTaxHousehold().get(0).getDeletedHouseholdMember();
		final int deletedSize = ReferralUtil.listSize(deletedHouseholdMemberList);
		for (int i = 0; i < deletedSize; i++) {
			populateSsapApplicants(ssapApplication, deletedHouseholdMemberList.get(i), ReferralConstants.N, isQe, null,responsibleMemberPersonId);
		}
		
		if(ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(StringUtils.stripToEmpty(requester))){
			List<SsapApplicant> ssapApplicants = ssapApplication.getSsapApplicants();
			ssapApplicantRepository.save(ssapApplicants);
			ssapApplication.setSsapApplicants(ssapApplicants);
		}
	}
	
	@Override
	public void populateSsapApplicants(SsapApplication ssapApplication, HouseholdMember member, String onApplication, boolean isQe, 
			List<BloodRelationship> bloodRelationship,String responsibleMemberPersonId) {
		String stateCode = DynamicPropertiesUtil.getPropertyValue(GlobalConfiguration.GlobalConfigurationEnum.STATE_CODE);
		SsapApplicant ssapApplicant = null;
		Date date = new TSDate();
		BigDecimal[] locations;
		String phoneNumber = null;

		if (member.isPrimaryContact() || member.isPrimaryTaxFiler()) // For Primary and primary tax filer.
		{
			locations = populateLocations(member);
			phoneNumber = ReferralUtil.fetchPhonenumber(member);
		} 
		else {
			locations = null;
			phoneNumber = null;
		}
		
		// if mes batch process use existing ssap applicant, dont create new. 
		// code starts
		boolean spanSsapApplicantFound = false;
		if(ssapApplication != null && !ssapApplication.getSsapApplicants().isEmpty()){
			List<SsapApplicant> spanSsapApplicants = ssapApplication.getSsapApplicants();
			for (SsapApplicant spanSsapApplicant : spanSsapApplicants) {
				if(spanSsapApplicant != null && spanSsapApplicant.getApplicantGuid() != null && spanSsapApplicant.getApplicantGuid().equalsIgnoreCase(member.getApplicantGuid())){
					ssapApplicant = spanSsapApplicant;
					spanSsapApplicantFound = true;
					break;
				}
			}
		}
		// code ends
		
		// if applicant is not found, create a new one. 
		if(!spanSsapApplicantFound){
			ssapApplicant = new SsapApplicant();
			// Add to application only when it is new object.
			ssapApplication.addSsapApplicant(ssapApplicant);
		}
		

		ssapApplicant.setCreationTimestamp(new Timestamp(date.getTime()));
		ssapApplicant.setLastUpdateTimestamp(new Timestamp(date.getTime()));
		ssapApplicant.setNameSuffix(member.getName().getSuffix());
		ssapApplicant.setFirstName(member.getName().getFirstName());
		ssapApplicant.setLastName(member.getName().getLastName());
		ssapApplicant.setEmailAddress(member.getHouseholdContact().getContactPreferences().getEmailAddress());
		ssapApplicant.setMiddleName(member.getName().getMiddleName());
		ssapApplicant.setBirthDate(member.getDateOfBirth());
		if (member.getGender() != null) {
			ssapApplicant.setGender(member.getGender());
		}
		if (member.getExternalId() != null) {
			ssapApplicant.setExternalApplicantId(member.getExternalId());
		}
		if (member.getMarriedIndicatorCode() != null) {
			ssapApplicant.setMarried(member.getMarriedIndicatorCode());
		}else {
			ssapApplicant.setMarried(ReferralUtil.converToYesNo(member.getMarriedIndicator()));
		}
		if ("CA".equalsIgnoreCase(stateCode)) {
			if(member.getMarriedIndicatorCode() != null) {
				ssapApplicant.setMarried(member.getMarriedIndicatorCode());
			}else {
				ssapApplicant.setMarried("R");
			}
		}
		else {
			ssapApplicant.setTobaccouser(ReferralUtil.converToYN(member.getTobaccoUserIndicator()));	
		}
		
		ssapApplicant.setApplyingForCoverage(ReferralUtil.converToYN(member.getApplyingForCoverageIndicator()));
		ssapApplicant.setHouseholdContactFlag(ReferralUtil.converToYesNo(member.getHouseholdContactIndicator()));
		if (locations != null) {
			if (locations[0] != null) {
				ssapApplicant.setOtherLocationId(locations[0]); // this is the primary location
			}
			if (locations[1] != null) {
				ssapApplicant.setMailiingLocationId(locations[1]);
			}
		}
		ssapApplicant.setPersonId(member.getPersonId());
		
		ssapApplicant.setPersonType(member.getApplicantPersonType());
		if (member.getSocialSecurityCard() != null && ReferralUtil.isNotNullAndEmpty(member.getSocialSecurityCard().getSocialSecurityNumber())) {
			ssapApplicant.setSsn(member.getSocialSecurityCard().getSocialSecurityNumber());
		}
		ssapApplicant.setNativeAmericanFlag(ReferralUtil.converToYN(member.getSpecialCircumstances().getAmericanIndianAlaskaNativeIndicator()));
		ssapApplicant.setApplicantGuid(member.getApplicantGuid());
		ssapApplicant.setPhoneNumber(phoneNumber);
		if (isQe) {
			ssapApplicant.setStatus(ApplicantStatusEnum.QUALIFYING_EVENT.value());
		} else {
			ssapApplicant.setStatus(member.getStatus());
		}

		ssapApplicant.setHardshipExempt(member.getHardshipExempt());
		ssapApplicant.setEcnNumber(member.getEcnNumber());
		ssapApplicant.setOnApplication(onApplication);
		ssapApplicant.setEligibilityStatus("NONE");
		
		if(bloodRelationship!=null && bloodRelationship.size()>0){
			ssapApplicant.setRelationship(ssapUtil.getApplicantRelation(bloodRelationship, ssapApplicant.getPersonId(),responsibleMemberPersonId));
		}

		
	}
	
	private BigDecimal[] populateLocations(HouseholdMember member) {
		BigDecimal[] ids = new BigDecimal[2];
		HouseholdContact householdContact = member.getHouseholdContact();
		Location location = null;
		if (householdContact != null) {
			boolean primary = false;
			if (householdContact.getHomeAddress() != null && ReferralUtil.isNotNullAndEmpty(householdContact.getHomeAddress().getPostalCode())) {
				location = createLocation(householdContact.getHomeAddress());
				ids[0] = new BigDecimal(location.getId());
				primary = true;
			}

			if (householdContact.getMailingAddress() != null && ReferralUtil.isNotNullAndEmpty(householdContact.getMailingAddress().getPostalCode())) {
				location = createLocation(householdContact.getMailingAddress());
				ids[1] = new BigDecimal(location.getId());
				if (!primary) {
					ids[0] = ids[1];
				}
			}
		}
		return ids;
	}

	private Location createLocation(Address address) {
		Location newLocation = new Location();
		newLocation.setAddress1(address.getStreetAddress1());
		newLocation.setAddress2(address.getStreetAddress2());
		newLocation.setCity(address.getCity());
		newLocation.setState(address.getState());
		newLocation.setZip(address.getPostalCode());
		newLocation.setCounty(address.getCounty());
		newLocation.setCountycode(address.getCountyCode());
		newLocation = iLocationRepository.save(newLocation);
		return newLocation;

	}

	private SsapApplication populateSsapApplication(String ssapJson, SingleStreamlinedApplication singleStreamlinedApplication, 
			boolean isLce, boolean isQe, boolean isRenewal, long coverageYear,String requester, long currentSpanApplicationId,long comparedToApplicationId, boolean isDMData) {
		Date date = new TSDate();
		final HouseholdMember primaryHouseholdMember = ReferralUtil.primaryMember(singleStreamlinedApplication);
		if (primaryHouseholdMember == null) {
			throw new GIRuntimeException(ReferralProcessingService.NO_DATA);
		}
		SsapApplication ssapApplication = null;
		
		//If mes batch flow - use existing application. 
		if(ReferralProcessingConstants.REQUESTER_MES_BATCH.equalsIgnoreCase(StringUtils.stripToEmpty(requester))){
			ssapApplication = ssapApplicationRepository.findAndLoadApplicantsByAppId(currentSpanApplicationId);
			
		}
		else{
			ssapApplication = new SsapApplication();
			ssapApplication.setCreationTimestamp(new Timestamp(date.getTime()));
		}
		
		ssapApplication.setLastUpdateTimestamp(new Timestamp(date.getTime()));
		if(isDMData)
		{
			ssapApplication.setSource(ApplicationSource.DATA_MIGRATION_AT.getApplicationSourceCode());
		}else if (!isRenewal) {
			ssapApplication.setSource(ApplicationSource.REFERRALS_FROM_STATE.getApplicationSourceCode());
		}
		else {
			ssapApplication.setSource(ApplicationSource.CONVERSION_INCASE_OF_ID.getApplicationSourceCode());
		}
		
		final SingleStreamlinedApplication ssappln = ssapJsonBuilder.transformFromJson(ssapJson);
		
		List<HouseholdMember> houseHoldMemberList = ssappln.getTaxHousehold().get(0).getHouseholdMember();
		List<String> applicantGuidList = new ArrayList<String>();
		
		if (houseHoldMemberList != null && !houseHoldMemberList.isEmpty()) {
			for (HouseholdMember householdMember : houseHoldMemberList) {
				String applicantguid = householdMember.getApplicantGuid();
				applicantGuidList.add(applicantguid);
			}
		}
		
		ssapApplication.setCaseNumber(singleStreamlinedApplication.getApplicationGuid());
		if (singleStreamlinedApplication.getExternalApplicationId() != null) {
			ssapApplication.setExternalApplicationId(singleStreamlinedApplication.getExternalApplicationId());
		}
		if (singleStreamlinedApplication.getOutboundCaseNumber() != null) {
			List<SsapApplication> ssabApplicationDbList = ssapApplicationRepository.findByCaseNumber(singleStreamlinedApplication.getOutboundCaseNumber());
			if(ssabApplicationDbList.size() > 1){
				ssapApplication.setOutboundCaseNumber(singleStreamlinedApplication.getOutboundCaseNumber());
			}		}
		ssapApplication.setEsignFirstName(primaryHouseholdMember.getName().getFirstName());
		ssapApplication.setEsignMiddleName(primaryHouseholdMember.getName().getMiddleName());
		ssapApplication.setEsignLastName(primaryHouseholdMember.getName().getLastName());
		ssapApplication.setEsignDate(new Timestamp(singleStreamlinedApplication.getApplicationSignatureDate().getTime()));
		ssapApplication.setApplicationData(ssapJson);
		ssapApplication.setApplicationStatus(ApplicationStatus.UNCLAIMED.getApplicationStatusCode());
		if (isLce) {
			ssapApplication.setApplicationType(SEP);
		} else if (isQe) {
			ssapApplication.setApplicationType(QEP);
		} else {
			ssapApplication.setApplicationType(OE);
		}
		ssapApplication.setCurrentPageId("APPSCR51");
		ssapApplication.setFinancialAssistanceFlag(singleStreamlinedApplication.getApplyingForFinancialAssistanceIndicator()?"Y":"N");
		ssapApplication.setEligibilityStatus(EligibilityStatus.PE);
		ssapApplication.setExchangeEligibilityStatus(ExchangeEligibilityStatus.NONE);
		ssapApplication.setCoverageYear(coverageYear);
		ssapApplication.setExemptHousehold(singleStreamlinedApplication.getExemptHousehold());

		return ssapApplication;
	}

	

}
