<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="ecm" uri="/WEB-INF/tld/ecm.tld"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<!-- <link href="resources/css/slideshow-css_ms.css" rel="stylesheet"  type="text/css" /> -->
<!-- <script type="text/javascript" src="resources/js/slideshow.js"></script> -->

<div class="row-fluid home-slider" role="main">		
	<div id="gallery">
		<div id="slides">	
			<div class="slide">
					<c:set var="employerRoleName" ><encryptor:enc value="employer" isurl="true"/> </c:set>
					<h1>Employers</h1>
					<h2><spring:message code="label.employerHeader"/></h2>
					<p><spring:message code="label.employerText"/></p>
					<p><a class="btn btn-primary large" href="<c:url value="/account/signup/${employerRoleName}" />"><spring:message code="label.firsttimehereEmployer"/></a></p>
					<img src="<c:url value="/resources/images/ms_homepage_2.jpg" />" width="" height="" alt="Employers" title="Employers"  />
			</div>
					
			<div class="slide"> 
					<h1>Employees</h1>
                	<h2><spring:message code="label.employeeHeader"/></h2>                                                  
                    <p><spring:message code="label.employeeText"/></p>
                    <p><a class="btn btn-primary large" href="<c:url value="/account/user/login" />"><spring:message code="label.signinEmployee"/></a></p>
                    <img src="<c:url value="/resources/images/dottey5.jpg" />" alt="Employee" title="Employee" />
            </div>
					
			<div class="slide">
					<c:set var="brokerRoleName" ><encryptor:enc value="broker" isurl="true"/> </c:set>
					<h1>Agents</h1> 
					<h2><spring:message code="label.brokerHeader"/></h2>							
					<p><spring:message code="label.brokerText"/></p>
					<p><a class="btn btn-primary large" href="<c:url value="/account/signup/${brokerRoleName}" />"><spring:message code="label.firsttimehereAgent"/></a></p>
<%-- 				<p class="small"><spring:message code="label.alreadymember"/> <a href="<c:url value="account/user/login" />"><spring:message code="label.signin"/></a></p> --%>
					<img src="<c:url value="/resources/images/ms_homepage_1.jpg" />" alt="Agents" title="Agents" />
			</div>
		</div><!-- #slides -->
	</div><!-- #gallery -->
</div><!-- .row-fluid .home-slider -->
	
			
<!-- 		<button type="button" data-toggle="modal" data-target="#myModal">Launch modal</button> -->
	    <div id="myModal" class="modal hide fade">
		    <div class="modal-header">
		   		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		    	<h3>&nbsp;</h3>
		    </div>
		    <div class="modal-body">
		    	<h3 class="alert alert-info"><spring:message code="label.featureNotAvailable"/></h3>
		    </div>
		    <div class="modal-footer">
			    <a href="#" class="btn" data-dismiss="modal" aria-hidden="true"><spring:message code="label.close"/></a>
		    </div>
	    </div>		

	
<script type="text/javascript">
		$('#myModal').modal("hide")
</script>
