(function (models) {
	models.Plan = Backbone.Model.extend({
		
		urlRoot : "getEmployeePlans",
		
		url : function () {
			if(this.id)
				return 'getEmployeePlans/'+this.id+'?'+(Math.random()*10);
			else
				return 'getEmployeePlans?'+(Math.random()*10);
		},
		
		//to specify default attributes for model
		defaults: {
			//attrName : default value
			"id"		: "",
			"name"		: "",
			"level"		: "",
			"premium"	: "",
			"grossPremium"	: "",
			"issuer"		: "",
			"issuerText"	: "",
			"networkType"	: "",
			"monthlyPayrollDeduction" : "",
			"oopEstimate"	:"",
			"estimatedTotalHealthCareCost" : "",
			"employerContribution"	: "",
			"planScore"	: "",
			"oopMax"	: "",
			"deductible"	: "",
			"totalDeductible"	 : "",
			"familyDeductible"	: "",
			"employeeId"	: "",
			"isFavorite"	:""
		}
	});
})(App.models);