(function(){
	_.templateSettings = {
		    interpolate: /\<\@\=(.+?)\@\>/gim,
		    evaluate: /\<\@(.+?)\@\>/gim
		};
	window.App = {};
	App.collections = {};
	App.models = {};
	App.views = {};
	App.backbone = {};
	
	// Defer initialization until doc ready.
	$(function(){
		var collection = new App.collections.Tiers();
		var tierModel = new App.models.Tier();
		App.views.Tiers = new App.views.Tiers({collection:collection, tierModel:tierModel});
	});

})();