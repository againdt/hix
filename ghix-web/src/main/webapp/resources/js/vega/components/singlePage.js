﻿define(function () {
    var singlePage = new baseComponent();
    singlePage.type = "singlePage";
    singlePage.getHTML = function (cgfObj) {
        var vm = this;
        var defCfg = {
        };
        cgfObj = $.extend(defCfg, cgfObj);
        var compData = cgfObj;
        var rawHtml = this.context._getRawHTML(cgfObj.pageData);
        rawHtml = "<div id=\"" + cgfObj.id + "\">" + rawHtml + "</div>";
        return rawHtml;
    };
    return singlePage;
});