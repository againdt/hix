package com.getinsured.hix.batch.bulkusers.config;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.log4j.Logger;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.util.ResourceUtils;
import org.springframework.web.client.RestTemplate;

import com.getinsured.hix.batch.bulkusers.common.CommonUtil;


public class RestTemplateConfig {

	private static final Logger logger = Logger.getLogger(RestTemplateConfig.class);

	private String allPassword;
	private String username;
	private String password;
	private String jksFilePath;
	private int maxTotal;
	private int restApiTimeout;
	private String jobProcessId;
	
	public RestTemplateConfig() {
	}
	
	public RestTemplate restTemplate() throws KeyManagementException, UnrecoverableKeyException,
			NoSuchAlgorithmException, KeyStoreException, CertificateException, FileNotFoundException, IOException {
		logger.info("Calling ms-usermanagement rest api.  jobProcessId : "+jobProcessId);
		String platformKeyStore = CommonUtil.getPlatformKeyStorePath(jksFilePath);
		SSLContext sslContext = SSLContextBuilder.create()
				.loadKeyMaterial(ResourceUtils.getFile(platformKeyStore), allPassword.toCharArray(),
						allPassword.toCharArray())
				.loadTrustMaterial(ResourceUtils.getFile(platformKeyStore), allPassword.toCharArray())
				.build();
		
		CredentialsProvider credentialsProvider = new BasicCredentialsProvider();

		credentialsProvider.setCredentials(AuthScope.ANY,
				new UsernamePasswordCredentials(username, password));
		
		HttpClient httpClient = HttpClients.custom().setSSLContext(sslContext)
				.setConnectionManager(new PoolingHttpClientConnectionManager(RegistryBuilder.<ConnectionSocketFactory> create()
					      .register("http", PlainConnectionSocketFactory.INSTANCE)
					      .register("https", new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE))
					      .build()))
				.setConnectionTimeToLive(this.restApiTimeout, TimeUnit.MINUTES)
				.setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
				.setDefaultCredentialsProvider(credentialsProvider).build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		
		requestFactory.setHttpClient(httpClient);
		
		return new RestTemplate(requestFactory);

	}

	public String getAllPassword() {
		return allPassword;
	}

	public void setAllPassword(String allPassword) {
		this.allPassword = allPassword;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getJksFilePath() {
		return jksFilePath;
	}

	public void setJksFilePath(String jksFilePath) {
		this.jksFilePath = jksFilePath;
	}

	public int getMaxTotal() {
		return maxTotal;
	}

	public void setMaxTotal(int maxTotal) {
		this.maxTotal = maxTotal;
	}

	public int getRestApiTimeout() {
		return restApiTimeout;
	}

	public void setRestApiTimeout(int restApiTimeout) {
		this.restApiTimeout = restApiTimeout;
	}

	public String getJobProcessId() {
		return jobProcessId;
	}

	public void setJobProcessId(String jobProcessId) {
		this.jobProcessId = jobProcessId;
	}
	
}
