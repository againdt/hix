package com.getinsured.hix.planmgmt.service.jpa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import com.getinsured.timeshift.util.TSDate;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.PlanCsrMultiplier;
import com.getinsured.hix.model.PlanCsrMultiplier.PlanLevel;
import com.getinsured.hix.planmgmt.repository.ICSRMultiplierRepository;
import com.getinsured.hix.planmgmt.service.CSRMultiplierService;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixConstants;
import com.getinsured.hix.platform.util.SecurityUtil;
import com.getinsured.hix.platform.util.exception.GIRuntimeException;
import com.getinsured.hix.util.PlanMgmtConstants;

@Service("cSRMultiplierService")
public class CSRMultiplierServiceImpl implements CSRMultiplierService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CSRMultiplierServiceImpl.class);
	@Autowired private ICSRMultiplierRepository icsrMultiplierRepository;
	@Autowired private UserService userService;
	@PersistenceUnit private EntityManagerFactory emf;
	private SimpleDateFormat dateFormat = null;
	private SimpleDateFormat formatDate = null;
	private static final String CS01 = "01";
	private static final String CS02 = "02";
	private static final String CS03 = "03";
	private static final String CS04 = "04";
	private static final String CS05 = "05";
	private static final String CS06 = "06";
	private static final int ZERO = 0;
	private static final int ONE = 1;
	private static final int TWO = 2;
	private static final int THREE = 3;
	private static final int FOUR = 4;
	private static final String ANY_TIER = "Any Tier";
	private static final String[] METAL_TIERS = {PlanLevel.SILVER.name(),PlanLevel.BRONZE.name(),PlanLevel.GOLD.name(),PlanLevel.PLATINUM.name(),ANY_TIER};

	@Value(PlanMgmtConstants.DATABASE_TYPE_CONFIG)
	private String DB_TYPE;

	@Override
	public List<PlanCsrMultiplier> getCSRMultiplierList() {
		return icsrMultiplierRepository.findAll();
	}

	@Override
	public void updateCSRMultiplier(String planCsrMultiplier) {
		try {
			if(StringUtils.isNotBlank(planCsrMultiplier)){
				String[] planCsrMultipliers = planCsrMultiplier.split("@");
				AccountUser user = userService.getLoggedInUser();
				LOGGER.info("Getting user from session");
				for(String multiplier : planCsrMultipliers) {
					String[] planCsr = multiplier.split("_");
					PlanCsrMultiplier csrMultiplier = icsrMultiplierRepository.findOne(Integer.parseInt(planCsr[ZERO]));
					double multiplierVal =  Double.parseDouble(planCsr[ONE]);
					if(multiplierVal != csrMultiplier.getMultiplier() && !(multiplierVal >= ONE) && planCsr[ONE].contains(".") && !(planCsr[ONE].substring(planCsr[ONE].indexOf(".")).substring(ONE).length()>TWO)){
						csrMultiplier.setMultiplier(csrMultiplier.getMultiplier());
						csrMultiplier.setLastUpdateTimestamp(new TSDate());
						csrMultiplier.setLastUpdatedBy(user.getId());
						csrMultiplier = icsrMultiplierRepository.save(csrMultiplier);
						
						csrMultiplier.setMultiplier(multiplierVal);
						icsrMultiplierRepository.save(csrMultiplier);
						
						LOGGER.info("csrMultiplier update success");
					}
				}
			}
		} catch (Exception e) {
			LOGGER.error("Error while Updating CSR",e);
		}
	}

	@Override
	@Deprecated
	public List<Map<String,Object>> getCSRMultiplierHistory(HttpServletRequest request,Model model) {
		List<Map<String,Object>> csrHistoryList = new ArrayList<Map<String,Object>>();
		StringBuilder queryObj = new StringBuilder();
		StringBuilder queryForCount = new StringBuilder();
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		formatDate = new SimpleDateFormat("dd/MM/yyyy");
		Integer pageSize = GhixConstants.PAGE_SIZE;
		String param = request.getParameter("pageNumber");
		Integer startRecord = 0;
		EntityManager em = null;
		int resultCount = 0;
		
		if (param != null) {
			startRecord = (Integer.parseInt(param) - 1) * pageSize;
		}
		
		queryObj.append("SELECT t.* FROM (");

		if (null != DB_TYPE && DB_TYPE.equalsIgnoreCase(PlanMgmtConstants.POSTGRES)) {
			queryObj.append("SELECT t.* FROM (SELECT pcma.id, pcma.last_update_timestamp,pcma.CSR_VARIATION, pcma.PLAN_LEVEL,pcma.MULTIPLIER,(SELECT u.FIRST_NAME FROM users u WHERE u.id=pcma.LAST_UPDATED_BY) FROM PM_CSR_MULTIPLIER_AUD pcma ORDER BY pcma.REV DESC");
			queryObj.append(" ) t ");
			queryObj.append(" offset " + startRecord);
			queryObj.append(" limit " + pageSize);
		}
		else {
			queryObj.append("SELECT ROWNUM AS rn,t.* FROM (SELECT pcma.last_update_timestamp,pcma.CSR_VARIATION, pcma.PLAN_LEVEL,pcma.MULTIPLIER,(SELECT u.FIRST_NAME FROM users u WHERE u.id=pcma.LAST_UPDATED_BY) FROM PM_CSR_MULTIPLIER_AUD pcma ORDER BY pcma.REV DESC");
			queryObj.append(") t WHERE ROWNUM <= " + (startRecord + pageSize) + " ) t ");
			queryObj.append(" WHERE rn > " + startRecord);
		}
		
		queryForCount.append("SELECT count(*) FROM PM_CSR_MULTIPLIER_AUD pcma");
		
		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("Build data query " + SecurityUtil.sanitizeForLogging(String.valueOf(queryObj.toString())));
		}
		
		try{
			em = emf.createEntityManager();
			List rsList = em.createNativeQuery(queryObj.toString()).getResultList();
			Iterator rsIterator = rsList.iterator();
			
			while (rsIterator.hasNext()) {
				Map<String,Object> csrDataMap = new HashMap<String,Object>(); 
				Object[] objArray = (Object[]) rsIterator.next();
				try {
					csrDataMap.put("creation_timestamp",formatDate.format(dateFormat.parse(objArray[1].toString())));
				} catch (ParseException e) {
					LOGGER.error("date parse error",e);
				}
				csrDataMap.put("csrVariation", objArray[2].toString());
				csrDataMap.put("planLevel", objArray[3].toString());
				csrDataMap.put("multiplier", objArray[4].toString());
				csrDataMap.put("newMultiplierVal", objArray[4].toString());
				csrDataMap.put("lastUpdatedBy", objArray[5].toString());
				
				csrHistoryList.add(csrDataMap);
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getCSRMultiplierHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getCSRMultiplierHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
						
		if(LOGGER.isDebugEnabled()){
		   LOGGER.debug("Build count query " + SecurityUtil.sanitizeForLogging(String.valueOf(queryForCount)));
		}
		
		try{
			em = emf.createEntityManager();
		   	Object objCount = em.createNativeQuery(queryForCount.toString()).getSingleResult();
			if (objCount != null) {
				resultCount = Integer.parseInt(objCount.toString());
			}
		}catch (Exception e) {
			LOGGER.error("ERROR in getCSRMultiplierHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			throw new GIRuntimeException("ERROR in getCSRMultiplierHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
		}finally{
			if(em !=null && em.isOpen()){
				em.clear();
				em.close();
			}
		}
		
		model.addAttribute("resultSize",resultCount);
		return csrHistoryList;
	}

	@Override
	public List<Map<String, Object>> getCSRMultiplierHistory() {
		List<Map<String,Object>> csrHistoryList = new ArrayList<Map<String,Object>>();
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
		formatDate = new SimpleDateFormat("dd/MM/yyyy");
		String[] csrVariations = {CS01,CS02,CS03,CS04,CS05,CS06};
		for(String metalTier : METAL_TIERS) {
			List<Map<String,Object>> csrDataMap = null;
			csrDataMap = getLatestHistory(csrVariations,metalTier);
			
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Result data for metalTier "+ SecurityUtil.sanitizeForLogging(String.valueOf(metalTier)) +" result data "+ SecurityUtil.sanitizeForLogging(String.valueOf(csrDataMap)));
			}
			if(csrDataMap.size() > ZERO){
				csrHistoryList.addAll(csrDataMap);
			}
		}
		return csrHistoryList;
	}
	
	private List<Map<String,Object>> getLatestHistory(String[] csrVariation,String metalTier){
		
		List<Map<String,Object>> csrHistoryList = new ArrayList<Map<String,Object>>();;
		Map<String,Object> csrDataMap = null;
		EntityManager em = null;
		for(String csr : csrVariation) {
			
			String queryObj = "SELECT pcma.last_update_timestamp,pcma.CSR_VARIATION, pcma.PLAN_LEVEL,pcma.MULTIPLIER,(SELECT u.FIRST_NAME FROM users u WHERE u.id=pcma.LAST_UPDATED_BY) FROM PM_CSR_MULTIPLIER_AUD pcma WHERE lower(pcma.PLAN_LEVEL)=lower('"+metalTier+"') AND pcma.CSR_VARIATION='"+csr+"' ORDER BY pcma.REV DESC";
			
			if(LOGGER.isDebugEnabled()){
			   LOGGER.debug("Build data query " + SecurityUtil.sanitizeForLogging(String.valueOf(queryObj.toString())));
			}
			
			try {
				em = emf.createEntityManager();
				List rsList = em.createNativeQuery(queryObj.toString()).getResultList();
				
				if(rsList != null){
					
					Iterator rsIterator = rsList.iterator();
					
					while(rsIterator.hasNext()) {
						csrDataMap = new HashMap<String,Object>(); 
						Object[] latestObjArray = (Object[]) rsIterator.next();
						try {
							csrDataMap.put("creation_timestamp",formatDate.format(dateFormat.parse(latestObjArray[ZERO].toString())));
						} catch (ParseException e) {
							LOGGER.error("Error while parsing date",e.getMessage());
						}
						csrDataMap.put("csrVariation", latestObjArray[ONE].toString());
						csrDataMap.put("planLevel", latestObjArray[TWO].toString());
						csrDataMap.put("newMultiplierVal", latestObjArray[THREE].toString());
						csrDataMap.put("lastUpdatedBy", latestObjArray[FOUR].toString());
						try {
							Object[] prvObjArray = (Object[]) rsIterator.next();
							csrDataMap.put("prvMultiplierVal", prvObjArray[THREE].toString());
						} catch (Exception e) {
							LOGGER.error("Error while getting next records",e.getMessage());
						}
						csrHistoryList.add(csrDataMap);
						
						if(LOGGER.isDebugEnabled()){
						   LOGGER.debug("final data map " + SecurityUtil.sanitizeForLogging(String.valueOf(csrDataMap)));
						}
						csrDataMap = null;
					}
				}
			}catch (Exception e) {
				LOGGER.error("ERROR in getLatestHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
				throw new GIRuntimeException("ERROR in getLatestHistory >>>>>>>>>>>>>>>>>>>>>>>>>>>"+e.getMessage());
			}finally{
				if(em !=null && em.isOpen()){
					em.clear();
					em.close();
				}
			}
		}
		return csrHistoryList;
	}
}
