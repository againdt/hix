package com.getinsured.hix.iex.lce.task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.getinsured.hix.consumer.util.ConsumerPortalUtil;
import com.getinsured.hix.model.AccountUser;
import com.getinsured.hix.model.consumer.Household;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.security.exception.InvalidUserException;
import com.getinsured.hix.platform.security.service.UserService;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.iex.client.ResourceCreator;
import com.getinsured.iex.dto.SepEventsResource;
import com.getinsured.iex.dto.SsapApplicationResource;
import com.getinsured.iex.util.LifeChangeEventConstant;
import com.getinsured.iex.util.SsapUtil;
import com.thoughtworks.xstream.XStream;
import com.getinsured.hix.platform.util.GhixUtils;

@Component
public class LifeChangeEventHandler {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LifeChangeEventHandler.class);
	
	protected ThreadLocal<Map<String, Boolean>> demographicFlagMap;
	protected ThreadLocal<Map<Long, List<String>>> demographicEventsData;
	protected ThreadLocal<Map<Long, List<SepEventsResource>>> multipleEventData;
	protected ThreadLocal<List<Long>> currentApplication;
	protected ThreadLocal<Set<Long>> currentApplicants;
	protected ThreadLocal<List<Long>> currentapplicationEvent;
	protected ThreadLocal<Set<Long>> currentApplicantEvents;
	protected ThreadLocal<Map<Long, String>>  previousApplicationDetails;
	protected ThreadLocal<String>  isUserSwitchToOtherView;
	protected ThreadLocal<Date>  dobEventDate;
	protected ThreadLocal<Date>  demographicEventDate;
	protected ThreadLocal<List<Boolean>>  homeAddressChangeDemoFlags;
	
	@Autowired
	private ResourceCreator resourceCreator;
	@Autowired
	private UserService userService;
	@Autowired 
	private ConsumerPortalUtil consumerPortalUtil;
	@Autowired
	private GhixRestTemplate ghixRestTemplate;

	public void handleTrasaction(){
		String tableName;
		
		try {
			tableName = SsapUtil.SSAP_TABLE_NAMES.SSAP_APPLICANT_EVENTS.toString();
			if(currentApplicantEvents.get()!=null && currentApplicantEvents.get().size()>0){
				deleteRecords(tableName, currentApplicantEvents.get());
			}
			
			tableName = SsapUtil.SSAP_TABLE_NAMES.SSAP_APPLICATION_EVENTS.toString();
			if(currentapplicationEvent.get()!=null && currentapplicationEvent.get().size()>0){
				deleteRecords(tableName, currentapplicationEvent.get());
			}
			
			tableName = SsapUtil.SSAP_TABLE_NAMES.SSAP_APPLICANTS.toString();
			if(currentApplicants.get()!=null && currentApplicants.get().size()>0){
				deleteRecords(tableName, currentApplicants.get());
			}
			
			tableName = SsapUtil.SSAP_TABLE_NAMES.SSAP_APPLICATIONS.toString();
			if(currentApplication.get()!=null && currentApplication.get().size()>0){
				deleteRecords(tableName, currentApplication.get());
			}
			
			reset();
		} catch (Exception e) {
		}
	}
	
	public void deleteRecords(String tableName, Collection<Long> ids){
		for(Long id:ids){
			resourceCreator.deleteRecordById(tableName, id);
		}
	}
	
	public void init() throws InvalidUserException{
		isUserSwitchToOtherView = new ThreadLocal<String>();
		demographicFlagMap = new ThreadLocal<Map<String, Boolean>>();
		demographicEventsData = new ThreadLocal<Map<Long, List<String>>>();
		multipleEventData = new ThreadLocal<Map<Long, List<SepEventsResource>>>();
		currentApplication = new ThreadLocal<List<Long>>();
		currentApplicants = new ThreadLocal<Set<Long>>();
		currentapplicationEvent = new ThreadLocal<List<Long>>();
		currentApplicantEvents = new ThreadLocal<Set<Long>>();
		previousApplicationDetails = new ThreadLocal<Map<Long, String>>();
		dobEventDate = new ThreadLocal<Date>();
		demographicEventDate = new ThreadLocal<Date>();
		homeAddressChangeDemoFlags = new ThreadLocal<List<Boolean>>(); 
		
		demographicFlagMap.set(new HashMap<String, Boolean>());
		demographicEventsData.set(new HashMap<Long, List<String>>());
		multipleEventData.set(new HashMap<Long, List<SepEventsResource>>());
		currentApplication.set(new ArrayList<Long>());
		currentApplicants.set(new HashSet<Long>());
		currentapplicationEvent.set(new ArrayList<Long>());
		currentApplicantEvents.set(new HashSet<Long>());
		previousApplicationDetails.set(new HashMap<Long, String>());
		homeAddressChangeDemoFlags.set(new ArrayList<Boolean>());
		
		demographicFlagMap.get().put(LifeChangeEventConstant.IS_DEMOGRAPHIC_CHANGE, false);
		demographicFlagMap.get().put(LifeChangeEventConstant.IS_DOB_CHANGE, false);
		demographicFlagMap.get().put(LifeChangeEventConstant.IS_OTHER_CHANGE, false);
		demographicFlagMap.get().put(LifeChangeEventConstant.IS_ADDRESS_DEMOGRAPHIC_CHANGE, false);
	}
	
	public void reset(){
		currentApplication = null;
		currentApplicants = null;
		currentapplicationEvent = null;
		currentApplicantEvents = null;
		demographicFlagMap =null;
		demographicEventsData = null;
		previousApplicationDetails = null;
		isUserSwitchToOtherView = null;
	}
	
	public SsapApplicationResource updateSsapApplication(long applicationId, SsapApplicationResource ssapApplicationResource) throws Exception {
		try {
			if(ssapApplicationResource != null){
				return resourceCreator.updateSsapApplicationById(applicationId, ssapApplicationResource);
			} else{
				throw new Exception("SSAP Application does not exist.");
			}
		} catch (Exception exception) {
			LOGGER.error("Exception occurred while updating ssap json : ", exception);
			throw exception;
		}
	}
	
	protected AccountUser getUser() throws Exception{
		AccountUser user;
		if(isUserSwitchToOtherView != null && isUserSwitchToOtherView.get()!=null && isUserSwitchToOtherView.get().equalsIgnoreCase("y")){
			user = getHouseHoldByHouseholdId(userService.getLoggedInUser().getActiveModuleId()).getUser();
			if(user == null) {
				user = userService.getLoggedInUser();
			}
		} else {
			user = userService.getLoggedInUser();
		}
		
		return user;
	}
	
	protected Household getHouseHoldByHouseholdId(int householdId) {
		LOGGER.info("Rest call to get householeId : Starts");
		Household household = null;
		try {
			household = consumerPortalUtil.getHouseholdRecord(householdId);			
		} catch(Exception exception){
			LOGGER.error("Rest call to find household failed", exception);
			throw exception;
		}
		
		LOGGER.info("Rest call to get householeId : Ends");
		return household;
	}
	
	protected Household getHouseHoldByUserId(int userId){
		LOGGER.info("Rest call to get householeId : Starts");

		Household household = null;
		XStream xstream = null;
		String response = null;

		try {
			xstream = GhixUtils.getXStreamStaxObject();
			response = ghixRestTemplate.postForObject(GhixEndPoints.ConsumerServiceEndPoints.GET_HOUSEHOLD_BY_USER_ID, String.valueOf(userId), String.class);
			
			if(response != null){
				household = (Household) xstream.fromXML(response);
			}
		} catch(Exception exception){
			LOGGER.error("Rest call to find household failed", exception);
			throw exception;
		}
		
		LOGGER.info("Rest call to get householeId : Ends");
		return household;
	}
}
