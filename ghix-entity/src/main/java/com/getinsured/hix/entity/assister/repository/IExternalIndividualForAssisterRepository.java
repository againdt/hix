package com.getinsured.hix.entity.assister.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.getinsured.hix.model.ExternalIndividual;

/**
 * Repository to fetch data from external individual table
 */
public interface IExternalIndividualForAssisterRepository extends JpaRepository<ExternalIndividual, Long> {

	/**
	 * Retrieves data for external individual
	 * 
	 * @param extindividualid external individual id
	 * @return {@link ExternalIndividual}
	 */
	@Query("FROM ExternalIndividual as an where an.id = :extindividualid")
	ExternalIndividual findByIndividualCaseId(
			@Param("extindividualid") long extindividualid);

}