package com.getinsured.eligibility.at.ref.service.migration;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.getinsured.eligibility.at.ref.dto.LCEProcessRequestDTO;
import com.getinsured.eligibility.at.ref.service.FinancialApplicationConversionService;
import com.getinsured.eligibility.at.resp.si.dto.ApplicantEvent;
import com.getinsured.eligibility.enums.ApplicationStatus;
import com.getinsured.iex.ssap.model.SsapApplication;
import com.getinsured.iex.ssap.repository.SsapApplicationRepository;

@Component("entireHHLostEligibilityAndConversionMigration")
public class EntireHHLostEligibilityAndConversionMigrationHandler extends LceProcessHandlerBaseMigrationService {

	@Autowired
	@Qualifier("ssapApplicationEventMigrationService")
	private SsapApplicationEventMigrationService ssapApplicationEventMigrationService;

	@Autowired
	private FinancialApplicationConversionService financialApplicationConversionService;
	
	@Autowired
	private SsapApplicationRepository ssapApplicationRepository;
	
	/**
	 * When some applicant has non Qhp indicator create application and applicant events by taking least event date as enrollment start date +70 days as end date modify json and other data for financail to non finacial conv. update current application to SEP-ER.
	 * 
	 * @param enrolledApplicantsGuid
	 * @param lceProcessRequestDTO 
	 */

	public void executeSomeApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, List<String> enrolledApplicantsGuid, LCEProcessRequestDTO lceProcessRequestDTO) {
		ssapApplicationEventMigrationService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		getValidationStatus(currentApplication.getCaseNumber());
		
		//HIX-108047
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		
		financialApplicationConversionService.convertFinancialAppToNonFinancial(currentApplication, enrolledApplication, applicantEventMap);
		currentApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		updateAllowEnrollment(currentApplication, Y);

	}

	/**
	 * When no applicant has non Qhp indicator create application and applicant events by taking today's date as enrollment start date +70 days as end date modify json and other data for financail to non finacial conv. Check enrolled plan has cs plan benefits and handle.
	 * 
	 * @param enrolledApplicantsGuid
	 * @param lceProcessRequestDTO 
	 * 
	 */

	public void executeNoApplicantHasNonQhpIndicator(SsapApplication currentApplication, SsapApplication enrolledApplication, Map<String, ApplicantEvent> applicantEventMap, List<String> enrolledApplicantsGuid, String enrollmentCostSharing, LCEProcessRequestDTO lceProcessRequestDTO) {
		ssapApplicationEventMigrationService.createDeniedHouseholdApplicationEvent(currentApplication, applicantEventMap);
		getValidationStatus(currentApplication.getCaseNumber());
		
		//HIX-108047
		if(   currentApplication.getId() != 0  ) {
			currentApplication = loadCurrentApplication(currentApplication.getId());
		}
		
		financialApplicationConversionService.convertFinancialAppToNonFinancial(currentApplication, enrolledApplication, applicantEventMap);
		currentApplication.setApplicationStatus(ApplicationStatus.ELIGIBILITY_RECEIVED.getApplicationStatusCode());
		updateAllowEnrollment(currentApplication, Y);
    }
	
	public SsapApplication loadCurrentApplication(Long currentApplicationId) {
		return ssapApplicationRepository.findAndLoadApplicantsByAppId(currentApplicationId);
	}

}

