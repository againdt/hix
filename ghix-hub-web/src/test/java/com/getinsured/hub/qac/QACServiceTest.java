package com.getinsured.hub.qac;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ws.client.core.WebServiceTemplate;

import com.getinsured.hub.platform.GhixBaseTest;
import com.getinsured.iex.hub.platform.BridgeException;
import com.getinsured.iex.hub.platform.HubMappingException;
import com.getinsured.iex.hub.platform.HubServiceBridge;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.platform.RequestStatus;
import com.getinsured.iex.hub.platform.ServiceHandler;
import com.getinsured.iex.hub.platform.models.GIHUBResponse;
import com.getinsured.iex.hub.platform.models.GIWSPayload;
import com.getinsured.iex.hub.platform.repository.base.GIWSPayloadRepository;
import com.getinsured.iex.hub.platform.repository.base.HubResponseRepository;

public class QACServiceTest extends GhixBaseTest {

	@Autowired
	private GIWSPayloadRepository giWSPayloadRepository;
	@Autowired
	private HubResponseRepository hubResponseRepository;
	@Autowired
	private WebServiceTemplate qacServiceTemplate;
	
	@Test
	public void testInvalidSSNForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidSSN = {"22222479","abc","22222479133","","23@#$@$","Barrera"};
		
		for(int i=0; i<invalidSSN.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"" + invalidSSN[i] + "\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidSSNForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validSSN = {"222224850","222-22-4850"};
		
		for(int i=0; i<validSSN.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"" + validSSN[i] + "\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	
	@Test
	public void testInvalidDOBForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidDOB = {"5 May, 2013","02/30/1988","asda","","02/21/2016","Barrera","Jul 30, 1950 00:00:00 AM","Jul 30, 1950 00:00:00 PM","Jul 30, 1950 00:00:00 BM","Jul 30, 1950 0:0:0","Jul 30, 2015 0:0:0","Jul 30, 2015 000012:0000011:000000055", "JUL 30, 1950 00:00:00","AUGUST 30, 2015 00:00:00"};
		
		for(int i=0; i<invalidDOB.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"" + invalidDOB[i] + "\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidDOBForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validDOB = {"02/21/1988","05/09/2014","06/06/1966"};
		
		for(int i=0; i<validDOB.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"" + validDOB[i] + "\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	@Test
	public void testInvalidFirstNameForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidName = {"", "ghghhghghghghghghghdsdsdsdsdsdsdsdsdsddsdsdsdsghghsdsddsdsdsdsdsdsdsdsdssdsdsdsdddde"};
		
		for(int i=0; i<invalidName.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"" + invalidName[i] + "\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidFirstNameForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validName = {"joy","j","poiuytrewqlkjhgfdsamnbvcxzpoiuytrewqlkjhgfdsamnbvc"};
		
		for(int i=0; i<validName.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"" + validName[i] + "\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	@Test
	public void testInvalidLastNameForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidName = {"", "ghghhghghghghghghghdsdsdsdsdsdsdsdsdsddsdsdsdsghghsdsddsdsdsdsdsdsdsdsdssdsdsdsdddde"};
		
		for(int i=0; i<invalidName.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"" + invalidName[i] + "\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidLastNameForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validName = {"joy","j","poiuytrewqlkjhgfdsamnbvcxzpoiuytrewqlkjhgfdsamnbvc"};
		
		for(int i=0; i<validName.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"" + validName + "\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	@Test
	public void testInvalidCoverageEndDateForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidDate = {"5 May, 2013","02/30/1988","asda","","02/21/2016","Barrera","Jul 30, 1950 00:00:00 AM","Jul 30, 1950 00:00:00 PM","Jul 30, 1950 00:00:00 BM","Jul 30, 1950 0:0:0","Jul 30, 2015 0:0:0","Jul 30, 2015 000012:0000011:000000055", "JUL 30, 1950 00:00:00","AUGUST 30, 2015 00:00:00"};
		
		for(int i=0; i<invalidDate.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"" + invalidDate + "\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidCoverageEndDateForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validDate = {"02/21/1988","05/09/2014","06/06/1966"};
		
		for(int i=0; i< validDate.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"" + validDate[i] + "\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	
	@Test
	public void testInvalidCoverageStartDateForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] invalidDate = {"5 May, 2013","02/30/1988","asda","","02/21/2016","Barrera","Jul 30, 1950 00:00:00 AM","Jul 30, 1950 00:00:00 PM","Jul 30, 1950 00:00:00 BM","Jul 30, 1950 0:0:0","Jul 30, 2015 0:0:0","Jul 30, 2015 000012:0000011:000000055", "JUL 30, 1950 00:00:00","AUGUST 30, 2015 00:00:00"};
		
		for(int i=0; i<invalidDate.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"" + invalidDate + "\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNull(requestPayload);
		}
	}
	
	@Test
	public void testValidCoverageStartDateForQAC(){
		
		Object requestPayload = null;
		ServiceHandler handler = null;
		
		String [] validDate = {"02/21/1988","05/09/2014","06/06/1966"};
		
		for(int i=0; i<validDate.length; i++){
			try{
				requestPayload = null;
				String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"" + validDate[i] + "\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
				HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
				handler = qacBridge.getServiceHandler();
				handler.setJsonInput(qacInputJSON);
		
				// Creating the payload
				requestPayload = handler.getRequestpayLoad();
			}
			catch(Exception e){
				getLog().error(e.getMessage());
			}
			
			Assert.assertNotNull(requestPayload);
		}
	}
	@Test
	@Transactional(readOnly = true)
	public void testQACService() throws HubMappingException, BridgeException, HubServiceException {

		ServiceHandler handler = null;
		GIWSPayload requestRecord = null;

		String qacInputJSON = "{\"taxHousehold\":[{\"householdMember\":[{\"insurance\":{\"insurancePolicyEffectiveDate\":\"01/01/2014\",\"insurancePolicyExpirationDate\":\"01/01/2015\"},\"dateOfBirth\":\"05/04/2006\",\"socialSecurityCard\":{\"socialSecurityNumber\":\"765544352\"},\"name\":{\"lastName\":\"Sen\",\"firstName\":\"Kyle\"}}]}]}";
		HubServiceBridge qacBridge = HubServiceBridge.getHubServiceBridge("QACRequest");
		handler = qacBridge.getServiceHandler();
		handler.setJsonInput(qacInputJSON);

		requestRecord = new GIWSPayload();
		requestRecord.setEndpointFunction("QAC");
		requestRecord.setEndpointOperationName("QAC");
		requestRecord.setAccessIp("0.0.0.0");
		requestRecord.setStatus("PENDING");
		requestRecord.setSsapApplicationId(null);

		// Persist initial request
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);
		Assert.assertNotSame(0L, requestRecord.getId());

		// Creating the payload
		Object requestPayload = handler.getRequestpayLoad();
		Assert.assertNotNull(requestPayload);

		// Call HUB service
		long start = System.currentTimeMillis();
		Object webserviceResponse = qacServiceTemplate.marshalSendAndReceive(requestPayload);
		long timeTaken = System.currentTimeMillis() - start;
		Assert.assertNotNull(webserviceResponse);

		// Save final request
		requestRecord.setEndpointUrl(qacServiceTemplate.getDefaultUri());
		requestRecord.setRequestPayload(handler.getJsonInput());
		requestRecord.setStatus(RequestStatus.SUCCESS.getStatusCode());
		requestRecord = this.giWSPayloadRepository.saveAndFlush(requestRecord);

		// Process and save hub response
		String responseStr = handler.handleResponse(webserviceResponse);
		Assert.assertNotNull(responseStr);
		Assert.assertNotSame(responseStr, "");
		GIHUBResponse hubResponse = new GIHUBResponse();
		hubResponse.setEndPointURL(qacServiceTemplate.getDefaultUri());
		hubResponse.setGiWspayloadId(requestRecord.getId());
		hubResponse.setClientNodeIp("0.0.0.0");
		hubResponse.setSsapApplicationId(requestRecord.getSsapApplicantId());
		hubResponse.setStatus(RequestStatus.SUCCESS.getStatusCode());
		hubResponse.setElapsedTime(timeTaken);
		hubResponse.setHubResponsePayload(responseStr);
		hubResponse.setHubResponseCode(handler.getResponseCode());
		hubResponse = hubResponseRepository.save(hubResponse);
		Assert.assertNotSame(0L, hubResponse.getId());
		Assert.assertNotNull(hubResponse.getHubResponsePayload());

	}

}
