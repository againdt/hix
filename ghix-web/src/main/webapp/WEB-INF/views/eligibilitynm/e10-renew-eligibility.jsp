<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<title>View account</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/bootstrap.css">
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghixcustom.css"> -->
<!-- <link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ghix-blue-theme.css"> -->
<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.8.20.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/ms-custom.css">
<link type="text/css" rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Droid+Serif:400,700">
<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
      <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<link rel="stylesheet" href="../resources/eligibilitynm/css/planView-v2.css" type="text/css">
<link rel="stylesheet" href="../resources/eligibilitynm/css/planViewStyles-v2.css" type="text/css">
	<link href="../resources/eligibilitynm/css/ui-lightness/jquery-ui-1.9.1.custom.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="../resources/eligibilitynm/css/datepicker.css" />

<style type="text/css">
.row-fluid .span12 {
	width:99.9%;
}
.clear {
	clear:both;
	display:block;
}
.form-vertical hr {
	margin: 10px 0;
	clear: both;
	display: block;
}
h4 {
	font-size:18px;
	line-height:30px;
	padding:10px 0 0;
}
h5 {
	padding:10px 0 0;
}
input.no-margin {
	margin:0;
}
input.txt-center {
	text-align:center;

}
</style>
</head>

<body>

<div class="row-fluid" id="main">
    <div class="row-fluid" id="titlebar">
      <div class="gutter10 span3">
        <ul class="nav nav-pills">
          <li> <a href="e1-1-start-app">Back </a> </li>
        </ul>
      </div>
      <div class="span9 pull-left">
        <h3>Renew Eligibility Determination</h3>
      </div>
    </div>
    <!--titlebar-->
    <div class="row-fluid">
      <div class="span3 gutter10" id="sidebar">
        <ul class="nav nav-tabs nav-stacked gutter10"  data-spy="affix">
          <li> <a href="e1-1-start-app"> <i class="icon-ok"></i> Start Your Application</a></li>
          <li><a href="e3-build-your-household">  <i class="icon-ok"></i> Build Your Household</a></li>
          <li><a href="e4-household-income"> <i class="icon-ok"></i> Your Household Income </a></li>
          <li><a href="e5-additional-questions"> <i class="icon-ok"></i> Additional Questions </a></li>
          <li class="active"><a href="e6-review-declare-file"> <i class="icon-ok"></i> Review, Declare &amp; File </a></li>
        </ul>
      </div>
      <!--sidebar-->
      <div class="span9" id="rightpanel">
        <div class="gutter10">
                <p>Thank you for completing your renewal application.  Based on the information you submitted, which have been verified by external data sources, your household still qualifies for financial assistance.
        </p>

<p>To review your eligibility determination, including premium tax credit calculation, <a href="#">please click here.</a></p>

<div class="control-group">
              <h5>Would you like to:</h5>
              <div class="controls">
                <label class="checkbox">
                  <input type="checkbox" value="">
                  Remain enrolled in current health plan(s) </label>
                <label class="checkbox">
                  <input type="checkbox" value="" >
                  Shop for new plan(s) </label>
              </div>
            </div>
             <div class="well well-small">
    <a class="btn btn-primary offset10" href="#">Continue</a>
    </div>
    <!--gutter10-->
  </div>
  <!--rightpanel-->
</div>
<!--row-fluid-->
</div>
<!--main-->
</div>


<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr.custom.js"></script>
<script src="../resources/eligibilitynm/js/jquery-1.7.2.min.js" type="text/javascript"></script>
<script src="../resources/eligibilitynm/js/bootstrap.js" type="text/javascript"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/modernizr-2.0.6.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/waypoints.min.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/ghixcustom.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-datepicker.js"></script>
<script type="text/javascript" src="../resources/eligibilitynm/js/bootstrap-modal.js"></script>
<script src="../resources/eligibilitynm/js/jquery-ui-1.9.1.custom.js"></script>
<script>
$(document).ready(function(){
$('.qualify').hide();
$('#qualify-coverage').modal('hide');
$('#sidebar').affix();
$("input[name='immigration']").change(function(){
	$('.immigration').fadeToggle(this.value == "Yes");
});
$('.date').datepicker();
});
$( "#slider" ).slider({
						orientation: "vertical",
														range: "min",
														min: 0,
														max: 1500,
														value: 0,
														slide: function( event, ui ) {
															if(ui.value == 0){
																$( "#mtc" ).val(125);
																$( "#atc" ).val(0);
															}else if(ui.value == 1500){
																$( "#mtc" ).val(0);
																$( "#atc" ).val(1500);
															}
															else {
																$( "#mtc" ).val(125 - Math.round(ui.value/12));
																$( "#atc" ).val( ui.value );

															}

																													}
													});
													$( "#mtc" ).val( $( "#slider" ).slider( "value" ) );
													$( "#atc" ).val( $( "#slider" ).slider( "value" ) );

/*
$( "#slider" ).slider({
	orientation: "vertical",
            range: "min",
            min: 0,
            max: 1500,
            value: 0,
			rmtc:0,
            slide: function( event, ui ) {
               var atc = $( "#atc" ).val( ui.value );
			   var mtc = 125
				var mtc = (atc.val()/12);
				var rmtc = Math.round(mtc*100)/100;
				var rmtc = (rmtc*12)/12;
				console.log(rmtc);
				$("#mtc").val(rmtc);
            }
        });
        $( "#atc" ).val( $( "#slider" ).slider( "value" ) );
		     */

</script>
</body>
</html>
