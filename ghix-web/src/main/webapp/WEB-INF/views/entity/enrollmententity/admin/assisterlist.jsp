<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<%@ taglib uri="/WEB-INF/tld/datalisting.tld" prefix="dl"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.validate-1.9.0.min.js" />"></script>
				<div class="gutter10-lr">
					<div class="l-page-breadcrumb hide">
						<!--start page-breadcrumb -->
						<div class="row-fluid">
							<ul class="page-breadcrumb">
								<li><a href="#">&lt; <spring:message code="label.assister.back"/></a></li>
								<li><a href=""><spring:message code="label.assister.assisters"/></a></li>
								<li><spring:message code="label.assister.manage"/></li>
								
							</ul>
							<!--page-breadcrumb ends-->
						</div><!-- end of .row-fluid -->
					</div><!--l-page-breadcrumb ends-->
					<div class="row-fluid">
						<h1 class="pull-left">
							<spring:message code="label.assister.assisters"/> <small>${totalenrollmententitiesbysearch} <spring:message code="label.assister.matchingCounselors" /></small>
						</h1>
						<c:if test="${ID_STATE_CODE || NV_STATE_CODE}">
						<a class="btn btn-primary pull-right margin10-t" id="subSiteDone" onClick="window.location.href='<c:url value="/entity/assister/addnewassister" />'"><spring:message code="label.assister.addNewAssister"/></a>
					  	</c:if>
					</div>
					<div class="row-fluid">
						<div id="sidebar" class="span3">
							<form id="searchassister" id="searchassister"  action="<c:url value="/entity/entityadmin/searchofenrollmententityassisters"/>">
								<div class="header">
									<h4><spring:message code="label.assister.refineResultsBy"/> <a class="pull-right" href="#" onclick="resetAll()"><spring:message code="label.assister.resetAll"/></a></h4>
								</div>
								<div class="gutter10 graybg">
									<div class="control-group">
										<label class="control-label" for="firstName"><spring:message code="label.assister.assistersFirstName"/></label>
											<div class="controls">
												<input type="text" name="firstName" id="firstName" value="${assisterSearchParameters.firstName}" class="input-medium">
											</div> <!-- end of controls-->
									</div><!--control-group-->
									<div class="control-group">
										<label class="control-label" for="lastName"><spring:message code="label.assister.assistersLastName"/></label>
											<div class="controls">
												<input type="text" name="lastName" id="lastName" value="${assisterSearchParameters.lastName}" class="input-medium">
											</div> <!-- end of controls-->
									</div>
									<div class="control-group">
										<fieldset>
										<legend class="control-label"><spring:message code="label.assister.status"/></legend>
											<div class="controls">
												<label class="checkbox" for="statusActive">
													<input type="checkbox" name="statusActive" id="statusActive" value="Active" ${fn:contains(requestScope.statusActive,'Active') ? 'checked="checked"' : ''}><spring:message code="label.assister.active"/>
												</label>
												<label class="checkbox" for="statusInactive">
													<input type="checkbox" name="statusInactive" id="statusInactive" value="InActive"  ${fn:contains(requestScope.statusInactive,'InActive') ? 'checked="checked"' : ''}><spring:message code="label.assister.in-Active"/>
												</label>
											</div> <!-- end of controls-->
										</fieldset>
									</div><!--control-group-->
									<div class="control-group">
									<label class="control-label" for="certificationStatus"><spring:message code="label.assister.certificationStatus"/></label>
										<select  id="certificationStatus" name="certificationStatus" class="input-medium">
											<option value=""><spring:message code="label.assister.select"/></option>
											<c:forEach  items="${statusList}" var= "value">
											<option value="${value}" <c:if test="${assisterSearchParameters.certificationStatus == value}"> SELECTED </c:if>>${value}</option> 
											</c:forEach>
										</select>
									
									</div><!--control-group-->
									<div class="control-group">
										<fieldset>
											<legend class="control-label"><spring:message code="label.assister.CertificationRenewalDate"/></legend>
											<div class="controls">
												<div class="input-append date date-picker" data-date="">
													<label class="control-label"><spring:message code="label.assister.from"/></label>
													${requestScope.fromDate}
													<input type="text" id="fromDate" name="fromDate" class="span10" value="${requestScope.fromDate}" title="MM-dd-yyyy"><span class="add-on"><i class="icon-calendar"></i></span>
													<div id="fromDate_error"></div>
												</div>
												<div class="input-append date date-picker" data-date="">
													<label class="control-label"><spring:message code="label.assister.to"/></label>
													<input type="text" id="toDate" name="toDate" class="span10" value="${requestScope.toDate}" title="MM-dd-yyyy"><span class="add-on"><i class="icon-calendar"></i></span>
													<div id="toDate_error"></div>
												</div>
											</div> <!-- end of controls-->
										</fieldset>
									</div><!--control-group-->
									
 									<div class="txt-center">
 										<input type="submit" value="<spring:message code='label.assister.indGo'/>" class="btn">
 									</div>
									
								</div>
							</form>
					
						</div><!-- end of span3 -->
						<div class="span9" id="rightpanel">
							<form class="form-horizontal" id="frmviewCertification" name="frmviewCertification"   novalidate="novalidate"
							action="displayassisters" method="post">
								<df:csrfToken/>
								<c:choose>
									<c:when test="${fn:length(assisterList) > 0}">
										<table class="table"  id="assisterlist" name="assisterlist">
                                   				
			                                 	<thead>
													<tr class="header">					
														<spring:message code="label.assister.name1" var="nameVal"/>
														<spring:message code="label.assister.noOfClients" var="entityNameVal"/>
														<spring:message code="label.assister.certificationRen" var="reCertificationDateVal"/>
														<spring:message code="label.assister.STatus" var="statusVal"/>
														<spring:message code="label.assister.CertificationSTatus" var="certificationStatusVal"/>
														<c:set var="clintSort" value="${sortOrder}" />
														<c:set var="renewalDateSort" value="${sortOrder}" />
														<c:set var="statusSort" value="${sortOrder}" /> 
														<c:set var="certStatusSort" value="${sortOrder}" /> 
														<th class="sortable" scope="col" style="width: 250px;"><dl:sort title="${nameVal}" sortBy="FIRST_NAME" sortOrder="${sortOrder}"></dl:sort></th>
													    <th class="sortable" scope="col" style="width: 70px;"><dl:sort title="${entityNameVal}" sortBy="CLIENT_COUNT" sortOrder="${clintSort}"></dl:sort></th>
													    <th class="sortable" scope="col" style="width: 160px;"><dl:sort title="${reCertificationDateVal}" sortBy="RECERTIFICATION_DATE" sortOrder="${renewalDateSort}"></dl:sort></th>
													    <th class="sortable" scope="col"  style="width: 70px;"><dl:sort title="${statusVal}" sortBy="STATUS" sortOrder="${statusSort}"></dl:sort></th>
													    <th class="sortable" scope="col" style="width: 90px;"><dl:sort title="${certificationStatusVal}" sortBy="CERTIFICATION_STATUS" sortOrder="${certStatusSort}"></dl:sort></th>
													    <th class="sortable" scope="col" style="width:45px">&nbsp;</th>
													</tr>
												</thead>	
												<tbody>
													<c:set var="counter" value="0" />
								
														<c:forEach items="${assisterList}" var="assisterLocal">
											            	 <tr>											            	 	
											            	 	<!-- <td><label class="checkbox" for="${counter}"><span
																	class="hide">Checkbox</span><input id="${counter}"
																	class="indCheckboxActive" type="checkbox"
																	name="indCheckboxActive" value="908"></label>
																</td> -->
																<c:set var="assisterLocalId" ><encryptor:enc value="${assisterLocal.id}" isurl="true"/> </c:set>
											            	 	<td><a href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${assisterLocalId}"/>">${assisterLocal.firstName} ${assisterLocal.lastName}</a></td>
																<td><c:out value="${assisterLocal.clientCount}"></c:out></td> 
																<td><fmt:formatDate value="${assisterLocal.reCertificationDate}" pattern="MM/dd/yyyy" /></td>
																<td><c:out value="${assisterLocal.status}"></c:out></td>
												                <td><c:out value="${assisterLocal.certificationStatus}"></c:out></td> 					                 		 
											                 	<td>
													                 <div class="dropdown" style="width:50px">
														                <a class="dropdown-toggle" data-toggle="dropdown" href="#" ><i class="icon-cog"></i><i class="caret"></i></a>
																		<ul class="dropdown-menu"><li><a id ="editId" href="<c:url value="/entity/entityadmin/assisterinformation?assisterId=${assisterLocalId}"/>"class="offset1">
																		<i class="icon-pencil"></i><spring:message code='label.assister.edit'/></a></li></ul>										
																	</div>									
																</td>																		
												              </tr>	
												              <c:set var="counter" value="${counter+1}" />			                  
									                 	</c:forEach>
									                 
												</tbody>
												
											</table>
											
											<div class="pagination center">
												<dl:paginate resultSize="${totalenrollmententitiesbysearch + 0}"
													pageSize="${pageSize + 0}" hideTotal="true" />
											</div>
										</c:when>
										<c:otherwise>
													<b><spring:message code="label.assister.noMatchRecFound"/></b>
										</c:otherwise>
									</c:choose>
								</form>
							</div>
					</div>
				</div>

<script type="text/javascript">
	$(function(){$("#getAssistance").click(function(e){e.preventDefault();var href=$(this).attr('href');if(href.indexOf('#')!=0){$('<div id="searchBox" class="modal bigModal" data-backdrop="static"><div class="searchModal-header gutter10-lr"><button type="button" onclick="window.location.reload()" class="close" data-dismiss="modal" aria-hidden="true">�<\/button><\/div><div class=""><iframe id="search" src="'+href+'" class="searchModal-body"><\/iframe><\/div><\/div>').modal();}});});
	function closeSearchLightBox(){$("#searchBox").remove();window.location.reload();}
	function closeSearchLightBoxOnCancel(){$("#searchBox").remove();}
	if(!NREUMQ.f){NREUMQ.f=function(){NREUMQ.push(["load",new Date().getTime()]);var e=document.createElement("script");e.type="text/javascript";e.src=(("http:"===document.location.protocol)?"http:":"https:")+"//"+"d1ros97qkrwjf5.cloudfront.net/42/eum/rum.js";document.body.appendChild(e);if(NREUMQ.a)NREUMQ.a();};NREUMQ.a=window.onload;window.onload=NREUMQ.f;};NREUMQ.push(["nrfj","beacon-3.newrelic.com","18650c3d6e","1192004","ZVVQN0FQWRBZABBfDFwfeDBjHmAmek4teCUdRlsGREIYD1kaC0MXQR9BC1ZdW01SEBQ=",0,194,new Date().getTime(),"","","","",""]);
</script>
		
<script type="text/javascript">

$('.date-picker').datepicker({
    autoclose: true,
    format: 'mm-dd-yyyy'
});

var validator = $("#searchassister").validate({ 
	onkeyup: function(element) {$(element).valid()},
	onclick: true,
	onblur: true,
	 rules : {
		 fromDate : {chkDateFormat : true},
		 toDate :{chckDate : true, chkDateFormat : true},
	 },
	 messages : {
		fromDate : { chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"},
		toDate : { chckDate : "<span><em class='excl'>!</em><spring:message  code='label.validateFromAndToDate' javaScriptEscape='true'/></span>",
			chkDateFormat : "<span> <em class='excl'>!</em><spring:message code='err.validateDateFormat' javaScriptEscape='true'/></span>"}
	 },
	 errorClass: "error",
	 errorPlacement: function(error, element) {
	 var elementId = element.attr('id');
	 error.appendTo( $("#" + elementId + "_error"));
	 $("#" + elementId + "_error").attr('class','error help-inline');
	}	 
});	

jQuery.validator.addMethod("chkDateFormat", function(value, element, param) {
	// if license renewal date is not added do not validate anything
	if(value == '') {
		return true; 
	} 
	if(value != '') {
		return /^(0[1-9]|1[012])[-](0[1-9]|[12][0-9]|3[01])[-](19|20)\d\d$/.test(value);
	}	
});

jQuery.validator.addMethod("chckDate", function(value, element, param) {
	 var fromDate=document.getElementById('fromDate').value;
	 var toDate=document.getElementById('toDate').value;
	 
	 fromDate = fromDate.replace(/-/g, '/');
	 toDate = toDate.replace(/-/g, '/');
	 
	 if((fromDate != null) &&(toDate !=null)){
		if(new Date(fromDate).getTime()  > new Date(toDate).getTime() ){
			return false;
		}  
	 }
	return true;
});

function resetAll() {
	 var  contextPath =  "<%=request.getContextPath()%>";
	    var documentUrl = contextPath + "/entity/entityadmin/resetallassisters";
	    window.open(documentUrl,"_self","directories=no, status=no, menubar=no, scrollbars=yes, resizable=no,width=600, height=280,top=200,left=200");
}

</script>
