<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page import="com.getinsured.hix.platform.util.PhoneUtil"%>
<%@ page isELIgnored="false"%>
<%@ taglib uri="/WEB-INF/tld/csrf.tld" prefix="df"%>
<div class="gutter10">
	<div class="row-fluid">
		<ul class="page-breadcrumb">
            <li><a href="#">&lt; <spring:message  code="label.back"/></a></li>
            <li><a href="#"><spring:message  code="label.issuerTitle"/></a></li>
            <li><a href="<c:url value="/admin/addnewissuer"/>"><spring:message  code='label.addNewIssuer'/></a></li>
            <li><spring:message  code='label.reviewandsubmit'/></li>
        </ul><!--page-breadcrumb ends-->
        <h1><a name="skip"></a><spring:message  code='label.addNewIssuer'/></h1>
	</div><!--end of row-fluid-->
    
    <div class="row-fluid">
    	<div class="span3" id="sidebar">
        	<div class="header">
            	<h4><spring:message  code='label.steps'/></h4>
            </div>
            <!--  beginning of side bar -->
            <ul class="nav nav-list">
            	<li> <a href="<c:url value="/admin/addnewissuer"> <c:param name="isBack" value="true"/>  </c:url>" ><spring:message  code="label.issuerDetails"/></a></li>
               <%--  <li><a href="<c:url value="/admin/addnewissuer"/>"><spring:message  code='label.issuerDetails'/></a></li> --%>
               <li> <a href="<c:url value="/admin/issuer/primaryrepinfo"> <c:param name="isBack" value="true"/>  </c:url>" ><spring:message  code="label.primarycontactdetails"/></a></li>
               <%--  <li><a href="<c:url value="/admin/issuer/primaryrepinfo"/>"><spring:message  code='label.primarycontactdetails'/></a></li> --%>
                <li class="active"><a href="<c:url value="/admin/issuer/reviewissuer"/>"><spring:message  code='label.reviewandsubmit'/></a></li>
            </ul><!-- end of side bar -->
		</div><!-- end of span3-->
        <div class="span9" id="rightpanel">
        	<div class="header">
                <h4 class="span9"><spring:message  code='label.reviewandsubmit'/></h4>
            </div>
            <div class="gutter10">
            	<form class="form-horizontal" id="frmReviewandSubmit" name="frmReviewandSubmit" action="<c:url value='/admin/issuer/reviewissuer'/>" method="POST">
	            	<df:csrfToken/>
	            	<c:if test="${issuerDetailsNotSubmitted == 'true'}">
		               <div class="error">
		                  <label class="error"><span><spring:message  code="err.enterIssuerDetails"/></span></label>
		               </div> 
		            </c:if>
		            <c:if test="${repDetailsNotSubmitted == 'true'}">
		               <div class="error">
		                  <label class="error"><span><spring:message  code="err.enterIssuerRepDetails"/></span></label>
		               </div> 
		            </c:if>
		            
		            <!-- If both issuer and representative details not submitted then don't show the details -->
		            <c:if test="${issuerDetailsNotSubmitted != 'true' && repDetailsNotSubmitted != 'true'}">
	                	<input type="hidden" name="reviewNSubmit" id="reviewNSubmit" value="createIssuerRep">
	                	<div class="profile">
	                    	<table class="table table-border-none">
								<tbody>
									<tr>
										<td class="txt-right span4"><spring:message  code="label.name"/></td>
										<td><strong>${issuerDetailsSubmitted.name}</strong></td>
									</tr>
									<tr>
										<td class="txt-right span4"><spring:message  code="label.companyLegalName"/></td>
										<td><strong>${issuerDetailsSubmitted.companyLegalName}</strong></td>
									</tr>
									<tr>
										<td class="txt-right span4"><spring:message  code="label.shortName"/></td>
										<td><strong>${issuerDetailsSubmitted.shortName}</strong></td>
									</tr>								
	                            	<%--
	                            	This code is commented as per JIRA Number HIX-6752
	                            	<tr>
										<td class="txt-right span4"><spring:message  code="label.licenseNumber"/></td>
										<td><strong>${issuerDetailsSubmitted.licenseNumber}</strong></td>
									</tr>
	                                <tr>
										<td class="txt-right span4"><spring:message  code="label.licenseStatus"/></td>
										<td><strong>${issuerDetailsSubmitted.licenseStatus}</strong></td>
									</tr> 
									--%>
	                                <tr>
										<td class="txt-right span4"><spring:message  code="label.NAICCompanyCode"/></td>
										<td><strong>${issuerDetailsSubmitted.naicCompanyCode}</strong></td>
									</tr>
	                                <tr>
										<td class="txt-right span4"><spring:message  code="label.NAICGroupCode"/></td>
										<td><strong>${issuerDetailsSubmitted.naicGroupCode}</strong></td>
									</tr>
	                                <tr>
										<td class="txt-right span4"><spring:message code="label.FederalEmployerId"/></td>
										<td><strong>${issuerDetailsSubmitted.federalEin}</strong></td>
									</tr>
	                                <tr>
										<td class="txt-right span4"><spring:message  code="label.HIOSIssuerId"/></td>
										<td><strong>${issuerDetailsSubmitted.hiosIssuerId}</strong></td>
										<c:if test="${duplicateHios!=null}">
		               					<tr><td></td><td>
		                  					<label class="error"><span><spring:message  code="err.duplicateHiosBack"/></span></label>
		               					<td></tr> 
		            					</c:if>
									</tr>
									<%-- <tr>
										<td class="txt-right span4"><spring:message  code="label.txn834Version"/></td>
										<td><strong>${issuerDetailsSubmitted.txn834Version}</strong></td>
									</tr>
									<tr>
										<td class="txt-right span4"><spring:message  code="label.txn820Version"/></td>
										<td><strong>${issuerDetailsSubmitted.txn820Version}</strong></td>
									</tr> --%>
	                                <tr>
										<td class="txt-right span4 valignTop"><spring:message  code="label.issuerAddress"/></td>
										<td><strong>${issuerDetailsSubmitted.addressLine1}
										<br> ${issuerDetailsSubmitted.addressLine2}
										<br>${issuerDetailsSubmitted.city}, ${issuerDetailsSubmitted.state} ${issuerDetailsSubmitted.zip}
										</strong></td>
									</tr>
	                            </tbody>
	                        </table><br />
                            <div class="graydrkaction margin0">
								<h4><spring:message  code="label.primarycontactdetails"/></h4>
                            </div>
							
	                        <table class="table table-border-none">
	                        	<tr>
	                                <td class="txt-right span4"><spring:message  code="label.firstName"/></td>
	                                <td><strong>${primaryRepDetailsSubmitted.firstName}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.lastName"/></td>
	                                <td><strong>${primaryRepDetailsSubmitted.lastName}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.title"/></td>
	                                <td><strong>${primaryRepDetailsSubmitted.title}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.phnNumber"/></td>
	                                <c:set var="phnNumber" value="${primaryRepDetailsSubmitted.phone}"/>
	                                <td><strong><%=PhoneUtil.formatPhoneNumber((String)pageContext.getAttribute("phnNumber"))%></strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.EmailAddress"/></td>
	                                <td><strong>${primaryRepDetailsSubmitted.email}</strong></td>
	                            </tr>
	                            <%-- Newly added column as per jira HIX-7757 --%>
	                             <c:if test="${STATE_CODE != null && STATE_CODE == 'CA'}">
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.addressLine1"/></td>
	                                <td><strong>${primaryAdditionalDetailsSubmitted.address1}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.addressLine2"/></td>
	                                <td><strong>${primaryAdditionalDetailsSubmitted.address2}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.city"/></td>
	                                <td><strong>${primaryAdditionalDetailsSubmitted.city}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.state"/></td>
	                                <td><strong>${primaryAdditionalDetailsSubmitted.state}</strong></td>
	                            </tr>
	                            <tr>
	                                <td class="txt-right span4"><spring:message  code="label.zipcode"/></td>
	                                <td><strong>${primaryAdditionalDetailsSubmitted.zip}</strong></td>
	                            </tr>
	                            </c:if>
	                            <%--HIX-7757 ends here--%>
	                            
	                        </table>
	                    </div>
	                    <div class="form-actions">
	                    	<a href="<c:url value="/admin/issuer/primaryrepinfo"> <c:param name="isBack" value="true"/>  </c:url>" class="btn back-btn"><spring:message  code="label.back"/></a>
	                        <input type="submit" name="submit1" id="submit1" value="<spring:message  code="label.btnSubmit"/>" class="btn btn-primary" title="<spring:message  code="label.btnSubmit"/>"/>
	                    </div><!-- end of form-actions -->
	            	</c:if>
	            </form>
            </div>
        </div><!--end of span9-->
	</div><!--end of row-fluid-->
</div>
