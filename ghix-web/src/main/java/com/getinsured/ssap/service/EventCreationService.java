package com.getinsured.ssap.service;

/**
 * App event creation service definition.
 *
 * @author Yevgen Golubenko
 * @since 2019-06-04
 */
public interface EventCreationService {
  // TODO: Refactor this copy/paste junk
  void startEventCreation(long ssapApplicationId, String qepEvent, String qepEventDate, String caseNumber, String applicationType);

  // TODO: Refactor this copy/paste junk
  void logIndividualAppEvent(String eventType, long applicationId, boolean financial);
}
