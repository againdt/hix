//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2013.06.05 at 10:47:57 AM IST 
//


package com.getinsured.iex.org.nmhix.ssa.person;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CSRLevel-Enum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CSRLevel-Enum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="NON_AIAN_LEVEL_1"/>
 *     &lt;enumeration value="NON_AIAN_LEVEL_2"/>
 *     &lt;enumeration value="NON_AIAN_LEVEL_3"/>
 *     &lt;enumeration value="AIAN_LEVEL_1"/>
 *     &lt;enumeration value="AIAN_LEVEL_2"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "CSRLevel-Enum")
@XmlEnum
public enum CSRLevelEnum {

    NON_AIAN_LEVEL_1,
    NON_AIAN_LEVEL_2,
    NON_AIAN_LEVEL_3,
    AIAN_LEVEL_1,
    AIAN_LEVEL_2;

    public String value() {
        return name();
    }

    public static CSRLevelEnum fromValue(String v) {
        return valueOf(v);
    }

}
