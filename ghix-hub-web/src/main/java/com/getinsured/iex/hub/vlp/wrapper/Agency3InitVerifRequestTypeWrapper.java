package com.getinsured.iex.hub.vlp.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.Agency3InitVerifRequestSetType;
import com.getinsured.iex.hub.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;
import com.getinsured.iex.hub.vlp.wrapper.util.Agency3InitVerifRequestTypeWrapperUtil;

/**
 * This is the wrapper class for Agency3InitVerifRequestSetType
 * 
 * @author Nikhil Talreja
 * @since 14-Feb-2014
 *
 */
public class Agency3InitVerifRequestTypeWrapper implements JSONAware{
	
	private static ObjectFactory factory = null;
	
	private Agency3InitVerifRequestSetType request = null;
	
	private DHSIDTypeWrapper dhsid;
	private String firstName;
	private String middleName;
	private String lastName;
	private String dateOfBirth;
	private String aka;
	private String i94Number;
	private String comments;
	private String requestedCoverageStartDate;
	private boolean fiveYearBarApplicabilityIndicator;
	private boolean requestSponsorDataIndicator;
	private boolean requestGrantDateIndicator;
	private String requesterCommentsForHub;
	private String categoryCode;
	
	private static final String DHSID_KEY = "DHSID";
	private static final String FIRSTNAME_KEY = "FirstName";
	private static final String MIDDLENAME_KEY = "MiddleName";
	private static final String LASTNAME_KEY = "LastName";
	private static final String DATEOFBIRTH_KEY = "DateOfBirth";
	private static final String AKA_KEY = "AKA";
	private static final String I94NUMBER_KEY = "I94Number";
	private static final String COMMENTS_KEY = "Comments";
	private static final String REQUESTEDCOVERAGESTARTDATE_KEY = "RequestedCoverageStartDate";
	private static final String FIVEYEARBARAPPLICABILITYINDICATOR_KEY = "FiveYearBarApplicabilityIndicator";
	private static final String REQUESTSPONSORDATAINDICATOR_KEY = "RequestSponsorDataIndicator";
	private static final String REQUESTGRANTDATEINDICATOR_KEY = "RequestGrantDateIndicator";
	private static final String REQUESTERCOMMENTSFORHUB_KEY = "RequesterCommentsForHub";
	private static final String CATEGORYCODE_KEY = "CategoryCode";

	
	
	public Agency3InitVerifRequestTypeWrapper(){
		factory = new ObjectFactory();
		request = factory.createAgency3InitVerifRequestSetType();
	}
	
	public void setRequest(Agency3InitVerifRequestSetType request) {
		this.request = request;
	}
	
	/**
	 * This method will also do the request specific validations
	 */
	public Agency3InitVerifRequestSetType getRequest() throws HubServiceException {
		
		if(this.request.getFirstName() == null){
			throw new HubServiceException("First Name is required for Agency3InitVerifRequest");
		}
		
		if(this.request.getLastName() == null){
			throw new HubServiceException("Last Name is required for Agency3InitVerifRequest");
		}
		
		if(this.request.getDateOfBirth() == null){
			throw new HubServiceException("Date of Birth is required for Agency3InitVerifRequest");
		}
		
		if(this.request.getRequestedCoverageStartDate() == null){
			throw new HubServiceException("Requested Coverage Start Date is required for Agency3InitVerifRequest");
		}
	
		return this.request;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.request.setFirstName(firstName);
		this.firstName = firstName;
	}

	public String getMiddleName() {
		return middleName;
	}

	public void setMiddleName(String middleName) {
		this.request.setMiddleName(middleName);
		this.middleName = middleName;
	}
	
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.request.setLastName(lastName);
		this.lastName = lastName;
	}

	public String getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(String dateOfBirth) throws HubServiceException {
		
		try{
			this.request.setDateOfBirth(VLPServiceUtil.stringToXMLDate(dateOfBirth, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		this.dateOfBirth = dateOfBirth;
	}

	public String getAKA() {
		return aka;
	}

	public void setAKA(String aka) {
		this.request.setAKA(aka);
		this.aka = aka;
	}

	public String getI94Number() {
		return i94Number;
	}

	public void setI94Number(String i94Number) {
		this.request.setI94Number(i94Number);
		this.i94Number = i94Number;
	}
	
	public String getRequestedCoverageStartDate() {
		return requestedCoverageStartDate;
	}

	public void setRequestedCoverageStartDate(String requestedCoverageStartDate) throws HubServiceException {
		
		try{
			this.request.setRequestedCoverageStartDate(VLPServiceUtil.stringToXMLDate(requestedCoverageStartDate, VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
		}
		catch (Exception e){
			throw new HubServiceException(e);
		}
		
		this.requestedCoverageStartDate = requestedCoverageStartDate;
	}

	public boolean isFiveYearBarApplicabilityIndicator() {
		return fiveYearBarApplicabilityIndicator;
	}

	public void setFiveYearBarApplicabilityIndicator(
			boolean fiveYearBarApplicabilityIndicator) {
		this.fiveYearBarApplicabilityIndicator = fiveYearBarApplicabilityIndicator;
		this.request.setFiveYearBarApplicabilityIndicator(fiveYearBarApplicabilityIndicator);
	}

	public DHSIDTypeWrapper getDhsid() {
		return dhsid;
	}

	public void setDhsid(DHSIDTypeWrapper dhsid) {
		this.dhsid = dhsid;
		this.request.setDHSID(dhsid.getDhsId());
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
		this.request.setComments(comments);
	}

	public boolean isRequestSponsorDataIndicator() {
		return requestSponsorDataIndicator;
	}

	public void setRequestSponsorDataIndicator(boolean requestSponsorDataIndicator) {
		this.requestSponsorDataIndicator = requestSponsorDataIndicator;
		this.request.setRequestSponsorDataIndicator(requestSponsorDataIndicator);
	}

	public boolean isRequestGrantDateIndicator() {
		return requestGrantDateIndicator;
	}

	public void setRequestGrantDateIndicator(boolean requestGrantDateIndicator) {
		this.requestGrantDateIndicator = requestGrantDateIndicator;
		this.request.setRequestGrantDateIndicator(requestGrantDateIndicator);
	}

	public String getRequesterCommentsForHub() {
		return requesterCommentsForHub;
	}

	public void setRequesterCommentsForHub(String requesterCommentsForHub) {
		this.requesterCommentsForHub = requesterCommentsForHub;
		this.request.setRequesterCommentsForHub(requesterCommentsForHub);
	}

	public String getCategoryCode() {
		return categoryCode;
	}

	public void setCategoryCode(String categoryCode) {
		this.categoryCode = categoryCode;
		this.request.setCategoryCode(categoryCode);
	}
	
	/**
	 * Converts the JSON String to Agency3InitVerifRequestTypeWrapper object
	 * 
	 * @param jsonString - String representation for Agency3InitVerifRequestTypeWrapper
	 * @return Agency3InitVerifRequestTypeWrapper Object 
	 */
	public static Agency3InitVerifRequestTypeWrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}
		
		Agency3InitVerifRequestTypeWrapper wrapper = new Agency3InitVerifRequestTypeWrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		Object param = null;
		
		param = obj.get(DHSID_KEY);
		if(param != null){
			JSONObject dhsId = (JSONObject)param;
			wrapper.setDhsid(DHSIDTypeWrapper.fromJsonObject(dhsId));
		}
		
		Agency3InitVerifRequestTypeWrapperUtil.addNameToWrapper(obj, wrapper);
		
		param = obj.get(AKA_KEY);
		if(param != null){
			wrapper.setAKA((String)param);
		}
		
		param = obj.get(I94NUMBER_KEY);
		if(param != null){
			wrapper.setI94Number((String)param);
		}
		
		param = obj.get(COMMENTS_KEY);
		if(param != null){
			wrapper.setComments((String)param);
		}
		
		param = obj.get(REQUESTEDCOVERAGESTARTDATE_KEY);
		if(param != null){
			try {
				wrapper.setRequestedCoverageStartDate((String)param);
			} catch (Exception e) {
				throw new HubServiceException(e);
			}
		}
		
		Agency3InitVerifRequestTypeWrapperUtil.addIndicatorsToWrapper(obj, wrapper);
		
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		if(this.dhsid != null){
			obj.put(DHSID_KEY, this.dhsid.toJSONObject());
		}
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(AKA_KEY, this.aka);
		obj.put(I94NUMBER_KEY, this.i94Number);
		obj.put(COMMENTS_KEY, this.comments);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);
		return obj.toJSONString();

	}
	
	public Agency3InitVerifRequestTypeWrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		
		Agency3InitVerifRequestTypeWrapper wrapper = new Agency3InitVerifRequestTypeWrapper();
		if(jsonObj.get(DHSID_KEY) != null){
			wrapper.setDhsid(DHSIDTypeWrapper.fromJsonObject((JSONObject) jsonObj.get(DHSID_KEY)));
		}
		wrapper.setFirstName((String)jsonObj.get(FIRSTNAME_KEY));
		wrapper.setMiddleName((String)jsonObj.get(MIDDLENAME_KEY));
		wrapper.setLastName((String)jsonObj.get(LASTNAME_KEY));
		wrapper.setDateOfBirth((String)jsonObj.get(DATEOFBIRTH_KEY));
		wrapper.setAKA((String)jsonObj.get(AKA_KEY));
		wrapper.setI94Number((String)jsonObj.get(I94NUMBER_KEY));
		wrapper.setComments((String)jsonObj.get(COMMENTS_KEY));
		wrapper.setRequestedCoverageStartDate((String)jsonObj.get(REQUESTEDCOVERAGESTARTDATE_KEY));
		wrapper.setFiveYearBarApplicabilityIndicator(Boolean.parseBoolean((String)jsonObj.get(FIVEYEARBARAPPLICABILITYINDICATOR_KEY)));
		wrapper.setRequestSponsorDataIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTSPONSORDATAINDICATOR_KEY)));
		wrapper.setRequestGrantDateIndicator(Boolean.parseBoolean((String)jsonObj.get(REQUESTGRANTDATEINDICATOR_KEY)));
		wrapper.setRequesterCommentsForHub((String)jsonObj.get(REQUESTERCOMMENTSFORHUB_KEY));
		wrapper.setCategoryCode((String)jsonObj.get(CATEGORYCODE_KEY));
		return wrapper;
		
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		if(this.dhsid != null){
			obj.put(DHSID_KEY, this.dhsid.toJSONObject());
		}
		obj.put(FIRSTNAME_KEY, this.firstName);
		obj.put(MIDDLENAME_KEY, this.middleName);
		obj.put(LASTNAME_KEY, this.lastName);
		obj.put(DATEOFBIRTH_KEY, this.dateOfBirth);
		obj.put(AKA_KEY, this.aka);
		obj.put(I94NUMBER_KEY, this.i94Number);
		obj.put(COMMENTS_KEY, this.comments);
		obj.put(REQUESTEDCOVERAGESTARTDATE_KEY, this.requestedCoverageStartDate);
		obj.put(FIVEYEARBARAPPLICABILITYINDICATOR_KEY, this.fiveYearBarApplicabilityIndicator);
		obj.put(REQUESTSPONSORDATAINDICATOR_KEY, this.requestSponsorDataIndicator);
		obj.put(REQUESTGRANTDATEINDICATOR_KEY, this.requestGrantDateIndicator);
		obj.put(REQUESTERCOMMENTSFORHUB_KEY, this.requesterCommentsForHub);
		obj.put(CATEGORYCODE_KEY, this.categoryCode);
		return obj;
	}
}
