package com.getinsured.hix.finance.controller;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.getinsured.hix.dto.enrollment.EnrollmentPaymentDTO;
import com.getinsured.hix.dto.planmgmt.IssuerPaymentInfoResponse;
import com.getinsured.hix.dto.planmgmt.IssuerRequest;
import com.getinsured.hix.platform.config.DynamicPropertiesUtil;
import com.getinsured.hix.platform.config.FinanceConfiguration;
import com.getinsured.hix.platform.security.GhixRestTemplate;
import com.getinsured.hix.platform.util.GhixEndPoints;
import com.getinsured.saml.payment.IssuerConfig;
import com.getinsured.saml.payment.IssuerPaymentConfig;
import com.google.gson.Gson;

@Service
public class IssuerPaymentConfigurationService {
	private HashMap<String, IssuerPaymentConfiguration> issuerCofigRepo = new HashMap<>();
	@Autowired
	private GhixRestTemplate ghixRestTemplate;
	@Autowired
	private Gson platformGson;
	public static final String PAYMENT_CONFIG_HOME;
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	static {
		PAYMENT_CONFIG_HOME = System.getProperty("GHIX_HOME")+File.separatorChar+
				"ghix-setup"+File.separatorChar+
				"conf"+File.separatorChar+
				"saml"+File.separatorChar+
				"payment";
		
	}
	
	private IssuerPaymentConfig samlConfigRepo = IssuerPaymentConfig.getIssuerConfigs(PAYMENT_CONFIG_HOME);
	
	public IssuerPaymentConfiguration getIssuerPaymentInfo(String hiosId) {
		this.logger.info("Retrieving the Issuer information for HIOS ID:{}",hiosId);
		IssuerPaymentConfiguration config = this.issuerCofigRepo.get(hiosId);
		if(config == null || config.isStale()) {
			this.logger.info("Issuer Information not found in cache or stale for HIOS ID:{} retrieving...",hiosId);
			IssuerRequest issuerRequest = new IssuerRequest();
			issuerRequest.setId(hiosId);
			ResponseEntity<String> response = ghixRestTemplate.exchange(
					GhixEndPoints.PlanMgmtEndPoints.GET_ISSUER_PAYMENT_INFO_BY_HIOSID, 
					DynamicPropertiesUtil.getPropertyValue(FinanceConfiguration.FinanceConfigurationEnum.FINANCE_SYSTEM_USER), 
					HttpMethod.POST, 
					MediaType.APPLICATION_JSON, 
					String.class, 
					issuerRequest);
			if(response != null){
				logger.info("Retrieved response for issuer payment information from finance sub system, checking validity");
				IssuerPaymentInfoResponse issuerPaymentInfoResponse = platformGson.fromJson(response.getBody(), IssuerPaymentInfoResponse.class);
				logger.info("Valid response for issuer payment information from finance sub system");
				IssuerConfig iConfig = this.samlConfigRepo.getIssuerConfig(hiosId);
				config = new IssuerPaymentConfiguration(hiosId, issuerPaymentInfoResponse, iConfig);
				this.issuerCofigRepo.put(hiosId, config);
			}
		}
		return config;
	}
	/**
	 * @param paymentDTO
	 * @return
	 */
	
	public HashMap<String, String> getPaymentData(EnrollmentPaymentDTO paymentDTO){
		SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		HashMap<String, String> paymentData = new HashMap<>();
		paymentData.put(CMSConstants.FIRST_NAME, paymentDTO.getFirstName());
		paymentData.put(CMSConstants.LAST_NAME, paymentDTO.getLastName());
		paymentData.put(CMSConstants.MIDDLE_NAME, paymentDTO.getMiddleName());
		paymentData.put(CMSConstants.APTC_AMOUNT, getRoundedOffValue(BigDecimal.valueOf(paymentDTO.getAptcAmt()),2));
		paymentData.put(CMSConstants.PREMIUM_AMOUNT_TOTAL, getRoundedOffValue(BigDecimal.valueOf(paymentDTO.getGrossPremiumAmt()),2));
		paymentData.put(CMSConstants.TOTAL_AMOUNT_OWNED, getRoundedOffValue(BigDecimal.valueOf(paymentDTO.getNetPremiumAmt()),2));
		paymentData.put(CMSConstants.STREET_NAME_1, paymentDTO.getAddressLine1());
		paymentData.put(CMSConstants.STREET_NAME_2, paymentDTO.getAddressLine2());
		paymentData.put(CMSConstants.ZIPCODE, paymentDTO.getZip());
		paymentData.put(CMSConstants.STATE, paymentDTO.getState());
		paymentData.put(CMSConstants.PREFERRED_LANGUAGE_CODE, paymentDTO.getPreferredLanguage());
		paymentData.put(CMSConstants.MARKET_INDICATOR, "individual");
		paymentData.put(CMSConstants.PAYMENT_TRANSACTION_ID, paymentDTO.getPaymentTxnId());
		paymentData.put(CMSConstants.ASSIGNED_QHP_IDENTIFIER,paymentDTO.getCmsPlanId());
		paymentData.put(CMSConstants.PROPOSED_COVERAGE_EFFECTIVE_DATE,dateFormat.format(paymentDTO.getBenefitEffectiveDate()));
		paymentData.put(CMSConstants.CONTACT_EMAIL_ADDRESS,paymentDTO.getContactEmail());
		paymentData.put(CMSConstants.ADDITIONAL_INFORMATION, paymentDTO.getEnrolleeNames());
		paymentData.put(CMSConstants.SUBSCRIBER_IDENTIFIER,paymentDTO.getExchgSubscriberIdentifier());
		paymentData.put(CMSConstants.PARTNER_ASSIGNED_CONSUMER_ID, Integer.toString(paymentDTO.getEnrollmentId())); 
		paymentData.put(CMSConstants.CITY_NAME, paymentDTO.getCity()); 
		return paymentData;
	}
	
	public String getRoundedOffValue(BigDecimal input, int decimalPlaces) {
		input = input.setScale(decimalPlaces, RoundingMode.HALF_UP);
		return input.toString();
	}
 	
	

}
