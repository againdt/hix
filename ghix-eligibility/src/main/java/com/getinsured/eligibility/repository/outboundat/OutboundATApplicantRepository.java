package com.getinsured.eligibility.repository.outboundat;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.getinsured.iex.ssap.model.OutboundATApplicant;

@Repository
public interface OutboundATApplicantRepository extends JpaRepository<OutboundATApplicant, Long> {

	List<OutboundATApplicant> findByApplicantGuid(@Param("applicantGuid") String applicantGuid);

	@Query("from OutboundATApplicant outboundATApplicant where applicantGuid in(:applicantGuids) and ssapApplicationId = :applicationId")
	List<OutboundATApplicant> findByApplicantGuidAndApplicationId(@Param("applicantGuids") List<String> applicantGuids, @Param("applicationId") Long applicationId);
}
