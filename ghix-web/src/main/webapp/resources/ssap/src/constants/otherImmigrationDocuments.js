export const OTHER_IMMIGRATION_DOCUMENTS = [
    {
        text: 'Certification From U.S. Department of Health and Human Services (HHS) Office of Refugee Resettlement (ORR)',
        name: 'orrcertificationIndicator'
    },
    {
        text: 'Office of Refugee Resettlement (ORR) Eligibility Letter (if Under 18)',
        name: 'orreligibilityLetterIndicator'
    },
    {
        text: 'Cuban/Haitian Entrant',
        name: 'cubanHaitianEntrantIndicator'
    },
    {
        text: 'Resident of American Samoa',
        name: 'americanSamoanIndicator'
    },
    {
        text: 'Battered spouse, child, or parent under Violence Against Women Act',
        name: 'domesticViolence'
    },
    {
        text: 'Document indicating member of federally-recognized Indian tribe or American Indian born in Canada',
        name: 'indianOfUsOrCanada'
    },
    {
        text: 'None of these',
        name: 'noneOfTheseOther'
    }
];