//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vhudson-jaxb-ri-2.1-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2019.01.08 at 03:30:17 PM IST 
//


package com.getinsured.iex.erp.gov.cms.hix._0_1.hix_core;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * A data type for an applied augmentation for type nc:PersonType.
 * 
 * <p>Java class for PersonType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://niem.gov/niem/niem-core/2.0}PersonType">
 *       &lt;sequence>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}TribalAugmentation" minOccurs="0"/>
 *         &lt;element ref="{http://hix.cms.gov/0.1/hix-core}PersonAugmentation" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonType", propOrder = {
    "tribalAugmentation",
    "personAugmentation"
})
public class PersonType
    extends com.getinsured.iex.erp.gov.niem.niem.niem_core._2.PersonType
{

    @XmlElement(name = "TribalAugmentation")
    protected TribalAugmentationType tribalAugmentation;
    @XmlElement(name = "PersonAugmentation")
    protected PersonAugmentationType personAugmentation;

    /**
     * Gets the value of the tribalAugmentation property.
     * 
     * @return
     *     possible object is
     *     {@link TribalAugmentationType }
     *     
     */
    public TribalAugmentationType getTribalAugmentation() {
        return tribalAugmentation;
    }

    /**
     * Sets the value of the tribalAugmentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link TribalAugmentationType }
     *     
     */
    public void setTribalAugmentation(TribalAugmentationType value) {
        this.tribalAugmentation = value;
    }

    /**
     * Gets the value of the personAugmentation property.
     * 
     * @return
     *     possible object is
     *     {@link PersonAugmentationType }
     *     
     */
    public PersonAugmentationType getPersonAugmentation() {
        return personAugmentation;
    }

    /**
     * Sets the value of the personAugmentation property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonAugmentationType }
     *     
     */
    public void setPersonAugmentation(PersonAugmentationType value) {
        this.personAugmentation = value;
    }

}
