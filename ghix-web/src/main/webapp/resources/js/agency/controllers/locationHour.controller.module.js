(function() {
'use strict';

angular.module('locationHourCtrlModule',[])
.controller('LocationHourCtrl',LocationHourCtrl);

LocationHourCtrl.$inject = ['$http', '$scope', '$stateParams','$location', 'LocationHoursService', '$timeout', 'dataStorageService'];
function LocationHourCtrl ($http, $scope, $stateParams, $location, LocationHoursService, $timeout, dataStorageService ) {
        var vm = this;

        vm.init = init;
        vm.addSubsite = addSubsite;
        vm.saveSite = saveSite;
        vm.applyToAll = applyToAll;
        vm.toggleIcon = toggleIcon;
        vm.setOperationHours = setOperationHours;
        vm.validateTime = validateTime;
        vm.nextPage = nextPage;
        vm.enableEditMode = enableEditMode;
        vm.certificationStatus = $('#agencyCertificationStatus').val();
        vm.applyToAllWeekdays = {};
        vm.applyWeekdays = {};
        // vm.applyToAllFlag = true;
        // vm.applyWeekdays = false;
        function init(){
        	vm.loader = true;

        	vm.disableEditContent = {};
            vm.disableViewContent = {};
            vm.flowFlag = {};

            updateFlowFlag();
            if(vm.flowFlag.adminFlow){
                vm.agencyId =  dataStorageService.getItem('encryptedAgencyId');
            } else {
                vm.agencyId =  $("#agencyId").val() || dataStorageService.getItem('agencyId');
            }
            if(!vm.agencyId && vm.flowFlag.adminFlow){
        		$state.go('manageAgencies');

        	}

        	if(!vm.flowFlag.adminFlow){
                $scope.$parent.vm.sideNavListOne[0].showOKicon = true;
                $scope.$parent.vm.sideNavListOne[2].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[3].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[4].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[5].showOKicon = false;
                $scope.$parent.vm.sideNavListOne[0].disableLink = false;
                $scope.$parent.vm.sideNavListOne[1].disableLink = false;
                $scope.$parent.vm.sideNavListOne[2].disableLink = true;
                $scope.$parent.vm.sideNavListOne[3].disableLink = true;
                $scope.$parent.vm.sideNavListOne[4].disableLink = true;
                $scope.$parent.vm.sideNavListOne[5].disableLink = true;
        	}

			var promise = $http.get('/hix/agency/site/list/' + vm.agencyId);
			promise.then(function(response){
				vm.loader = false;
				vm.agencyLocationHoursList = response.data;

				if(vm.agencyLocationHoursList.length === 1 && !vm.agencyLocationHoursList[0].siteLocationName){
					vm.hasValidSiteSaved = false;
					vm.agencyLocationHoursList[0].siteLocationName = vm.agencyLocationHoursList[0].location.city + ' Location'
				}else{
					vm.hasValidSiteSaved = true;
				}

				angular.forEach(vm.agencyLocationHoursList, function (value, key) {
					value.showHeader = vm.hasValidSiteSaved;
					value.iconMinus = !vm.hasValidSiteSaved;

					value.location.addressLine1 = value.location.address1;
					value.location.addressLine2 = value.location.address2;
					value.location.zipcode = value.location.zip;

					value.locationInHeader = angular.copy(value.location);
					value.locationInHeader.siteLocationName = angular.copy(value.siteLocationName);

					value.collapseIn = vm.hasValidSiteSaved? false: true;

					if(value.phoneNumber) {
						value.phone1 = value.phoneNumber.substring(0, 3);
						value.phone2 = value.phoneNumber.substring(3, 6);
						value.phone3 = value.phoneNumber.substring(6, 10);
					}

					if (value.siteLocationHours.length === 0) {
						value.siteLocationHours = LocationHoursService.populateDays().weekdays;
					}
					if(vm.flowFlag.agencyResgisterFlow){
						value.editMode = true;
					}else{
						value.editMode = false;
					}

				});

			}).catch(function(error){
				vm.loader = false;
				console.log (error);
			});
        }

        function addSubsite(){
            vm.agencyLocationHoursList = vm.agencyLocationHoursList || [];
        	vm.agencyLocationHoursList.push({
        		siteLocationHours : LocationHoursService.populateDays().weekdays,
        		siteType: 'SUBSITE',
        		agencyId: vm.agencyId,
        		showHeader: false,
        		iconMinus: true,
        		collapseIn: true,
        		editMode: true,
        		hideCancel: true,
                tabindex: 0
        	});

            if (vm.certificationStatus !== 'INCOMPLETE' || !vm.certificationStatus) {
               vm.disableViewContent[vm.agencyLocationHoursList.length-1] = true;
           }

        }

        function saveSite(site, index){
        	vm.loader = true;
        	var siteLocationHour = angular.copy(site.siteLocationHours);

        	var phoneNumber = '';
        	if(site.phone1 !== undefined && site.phone2 !== undefined && site.phone3 !== undefined){
        		phoneNumber = "" + site.phone1 + site.phone2 + site.phone3;
        	}

            var siteData = {
                id: site.id? site.id : null,
                agencyId: site.agencyId,
                siteLocationName: site.siteLocationName,
                emailAddress: site.emailAddress,
                phoneNumber : phoneNumber,
                siteType: site.siteType,
                location: {
                    address1: site.location.addressLine1,
                    address2: site.location.addressLine2,
                    city: site.location.city,
                    state: site.location.state,
                    zip: site.location.zipcode,
                    lat: site.location.lat,
                    lon: site.location.lon
                },
                siteLocationHours: siteLocationHour

            };

        	var promise = $http.post('/hix/agency/site', siteData);
        	promise.then(function(response){
        		vm.loader = false;

                if(response.status == '200'){
                	site.showHeader = true;
                    site.iconMinus = false;
                	$('#site-'+index).collapse('hide');
                	vm.hasValidSiteSaved = true;

                	if(!site.id){
                		site.id = angular.copy(response.data.id);
                    	site.siteLocationHours = angular.copy(response.data.siteLocationHours);
                	}

                	site.locationInHeader = angular.copy(site.location);
                    site.locationInHeader.siteLocationName = angular.copy(site.siteLocationName);
                }

            })
            .catch(function(error){
            	vm.loader = false;
                console.log (error);
            });

        }

      function applyToAll(item, index) {
          vm.applyWeekdays[index] = true;
	      if(vm.applyWeekdays[index]){
            vm.applyToAllWeekdays[index] = true;
            angular.forEach(item.siteLocationHours, function (value, key) {
              if (key < 5) {
                  value.fromTime = item.siteLocationHours[0].fromTime;
                  value.toTime = item.siteLocationHours[0].toTime;
              }
            });
	      }
	  };


	  function toggleIcon($event, item){
		  var element = $event.currentTarget;
          $event.preventDefault();
		  $timeout(function(){
  			if($(element).closest('.accordion-group').find('.in').length > 0){
  				item.iconMinus = true;
          	}else{
          		item.iconMinus = false;
          	}
  		}, 100)

	  }


	  function setOperationHours(timePickerForm, dayHour, openFlag, index){
        dayHour.fromTime = openFlag;
        dayHour.toTime = openFlag;
        vm.chkBoxChecked = false;
        vm.applyWeekdays[index] = false;
        timePickerForm.fromTime.$setValidity('startBeforeEnd', true);
        if(dayHour.day === "Monday" && dayHour.fromTime !== "select" && dayHour.toTime !== "select") {
            vm['applyToAllFlag' + index]= false;
        }
        if(dayHour.day === "Monday" && dayHour.fromTime === "select" && dayHour.toTime === "select") {
            vm['applyToAllFlag' + index] = true;
            vm.applyWeekdays[index] = false;
        }
	  }

    function validateTime (timePickerForm, list, index) {

	    var startTime = list.fromTime;
	    var endTime = list.toTime;
        var day = list.day;
	    var startBeforeEnd = true;
        vm.applyToAllWeekdays[index] = false;
        vm['applyToAllFlag' + index] = true;
        if (day === "Saturday" || day === "Sunday") {
            vm.applyWeekdays[index] = vm.applyWeekdays[index];
        } else {
            vm.applyWeekdays[index] = false;
        }

	    if(startTime !== "select" && startTime !== "closed" && endTime !== "select" && endTime !== "closed") {
	        var sHours = Number(startTime.match(/^(\d+)/)[1]);
	        var sMinutes = Number(startTime.match(/:(\d+)/)[1]);
	        var sAMPM = startTime.match(/\s(.*)$/)[1];
	        var eHours = Number(endTime.match(/^(\d+)/)[1]);
	        var eMinutes = Number(endTime.match(/:(\d+)/)[1]);
	        var eAMPM = endTime.match(/\s(.*)$/)[1];

	        if(sAMPM == "pm" && sHours<12){
	        	sHours = sHours+12;
	        }else if(sAMPM == "am" && sHours == 12){
	        	sHours = sHours-12;
	        }

	        if(eAMPM == "pm" && eHours<12){
	        	eHours = eHours+12;
	        }else if(eAMPM == "am" && eHours == 12){
	        	eHours = eHours-12;
	        }

	        if (sHours > eHours){
	        	startBeforeEnd = false;
                if(day === "Monday") {
                    vm['applyToAllFlag' + index] = true;
                }
	        }else if(sHours < eHours || sMinutes < eMinutes){
	        	startBeforeEnd = true;
                if(day === "Monday") {
                    vm['applyToAllFlag' + index] = false;
                }
	        }

            if(sAMPM == eAMPM && sHours >= eHours && sMinutes >= eMinutes) {
                startBeforeEnd = false;
                if(day === "Monday") {
                    vm['applyToAllFlag' + index] = true;
                }
            }
	    }else if((startTime === "select" && endTime === "select" ) || ( startTime === "closed" && endTime === "closed")){
	    	startBeforeEnd = true;
            if(day === "Monday" && startTime === "select" && endTime === "select" ) {
                vm['applyToAllFlag' + index] = true;
            }
	    }else{
	    	startBeforeEnd = false;
            if(day === "Monday" && startTime === "closed" && endTime === "closed") {
                vm['applyToAllFlag' + index] = true;
            }
	    }


	    timePickerForm.fromTime.$setValidity('startBeforeEnd', startBeforeEnd);

	}


    function nextPage() {
        $scope.$parent.vm.sideNavListOne[2].disableLink = false;
    	LocationHoursService.nextPage();
    }

    function updateFlowFlag(){

    	//for admin flow
    	var isAdminRole = $("#isAdminRole").val();

    	//for agency flow
    	var certificationStatus = $('#agencyCertificationStatus').val();


    	if(isAdminRole === 'true'){
    		vm.flowFlag.adminFlow = true;
    	}else{
    		if(certificationStatus === '' || certificationStatus === 'INCOMPLETE'){
    			vm.flowFlag.agencyResgisterFlow = true;
    		}else if(certificationStatus === 'CERTIFIED'){
    			vm.flowFlag.agencyViewEditFlow = true;
    		}else{
    			vm.flowFlag.agencyViewOnlyFlow = true;
    		}
    	}
    }

    function enableEditMode(site, index){
    	if(!site.editMode){
    		site.editMode = true;
    	}

    	var collapseElem = $("#site-" + index);
		if(!collapseElem.hasClass('in')){
			collapseElem.collapse('show');
		}
    }


    }

})();
