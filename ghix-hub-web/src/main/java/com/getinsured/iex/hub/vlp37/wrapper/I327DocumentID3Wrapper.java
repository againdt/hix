package com.getinsured.iex.hub.vlp37.wrapper;

import org.json.simple.JSONAware;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.I327DocumentID3Type;
import com.getinsured.iex.hub.vlp37.gov.hhs.cms.dsh.sim.ee.vlp.ObjectFactory;
import com.getinsured.iex.hub.platform.HubServiceException;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Wrapper Class for I327DocumentID3Wrapper
 * 
 * @author Abhai Chaudhary, Nikhil Talreja
 * @since  20-Jan-2014 
 * 
 */
public class I327DocumentID3Wrapper implements JSONAware{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(I327DocumentID3Wrapper.class);
	
	private ObjectFactory factory;
	private I327DocumentID3Type i327DocumentID3Type;
	
	private String alienNumber;
	private String expiryDate;

	private static final String ALIEN_NUMBER_KEY = "AlienNumber";
	private static final String EXPIRY_DATE_KEY = "DocExpirationDate";
	

	public I327DocumentID3Wrapper(){
		this.factory = new ObjectFactory();
		this.i327DocumentID3Type = factory.createI327DocumentID3Type();
	}
	
	public void setAlienNumber(String alienNumber) throws HubServiceException{
		
		if(alienNumber == null){
			throw new HubServiceException("No Alien number provided");
		}
		
		this.alienNumber = alienNumber;
		this.i327DocumentID3Type.setAlienNumber(this.alienNumber);
	}
	
	public String getAlienNumber() {
		return alienNumber;
	}
	
	public void setExpiryDate(String expiryDate) {
		
		try{
			this.i327DocumentID3Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			this.expiryDate = expiryDate;
		}catch(Exception e){
			LOGGER.error("Exception occured while setting I551 Document Expiration date",e);
		}
	}
	
	public String getExpiryDate() {
		return expiryDate;
	}

	public I327DocumentID3Type geti327DocumentID3Type(){
		return this.i327DocumentID3Type;
	}
	
	/**
	 * Converts the JSON String to I327DocumentID3Wrapper object
	 * 
	 * @param jsonString - String representation for I327DocumentID3Wrapper
	 * @return I327DocumentID3Wrapper Object
	 */
	public static I327DocumentID3Wrapper fromJsonString(String jsonString) throws HubServiceException{
		
		if(jsonString == null || jsonString.length() == 0){
			throw new HubServiceException("Can not create I327DocumentID3Wrapper from null or empty input");
		}

		Object tmpObj = null;
		
		I327DocumentID3Wrapper wrapper = new I327DocumentID3Wrapper();
		JSONObject obj  = (JSONObject) JSONValue.parse(jsonString);
		
		wrapper.factory = new ObjectFactory();
		
		tmpObj = obj.get(ALIEN_NUMBER_KEY);
		if(tmpObj != null){
			wrapper.alienNumber = (String)tmpObj;
			wrapper.i327DocumentID3Type.setAlienNumber(wrapper.alienNumber);
		}
		
		tmpObj = obj.get(EXPIRY_DATE_KEY);
		if(tmpObj != null){
			try {
				wrapper.expiryDate = (String)tmpObj;
				wrapper.i327DocumentID3Type.setDocExpirationDate(VLPServiceUtil.stringToXMLDate(wrapper.expiryDate,VLPServiceConstants.VLP_SERVICE_DATE_FORMAT));
			} catch (Exception e) {
				throw new HubServiceException("Failed creating I327DocumentID3Wrapper from JSON, Invalid expiration date",e);
			}
		}

		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public String toJSONString() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj.toJSONString();
	}
	
	public static I327DocumentID3Wrapper fromJsonObject(JSONObject jsonObj) throws HubServiceException{
		I327DocumentID3Wrapper wrapper = new I327DocumentID3Wrapper();
		wrapper.setAlienNumber((String) jsonObj.get(ALIEN_NUMBER_KEY));
		wrapper.setExpiryDate((String) jsonObj.get(EXPIRY_DATE_KEY));
		return wrapper;
	}
	
	@SuppressWarnings("unchecked")
	public JSONObject toJSONObject() {
		JSONObject obj = new JSONObject();
		obj.put(ALIEN_NUMBER_KEY, this.alienNumber);
		obj.put(EXPIRY_DATE_KEY, this.expiryDate);
		return obj;
	}
}
