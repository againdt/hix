package com.getinsured.eligibility.redetermination.service;

import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Interface is used to provide services for Clone.
 */
public interface ApplicationCloneService {

	String AUTO_ENROLL_JOB_NAME = "ssapRedeterminationCloneJob";

	enum APPLICATION_STATUS {
		EN, SG, UC;
	}

	Map<String, String> cloneSsap(SsapApplication ssapApplication, boolean createEventsFlag, AtomicBoolean cloneProgramEligibilities, boolean changeAppData);
}
