package com.getinsured.hix.batch.enrollment.service;

import java.util.List;

import com.getinsured.hix.model.batch.BatchJobExecution;
import com.getinsured.hix.platform.util.exception.GIException;


public interface EnrollmentMonthlyIRSBatchService {
	
	List<String> getUniqueHouseHolds(int month, int year) throws GIException;
	
	void processIRSHousehold(List<String> householdCaseIdList, int applicableMonth, int applicableYear, int partition) throws GIException;
	
	void generateFilePayloadBatchTransmissionXML(int month, int year) throws GIException;
	
	/**
	 * 
	 * @param jobName
	 * @return
	 */
	List<BatchJobExecution> getRunningBatchList(String jobName);

	/**
	 * Generate manifest file
	 * @param string
	 * @param irsMonthlyResponseWipPath
	 * @param originalBatchId
	 * @param batchId
	 * @param year
	 * @param month
	 * @throws GIException 
	 */
	void generateManifestXML(String string, String irsMonthlyResponseWipPath, String originalBatchId, String batchId,
			Integer year, Integer month) throws GIException;

	/**
	 * Get unique householdIds for regeneration from executions table
	 * @param regenBatchId
	 * @param giAction 
	 * @return
	 */
	List<String> getUniqueHouseholdsForRegen(String regenBatchId, String giAction);
	
	/**
	 * Validation Service
	 * @param jobId
	 * @return
	 */
	boolean generatedXMLsValid(Long jobId);
}
