package com.getinsured.hix.batch.ssap.renewal.task;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;

import com.getinsured.hix.batch.ssap.renewal.service.CloneService;
import com.getinsured.hix.batch.ssap.renewal.util.ClonePartitionerParams;
import com.getinsured.hix.batch.ssap.renewal.util.RenewalUtils;
import com.getinsured.iex.ssap.model.SsapApplication;

/**
 * Clone Reader class is used to get read SSAP Application from Partitioner.
 * 
 * @since July 30, 2019
 */
public class CloneReader implements ItemReader<SsapApplication> {

	private static final Logger LOGGER = LoggerFactory.getLogger(CloneReader.class);
	private List<SsapApplication> readSsapApplicationList;
	private int partition;
	private int loopCount;
	private int startIndex;
	private int endIndex;

	private CloneService cloneService;
	private ClonePartitionerParams clonePartitionerParams;

	@BeforeStep
	public void saveStepExecution(StepExecution stepExecution) {
		LOGGER.debug(Thread.currentThread().getName() + " : beforeStep execution for Reader ");

		ExecutionContext executionContext = stepExecution.getExecutionContext();

		if (executionContext != null) {
			partition = executionContext.getInt("partition");
			startIndex = executionContext.getInt("startIndex");
			endIndex = executionContext.getInt("endIndex");
			LOGGER.debug("partition: {}, startIndex: {}, endIndex: {}", partition, startIndex, endIndex);

			List<Long> readSsapApplicationIdList = null;

			if (null != clonePartitionerParams && CollectionUtils.isNotEmpty(clonePartitionerParams.getSsapApplicationIdList())) {

				if (startIndex < clonePartitionerParams.getSsapApplicationIdList().size()
						&& endIndex <= clonePartitionerParams.getSsapApplicationIdList().size()) {
					readSsapApplicationIdList = new CopyOnWriteArrayList<Long>(clonePartitionerParams.getSsapApplicationIdList().subList(startIndex, endIndex));
				}
			}

			if (CollectionUtils.isNotEmpty(readSsapApplicationIdList)) {
				List<List<Long>> chunkedListOfList = RenewalUtils.chunkList(readSsapApplicationIdList, RenewalUtils.INNER_QUERY_CHUNK_SIZE);
				readSsapApplicationList = new CopyOnWriteArrayList<SsapApplication>();

				for (List<Long> chunkedList : chunkedListOfList) {
					readSsapApplicationList.addAll(new CopyOnWriteArrayList<SsapApplication>(cloneService.getCloneSsapApplListByIds(chunkedList)));
				}
			}

			if (LOGGER.isDebugEnabled()) {
				LOGGER.debug("readSsapApplicationList is null: {}", CollectionUtils.isEmpty(readSsapApplicationList));
			}
		}
	}

	@Override
	public SsapApplication read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {

		boolean readerFlag = false;
		SsapApplication readerSsapApplication = null;

		if (CollectionUtils.isNotEmpty(readSsapApplicationList)) {

			if (loopCount < readSsapApplicationList.size()) {
				loopCount++;
				readerFlag = true;
				readerSsapApplication = readSsapApplicationList.get(loopCount - 1);
			}
		}

		if (!readerFlag) {
			LOGGER.warn("Unable to find SSAP Application.");
		}
		return readerSsapApplication;
	}

	public CloneService getCloneService() {
		return cloneService;
	}

	public void setCloneService(CloneService cloneService) {
		this.cloneService = cloneService;
	}

	public ClonePartitionerParams getClonePartitionerParams() {
		return clonePartitionerParams;
	}

	public void setClonePartitionerParams(ClonePartitionerParams clonePartitionerParams) {
		this.clonePartitionerParams = clonePartitionerParams;
	}
}
