package com.getinsured.iex.hub.vlp.validator;

import java.text.ParseException;
import java.util.Map;

import javax.xml.datatype.DatatypeConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.InputDataValidationException;
import com.getinsured.iex.hub.platform.Validator;
import com.getinsured.iex.hub.vlp.service.VLPServiceConstants;
import com.getinsured.iex.hub.vlp.service.VLPServiceUtil;

/**
 * Validator for coverage start date
 * 
 * @author Nikhil Talreja
 * @since 05-Feb-2014
 *
 */
public class CoverageStartDateValidator extends Validator {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CoverageStartDateValidator.class);
	
	/** 
	 * Validations for coverage start date
	 * 1. Should be a valid date
	 * 2. Date format is YYYY-MM-DD
	 * 
	 * Required : Yes
	 * 
	 */
	public void validate(String name, Object value)
			throws InputDataValidationException {
		
		//Value is mandatory
		if(value == null){
			throw new InputDataValidationException(VLPServiceConstants.COVERAGE_DATE_REQUIRED_ERROR_MESSAGE);
		}
		
		LOGGER.debug("Validating " +name+" for value " + value);
		
		try{
			VLPServiceUtil.stringToXMLDate(value.toString(), VLPServiceConstants.VLP_SERVICE_DATE_FORMAT);
		}
		catch(ParseException e){
			throw new InputDataValidationException(VLPServiceConstants.DATE_ERROR_MESSAGE+". Expected format is " + VLPServiceConstants.VLP_SERVICE_DATE_FORMAT.toUpperCase(), e);
		} 
		catch (DatatypeConfigurationException e) {
			throw new InputDataValidationException(VLPServiceConstants.DATE_ERROR_MESSAGE+". Expected format is " + VLPServiceConstants.VLP_SERVICE_DATE_FORMAT.toUpperCase(), e);
		}
	}

	public void setContext(Map<String, Object> context) {
		//Context not required for this validator
	}
}
