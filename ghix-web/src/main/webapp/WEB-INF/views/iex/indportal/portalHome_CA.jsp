<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="encryptor" uri="/WEB-INF/tld/ghix-encryptor.tld" %>

<script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/babel-core/5.8.24/browser-polyfill.min.js"></script>

<script>
    $(document).ready(function() {
		var scope = angular.element(document.getElementById("portalHome")).scope();
		if (scope) {
			scope.$apply(function() {
                scope['portalHome'] = {title: '<spring:message code="indportal.portalhome.dashboard" javaScriptEscape="true"/>'};
			});
		}
	});
</script>
<script type="text/ng-template" id="portalHome">
    <div id="myReactID" data-react='{"user":{"name":"BTM","email":"example@example.com"}}'></div>
    <link rel="stylesheet"  href="<gi:cdnurl value="/resources/react-components/dist/index_bundle.css"/>">
    <script type="text/javascript" src="<gi:cdnurl value="/resources/react-components/dist/index_bundle.js"/>">
</script>
