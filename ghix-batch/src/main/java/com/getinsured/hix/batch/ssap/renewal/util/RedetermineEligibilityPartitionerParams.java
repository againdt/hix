package com.getinsured.hix.batch.ssap.renewal.util;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.springframework.stereotype.Component;

@Component
public class RedetermineEligibilityPartitionerParams {

	List<Long> ssapApplicationIdsList = null;

	String outboundAt;
	
	
	public String getOutboundAt() {
		return outboundAt;
	}

	public void setOutboundAt(String outboundAt) {
		this.outboundAt = outboundAt;
	}

	public RedetermineEligibilityPartitionerParams() {
		this.ssapApplicationIdsList = new CopyOnWriteArrayList<Long>();
	}

	public List<Long> getSsapApplicationIdList() {
		return ssapApplicationIdsList;
	}

	public synchronized void clearSsapApplicationIdList() {
		this.ssapApplicationIdsList.clear();
	}

	public synchronized void addAllToSsapApplicationIdList(List<Long> enrollmentIdList) {
		this.ssapApplicationIdsList.addAll(enrollmentIdList);
	}

	public synchronized void addToSsapApplicationIdList(Long ssapApplicationId) {
		this.ssapApplicationIdsList.add(ssapApplicationId);
	}
}

