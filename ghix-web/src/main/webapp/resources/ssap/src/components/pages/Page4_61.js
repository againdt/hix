import React, { Fragment, useEffect, useState } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Components

import InstructionalText from '../notifications/InstructionalText';
import Form from './Form';
import UniversalInput from '../../lib/UniversalInput';
import ValidationErrorMessage from '../notifications/ValidationErrorMessage';

// ActionCreators

import { changeData } from '../../actions/actionCreators';

// Utils

import normalizeNameOfPerson from '../../utils/normalizeNameOfPerson';
import cloneObject from '../../utils/cloneObject';
import { getCoverageProgramName } from '../../utils/getCoverageProgramName';


// Constants

import INPUT_TYPES from '../../constants/inputTypes';
import DROPDOWN_OPTIONS from '../../constants/dropdownOptions';
import {OTHER_PROGRAMS_LIST} from '../../constants/otherProgramsList';
import VALIDATION_TYPES from '../../constants/validationTypes';





/**
 *
 * @param {Object} props
 * @returns {React.Component} - 'Additional Information - Member Enrolled In Other Health Insurance
 *
 */
const Page4_61 = props => {

    const {
        data,
        householdMembers,
        form: {
            Form: {
                isFormSubmitted
            }
        }
    } = props;

    const {
        hhmIndex : member,
        currentHhm
    } = props;

    const {
        healthCoverage : {
            currentlyHasHealthInsuranceIndicator
        },
        name
    } = currentHhm;

    const currentEmployerInsurance = (
        currentEmployerArr =>
            currentEmployerArr.length > 0 ?
                currentEmployerArr[0].currentEmployerInsurance :
                {})(householdMembers[member].healthCoverage.currentEmployer);

    const otherInsurance = householdMembers[member]
        .healthCoverage
        .otherInsurance;

    // Local State of Functional Component
    const [ localState, setLocalState ] = useState({
        otherProgramTypes: prepareValuesForOtherProgramTypes(),
        hasNonESI: false
    });

    const { otherProgramTypes} = localState;

    console.log(localState.otherProgramTypes.map(pgm => Object.values(pgm).toLocaleString()).toLocaleString());
    console.log(localState.hasNonESI);
    useEffect(() => {
        const newState = { ...data };
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .healthCoverage
            .currentOtherInsurance
            .otherStateOrFederalPrograms = localState.otherProgramTypes;
        newState
            .singleStreamlinedApplication
            .taxHousehold[0]
            .householdMember[member]
            .healthCoverage
            .currentOtherInsurance
            .hasNonESI = localState.hasNonESI;
        props.actions.changeData(newState);
    }, [localState.otherProgramTypes.map(pgm => Object.values(pgm).toLocaleString()).toLocaleString()]);

    function prepareValuesForOtherProgramTypes() {
        return OTHER_PROGRAMS_LIST.map(program => {
            const matchingProgram = householdMembers[member]
                .healthCoverage
                .currentOtherInsurance
                .otherStateOrFederalPrograms
                .find(type => type.name === program.name);
            return {
                ...program,
                eligible : matchingProgram ? matchingProgram.eligible : false
            };
        });
    }

    const onFieldChange = newData => {
        setLocalState(state => {
            return {
                ...state
            };
        });
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member] = {...newData};
        props.actions.changeData(newState);
    };

    const onFieldChange_programType = (...args) => {
        const [ newData, , , [ name, value ] ] = args;
        console.log(name, value);
        if (newData.name !== 'None Of the Above') {
            setLocalState(state => {
                const updated = state.otherProgramTypes.map(type => type.name === newData.name ?
                    {
                        ...newData,
                        eligible: !newData.eligible
                    } : type.name === 'None Of the Above' ?
                        {
                            ...type,
                            eligible: null
                        } : type
                );
                return {
                    ...state,
                    otherProgramTypes: updated,
                    hasNonESI: updated.filter(pgm => pgm.eligible && pgm.type !== 'OTHER_COVERAGE').length > 0
                };
            });
        } else {
            setLocalState(state => {
                return {
                    ...state,
                    otherProgramTypes: state.otherProgramTypes.map(program => {
                        if (program.name !== newData.name) {
                            return {
                                ...program,
                                eligible: null
                            };
                        } else {
                            return {
                                ...newData,
                                eligible: !newData.eligible
                            };
                        }

                    }),
                    hasNonESI: false
                };
            });
        }
    };

    const onFieldChange_otherInsurance = newData => {
        const newState = {...data };
        newState.singleStreamlinedApplication.taxHousehold[0].householdMember[member].healthCoverage
            .otherInsurance = newData;
        props.actions.changeData(newState);

    };
    // Iterating through all household members and providing for rendering data
    // UI is bound to Local State if household member is checked or not

    const checkTopLevelValidation = () => {
        //return true;
        const {
            currentlyHasHealthInsuranceIndicator
        } = householdMembers[member].healthCoverage;
        return !currentlyHasHealthInsuranceIndicator ? true  : localState.otherProgramTypes.some(pgm => pgm.eligible);
    };
    return (
        <Fragment>
            <Form topLevelValidity={checkTopLevelValidation()}>
                <div id="aiOtherInsurance" className="fade-in">
                    <div className="subsection">
                        <UniversalInput
                            legend={{
                                legendText: <span>
                                    Is &nbsp;
                                    <strong>
                                        {normalizeNameOfPerson(name)} &nbsp;
                                    </strong>
                                    currently enrolled in health coverage?
                                </span>,
                                dataInstructions: 'coverage_currently_enrolled'
                            }}
                            label={{
                                text: '',
                                labelClass: ['usa-width-one-whole', 'layout--02'],
                                ulClass: ['usa-width-one-whole'],
                                showRequiredIcon: true
                            }}
                            inputData={{
                                isInputRequired: true,
                                type: INPUT_TYPES.RADIO_BUTTONS,
                                name: 'currentlyHasHealthInsuranceIndicator',
                                value: currentlyHasHealthInsuranceIndicator,
                                options: DROPDOWN_OPTIONS.YES_NO,
                                currentData: householdMembers[member],
                                validationType: VALIDATION_TYPES.CHOSEN,
                                fields: ['healthCoverage']

                            }}
                            inputActions={{
                                onFieldChange
                            }}
                        />
                        {/**********************************************************************************************************************
                         Begin - Were any of these people found not eligible for Medicaid or Nevada Check Up (CHIP) in the past 90 days? (YES)
                         ***********************************************************************************************************************/}
                        {
                            currentlyHasHealthInsuranceIndicator &&
                            <Fragment>
                                <ValidationErrorMessage
                                    isVisible={isFormSubmitted && !checkTopLevelValidation()}
                                    messageText='Please select the type of coverage you are currently enrolled'
                                    errorClasses={['margin-l-20-ve']}
                                />
                                <div className="usa-width-one-whole margin-t-30">
                                    <fieldset className="usa-fieldset-inputs usa-sans">
                                        <legend
                                            data-instructions="coverage_type"
                                            className="gi-checkbox margin-b-10"
                                        >
                                            <span className="required-true">
                                                What type of coverage does <strong>{normalizeNameOfPerson(name)} &nbsp;</strong>have?
                                            </span>
                                            <InstructionalText
                                                Component={
                                                    <span data-instructions="coverage_type"/>
                                                }
                                                replacements={{
                                                    stateMedicaidProgramName: getCoverageProgramName('MEDICAID')
                                                }}
                                            />
                                        </legend>


                                        <ul className="usa-unstyled-list">
                                            {
                                                otherProgramTypes.map(program => {
                                                    return (
                                                        <Fragment key={program.type}>
                                                            <li>
                                                                <UniversalInput
                                                                    label={{
                                                                        text: program.name
                                                                    }}
                                                                    inputData={{
                                                                        isInputRequired: false,
                                                                        classes: ['layout--01'],
                                                                        type: INPUT_TYPES.CHECKBOX,
                                                                        name: program.type,
                                                                        checked: program.eligible,
                                                                        value: program.eligible,
                                                                        currentData: cloneObject(program),
                                                                        fields: ['healthCoverage']
                                                                    }}
                                                                    inputActions={{
                                                                        onFieldChange: onFieldChange_programType
                                                                    }}
                                                                />
                                                            </li>
                                                            {/**********************************************************************************************************************
                                                             Begin - Employer Insurance (Yes)
                                                             ***********************************************************************************************************************/}
                                                            {
                                                                program && program.name === 'Other Coverage' && program.eligible &&

                                                                <Fragment>
                                                                    <li>
                                                                        <UniversalInput
                                                                            label={{
                                                                                text: 'Insurance Name',
                                                                                labelClasses: ['spl__width--01']
                                                                            }}
                                                                            inputData={{
                                                                                isInputRequired: false,
                                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                                name: 'insuranceName',
                                                                                helperClasses: [],
                                                                                value: otherInsurance.insuranceName,
                                                                                placeholder: 'Enter Insurance Name',
                                                                                validationType: VALIDATION_TYPES.NOT_EMPTY,
                                                                                currentData: otherInsurance,
                                                                                fields: []
                                                                            }}
                                                                            inputActions={{
                                                                                onFieldChange: onFieldChange_otherInsurance
                                                                            }}
                                                                        />
                                                                    </li>
                                                                    <li>
                                                                        <UniversalInput
                                                                            label={{
                                                                                text: 'Policy Number',
                                                                                labelClasses: ['spl__width--01']
                                                                            }}
                                                                            inputData={{
                                                                                isInputRequired: false,
                                                                                type: INPUT_TYPES.REGULAR_INPUT,
                                                                                name: 'policyNumber',
                                                                                helperClasses: [],
                                                                                value: program.name === 'Employer Insurance' ? currentEmployerInsurance.policyNumber : otherInsurance.policyNumber,
                                                                                placeholder: 'Enter Policy Number',
                                                                                validationType: VALIDATION_TYPES.NOT_EMPTY,
                                                                                currentData: program.name === 'Employer Insurance' ? currentEmployerInsurance : otherInsurance,
                                                                                fields: []
                                                                            }}
                                                                            inputActions={{
                                                                                onFieldChange: onFieldChange_otherInsurance
                                                                            }}
                                                                        />
                                                                    </li>
                                                                    {
                                                                        program && program.name === 'Other Coverage' && program.eligible &&

                                                                        <Fragment>
                                                                            <UniversalInput
                                                                                legend={{
                                                                                    legendText: 'Is this a limited benefit coverage?'
                                                                                }}
                                                                                label={{
                                                                                    text: '',
                                                                                    labelClass: ['usa-width-one-whole', 'layout--02', 'offset-2'],
                                                                                    ulClass: ['usa-width-one-whole offset-4'],
                                                                                    showRequiredIcon: false
                                                                                }}
                                                                                inputData={{
                                                                                    isInputRequired: false,
                                                                                    type: INPUT_TYPES.RADIO_BUTTONS,
                                                                                    name: 'limitedBenefit',
                                                                                    value: otherInsurance.limitedBenefit,
                                                                                    options: DROPDOWN_OPTIONS.YES_NO,
                                                                                    currentData: otherInsurance,
                                                                                    fields: []

                                                                                }}
                                                                                inputActions={{
                                                                                    onFieldChange: onFieldChange_otherInsurance
                                                                                }}
                                                                            />
                                                                        </Fragment>
                                                                    }

                                                                </Fragment>
                                                            }
                                                            {/**********************************************************************************************************************
                                                             End - Employer Insurance (Yes)
                                                             ***********************************************************************************************************************/}
                                                        </Fragment>
                                                    );
                                                })
                                            }
                                        </ul>
                                    </fieldset>
                                </div>
                            </Fragment>
                        }
                        {/**********************************************************************************************************************
                         End - Were any of these people found not eligible for Medicaid or Nevada Check Up (CHIP) in the past 90 days? (YES)
                         ***********************************************************************************************************************/}
                    </div>
                </div>
            </Form>
        </Fragment>
    );
};

const mapStateToProps = state => ({
    data: state.data,
    form: state.form,

    householdMembers: state
        .data
        .singleStreamlinedApplication
        .taxHousehold[0]
        .householdMember
});

const mapDispatchToProps = dispatch => ({
    actions: bindActionCreators({
        changeData
    }, dispatch)
});

Page4_61.defaultProps = {
    name: 'Page4_61'
};

export default connect(mapStateToProps, mapDispatchToProps)(Page4_61);
