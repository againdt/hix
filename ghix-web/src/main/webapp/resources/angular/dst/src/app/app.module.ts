import { environment } from './../environments/environment';
import { DrugSearchTypeaheadComponent } from './components/drug-search-typeahead/drug-search-typeahead.component';
import { DocSearchTypeaheadComponent } from './components/doc-search-typeahead/doc-search-typeahead.component';
import { DrugCardComponent } from './components/drug-card/drug-card.component';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/delayWhen';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/take';
import { AppComponent } from './app.component';
import { DoctorCardComponent } from './components/doctor-card/doctor-card.component';
import { DocVisitComponent } from './components/forms/doc-visit/doc-visit.component';
import { DocWhoComponent } from './components/forms/doc-who/doc-who.component';
import { DrugWhichComponent } from './components/forms/drug-which/drug-which.component';
import { PrescriptionSearchComponent } from './components/forms/prescription-search/prescription-search.component';
import { SubmitSetComponent } from './components/forms/submit-set/submit-set.component';
import { HeaderComponent } from './components/header/header.component';
import { InputGroupComponents } from './components/input-group/input-group.component';
import { MainComponent } from './components/main/main.component';
import { DataKeepService } from './services/data-keep.service';
import { DefaultRequestOptions } from './services/default-request-options.service';
import { FetchService } from './services/fetch.service';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions, JsonpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { CapFirstPipe } from './pipes/cap-first.pipe';
import { OrderBy } from './pipes/orderBy.pipe';
import { TranslateModule, TranslateLoader } from "@ngx-translate/core";
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { DocOrFac } from "./pipes/docOrFac.pipe";
import { ErrorScreenComponent } from './components/error-screen/error-screen.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';

export function HttpLoaderFactory(http: HttpClient) {
    if (environment.production) {
        return new TranslateHttpLoader(http, '/hix/resources/angular/dst/dist/assets/i18n/', '.json');
    }else{
        return new TranslateHttpLoader(http, './assets/i18n/', '.json');
    }
}

/*

translation library documentation
https://www.npmjs.com/package/@ngx-translate/core

*/
const appRoutes: Routes = [
    {
        path: "dst", component: MainComponent
    },
    {
        path: "**", component: MainComponent
    }
]

@NgModule({
    declarations: [
        // components
        AppComponent,
            MainComponent,
                HeaderComponent,
                DocVisitComponent,
                PrescriptionSearchComponent,
                DocWhoComponent,
                DrugWhichComponent,
                SubmitSetComponent,
                ErrorScreenComponent,

        // shared small components
        InputGroupComponents,
        DocSearchTypeaheadComponent,
        DrugSearchTypeaheadComponent,
        DoctorCardComponent,
        DrugCardComponent,
        NavbarComponent,
        FooterComponent,

        // directive

        // pipes
        CapFirstPipe,
        DocOrFac,
        OrderBy,
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        HttpClientModule,
        BrowserAnimationsModule,
        BrowserModule,
        FormsModule,
        HttpModule,
        NgbModule.forRoot(),
        JsonpModule,
    ],
    providers: [
        { provide: RequestOptions, useClass: DefaultRequestOptions },
        // services
        DataKeepService,
        FetchService,
    ],
    bootstrap: [AppComponent],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }

