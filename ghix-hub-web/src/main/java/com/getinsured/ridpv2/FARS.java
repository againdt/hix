package com.getinsured.ridpv2;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.getinsured.iex.hub.platform.HubServiceBridge;

/**
 * Service handler for FARS Service
 * 
 * @author Nikhil Talreja
 * @since 20-Mar-2014
 *
 */
public final class FARS {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(RIDPPrimary.class);
	
	private FARS(){
		
	}
	
	public static void main(String[] args) {
		try{
			HubServiceBridge bridge = HubServiceBridge.getHubServiceBridge("FARSRequest");
			bridge.getServiceHandler().setJsonInput("{\"dshReferenceNumber\":\"c3c8-9e-6097\"}");
			
			Object payLoad = bridge.getServiceHandler().getRequestpayLoad();
			
			if(payLoad != null){
				JAXBContext jc = JAXBContext.newInstance("com.getinsured.iex.hub.ridp.fars.cms.dsh.ridp.exchange._1");
				Marshaller m = jc.createMarshaller();
				m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
				m.setProperty("com.sun.xml.bind.namespacePrefixMapper", new RIDPNamespaceMapper());
				//m.marshal(payLoad, System.out);
			}
		}
		catch(Exception e){
			LOGGER.error(e.getMessage(),e);
		}
	}

}
