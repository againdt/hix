import React from 'react';

function HealthCoverageStatus({header, headingType, status}) {

    return (
        <div className="ds-u-margin--2">
            <div className="ds-l-row ds-u-margin--1">
                <span>
                    <span className="ds-u-font-weight--semibold" id="further-action-header">{header}: </span>
                    <span id="further-action-header-status">{status}</span>
                </span>
            </div>
        </div>
    )
}

export default HealthCoverageStatus